﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomReportSelection.
	/// </summary>
	partial class frmCustomReportSelection : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraRangeSelection;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCButton cmdCancelRange;
		public fecherFoundation.FCComboBox cboReports;
		public fecherFoundation.FCButton mnuFileDelete;
		public fecherFoundation.FCButton mnuFileSetup;
		public fecherFoundation.FCButton cmdFilePrint;
		public fecherFoundation.FCButton cmdFilePreview;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReportSelection));
			this.fraRangeSelection = new fecherFoundation.FCFrame();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.cboEndingMonth = new fecherFoundation.FCComboBox();
			this.cboBeginningMonth = new fecherFoundation.FCComboBox();
			this.cboSingleMonth = new fecherFoundation.FCComboBox();
			this.lblTo_0 = new fecherFoundation.FCLabel();
			this.cmdCancelRange = new fecherFoundation.FCButton();
			this.cboReports = new fecherFoundation.FCComboBox();
			this.mnuFileDelete = new fecherFoundation.FCButton();
			this.mnuFileSetup = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRangeSelection)).BeginInit();
			this.fraRangeSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboReports);
			this.ClientArea.Controls.Add(this.fraRangeSelection);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.mnuFileSetup);
            this.TopPanel.Controls.Add(this.mnuFileDelete);
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(267, 30);
			this.HeaderText.Text = "Report Writer Selection";
			// 
			// fraRangeSelection
			// 
			this.fraRangeSelection.AppearanceKey = "groupBoxLeftBorder";
			this.fraRangeSelection.Controls.Add(this.fraDateRange);
			this.fraRangeSelection.Controls.Add(this.cmdCancelRange);
			this.fraRangeSelection.Location = new System.Drawing.Point(30, 30);
			this.fraRangeSelection.Name = "fraRangeSelection";
			this.fraRangeSelection.Size = new System.Drawing.Size(432, 208);
			this.fraRangeSelection.TabIndex = 0;
			this.fraRangeSelection.Text = "Criteria Selection";
			this.fraRangeSelection.Visible = false;
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.cboEndingMonth);
			this.fraDateRange.Controls.Add(this.cboBeginningMonth);
			this.fraDateRange.Controls.Add(this.cboSingleMonth);
			this.fraDateRange.Controls.Add(this.lblTo_0);
			this.fraDateRange.Location = new System.Drawing.Point(20, 30);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(412, 90);
			this.fraDateRange.TabIndex = 1;
			this.fraDateRange.Text = "Date Range";
			// 
			// cboEndingMonth
			// 
			this.cboEndingMonth.AutoSize = false;
			this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingMonth.FormattingEnabled = true;
			this.cboEndingMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboEndingMonth.Location = new System.Drawing.Point(232, 30);
			this.cboEndingMonth.Name = "cboEndingMonth";
			this.cboEndingMonth.Size = new System.Drawing.Size(160, 40);
			this.cboEndingMonth.TabIndex = 4;
			this.cboEndingMonth.Visible = false;
			// 
			// cboBeginningMonth
			// 
			this.cboBeginningMonth.AutoSize = false;
			this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningMonth.FormattingEnabled = true;
			this.cboBeginningMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboBeginningMonth.Location = new System.Drawing.Point(20, 30);
			this.cboBeginningMonth.Name = "cboBeginningMonth";
			this.cboBeginningMonth.Size = new System.Drawing.Size(160, 40);
			this.cboBeginningMonth.TabIndex = 3;
			this.cboBeginningMonth.Visible = false;
			// 
			// cboSingleMonth
			// 
			this.cboSingleMonth.AutoSize = false;
			this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleMonth.FormattingEnabled = true;
			this.cboSingleMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboSingleMonth.Location = new System.Drawing.Point(20, 30);
			this.cboSingleMonth.Name = "cboSingleMonth";
			this.cboSingleMonth.Size = new System.Drawing.Size(160, 40);
			this.cboSingleMonth.TabIndex = 2;
			this.cboSingleMonth.Visible = false;
			// 
			// lblTo_0
			// 
			this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_0.Location = new System.Drawing.Point(198, 44);
			this.lblTo_0.Name = "lblTo_0";
			this.lblTo_0.Size = new System.Drawing.Size(20, 23);
			this.lblTo_0.TabIndex = 5;
			this.lblTo_0.Text = "TO";
			this.lblTo_0.Visible = false;
			// 
			// cmdCancelRange
			// 
			this.cmdCancelRange.AppearanceKey = "actionButton";
			this.cmdCancelRange.Location = new System.Drawing.Point(20, 140);
			this.cmdCancelRange.Name = "cmdCancelRange";
			this.cmdCancelRange.Size = new System.Drawing.Size(73, 48);
			this.cmdCancelRange.TabIndex = 7;
			this.cmdCancelRange.Text = "Cancel";
			this.cmdCancelRange.Click += new System.EventHandler(this.cmdCancelRange_Click);
			// 
			// cboReports
			// 
			this.cboReports.AutoSize = false;
			this.cboReports.BackColor = System.Drawing.SystemColors.Window;
			this.cboReports.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReports.FormattingEnabled = true;
			this.cboReports.Location = new System.Drawing.Point(30, 30);
			this.cboReports.Name = "cboReports";
			this.cboReports.Size = new System.Drawing.Size(302, 40);
			this.cboReports.TabIndex = 8;
			this.cboReports.DropDown += new System.EventHandler(this.cboReports_DropDown);
            // 
            // mnuFileDelete
            // 
            this.mnuFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.mnuFileDelete.AppearanceKey = "toolbarButton";
			this.mnuFileDelete.Name = "mnuFileDelete";
            this.mnuFileDelete.Size = new System.Drawing.Size(54, 24);
            this.mnuFileDelete.TabIndex = 1;
            this.mnuFileDelete.Location = new System.Drawing.Point(831, 28);
            this.mnuFileDelete.Text = "Delete";
			this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // mnuFileSetup
            // 
            this.mnuFileSetup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.mnuFileSetup.AppearanceKey = "toolbarButton";
			this.mnuFileSetup.Name = "mnuFileSetup";
            this.mnuFileSetup.Size = new System.Drawing.Size(94, 24);
            this.mnuFileSetup.TabIndex = 3;
            this.mnuFileSetup.Location = new System.Drawing.Point(935, 28);
            this.mnuFileSetup.Text = "Setup Report";
			this.mnuFileSetup.Click += new System.EventHandler(this.mnuFileSetup_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(999, 28);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(44, 24);
			this.cmdFilePrint.TabIndex = 5;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(274, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(120, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.cmdFilePreview_Click);
			// 
			// frmCustomReportSelection
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCustomReportSelection";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Report Writer Selection";
			this.Load += new System.EventHandler(this.frmCustomReportSelection_Load);
			this.Activated += new System.EventHandler(this.frmCustomReportSelection_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomReportSelection_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRangeSelection)).EndInit();
			this.fraRangeSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
