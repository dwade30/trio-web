﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReportSetup.
	/// </summary>
	public partial class frmReportSetup : BaseForm
	{
		public frmReportSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbExpSummary.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReportSetup InstancePtr
		{
			get
			{
				return (frmReportSetup)Sys.GetInstance(typeof(frmReportSetup));
			}
		}

		protected frmReportSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/8/02
		// This form will be used to create new reports, delete reports
		// or edit reports for the summary an detail reports
		// ********************************************************
		int KeyCol;
		int DeleteCol;
		int ReportNameCol;
		int CriteriaCol;
		int FormatCol;
		bool blnDirty;
		bool blnUnload;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsCriteria = new clsDRWrapper();
			clsDRWrapper rsFormat = new clsDRWrapper();
			string strComboList;
			int counter = 0;
			//FC:TODO:AM
			//vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			// set up combo list for format column
			if (cmbExpSummary.SelectedIndex == 0)
			{
				rsFormat.OpenRecordset("SELECT * FROM ExpenseSummaryFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 3)
			{
				rsFormat.OpenRecordset("SELECT * FROM ExpenseDetailFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 1)
			{
				rsFormat.OpenRecordset("SELECT * FROM RevenueSummaryFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 6)
			{
				rsFormat.OpenRecordset("SELECT * FROM ProjectSummaryFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 4)
			{
				rsFormat.OpenRecordset("SELECT * FROM RevenueDetailFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 2)
			{
				rsFormat.OpenRecordset("SELECT * FROM LedgerSummaryFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 5)
			{
				rsFormat.OpenRecordset("SELECT * FROM LedgerDetailFormats ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 7)
			{
				rsFormat.OpenRecordset("SELECT * FROM ProjectDetailFormats ORDER BY Description");
			}
			strComboList = "";
			if (rsFormat.EndOfFile() != true && rsFormat.BeginningOfFile() != true)
			{
				do
				{
					strComboList += "'" + rsFormat.Get_Fields_Int32("ID") + ";" + rsFormat.Get_Fields_String("Description") + "|";
					rsFormat.MoveNext();
				}
				while (rsFormat.EndOfFile() != true);
			}
			if (strComboList != "")
			{
				strComboList = Strings.Left(strComboList, strComboList.Length - 1);
			}
			else
			{
				strComboList = "#0; ";
			}
			vs1.ColComboList(FormatCol, strComboList);
			// set up combo list for criteria column
			if (cmbExpSummary.SelectedIndex == 0)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'ES' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 3)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'ED' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 1)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'RS' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 4)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'RD' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 2)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LS' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 5)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LD' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 6)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'PS' ORDER BY Description");
			}
			else if (cmbExpSummary.SelectedIndex == 7)
			{
				rsCriteria.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'PD' ORDER BY Description");
			}
			strComboList = "";
			if (rsCriteria.EndOfFile() != true && rsCriteria.BeginningOfFile() != true)
			{
				do
				{
					strComboList += "'" + rsCriteria.Get_Fields_Int32("ID") + ";" + rsCriteria.Get_Fields_String("Description") + "|";
					rsCriteria.MoveNext();
				}
				while (rsCriteria.EndOfFile() != true);
			}
			if (strComboList != "")
			{
				strComboList = Strings.Left(strComboList, strComboList.Length - 1);
			}
			else
			{
				strComboList = "#0; ";
			}
			vs1.ColComboList(CriteriaCol, strComboList);
			if (cmbExpSummary.SelectedIndex == 0)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'ES' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 3)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'ED' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 1)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'RS' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 4)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'RD' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 2)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'LS' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 5)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'LD' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 6)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'PS' ORDER BY Title");
			}
			else if (cmbExpSummary.SelectedIndex == 7)
			{
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'PD' ORDER BY Title");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				vs1.Rows = rs.RecordCount() + 1;
				counter = 1;
				do
				{
					vs1.TextMatrix(counter, KeyCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					vs1.TextMatrix(counter, DeleteCol, FCConvert.ToString(false));
					vs1.TextMatrix(counter, ReportNameCol, FCConvert.ToString(rs.Get_Fields_String("Title")));
					// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
					rsFormat.FindFirstRecord("ID", rs.Get_Fields("FormatID"));
					if (rsFormat.NoMatch != true)
					{
						vs1.TextMatrix(counter, FormatCol, FCConvert.ToString(rsFormat.Get_Fields_String("Description")));
					}
					rsCriteria.FindFirstRecord("ID", rs.Get_Fields_Int32("CriteriaID"));
					if (rsCriteria.NoMatch != true)
					{
						vs1.TextMatrix(counter, CriteriaCol, FCConvert.ToString(rsCriteria.Get_Fields_String("Description")));
					}
					counter += 1;
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			else
			{
				vs1.Rows = 2;
				vs1.TextMatrix(1, KeyCol, "0");
			}
			vs1.Visible = true;
			fraReportType.Visible = false;
			if (cmbExpSummary.SelectedIndex == 0)
			{
				lblType.Text = "Town Expense Summary Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 3)
			{
				lblType.Text = "Town Expense Detail Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 1)
			{
				lblType.Text = "Town Revenue Summary Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 4)
			{
				lblType.Text = "Town Revenue Detail Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 2)
			{
				lblType.Text = "Town Ledger Summary Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 5)
			{
				lblType.Text = "Town Ledger Detail Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 6)
			{
				lblType.Text = "Project Summary Reports";
			}
			else if (cmbExpSummary.SelectedIndex == 7)
			{
				lblType.Text = "Project Detail Reports";
			}
			Form_Resize();
			cmdChange.Enabled = true;
			cmdNew.Enabled = true;
			cmdDelete.Enabled = true;
			cmdSave.Enabled = true;
		}

		private void frmReportSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmReportSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmReportSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReportSetup.FillStyle	= 0;
			//frmReportSetup.ScaleWidth	= 9045;
			//frmReportSetup.ScaleHeight	= 7110;
			//frmReportSetup.LinkTopic	= "Form2";
			//frmReportSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			KeyCol = 0;
			DeleteCol = 1;
			ReportNameCol = 2;
			CriteriaCol = 3;
			FormatCol = 4;
			vs1.ColWidth(KeyCol, 0);
			vs1.ColWidth(DeleteCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0918544));
			vs1.ColWidth(ReportNameCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.289428));
			vs1.ColWidth(CriteriaCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.289428));
			vs1.ColWidth(FormatCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.289428));
			vs1.ColDataType(DeleteCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vs1.TextMatrix(0, DeleteCol, "Select");
			vs1.TextMatrix(0, ReportNameCol, "Report Name");
			vs1.TextMatrix(0, CriteriaCol, "Criteria");
			vs1.TextMatrix(0, FormatCol, "Format");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			blnDirty = false;
			if (!modBudgetaryMaster.Statics.ProjectFlag)
			{
				//optProjDetail.Enabled = false;
				if (cmbExpSummary.Items.Contains("Project Detail"))
				{
					cmbExpSummary.Items.Remove("Project Detail");
				}
				//optProjSummary.Enabled = false;
				if (cmbExpSummary.Items.Contains("Project Summary"))
				{
					cmbExpSummary.Items.Remove("Project Summary");
				}
			}
			cmbExpSummary.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmReportSetup_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(KeyCol, 0);
			vs1.ColWidth(DeleteCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0918544));
			vs1.ColWidth(ReportNameCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.289428));
			vs1.ColWidth(CriteriaCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.289428));
			vs1.ColWidth(FormatCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.289428));
			//FC:FINAL:BBE - Use Anchoring
			//if (vs1.Rows > 18)
			//{
			//    vs1.Height = (vs1.RowHeight(0) * 18) + 75;
			//}
			//else
			//{
			//    vs1.Height = (vs1.RowHeight(0) * vs1.Rows) + 75;
			//}
			//FC:FINAL:BBE - center frames in ClientArea
			fraReportType.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmReportSetup_Resize(this, new System.EventArgs());
		}

		private void frmReportSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void cmdChange_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("Any changes you have made will be lost if you do not save.  Do you wish to continue?", "Change Report Type?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.No)
				{
					return;
				}
				else
				{
					lblType.Text = "";
					vs1.Visible = false;
					fraReportType.Visible = true;
					cmdChange.Enabled = false;
					cmdNew.Enabled = false;
					cmdDelete.Enabled = false;
					cmdSave.Enabled = false;
				}
			}
			else
			{
				lblType.Text = "";
				vs1.Visible = false;
				fraReportType.Visible = true;
				cmdChange.Enabled = false;
				cmdNew.Enabled = false;
				cmdDelete.Enabled = false;
				cmdSave.Enabled = false;
			}
		}

		private void cmdCreate_Click(object sender, System.EventArgs e)
		{
			vs1.Rows += 1;
			vs1.TextMatrix(vs1.Rows - 1, KeyCol, "0");
			vs1.TextMatrix(vs1.Rows - 1, DeleteCol, FCConvert.ToString(false));
			Form_Resize();
			vs1.Row = vs1.Rows - 1;
			vs1.Col = ReportNameCol;
			vs1.EditCell();
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vs1.TextMatrix(counter, DeleteCol)) == true)
				{
					if (Strings.Trim(Strings.UCase(vs1.TextMatrix(counter, ReportNameCol))) == "DEFAULT")
					{
						MessageBox.Show("You may not delete the default report.  You must unselect the report to continue", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			ans = MessageBox.Show("Are you sure you wish to delete the selected report(s)?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				CheckAgain:
				;
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vs1.TextMatrix(counter, DeleteCol)) == true)
					{
						if (vs1.TextMatrix(counter, KeyCol) != "0")
						{
							rsDelete.OpenRecordset("SELECT * FROM Reports WHERE ID = " + vs1.TextMatrix(counter, KeyCol));
							rsDelete.Delete();
							rsDelete.Update();
						}
						vs1.RemoveItem(counter);
						goto CheckAgain;
					}
				}
				Form_Resize();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				blnDirty = true;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.Col == ReportNameCol)
			{
				vs1.EditMaxLength = 25;
			}
			else
			{
				vs1.EditMaxLength = 0;
			}
		}

		private void SetCustomFormColors()
		{
			// lblType.ForeColor = &HFF0000
		}

		private void SaveInfo()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			int counter;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (Strings.Trim(vs1.TextMatrix(counter, ReportNameCol)) != "" && Strings.Trim(vs1.TextMatrix(counter, CriteriaCol)) != "" && Strings.Trim(vs1.TextMatrix(counter, FormatCol)) != "")
				{
					if (vs1.TextMatrix(counter, KeyCol) != "0")
					{
						if (Strings.Trim(Strings.UCase(vs1.TextMatrix(counter, ReportNameCol))) == "DEFAULT")
						{
							goto CheckNextReport;
						}
						rsSave.OpenRecordset("SELECT * FROM Reports WHERE ID = " + vs1.TextMatrix(counter, KeyCol));
						rsSave.Edit();
					}
					else
					{
						rsSave.OpenRecordset("SELECT * FROM Reports WHERE ID = 0");
						rsSave.AddNew();
						vs1.TextMatrix(counter, KeyCol, FCConvert.ToString(rsSave.Get_Fields_Int32("ID")));
					}
					if (cmbExpSummary.SelectedIndex == 3)
					{
						rsSave.Set_Fields("Type", "ED");
					}
					else if (cmbExpSummary.SelectedIndex == 0)
					{
						rsSave.Set_Fields("Type", "ES");
					}
					else if (cmbExpSummary.SelectedIndex == 4)
					{
						rsSave.Set_Fields("Type", "RD");
					}
					else if (cmbExpSummary.SelectedIndex == 1)
					{
						rsSave.Set_Fields("Type", "RS");
					}
					else if (cmbExpSummary.SelectedIndex == 5)
					{
						rsSave.Set_Fields("Type", "LD");
					}
					else if (cmbExpSummary.SelectedIndex == 2)
					{
						rsSave.Set_Fields("Type", "LS");
					}
					else if (cmbExpSummary.SelectedIndex == 7)
					{
						rsSave.Set_Fields("Type", "PD");
					}
					else if (cmbExpSummary.SelectedIndex == 6)
					{
						rsSave.Set_Fields("Type", "PS");
					}
					rsSave.Set_Fields("Title", Strings.Trim(vs1.TextMatrix(counter, ReportNameCol)));
					vs1.Select(counter, CriteriaCol);
					vs1.EditCell();
					rsSave.Set_Fields("CriteriaID", vs1.ComboData(vs1.ComboIndex));
					vs1.Select(counter, FormatCol);
					vs1.EditCell();
					rsSave.Set_Fields("FormatID", vs1.ComboData(vs1.ComboIndex));
					blnDirty = false;
					rsSave.Update();
				}
				CheckNextReport:
				;
			}
            //FC:FINAL:AM:#3152 - add save message
            MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (blnUnload)
			{
				Close();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, e);
		}
	}
}
