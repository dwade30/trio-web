﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class clsACHBlockFiller
	{
		//=========================================================
		private int intRecordSize;
		private string strFillerChar = string.Empty;

		public clsACHBlockFiller() : base()
		{
			intRecordSize = 94;
			strFillerChar = "9";
		}

		public string FillerChar
		{
			get
			{
				string FillerChar = "";
				FillerChar = strFillerChar;
				return FillerChar;
			}
			set
			{
				strFillerChar = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short RecordSize
		{
			get
			{
				short RecordSize = 0;
				RecordSize = FCConvert.ToInt16(intRecordSize);
				return RecordSize;
			}
			set
			{
				intRecordSize = value;
			}
		}

		public string OutputLine()
		{
			string OutputLine = "";
			string strLine;
			if (strFillerChar == "")
				strFillerChar = "9";
			if (strFillerChar.Length > 1)
			{
				strFillerChar = "9";
			}
			strLine = Strings.StrDup(intRecordSize, strFillerChar);
			OutputLine = strLine;
			return OutputLine;
		}
	}
}
