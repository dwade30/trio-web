﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeExpenseDetailVendorSummary.
	/// </summary>
	public partial class frmCustomizeExpenseDetailVendorSummary : BaseForm
	{
		public frmCustomizeExpenseDetailVendorSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//FC:FINAL:BSE #3083 wrong default item
			this.cmbPendingDetail.SelectedIndex = 2;
			this.cmbPrint.SelectedIndex = 0;
			this.cmbShortDescriptions.SelectedIndex = 1;
			this.cmbWide.SelectedIndex = 0;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeExpenseDetailVendorSummary InstancePtr
		{
			get
			{
				return (frmCustomizeExpenseDetailVendorSummary)Sys.GetInstance(typeof(frmCustomizeExpenseDetailVendorSummary));
			}
		}

		protected frmCustomizeExpenseDetailVendorSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		public bool FromExp;
		bool blnSavedFormat;

		private void frmCustomizeExpenseDetailVendorSummary_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtDescription.Focus();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnExpenseDetailEdit && !modBudgetaryMaster.Statics.blnExpenseDetailReportEdit)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					cmbPrint.SelectedIndex =
                        FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Use")) == "P"
                            ? 0
                            : 1;
					cmbWide.SelectedIndex =
                        FCConvert.ToString(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("PaperWidth")) == "N"
                            ? 0
                            : 1;
					cmbShortDescriptions.SelectedIndex =
                        FCConvert.ToString(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("DescriptionLength")) == "L"
                            ? 1
                            : 0;
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					bool optPendingDetail = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingDetail"));
					bool optPendingSummary = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingSummary"));
					if (!optPendingDetail && !optPendingSummary)
					{
						cmbPendingDetail.SelectedIndex = 2;
					}
					else if (optPendingDetail)
					{
						cmbPendingDetail.SelectedIndex = 0;
					}
					else 
					{
						cmbPendingDetail.SelectedIndex = 1;
					}
				}
			}
		}

		private void frmCustomizeExpenseDetailVendorSummary_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeExpenseDetailVendorSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeExpenseDetailVendorSummary.FillStyle	= 0;
			//frmCustomizeExpenseDetailVendorSummary.ScaleWidth	= 9300;
			//frmCustomizeExpenseDetailVendorSummary.ScaleHeight	= 7245;
			//frmCustomizeExpenseDetailVendorSummary.LinkTopic	= "Form2";
			//frmCustomizeExpenseDetailVendorSummary.LockControls	= -1  'True;
			//frmCustomizeExpenseDetailVendorSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromExp)
			{
				FromExp = false;
				frmExpenseDetailVendorSummarySelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnExpenseDetailReportEdit)
					{
						modBudgetaryMaster.Statics.blnExpenseDetailReportEdit = false;
						frmExpenseDetailVendorSummarySelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmExpenseDetailVendorSummarySelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnExpenseDetailReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnExpenseDetailEdit)
			{
				modBudgetaryMaster.Statics.blnExpenseDetailEdit = false;
				frmGetExpenseDetail.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeExpenseDetailVendorSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM ExpenseDetailFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("YTDDebitCredit", false);
					rs.Set_Fields("YTDNet", true);
                    rs.Set_Fields("DescriptionLength", cmbShortDescriptions.SelectedIndex == 1 ? "L" : "S");
                    rs.Set_Fields("PendingDetail", cmbPendingDetail.SelectedIndex == 0);
					rs.Set_Fields("PendingSummary", cmbPendingDetail.SelectedIndex == 1);
					rs.Set_Fields("Printer", "O");
					rs.Set_Fields("Font", "S");
                    rs.Set_Fields("PaperWidth", cmbWide.SelectedIndex == 0 ? "N" : "L");
                    rs.Set_Fields("Use", cmbPrint.SelectedIndex == 0 ? "P" : "D");
                    rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExpenseDetailFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("YTDDebitCredit", false);
				rs.Set_Fields("YTDNet", true);
                rs.Set_Fields("DescriptionLength", cmbShortDescriptions.SelectedIndex == 1 ? "L" : "S");
                rs.Set_Fields("PendingDetail", cmbPendingDetail.SelectedIndex == 0);
				rs.Set_Fields("PendingSummary", cmbPendingDetail.SelectedIndex == 1);
				rs.Set_Fields("Printer", "O");
				rs.Set_Fields("Font", "S");
				rs.Set_Fields("PaperWidth", cmbWide.SelectedIndex == 0 ? "N" : "L");
                rs.Set_Fields("Use", cmbPrint.SelectedIndex == 0 ? "P" : "D");
                rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}
	}
}
