﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBDAccountInquiry.
	/// </summary>
	public partial class rptBDAccountInquiry : BaseSectionReport
	{
		public static rptBDAccountInquiry InstancePtr
		{
			get
			{
				return (rptBDAccountInquiry)Sys.GetInstance(typeof(rptBDAccountInquiry));
			}
		}

		protected rptBDAccountInquiry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBDAccountInquiry	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cBDAccountInquiryReport theReport;
		private int lngPage;
		private cAccountSummaryItem currAccount;

		public rptBDAccountInquiry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Inquiry";
		}

		public void Init(ref cBDAccountInquiryReport repObj)
		{
			theReport = repObj;
			theReport.Accounts.MoveFirst();
			frmReportViewer.InstancePtr.Init(this);
			lngPage = 1;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (theReport.Accounts.IsCurrent())
			{
				currAccount = (cAccountSummaryItem)theReport.Accounts.GetCurrentItem();
				eArgs.EOF = false;
				theReport.Accounts.MoveNext();
			}
			else
			{
				currAccount = null;
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Label8.Text = modGlobalConstants.Statics.MuniName;
			Label10.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblUser.Text = modGlobalConstants.Statics.clsSecurityClass.Get_UserName();
			lblSubtitle1.Text = "";
			lblSubTitle2.Text = "";
			string strFilter = "";
			string strTemp = "";
			string strSepar;
			int x;
			strSepar = "";
			if (!(theReport == null))
			{
				if (theReport.AccountTypeToShow > 0)
				{
					switch (theReport.AccountTypeToShow)
					{
						case 1:
							{
								strFilter = "E ";
								break;
							}
						case 2:
							{
								strFilter = "R ";
								break;
							}
						case 3:
							{
								strFilter = "G ";
								break;
							}
					}
					//end switch
					for (x = 1; x <= 4; x++)
					{
						if (theReport.GetSegmentLength(x) > 0)
						{
							strTemp = theReport.GetSegmentFilter(x);
							if (strTemp == "")
							{
								strTemp = Strings.StrDup(theReport.GetSegmentLength(x), "X");
							}
							strFilter += strSepar + strTemp;
							strSepar = "-";
						}
					}
					// x
					lblSubtitle1.Text = strFilter;
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(currAccount == null))
			{
				txtAccount.Text = currAccount.Account;
				txtAccountDescription.Text = currAccount.ShortDescription;
				txtBegBalanceBudget.Text = Strings.Format(currAccount.BeginningNet, "#,###,###,###,##0.00");
				txtDebits.Text = Strings.Format(currAccount.Debits, "#,###,###,###,##0.00");
				txtCredits.Text = Strings.Format(currAccount.Credits, "#,###,###,###,##0.00");
				txtNet.Text = Strings.Format(currAccount.Net, "#,###,###,###,##0.00");
				txtEndBalanceBudget.Text = Strings.Format(currAccount.EndBalance, "#,###,###,###,##0.00");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		private void rptBDAccountInquiry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBDAccountInquiry.Caption	= "Account Inquiry";
			//rptBDAccountInquiry.Left	= 0;
			//rptBDAccountInquiry.Top	= 0;
			//rptBDAccountInquiry.Width	= 19260;
			//rptBDAccountInquiry.Height	= 8445;
			//rptBDAccountInquiry.StartUpPosition	= 3;
			//rptBDAccountInquiry.SectionData	= "rptBDAccountInquiry.dsx":0000;
			//End Unmaped Properties
		}
	}
}
