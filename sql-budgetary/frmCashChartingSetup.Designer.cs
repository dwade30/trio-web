﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCashChartingSetup.
	/// </summary>
	partial class frmCashChartingSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbYTDTotals;
		public fecherFoundation.FCLabel lblYTDTotals;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraGraphItem;
		public fecherFoundation.FCFrame fraExpense;
		public fecherFoundation.FCButton cmdExpenseClear;
		public fecherFoundation.FCButton cmdExpenseSelectAll;
		public fecherFoundation.FCComboBox cboExpenseSelection;
		public fecherFoundation.FCFrame fraExpenseAccountRange;
		public fecherFoundation.FCComboBox cboBeginningExpense;
		public fecherFoundation.FCComboBox cboEndingExpense;
		public fecherFoundation.FCComboBox cboSingleExpense;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public FCGrid vsHighAccount;
		public FCGrid vsLowAccount;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCGrid vsSelectedExpenses;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraCash;
		public fecherFoundation.FCButton cmdLiabilityClear;
		public fecherFoundation.FCButton cmdLiabilitySelectAll;
		public fecherFoundation.FCButton cmdCashClear;
		public fecherFoundation.FCButton cmdCashSelectAll;
		public fecherFoundation.FCComboBox cboCashSelection;
		public fecherFoundation.FCGrid vsSelectedCash;
		public fecherFoundation.FCGrid vsSelectedLiabilities;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblLiability;
		public fecherFoundation.FCFrame fraRevenue;
		public fecherFoundation.FCButton cmdRevenueClear;
		public fecherFoundation.FCButton cmdRevenueSelectAll;
		public fecherFoundation.FCComboBox cboRevenueSelection;
		public fecherFoundation.FCFrame fraRevenueAccountRange;
		public fecherFoundation.FCComboBox cboSingleRev;
		public fecherFoundation.FCComboBox cboEndingRev;
		public fecherFoundation.FCComboBox cboBeginningRev;
		public FCGrid vsLowRevAccount;
		public FCGrid vsHighRevAccount;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCGrid vsSelectedRevenues;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraTax;
		public fecherFoundation.FCButton cmdTaxClear;
		public fecherFoundation.FCButton cmdTaxSelectAll;
		public fecherFoundation.FCGrid vsSelectedTax;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCTextBox txtTitle;
		public fecherFoundation.FCComboBox cboGraphType;
		public fecherFoundation.FCButton cmdCreate;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCFrame fraExistingItem;
		public fecherFoundation.FCComboBox cboExistingItems;
		public fecherFoundation.FCButton cmdAddExisting;
		public fecherFoundation.FCButton cmdCancelAdd;
		public fecherFoundation.FCGrid vsGraph;
		public fecherFoundation.FCTextBox txtChartTitle;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCButton cmdGetExisting;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCashChartingSetup));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmbYTDTotals = new fecherFoundation.FCComboBox();
            this.lblYTDTotals = new fecherFoundation.FCLabel();
            this.fraGraphItem = new fecherFoundation.FCFrame();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.cboGraphType = new fecherFoundation.FCComboBox();
            this.cmdCreate = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblAccountTitle = new fecherFoundation.FCLabel();
            this.fraExpense = new fecherFoundation.FCFrame();
            this.fraExpenseAccountRange = new fecherFoundation.FCFrame();
            this.cboBeginningExpense = new fecherFoundation.FCComboBox();
            this.cboEndingExpense = new fecherFoundation.FCComboBox();
            this.cboSingleExpense = new fecherFoundation.FCComboBox();
            this.cboBeginningDept = new fecherFoundation.FCComboBox();
            this.cboEndingDept = new fecherFoundation.FCComboBox();
            this.cboSingleDept = new fecherFoundation.FCComboBox();
            this.vsHighAccount = new fecherFoundation.FCGrid();
            this.vsLowAccount = new fecherFoundation.FCGrid();
            this.lblTo_2 = new fecherFoundation.FCLabel();
            this.cmdExpenseClear = new fecherFoundation.FCButton();
            this.cmdExpenseSelectAll = new fecherFoundation.FCButton();
            this.cboExpenseSelection = new fecherFoundation.FCComboBox();
            this.vsSelectedExpenses = new fecherFoundation.FCGrid();
            this.Label3 = new fecherFoundation.FCLabel();
            this.fraRevenue = new fecherFoundation.FCFrame();
            this.cmdRevenueClear = new fecherFoundation.FCButton();
            this.cmdRevenueSelectAll = new fecherFoundation.FCButton();
            this.cboRevenueSelection = new fecherFoundation.FCComboBox();
            this.fraRevenueAccountRange = new fecherFoundation.FCFrame();
            this.cboSingleRev = new fecherFoundation.FCComboBox();
            this.cboEndingRev = new fecherFoundation.FCComboBox();
            this.cboBeginningRev = new fecherFoundation.FCComboBox();
            this.vsLowRevAccount = new fecherFoundation.FCGrid();
            this.vsHighRevAccount = new fecherFoundation.FCGrid();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.vsSelectedRevenues = new fecherFoundation.FCGrid();
            this.Label4 = new fecherFoundation.FCLabel();
            this.fraCash = new fecherFoundation.FCFrame();
            this.cmdLiabilityClear = new fecherFoundation.FCButton();
            this.cmdLiabilitySelectAll = new fecherFoundation.FCButton();
            this.cmdCashClear = new fecherFoundation.FCButton();
            this.cmdCashSelectAll = new fecherFoundation.FCButton();
            this.cboCashSelection = new fecherFoundation.FCComboBox();
            this.vsSelectedCash = new fecherFoundation.FCGrid();
            this.vsSelectedLiabilities = new fecherFoundation.FCGrid();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblLiability = new fecherFoundation.FCLabel();
            this.fraTax = new fecherFoundation.FCFrame();
            this.cmdTaxClear = new fecherFoundation.FCButton();
            this.cmdTaxSelectAll = new fecherFoundation.FCButton();
            this.vsSelectedTax = new fecherFoundation.FCGrid();
            this.Label8 = new fecherFoundation.FCLabel();
            this.fraExistingItem = new fecherFoundation.FCFrame();
            this.cboExistingItems = new fecherFoundation.FCComboBox();
            this.cmdAddExisting = new fecherFoundation.FCButton();
            this.cmdCancelAdd = new fecherFoundation.FCButton();
            this.vsGraph = new fecherFoundation.FCGrid();
            this.txtChartTitle = new fecherFoundation.FCTextBox();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmdGetExisting = new fecherFoundation.FCButton();
            this.fraMonths = new fecherFoundation.FCFrame();
            this.cboEndingMonth = new fecherFoundation.FCComboBox();
            this.cboBeginningMonth = new fecherFoundation.FCComboBox();
            this.cboSingleMonth = new fecherFoundation.FCComboBox();
            this.lblTo_1 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraGraphItem)).BeginInit();
            this.fraGraphItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraExpense)).BeginInit();
            this.fraExpense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraExpenseAccountRange)).BeginInit();
            this.fraExpenseAccountRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExpenseClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExpenseSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRevenue)).BeginInit();
            this.fraRevenue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRevenueClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRevenueSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRevenueAccountRange)).BeginInit();
            this.fraRevenueAccountRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowRevAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighRevAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedRevenues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCash)).BeginInit();
            this.fraCash.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLiabilityClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLiabilitySelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCashClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCashSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedLiabilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTax)).BeginInit();
            this.fraTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTaxClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTaxSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraExistingItem)).BeginInit();
            this.fraExistingItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddExisting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetExisting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
            this.fraMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraGraphItem);
            this.ClientArea.Controls.Add(this.fraExistingItem);
            this.ClientArea.Controls.Add(this.fraMonths);
            this.ClientArea.Controls.Add(this.vsGraph);
            this.ClientArea.Controls.Add(this.txtChartTitle);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Controls.Add(this.cmdAdd);
            this.ClientArea.Controls.Add(this.cmdGetExisting);
            this.ClientArea.Controls.Add(this.cmbYTDTotals);
            this.ClientArea.Controls.Add(this.lblYTDTotals);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(240, 30);
            this.HeaderText.Text = "Cash Charting Setup";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "Range of Months",
            "Single Month",
            "All"});
            this.cmbRange.Location = new System.Drawing.Point(140, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(200, 40);
            this.cmbRange.TabIndex = 36;
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(61, 15);
            this.lblRange.TabIndex = 37;
            this.lblRange.Text = "MONTHS";
            // 
            // cmbYTDTotals
            // 
            this.cmbYTDTotals.Items.AddRange(new object[] {
            "YTD Totals",
            "Monthly Totals"});
            this.cmbYTDTotals.Location = new System.Drawing.Point(190, 430);
            this.cmbYTDTotals.Name = "cmbYTDTotals";
            this.cmbYTDTotals.Size = new System.Drawing.Size(160, 40);
            this.cmbYTDTotals.TabIndex = 59;
            // 
            // lblYTDTotals
            // 
            this.lblYTDTotals.AutoSize = true;
            this.lblYTDTotals.Location = new System.Drawing.Point(30, 444);
            this.lblYTDTotals.Name = "lblYTDTotals";
            this.lblYTDTotals.Size = new System.Drawing.Size(106, 15);
            this.lblYTDTotals.TabIndex = 60;
            this.lblYTDTotals.Text = "TOTAL OPTIONS";
            // 
            // fraGraphItem
            // 
            this.fraGraphItem.AppearanceKey = "groupBoxLeftBorder";
            this.fraGraphItem.BackColor = System.Drawing.Color.White;
            this.fraGraphItem.Controls.Add(this.txtTitle);
            this.fraGraphItem.Controls.Add(this.cboGraphType);
            this.fraGraphItem.Controls.Add(this.cmdCreate);
            this.fraGraphItem.Controls.Add(this.cmdCancel);
            this.fraGraphItem.Controls.Add(this.lblTitle);
            this.fraGraphItem.Controls.Add(this.Label2);
            this.fraGraphItem.Controls.Add(this.lblAccountTitle);
            this.fraGraphItem.Controls.Add(this.fraExpense);
            this.fraGraphItem.Controls.Add(this.fraRevenue);
            this.fraGraphItem.Controls.Add(this.fraCash);
            this.fraGraphItem.Controls.Add(this.fraTax);
            this.fraGraphItem.Location = new System.Drawing.Point(30, 30);
            this.fraGraphItem.Name = "fraGraphItem";
            this.fraGraphItem.Size = new System.Drawing.Size(998, 639);
            this.fraGraphItem.TabIndex = 22;
            this.fraGraphItem.Text = "Graph Item";
            this.fraGraphItem.Visible = false;
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTitle.Location = new System.Drawing.Point(148, 30);
            this.txtTitle.MaxLength = 25;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(205, 40);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
            // 
            // cboGraphType
            // 
            this.cboGraphType.BackColor = System.Drawing.SystemColors.Window;
            this.cboGraphType.Items.AddRange(new object[] {
            "Expense",
            "Revenue",
            "Cash",
            "Tax Receivables"});
            this.cboGraphType.Location = new System.Drawing.Point(148, 90);
            this.cboGraphType.Name = "cboGraphType";
            this.cboGraphType.Size = new System.Drawing.Size(205, 40);
            this.cboGraphType.TabIndex = 2;
            this.cboGraphType.SelectedIndexChanged += new System.EventHandler(this.cboGraphType_SelectedIndexChanged);
            // 
            // cmdCreate
            // 
            this.cmdCreate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdCreate.AppearanceKey = "actionButton";
            this.cmdCreate.Location = new System.Drawing.Point(20, 579);
            this.cmdCreate.Name = "cmdCreate";
            this.cmdCreate.Size = new System.Drawing.Size(160, 40);
            this.cmdCreate.TabIndex = 20;
            this.cmdCreate.Text = "Save Graph Item";
            this.cmdCreate.Click += new System.EventHandler(this.cmdCreate_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(200, 579);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(122, 40);
            this.cmdCancel.TabIndex = 21;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(20, 44);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(31, 20);
            this.lblTitle.TabIndex = 57;
            this.lblTitle.Text = "TITLE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 20);
            this.Label2.TabIndex = 56;
            this.Label2.Text = "ITEM TYPE";
            // 
            // lblAccountTitle
            // 
            this.lblAccountTitle.Location = new System.Drawing.Point(20, 150);
            this.lblAccountTitle.Name = "lblAccountTitle";
            this.lblAccountTitle.Size = new System.Drawing.Size(397, 45);
            this.lblAccountTitle.TabIndex = 55;
            this.lblAccountTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraExpense
            // 
            this.fraExpense.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraExpense.AppearanceKey = "groupBoxLeftBorder";
            this.fraExpense.Controls.Add(this.fraExpenseAccountRange);
            this.fraExpense.Controls.Add(this.cmdExpenseClear);
            this.fraExpense.Controls.Add(this.cmdExpenseSelectAll);
            this.fraExpense.Controls.Add(this.cboExpenseSelection);
            this.fraExpense.Controls.Add(this.vsSelectedExpenses);
            this.fraExpense.Controls.Add(this.Label3);
            this.fraExpense.Location = new System.Drawing.Point(20, 215);
            this.fraExpense.Name = "fraExpense";
            this.fraExpense.Size = new System.Drawing.Size(958, 344);
            this.fraExpense.TabIndex = 51;
            this.fraExpense.Text = "Town Expense Item";
            this.fraExpense.Visible = false;
            // 
            // fraExpenseAccountRange
            // 
            this.fraExpenseAccountRange.Controls.Add(this.cboBeginningExpense);
            this.fraExpenseAccountRange.Controls.Add(this.cboEndingExpense);
            this.fraExpenseAccountRange.Controls.Add(this.cboSingleExpense);
            this.fraExpenseAccountRange.Controls.Add(this.cboBeginningDept);
            this.fraExpenseAccountRange.Controls.Add(this.cboEndingDept);
            this.fraExpenseAccountRange.Controls.Add(this.cboSingleDept);
            this.fraExpenseAccountRange.Controls.Add(this.vsHighAccount);
            this.fraExpenseAccountRange.Controls.Add(this.vsLowAccount);
            this.fraExpenseAccountRange.Controls.Add(this.lblTo_2);
            this.fraExpenseAccountRange.Location = new System.Drawing.Point(20, 90);
            this.fraExpenseAccountRange.Name = "fraExpenseAccountRange";
            this.fraExpenseAccountRange.Size = new System.Drawing.Size(492, 90);
            this.fraExpenseAccountRange.TabIndex = 52;
            this.fraExpenseAccountRange.Text = "Frame1";
            this.fraExpenseAccountRange.Visible = false;
            // 
            // cboBeginningExpense
            // 
            this.cboBeginningExpense.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningExpense.Location = new System.Drawing.Point(20, 30);
            this.cboBeginningExpense.Name = "cboBeginningExpense";
            this.cboBeginningExpense.Size = new System.Drawing.Size(200, 40);
            this.cboBeginningExpense.TabIndex = 13;
            this.cboBeginningExpense.Visible = false;
            this.cboBeginningExpense.DropDown += new System.EventHandler(this.cboBeginningExpense_DropDown);
            // 
            // cboEndingExpense
            // 
            this.cboEndingExpense.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingExpense.Location = new System.Drawing.Point(274, 30);
            this.cboEndingExpense.Name = "cboEndingExpense";
            this.cboEndingExpense.Size = new System.Drawing.Size(200, 40);
            this.cboEndingExpense.TabIndex = 15;
            this.cboEndingExpense.Visible = false;
            this.cboEndingExpense.DropDown += new System.EventHandler(this.cboEndingExpense_DropDown);
            // 
            // cboSingleExpense
            // 
            this.cboSingleExpense.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleExpense.Location = new System.Drawing.Point(20, 30);
            this.cboSingleExpense.Name = "cboSingleExpense";
            this.cboSingleExpense.Size = new System.Drawing.Size(200, 40);
            this.cboSingleExpense.TabIndex = 14;
            this.cboSingleExpense.Visible = false;
            this.cboSingleExpense.DropDown += new System.EventHandler(this.cboSingleExpense_DropDown);
            // 
            // cboBeginningDept
            // 
            this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningDept.Location = new System.Drawing.Point(20, 30);
            this.cboBeginningDept.Name = "cboBeginningDept";
            this.cboBeginningDept.Size = new System.Drawing.Size(200, 40);
            this.cboBeginningDept.TabIndex = 16;
            this.cboBeginningDept.Visible = false;
            this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
            // 
            // cboEndingDept
            // 
            this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingDept.Location = new System.Drawing.Point(274, 30);
            this.cboEndingDept.Name = "cboEndingDept";
            this.cboEndingDept.Size = new System.Drawing.Size(200, 40);
            this.cboEndingDept.TabIndex = 18;
            this.cboEndingDept.Visible = false;
            this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
            // 
            // cboSingleDept
            // 
            this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleDept.Location = new System.Drawing.Point(20, 30);
            this.cboSingleDept.Name = "cboSingleDept";
            this.cboSingleDept.Size = new System.Drawing.Size(200, 40);
            this.cboSingleDept.TabIndex = 17;
            this.cboSingleDept.Visible = false;
            this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
            // 
            // vsHighAccount
            // 
            this.vsHighAccount.Cols = 1;
            this.vsHighAccount.ColumnHeadersVisible = false;
            this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsHighAccount.FixedCols = 0;
            this.vsHighAccount.FixedRows = 0;
            this.vsHighAccount.Location = new System.Drawing.Point(274, 30);
            this.vsHighAccount.Name = "vsHighAccount";
            this.vsHighAccount.ReadOnly = false;
            this.vsHighAccount.RowHeadersVisible = false;
            this.vsHighAccount.Rows = 1;
            this.vsHighAccount.Size = new System.Drawing.Size(200, 42);
            this.vsHighAccount.TabIndex = 74;
            // 
            // vsLowAccount
            // 
            this.vsLowAccount.Cols = 1;
            this.vsLowAccount.ColumnHeadersVisible = false;
            this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLowAccount.FixedCols = 0;
            this.vsLowAccount.FixedRows = 0;
            this.vsLowAccount.Location = new System.Drawing.Point(20, 30);
            this.vsLowAccount.Name = "vsLowAccount";
            this.vsLowAccount.ReadOnly = false;
            this.vsLowAccount.RowHeadersVisible = false;
            this.vsLowAccount.Rows = 1;
            this.vsLowAccount.Size = new System.Drawing.Size(200, 42);
            this.vsLowAccount.TabIndex = 73;
            // 
            // lblTo_2
            // 
            this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_2.Location = new System.Drawing.Point(239, 44);
            this.lblTo_2.Name = "lblTo_2";
            this.lblTo_2.Size = new System.Drawing.Size(21, 20);
            this.lblTo_2.TabIndex = 53;
            this.lblTo_2.Text = "TO";
            this.lblTo_2.Visible = false;
            // 
            // cmdExpenseClear
            // 
            this.cmdExpenseClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdExpenseClear.AppearanceKey = "actionButton";
            this.cmdExpenseClear.Location = new System.Drawing.Point(120, 284);
            this.cmdExpenseClear.Name = "cmdExpenseClear";
            this.cmdExpenseClear.Size = new System.Drawing.Size(80, 40);
            this.cmdExpenseClear.TabIndex = 64;
            this.cmdExpenseClear.Text = "Clear ";
            this.cmdExpenseClear.Visible = false;
            this.cmdExpenseClear.Click += new System.EventHandler(this.cmdExpenseClear_Click);
            // 
            // cmdExpenseSelectAll
            // 
            this.cmdExpenseSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdExpenseSelectAll.AppearanceKey = "actionButton";
            this.cmdExpenseSelectAll.Location = new System.Drawing.Point(20, 284);
            this.cmdExpenseSelectAll.Name = "cmdExpenseSelectAll";
            this.cmdExpenseSelectAll.Size = new System.Drawing.Size(80, 40);
            this.cmdExpenseSelectAll.TabIndex = 63;
            this.cmdExpenseSelectAll.Text = "Select All";
            this.cmdExpenseSelectAll.Visible = false;
            this.cmdExpenseSelectAll.Click += new System.EventHandler(this.cmdExpenseSelectAll_Click);
            // 
            // cboExpenseSelection
            // 
            this.cboExpenseSelection.BackColor = System.Drawing.SystemColors.Window;
            this.cboExpenseSelection.Items.AddRange(new object[] {
            "All Expense Accounts",
            "Single Department",
            "Single Expense",
            "Range of Departments",
            "Range of Expenses",
            "Range of Accounts",
            "Selected Accounts"});
            this.cboExpenseSelection.Location = new System.Drawing.Point(212, 30);
            this.cboExpenseSelection.Name = "cboExpenseSelection";
            this.cboExpenseSelection.Size = new System.Drawing.Size(258, 40);
            this.cboExpenseSelection.TabIndex = 3;
            this.cboExpenseSelection.SelectedIndexChanged += new System.EventHandler(this.cboExpenseSelection_SelectedIndexChanged);
            // 
            // vsSelectedExpenses
            // 
            this.vsSelectedExpenses.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSelectedExpenses.FixedCols = 0;
            this.vsSelectedExpenses.Location = new System.Drawing.Point(20, 90);
            this.vsSelectedExpenses.Name = "vsSelectedExpenses";
            this.vsSelectedExpenses.RowHeadersVisible = false;
            this.vsSelectedExpenses.Rows = 1;
            this.vsSelectedExpenses.Size = new System.Drawing.Size(919, 174);
            this.vsSelectedExpenses.TabIndex = 19;
            this.vsSelectedExpenses.Visible = false;
            this.vsSelectedExpenses.CurrentCellChanged += new System.EventHandler(this.vsSelectedExpenses_RowColChange);
            this.vsSelectedExpenses.Enter += new System.EventHandler(this.vsSelectedExpenses_Enter);
            this.vsSelectedExpenses.Click += new System.EventHandler(this.vsSelectedExpenses_ClickEvent);
            this.vsSelectedExpenses.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSelectedExpenses_KeyDownEvent);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(134, 20);
            this.Label3.TabIndex = 54;
            this.Label3.Text = "ACCOUNT SELECTION";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraRevenue
            // 
            this.fraRevenue.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraRevenue.AppearanceKey = "groupBoxLeftBorder";
            this.fraRevenue.Controls.Add(this.cmdRevenueClear);
            this.fraRevenue.Controls.Add(this.cmdRevenueSelectAll);
            this.fraRevenue.Controls.Add(this.cboRevenueSelection);
            this.fraRevenue.Controls.Add(this.fraRevenueAccountRange);
            this.fraRevenue.Controls.Add(this.vsSelectedRevenues);
            this.fraRevenue.Controls.Add(this.Label4);
            this.fraRevenue.Location = new System.Drawing.Point(20, 215);
            this.fraRevenue.Name = "fraRevenue";
            this.fraRevenue.Size = new System.Drawing.Size(958, 344);
            this.fraRevenue.TabIndex = 45;
            this.fraRevenue.Text = "Town Revenue Item";
            this.fraRevenue.Visible = false;
            // 
            // cmdRevenueClear
            // 
            this.cmdRevenueClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdRevenueClear.AppearanceKey = "actionButton";
            this.cmdRevenueClear.Location = new System.Drawing.Point(120, 284);
            this.cmdRevenueClear.Name = "cmdRevenueClear";
            this.cmdRevenueClear.Size = new System.Drawing.Size(80, 40);
            this.cmdRevenueClear.TabIndex = 66;
            this.cmdRevenueClear.Text = "Clear ";
            this.cmdRevenueClear.Visible = false;
            this.cmdRevenueClear.Click += new System.EventHandler(this.cmdRevenueClear_Click);
            // 
            // cmdRevenueSelectAll
            // 
            this.cmdRevenueSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdRevenueSelectAll.AppearanceKey = "actionButton";
            this.cmdRevenueSelectAll.Location = new System.Drawing.Point(20, 284);
            this.cmdRevenueSelectAll.Name = "cmdRevenueSelectAll";
            this.cmdRevenueSelectAll.Size = new System.Drawing.Size(80, 40);
            this.cmdRevenueSelectAll.TabIndex = 65;
            this.cmdRevenueSelectAll.Text = "Select All";
            this.cmdRevenueSelectAll.Visible = false;
            this.cmdRevenueSelectAll.Click += new System.EventHandler(this.cmdRevenueSelectAll_Click);
            // 
            // cboRevenueSelection
            // 
            this.cboRevenueSelection.BackColor = System.Drawing.SystemColors.Window;
            this.cboRevenueSelection.Items.AddRange(new object[] {
            "All Revenue Accounts",
            "Single Department",
            "Range of Departments",
            "Range of Accounts",
            "Selected Accounts"});
            this.cboRevenueSelection.Location = new System.Drawing.Point(212, 30);
            this.cboRevenueSelection.Name = "cboRevenueSelection";
            this.cboRevenueSelection.Size = new System.Drawing.Size(258, 40);
            this.cboRevenueSelection.TabIndex = 4;
            this.cboRevenueSelection.SelectedIndexChanged += new System.EventHandler(this.cboRevenueSelection_SelectedIndexChanged);
            // 
            // fraRevenueAccountRange
            // 
            this.fraRevenueAccountRange.Controls.Add(this.cboSingleRev);
            this.fraRevenueAccountRange.Controls.Add(this.cboEndingRev);
            this.fraRevenueAccountRange.Controls.Add(this.cboBeginningRev);
            this.fraRevenueAccountRange.Controls.Add(this.vsLowRevAccount);
            this.fraRevenueAccountRange.Controls.Add(this.vsHighRevAccount);
            this.fraRevenueAccountRange.Controls.Add(this.lblTo_0);
            this.fraRevenueAccountRange.Location = new System.Drawing.Point(20, 90);
            this.fraRevenueAccountRange.Name = "fraRevenueAccountRange";
            this.fraRevenueAccountRange.Size = new System.Drawing.Size(492, 90);
            this.fraRevenueAccountRange.TabIndex = 47;
            this.fraRevenueAccountRange.Text = "Frame1";
            this.fraRevenueAccountRange.Visible = false;
            // 
            // cboSingleRev
            // 
            this.cboSingleRev.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleRev.Location = new System.Drawing.Point(20, 30);
            this.cboSingleRev.Name = "cboSingleRev";
            this.cboSingleRev.Size = new System.Drawing.Size(200, 40);
            this.cboSingleRev.TabIndex = 9;
            this.cboSingleRev.Visible = false;
            this.cboSingleRev.DropDown += new System.EventHandler(this.cboSingleRev_DropDown);
            // 
            // cboEndingRev
            // 
            this.cboEndingRev.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingRev.Location = new System.Drawing.Point(274, 30);
            this.cboEndingRev.Name = "cboEndingRev";
            this.cboEndingRev.Size = new System.Drawing.Size(200, 40);
            this.cboEndingRev.TabIndex = 10;
            this.cboEndingRev.Visible = false;
            this.cboEndingRev.DropDown += new System.EventHandler(this.cboEndingRev_DropDown);
            // 
            // cboBeginningRev
            // 
            this.cboBeginningRev.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningRev.Location = new System.Drawing.Point(20, 30);
            this.cboBeginningRev.Name = "cboBeginningRev";
            this.cboBeginningRev.Size = new System.Drawing.Size(200, 40);
            this.cboBeginningRev.TabIndex = 8;
            this.cboBeginningRev.Visible = false;
            this.cboBeginningRev.DropDown += new System.EventHandler(this.cboBeginningRev_DropDown);
            // 
            // vsLowRevAccount
            // 
            this.vsLowRevAccount.Cols = 1;
            this.vsLowRevAccount.ColumnHeadersVisible = false;
            this.vsLowRevAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLowRevAccount.FixedCols = 0;
            this.vsLowRevAccount.FixedRows = 0;
            this.vsLowRevAccount.Location = new System.Drawing.Point(20, 30);
            this.vsLowRevAccount.Name = "vsLowRevAccount";
            this.vsLowRevAccount.ReadOnly = false;
            this.vsLowRevAccount.RowHeadersVisible = false;
            this.vsLowRevAccount.Rows = 1;
            this.vsLowRevAccount.Size = new System.Drawing.Size(200, 42);
            this.vsLowRevAccount.TabIndex = 75;
            // 
            // vsHighRevAccount
            // 
            this.vsHighRevAccount.Cols = 1;
            this.vsHighRevAccount.ColumnHeadersVisible = false;
            this.vsHighRevAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsHighRevAccount.FixedCols = 0;
            this.vsHighRevAccount.FixedRows = 0;
            this.vsHighRevAccount.Location = new System.Drawing.Point(274, 30);
            this.vsHighRevAccount.Name = "vsHighRevAccount";
            this.vsHighRevAccount.ReadOnly = false;
            this.vsHighRevAccount.RowHeadersVisible = false;
            this.vsHighRevAccount.Rows = 1;
            this.vsHighRevAccount.Size = new System.Drawing.Size(200, 42);
            this.vsHighRevAccount.TabIndex = 76;
            // 
            // lblTo_0
            // 
            this.lblTo_0.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_0.Location = new System.Drawing.Point(239, 44);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(21, 20);
            this.lblTo_0.TabIndex = 49;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.Visible = false;
            // 
            // vsSelectedRevenues
            // 
            this.vsSelectedRevenues.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSelectedRevenues.FixedCols = 0;
            this.vsSelectedRevenues.Location = new System.Drawing.Point(20, 90);
            this.vsSelectedRevenues.Name = "vsSelectedRevenues";
            this.vsSelectedRevenues.RowHeadersVisible = false;
            this.vsSelectedRevenues.Rows = 1;
            this.vsSelectedRevenues.Size = new System.Drawing.Size(919, 174);
            this.vsSelectedRevenues.TabIndex = 11;
            this.vsSelectedRevenues.Visible = false;
            this.vsSelectedRevenues.CurrentCellChanged += new System.EventHandler(this.vsSelectedRevenues_RowColChange);
            this.vsSelectedRevenues.Enter += new System.EventHandler(this.vsSelectedRevenues_Enter);
            this.vsSelectedRevenues.Click += new System.EventHandler(this.vsSelectedRevenues_ClickEvent);
            this.vsSelectedRevenues.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSelectedRevenues_KeyDownEvent);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(126, 20);
            this.Label4.TabIndex = 50;
            this.Label4.Text = "ACCOUNT SELECTION";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraCash
            // 
            this.fraCash.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraCash.Controls.Add(this.cmdLiabilityClear);
            this.fraCash.Controls.Add(this.cmdLiabilitySelectAll);
            this.fraCash.Controls.Add(this.cmdCashClear);
            this.fraCash.Controls.Add(this.cmdCashSelectAll);
            this.fraCash.Controls.Add(this.cboCashSelection);
            this.fraCash.Controls.Add(this.vsSelectedCash);
            this.fraCash.Controls.Add(this.vsSelectedLiabilities);
            this.fraCash.Controls.Add(this.Label5);
            this.fraCash.Controls.Add(this.Label6);
            this.fraCash.Controls.Add(this.lblLiability);
            this.fraCash.Location = new System.Drawing.Point(20, 215);
            this.fraCash.Name = "fraCash";
            this.fraCash.Size = new System.Drawing.Size(958, 344);
            this.fraCash.TabIndex = 40;
            this.fraCash.Text = "Town Cash Item";
            this.fraCash.Visible = false;
            // 
            // cmdLiabilityClear
            // 
            this.cmdLiabilityClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdLiabilityClear.AppearanceKey = "actionButton";
            this.cmdLiabilityClear.Location = new System.Drawing.Point(589, 284);
            this.cmdLiabilityClear.Name = "cmdLiabilityClear";
            this.cmdLiabilityClear.Size = new System.Drawing.Size(80, 40);
            this.cmdLiabilityClear.TabIndex = 70;
            this.cmdLiabilityClear.Text = "Clear ";
            this.cmdLiabilityClear.Visible = false;
            this.cmdLiabilityClear.Click += new System.EventHandler(this.cmdLiabilityClear_Click);
            // 
            // cmdLiabilitySelectAll
            // 
            this.cmdLiabilitySelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdLiabilitySelectAll.AppearanceKey = "actionButton";
            this.cmdLiabilitySelectAll.Location = new System.Drawing.Point(489, 284);
            this.cmdLiabilitySelectAll.Name = "cmdLiabilitySelectAll";
            this.cmdLiabilitySelectAll.Size = new System.Drawing.Size(80, 40);
            this.cmdLiabilitySelectAll.TabIndex = 69;
            this.cmdLiabilitySelectAll.Text = "Select All";
            this.cmdLiabilitySelectAll.Visible = false;
            this.cmdLiabilitySelectAll.Click += new System.EventHandler(this.cmdLiabilitySelectAll_Click);
            // 
            // cmdCashClear
            // 
            this.cmdCashClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdCashClear.AppearanceKey = "actionButton";
            this.cmdCashClear.Location = new System.Drawing.Point(120, 284);
            this.cmdCashClear.Name = "cmdCashClear";
            this.cmdCashClear.Size = new System.Drawing.Size(80, 40);
            this.cmdCashClear.TabIndex = 68;
            this.cmdCashClear.Text = "Clear ";
            this.cmdCashClear.Click += new System.EventHandler(this.cmdCashClear_Click);
            // 
            // cmdCashSelectAll
            // 
            this.cmdCashSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdCashSelectAll.AppearanceKey = "actionButton";
            this.cmdCashSelectAll.Location = new System.Drawing.Point(20, 284);
            this.cmdCashSelectAll.Name = "cmdCashSelectAll";
            this.cmdCashSelectAll.Size = new System.Drawing.Size(80, 40);
            this.cmdCashSelectAll.TabIndex = 67;
            this.cmdCashSelectAll.Text = "Select All";
            this.cmdCashSelectAll.Click += new System.EventHandler(this.cmdCashSelectAll_Click);
            // 
            // cboCashSelection
            // 
            this.cboCashSelection.BackColor = System.Drawing.SystemColors.Window;
            this.cboCashSelection.Items.AddRange(new object[] {
            "Total Amount",
            "Net Amount"});
            this.cboCashSelection.Location = new System.Drawing.Point(212, 30);
            this.cboCashSelection.Name = "cboCashSelection";
            this.cboCashSelection.Size = new System.Drawing.Size(257, 40);
            this.cboCashSelection.TabIndex = 12;
            this.cboCashSelection.SelectedIndexChanged += new System.EventHandler(this.cboCashSelection_SelectedIndexChanged);
            // 
            // vsSelectedCash
            // 
            this.vsSelectedCash.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSelectedCash.FixedCols = 0;
            this.vsSelectedCash.Location = new System.Drawing.Point(20, 122);
            this.vsSelectedCash.Name = "vsSelectedCash";
            this.vsSelectedCash.RowHeadersVisible = false;
            this.vsSelectedCash.Rows = 1;
            this.vsSelectedCash.Size = new System.Drawing.Size(449, 142);
            this.vsSelectedCash.TabIndex = 6;
            this.vsSelectedCash.CurrentCellChanged += new System.EventHandler(this.vsSelectedCash_RowColChange);
            this.vsSelectedCash.Enter += new System.EventHandler(this.vsSelectedCash_Enter);
            this.vsSelectedCash.Click += new System.EventHandler(this.vsSelectedCash_ClickEvent);
            this.vsSelectedCash.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSelectedCash_KeyDownEvent);
            // 
            // vsSelectedLiabilities
            // 
            this.vsSelectedLiabilities.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSelectedLiabilities.FixedCols = 0;
            this.vsSelectedLiabilities.Location = new System.Drawing.Point(489, 122);
            this.vsSelectedLiabilities.Name = "vsSelectedLiabilities";
            this.vsSelectedLiabilities.RowHeadersVisible = false;
            this.vsSelectedLiabilities.Rows = 1;
            this.vsSelectedLiabilities.Size = new System.Drawing.Size(449, 142);
            this.vsSelectedLiabilities.TabIndex = 7;
            this.vsSelectedLiabilities.Visible = false;
            this.vsSelectedLiabilities.CurrentCellChanged += new System.EventHandler(this.vsSelectedLiabilities_RowColChange);
            this.vsSelectedLiabilities.Enter += new System.EventHandler(this.vsSelectedLiabilities_Enter);
            this.vsSelectedLiabilities.Click += new System.EventHandler(this.vsSelectedLiabilities_ClickEvent);
            this.vsSelectedLiabilities.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSelectedLiabilities_KeyDownEvent);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(126, 20);
            this.Label5.TabIndex = 43;
            this.Label5.Text = "ACCOUNT SELECTION";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 90);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(106, 20);
            this.Label6.TabIndex = 42;
            this.Label6.Text = "CASH ACCOUNTS";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLiability
            // 
            this.lblLiability.Location = new System.Drawing.Point(489, 90);
            this.lblLiability.Name = "lblLiability";
            this.lblLiability.Size = new System.Drawing.Size(124, 20);
            this.lblLiability.TabIndex = 41;
            this.lblLiability.Text = "LIABILITY ACCOUNTS";
            this.lblLiability.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraTax
            // 
            this.fraTax.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraTax.Controls.Add(this.cmdTaxClear);
            this.fraTax.Controls.Add(this.cmdTaxSelectAll);
            this.fraTax.Controls.Add(this.vsSelectedTax);
            this.fraTax.Controls.Add(this.Label8);
            this.fraTax.Location = new System.Drawing.Point(20, 215);
            this.fraTax.Name = "fraTax";
            this.fraTax.Size = new System.Drawing.Size(958, 344);
            this.fraTax.TabIndex = 38;
            this.fraTax.Text = "Tax Recievable Item";
            this.fraTax.Visible = false;
            // 
            // cmdTaxClear
            // 
            this.cmdTaxClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdTaxClear.AppearanceKey = "actionButton";
            this.cmdTaxClear.Location = new System.Drawing.Point(120, 284);
            this.cmdTaxClear.Name = "cmdTaxClear";
            this.cmdTaxClear.Size = new System.Drawing.Size(80, 40);
            this.cmdTaxClear.TabIndex = 72;
            this.cmdTaxClear.Text = "Clear ";
            this.cmdTaxClear.Click += new System.EventHandler(this.cmdTaxClear_Click);
            // 
            // cmdTaxSelectAll
            // 
            this.cmdTaxSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdTaxSelectAll.AppearanceKey = "actionButton";
            this.cmdTaxSelectAll.Location = new System.Drawing.Point(20, 284);
            this.cmdTaxSelectAll.Name = "cmdTaxSelectAll";
            this.cmdTaxSelectAll.Size = new System.Drawing.Size(80, 40);
            this.cmdTaxSelectAll.TabIndex = 71;
            this.cmdTaxSelectAll.Text = "Select All";
            this.cmdTaxSelectAll.Click += new System.EventHandler(this.cmdTaxSelectAll_Click);
            // 
            // vsSelectedTax
            // 
            this.vsSelectedTax.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSelectedTax.FixedCols = 0;
            this.vsSelectedTax.Location = new System.Drawing.Point(20, 56);
            this.vsSelectedTax.Name = "vsSelectedTax";
            this.vsSelectedTax.RowHeadersVisible = false;
            this.vsSelectedTax.Rows = 1;
            this.vsSelectedTax.Size = new System.Drawing.Size(918, 204);
            this.vsSelectedTax.TabIndex = 5;
            this.vsSelectedTax.CurrentCellChanged += new System.EventHandler(this.vsSelectedTax_RowColChange);
            this.vsSelectedTax.Enter += new System.EventHandler(this.vsSelectedTax_Enter);
            this.vsSelectedTax.Click += new System.EventHandler(this.vsSelectedTax_ClickEvent);
            this.vsSelectedTax.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSelectedTax_KeyDownEvent);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 30);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(173, 20);
            this.Label8.TabIndex = 39;
            this.Label8.Text = " TAX RECEIVABLE ACCOUNTS";
            // 
            // fraExistingItem
            // 
            this.fraExistingItem.BackColor = System.Drawing.Color.White;
            this.fraExistingItem.Controls.Add(this.cboExistingItems);
            this.fraExistingItem.Controls.Add(this.cmdAddExisting);
            this.fraExistingItem.Controls.Add(this.cmdCancelAdd);
            this.fraExistingItem.Location = new System.Drawing.Point(30, 30);
            this.fraExistingItem.Name = "fraExistingItem";
            this.fraExistingItem.Size = new System.Drawing.Size(208, 150);
            this.fraExistingItem.Text = "Existing Item";
            this.fraExistingItem.Visible = false;
            // 
            // cboExistingItems
            // 
            this.cboExistingItems.BackColor = System.Drawing.SystemColors.Window;
            this.cboExistingItems.Location = new System.Drawing.Point(20, 30);
            this.cboExistingItems.Name = "cboExistingItems";
            this.cboExistingItems.Size = new System.Drawing.Size(168, 40);
            this.cboExistingItems.TabIndex = 44;
            // 
            // cmdAddExisting
            // 
            this.cmdAddExisting.AppearanceKey = "actionButton";
            this.cmdAddExisting.Location = new System.Drawing.Point(20, 90);
            this.cmdAddExisting.Name = "cmdAddExisting";
            this.cmdAddExisting.Size = new System.Drawing.Size(74, 40);
            this.cmdAddExisting.TabIndex = 46;
            this.cmdAddExisting.Text = "Add";
            this.cmdAddExisting.Click += new System.EventHandler(this.cmdAddExisting_Click);
            // 
            // cmdCancelAdd
            // 
            this.cmdCancelAdd.AppearanceKey = "actionButton";
            this.cmdCancelAdd.Location = new System.Drawing.Point(114, 90);
            this.cmdCancelAdd.Name = "cmdCancelAdd";
            this.cmdCancelAdd.Size = new System.Drawing.Size(74, 40);
            this.cmdCancelAdd.TabIndex = 48;
            this.cmdCancelAdd.Text = "Cancel";
            this.cmdCancelAdd.Click += new System.EventHandler(this.cmdCancelAdd_Click);
            // 
            // vsGraph
            // 
            this.vsGraph.Cols = 4;
            this.vsGraph.Location = new System.Drawing.Point(30, 136);
            this.vsGraph.Name = "vsGraph";
            this.vsGraph.Rows = 1;
            this.vsGraph.Size = new System.Drawing.Size(998, 203);
            this.vsGraph.TabIndex = 24;
            this.vsGraph.DoubleClick += new System.EventHandler(this.vsGraph_DblClick);
            // 
            // txtChartTitle
            // 
            this.txtChartTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtChartTitle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtChartTitle.Location = new System.Drawing.Point(170, 30);
            this.txtChartTitle.Name = "txtChartTitle";
            this.txtChartTitle.Size = new System.Drawing.Size(173, 40);
            this.txtChartTitle.TabIndex = 23;
            this.txtChartTitle.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtChartTitle_KeyPress);
            // 
            // cmdDelete
            // 
            this.cmdDelete.AppearanceKey = "actionButton";
            this.cmdDelete.Location = new System.Drawing.Point(354, 364);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(126, 40);
            this.cmdDelete.TabIndex = 27;
            this.cmdDelete.Text = "Remove Item";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(30, 364);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(126, 40);
            this.cmdAdd.TabIndex = 25;
            this.cmdAdd.Text = "Add New Item";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cmdGetExisting
            // 
            this.cmdGetExisting.AppearanceKey = "actionButton";
            this.cmdGetExisting.Location = new System.Drawing.Point(183, 364);
            this.cmdGetExisting.Name = "cmdGetExisting";
            this.cmdGetExisting.Size = new System.Drawing.Size(148, 40);
            this.cmdGetExisting.TabIndex = 26;
            this.cmdGetExisting.Text = "Add Existing Item";
            this.cmdGetExisting.Click += new System.EventHandler(this.cmdGetExisting_Click);
            // 
            // fraMonths
            // 
            this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
            this.fraMonths.Controls.Add(this.cboEndingMonth);
            this.fraMonths.Controls.Add(this.cmbRange);
            this.fraMonths.Controls.Add(this.lblRange);
            this.fraMonths.Controls.Add(this.cboBeginningMonth);
            this.fraMonths.Controls.Add(this.cboSingleMonth);
            this.fraMonths.Controls.Add(this.lblTo_1);
            this.fraMonths.Location = new System.Drawing.Point(30, 490);
            this.fraMonths.Name = "fraMonths";
            this.fraMonths.Size = new System.Drawing.Size(360, 150);
            this.fraMonths.TabIndex = 58;
            this.fraMonths.FormatCaption = false;
            this.fraMonths.Text = "Month(s) To Report";
            // 
            // cboEndingMonth
            // 
            this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndingMonth.Location = new System.Drawing.Point(210, 90);
            this.cboEndingMonth.Name = "cboEndingMonth";
            this.cboEndingMonth.Size = new System.Drawing.Size(130, 40);
            this.cboEndingMonth.TabIndex = 35;
            this.cboEndingMonth.Visible = false;
            // 
            // cboBeginningMonth
            // 
            this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboBeginningMonth.Location = new System.Drawing.Point(20, 90);
            this.cboBeginningMonth.Name = "cboBeginningMonth";
            this.cboBeginningMonth.Size = new System.Drawing.Size(130, 40);
            this.cboBeginningMonth.TabIndex = 33;
            this.cboBeginningMonth.Visible = false;
            // 
            // cboSingleMonth
            // 
            this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboSingleMonth.Location = new System.Drawing.Point(140, 90);
            this.cboSingleMonth.Name = "cboSingleMonth";
            this.cboSingleMonth.Size = new System.Drawing.Size(200, 40);
            this.cboSingleMonth.TabIndex = 34;
            this.cboSingleMonth.Visible = false;
            // 
            // lblTo_1
            // 
            this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_1.Location = new System.Drawing.Point(170, 104);
            this.lblTo_1.Name = "lblTo_1";
            this.lblTo_1.Size = new System.Drawing.Size(20, 16);
            this.lblTo_1.TabIndex = 59;
            this.lblTo_1.Text = "TO";
            this.lblTo_1.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 92);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(153, 22);
            this.Label1.TabIndex = 62;
            this.Label1.Text = "GRAPH INFORMATION";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(137, 20);
            this.Label7.TabIndex = 61;
            this.Label7.Text = "CHART TITLE";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(80, 48);
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmCashChartingSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCashChartingSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Cash Charting Setup";
            this.Load += new System.EventHandler(this.frmCashChartingSetup_Load);
            this.Activated += new System.EventHandler(this.frmCashChartingSetup_Activated);
            this.Resize += new System.EventHandler(this.frmCashChartingSetup_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCashChartingSetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCashChartingSetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraGraphItem)).EndInit();
            this.fraGraphItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraExpense)).EndInit();
            this.fraExpense.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraExpenseAccountRange)).EndInit();
            this.fraExpenseAccountRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExpenseClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExpenseSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRevenue)).EndInit();
            this.fraRevenue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdRevenueClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRevenueSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRevenueAccountRange)).EndInit();
            this.fraRevenueAccountRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLowRevAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighRevAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedRevenues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCash)).EndInit();
            this.fraCash.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdLiabilityClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLiabilitySelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCashClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCashSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedLiabilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTax)).EndInit();
            this.fraTax.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdTaxClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTaxSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraExistingItem)).EndInit();
            this.fraExistingItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddExisting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetExisting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
            this.fraMonths.ResumeLayout(false);
            this.fraMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
