﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptNewPurchaseOrder.
	/// </summary>
	partial class rptNewPurchaseOrder
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewPurchaseOrder));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDetailDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldQuantity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLineTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.imgIcon = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldVendorNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldInvoiceName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoiceAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoiceAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoiceAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoiceAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDepartment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldComments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldDetailDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLineTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDetailDescription,
				this.fldAccount,
				this.fldPrice,
				this.fldQuantity,
				this.fldLineTotal
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDetailDescription
			// 
			this.fldDetailDescription.Height = 0.1875F;
			this.fldDetailDescription.Left = 0.9375F;
			this.fldDetailDescription.Name = "fldDetailDescription";
			this.fldDetailDescription.Text = "Field1";
			this.fldDetailDescription.Top = 0F;
			this.fldDetailDescription.Width = 1.875F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 2.84375F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.71875F;
			// 
			// fldPrice
			// 
			this.fldPrice.Height = 0.1875F;
			this.fldPrice.Left = 4.59375F;
			this.fldPrice.Name = "fldPrice";
			this.fldPrice.Style = "text-align: right";
			this.fldPrice.Text = "Field1";
			this.fldPrice.Top = 0F;
			this.fldPrice.Width = 0.90625F;
			// 
			// fldQuantity
			// 
			this.fldQuantity.Height = 0.1875F;
			this.fldQuantity.Left = 0F;
			this.fldQuantity.Name = "fldQuantity";
			this.fldQuantity.Style = "text-align: right";
			this.fldQuantity.Text = "Field1";
			this.fldQuantity.Top = 0F;
			this.fldQuantity.Width = 0.8125F;
			// 
			// fldLineTotal
			// 
			this.fldLineTotal.Height = 0.1875F;
			this.fldLineTotal.Left = 5.5625F;
			this.fldLineTotal.Name = "fldLineTotal";
			this.fldLineTotal.Style = "text-align: right";
			this.fldLineTotal.Text = "Field1";
			this.fldLineTotal.Top = 0F;
			this.fldLineTotal.Width = 1.03125F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanShrink = true;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.imgIcon
			});
			this.PageHeader.Height = 0.96875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: right; ddo" + "-char-set: 1";
			this.Label1.Text = "Purchase Order";
			this.Label1.Top = 0F;
			this.Label1.Width = 1.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.25F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.71875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 3.1875F;
			// 
			// imgIcon
			// 
			this.imgIcon.Height = 0.96875F;
			this.imgIcon.HyperLink = null;
			this.imgIcon.ImageData = null;
			this.imgIcon.Left = 0F;
			this.imgIcon.LineWeight = 1F;
			this.imgIcon.Name = "imgIcon";
			this.imgIcon.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.imgIcon.Top = 0F;
			this.imgIcon.Width = 0.96875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.fldVendorNumber,
				this.fldVendorName,
				this.fldVendorAddress1,
				this.fldVendorAddress2,
				this.fldVendorAddress3,
				this.fldVendorAddress4,
				this.Label4,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.fldPO,
				this.fldDate,
				this.fldDescription,
				this.Line1,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.fldInvoiceName,
				this.fldInvoiceAddress1,
				this.fldInvoiceAddress2,
				this.fldInvoiceAddress3,
				this.fldInvoiceAddress4,
				this.Label14,
				this.fldDepartment,
				this.Label15,
				this.Label16
			});
			this.GroupHeader1.Height = 3.145833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.Label3.Text = "Vendor:";
			this.Label3.Top = 1.625F;
			this.Label3.Width = 0.875F;
			// 
			// fldVendorNumber
			// 
			this.fldVendorNumber.Height = 0.1875F;
			this.fldVendorNumber.Left = 2.0625F;
			this.fldVendorNumber.Name = "fldVendorNumber";
			this.fldVendorNumber.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendorNumber.Text = "Field1";
			this.fldVendorNumber.Top = 1.625F;
			this.fldVendorNumber.Width = 0.625F;
			// 
			// fldVendorName
			// 
			this.fldVendorName.Height = 0.1875F;
			this.fldVendorName.Left = 0.375F;
			this.fldVendorName.Name = "fldVendorName";
			this.fldVendorName.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendorName.Text = "Field1";
			this.fldVendorName.Top = 1.8125F;
			this.fldVendorName.Width = 2.3125F;
			// 
			// fldVendorAddress1
			// 
			this.fldVendorAddress1.Height = 0.1875F;
			this.fldVendorAddress1.Left = 0.375F;
			this.fldVendorAddress1.Name = "fldVendorAddress1";
			this.fldVendorAddress1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendorAddress1.Text = "Field1";
			this.fldVendorAddress1.Top = 2F;
			this.fldVendorAddress1.Width = 2.3125F;
			// 
			// fldVendorAddress2
			// 
			this.fldVendorAddress2.Height = 0.1875F;
			this.fldVendorAddress2.Left = 0.375F;
			this.fldVendorAddress2.Name = "fldVendorAddress2";
			this.fldVendorAddress2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendorAddress2.Text = "Field1";
			this.fldVendorAddress2.Top = 2.1875F;
			this.fldVendorAddress2.Width = 2.3125F;
			// 
			// fldVendorAddress3
			// 
			this.fldVendorAddress3.Height = 0.1875F;
			this.fldVendorAddress3.Left = 0.375F;
			this.fldVendorAddress3.Name = "fldVendorAddress3";
			this.fldVendorAddress3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendorAddress3.Text = "Field1";
			this.fldVendorAddress3.Top = 2.375F;
			this.fldVendorAddress3.Width = 2.3125F;
			// 
			// fldVendorAddress4
			// 
			this.fldVendorAddress4.Height = 0.1875F;
			this.fldVendorAddress4.Left = 0.375F;
			this.fldVendorAddress4.Name = "fldVendorAddress4";
			this.fldVendorAddress4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendorAddress4.Text = "Field1";
			this.fldVendorAddress4.Top = 2.5625F;
			this.fldVendorAddress4.Width = 2.3125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.25F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.Label4.Text = "Shipping Address:";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 2.125F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 3.6875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldName.Text = "Field1";
			this.fldName.Top = 0.375F;
			this.fldName.Width = 2.3125F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 3.6875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldAddress1.Text = "Field1";
			this.fldAddress1.Top = 0.5625F;
			this.fldAddress1.Width = 2.3125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 3.6875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldAddress2.Text = "Field1";
			this.fldAddress2.Top = 0.75F;
			this.fldAddress2.Width = 2.3125F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 3.6875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldAddress3.Text = "Field1";
			this.fldAddress3.Top = 0.9375F;
			this.fldAddress3.Width = 2.3125F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 3.6875F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldAddress4.Text = "Field1";
			this.fldAddress4.Top = 1.125F;
			this.fldAddress4.Width = 2.3125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Label5.Text = "Purchase Order#";
			this.Label5.Top = 1.625F;
			this.Label5.Width = 1.3125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Label6.Text = "Date";
			this.Label6.Top = 1.8125F;
			this.Label6.Width = 1.3125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Label7.Text = "Description";
			this.Label7.Top = 2F;
			this.Label7.Width = 1.3125F;
			// 
			// fldPO
			// 
			this.fldPO.Height = 0.1875F;
			this.fldPO.Left = 4.375F;
			this.fldPO.Name = "fldPO";
			this.fldPO.Text = "Field1";
			this.fldPO.Top = 1.625F;
			this.fldPO.Width = 1.125F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 4.375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 1.8125F;
			this.fldDate.Width = 1.125F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 4.375F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Text = "Field1";
			this.fldDescription.Top = 2F;
			this.fldDescription.Width = 2.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.125F;
			this.Line1.Width = 6.5625F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 6.625F;
			this.Line1.Y1 = 3.125F;
			this.Line1.Y2 = 3.125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.9375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "";
			this.Label10.Text = "Description";
			this.Label10.Top = 2.9375F;
			this.Label10.Width = 1.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "";
			this.Label11.Text = "Account";
			this.Label11.Top = 2.9375F;
			this.Label11.Width = 1.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: right";
			this.Label12.Text = "Price";
			this.Label12.Top = 2.9375F;
			this.Label12.Width = 0.875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.Label13.Text = "Send All Invoices To:";
			this.Label13.Top = 0.1875F;
			this.Label13.Width = 1.9375F;
			// 
			// fldInvoiceName
			// 
			this.fldInvoiceName.Height = 0.1875F;
			this.fldInvoiceName.Left = 0.4375F;
			this.fldInvoiceName.Name = "fldInvoiceName";
			this.fldInvoiceName.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInvoiceName.Text = "Field1";
			this.fldInvoiceName.Top = 0.375F;
			this.fldInvoiceName.Width = 2.375F;
			// 
			// fldInvoiceAddress1
			// 
			this.fldInvoiceAddress1.Height = 0.1875F;
			this.fldInvoiceAddress1.Left = 0.4375F;
			this.fldInvoiceAddress1.Name = "fldInvoiceAddress1";
			this.fldInvoiceAddress1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInvoiceAddress1.Text = "Field1";
			this.fldInvoiceAddress1.Top = 0.5625F;
			this.fldInvoiceAddress1.Width = 2.375F;
			// 
			// fldInvoiceAddress2
			// 
			this.fldInvoiceAddress2.Height = 0.1875F;
			this.fldInvoiceAddress2.Left = 0.4375F;
			this.fldInvoiceAddress2.Name = "fldInvoiceAddress2";
			this.fldInvoiceAddress2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInvoiceAddress2.Text = "Field1";
			this.fldInvoiceAddress2.Top = 0.75F;
			this.fldInvoiceAddress2.Width = 2.375F;
			// 
			// fldInvoiceAddress3
			// 
			this.fldInvoiceAddress3.Height = 0.1875F;
			this.fldInvoiceAddress3.Left = 0.4375F;
			this.fldInvoiceAddress3.Name = "fldInvoiceAddress3";
			this.fldInvoiceAddress3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInvoiceAddress3.Text = "Field1";
			this.fldInvoiceAddress3.Top = 0.9375F;
			this.fldInvoiceAddress3.Width = 2.375F;
			// 
			// fldInvoiceAddress4
			// 
			this.fldInvoiceAddress4.Height = 0.1875F;
			this.fldInvoiceAddress4.Left = 0.4375F;
			this.fldInvoiceAddress4.Name = "fldInvoiceAddress4";
			this.fldInvoiceAddress4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInvoiceAddress4.Text = "Field1";
			this.fldInvoiceAddress4.Top = 1.125F;
			this.fldInvoiceAddress4.Width = 2.375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Label14.Text = "Department";
			this.Label14.Top = 2.1875F;
			this.Label14.Width = 1.3125F;
			// 
			// fldDepartment
			// 
			this.fldDepartment.Height = 0.1875F;
			this.fldDepartment.Left = 4.375F;
			this.fldDepartment.Name = "fldDepartment";
			this.fldDepartment.Text = "Field1";
			this.fldDepartment.Top = 2.1875F;
			this.fldDepartment.Width = 2.25F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "text-align: right";
			this.Label15.Text = "Qty";
			this.Label15.Top = 2.9375F;
			this.Label15.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.5625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "text-align: right";
			this.Label16.Text = "Line Total";
			this.Label16.Top = 2.9375F;
			this.Label16.Width = 1.0625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8,
				this.fldTotal,
				this.Line2,
				this.fldComments,
				this.Label17,
				this.Line3,
				this.Label18,
				this.Label19,
				this.Line4,
				this.Label20,
				this.Label21
			});
			this.GroupFooter1.Height = 1.947917F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.96875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Label8.Text = "Total:";
			this.Label8.Top = 0.03125F;
			this.Label8.Width = 0.5F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 5.5625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-weight: bold; text-align: right";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0.03125F;
			this.fldTotal.Width = 1.03125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 1.75F;
			this.Line2.X1 = 4.875F;
			this.Line2.X2 = 6.625F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldComments
			// 
			this.fldComments.Height = 0.875F;
			this.fldComments.Left = 0.0625F;
			this.fldComments.Name = "fldComments";
			this.fldComments.Text = "Field1";
			this.fldComments.Top = 0.40625F;
			this.fldComments.Width = 6.5F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.03125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: left";
			this.Label17.Text = "Comments:";
			this.Label17.Top = 0.21875F;
			this.Label17.Width = 0.78125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 3.59375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.6875F;
			this.Line3.Width = 2.90625F;
			this.Line3.X1 = 3.59375F;
			this.Line3.X2 = 6.5F;
			this.Line3.Y1 = 1.6875F;
			this.Line3.Y2 = 1.6875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: left";
			this.Label18.Text = "Approved By";
			this.Label18.Top = 1.71875F;
			this.Label18.Width = 1.09375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.53125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold; text-align: right";
			this.Label19.Text = "Date";
			this.Label19.Top = 1.71875F;
			this.Label19.Width = 0.78125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.25F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.6875F;
			this.Line4.Width = 2.875F;
			this.Line4.X1 = 0.25F;
			this.Line4.X2 = 3.125F;
			this.Line4.Y1 = 1.6875F;
			this.Line4.Y2 = 1.6875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.25F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: left";
			this.Label20.Text = "Approved By";
			this.Label20.Top = 1.71875F;
			this.Label20.Width = 1.125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 2.1875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold; text-align: right";
			this.Label21.Text = "Date";
			this.Label21.Top = 1.71875F;
			this.Label21.Width = 0.75F;
			// 
			// rptNewPurchaseOrder
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptNewPurchaseOrder_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldDetailDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLineTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetailDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLineTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgIcon;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldComments;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
	}
}
