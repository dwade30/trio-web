﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetEncDataEntry.
	/// </summary>
	partial class frmGetEncDataEntry : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCComboBox cboEntry;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent5 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetEncDataEntry));
            this.cmbSearchType = new fecherFoundation.FCComboBox();
            this.lblSearchType = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.vs1 = new fecherFoundation.FCGrid();
            this.cmdGet = new fecherFoundation.FCButton();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.cmdGetAccountNumber = new fecherFoundation.FCButton();
            this.cboEntry = new fecherFoundation.FCComboBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.lblSearchInfo = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblLastAccount = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 518);
            this.BottomPanel.Size = new System.Drawing.Size(873, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdGetAccountNumber);
            this.ClientArea.Controls.Add(this.cboEntry);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblLastAccount);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(873, 458);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(873, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(275, 30);
            this.HeaderText.Text = "Get Encumbrance Entry";
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.Items.AddRange(new object[] {
            "Journal Number",
            "Vendor Number",
            "Vendor Name",
            "Description",
            "PO"});
            this.cmbSearchType.Location = new System.Drawing.Point(230, 30);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(215, 40);
            this.cmbSearchType.TabIndex = 12;
            this.cmbSearchType.SelectedIndexChanged += new System.EventHandler(this.optSearchType_CheckedChanged);
            // 
            // lblSearchType
            // 
            this.lblSearchType.AutoSize = true;
            this.lblSearchType.Location = new System.Drawing.Point(20, 44);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(79, 15);
            this.lblSearchType.TabIndex = 13;
            this.lblSearchType.Text = "SEARCH BY";
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.White;
            this.Frame3.Controls.Add(this.vs1);
            this.Frame3.Controls.Add(this.cmdGet);
            this.Frame3.Controls.Add(this.cmdReturn);
            this.Frame3.Location = new System.Drawing.Point(30, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(801, 407);
            this.Frame3.TabIndex = 15;
            this.Frame3.Text = "Multiple Records";
            this.Frame3.Visible = false;
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 6;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(20, 30);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 1;
            this.vs1.Size = new System.Drawing.Size(761, 313);
            this.vs1.TabIndex = 18;
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
            // 
            // cmdGet
            // 
            this.cmdGet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdGet.AppearanceKey = "actionButton";
            this.cmdGet.Location = new System.Drawing.Point(247, 358);
            this.cmdGet.Name = "cmdGet";
            this.cmdGet.Size = new System.Drawing.Size(158, 40);
            this.cmdGet.TabIndex = 17;
            this.cmdGet.Text = "Retrieve Record";
            this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
            // 
            // cmdReturn
            // 
            this.cmdReturn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(20, 358);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(212, 40);
            this.cmdReturn.TabIndex = 16;
            this.cmdReturn.Text = "Return to Search Screen";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // cmdGetAccountNumber
            // 
            this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
            this.cmdGetAccountNumber.Location = new System.Drawing.Point(30, 458);
            this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
            this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdGetAccountNumber.Size = new System.Drawing.Size(100, 48);
            this.cmdGetAccountNumber.TabIndex = 2;
            this.cmdGetAccountNumber.Text = "Process";
            this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // cboEntry
            // 
            this.cboEntry.BackColor = System.Drawing.SystemColors.Window;
            this.cboEntry.Location = new System.Drawing.Point(311, 102);
            this.cboEntry.Name = "cboEntry";
            this.cboEntry.Size = new System.Drawing.Size(348, 40);
            this.cboEntry.TabIndex = 1;
            this.cboEntry.DropDown += new System.EventHandler(this.cboEntry_DropDown);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmdClear);
            this.Frame2.Controls.Add(this.cmbSearchType);
            this.Frame2.Controls.Add(this.cmdSearch);
            this.Frame2.Controls.Add(this.lblSearchType);
            this.Frame2.Controls.Add(this.txtSearch);
            this.Frame2.Controls.Add(this.lblSearchInfo);
            this.Frame2.Location = new System.Drawing.Point(30, 162);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(465, 215);
            this.Frame2.Text = "Update";
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = Wisej.Web.AnchorStyles.Top;
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.Location = new System.Drawing.Point(20, 150);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(137, 40);
            this.cmdClear.TabIndex = 11;
            this.cmdClear.Text = "Clear Search";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = Wisej.Web.AnchorStyles.Top;
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(164, 150);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(83, 40);
            this.cmdSearch.TabIndex = 10;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            clientEvent5.Event = "keypress";
            clientEvent5.JavaScript = resources.GetString("clientEvent5.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.txtSearch).Add(clientEvent5);
            this.txtSearch.Location = new System.Drawing.Point(230, 90);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(215, 40);
            this.txtSearch.TabIndex = 9;
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // lblSearchInfo
            // 
            this.lblSearchInfo.Location = new System.Drawing.Point(20, 104);
            this.lblSearchInfo.Name = "lblSearchInfo";
            this.lblSearchInfo.Size = new System.Drawing.Size(167, 15);
            this.lblSearchInfo.TabIndex = 14;
            this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 116);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(216, 16);
            this.Label3.TabIndex = 23;
            this.Label3.Text = "ENTRIES WILL BE ADDED TO JOURNAL";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLastAccount
            // 
            this.lblLastAccount.Location = new System.Drawing.Point(191, 30);
            this.lblLastAccount.Name = "lblLastAccount";
            this.lblLastAccount.Size = new System.Drawing.Size(55, 16);
            this.lblLastAccount.TabIndex = 22;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(157, 16);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "LAST ENTRY ACCESSED";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 66);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(418, 16);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "SELECT A JOURNAL NUMBER OR ENTER 0 TO CREATE A NEW JOURNAL";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmGetEncDataEntry
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(873, 626);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmGetEncDataEntry";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Get Encumbrance Entry";
            this.Load += new System.EventHandler(this.frmGetEncDataEntry_Load);
            this.Activated += new System.EventHandler(this.frmGetEncDataEntry_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetEncDataEntry_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetEncDataEntry_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private JavaScript javaScript1;
	}
}
