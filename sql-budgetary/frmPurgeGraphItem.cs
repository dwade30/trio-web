﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurgeGraphItem.
	/// </summary>
	public partial class frmPurgeGraphItem : BaseForm
	{
		public frmPurgeGraphItem()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgeGraphItem InstancePtr
		{
			get
			{
				return (frmPurgeGraphItem)Sys.GetInstance(typeof(frmPurgeGraphItem));
			}
		}

		protected frmPurgeGraphItem _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmPurgeGraphItem_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (cboGraphItem.Items.Count == 0)
			{
				MessageBox.Show("No graph items to purge", "No Graph Items", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			this.Refresh();
		}

		private void frmPurgeGraphItem_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeGraphItem.FillStyle	= 0;
			//frmPurgeGraphItem.ScaleWidth	= 3885;
			//frmPurgeGraphItem.ScaleHeight	= 1995;
			//frmPurgeGraphItem.LinkTopic	= "Form2";
			//frmPurgeGraphItem.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsChartInfo = new clsDRWrapper();
			rsChartInfo.OpenRecordset("SELECT * FROM GraphItems ORDER BY Title");
			if (rsChartInfo.EndOfFile() != true && rsChartInfo.BeginningOfFile() != true)
			{
				do
				{
					cboGraphItem.AddItem(rsChartInfo.Get_Fields_String("Title"));
					cboGraphItem.ItemData(cboGraphItem.ListCount - 1, FCConvert.ToInt32(rsChartInfo.Get_Fields_Int32("ID")));
					rsChartInfo.MoveNext();
				}
				while (rsChartInfo.EndOfFile() != true);
			}
			if (cboGraphItem.Items.Count > 0)
			{
				cboGraphItem.SelectedIndex = 0;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeGraphItem_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsChartInfo = new clsDRWrapper();
			if (cboGraphItem.Items.Count > 0)
			{
				rsChartInfo.OpenRecordset("SELECT * FROM ItemsInChart WHERE GraphItemID = " + FCConvert.ToString(cboGraphItem.ItemData(cboGraphItem.SelectedIndex)));
				if (rsChartInfo.EndOfFile() != true && rsChartInfo.BeginningOfFile() != true)
				{
					MessageBox.Show("This graph item is used in one or more cash charts.  You must remove the item from these charts before you may proceed.", "Item In Chart(s)", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				ans = MessageBox.Show("Are you sure you wish to delete this graph item?", "Delete Graph Item?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					rs.Execute("DELETE FROM GraphItems WHERE ID = " + FCConvert.ToString(cboGraphItem.ItemData(cboGraphItem.SelectedIndex)), "Budgetary");
					MessageBox.Show("Graph Item Deleted", "Graph Item Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboGraphItem.Items.RemoveAt(cboGraphItem.SelectedIndex);
					if (cboGraphItem.Items.Count > 0)
					{
						cboGraphItem.SelectedIndex = 0;
					}
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
