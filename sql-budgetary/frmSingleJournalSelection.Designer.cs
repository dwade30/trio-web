﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSingleJournalSelection.
	/// </summary>
	partial class frmSingleJournalSelection : BaseForm
	{
		public fecherFoundation.FCComboBox cboJournals;
		public fecherFoundation.FCButton CancelButton;
		public fecherFoundation.FCButton OKButton;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSingleJournalSelection));
			this.cboJournals = new fecherFoundation.FCComboBox();
			this.CancelButton = new fecherFoundation.FCButton();
			this.OKButton = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OKButton)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 248);
			this.BottomPanel.Size = new System.Drawing.Size(452, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboJournals);
			this.ClientArea.Controls.Add(this.CancelButton);
			this.ClientArea.Controls.Add(this.OKButton);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(452, 188);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(452, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(201, 30);
			this.HeaderText.Text = "Journal Selection";
			// 
			// cboJournals
			// 
			this.cboJournals.AutoSize = false;
			this.cboJournals.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournals.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournals.FormattingEnabled = true;
			this.cboJournals.Location = new System.Drawing.Point(30, 68);
			this.cboJournals.Name = "cboJournals";
			this.cboJournals.Size = new System.Drawing.Size(300, 40);
			this.cboJournals.TabIndex = 3;
			this.cboJournals.SelectedIndexChanged += new System.EventHandler(this.cboJournals_SelectedIndexChanged);
			// 
			// CancelButton
			// 
			this.CancelButton.AppearanceKey = "actionButton";
			this.CancelButton.Location = new System.Drawing.Point(142, 140);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(82, 40);
			this.CancelButton.TabIndex = 1;
			this.CancelButton.Text = "Cancel";
			this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// OKButton
			// 
			this.OKButton.AppearanceKey = "actionButton";
			this.OKButton.Location = new System.Drawing.Point(30, 138);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size(82, 40);
			this.OKButton.TabIndex = 0;
			this.OKButton.Text = "OK";
			this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(339, 16);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "PLEASE SELECT A JOURNAL AND CLICK THE OK BUTTON";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmSingleJournalSelection
            // 
            this.AcceptButton = this.OKButton;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(452, 356);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSingleJournalSelection";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Journal Selection";
			this.Load += new System.EventHandler(this.frmSingleJournalSelection_Load);
			this.Activated += new System.EventHandler(this.frmSingleJournalSelection_Activated);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OKButton)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
