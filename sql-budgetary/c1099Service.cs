﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Linq;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using TWSharedLibrary;

namespace TWBD0000
{
	public class c1099Service
	{
		//=========================================================
		public void ArchiveExtractFile()
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorTaxInfo");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				rsArchiveInfo.OpenRecordset("SELECT * FROM VendorTaxInfoArchive WHERE Year = " + rsVendorInfo.Get_Fields("Year"));
				if (rsArchiveInfo.EndOfFile() != true && rsArchiveInfo.BeginningOfFile() != true)
				{
					answer = MessageBox.Show("There is already archived information for this year.  You will lose all your previously archived data by proceeding.  Do you wish to continue?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.No)
					{
						return;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Archiving 1099 Data";
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.Show();
				rsArchiveInfo.OmitNullsOnInsert = true;
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				rsArchiveInfo.Execute("DELETE FROM VendorTaxInfoArchive WHERE Year = " + rsVendorInfo.Get_Fields("Year"), "Budgetary");
				rsArchiveInfo.OpenRecordset("SELECT * FROM VendorTaxInfoArchive WHERE ID = 0");
				do
				{
					//Application.DoEvents();
					rsArchiveInfo.AddNew();
					rsArchiveInfo.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
					rsArchiveInfo.Set_Fields("Class", rsVendorInfo.Get_Fields_String("Class"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					rsArchiveInfo.Set_Fields("Amount", rsVendorInfo.Get_Fields("Amount"));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					rsArchiveInfo.Set_Fields("Year", rsVendorInfo.Get_Fields("Year"));
					rsArchiveInfo.Update(true);
					rsVendorInfo.MoveNext();
				}
				while (rsVendorInfo.EndOfFile() != true);
			}
			FCGlobal.Screen.MousePointer = 0;
			frmWait.InstancePtr.Unload();
			//Application.DoEvents();
			MessageBox.Show("Archive Successful", "Archive Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void CreateElectronic1099File(TaxFormType selectedType)
		{
			int intDisk;
			int intVendorCounter;
			int intRecordCounter;
			clsDRWrapper rsDefaults = new clsDRWrapper();
			int counter;
			int ans;
			string strTemp;
			string strDirectory;
			string temp = "";
			//FC:FINAL:BBE:#i674 - save file to temp folder, and download to client
			//cSaveFileDialog ofd = new cSaveFileDialog();
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
			Decimal curTotalAmount;
			c1099ElectronicFile eFile = new c1099ElectronicFile();
			FCFileSystem.FileClose(1);
			intDisk = 1;
			intVendorCounter = 0;
			intRecordCounter = 1;

            ITaxFormService taxFormService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxFormService()).Result;

			for (counter = 0; counter <= 16; counter++)
			{
				eFile.Set_curTaxTotals(counter, 0);
			}
			strDirectory = FCFileSystem.Statics.UserDataFolder;
			Information.Err().Clear();

			temp = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp", "IRSTax.txt");
			if (!Directory.Exists(Path.GetDirectoryName(temp)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(temp));
			}

            var eligibleVendors = taxFormService.GetEligibleVendorNumbers(selectedType, 600);

            if (eligibleVendors.Count > 0)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("There is no vendor information to report", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			FCFileSystem.FileClose(1);
            if (FCFileSystem.FileExists(temp))
            {
                FCFileSystem.DeleteFile(temp);
			}
            
			FCFileSystem.FileOpen(1, temp, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(modElectronic1099.Statics.AR));
			CreateTRecord(FCConvert.ToInt16(intRecordCounter), eligibleVendors.Count);
			FCFileSystem.FilePut(1, modElectronic1099.Statics.TR, -1/*? intRecordCounter */);
			intRecordCounter += 1;
			CreateARecord(FCConvert.ToInt16(intRecordCounter), selectedType);
			FCFileSystem.FilePut(1, modElectronic1099.Statics.AR, -1/*? intRecordCounter */);
			intRecordCounter += 1;
			do
			{
				CreateBRecord(ref intRecordCounter, eligibleVendors[intVendorCounter], ref eFile, selectedType, taxFormService);
				FCFileSystem.FilePut(1, modElectronic1099.Statics.BR, -1/*? intRecordCounter */);
				intRecordCounter += 1;
                intVendorCounter++;
            }
			while (intVendorCounter < eligibleVendors.Count);
			CreateCRecord(FCConvert.ToInt16(intRecordCounter), eligibleVendors.Count, eFile);
			FCFileSystem.FilePut(1, modElectronic1099.Statics.CR, -1/*? intRecordCounter */);
			intRecordCounter += 1;
            if (selectedType == TaxFormType.MISC1099)
            {
                CreateKRecord(ref intRecordCounter, eligibleVendors.Count, ref eFile);
                FCFileSystem.FilePut(1, modElectronic1099.Statics.KR, -1/*? intRecordCounter */);
                intRecordCounter += 1;
			}
			CreateFRecord(ref intRecordCounter, eligibleVendors.Count);
			FCFileSystem.FilePut(1, modElectronic1099.Statics.FR, -1/*? intRecordCounter */);
			intRecordCounter += 1;
			FCFileSystem.FileClose(1);
			curTotalAmount = 0;
			for (counter = 0; counter <= 16; counter++)
			{
				curTotalAmount += eFile.Get_curTaxTotals(counter);
			}

            string type = "";

            if (selectedType == TaxFormType.MISC1099)
            {
                type = "MISC";
			}
            else
            {
                type = "NEC";
            }

            rsDefaults.OpenRecordset("SELECT * FROM [1099FormTotals] WHERE FormName = '" + type + "'");

			if (!rsDefaults.EndOfFile() && !rsDefaults.BeginningOfFile())
            {
                rsDefaults.Edit();
			}
            else
            {
				rsDefaults.AddNew();
				rsDefaults.Set_Fields("FormName", type);
			}
			
			rsDefaults.Set_Fields("TotalAmountReported", curTotalAmount);
			rsDefaults.Set_Fields("TotalNumberOfFormsPrinted", eligibleVendors.Count);
			rsDefaults.Update();
			//FC:FINAL:BBE:#i674 - save file to temp folder, and download to client
			FCUtils.Download(temp);
			MessageBox.Show("Process Completed Successfully!", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		// vbPorter upgrade warning: intRecord As short	OnWriteFCConvert.ToInt32(
		private void CreateTRecord(short intRecord, int vendorCount)
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
			string strAddress;
			rsDefaults.OpenRecordset("SELECT * FROM [1099Information]");
			modElectronic1099.Statics.TR.RecType = "T";
			if (DateTime.Now.Month > 10)
			{
				modElectronic1099.Statics.TR.PaymentYear = FCConvert.ToString(DateTime.Now.Year);
			}
			else
			{
				modElectronic1099.Statics.TR.PaymentYear = FCConvert.ToString(DateTime.Now.Year - 1);
			}
			modElectronic1099.Statics.TR.PriorYearData = " ";
			modElectronic1099.Statics.TR.TransmittersTIN = Strings.Left(FCConvert.ToString(rsDefaults.Get_Fields_String("FederalCode")).Replace("-", "").Replace(" ", ""), 9);
			modElectronic1099.Statics.TR.TransmittersControlCode = Strings.Left(FCConvert.ToString(rsDefaults.Get_Fields_String("TCC")), 5);
			modElectronic1099.Statics.TR.Blank1 = Strings.StrDup(7, " ");
			modElectronic1099.Statics.TR.TestFileIndicator = " ";
			modElectronic1099.Statics.TR.ForeignEntityIndicator = " ";
			if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length > 40)
			{
				modElectronic1099.Statics.TR.TransmitterName = Strings.UCase(Strings.Left(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))), 40));
				modElectronic1099.Statics.TR.TransmitterNameContinued = Strings.UCase(Strings.Right(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))), Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length - 40));
			}
			else
			{
				modElectronic1099.Statics.TR.TransmitterName = Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length, " "));
				modElectronic1099.Statics.TR.TransmitterNameContinued = Strings.StrDup(40, " ");
			}
			if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length > 40)
			{
				modElectronic1099.Statics.TR.CompanyName = Strings.UCase(Strings.Left(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))), 40));
				modElectronic1099.Statics.TR.CompanyNameContinued = Strings.UCase(Strings.Right(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))), Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length - 40));
			}
			else
			{
				modElectronic1099.Statics.TR.CompanyName = Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length, " "));
				modElectronic1099.Statics.TR.CompanyNameContinued = Strings.StrDup(40, " ");
			}
			strAddress = Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Address1")))) + " " + Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Address2"))));
			if (strAddress.Length > 40)
			{
				modElectronic1099.Statics.TR.CompanyMailingAddress = Strings.Left(strAddress, 40);
			}
			else
			{
				modElectronic1099.Statics.TR.CompanyMailingAddress = (strAddress + Strings.StrDup(40 - strAddress.Length, " "));
			}
			modElectronic1099.Statics.TR.CompanyCity = (Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("City")))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("City"))).Length, " "));
			modElectronic1099.Statics.TR.CompanyState = Strings.UCase(FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
			modElectronic1099.Statics.TR.CompanyZip = (rsDefaults.Get_Fields_String("Zip") + rsDefaults.Get_Fields_String("Zip4"));
			modElectronic1099.Statics.TR.Blank2 = Strings.StrDup(15, " ");
			modElectronic1099.Statics.TR.TotalNumberOfPayees = (Strings.Trim(vendorCount.ToString()) + Strings.StrDup(8 - Strings.Trim(vendorCount.ToString()).Length, " "));
			if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Contact"))).Length > 40)
			{
				modElectronic1099.Statics.TR.ContactName = Strings.UCase(Strings.Left(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Contact"))), 40));
			}
			else
			{
				modElectronic1099.Statics.TR.ContactName = (Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Contact")))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Contact"))).Length, " "));
			}
			strAddress = Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Phone")).Replace("(", "").Replace(")", "").Replace("-", "")) + Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Extension")));
			if (strAddress.Length > 15)
			{
				modElectronic1099.Statics.TR.ContactPhoneAndExtension = Strings.Left(strAddress, 15);
			}
			else
			{
				modElectronic1099.Statics.TR.ContactPhoneAndExtension = (strAddress + Strings.StrDup(15 - strAddress.Length, " "));
			}
			if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Email"))).Length > 50)
			{
				modElectronic1099.Statics.TR.ContactEmail = Strings.Left(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Email"))), 50);
			}
			else
			{
				modElectronic1099.Statics.TR.ContactEmail = (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Email"))) + Strings.StrDup(50 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Email"))).Length, " "));
			}
			modElectronic1099.Statics.TR.Blank3 = Strings.StrDup(91, " ");
			modElectronic1099.Statics.TR.RecordSequenceNumber = (Strings.StrDup(8 - Strings.Trim(intRecord.ToString()).Length, "0") + FCConvert.ToString(intRecord));
			modElectronic1099.Statics.TR.Blank4 = Strings.StrDup(10, " ");
			modElectronic1099.Statics.TR.VendorIndicator = "V";
			modElectronic1099.Statics.TR.VendorName = ("HARRIS COMPUTER SYSTEMS" + Strings.StrDup(17, " "));
			modElectronic1099.Statics.TR.VendorMailingAddress = ("56 BANAIR ROAD" + Strings.StrDup(26, " "));
			modElectronic1099.Statics.TR.VendorCity = ("BANGOR" + Strings.StrDup(34, " "));
			modElectronic1099.Statics.TR.VendorState = "ME";
			modElectronic1099.Statics.TR.VendorZip = "04401    ";
			modElectronic1099.Statics.TR.VendorContact = ("DAVID WADE" + Strings.StrDup(30, " "));
			modElectronic1099.Statics.TR.VendorContactPhoneAndExtension = "2079426222     ";
			modElectronic1099.Statics.TR.Blank6 = Strings.StrDup(35, " ");
			modElectronic1099.Statics.TR.VendorForeignEntityIndicator = " ";
			modElectronic1099.Statics.TR.Blank5 = Strings.StrDup(8, " ");
			modElectronic1099.Statics.TR.BlankOrCRLF = "\r\n";
		}
		// vbPorter upgrade warning: intRecord As short	OnWriteFCConvert.ToInt32(
		private void CreateARecord(short intRecord, TaxFormType selectedType)
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
			string strAddress;
			rsDefaults.OpenRecordset("SELECT * FROM [1099Information]");
			modElectronic1099.Statics.AR.RecordType = "A";
			if (DateTime.Now.Month > 10)
			{
				modElectronic1099.Statics.AR.PaymentYear = FCConvert.ToString(DateTime.Now.Year);
			}
			else
			{
				modElectronic1099.Statics.AR.PaymentYear = FCConvert.ToString(DateTime.Now.Year - 1);
			}
			modElectronic1099.Statics.AR.CombinedFederalStateFiler = selectedType == TaxFormType.MISC1099 ? "1" : " ";
			modElectronic1099.Statics.AR.Blank1 = Strings.StrDup(5, " ");
			modElectronic1099.Statics.AR.PayerTIN = Strings.Left(FCConvert.ToString(rsDefaults.Get_Fields_String("FederalCode")).Replace("-", "").Replace(" ", ""), 9);
			modElectronic1099.Statics.AR.PayerNameControl = Strings.StrDup(4, " ");
			modElectronic1099.Statics.AR.LastFilingIndicator = " ";
			modElectronic1099.Statics.AR.TypeOfReturn = selectedType == TaxFormType.MISC1099 ? "A " : "NE";
			modElectronic1099.Statics.AR.AmountCodes = selectedType == TaxFormType.MISC1099 ? "1234568ABCDE    " : "14              ";
			modElectronic1099.Statics.AR.Blank2 = Strings.StrDup(8, " ");
			modElectronic1099.Statics.AR.ForeignEntityIndicator = " ";
			if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length > 40)
			{
				modElectronic1099.Statics.AR.FirstPayerNameLine = Strings.UCase(Strings.Left(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))), 40));
				modElectronic1099.Statics.AR.SecondPayerNameLine = Strings.UCase(Strings.Right(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))), Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length - 40));
			}
			else
			{
				modElectronic1099.Statics.AR.FirstPayerNameLine = Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Municipality"))).Length, " "));
				modElectronic1099.Statics.AR.SecondPayerNameLine = Strings.StrDup(40, " ");
			}
			modElectronic1099.Statics.AR.TransferAgentIndicator = "0";
			strAddress = Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Address1")))) + " " + Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Address2"))));
			if (strAddress.Length > 40)
			{
				modElectronic1099.Statics.AR.PayerShippingAddress = Strings.Left(strAddress, 40);
			}
			else
			{
				modElectronic1099.Statics.AR.PayerShippingAddress = (strAddress + Strings.StrDup(40 - strAddress.Length, " "));
			}
			modElectronic1099.Statics.AR.PayerCity = (Strings.UCase(Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("City")))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("City"))).Length, " "));
			modElectronic1099.Statics.AR.PayerState = Strings.UCase(FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
			modElectronic1099.Statics.AR.PayerZip = (rsDefaults.Get_Fields_String("Zip") + rsDefaults.Get_Fields_String("Zip4"));
			strAddress = Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Phone")).Replace("(", "").Replace(")", "").Replace("-", "")) + Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("Extension")));
			if (strAddress.Length > 15)
			{
				modElectronic1099.Statics.AR.PayerPhoneAndExtension = Strings.Left(strAddress, 15);
			}
			else
			{
				modElectronic1099.Statics.AR.PayerPhoneAndExtension = (strAddress + Strings.StrDup(15 - strAddress.Length, " "));
			}
			modElectronic1099.Statics.AR.Blank4 = Strings.StrDup(260, " ");
			modElectronic1099.Statics.AR.RecordSequenceNumber = (Strings.StrDup(8 - Strings.Trim(intRecord.ToString()).Length, "0") + FCConvert.ToString(intRecord));
			modElectronic1099.Statics.AR.Blank5 = Strings.StrDup(241, " ");
			modElectronic1099.Statics.AR.BlankOrCRLF = "\r\n";
		}
		// vbPorter upgrade warning: intRecord As short	OnWriteFCConvert.ToInt32(
		private void CreateBRecord(ref int intRecord, int vendorNumber, ref c1099ElectronicFile eFile, TaxFormType selectedType, ITaxFormService taxFormService)
		{
			Decimal curTotal;
			string strAddress;
			clsDRWrapper rsDefaults = new clsDRWrapper();
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			rsDefaults.OpenRecordset("SELECT * FROM [1099Information]");
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + vendorNumber);
			modElectronic1099.Statics.BR.RecordType = "B";
			if (DateTime.Now.Month > 10)
			{
				modElectronic1099.Statics.BR.PaymentYear = FCConvert.ToString(DateTime.Now.Year);
			}
			else
			{
				modElectronic1099.Statics.BR.PaymentYear = FCConvert.ToString(DateTime.Now.Year - 1);
			}
			modElectronic1099.Statics.BR.CorrectedReturnIndicator = " ";
			modElectronic1099.Statics.BR.NameControl = "    ";
			modElectronic1099.Statics.BR.TypeOfTIN = " ";
			modElectronic1099.Statics.BR.PayeesTIN = Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields_String("TaxNumber")).Replace("-", "").Replace(" ", ""), 9);
			modElectronic1099.Statics.BR.PayersAccountNumberForPayee = Strings.StrDup(20, " ");
			modElectronic1099.Statics.BR.PayersOfficeCode = Strings.StrDup(4, " ");
			modElectronic1099.Statics.BR.Blank1 = Strings.StrDup(10, " ");
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 1);
			eFile.Set_curTaxTotals(1, eFile.Get_curTaxTotals(1) + curTotal);
			modElectronic1099.Statics.BR.Payment1 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 2);
			eFile.Set_curTaxTotals(2, eFile.Get_curTaxTotals(2) + curTotal);
			modElectronic1099.Statics.BR.Payment2 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 3);
			eFile.Set_curTaxTotals(3, eFile.Get_curTaxTotals(3) + curTotal);
			modElectronic1099.Statics.BR.Payment3 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 4);
			eFile.Set_curTaxTotals(4, eFile.Get_curTaxTotals(4) + curTotal);
			modElectronic1099.Statics.BR.Payment4 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 5);
			eFile.Set_curTaxTotals(5, eFile.Get_curTaxTotals(5) + curTotal);
			modElectronic1099.Statics.BR.Payment5 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 6);
			eFile.Set_curTaxTotals(6, eFile.Get_curTaxTotals(6) + curTotal);
			modElectronic1099.Statics.BR.Payment6 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 7);
			eFile.Set_curTaxTotals(7, eFile.Get_curTaxTotals(7) + curTotal);
			modElectronic1099.Statics.BR.Payment7 = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			modElectronic1099.Statics.BR.Payment8 = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.Payment9 = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.PaymentA = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.PaymentB = Strings.StrDup(12, "0");
			curTotal = taxFormService.GetVendorTaxCategoryTotal(vendorNumber, selectedType, 14);
			eFile.Set_curTaxTotals(12, eFile.Get_curTaxTotals(12) + curTotal);
			modElectronic1099.Statics.BR.PaymentC = (Strings.StrDup(12 - ((int)(curTotal * 100)).ToString().Trim().Length, "0") + ((int)(curTotal * 100)).ToString().Trim());
			modElectronic1099.Statics.BR.PaymentD = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.PaymentE = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.PaymentF = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.PaymentG = Strings.StrDup(12, "0");
			modElectronic1099.Statics.BR.ForeignCountryIndicator = " ";
			if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("1099UseCorrName")))
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))).Length > 40)
				{
					modElectronic1099.Statics.BR.FirstPayeeNameLine = Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))), 40);
					modElectronic1099.Statics.BR.SecondPayeeNameLine = Strings.Right(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))), Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))).Length - 40);
				}
				else
				{
					modElectronic1099.Statics.BR.FirstPayeeNameLine = (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))).Length, " "));
					modElectronic1099.Statics.BR.SecondPayeeNameLine = "";
				}
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))).Length > 40)
				{
					modElectronic1099.Statics.BR.FirstPayeeNameLine = Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))), 40);
					modElectronic1099.Statics.BR.SecondPayeeNameLine = Strings.Right(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))), Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))).Length - 40);
				}
				else
				{
					modElectronic1099.Statics.BR.FirstPayeeNameLine = (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))).Length, " "));
					modElectronic1099.Statics.BR.SecondPayeeNameLine = "";
				}
			}
			modElectronic1099.Statics.BR.Blank2 = Strings.StrDup(40, " ");
			strAddress = Strings.Trim(Strings.UCase(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1")))) + " " + Strings.UCase(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2")))) + " " + Strings.UCase(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3")))));
			if (strAddress.Length > 40)
			{
				modElectronic1099.Statics.BR.PayeeMailingAddress = Strings.Left(strAddress, 40);
			}
			else
			{
				modElectronic1099.Statics.BR.PayeeMailingAddress = (strAddress + Strings.StrDup(40 - strAddress.Length, " "));
			}
			modElectronic1099.Statics.BR.Blank3 = Strings.StrDup(40, " ");
			modElectronic1099.Statics.BR.PayeeCity = (Strings.UCase(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity")))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("City"))).Length, " "));
			modElectronic1099.Statics.BR.PayeeState = Strings.UCase(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState")));
			modElectronic1099.Statics.BR.PayeeZip = (rsVendorInfo.Get_Fields_String("CheckZip") + rsVendorInfo.Get_Fields_String("CheckZip4"));
			modElectronic1099.Statics.BR.Blank4 = " ";
			modElectronic1099.Statics.BR.RecordSequenceNumber = (Strings.StrDup(8 - Strings.Trim(intRecord.ToString()).Length, "0") + FCConvert.ToString(intRecord));
			modElectronic1099.Statics.BR.Blank5 = Strings.StrDup(36, " ");
			modElectronic1099.Statics.BR.SecondTINNotice = " ";
			modElectronic1099.Statics.BR.Blank6 = "  ";
			modElectronic1099.Statics.BR.DirectSalesIndicator = " ";
			modElectronic1099.Statics.BR.Blank7 = Strings.StrDup(115, " ");
			modElectronic1099.Statics.BR.SpecialDataEntries = Strings.StrDup(60, " ");
			modElectronic1099.Statics.BR.StateIncomeTaxWithheld = Strings.StrDup(12, " ");
			modElectronic1099.Statics.BR.LocalIncomeTaxWithheld = Strings.StrDup(12, " ");
			modElectronic1099.Statics.BR.CombinedFederalStateCode = selectedType == TaxFormType.MISC1099 ? "23" : "  ";
			modElectronic1099.Statics.BR.BlankOrCRLF = "\r\n";
		}
		// vbPorter upgrade warning: intRecord As short	OnWriteFCConvert.ToInt32(
		private object CreateCRecord(short intRecord, int vendorCount, c1099ElectronicFile eFile)
		{
			object CreateCRecord = null;
			modElectronic1099.Statics.CR.RecordType = "C";
			modElectronic1099.Statics.CR.NumberOfPayees = (Strings.StrDup(8 - Strings.Trim(vendorCount.ToString()).Length, "0") + Strings.Trim(vendorCount.ToString()));
			modElectronic1099.Statics.CR.Blank1 = Strings.StrDup(6, " ");
			modElectronic1099.Statics.CR.ControlTotal1 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(1) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(1) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal2 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(2) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(2) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal3 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(3) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(3) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal4 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(4) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(4) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal5 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(5) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(5) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal6 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(6) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(6) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal7 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(7) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(7) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal8 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(8) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(8) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotal9 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(9) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(9) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalA = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(10) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(10) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalB = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(11) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(11) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalC = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(12) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(12) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalD = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(13) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(13) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalE = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(14) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(14) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalF = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(15) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(15) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.ControlTotalG = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(16) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(16) * 100)).ToString().Trim());
			modElectronic1099.Statics.CR.Blank2 = Strings.StrDup(232, " ");
			modElectronic1099.Statics.CR.RecordSequenceNumber = (Strings.StrDup(8 - Strings.Trim(intRecord.ToString()).Length, "0") + FCConvert.ToString(intRecord));
			modElectronic1099.Statics.CR.Blank3 = Strings.StrDup(241, " ");
			modElectronic1099.Statics.CR.BlankOrCRLF = "\r\n";
			return CreateCRecord;
		}
		// vbPorter upgrade warning: intRecord As short	OnWriteFCConvert.ToInt32(
		private object CreateKRecord(ref int intRecord, int vendorCount, ref c1099ElectronicFile eFile)
		{
			object CreateKRecord = null;
			modElectronic1099.Statics.KR.RecordType = "K";
			modElectronic1099.Statics.KR.NumberOfPayees = (Strings.StrDup(8 - Strings.Trim(vendorCount.ToString()).Length, "0") + Strings.Trim(vendorCount.ToString()));
			modElectronic1099.Statics.KR.Blank1 = Strings.StrDup(6, " ");
			modElectronic1099.Statics.KR.ControlTotal1 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(1) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(1) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal2 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(2) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(2) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal3 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(3) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(3) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal4 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(4) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(4) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal5 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(5) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(5) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal6 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(6) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(6) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal7 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(7) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(7) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal8 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(8) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(8) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotal9 = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(9) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(9) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalA = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(10) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(10) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalB = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(11) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(11) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalC = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(12) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(12) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalD = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(13) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(13) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalE = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(14) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(14) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalF = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(15) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(15) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.ControlTotalG = (Strings.StrDup(18 - ((int)(eFile.Get_curTaxTotals(16) * 100)).ToString().Trim().Length, "0") + ((int)(eFile.Get_curTaxTotals(16) * 100)).ToString().Trim());
			modElectronic1099.Statics.KR.Blank2 = Strings.StrDup(232, " ");
			modElectronic1099.Statics.KR.RecordSequenceNumber = (Strings.StrDup(8 - Strings.Trim(intRecord.ToString()).Length, "0") + FCConvert.ToString(intRecord));
			modElectronic1099.Statics.KR.Blank3 = Strings.StrDup(199, " ");
			modElectronic1099.Statics.KR.StateIncomeTaxWithheld = Strings.StrDup(18, " ");
			modElectronic1099.Statics.KR.LocalIncomeTaxWithheld = Strings.StrDup(18, " ");
			modElectronic1099.Statics.KR.Blank4 = Strings.StrDup(4, " ");
			modElectronic1099.Statics.KR.CombinedFederalStateCode = "23";
			modElectronic1099.Statics.KR.BlankOrCRLF = "\r\n";
			return CreateKRecord;
		}
		// vbPorter upgrade warning: intRecord As short	OnWriteFCConvert.ToInt32(
		private object CreateFRecord(ref int intRecord, int vendorCount)
		{
			object CreateFRecord = null;
			modElectronic1099.Statics.FR.RecordType = "F";
			modElectronic1099.Statics.FR.NumberOfARecords = "00000001";
			modElectronic1099.Statics.FR.Zeros = Strings.StrDup(21, "0");
			modElectronic1099.Statics.FR.Blank1 = Strings.StrDup(19, " ");
			modElectronic1099.Statics.FR.TotalNumberOfPayees = (Strings.StrDup(8 - Strings.Trim(vendorCount.ToString()).Length, "0") + Strings.Trim(vendorCount.ToString()));
			modElectronic1099.Statics.FR.Blank2 = Strings.StrDup(442, " ");
			modElectronic1099.Statics.FR.RecordSequenceNumber = (Strings.StrDup(8 - Strings.Trim(intRecord.ToString()).Length, "0") + FCConvert.ToString(intRecord));
			modElectronic1099.Statics.FR.Blank3 = Strings.StrDup(241, " ");
			modElectronic1099.Statics.FR.BlankOrCRLF = "\r\n";
			return CreateFRecord;
		}
    }
}
