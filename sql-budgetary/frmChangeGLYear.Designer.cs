﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeGLYear.
	/// </summary>
	partial class frmChangeGLYear : BaseForm
	{
		public fecherFoundation.FCComboBox cboGLYear;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangeGLYear));
			this.cboGLYear = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 189);
			this.BottomPanel.Size = new System.Drawing.Size(440, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboGLYear);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.cmdProcessSave);
			this.ClientArea.Size = new System.Drawing.Size(460, 363);
			this.ClientArea.Controls.SetChildIndex(this.cmdProcessSave, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.cboGLYear, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(460, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(179, 30);
			this.HeaderText.Text = "Select GL Year";
			// 
			// cboGLYear
			// 
			this.cboGLYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboGLYear.Location = new System.Drawing.Point(30, 73);
			this.cboGLYear.Name = "cboGLYear";
			this.cboGLYear.Size = new System.Drawing.Size(204, 40);
			this.cboGLYear.TabIndex = 1001;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(280, 23);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "PLEASE SELECT THE GL YEAR YOU WISH TO USE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(30, 141);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(104, 48);
			this.cmdProcessSave.TabIndex = 2;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmChangeGLYear
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(460, 423);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmChangeGLYear";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select GL Year";
			this.Load += new System.EventHandler(this.frmChangeGLYear_Load);
			this.Activated += new System.EventHandler(this.frmChangeGLYear_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChangeGLYear_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
