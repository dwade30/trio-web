﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using GrapeCity.ActiveReports;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintPurchaseOrderSummary.
	/// </summary>
	partial class frmPrintPurchaseOrderSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbVendor;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCComboBox cmbBoth;
		public fecherFoundation.FCLabel lblBoth;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintPurchaseOrderSummary));
			this.cmbVendor = new fecherFoundation.FCComboBox();
			this.lblVendor = new fecherFoundation.FCLabel();
			this.cmbBoth = new fecherFoundation.FCComboBox();
			this.lblBoth = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			//this.BottomPanel.Controls.Add(this.cmdPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 215);
			this.BottomPanel.Size = new System.Drawing.Size(415, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbVendor);
			this.ClientArea.Controls.Add(this.lblVendor);
			this.ClientArea.Controls.Add(this.cmbBoth);
			this.ClientArea.Controls.Add(this.lblBoth);
            this.ClientArea.Controls.Add(this.cmdPreview);
			this.ClientArea.Size = new System.Drawing.Size(415, 155);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(415, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(298, 30);
			this.HeaderText.Text = "Purchase Order Summary";
			// 
			// cmbVendor
			// 
			this.cmbVendor.AutoSize = false;
			this.cmbVendor.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbVendor.FormattingEnabled = true;
			this.cmbVendor.Items.AddRange(new object[] {
				"Vendor",
				"Date"
			});
			this.cmbVendor.Location = new System.Drawing.Point(252, 90);
			this.cmbVendor.Name = "cmbVendor";
			this.cmbVendor.Size = new System.Drawing.Size(121, 40);
			this.cmbVendor.TabIndex = 0;
			// 
			// lblVendor
			// 
			this.lblVendor.Location = new System.Drawing.Point(30, 104);
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Size = new System.Drawing.Size(70, 16);
			this.lblVendor.TabIndex = 1;
			this.lblVendor.Text = "ORDER BY";
			// 
			// cmbBoth
			// 
			this.cmbBoth.AutoSize = false;
			this.cmbBoth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBoth.FormattingEnabled = true;
			this.cmbBoth.Items.AddRange(new object[] {
				"Both",
				"Closed",
				"Open"
			});
			this.cmbBoth.Location = new System.Drawing.Point(252, 30);
			this.cmbBoth.Name = "cmbBoth";
			this.cmbBoth.Size = new System.Drawing.Size(121, 40);
			this.cmbBoth.TabIndex = 2;
			// 
			// lblBoth
			// 
			this.lblBoth.Location = new System.Drawing.Point(30, 44);
			this.lblBoth.Name = "lblBoth";
			this.lblBoth.Size = new System.Drawing.Size(157, 16);
			this.lblBoth.TabIndex = 3;
			this.lblBoth.Text = "PURCHASE ORDER STATUS";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 1;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Print Preview";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(40, 160);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(100, 48);
			this.cmdPreview.TabIndex = 1;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmPrintPurchaseOrderSummary
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(415, 323);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPrintPurchaseOrderSummary";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purchase Order Summary";
			this.Load += new System.EventHandler(this.frmPrintPurchaseOrderSummary_Load);
			this.Activated += new System.EventHandler(this.frmPrintPurchaseOrderSummary_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintPurchaseOrderSummary_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdPreview;
	}
}
