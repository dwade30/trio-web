﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAccountSetup.
	/// </summary>
	public partial class frmAccountSetup : BaseForm
	{
		public frmAccountSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtInfo = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtInfo.AddControlArrayElement(txtInfo_2, FCConvert.ToInt16(2));
			this.txtInfo.AddControlArrayElement(txtInfo_1, FCConvert.ToInt16(1));
			this.txtInfo.AddControlArrayElement(txtInfo_0, FCConvert.ToInt16(0));
			this.txtInfo.AddControlArrayElement(txtInfo_3, FCConvert.ToInt16(3));
			this.txtInfo.AddControlArrayElement(txtInfo_5, FCConvert.ToInt16(5));
			this.txtInfo.AddControlArrayElement(txtInfo_4, FCConvert.ToInt16(4));
			this.txtInfo.AddControlArrayElement(txtInfo_6, FCConvert.ToInt16(6));
			this.txtInfo.AddControlArrayElement(txtInfo_8, FCConvert.ToInt16(8));
			this.txtInfo.AddControlArrayElement(txtInfo_7, FCConvert.ToInt16(7));
			this.txtInfo.AddControlArrayElement(txtInfo_9, FCConvert.ToInt16(9));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			Form_Initialize();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAccountSetup InstancePtr
		{
			get
			{
				return (frmAccountSetup)Sys.GetInstance(typeof(frmAccountSetup));
			}
		}

		protected frmAccountSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool flag;
		bool blnUnload;

		private void cboAccountTypes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboAccountTypes.SelectedIndex == 0 || cboAccountTypes.SelectedIndex == 2)
			{
				frame1.Enabled = true;
				Frame2.Enabled = true;
				Frame3.Enabled = true;
			}
			else
			{
				frame1.Enabled = false;
				Frame2.Enabled = false;
				Frame3.Enabled = false;
			}
		}

		private void frmAccountSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			flag = true;
            txtInfo[0].Focus();
			frmAccountSetup.InstancePtr.Refresh();
		}

		private void Form_Initialize()
		{
			// initialize for the random function
		}

		private void frmAccountSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmAccountSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void frmAccountSetup_Load(object sender, System.EventArgs e)
		{
			string temp;
			int counter;
			int counter2 = 0;
			cboAccountTypes.SelectedIndex = 0;
			flag = false;
			temp = modAccountTitle.Statics.Exp;
			if (!(temp == "    "))
			{
				for (counter = 0; counter <= 3; counter++)
				{
					// show curent format numbers on screen
					counter2 = counter * 2;
					txtInfo[FCConvert.ToInt16(counter)].Text = Strings.Mid(temp, counter2 + 1, 2);
				}
				lblExpenditureDemo.Text = Demo(ref temp);
				// show an example of what it looks like
			}
			temp = modAccountTitle.Statics.Rev;
			if (!(temp == "   "))
			{
				for (counter = 4; counter <= 6; counter++)
				{
					// shwo current format numbers on screen
					counter2 = (counter - 4) * 2;
					txtInfo[FCConvert.ToInt16(counter)].Text = Strings.Mid(temp, counter2 + 1, 2);
				}
				lblRevenueDemo.Text = Demo(ref temp);
				// show an example of what it looks like
			}
			temp = modAccountTitle.Statics.Ledger;
			if (!(temp == "   "))
			{
				for (counter = 7; counter <= 9; counter++)
				{
					// show current format numbers
					counter2 = (counter - 7) * 2;
					txtInfo[FCConvert.ToInt16(counter)].Text = Strings.Mid(temp, counter2 + 1, 2);
				}
				lblLedgerDemo.Text = Demo(ref temp);
				// show an example of what it looks like
			}
			for (counter = 0; counter <= 9; counter++)
			{
				// set datachanged to false for all text boxes
				txtInfo[counter].DataChanged = false;
			}
			if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				chkPUCChartOfAccounts.CheckState = CheckState.Checked;
			}
			else
			{
				chkPUCChartOfAccounts.CheckState = CheckState.Unchecked;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void txtInfo_TextChanged(int Index, object sender, System.EventArgs e)
		{
			if (flag)
			{
				// if we should check
				if (txtInfo[Index].Text.Length == 2)
					Support.SendKeys("{TAB}", false);
				// if the length of the textbox is 1 send the user to the next box
			}
		}

		private void txtInfo_TextChanged(object sender, System.EventArgs e)
		{
			int index = txtInfo.IndexOf((FCTextBox)sender);
			txtInfo_TextChanged(index, sender, e);
		}

		private void txtInfo_Enter(int Index, object sender, System.EventArgs e)
		{
			txtInfo[Index].SelectionStart = 0;
			// highlight everything in a textbox when you enter it
			txtInfo[Index].SelectionLength = 2;
		}

		private void txtInfo_Enter(object sender, System.EventArgs e)
		{
			int index = txtInfo.IndexOf((FCTextBox)sender);
			txtInfo_Enter(index, sender, e);
		}

		private void txtInfo_Validating(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			int counter;
			if (!Information.IsNumeric(txtInfo[Index].Text))
			{
				// if the user enters something other than a number
				MessageBox.Show("You must enter a number in this field", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				// show an error message
				e.Cancel = true;
				// cancel the action
				txtInfo[Index].SelectionStart = 0;
				// highlight the incorrect field
				txtInfo[Index].SelectionLength = 2;
				return;
			}
			if (Index == 9)
			{
				// if we are on the year field
				if (FCConvert.ToDouble(txtInfo[Index].Text) != 0 && FCConvert.ToDouble(txtInfo[Index].Text) != 2)
				{
					// if it is not equal to 0 or 2 give an error message
					MessageBox.Show("You may only have a value of 0 or 2 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					// cancel the action
					txtInfo[Index].SelectionStart = 0;
					// highlight the incorrect field
					txtInfo[Index].SelectionLength = 2;
					return;
				}
			}
			if (Index == 7)
			{
				// if we are on the fund field
				if (FCConvert.ToDouble(txtInfo[Index].Text) != 1 && FCConvert.ToDouble(txtInfo[Index].Text) != 2)
				{
					// if it is not equal to 1 or 2 then give an error message
					MessageBox.Show("You may only have a value of 1 or 2 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					// cancel the action and highlight the incorrect field
					txtInfo[Index].SelectionStart = 0;
					txtInfo[Index].SelectionLength = 2;
					return;
				}
			}
			if (FCConvert.ToDouble(txtInfo[Index].Text) > 11)
			{
				MessageBox.Show("The maximum possible value you may have in a field is 11", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtInfo[Index].SelectionStart = 0;
				txtInfo[Index].SelectionLength = 2;
				return;
			}
			switch (Index)
			{
				case 0:
				case 2:
				case 4:
				case 6:
				case 7:
				case 8:
					{
						// check to make sure all the required fields have been filled in
						if (FCConvert.ToDouble(txtInfo[Index].Text) <= 0)
						{
							MessageBox.Show("This is not an optional field you must enter a number", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							txtInfo[Index].SelectionStart = 0;
							txtInfo[Index].SelectionLength = 2;
							return;
						}
						break;
					}
			}
			//end switch
			if (txtInfo[Index].Text.Length == 1)
			{
				flag = false;
				txtInfo[Index].Text = "0" + txtInfo[Index].Text;
				flag = true;
			}
			if (txtInfo[Index].DataChanged)
			{
				if (Index >= 0 && Index <= 3)
				{
					// save the new data and show a new demo
					for (counter = 0; counter <= 3; counter++)
					{
						temp += txtInfo[FCConvert.ToInt16(counter)].Text;
					}
					lblExpenditureDemo.Text = Demo(ref temp);
					txtInfo[Index].DataChanged = false;
				}
				else if (Index >= 4 && Index <= 6)
				{
					for (counter = 4; counter <= 6; counter++)
					{
						temp += txtInfo[FCConvert.ToInt16(counter)].Text;
					}
					lblRevenueDemo.Text = Demo(ref temp);
					txtInfo[Index].DataChanged = false;
				}
				else if (Index >= 7 && Index <= 9)
				{
					for (counter = 7; counter <= 9; counter++)
					{
						temp += txtInfo[FCConvert.ToInt16(counter)].Text;
					}
					lblLedgerDemo.Text = Demo(ref temp);
					txtInfo[Index].DataChanged = false;
				}
			}
			if (Index == 0)
			{
				// change the department number for revenue when the department number for expenditures is changed
				flag = false;
				// dont check for a change in the text box
				txtInfo[4].Text = txtInfo[Index].Text;
			}
			flag = true;
			// reinitialize the flag after the change is made
		}

		private void txtInfo_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int index = txtInfo.IndexOf((FCTextBox)sender);
			txtInfo_Validating(index, sender, e);
		}
		// creates an example account number based on the information entered by the user
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string Demo(ref string x)
		{
			string Demo = "";
			string temp = "";
			// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
			int counter;
			string strNextChar = "";
			if (Strings.Trim(x) != "")
			{
				while (x.Length > 0)
				{
					temp = Strings.Left(x, 2);
					// get first number from string
					if (x.Length > 2)
					{
						// if x has more than 1 character left in it
						x = Strings.Right(x, x.Length - 2);
						// take the first character off of it
					}
					else
					{
						x = "";
						// else set it equal to the null string
					}
					if (Conversion.Val(temp) > 0)
					{
						// if the size of this field i sgreater than 0 create a random number the size of the field
						for (counter = 1; counter <= FCConvert.ToInt32(Conversion.Val(temp)); counter++)
						{
							strNextChar = Strings.Trim(Conversion.Str(FCConvert.ToInt32(FCUtils.Rnd() * 9)));
							if (Conversion.Val(temp) == 1 && strNextChar == "0")
							{
								strNextChar = "1";
							}
							Demo = Strings.Trim(Demo) + strNextChar;
						}
						Demo = Strings.Trim(Demo) + "-";
						// seperate this field from the next
					}
				}
				temp = Strings.Mid(Demo, FCConvert.ToString(Demo).Length, 1);
				// if there is a seperator at the end of the string take it off
				if (temp == "-")
				{
					Demo = Strings.Left(Demo, FCConvert.ToString(Demo).Length - 1);
					// return the string
				}
			}
			return Demo;
		}

		private void SetCustomFormColors()
		{
			lblRevenueDemo.ForeColor = Color.Blue;
			lblLedgerDemo.ForeColor = Color.Blue;
			lblExpenditureDemo.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			bool Check;
			// vbPorter upgrade warning: total As short --> As int	OnWrite(short, double)
			int total;
			string temp = "";
			int counter;
			// make sure the division fields are the same or the revenue division is 0
			if (txtInfo[1].Text != txtInfo[5].Text && FCConvert.ToDouble(txtInfo[5].Text) != 0)
			{
				MessageBox.Show("The number in the 2 division fields must match or the Revenue Division must be 0", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			total = 0;
			for (counter = 0; counter <= 3; counter++)
			{
				total += FCConvert.ToInt32(Conversion.Val(txtInfo[FCConvert.ToInt16(counter)].Text));
			}
			if (total > 12)
			{
				MessageBox.Show("Expenditure format can only be a total of 12 characters in length", "Invalid Expenditure Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				for (counter = 0; counter <= 3; counter++)
				{
					// put the numbers into a string
					temp += txtInfo[FCConvert.ToInt16(counter)].Text;
				}
				Check = modBudgetaryAccounting.UpdateBDVariable("Expenditure", temp);
				modAccountTitle.Statics.Exp = temp;
				temp = "";
			}
			total = 0;
			for (counter = 4; counter <= 6; counter++)
			{
				total += FCConvert.ToInt32(Conversion.Val(txtInfo[FCConvert.ToInt16(counter)].Text));
			}
			if (total > 12)
			{
				MessageBox.Show("Revenue format can only be a total of 12 characters in length", "Invalid Revenue Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				for (counter = 4; counter <= 6; counter++)
				{
					// put the numbers in a string
					temp += txtInfo[FCConvert.ToInt16(counter)].Text;
				}
				Check = modBudgetaryAccounting.UpdateBDVariable("Revenue", temp);
				modAccountTitle.Statics.Rev = temp;
				temp = "";
			}
			total = 0;
			for (counter = 7; counter <= 9; counter++)
			{
				total += FCConvert.ToInt32(Conversion.Val(txtInfo[FCConvert.ToInt16(counter)].Text));
			}
			if (total > 12)
			{
				MessageBox.Show("Ledger format can only be a total of 12 characters in length", "Invalid Ledger Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				for (counter = 7; counter <= 9; counter++)
				{
					// put the numbers into a string
					temp += txtInfo[FCConvert.ToInt16(counter)].Text;
				}
				Check = modBudgetaryAccounting.UpdateBDVariable("Ledger", temp);
				modAccountTitle.Statics.Ledger = temp;
			}
			if (Conversion.Val(txtInfo[1].Text) == 0)
			{
				modAccountTitle.Statics.ExpDivFlag = true;
			}
			else
			{
				modAccountTitle.Statics.ExpDivFlag = false;
			}
			if (Conversion.Val(txtInfo[3].Text) == 0)
			{
				modAccountTitle.Statics.ObjFlag = true;
			}
			else
			{
				modAccountTitle.Statics.ObjFlag = false;
			}
			if (Conversion.Val(txtInfo[5].Text) == 0)
			{
				modAccountTitle.Statics.RevDivFlag = true;
			}
			else
			{
				modAccountTitle.Statics.RevDivFlag = false;
			}
			if (Conversion.Val(txtInfo[9].Text) == 0)
			{
				modAccountTitle.Statics.YearFlag = true;
			}
			else
			{
				modAccountTitle.Statics.YearFlag = false;
			}
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				modBudgetaryAccounting.Statics.strZeroDiv = "0";
			}
			else
			{
				modBudgetaryAccounting.Statics.strZeroDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
			}

			

			if (chkPUCChartOfAccounts.CheckState == CheckState.Checked)
			{
				modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts = true;
				modBudgetaryAccounting.UpdateBDVariable("PUCChartOfAccounts", true);
			}
			else
			{
				modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts = false;
				modBudgetaryAccounting.UpdateBDVariable("PUCChartOfAccounts", false);
			}
			modBudgetaryMaster.WriteAuditRecord_8("Saved information", "Account Format Setup Screen");
            //FC:FINAL:AM:#3147 - add save message
            MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (blnUnload)
			{
				// MDIParent.GetAccountMenu
				Close();
			}
		}
	}
}
