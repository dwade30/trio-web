﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBDAccountInquiry.
	/// </summary>
	public partial class frmBDAccountInquiry : BaseForm
	{
		public frmBDAccountInquiry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBDAccountInquiry InstancePtr
		{
			get
			{
				return (frmBDAccountInquiry)Sys.GetInstance(typeof(frmBDAccountInquiry));
			}
		}

		protected frmBDAccountInquiry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cBDAccountInquiryView theView;
		const int GridAccountColAccount = 0;
		const int GridAccountColNetBegin = 2;
		const int GridAccountColCredits = 3;
		const int GridAccountColDebits = 4;
		const int GridAccountColNet = 5;
		const int GridAccountColNetEnd = 6;
		const int GridAccountColDescription = 1;
		const int GridAccountColFullDescription = 7;
		private bool boolUpdating;
		private int[] segmentLengths = new int[5 + 1];

		private void chkHideNoActivity_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				theView.ExcludeNonActivity = chkHideNoActivity.CheckState == CheckState.Checked;
			}
		}

		private void chkHideZeroBalance_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				theView.ExcludeZeroBalance = chkHideZeroBalance.CheckState == CheckState.Checked;
			}
		}

		private void cmbAccountType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				if (cmbAccountType.SelectedIndex >= 0)
				{
					theView.AccountTypeToShow = FCConvert.ToInt16(cmbAccountType.ItemData(cmbAccountType.SelectedIndex));
				}
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			theView.PrintInquiry();
		}

		private void cmdPrintStatus_Click(object sender, System.EventArgs e)
		{
			FCUtils.StartTask(this, () =>
			{
				this.ShowWait();
				this.UpdateWait("Loading Data ...");
				PrintCurrentAccountStatus();
				this.EndWait();
			});
		}

		private void PrintCurrentAccountStatus()
		{
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				// lngRow = GridAccounts.
				lngRow = GridAccounts.Row;
				if (lngRow > 0)
				{
					theView.PrintAccountStatus(GridAccounts.TextMatrix(lngRow, GridAccountColAccount));
				}
				else
				{
					MessageBox.Show("You must select an account to print first", "No Account Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmBDAccountInquiry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBDAccountInquiry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBDAccountInquiry.FillStyle	= 0;
			//frmBDAccountInquiry.ScaleWidth	= 9300;
			//frmBDAccountInquiry.ScaleHeight	= 7800;
			//frmBDAccountInquiry.LinkTopic	= "Form2";
			//frmBDAccountInquiry.LockControls	= -1  'True;
			//frmBDAccountInquiry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//frmBusy bwait = new frmBusy();
			this.ShowWait();
			FCUtils.StartTask(this, () =>
			{
				try
				{
					this.UpdateWait("Loading Accounts");
					// On Error GoTo ErrorHandler
					theView = new cBDAccountInquiryView();
					//FC:FINAL:ASZ: Begin adding event handlers
					theView.AccountListChanged += theView_AccountListChanged;
					theView.AccountFilterChanged += theView_AccountFilterChanged;
					//FC:FINAL:ASZ: End adding event handlers
					boolUpdating = true;
					modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
					modGlobalFunctions.SetTRIOColors(this);
					SetupGridAccounts();
					SetupAccountTypeCombo();
					FillPeriodCombos();
					cmbPeriodStart.SelectedIndex = theView.StartPeriod - 1;
					cmbPeriodEnd.SelectedIndex = theView.EndPeriod - 1;
					//bwait.Message = "Loading Accounts";
					//bwait.StartBusy();
					//bwait.Show();
					theView.Refresh();
					//FC:FINAL:BBE:#i774 - Hide the frmBusy when the view is refreshed without any exception correctly.
					//bwait.StopBusy();
					//bwait.Hide();
					// RefreshAccountsGrid
					this.EndWait();
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					//bwait.StopBusy();
					//FC:FINAL:BBE:#i774 - Hide frmBusy instead of Unload to reactivate this form where we are working on. Then the click event for buttons is fired correctly.
					//bwait.Unload();
					//bwait.Hide();
					/*- bwait = null; */
					this.EndWait();
					boolUpdating = false;
					if (Information.Err(ex).Number == 0)
						return;
					if (Information.Err(ex).Number == 457)
					{
						MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "This may be caused by duplicate accounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			});
		}

		private void frmBDAccountInquiry_Resize(object sender, System.EventArgs e)
		{
			ResizeGridAccounts();
		}

		private void GridAccounts_DblClick(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = GridAccounts.MouseRow;
			if (lngRow > 0)
			{
				theView.ShowDetails(GridAccounts.TextMatrix(lngRow, GridAccountColAccount), this);
			}
		}

		private void GridAccounts_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridAccounts[e.ColumnIndex, e.RowIndex];
            int lngRow;
			lngRow = GridAccounts.GetFlexRowIndex(e.RowIndex);
			if (lngRow > 0)
			{
				//ToolTip1.SetToolTip(GridAccounts, GridAccounts.TextMatrix(lngRow, GridAccountColFullDescription));
				cell.ToolTipText =  GridAccounts.TextMatrix(lngRow, GridAccountColFullDescription);
			}
			else
			{
                //ToolTip1.SetToolTip(GridAccounts, "");
                cell.ToolTipText = "";
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
			Close();
		}

		private void SetupGridAccounts()
		{
			GridAccounts.Rows = 1;
			GridAccounts.ColHidden(GridAccountColFullDescription, true);
			GridAccounts.TextMatrix(0, GridAccountColAccount, "Account");
			GridAccounts.TextMatrix(0, GridAccountColNetBegin, "Net Begin");
			GridAccounts.TextMatrix(0, GridAccountColCredits, "Credits");
			GridAccounts.TextMatrix(0, GridAccountColDebits, "Debits");
			GridAccounts.TextMatrix(0, GridAccountColNet, "Net");
			GridAccounts.TextMatrix(0, GridAccountColNetEnd, "Net End");
			GridAccounts.TextMatrix(0, GridAccountColDescription, "Description");
			GridAccounts.ColAlignment(GridAccountColNetBegin, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridAccounts.ColAlignment(GridAccountColNetEnd, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridAccounts.ColAlignment(GridAccountColCredits, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridAccounts.ColAlignment(GridAccountColDebits, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridAccounts.ColAlignment(GridAccountColNet, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void SetupAccountTypeCombo()
		{
			cmbAccountType.Clear();
			cmbAccountType.AddItem("All");
			cmbAccountType.ItemData(0, 0);
			cmbAccountType.AddItem("E");
			cmbAccountType.ItemData(cmbAccountType.NewIndex, 1);
			cmbAccountType.AddItem("G");
			cmbAccountType.ItemData(cmbAccountType.NewIndex, 3);
			cmbAccountType.AddItem("R");
			cmbAccountType.ItemData(cmbAccountType.NewIndex, 2);
			cmbAccountType.SelectedIndex = 0;
		}

		private void ResizeGridAccounts()
		{
			int lngWidth;
			lngWidth = GridAccounts.WidthOriginal;
			// GridAccounts.ColWidth(GridAccountColAccount) = 0.2 * lngWidth
			// GridAccounts.ColWidth(GridAccountColNetBegin) = 0.15 * lngWidth
			// GridAccounts.ColWidth(GridAccountColCredits) = 0.15 * lngWidth
			// GridAccounts.ColWidth(GridAccountColDebits) = 0.15 * lngWidth
			// GridAccounts.ColWidth(GridAccountColNet) = 0.15 * lngWidth
			GridAccounts.ColWidth(GridAccountColAccount, FCConvert.ToInt32(0.17 * lngWidth));
			GridAccounts.ColWidth(GridAccountColDescription, FCConvert.ToInt32(0.27 * lngWidth));
			GridAccounts.ColWidth(GridAccountColNetBegin, FCConvert.ToInt32(0.11 * lngWidth));
			GridAccounts.ColWidth(GridAccountColCredits, FCConvert.ToInt32(0.11 * lngWidth));
			GridAccounts.ColWidth(GridAccountColDebits, FCConvert.ToInt32(0.11 * lngWidth));
			GridAccounts.ColWidth(GridAccountColNet, FCConvert.ToInt32(0.11 * lngWidth));
		}

		private void RefreshAccountsGrid()
		{
			int intPeriodStart;
			int intPeriodEnd;
			intPeriodStart = theView.StartPeriod;
			intPeriodEnd = theView.EndPeriod;
			boolUpdating = true;
			theView.AccountList.MoveFirst();
			cAccountSummaryItem summItem;
			GridAccounts.Rows = 1;
			int lngRow;
			GridAccounts.Visible = false;
			while (theView.AccountList.IsCurrent())
			{
				//Application.DoEvents();
				summItem = (cAccountSummaryItem)theView.AccountList.GetCurrentItem();
				if (!(summItem == null))
				{
					if (summItem.Include)
					{
						GridAccounts.Rows += 1;
						lngRow = GridAccounts.Rows - 1;
						GridAccounts.TextMatrix(lngRow, GridAccountColAccount, summItem.Account);
						GridAccounts.TextMatrix(lngRow, GridAccountColCredits, Strings.Format(summItem.Credits, "#,###,###,##0.00"));
						GridAccounts.TextMatrix(lngRow, GridAccountColDebits, Strings.Format(summItem.Debits, "#,###,###,##0.00"));
						GridAccounts.TextMatrix(lngRow, GridAccountColNet, Strings.Format(summItem.Net, "#,###,###,##0.00"));
						GridAccounts.TextMatrix(lngRow, GridAccountColNetBegin, Strings.Format(summItem.BeginningNet, "#,###,###,##0.00"));
						GridAccounts.TextMatrix(lngRow, GridAccountColNetEnd, Strings.Format(summItem.EndBalance, "#,###,###,##0.00"));
						GridAccounts.TextMatrix(lngRow, GridAccountColDescription, summItem.ShortDescription);
						GridAccounts.TextMatrix(lngRow, GridAccountColFullDescription, summItem.Description);
					}
				}
				theView.AccountList.MoveNext();
			}
			boolUpdating = false;
			GridAccounts.Visible = true;
            //FC:FINAL:AM:#2483 - select the first row
            if(GridAccounts.Rows > 1)
            {
                GridAccounts.Row = 1;
            }
		}

		private void RefreshAccountsFilter()
		{
			string strTemp = "";
			int intLen;
			boolUpdating = true;
			txtSegment1.Text = "";
			txtSegment2.Text = "";
			txtSegment3.Text = "";
			txtSegment4.Text = "";
			intLen = theView.GetSegmentLength(1);
			if (intLen == 0)
			{
				txtSegment1.Visible = false;
			}
			else
			{
				txtSegment1.Visible = true;
				txtSegment1.MaxLength = intLen;
				txtSegment1.Text = theView.GetSegmentFilter(1);
			}
			intLen = theView.GetSegmentLength(2);
			if (intLen == 0)
			{
				txtSegment2.Visible = false;
			}
			else
			{
				txtSegment2.Visible = true;
				txtSegment2.MaxLength = intLen;
				txtSegment2.Text = theView.GetSegmentFilter(2);
			}
			intLen = theView.GetSegmentLength(3);
			if (intLen == 0)
			{
				txtSegment3.Visible = false;
			}
			else
			{
				txtSegment3.Visible = true;
				txtSegment3.MaxLength = intLen;
				txtSegment3.Text = theView.GetSegmentFilter(3);
			}
			intLen = theView.GetSegmentLength(4);
			if (intLen == 0)
			{
				txtSegment4.Visible = false;
			}
			else
			{
				txtSegment4.Visible = true;
				txtSegment4.MaxLength = intLen;
				txtSegment4.Text = theView.GetSegmentFilter(4);
			}
			boolUpdating = false;
		}

		private void theView_AccountFilterChanged()
		{
			RefreshAccountsFilter();
		}

		private void theView_AccountListChanged()
		{
			RefreshAccountsGrid();
		}

		private void txtSegment1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
                //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                //Support.SendKeys("{TAB}", false);
                txtSegment2.Focus();
			}
		}

		private void txtSegment1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (FCConvert.ToInt32(KeyAscii) >= 0 && FCConvert.ToInt32(KeyAscii) <= 30)
			{
			}
			else if ((FCConvert.ToInt32(KeyAscii) >= FCConvert.ToInt32(Keys.D0)) && FCConvert.ToInt32(KeyAscii) <= FCConvert.ToInt32(Keys.D9) || (e.KeyChar == 'X' || e.KeyChar == 'x'))
			{
                //FC:FINAL:AM:#2482 - the new char is already added to the text
                //if (txtSegment1.Text.Length + 1 >= theView.GetSegmentLength(1))
                if (txtSegment1.Text.Length >= theView.GetSegmentLength(1))
                {
                    //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                    //Support.SendKeys("{TAB}", false);
                    txtSegment2.Focus();
				}
			}
			else
			{
				KeyAscii = Keys.D0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSegment1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolUpdating)
			{
				theView.SetSegmentFilter(1, txtSegment1.Text);
			}
		}

		private void txtSegment2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
                //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                //Support.SendKeys("{TAB}", false);
                txtSegment3.Focus();

            }
        }

		private void txtSegment2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (FCConvert.ToInt32(KeyAscii) >= 0 && FCConvert.ToInt32(KeyAscii) <= 30)
			{
			}
			else if ((FCConvert.ToInt32(KeyAscii) >= FCConvert.ToInt32(Keys.D0)) && FCConvert.ToInt32(KeyAscii) <= FCConvert.ToInt32(Keys.D9) || (e.KeyChar == 'X' || e.KeyChar == 'x'))
			{
				{
                    //FC:FINAL:AM:#2482 - the new char is already added to the text
                    //if (txtSegment2.Text.Length + 1 >= theView.GetSegmentLength(2))
                    if (txtSegment2.Text.Length >= theView.GetSegmentLength(2))
                    {
                        //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                        //Support.SendKeys("{TAB}", false);
                        txtSegment3.Focus();

                    }
                }
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSegment2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolUpdating)
			{
				theView.SetSegmentFilter(2, txtSegment2.Text);
			}
		}

		private void txtSegment3_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
                //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                //Support.SendKeys("{TAB}", false);
                txtSegment4.Focus();

            }
        }

		private void txtSegment3_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (FCConvert.ToInt32(KeyAscii) >= 0 && FCConvert.ToInt32(KeyAscii) <= 30)
			{
			}
			else if ((FCConvert.ToInt32(KeyAscii) >= FCConvert.ToInt32(Keys.D0)) && FCConvert.ToInt32(KeyAscii) <= FCConvert.ToInt32(Keys.D9) || (e.KeyChar == 'X' || e.KeyChar == 'x'))
			{
                //FC:FINAL:AM:#2482 - the new char is already added to the text
                //if (txtSegment3.Text.Length + 1 >= theView.GetSegmentLength(3))
                if (txtSegment3.Text.Length >= theView.GetSegmentLength(3))
                {
                    //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                    //Support.SendKeys("{TAB}", false);
                    txtSegment4.Focus();

                }
            }
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSegment3_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolUpdating)
			{
				theView.SetSegmentFilter(3, txtSegment3.Text);
			}
		}

		private void txtSegment4_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
                //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                //Support.SendKeys("{TAB}", false);
                chkHideNoActivity.Focus();
			}
		}

		private void txtSegment4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (FCConvert.ToInt32(KeyAscii) >= 0 && FCConvert.ToInt32(KeyAscii) <= 30)
			{
			}
			else if ((FCConvert.ToInt32(KeyAscii) >= FCConvert.ToInt32(Keys.D0)) && FCConvert.ToInt32(KeyAscii) <= FCConvert.ToInt32(Keys.D9) || (e.KeyChar == 'X' || e.KeyChar == 'x'))
			{
                //FC:FINAL:AM:#2482 - the new char is already added to the text
                //if (txtSegment4.Text.Length + 1 >= theView.GetSegmentLength(4))
                if (txtSegment4.Text.Length >= theView.GetSegmentLength(4))
                {
                    //FC:FINAL:PB - issue #2775: "TAB" key kept calling Keydown and Keypress events, replaced with .Focus()
                    //Support.SendKeys("{TAB}", false);
                    chkHideNoActivity.Focus();
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSegment4_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolUpdating)
			{
                //FC:FINAL:PB - issue #2775
                //theView.SetSegmentFilter(1, txtSegment1.Text);
                theView.SetSegmentFilter(4, txtSegment4.Text);
            }
		}

		private void cmbPeriodStart_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				if (theView.StartPeriod != cmbPeriodStart.SelectedIndex + 1)
				{
					theView.StartPeriod = cmbPeriodStart.SelectedIndex + 1;
					theView.FilterAccountList();
				}
			}
		}

		private void cmbPeriodEnd_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				if (theView.EndPeriod != cmbPeriodEnd.SelectedIndex + 1)
				{
					theView.EndPeriod = cmbPeriodEnd.SelectedIndex + 1;
					theView.FilterAccountList();
				}
			}
		}

		private void FillPeriodCombos()
		{
			int x;
			cmbPeriodEnd.Clear();
			cmbPeriodStart.Clear();
			string strTemp = "";
			for (x = 1; x <= 12; x++)
			{
				switch (x)
				{
					case 1:
						{
							strTemp = "January";
							break;
						}
					case 2:
						{
							strTemp = "February";
							break;
						}
					case 3:
						{
							strTemp = "March";
							break;
						}
					case 4:
						{
							strTemp = "April";
							break;
						}
					case 5:
						{
							strTemp = "May";
							break;
						}
					case 6:
						{
							strTemp = "June";
							break;
						}
					case 7:
						{
							strTemp = "July";
							break;
						}
					case 8:
						{
							strTemp = "August";
							break;
						}
					case 9:
						{
							strTemp = "September";
							break;
						}
					case 10:
						{
							strTemp = "October";
							break;
						}
					case 11:
						{
							strTemp = "November";
							break;
						}
					case 12:
						{
							strTemp = "December";
							break;
						}
				}
				//end switch
				cmbPeriodStart.AddItem(strTemp);
				cmbPeriodEnd.AddItem(strTemp);
			}
		}
	}
}
