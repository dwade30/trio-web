﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptExpRevSummaryRevLevel.
	/// </summary>
	public partial class srptExpRevSummaryRevLevel : FCSectionReport
	{
		public static srptExpRevSummaryRevLevel InstancePtr
		{
			get
			{
				return (srptExpRevSummaryRevLevel)Sys.GetInstance(typeof(srptExpRevSummaryRevLevel));
			}
		}

		protected srptExpRevSummaryRevLevel _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRevActivityDetail.Dispose();
				rsRevBudgetInfo.Dispose();
				rsRevYTDActivity.Dispose();
				rsRevenueInfo.Dispose();
				rsRevCurrentActivity.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptExpRevSummaryRevLevel	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strLowDetail = "";
		string strDateRange;
		// reporting range A - All, M - Month Range, S - Single Month
		int intLowDate;
		int intHighDate;
		bool blnIncEnc;
		// should encumbrance money be calculated into YTD amounts
		bool blnIncPending;
		// should pending activity be included in YTD amounts
		clsDRWrapper rsRevCurrentActivity = new clsDRWrapper();
		// recordset holds money activity within date rnage for selected departments in revenues
		clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
		// recordset holds monthly money activity within date rnage for selected departments in revenues
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		// recordset holds YTD money activity selected departments in revenues
		clsDRWrapper rsRevBudgetInfo = new clsDRWrapper();
		string strPeriodCheck = "";
		// AND or OR depending on if intLowDate > intHighDate
		string strExpOrRev = "";
		// R or E telling program whether we are reporting on Revenues or Expenses at the moment
		int intCurrentMonth;
		bool blnFirstRecord;
		// used in fetchdata to let us know not to move forward in the recordset on the first pass
		clsDRWrapper rsRevenueInfo = new clsDRWrapper();
		// holds division info for departments we are reporting on
		string CurrentDepartment = "";
		// current department we are reporting on
		string CurrentDivision = "";
		// current division we are reporting on
		string CurrentRevenue = "";
		string strParentReport = "";

		public srptExpRevSummaryRevLevel()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: blnAdd As Variant --> As bool
			bool blnAdd;
			// - "AutoDim"
			int counter;
			blnAdd = true;
			for (counter = 1; counter <= this.Fields.Count; counter++)
			{
				if (this.Fields[counter - 1].Name == "Binder")
				{
					blnAdd = false;
					break;
				}
			}
			if (blnAdd)
			{
				this.Fields.Add("Binder");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsRevenueInfo.EndOfFile();
			}
			else
			{
				rsRevenueInfo.MoveNext();
				eArgs.EOF = rsRevenueInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				CurrentRevenue = FCConvert.ToString(rsRevenueInfo.Get_Fields_String("Revenue"));
				this.Fields["Binder"].Value = rsRevenueInfo.Get_Fields_String("Revenue");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsRevenueTitle = new clsDRWrapper())
            {
                // get division title
                if (modAccountTitle.Statics.RevDivFlag)
                {
                    rsRevenueTitle.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + CurrentDepartment +
                                                 "' AND Revenue = '" + CurrentRevenue + "'");
                }
                else
                {
                    rsRevenueTitle.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + CurrentDepartment +
                                                 "' AND Division = '" + CurrentDivision + "' AND Revenue = '" +
                                                 CurrentRevenue + "'");
                }

                // show information for division
                if (rsRevenueTitle.EndOfFile() != true && rsRevenueTitle.BeginningOfFile() != true)
                {
                    fldAccount.Text = CurrentRevenue + "  " + rsRevenueTitle.Get_Fields_String("ShortDescription");
                }
                else
                {
                    fldAccount.Text = CurrentRevenue + "  UNKNOWN";
                }

                fldBudget.Text = Strings.Format(GetNetBudget(), "#,##0.00");
                fldCurrentMonth.Text = Strings.Format(GetCurrentNet(), "#,##0.00");
                fldYTD.Text = Strings.Format(GetYTDNet(), "#,##0.00");
                // If strExpOrRev = "E" Then
                fldBalance.Text = Strings.Format(GetBalance(), "#,##0.00");
                // Else
                // fldBalance = format(GetBalance * -1, "#,##0.00")
                // End If
                fldSpent.Text = Strings.Format(GetSpent() * 100, "0.00");
                // update totals
                if (strParentReport == "DE")
                {
                    if (modAccountTitle.Statics.RevDivFlag)
                    {
                        rptExpRevSummary.InstancePtr.curRevBudget += FCConvert.ToDecimal(fldBudget.Text);
                        rptExpRevSummary.InstancePtr.curRevCurrentNet += FCConvert.ToDecimal(fldCurrentMonth.Text);
                        rptExpRevSummary.InstancePtr.curRevYTDNet += FCConvert.ToDecimal(fldYTD.Text);
                        // Dave 4/9/03
                        rptExpRevSummary.InstancePtr.curRevBalance += FCConvert.ToDecimal(fldBalance.Text);
                    }
                }
                else
                {
                    rptExpRevDivSummary.InstancePtr.curRevBudget += FCConvert.ToDecimal(fldBudget.Text);
                    rptExpRevDivSummary.InstancePtr.curRevCurrentNet += FCConvert.ToDecimal(fldCurrentMonth.Text);
                    rptExpRevDivSummary.InstancePtr.curRevYTDNet += FCConvert.ToDecimal(fldYTD.Text);
                    // Dave 4/9/03
                    rptExpRevDivSummary.InstancePtr.curRevBalance += FCConvert.ToDecimal(fldBalance.Text);
                }
            }
        }

		private void RetrieveInfo()
		{
			int HighDate;
			int LowDate;
			string strPeriodCheckHolder;
			HighDate = intHighDate;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rsRevYTDActivity.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division, Revenue");
			rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department, Division, Revenue");
			strPeriodCheck = strPeriodCheckHolder;
			rsRevCurrentActivity.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Revenue");
			rsRevActivityDetail.OpenRecordset("SELECT Period, Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Revenue, Period");
		}

		private decimal GetCurrentDebits()
		{
			decimal GetCurrentDebits = 0;
			if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
			}
			else
			{
				GetCurrentDebits = 0;
			}
			return GetCurrentDebits;
		}

		private decimal GetCurrentCredits()
		{
			decimal GetCurrentCredits = 0;
			if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
			}
			else
			{
				GetCurrentCredits = 0;
			}
			return GetCurrentCredits;
		}

		private decimal GetCurrentNet()
		{
			decimal GetCurrentNet = 0;
			GetCurrentNet = (GetCurrentDebits() + GetCurrentCredits()) * -1;
			return GetCurrentNet;
		}

		private decimal GetYTDDebit()
		{
			decimal GetYTDDebit = 0;
			if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
			}
			else
			{
				GetYTDDebit = 0;
			}
			if (blnIncEnc)
			{
				GetYTDDebit += GetEncumbrance();
			}
			if (blnIncPending)
			{
				GetYTDDebit += GetPendingDebits();
			}
			return GetYTDDebit;
		}

		private decimal GetYTDCredit()
		{
			decimal GetYTDCredit = 0;
			if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
			}
			else
			{
				GetYTDCredit = 0;
			}
			if (blnIncPending)
			{
				GetYTDCredit += GetPendingCredits();
			}
			return GetYTDCredit;
		}

		private decimal GetYTDNet()
		{
			decimal GetYTDNet = 0;
			GetYTDNet = (GetYTDDebit() + GetYTDCredit()) * -1;
			return GetYTDNet;
		}

		private decimal GetEncumbrance()
		{
			decimal GetEncumbrance = 0;
			if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private decimal GetPending()
		{
			decimal GetPending = 0;
			GetPending = (GetPendingDebits() + GetPendingCredits()) * -1;
			return GetPending;
		}

		private decimal GetBalance()
		{
			decimal GetBalance = 0;
			GetBalance = GetNetBudget() - ((GetYTDDebit() + GetYTDCredit()) * -1);
			return GetBalance;
		}

		private decimal GetSpent()
		{
			decimal GetSpent = 0;
			int temp;
			decimal temp2;
			decimal temp3;
			temp2 = GetBalance();
			temp3 = GetNetBudget();
			if (temp3 == 0)
			{
				GetSpent = 0;
				return GetSpent;
			}
			if (temp2 > temp3)
			{
				GetSpent = (temp3 - temp2) / temp3;
				return GetSpent;
			}
			GetSpent = (temp3 - temp2) / temp3;
			return GetSpent;
		}

		private decimal GetPendingCredits()
		{
			decimal GetPendingCredits = 0;
			if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
			}
			else
			{
				GetPendingCredits = 0;
			}
			return GetPendingCredits;
		}

		private decimal GetPendingDebits()
		{
			decimal GetPendingDebits = 0;
			if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
					}
					else
					{
						if (rsRevCurrentActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
			}
			else
			{
				GetPendingDebits = 0;
			}
			return GetPendingDebits;
		}

		private decimal GetOriginalBudget()
		{
			decimal GetOriginalBudget = 0;
			if (rsRevBudgetInfo.EndOfFile() != true && rsRevBudgetInfo.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
					}
					else
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
					}
					else
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private decimal GetBudgetAdjustments()
		{
			decimal GetBudgetAdjustments = 0;
			if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Revenue")) == CurrentRevenue)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private decimal GetNetBudget()
		{
			decimal GetNetBudget = 0;
			GetNetBudget = GetOriginalBudget() + GetBudgetAdjustments();
			return GetNetBudget;
		}

		private string MonthCalc(int X)
		{
			string MonthCalc = "";
			switch (X)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}
		// vbPorter upgrade warning: intLowMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intHighMonth As short	OnWriteFCConvert.ToInt32(
		public void Init(string strMonthRange, bool blnIncludeEncumbrances, bool blnIncludePendingInformation, string strDept, string strDiv = "", short intLowMonth = 0, short intHighMonth = 0, string strWhereToSendTotals = "DE")
		{
			// initialize all variables for this report
			strParentReport = strWhereToSendTotals;
			CurrentDepartment = strDept;
			CurrentDivision = strDiv;
			strDateRange = strMonthRange;
			blnIncEnc = blnIncludeEncumbrances;
			blnIncPending = blnIncludePendingInformation;
			if (strDateRange == "A")
			{
				intLowDate = modBudgetaryMaster.Statics.FirstMonth;
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					intHighDate = 12;
				}
				else
				{
					intHighDate = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else if (strDateRange == "M")
			{
				intLowDate = intLowMonth;
				intHighDate = intHighMonth;
			}
			else
			{
				intLowDate = intLowMonth;
				intHighDate = intLowMonth;
			}
			if (intLowDate > intHighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			// put information into recordsets
			RetrieveInfo();
			// check to see if there are any departments with activity to report on
			if (modAccountTitle.Statics.RevDivFlag)
			{
				rsRevenueInfo.OpenRecordset("SELECT DISTINCT Revenue FROM RevenueReportInfo WHERE Department = '" + CurrentDepartment + "'");
			}
			else
			{
				rsRevenueInfo.OpenRecordset("SELECT DISTINCT Revenue FROM RevenueReportInfo WHERE Department = '" + CurrentDepartment + "' AND Division = '" + CurrentDivision + "'");
			}
			if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
				return;
			}
		}

		
		private void srptExpRevSummaryRevLevel_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
