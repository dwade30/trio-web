﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptBDAccountStatusDetail.
	/// </summary>
	partial class srptBDAccountStatusDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBDAccountStatusDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBudgetAdjustments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBudAdj = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldYTDExp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYTDEnc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblYTDExp = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYTDEnc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldMonthJan = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthFeb = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthMar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthApr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthMay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthJune = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthJuly = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthAug = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthSept = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthOct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthNov = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthDec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJanRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJanRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJanBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJanBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFebRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFebRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFebBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFebBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMarRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMarRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMarBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMarBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAprRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAprRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAprBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAprBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMayRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMayRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMayBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMayBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJuneRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJuneRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJuneBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJuneBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJulyRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJulyRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJulyBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJulyBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAugRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAugRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAugBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAugBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeptRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeptRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeptBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeptBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOctRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOctRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOctBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOctBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNovRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNovRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNovBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNovBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDecRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDecRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDecBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDecBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalRegDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalBudDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalBudCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudgetAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBudAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDExp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDEnc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYTDExp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYTDEnc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthJan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthFeb)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthMar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthApr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthMay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthJune)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthJuly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthAug)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthSept)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthOct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthNov)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthDec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalBudDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalBudCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPeriod,
				this.fldJournal,
				this.fldDate,
				this.fldVendor,
				this.fldDescription,
				this.fldRCB,
				this.fldDebits,
				this.fldCredits,
				this.fldType,
				this.fldCheck
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblDateRange,
				this.lblAccount,
				this.fldBudget,
				this.fldBudgetAdjustments,
				this.lblBudget,
				this.lblBudAdj,
				this.fldYTDExp,
				this.fldYTDEnc,
				this.lblYTDExp,
				this.lblYTDEnc,
				this.fldBalance,
				this.lblBalance,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Line1,
				this.Label30,
				this.Binder
			});
			this.GroupHeader1.Height = 1.364583F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Line2,
				this.fldMonthJan,
				this.fldMonthFeb,
				this.fldMonthMar,
				this.fldMonthApr,
				this.fldMonthMay,
				this.fldMonthJune,
				this.fldMonthJuly,
				this.fldMonthAug,
				this.fldMonthSept,
				this.fldMonthOct,
				this.fldMonthNov,
				this.fldMonthDec,
				this.fldTotals,
				this.fldJanRegDebit,
				this.fldJanRegCredit,
				this.fldJanBudDebit,
				this.fldJanBudCredit,
				this.fldFebRegDebit,
				this.fldFebRegCredit,
				this.fldFebBudDebit,
				this.fldFebBudCredit,
				this.fldMarRegDebit,
				this.fldMarRegCredit,
				this.fldMarBudDebit,
				this.fldMarBudCredit,
				this.fldAprRegDebit,
				this.fldAprRegCredit,
				this.fldAprBudDebit,
				this.fldAprBudCredit,
				this.fldMayRegDebit,
				this.fldMayRegCredit,
				this.fldMayBudDebit,
				this.fldMayBudCredit,
				this.fldJuneRegDebit,
				this.fldJuneRegCredit,
				this.fldJuneBudDebit,
				this.fldJuneBudCredit,
				this.fldJulyRegDebit,
				this.fldJulyRegCredit,
				this.fldJulyBudDebit,
				this.fldJulyBudCredit,
				this.fldAugRegDebit,
				this.fldAugRegCredit,
				this.fldAugBudDebit,
				this.fldAugBudCredit,
				this.fldSeptRegDebit,
				this.fldSeptRegCredit,
				this.fldSeptBudDebit,
				this.fldSeptBudCredit,
				this.fldOctRegDebit,
				this.fldOctRegCredit,
				this.fldOctBudDebit,
				this.fldOctBudCredit,
				this.fldNovRegDebit,
				this.fldNovRegCredit,
				this.fldNovBudDebit,
				this.fldNovBudCredit,
				this.fldDecRegDebit,
				this.fldDecRegCredit,
				this.fldDecBudDebit,
				this.fldDecBudCredit,
				this.fldTotalRegDebit,
				this.fldTotalRegCredit,
				this.fldTotalBudDebit,
				this.fldTotalBudCredit
			});
			this.GroupFooter1.Height = 3.65625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label21,
				this.fldDebitTotal,
				this.fldCreditTotal,
				this.Line3
			});
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Current Account Status";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.78125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.5F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 2.125F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = null;
			this.lblDateRange.Top = 0.1875F;
			this.lblDateRange.Width = 3.5625F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 1F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.lblAccount.Text = "Label8";
			this.lblAccount.Top = 0.375F;
			this.lblAccount.Width = 5.8125F;
			// 
			// fldBudget
			// 
			this.fldBudget.Height = 0.1875F;
			this.fldBudget.Left = 0.34375F;
			this.fldBudget.Name = "fldBudget";
			this.fldBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldBudget.Text = null;
			this.fldBudget.Top = 0.65625F;
			this.fldBudget.Width = 1.0625F;
			// 
			// fldBudgetAdjustments
			// 
			this.fldBudgetAdjustments.Height = 0.1875F;
			this.fldBudgetAdjustments.Left = 0.34375F;
			this.fldBudgetAdjustments.Name = "fldBudgetAdjustments";
			this.fldBudgetAdjustments.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldBudgetAdjustments.Text = null;
			this.fldBudgetAdjustments.Top = 0.84375F;
			this.fldBudgetAdjustments.Width = 1.0625F;
			// 
			// lblBudget
			// 
			this.lblBudget.Height = 0.1875F;
			this.lblBudget.HyperLink = null;
			this.lblBudget.Left = 1.4375F;
			this.lblBudget.Name = "lblBudget";
			this.lblBudget.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblBudget.Text = "= Budget";
			this.lblBudget.Top = 0.65625F;
			this.lblBudget.Width = 0.71875F;
			// 
			// lblBudAdj
			// 
			this.lblBudAdj.Height = 0.1875F;
			this.lblBudAdj.HyperLink = null;
			this.lblBudAdj.Left = 1.4375F;
			this.lblBudAdj.Name = "lblBudAdj";
			this.lblBudAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblBudAdj.Text = "= Bud Adj";
			this.lblBudAdj.Top = 0.84375F;
			this.lblBudAdj.Width = 0.71875F;
			// 
			// fldYTDExp
			// 
			this.fldYTDExp.Height = 0.1875F;
			this.fldYTDExp.Left = 2.71875F;
			this.fldYTDExp.Name = "fldYTDExp";
			this.fldYTDExp.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldYTDExp.Text = null;
			this.fldYTDExp.Top = 0.65625F;
			this.fldYTDExp.Width = 1.0625F;
			// 
			// fldYTDEnc
			// 
			this.fldYTDEnc.Height = 0.1875F;
			this.fldYTDEnc.Left = 2.71875F;
			this.fldYTDEnc.Name = "fldYTDEnc";
			this.fldYTDEnc.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldYTDEnc.Text = null;
			this.fldYTDEnc.Top = 0.84375F;
			this.fldYTDEnc.Width = 1.0625F;
			// 
			// lblYTDExp
			// 
			this.lblYTDExp.Height = 0.1875F;
			this.lblYTDExp.HyperLink = null;
			this.lblYTDExp.Left = 3.8125F;
			this.lblYTDExp.Name = "lblYTDExp";
			this.lblYTDExp.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblYTDExp.Text = "= YTD Exp";
			this.lblYTDExp.Top = 0.65625F;
			this.lblYTDExp.Width = 0.78125F;
			// 
			// lblYTDEnc
			// 
			this.lblYTDEnc.Height = 0.1875F;
			this.lblYTDEnc.HyperLink = null;
			this.lblYTDEnc.Left = 3.8125F;
			this.lblYTDEnc.Name = "lblYTDEnc";
			this.lblYTDEnc.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblYTDEnc.Text = "= YTD Enc";
			this.lblYTDEnc.Top = 0.84375F;
			this.lblYTDEnc.Width = 0.78125F;
			// 
			// fldBalance
			// 
			this.fldBalance.Height = 0.1875F;
			this.fldBalance.Left = 5.21875F;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldBalance.Text = null;
			this.fldBalance.Top = 0.65625F;
			this.fldBalance.Width = 1.0625F;
			// 
			// lblBalance
			// 
			this.lblBalance.Height = 0.1875F;
			this.lblBalance.HyperLink = null;
			this.lblBalance.Left = 6.3125F;
			this.lblBalance.Name = "lblBalance";
			this.lblBalance.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblBalance.Text = "= Balance";
			this.lblBalance.Top = 0.65625F;
			this.lblBalance.Width = 0.71875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label13.Text = "Per";
			this.Label13.Top = 1.15625F;
			this.Label13.Width = 0.28125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.34375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label14.Text = "Jrnl";
			this.Label14.Top = 1.15625F;
			this.Label14.Width = 0.40625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 1.3125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label15.Text = "Date";
			this.Label15.Top = 1.15625F;
			this.Label15.Width = 0.4375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.8125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "Vendor---------";
			this.Label16.Top = 1.15625F;
			this.Label16.Width = 1.375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.25F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label17.Text = "Description--------";
			this.Label17.Top = 1.15625F;
			this.Label17.Width = 1.65625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 4.9375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label18.Text = "RCB / Type";
			this.Label18.Top = 1.15625F;
			this.Label18.Width = 0.75F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.6875F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label19.Text = "Debits";
			this.Label19.Top = 1.15625F;
			this.Label19.Width = 1F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 6.78125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label20.Text = "Credits";
			this.Label20.Top = 1.15625F;
			this.Label20.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.34375F;
			this.Line1.Width = 7.8125F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.8125F;
			this.Line1.Y1 = 1.34375F;
			this.Line1.Y2 = 1.34375F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.78125F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label30.Text = "Check";
			this.Label30.Top = 1.15625F;
			this.Label30.Width = 0.46875F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.0625F;
			this.Binder.Left = 7.03125F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.5F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.28125F;
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.1875F;
			this.fldPeriod.Left = 0F;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldPeriod.Text = null;
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.28125F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 0.34375F;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldJournal.Text = null;
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.40625F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 1.28125F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.53125F;
			// 
			// fldVendor
			// 
			this.fldVendor.CanGrow = false;
			this.fldVendor.Height = 0.1875F;
			this.fldVendor.Left = 1.84375F;
			this.fldVendor.MultiLine = false;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldVendor.Text = null;
			this.fldVendor.Top = 0F;
			this.fldVendor.Width = 1.375F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 3.25F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.6875F;
			// 
			// fldRCB
			// 
			this.fldRCB.Height = 0.1875F;
			this.fldRCB.Left = 5F;
			this.fldRCB.Name = "fldRCB";
			this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldRCB.Text = null;
			this.fldRCB.Top = 0F;
			this.fldRCB.Width = 0.1875F;
			// 
			// fldDebits
			// 
			this.fldDebits.Height = 0.1875F;
			this.fldDebits.Left = 5.65625F;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDebits.Text = null;
			this.fldDebits.Top = 0F;
			this.fldDebits.Width = 1.03125F;
			// 
			// fldCredits
			// 
			this.fldCredits.Height = 0.1875F;
			this.fldCredits.Left = 6.75F;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCredits.Text = null;
			this.fldCredits.Top = 0F;
			this.fldCredits.Width = 1.03125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 5.3125F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 0.78125F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = null;
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.46875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.90625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.Label21.Text = "Totals--";
			this.Label21.Top = 0.0625F;
			this.Label21.Width = 0.5625F;
			// 
			// fldDebitTotal
			// 
			this.fldDebitTotal.Height = 0.1875F;
			this.fldDebitTotal.Left = 5.59375F;
			this.fldDebitTotal.Name = "fldDebitTotal";
			this.fldDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDebitTotal.Text = null;
			this.fldDebitTotal.Top = 0.0625F;
			this.fldDebitTotal.Width = 1.09375F;
			// 
			// fldCreditTotal
			// 
			this.fldCreditTotal.Height = 0.1875F;
			this.fldCreditTotal.Left = 6.75F;
			this.fldCreditTotal.Name = "fldCreditTotal";
			this.fldCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCreditTotal.Text = null;
			this.fldCreditTotal.Top = 0.0625F;
			this.fldCreditTotal.Width = 1.03125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.90625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.03125F;
			this.Line3.Width = 2.875F;
			this.Line3.X1 = 4.90625F;
			this.Line3.X2 = 7.78125F;
			this.Line3.Y1 = 0.03125F;
			this.Line3.Y2 = 0.03125F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.21875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 11pt; font-weight: bold; text-align: center";
			this.Label22.Text = "Monthly Summary";
			this.Label22.Top = 0.1875F;
			this.Label22.Width = 7.53125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 2.65625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label23.Text = "--Regular Entries--";
			this.Label23.Top = 0.5F;
			this.Label23.Width = 1.625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 4.625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label24.Text = "--Budget Entries--";
			this.Label24.Top = 0.5F;
			this.Label24.Width = 1.65625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 1.34375F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label25.Text = "Month";
			this.Label25.Top = 0.6875F;
			this.Label25.Width = 0.75F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 2.40625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label26.Text = "Debits";
			this.Label26.Top = 0.6875F;
			this.Label26.Width = 0.75F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 3.5F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label27.Text = "Credits";
			this.Label27.Top = 0.6875F;
			this.Label27.Width = 0.65625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 4.5F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label28.Text = "Debits";
			this.Label28.Top = 0.6875F;
			this.Label28.Width = 0.65625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.4375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label29.Text = "Credits";
			this.Label29.Top = 0.6875F;
			this.Label29.Width = 0.71875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.3125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.875F;
			this.Line2.Width = 4.84375F;
			this.Line2.X1 = 6.15625F;
			this.Line2.X2 = 1.3125F;
			this.Line2.Y1 = 0.875F;
			this.Line2.Y2 = 0.875F;
			// 
			// fldMonthJan
			// 
			this.fldMonthJan.CanShrink = true;
			this.fldMonthJan.Height = 0.1875F;
			this.fldMonthJan.Left = 1.34375F;
			this.fldMonthJan.MultiLine = false;
			this.fldMonthJan.Name = "fldMonthJan";
			this.fldMonthJan.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 0";
			this.fldMonthJan.Text = null;
			this.fldMonthJan.Top = 0.90625F;
			this.fldMonthJan.Visible = false;
			this.fldMonthJan.Width = 0.75F;
			// 
			// fldMonthFeb
			// 
			this.fldMonthFeb.CanShrink = true;
			this.fldMonthFeb.Height = 0.1875F;
			this.fldMonthFeb.Left = 1.34375F;
			this.fldMonthFeb.MultiLine = false;
			this.fldMonthFeb.Name = "fldMonthFeb";
			this.fldMonthFeb.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 0";
			this.fldMonthFeb.Text = null;
			this.fldMonthFeb.Top = 1.09375F;
			this.fldMonthFeb.Visible = false;
			this.fldMonthFeb.Width = 0.75F;
			// 
			// fldMonthMar
			// 
			this.fldMonthMar.CanShrink = true;
			this.fldMonthMar.Height = 0.1875F;
			this.fldMonthMar.Left = 1.34375F;
			this.fldMonthMar.MultiLine = false;
			this.fldMonthMar.Name = "fldMonthMar";
			this.fldMonthMar.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 0";
			this.fldMonthMar.Text = null;
			this.fldMonthMar.Top = 1.28125F;
			this.fldMonthMar.Visible = false;
			this.fldMonthMar.Width = 0.75F;
			// 
			// fldMonthApr
			// 
			this.fldMonthApr.CanShrink = true;
			this.fldMonthApr.Height = 0.1875F;
			this.fldMonthApr.Left = 1.34375F;
			this.fldMonthApr.MultiLine = false;
			this.fldMonthApr.Name = "fldMonthApr";
			this.fldMonthApr.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 0";
			this.fldMonthApr.Text = null;
			this.fldMonthApr.Top = 1.46875F;
			this.fldMonthApr.Visible = false;
			this.fldMonthApr.Width = 0.75F;
			// 
			// fldMonthMay
			// 
			this.fldMonthMay.CanShrink = true;
			this.fldMonthMay.Height = 0.1875F;
			this.fldMonthMay.Left = 1.34375F;
			this.fldMonthMay.Name = "fldMonthMay";
			this.fldMonthMay.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthMay.Text = null;
			this.fldMonthMay.Top = 1.65625F;
			this.fldMonthMay.Visible = false;
			this.fldMonthMay.Width = 0.75F;
			// 
			// fldMonthJune
			// 
			this.fldMonthJune.CanShrink = true;
			this.fldMonthJune.Height = 0.1875F;
			this.fldMonthJune.Left = 1.34375F;
			this.fldMonthJune.Name = "fldMonthJune";
			this.fldMonthJune.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthJune.Text = null;
			this.fldMonthJune.Top = 1.84375F;
			this.fldMonthJune.Visible = false;
			this.fldMonthJune.Width = 0.75F;
			// 
			// fldMonthJuly
			// 
			this.fldMonthJuly.CanShrink = true;
			this.fldMonthJuly.Height = 0.1875F;
			this.fldMonthJuly.Left = 1.34375F;
			this.fldMonthJuly.Name = "fldMonthJuly";
			this.fldMonthJuly.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthJuly.Text = null;
			this.fldMonthJuly.Top = 2.03125F;
			this.fldMonthJuly.Visible = false;
			this.fldMonthJuly.Width = 0.75F;
			// 
			// fldMonthAug
			// 
			this.fldMonthAug.CanShrink = true;
			this.fldMonthAug.Height = 0.1875F;
			this.fldMonthAug.Left = 1.34375F;
			this.fldMonthAug.Name = "fldMonthAug";
			this.fldMonthAug.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthAug.Text = null;
			this.fldMonthAug.Top = 2.21875F;
			this.fldMonthAug.Visible = false;
			this.fldMonthAug.Width = 0.75F;
			// 
			// fldMonthSept
			// 
			this.fldMonthSept.CanShrink = true;
			this.fldMonthSept.Height = 0.1875F;
			this.fldMonthSept.Left = 1.34375F;
			this.fldMonthSept.Name = "fldMonthSept";
			this.fldMonthSept.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthSept.Text = null;
			this.fldMonthSept.Top = 2.40625F;
			this.fldMonthSept.Visible = false;
			this.fldMonthSept.Width = 0.75F;
			// 
			// fldMonthOct
			// 
			this.fldMonthOct.CanShrink = true;
			this.fldMonthOct.Height = 0.1875F;
			this.fldMonthOct.Left = 1.34375F;
			this.fldMonthOct.Name = "fldMonthOct";
			this.fldMonthOct.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthOct.Text = null;
			this.fldMonthOct.Top = 2.59375F;
			this.fldMonthOct.Visible = false;
			this.fldMonthOct.Width = 0.75F;
			// 
			// fldMonthNov
			// 
			this.fldMonthNov.CanShrink = true;
			this.fldMonthNov.Height = 0.1875F;
			this.fldMonthNov.Left = 1.34375F;
			this.fldMonthNov.Name = "fldMonthNov";
			this.fldMonthNov.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthNov.Text = null;
			this.fldMonthNov.Top = 2.78125F;
			this.fldMonthNov.Visible = false;
			this.fldMonthNov.Width = 0.75F;
			// 
			// fldMonthDec
			// 
			this.fldMonthDec.CanShrink = true;
			this.fldMonthDec.Height = 0.1875F;
			this.fldMonthDec.Left = 1.34375F;
			this.fldMonthDec.Name = "fldMonthDec";
			this.fldMonthDec.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMonthDec.Text = null;
			this.fldMonthDec.Top = 2.96875F;
			this.fldMonthDec.Visible = false;
			this.fldMonthDec.Width = 0.75F;
			// 
			// fldTotals
			// 
			this.fldTotals.CanShrink = true;
			this.fldTotals.Height = 0.1875F;
			this.fldTotals.Left = 1.34375F;
			this.fldTotals.MultiLine = false;
			this.fldTotals.Name = "fldTotals";
			this.fldTotals.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 0";
			this.fldTotals.Text = "Totals";
			this.fldTotals.Top = 3.21875F;
			this.fldTotals.Width = 0.75F;
			// 
			// fldJanRegDebit
			// 
			this.fldJanRegDebit.CanShrink = true;
			this.fldJanRegDebit.Height = 0.1875F;
			this.fldJanRegDebit.Left = 2.1875F;
			this.fldJanRegDebit.Name = "fldJanRegDebit";
			this.fldJanRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJanRegDebit.Text = null;
			this.fldJanRegDebit.Top = 0.90625F;
			this.fldJanRegDebit.Visible = false;
			this.fldJanRegDebit.Width = 0.96875F;
			// 
			// fldJanRegCredit
			// 
			this.fldJanRegCredit.CanShrink = true;
			this.fldJanRegCredit.Height = 0.1875F;
			this.fldJanRegCredit.Left = 3.1875F;
			this.fldJanRegCredit.Name = "fldJanRegCredit";
			this.fldJanRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJanRegCredit.Text = null;
			this.fldJanRegCredit.Top = 0.90625F;
			this.fldJanRegCredit.Visible = false;
			this.fldJanRegCredit.Width = 0.96875F;
			// 
			// fldJanBudDebit
			// 
			this.fldJanBudDebit.CanShrink = true;
			this.fldJanBudDebit.Height = 0.1875F;
			this.fldJanBudDebit.Left = 4.1875F;
			this.fldJanBudDebit.Name = "fldJanBudDebit";
			this.fldJanBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJanBudDebit.Text = null;
			this.fldJanBudDebit.Top = 0.90625F;
			this.fldJanBudDebit.Visible = false;
			this.fldJanBudDebit.Width = 0.96875F;
			// 
			// fldJanBudCredit
			// 
			this.fldJanBudCredit.CanShrink = true;
			this.fldJanBudCredit.Height = 0.1875F;
			this.fldJanBudCredit.Left = 5.1875F;
			this.fldJanBudCredit.Name = "fldJanBudCredit";
			this.fldJanBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJanBudCredit.Text = null;
			this.fldJanBudCredit.Top = 0.90625F;
			this.fldJanBudCredit.Visible = false;
			this.fldJanBudCredit.Width = 0.96875F;
			// 
			// fldFebRegDebit
			// 
			this.fldFebRegDebit.CanShrink = true;
			this.fldFebRegDebit.Height = 0.1875F;
			this.fldFebRegDebit.Left = 2.1875F;
			this.fldFebRegDebit.Name = "fldFebRegDebit";
			this.fldFebRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldFebRegDebit.Text = null;
			this.fldFebRegDebit.Top = 1.09375F;
			this.fldFebRegDebit.Visible = false;
			this.fldFebRegDebit.Width = 0.96875F;
			// 
			// fldFebRegCredit
			// 
			this.fldFebRegCredit.CanShrink = true;
			this.fldFebRegCredit.Height = 0.1875F;
			this.fldFebRegCredit.Left = 3.1875F;
			this.fldFebRegCredit.Name = "fldFebRegCredit";
			this.fldFebRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldFebRegCredit.Text = null;
			this.fldFebRegCredit.Top = 1.09375F;
			this.fldFebRegCredit.Visible = false;
			this.fldFebRegCredit.Width = 0.96875F;
			// 
			// fldFebBudDebit
			// 
			this.fldFebBudDebit.CanShrink = true;
			this.fldFebBudDebit.Height = 0.1875F;
			this.fldFebBudDebit.Left = 4.1875F;
			this.fldFebBudDebit.Name = "fldFebBudDebit";
			this.fldFebBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldFebBudDebit.Text = null;
			this.fldFebBudDebit.Top = 1.09375F;
			this.fldFebBudDebit.Visible = false;
			this.fldFebBudDebit.Width = 0.96875F;
			// 
			// fldFebBudCredit
			// 
			this.fldFebBudCredit.CanShrink = true;
			this.fldFebBudCredit.Height = 0.1875F;
			this.fldFebBudCredit.Left = 5.1875F;
			this.fldFebBudCredit.Name = "fldFebBudCredit";
			this.fldFebBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldFebBudCredit.Text = null;
			this.fldFebBudCredit.Top = 1.09375F;
			this.fldFebBudCredit.Visible = false;
			this.fldFebBudCredit.Width = 0.96875F;
			// 
			// fldMarRegDebit
			// 
			this.fldMarRegDebit.CanShrink = true;
			this.fldMarRegDebit.Height = 0.1875F;
			this.fldMarRegDebit.Left = 2.1875F;
			this.fldMarRegDebit.Name = "fldMarRegDebit";
			this.fldMarRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMarRegDebit.Text = null;
			this.fldMarRegDebit.Top = 1.28125F;
			this.fldMarRegDebit.Visible = false;
			this.fldMarRegDebit.Width = 0.96875F;
			// 
			// fldMarRegCredit
			// 
			this.fldMarRegCredit.CanShrink = true;
			this.fldMarRegCredit.Height = 0.1875F;
			this.fldMarRegCredit.Left = 3.1875F;
			this.fldMarRegCredit.Name = "fldMarRegCredit";
			this.fldMarRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMarRegCredit.Text = null;
			this.fldMarRegCredit.Top = 1.28125F;
			this.fldMarRegCredit.Visible = false;
			this.fldMarRegCredit.Width = 0.96875F;
			// 
			// fldMarBudDebit
			// 
			this.fldMarBudDebit.CanShrink = true;
			this.fldMarBudDebit.Height = 0.1875F;
			this.fldMarBudDebit.Left = 4.1875F;
			this.fldMarBudDebit.Name = "fldMarBudDebit";
			this.fldMarBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMarBudDebit.Text = null;
			this.fldMarBudDebit.Top = 1.28125F;
			this.fldMarBudDebit.Visible = false;
			this.fldMarBudDebit.Width = 0.96875F;
			// 
			// fldMarBudCredit
			// 
			this.fldMarBudCredit.CanShrink = true;
			this.fldMarBudCredit.Height = 0.1875F;
			this.fldMarBudCredit.Left = 5.1875F;
			this.fldMarBudCredit.Name = "fldMarBudCredit";
			this.fldMarBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMarBudCredit.Text = null;
			this.fldMarBudCredit.Top = 1.28125F;
			this.fldMarBudCredit.Visible = false;
			this.fldMarBudCredit.Width = 0.96875F;
			// 
			// fldAprRegDebit
			// 
			this.fldAprRegDebit.CanShrink = true;
			this.fldAprRegDebit.Height = 0.1875F;
			this.fldAprRegDebit.Left = 2.1875F;
			this.fldAprRegDebit.Name = "fldAprRegDebit";
			this.fldAprRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAprRegDebit.Text = null;
			this.fldAprRegDebit.Top = 1.46875F;
			this.fldAprRegDebit.Visible = false;
			this.fldAprRegDebit.Width = 0.96875F;
			// 
			// fldAprRegCredit
			// 
			this.fldAprRegCredit.CanShrink = true;
			this.fldAprRegCredit.Height = 0.1875F;
			this.fldAprRegCredit.Left = 3.1875F;
			this.fldAprRegCredit.Name = "fldAprRegCredit";
			this.fldAprRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAprRegCredit.Text = null;
			this.fldAprRegCredit.Top = 1.46875F;
			this.fldAprRegCredit.Visible = false;
			this.fldAprRegCredit.Width = 0.96875F;
			// 
			// fldAprBudDebit
			// 
			this.fldAprBudDebit.CanShrink = true;
			this.fldAprBudDebit.Height = 0.1875F;
			this.fldAprBudDebit.Left = 4.1875F;
			this.fldAprBudDebit.Name = "fldAprBudDebit";
			this.fldAprBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAprBudDebit.Text = null;
			this.fldAprBudDebit.Top = 1.46875F;
			this.fldAprBudDebit.Visible = false;
			this.fldAprBudDebit.Width = 0.96875F;
			// 
			// fldAprBudCredit
			// 
			this.fldAprBudCredit.CanShrink = true;
			this.fldAprBudCredit.Height = 0.1875F;
			this.fldAprBudCredit.Left = 5.1875F;
			this.fldAprBudCredit.Name = "fldAprBudCredit";
			this.fldAprBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAprBudCredit.Text = null;
			this.fldAprBudCredit.Top = 1.46875F;
			this.fldAprBudCredit.Visible = false;
			this.fldAprBudCredit.Width = 0.96875F;
			// 
			// fldMayRegDebit
			// 
			this.fldMayRegDebit.CanShrink = true;
			this.fldMayRegDebit.Height = 0.1875F;
			this.fldMayRegDebit.Left = 2.1875F;
			this.fldMayRegDebit.Name = "fldMayRegDebit";
			this.fldMayRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMayRegDebit.Text = null;
			this.fldMayRegDebit.Top = 1.65625F;
			this.fldMayRegDebit.Visible = false;
			this.fldMayRegDebit.Width = 0.96875F;
			// 
			// fldMayRegCredit
			// 
			this.fldMayRegCredit.CanShrink = true;
			this.fldMayRegCredit.Height = 0.1875F;
			this.fldMayRegCredit.Left = 3.1875F;
			this.fldMayRegCredit.Name = "fldMayRegCredit";
			this.fldMayRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMayRegCredit.Text = null;
			this.fldMayRegCredit.Top = 1.65625F;
			this.fldMayRegCredit.Visible = false;
			this.fldMayRegCredit.Width = 0.96875F;
			// 
			// fldMayBudDebit
			// 
			this.fldMayBudDebit.CanShrink = true;
			this.fldMayBudDebit.Height = 0.1875F;
			this.fldMayBudDebit.Left = 4.1875F;
			this.fldMayBudDebit.Name = "fldMayBudDebit";
			this.fldMayBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMayBudDebit.Text = null;
			this.fldMayBudDebit.Top = 1.65625F;
			this.fldMayBudDebit.Visible = false;
			this.fldMayBudDebit.Width = 0.96875F;
			// 
			// fldMayBudCredit
			// 
			this.fldMayBudCredit.CanShrink = true;
			this.fldMayBudCredit.Height = 0.1875F;
			this.fldMayBudCredit.Left = 5.1875F;
			this.fldMayBudCredit.Name = "fldMayBudCredit";
			this.fldMayBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMayBudCredit.Text = null;
			this.fldMayBudCredit.Top = 1.65625F;
			this.fldMayBudCredit.Visible = false;
			this.fldMayBudCredit.Width = 0.96875F;
			// 
			// fldJuneRegDebit
			// 
			this.fldJuneRegDebit.CanShrink = true;
			this.fldJuneRegDebit.Height = 0.1875F;
			this.fldJuneRegDebit.Left = 2.1875F;
			this.fldJuneRegDebit.Name = "fldJuneRegDebit";
			this.fldJuneRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJuneRegDebit.Text = null;
			this.fldJuneRegDebit.Top = 1.84375F;
			this.fldJuneRegDebit.Visible = false;
			this.fldJuneRegDebit.Width = 0.96875F;
			// 
			// fldJuneRegCredit
			// 
			this.fldJuneRegCredit.CanShrink = true;
			this.fldJuneRegCredit.Height = 0.1875F;
			this.fldJuneRegCredit.Left = 3.1875F;
			this.fldJuneRegCredit.Name = "fldJuneRegCredit";
			this.fldJuneRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJuneRegCredit.Text = null;
			this.fldJuneRegCredit.Top = 1.84375F;
			this.fldJuneRegCredit.Visible = false;
			this.fldJuneRegCredit.Width = 0.96875F;
			// 
			// fldJuneBudDebit
			// 
			this.fldJuneBudDebit.CanShrink = true;
			this.fldJuneBudDebit.Height = 0.1875F;
			this.fldJuneBudDebit.Left = 4.1875F;
			this.fldJuneBudDebit.Name = "fldJuneBudDebit";
			this.fldJuneBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJuneBudDebit.Text = null;
			this.fldJuneBudDebit.Top = 1.84375F;
			this.fldJuneBudDebit.Visible = false;
			this.fldJuneBudDebit.Width = 0.96875F;
			// 
			// fldJuneBudCredit
			// 
			this.fldJuneBudCredit.CanShrink = true;
			this.fldJuneBudCredit.Height = 0.1875F;
			this.fldJuneBudCredit.Left = 5.1875F;
			this.fldJuneBudCredit.Name = "fldJuneBudCredit";
			this.fldJuneBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJuneBudCredit.Text = null;
			this.fldJuneBudCredit.Top = 1.84375F;
			this.fldJuneBudCredit.Visible = false;
			this.fldJuneBudCredit.Width = 0.96875F;
			// 
			// fldJulyRegDebit
			// 
			this.fldJulyRegDebit.CanShrink = true;
			this.fldJulyRegDebit.Height = 0.1875F;
			this.fldJulyRegDebit.Left = 2.1875F;
			this.fldJulyRegDebit.Name = "fldJulyRegDebit";
			this.fldJulyRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJulyRegDebit.Text = null;
			this.fldJulyRegDebit.Top = 2.03125F;
			this.fldJulyRegDebit.Visible = false;
			this.fldJulyRegDebit.Width = 0.96875F;
			// 
			// fldJulyRegCredit
			// 
			this.fldJulyRegCredit.CanShrink = true;
			this.fldJulyRegCredit.Height = 0.1875F;
			this.fldJulyRegCredit.Left = 3.1875F;
			this.fldJulyRegCredit.Name = "fldJulyRegCredit";
			this.fldJulyRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJulyRegCredit.Text = null;
			this.fldJulyRegCredit.Top = 2.03125F;
			this.fldJulyRegCredit.Visible = false;
			this.fldJulyRegCredit.Width = 0.96875F;
			// 
			// fldJulyBudDebit
			// 
			this.fldJulyBudDebit.CanShrink = true;
			this.fldJulyBudDebit.Height = 0.1875F;
			this.fldJulyBudDebit.Left = 4.1875F;
			this.fldJulyBudDebit.Name = "fldJulyBudDebit";
			this.fldJulyBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJulyBudDebit.Text = null;
			this.fldJulyBudDebit.Top = 2.03125F;
			this.fldJulyBudDebit.Visible = false;
			this.fldJulyBudDebit.Width = 0.96875F;
			// 
			// fldJulyBudCredit
			// 
			this.fldJulyBudCredit.CanShrink = true;
			this.fldJulyBudCredit.Height = 0.1875F;
			this.fldJulyBudCredit.Left = 5.1875F;
			this.fldJulyBudCredit.Name = "fldJulyBudCredit";
			this.fldJulyBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldJulyBudCredit.Text = null;
			this.fldJulyBudCredit.Top = 2.03125F;
			this.fldJulyBudCredit.Visible = false;
			this.fldJulyBudCredit.Width = 0.96875F;
			// 
			// fldAugRegDebit
			// 
			this.fldAugRegDebit.CanShrink = true;
			this.fldAugRegDebit.Height = 0.1875F;
			this.fldAugRegDebit.Left = 2.1875F;
			this.fldAugRegDebit.Name = "fldAugRegDebit";
			this.fldAugRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAugRegDebit.Text = null;
			this.fldAugRegDebit.Top = 2.21875F;
			this.fldAugRegDebit.Visible = false;
			this.fldAugRegDebit.Width = 0.96875F;
			// 
			// fldAugRegCredit
			// 
			this.fldAugRegCredit.CanShrink = true;
			this.fldAugRegCredit.Height = 0.1875F;
			this.fldAugRegCredit.Left = 3.1875F;
			this.fldAugRegCredit.Name = "fldAugRegCredit";
			this.fldAugRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAugRegCredit.Text = null;
			this.fldAugRegCredit.Top = 2.21875F;
			this.fldAugRegCredit.Visible = false;
			this.fldAugRegCredit.Width = 0.96875F;
			// 
			// fldAugBudDebit
			// 
			this.fldAugBudDebit.CanShrink = true;
			this.fldAugBudDebit.Height = 0.1875F;
			this.fldAugBudDebit.Left = 4.1875F;
			this.fldAugBudDebit.Name = "fldAugBudDebit";
			this.fldAugBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAugBudDebit.Text = null;
			this.fldAugBudDebit.Top = 2.21875F;
			this.fldAugBudDebit.Visible = false;
			this.fldAugBudDebit.Width = 0.96875F;
			// 
			// fldAugBudCredit
			// 
			this.fldAugBudCredit.CanShrink = true;
			this.fldAugBudCredit.Height = 0.1875F;
			this.fldAugBudCredit.Left = 5.1875F;
			this.fldAugBudCredit.Name = "fldAugBudCredit";
			this.fldAugBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAugBudCredit.Text = null;
			this.fldAugBudCredit.Top = 2.21875F;
			this.fldAugBudCredit.Visible = false;
			this.fldAugBudCredit.Width = 0.96875F;
			// 
			// fldSeptRegDebit
			// 
			this.fldSeptRegDebit.CanShrink = true;
			this.fldSeptRegDebit.Height = 0.1875F;
			this.fldSeptRegDebit.Left = 2.1875F;
			this.fldSeptRegDebit.Name = "fldSeptRegDebit";
			this.fldSeptRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSeptRegDebit.Text = null;
			this.fldSeptRegDebit.Top = 2.40625F;
			this.fldSeptRegDebit.Visible = false;
			this.fldSeptRegDebit.Width = 0.96875F;
			// 
			// fldSeptRegCredit
			// 
			this.fldSeptRegCredit.CanShrink = true;
			this.fldSeptRegCredit.Height = 0.1875F;
			this.fldSeptRegCredit.Left = 3.1875F;
			this.fldSeptRegCredit.Name = "fldSeptRegCredit";
			this.fldSeptRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSeptRegCredit.Text = null;
			this.fldSeptRegCredit.Top = 2.40625F;
			this.fldSeptRegCredit.Visible = false;
			this.fldSeptRegCredit.Width = 0.96875F;
			// 
			// fldSeptBudDebit
			// 
			this.fldSeptBudDebit.CanShrink = true;
			this.fldSeptBudDebit.Height = 0.1875F;
			this.fldSeptBudDebit.Left = 4.1875F;
			this.fldSeptBudDebit.Name = "fldSeptBudDebit";
			this.fldSeptBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSeptBudDebit.Text = null;
			this.fldSeptBudDebit.Top = 2.40625F;
			this.fldSeptBudDebit.Visible = false;
			this.fldSeptBudDebit.Width = 0.96875F;
			// 
			// fldSeptBudCredit
			// 
			this.fldSeptBudCredit.CanShrink = true;
			this.fldSeptBudCredit.Height = 0.1875F;
			this.fldSeptBudCredit.Left = 5.1875F;
			this.fldSeptBudCredit.Name = "fldSeptBudCredit";
			this.fldSeptBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSeptBudCredit.Text = null;
			this.fldSeptBudCredit.Top = 2.40625F;
			this.fldSeptBudCredit.Visible = false;
			this.fldSeptBudCredit.Width = 0.96875F;
			// 
			// fldOctRegDebit
			// 
			this.fldOctRegDebit.CanShrink = true;
			this.fldOctRegDebit.Height = 0.1875F;
			this.fldOctRegDebit.Left = 2.1875F;
			this.fldOctRegDebit.Name = "fldOctRegDebit";
			this.fldOctRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldOctRegDebit.Text = null;
			this.fldOctRegDebit.Top = 2.59375F;
			this.fldOctRegDebit.Visible = false;
			this.fldOctRegDebit.Width = 0.96875F;
			// 
			// fldOctRegCredit
			// 
			this.fldOctRegCredit.CanShrink = true;
			this.fldOctRegCredit.Height = 0.1875F;
			this.fldOctRegCredit.Left = 3.1875F;
			this.fldOctRegCredit.Name = "fldOctRegCredit";
			this.fldOctRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldOctRegCredit.Text = null;
			this.fldOctRegCredit.Top = 2.59375F;
			this.fldOctRegCredit.Visible = false;
			this.fldOctRegCredit.Width = 0.96875F;
			// 
			// fldOctBudDebit
			// 
			this.fldOctBudDebit.CanShrink = true;
			this.fldOctBudDebit.Height = 0.1875F;
			this.fldOctBudDebit.Left = 4.1875F;
			this.fldOctBudDebit.Name = "fldOctBudDebit";
			this.fldOctBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldOctBudDebit.Text = null;
			this.fldOctBudDebit.Top = 2.59375F;
			this.fldOctBudDebit.Visible = false;
			this.fldOctBudDebit.Width = 0.96875F;
			// 
			// fldOctBudCredit
			// 
			this.fldOctBudCredit.CanShrink = true;
			this.fldOctBudCredit.Height = 0.1875F;
			this.fldOctBudCredit.Left = 5.1875F;
			this.fldOctBudCredit.Name = "fldOctBudCredit";
			this.fldOctBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldOctBudCredit.Text = null;
			this.fldOctBudCredit.Top = 2.59375F;
			this.fldOctBudCredit.Visible = false;
			this.fldOctBudCredit.Width = 0.96875F;
			// 
			// fldNovRegDebit
			// 
			this.fldNovRegDebit.CanShrink = true;
			this.fldNovRegDebit.Height = 0.1875F;
			this.fldNovRegDebit.Left = 2.1875F;
			this.fldNovRegDebit.Name = "fldNovRegDebit";
			this.fldNovRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldNovRegDebit.Text = null;
			this.fldNovRegDebit.Top = 2.78125F;
			this.fldNovRegDebit.Visible = false;
			this.fldNovRegDebit.Width = 0.96875F;
			// 
			// fldNovRegCredit
			// 
			this.fldNovRegCredit.CanShrink = true;
			this.fldNovRegCredit.Height = 0.1875F;
			this.fldNovRegCredit.Left = 3.1875F;
			this.fldNovRegCredit.Name = "fldNovRegCredit";
			this.fldNovRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldNovRegCredit.Text = null;
			this.fldNovRegCredit.Top = 2.78125F;
			this.fldNovRegCredit.Visible = false;
			this.fldNovRegCredit.Width = 0.96875F;
			// 
			// fldNovBudDebit
			// 
			this.fldNovBudDebit.CanShrink = true;
			this.fldNovBudDebit.Height = 0.1875F;
			this.fldNovBudDebit.Left = 4.1875F;
			this.fldNovBudDebit.Name = "fldNovBudDebit";
			this.fldNovBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldNovBudDebit.Text = null;
			this.fldNovBudDebit.Top = 2.78125F;
			this.fldNovBudDebit.Visible = false;
			this.fldNovBudDebit.Width = 0.96875F;
			// 
			// fldNovBudCredit
			// 
			this.fldNovBudCredit.CanShrink = true;
			this.fldNovBudCredit.Height = 0.1875F;
			this.fldNovBudCredit.Left = 5.1875F;
			this.fldNovBudCredit.Name = "fldNovBudCredit";
			this.fldNovBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldNovBudCredit.Text = null;
			this.fldNovBudCredit.Top = 2.78125F;
			this.fldNovBudCredit.Visible = false;
			this.fldNovBudCredit.Width = 0.96875F;
			// 
			// fldDecRegDebit
			// 
			this.fldDecRegDebit.CanShrink = true;
			this.fldDecRegDebit.Height = 0.1875F;
			this.fldDecRegDebit.Left = 2.1875F;
			this.fldDecRegDebit.Name = "fldDecRegDebit";
			this.fldDecRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldDecRegDebit.Text = null;
			this.fldDecRegDebit.Top = 2.96875F;
			this.fldDecRegDebit.Visible = false;
			this.fldDecRegDebit.Width = 0.96875F;
			// 
			// fldDecRegCredit
			// 
			this.fldDecRegCredit.CanShrink = true;
			this.fldDecRegCredit.Height = 0.1875F;
			this.fldDecRegCredit.Left = 3.1875F;
			this.fldDecRegCredit.Name = "fldDecRegCredit";
			this.fldDecRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldDecRegCredit.Text = null;
			this.fldDecRegCredit.Top = 2.96875F;
			this.fldDecRegCredit.Visible = false;
			this.fldDecRegCredit.Width = 0.96875F;
			// 
			// fldDecBudDebit
			// 
			this.fldDecBudDebit.CanShrink = true;
			this.fldDecBudDebit.Height = 0.1875F;
			this.fldDecBudDebit.Left = 4.1875F;
			this.fldDecBudDebit.Name = "fldDecBudDebit";
			this.fldDecBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldDecBudDebit.Text = null;
			this.fldDecBudDebit.Top = 2.96875F;
			this.fldDecBudDebit.Visible = false;
			this.fldDecBudDebit.Width = 0.96875F;
			// 
			// fldDecBudCredit
			// 
			this.fldDecBudCredit.CanShrink = true;
			this.fldDecBudCredit.Height = 0.1875F;
			this.fldDecBudCredit.Left = 5.1875F;
			this.fldDecBudCredit.Name = "fldDecBudCredit";
			this.fldDecBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldDecBudCredit.Text = null;
			this.fldDecBudCredit.Top = 2.96875F;
			this.fldDecBudCredit.Visible = false;
			this.fldDecBudCredit.Width = 0.96875F;
			// 
			// fldTotalRegDebit
			// 
			this.fldTotalRegDebit.CanShrink = true;
			this.fldTotalRegDebit.Height = 0.1875F;
			this.fldTotalRegDebit.Left = 2.1875F;
			this.fldTotalRegDebit.MultiLine = false;
			this.fldTotalRegDebit.Name = "fldTotalRegDebit";
			this.fldTotalRegDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap; ddo-char-set: 0";
			this.fldTotalRegDebit.Text = null;
			this.fldTotalRegDebit.Top = 3.21875F;
			this.fldTotalRegDebit.Width = 0.96875F;
			// 
			// fldTotalRegCredit
			// 
			this.fldTotalRegCredit.CanShrink = true;
			this.fldTotalRegCredit.Height = 0.1875F;
			this.fldTotalRegCredit.Left = 3.1875F;
			this.fldTotalRegCredit.MultiLine = false;
			this.fldTotalRegCredit.Name = "fldTotalRegCredit";
			this.fldTotalRegCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap; ddo-char-set: 0";
			this.fldTotalRegCredit.Text = null;
			this.fldTotalRegCredit.Top = 3.21875F;
			this.fldTotalRegCredit.Width = 0.96875F;
			// 
			// fldTotalBudDebit
			// 
			this.fldTotalBudDebit.CanShrink = true;
			this.fldTotalBudDebit.Height = 0.1875F;
			this.fldTotalBudDebit.Left = 4.1875F;
			this.fldTotalBudDebit.MultiLine = false;
			this.fldTotalBudDebit.Name = "fldTotalBudDebit";
			this.fldTotalBudDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap; ddo-char-set: 0";
			this.fldTotalBudDebit.Text = null;
			this.fldTotalBudDebit.Top = 3.21875F;
			this.fldTotalBudDebit.Width = 0.96875F;
			// 
			// fldTotalBudCredit
			// 
			this.fldTotalBudCredit.CanShrink = true;
			this.fldTotalBudCredit.Height = 0.1875F;
			this.fldTotalBudCredit.Left = 5.1875F;
			this.fldTotalBudCredit.MultiLine = false;
			this.fldTotalBudCredit.Name = "fldTotalBudCredit";
			this.fldTotalBudCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap; ddo-char-set: 0";
			this.fldTotalBudCredit.Text = null;
			this.fldTotalBudCredit.Top = 3.21875F;
			this.fldTotalBudCredit.Width = 0.96875F;
			// 
			// srptBDAccountStatusDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.822917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Tahoma\'; font-style: inherit; font-variant: inherit; font-weight: b" + "old; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudgetAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBudAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDExp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDEnc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYTDExp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYTDEnc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthJan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthFeb)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthMar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthApr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthMay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthJune)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthJuly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthAug)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthSept)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthOct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthNov)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthDec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJanBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFebBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMarBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAprBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMayBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJuneBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJulyBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAugBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeptBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOctBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNovBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDecBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalBudDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalBudCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBudgetAdjustments;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBudAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDExp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDEnc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYTDExp;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYTDEnc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthJan;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthFeb;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthMar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthApr;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthMay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthJune;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthJuly;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthAug;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthSept;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthOct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthNov;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthDec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJanRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJanRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJanBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJanBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFebRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFebRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFebBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFebBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMarRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMarRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMarBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMarBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAprRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAprRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAprBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAprBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMayRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMayRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMayBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMayBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJuneRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJuneRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJuneBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJuneBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJulyRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJulyRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJulyBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJulyBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAugRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAugRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAugBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAugBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeptRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeptRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeptBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeptBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOctRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOctRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOctBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOctBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNovRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNovRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNovBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNovBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDecRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDecRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDecBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDecBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalBudDebit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalBudCredit;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
	}
}
