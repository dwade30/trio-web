﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmArchiveViewSelection.
	/// </summary>
	public partial class frmArchiveViewSelection : BaseForm
	{
		public frmArchiveViewSelection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: add code from Load
			// define column variables
			RecordCol = 0;
			BankCol = 1;
			TypeCol = 2;
			CheckCol = 3;
			AmountCol = 4;
			CodeCol = 5;
			StatusDateCol = 6;
			PayeeCol = 7;
			CheckDateCol = 8;
			// set alignment of grid
			//FC:FINAL:CHN: Set correct cell alignment.
			// vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(BankCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// set mergerows for title
			vs1.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			vs1.MergeRow(0, true);
			// show titles for columns
			//FC:FINAL:MSH - Issue #723: set headers to appropriate cells
			//vs1.TextMatrix(0, CodeCol, "--Status--");
			//vs1.TextMatrix(0, StatusDateCol, "--Status--");
			//vs1.TextMatrix(1, BankCol, "Bank");
			//vs1.TextMatrix(1, TypeCol, "Type");
			//vs1.TextMatrix(1, CheckCol, "Check");
			//vs1.TextMatrix(1, AmountCol, "Amount");
			//vs1.TextMatrix(1, CodeCol, "Code");
			//vs1.TextMatrix(1, StatusDateCol, "Date");
			//vs1.TextMatrix(1, PayeeCol, "Payee");
			//vs1.TextMatrix(1, CheckDateCol, "Date");
			vs1.TextMatrix(0, BankCol, "Bank");
			vs1.TextMatrix(0, TypeCol, "Type");
			vs1.TextMatrix(0, CheckCol, "Check");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, CodeCol, "Code");
			vs1.TextMatrix(0, StatusDateCol, "Date");
			vs1.TextMatrix(0, PayeeCol, "Payee");
			vs1.TextMatrix(0, CheckDateCol, "Date");
			//FC:FINAL:DDU:#3016 - aligned columns
			vs1.ColAlignment(BankCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CheckCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(StatusDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(PayeeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CheckDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set format type for columns
			vs1.ColFormat(AmountCol, "#,###.00");
			//FC:FINAL:MSH - Issue #716: setting the short date format for the date columns 
			vs1.ColFormat(StatusDateCol, "MM/dd/yyyy");
			vs1.ColFormat(CheckDateCol, "MM/dd/yyyy");
			//FC:FINAL:CHN - issue #1608: Set column data type to fix ordering.
			vs1.ColDataType(AmountCol, FCGrid.DataTypeSettings.flexDTDouble);
			// set up column sizes
			vs1.ColWidth(RecordCol, 0);
			vs1.ColWidth(BankCol, 500);
			vs1.ColWidth(TypeCol, 500);
			vs1.ColWidth(CheckCol, 800);
			vs1.ColWidth(AmountCol, 1200);
			vs1.ColWidth(CodeCol, 600);
			vs1.ColWidth(StatusDateCol, 800);
			vs1.ColWidth(PayeeCol, 3400);
			vs1.ColWidth(CheckDateCol, 800);
			dblColPercents[0] = 0;
			dblColPercents[1] = 0.0556179;
			dblColPercents[2] = 0.0556179;
			dblColPercents[3] = 0.0893258;
			dblColPercents[4] = 0.1348314;
			dblColPercents[5] = 0.0674157;
			dblColPercents[6] = 0.0893258;
			dblColPercents[7] = 0.3825842;
			dblColPercents[8] = 0.0893258;
			// show a border between the titles and the data input section of the grid
			vs1.Select(1, 0, 1, vs1.Cols - 1);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			lblBank.Text = App.MainForm.StatusBarText3;
			txtStartDate.Text = "01/01/1900";
			txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//FC:FINAL:ASZ: default selection - All
			cmbTypeAll.SelectedIndex = 0;
			cmbStatusAll.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmArchiveViewSelection InstancePtr
		{
			get
			{
				return (frmArchiveViewSelection)Sys.GetInstance(typeof(frmArchiveViewSelection));
			}
		}

		protected frmArchiveViewSelection _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/13/02
		// This form will be used to select and view different check
		// records that have been saved to the archive file.  This form
		// will also allow them to print the information they are viewing
		// ********************************************************
		public string strTemp = string.Empty;
		public string strCheckSQL = string.Empty;
		public string strOtherSQL = string.Empty;
		public string strTitle = string.Empty;
		clsDRWrapper rsInfo = new clsDRWrapper();
		int RecordCol;
		int BankCol;
		int TypeCol;
		int CheckCol;
		int AmountCol;
		int CodeCol;
		int StatusDateCol;
		int PayeeCol;
		int CheckDateCol;
		double[] dblColPercents = new double[9 + 1];
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16
		DateTime datReturnedDate;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			//if (vs1.Visible == false)
			//{
			//    Close();
			//}
			//else
			//{
			fraDateRange.Visible = true;
			fraStatusType.Visible = true;
			fraAccountType.Visible = true;
			txtLowCheck.Visible = true;
			Label1.Visible = true;
			lblBank.Visible = false;
			vs1.Visible = false;
			cmdCancel.Visible = false;
			//FC:FINAL:RPU: Change the width of button according to its new Text
			cmdPrint.Width = 100;
			cmdPrint.Text = "Display";
			//}
		}

		private void cmdDisplay_Click(object sender, System.EventArgs e)
		{
			int counter;
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue.", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue.", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your start date before you may continue.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your end date before you may continue.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must not be later than the end date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			CreateSQL();
			CreateTitle();
			//FC:FINAL:MSH - Issue #723: filling must starts from 1 row
			counter = 1;
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				vs1.Rows = counter + rsInfo.RecordCount();
				do
				{
					vs1.TextMatrix(counter, RecordCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, BankCol, FCConvert.ToString(rsInfo.Get_Fields("BankNumber")));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, TypeCol, FCConvert.ToString(rsInfo.Get_Fields("Type")));
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, CheckCol, FCConvert.ToString(rsInfo.Get_Fields("CheckNumber")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(rsInfo.Get_Fields("Amount")));
					vs1.TextMatrix(counter, CodeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Status")));
					vs1.TextMatrix(counter, StatusDateCol, FCConvert.ToString(rsInfo.Get_Fields_DateTime("StatusDate")));
					vs1.TextMatrix(counter, PayeeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
					vs1.TextMatrix(counter, CheckDateCol, FCConvert.ToString(rsInfo.Get_Fields_DateTime("CheckDate")));
					counter += 1;
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			else
			{
				vs1.Rows = counter;
			}
			fraStatusType.Visible = false;
			fraAccountType.Visible = false;
			txtLowCheck.Visible = false;
			Label1.Visible = false;
			fraDateRange.Visible = false;
			lblBank.Visible = true;
			vs1.Visible = true;
			vs1.Focus();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue.", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue.", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your start date before you may continue.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your end date before you may continue.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must not be later than the end date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			CreateSQL();
			CreateTitle();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCheckArchive.InstancePtr);
		}

		private void cmdRequestDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void frmArchiveViewSelection_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmArchiveViewSelection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmArchiveViewSelection.FillStyle	= 0;
			//frmArchiveViewSelection.ScaleWidth	= 9480;
			//frmArchiveViewSelection.ScaleHeight	= 7230;
			//frmArchiveViewSelection.LinkTopic	= "Form2";
			//frmArchiveViewSelection.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//FC:FINAL:ASZ: moved code to Constructor
			//// define column variables
			//RecordCol = 0;
			//BankCol = 1;
			//TypeCol = 2;
			//CheckCol = 3;
			//AmountCol = 4;
			//CodeCol = 5;
			//StatusDateCol = 6;
			//PayeeCol = 7;
			//CheckDateCol = 8;
			//// set alignment of grid
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(BankCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//// set mergerows for title
			//vs1.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			//vs1.MergeRow(0, true);
			//// show titles for columns
			//vs1.TextMatrix(0, CodeCol, "--Status--");
			//vs1.TextMatrix(0, StatusDateCol, "--Status--");
			//vs1.TextMatrix(1, BankCol, "Bank");
			//vs1.TextMatrix(1, TypeCol, "Type");
			//vs1.TextMatrix(1, CheckCol, "Check");
			//vs1.TextMatrix(1, AmountCol, "Amount");
			//vs1.TextMatrix(1, CodeCol, "Code");
			//vs1.TextMatrix(1, StatusDateCol, "Date");
			//vs1.TextMatrix(1, PayeeCol, "Payee");
			//vs1.TextMatrix(1, CheckDateCol, "Date");
			//// set format type for columns
			//vs1.ColFormat(AmountCol, "#,###.00");
			//// set up column sizes
			//vs1.ColWidth(RecordCol, 0);
			//vs1.ColWidth(BankCol, 500);
			//vs1.ColWidth(TypeCol, 500);
			//vs1.ColWidth(CheckCol, 800);
			//vs1.ColWidth(AmountCol, 1200);
			//vs1.ColWidth(CodeCol, 600);
			//vs1.ColWidth(StatusDateCol, 800);
			//vs1.ColWidth(PayeeCol, 3400);
			//vs1.ColWidth(CheckDateCol, 800);
			//dblColPercents[0] = 0;
			//dblColPercents[1] = 0.0556179;
			//dblColPercents[2] = 0.0556179;
			//dblColPercents[3] = 0.0893258;
			//dblColPercents[4] = 0.1348314;
			//dblColPercents[5] = 0.0674157;
			//dblColPercents[6] = 0.0893258;
			//dblColPercents[7] = 0.3825842;
			//dblColPercents[8] = 0.0893258;
			//// show a border between the titles and the data input section of the grid
			//vs1.Select(1, 0, 1, vs1.Cols - 1);
			//vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			//lblBank.Text = App.MainForm.StatusBarText2;
			//txtStartDate.Text = "01/01/1900";
			//txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			//modGlobalFunctions.SetTRIOColors(this);
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void frmArchiveViewSelection_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 8; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * dblColPercents[counter]));
			}
			//FC:FINAL:ASZ - use anchors: vs1.Height = vs1.RowHeight(0) * 17 + 75;
		}

		private void frmArchiveViewSelection_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue.", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue.", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your start date before you may continue.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your end date before you may continue.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must not be later than the end date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			CreateSQL();
			CreateTitle();
			modDuplexPrinting.DuplexPrintReport(rptCheckArchive.InstancePtr);
		}

		private void mnuPreview_Click(object sender, System.EventArgs e)
		{
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue.", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue.", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your start date before you may continue.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date for your end date before you may continue.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must not be later than the end date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			CreateSQL();
			CreateTitle();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCheckArchive.InstancePtr);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void CreateSQL()
		{
			strTemp = "SELECT * FROM CheckRecArchive WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND CheckNumber >= " + txtLowCheck.Text + " AND (CheckDate >= '" + txtStartDate.Text + "' AND CheckDate <= '" + txtEndDate.Text + "') AND (";
			strCheckSQL = "SELECT * FROM CheckRecArchive WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND CheckNumber >= " + txtLowCheck.Text + " AND (CheckDate >= '" + txtStartDate.Text + "' AND CheckDate <= '" + txtEndDate.Text + "') AND (";
			strOtherSQL = "SELECT * FROM CheckRecArchive WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND CheckNumber >= " + txtLowCheck.Text + " AND (CheckDate >= '" + txtStartDate.Text + "' AND CheckDate <= '" + txtEndDate.Text + "') AND (";
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '1' OR ";
					strCheckSQL += "Type = '1' OR ";
				}
				if (chkPY.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '2' OR ";
					strCheckSQL += "Type = '2' OR ";
				}
				if (chkDP.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '3' OR ";
					strOtherSQL += "Type = '3' OR ";
				}
				if (chkRT.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '4' OR ";
					strOtherSQL += "Type = '4' OR ";
				}
				if (chkIN.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '5' OR ";
					strOtherSQL += "Type = '5' OR ";
				}
				if (chkOC.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '6' OR ";
					strOtherSQL += "Type = '6' OR ";
				}
				if (chkOD.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '7' OR ";
					strOtherSQL += "Type = '7' OR ";
				}
				if (Strings.Right(strTemp, 1) == "(")
				{
					strTemp = Strings.Left(strTemp, strTemp.Length - 6) + " AND (";
				}
				else
				{
					strTemp = Strings.Left(strTemp, strTemp.Length - 4) + ") AND (";
				}
				if (Strings.Right(strCheckSQL, 1) == "(")
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 6) + " AND (";
				}
				else
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 4) + ") AND (";
				}
				if (Strings.Right(strOtherSQL, 1) == "(")
				{
					strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 6) + " AND (";
				}
				else
				{
					strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 4) + ") AND (";
				}
			}
			else
			{
				strTemp = Strings.Left(strTemp, strTemp.Length - 6) + " AND (";
				strCheckSQL += "Type = '1' OR Type = '2') AND (";
				strOtherSQL += "Type = '3' OR Type = '4' OR Type = '5' OR Type = '6' OR Type = '7') AND (";
			}
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkCashed.CheckState == CheckState.Checked)
				{
					strTemp += "Status = '3' OR ";
					strCheckSQL += "Status = '3' OR ";
					strOtherSQL += "Status = '3' OR ";
				}
				if (chkVoided.CheckState == CheckState.Checked)
				{
					strTemp += "Status = 'V' OR ";
					strCheckSQL += "Status = 'V' OR ";
					strOtherSQL += "Status = 'V' OR ";
				}
				if (chkDeleted.CheckState == CheckState.Checked)
				{
					strTemp += "Status = 'D' OR ";
					strCheckSQL += "Status = 'D' OR ";
					strOtherSQL += "Status = 'D' OR ";
				}
				if (Strings.Right(strTemp, 1) == "(")
				{
					strTemp = Strings.Left(strTemp, strTemp.Length - 6);
				}
				else
				{
					strTemp = Strings.Left(strTemp, strTemp.Length - 4) + ")";
				}
				if (Strings.Right(strCheckSQL, 1) == "(")
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 6);
				}
				else
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 4) + ")";
				}
				if (Strings.Right(strOtherSQL, 1) == "(")
				{
					strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 6);
				}
				else
				{
					strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 4) + ")";
				}
			}
			else
			{
				strTemp = Strings.Left(strTemp, strTemp.Length - 6);
				strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 6);
				strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 6);
			}
			rsInfo.OpenRecordset(strTemp + " ORDER BY CheckNumber");
		}

		private void CreateTitle()
		{
			strTitle = "";
			if (cmbStatusAll.Text == "All")
			{
				strTitle = "Cashed, Voided, Deleted, ";
			}
			else
			{
				if (chkCashed.CheckState == CheckState.Checked)
				{
					strTitle += "Cashed, ";
				}
				if (chkVoided.CheckState == CheckState.Checked)
				{
					strTitle += "Voided, ";
				}
				if (chkDeleted.CheckState == CheckState.Checked)
				{
					strTitle += "Deleted, ";
				}
			}
			if (cmbTypeAll.Text == "All")
			{
				strTitle += "AP Checks, PY Checks, Deposits, Returned Checks, Interest, Other Credits, Other Debits";
			}
			else
			{
				if (chkAP.CheckState == CheckState.Checked)
				{
					strTitle += "AP Checks, ";
				}
				if (chkPY.CheckState == CheckState.Checked)
				{
					strTitle += "PY Checks, ";
				}
				if (chkDP.CheckState == CheckState.Checked)
				{
					strTitle += "Deposits, ";
				}
				if (chkRT.CheckState == CheckState.Checked)
				{
					strTitle += "Returned Checks, ";
				}
				if (chkIN.CheckState == CheckState.Checked)
				{
					strTitle += "Interest, ";
				}
				if (chkOC.CheckState == CheckState.Checked)
				{
					strTitle += "Other Credits, ";
				}
				if (chkOD.CheckState == CheckState.Checked)
				{
					strTitle += "Other Debits, ";
				}
				strTitle = Strings.Left(strTitle, strTitle.Length - 2);
			}
		}

		private void optStatusAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraStatusOptions.Enabled = false;
			chkCashed.CheckState = CheckState.Unchecked;
			chkVoided.CheckState = CheckState.Unchecked;
			chkDeleted.CheckState = CheckState.Unchecked;
		}

		private void optStatusSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//fraStatusOptions.Enabled = true;
			if (sender == cmbStatusAll)
			{
				switch (cmbStatusAll.SelectedIndex)
				{
					case 1:
						{
							fraStatusOptions.Enabled = true;
							break;
						}
					case 0:
						{
							optStatusAll_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void optTypeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraTypeOptions.Enabled = false;
			chkAP.CheckState = CheckState.Unchecked;
			chkPY.CheckState = CheckState.Unchecked;
			chkDP.CheckState = CheckState.Unchecked;
			chkRT.CheckState = CheckState.Unchecked;
			chkIN.CheckState = CheckState.Unchecked;
			chkOC.CheckState = CheckState.Unchecked;
			chkOD.CheckState = CheckState.Unchecked;
		}

		private void optTypeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//fraTypeOptions.Enabled = true;
			if (sender == cmbTypeAll)
			{
				switch (cmbTypeAll.SelectedIndex)
				{
					case 1:
						{
							fraTypeOptions.Enabled = true;
							break;
						}
					case 0:
						{
							optTypeAll_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void txtLowCheck_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			if (vs1.Visible)
			{
				cmdPrint_Click(sender, e);
			}
			else
			{
				cmdDisplay_Click(sender, e);
				cmdCancel.Visible = true;
				//FC:FINAL:RPU: Change the width of button according to its new Text
				cmdPrint.Width = 146;
				cmdPrint.Text = "Print Preview";
			}
		}
	}
}
