﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptUnpostedJournal.
	/// </summary>
	public partial class rptUnpostedJournal : BaseSectionReport
	{
		public static rptUnpostedJournal InstancePtr
		{
			get
			{
				return (rptUnpostedJournal)Sys.GetInstance(typeof(rptUnpostedJournal));
			}
		}

		protected rptUnpostedJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEntry.Dispose();
				rsDetail.Dispose();
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUnpostedJournal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsDetail = new clsDRWrapper();
		clsDRWrapper rsEntry = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotalDebits As Decimal	OnWrite(short, Decimal)
		Decimal curTotalDebits;
		// vbPorter upgrade warning: curTotalCredits As Decimal	OnWrite(short, Decimal)
		Decimal curTotalCredits;

		public rptUnpostedJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Unposted Journal Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsDetail.MoveNext();
                if (FCConvert.ToString(rsInfo.Get_Fields("Type")) != "AP" && FCConvert.ToString(rsInfo.Get_Fields("Type")) != "AC" && FCConvert.ToString(rsInfo.Get_Fields("Type")) != "EN")
				{
					rsEntry.MoveNext();
					eArgs.EOF = rsDetail.EndOfFile();
				}
				else
				{
					CheckAgain:
					;
					if (rsDetail.EndOfFile())
					{
						rsEntry.MoveNext();
						if (rsEntry.EndOfFile() != true)
						{
                            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
							{
								rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsEntry.Get_Fields_Int32("ID") + " ORDER BY ID");
							}
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							else if (rsInfo.Get_Fields("Type") == "EN")
							{
								rsDetail.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + rsEntry.Get_Fields_Int32("ID") + " ORDER BY ID");
							}
							goto CheckAgain;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)));
			fldJournal.Text = "Journal - " + Strings.Format(rptMultipleJournals.InstancePtr.UserData, "0000") + " - " + rsInfo.Get_Fields_String("Description");
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
			{
				rsEntry.OpenRecordset("SELECT * FROM APJournal WHERE Status <> 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)));
				rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsEntry.Get_Fields_Int32("ID") + " ORDER BY ID");
			}
            else if (rsInfo.Get_Fields("Type") == "EN")
			{
				rsEntry.OpenRecordset("SELECT * FROM Encumbrances WHERE Status <> 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)));
				rsDetail.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + rsEntry.Get_Fields_Int32("ID") + " ORDER BY ID");
			}
			else
			{
				rsEntry.OpenRecordset("SELECT * FROM JournalEntries WHERE Status <> 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
				rsDetail.OpenRecordset("SELECT * FROM JournalEntries WHERE Status <> 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
			}
			blnFirstRecord = true;
			if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				rptUnpostedJournal.InstancePtr.Cancel();
				return;
			}
			curTotalDebits = 0;
			curTotalCredits = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            fldPeriod.Text = Strings.Format(rsEntry.Get_Fields("Period"), "00");
            fldType.Text = FCConvert.ToString(rsInfo.Get_Fields("Type"));
			if (rsEntry.Get_Fields_Int32("VendorNumber") != 0)
			{
				fldVendor.Text = Strings.Format(rsEntry.Get_Fields_Int32("VendorNumber"), "00000");
			}
			else
			{
				fldVendor.Text = "";
			}
			fldDescription.Text = rsDetail.Get_Fields_String("Description");
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
			{
				if (rsDetail.Get_Fields_Decimal("Encumbrance") == 0)
				{
                    if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && rsDetail.Get_Fields_Decimal("Amount") < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && rsDetail.Get_Fields_Decimal("Amount") > 0))
					{
                        fldDebits.Text = (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")).FormatAsCurrencyNoSymbol();
                        curTotalDebits += rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount");
						fldCredits.Text = "";
					}
					else
					{
                        fldCredits.Text = ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1).FormatAsCurrencyNoSymbol();
                        curTotalCredits += ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1);
						fldDebits.Text = "";
					}
				}
				else
				{
					if (rptMultipleJournals.InstancePtr.blnShowLiquidatedEncActivity == true)
					{
                        if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && FCConvert.ToDecimal(rsDetail.Get_Fields("Amount")) < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && FCConvert.ToDecimal(rsDetail.Get_Fields("Amount")) > 0))
                        {
                            fldDebits.Text =
                                (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount"))
                                .FormatAsCurrencyNoSymbol();
                            curTotalDebits += rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount");
							fldCredits.Text = Strings.Format(rsDetail.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
							curTotalCredits += rsDetail.Get_Fields_Decimal("Encumbrance");
						}
						else
						{
                            fldCredits.Text = ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1).FormatAsCurrencyNoSymbol();
                            curTotalCredits += ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1);
							fldDebits.Text = Strings.Format(FCConvert.ToDecimal(rsDetail.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00");
							curTotalDebits += (FCConvert.ToDecimal(rsDetail.Get_Fields_Decimal("Encumbrance")) * -1);
						}
					}
					else
					{
                        if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && rsDetail.Get_Fields_Decimal("Amount") > 0))
						{
                            fldDebits.Text =(rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")).FormatAsCurrencyNoSymbol();
                            curTotalDebits += (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance"));
							fldCredits.Text = "";
						}
						else
						{
                            fldCredits.Text = ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1).FormatAsCurrencyNoSymbol();
                            curTotalCredits += ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1);
							fldDebits.Text = "";
						}
					}
				}
			}

			else if (rsInfo.Get_Fields("Type") == "EN")
			{
                if (rsDetail.Get_Fields_Decimal("Amount") > 0)
				{
                    fldDebits.Text = rsDetail.Get_Fields_Decimal("Amount").FormatAsCurrencyNoSymbol();
                    curTotalDebits += rsDetail.Get_Fields_Decimal("Amount");
					fldCredits.Text = "";
				}
				else
				{
                    fldCredits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
                    curTotalCredits += rsDetail.Get_Fields_Decimal("Amount") * -1;
					fldDebits.Text = "";
				}
			}
			else
			{
                if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && FCConvert.ToDecimal(rsDetail.Get_Fields("Amount")) < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && FCConvert.ToDecimal(rsDetail.Get_Fields("Amount")) > 0))
				{
                    fldDebits.Text = Strings.Format(rsDetail.Get_Fields("Amount"), "#,##0.00");
                    curTotalDebits += rsDetail.Get_Fields("Amount");
					fldCredits.Text = "";
				}
				else
				{
                    if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "PY" && FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "E")
					{
						if (rptMultipleJournals.InstancePtr.blnShowLiquidatedEncActivity == true)
						{
                            fldCredits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
                            curTotalCredits += (rsDetail.Get_Fields_Decimal("Amount") * -1);
                            fldDebits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
                            curTotalDebits += (rsDetail.Get_Fields_Decimal("Amount") * -1);
						}
						else
						{
							fldCredits.Text = "0.00";
							fldDebits.Text = "";
						}
					}
					else
					{
                        fldCredits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
                        curTotalCredits += (rsDetail.Get_Fields_Decimal("Amount") * -1);
						fldDebits.Text = "";
					}
				}
			}
            fldAccount.Text = rsDetail.Get_Fields_String("Account");
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP")
			{
                fldWarrant.Text = Strings.Format(rsEntry.Get_Fields("Warrant"), "0000");
			}
			else
			{
				fldWarrant.Text = "";
			}
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
			{
                fldCheck.Text = FCConvert.ToString(rsEntry.Get_Fields("CheckNumber"));
                if (rsEntry.Get_Fields_DateTime("CheckDate").IsEarlierThan(new DateTime(1901, 1, 1)))
                {
                    fldDate.Text = "";
                }
                else
                {
                    fldDate.Text = Strings.Format(rsEntry.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
                }
            }
            else if (rsInfo.Get_Fields("Type") == "EN")
			{
				fldCheck.Text = "";
				fldDate.Text = Strings.Format(rsEntry.Get_Fields_DateTime("EncumbrancesDate"), "MM/dd/yy");
			}
			else
			{
                fldCheck.Text = FCConvert.ToString(rsEntry.Get_Fields("CheckNumber"));
				fldDate.Text = Strings.Format(rsEntry.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yy");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalDebits.Text = Strings.Format(curTotalDebits, "#,##0.00");
			fldTotalCredits.Text = Strings.Format(curTotalCredits, "#,##0.00");
		}

		private void rptUnpostedJournal_Load(object sender, System.EventArgs e)
		{

		}
	}
}
