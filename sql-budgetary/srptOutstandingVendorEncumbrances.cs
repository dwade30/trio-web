﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptOutstandingVendorEncumbrances.
	/// </summary>
	public partial class srptOutstandingVendorEncumbrances : FCSectionReport
	{
		public static srptOutstandingVendorEncumbrances InstancePtr
		{
			get
			{
				return (srptOutstandingVendorEncumbrances)Sys.GetInstance(typeof(srptOutstandingVendorEncumbrances));
			}
		}

		protected srptOutstandingVendorEncumbrances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorNames.Dispose();
				rsEncJournalInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOutstandingVendorEncumbrances	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rsEncJournalInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curDebitsTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDebitsTotal;
		// vbPorter upgrade warning: curCreditsTotal As Decimal	OnWrite(Decimal, short)
		Decimal curCreditsTotal;
		//FC:FINAL:AM: missing declaration
		clsDRWrapper rsVendorNames = new clsDRWrapper();

		public srptOutstandingVendorEncumbrances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outstanding Encumbrances";
		}

		private void GetEncData()
		{
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = modValidateAccount.GetFormat_6(rsEncJournalInfo.Get_Fields("Period"), 2);
			fldWarrant.Text = "";
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsEncJournalInfo.Get_Fields("JournalNumber"), 4);
			fldDate.Text = Strings.Format(rsEncJournalInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy");
			if (FCConvert.ToDouble(rptVendorDetail.InstancePtr.strReportType) == 4)
			{
				if (FCConvert.ToInt32(rsEncJournalInfo.Get_Fields_Int32("VendorNumber")) == 0)
				{
					fldAccount.Text = modValidateAccount.GetFormat_6(rsEncJournalInfo.Get_Fields_Int32("VendorNumber"), 5) + " " + rsEncJournalInfo.Get_Fields_String("TempVendorName");
				}
				else
				{
					if (rsVendorNames.EndOfFile() != true && rsVendorNames.BeginningOfFile() != true)
					{
						if (rsVendorNames.FindFirstRecord("VendorNumber", rsEncJournalInfo.Get_Fields_Int32("VendorNumber")))
						{
							fldAccount.Text = modValidateAccount.GetFormat_6(rsEncJournalInfo.Get_Fields_Int32("VendorNumber"), 5) + " " + rsVendorNames.Get_Fields_String("CheckName");
						}
						else
						{
							fldAccount.Text = modValidateAccount.GetFormat_6(rsEncJournalInfo.Get_Fields_Int32("VendorNumber"), 5) + " UNKNOWN";
						}
					}
					else
					{
						fldAccount.Text = modValidateAccount.GetFormat_6(rsEncJournalInfo.Get_Fields_Int32("VendorNumber"), 5) + " UNKNOWN";
					}
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = rsEncJournalInfo.Get_Fields_String("Account");
			}
			fldDescription.Text = rsEncJournalInfo.Get_Fields_String("Description");
			fldRCB.Text = "R";
			fldType.Text = "E";
			fldCheck.Text = "";
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format(rsEncJournalInfo.Get_Fields("Amount") + rsEncJournalInfo.Get_Fields_Decimal("Adjustments") - rsEncJournalInfo.Get_Fields_Decimal("Liquidated"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			if (rsEncJournalInfo.Get_Fields("Amount") + rsEncJournalInfo.Get_Fields_Decimal("Adjustments") - rsEncJournalInfo.Get_Fields_Decimal("Liquidated") > 0)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curDebitsTotal += (rsEncJournalInfo.Get_Fields("Amount") + rsEncJournalInfo.Get_Fields_Decimal("Adjustments") - rsEncJournalInfo.Get_Fields_Decimal("Liquidated"));
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curCreditsTotal += ((rsEncJournalInfo.Get_Fields("Amount") + rsEncJournalInfo.Get_Fields_Decimal("Adjustments") - rsEncJournalInfo.Get_Fields_Decimal("Liquidated")) * -1);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsEncJournalInfo.MoveNext();
				eArgs.EOF = rsEncJournalInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsEncJournalInfo.OpenRecordset("SELECT * FROM EncumbranceInfo");
			blnFirstRecord = true;
			curCreditsTotal = 0;
			curDebitsTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			GetEncData();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curDebitsTotal - curCreditsTotal, "#,##0.00");
			if (curDebitsTotal == 0 || curCreditsTotal == 0)
			{
				fldDebits.Text = "";
				fldCredits.Text = "";
				lblDebits.Text = "";
				lblCredits.Text = "";
				fldDebitsStar.Visible = false;
				fldCreditsStar.Visible = false;
				fldDebits.Visible = false;
				fldCredits.Visible = false;
				lblCredits.Visible = false;
				lblDebits.Visible = false;
			}
			else
			{
				lblDebits.Text = "Debits";
				lblCredits.Text = "Credits";
				fldDebits.Text = Strings.Format(curDebitsTotal, "#,##0.00");
				fldCredits.Text = Strings.Format(curCreditsTotal, "#,##0.00");
				fldDebits.Visible = true;
				fldCredits.Visible = true;
				lblCredits.Visible = true;
				lblDebits.Visible = true;
				fldDebitsStar.Visible = true;
				fldCreditsStar.Visible = true;
			}
			rptVendorDetail.InstancePtr.curGrandTotal += curDebitsTotal - curCreditsTotal;
		}

		
	}
}
