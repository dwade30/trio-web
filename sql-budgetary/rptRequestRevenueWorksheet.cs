﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptRequestRevenueWorksheet.
	/// </summary>
	public partial class rptRequestRevenueWorksheet : BaseSectionReport
	{
		public static rptRequestRevenueWorksheet InstancePtr
		{
			get
			{
				return (rptRequestRevenueWorksheet)Sys.GetInstance(typeof(rptRequestRevenueWorksheet));
			}
		}

		protected rptRequestRevenueWorksheet _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo.Dispose();
				rsBudgetInfo.Dispose();
				rsDepartmentInfo.Dispose();
				rsDivisionInfo.Dispose();
				rsYTDActivity.Dispose();
				rsRevenueInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRequestRevenueWorksheet	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool DeptBreakFlag;
		bool DivBreakFlag;
		bool blnComments;
		bool blnDepartmentTotals;
		bool blnDivisionTotals;
		string strAccountType;
		string strSingleDepartment = "";
		string strSingleDivision = "";
		string strLowDepartment = "";
		string strHighDepartment = "";
		clsDRWrapper rsDepartmentInfo = new clsDRWrapper();
		clsDRWrapper rsDivisionInfo = new clsDRWrapper();
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		clsDRWrapper rsRevenueInfo = new clsDRWrapper();
		clsDRWrapper rsBudgetInfo = new clsDRWrapper();
		bool blnDeptChange;
		bool blnFirstRecord;
		// vbPorter upgrade warning: curDivisionCurrentBudgetTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionCurrentBudgetTotal;
		// vbPorter upgrade warning: curDepartmentCurrentBudgetTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentCurrentBudgetTotal;
		Decimal curCurrentBudgetTotal;
		// vbPorter upgrade warning: curDivisionInitialRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionInitialRequestTotal;
		// vbPorter upgrade warning: curDepartmentInitialRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentInitialRequestTotal;
		Decimal curInitialRequestTotal;
		// vbPorter upgrade warning: curDivisionManagerRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionManagerRequestTotal;
		// vbPorter upgrade warning: curDepartmentManagerRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentManagerRequestTotal;
		Decimal curManagerRequestTotal;
		// vbPorter upgrade warning: curDivisionCommitteeRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionCommitteeRequestTotal;
		// vbPorter upgrade warning: curDepartmentCommitteeRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentCommitteeRequestTotal;
		Decimal curCommitteeRequestTotal;
		// vbPorter upgrade warning: curDivisionElectedRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionElectedRequestTotal;
		// vbPorter upgrade warning: curDepartmentElectedRequestTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentElectedRequestTotal;
		Decimal curElectedRequestTotal;
		// vbPorter upgrade warning: curDivisionApprovedAmountTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionApprovedAmountTotal;
		// vbPorter upgrade warning: curDepartmentApprovedAmountTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentApprovedAmountTotal;
		Decimal curApprovedAmountTotal;
		string strDivFooterTitle = "";
		string strDeptFooterTitle = "";
		bool blnCurrentYearBudget;
		int lngCurrent;
		int lngBudget;
		string strReport = "";
		string strInfoType = "";
		string strHeaderTitle = "";
		string strReportTitle = "";
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		bool blnIncBudAdj;
		bool blnOnlyChangedAmounts;
		// Dim strDept As String
		public rptRequestRevenueWorksheet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
			this.Fields.Add("GroupTitle");
			this.Fields.Add("DeptGroupFooter");
			this.Fields.Add("DivGroupFooter");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTempAccount = "";
			bool blnChangeDeptDiv = false;
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (rsDepartmentInfo.EndOfFile() && rsDivisionInfo.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
			}
			else
			{
				if (rsDepartmentInfo.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
			}
			if (blnFirstRecord)
			{
				if (strInfoType == "V")
				{
					rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strSingleDepartment + "' AND Division = '" + strSingleDivision + "'");
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + strSingleDepartment + "' AND SecondAccountField = '" + strSingleDivision + "' ORDER BY ThirdAccountField");
					blnFirstRecord = false;
					CheckAccountAgain:
					;
					if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
					{
						strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
						strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
						if (BudgetAccountExists(strTempAccount))
						{
							// do nothing
						}
						else
						{
							rsAccountInfo.MoveNext();
							goto CheckAccountAgain;
						}
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
				else
				{
					CheckDivInfo:
					;
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2))) + "' ORDER BY Division");
						if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
						{
							// do nothing
						}
						else
						{
							rsDepartmentInfo.MoveNext();
							if (rsDepartmentInfo.EndOfFile() != true)
							{
								goto CheckDivInfo;
							}
							else
							{
								eArgs.EOF = true;
								return;
							}
						}
					}
					CheckAccountInfo:
					;
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND SecondAccountField = '" + rsDivisionInfo.Get_Fields_String("Division") + "' ORDER BY ThirdAccountField");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' ORDER BY SecondAccountField, ThirdAccountField");
					}
					blnFirstRecord = false;
					CheckAccountAgain2:
					;
					if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
					{
						strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
						strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
						if (BudgetAccountExists(strTempAccount))
						{
							// do nothing
						}
						else
						{
							rsAccountInfo.MoveNext();
							goto CheckAccountAgain2;
						}
					}
					else
					{
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							rsDivisionInfo.MoveNext();
							if (rsDivisionInfo.EndOfFile() != true)
							{
								goto CheckAccountInfo;
							}
							else
							{
								rsDepartmentInfo.MoveNext();
								if (rsDepartmentInfo.EndOfFile() != true)
								{
									goto CheckDivInfo;
								}
								else
								{
									eArgs.EOF = true;
									return;
								}
							}
						}
						else
						{
							rsDepartmentInfo.MoveNext();
							if (rsDepartmentInfo.EndOfFile() != true)
							{
								goto CheckDivInfo;
							}
							else
							{
								eArgs.EOF = true;
								return;
							}
						}
					}
				}
				blnChangeDeptDiv = false;
				//FC:FINAL:BBE:#i597 - initialize array with empty string when null
				if (this.Fields["GroupTitle"].Value == null)
				{
					this.Fields["GroupTitle"].Value = string.Empty;
				}
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					if (Strings.Left(FCConvert.ToString(this.Fields["GroupTitle"].Value), ("Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription") + " / " + rsDivisionInfo.Get_Fields_String("ShortDescription")).Length) == "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription") + " / " + rsDivisionInfo.Get_Fields_String("ShortDescription"))
					{
						blnChangeDeptDiv = false;
					}
					else
					{
						blnChangeDeptDiv = true;
					}
				}
				else
				{
					if (Strings.Left(FCConvert.ToString(this.Fields["GroupTitle"].Value), ("Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription")).Length) == "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription"))
					{
						blnChangeDeptDiv = false;
					}
					else
					{
						blnChangeDeptDiv = true;
					}
				}
				if (blnChangeDeptDiv)
				{
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						this.Fields["GroupTitle"].Value = "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription") + " / " + rsDivisionInfo.Get_Fields_String("ShortDescription");
					}
					else
					{
						this.Fields["GroupTitle"].Value = "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription");
					}
				}
				else
				{
					if (Strings.Right(this.Fields["GroupTitle"].Value.ToString(), 6) != "CONT'D")
					{
						this.Fields["GroupTitle"].Value = this.Fields["GroupTitle"].Value + " CONT'D";
					}
					if (Strings.Right(fldDeptDivTitle.Text, 6) != "CONT'D")
					{
						fldDeptDivTitle.Text = fldDeptDivTitle.Text + " CONT'D";
					}
				}
				// set title for group footer
				this.Fields["DeptGroupFooter"].Value = rsDepartmentInfo.Get_Fields_String("ShortDescription");
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					this.Fields["DivGroupFooter"].Value = rsDivisionInfo.Get_Fields_String("ShortDescription");
				}
				else
				{
					this.Fields["DivGroupFooter"].Value = "";
				}
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department") + rsDivisionInfo.Get_Fields_String("Division");
				}
				else
				{
					this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department");
				}
				eArgs.EOF = false;
				return;
			}
			rsAccountInfo.MoveNext();
			CheckAccountAgain3:
			;
			if (rsAccountInfo.EndOfFile() != true)
			{
				strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
				strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
				if (BudgetAccountExists(strTempAccount))
				{
					// do nothing
				}
				else
				{
					rsAccountInfo.MoveNext();
					goto CheckAccountAgain3;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					CheckNewDiv:
					;
					rsDivisionInfo.MoveNext();
					CheckNewDept:
					;
					if (rsDivisionInfo.EndOfFile() != true)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND SecondAccountField = '" + rsDivisionInfo.Get_Fields_String("Division") + "' ORDER BY ThirdAccountField");
						CheckAccountAgain4:
						;
						if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								// do nothing
							}
							else
							{
								rsAccountInfo.MoveNext();
								goto CheckAccountAgain4;
							}
						}
						else
						{
							goto CheckNewDiv;
						}
					}
					else
					{
						rsDepartmentInfo.MoveNext();
						blnDeptChange = true;
						if (rsDepartmentInfo.EndOfFile() != true)
						{
							rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2))) + "' ORDER BY Division");
							goto CheckNewDept;
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				else
				{
					CheckNewDept2:
					;
					rsDepartmentInfo.MoveNext();
					blnDeptChange = true;
					if (rsDepartmentInfo.EndOfFile() != true)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' ORDER BY SecondAccountField");
						CheckAccountAgain5:
						;
						if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								// do nothing
							}
							else
							{
								rsAccountInfo.MoveNext();
								goto CheckAccountAgain5;
							}
						}
						else
						{
							goto CheckNewDept2;
						}
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
			}
			blnChangeDeptDiv = false;
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (Strings.Left(FCConvert.ToString(this.Fields["GroupTitle"].Value), ("Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription") + " / " + rsDivisionInfo.Get_Fields_String("ShortDescription")).Length) == "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription") + " / " + rsDivisionInfo.Get_Fields_String("ShortDescription"))
				{
					blnChangeDeptDiv = false;
				}
				else
				{
					blnChangeDeptDiv = true;
				}
			}
			else
			{
				if (Strings.Left(FCConvert.ToString(this.Fields["GroupTitle"].Value), ("Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription")).Length) == "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription"))
				{
					blnChangeDeptDiv = false;
				}
				else
				{
					blnChangeDeptDiv = true;
				}
			}
			if (blnChangeDeptDiv)
			{
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					this.Fields["GroupTitle"].Value = "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription") + " / " + rsDivisionInfo.Get_Fields_String("ShortDescription");
				}
				else
				{
					this.Fields["GroupTitle"].Value = "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("ShortDescription");
				}
			}
			else
			{
				if (Strings.Right(this.Fields["GroupTitle"].Value.ToString(), 6) != "CONT'D")
				{
					this.Fields["GroupTitle"].Value = this.Fields["GroupTitle"].Value + " CONT'D";
				}
				if (Strings.Right(fldDeptDivTitle.Text, 6) != "CONT'D")
				{
					fldDeptDivTitle.Text = fldDeptDivTitle.Text + " CONT'D";
				}
			}
			// set title for group footer
			this.Fields["DeptGroupFooter"].Value = rsDepartmentInfo.Get_Fields_String("ShortDescription");
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				this.Fields["DivGroupFooter"].Value = rsDivisionInfo.Get_Fields_String("ShortDescription");
			}
			else
			{
				this.Fields["DivGroupFooter"].Value = "";
			}
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department") + rsDivisionInfo.Get_Fields_String("Division");
			}
			else
			{
				this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department");
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// Dim pg As New Canvas
			// Dim intPageIndex As Integer
			// 
			// If strAccountType = "B" Then
			// intPageIndex = 0
			// For Each pg In rptRequestExpenseWorkSheet.Pages
			// Me.Pages.Insert intPageIndex, pg
			// intPageIndex = intPageIndex + 1
			// Next pg
			// Me.Pages.Commit
			// Me.Refresh
			// DoEvents
			// Unload rptRequestExpenseWorkSheet
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnDeptChange = false;
			Line3.Visible = true;
			Label1.Text = strReportTitle;
			frmReportViewer.InstancePtr.Text = strReportTitle;
			lblCurrentBudget.Text = FCConvert.ToString(lngCurrent);
			lblInitialBudget.Text = FCConvert.ToString(lngBudget);
			lblManagerBudget.Text = FCConvert.ToString(lngBudget);
			lblCommitteeBudget.Text = FCConvert.ToString(lngBudget);
			lblElectedBudget.Text = FCConvert.ToString(lngBudget);
			lblApprovedBudget.Text = FCConvert.ToString(lngBudget);
			if (blnCurrentYearBudget)
			{
				lblCurrentBudget.Visible = false;
				lblCurrentBudget2.Visible = false;
				fldCurrentBudget.Visible = false;
				fldDivisionTotalCurrentBudget.Visible = false;
				fldDepartmentTotalCurrentBudget.Visible = false;
			}
			lblCurrentBudget.Text = FCConvert.ToString(lngCurrent);
			lblInitialBudget.Text = FCConvert.ToString(lngBudget) + " ";
			lblManagerBudget.Text = FCConvert.ToString(lngBudget) + " ";
			lblCommitteeBudget.Text = FCConvert.ToString(lngBudget) + " ";
			lblElectedBudget.Text = FCConvert.ToString(lngBudget) + " ";
			lblApprovedBudget.Text = FCConvert.ToString(lngBudget) + " ";
			if (strReport == "I")
			{
				lblCurrentBudget.Text = "Current";
				lblCurrentBudget2.Text = "Budget";
				lblInitialBudget.Text = "Budget";
				lblInitialBudget2.Text = "Request";
				lblManagerBudget.Visible = false;
				lblManagerBudget2.Visible = false;
				lblCommitteeBudget.Visible = false;
				lblCommitteeBudget2.Visible = false;
				lblElectedBudget.Visible = false;
				lblElectedBudget2.Visible = false;
				lblApprovedBudget.Visible = false;
				lblApprovedBudget2.Visible = false;
				Line3.X2 = fldInitialBudget.Left + fldInitialBudget.Width;
			}
			else if (strReport == "M")
			{
				lblCommitteeBudget.Visible = false;
				lblCommitteeBudget2.Visible = false;
				lblElectedBudget.Visible = false;
				lblElectedBudget2.Visible = false;
				lblApprovedBudget.Visible = false;
				lblApprovedBudget2.Visible = false;
				Line3.X2 = fldManagerBudget.Left + fldManagerBudget.Width;
			}
			else if (strReport == "C")
			{
				lblElectedBudget.Visible = false;
				lblElectedBudget2.Visible = false;
				lblApprovedBudget.Visible = false;
				lblApprovedBudget2.Visible = false;
				Line3.X2 = fldCommitteeBudget.Left + fldCommitteeBudget.Width;
			}
			else if (strReport == "E")
			{
				lblApprovedBudget.Visible = false;
				lblApprovedBudget2.Visible = false;
				Line3.X2 = fldElectedBudget.Left + fldElectedBudget.Width;
			}
			else
			{
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				this.PrintWidth = 14608 / 1440f;
				Line2.X2 = this.PrintWidth - 100 / 1440f;
				Label3.Left = this.PrintWidth - Label3.Width - 100 / 1440f;
				Label4.Left = this.PrintWidth - Label4.Width - 100 / 1440f;
				Label1.Width = this.PrintWidth - 100 / 1440f - Label3.Width - Label1.Left;
				Label14.Width = this.PrintWidth - 100 / 1440f - Label3.Width - Label14.Left;
				lblApprovedBudget.Left = lblElectedBudget.Left + lblElectedBudget.Width + 135 / 1440f;
				lblApprovedBudget2.Left = lblApprovedBudget.Left;
				fldApprovedAmount.Left = lblApprovedBudget.Left + lblApprovedBudget.Width - fldApprovedAmount.Width;
				fldDivisionTotalApprovedAmount.Left = lblApprovedBudget.Left + lblApprovedBudget.Width - fldDivisionTotalApprovedAmount.Width;
				fldDepartmentTotalApprovedAmount.Left = lblApprovedBudget.Left + lblApprovedBudget.Width - fldDepartmentTotalApprovedAmount.Width;
				fldTotalApprovedAmount.Left = lblApprovedBudget.Left + lblApprovedBudget.Width - fldTotalApprovedAmount.Width;
				Line1.X2 = fldApprovedAmount.Left + fldApprovedAmount.Width;
				Line3.X2 = fldApprovedAmount.Left + fldApprovedAmount.Width;
			}
			if (blnCurrentYearBudget)
			{
				fldCurrentBudget.Visible = false;
				fldDivisionTotalCurrentBudget.Visible = false;
				fldDepartmentTotalCurrentBudget.Visible = false;
				fldTotalCurrentBudget.Visible = false;
			}
			if (strReport == "I")
			{
				fldManagerBudget.Visible = false;
				fldDivisionTotalManagerRequest.Visible = false;
				fldDepartmentTotalManagerRequest.Visible = false;
				fldTotalManagerRequest.Visible = false;
				fldCommitteeBudget.Visible = false;
				fldDivisionTotalCommitteeRequest.Visible = false;
				fldDepartmentTotalCommitteeRequest.Visible = false;
				fldTotalCommitteeRequest.Visible = false;
				fldElectedBudget.Visible = false;
				fldDivisionTotalElectedRequest.Visible = false;
				fldDepartmentTotalElectedRequest.Visible = false;
				fldTotalElectedRequest.Visible = false;
				fldApprovedAmount.Visible = false;
				fldDivisionTotalApprovedAmount.Visible = false;
				fldDepartmentTotalApprovedAmount.Visible = false;
				fldTotalApprovedAmount.Visible = false;
			}
			else if (strReport == "M")
			{
				fldCommitteeBudget.Visible = false;
				fldDivisionTotalCommitteeRequest.Visible = false;
				fldDepartmentTotalCommitteeRequest.Visible = false;
				fldTotalCommitteeRequest.Visible = false;
				fldElectedBudget.Visible = false;
				fldDivisionTotalElectedRequest.Visible = false;
				fldDepartmentTotalElectedRequest.Visible = false;
				fldTotalElectedRequest.Visible = false;
				fldApprovedAmount.Visible = false;
				fldDivisionTotalApprovedAmount.Visible = false;
				fldDepartmentTotalApprovedAmount.Visible = false;
				fldTotalApprovedAmount.Visible = false;
			}
			else if (strReport == "C")
			{
				fldElectedBudget.Visible = false;
				fldDivisionTotalElectedRequest.Visible = false;
				fldDepartmentTotalElectedRequest.Visible = false;
				fldTotalElectedRequest.Visible = false;
				fldApprovedAmount.Visible = false;
				fldDivisionTotalApprovedAmount.Visible = false;
				fldDepartmentTotalApprovedAmount.Visible = false;
				fldTotalApprovedAmount.Visible = false;
			}
			else if (strReport == "E")
			{
				fldApprovedAmount.Visible = false;
				fldDivisionTotalApprovedAmount.Visible = false;
				fldDepartmentTotalApprovedAmount.Visible = false;
				fldTotalApprovedAmount.Visible = false;
			}
			// retrieve summary informaiton so we can get at it easily
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();
			// CalculateAccountInfo True, True, False, "R"
			RetrieveInfo();
			// If rsAccountInfo.EndOfFile <> True And rsAccountInfo.BeginningOfFile <> True Then
			// do nothing
			// Else
			// Me.Cancel
			// Unload Me
			// Exit Sub
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTempAccount;
			strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
			strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
			rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + strTempAccount + "'");
			if (modAccountTitle.Statics.RevDivFlag)
			{
				rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Revenue = '" + rsAccountInfo.Get_Fields_String("SecondAccountField") + "'");
				if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("SecondAccountField") + " " + rsRevenueInfo.Get_Fields_String("ShortDescription");
					if (blnIncBudAdj)
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						fldCurrentBudget.Text = Strings.Format(GetNetBudget_2(rsAccountInfo.Get_Fields("account")), "#,##0.00");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						fldCurrentBudget.Text = Strings.Format(GetOriginalBudget_2(rsAccountInfo.Get_Fields("account")), "#,##0.00");
					}
					if (strReport == "I")
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) == 0)
						{
							fldInitialBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
							fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
					}
					if (strReport == "M" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("InitialRequest") == rsBudgetInfo.Get_Fields("ManagerRequest"))
						{
							fldManagerBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
							fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
					}
					if (strReport == "C" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ManagerRequest") == rsBudgetInfo.Get_Fields("CommitteeRequest"))
						{
							fldCommitteeBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
							fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
					}
					if (strReport == "E" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("CommitteeRequest") == rsBudgetInfo.Get_Fields("ElectedRequest"))
						{
							fldElectedBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
							fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
					}
					if (strReport == "A" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ElectedRequest") == rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"))
						{
							fldApprovedAmount.Text = "";
						}
						else
						{
							fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
						}
					}
					else
					{
						fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
					}
				}
				else
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("SecondAccountField") + " UNKNOWN";
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					fldCurrentBudget.Text = Strings.Format(GetNetBudget_2(rsAccountInfo.Get_Fields("account")), "#,##0.00");
					if (strReport == "I")
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) == 0)
						{
							fldInitialBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
							fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
					}
					if (strReport == "M" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("InitialRequest") == rsBudgetInfo.Get_Fields("ManagerRequest"))
						{
							fldManagerBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
							fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
					}
					if (strReport == "C" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ManagerRequest") == rsBudgetInfo.Get_Fields("CommitteeRequest"))
						{
							fldCommitteeBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
							fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
					}
					if (strReport == "E" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("CommitteeRequest") == rsBudgetInfo.Get_Fields("ElectedRequest"))
						{
							fldElectedBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
							fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
					}
					if (strReport == "A" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ElectedRequest") == rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"))
						{
							fldApprovedAmount.Text = "";
						}
						else
						{
							fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
						}
					}
					else
					{
						fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
					}
				}
			}
			else
			{
				rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND Revenue = '" + rsAccountInfo.Get_Fields_String("ThirdAccountField") + "'");
				if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("ThirdAccountField") + " " + rsRevenueInfo.Get_Fields_String("ShortDescription");
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					fldCurrentBudget.Text = Strings.Format(GetNetBudget_2(rsAccountInfo.Get_Fields("account")), "#,##0.00");
					if (strReport == "I")
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) == 0)
						{
							fldInitialBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
							fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
					}
					if (strReport == "M" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("InitialRequest") == rsBudgetInfo.Get_Fields("ManagerRequest"))
						{
							fldManagerBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
							fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
					}
					if (strReport == "C" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ManagerRequest") == rsBudgetInfo.Get_Fields("CommitteeRequest"))
						{
							fldCommitteeBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
							fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
					}
					if (strReport == "E" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("CommitteeRequest") == rsBudgetInfo.Get_Fields("ElectedRequest"))
						{
							fldElectedBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
							fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
					}
					if (strReport == "A" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ElectedRequest") == rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"))
						{
							fldApprovedAmount.Text = "";
						}
						else
						{
							fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
						}
					}
					else
					{
						fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
					}
				}
				else
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("ThirdAccountField") + " UNKNOWN";
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					fldCurrentBudget.Text = Strings.Format(GetNetBudget_2(rsAccountInfo.Get_Fields("account")), "#,##0.00");
					if (strReport == "I")
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) == 0)
						{
							fldInitialBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
							fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						fldInitialBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("InitialRequest"), "#,##0.00");
					}
					if (strReport == "M" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("InitialRequest") == rsBudgetInfo.Get_Fields("ManagerRequest"))
						{
							fldManagerBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
							fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						fldManagerBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ManagerRequest"), "#,##0.00");
					}
					if (strReport == "C" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ManagerRequest") == rsBudgetInfo.Get_Fields("CommitteeRequest"))
						{
							fldCommitteeBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
							fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						fldCommitteeBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("CommitteeRequest"), "#,##0.00");
					}
					if (strReport == "E" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("CommitteeRequest") == rsBudgetInfo.Get_Fields("ElectedRequest"))
						{
							fldElectedBudget.Text = "";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
							fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						fldElectedBudget.Text = Strings.Format(rsBudgetInfo.Get_Fields("ElectedRequest"), "#,##0.00");
					}
					if (strReport == "A" && blnOnlyChangedAmounts)
					{
						// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
						if (rsBudgetInfo.Get_Fields("ElectedRequest") == rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"))
						{
							fldApprovedAmount.Text = "";
						}
						else
						{
							fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
						}
					}
					else
					{
						fldApprovedAmount.Text = Strings.Format(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"), "#,##0.00");
					}
				}
			}
			curDivisionCurrentBudgetTotal += FCConvert.ToDecimal(fldCurrentBudget.Text);
			curDepartmentCurrentBudgetTotal += FCConvert.ToDecimal(fldCurrentBudget.Text);
			curCurrentBudgetTotal += FCConvert.ToDecimal(fldCurrentBudget.Text);
			if (fldInitialBudget.Text != "")
			{
				// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
				curDivisionInitialRequestTotal += rsBudgetInfo.Get_Fields("InitialRequest");
				// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
				curDepartmentInitialRequestTotal += rsBudgetInfo.Get_Fields("InitialRequest");
				// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
				curInitialRequestTotal += rsBudgetInfo.Get_Fields("InitialRequest");
			}
			if (fldManagerBudget.Text != "")
			{
				// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
				curDivisionManagerRequestTotal += rsBudgetInfo.Get_Fields("ManagerRequest");
				// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
				curDepartmentManagerRequestTotal += rsBudgetInfo.Get_Fields("ManagerRequest");
				// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
				curManagerRequestTotal += rsBudgetInfo.Get_Fields("ManagerRequest");
			}
			if (fldCommitteeBudget.Text != "")
			{
				// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
				curDivisionCommitteeRequestTotal += rsBudgetInfo.Get_Fields("CommitteeRequest");
				// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
				curDepartmentCommitteeRequestTotal += rsBudgetInfo.Get_Fields("CommitteeRequest");
				// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
				curCommitteeRequestTotal += rsBudgetInfo.Get_Fields("CommitteeRequest");
			}
			if (fldElectedBudget.Text != "")
			{
				// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
				curDivisionElectedRequestTotal += FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("ElectedRequest"));
				// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
				curDepartmentElectedRequestTotal += FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("ElectedRequest"));
				// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
				curElectedRequestTotal += FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("ElectedRequest"));
			}
			if (fldApprovedAmount.Text != "")
			{
				curDivisionApprovedAmountTotal += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
				curDepartmentApprovedAmountTotal += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
				curApprovedAmountTotal += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
			}
			if (blnComments)
			{
				if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
				{
					fldComments.Text = rsBudgetInfo.Get_Fields_String("Comments");
				}
				else
				{
					fldComments.Text = "";
				}
			}
			else
			{
				fldComments.Text = "";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			bool blnShowFinalTotals;
			if (modAccountTitle.Statics.RevDivFlag)
			{
				if (blnDepartmentTotals)
				{
					fldDepartmentName.Text = strDeptFooterTitle;
					fldDepartmentTotalCurrentBudget.Text = Strings.Format(curDepartmentCurrentBudgetTotal, "#,##0.00");
					fldDepartmentTotalInitialRequest.Text = Strings.Format(curDepartmentInitialRequestTotal, "#,##0.00");
					fldDepartmentTotalManagerRequest.Text = Strings.Format(curDepartmentManagerRequestTotal, "#,##0.00");
					fldDepartmentTotalCommitteeRequest.Text = Strings.Format(curDepartmentCommitteeRequestTotal, "#,##0.00");
					fldDepartmentTotalElectedRequest.Text = Strings.Format(curDepartmentElectedRequestTotal, "#,##0.00");
					fldDepartmentTotalApprovedAmount.Text = Strings.Format(curDepartmentApprovedAmountTotal, "#,##0.00");
				}
				else
				{
					fldDepartmentName.Text = "";
					fldDepartmentTotalCurrentBudget.Text = "";
					fldDepartmentTotalInitialRequest.Text = "";
					fldDepartmentTotalManagerRequest.Text = "";
					fldDepartmentTotalCommitteeRequest.Text = "";
					fldDepartmentTotalElectedRequest.Text = "";
					fldDepartmentTotalApprovedAmount.Text = "";
				}
				fldDivisionName.Text = "";
				fldDivisionTotalCurrentBudget.Text = "";
				fldDivisionTotalInitialRequest.Text = "";
				fldDivisionTotalManagerRequest.Text = "";
				fldDivisionTotalCommitteeRequest.Text = "";
				fldDivisionTotalElectedRequest.Text = "";
				fldDivisionTotalApprovedAmount.Text = "";
				curDepartmentCurrentBudgetTotal = 0;
				curDepartmentInitialRequestTotal = 0;
				curDepartmentManagerRequestTotal = 0;
				curDepartmentCommitteeRequestTotal = 0;
				curDepartmentElectedRequestTotal = 0;
				curDepartmentApprovedAmountTotal = 0;
				curDivisionCurrentBudgetTotal = 0;
				curDivisionInitialRequestTotal = 0;
				curDivisionManagerRequestTotal = 0;
				curDivisionCommitteeRequestTotal = 0;
				curDivisionElectedRequestTotal = 0;
				curDivisionApprovedAmountTotal = 0;
				if (DeptBreakFlag)
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				else
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
			}
			else
			{
				if (blnDeptChange)
				{
					blnDeptChange = false;
					if (blnDepartmentTotals)
					{
						fldDepartmentName.Text = strDeptFooterTitle;
						fldDepartmentTotalCurrentBudget.Text = Strings.Format(curDepartmentCurrentBudgetTotal, "#,##0.00");
						fldDepartmentTotalInitialRequest.Text = Strings.Format(curDepartmentInitialRequestTotal, "#,##0.00");
						fldDepartmentTotalManagerRequest.Text = Strings.Format(curDepartmentManagerRequestTotal, "#,##0.00");
						fldDepartmentTotalCommitteeRequest.Text = Strings.Format(curDepartmentCommitteeRequestTotal, "#,##0.00");
						fldDepartmentTotalElectedRequest.Text = Strings.Format(curDepartmentElectedRequestTotal, "#,##0.00");
						fldDepartmentTotalApprovedAmount.Text = Strings.Format(curDepartmentApprovedAmountTotal, "#,##0.00");
					}
					else
					{
						fldDepartmentName.Text = "";
						fldDepartmentTotalCurrentBudget.Text = "";
						fldDepartmentTotalInitialRequest.Text = "";
						fldDepartmentTotalManagerRequest.Text = "";
						fldDepartmentTotalCommitteeRequest.Text = "";
						fldDepartmentTotalElectedRequest.Text = "";
						fldDepartmentTotalApprovedAmount.Text = "";
					}
					if (blnDivisionTotals)
					{
						fldDivisionName.Text = strDivFooterTitle;
						fldDivisionTotalCurrentBudget.Text = Strings.Format(curDivisionCurrentBudgetTotal, "#,##0.00");
						fldDivisionTotalInitialRequest.Text = Strings.Format(curDivisionInitialRequestTotal, "#,##0.00");
						fldDivisionTotalManagerRequest.Text = Strings.Format(curDivisionManagerRequestTotal, "#,##0.00");
						fldDivisionTotalCommitteeRequest.Text = Strings.Format(curDivisionCommitteeRequestTotal, "#,##0.00");
						fldDivisionTotalElectedRequest.Text = Strings.Format(curDivisionElectedRequestTotal, "#,##0.00");
						fldDivisionTotalApprovedAmount.Text = Strings.Format(curDivisionApprovedAmountTotal, "#,##0.00");
					}
					else
					{
						fldDivisionName.Text = "";
						fldDivisionTotalCurrentBudget.Text = "";
						fldDivisionTotalInitialRequest.Text = "";
						fldDivisionTotalManagerRequest.Text = "";
						fldDivisionTotalCommitteeRequest.Text = "";
						fldDivisionTotalElectedRequest.Text = "";
						fldDivisionTotalApprovedAmount.Text = "";
					}
					curDepartmentCurrentBudgetTotal = 0;
					curDepartmentInitialRequestTotal = 0;
					curDepartmentManagerRequestTotal = 0;
					curDepartmentCommitteeRequestTotal = 0;
					curDepartmentElectedRequestTotal = 0;
					curDepartmentApprovedAmountTotal = 0;
					curDivisionCurrentBudgetTotal = 0;
					curDivisionInitialRequestTotal = 0;
					curDivisionManagerRequestTotal = 0;
					curDivisionCommitteeRequestTotal = 0;
					curDivisionElectedRequestTotal = 0;
					curDivisionApprovedAmountTotal = 0;
					if (DeptBreakFlag || DivBreakFlag)
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
					}
					else
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
					}
				}
				else
				{
					fldDepartmentName.Text = "";
					fldDepartmentTotalCurrentBudget.Text = "";
					fldDepartmentTotalInitialRequest.Text = "";
					fldDepartmentTotalManagerRequest.Text = "";
					fldDepartmentTotalCommitteeRequest.Text = "";
					fldDepartmentTotalElectedRequest.Text = "";
					fldDepartmentTotalApprovedAmount.Text = "";
					if (blnDivisionTotals)
					{
						fldDivisionName.Text = strDivFooterTitle;
						fldDivisionTotalCurrentBudget.Text = Strings.Format(curDivisionCurrentBudgetTotal, "#,##0.00");
						fldDivisionTotalInitialRequest.Text = Strings.Format(curDivisionInitialRequestTotal, "#,##0.00");
						fldDivisionTotalManagerRequest.Text = Strings.Format(curDivisionManagerRequestTotal, "#,##0.00");
						fldDivisionTotalCommitteeRequest.Text = Strings.Format(curDivisionCommitteeRequestTotal, "#,##0.00");
						fldDivisionTotalElectedRequest.Text = Strings.Format(curDivisionElectedRequestTotal, "#,##0.00");
						fldDivisionTotalApprovedAmount.Text = Strings.Format(curDivisionApprovedAmountTotal, "#,##0.00");
					}
					else
					{
						fldDivisionName.Text = "";
						fldDivisionTotalCurrentBudget.Text = "";
						fldDivisionTotalInitialRequest.Text = "";
						fldDivisionTotalManagerRequest.Text = "";
						fldDivisionTotalCommitteeRequest.Text = "";
						fldDivisionTotalElectedRequest.Text = "";
						fldDivisionTotalApprovedAmount.Text = "";
					}
					curDivisionCurrentBudgetTotal = 0;
					curDivisionInitialRequestTotal = 0;
					curDivisionManagerRequestTotal = 0;
					curDivisionCommitteeRequestTotal = 0;
					curDivisionElectedRequestTotal = 0;
					curDivisionApprovedAmountTotal = 0;
					if (DivBreakFlag)
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
					}
					else
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
					}
				}
			}
			// If strDept <> "" Then
			// rsDepartmentInfo.MovePrevious
			// If Not rsDepartmentInfo.BeginningOfFile Then
			// If rsDepartmentInfo.Fields["Department"] = strDept Then
			// blnShowFinalTotals = True
			// Else
			// blnShowFinalTotals = False
			// End If
			// End If
			// rsDepartmentInfo.MoveNext
			// End If
			// If RevDivFlag Then
			// rsDepartmentInfo.MoveNext
			// If rsDepartmentInfo.EndOfFile <> True Or Not blnShowFinalTotals Then
			Line1.Visible = false;
			fldTotalName.Text = "";
			ClearTotals();
			// Else
			// Line1.Visible = True
			// fldTotalName = "Revenue Totals:"
			// FillTotals
			// End If
			// rsDepartmentInfo.MovePrevious
			// Else
			// rsDepartmentInfo.MoveNext
			// If rsDepartmentInfo.EndOfFile <> True Or rsDivisionInfo.EndOfFile <> True Or Not blnShowFinalTotals Then
			// Line1.Visible = False
			// fldTotalName = ""
			// ClearTotals
			// Else
			// 
			// End If
			// rsDepartmentInfo.MovePrevious
			// End If
			// strDept = rsDepartmentInfo.Fields["Department"]
			if (rsDepartmentInfo.EndOfFile())
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			else
			{
				rsDepartmentInfo.MoveNext();
				if (rsDepartmentInfo.EndOfFile())
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				rsDepartmentInfo.MovePrevious();
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			Line1.Visible = true;
			fldTotalName.Text = "Revenue Totals:";
			FillTotals();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			
		}
		// vbPorter upgrade warning: lngBudgetYear As int	OnWrite(string, int)
		// vbPorter upgrade warning: lngCurrentYear As int	OnWrite(string, int)
		// vbPorter upgrade warning: intPageToStartOn As short	OnWriteFCConvert.ToInt32(
		public void Init(bool blnOnlyShowChangedAmounts, bool blnIncludeBudAdj, string strTitle, string strTypeOfAccounts, string strBudgetType, bool blnCurrent, int lngBudgetYear, int lngCurrentYear, bool blnCom, bool blnDeptTotal, bool blnDivTotal, bool blnDeptBreak, bool blnDivBreak, string strInfo, string strFirstDept = "", string strSecondDept = "", string strDiv = "", short intPageToStartOn = 0)
		{
			strReportTitle = strTitle;
			strReport = strBudgetType;
			blnCurrentYearBudget = blnCurrent;
			lngCurrent = lngCurrentYear;
			lngBudget = lngBudgetYear;
			blnComments = blnCom;
			blnDepartmentTotals = blnDeptTotal;
			blnDivisionTotals = blnDivTotal;
			DeptBreakFlag = blnDeptBreak;
			DivBreakFlag = blnDivBreak;
			PageCounter = intPageToStartOn;
			strAccountType = strTypeOfAccounts;
			strReportTitle = strTitle;
			blnIncBudAdj = blnIncludeBudAdj;
			blnOnlyChangedAmounts = blnOnlyShowChangedAmounts;
			if (strInfo == "A")
			{
				strInfoType = "A";
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else if (strInfo == "R")
			{
				strInfoType = "R";
				strLowDepartment = strFirstDept;
				strHighDepartment = strSecondDept;
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department >= '" + strLowDepartment + "' AND Department <= '" + strHighDepartment + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else if (strInfo == "D")
			{
				strInfoType = "D";
				strSingleDepartment = strFirstDept;
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strSingleDepartment + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				strInfoType = "V";
				strSingleDepartment = strFirstDept;
				strSingleDivision = strDiv;
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strSingleDepartment + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			rsDepartmentInfo.MoveLast();
			rsDepartmentInfo.MoveFirst();
			blnFirstRecord = true;
			if (strAccountType == "T")
			{
				this.Run(false);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private bool BudgetAccountExists(string strAccount)
		{
			bool BudgetAccountExists = false;
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                rsTemp.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + strAccount + "'");
                if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                {
                    BudgetAccountExists = true;
                }
                else
                {
                    BudgetAccountExists = false;
                }
            }

            return BudgetAccountExists;
		}

		private void ClearTotals()
		{
			fldTotalCurrentBudget.Text = "";
			fldTotalInitialRequest.Text = "";
			fldTotalManagerRequest.Text = "";
			fldTotalCommitteeRequest.Text = "";
			fldTotalElectedRequest.Text = "";
			fldTotalApprovedAmount.Text = "";
		}

		private void FillTotals()
		{
			fldTotalCurrentBudget.Text = Strings.Format(curCurrentBudgetTotal, "#,##0.00");
			fldTotalInitialRequest.Text = Strings.Format(curInitialRequestTotal, "#,##0.00");
			fldTotalManagerRequest.Text = Strings.Format(curManagerRequestTotal, "#,##0.00");
			fldTotalCommitteeRequest.Text = Strings.Format(curCommitteeRequestTotal, "#,##0.00");
			fldTotalElectedRequest.Text = Strings.Format(curElectedRequestTotal, "#,##0.00");
			fldTotalApprovedAmount.Text = Strings.Format(curApprovedAmountTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			int HighDateCurrent;
			int LowDateCurrent;
			string strPeriodCheck = "";
			string strPeriodCheckCurrent = "";
			string strTable;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			strTable = "RevenueReportInfo";
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
		}

		private double GetNetBudget_2(string strAcct)
		{
			return GetNetBudget(ref strAcct);
		}

		private double GetNetBudget(ref string strAcct)
		{
			double GetNetBudget = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
				GetNetBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
			}
			else
			{
				GetNetBudget = 0;
			}
			return GetNetBudget;
		}

		private double GetOriginalBudget_2(string strAcct)
		{
			return GetOriginalBudget(ref strAcct);
		}

		private double GetOriginalBudget(ref string strAcct)
		{
			double GetOriginalBudget = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
				GetOriginalBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal"));
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		
	}
}
