﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cRevenueDetailItem
	{
		//=========================================================
		private string strAccount = string.Empty;
		private string strFund = string.Empty;
		private string strDepartment = string.Empty;
		private string strDivision = string.Empty;
		private string strDescription = string.Empty;
		private string strJournalDate = string.Empty;
		private string strJournalType = string.Empty;
		private int intJournalNumber;
		// Private strCheckNumber As String
		private string strVendor = string.Empty;
		private double dblCredit;
		private double dblDebit;
		private double dblCurrentBudget;
		private double dblUncollectedBalance;
		private string strWarrant = string.Empty;
		private int intPeriod;
		private string strRCB = string.Empty;
		private string strTransactionDate = string.Empty;
		private string strRevenue = string.Empty;
		private int lngCheckNumber;

		public string Vendor
		{
			set
			{
				strVendor = value;
			}
			get
			{
				string Vendor = "";
				Vendor = strVendor;
				return Vendor;
			}
		}

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}

		public string Revenue
		{
			set
			{
				strRevenue = value;
			}
			get
			{
				string Revenue = "";
				Revenue = strRevenue;
				return Revenue;
			}
		}

		public string TransactionDate
		{
			set
			{
				strTransactionDate = value;
			}
			get
			{
				string TransactionDate = "";
				TransactionDate = strTransactionDate;
				return TransactionDate;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public int Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int Period = 0;
				Period = intPeriod;
				return Period;
			}
		}

		public string Warrant
		{
			set
			{
				strWarrant = value;
			}
			get
			{
				string Warrant = "";
				Warrant = strWarrant;
				return Warrant;
			}
		}

		public double UncollectedBalance
		{
			set
			{
				dblUncollectedBalance = value;
			}
			get
			{
				double UncollectedBalance = 0;
				UncollectedBalance = dblUncollectedBalance;
				return UncollectedBalance;
			}
		}

		public double CurrentBudget
		{
			set
			{
				dblCurrentBudget = value;
			}
			get
			{
				double CurrentBudget = 0;
				CurrentBudget = dblCurrentBudget;
				return CurrentBudget;
			}
		}

		public double Credit
		{
			set
			{
				dblCredit = value;
			}
			get
			{
				double Credit = 0;
				Credit = dblCredit;
				return Credit;
			}
		}

		public double Debit
		{
			set
			{
				dblDebit = value;
			}
			get
			{
				double Debit = 0;
				Debit = dblDebit;
				return Debit;
			}
		}

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string JournalDate
		{
			set
			{
				strJournalDate = value;
			}
			get
			{
				string JournalDate = "";
				JournalDate = strJournalDate;
				return JournalDate;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public int JournalNumber
		{
			set
			{
				intJournalNumber = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int JournalNumber = 0;
				JournalNumber = intJournalNumber;
				return JournalNumber;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string Division
		{
			set
			{
				strDivision = value;
			}
			get
			{
				string Division = "";
				Division = strDivision;
				return Division;
			}
		}
	}
}
