﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerControl.
	/// </summary>
	partial class frmLedgerControl : BaseForm
	{
		public fecherFoundation.FCTabControl tabStandardAccounts;
		public fecherFoundation.FCTabPage tabStandardAccounts_Page1;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCTabPage tabStandardAccounts_Page2;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLedgerControl));
			this.tabStandardAccounts = new fecherFoundation.FCTabControl();
			this.tabStandardAccounts_Page1 = new fecherFoundation.FCTabPage();
			this.vs1 = new fecherFoundation.FCGrid();
			this.tabStandardAccounts_Page2 = new fecherFoundation.FCTabPage();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.tabStandardAccounts.SuspendLayout();
			this.tabStandardAccounts_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.tabStandardAccounts);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(508, 30);
			this.HeaderText.Text = "Default Accounts";
			// 
			// tabStandardAccounts
			// 
			this.tabStandardAccounts.Controls.Add(this.tabStandardAccounts_Page1);
			this.tabStandardAccounts.Controls.Add(this.tabStandardAccounts_Page2);
			this.tabStandardAccounts.Location = new System.Drawing.Point(30, 20);
			this.tabStandardAccounts.Name = "tabStandardAccounts";
			this.tabStandardAccounts.PageInsets = new Wisej.Web.Padding(1, 50, 1, 1);
			this.tabStandardAccounts.ShowFocusRect = false;
			this.tabStandardAccounts.Size = new System.Drawing.Size(1018, 468);
			this.tabStandardAccounts.TabIndex = 0;
			this.tabStandardAccounts.TabsPerRow = 0;
			this.tabStandardAccounts.Text = "Town";
			this.tabStandardAccounts.WordWrap = false;
			// 
			// tabStandardAccounts_Page1
			// 
			this.tabStandardAccounts_Page1.Controls.Add(this.vs1);
			this.tabStandardAccounts_Page1.Location = new System.Drawing.Point(1, 50);
			this.tabStandardAccounts_Page1.Name = "tabStandardAccounts_Page1";
			this.tabStandardAccounts_Page1.Text = "Town";
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.Enabled = false;
			this.vs1.ExtendLastCol = true;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(0, 0);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 36;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1016, 414);
			this.vs1.StandardTab = true;
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vs1.TabIndex = 1;
			this.vs1.Visible = false;
			//FC:FINAL:MSH - Issue #745: KeyDownEdit doesn't work
			//this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			//this.vs1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
			//this.vs1.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyUpEdit);
			this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_BeforeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ClickEvent);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			// 
			// tabStandardAccounts_Page2
			// 
			this.tabStandardAccounts_Page2.Location = new System.Drawing.Point(1, 50);
			this.tabStandardAccounts_Page2.Name = "tabStandardAccounts_Page2";
			this.tabStandardAccounts_Page2.Text = "School";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(489, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmLedgerControl
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLedgerControl";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Default Accounts";
			this.Load += new System.EventHandler(this.frmLedgerControl_Load);
			this.Activated += new System.EventHandler(this.frmLedgerControl_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLedgerControl_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLedgerControl_KeyPress);
			this.Resize += new System.EventHandler(this.frmLedgerControl_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.tabStandardAccounts.ResumeLayout(false);
			this.tabStandardAccounts_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
