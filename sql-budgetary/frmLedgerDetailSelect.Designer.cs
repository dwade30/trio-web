﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerDetailSelect.
	/// </summary>
	partial class frmLedgerDetailSelect : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraRangeSelection;
		public fecherFoundation.FCButton cmdCancelRange;
		public fecherFoundation.FCFrame fraAccountRange;
		public fecherFoundation.FCComboBox cboSingleFund;
		public fecherFoundation.FCComboBox cboEndingFund;
		public fecherFoundation.FCComboBox cboBeginningFund;
		public FCGrid vsLowAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraReportSelection;
		public fecherFoundation.FCCheckBox chkShowReportTitle;
		public fecherFoundation.FCButton cmdCreateFormat;
		public fecherFoundation.FCButton cmdEditFormat;
		public fecherFoundation.FCButton cmdCreateCriteria;
		public fecherFoundation.FCButton cmdEditCriteria;
		public fecherFoundation.FCButton cmdProcessSave;
		public fecherFoundation.FCTextBox txtReportTitle;
		public fecherFoundation.FCButton cmdCancelSelection;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCComboBox cboFormat;
		public fecherFoundation.FCComboBox cboCriteria;
		public fecherFoundation.FCLabel lblReportTitle;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCLabel lblCriteria;
		public fecherFoundation.FCCheckBox chkDefault;
		public fecherFoundation.FCComboBox cboReports;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCLabel lblInstructions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLedgerDetailSelect));
            this.fraRangeSelection = new fecherFoundation.FCFrame();
            this.cmdCancelRange = new fecherFoundation.FCButton();
            this.fraAccountRange = new fecherFoundation.FCFrame();
            this.cboSingleFund = new fecherFoundation.FCComboBox();
            this.cboEndingFund = new fecherFoundation.FCComboBox();
            this.cboBeginningFund = new fecherFoundation.FCComboBox();
            this.vsLowAccount = new fecherFoundation.FCGrid();
            this.vsHighAccount = new fecherFoundation.FCGrid();
            this.lblTo_2 = new fecherFoundation.FCLabel();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.cboEndingMonth = new fecherFoundation.FCComboBox();
            this.cboBeginningMonth = new fecherFoundation.FCComboBox();
            this.cboSingleMonth = new fecherFoundation.FCComboBox();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.fraReportSelection = new fecherFoundation.FCFrame();
            this.chkShowReportTitle = new fecherFoundation.FCCheckBox();
            this.cmdCreateFormat = new fecherFoundation.FCButton();
            this.cmdEditFormat = new fecherFoundation.FCButton();
            this.cmdCreateCriteria = new fecherFoundation.FCButton();
            this.cmdEditCriteria = new fecherFoundation.FCButton();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.txtReportTitle = new fecherFoundation.FCTextBox();
            this.cmdCancelSelection = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cboFormat = new fecherFoundation.FCComboBox();
            this.cboCriteria = new fecherFoundation.FCComboBox();
            this.lblReportTitle = new fecherFoundation.FCLabel();
            this.lblFormat = new fecherFoundation.FCLabel();
            this.lblCriteria = new fecherFoundation.FCLabel();
            this.chkDefault = new fecherFoundation.FCCheckBox();
            this.cboReports = new fecherFoundation.FCComboBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRangeSelection)).BeginInit();
            this.fraRangeSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).BeginInit();
            this.fraAccountRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSelection)).BeginInit();
            this.fraReportSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateFormat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditFormat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 538);
            this.BottomPanel.Size = new System.Drawing.Size(606, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.fraReportSelection);
            this.ClientArea.Controls.Add(this.fraRangeSelection);
            this.ClientArea.Controls.Add(this.chkDefault);
            this.ClientArea.Controls.Add(this.cboReports);
            this.ClientArea.Controls.Add(this.lblInstructions);
            this.ClientArea.Size = new System.Drawing.Size(626, 628);
            this.ClientArea.Controls.SetChildIndex(this.lblInstructions, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboReports, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkDefault, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRangeSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraReportSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdProcess, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Size = new System.Drawing.Size(626, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(225, 28);
            this.HeaderText.Text = "Ledger Detail Report";
            // 
            // fraRangeSelection
            // 
            this.fraRangeSelection.AppearanceKey = "groupBoxLeftBorder";
            this.fraRangeSelection.Controls.Add(this.cmdCancelRange);
            this.fraRangeSelection.Controls.Add(this.fraAccountRange);
            this.fraRangeSelection.Controls.Add(this.fraDateRange);
            this.fraRangeSelection.Location = new System.Drawing.Point(30, 56);
            this.fraRangeSelection.Name = "fraRangeSelection";
            this.fraRangeSelection.Size = new System.Drawing.Size(570, 310);
            this.fraRangeSelection.TabIndex = 20;
            this.fraRangeSelection.Text = "Criteria Selection";
            this.fraRangeSelection.Visible = false;
            // 
            // cmdCancelRange
            // 
            this.cmdCancelRange.AppearanceKey = "actionButton";
            this.cmdCancelRange.Location = new System.Drawing.Point(20, 250);
            this.cmdCancelRange.Name = "cmdCancelRange";
            this.cmdCancelRange.Size = new System.Drawing.Size(90, 40);
            this.cmdCancelRange.TabIndex = 29;
            this.cmdCancelRange.Text = "Cancel";
            this.cmdCancelRange.Click += new System.EventHandler(this.cmdCancelRange_Click);
            // 
            // fraAccountRange
            // 
            this.fraAccountRange.Controls.Add(this.cboSingleFund);
            this.fraAccountRange.Controls.Add(this.cboEndingFund);
            this.fraAccountRange.Controls.Add(this.cboBeginningFund);
            this.fraAccountRange.Controls.Add(this.vsLowAccount);
            this.fraAccountRange.Controls.Add(this.vsHighAccount);
            this.fraAccountRange.Controls.Add(this.lblTo_2);
            this.fraAccountRange.Location = new System.Drawing.Point(20, 140);
            this.fraAccountRange.Name = "fraAccountRange";
            this.fraAccountRange.Size = new System.Drawing.Size(480, 90);
            this.fraAccountRange.TabIndex = 26;
            this.fraAccountRange.Text = "Account Range";
            // 
            // cboSingleFund
            // 
            this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleFund.Location = new System.Drawing.Point(20, 30);
            this.cboSingleFund.Name = "cboSingleFund";
            this.cboSingleFund.Size = new System.Drawing.Size(440, 40);
            this.cboSingleFund.TabIndex = 32;
            this.cboSingleFund.Visible = false;
            this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
            // 
            // cboEndingFund
            // 
            this.cboEndingFund.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingFund.Location = new System.Drawing.Point(270, 30);
            this.cboEndingFund.Name = "cboEndingFund";
            this.cboEndingFund.Size = new System.Drawing.Size(190, 40);
            this.cboEndingFund.TabIndex = 31;
            this.cboEndingFund.Visible = false;
            this.cboEndingFund.DropDown += new System.EventHandler(this.cboEndingFund_DropDown);
            // 
            // cboBeginningFund
            // 
            this.cboBeginningFund.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningFund.Location = new System.Drawing.Point(20, 30);
            this.cboBeginningFund.Name = "cboBeginningFund";
            this.cboBeginningFund.Size = new System.Drawing.Size(190, 40);
            this.cboBeginningFund.TabIndex = 30;
            this.cboBeginningFund.Visible = false;
            this.cboBeginningFund.DropDown += new System.EventHandler(this.cboBeginningFund_DropDown);
            // 
            // vsLowAccount
            // 
            this.vsLowAccount.Cols = 1;
            this.vsLowAccount.ColumnHeadersVisible = false;
            this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLowAccount.FixedCols = 0;
            this.vsLowAccount.FixedRows = 0;
            this.vsLowAccount.Location = new System.Drawing.Point(20, 30);
            this.vsLowAccount.Name = "vsLowAccount";
            this.vsLowAccount.ReadOnly = false;
            this.vsLowAccount.RowHeadersVisible = false;
            this.vsLowAccount.Rows = 1;
            this.vsLowAccount.Size = new System.Drawing.Size(190, 42);
            this.vsLowAccount.TabIndex = 33;
            this.vsLowAccount.Visible = false;
            // 
            // vsHighAccount
            // 
            this.vsHighAccount.Cols = 1;
            this.vsHighAccount.ColumnHeadersVisible = false;
            this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsHighAccount.FixedCols = 0;
            this.vsHighAccount.FixedRows = 0;
            this.vsHighAccount.Location = new System.Drawing.Point(270, 30);
            this.vsHighAccount.Name = "vsHighAccount";
            this.vsHighAccount.ReadOnly = false;
            this.vsHighAccount.RowHeadersVisible = false;
            this.vsHighAccount.Rows = 1;
            this.vsHighAccount.Size = new System.Drawing.Size(190, 42);
            this.vsHighAccount.TabIndex = 34;
            this.vsHighAccount.Visible = false;
            // 
            // lblTo_2
            // 
            this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_2.Location = new System.Drawing.Point(230, 44);
            this.lblTo_2.Name = "lblTo_2";
            this.lblTo_2.Size = new System.Drawing.Size(20, 16);
            this.lblTo_2.TabIndex = 27;
            this.lblTo_2.Text = "TO";
            this.lblTo_2.Visible = false;
            // 
            // fraDateRange
            // 
            this.fraDateRange.Controls.Add(this.cboEndingMonth);
            this.fraDateRange.Controls.Add(this.cboBeginningMonth);
            this.fraDateRange.Controls.Add(this.cboSingleMonth);
            this.fraDateRange.Controls.Add(this.lblTo_0);
            this.fraDateRange.Location = new System.Drawing.Point(20, 30);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(480, 90);
            this.fraDateRange.TabIndex = 21;
            this.fraDateRange.Text = "Date Range";
            // 
            // cboEndingMonth
            // 
            this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndingMonth.Location = new System.Drawing.Point(270, 30);
            this.cboEndingMonth.Name = "cboEndingMonth";
            this.cboEndingMonth.Size = new System.Drawing.Size(190, 40);
            this.cboEndingMonth.TabIndex = 24;
            this.cboEndingMonth.Visible = false;
            // 
            // cboBeginningMonth
            // 
            this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboBeginningMonth.Location = new System.Drawing.Point(20, 30);
            this.cboBeginningMonth.Name = "cboBeginningMonth";
            this.cboBeginningMonth.Size = new System.Drawing.Size(190, 40);
            this.cboBeginningMonth.TabIndex = 23;
            this.cboBeginningMonth.Visible = false;
            // 
            // cboSingleMonth
            // 
            this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboSingleMonth.Location = new System.Drawing.Point(20, 30);
            this.cboSingleMonth.Name = "cboSingleMonth";
            this.cboSingleMonth.Size = new System.Drawing.Size(190, 40);
            this.cboSingleMonth.TabIndex = 22;
            this.cboSingleMonth.Visible = false;
            // 
            // lblTo_0
            // 
            this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_0.Location = new System.Drawing.Point(230, 44);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(20, 16);
            this.lblTo_0.TabIndex = 25;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.Visible = false;
            // 
            // fraReportSelection
            // 
            this.fraReportSelection.Controls.Add(this.chkShowReportTitle);
            this.fraReportSelection.Controls.Add(this.cmdCreateFormat);
            this.fraReportSelection.Controls.Add(this.cmdEditFormat);
            this.fraReportSelection.Controls.Add(this.cmdCreateCriteria);
            this.fraReportSelection.Controls.Add(this.cmdEditCriteria);
            this.fraReportSelection.Controls.Add(this.cmdProcessSave);
            this.fraReportSelection.Controls.Add(this.txtReportTitle);
            this.fraReportSelection.Controls.Add(this.cmdCancelSelection);
            this.fraReportSelection.Controls.Add(this.cmdSave);
            this.fraReportSelection.Controls.Add(this.cboFormat);
            this.fraReportSelection.Controls.Add(this.cboCriteria);
            this.fraReportSelection.Controls.Add(this.lblReportTitle);
            this.fraReportSelection.Controls.Add(this.lblFormat);
            this.fraReportSelection.Controls.Add(this.lblCriteria);
            this.fraReportSelection.Location = new System.Drawing.Point(30, 56);
            this.fraReportSelection.Name = "fraReportSelection";
            this.fraReportSelection.Size = new System.Drawing.Size(570, 416);
            this.fraReportSelection.TabIndex = 16;
            this.fraReportSelection.Text = "Report Selections";
            this.fraReportSelection.Visible = false;
            // 
            // chkShowReportTitle
            // 
            this.chkShowReportTitle.Location = new System.Drawing.Point(177, 80);
            this.chkShowReportTitle.Name = "chkShowReportTitle";
            this.chkShowReportTitle.Size = new System.Drawing.Size(135, 22);
            this.chkShowReportTitle.TabIndex = 35;
            this.chkShowReportTitle.Text = "Show Report Title";
            // 
            // cmdCreateFormat
            // 
            this.cmdCreateFormat.AppearanceKey = "actionButton";
            this.cmdCreateFormat.Location = new System.Drawing.Point(177, 296);
            this.cmdCreateFormat.Name = "cmdCreateFormat";
            this.cmdCreateFormat.Size = new System.Drawing.Size(125, 40);
            this.cmdCreateFormat.TabIndex = 8;
            this.cmdCreateFormat.Text = "Create New";
            this.cmdCreateFormat.Click += new System.EventHandler(this.cmdCreateFormat_Click);
            // 
            // cmdEditFormat
            // 
            this.cmdEditFormat.AppearanceKey = "actionButton";
            this.cmdEditFormat.Location = new System.Drawing.Point(322, 296);
            this.cmdEditFormat.Name = "cmdEditFormat";
            this.cmdEditFormat.Size = new System.Drawing.Size(75, 40);
            this.cmdEditFormat.TabIndex = 9;
            this.cmdEditFormat.Text = "Edit";
            this.cmdEditFormat.Click += new System.EventHandler(this.cmdEditFormat_Click);
            // 
            // cmdCreateCriteria
            // 
            this.cmdCreateCriteria.AppearanceKey = "actionButton";
            this.cmdCreateCriteria.Location = new System.Drawing.Point(177, 176);
            this.cmdCreateCriteria.Name = "cmdCreateCriteria";
            this.cmdCreateCriteria.Size = new System.Drawing.Size(125, 40);
            this.cmdCreateCriteria.TabIndex = 5;
            this.cmdCreateCriteria.Text = "Create New";
            this.cmdCreateCriteria.Click += new System.EventHandler(this.cmdCreateCriteria_Click);
            // 
            // cmdEditCriteria
            // 
            this.cmdEditCriteria.AppearanceKey = "actionButton";
            this.cmdEditCriteria.Location = new System.Drawing.Point(322, 176);
            this.cmdEditCriteria.Name = "cmdEditCriteria";
            this.cmdEditCriteria.Size = new System.Drawing.Size(75, 40);
            this.cmdEditCriteria.TabIndex = 6;
            this.cmdEditCriteria.Text = "Edit";
            this.cmdEditCriteria.Click += new System.EventHandler(this.cmdEditCriteria_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "actionButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(160, 356);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Size = new System.Drawing.Size(150, 40);
            this.cmdProcessSave.TabIndex = 11;
            this.cmdProcessSave.Text = "Save & Process";
            this.cmdProcessSave.Click += new System.EventHandler(this.cmdProcessSave_Click);
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtReportTitle.Location = new System.Drawing.Point(177, 30);
            this.txtReportTitle.MaxLength = 50;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Size = new System.Drawing.Size(373, 40);
            this.txtReportTitle.TabIndex = 3;
            // 
            // cmdCancelSelection
            // 
            this.cmdCancelSelection.AppearanceKey = "actionButton";
            this.cmdCancelSelection.Location = new System.Drawing.Point(330, 356);
            this.cmdCancelSelection.Name = "cmdCancelSelection";
            this.cmdCancelSelection.Size = new System.Drawing.Size(90, 40);
            this.cmdCancelSelection.TabIndex = 13;
            this.cmdCancelSelection.Text = "Cancel";
            this.cmdCancelSelection.Click += new System.EventHandler(this.cmdCancelSelection_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "actionButton";
            this.cmdSave.Location = new System.Drawing.Point(20, 356);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(120, 40);
            this.cmdSave.TabIndex = 10;
            this.cmdSave.Text = "Save Report";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cboFormat
            // 
            this.cboFormat.BackColor = System.Drawing.SystemColors.Window;
            this.cboFormat.Location = new System.Drawing.Point(177, 236);
            this.cboFormat.Name = "cboFormat";
            this.cboFormat.Size = new System.Drawing.Size(373, 40);
            this.cboFormat.TabIndex = 7;
            // 
            // cboCriteria
            // 
            this.cboCriteria.BackColor = System.Drawing.SystemColors.Window;
            this.cboCriteria.Location = new System.Drawing.Point(177, 116);
            this.cboCriteria.Name = "cboCriteria";
            this.cboCriteria.Size = new System.Drawing.Size(373, 40);
            this.cboCriteria.TabIndex = 4;
            // 
            // lblReportTitle
            // 
            this.lblReportTitle.Location = new System.Drawing.Point(20, 44);
            this.lblReportTitle.Name = "lblReportTitle";
            this.lblReportTitle.Size = new System.Drawing.Size(85, 16);
            this.lblReportTitle.TabIndex = 19;
            this.lblReportTitle.Text = "REPORT TITLE";
            // 
            // lblFormat
            // 
            this.lblFormat.Location = new System.Drawing.Point(20, 250);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(102, 16);
            this.lblFormat.TabIndex = 18;
            this.lblFormat.Text = "REPORT FORMAT";
            // 
            // lblCriteria
            // 
            this.lblCriteria.Location = new System.Drawing.Point(20, 130);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(122, 16);
            this.lblCriteria.TabIndex = 17;
            this.lblCriteria.Text = "SELECTION CRITERIA";
            // 
            // chkDefault
            // 
            this.chkDefault.Location = new System.Drawing.Point(30, 66);
            this.chkDefault.Name = "chkDefault";
            this.chkDefault.Size = new System.Drawing.Size(190, 22);
            this.chkDefault.TabIndex = 15;
            this.chkDefault.Text = "Make this the default report";
            this.chkDefault.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
            // 
            // cboReports
            // 
            this.cboReports.BackColor = System.Drawing.SystemColors.Window;
            this.cboReports.Location = new System.Drawing.Point(30, 112);
            this.cboReports.Name = "cboReports";
            this.cboReports.Size = new System.Drawing.Size(449, 40);
            this.cboReports.TabIndex = 0;
            this.cboReports.SelectedIndexChanged += new System.EventHandler(this.cboReports_SelectedIndexChanged);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 490);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(110, 48);
            this.cmdProcess.TabIndex = 1;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(410, 16);
            this.lblInstructions.TabIndex = 14;
            this.lblInstructions.Text = "PLEASE SELECT THE REPORT YOU WISH TO PRINT AND CLICK PROCESS";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(542, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(54, 24);
            this.cmdDelete.TabIndex = 29;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // frmLedgerDetailSelect
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(626, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmLedgerDetailSelect";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Ledger Detail Report";
            this.Load += new System.EventHandler(this.frmLedgerDetailSelect_Load);
            this.Activated += new System.EventHandler(this.frmLedgerDetailSelect_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLedgerDetailSelect_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLedgerDetailSelect_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRangeSelection)).EndInit();
            this.fraRangeSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).EndInit();
            this.fraAccountRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSelection)).EndInit();
            this.fraReportSelection.ResumeLayout(false);
            this.fraReportSelection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateFormat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditFormat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdDelete;
	}
}
