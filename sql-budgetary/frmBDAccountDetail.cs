﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBDAccountDetail.
	/// </summary>
	public partial class frmBDAccountDetail : BaseForm
	{
		public frmBDAccountDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			//FC:FINAL:ASZ: Begin adding code from Load
			boolUpdating = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridDetails();
			FillPeriodCombos();
			if (theView == null)
			{
				theView = new cBDAccountDetailView();
			}
			cmbPeriodStart.SelectedIndex = theView.StartPeriod - 1;
			cmbPeriodEnd.SelectedIndex = theView.EndPeriod - 1;
			boolUpdating = false;
			//FC:FINAL:ASZ: End adding code from Load
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBDAccountDetail InstancePtr
		{
			get
			{
				return (frmBDAccountDetail)Sys.GetInstance(typeof(frmBDAccountDetail));
			}
		}

		protected frmBDAccountDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private cBDAccountDetailView theView;
		const int GridDetailColPeriod = 0;
		const int GridDetailColJournal = 1;
		const int GridDetailColCheck = 4;
		const int GridDetailColDate = 3;
		const int GridDetailColJournalType = 2;
		const int GridDetailColDescription = 5;
		const int GridDetailColDebits = 6;
		const int GridDetailColCredits = 7;
		const int GridDetailColParentID = 8;
		const int GridDetailColTypeOfJournal = 9;
		private bool boolUpdating;

		public cBDAccountDetailView DataContext
		{
			set
			{
				theView = value;
			}
		}

		private void cmbPeriodEnd_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				if (theView.EndPeriod != cmbPeriodEnd.SelectedIndex + 1)
				{
					theView.EndPeriod = FCConvert.ToInt16(cmbPeriodEnd.SelectedIndex + 1);
					theView.FilterDetails();
				}
			}
		}

		private void cmbPeriodStart_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				if (theView.StartPeriod != cmbPeriodStart.SelectedIndex + 1)
				{
					theView.StartPeriod = FCConvert.ToInt16(cmbPeriodStart.SelectedIndex + 1);
					theView.FilterDetails();
				}
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			PrintAccountDetail();
		}

		private void PrintAccountDetail()
		{
			theView.PrintAccountDetail();
		}

		private void frmBDAccountDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBDAccountDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBDAccountDetail.FillStyle	= 0;
			//frmBDAccountDetail.ScaleWidth	= 9300;
			//frmBDAccountDetail.ScaleHeight	= 7905;
			//frmBDAccountDetail.LinkTopic	= "Form2";
			//frmBDAccountDetail.LockControls	= -1  'True;
			//frmBDAccountDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//FC:FINAL:ASZ: Begin added code in constructor
			//boolUpdating = true;
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
			//SetupGridDetails();
			//FillPeriodCombos();
			//if (theView == null)
			//{
			//    theView = new cBDAccountDetailView();
			//}
			//cmbPeriodStart.SelectedIndex = theView.StartPeriod - 1;
			//cmbPeriodEnd.SelectedIndex = theView.EndPeriod - 1;
			//boolUpdating = false;
			//FC:FINAL:ASZ: End added code in constructor
		}

		private void SetupGridDetails()
		{
			GridDetails.ColHidden(GridDetailColTypeOfJournal, true);
			GridDetails.ColHidden(GridDetailColParentID, true);
			GridDetails.TextMatrix(0, GridDetailColPeriod, "Per");
			GridDetails.TextMatrix(0, GridDetailColJournal, "Jrnl");
			GridDetails.TextMatrix(0, GridDetailColCheck, "Check");
			GridDetails.TextMatrix(0, GridDetailColDate, "Date");
			GridDetails.TextMatrix(0, GridDetailColJournalType, "Journal Type");
			GridDetails.TextMatrix(0, GridDetailColDebits, "Debits");
			GridDetails.TextMatrix(0, GridDetailColCredits, "Credits");
			GridDetails.TextMatrix(0, GridDetailColDescription, "Description");
			//FC:FINAL:DDU:#2966 - aligned columns
			GridDetails.ColAlignment(GridDetailColPeriod, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDetails.ColAlignment(GridDetailColJournal, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDetails.ColAlignment(GridDetailColCheck, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDetails.ColAlignment(GridDetailColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDetails.ColAlignment(GridDetailColJournalType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDetails.ColAlignment(GridDetailColDebits, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridDetails.ColAlignment(GridDetailColCredits, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridDetails.ColAlignment(GridDetailColDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridDetails()
		{
			int GridWidth;
			GridWidth = GridDetails.WidthOriginal;
			GridDetails.ColWidth(GridDetailColPeriod, FCConvert.ToInt32(0.04 * GridWidth));
			GridDetails.ColWidth(GridDetailColJournal, FCConvert.ToInt32(0.08 * GridWidth));
			GridDetails.ColWidth(GridDetailColCheck, FCConvert.ToInt32(0.08 * GridWidth));
			GridDetails.ColWidth(GridDetailColDate, FCConvert.ToInt32(0.1 * GridWidth));
			GridDetails.ColWidth(GridDetailColJournalType, FCConvert.ToInt32(0.2 * GridWidth));
			GridDetails.ColWidth(GridDetailColDebits, FCConvert.ToInt32(0.12 * GridWidth));
			GridDetails.ColWidth(GridDetailColCredits, FCConvert.ToInt32(0.12 * GridWidth));
			GridDetails.ColWidth(GridDetailColDescription, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void frmBDAccountDetail_Resize(object sender, System.EventArgs e)
		{
			ResizeGridDetails();
		}

		private void GridDetails_DblClick(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			if (!boolUpdating)
			{
				lngRow = GridDetails.MouseRow;
				if (lngRow > 0)
				{
					// theView.ShowDetails (GridDetails.TextMatrix(lngRow, GridAccountColAccount))
					theView.ShowJournal(FCConvert.ToInt32(GridDetails.TextMatrix(lngRow, GridDetailColParentID)), GridDetails.TextMatrix(lngRow, GridDetailColTypeOfJournal));
				}
			}
		}

		private void GridDetails_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridDetails[e.ColumnIndex, e.RowIndex];
            int lngRow;
			lngRow = GridDetails.GetFlexRowIndex(e.RowIndex);
			if (lngRow > 0)
			{
				if ((Strings.LCase(GridDetails.TextMatrix(lngRow, GridDetailColTypeOfJournal)) == "ap") || (Strings.LCase(GridDetails.TextMatrix(lngRow, GridDetailColTypeOfJournal)) == "ac"))
				{
					//ToolTip1.SetToolTip(GridDetails, "Double click to view entry details");
					cell.ToolTipText =  "Double click to view entry details";
				}
				else
				{
                    //ToolTip1.SetToolTip(GridDetails, "");
                    cell.ToolTipText = "";
				}
			}
			else
			{
                //ToolTip1.SetToolTip(GridDetails, "");
                cell.ToolTipText = "";
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(null, new System.EventArgs());
		}

		public void theView_AccountChanged()
		{
			RefreshAccount();
		}

		public void theView_DetailsChanged()
		{
			RefreshDetails();
		}

		private void RefreshAccount()
		{
			if (!(theView == null))
			{
				cBDAccount currAccount;
				currAccount = theView.currentAccount;
				if (!(currAccount == null))
				{
					lblAccount.Text = currAccount.Account;
					lblAccountDescription.Text = currAccount.Description;
				}
				else
				{
					lblAccount.Text = "";
					lblAccountDescription.Text = "";
				}
			}
		}

		private void RefreshDetails()
		{
			GridDetails.Rows = 1;
			GridDetails.Visible = false;
			int lngRow;
			if (!(theView == null))
			{
				cAccountDetailSummaryInfo currDetail;
				theView.Details.MoveFirst();
				while (theView.Details.IsCurrent())
				{
					//Application.DoEvents();
					currDetail = (cAccountDetailSummaryInfo)theView.Details.GetCurrentItem();
					if (!(currDetail == null))
					{
						if (currDetail.Include)
						{
							GridDetails.Rows += 1;
							lngRow = GridDetails.Rows - 1;
							GridDetails.TextMatrix(lngRow, GridDetailColCheck, currDetail.Check);
							GridDetails.TextMatrix(lngRow, GridDetailColCredits, Strings.Format(currDetail.Credits, "#,###,###,##0.00"));
							//FC:FINAL:MSH - Issue #624: String.Format added for getting short date from 'EntryDate'.
							DateTime tempDate;
							GridDetails.TextMatrix(lngRow, GridDetailColDate, DateTime.TryParse(currDetail.EntryDate, out tempDate) ? String.Format("{0:d}", tempDate) : currDetail.EntryDate);
							GridDetails.TextMatrix(lngRow, GridDetailColDebits, Strings.Format(currDetail.Debits, "#,###,###,##0.00"));
							GridDetails.TextMatrix(lngRow, GridDetailColDescription, currDetail.Description);
							GridDetails.TextMatrix(lngRow, GridDetailColJournal, FCConvert.ToString(currDetail.JournalNumber));
							GridDetails.TextMatrix(lngRow, GridDetailColJournalType, currDetail.JournalTypeDescription);
							GridDetails.TextMatrix(lngRow, GridDetailColPeriod, FCConvert.ToString(currDetail.Period));
							GridDetails.TextMatrix(lngRow, GridDetailColTypeOfJournal, currDetail.JournalType);
							GridDetails.TextMatrix(lngRow, GridDetailColParentID, FCConvert.ToString(currDetail.ParentID));
						}
					}
					theView.Details.MoveNext();
				}
				if (cmbPeriodStart.SelectedIndex != theView.StartPeriod - 1)
				{
					cmbPeriodStart.SelectedIndex = theView.StartPeriod - 1;
				}
				if (cmbPeriodEnd.SelectedIndex != theView.EndPeriod - 1)
				{
					cmbPeriodEnd.SelectedIndex = theView.EndPeriod - 1;
				}
			}
			GridDetails.Visible = true;
		}

		private void FillPeriodCombos()
		{
			int X;
			cmbPeriodEnd.Clear();
			cmbPeriodStart.Clear();
			string strTemp = "";
			for (X = 1; X <= 12; X++)
			{
				switch (X)
				{
					case 1:
						{
							strTemp = "January";
							break;
						}
					case 2:
						{
							strTemp = "February";
							break;
						}
					case 3:
						{
							strTemp = "March";
							break;
						}
					case 4:
						{
							strTemp = "April";
							break;
						}
					case 5:
						{
							strTemp = "May";
							break;
						}
					case 6:
						{
							strTemp = "June";
							break;
						}
					case 7:
						{
							strTemp = "July";
							break;
						}
					case 8:
						{
							strTemp = "August";
							break;
						}
					case 9:
						{
							strTemp = "September";
							break;
						}
					case 10:
						{
							strTemp = "October";
							break;
						}
					case 11:
						{
							strTemp = "November";
							break;
						}
					case 12:
						{
							strTemp = "December";
							break;
						}
				}
				//end switch
				cmbPeriodStart.AddItem(strTemp);
				cmbPeriodEnd.AddItem(strTemp);
			}
		}
	}
}
