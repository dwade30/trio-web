﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSelectYear.
	/// </summary>
	public partial class frmSelectYear : BaseForm
	{
		public frmSelectYear()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectYear InstancePtr
		{
			get
			{
				return (frmSelectYear)Sys.GetInstance(typeof(frmSelectYear));
			}
		}

		protected frmSelectYear _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         1/13/03
		// This form will be used to select the year you wish to
		// extract 1099 information for
		// ********************************************************
		private struct VendorTax
		{
			// vbPorter upgrade warning: Code1 As Decimal	OnWrite(short, Decimal)
			public Decimal Code1;
			// vbPorter upgrade warning: Code2 As Decimal	OnWrite(short, Decimal)
			public Decimal Code2;
			// vbPorter upgrade warning: Code3 As Decimal	OnWrite(short, Decimal)
			public Decimal Code3;
			// vbPorter upgrade warning: Code4 As Decimal	OnWrite(short, Decimal)
			public Decimal Code4;
			// vbPorter upgrade warning: Code5 As Decimal	OnWrite(short, Decimal)
			public Decimal Code5;
			// vbPorter upgrade warning: Code6 As Decimal	OnWrite(short, Decimal)
			public Decimal Code6;
			// vbPorter upgrade warning: Code7 As Decimal	OnWrite(short, Decimal)
			public Decimal Code7;
			// vbPorter upgrade warning: Code8 As Decimal	OnWrite(short, Decimal)
			public Decimal Code8;
			// vbPorter upgrade warning: Code9 As Decimal	OnWrite(short, Decimal)
			public Decimal Code9;
		};

		VendorTax VendorTaxInfo = new VendorTax();
		object ans;
		string strRec1 = "";
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsArchiveInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsAdjustments = new clsDRWrapper();
		clsDRWrapper rsTaxTable = new clsDRWrapper();
		int counter;
		int intYear;
		string strReturn;

		private void frmSelectYear_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			cboYear.Clear();
			for (counter = DateTime.Today.Year - 2; counter <= DateTime.Today.Year + 1; counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
			if (DateTime.Today.Month < 6)
			{
				cboYear.SelectedIndex = 1;
			}
			else
			{
				cboYear.SelectedIndex = 2;
			}
			cboYear.Focus();
			this.Refresh();
		}

		private void frmSelectYear_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectYear.FillStyle	= 0;
			//frmSelectYear.ScaleWidth	= 2895;
			//frmSelectYear.ScaleHeight	= 1500;
			//frmSelectYear.LinkTopic	= "Form2";
			//frmSelectYear.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSelectYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			string strCurDir = "";
			int ans;
			string strArchivePath = "";
			clsDRWrapper rsOldAP = new clsDRWrapper();
			clsDRWrapper rsNewAP = new clsDRWrapper();
			clsDRWrapper rsOldAPDetail = new clsDRWrapper();
			clsDRWrapper rsNewAPDetail = new clsDRWrapper();
			clsDRWrapper rsOldCD = new clsDRWrapper();
			clsDRWrapper rsNewCD = new clsDRWrapper();
			int fnx;
			int lngNewRecord = 0;
			cArchiveUtility autPastYear = new cArchiveUtility();
			FCCollection colDataInfo = new FCCollection();
			ConnectionGroup tGroup = new ConnectionGroup();
			clsDRWrapper rsTemp = new clsDRWrapper();
			bool boolUseArchive;
			strReturn = cboYear.Text;
			if (Strings.Trim(strReturn) == "")
			{
				intYear = 0;
			}
			else
			{
				intYear = FCConvert.ToInt16(FCConvert.ToDouble(strReturn));
			}
			if (intYear == 0)
			{
				return;
			}
			else if (Strings.Trim(Conversion.Str(intYear)).Length != 4)
			{
				MessageBox.Show("You must input a four digit year before you may continue.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// ans = MsgBox("Does your stored 1099 data need to be updated?  Only select yes if an Accounts Payable journal or a Cash Disbursement journal has been posted in your prior fiscal year that needs to be included on this years 1099's.", vbQuestion + vbYesNo, "Update 1099 Info?")
			// If ans = vbYes Then
			boolUseArchive = false;
			rsTemp.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' ORDER BY ArchiveID DESC", "SystemSettings");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ArchiveID")) > 0)
				{
					strArchivePath = autPastYear.DeriveGroupName("Archive", FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ArchiveID")));
					boolUseArchive = true;
				}
				else
				{
					// MsgBox "No previous years available.", vbInformation, "No Previous Year Data"
					// Exit Sub
					boolUseArchive = false;
				}
			}
			else
			{
				// MsgBox "No previous years available.", vbInformation, "No Previous Year Data"
				// Exit Sub
				boolUseArchive = false;
			}
			if (boolUseArchive)
			{
				rsOldAP.GroupName = strArchivePath;
				rsOldAPDetail.GroupName = strArchivePath;
				rsOldCD.GroupName = strArchivePath;
			}
			rsNewAP.Execute("DELETE FROM APJournalArchive", "Budgetary");
			rsNewAP.Execute("DELETE FROM APJournalDetailArchive", "Budgetary");
			rsNewCD.Execute("DELETE FROM CDJournalArchive", "Budgetary");
			rsNewAP.OmitNullsOnInsert = true;
			rsNewAPDetail.OmitNullsOnInsert = true;
			rsNewAP.OpenRecordset("SELECT * FROM APJournalArchive WHERE ID = 0");
			rsNewAPDetail.OpenRecordset("SELECT * FROM APJournalDetailArchive WHERE ID = 0");
			if (boolUseArchive)
			{
				rsOldAP.OpenRecordset("SELECT * FROM APJournal WHERE CheckDate BETWEEN '1/1/" + cboYear.Text + "' AND '12/31/" + cboYear.Text + "'");
				if (rsOldAP.EndOfFile() != true && rsOldAP.BeginningOfFile() != true)
				{
					do
					{
						rsNewAP.AddNew();
						for (fnx = 0; fnx <= rsOldAP.FieldsCount - 1; fnx++)
						{
							if (rsOldAP.Get_FieldsIndexName(fnx) == "ID")
							{
								goto TryAgain;
							}
							else
							{
								rsNewAP.Set_Fields(rsOldAP.Get_FieldsIndexName(fnx), rsOldAP.Get_FieldsIndexValue(fnx));
							}
							TryAgain:
							;
						}
						rsNewAP.Update(true);
						lngNewRecord = FCConvert.ToInt32(rsNewAP.Get_Fields_Int32("ID"));
						rsOldAPDetail.OpenRecordset("SELECT * FROM APjournalDetail WHERE APJournalID = " + rsOldAP.Get_Fields_Int32("ID"));
						if (rsOldAPDetail.EndOfFile() != true && rsOldAPDetail.BeginningOfFile() != true)
						{
							do
							{
								rsNewAPDetail.AddNew();
								for (fnx = 0; fnx <= rsOldAPDetail.FieldsCount - 1; fnx++)
								{
									if (rsOldAPDetail.Get_FieldsIndexName(fnx) == "ID")
									{
										goto TRYAGAIN2;
									}
									else if (rsOldAPDetail.Get_FieldsIndexName(fnx) == "APJournalID")
									{
										rsNewAPDetail.Set_Fields("APJournalArchiveID", lngNewRecord);
									}
									else
									{
										rsNewAPDetail.Set_Fields(rsOldAPDetail.Get_FieldsIndexName(fnx), rsOldAPDetail.Get_FieldsIndexValue(fnx));
									}
									TRYAGAIN2:
									;
								}
								rsNewAPDetail.Update(true);
								rsOldAPDetail.MoveNext();
							}
							while (rsOldAPDetail.EndOfFile() != true);
						}
						rsOldAP.MoveNext();
					}
					while (rsOldAP.EndOfFile() != true);
				}
			}
			rsNewCD.OmitNullsOnInsert = true;
			rsNewCD.OpenRecordset("SELECT * FROM CDJournalArchive WHERE ID = 0");
			if (boolUseArchive)
			{
				rsOldCD.OpenRecordset("SELECT * FROM JournalEntries WHERE Type = 'D' AND JournalEntriesDate BETWEEN '1/1/" + cboYear.Text + "' AND '12/31/" + cboYear.Text + "'");
				if (rsOldCD.EndOfFile() != true && rsOldCD.BeginningOfFile() != true)
				{
					do
					{
						rsNewCD.AddNew();
						for (fnx = 0; fnx <= rsNewCD.FieldsCount - 1; fnx++)
						{
							if (rsNewCD.Get_FieldsIndexName(fnx) == "ID")
							{
								goto TRYAGAIN3;
							}
							else
							{
								rsNewCD.Set_Fields(rsNewCD.Get_FieldsIndexName(fnx), rsOldCD.Get_Fields(rsNewCD.Get_FieldsIndexName(fnx)));
							}
							TRYAGAIN3:
							;
						}
						rsNewCD.Update(true);
						rsOldCD.MoveNext();
					}
					while (rsOldCD.EndOfFile() != true);
				}
			}
			// End If
			rsOldAP.GroupName = rsNewAP.GroupName;
			rsOldAPDetail.GroupName = rsNewAPDetail.GroupName;
			rsOldCD.GroupName = rsNewCD.GroupName;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Extracting 1099 Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE [1099] = 1 ORDER BY VendorNumber");
			rsTaxTable.Execute("DELETE FROM VendorTaxInfo", "Budgetary");
			rsTaxTable.OpenRecordset("SELECT * FROM VendorTaxInfo");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				/*? On Error GoTo 0 */
                do
                {
                    //Application.DoEvents();
                    rsAdjustments.OpenRecordset("SELECT * FROM Adjustments WHERE VendorNumber = " +
                                                rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY Code");
                    // If we are including all their payments in their 1099s
                    if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("IncludeAllPaymentsFor1099")))
                    {
                        // If the vendor was set up with a specific tax code
                        if (Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code")) != 0)
                        {
                            // Get all entries against the vendor to sum up for 1099s
                            rsOldAP.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(APJournalDetail.Amount - APJournalDetail.Discount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE rtrim(APJournal.CheckNumber) <> '' AND APJournal.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournal.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' GROUP BY APJournalDetail.[1099] ) as temp GROUP BY [1099] ORDER BY [1099]");
                            rsOldCD.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(Amount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM JournalEntries WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' GROUP BY [1099] ) as temp GROUP BY [1099] ORDER BY [1099]");
                            // If FirstMonth <> 1 Then
                            rsNewAP.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(APJournalDetailArchive.Amount - APJournalDetailArchive.Discount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM (APJournalDetailArchive INNER JOIN APJournalArchive ON APJournalDetailArchive.APJournalArchiveID = APJournalArchive.ID) WHERE rtrim(APJournalArchive.CheckNumber) <> '' AND APJournalArchive.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournalArchive.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' GROUP BY APJournalDetailArchive.[1099] ) as temp GROUP BY [1099] ORDER BY [1099]");
                            // End If
                            // If FirstMonth <> 1 Then
                            rsNewCD.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(Amount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM CDJournalArchive WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND CDJournalArchiveDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' GROUP BY [1099] ) as temp GROUP BY [1099] ORDER BY [1099]");
                            // End If
                        }
                        else
                        {
                            // Get all numeric 1099 code items
                            rsOldAP.OpenRecordset(
                                "SELECT SUM(APJournalDetail.Amount - APJournalDetail.Discount) AS TotalAmount, APJournalDetail.[1099] as [1099] FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE rtrim(APJournal.CheckNumber) <> '' AND APJournal.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournal.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetail.[1099] <> 'D' AND APJournalDetail.[1099] <> 'N' GROUP BY APJournalDetail.[1099] ORDER BY APJournalDetail.[1099]");
                            rsOldCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, [1099] FROM JournalEntries WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND [1099] <> 'D' AND [1099] <> 'N' GROUP BY [1099] ORDER BY [1099]");
                            rsNewAP.OpenRecordset(
                                "SELECT SUM(APJournalDetailArchive.Amount - APJournalDetailArchive.Discount) AS TotalAmount, APJournalDetailArchive.[1099] as [1099] FROM (APJournalDetailArchive INNER JOIN APJournalArchive ON APJournalDetailArchive.APJournalArchiveID = APJournalArchive.ID) WHERE rtrim(APJournalArchive.CheckNumber) <> '' AND APJournalArchive.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournalArchive.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetailArchive.[1099] <> 'D' AND APJournalDetailArchive.[1099] <> 'N' GROUP BY APJournalDetailArchive.[1099] ORDER BY APJournalDetailArchive.[1099]");
                            rsNewCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, [1099] FROM CDJournalArchive WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND CDJournalArchiveDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND [1099] <> 'D' AND [1099] <> 'N' GROUP BY [1099] ORDER BY [1099]");
                        }
                    }
                    else
                    {
                        // If vendor has been set up for a specific 1099 code
                        if (Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code")) != 0)
                        {
                            // get all entries except ones that were coded with an N
                            rsOldAP.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(APJournalDetail.Amount - APJournalDetail.Discount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE rtrim(APJournal.CheckNumber) <> '' AND APJournal.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournal.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetail.[1099] <> 'N' GROUP BY APJournalDetail.[1099]) as temp GROUP BY [1099] ORDER BY [1099]");
                            rsOldCD.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(Amount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM JournalEntries WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND [1099] <> 'N' GROUP BY [1099] ) as temp GROUP BY [1099] ORDER BY [1099]");
                            rsNewAP.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(APJournalDetailArchive.Amount - APJournalDetailArchive.Discount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM (APJournalDetailArchive INNER JOIN APJournalArchive ON APJournalDetailArchive.APJournalArchiveID = APJournalArchive.ID) WHERE rtrim(APJournalArchive.CheckNumber) <> '' AND APJournalArchive.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournalArchive.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetailArchive.[1099] <> 'N' GROUP BY APJournalDetailArchive.[1099] ) as temp GROUP BY [1099] ORDER BY [1099]");
                            rsNewCD.OpenRecordset(
                                "SELECT SUM(TotalAmount2) as TotalAmount, [1099] FROM (SELECT SUM(Amount) AS TotalAmount2, " +
                                FCConvert.ToString(Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code"))) +
                                " as [1099] FROM CDJournalArchive WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND CDJournalArchiveDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND [1099] <> 'N' GROUP BY [1099]) as temp GROUP BY [1099] ORDER BY [1099]");
                        }
                        else
                        {
                            // else get all numeric 1099 entries against vendor
                            rsOldAP.OpenRecordset(
                                "SELECT SUM(APJournalDetail.Amount - APJournalDetail.Discount) AS TotalAmount, APJournalDetail.[1099] as [1099] FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE rtrim(APJournal.CheckNumber) <> '' AND APJournal.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournal.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetail.[1099] <> 'N' AND APJournalDetail.[1099] <> 'D' GROUP BY APJournalDetail.[1099] ORDER BY APJournalDetail.[1099]");
                            rsOldCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, [1099] FROM JournalEntries WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND [1099] <> 'D' AND [1099] <> 'N' GROUP BY [1099] ORDER BY [1099]");
                            rsNewAP.OpenRecordset(
                                "SELECT SUM(APJournalDetailArchive.Amount - APJournalDetailArchive.Discount) AS TotalAmount, APJournalDetailArchive.[1099] as [1099] FROM (APJournalDetailArchive INNER JOIN APJournalArchive ON APJournalDetailArchive.APJournalArchiveID = APJournalArchive.ID) WHERE rtrim(APJournalArchive.CheckNumber) <> '' AND APJournalArchive.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournalArchive.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetailArchive.[1099] <> 'N' AND APJournalDetailArchive.[1099] <> 'D' GROUP BY APJournalDetailArchive.[1099] ORDER BY APJournalDetailArchive.[1099]");
                            rsNewCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, [1099] FROM CDJournalArchive WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND CDJournalArchiveDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND [1099] <> 'D' AND [1099] <> 'N' GROUP BY [1099] ORDER BY [1099]");
                        }
                    }

                    ClearTotal();
                    for (counter = 1; counter <= 9; counter++)
                    {
                        FillTotal_2(Strings.Trim(counter.ToString()), rsAdjustments, rsOldAP, rsNewAP, rsOldCD,
                            rsNewCD);
                    }

                    if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("IncludeAllPaymentsFor1099")))
                    {
                        if (Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code")) == 0)
                        {
                            rsOldAP.OpenRecordset(
                                "SELECT SUM(APJournalDetail.Amount - APJournalDetail.Discount) AS TotalAmount, APJournalDetail.Account AS Account FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE rtrim(APJournal.CheckNumber) <> '' AND APJournal.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournal.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND (APJournalDetail.[1099] = 'D' or APJournalDetail.[1099] = 'N') as temp GROUP BY APJournalDetail.Account");
                            rsOldCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, Account FROM JournalEntries WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND ([1099] = 'D' or [1099] = 'N') as temp GROUP BY Account");
                            rsNewAP.OpenRecordset(
                                "SELECT SUM(APJournalDetailArchive.Amount - APJournalDetailArchive.Discount) AS TotalAmount, APJournalDetailArchive.Account AS Account FROM (APJournalDetailArchive INNER JOIN APJournalArchive ON APJournalDetailArchive.APJournalArchiveID = APJournalArchive.ID) WHERE rtrim(APJournalArchive.CheckNumber) <> '' AND APJournalArchive.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournalArchive.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND (APJournalDetailArchive.[1099] = 'D' or APJournalDetailArchive.[1099] = 'N') as temp GROUP BY APJournalDetailArchive.Account");
                            rsNewCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, Account FROM CDJournalArchive WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND CDJournalArchiveDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND ([1099] = 'D' or [1099] = 'N') as temp GROUP BY Account");
                        }
                    }
                    else
                    {
                        if (Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code")) == 0)
                        {
                            rsOldAP.OpenRecordset(
                                "SELECT SUM(APJournalDetail.Amount - APJournalDetail.Discount) AS TotalAmount, APJournalDetail.Account AS Account FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE rtrim(APJournal.CheckNumber) <> '' AND APJournal.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournal.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetail.[1099] = 'D' GROUP BY APJournalDetail.Account");
                            rsNewAP.OpenRecordset(
                                "SELECT SUM(APJournalDetailArchive.Amount - APJournalDetailArchive.Discount) AS TotalAmount, APJournalDetailArchive.Account AS Account FROM (APJournalDetailArchive INNER JOIN APJournalArchive ON APJournalDetailArchive.APJournalArchiveID = APJournalArchive.ID) WHERE rtrim(APJournalArchive.CheckNumber) <> '' AND APJournalArchive.VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND APJournalArchive.CheckDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) +
                                "' AND APJournalDetailArchive.[1099] = 'D' GROUP BY APJournalDetailArchive.Account");
                            rsOldCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, Account FROM JournalEntries WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) + "' AND [1099] = 'D' GROUP BY Account");
                            rsNewCD.OpenRecordset(
                                "SELECT SUM(Amount) AS TotalAmount, Account FROM CDJournalArchive WHERE Type = 'D' AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND CDJournalArchiveDate BETWEEN '1/1/" + FCConvert.ToString(intYear) +
                                "' AND '12/31/" + FCConvert.ToString(intYear) + "' AND [1099] = 'D' GROUP BY Account");
                        }
                    }

                    if (Conversion.Val(rsVendorInfo.Get_Fields_String("1099Code")) == 0)
                    {
                        if (boolUseArchive)
                        {
                            if (rsOldAP.EndOfFile() != true && rsOldAP.BeginningOfFile() != true)
                            {
                                do
                                {
                                    // Dave 12/29/05 Call ID 84557 We should never need to run thi spart if the vendor has a 1099 code specified
                                    AddDefaultToVendorTax(ref rsOldAP);
                                    rsOldAP.MoveNext();
                                } while (rsOldAP.EndOfFile() != true);
                            }
                        }

                        if (rsNewAP.EndOfFile() != true && rsNewAP.BeginningOfFile() != true)
                        {
                            do
                            {
                                AddDefaultToVendorTax(ref rsNewAP);
                                rsNewAP.MoveNext();
                            } while (rsNewAP.EndOfFile() != true);
                        }

                        if (boolUseArchive)
                        {
                            if (rsOldCD.EndOfFile() != true && rsOldCD.BeginningOfFile() != true)
                            {
                                do
                                {
                                    AddDefaultToVendorTax(ref rsOldCD);
                                    rsOldCD.MoveNext();
                                } while (rsOldCD.EndOfFile() != true);
                            }
                        }

                        if (rsNewCD.EndOfFile() != true && rsNewCD.BeginningOfFile() != true)
                        {
                            do
                            {
                                AddDefaultToVendorTax(ref rsNewCD);
                                rsNewCD.MoveNext();
                            } while (rsNewCD.EndOfFile() != true);
                        }

                        // End If
                    }

                    rsTaxTable.OmitNullsOnInsert = true;
                    if (VendorTaxInfo.Code1 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "1");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code1);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code2 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "2");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code2);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code3 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "3");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code3);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code4 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "4");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code4);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code5 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "5");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code5);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code6 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "6");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code6);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code7 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "7");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code7);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code8 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "8");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code8);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    if (VendorTaxInfo.Code9 != 0)
                    {
                        rsTaxTable.AddNew();
                        rsTaxTable.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        rsTaxTable.Set_Fields("Class", "9");
                        rsTaxTable.Set_Fields("Amount", VendorTaxInfo.Code9);
                        rsTaxTable.Set_Fields("Year", intYear);
                        rsTaxTable.Update();
                    }

                    rsVendorInfo.MoveNext();
                } while (rsVendorInfo.EndOfFile() != true);

				clsDRWrapper rsDelete = new clsDRWrapper();
                rsDelete.Execute("DELETE FROM [1099FormTotals]", "Budgetary");

                frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("Extract Successful!", "Extract Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("There were no vendors set up to recieve 1099 information", "No Vendors", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			Close();
		}

		private void ClearTotal()
		{
			VendorTaxInfo.Code1 = 0;
			VendorTaxInfo.Code2 = 0;
			VendorTaxInfo.Code3 = 0;
			VendorTaxInfo.Code4 = 0;
			VendorTaxInfo.Code5 = 0;
			VendorTaxInfo.Code6 = 0;
			VendorTaxInfo.Code7 = 0;
			VendorTaxInfo.Code8 = 0;
			VendorTaxInfo.Code9 = 0;
		}

		private void FillTotal_2(string strCode, clsDRWrapper rsAdj, clsDRWrapper rsData, clsDRWrapper rsArchiveData = null, clsDRWrapper rsCDData = null, clsDRWrapper rsArchiveCDData = null)
		{
			FillTotal(ref strCode, ref rsAdj, ref rsData, rsArchiveData, rsCDData, rsArchiveCDData);
		}

		private void FillTotal(ref string strCode, ref clsDRWrapper rsAdj, ref clsDRWrapper rsData, clsDRWrapper rsArchiveData = null, clsDRWrapper rsCDData = null, clsDRWrapper rsArchiveCDData = null)
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotal;
			curTotal = 0;
			CheckNextAdjustment:
			;
			if (rsAdj.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				if (rsAdj.Get_Fields("Code").ToString() == strCode)
				{
					if (rsAdj.Get_Fields_Boolean("Positive"))
					{
						curTotal += rsAdj.Get_Fields_Decimal("Adjustment");
					}
					else
					{
						curTotal -= rsAdj.Get_Fields_Decimal("Adjustment");
					}
					rsAdj.MoveNext();
					goto CheckNextAdjustment;
				}
			}
			/*? On Error GoTo 0 */
			if (!(rsData == null))
			{
				if (rsData.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					if (rsData.Get_Fields("1099").ToString() == strCode)
					{
						curTotal += rsData.Get_Fields_Decimal("TotalAmount");
						rsData.MoveNext();
					}
				}
			}
			if (!(rsArchiveData == null))
			{
				if (rsArchiveData.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					if (rsArchiveData.Get_Fields("1099").ToString() == strCode)
					{
						curTotal += rsArchiveData.Get_Fields_Decimal("TotalAmount");
						rsArchiveData.MoveNext();
					}
				}
			}
			if (!(rsCDData == null))
			{
				if (rsCDData.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					if (rsCDData.Get_Fields("1099").ToString() == strCode)
					{
						curTotal += rsCDData.Get_Fields_Decimal("TotalAmount");
						rsCDData.MoveNext();
					}
				}
			}
			if (!(rsArchiveCDData == null))
			{
				if (rsArchiveCDData.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					if (rsArchiveCDData.Get_Fields("1099").ToString() == strCode)
					{
						curTotal += rsArchiveCDData.Get_Fields_Decimal("TotalAmount");
						rsArchiveCDData.MoveNext();
					}
				}
			}
			if (strCode == "1")
			{
				VendorTaxInfo.Code1 = curTotal;
			}
			else if (strCode == "2")
			{
				VendorTaxInfo.Code2 = curTotal;
			}
			else if (strCode == "3")
			{
				VendorTaxInfo.Code3 = curTotal;
			}
			else if (strCode == "4")
			{
				VendorTaxInfo.Code4 = curTotal;
			}
			else if (strCode == "5")
			{
				VendorTaxInfo.Code5 = curTotal;
			}
			else if (strCode == "6")
			{
				VendorTaxInfo.Code6 = curTotal;
			}
			else if (strCode == "7")
			{
				VendorTaxInfo.Code7 = curTotal;
			}
			else if (strCode == "8")
			{
				VendorTaxInfo.Code8 = curTotal;
			}
			else if (strCode == "9")
			{
				VendorTaxInfo.Code9 = curTotal;
			}
			else if (strCode == "D")
			{
			}
		}

		private void AddDefaultToVendorTax(ref clsDRWrapper rsAmount, string strTaxCode = "D")
		{
			clsDRWrapper rsExpense = new clsDRWrapper();
			string strCodeToCheck = "";
			if (strTaxCode == "D")
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Left(rsAmount.Get_Fields("Account"), 1) == "E")
				{
					if (modAccountTitle.Statics.ObjFlag)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsExpense.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense(rsAmount.Get_Fields("Account")) + "'");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsExpense.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense(rsAmount.Get_Fields("Account")) + "' AND Object = '" + Object(rsAmount.Get_Fields("Account")) + "'");
					}
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				else if (Strings.Left(rsAmount.Get_Fields("Account"), 1) == "G")
				{
					if (modAccountTitle.Statics.YearFlag)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsExpense.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount(rsAmount.Get_Fields("Account"))) + "' AND Account = '" + modBudgetaryAccounting.GetAccount(rsAmount.Get_Fields("Account")) + "'");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsExpense.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount(rsAmount.Get_Fields("Account"))) + "' AND Account = '" + modBudgetaryAccounting.GetAccount(rsAmount.Get_Fields("Account")) + "' AND Year = '" + modBudgetaryAccounting.GetSuffix(rsAmount.Get_Fields("Account")) + "'");
					}
				}
				if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
					strCodeToCheck = FCConvert.ToString(rsExpense.Get_Fields("Tax"));
				}
				else
				{
					return;
				}
			}
			else
			{
				strCodeToCheck = strTaxCode;
			}
			if (strCodeToCheck == "1")
			{
				VendorTaxInfo.Code1 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "2")
			{
				VendorTaxInfo.Code2 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "3")
			{
				VendorTaxInfo.Code3 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "4")
			{
				VendorTaxInfo.Code4 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "5")
			{
				VendorTaxInfo.Code5 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "6")
			{
				VendorTaxInfo.Code6 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "7")
			{
				VendorTaxInfo.Code7 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "8")
			{
				VendorTaxInfo.Code8 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else if (strCodeToCheck == "9")
			{
				VendorTaxInfo.Code9 += rsAmount.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				// do nothing
			}
		}

		private string Expense(string x)
		{
			string Expense = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Expense = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			else
			{
				Expense = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			return Expense;
		}

		private string Department(ref string x)
		{
			string Department = "";
			Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return Department;
		}

		private string Division(ref string x)
		{
			string Division = "";
			Division = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			return Division;
		}

		private string Object(string x)
		{
			string Object = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Object = Strings.Mid(x, 6 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			else
			{
				Object = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			return Object;
		}
	}
}
