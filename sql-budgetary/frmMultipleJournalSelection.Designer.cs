﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmMultipleJournalSelection.
	/// </summary>
	partial class frmMultipleJournalSelection : BaseForm
	{
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncumbranceActivity;
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMultipleJournalSelection));
			this.chkShowLiquidatedEncumbranceActivity = new fecherFoundation.FCCheckBox();
			this.vsJournals = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 467);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkShowLiquidatedEncumbranceActivity);
			this.ClientArea.Controls.Add(this.vsJournals);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 407);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(201, 30);
			this.HeaderText.Text = "Journal Selection";
			// 
			// chkShowLiquidatedEncumbranceActivity
			// 
			this.chkShowLiquidatedEncumbranceActivity.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.chkShowLiquidatedEncumbranceActivity.Location = new System.Drawing.Point(30, 359);
			this.chkShowLiquidatedEncumbranceActivity.Name = "chkShowLiquidatedEncumbranceActivity";
			this.chkShowLiquidatedEncumbranceActivity.Size = new System.Drawing.Size(311, 27);
			this.chkShowLiquidatedEncumbranceActivity.TabIndex = 4;
			this.chkShowLiquidatedEncumbranceActivity.Text = "Show Liquidated Encumbrance Activity";
			// 
			// vsJournals
			// 
			this.vsJournals.AllowSelection = false;
			this.vsJournals.AllowUserToResizeColumns = false;
			this.vsJournals.AllowUserToResizeRows = false;
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsJournals.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsJournals.BackColorBkg = System.Drawing.Color.Empty;
			this.vsJournals.BackColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.BackColorSel = System.Drawing.Color.Empty;
			this.vsJournals.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsJournals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsJournals.ColumnHeadersHeight = 30;
			this.vsJournals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsJournals.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsJournals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsJournals.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsJournals.ExtendLastCol = true;
			this.vsJournals.FixedCols = 0;
			this.vsJournals.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.FrozenCols = 0;
			this.vsJournals.GridColor = System.Drawing.Color.Empty;
			this.vsJournals.Location = new System.Drawing.Point(30, 77);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.ReadOnly = true;
			this.vsJournals.RowHeadersVisible = false;
			this.vsJournals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsJournals.RowHeightMin = 0;
			this.vsJournals.Rows = 1;
			this.vsJournals.ShowColumnVisibilityMenu = false;
			this.vsJournals.Size = new System.Drawing.Size(1020, 259);
			this.vsJournals.StandardTab = true;
			this.vsJournals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsJournals.TabIndex = 0;
			this.vsJournals.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsJournals_MouseMoveEvent);
			this.vsJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsJournals_KeyDownEvent);
			this.vsJournals.Click += new System.EventHandler(this.vsJournals_ClickEvent);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(480, 21);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "PLEASE SELECT THE JOURNALS YOU WISH TO VIEW AND CLICK THE OK BUTTON";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmMultipleJournalSelection
            // 
            this.AcceptButton = this.cmdProcessSave;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 575);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmMultipleJournalSelection";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Journal Selection";
			this.Load += new System.EventHandler(this.frmMultipleJournalSelection_Load);
			this.Activated += new System.EventHandler(this.frmMultipleJournalSelection_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMultipleJournalSelection_KeyPress);
			this.Resize += new System.EventHandler(this.frmMultipleJournalSelection_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
