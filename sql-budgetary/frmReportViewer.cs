﻿using fecherFoundation;
using GrapeCity.ActiveReports;
using System;
using System.IO;
using TWBD0000;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for frmReportViewer.
    /// </summary>
    public partial class frmReportViewer : BaseForm
    {
        public frmReportViewer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;

            base.Cancelled += Report_Cancelled;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmReportViewer InstancePtr
        {
            get
            {
                return (frmReportViewer)Sys.GetInstance(typeof(frmReportViewer));
            }
        }

        protected frmReportViewer _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Corey Gray              *
        // DATE           :               10/02/2003              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               03/15/2006              *
        // ********************************************************
        bool boolShowPrintDialog;
        bool boolPrintDuplex;
        bool boolDuplexForcePairs;
        string strDuplexLabel;
        bool boolUseFilename;
        int intCopies;
        bool pboolOverRideDuplex;
        int intFormModal;
        bool boolEmail;
        string strEmailAttachmentName;
        bool boolAutoPrintDuplex;
        bool boolCantChoosePages;
        bool blnCurrentAccountStatus;
        bool blnDontClose;
        private bool cancelled = false;

        private FCSectionReport currentReport = null;
        // VBto upgrade warning: rptObj As object	OnWrite(rptTaxAcquiredProperties, rptLoadbackReportMaster, arTextDump, ar30DayNotice, arLienProcess, arLienMaturity, arLienProcessEditReport, rptOutstandingPeriodBalances, rptOutprintingFormat, arLienTransferReport, rptInterestedPartyMasterList, rptCustomForms, rptReminderNoticeLabels, rptCustomMHLabels, rptDemandFeeList, rptReminderLaserPostCard, rptReminderForm, rptCLDailyAuditMaster, rptReminderMailer, rptTaxClubOutstanding, rptTaxClubMaster, rptTaxServiceSummary, rptLienDischargeBP, rptTaxClubBooklet, rptTaxClubActivity, rptLienDateLine, rptCustomLabels, rptCertificateOfSettlement, rptCertificateOfRecommitment, arLienPartialPayment, rptTaxRateReport, rptREIntParty, rptMHWithREAccounts, arWaiverOfForeclosure, rptBankruptcy, rptPurge, rptLienRemovalPayments, arStatusLists, rptOutstandingBalances, rptOutstandingLienBalances, rptOutstandingBalancesAll, arLienStatusReport, rptStatusListAccountDetail, rptAuditStatusAll, rptBankruptAccounts, rptAccountInformationSheet, arBatchListing, arLienDischargeNotice, rptREbyHolderSplit, arBookPageAccount, rptGroupInformation, arAllLienDischargeNotice, rptUpdatedAddressList, rptTaxAcquiredList, rptReminderPostCard, rptLienMaturityFees, rptMHWithUTAccounts, rptUTbyHolder, rptUTIntParty, arAccountDetail, arCLInformationScreen, rptREbyHolder, rptMortgageHolderMasterList, rptREMortgage, rptUTMortgage, rptUpdatedBookPage, rptPrintUnprintedPartialPaymentWaivers, rptEmailContacts, rptMultiModulePosting)
        public void Init(FCSectionReport rptObj = null, string strPrinter = "", short intModal = 0, bool boolDuplex = false, bool boolDuplexPairsMandatory = false, string strDuplexTitle = "Pages", bool boolLandscape = false, string strFileName = "", string strFileTitle = "TRIO Software", bool boolOverRideDuplex = false, bool boolAllowEmail = true, string strAttachmentName = "", bool boolDontAllowPagesChoice = false, bool blnDontAllowCloseTilReportDone = false, bool showModal = false, bool showProgressDialog = true, bool allowCancel = false)
        {
            // if printing duplex and it is a normal type report, boolDuplexForcePairs should be false
            // if printing duplex and Front and Back pages must be paired the way they are in the report (such as property cards) set boolDuplexForcePairs = true
            // set the duplex title if pages is not a good description for what is printing (such as property cards)
            // strAttachment name is the name to call the attachment.  It will be created with a meaningless temp name.  This will be the filename the email receiver will see
            try
            {
                // On Error GoTo ErrorHandler
                string strTemp = "";
                boolCantChoosePages = boolDontAllowPagesChoice;
                boolAutoPrintDuplex = false;
                if (!(rptObj == null))
                {
                    if (rptObj.Name == "Current Account Status")
                    {
                        blnCurrentAccountStatus = true;
                    }
                    else
                    {
                        blnCurrentAccountStatus = false;
                        //FC:FINAL:AM: don't show the menu; there are no items in it
                        Menu = null;
                    }
                }
                strEmailAttachmentName = strAttachmentName;
                intCopies = 1;
                boolEmail = boolAllowEmail;
                blnDontClose = blnDontAllowCloseTilReportDone;
                boolUseFilename = false;
                currentReport = rptObj;
                cancelled = false;

                if (strFileName != "")
                {
                    // make sure that the file exists
                    if (File.Exists(strFileName))
                    {
                        //FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
                        //ARViewer21.ReportSource.Document.Load(strFileName);
                        ARViewer21.ReportName = strFileName;
                        boolUseFilename = true;
                        Text = strFileTitle;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    // load the report from the object passed in
                    Text = rptObj.Name;
                    //FC:FINAL:SBE - #96 - dispose current viewer, and create a new one when ReportSource is changed
                    if (ARViewer21.ReportSource != null)
                    {
                        if (!ARViewer21.ReportSource.Disposing)
                        {
                            ARViewer21.ReportSource.Dispose();
                        }
                        ARViewer21.Dispose();
                        ARViewer21 = null;
                        ARViewer21 = new ARViewer();
                        ARViewer21.Dock = DockStyle.Fill;
                        ClientArea.Controls.Add(ARViewer21);
                        ClientArea.Controls.SetChildIndex(ARViewer21, 0);
                    }
                    //FC:FINAL:SBE - #2519 - run report asynchronously to avoid thread abort exception for long running Reports
                    // THIS CAN LEAVE ARViewer21.ReportSource NULL FOR AN INDETERMINATE PERIOD OF TIME
                    // WHICH CAN CAUSE AN EXCEPTION BELOW ON ARViewer21.Printer
                    if (showProgressDialog)
                    {
                        rptObj.PageEnd += (s, e) =>
                        {
                            if (rptObj.FetchDataStopped)
                            {
                                return;
                            }
                            UpdateWait($"Running report ... Page {rptObj.PageNumber}");
                        };
                        ShowWait(disableformClosing: false, enableCancel: allowCancel);

                        Application.StartTask(() =>
                        {
                            try
                            {
                                ARViewer21.ReportSource = rptObj;
                                    //FC:FINAL:AM:#3832 - check if report has been already generated (by calling Run)
                                    if (rptObj.State != SectionReport.ReportState.Completed)
                                {
                                    rptObj.RunReport();
                                }
                            }
                            finally
                            {
                                if (!cancelled)
                                {
                                    EndWait();
                                    SetPrinterOptions(strPrinter, boolLandscape);
                                }
                            }

                        });
                    }
                    else
                    {
                        ARViewer21.ReportSource = rptObj;
                    }
                }

                if (boolEmail && Strings.Trim(strEmailAttachmentName) == string.Empty)
                {
                    strTemp = Text;
                    strTemp = Strings.Replace(strTemp, "/", "", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Replace(strTemp, "\\", "", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Replace(strTemp, " ", "", 1, -1, CompareConstants.vbTextCompare);
                    strEmailAttachmentName = strTemp;
                }
                //ARViewer21.Printer.Zoom = -1;
                boolPrintDuplex = boolDuplex;
                pboolOverRideDuplex = boolOverRideDuplex;
                boolDuplexForcePairs = boolDuplexPairsMandatory;
                strDuplexLabel = strDuplexTitle;
                if (blnCurrentAccountStatus)
                {
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MovePrevious();
                    if (frmCurrentAccountStatus.InstancePtr.rsAccountInfo.BeginningOfFile() == true)
                    {
                        mnuFilePreviousAccount.Enabled = false;
                    }
                    else
                    {
                        mnuFilePreviousAccount.Enabled = true;
                    }
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
                    if (frmCurrentAccountStatus.InstancePtr.rsAccountInfo.EndOfFile() == true)
                    {
                        mnuFileNextAccount.Enabled = false;
                    }
                    else
                    {
                        mnuFileNextAccount.Enabled = true;
                    }
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MovePrevious();
                }
                if (!showProgressDialog)
                {
                    SetPrinterOptions(strPrinter, boolLandscape);
                }
                intFormModal = intModal;
                //FC:FINAL:AM: show always in the mainform
                //switch (intModal)
                //{
                //    case 0:
                //        {
                //            this.Show(App.MainForm);
                //            break;
                //        }
                //    case 1:
                //        {
                //            this.Show(FormShowEnum.Modal);
                //            break;
                //        }
                //}
                //FC:FINAL:IPI - #1249 - allow report to be shown modal as MessageBox & frmSignaturePassword should be shown in the right order
                if (showModal)
                {
                    Show(FormShowEnum.Modal);
                }
                else
                {
                    Show(App.MainForm);
                }
                if (blnCurrentAccountStatus)
                {
                    frmCurrentAccountStatus.InstancePtr.Hide();
                }
                //end switch
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmReportViewer_Init", MsgBoxStyle.Critical, "Error");
            }
        }

        private void SetPrinterOptions(string strPrinter, bool boolLandscape)
        {
            if (strPrinter != string.Empty)
            {
                if (Strings.UCase(ARViewer21.Printer.PrinterName) != Strings.UCase(strPrinter))
                {
                    ARViewer21.Printer.PrinterName = strPrinter;
                }
                //ARViewer21.Printer.UseSourcePrinter = true;
                boolShowPrintDialog = false;
                if (boolLandscape)
                {
                    ARViewer21.Printer.PrinterSettings.DefaultPageSettings.Landscape = true;
                }
            }
            else
            {
                boolShowPrintDialog = true;
                //ARViewer21.Printer.UseSourcePrinter = false;
            }
        }

        private void Report_Cancelled(object sender, EventArgs e)
        {
            cancelled = true;
            currentReport.StopFetchingData();
            Close();
        }

        private void ARViewer21_KeyDown(ref Keys KeyCode, ref short Shift)
        {
            if (KeyCode == Keys.Escape)
            {
                KeyCode = 0;
                mnuExit_Click();
            }
        }

        private void frmReportViewer_Activated(object sender, System.EventArgs e)
        {
            //if (this.WindowState != FormWindowState.Maximized && this.WindowState != FormWindowState.Minimized)
            //{
            //	this.Height = this.Height - 15;
            //}
        }

        private void frmReportViewer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Escape)
            {
                KeyCode = 0;
                mnuExit_Click();
            }
        }

        private void frmReportViewer_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmReportViewer.Icon	= "frmReportViewer.frx":0000";
            //frmReportViewer.ScaleWidth	= 9225;
            //frmReportViewer.ScaleHeight	= 8235;
            //frmReportViewer.LinkTopic	= "Form1";
            //ARViewer21.SectionData	= "frmReportViewer.frx":058A;
            //vsElasticLight1.OleObjectBlob	= "frmReportViewer.frx":05C6";
            //End Unmaped Properties
            // Call SetFixedSize(Me, TRIOWINDOWSIZEBIGGIE)
            //FC:FINAL:DDU:#2684 - show modal report full mdiclient size
            Top = App.MainForm.MdiClient.Top + 46;
            Left = App.MainForm.MdiClient.Left;
            Width = App.MainForm.MdiClient.Width;
            Height = App.MainForm.MdiClient.Height - 46;
            int cnt;
            int lngID = 0;
            //this.WindowState = FormWindowState.Maximized;
            // const_printtoolid = 9951
            //FC:TODO:AM
            //for (cnt = 0; cnt <= ARViewer21ToolBar.Tools.Count - 1; cnt++)
            //{
            //    if (lngID < ARViewer21.ToolBar.Tools(cnt).ID)
            //    {
            //        lngID = ARViewer21.ToolBar.Tools(cnt).ID;
            //    }
            //} // cnt
            //for (cnt = 0; cnt <= ARViewer21.ToolBar.Tools.Count - 1; cnt++)
            //{
            //    if ("Print..." == ARViewer21.ToolBar.Tools(cnt).Caption)
            //    {
            //        // disable the automatic handling of the button
            //        ARViewer21.ToolBar.Tools(cnt).ID = lngID + 1;
            //        ARViewer21.ToolBar.Tools(cnt).Enabled = true;
            //    }
            //} // cnt
            if (boolEmail)
            {
                mnuEmail.Visible = true;
            }
            else
            {
                mnuEmail.Visible = false;
            }
        }
        // VBto upgrade warning: Cancel As short	OnWrite(bool)
        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            try
            {
            // On Error GoTo ErrorHandler
            iLabel1:
                if (blnDontClose)
                {
                iLabel2:
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                    iLabel3:
                        MessageBox.Show("Report not done", "Report Loading", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    iLabel4:
                        e.Cancel = true;
                    iLabel5:
                        return;
                    iLabel6:
                        ;
                    }
                iLabel7:
                    ;
                }
            iLabel8:
                if (!boolUseFilename)
                {
                //iLabel9: ARViewer21.ReportSource.Unload();
                iLabel10:
                    ;
                }
            iLabel11:
                if (blnCurrentAccountStatus)
                {
                iLabel12:
                    frmCurrentAccountStatus.InstancePtr.Unload();
                iLabel13:
                    frmCurrentAccountStatus.InstancePtr.Show(App.MainForm);
                iLabel14:
                    return;
                iLabel15:
                    ;
                }
            //iLabel16:
                //if (Text == "Warrant Preview")
                //{
                //iLabel17:
                //    if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("WarrantPrevDeptSummary")))
                //    {
                //    iLabel18:
                //        e.Cancel = true;
                //    iLabel19:
                //        Init(rptWarrantPreviewDeptSummary.InstancePtr, string.Empty, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true);
                //    iLabel20:
                //        ;
                //    }
                //iLabel21:
                //    return;
                //iLabel22:
                //    ;
                //}
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Report Viewer Unload on line: " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!boolUseFilename)
                {
                    //VBtoConverter.UnloadControl(ARViewer21.ReportSource);
                    if (ARViewer21.ReportSource != null)
                    {
                        ARViewer21.ReportSource.StopFetchingData();
                    }
                }
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Report Viewer Unload", MsgBoxStyle.Exclamation, "Error");
            }
            base.OnFormClosing(e);
        }

        //FC:FINAL:SBE - #70 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            try
            {
                // On Error GoTo ErrorHandler
                if (!boolUseFilename)
                {
                    //VBtoConverter.UnloadControl(ARViewer21.ReportSource);
                    if (ARViewer21.ReportSource != null)
                    {
                        //ARViewer21.ReportSource.StopFetchingData();
                        if (ARViewer21.ReportSource.Document != null)
                        {
                            ARViewer21.ReportSource.Document.Dispose();
                        }
                        if (!ARViewer21.ReportSource.Disposing)
                        {
                            ARViewer21.ReportSource.Dispose();
                        }
                        ARViewer21.ReportSource = null;
                    }
                }

                System.Diagnostics.Process pc = System.Diagnostics.Process.GetCurrentProcess();
                pc.MaxWorkingSet = pc.MinWorkingSet;
                // Doevents
                if (App.MainForm.Enabled && App.MainForm.Visible)
                {
                    App.MainForm.Focus();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                if (Information.Err(ex).Number == 91)
                {
                    // do nothing
                }
                else
                {
                    FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Report Viewer Unload", MsgBoxStyle.Exclamation, "Error");
                }
            }
        }

        private void mnuEmailPDF_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                string strList = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                string fileName = string.Empty;
                FCSectionReport report = null;
                if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
                {
                    Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
                }
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Name + ".pdf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report name from frmReportViewer
                if (!(ARViewer21.ReportSource == null))
                {
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
                    report = ARViewer21.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                //FC:FINAL:RPU:#1363 - Use report.Document
                //a.Export(ARViewer21.ReportSource.Document, fileName);
                a.Export(report.Document, fileName);
                // FC:FINAL: RPU:#1363 - No more need for report
                report = null;
                if (!File.Exists(fileName))
                {
                    FCMessageBox.Show("Could not create report file to send as attachment", MsgBoxStyle.Exclamation, "Cannot E-mail");
                    return;
                }
                else
                {
                    if (Strings.Trim(strEmailAttachmentName) != string.Empty)
                    {
                        strList = fileName + ";" + strEmailAttachmentName + ".pdf";
                    }
                    else
                    {
                        strList = fileName;
                    }
                    frmEMail.InstancePtr.Init(strList, "", "", "", false, true, true, showAsModalForm: true);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailPDF_Click");
            }
        }

        private void mnuEMailRTF_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                string strList = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                string fileName = string.Empty;
                FCSectionReport report = null;
                if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
                {
                    Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
                }
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".rtf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report name from frmReportViewer
                if (!(ARViewer21.ReportSource == null))
                {
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".rtf";
                    report = ARViewer21.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                //FC:FINAL:RPU:#1363 - Use report.Document
                //a.Export(ARViewer21.ReportSource.Document, fileName);
                a.Export(report.Document, fileName);
                // FC:FINAL: RPU:#1363 - No more need for report
                report = null;
                if (!File.Exists(fileName))
                {
                    FCMessageBox.Show("Could not create report file to send as attachment", MsgBoxStyle.Exclamation, "Cannot E-mail");
                    return;
                }
                else
                {
                    if (Strings.Trim(strEmailAttachmentName) != string.Empty)
                    {
                        strList = fileName + ";" + strEmailAttachmentName + ".rtf";
                    }
                    else
                    {
                        strList = fileName;
                    }
                    frmEMail.InstancePtr.Init(strList, "", "", "", false, true, true, showAsModalForm: true);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailRTF_Click");
            }
        }

        private void mnuExcel_Click(object sender, System.EventArgs e)
        {
            try
            {
                string strOldDir;
                string strFileName = "";
                string strFullName = "";
                string strPathName = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                //strOldDir = Application.StartupPath;
                //Information.Err(ex).Clear();
                //App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
                //// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //App.MainForm.CommonDialog1.CancelError = true;
                ///*? On Error Resume Next  */
                //App.MainForm.CommonDialog1.FileName = "";
                //App.MainForm.CommonDialog1.Filter = "*.xls";
                //App.MainForm.CommonDialog1.DefaultExt = "xls";
                //App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
                //App.MainForm.CommonDialog1.ShowSave();
                //if (Information.Err(ex).Number == 0)
                //{
                //	strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
                //	// name without extension
                //	strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
                //	// name with extension
                //	strPathName = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
                //	if (Strings.Right(strPathName, 1) != "\\")
                //		strPathName += "\\";
                //	GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                //	if (!Directory.Exists(strPathName + strFileName))
                //	{
                //		Directory.CreateDirectory(strPathName + strFileName);
                //		strPathName += strFileName + "\\";
                //	}
                //	// a.FileName = .FileName
                //	//a.FileName = strPathName + strFullName;
                //	string fileName = strPathName = strFullName;
                //	//a.Version = 8;
                //	a.Export(ARViewer21.ReportSource.Document, fileName);
                //	if (!File.Exists(fileName))
                //	{
                //		FCMessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", MsgBoxStyle.Exclamation, "Not Successful");
                //		// MsgBox "No file created" & vbNewLine & "Export not successful", MsgBoxStyle.Exclamation, "Not Exported"
                //	}
                //	else
                //	{
                //		FileInfo fl = new FileInfo(fileName);
                //		if (FCConvert.ToInt32(fl.Length) > 0)
                //		{
                //			FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
                //		}
                //		else
                //		{
                //			FCMessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", MsgBoxStyle.Exclamation, "Not Exported");
                //		}
                //	}
                //}
                //else
                //{
                //	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
                //}
                //// ChDrive strOldDir
                //// ChDir strOldDir
                //modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
                GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                //string fileName = ARViewer21.ReportSource.Name + ".xls";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".xls";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".xls";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    // FC:FINAL: RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Excel export.", MsgBoxStyle.Critical, "Error");
            }
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuFileNextAccount_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
                if (frmCurrentAccountStatus.InstancePtr.rsAccountInfo.EndOfFile() == true)
                {
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MovePrevious();
                    return;
                }
                else
                {
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
                    if (frmCurrentAccountStatus.InstancePtr.rsAccountInfo.EndOfFile() == true)
                    {
                        mnuFileNextAccount.Visible = false;
                    }
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MovePrevious();
                    //rptCurrentAccountStatus3.InstancePtr.Hide();
                    Init(rptCurrentAccountStatus3.InstancePtr);
                    ARViewer21.Refresh();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Next Report function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuFilePreviousAccount_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MovePrevious();
                if (frmCurrentAccountStatus.InstancePtr.rsAccountInfo.BeginningOfFile() == true)
                {
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
                    return;
                }
                else
                {
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MovePrevious();
                    if (frmCurrentAccountStatus.InstancePtr.rsAccountInfo.BeginningOfFile() == true)
                    {
                        mnuFilePreviousAccount.Visible = false;
                    }
                    frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
                    //rptCurrentAccountStatus3.InstancePtr.Hide();
                    Init(rptCurrentAccountStatus3.InstancePtr);
                    ARViewer21.Refresh();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Previous Report function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuHTML_Click(object sender, System.EventArgs e)
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_ErrorHandler = 1;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                string strFileName = "";
                DialogResult intResp = 0;
                string strOldDir;
                string strFullName = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                //strOldDir = Application.StartupPath;
                //Information.Err(ex).Clear();
                //App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
                //// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //App.MainForm.CommonDialog1.CancelError = true;
                ///*? On Error Resume Next  */
                //App.MainForm.CommonDialog1.FileName = "";
                //App.MainForm.CommonDialog1.Filter = "*.htm";
                //App.MainForm.CommonDialog1.DefaultExt = "htm";
                //App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
                //App.MainForm.CommonDialog1.ShowSave();
                //if (Information.Err(ex).Number == 0)
                //{
                //	GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
                //	strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
                //	//a.FileNamePrefix = strFileName;
                //	strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
                //	string HTMLOutputPath = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
                //	//a"OutputPath = Path.GetDirectoryName(App.MainForm.CommonDialog1.FileName);
                //	//a.MHTOutput = false;
                //	a.MultiPage = false;
                //	if (ARViewer21.ReportSource.Document.Pages.Count > 1)
                //	{
                //		intResp = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
                //		if (intResp == DialogResult.No)
                //			a.MultiPage = true;
                //	}
                //	if (!Directory.Exists(HTMLOutputPath + strFileName))
                //	{
                //		Directory.CreateDirectory(HTMLOutputPath + strFileName);
                //		HTMLOutputPath = HTMLOutputPath + strFileName;
                //	}
                //	a.Export(ARViewer21.ReportSource.Document, HTMLOutputPath);
                //	FCMessageBox.Show("Export to file  " + HTMLOutputPath + strFullName + "  was completed successfully." + "\r\n" + "Please note that supporting files may also have been created in the same directory.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
                //}
                //else
                //{
                //	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
                //}
                //vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                ///* On Error GoTo ErrorHandler */// ChDrive strOldDir
                //// ChDir strOldDir
                //modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
                GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name);
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text);
                }
                //FC:FINAL:RPU:#1363 - Use the report variable
                //if (ARViewer21.ReportSource.Document.Pages.Count > 1)
                if (report.Document.Pages.Count > 1)
                {
                    {
                        DialogResult res = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
                        if (res == DialogResult.No)
                        {
                            a.MultiPage = true;
                        }
                    }
                    if (a.MultiPage)
                    {
                        string zipFilePath = GetTempFileName(".zip");
                        //FC:FINAL:RPU:#1363 - Concatenate just extension
                        //string fileName = ARViewer21.ReportSource.Name + ".zip";
                        fileName += ".zip";
                        var zip = fecherFoundation.ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
                        //FC:FINAL:RPU:#1363 - Use the report variable
                        //for (int i = 0; i < ARViewer21.ReportSource.Document.Pages.Count; i++)
                        for (int i = 0; i < report.Document.Pages.Count; i++)
                        {
                            string currentPage = (i + 1).ToString();
                            //string htmlFileName = ARViewer21.ReportSource.Name + "_Page" + currentPage + ".html";
                            string htmlFileName = report.Name + "_Page" + currentPage + ".html";
                            string tempFilePath = GetTempFileName(".html");
                            //a.Export(ARViewer21.ReportSource.Document, tempFilePath, currentPage);
                            a.Export(report.Document, tempFilePath, currentPage);
                            zip.CreateEntryFromFile(tempFilePath, htmlFileName, System.IO.Compression.CompressionLevel.Optimal);
                        }
                        zip.Dispose();
                        //FC:FINAL:DSE use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                        //FCUtils.Download(zipFilePath, fileName);
                        FCUtils.DownloadAndOpen("_blank", zipFilePath, fileName);
                        //FC:FINAL:RPU:#1363 - No more need for report
                        report = null;
                    }
                    else
                    {
                        //FC:FINAL:RPU:#1363 - Concatenate just extension
                        fileName += ".html";
                        string tempFilePath = GetTempFileName(".html");
                        //FC:FINAL:RPU:#1363 - Use the report variable
                        //a.Export(ARViewer21.ReportSource.Document, tempFilePath);
                        a.Export(report.Document, tempFilePath);
                        //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                        //FCUtils.Download(stream, fileName);
                        FCUtils.DownloadAndOpen("_blank", tempFilePath, fileName);
                        //FC:FINAL:RPU:#1363 - No more need for report
                        report = null;
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HTML export", MsgBoxStyle.Critical, "Error");
            }
        }

        private string GetTempFileName(string extension)
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            //string tempFolder = Path.Combine(Path.GetTempPath(), "Wisej", applicationName, "Temp", "PDF");
            string tempFolder = Path.Combine(Application.StartupPath, "Temp", "ReportViewer");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }

        private void mnuPrint_Click(object sender, System.EventArgs e)
        {
            if (ARViewer21.ReportSource != null)
            {
                if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                {
                    FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                    return;
                }
            }
            try
            {
                FCSectionReport report;
                if (ARViewer21.ReportSource != null)
                {
                    report = ARViewer21.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document?.Load(ARViewer21.ReportName);
                }
                report.ExportToPDFAndPrint();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuPrint", MsgBoxStyle.Critical, "Error");
            }
        }

        /*
        int intStartPage = 0;
        int intEndPage = 0;
        string strDuplex = "";
        int intStart = 0;
        // VBto upgrade warning: intPgs As short --> As int	OnWriteFCConvert.ToDouble(
        int intPgs = 0;
        int intEnd = 0;
        int intIndex;
        DialogResult intReturn = 0;
        string strPrinter = "";
        string strOldPrinter;
        // VBto upgrade warning: intOrientation As short --> As int	OnWrite(DDActiveReportsViewer2.PrtOrientation)
        int intOrientation;
        int x;
        FCPrinter tPrinter = new FCPrinter();
        strOldPrinter = "";
        if (!boolUseFilename)
        {
            // On Error Resume Next  
            if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
            {
                FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                return;
            }
        }
        try
        {
            // On Error GoTo ErrorHandler
            intOrientation = FCConvert.ToInt32(FCCommonDialog.PrinterOrientationConstants.cdlPortrait;
            if (!boolPrintDuplex)
            {
                // this is the automatic duplex printing that uses the GN settings
                if (boolShowPrintDialog)
                {
                    // show the dialog box manually
                    // strOldPrinter = ARViewer21.Document.Printer.PrinterName
                    tPrinter = modCustomPageSize.GetDefaultPrinter();
                    if (!(tPrinter == null))
                    {
                        strOldPrinter = modCustomPageSize.GetDefaultPrinter().DeviceName;
                    }
                    strPrinter = modDuplexPrinting.GetPrinterManually(ARViewer21.ReportSource.Document.Pages.Count, intStartPage, intEndPage, intCopies, boolCantChoosePages, intOrientation);
                    if (strPrinter == "")
                    {
                        return;
                    }
                    else
                    {
                        ARViewer21.Printer.PrinterName = strPrinter;
                        // ARViewer21.Printer.Copies = intCopies
                    }
                }
                else
                {
                    intStartPage = 1;
                    intEndPage = ARViewer21.ReportSource.Document.Pages.Count;
                    if (!boolCantChoosePages)
                    {
                        if (frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
                        {
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                strPrinter = ARViewer21.Printer.PrinterName;
                ARViewer21.Printer.PrinterSettings.FromPage = intStartPage;
                ARViewer21.Printer.PrinterSettings.ToPage = intEndPage;
                // ARViewer21.Printer.Orientation = intOrientation
                // check to see if the user wants to print duplex
                if (!boolAutoPrintDuplex)
                {
                    if (modDuplexPrinting.PrintDuplex(strPrinter))
                    {
                        if (pboolOverRideDuplex)
                        {
                            // MATTHEW 8/19/2005 CALL ID 75210
                            // TURN OFF DUPLEX PRINTING
                            ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                        }
                        else
                        {
                            if (modDuplexPrinting.Statics.gstrPaperFlipOption == "V")
                            {
                                ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                            }
                            else
                            {
                                ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
                            }
                        }
                        // ARViewer21.PrintReport (False)
                        ARViewer21.Printer.PrinterSettings.Copies = 1;
                        for (x = 1; x <= intCopies; x++)
                        {
                            //FC:FINAL:SBE - use Print() extension for Document
                            //ARViewer21.Printer.Print();
                            ARViewer21.PrintReport();
                        }
                        // x
                    }
                    else
                    {
                        if (pboolOverRideDuplex)
                        {
                            ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                        }
                        if (intOrientation >= 0)
                        {
                            //FC:TODO:AM
                            //ARViewer21.Printer.Orientation = intOrientation;
                        }
                        ARViewer21.Printer.PrinterSettings.Copies = 1;
                        for (x = 1; x <= intCopies; x++)
                        {
                            //FC:FINAL:SBE - use Print() extension for Document
                            //ARViewer21.Printer.Print();
                            ARViewer21.PrintReport();
                        }
                        // x
                    }
                }
                else
                {
                    if (modDuplexPrinting.Statics.gstrPaperFlipOption == "V")
                    {
                        ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                    }
                    else
                    {
                        ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
                    }
                    ARViewer21.Printer.PrinterSettings.Copies = 1;
                    for (x = 1; x <= intCopies; x++)
                    {
                        //FC:FINAL:SBE - use Print() extension for Document
                        //ARViewer21.Printer.Print();
                        ARViewer21.PrintReport();
                    }
                    // x
                    // ARViewer21.PrintReport (False)
                }
                modCustomPageSize.Statics.boolChangedDefault = true;
                modCustomPageSize.Statics.strDefaultPrinter = strOldPrinter;
                modCustomPageSize.SetDefaultPrinterWinNT();
                modCustomPageSize.Statics.boolChangedDefault = false;
            }
            else
            {
                // this is the forced duplex printing, so that the user can print on a non duplex printer
                // this is used in RE and PP to print property cards
                if (boolShowPrintDialog)
                {
                    //FC:TODO:AM
                    //if (!ARViewer21.Printer.SetupDialog(App.MainForm.Handle.ToInt32()))
                    //{
                    //    return;
                    //}
                }
                // have to see how many pages and check duplex info
                if (!boolAutoPrintDuplex)
                {
                    strDuplex = frmPrintDuplex.InstancePtr.Init();
                }
                else
                {
                    strDuplex = "FULL";
                }
                if (strDuplex != string.Empty)
                {
                    if (Strings.UCase(Strings.Trim(strDuplex)) == "NONE")
                    {
                        // print regular
                        ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                        intStartPage = 1;
                        intEndPage = ARViewer21.ReportSource.Document.Pages.Count;
                        if (!boolCantChoosePages)
                        {
                            if (frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
                            {
                                ARViewer21.Printer.PrinterSettings.FromPage = intStartPage;
                                ARViewer21.Printer.PrinterSettings.ToPage = intEndPage;
                                //FC:FINAL:SBE - use Print() extension for Document
                                //ARViewer21.Printer.Print();
                                ARViewer21.PrintReport();
                            }
                        }
                        else
                        {
                            //FC:FINAL:SBE - use Print() extension for Document
                            //ARViewer21.Printer.Print();
                            ARViewer21.PrintReport();
                        }
                        return;
                    }
                    else if (Strings.UCase(Strings.Trim(strDuplex)) == "FULL")
                    {
                        // printer can handle duplex by itself
                        // print regular but set printer to duplex
                        ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                        if (modDuplexPrinting.Statics.gstrPaperFlipOption == "V")
                        {
                            ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                        }
                        else
                        {
                            ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
                        }
                        intStartPage = 1;
                        intEndPage = ARViewer21.ReportSource.Document.Pages.Count;
                        if (!boolCantChoosePages)
                        {
                            if (frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
                            {
                                ARViewer21.Printer.PrinterSettings.FromPage = intStartPage;
                                ARViewer21.Printer.PrinterSettings.ToPage = intEndPage;
                                //FC:FINAL:SBE - use Print() extension for Document
                                //ARViewer21.Printer.Print();
                                ARViewer21.PrintReport();
                            }
                        }
                        else
                        {
                            //FC:FINAL:SBE - use Print() extension for Document
                            //ARViewer21.Printer.Print();
                            ARViewer21.PrintReport();
                        }
                        return;
                    }
                    else if (Strings.UCase(Strings.Trim(strDuplex)) == "MANUALTOPISLAST")
                    {
                        // when flipped, must reverse pages
                        // move all even pages behind all odd pages and reverse even pages order
                        ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                        intStart = 1;
                        if (boolDuplexForcePairs)
                        {
                            intPgs = FCConvert.ToInt32(ARViewer21.ReportSource.Document.Pages.Count / 2);
                            if (!boolCantChoosePages)
                            {
                                if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
                                {
                                    // these will be the pairs to print (such as cards etc), not pages
                                    intEnd = intPgs;
                                    intStart = (intStart * 2) - 1;
                                    // front side of first card to print
                                    intEnd = (intEnd * 2);
                                    // second side of last card to print
                                }
                            }
                            else
                            {
                                intEnd = intPgs;
                                intStart = (intStart * 2) - 1;
                                // front side of first card to print
                                intEnd = (intEnd * 2);
                                // second side of last card to print
                            }
                        }
                        else
                        {
                            intPgs = ARViewer21.ReportSource.Document.Pages.Count;
                            if (!boolCantChoosePages)
                            {
                                if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
                                {
                                    // these will be the pages to print
                                    intEnd = intPgs;
                                }
                            }
                            else
                            {
                                intEnd = intPgs;
                            }
                        }
                        // print odds first
                        //FC:FINAL:SBE - use Print() extension for Document
                        //ARViewer21.Printer.Print();
                        ARViewer21.PrintReport();
                        //FC:TODO:AM
                        //for (intIndex = intStart; intIndex <= intEnd; intIndex += 2)
                        //{
                        //    // this will print odds
                        //    ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
                        //} // intIndex
                        //ARViewer21.Printer.EndJob();
                        //if (intEnd > intStart)
                        //{
                        //    // check to make sure there is more than one page
                        //    // now print evens
                        //    intReturn = FCMessageBox.Show("When " + strDuplexLabel + " are done printing, replace them in the printer so the back sides will print and click OK.", MsgBoxStyle.OkCancel, "Print second side(s)");
                        //    if (intReturn != DialogResult.Cancel)
                        //    {
                        //        ARViewer21.Printer.StartJob(this.Text);
                        //        for (intIndex = intEnd; intIndex >= intStart; intIndex += -2)
                        //        {
                        //            // this will print evens
                        //            ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
                        //        } // intIndex
                        //        ARViewer21.Printer.EndJob();
                        //    }
                        //}
                    }
                    else if (Strings.UCase(Strings.Trim(strDuplex)) == "MANUALTOPISFIRST")
                    {
                        // no need to reverse pages
                        // move all even pages behind all odd pages
                        ARViewer21.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                        intStart = 1;
                        if (boolDuplexForcePairs)
                        {
                            intPgs = FCConvert.ToInt32(ARViewer21.ReportSource.Document.Pages.Count / 2);
                            if (!boolCantChoosePages)
                            {
                                if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
                                {
                                    // these will be the *CARDS* to print, not pages
                                    intEnd = intPgs;
                                    intStart = (intStart * 2) - 1;
                                    // front side of first card to print
                                    intEnd = (intEnd * 2);
                                    // second side of last card to print
                                }
                            }
                            else
                            {
                                intEnd = intPgs;
                                intStart = (intStart * 2) - 1;
                                // front side of first card to print
                                intEnd = (intEnd * 2);
                                // second side of last card to print
                            }
                        }
                        else
                        {
                            intPgs = ARViewer21.ReportSource.Document.Pages.Count;
                            if (!boolCantChoosePages)
                            {
                                if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
                                {
                                    // These will the the pages to print
                                    intEnd = intPgs;
                                }
                            }
                            else
                            {
                                intEnd = intPgs;
                            }
                        }
                        // print odds first
                        //FC:FINAL:SBE - use Print() extension for Document
                        //ARViewer21.Printer.Print();
                        ARViewer21.PrintReport();
                        //FC:TODO:AM
                        //for (intIndex = intStart; intIndex <= intEnd; intIndex += 2)
                        //{
                        //    // this will print odds
                        //    ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
                        //} // intIndex
                        //ARViewer21.Printer.EndJob();
                        //if (intEnd > intStart)
                        //{
                        //    // only do this if there is more than one page
                        //    // now print evens
                        //    intReturn = FCMessageBox.Show("When " + strDuplexLabel + " are done printing, replace them in the printer so the back sides will print and click OK.", MsgBoxStyle.OkCancel, "Print second side(s)");
                        //    if (intReturn != DialogResult.Cancel)
                        //    {
                        //        ARViewer21.Printer.StartJob(this.Text);
                        //        for (intIndex = intStart + 1; intIndex <= intEnd; intIndex += 2)
                        //        {
                        //            // this will print evens
                        //            ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
                        //        } // intIndex
                        //        ARViewer21.Printer.EndJob();
                        //    }
                        //}
                    }
                }
            }
            return;
        }*/

        private void mnuRTF_Click(object sender, System.EventArgs e)
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_ErrorHandler = 1;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                string strOldDir;
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                strOldDir = Application.StartupPath;
                Information.Err().Clear();
                //FC:FINAL:AM: download instead the file
                //App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
                //// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //App.MainForm.CommonDialog1.CancelError = true;
                ///*? On Error Resume Next  */
                //App.MainForm.CommonDialog1.FileName = "";
                //App.MainForm.CommonDialog1.Filter = "*.rtf";
                //App.MainForm.CommonDialog1.DefaultExt = "rtf";
                //App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
                //App.MainForm.CommonDialog1.ShowSave();
                //if (Information.Err(ex).Number == 0)
                //{
                //GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                //string fileName = App.MainForm.CommonDialog1.FileName;                    
                //a.Export(ARViewer21.ReportSource.Document, fileName);
                //if (!File.Exists(fileName))
                //{
                //                   // If Left(UCase(fso.GetDriveName(a.FileName)), 1) = "A" Then
                //	FCMessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", MsgBoxStyle.Exclamation, "Not Exported");
                //	// Else
                //	// MsgBox "No file created" & vbNewLine & "Export not successful", MsgBoxStyle.Exclamation, "Not Exported"
                //	// End If
                //}
                //else
                //{
                //	FileInfo fl = new FileInfo(fileName);
                //	if (FCConvert.ToInt32(fl.Length) > 0)
                //	{
                //		FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
                //	}
                //	else
                //	{
                //		FCMessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", MsgBoxStyle.Exclamation, "Not Exported");
                //	}
                //}
                //}
                //else
                //{
                //	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
                //}
                //vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                /* On Error GoTo ErrorHandler */// ChDrive strOldDir
                                                // ChDir strOldDir
                                                //modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
                GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                //string fileName = ARViewer21.ReportSource.Name + ".rtf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".rtf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In RTF export", MsgBoxStyle.Critical, "Error");
            }
        }

        private void mnuExportPDF_Click(object sender, System.EventArgs e)
        {
            try
            {
                string strOldDir;
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(ARViewer21.ReportSource == null))
                {
                    if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                //strOldDir = Application.StartupPath;
                //Information.Err(ex).Clear();
                //App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
                //// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //App.MainForm.CommonDialog1.CancelError = true;
                ///*? On Error Resume Next  */
                //App.MainForm.CommonDialog1.FileName = "";
                //App.MainForm.CommonDialog1.Filter = "*.pdf";
                //App.MainForm.CommonDialog1.DefaultExt = "pdf";
                //App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
                //App.MainForm.CommonDialog1.ShowSave();
                //if (Information.Err(ex).Number == 0)
                //{
                //	GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                //	string fileName = App.MainForm.CommonDialog1.FileName;
                //	a.Export(ARViewer21.ReportSource.Document, fileName);
                //	if (!File.Exists(fileName))
                //	{
                //		// MsgBox "No file created" & vbNewLine & "Export not successful", MsgBoxStyle.Exclamation, "Not Exported"
                //		FCMessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", MsgBoxStyle.Exclamation, "Not Exported");
                //	}
                //	else
                //	{
                //		FileInfo fl = new FileInfo(fileName);
                //		if (FCConvert.ToInt32(fl.Length) > 0)
                //		{
                //			FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
                //		}
                //		else
                //		{
                //			FCMessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", MsgBoxStyle.Exclamation, "Not Exported");
                //		}
                //	}
                //}
                //else
                //{
                //	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
                //}
                //// ChDrive strOldDir
                //// ChDir strOldDir
                //modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                //string fileName = ARViewer21.ReportSource.Name + ".pdf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                //a.Export(ARViewer21.ReportSource.Document, Path.Combine(Application.StartupPath,fileName));
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In PDF export", MsgBoxStyle.Critical, "Error");
            }
        }

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == toolBarButtonEmailPDF)
            {
                mnuEmailPDF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonEmailRTF)
            {
                mnuEMailRTF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportPDF)
            {
                mnuExportPDF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportRTF)
            {
                mnuRTF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportHTML)
            {
                mnuHTML_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportExcel)
            {
                mnuExcel_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonPrint)
            {
                mnuPrint_Click(e.Button, EventArgs.Empty);
            }
        }
    }
}
