﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEOYProcessing.
	/// </summary>
	public partial class frmEOYProcessing : BaseForm
	{
        private List<string> batchReports;

        public frmEOYProcessing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: add code from Load
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			//FC:FINAL:ASZ: default selection - All
			cmbNo.SelectedIndex = 0;
			backrest.BackupCompleted += new BackupRestore.BackupCompletedEventHandler(backrest_BackupCompleted);
			backrest.RestoreCompleted += new BackupRestore.RestoreCompletedEventHandler(backrest_RestoreCompleted);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEOYProcessing InstancePtr
		{
			get
			{
				return (frmEOYProcessing)Sys.GetInstance(typeof(frmEOYProcessing));
			}
		}

		protected frmEOYProcessing _InstancePtr = null;

		public int lngClosingJournal;
		clsDRWrapper rsEOYProcess = new clsDRWrapper();
		int SelectCol;
		int DescriptionCol;
		int AccountCol;
		int KeyCol;
		int AmountCol;
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		clsDRWrapper rsLedgerActivityDetail = new clsDRWrapper();
		clsDRWrapper rsExpDeptYTDActivity = new clsDRWrapper();
		clsDRWrapper rsExpDivYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevDeptYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevDivYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevRevYTDActivity = new clsDRWrapper();

		private TableItem YTDExpActivity;
		private TableItem YTDExpActivityDetail;
		private TableItem YTDRevActivity;
		private TableItem YTDRevActivityDetail;
		private TableItem YTDLedgerActivity;
		private TableItem YTDLedgerActivityDetail;

		string strCloseoutYear = "";
		BackupRestore backrest = new BackupRestore();
		// vbPorter upgrade warning: strYear As string	OnWrite(double, string)
		string strYear = "";
		int VendorNumberCol;
		int TempVendorNameCol;
		int TempVendorAddress1Col;
		int TempVendorAddress2Col;
		int TempVendorAddress3Col;
		int TempVendorAddress4Col;
		int TempVendorCityCol;
		int TempVendorStateCol;
		int TempVendorZipCol;
		int TempVendorZip4Col;
		int POCol;
		bool blnBDCompleted;
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo;
		string strLastStep = "";
		bool boolArchiveErrors;
		string strLastArchiveError = "";
		//ASZ:?
		//AbaleZipLibrary.AbaleZip Zip1 = new AbaleZipLibrary.AbaleZip();
		private void backrest_BackupCompleted(object sender, BackupCompletedArgs tArgs)
		{
			clsDRWrapper temp = new clsDRWrapper();
			string strBackupPath = "";
			if (Strings.LCase(strLastStep) == "budgetary_back")
			{
				if (tArgs.Success)
				{
					backrest = new BackupRestore();
					backrest.BackupCompleted += new BackupRestore.BackupCompletedEventHandler(backrest_BackupCompleted);
					backrest.RestoreCompleted += new BackupRestore.RestoreCompletedEventHandler(backrest_RestoreCompleted);
					strLastStep = "CentralData_Back";
					backrest.Backup(temp.Get_ConnectionInformation("CentralData"));
				}
				else
				{
					boolArchiveErrors = true;
					LogError(tArgs.ErrorMessage);
					MessageBox.Show("Error archiving budgetary" + "\r\n" + tArgs.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else if (Strings.LCase(strLastStep) == "centraldata_back")
			{
				if (tArgs.Success)
				{
					backrest = new BackupRestore();
					backrest.BackupCompleted += new BackupRestore.BackupCompletedEventHandler(backrest_BackupCompleted);
					backrest.RestoreCompleted += new BackupRestore.RestoreCompletedEventHandler(backrest_RestoreCompleted);
					strLastStep = "CentralDocuments_Back";
					backrest.Backup(temp.Get_ConnectionInformation("CentralDocuments"));
				}
				else
				{
					boolArchiveErrors = true;
					LogError(tArgs.ErrorMessage);
					MessageBox.Show("Error archiving central data" + "\r\n" + tArgs.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else if (Strings.LCase(strLastStep) == "centraldocuments_back")
			{
				if (tArgs.Success)
				{
					blnBDCompleted = false;
					strBackupPath = backrest.GetBackupPath(temp.Get_ConnectionInformation("Budgetary")) + "\\" + temp.Get_GetFullDBName("Budgetary") + ".bak";
					strLastStep = "Budgetary_Restore";
					backrest.Restore(temp.Get_ConnectionInformation("Budgetary"), "TRIO_" + temp.MegaGroup + "_Archive_" + strYear + "_" + "Budgetary", strBackupPath, temp.Get_GetFullDBName("Budgetary"));
				}
				else
				{
					boolArchiveErrors = true;
					LogError(tArgs.ErrorMessage);
					MessageBox.Show("Error archiving central documents" + "\r\n" + tArgs.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}

		}

		private void backrest_RestoreCompleted(object sender, RestoreCompletedArgs tArgs)
		{
			clsDRWrapper rsArchive = new clsDRWrapper();
			clsDRWrapper temp = new clsDRWrapper();
			string strBackupPath = "";
			if (Strings.LCase(strLastStep) == "budgetary_restore")
			{
				if (tArgs.Success)
				{
					strBackupPath = backrest.GetBackupPath(temp.Get_ConnectionInformation("CentralData")) + "\\" + temp.Get_GetFullDBName("CentralData") + ".bak";
					strLastStep = "CentralData_Restore";
					backrest.Restore(temp.Get_ConnectionInformation("CentralData"), "TRIO_" + temp.MegaGroup + "_Archive_" + strYear + "_" + "CentralData", strBackupPath, temp.Get_GetFullDBName("CentralData"));
				}
				else
				{
					boolArchiveErrors = true;
					LogError(tArgs.ErrorMessage);
					MessageBox.Show("Error restoring budgetary archive" + "\r\n" + tArgs.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else if (Strings.LCase(strLastStep) == "centraldata_restore")
			{
				if (tArgs.Success)
				{
					strBackupPath = backrest.GetBackupPath(temp.Get_ConnectionInformation("CentralDocuments")) + "\\" + temp.Get_GetFullDBName("CentralDocuments") + ".bak";
					strLastStep = "CentralDocuments_Restore";
					backrest.Restore(temp.Get_ConnectionInformation("CentralDocuments"), "TRIO_" + temp.MegaGroup + "_Archive_" + strYear + "_" + "CentralDocuments", strBackupPath, temp.Get_GetFullDBName("CentralDocuments"));
				}
				else
				{
					boolArchiveErrors = true;
					LogError(tArgs.ErrorMessage);
					MessageBox.Show("Error restoring central data archive" + "\r\n" + tArgs.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else if (Strings.LCase(strLastStep) == "centraldocuments_restore")
			{
				if (tArgs.Success)
				{
				}
				else
				{
					boolArchiveErrors = true;
					LogError(tArgs.ErrorMessage);
					MessageBox.Show("Error restoring central documents archive" + "\r\n" + tArgs.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				if (boolArchiveErrors)
				{
					if (MessageBox.Show("There were errors creating the archive." + "\r\n" + "You should only continue if you have manually created the archive or have just backed up the databases to allow creating the archive." + "\r\n" + "Do you want to continue?", "Archive Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
					{
						return;
					}
					modGlobalFunctions.AddCYAEntry_8("BD", "Continued EOY without creating archive");
				}
				rsArchive.Execute("UPDATE CheckRecMaster SET Transferred = 1", "Budgetary");
				//Application.DoEvents();
				rsArchive.Execute("UPDATE CheckRecArchive SET Transferred = 1", "Budgetary");
				//Application.DoEvents();
				modGlobalFunctions.LoadSQLConfig();
				rsArchive.GroupName = "Archive_" + strYear;
				rsArchive.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				rsArchive.Edit();
				rsArchive.Set_Fields("ArchiveYear", Strings.Right(strYear, 2));
				rsArchive.Update(true);
				imgCreateArchiveProcessing.Visible = false;
				imgCreateArchive.Visible = true;
				this.Refresh();
				FCUtils.ApplicationUpdate(this);
				rsEOYProcess.Edit();
				rsEOYProcess.Set_Fields("CreateArchive", true);
				rsEOYProcess.Update(true);
				rsArchive.OpenRecordset("SELECT * FROM Archives WHERE ID = 0", "SystemSettings");
				rsArchive.AddNew();
				rsArchive.Set_Fields("ArchiveType", "Archive");
				rsArchive.Set_Fields("ArchiveID", FCConvert.ToString(Conversion.Val(strYear)));
				rsArchive.Update();
				rsArchive.Execute("Delete from [Documents] where not ReferenceGroup = 'Budgetary'", "CentralDocuments");
				ContinueEOY();
			}
			// 
			// If Not blnBDCompleted Then
			// blnBDCompleted = True
			// strBackupPath = backrest.GetBackupPath(temp.ConnectionInformation("CentralData")) & "\" & temp.GetFullDBName("CentralData") & ".bak"
			// 
			// Call backrest.Restore(temp.ConnectionInformation("CentralData"), "TRIO_" & temp.MegaGroup & "_Archive_" & strYear & "_" & "CentralData", strBackupPath, temp.GetFullDBName("CentralData"))
			// Else
			// rsArchive.Execute "UPDATE CheckRecMaster SET Transferred = 1", "Budgetary"
			// DoEvents
			// rsArchive.Execute "UPDATE CheckRecArchive SET Transferred = 1", "Budgetary"
			// DoEvents
			// 
			// rsArchive.GroupName = "Archive_" & strYear
			// rsArchive.OpenRecordset "SELECT * FROM GlobalVariables", "SystemSettings"
			// rsArchive.Edit
			// rsArchive.Fields["ArchiveYear"] = Right(strYear, 2)
			// rsArchive.Update True
			// imgCreateArchiveProcessing.Visible = False
			// imgCreateArchive.Visible = True
			// Me.Refresh
			// 
			// rsEOYProcess.Edit
			// rsEOYProcess.Fields["CreateArchive"] = True
			// rsEOYProcess.Update True
			// 
			// rsArchive.OpenRecordset "SELECT * FROM Archives WHERE ID = 0", "SystemSettings"
			// rsArchive.AddNew
			// rsArchive.Fields["ArchiveType"] = "Archive"
			// rsArchive.Fields["ArchiveID"] = Val(strYear)
			// rsArchive.Update
			// 
			// ContinueEOY
			// End If
		}

		private void ContinueEOY()
		{
			int ans;
			clsDRWrapper rsInfo = new clsDRWrapper();
			SettingsInfo s = new SettingsInfo();
			try
			{
                // On Error GoTo ErrorHandler
                if (FCConvert.ToInt32(strYear) == 0)
                {
                    strYear = GetLatestArchiveYear().ToString();
                }
				imgCloseUnliquidatedProcessing.Visible = true;
				this.Refresh();
				FCUtils.ApplicationUpdate(this);

				modBudgetaryAccounting.CalculateAccountInfo();

				rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account");
				rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
				rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account");
				rsActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account, Period");
				rsLedgerActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account, Period");
				rsRevActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account, Period");
				rsExpDeptYTDActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department");
				rsExpDivYTDActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department, Division");
				rsRevDeptYTDActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Department");
				rsRevDivYTDActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Department, Division");
				rsRevRevYTDActivity.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Department, Division, Revenue");

				YTDExpActivity = rsYTDActivity.TableToTableItem();
				YTDExpActivityDetail = rsActivityDetail.TableToTableItem();
				YTDRevActivity = rsRevYTDActivity.TableToTableItem();
				YTDRevActivityDetail = rsRevActivityDetail.TableToTableItem();
				YTDLedgerActivity = rsLedgerYTDActivity.TableToTableItem();
				YTDLedgerActivityDetail = rsLedgerActivityDetail.TableToTableItem();

				CalculateEndingBalances();
				if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("CloseUnliquidated")))
				{
					if (!CloseUnliquidated(batchReports: batchReports))
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("CloseUnliquidated", true);
						rsEOYProcess.Update(true);
					}
				}
				rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
				rsLedgerActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account, Period");
				if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("CloseControl")))
				{
					if (!CloseControls())
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("CloseControl", true);
						rsEOYProcess.Update(true);
					}
				}
				if (lngClosingJournal != 0)
				{
					if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("PostClosing")))
					{
						if (!PostClosingJournal(batchReports: batchReports))
						{
							return;
						}
						else
						{
							rsEOYProcess.Edit();
							rsEOYProcess.Set_Fields("PostClosing", true);
							rsEOYProcess.Update(true);
						}
					}
				}
				else
				{
					imgPostClosingProcessing.Visible = false;
					imgPostClosing.Visible = true;
					this.Refresh();
					FCUtils.ApplicationUpdate(this);
					rsEOYProcess.Edit();
					rsEOYProcess.Set_Fields("PostClosing", true);
					rsEOYProcess.Update(true);
				}
				imgSave1099Processing.Visible = true;
				this.Refresh();
				FCUtils.ApplicationUpdate(this);
				//Application.DoEvents();
				rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
				rsLedgerActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account, Period");
				if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("Save1099")))
				{
					if (!Save1099())
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("Save1099", true);
						rsEOYProcess.Update(true);
					}
				}
				if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("ClearBudgets")))
				{
					if (!ClearBudgetAmounts())
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("ClearBudgets", true);
						rsEOYProcess.Update(true);
					}
				}
				if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("ResetNumbers")))
				{
					if (!ResetJournalWarrant())
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("ResetNumbers", true);
						rsEOYProcess.Update(true);
					}
				}
				if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("ClearDetail")))
				{
					if (!ClearDetailAmounts())
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("ClearDetail", true);
						rsEOYProcess.Update(true);
					}
				}
				if (cmbNo.SelectedIndex == 1 && vsEncumbrances.Rows > 0)
				{
					if (!CarryOver(batchReports: batchReports))
					{
						return;
					}
					else
					{
						rsEOYProcess.Edit();
						rsEOYProcess.Set_Fields("CarryOver", true);
						rsEOYProcess.Update(true);
					}
				}
				else
				{
					rsEOYProcess.Edit();
					rsEOYProcess.Set_Fields("CarryOver", true);
					rsEOYProcess.Update(true);
					imgCarryOver.Visible = true;
					this.Refresh();
					FCUtils.ApplicationUpdate(this);
				}
				frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Recalculating Account Summary Information", true);
				frmWait.InstancePtr.Show();
				FCUtils.ApplicationUpdate(this);
				this.Refresh();
				FCUtils.ApplicationUpdate(this);
				// troges126
				modBudgetaryAccounting.CalculateAccountInfo("" ,true);
				
				frmWait.InstancePtr.prgProgress.Value = 80;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Finalizing Account Information";
                frmWait.InstancePtr.Refresh();
                FCUtils.ApplicationUpdate(this);
				//Application.DoEvents();
				this.Refresh();
				
				frmWait.InstancePtr.Unload();
				/*? On Error GoTo 0 */
				rsEOYProcess.OpenRecordset("SELECT * FROM EOYProcess");
				rsEOYProcess.Edit();
				rsEOYProcess.Set_Fields("CreateArchive", false);
				rsEOYProcess.Set_Fields("CloseControl", false);
				rsEOYProcess.Set_Fields("PostClosing", false);
				rsEOYProcess.Set_Fields("Save1099", false);
				rsEOYProcess.Set_Fields("ClearBudgets", false);
				rsEOYProcess.Set_Fields("ResetNumbers", false);
				rsEOYProcess.Set_Fields("ClearDetail", false);
				rsEOYProcess.Set_Fields("CloseUnliquidated", false);
				rsEOYProcess.Set_Fields("CarryOver", false);
				rsEOYProcess.Set_Fields("EOYCompletedDate", DateTime.Today);
				rsEOYProcess.Update(true);
				if (!s.LoadArchives())
				{
					MessageBox.Show("Error loading archive information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				MessageBox.Show("End of year process completed successfully", "Completed EOY", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			int counter;
			fraEncumbrances.Visible = false;
			for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
			{
				if (vsEncumbrances.RowOutlineLevel(counter) == 1)
				{
					vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
				}
			}
			cmbNo.SelectedIndex = 0;
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			ans = MessageBox.Show("Are you sure you have selected all the encumbrance entries you wish to carry forward?", "Selection Complete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.No)
			{
				return;
			}
			else
			{
				fraEncumbrances.Visible = false;
			}
		}

        private bool VerifyProcessRun()
        {
            DateTime firstDayOfFiscalYear;

            if (modBudgetaryMaster.Statics.FirstMonth == 1)
            {
                if (DateTime.Today.Month == 12)
                {
                    firstDayOfFiscalYear = new DateTime(DateTime.Today.Year + 1, modBudgetaryMaster.Statics.FirstMonth, 1);
				}
                else
                {
					firstDayOfFiscalYear = new DateTime(DateTime.Today.Year, modBudgetaryMaster.Statics.FirstMonth, 1);
				}
            }
            else
            {
				firstDayOfFiscalYear = new DateTime(DateTime.Today.Year, modBudgetaryMaster.Statics.FirstMonth, 1);
			}

            if (DateTime.Today >= firstDayOfFiscalYear.AddDays(-14) &&
                DateTime.Today <= firstDayOfFiscalYear.AddDays(14))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

		private void cmdProcess_Click(object sender, System.EventArgs e)
        {
            LockCloseDuringLongProcess();
            //FC:FINAL:SBE - start async process to avoid response timeout
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    // vbPorter upgrade warning: ans As short, int --> As DialogResult
                    DialogResult ans;
                    clsDRWrapper rsInfo = new clsDRWrapper();
                    clsDRWrapper rsAPCheck = new clsDRWrapper();

                    rsEOYProcess.OpenRecordset("SELECT * FROM EOYProcess");
                    if (VerifyProcessRun())
                    {
                        DateTime dt = new DateTime(DateTime.Today.Year, modBudgetaryMaster.Statics.FirstMonth, 1); ;

						ans = MessageBox.Show("Your first fiscal month is set to " + dt.ToString("MMMM") + ".  The Fiscal End of Year process should only be run at the end of your fiscal year.  Running this outside of your normal fiscal year may cause reporting issues.  Do you wish to continue?", "Process EOY", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (ans == DialogResult.No)
                        {
                            return;
                        }
                        else
                        {
                            modBudgetaryMaster.WriteAuditRecord_8("Processing End of Year outside of normal date range.", "End of Year Processing");
                        }
					}
					
                    rsInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Status <> 'P' AND Status <> 'D' AND Status <> 'E' and Status <> 'V' AND Type = 'AP'");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        MessageBox.Show("There are one or more journals that are part way through the AP process.  You must finish the AP process and post your journal(s) before you may continue with this process.", "Post Journals", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    rsInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AP'");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        do
                        {
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            rsAPCheck.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + rsInfo.Get_Fields("JournalNumber") + " AND PrintedIndividual = 1", "TWBD0000.vb1");
                            if (rsAPCheck.EndOfFile() != true && rsAPCheck.BeginningOfFile() != true)
                            {
                                MessageBox.Show("There are one or more journals that are part way through the AP process.  You must finish the AP process and post your journal(s) before you may continue with this process.", "Post Journals", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            rsInfo.MoveNext();
                        }
                        while (rsInfo.EndOfFile() != true);
                    }
                    rsInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Status <> 'P' AND Status <> 'D'");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        ans = MessageBox.Show("There are one or more journals that have not been posted yet.  Would you like them brought over to the new fiscal period?", "Unposted Journals", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.No)
                        {
                            MessageBox.Show("You must post your journals before you may continue with this process.", "Post Journals", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (cmbNo.SelectedIndex == 1)
                    {
                        ans = MessageBox.Show("Are you sure you have selected all the encumbrances you wish to carry over?", "Encumbrances Selected?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.No)
                        {
                            fraEncumbrances.Visible = true;
                            return;
                        }
                    }
                    ans = MessageBox.Show("Make sure you have a backup of your Budgetary database before running this process.  Do you wish to continue?", "Process EOY?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        return;
                    }
                    try
                    {
                        // On Error GoTo ErrorHandler
                        batchReports = new List<string>();
                        if (!FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("CreateArchive")))
                        {
                            CreateArchive();
                        }
                        else
                        {
                            ContinueEOY();
                        }

                        Global.Extensions.CombineAndPrintPDFs(batchReports: batchReports);
                        return;
                    }
                    catch (Exception ex)
                    {
                        // ErrorHandler:
                        MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
                finally
                {
                    UnlockCloseDuringLongProcess();
                }
            });
        }

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
			{
				if (vsEncumbrances.RowOutlineLevel(counter) == 1)
				{
					vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
			{
				if (vsEncumbrances.RowOutlineLevel(counter) == 1)
				{
					vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void frmEOYProcessing_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			KeyCol = 1;
			SelectCol = 2;
			VendorNumberCol = 3;
			TempVendorNameCol = 4;
			TempVendorAddress1Col = 5;
			TempVendorAddress2Col = 6;
			TempVendorAddress3Col = 7;
			TempVendorAddress4Col = 8;
			TempVendorCityCol = 9;
			TempVendorStateCol = 10;
			TempVendorZipCol = 11;
			TempVendorZip4Col = 12;
			POCol = 13;
			DescriptionCol = 14;
			AccountCol = 15;
			AmountCol = 16;
			vsEncumbrances.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsEncumbrances.ColWidth(KeyCol, 0);
			vsEncumbrances.ColHidden(VendorNumberCol, true);
			vsEncumbrances.ColHidden(TempVendorNameCol, true);
			vsEncumbrances.ColHidden(TempVendorAddress1Col, true);
			vsEncumbrances.ColHidden(TempVendorAddress2Col, true);
			vsEncumbrances.ColHidden(TempVendorAddress3Col, true);
			vsEncumbrances.ColHidden(TempVendorAddress4Col, true);
			vsEncumbrances.ColHidden(TempVendorCityCol, true);
			vsEncumbrances.ColHidden(TempVendorStateCol, true);
			vsEncumbrances.ColHidden(TempVendorZipCol, true);
			vsEncumbrances.ColHidden(TempVendorZip4Col, true);
			vsEncumbrances.ColHidden(POCol, true);
			vsEncumbrances.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsEncumbrances.ColWidth(KeyCol, 0);
			vsEncumbrances.ColWidth(0, 200);
			//FC:FINAL:MSH - issue #836: change size column for correct displaying checkboxes
			//vsEncumbrances.ColWidth(SelectCol, 400);
			vsEncumbrances.ColWidth(SelectCol, 1000);
			vsEncumbrances.ColWidth(DescriptionCol, 2500);
			vsEncumbrances.ColWidth(AccountCol, 2500);
			vsEncumbrances.ColWidth(AmountCol, 1550);
			rsAccountInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P' AND Description <> 'Control Entries' AND (Amount + Adjustments - Liquidated) <> 0");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					vsEncumbrances.Rows += 1;
					//FC:FINAL:MSH - issue #836: change BackColor and ForeColor for rows
					//vsEncumbrances.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsEncumbrances.Rows - 1, 0, vsEncumbrances.Rows - 1, vsEncumbrances.Cols - 1, 0x80000017);
					//vsEncumbrances.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsEncumbrances.Rows - 1, 0, vsEncumbrances.Rows - 1, vsEncumbrances.Cols - 1, 0x80000018);
					vsEncumbrances.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsEncumbrances.Rows - 1, 0, vsEncumbrances.Rows - 1, vsEncumbrances.Cols - 1, Color.Black);
					vsEncumbrances.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsEncumbrances.Rows - 1, 0, vsEncumbrances.Rows - 1, vsEncumbrances.Cols - 1, Color.White);
					//FC:FINAL:MSH - issue #836: merge cells, which we need
					//vsEncumbrances.MergeRow(vsEncumbrances.Rows - 1, true);
					vsEncumbrances.MergeRow(vsEncumbrances.Rows - 1, true, DescriptionCol, AmountCol);
					vsEncumbrances.RowOutlineLevel(vsEncumbrances.Rows - 1, 0);
					vsEncumbrances.IsSubtotal(vsEncumbrances.Rows - 1, true);
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, KeyCol, FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("ID")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, VendorNumberCol, FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("VendorNumber")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorNameCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorName")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorAddress1Col, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorAddress1")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorAddress2Col, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorAddress2")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorAddress3Col, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorAddress3")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorAddress4Col, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorAddress4")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorCityCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorCity")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorStateCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorState")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorZipCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorZip")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, TempVendorZip4Col, FCConvert.ToString(rsAccountInfo.Get_Fields_String("TempVendorZip4")));
					// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, POCol, FCConvert.ToString(rsAccountInfo.Get_Fields("PO")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, DescriptionCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("Description")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, AccountCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("Description")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, AmountCol, FCConvert.ToString(rsAccountInfo.Get_Fields_String("Description")));
					rsDetailInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + rsAccountInfo.Get_Fields_Int32("ID") + " AND (Amount + Adjustments - Liquidated) <> 0");
					if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
					{
						do
						{
							vsEncumbrances.Rows += 1;
							vsEncumbrances.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsEncumbrances.Rows - 1, 0, vsEncumbrances.Rows - 1, vsEncumbrances.Cols - 1, Color.White);
							vsEncumbrances.RowOutlineLevel(vsEncumbrances.Rows - 1, 1);
							vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, KeyCol, FCConvert.ToString(rsDetailInfo.Get_Fields_Int32("ID")));
							vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, SelectCol, FCConvert.ToString(false));
							vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, AccountCol, FCConvert.ToString(rsDetailInfo.Get_Fields("Account")));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, AmountCol, Strings.Format(rsDetailInfo.Get_Fields("Amount") + rsDetailInfo.Get_Fields_Decimal("Adjustments") - rsDetailInfo.Get_Fields_Decimal("Liquidated"), "0.00"));
							rsDetailInfo.MoveNext();
						}
						while (rsDetailInfo.EndOfFile() != true);
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
			{
				if (vsEncumbrances.RowOutlineLevel(counter) == 1)
				{
					vsEncumbrances.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			vsEncumbrances_AfterCollapse(0, true);
            modColorScheme.ColorGrid(vsEncumbrances);
			rsEOYProcess.OpenRecordset("SELECT * FROM EOYProcess");
			if (rsEOYProcess.EndOfFile() != true && rsEOYProcess.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				rsEOYProcess.AddNew();
				rsEOYProcess.Update(true);
				rsEOYProcess.OpenRecordset("SELECT * FROM EOYProcess");
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("CreateArchive")))
			{
				imgCreateArchive.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("CloseUnliquidated")))
			{
				imgCloseUnliquidated.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("CloseControl")))
			{
				imgCloseControl.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("PostClosing")))
			{
				imgPostClosing.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("Save1099")))
			{
				imgSave1099.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("ClearBudgets")))
			{
				imgClearBudgets.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("ResetNumbers")))
			{
				imgResetNumbers.Visible = true;
			}
			if (FCConvert.ToBoolean(rsEOYProcess.Get_Fields_Boolean("ClearDetail")))
			{
				imgClearDetail.Visible = true;
			}
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
		}

		private void frmEOYProcessing_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void CreateArchive()
		{
			clsDRWrapper rsArchive = new clsDRWrapper();
			boolArchiveErrors = false;
			imgCreateArchiveProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			rsArchive.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' ORDER BY ArchiveID DESC", "SystemSettings");
			if (rsArchive.EndOfFile() != true && rsArchive.BeginningOfFile() != true)
			{
				strYear = FCConvert.ToString(Conversion.Val(rsArchive.Get_Fields_Int32("ArchiveID")) + 1);
			}
			else
			{
				strYear = "0";
			}
			if (Conversion.Val(strYear) < 2000)
			{
				// Create Archive Folder
				strYear = Strings.Right(Strings.Trim(Conversion.Str(DateTime.Today.Year)), 2);
                var maxYear = GetLatestYearFromPastBudgets();
                if (maxYear >= DateTime.Now.Year)
                {
                    strYear = ((maxYear - 2000) + 1).ToString();
                }
				strYear = Interaction.InputBox("Please input the last 2 digits of the fiscal year you are closing off", "Enter Closeout Year", strYear);
				if (strYear.Length == 2 && Information.IsNumeric(strYear))
				{
					strYear = FCConvert.ToString(Conversion.Val(strYear) + 2000);
				}
				else
				{
					MessageBox.Show("EOY Process Ended because an invalid closeout year was specified", "No Closeout Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
            if (FCConvert.ToInt32(strYear) <= GetLatestYearFromPastBudgets())
            {
                MessageBox.Show("The end of year process has already been completed for the year entered.\n" + "Please try the process again and enter a different close out year.","Invalid Closeout Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
			backrest = new BackupRestore();
			backrest.BackupCompleted += new BackupRestore.BackupCompletedEventHandler(backrest_BackupCompleted);
			backrest.RestoreCompleted += new BackupRestore.RestoreCompletedEventHandler(backrest_RestoreCompleted);
			strLastStep = "Budgetary_Back";
			modGlobalFunctions.AddCYAEntry_26("GN", "Creating BD EOY archives", "backup:" + backrest.GetBackupPath(rsArchive.Get_ConnectionInformation("Budgetary")));
			backrest.Backup(rsArchive.Get_ConnectionInformation("Budgetary"));
		}

        private int GetLatestYearFromPastBudgets()
        {
            var rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("select max([year]) as highestyear from pastbudgets", "Budgetary");
            if (!rsLoad.EndOfFile())
            {
                return rsLoad.Get_Fields_Int32("highestyear");
            }
            return 0;
        }

		private bool CloseUnliquidated(List<string> batchReports)
		{
			bool CloseUnliquidated = false;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsEnc = new clsDRWrapper();
			string strFunBalAcct = "";
			string strEncOffAcct = "";
			string strUnliqEncAcct = "";
			string strExpCtrlAcct = "";
			string strRevCtrlAcct = "";
			double[] dblFund = new double[99 + 1];
	
			clsDRWrapper rs = new clsDRWrapper();
			int counter;
			int lngFund = 0;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			clsDRWrapper Master = new clsDRWrapper();
			int TempJournal = 0;
			// vbPorter upgrade warning: dblExpAmount As Decimal	OnWrite(short, Decimal)
			Decimal[] dblExpAmount = new Decimal[99 + 1];
			// vbPorter upgrade warning: dblRevAmount As Decimal	OnWrite(short, Decimal)
			Decimal[] dblRevAmount = new Decimal[99 + 1];
			CloseUnliquidated = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			rsEnc.OpenRecordset("SELECT * FROM Encumbrances");
			if (rsEnc.EndOfFile() != true && rsEnc.BeginningOfFile() != true)
			{
				if (vsEncumbrances.Rows == 0)
				{
					imgCloseUnliquidatedProcessing.Visible = false;
					imgCloseUnliquidated.Visible = true;
					this.Refresh();
					FCUtils.ApplicationUpdate(this);
					return CloseUnliquidated;
				}
				rsInfo.OpenRecordset("SELECT * FROM StandardAccounts");

				rsInfo.FindFirstRecord("Code", "UE");
				if (rsInfo.NoMatch)
				{
					MessageBox.Show("You must set up your Town Unliquidated Encumbrance Account before you may proceed", "Setup Town Unliquidated Encumbrance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Account"))) == 0)
					{
						MessageBox.Show("You must set up your Town Unliquidated Encumbrance Account before you may proceed", "Setup Town Unliquidated Encumbrance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseUnliquidated = false;
						return CloseUnliquidated;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strUnliqEncAcct = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					}
				}
				rsInfo.FindFirstRecord("Code", "EC");
				if (rsInfo.NoMatch)
				{
					MessageBox.Show("You must set up your Town Expense Control Account before you may proceed", "Setup Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Account"))) == 0)
					{
						MessageBox.Show("You must set up your Town Expense Control Account before you may proceed", "Setup Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseUnliquidated = false;
						return CloseUnliquidated;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strExpCtrlAcct = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					}
				}
				rsInfo.FindFirstRecord("Code", "RC");
				if (rsInfo.NoMatch)
				{
					MessageBox.Show("You must set up your Town Revenue Control Account before you may proceed", "Setup Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Account"))) == 0)
					{
						MessageBox.Show("You must set up your Town Revenue Control Account before you may proceed", "Setup Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseUnliquidated = false;
						return CloseUnliquidated;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRevCtrlAcct = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					}
				}
				rsInfo.FindFirstRecord("Code", "EO");
				if (rsInfo.NoMatch)
				{
					MessageBox.Show("You must set up your Town Encumbrance Offest Account before you may proceed", "Setup Town Encumbrance Offset Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Account"))) == 0)
					{
						MessageBox.Show("You must set up your Town Encumbrance Offest Account before you may proceed", "Setup Town Encumbrance Offset Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseUnliquidated = false;
						return CloseUnliquidated;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strEncOffAcct = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					}
				}
				rsInfo.FindFirstRecord("Code", "FB");
				if (rsInfo.NoMatch)
				{
					MessageBox.Show("You must set up your Town Fund Balance Account before you may proceed", "Setup Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Account"))) == 0)
					{
						MessageBox.Show("You must set up your Town Fund Balance Account before you may proceed", "Setup Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseUnliquidated = false;
						return CloseUnliquidated;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strFunBalAcct = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					}
				}
				
				for (counter = 0; counter <= 99; counter++)
				{
					dblFund[counter] = 0;
					dblRevAmount[counter] = 0;
					dblExpAmount[counter] = 0;
				}
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "EOY Enc Closing Entries");
				Master.Set_Fields("Type", "GJ");
				Master.Set_Fields("Period", DateTime.Today.Month);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				lngClosingJournal = TempJournal;
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
				for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
				{
					if (vsEncumbrances.RowOutlineLevel(counter) == 1)
					{
						lngFund = modBudgetaryAccounting.GetFundFromAccount(vsEncumbrances.TextMatrix(counter, AccountCol));
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY Enc Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						// If Left(vsEncumbrances.TextMatrix(counter, AccountCol), 1) = "E" Or Left(vsEncumbrances.TextMatrix(counter, AccountCol), 1) = "R" Or Left(vsEncumbrances.TextMatrix(counter, AccountCol), 1) = "G" Then
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strEncOffAcct + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strEncOffAcct);
						}
						// End If
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", vsEncumbrances.TextMatrix(counter, AmountCol));
						rs.Set_Fields("Status", "E");
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						modBudgetaryMaster.AddEOYAdjustment_26(rs.Get_Fields("Account"), rs.Get_Fields("Amount"), FCConvert.ToInt32(Conversion.Val(strYear)));
						rs.Update();
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY Enc Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						rs.Set_Fields("account", vsEncumbrances.TextMatrix(counter, AccountCol));
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", FCConvert.ToDecimal(vsEncumbrances.TextMatrix(counter, AmountCol)) * -1);
						rs.Set_Fields("Status", "E");
						rs.Update();
						dblFund[lngFund] += FCConvert.ToDouble(vsEncumbrances.TextMatrix(counter, AmountCol));
						if (Strings.Left(vsEncumbrances.TextMatrix(counter, AccountCol), 1) == "E")
						{
							dblExpAmount[lngFund] += (FCConvert.ToDecimal(vsEncumbrances.TextMatrix(counter, AmountCol)) * -1);
						}
						else if (Strings.Left(vsEncumbrances.TextMatrix(counter, AccountCol), 1) == "R")
						{
							dblRevAmount[lngFund] += (FCConvert.ToDecimal(vsEncumbrances.TextMatrix(counter, AmountCol)) * -1);
						}
					}
				}
				for (counter = 1; counter <= 99; counter++)
				{
					if (dblFund[counter] != 0)
					{
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY Enc Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strFunBalAcct + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strFunBalAcct);
						}
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", dblFund[counter]);
						rs.Set_Fields("Status", "E");
						rs.Update();
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY Enc Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strUnliqEncAcct + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strUnliqEncAcct);
						}
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", dblFund[counter] * -1);
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						modBudgetaryMaster.AddEOYAdjustment_26(rs.Get_Fields("Account"), rs.Get_Fields("Amount"), FCConvert.ToInt32(Conversion.Val(strYear)));
						rs.Set_Fields("Status", "E");
						rs.Update();
					}
				}
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalNumber = TempJournal;
				clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
				clsJournalInfo.JournalType = "GJ";
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				clsPostInfo.AllowPreview = false;
				clsPostInfo.SavePostingReport = false;
				if (!clsPostInfo.PostJournals(batchReports: batchReports))
				{
					MessageBox.Show("End of Year Processing is Stopping because there is a problem posting the Encumbrance Closing Entries in Journal " + modValidateAccount.GetFormat_6(FCConvert.ToString(lngClosingJournal), 4), "Unable to Post Encumbrnace Closing Entries", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CloseUnliquidated = false;
					return CloseUnliquidated;
				}
				//Application.DoEvents();
			}
			else
			{
				// do nothing
			}
			imgCloseUnliquidatedProcessing.Visible = false;
			imgCloseUnliquidated.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return CloseUnliquidated;
		}

		private bool CloseControls()
		{
			bool CloseControls = false;
			clsDRWrapper rsControlInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curExpControlAmount As Decimal	OnWrite(short, double, Decimal)
			Decimal[] curExpControlAmount = new Decimal[99 + 1];
			// vbPorter upgrade warning: curRevControlAmount As Decimal	OnWrite(short, double, Decimal)
			Decimal[] curRevControlAmount = new Decimal[99 + 1];
			int TempJournal = 0;
			clsDRWrapper Master = new clsDRWrapper();
			clsDRWrapper rs = new clsDRWrapper();
			// vbPorter upgrade warning: curExpTotal As Decimal	OnWriteFCConvert.ToInt16
			Decimal curExpTotal;
			// vbPorter upgrade warning: curRevTotal As Decimal	OnWriteFCConvert.ToInt16
			Decimal curRevTotal;
			string strRevAcct = "";
			string strExpAcct = "";
			string strFundBal = "";
			int counter;
			bool blnInfoToClose;
			clsDRWrapper rsDepartmentInfo = new clsDRWrapper();
			clsDRWrapper rsDivisionInfo = new clsDRWrapper();
			clsDRWrapper rsRevenueInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curDeptExpTotal As Decimal	OnWrite(double, short, Decimal)
			Decimal curDeptExpTotal = 0.0M;
			// vbPorter upgrade warning: curDeptRevTotal As Decimal	OnWrite(double, short, Decimal)
			Decimal curDeptRevTotal = 0.0M;
			// vbPorter upgrade warning: curDivExpTotal As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curDivExpTotal;
			// vbPorter upgrade warning: curDivRevTotal As Decimal	OnWrite(double, Decimal)
			Decimal curDivRevTotal;
			clsDRWrapper rsCloseout = new clsDRWrapper();
			imgCloseControlProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			CloseControls = true;
			curExpTotal = 0;
			curRevTotal = 0;
			// If gboolTownAccounts Then
			rsControlInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EC'");
			if (rsControlInfo.EndOfFile() != true && rsControlInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account"))) != "" && Conversion.Val(Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account")))) != 0)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strExpAcct = Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Expense Control Account", "Invalid Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CloseControls = false;
					return CloseControls;
				}
			}
			else
			{
				MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Expense Control Account", "Invalid Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CloseControls = false;
				return CloseControls;
			}
			rsControlInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RC'");
			if (rsControlInfo.EndOfFile() != true && rsControlInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account"))) != "" && Conversion.Val(Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account")))) != 0)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strRevAcct = Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Revenue Control Account", "Invalid Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CloseControls = false;
					return CloseControls;
				}
			}
			else
			{
				MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Revenue Control Account", "Invalid Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CloseControls = false;
				return CloseControls;
			}
			rsControlInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'FB'");
			if (rsControlInfo.EndOfFile() != true && rsControlInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account"))) != "" && Conversion.Val(Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account")))) != 0)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strFundBal = Strings.Trim(FCConvert.ToString(rsControlInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Fund Balance Account", "Invalid Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CloseControls = false;
					return CloseControls;
				}
			}
			else
			{
				MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Fund Balance Account", "Invalid Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CloseControls = false;
				return CloseControls;
			}
			// End If
			for (counter = 1; counter <= 99; counter++)
			{
				curExpControlAmount[counter] = 0;
				curRevControlAmount[counter] = 0;
			}
			blnInfoToClose = false;
			for (counter = 1; counter <= 99; counter++)
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					curExpControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strExpAcct + "-00"));
					curRevControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strRevAcct + "-00"));
				}
				else
				{
					curExpControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strExpAcct));
					curRevControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strRevAcct));
				}
				if (curRevControlAmount[counter] != 0 || curExpControlAmount[counter] != 0)
				{
					blnInfoToClose = true;
				}
			}
			if (blnInfoToClose)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseControls = false;
					return CloseControls;
				}
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "EOY Closing Entries");
				Master.Set_Fields("Type", "GJ");
				Master.Set_Fields("Period", DateTime.Today.Month);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				lngClosingJournal = TempJournal;
				rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
				rs.OmitNullsOnInsert = true;
				for (counter = 1; counter <= 99; counter++)
				{
					if (curExpControlAmount[counter] != 0)
					{
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY EC Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strExpAcct + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strExpAcct);
						}
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", curExpControlAmount[counter] * -1);
						rs.Set_Fields("Status", "E");
						rs.Update();
					}
					if (curRevControlAmount[counter] != 0)
					{
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY RC Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strRevAcct + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strRevAcct);
						}
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", curRevControlAmount[counter] * -1);
						rs.Set_Fields("Status", "E");
						rs.Update();
					}
					rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Fund = '" + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "' ORDER BY Department");
					if (rsDepartmentInfo.EndOfFile() != true && rsDepartmentInfo.BeginningOfFile() != true)
					{
						do
						{
							if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
							{
								if (rsExpDeptYTDActivity.FindFirstRecord("Department", rsDepartmentInfo.Get_Fields_String("Department")))
								{
									// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									curDeptExpTotal = FCConvert.ToDecimal(rsExpDeptYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDeptYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDeptYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDeptYTDActivity.Get_Fields("PostedCreditsTotal"));
								}
								else
								{
									curDeptExpTotal = 0;
								}
								curExpControlAmount[counter] += curDeptExpTotal;
								if (rsRevDeptYTDActivity.FindFirstRecord("Department", rsDepartmentInfo.Get_Fields_String("Department")))
								{
									// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									curDeptRevTotal = FCConvert.ToDecimal(rsRevDeptYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDeptYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDeptYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDeptYTDActivity.Get_Fields("PostedCreditsTotal"));
								}
								else
								{
									curDeptRevTotal = 0;
								}
								curRevControlAmount[counter] -= curDeptRevTotal;
							}
							if (!modAccountTitle.Statics.ExpDivFlag)
							{
								rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Division");
								if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
								{
									do
									{
										if (Strings.Trim(FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"))) != "")
										{
											if (!modAccountTitle.Statics.RevDivFlag)
											{
												if (rsExpDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
												{
													if (rsRevDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
													{
														// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
														if (((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) + (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
														{
															if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
															{
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																curDeptExpTotal -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																curDeptRevTotal -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
															}
															else
															{
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																curExpControlAmount[counter] += (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																curRevControlAmount[counter] -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
															}
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															curDivExpTotal = FCConvert.ToDecimal((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")));
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															curDivRevTotal = FCConvert.ToDecimal((rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal")));
															rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
															if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
															{
																do
																{
																	if (rsRevRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
																	{
																		// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																		if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
																		{
																			if (Strings.Trim(FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"))) != "")
																			{
																				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																				curDivRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																			}
																			else if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
																			{
																				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																				curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																			}
																			else
																			{
																				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																				curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																			}
																			rs.AddNew();
																			rs.Set_Fields("Type", "G");
																			rs.Set_Fields("JournalEntriesDate", DateTime.Today);
																			rs.Set_Fields("Description", "EOY FB Closing Entries");
																			rs.Set_Fields("JournalNumber", TempJournal);
																			rs.Set_Fields("Period", DateTime.Today.Month);
																			rs.Set_Fields("Project", "");
																			rs.Set_Fields("account", rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
																			rs.Set_Fields("RCB", "L");
																			// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																			rs.Set_Fields("Amount", ((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))));
																			rs.Set_Fields("Status", "E");
																			rs.Update();
																		}
																	}
																	rsRevenueInfo.MoveNext();
																}
																while (rsRevenueInfo.EndOfFile() != true);
															}
															rs.AddNew();
															rs.Set_Fields("Type", "G");
															rs.Set_Fields("JournalEntriesDate", DateTime.Today);
															rs.Set_Fields("Description", "EOY FB Closing Entries");
															rs.Set_Fields("JournalNumber", TempJournal);
															rs.Set_Fields("Period", DateTime.Today.Month);
															rs.Set_Fields("Project", "");
															rs.Set_Fields("account", rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
															rs.Set_Fields("RCB", "L");
															rs.Set_Fields("Amount", curDivRevTotal - curDivExpTotal);
															rs.Set_Fields("Status", "E");
															rs.Update();
														}
													}
													else
													{
														if (rsExpDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
														{
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															if ((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) != 0)
															{
																if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
																{
																	// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																	curDeptExpTotal -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
																}
																else
																{
																	// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																	curExpControlAmount[counter] += (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
																}
																rs.AddNew();
																rs.Set_Fields("Type", "G");
																rs.Set_Fields("JournalEntriesDate", DateTime.Today);
																rs.Set_Fields("Description", "EOY FB Closing Entries");
																rs.Set_Fields("JournalNumber", TempJournal);
																rs.Set_Fields("Period", DateTime.Today.Month);
																rs.Set_Fields("Project", "");
																rs.Set_Fields("account", rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
																rs.Set_Fields("RCB", "L");
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																rs.Set_Fields("Amount", (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) * -1);
																rs.Set_Fields("Status", "E");
																rs.Update();
															}
														}
													}
												}
												else
												{
													if (rsRevDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
													{
														// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
														if ((rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal")) != 0)
														{
															if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
															{
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																curDeptRevTotal -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
															}
															else
															{
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																curRevControlAmount[counter] -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
															}
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															curDivRevTotal = FCConvert.ToDecimal((rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal")));
															rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
															if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
															{
																do
																{
																	if (rsRevRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
																	{
																		// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																		if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
																		{
																			if (Strings.Trim(FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"))) != "")
																			{
																				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																				curDivRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																			}
																			else if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
																			{
																				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																				curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																			}
																			else
																			{
																				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																				curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																			}
																			rs.AddNew();
																			rs.Set_Fields("Type", "G");
																			rs.Set_Fields("JournalEntriesDate", DateTime.Today);
																			rs.Set_Fields("Description", "EOY FB Closing Entries");
																			rs.Set_Fields("JournalNumber", TempJournal);
																			rs.Set_Fields("Period", DateTime.Today.Month);
																			rs.Set_Fields("Project", "");
																			rs.Set_Fields("account", rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
																			rs.Set_Fields("RCB", "L");
																			// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																			rs.Set_Fields("Amount", ((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))));
																			rs.Set_Fields("Status", "E");
																			rs.Update();
																		}
																	}
																	rsRevenueInfo.MoveNext();
																}
																while (rsRevenueInfo.EndOfFile() != true);
															}
															if (curDivRevTotal != 0)
															{
																rs.AddNew();
																rs.Set_Fields("Type", "G");
																rs.Set_Fields("JournalEntriesDate", DateTime.Today);
																rs.Set_Fields("Description", "EOY FB Closing Entries");
																rs.Set_Fields("JournalNumber", TempJournal);
																rs.Set_Fields("Period", DateTime.Today.Month);
																rs.Set_Fields("Project", "");
																rs.Set_Fields("account", rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
																rs.Set_Fields("RCB", "L");
																rs.Set_Fields("Amount", curDivRevTotal);
																rs.Set_Fields("Status", "E");
																rs.Update();
															}
														}
													}
												}
											}
											else
											{
												if (rsExpDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
												{
													// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
													if ((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) != 0)
													{
														if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
														{
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															curDeptExpTotal -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
														}
														else
														{
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															curExpControlAmount[counter] += (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
														}
														rs.AddNew();
														rs.Set_Fields("Type", "G");
														rs.Set_Fields("JournalEntriesDate", DateTime.Today);
														rs.Set_Fields("Description", "EOY FB Closing Entries");
														rs.Set_Fields("JournalNumber", TempJournal);
														rs.Set_Fields("Period", DateTime.Today.Month);
														rs.Set_Fields("Project", "");
														rs.Set_Fields("account", rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
														rs.Set_Fields("RCB", "L");
														// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
														rs.Set_Fields("Amount", (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) * -1);
														rs.Set_Fields("Status", "E");
														rs.Update();
													}
												}
											}
										}
										else
										{
											if (!modAccountTitle.Statics.RevDivFlag)
											{
												rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
												if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
												{
													do
													{
														if (rsRevRevYTDActivity.FindFirstRecord2("Department, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
														{
															// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
															// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
															if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
															{
																if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
																{
																	// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																	curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																}
																else
																{
																	// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																	// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																	curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
																}
																rs.AddNew();
																rs.Set_Fields("Type", "G");
																rs.Set_Fields("JournalEntriesDate", DateTime.Today);
																rs.Set_Fields("Description", "EOY FB Closing Entries");
																rs.Set_Fields("JournalNumber", TempJournal);
																rs.Set_Fields("Period", DateTime.Today.Month);
																rs.Set_Fields("Project", "");
																rs.Set_Fields("account", rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
																rs.Set_Fields("RCB", "L");
																// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
																// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
																rs.Set_Fields("Amount", ((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))));
																rs.Set_Fields("Status", "E");
																rs.Update();
															}
														}
														rsRevenueInfo.MoveNext();
													}
													while (rsRevenueInfo.EndOfFile() != true);
												}
											}
										}
										rsDivisionInfo.MoveNext();
									}
									while (rsDivisionInfo.EndOfFile() != true);
								}
								if (modAccountTitle.Statics.RevDivFlag)
								{
									rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
									if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
									{
										do
										{
											if (rsRevRevYTDActivity.FindFirstRecord2("Department, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
											{
												// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
												// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
												// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
												// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
												if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
												{
													if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
													{
														// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
														curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
													}
													else
													{
														// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
														// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
														curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
													}
													rs.AddNew();
													rs.Set_Fields("Type", "G");
													rs.Set_Fields("JournalEntriesDate", DateTime.Today);
													rs.Set_Fields("Description", "EOY FB Closing Entries");
													rs.Set_Fields("JournalNumber", TempJournal);
													rs.Set_Fields("Period", DateTime.Today.Month);
													rs.Set_Fields("Project", "");
													rs.Set_Fields("account", rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
													rs.Set_Fields("RCB", "L");
													// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
													rs.Set_Fields("Amount", ((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))));
													rs.Set_Fields("Status", "E");
													rs.Update();
												}
											}
											rsRevenueInfo.MoveNext();
										}
										while (rsRevenueInfo.EndOfFile() != true);
									}
								}
							}
							else
							{
								rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
								if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
								{
									do
									{
										if (rsRevRevYTDActivity.FindFirstRecord2("Department, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
										{
											// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
											if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
											{
												if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
												{
													// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
													curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
												}
												else
												{
													// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
													// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
													curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
												}
												rs.AddNew();
												rs.Set_Fields("Type", "G");
												rs.Set_Fields("JournalEntriesDate", DateTime.Today);
												rs.Set_Fields("Description", "EOY FB Closing Entries");
												rs.Set_Fields("JournalNumber", TempJournal);
												rs.Set_Fields("Period", DateTime.Today.Month);
												rs.Set_Fields("Project", "");
												rs.Set_Fields("account", rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
												rs.Set_Fields("RCB", "L");
												// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
												// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
												// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
												// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
												rs.Set_Fields("Amount", ((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))));
												rs.Set_Fields("Status", "E");
												rs.Update();
											}
										}
										rsRevenueInfo.MoveNext();
									}
									while (rsRevenueInfo.EndOfFile() != true);
								}
							}
							if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
							{
								if (curDeptRevTotal - curDeptExpTotal != 0)
								{
									rs.AddNew();
									rs.Set_Fields("Type", "G");
									rs.Set_Fields("JournalEntriesDate", DateTime.Today);
									rs.Set_Fields("Description", "EOY FB Closing Entries");
									rs.Set_Fields("JournalNumber", TempJournal);
									rs.Set_Fields("Period", DateTime.Today.Month);
									rs.Set_Fields("Project", "");
									rs.Set_Fields("account", rsDepartmentInfo.Get_Fields_String("CloseoutAccount"));
									rs.Set_Fields("RCB", "L");
									rs.Set_Fields("Amount", curDeptRevTotal - curDeptExpTotal);
									rs.Set_Fields("Status", "E");
									rs.Update();
								}
							}
							rsDepartmentInfo.MoveNext();
						}
						while (rsDepartmentInfo.EndOfFile() != true);
					}
					if (curExpControlAmount[counter] + curRevControlAmount[counter] != 0)
					{
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", DateTime.Today);
						rs.Set_Fields("Description", "EOY FB Closing Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", DateTime.Today.Month);
						rs.Set_Fields("Project", "");
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strFundBal + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strFundBal);
						}
						rs.Set_Fields("RCB", "L");
						rs.Set_Fields("Amount", curExpControlAmount[counter] + curRevControlAmount[counter]);
						rs.Set_Fields("Status", "E");
						rs.Update();
					}
				}
			}
			else
			{
				lngClosingJournal = 0;
			}
			imgCloseControlProcessing.Visible = false;
			imgCloseControl.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return CloseControls;
		}

		private double GetYTDDebit(ref string strAcct, short intMonth = -1)
		{
			double GetYTDDebit = 0;
			string account = strAcct;
			if (intMonth == -1)
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (YTDExpActivity.Records.Any())
					{
						RecordItem record = YTDExpActivity.Records.FirstOrDefault(x => x.Fields["Account"] == account);
						GetYTDDebit = record != null ? Convert.ToDouble(record.Fields["PostedDebitsTotal"]) : 0;
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (YTDRevActivity.Records.Any())
					{
						RecordItem record = YTDRevActivity.Records.FirstOrDefault(x => x.Fields["Account"] == account);
						GetYTDDebit = record != null ? Convert.ToDouble(record.Fields["PostedDebitsTotal"]) : 0;
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (YTDLedgerActivity.Records.Any())
					{
						RecordItem record = YTDLedgerActivity.Records.FirstOrDefault(x => x.Fields["Account"] == account);
						GetYTDDebit = record != null ? Convert.ToDouble(record.Fields["PostedDebitsTotal"]) : 0;
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			else
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (YTDExpActivityDetail.Records.Any())
					{
						RecordItem record = YTDExpActivityDetail.Records.FirstOrDefault(x => x.Fields["Account"] == account && Convert.ToInt16(x.Fields["Period"]) == intMonth);
						GetYTDDebit = record != null ? Convert.ToDouble(record.Fields["PostedDebitsTotal"]) : 0;
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (YTDRevActivityDetail.Records.Any())
					{
						RecordItem record = YTDRevActivityDetail.Records.FirstOrDefault(x => x.Fields["Account"] == account && Convert.ToInt16(x.Fields["Period"]) == intMonth);
						GetYTDDebit = record != null ? Convert.ToDouble(record.Fields["PostedDebitsTotal"]) : 0;
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (YTDLedgerActivityDetail.Records.Any())
					{
						RecordItem record = YTDLedgerActivityDetail.Records.FirstOrDefault(x => x.Fields["Account"] == account && Convert.ToInt16(x.Fields["Period"]) == intMonth);
						GetYTDDebit = record != null ? Convert.ToDouble(record.Fields["PostedDebitsTotal"]) : 0;
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			GetYTDDebit += GetEncumbrance(ref strAcct, intMonth);
			return GetYTDDebit;
		}

		private double GetEncumbrance(ref string strAcct, short intMonth = -1)
		{
			double GetEncumbrance = 0;
			if (intMonth == -1)
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
					{
						if (rsYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
					{
						if (rsRevYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsRevYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (rsLedgerYTDActivity.EndOfFile() != true && rsLedgerYTDActivity.BeginningOfFile() != true)
					{
						if (rsLedgerYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
			}
			else
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
					{
						if (rsActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (rsRevActivityDetail.EndOfFile() != true && rsRevActivityDetail.BeginningOfFile() != true)
					{
						if (rsRevActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsRevActivityDetail.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (rsLedgerActivityDetail.EndOfFile() != true && rsLedgerActivityDetail.BeginningOfFile() != true)
					{
						if (rsLedgerActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsLedgerActivityDetail.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
			}
			return GetEncumbrance;
		}

		private double GetYTDCredit(ref string strAcct, short intMonth = -1)
		{
			double GetYTDCredit = 0;
			string account = strAcct;
			if (intMonth == -1)
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (YTDExpActivity.Records.Any())
					{
						RecordItem record = YTDExpActivity.Records.FirstOrDefault(x => x.Fields["Account"] == account);
						GetYTDCredit = record != null ? Convert.ToDouble(record.Fields["PostedCreditsTotal"]) * -1 : 0;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (YTDRevActivity.Records.Any())
					{
						RecordItem record = YTDRevActivity.Records.FirstOrDefault(x => x.Fields["Account"] == account);
						GetYTDCredit = record != null ? Convert.ToDouble(record.Fields["PostedCreditsTotal"]) * -1 : 0;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (YTDLedgerActivity.Records.Any())
					{
						RecordItem record = YTDLedgerActivity.Records.FirstOrDefault(x => x.Fields["Account"] == account);
						GetYTDCredit = record != null ? Convert.ToDouble(record.Fields["PostedCreditsTotal"]) * -1 : 0;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			else
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (YTDExpActivityDetail.Records.Any())
					{
						RecordItem record = YTDExpActivityDetail.Records.FirstOrDefault(x => x.Fields["Account"] == account && Convert.ToInt16(x.Fields["Period"]) == intMonth);
						GetYTDCredit = record != null ? Convert.ToDouble(record.Fields["PostedCreditsTotal"]) * -1 : 0;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (YTDRevActivityDetail.Records.Any())
					{
						RecordItem record = YTDRevActivityDetail.Records.FirstOrDefault(x => x.Fields["Account"] == account && Convert.ToInt16(x.Fields["Period"]) == intMonth);
						GetYTDCredit = record != null ? Convert.ToDouble(record.Fields["PostedCreditsTotal"]) * -1 : 0;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (YTDLedgerActivityDetail.Records.Any())
					{
						RecordItem record = YTDLedgerActivityDetail.Records.FirstOrDefault(x => x.Fields["Account"] == account && Convert.ToInt16(x.Fields["Period"]) == intMonth);
						GetYTDCredit = record != null ? Convert.ToDouble(record.Fields["PostedCreditsTotal"]) * -1 : 0;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			return GetYTDCredit;
		}

		private double GetYTDNet_2(string strAcct, short intMonth = -1)
		{
			return GetYTDNet(ref strAcct, intMonth);
		}

		private double GetYTDNet_6(string strAcct, short intMonth = -1)
		{
			return GetYTDNet(ref strAcct, intMonth);
		}

		private double GetYTDNet(ref string strAcct, short intMonth = -1)
		{
			double GetYTDNet = 0;
			int temp;
			if (intMonth == -1)
			{
				GetYTDNet = GetYTDDebit(ref strAcct) - GetYTDCredit(ref strAcct);
			}
			else
			{
				GetYTDNet = GetYTDDebit(ref strAcct, intMonth) - GetYTDCredit(ref strAcct, intMonth);
			}
			return GetYTDNet;
		}

		private bool PostClosingJournal(List<string> batchReports)
		{
			bool PostClosingJournal = false;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			imgPostClosingProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			PostClosingJournal = true;
			rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngClosingJournal));
			if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
			{
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalNumber = lngClosingJournal;
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				clsJournalInfo.Period = FCConvert.ToString(rsJournalInfo.Get_Fields("Period"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				clsJournalInfo.JournalType = FCConvert.ToString(rsJournalInfo.Get_Fields("Type"));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				clsPostInfo.AllowPreview = false;
				clsPostInfo.SavePostingReport = true;
				clsPostInfo.SavedReportFileName = "EOYClosingJournal";
				if (!clsPostInfo.PostJournals(batchReports: batchReports))
				{
					MessageBox.Show("End of Year Processing is Stopping because there is a problem posting the Closing Entries in Journal " + modValidateAccount.GetFormat_6(FCConvert.ToString(lngClosingJournal), 4), "Unable to Post Closing Entries", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					PostClosingJournal = false;
					return PostClosingJournal;
				}
			}
			else
			{
				MessageBox.Show("End of Year Processing is Stopping because there is a problem posting the Closing Entries in Journal " + modValidateAccount.GetFormat_6(FCConvert.ToString(lngClosingJournal), 4), "Unable to Post Closing Entries", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				PostClosingJournal = false;
				return PostClosingJournal;
			}

			File.Copy("EOYClosingJournal.rdf", System.IO.Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, strYear + "EOYClosingJournal.rdf"));
			File.Delete("EOYClosingJournal.rdf");

			imgPostClosingProcessing.Visible = false;
			imgPostClosing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return PostClosingJournal;
		}

		private bool ClearBudgetAmounts()
		{
			bool ClearBudgetAmounts = false;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsPastInfo = new clsDRWrapper();
			string strAccount = "";
			int ans;
			imgClearBudgetProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			ClearBudgetAmounts = true;
			rsPastInfo.OmitNullsOnInsert = true;
			rsPastInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE ID = 0");
			rsInfo.OpenRecordset("SELECT * FROM AccountMaster");
			// WHERE AccountType <> 'G'"
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsPastInfo.AddNew();
					rsPastInfo.Set_Fields("Year", strYear);
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					rsPastInfo.Set_Fields("Account", strAccount);
					// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
					rsPastInfo.Set_Fields("StartBudget", rsInfo.Get_Fields("CurrentBudget"));
					rsPastInfo.Set_Fields("JanBudget", rsInfo.Get_Fields_Decimal("JanBudget"));
					rsPastInfo.Set_Fields("FebBudget", rsInfo.Get_Fields_Decimal("FebBudget"));
					rsPastInfo.Set_Fields("MarBudget", rsInfo.Get_Fields_Decimal("MarBudget"));
					rsPastInfo.Set_Fields("AprBudget", rsInfo.Get_Fields_Decimal("AprBudget"));
					rsPastInfo.Set_Fields("MayBudget", rsInfo.Get_Fields_Decimal("MayBudget"));
					rsPastInfo.Set_Fields("JuneBudget", rsInfo.Get_Fields_Decimal("JuneBudget"));
					rsPastInfo.Set_Fields("JulyBudget", rsInfo.Get_Fields_Decimal("JulyBudget"));
					rsPastInfo.Set_Fields("AugBudget", rsInfo.Get_Fields_Decimal("AugBudget"));
					rsPastInfo.Set_Fields("SeptBudget", rsInfo.Get_Fields_Decimal("SeptBudget"));
					rsPastInfo.Set_Fields("OctBudget", rsInfo.Get_Fields_Decimal("OctBudget"));
					rsPastInfo.Set_Fields("NovBudget", rsInfo.Get_Fields_Decimal("NovBudget"));
					rsPastInfo.Set_Fields("DecBudget", rsInfo.Get_Fields_Decimal("DecBudget"));
					rsPastInfo.Set_Fields("JanActual", GetYTDNet_6(strAccount, 1));
					rsPastInfo.Set_Fields("FebActual", GetYTDNet_6(strAccount, 2));
					rsPastInfo.Set_Fields("MarActual", GetYTDNet_6(strAccount, 3));
					rsPastInfo.Set_Fields("AprActual", GetYTDNet_6(strAccount, 4));
					rsPastInfo.Set_Fields("MayActual", GetYTDNet_6(strAccount, 5));
					rsPastInfo.Set_Fields("JuneActual", GetYTDNet_6(strAccount, 6));
					rsPastInfo.Set_Fields("JulyActual", GetYTDNet_6(strAccount, 7));
					rsPastInfo.Set_Fields("AugActual", GetYTDNet_6(strAccount, 8));
					rsPastInfo.Set_Fields("SeptActual", GetYTDNet_6(strAccount, 9));
					rsPastInfo.Set_Fields("OctActual", GetYTDNet_6(strAccount, 10));
					rsPastInfo.Set_Fields("NovActual", GetYTDNet_6(strAccount, 11));
					rsPastInfo.Set_Fields("DecActual", GetYTDNet_6(strAccount, 12));
					double budgetAdjustments = GetBudgetAdjustments(ref strAccount);
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
					{
						// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
						rsPastInfo.Set_Fields("EndBudget", FCConvert.ToDouble(rsInfo.Get_Fields("CurrentBudget")) - budgetAdjustments);
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
						rsPastInfo.Set_Fields("EndBudget", FCConvert.ToDouble(rsInfo.Get_Fields("CurrentBudget")) + budgetAdjustments);
					}
					rsPastInfo.Set_Fields("BudgetAdjustments", budgetAdjustments);
					rsPastInfo.Set_Fields("CarryForward", GetCarryOver(ref strAccount));
					rsPastInfo.Set_Fields("ActualSpent", GetYTDNet(ref strAccount));
					rsPastInfo.Update(true);
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				rsInfo.MoveFirst();
				do
				{
					if (FCConvert.ToString(rsInfo.Get_Fields_String("AccountType")) != "G")
					{
						rsInfo.Edit();
						rsInfo.Set_Fields("CurrentBudget", 0);
						rsInfo.Update(true);
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			imgClearBudgetProcessing.Visible = false;
			imgClearBudgets.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return ClearBudgetAmounts;
		}

		private bool ResetJournalWarrant()
		{
			bool ResetJournalWarrant = false;
			clsDRWrapper rsJournal = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			int intOldJournal = 0;
			int intNewJournal;
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			string strCheck = "";
			clsDRWrapper rsReprint = new clsDRWrapper();
			imgResetNumbersProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			ResetJournalWarrant = true;
			intNewJournal = 1;
			rsJournal.Execute("DELETE FROM JournalMaster WHERE Status = 'P' or Status = 'D'", "Budgetary");
			rsJournal.OpenRecordset("SELECT * FROM JournalMaster ORDER BY JournalNumber");
			if (rsJournal.EndOfFile() != true && rsJournal.BeginningOfFile() != true)
			{
				do
				{
					intOldJournal = FCConvert.ToInt32(rsJournal.Get_Fields_Int32("JournalNumber"));
					if (FCConvert.ToString(rsJournal.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsJournal.Get_Fields("Type")) == "AC")
					{
						rsInfo.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(intOldJournal));
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							do
							{                                
								rsInfo.Edit();
								rsInfo.Set_Fields("JournalNumber", intNewJournal);
								rsInfo.Update(true);
								if (rsInfo.Get_Fields_Boolean("PrintedIndividual") == true)
								{
									// TODO Get_Fields: Field [Check] not found!! (maybe it is an alias?)
									rsDetailInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + rsInfo.Get_Fields("Check") + " AND VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"), "Budgetary");
									if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
									{
										rsDetailInfo.Edit();
										rsDetailInfo.Set_Fields("Journals", FCConvert.ToString(rsDetailInfo.Get_Fields_String("Journals")).Replace("JournalNumber = " + FCConvert.ToString(intOldJournal), "JournalNumber = " + FCConvert.ToString(intNewJournal)));
										rsDetailInfo.Update();
										if (FCConvert.ToBoolean(rsDetailInfo.Get_Fields_Boolean("ReprintedCheck")))
										{
											strCheck = FCConvert.ToString(rsDetailInfo.Get_Fields_String("OldCheckNumber"));
											do
											{
												rsReprint.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + strCheck + " AND VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber") + " AND PrintedIndividual = 1", "Budgetary");
												if (rsReprint.EndOfFile() != true && rsReprint.BeginningOfFile() != true)
												{
													rsReprint.Edit();
													rsReprint.Set_Fields("Journals", FCConvert.ToString(rsReprint.Get_Fields_String("Journals")).Replace("JournalNumber = " + FCConvert.ToString(intOldJournal), "JournalNumber = " + FCConvert.ToString(intNewJournal)));
													rsReprint.Update();
													strCheck = FCConvert.ToString(rsReprint.Get_Fields_String("OldCheckNumber"));
												}
												else
												{
													break;
												}
											}
											while (rsReprint.Get_Fields_Boolean("ReprintedCheck"));
										}
									}
								}
								rsInfo.MoveNext();
							}
							while (rsInfo.EndOfFile() != true);
						}
					}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsJournal.Get_Fields("Type") == "EN")
					{
						rsInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(intOldJournal));
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							do
							{
								rsInfo.Edit();
								rsInfo.Set_Fields("JournalNumber", intNewJournal);
								rsInfo.Update(true);
								rsInfo.MoveNext();
							}
							while (rsInfo.EndOfFile() != true);
						}
					}
					else
					{
						rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(intOldJournal));
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							do
							{
								rsInfo.Edit();
								rsInfo.Set_Fields("JournalNumber", intNewJournal);
								rsInfo.Update(true);
								rsInfo.MoveNext();
							}
							while (rsInfo.EndOfFile() != true);
						}
					}
					rsJournal.Edit();
					rsJournal.Set_Fields("JournalNumber", intNewJournal);
					rsJournal.Update(true);
					intNewJournal += 1;
					rsJournal.MoveNext();
				}
				while (rsJournal.EndOfFile() != true);
			}
			// rsJournal.Execute "DELETE FROM WarrantMaster"
			imgResetNumbersProcessing.Visible = false;
			imgResetNumbers.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return ResetJournalWarrant;
		}

		private bool Save1099()
		{
			bool Save1099 = false;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsDetail = new clsDRWrapper();
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			clsDRWrapper rsArchiveDetail = new clsDRWrapper();
			clsDRWrapper rsArchiveCDInfo = new clsDRWrapper();
			int fnx;
			int lngNewRecord = 0;
			Save1099 = true;
			rsInfo.Execute("DELETE FROM APJournalArchive", "Budgetary");
			rsInfo.Execute("DELETE FROM APJournalDetailArchive", "Budgetary");
			rsInfo.Execute("DELETE FROM CDJournalArchive", "Budgetary");
			rsArchiveInfo.OmitNullsOnInsert = true;
			rsArchiveDetail.OmitNullsOnInsert = true;
			rsArchiveInfo.OpenRecordset("SELECT * FROM APJournalArchive WHERE ID = 0");
			rsArchiveDetail.OpenRecordset("SELECT * FROM APJournalDetailArchive WHERE ID = 0");
			rsInfo.OpenRecordset("SELECT * FROM APJournal WHERE CheckDate BETWEEN '1/1/" + FCConvert.ToString(DateTime.Today.Year) + "' AND '12/31/" + FCConvert.ToString(DateTime.Today.Year) + "'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsArchiveInfo.AddNew();
					for (fnx = 0; fnx <= rsInfo.FieldsCount - 1; fnx++)
					{
						if (rsInfo.Get_FieldsIndexName(fnx) == "ID")
						{
							goto TryAgain;
						}
						else
						{
							rsArchiveInfo.Set_Fields(rsInfo.Get_FieldsIndexName(fnx), rsInfo.Get_FieldsIndexValue(fnx));
						}
						TryAgain:
						;
					}
					rsArchiveInfo.Update(true);
					lngNewRecord = FCConvert.ToInt32(rsArchiveInfo.Get_Fields_Int32("ID"));
					rsDetail.OpenRecordset("SELECT * FROM APjournalDetail WHERE APJournalID = " + rsInfo.Get_Fields_Int32("ID"));
					if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
					{
						do
						{
							rsArchiveDetail.AddNew();
							for (fnx = 0; fnx <= rsDetail.FieldsCount - 1; fnx++)
							{
								if (rsDetail.Get_FieldsIndexName(fnx) == "ID")
								{
									goto TRYAGAIN2;
								}
								else if (rsDetail.Get_FieldsIndexName(fnx) == "APJournalID")
								{
									rsArchiveDetail.Set_Fields("APJournalArchiveID", lngNewRecord);
								}
								else
								{
									rsArchiveDetail.Set_Fields(rsDetail.Get_FieldsIndexName(fnx), rsDetail.Get_FieldsIndexValue(fnx));
								}
								TRYAGAIN2:
								;
							}
							rsArchiveDetail.Update(true);
							rsDetail.MoveNext();
						}
						while (rsDetail.EndOfFile() != true);
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			rsArchiveCDInfo.OmitNullsOnInsert = true;
			rsArchiveCDInfo.OpenRecordset("SELECT * FROM CDJournalArchive WHERE ID = 0");
			rsInfo.Execute("UPDATE JournalEntries SET BankNumber = 0 WHERE convert(int, isnull(BankNumber, 0)) = 0", "Budgetary");
			rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE Type = 'D' AND JournalEntriesDate BETWEEN '1/1/" + FCConvert.ToString(DateTime.Today.Year) + "' AND '12/31/" + FCConvert.ToString(DateTime.Today.Year) + "'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsArchiveCDInfo.AddNew();
					for (fnx = 0; fnx <= rsInfo.FieldsCount - 1; fnx++)
					{
						if (rsInfo.Get_FieldsIndexName(fnx) == "ID")
						{
							goto TRYAGAIN3;
						}
						else if (rsInfo.Get_FieldsIndexName(fnx) == "JournalEntriesDate")
						{
							rsArchiveCDInfo.Set_Fields("CDJournalArchiveDate", rsInfo.Get_Fields(rsInfo.Get_FieldsIndexName(fnx)));
						}
						else
						{
							rsArchiveCDInfo.Set_Fields(rsInfo.Get_FieldsIndexName(fnx), rsInfo.Get_Fields(rsInfo.Get_FieldsIndexName(fnx)));
						}
						TRYAGAIN3:
						;
					}
					rsArchiveCDInfo.Update(true);
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			imgSave1099Processing.Visible = false;
			imgSave1099.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return Save1099;
		}

		private bool ClearDetailAmounts()
		{
			bool ClearDetailAmounts = false;
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			string strAccount = "";
			string strIDs;
			clsDRWrapper rsReprint = new clsDRWrapper();
			string strCheck = "";
			imgClearDetailsProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			ClearDetailAmounts = true;
			// update general ledger account balances
			rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY Account");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					rsAccountInfo.Edit();
					if (rsLedgerYTDActivity.FindFirstRecord("Account", strAccount))
					{
						// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						rsAccountInfo.Set_Fields("CurrentBudget", FCConvert.ToDouble(rsAccountInfo.Get_Fields("CurrentBudget")) + FCConvert.ToDouble(rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal")) + GetYTDNet(ref strAccount));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
						rsAccountInfo.Set_Fields("CurrentBudget", FCConvert.ToDouble(rsAccountInfo.Get_Fields("CurrentBudget")) + GetYTDNet(ref strAccount));
					}
					rsAccountInfo.Update(true);
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			// delete records from AP tables
			rsAccountInfo.Execute("DELETE FROM JournalMaster WHERE Status = 'P' OR Status = 'D'", "Budgetary");
			rsAccountInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P'");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					rsDetailInfo.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + rsAccountInfo.Get_Fields_Int32("ID"), "Budgetary");
					rsAccountInfo.Delete();
					rsAccountInfo.Update();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			strIDs = "";
			rsAccountInfo.OpenRecordset("SELECT * FROM APJournal WHERE PrintedIndividual = 1", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Field [Check] not found!! (maybe it is an alias?)
					rsDetailInfo.Execute("SELECT * FROM TempCheckFile WHERE CheckNumber = " + rsAccountInfo.Get_Fields("Check") + " AND VendorNumber = " + rsAccountInfo.Get_Fields_Int32("VendorNumber") + " AND PrintedIndividual = 1", "Budgetary");
					if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
					{
						strIDs += rsDetailInfo.Get_Fields_Int32("ID") + ", ";
						if (FCConvert.ToBoolean(rsDetailInfo.Get_Fields_Boolean("ReprintedCheck")))
						{
							strCheck = FCConvert.ToString(rsDetailInfo.Get_Fields_String("OldCheckNumber"));
							do
							{
								rsReprint.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + strCheck + " AND VendorNumber = " + rsAccountInfo.Get_Fields_Int32("VendorNumber") + " AND PrintedIndividual = 1", "Budgetary");
								if (rsReprint.EndOfFile() != true && rsReprint.BeginningOfFile() != true)
								{
									strIDs += rsReprint.Get_Fields_Int32("ID") + ", ";
									strCheck = FCConvert.ToString(rsReprint.Get_Fields_String("OldCheckNumber"));
								}
								else
								{
									break;
								}
							}
							while (rsReprint.Get_Fields_Boolean("ReprintedCheck"));
						}
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
            rsAccountInfo.Execute("update apjournal set status = 'E', creditmemorecord = 0, warrant = 0 where status = 'V'", "Budgetary");
            rsAccountInfo.Execute("update journalmaster set status = 'E' where status = 'V'", "Budgetary");
			// delete records from CM tables
			rsAccountInfo.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'P' AND Amount + Adjustments - Liquidated = 0");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					rsDetailInfo.Execute("DELETE FROM CreditMemoDetail WHERE CreditMemoID = " + rsAccountInfo.Get_Fields_Int32("ID"), "Budgetary");
					rsAccountInfo.Delete();
					rsAccountInfo.Update();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}

			rsAccountInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P'");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					rsDetailInfo.Execute("DELETE FROM EncumbranceDetail WHERE EncumbranceID = " + rsAccountInfo.Get_Fields_Int32("ID"), "Budgetary");
					rsAccountInfo.Delete();
					rsAccountInfo.Update();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			// End If
			rsAccountInfo.Execute("DELETE FROM JournalEntries WHERE Status = 'P'", "Budgetary");
			if (strIDs != "")
			{
				strIDs = "(" + Strings.Left(strIDs, strIDs.Length - 2) + ")";
				rsAccountInfo.Execute("DELETE FROM TempCheckFile WHERE ID NOT IN " + strIDs, "Budgetary");
			}
			else
			{
				rsAccountInfo.Execute("DELETE FROM TempCheckFile", "Budgetary");
			}
			rsAccountInfo.Execute("DELETE FROM VendorTaxInfo", "Budgetary");
			rsAccountInfo.Execute("DELETE FROM WarrantMaster WHERE rtrim(convert(nvarchar(10), WarrantNumber)) NOT IN (SELECT DISTINCT Warrant FROM APJournal WHERE Warrant <> '0' and Warrant <> '')", "Budgetary");
			rsAccountInfo.Execute("DELETE FROM Reprint WHERE rtrim(convert(nvarchar(10), WarrantNumber)) NOT IN (SELECT DISTINCT Warrant FROM APJournal WHERE Warrant <> '0' and Warrant <> '')", "Budgetary");
			rsAccountInfo.Execute("DELETE FROM EOYAdjustments WHERE Year <> " + FCConvert.ToString(Conversion.Val(strYear)), "Budgetary");
			rsAccountInfo.Execute("DELETE FROM AccountMaster WHERE AccountType = 'E' AND Valid = 0", "Budgetary");
			imgClearDetailsProcessing.Visible = false;
			imgClearDetail.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return ClearDetailAmounts;
		}

		private double GetBudgetAdjustments(ref string strAcct)
		{
			double GetBudgetAdjustments = 0;
			int temp;
			int HighDate;
			int LowDate;
			int intExpStart;
			string strPeriodCheck = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			HighDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (HighDate == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate -= 1;
			}
			LowDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rs2.OpenRecordset("SELECT SUM(Amount) AS BudAdj FROM JournalEntries WHERE Account = '" + strAcct + "' AND RCB = 'B' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
			// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs2.Get_Fields("BudAdj")) == ""))
			{
				// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
				GetBudgetAdjustments = FCConvert.ToDouble(rs2.Get_Fields("BudAdj"));
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private double GetCarryOver(ref string strAcct)
		{
			double GetCarryOver = 0;
			int temp;
			int HighDate;
			int LowDate;
			int intExpStart;
			string strPeriodCheck = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			HighDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (HighDate == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate -= 1;
			}
			LowDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rs2.OpenRecordset("SELECT SUM(Amount) AS BudAdj FROM JournalEntries WHERE Account = '" + strAcct + "' AND RCB = 'B' and CarryForward = 1 AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
			// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs2.Get_Fields("BudAdj")) == ""))
			{
				// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
				GetCarryOver = FCConvert.ToDouble(rs2.Get_Fields("BudAdj"));
			}
			else
			{
				GetCarryOver = 0;
			}
			return GetCarryOver;
		}

		private bool CarryOver(List<string> batchReports)
		{
			bool CarryOver = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strUnliqEncAcct = "";
			clsDRWrapper Master = new clsDRWrapper();
			int TempJournal = 0;
			clsDRWrapper rs = new clsDRWrapper();
			int counter;
			int lngFund = 0;
			// vbPorter upgrade warning: datDateToUse As DateTime	OnWrite(string)
			DateTime datDateToUse;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotal = 0.0M;
			int intHeaderRow = 0;
			int intRecordNumber = 0;
			clsDRWrapper rsDetails = new clsDRWrapper();
			CarryOver = true;
			imgCarryOverProcessing.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);

			rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EC'");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Conversion.Val(rsTemp.Get_Fields("Account")) == 0)
				{
					MessageBox.Show("You must set up a Town Expense Control Account before you may continue.", "No Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CarryOver = false;
					return CarryOver;
				}
			}
			else
			{
				MessageBox.Show("You must set up a Town Expense Control Account before you may continue.", "No Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CarryOver = false;
				return CarryOver;
			}
			rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'UE'");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Conversion.Val(rsTemp.Get_Fields("Account")) == 0)
				{
					MessageBox.Show("You must set up a Town Unliquidated Encumbrance Account before you may continue.", "No Town Unliquidated Encumbrance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CarryOver = false;
					return CarryOver;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strUnliqEncAcct = FCConvert.ToString(rsTemp.Get_Fields("Account"));
				}
			}
			else
			{
				MessageBox.Show("You must set up a Town Unliquidated Encumbrance Account before you may continue.", "No Town Unliquidated Encumbrance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CarryOver = false;
				return CarryOver;
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("EncCarryForward")) != 0)
			{
				if (modBudgetaryAccounting.Statics.strPastYearEncControlAccount == "" || Conversion.Val(modBudgetaryAccounting.Statics.strPastYearEncControlAccount) == 0)
				{
					MessageBox.Show("You must set up a Town Past Year Encumbrance Control Account before you may continue.", "No Town Past Year Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CarryOver = false;
					return CarryOver;
				}
			}
			// End If
			if (modBudgetaryMaster.Statics.FirstMonth == 1 && DateTime.Today.Month > 9)
			{
				datDateToUse = FCConvert.ToDateTime(FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
			}
			else
			{
				datDateToUse = FCConvert.ToDateTime(FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
			}
			if (modBudgetaryAccounting.LockJournal() == false)
			{
				MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CarryOver = false;
				return CarryOver;
			}
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
			{
				Master.MoveLast();
				Master.MoveFirst();
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
			}
			else
			{
				TempJournal = 1;
			}
			Master.AddNew();
			Master.Set_Fields("JournalNumber", TempJournal);
			Master.Set_Fields("Status", "E");
			Master.Set_Fields("Description", "Enc Carry Over Entries");
			Master.Set_Fields("Type", "GJ");
			Master.Set_Fields("Period", datDateToUse.Month);
			Master.Update();
			modBudgetaryAccounting.UnlockJournal();
			lngClosingJournal = TempJournal;
			clsJournalInfo = new clsPostingJournalInfo();
			clsJournalInfo.JournalNumber = lngClosingJournal;
			clsJournalInfo.Period = FCConvert.ToString(datDateToUse.Month);
			clsJournalInfo.JournalType = "GJ";
			clsJournalInfo.CheckDate = "";
			clsPostInfo.ClearJournals();
			clsPostInfo.AddJournal(clsJournalInfo);
			rs.OmitNullsOnInsert = true;
			rs.OpenRecordset("SELECT * FROM JournalEntries");
			for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
			{
				if (vsEncumbrances.RowOutlineLevel(counter) == 1)
				{
					if (FCUtils.CBool(vsEncumbrances.TextMatrix(counter, SelectCol)) == true)
					{
						lngFund = modBudgetaryAccounting.GetFundFromAccount(vsEncumbrances.TextMatrix(counter, AccountCol));
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", datDateToUse);
						rs.Set_Fields("Description", "Enc Carry Over Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", datDateToUse.Month);
						rs.Set_Fields("Project", "");
					
						if (!modAccountTitle.Statics.YearFlag)
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strUnliqEncAcct + "-00");
						}
						else
						{
							rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strUnliqEncAcct);
						}

						rs.Set_Fields("RCB", "R");
						rs.Set_Fields("Amount", vsEncumbrances.TextMatrix(counter, AmountCol));
						rs.Set_Fields("Status", "E");
						rs.Update();
						rs.AddNew();
						rs.Set_Fields("Type", "G");
						rs.Set_Fields("JournalEntriesDate", datDateToUse);
						rs.Set_Fields("Description", "Enc Carry Over Entries");
						rs.Set_Fields("JournalNumber", TempJournal);
						rs.Set_Fields("Period", datDateToUse.Month);
						rs.Set_Fields("Project", "");
						if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("EncCarryForward")) != 0)
						{
							if (!modAccountTitle.Statics.YearFlag)
							{
								rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strPastYearEncControlAccount + "-00");
							}
							else
							{
								rs.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strPastYearEncControlAccount);
							}
						}
						else
						{
							rs.Set_Fields("account", vsEncumbrances.TextMatrix(counter, AccountCol));
						}
						rs.Set_Fields("RCB", "B");
						rs.Set_Fields("CarryForward", true);
						rs.Set_Fields("Amount", FCConvert.ToDecimal(vsEncumbrances.TextMatrix(counter, AmountCol)) * -1);
						rs.Set_Fields("Status", "E");
						rs.Update();
					}
				}
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("EncCarryForward")) != 0)
			{
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("Description", "Enc Carry Over Entries");
				Master.Set_Fields("Type", "EN");
				Master.Set_Fields("Period", datDateToUse.Month);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				// lngClosingJournal = TempJournal
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalNumber = TempJournal;
				clsJournalInfo.Period = FCConvert.ToString(datDateToUse.Month);
				clsJournalInfo.JournalType = "EN";
				clsJournalInfo.CheckDate = "";
				clsPostInfo.AddJournal(clsJournalInfo);
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM Encumbrances");
				rsDetails.OpenRecordset("SELECT * FROM EncumbranceDetail");
				for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
				{
					if (vsEncumbrances.RowOutlineLevel(counter) == 0)
					{
						if (curTotal != 0)
						{
                            rs.Edit();
							rs.Set_Fields("Amount", curTotal);
							rs.Update();
						}
						curTotal = 0;
						intHeaderRow = counter;
						intRecordNumber = 0;
					}
					else if (vsEncumbrances.RowOutlineLevel(counter) == 1)
					{
						if (FCUtils.CBool(vsEncumbrances.TextMatrix(counter, SelectCol)) == true)
						{
							if (intRecordNumber == 0)
							{
                                rs.OpenRecordset("select * from encumbrances where id = -1", "Budgetary");
								rs.AddNew();
								rs.Set_Fields("JournalNumber", TempJournal);
								rs.Set_Fields("VendorNumber", vsEncumbrances.TextMatrix(intHeaderRow, VendorNumberCol));
								if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
								{
									rs.Set_Fields("TempVendorName", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorNameCol));
									rs.Set_Fields("TempVendorAddress1", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorAddress1Col));
									rs.Set_Fields("TempVendorAddress2", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorAddress2Col));
									rs.Set_Fields("TempVendorAddress3", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorAddress3Col));
									rs.Set_Fields("TempVendorCity", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorCityCol));
									rs.Set_Fields("TempVendorState", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorStateCol));
									rs.Set_Fields("TempVendorZip", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorZipCol));
									rs.Set_Fields("TempVendorZip4", vsEncumbrances.TextMatrix(intHeaderRow, TempVendorZip4Col));
								}
								rs.Set_Fields("EncumbrancesDate", datDateToUse);
								rs.Set_Fields("Description", vsEncumbrances.TextMatrix(intHeaderRow, DescriptionCol));
								rs.Set_Fields("Period", datDateToUse.Month);
								// rs.Fields["PastYearEnc"] = True
								rs.Set_Fields("PO", vsEncumbrances.TextMatrix(intHeaderRow, POCol));
								rs.Set_Fields("Status", "E");
                                if (FCConvert.ToDouble (vsEncumbrances.TextMatrix(counter,AmountCol)) > 0)
                                {
                                    rs.Update();
                                    intRecordNumber = rs.Get_Fields_Int32("id");
                                }
							}
							rsDetails.OmitNullsOnInsert = true;
							lngFund = modBudgetaryAccounting.GetFundFromAccount(vsEncumbrances.TextMatrix(counter, AccountCol));
							rsDetails.AddNew();
							// if so add it
							rsDetails.Set_Fields("EncumbranceID", intRecordNumber);
							rsDetails.Set_Fields("Description", vsEncumbrances.TextMatrix(counter, DescriptionCol));
							if (!modAccountTitle.Statics.YearFlag)
							{
								rsDetails.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strPastYearEncControlAccount + "-00");
							}
							else
							{
								rsDetails.Set_Fields("account", "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(lngFund)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strPastYearEncControlAccount);
							}
							rsDetails.Set_Fields("Amount", vsEncumbrances.TextMatrix(counter, AmountCol));
							curTotal += FCConvert.ToDecimal(vsEncumbrances.TextMatrix(counter, AmountCol));
							rsDetails.Set_Fields("Project", "");
							rsDetails.Update();
							// update the database
						}
					}
				}
				if (curTotal != 0)
				{
                    rs.Edit();
					rs.Set_Fields("Amount", curTotal);
					rs.Update();
				}
			}
			clsPostInfo.PageBreakBetweenJournalsOnReport = true;
			clsPostInfo.SavePostingReport = false;
			clsPostInfo.AllowPreview = false;
			if (!clsPostInfo.PostJournals(batchReports: batchReports))
			{
				MessageBox.Show("End of Year Processing is Stopping because there is a problem posting the Encumbrance Carry Over Entries in Journal " + modValidateAccount.GetFormat_6(FCConvert.ToString(lngClosingJournal), 4), "Unable to Post Encumbrance Carry Over Entries", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CarryOver = false;
				return CarryOver;
			}
			imgCarryOverProcessing.Visible = false;
			imgCarryOver.Visible = true;
			this.Refresh();
			FCUtils.ApplicationUpdate(this);
			return CarryOver;
		}

		private void optNo_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//cmdCancel_Click();
			if (sender == cmbNo)
			{
				switch (cmbNo.SelectedIndex)
				{
					case 0:
						{
							cmdCancel_Click();
							break;
						}
					case 1:
						{
							optYes_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void optYes_CheckedChanged(object sender, System.EventArgs e)
		{
			fraEncumbrances.Visible = true;
			vsEncumbrances.Focus();
		}

		private void vsEncumbrances_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vsEncumbrances_AfterCollapse(this.vsEncumbrances.GetFlexRowIndex(e.RowIndex), false);
		}

		private void vsEncumbrances_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vsEncumbrances_AfterCollapse(this.vsEncumbrances.GetFlexRowIndex(e.RowIndex), true);
		}

		private void vsEncumbrances_AfterCollapse(int row, bool isCollapsed)
		{
			int temp;
			int counter;
			int rows = 0;
			int height;
			bool DeptFlag = false;
			bool DivisionFlag;
			for (counter = 0; counter <= vsEncumbrances.Rows - 1; counter++)
			{
				if (vsEncumbrances.RowOutlineLevel(counter) == 0)
				{
					if (vsEncumbrances.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						DeptFlag = true;
					}
					else
					{
						rows += 1;
						DeptFlag = false;
					}
				}
				else
				{
					if (DeptFlag == true)
					{
						// do nothing
					}
					else
					{
						rows += 1;
					}
				}
			}
			//if (rows > 0)
			//{
			//	if (rows >= 13)
			//	{
			//		vsEncumbrances.Height = 13 * vsEncumbrances.RowHeight(0) + 75;
			//	}
			//	else
			//	{
			//		vsEncumbrances.Height = rows * vsEncumbrances.RowHeight(0) + 75;
			//	}
			//}
		}

		private void vsEncumbrances_ClickEvent(object sender, System.EventArgs e)
		{
			int counter;
			if (vsEncumbrances.Row > 0)
			{
				if (FCUtils.CBool(vsEncumbrances.TextMatrix(vsEncumbrances.Row, SelectCol)) == true)
				{
					vsEncumbrances.TextMatrix(vsEncumbrances.Row, SelectCol, FCConvert.ToString(false));
					if (vsEncumbrances.RowOutlineLevel(vsEncumbrances.Row) == 0)
					{
						for (counter = vsEncumbrances.Row + 1; counter <= vsEncumbrances.Rows - 1; counter++)
						{
							if (vsEncumbrances.RowOutlineLevel(counter) == 0)
							{
								break;
							}
							else
							{
								vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
							}
						}
					}
				}
				else
				{
					vsEncumbrances.TextMatrix(vsEncumbrances.Row, SelectCol, FCConvert.ToString(true));
					if (vsEncumbrances.RowOutlineLevel(vsEncumbrances.Row) == 0)
					{
						for (counter = vsEncumbrances.Row + 1; counter <= vsEncumbrances.Rows - 1; counter++)
						{
							if (vsEncumbrances.RowOutlineLevel(counter) == 0)
							{
								break;
							}
							else
							{
								vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
							}
						}
					}
				}
			}
		}

		private void vsEncumbrances_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int counter;
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (FCUtils.CBool(vsEncumbrances.TextMatrix(vsEncumbrances.Row, SelectCol)) == true)
				{
					vsEncumbrances.TextMatrix(vsEncumbrances.Row, SelectCol, FCConvert.ToString(false));
					if (vsEncumbrances.RowOutlineLevel(vsEncumbrances.Row) == 0)
					{
						for (counter = vsEncumbrances.Row + 1; counter <= vsEncumbrances.Rows - 1; counter++)
						{
							if (vsEncumbrances.RowOutlineLevel(counter) == 0)
							{
								break;
							}
							else
							{
								vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
							}
						}
					}
				}
				else
				{
					vsEncumbrances.TextMatrix(vsEncumbrances.Row, SelectCol, FCConvert.ToString(true));
					if (vsEncumbrances.RowOutlineLevel(vsEncumbrances.Row) == 0)
					{
						for (counter = vsEncumbrances.Row + 1; counter <= vsEncumbrances.Rows - 1; counter++)
						{
							if (vsEncumbrances.RowOutlineLevel(counter) == 0)
							{
								break;
							}
							else
							{
								vsEncumbrances.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
							}
						}
					}
				}
			}
		}

		private bool CloseExp(ref DateTime datDateToUse)
		{
			bool CloseExp = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			string strAccount = "";
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			rsControlEntry.OmitNullsOnInsert = true;
			CloseExp = true;
			rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EC'");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				strAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
			}
			else
			{
				MessageBox.Show("You must set up a Town Expense Control Account before you may continue.", "No Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CloseExp = false;
				return CloseExp;
			}
			rsControlEntry.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'E' AND JournalNumber = " + FCConvert.ToString(lngClosingJournal) + " AND Period = " + FCConvert.ToString(datDateToUse.Month) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")");
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
					{
						rsControlEntry.AddNew();
						rsControlEntry.Set_Fields("JournalNumber", lngClosingJournal);
						rsControlEntry.Set_Fields("Description", "Expense CTL");
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + strAccount + "-00");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + strAccount);
						}
						rsControlEntry.Set_Fields("RCB", "L");
						// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
						rsControlEntry.Set_Fields("Amount", rsTemp.Get_Fields("JournalTotal"));
						rsControlEntry.Set_Fields("Status", "P");
						rsControlEntry.Set_Fields("PostedDate", datDateToUse);
						rsControlEntry.Set_Fields("Period", datDateToUse.Month);
						rsControlEntry.Set_Fields("JournalEntriesDate", datDateToUse);
						rsControlEntry.Set_Fields("Type", "G");
						rsControlEntry.Update();
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			return CloseExp;
		}

		private void SetCustomFormColors()
		{
			lblWarning.ForeColor = Color.Red;
		}

		private void CalculateEndingBalances()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			string strAccount = "";
			rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY Account");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsInfo.Edit();
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
					if (rsLedgerYTDActivity.FindFirstRecord("Account", strAccount))
					{
						// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						rsInfo.Set_Fields("PreviousYearEndingBalance", FCConvert.ToDouble(rsInfo.Get_Fields("CurrentBudget")) + FCConvert.ToDouble(rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal")) + GetYTDNet(ref strAccount));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
						rsInfo.Set_Fields("PreviousYearEndingBalance", FCConvert.ToDouble(rsInfo.Get_Fields("CurrentBudget")) + GetYTDNet(ref strAccount));
					}
					rsInfo.Update(true);
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private bool CloseEnc(ref DateTime datDateToUse, ref short intPer, ref int lngJournal)
		{
			bool CloseEnc = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsTemp2 = new clsDRWrapper();
			clsDRWrapper rsTemp3 = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			string strAccount = "";
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			int lngCloseoutKey;
			CloseEnc = true;
			rsControlEntry.OmitNullsOnInsert = true;
			rsControlEntry.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = 0");
			rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = 0");
			rsGeneralInfo.AddNew();
			rsGeneralInfo.Set_Fields("JournalNumber", lngJournal);
			rsGeneralInfo.Set_Fields("Description", "Control Entries");
			rsGeneralInfo.Set_Fields("Status", "P");
			rsGeneralInfo.Set_Fields("EncumbrancesDate", datDateToUse);
			rsGeneralInfo.Set_Fields("Period", intPer);
			rsGeneralInfo.Update();
			// TODO Get_Fields: Field [Number] not found!! (maybe it is an alias?)
			lngCloseoutKey = FCConvert.ToInt32(rsGeneralInfo.Get_Fields("Number"));
			rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(lngCloseoutKey));
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					strJournalSQL = "SELECT Number FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPer);
					rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'R' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")");
					rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'E' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")");
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'");
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal"))) != 0)
					{
						rsControlEntry.AddNew();
						rsControlEntry.Set_Fields("EncumbranceID", lngCloseoutKey);
						rsControlEntry.Set_Fields("Description", "Encumbrance CTL");
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + modBudgetaryAccounting.Statics.strEncOffAccount + "-00");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + modBudgetaryAccounting.Statics.strEncOffAccount);
						}
						rsControlEntry.Set_Fields("Project", "CTRL");
						// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
						rsControlEntry.Set_Fields("Amount", (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal"))) * -1);
						rsControlEntry.Update();
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			return CloseEnc;
		}

		private void LogError(string strLogEntry)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.OpenRecordset("select * from applicationlog where id = -1", "CentralData");
			rsSave.AddNew();
			rsSave.Set_Fields("Entry", strLogEntry);
			rsSave.Set_Fields("EntryTimeStamp", DateTime.Now);
			rsSave.Set_Fields("UserID", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			rsSave.Set_Fields("User", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
			rsSave.Set_Fields("Origin", "BD EOY Process");
			rsSave.Update();
		}

        private int GetLatestArchiveYear()
        {
            var rsLoad = new clsDRWrapper();
            int intYear = 0;
            rsLoad.OpenRecordset("Select * from Archives where ArchiveType = 'Archive' Order by ArchiveID Desc","SystemSettings");
            if (!rsLoad.EndOfFile())
            {
                intYear = rsLoad.Get_Fields_Int32("archiveid");
            }

            return intYear;
        }
	}
}
