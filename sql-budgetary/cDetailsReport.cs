﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public interface cDetailsReport
	{
		//=========================================================
		cGenericCollection Details
		{
			get;
		}
	}
}
