﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeLedgerSummary.
	/// </summary>
	partial class frmCustomizeLedgerSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWide;
		public fecherFoundation.FCLabel lblWide;
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbShortDescriptions;
		public fecherFoundation.FCLabel lblShortDescriptions;
		public fecherFoundation.FCComboBox cmbShowPending;
		public fecherFoundation.FCLabel lblShowPending;
		public fecherFoundation.FCComboBox cmbShowOutstandingEncumbrance;
		public fecherFoundation.FCLabel lblShowOutstanding;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCFrame fraPreferences;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCFrame fraBudget;
		public fecherFoundation.FCCheckBox chkDCBalance;
		public fecherFoundation.FCCheckBox chkBudget;
		public fecherFoundation.FCCheckBox chkAdjustments;
		public fecherFoundation.FCCheckBox chkNetBudget;
		public fecherFoundation.FCCheckBox chkBalance;
		public fecherFoundation.FCFrame fraSelectedMonths;
		public fecherFoundation.FCCheckBox chkSelectedNet;
		public fecherFoundation.FCCheckBox chkSelectedDebitsCredits;
		public fecherFoundation.FCFrame fraYTD;
		public fecherFoundation.FCCheckBox chkYTDNet;
		public fecherFoundation.FCCheckBox chkYTDDebitsCredits;
		public fecherFoundation.FCLabel lblNumber;
		public fecherFoundation.FCLabel lblCount;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeLedgerSummary));
            this.cmbWide = new fecherFoundation.FCComboBox();
            this.lblWide = new fecherFoundation.FCLabel();
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.cmbShortDescriptions = new fecherFoundation.FCComboBox();
            this.lblShortDescriptions = new fecherFoundation.FCLabel();
            this.cmbShowPending = new fecherFoundation.FCComboBox();
            this.lblShowPending = new fecherFoundation.FCLabel();
            this.cmbShowOutstandingEncumbrance = new fecherFoundation.FCComboBox();
            this.lblShowOutstanding = new fecherFoundation.FCLabel();
            this.cdgFont = new fecherFoundation.FCCommonDialog();
            this.fraPreferences = new fecherFoundation.FCFrame();
            this.fraInformation = new fecherFoundation.FCFrame();
            this.fraBudget = new fecherFoundation.FCFrame();
            this.chkDCBalance = new fecherFoundation.FCCheckBox();
            this.chkBudget = new fecherFoundation.FCCheckBox();
            this.chkAdjustments = new fecherFoundation.FCCheckBox();
            this.chkNetBudget = new fecherFoundation.FCCheckBox();
            this.chkBalance = new fecherFoundation.FCCheckBox();
            this.fraSelectedMonths = new fecherFoundation.FCFrame();
            this.chkSelectedNet = new fecherFoundation.FCCheckBox();
            this.chkSelectedDebitsCredits = new fecherFoundation.FCCheckBox();
            this.fraYTD = new fecherFoundation.FCFrame();
            this.chkYTDNet = new fecherFoundation.FCCheckBox();
            this.chkYTDDebitsCredits = new fecherFoundation.FCCheckBox();
            this.lblNumber = new fecherFoundation.FCLabel();
            this.lblCount = new fecherFoundation.FCLabel();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.lblDescription = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).BeginInit();
            this.fraPreferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
            this.fraInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBudget)).BeginInit();
            this.fraBudget.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDCBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBudget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNetBudget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedMonths)).BeginInit();
            this.fraSelectedMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedDebitsCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTD)).BeginInit();
            this.fraYTD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebitsCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Controls.Add(this.fraInformation);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.fraPreferences);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(366, 30);
            this.HeaderText.Text = "Ledger Summary Report Format";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbWide
            // 
            this.cmbWide.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
            this.cmbWide.Location = new System.Drawing.Point(506, 30);
            this.cmbWide.Name = "cmbWide";
            this.cmbWide.Size = new System.Drawing.Size(200, 40);
            this.cmbWide.TabIndex = 29;
            this.cmbWide.Text = "Portrait";
            this.ToolTip1.SetToolTip(this.cmbWide, null);
            this.cmbWide.SelectedIndexChanged += new System.EventHandler(this.optWide_CheckedChanged);
            // 
            // lblWide
            // 
            this.lblWide.AutoSize = true;
            this.lblWide.Location = new System.Drawing.Point(362, 44);
            this.lblWide.Name = "lblWide";
            this.lblWide.Size = new System.Drawing.Size(93, 15);
            this.lblWide.TabIndex = 1;
            this.lblWide.Text = "PAPER WIDTH";
            this.lblWide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblWide, null);
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "Print",
            "Display"});
            this.cmbPrint.Location = new System.Drawing.Point(506, 90);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(200, 40);
            this.cmbPrint.TabIndex = 4;
            this.cmbPrint.Text = "Print";
            this.ToolTip1.SetToolTip(this.cmbPrint, null);
            this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.optPrint_CheckedChanged);
            // 
            // lblPrint
            // 
            this.lblPrint.AutoSize = true;
            this.lblPrint.Location = new System.Drawing.Point(362, 104);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(88, 15);
            this.lblPrint.TabIndex = 5;
            this.lblPrint.Text = "REPORT USE";
            this.lblPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblPrint, null);
            // 
            // cmbShortDescriptions
            // 
            this.cmbShortDescriptions.Items.AddRange(new object[] {
            "Short Descriptions",
            "Long Descriptions"});
            this.cmbShortDescriptions.Location = new System.Drawing.Point(142, 30);
            this.cmbShortDescriptions.Name = "cmbShortDescriptions";
            this.cmbShortDescriptions.Size = new System.Drawing.Size(200, 40);
            this.cmbShortDescriptions.TabIndex = 6;
            this.cmbShortDescriptions.Text = "Short Descriptions";
            this.ToolTip1.SetToolTip(this.cmbShortDescriptions, null);
            this.cmbShortDescriptions.SelectedIndexChanged += new System.EventHandler(this.optShortDescriptions_CheckedChanged);
            // 
            // lblShortDescriptions
            // 
            this.lblShortDescriptions.AutoSize = true;
            this.lblShortDescriptions.Location = new System.Drawing.Point(20, 44);
            this.lblShortDescriptions.Name = "lblShortDescriptions";
            this.lblShortDescriptions.Size = new System.Drawing.Size(100, 15);
            this.lblShortDescriptions.TabIndex = 7;
            this.lblShortDescriptions.Text = "DESCRIPTIONS";
            this.ToolTip1.SetToolTip(this.lblShortDescriptions, null);
            // 
            // cmbShowPending
            // 
            this.cmbShowPending.Items.AddRange(new object[] {
            "Show Pending Activity Separately",
            "Include Pending Activity in YTD Account Information",
            "Do Not Include Pending Activity"});
            this.cmbShowPending.Location = new System.Drawing.Point(523, 149);
            this.cmbShowPending.Name = "cmbShowPending";
            this.cmbShowPending.Size = new System.Drawing.Size(430, 40);
            this.cmbShowPending.TabIndex = 30;
            this.cmbShowPending.Text = "Include Pending Activity in YTD Account Information";
            this.ToolTip1.SetToolTip(this.cmbShowPending, null);
            this.cmbShowPending.SelectedIndexChanged += new System.EventHandler(this.optShowPending_CheckedChanged);
            // 
            // lblShowPending
            // 
            this.lblShowPending.AutoSize = true;
            this.lblShowPending.Location = new System.Drawing.Point(273, 163);
            this.lblShowPending.Name = "lblShowPending";
            this.lblShowPending.Size = new System.Drawing.Size(123, 15);
            this.lblShowPending.TabIndex = 31;
            this.lblShowPending.Text = "PENDING ACTIVITY";
            this.lblShowPending.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblShowPending, null);
            // 
            // cmbShowOutstandingEncumbrance
            // 
            this.cmbShowOutstandingEncumbrance.Items.AddRange(new object[] {
            "Show Encumbrances Separately",
            "Include Encumbrances in Regular Account Information"});
            this.cmbShowOutstandingEncumbrance.Location = new System.Drawing.Point(523, 209);
            this.cmbShowOutstandingEncumbrance.Name = "cmbShowOutstandingEncumbrance";
            this.cmbShowOutstandingEncumbrance.Size = new System.Drawing.Size(430, 40);
            this.cmbShowOutstandingEncumbrance.TabIndex = 32;
            this.cmbShowOutstandingEncumbrance.Text = "Show Encumbrances Separately";
            this.ToolTip1.SetToolTip(this.cmbShowOutstandingEncumbrance, null);
            this.cmbShowOutstandingEncumbrance.SelectedIndexChanged += new System.EventHandler(this.optShowOutstandingEncumbrance_CheckedChanged);
            // 
            // lblShowOutstanding
            // 
            this.lblShowOutstanding.AutoSize = true;
            this.lblShowOutstanding.Location = new System.Drawing.Point(273, 223);
            this.lblShowOutstanding.Name = "lblShowOutstanding";
            this.lblShowOutstanding.Size = new System.Drawing.Size(206, 15);
            this.lblShowOutstanding.TabIndex = 33;
            this.lblShowOutstanding.Text = "OUTSTANDING ENCUMBRANCES";
            this.lblShowOutstanding.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblShowOutstanding, null);
            // 
            // cdgFont
            // 
            this.cdgFont.Name = "cdgFont";
            this.cdgFont.Size = new System.Drawing.Size(0, 0);
            this.ToolTip1.SetToolTip(this.cdgFont, null);
            // 
            // fraPreferences
            // 
            this.fraPreferences.AppearanceKey = "groupBoxLeftBorder";
            this.fraPreferences.Controls.Add(this.lblWide);
            this.fraPreferences.Controls.Add(this.lblShortDescriptions);
            this.fraPreferences.Controls.Add(this.cmbPrint);
            this.fraPreferences.Controls.Add(this.lblPrint);
            this.fraPreferences.Controls.Add(this.cmbShortDescriptions);
            this.fraPreferences.Controls.Add(this.cmbWide);
            this.fraPreferences.Location = new System.Drawing.Point(30, 100);
            this.fraPreferences.Name = "fraPreferences";
            this.fraPreferences.Size = new System.Drawing.Size(730, 141);
            this.fraPreferences.TabIndex = 23;
            this.fraPreferences.Text = "General Preferences";
            this.ToolTip1.SetToolTip(this.fraPreferences, null);
            // 
            // fraInformation
            // 
            this.fraInformation.AppearanceKey = "groupBoxLeftBorder";
            this.fraInformation.Controls.Add(this.fraBudget);
            this.fraInformation.Controls.Add(this.cmbShowPending);
            this.fraInformation.Controls.Add(this.lblShowPending);
            this.fraInformation.Controls.Add(this.cmbShowOutstandingEncumbrance);
            this.fraInformation.Controls.Add(this.lblShowOutstanding);
            this.fraInformation.Controls.Add(this.fraSelectedMonths);
            this.fraInformation.Controls.Add(this.fraYTD);
            this.fraInformation.Controls.Add(this.lblNumber);
            this.fraInformation.Controls.Add(this.lblCount);
            this.fraInformation.Location = new System.Drawing.Point(30, 237);
            this.fraInformation.Name = "fraInformation";
            this.fraInformation.Size = new System.Drawing.Size(973, 360);
            this.fraInformation.TabIndex = 18;
            this.fraInformation.Text = "Information To Be Reported";
            this.ToolTip1.SetToolTip(this.fraInformation, null);
            // 
            // fraBudget
            // 
            this.fraBudget.Controls.Add(this.chkDCBalance);
            this.fraBudget.Controls.Add(this.chkBudget);
            this.fraBudget.Controls.Add(this.chkAdjustments);
            this.fraBudget.Controls.Add(this.chkNetBudget);
            this.fraBudget.Controls.Add(this.chkBalance);
            this.fraBudget.Location = new System.Drawing.Point(20, 149);
            this.fraBudget.Name = "fraBudget";
            this.fraBudget.Size = new System.Drawing.Size(232, 191);
            this.fraBudget.TabIndex = 29;
            this.fraBudget.Text = "Account Information";
            this.ToolTip1.SetToolTip(this.fraBudget, null);
            // 
            // chkDCBalance
            // 
            this.chkDCBalance.Location = new System.Drawing.Point(20, 150);
            this.chkDCBalance.Name = "chkDCBalance";
            this.chkDCBalance.Size = new System.Drawing.Size(203, 27);
            this.chkDCBalance.TabIndex = 41;
            this.chkDCBalance.Text = "Debits / Credits Balance";
            this.ToolTip1.SetToolTip(this.chkDCBalance, null);
            this.chkDCBalance.CheckedChanged += new System.EventHandler(this.chkDCBalance_CheckedChanged);
            // 
            // chkBudget
            // 
            this.chkBudget.Location = new System.Drawing.Point(20, 30);
            this.chkBudget.Name = "chkBudget";
            this.chkBudget.Size = new System.Drawing.Size(164, 27);
            this.chkBudget.TabIndex = 40;
            this.chkBudget.Text = "Beginning Balance";
            this.ToolTip1.SetToolTip(this.chkBudget, null);
            this.chkBudget.CheckedChanged += new System.EventHandler(this.chkBudget_CheckedChanged);
            // 
            // chkAdjustments
            // 
            this.chkAdjustments.Location = new System.Drawing.Point(20, 60);
            this.chkAdjustments.Name = "chkAdjustments";
            this.chkAdjustments.Size = new System.Drawing.Size(182, 27);
            this.chkAdjustments.TabIndex = 39;
            this.chkAdjustments.Text = "Balance Adjustments";
            this.ToolTip1.SetToolTip(this.chkAdjustments, null);
            this.chkAdjustments.CheckedChanged += new System.EventHandler(this.chkAdjustments_CheckedChanged);
            // 
            // chkNetBudget
            // 
            this.chkNetBudget.Location = new System.Drawing.Point(20, 90);
            this.chkNetBudget.Name = "chkNetBudget";
            this.chkNetBudget.Size = new System.Drawing.Size(194, 27);
            this.chkNetBudget.TabIndex = 38;
            this.chkNetBudget.Text = "Net Beginning Balance";
            this.ToolTip1.SetToolTip(this.chkNetBudget, null);
            this.chkNetBudget.CheckedChanged += new System.EventHandler(this.chkNetBudget_CheckedChanged);
            // 
            // chkBalance
            // 
            this.chkBalance.Location = new System.Drawing.Point(20, 120);
            this.chkBalance.Name = "chkBalance";
            this.chkBalance.Size = new System.Drawing.Size(116, 27);
            this.chkBalance.TabIndex = 30;
            this.chkBalance.Text = "Net Balance";
            this.ToolTip1.SetToolTip(this.chkBalance, null);
            this.chkBalance.CheckedChanged += new System.EventHandler(this.chkBalance_CheckedChanged);
            // 
            // fraSelectedMonths
            // 
            this.fraSelectedMonths.Controls.Add(this.chkSelectedNet);
            this.fraSelectedMonths.Controls.Add(this.chkSelectedDebitsCredits);
            this.fraSelectedMonths.FormatCaption = false;
            this.fraSelectedMonths.Location = new System.Drawing.Point(394, 30);
            this.fraSelectedMonths.Name = "fraSelectedMonths";
            this.fraSelectedMonths.Size = new System.Drawing.Size(343, 99);
            this.fraSelectedMonths.TabIndex = 22;
            this.fraSelectedMonths.Text = "Selected Month(s) Account Information";
            this.ToolTip1.SetToolTip(this.fraSelectedMonths, null);
            // 
            // chkSelectedNet
            // 
            this.chkSelectedNet.Location = new System.Drawing.Point(20, 60);
            this.chkSelectedNet.Name = "chkSelectedNet";
            this.chkSelectedNet.Size = new System.Drawing.Size(94, 27);
            this.chkSelectedNet.TabIndex = 11;
            this.chkSelectedNet.Text = "Net Total";
            this.ToolTip1.SetToolTip(this.chkSelectedNet, null);
            this.chkSelectedNet.CheckedChanged += new System.EventHandler(this.chkSelectedNet_CheckedChanged);
            // 
            // chkSelectedDebitsCredits
            // 
            this.chkSelectedDebitsCredits.Location = new System.Drawing.Point(20, 30);
            this.chkSelectedDebitsCredits.Name = "chkSelectedDebitsCredits";
            this.chkSelectedDebitsCredits.Size = new System.Drawing.Size(139, 27);
            this.chkSelectedDebitsCredits.TabIndex = 10;
            this.chkSelectedDebitsCredits.Text = "Debits / Credits";
            this.ToolTip1.SetToolTip(this.chkSelectedDebitsCredits, "You need to have 2 items available to show this item");
            this.chkSelectedDebitsCredits.CheckedChanged += new System.EventHandler(this.chkSelectedDebitsCredits_CheckedChanged);
            // 
            // fraYTD
            // 
            this.fraYTD.Controls.Add(this.chkYTDNet);
            this.fraYTD.Controls.Add(this.chkYTDDebitsCredits);
            this.fraYTD.FormatCaption = false;
            this.fraYTD.Location = new System.Drawing.Point(144, 30);
            this.fraYTD.Name = "fraYTD";
            this.fraYTD.Size = new System.Drawing.Size(230, 99);
            this.fraYTD.TabIndex = 21;
            this.fraYTD.Text = "YTD Account Information";
            this.ToolTip1.SetToolTip(this.fraYTD, null);
            // 
            // chkYTDNet
            // 
            this.chkYTDNet.Location = new System.Drawing.Point(20, 60);
            this.chkYTDNet.Name = "chkYTDNet";
            this.chkYTDNet.Size = new System.Drawing.Size(94, 27);
            this.chkYTDNet.TabIndex = 9;
            this.chkYTDNet.Text = "Net Total";
            this.ToolTip1.SetToolTip(this.chkYTDNet, null);
            this.chkYTDNet.CheckedChanged += new System.EventHandler(this.chkYTDNet_CheckedChanged);
            // 
            // chkYTDDebitsCredits
            // 
            this.chkYTDDebitsCredits.Location = new System.Drawing.Point(20, 30);
            this.chkYTDDebitsCredits.Name = "chkYTDDebitsCredits";
            this.chkYTDDebitsCredits.Size = new System.Drawing.Size(139, 27);
            this.chkYTDDebitsCredits.TabIndex = 8;
            this.chkYTDDebitsCredits.Text = "Debits / Credits";
            this.ToolTip1.SetToolTip(this.chkYTDDebitsCredits, "You need to have 2 items available to show this item");
            this.chkYTDDebitsCredits.CheckedChanged += new System.EventHandler(this.chkYTDDebitsCredits_CheckedChanged);
            // 
            // lblNumber
            // 
            this.lblNumber.BorderStyle = 1;
            this.lblNumber.Location = new System.Drawing.Point(20, 89);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(46, 22);
            this.lblNumber.TabIndex = 27;
            this.lblNumber.Text = "6";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblNumber, null);
            // 
            // lblCount
            // 
            this.lblCount.Location = new System.Drawing.Point(20, 30);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(116, 52);
            this.lblCount.TabIndex = 26;
            this.lblCount.Text = "NUMBER OF ITEMS YOU MAY ADD TO THIS REPORT";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblCount, null);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(227, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(201, 40);
            this.txtDescription.TabIndex = 24;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(100, 48);
            this.cmdProcessSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(21, 35);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(169, 18);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "FORMAT DESCRIPTION";
            // 
            // frmCustomizeLedgerSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomizeLedgerSummary";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Ledger Summary Report Format";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomizeLedgerSummary_Load);
            this.Activated += new System.EventHandler(this.frmCustomizeLedgerSummary_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeLedgerSummary_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeLedgerSummary_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).EndInit();
            this.fraPreferences.ResumeLayout(false);
            this.fraPreferences.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
            this.fraInformation.ResumeLayout(false);
            this.fraInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBudget)).EndInit();
            this.fraBudget.ResumeLayout(false);
            this.fraBudget.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDCBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBudget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNetBudget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedMonths)).EndInit();
            this.fraSelectedMonths.ResumeLayout(false);
            this.fraSelectedMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedDebitsCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTD)).EndInit();
            this.fraYTD.ResumeLayout(false);
            this.fraYTD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebitsCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
        public FCLabel lblDescription;
    }
}
