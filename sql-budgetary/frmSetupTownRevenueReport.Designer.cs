﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupTownRevenueReport.
	/// </summary>
	partial class frmSetupTownRevenueReport : BaseForm
	{
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCComboBox cboStartMonth;
		public fecherFoundation.FCComboBox cboEndMonth;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCButton cmdFilePrint;
		public fecherFoundation.FCButton cmdFilePreview;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupTownRevenueReport));
			this.fraRange = new fecherFoundation.FCFrame();
			this.cboStartMonth = new fecherFoundation.FCComboBox();
			this.cboEndMonth = new fecherFoundation.FCComboBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 371);
			this.BottomPanel.Size = new System.Drawing.Size(574, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Size = new System.Drawing.Size(574, 311);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(574, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(258, 30);
			this.HeaderText.Text = "Town Revenue Report";
			// 
			// fraRange
			// 
			this.fraRange.Controls.Add(this.cboStartMonth);
			this.fraRange.Controls.Add(this.cboEndMonth);
			this.fraRange.Controls.Add(this.lblTo);
			this.fraRange.Location = new System.Drawing.Point(30, 30);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(417, 90);
			this.fraRange.TabIndex = 1;
			this.fraRange.Text = "Date Range";
			// 
			// cboStartMonth
			// 
			this.cboStartMonth.AutoSize = false;
			this.cboStartMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStartMonth.FormattingEnabled = true;
			this.cboStartMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboStartMonth.Location = new System.Drawing.Point(20, 30);
			this.cboStartMonth.Name = "cboStartMonth";
			this.cboStartMonth.Size = new System.Drawing.Size(160, 40);
			this.cboStartMonth.TabIndex = 0;
			// 
			// cboEndMonth
			// 
			this.cboEndMonth.AutoSize = false;
			this.cboEndMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndMonth.FormattingEnabled = true;
			this.cboEndMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboEndMonth.Location = new System.Drawing.Point(237, 30);
			this.cboEndMonth.Name = "cboEndMonth";
			this.cboEndMonth.Size = new System.Drawing.Size(160, 40);
			this.cboEndMonth.TabIndex = 2;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(200, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(19, 16);
			this.lblTo.TabIndex = 3;
			this.lblTo.Text = "TO";
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(498, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(45, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(199, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(129, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmSetupTownRevenueReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(574, 479);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupTownRevenueReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Town Revenue Report";
			this.Load += new System.EventHandler(this.frmSetupTownRevenueReport_Load);
			this.Activated += new System.EventHandler(this.frmSetupTownRevenueReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupTownRevenueReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
