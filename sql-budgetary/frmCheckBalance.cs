﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCheckBalance.
	/// </summary>
	public partial class frmCheckBalance : BaseForm
	{
		public frmCheckBalance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            txtNewBalance.AllowOnlyNumericInput(true);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCheckBalance InstancePtr
		{
			get
			{
				return (frmCheckBalance)Sys.GetInstance(typeof(frmCheckBalance));
			}
		}

		protected frmCheckBalance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/30/02
		// This form will be used by customers to view all the info
		// currently in the check rec file so they can compare it to
		// the bank statements they receive and make sure it balances
		// ********************************************************
		int TitleCol;
		int AmountCol;
		int CountCol;
		public bool blnChangeBegBalance;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdBalance_Click(object sender, System.EventArgs e)
		{
			fraNewBalance.Visible = true;
			vs1.Enabled = false;
			//cmdCancel.Enabled = false;
			cmdBalance.Enabled = false;
			txtNewBalance.Focus();
		}

		public void cmdBalance_Click()
		{
			cmdBalance_Click(cmdBalance, new System.EventArgs());
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdCancel2_Click(object sender, System.EventArgs e)
		{
			fraNewBalance.Visible = false;
			vs1.Enabled = true;
			//cmdCancel.Enabled = true;
			cmdBalance.Enabled = true;
		}

		public void cmdCancel2_Click()
		{
			cmdCancel2_Click(cmdCancel2, new System.EventArgs());
		}

		private void cmdChange_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int x;
			if (!Information.IsNumeric(txtNewBalance.Text))
			{
				txtNewBalance.Text = "0.00";
			}
			ans = MessageBox.Show("Are you sure you want to change the balance?", "Change Balance?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				rsInfo.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank));
				rsInfo.Edit();
				rsInfo.Set_Fields("Balance", FCConvert.ToDecimal(txtNewBalance.Text));
				rsInfo.Update(true);
				vs1.TextMatrix(0, AmountCol, txtNewBalance.Text);
				Recalculate();
				cmdCancel2_Click();
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Check Rec Balance Screen", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()), "", "");
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
			}
		}

		private void frmCheckBalance_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			string temp;
			Decimal curTotal;
			clsDRWrapper rsSummaryInfo = new clsDRWrapper();
			int counter;
			string strCombinedSQL;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			TitleCol = 0;
			AmountCol = 1;
			CountCol = 2;
			vs1.ColWidth(TitleCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4525745));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2710027));
			vs1.ColFormat(AmountCol, "#,##0.00");
			temp = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("StatementDate"));
			if (temp != "")
			{
				lblStatementDate.Text = Strings.Format(temp, "MM/dd/yyyy");
			}
			else
			{
				lblStatementDate.Text = "00/00/0000";
			}
			if (blnChangeBegBalance)
			{
				cmdBalance.Visible = true;
				//cmdFileChange.Visible = true;
			}
			else
			{
				cmdBalance.Visible = false;
				//cmdFileChange.Visible = false;
				//FC:FINAL:ASZ: button position to Left
				//cmdCancel.Left = cmdBalance.Left;
			}
			rsInfo.OpenRecordset("SELECT * FROM Banks WHERE CurrentBank = 1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				HeaderText.Text = "Bank:  " + rsInfo.Get_Fields_Int32("ID") + " - " + rsInfo.Get_Fields_String("Name");
			}
			vs1.TextMatrix(0, TitleCol, "BEGINNING BALANCE......");
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			vs1.TextMatrix(0, AmountCol, Strings.Format(rsInfo.Get_Fields("Balance"), "#,##0.00"));
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			curTotal = FCConvert.ToDecimal(Conversion.Val(rsInfo.Get_Fields("Balance")));
			vs1.TextMatrix(0, CountCol, "");
			vs1.TextMatrix(1, TitleCol, " + DEPOSITS ON STMT....");
			vs1.TextMatrix(2, TitleCol, " - RETURNED CHECKS.....");
			vs1.TextMatrix(3, TitleCol, " + INTEREST............");
			vs1.TextMatrix(4, TitleCol, " + OTHER CREDITS.......");
			vs1.TextMatrix(5, TitleCol, " - CASHED CHECKS.......");
			vs1.TextMatrix(6, TitleCol, " - OTHER DEBITS........");
			vs1.Select(6, 0, 6, 2);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			vs1.TextMatrix(7, TitleCol, "STATEMENT BALANCE......");
			vs1.TextMatrix(8, TitleCol, "");
			vs1.TextMatrix(9, TitleCol, " + OUTSTANDING DEPOSITS");
			vs1.TextMatrix(10, TitleCol, " - OUTSTANDING CHECKS..");
			vs1.TextMatrix(11, TitleCol, " + OUTSTANDING OTHER..");
			vs1.Select(11, 0, 11, 2);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			vs1.TextMatrix(12, TitleCol, "CHECKBOOK AT STMT DATE.");
			vs1.TextMatrix(13, TitleCol, "");
			vs1.TextMatrix(14, TitleCol, " + OTHER DEPOSITS......");
			vs1.TextMatrix(15, TitleCol, " - ISSUED CHECKS.......");
			vs1.TextMatrix(16, TitleCol, " + ISSUED OTHER........");
			vs1.Select(16, 0, 16, 2);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			vs1.TextMatrix(17, TitleCol, "CURRENT CHECKBOOK......");
			strCombinedSQL = "SELECT Status, Type, SUM(Amount) as TotalAmount, COUNT(Amount) as TotalCount FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " GROUP BY Status, Type";
			// Cashed Deposits
			rsInfo.OpenRecordset(strCombinedSQL + " ORDER BY Status, Type");
			if (rsInfo.FindFirstRecord2("Status, Type", "3,3", ","))
			{
				vs1.TextMatrix(1, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(1, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(1, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(1, CountCol, FCConvert.ToString(0));
			}
			// Returned Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "3,4", ","))
			{
				vs1.TextMatrix(2, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(2, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(2, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(2, CountCol, FCConvert.ToString(0));
			}
			// Interest
			if (rsInfo.FindFirstRecord2("Status, Type", "3,5", ","))
			{
				vs1.TextMatrix(3, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(3, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(3, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(3, CountCol, FCConvert.ToString(0));
			}
			// Other Credits
			if (rsInfo.FindFirstRecord2("Status, Type", "3,6", ","))
			{
				vs1.TextMatrix(4, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(4, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(4, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(4, CountCol, FCConvert.ToString(0));
			}
			// Cashed Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "3,1", ","))
			{
				vs1.TextMatrix(5, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(5, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(5, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(5, CountCol, FCConvert.ToString(0));
			}
			if (rsInfo.FindFirstRecord2("Status, Type", "3,2", ","))
			{
				vs1.TextMatrix(5, AmountCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(5, AmountCol)) + rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(5, CountCol, FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(5, CountCol))) + FCConvert.ToInt32(rsInfo.Get_Fields("TotalCount"))));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				if (!Information.IsNumeric(vs1.TextMatrix(5, AmountCol)))
				{
					vs1.TextMatrix(5, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(5, CountCol, FCConvert.ToString(0));
				}
			}
			// Other Debits
			if (rsInfo.FindFirstRecord2("Status, Type", "3,7", ","))
			{
				vs1.TextMatrix(6, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(6, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(6, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(6, CountCol, FCConvert.ToString(0));
			}
			// Statement Total
			vs1.TextMatrix(7, AmountCol, FCConvert.ToString(curTotal));
			// Outstanding Deposits
			if (rsInfo.FindFirstRecord2("Status, Type", "2,3", ","))
			{
				vs1.TextMatrix(9, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(9, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(9, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(9, CountCol, FCConvert.ToString(0));
			}
			// Outstanding Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "2,1", ","))
			{
				vs1.TextMatrix(10, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(10, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(10, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(10, CountCol, FCConvert.ToString(0));
			}
			if (rsInfo.FindFirstRecord2("Status, Type", "2,2", ","))
			{
				vs1.TextMatrix(10, AmountCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(10, AmountCol)) + rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(10, CountCol, FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(10, CountCol))) + FCConvert.ToInt32(rsInfo.Get_Fields("TotalCount"))));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				if (!Information.IsNumeric(vs1.TextMatrix(10, AmountCol)))
				{
					vs1.TextMatrix(10, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(10, CountCol, FCConvert.ToString(0));
				}
			}
			// Outstanding Other
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '2' AND (Type = '6' OR Type = '5')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				vs1.TextMatrix(11, AmountCol, Strings.Format(rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00"));
				// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(11, CountCol, FCConvert.ToString(rsSummaryInfo.Get_Fields("OtherCount")));
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal += rsSummaryInfo.Get_Fields("OtherTotal");
			}
			else
			{
				vs1.TextMatrix(11, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(11, CountCol, FCConvert.ToString(0));
			}
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '2' AND (Type = '4' OR Type = '7')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				vs1.TextMatrix(11, AmountCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(11, AmountCol)) - rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00"));
				// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(11, CountCol, FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(11, CountCol))) + FCConvert.ToInt32(rsSummaryInfo.Get_Fields("OtherCount"))));
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal -= rsSummaryInfo.Get_Fields("OtherTotal");
			}
			else
			{
				if (!Information.IsNumeric(vs1.TextMatrix(11, AmountCol)))
				{
					vs1.TextMatrix(11, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(11, CountCol, FCConvert.ToString(0));
				}
			}
			// Checkbook at Statement Time
			vs1.TextMatrix(12, AmountCol, FCConvert.ToString(curTotal));
			// Other Deposits
			if (rsInfo.FindFirstRecord2("Status, Type", "1,3", ","))
			{
				vs1.TextMatrix(14, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(14, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(14, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(14, CountCol, FCConvert.ToString(0));
			}
			// Issued Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "1,1", ","))
			{
				vs1.TextMatrix(15, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(15, CountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalCount")));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				vs1.TextMatrix(15, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(15, CountCol, FCConvert.ToString(0));
			}
			if (rsInfo.FindFirstRecord2("Status, Type", "1,2", ","))
			{
				vs1.TextMatrix(15, AmountCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(15, AmountCol)) + rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(15, CountCol, FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(15, CountCol))) + FCConvert.ToInt32(rsInfo.Get_Fields("TotalCount"))));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				if (!Information.IsNumeric(vs1.TextMatrix(15, AmountCol)))
				{
					vs1.TextMatrix(15, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(15, CountCol, FCConvert.ToString(0));
				}
			}
			// Issued Other
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '1' AND (Type = '6' OR Type = '5')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				vs1.TextMatrix(16, AmountCol, Strings.Format(rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00"));
				// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(16, CountCol, FCConvert.ToString(rsSummaryInfo.Get_Fields("OtherCount")));
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal += rsSummaryInfo.Get_Fields("OtherTotal");
			}
			else
			{
				vs1.TextMatrix(16, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(16, CountCol, FCConvert.ToString(0));
			}
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '1' AND (Type = '4' OR Type = '7')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				vs1.TextMatrix(16, AmountCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(16, AmountCol)) - rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00"));
				// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
				vs1.TextMatrix(16, CountCol, FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(16, CountCol))) + FCConvert.ToInt32(rsSummaryInfo.Get_Fields("OtherCount"))));
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal -= rsSummaryInfo.Get_Fields("OtherTotal");
			}
			else
			{
				if (!Information.IsNumeric(vs1.TextMatrix(16, AmountCol)))
				{
					vs1.TextMatrix(16, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(16, CountCol, FCConvert.ToString(0));
				}
			}
			// Current Checkbook
			vs1.TextMatrix(17, AmountCol, FCConvert.ToString(curTotal));
			this.Refresh();
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
		}

		private void frmCheckBalance_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCheckBalance.FillStyle	= 0;
			//frmCheckBalance.ScaleWidth	= 9045;
			//frmCheckBalance.ScaleHeight	= 7320;
			//frmCheckBalance.LinkTopic	= "Form2";
			//frmCheckBalance.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:AM: decrease the row height
			vs1.RowHeight(-1, 400);
		}

		private void frmCheckBalance_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(TitleCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4525745));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2710027));
			//vs1.HeightOriginal = vs1.RowHeight(1) * 17 + vs1.RowHeight(0) + 75;
		}

		private void frmCheckBalance_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileChange_Click(object sender, System.EventArgs e)
		{
			cmdBalance_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void txtNewBalance_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Recalculate()
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(string, Decimal)
			Decimal curTotal = 0;
			curTotal = FCConvert.ToDecimal(vs1.TextMatrix(0, AmountCol));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(1, AmountCol));
			curTotal -= FCConvert.ToDecimal(vs1.TextMatrix(2, AmountCol));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(3, AmountCol));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(4, AmountCol));
			curTotal -= FCConvert.ToDecimal(vs1.TextMatrix(5, AmountCol));
			curTotal -= FCConvert.ToDecimal(vs1.TextMatrix(6, AmountCol));
			vs1.TextMatrix(7, AmountCol, FCConvert.ToString(curTotal));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(9, AmountCol));
			curTotal -= FCConvert.ToDecimal(vs1.TextMatrix(10, AmountCol));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(11, AmountCol));
			vs1.TextMatrix(12, AmountCol, FCConvert.ToString(curTotal));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(14, AmountCol));
			curTotal -= FCConvert.ToDecimal(vs1.TextMatrix(15, AmountCol));
			curTotal += FCConvert.ToDecimal(vs1.TextMatrix(16, AmountCol));
			vs1.TextMatrix(17, AmountCol, FCConvert.ToString(curTotal));
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			int intRows;
			int intCols;
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "vs1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = AmountCol;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Beginning Balance";
			intTotalNumberOfControls += 1;
		}
	}
}
