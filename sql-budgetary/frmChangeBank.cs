﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeBank.
	/// </summary>
	public partial class frmChangeBank : BaseForm
	{
		public frmChangeBank()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChangeBank InstancePtr
		{
			get
			{
				return (frmChangeBank)Sys.GetInstance(typeof(frmChangeBank));
			}
		}

		protected frmChangeBank _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/30/02
		// This form will be used by towns to change the bank they
		// are working with on the check reconcilliation process
		// ********************************************************
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSaveInfo = new clsDRWrapper();
			if (cboBanks.SelectedIndex != -1)
			{
				rsSaveInfo.OpenRecordset("SELECT * FROM Banks WHERE CurrentBank = 1");
				if (rsSaveInfo.EndOfFile() != true && rsSaveInfo.BeginningOfFile() != true)
				{
					rsSaveInfo.Edit();
					rsSaveInfo.Set_Fields("CurrentBank", false);
					rsSaveInfo.Update(true);
				}
				rsSaveInfo.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(Conversion.Val(Strings.Trim(Strings.Left(cboBanks.Text, 2)))));
				if (rsSaveInfo.EndOfFile() != true && rsSaveInfo.BeginningOfFile() != true)
				{
					rsSaveInfo.Edit();
					rsSaveInfo.Set_Fields("CurrentBank", true);
					if (!rsSaveInfo.IsFieldNull("StatementDate"))
					{
						modBudgetaryAccounting.UpdateBDVariable("StatementDate", rsSaveInfo.Get_Fields_DateTime("StatementDate"));
					}
					else
					{
						modBudgetaryAccounting.UpdateBDVariable("StatementDate", null);
					}
					rsSaveInfo.Update(true);
					modBudgetaryMaster.Statics.intCurrentBank = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Left(cboBanks.Text, 2)))));
				}
				App.MainForm.StatusBarText3 = "Bank:  " + rsSaveInfo.Get_Fields_Int32("ID") + " " + Strings.Trim(FCConvert.ToString(rsSaveInfo.Get_Fields_String("Name")));
				Close();
			}
			else
			{
				MessageBox.Show("You must make a selection before you may proceed", "No Bank Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void frmChangeBank_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			int intTemp;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rsBankInfo.OpenRecordset("SELECT * FROM Banks WHERE rTrim(Name) <> ''");
			cboBanks.Clear();
			intTemp = -1;
			do
			{
				cboBanks.AddItem(rsBankInfo.Get_Fields_Int32("ID") + " - " + Strings.Trim(FCConvert.ToString(rsBankInfo.Get_Fields_String("Name"))));
				if (FCConvert.ToBoolean(rsBankInfo.Get_Fields_Boolean("CurrentBank")))
				{
					intTemp = cboBanks.Items.Count - 1;
				}
				rsBankInfo.MoveNext();
			}
			while (rsBankInfo.EndOfFile() != true);
			cboBanks.SelectedIndex = intTemp;
			this.Refresh();
		}

		private void frmChangeBank_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmChangeBank_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChangeBank.FillStyle	= 0;
			//frmChangeBank.ScaleWidth	= 3885;
			//frmChangeBank.ScaleHeight	= 2280;
			//frmChangeBank.LinkTopic	= "Form2";
			//frmChangeBank.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void SetCustomFormColors()
		{
			Label1.ForeColor = Color.Blue;
		}
	}
}
