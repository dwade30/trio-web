﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptArticleEditReport.
	/// </summary>
	public partial class rptArticleEditReport : BaseSectionReport
	{
		public static rptArticleEditReport InstancePtr
		{
			get
			{
				return (rptArticleEditReport)Sys.GetInstance(typeof(rptArticleEditReport));
			}
		}

		protected rptArticleEditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsDetailInfo.Dispose();
				rsInfo.Dispose();
				rsExpAccountBudgetInfo.Dispose();
				rsRevAccountBudgetInfo.Dispose();
				rsLedgerAccountBudgetInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptArticleEditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		bool blnCheckedArticles;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curAccountBudget As Decimal	OnWrite(double, short)
		Decimal curAccountBudget;
		int LowDate;
		int HighDate;
		string strPeriodCheck = "";
		clsDRWrapper rsExpAccountBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsRevAccountBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsLedgerAccountBudgetInfo = new clsDRWrapper();

		public rptArticleEditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Article Edit Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckNext:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (!blnCheckedArticles)
				{
					rsDetailInfo.OpenRecordset("SELECT SUM(ArticleBudget) as TotalBudget FROM ArticleAccounts WHERE ArticleNumber = " + rsInfo.Get_Fields_Int32("ArticleNumber"));
					eArgs.EOF = false;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (rsExpAccountBudgetInfo.FindFirstRecord("Account", rsInfo.Get_Fields("Account")))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							curAccountBudget = FCConvert.ToDecimal(Conversion.Val(rsExpAccountBudgetInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsExpAccountBudgetInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							curAccountBudget = 0;
						}
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					else if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "R")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (rsRevAccountBudgetInfo.FindFirstRecord("Account", rsInfo.Get_Fields("Account")))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							curAccountBudget = FCConvert.ToDecimal(Conversion.Val(rsRevAccountBudgetInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsRevAccountBudgetInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							curAccountBudget = 0;
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (rsLedgerAccountBudgetInfo.FindFirstRecord("Account", rsInfo.Get_Fields("Account")))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							curAccountBudget = FCConvert.ToDecimal(Conversion.Val(rsLedgerAccountBudgetInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsLedgerAccountBudgetInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							curAccountBudget = 0;
						}
					}
					eArgs.EOF = false;
				}
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					if (!blnCheckedArticles)
					{
						rsDetailInfo.OpenRecordset("SELECT SUM(ArticleBudget) as TotalBudget FROM ArticleAccounts WHERE ArticleNumber = " + rsInfo.Get_Fields_Int32("ArticleNumber"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "E")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (rsExpAccountBudgetInfo.FindFirstRecord("Account", rsInfo.Get_Fields("Account")))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								curAccountBudget = FCConvert.ToDecimal(Conversion.Val(rsExpAccountBudgetInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsExpAccountBudgetInfo.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								curAccountBudget = 0;
							}
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (rsRevAccountBudgetInfo.FindFirstRecord("Account", rsInfo.Get_Fields("Account")))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								curAccountBudget = FCConvert.ToDecimal(Conversion.Val(rsRevAccountBudgetInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsRevAccountBudgetInfo.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								curAccountBudget = 0;
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (rsLedgerAccountBudgetInfo.FindFirstRecord("Account", rsInfo.Get_Fields("Account")))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								curAccountBudget = FCConvert.ToDecimal(Conversion.Val(rsLedgerAccountBudgetInfo.Get_Fields("OriginalBudgetTotal"))) + Conversion.Val(rsLedgerAccountBudgetInfo.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								curAccountBudget = 0;
							}
						}
					}
					eArgs.EOF = false;
				}
				else
				{
					if (!blnCheckedArticles)
					{
						blnCheckedArticles = true;
						blnFirstRecord = true;
						rsInfo.OpenRecordset("SELECT Account, SUM(ArticleBudget) as TotalBudget FROM ArticleAccounts GROUP BY Account ORDER BY Account");
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							// troges126
							modBudgetaryAccounting.CalculateAccountInfo();
							// CalculateAccountInfo True, True, False, "E"
							// CalculateAccountInfo True, True, False, "R"
							// CalculateAccountInfo True, True, False, "G"
							rsExpAccountBudgetInfo.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
							rsRevAccountBudgetInfo.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
							rsLedgerAccountBudgetInfo.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
							goto CheckNext;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			blnCheckedArticles = false;
			rsInfo.OpenRecordset("SELECT * FROM ArticleMaster WHERE ArticleBudget <> 0 ORDER BY ArticleNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				LowDate = modBudgetaryMaster.Statics.FirstMonth;
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					HighDate = 12;
				}
				else
				{
					HighDate = LowDate - 1;
				}
				if (LowDate > HighDate)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
			}
			else
			{
				MessageBox.Show("There are no Articles currently set up", "No Articles", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!blnCheckedArticles)
			{
				// TODO Get_Fields: Field [TotalBudget] not found!! (maybe it is an alias?)
				if (rsInfo.Get_Fields_Decimal("ArticleBudget") != rsDetailInfo.Get_Fields("TotalBudget"))
				{
					fldProblem.Visible = true;
					// TODO Get_Fields: Field [TotalBudget] not found!! (maybe it is an alias?)
					fldProblem.Text = "Article " + modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("ArticleNumber"), 3) + " Budget: " + Strings.Format(rsInfo.Get_Fields_Decimal("ArticleBudget"), "#,##0.00") + "               Total Budgeted for Article " + modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("ArticleNumber"), 3) + " Accounts: " + Strings.Format(rsDetailInfo.Get_Fields("TotalBudget"), "#,##0.00");
				}
				else
				{
					fldProblem.Visible = false;
					fldProblem.Text = "";
				}
			}
			else
			{
				// TODO Get_Fields: Field [TotalBudget] not found!! (maybe it is an alias?)
				if (rsInfo.Get_Fields("TotalBudget") != curAccountBudget)
				{
					fldProblem.Visible = true;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [TotalBudget] not found!! (maybe it is an alias?)
					fldProblem.Text = "Account " + rsInfo.Get_Fields("Account") + " Budget: " + Strings.Format(curAccountBudget, "#,##0.00") + "               Total Amount Budgeted to Article(s): " + Strings.Format(rsInfo.Get_Fields("TotalBudget"), "#,##0.00");
				}
				else
				{
					fldProblem.Visible = false;
					fldProblem.Text = "";
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptArticleEditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptArticleEditReport.Caption	= "Article Edit Report";
			//rptArticleEditReport.Icon	= "rptArticleEditReport.dsx":0000";
			//rptArticleEditReport.Left	= 0;
			//rptArticleEditReport.Top	= 0;
			//rptArticleEditReport.Width	= 11880;
			//rptArticleEditReport.Height	= 8595;
			//rptArticleEditReport.StartUpPosition	= 3;
			//rptArticleEditReport.SectionData	= "rptArticleEditReport.dsx":058A;
			//End Unmaped Properties
		}

		private void rptArticleEditReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
