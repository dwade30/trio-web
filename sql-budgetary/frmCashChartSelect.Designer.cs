﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCashChartSelect.
	/// </summary>
	partial class frmCashChartSelect : BaseForm
	{
		public fecherFoundation.FCFrame fraReportSelection;
		public fecherFoundation.FCGrid vsChartItems;
		public fecherFoundation.FCButton cmdEdit;
		public fecherFoundation.FCTextBox txtReportTitle;
		public fecherFoundation.FCButton cmdCancelSelection;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblReportTitle;
		public fecherFoundation.FCCheckBox chkDefault;
		public fecherFoundation.FCComboBox cboReports;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCButton cmdFileProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCashChartSelect));
			this.fraReportSelection = new fecherFoundation.FCFrame();
			this.vsChartItems = new fecherFoundation.FCGrid();
			this.cmdEdit = new fecherFoundation.FCButton();
			this.txtReportTitle = new fecherFoundation.FCTextBox();
			this.cmdCancelSelection = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblReportTitle = new fecherFoundation.FCLabel();
			this.chkDefault = new fecherFoundation.FCCheckBox();
			this.cboReports = new fecherFoundation.FCComboBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.cmdFileProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReportSelection)).BeginInit();
			this.fraReportSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsChartItems)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSelection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDefault)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraReportSelection);
			this.ClientArea.Controls.Add(this.chkDefault);
			this.ClientArea.Controls.Add(this.cboReports);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(169, 30);
			this.HeaderText.Text = "Cash Charting";
			// 
			// fraReportSelection
			// 
			this.fraReportSelection.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraReportSelection.Controls.Add(this.vsChartItems);
			this.fraReportSelection.Controls.Add(this.cmdEdit);
			this.fraReportSelection.Controls.Add(this.txtReportTitle);
			this.fraReportSelection.Controls.Add(this.cmdCancelSelection);
			this.fraReportSelection.Controls.Add(this.Label1);
			this.fraReportSelection.Controls.Add(this.lblReportTitle);
			this.fraReportSelection.Location = new System.Drawing.Point(30, 76);
			this.fraReportSelection.Name = "fraReportSelection";
			this.fraReportSelection.Size = new System.Drawing.Size(1018, 388);
			this.fraReportSelection.TabIndex = 5;
			this.fraReportSelection.Text = "Chart Selections";
			this.fraReportSelection.Visible = false;
			// 
			// vsChartItems
			// 
			this.vsChartItems.AllowSelection = false;
			this.vsChartItems.AllowUserToResizeColumns = false;
			this.vsChartItems.AllowUserToResizeRows = false;
			this.vsChartItems.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			//this.vsChartItems.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsChartItems.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsChartItems.BackColorBkg = System.Drawing.Color.Empty;
			this.vsChartItems.BackColorFixed = System.Drawing.Color.Empty;
			this.vsChartItems.BackColorSel = System.Drawing.Color.Empty;
			this.vsChartItems.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsChartItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsChartItems.ColumnHeadersHeight = 30;
			this.vsChartItems.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsChartItems.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsChartItems.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsChartItems.FixedCols = 0;
			this.vsChartItems.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsChartItems.FrozenCols = 0;
			this.vsChartItems.GridColor = System.Drawing.Color.Empty;
			this.vsChartItems.Location = new System.Drawing.Point(20, 123);
			this.vsChartItems.Name = "vsChartItems";
			this.vsChartItems.ReadOnly = true;
			this.vsChartItems.RowHeadersVisible = false;
			this.vsChartItems.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsChartItems.RowHeightMin = 0;
			this.vsChartItems.Rows = 1;
			this.vsChartItems.ShowColumnVisibilityMenu = false;
			this.vsChartItems.Size = new System.Drawing.Size(980, 182);
			this.vsChartItems.StandardTab = true;
			this.vsChartItems.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsChartItems.TabIndex = 12;
			// 
			// cmdEdit
			// 
			this.cmdEdit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdEdit.AppearanceKey = "actionButton";
			this.cmdEdit.Location = new System.Drawing.Point(20, 324);
			this.cmdEdit.Name = "cmdEdit";
			this.cmdEdit.Size = new System.Drawing.Size(92, 48);
			this.cmdEdit.TabIndex = 10;
			this.cmdEdit.Text = "Edit";
			this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
			// 
			// txtReportTitle
			// 
			this.txtReportTitle.AutoSize = false;
			this.txtReportTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtReportTitle.Enabled = false;
			this.txtReportTitle.Location = new System.Drawing.Point(155, 30);
			this.txtReportTitle.Name = "txtReportTitle";
			this.txtReportTitle.Size = new System.Drawing.Size(217, 40);
			this.txtReportTitle.TabIndex = 8;
			this.txtReportTitle.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			// 
			// cmdCancelSelection
			// 
			this.cmdCancelSelection.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdCancelSelection.AppearanceKey = "actionButton";
			this.cmdCancelSelection.Location = new System.Drawing.Point(130, 324);
			this.cmdCancelSelection.Name = "cmdCancelSelection";
			this.cmdCancelSelection.Size = new System.Drawing.Size(92, 48);
			this.cmdCancelSelection.TabIndex = 6;
			this.cmdCancelSelection.Text = "Cancel";
			this.cmdCancelSelection.Click += new System.EventHandler(this.cmdCancelSelection_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 90);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(92, 18);
			this.Label1.TabIndex = 11;
			this.Label1.Text = "CHART ITEMS";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblReportTitle
			// 
			this.lblReportTitle.Location = new System.Drawing.Point(20, 44);
			this.lblReportTitle.Name = "lblReportTitle";
			this.lblReportTitle.Size = new System.Drawing.Size(92, 18);
			this.lblReportTitle.TabIndex = 7;
			this.lblReportTitle.Text = "CHART TITLE";
			this.lblReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkDefault
			// 
			this.chkDefault.Location = new System.Drawing.Point(30, 70);
			this.chkDefault.Name = "chkDefault";
			this.chkDefault.Size = new System.Drawing.Size(226, 27);
			this.chkDefault.TabIndex = 4;
			this.chkDefault.Text = "Make this the default report";
			this.chkDefault.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
			// 
			// cboReports
			// 
			this.cboReports.AutoSize = false;
			this.cboReports.BackColor = System.Drawing.SystemColors.Window;
			this.cboReports.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReports.FormattingEnabled = true;
			this.cboReports.Location = new System.Drawing.Point(30, 120);
			this.cboReports.Name = "cboReports";
			this.cboReports.Size = new System.Drawing.Size(199, 40);
			this.cboReports.TabIndex = 2;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(385, 24);
			this.lblInstructions.TabIndex = 3;
			this.lblInstructions.Text = "PLEASE SELECT THE CHART YOU WISH TO PRINT AND CLICK OK";
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdFileProcess
			// 
			this.cmdFileProcess.AppearanceKey = "acceptButton";
			this.cmdFileProcess.Location = new System.Drawing.Point(274, 30);
			this.cmdFileProcess.Name = "cmdFileProcess";
			this.cmdFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileProcess.Size = new System.Drawing.Size(104, 48);
			this.cmdFileProcess.TabIndex = 0;
			this.cmdFileProcess.Text = "Process";
			this.cmdFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
			// 
			// frmCashChartSelect
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCashChartSelect";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Cash Charting";
			this.Load += new System.EventHandler(this.frmCashChartSelect_Load);
			this.Activated += new System.EventHandler(this.frmCashChartSelect_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCashChartSelect_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReportSelection)).EndInit();
			this.fraReportSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsChartItems)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSelection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDefault)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
