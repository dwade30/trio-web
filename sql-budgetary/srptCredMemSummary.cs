﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCredMemSummary.
	/// </summary>
	public partial class srptCredMemSummary : FCSectionReport
	{
		public static srptCredMemSummary InstancePtr
		{
			get
			{
				return (srptCredMemSummary)Sys.GetInstance(typeof(srptCredMemSummary));
			}
		}

		protected srptCredMemSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsJournalInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCredMemSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		bool blnFirstRecord;
        private List<int> selectedJournals;

		public srptCredMemSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public srptCredMemSummary(List<int> selectedJournals) : this()
        {
            this.selectedJournals = selectedJournals;
        }

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeNextRecord = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				
				rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + selectedJournals[intCounter]);
				if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					executeNextRecord = true;
				}
			}
			else
			{
				executeNextRecord = true;
			}

            NextRecord:

			if (executeNextRecord)
			{
				intCounter += 1;
				if (intCounter < selectedJournals.Count)
				{
                    rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + selectedJournals[intCounter]);
					if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						goto NextRecord;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			intCounter = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournalNumber.Text = Strings.Format(rsJournalInfo.Get_Fields("JournalNumber"), "0000");
			fldEnteredAmount.Text = Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
			fldCreditMemoAmount.Text = Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00");
		}
	}
}
