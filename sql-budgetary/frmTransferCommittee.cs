﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmTransferCommittee.
	/// </summary>
	public partial class frmTransferCommittee : BaseForm
	{
		public frmTransferCommittee()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbAll.SelectedIndex = 0;
			cmbAllCategories.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTransferCommittee InstancePtr
		{
			get
			{
				return (frmTransferCommittee)Sys.GetInstance(typeof(frmTransferCommittee));
			}
		}

		protected frmTransferCommittee _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/2/2002
		// This form will be used to transfer budget amounts from
		// Manager Request to Committee Request and Approved Amounts
		// ********************************************************
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			if (cmbAllCategories.SelectedIndex == 1)
			{
				if (chkElected.CheckState == CheckState.Unchecked && chkApproved.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select where the values will be transferred to before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbAll.SelectedIndex == 0)
			{
				rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E' OR Left(Account, 1) = 'R'");
			}
			else if (cmbAll.SelectedIndex == 1)
			{
				rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E'");
			}
			else
			{
				rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'R'");
			}
			if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
			{
				do
				{
					rsBudgetInfo.Edit();
					if (cmbAllCategories.SelectedIndex == 0)
					{
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						rsBudgetInfo.Set_Fields("ElectedRequest", rsBudgetInfo.Get_Fields("CommitteeRequest"));
						// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
						rsBudgetInfo.Set_Fields("ApprovedAmount", rsBudgetInfo.Get_Fields("CommitteeRequest"));
					}
					else
					{
						if (chkElected.CheckState == CheckState.Checked)
						{
							// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
							rsBudgetInfo.Set_Fields("ElectedRequest", rsBudgetInfo.Get_Fields("CommitteeRequest"));
						}
						if (chkApproved.CheckState == CheckState.Checked)
						{
							// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
							rsBudgetInfo.Set_Fields("ApprovedAmount", rsBudgetInfo.Get_Fields("CommitteeRequest"));
						}
					}
					rsBudgetInfo.Update(true);
					rsBudgetInfo.MoveNext();
				}
				while (rsBudgetInfo.EndOfFile() != true);
				modGlobalFunctions.AddCYAEntry_8("BD", "Transfer Committee Budget Request Done");
			}
			Close();
			MessageBox.Show("Committee request amounts transferred successfully!", "Successful Transfer", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void frmTransferCommittee_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmTransferCommittee_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTransferCommittee_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransferCommittee.FillStyle	= 0;
			//frmTransferCommittee.ScaleWidth	= 5880;
			//frmTransferCommittee.ScaleHeight	= 3810;
			//frmTransferCommittee.LinkTopic	= "Form2";
			//frmTransferCommittee.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAllCategories_CheckedChanged(object sender, System.EventArgs e)
		{
			chkElected.CheckState = CheckState.Unchecked;
			chkApproved.CheckState = CheckState.Unchecked;
			fraSelected.Enabled = false;
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelected.Enabled = true;
		}

		private void cmbAllCategories_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAllCategories.SelectedIndex == 0)
			{
				optAllCategories_CheckedChanged(sender, e);
			}
			else if (cmbAllCategories.SelectedIndex == 1)
			{
				optSelected_CheckedChanged(sender, e);
			}
		}
	}
}
