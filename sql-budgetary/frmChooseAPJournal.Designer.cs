﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmChooseAPJournal.
    /// </summary>
    partial class frmChooseAPJournal : BaseForm
	{
		public FCGrid gridJournals;
		public fecherFoundation.FCButton cmdOK;
		/// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseAPJournal));
			this.gridJournals = new fecherFoundation.FCGrid();
			this.cmdOK = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOK);
			this.BottomPanel.Location = new System.Drawing.Point(0, 352);
			this.BottomPanel.Size = new System.Drawing.Size(787, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.gridJournals);
			this.ClientArea.Size = new System.Drawing.Size(787, 292);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(787, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(188, 30);
			this.HeaderText.Text = "Select a Journal";
			// 
			// gridJournals
			// 
			this.gridJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.gridJournals.Cols = 6;
			this.gridJournals.FixedCols = 0;
			this.gridJournals.Location = new System.Drawing.Point(30, 30);
			this.gridJournals.Name = "gridJournals";
			this.gridJournals.RowHeadersVisible = false;
			this.gridJournals.Rows = 1;
			this.gridJournals.Size = new System.Drawing.Size(729, 233);
			this.gridJournals.Click += new System.EventHandler(this.gridJournals_ClickEvent);
			this.gridJournals.DoubleClick += new System.EventHandler(this.gridJournals_DoubleClick);
			this.gridJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.gridJournals_KeyDownEvent);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "acceptButton";
			this.cmdOK.Location = new System.Drawing.Point(333, 30);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdOK.Size = new System.Drawing.Size(120, 48);
			this.cmdOK.TabIndex = 3;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// frmChooseAPJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(787, 460);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmChooseAPJournal";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select a Journal";
			this.Load += new System.EventHandler(this.frmChooseAPJournal_Load);
			this.Activated += new System.EventHandler(this.frmChooseAPJournal_Activated);
			this.Resize += new System.EventHandler(this.frmChooseAPJournal_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChooseAPJournal_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion
    }
}
