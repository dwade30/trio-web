﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptLaserChecksTop.
	/// </summary>
	public partial class rptLaserChecksTop : BaseSectionReport
	{
		public static rptLaserChecksTop InstancePtr
		{
			get
			{
				return (rptLaserChecksTop)Sys.GetInstance(typeof(rptLaserChecksTop));
			}
		}

		protected rptLaserChecksTop _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLaserChecksTop	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		bool blnFirstRecord;
		string strSQL = "";
		// vbPorter upgrade warning: lngcheck As int	OnWrite(string, int)
		int lngcheck;
		int WarrantCol;
		int DescriptionCol;
		int ReferenceCol;
		int DiscountCol;
		int AmountCol;
		// vbPorter upgrade warning: lngCheckToReprint As int	OnWrite(string)
		int lngCheckToReprint;
		// vbPorter upgrade warning: lngLastCheckToReprint As int	OnWrite(string)
		int lngLastCheckToReprint;
		string strFooterVendorName = "";
		string strFooterAddress1 = "";
		string strFooterAddress2 = "";
		string strFooterAddress3 = "";
		string strFooterAddress4 = "";
		string strFooterCity = "";
		string strFooterState = "";
		string strFooterZip = "";
		string strFooterZip4 = "";
		string strFooterJournals = "";
		Decimal curFooterAmount;
		int lngFooterVendorNumber;
		clsDRWrapper rsPrePaidJournalInfo = new clsDRWrapper();
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		int lngLowCheck;
		int lngHighCheck;
		bool blnDonePreview;
		bool blnPrinting;

		public rptLaserChecksTop()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Checks";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			blnPrinting = true;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void ActiveReport_QueryClose(ref short Cancel, ref short CloseMode)
		{
			if (!blnDonePreview && blnPrinting)
			{
				MessageBox.Show("Checks have not finished processing.", "Checks Not Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel = FCConvert.ToInt16(true);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			clsDRWrapper rsAPJournals = new clsDRWrapper();
			if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(frmPrintChecks.InstancePtr.TempWarrant) + "')");
				if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
				{
					do
					{
						rsMasterJournal.Edit();
						rsMasterJournal.Set_Fields("Status", "C");
						rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
						rsMasterJournal.Set_Fields("CheckDate", frmPrintChecks.InstancePtr.txtPayDate.Text);
						rsMasterJournal.Update(true);
						rsMasterJournal.MoveNext();
					}
					while (rsMasterJournal.EndOfFile() != true);
					rsMasterJournal.MoveFirst();
					do
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						rsAPJournals.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + rsMasterJournal.Get_Fields("JournalNumber"));
						if (rsAPJournals.EndOfFile() != true && rsAPJournals.BeginningOfFile() != true)
						{
							rsAPJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE ID = 0");
							rsAPJournals.AddNew();
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							rsAPJournals.Set_Fields("JournalNumber", rsMasterJournal.Get_Fields("JournalNumber"));
							rsAPJournals.Set_Fields("Description", rsMasterJournal.Get_Fields_String("Description"));
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							rsAPJournals.Set_Fields("Type", rsMasterJournal.Get_Fields("Type"));
							rsAPJournals.Set_Fields("Status", "E");
							rsAPJournals.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
							rsAPJournals.Set_Fields("StatusChangeDate", DateTime.Today);
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsAPJournals.Set_Fields("Period", rsMasterJournal.Get_Fields("Period"));
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							rsAPJournals.Set_Fields("Type", rsMasterJournal.Get_Fields("Type"));
							rsAPJournals.Update(true);
						}
						rsMasterJournal.MoveNext();
					}
					while (rsMasterJournal.EndOfFile() != true);
				}
			}
			rsCheckInfo.OpenRecordset("SELECT * FROM LastChecksRun");
			if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
			{
				rsCheckInfo.Edit();
			}
			else
			{
				rsCheckInfo.OmitNullsOnInsert = true;
				rsCheckInfo.AddNew();
			}
			rsCheckInfo.Set_Fields("StartCheckNumber", lngLowCheck);
			rsCheckInfo.Set_Fields("EndCheckNumber", lngHighCheck);
			rsCheckInfo.Update(true);
			if (frmPrintChecks.InstancePtr.blnPrintingUnprintedChecks == false)
			{
				frmPrintChecks.InstancePtr.Unload();
			}
			blnDonePreview = true;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			int lngAdjust;
			clsDRWrapper rsIcon = new clsDRWrapper();
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			string strEnteredPassword = "";
			int lngWidth = 0;
			int lngHeight = 0;
			double dblRatio = 0;
			float lngSignatureBottom = 0;
			int cnt;
			int const_PrintToolID;
			const_PrintToolID = 9950;
			
			blnDonePreview = false;
			
			blnFirstRecord = true;
			WarrantCol = 0;
			DescriptionCol = 1;
			ReferenceCol = 2;
			DiscountCol = 3;
			AmountCol = 4;
			curTotal = 0;
			rsIcon.OpenRecordset("SELECT * FROM Budgetary");
			if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("ShowIcon")))
			{
				if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
				{
					if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon")))
					{
						imgIcon.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon"));
					}
				}
				else
				{
					if (File.Exists(FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon")))
					{
						imgIcon.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon"));
					}
				}
			}
			Image1.Visible = false;
			if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("PrintSignature")))
			{
				// must get password
				intResponse = MessageBox.Show("Do you want a signature to print on the checks?", "Print Signature?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.Yes)
				{
					modSignatureFile.SetupSigInformation();
					TryPassword:
					;
					// strEnteredPassword = UCase(frmSigPassword.Init)
					if (Strings.UCase(modSignatureFile.Statics.gstrBudgetarySigPassword) == strEnteredPassword)
					{
						if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\Sigfile\\TWBD0000.SIG"))
						{
							Image1.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "\\Sigfile\\TWBD0000.SIG");
							Image1.Visible = true;
							lngWidth = Image1.Image.Width;
							lngHeight = Image1.Image.Height;
							lngSignatureBottom = Image1.Top + Image1.Height;
							dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;
							if (dblRatio * Image1.Height > Image1.Width)
							{
								// keep width, change height
								Image1.Height = Image1.Width / FCConvert.ToSingle(dblRatio);
							}
							else
							{
								// keep height, change width
								Image1.Width = Image1.Height * FCConvert.ToSingle(dblRatio);
							}
							Image1.Top = lngSignatureBottom - Image1.Height;
							// keep bottom of signature on line. This is default laser check
						}
					}
					else
					{
						intResponse = MessageBox.Show("Incorrect Password. Re-enter password?", "Incorrect Password", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (intResponse == DialogResult.Yes)
							goto TryPassword;
					}
				}
			}
			if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				lngcheck = FCConvert.ToInt32(frmPrintChecks.InstancePtr.txtCheck.Text);
				lngLowCheck = lngcheck;
				strSQL = "APJournal.JournalNumber IN (";
				for (counter = 1; counter <= frmPrintChecks.InstancePtr.vsJournals.Rows - 1; counter++)
				{
					if (FCUtils.CBool(frmPrintChecks.InstancePtr.vsJournals.TextMatrix(counter, 0)) == true)
					{
						strSQL += FCConvert.ToString(Conversion.Val(frmPrintChecks.InstancePtr.vsJournals.TextMatrix(counter, 1))) + ", ";
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ") AND APJournal.Payable <= '" + frmPrintChecks.InstancePtr.txtPayDate.Text + "' ";
				FillTemp();
			}
			else
			{
				lngcheck = FCConvert.ToInt32(frmPrintChecks.InstancePtr.txtFirstCheck.Text);
				lngLowCheck = lngcheck;
				lngCheckToReprint = FCConvert.ToInt32(frmPrintChecks.InstancePtr.txtFirstReprintCheck.Text);
				lngLastCheckToReprint = FCConvert.ToInt32(frmPrintChecks.InstancePtr.txtLastReprintCheck.Text);
				rsVendorInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber >= " + FCConvert.ToString(lngCheckToReprint) + " ORDER BY CheckNumber");
			}
			lngAdjust = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CHECKLASERADJUSTMENT"))));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += (240 * lngAdjust) / 1440F;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.PageHeader.Controls)
			{
				ControlName.Top += (240 * lngAdjust) / 1440F;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.PageFooter.Controls)
			{
				ControlName.Top += (240 * lngAdjust) / 1440F;
			}
		}

		private void FillTemp()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4 FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
			// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
			strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4 FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 ORDER BY TempVendorName";
			// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
			strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4 FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4 FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 ORDER BY TempVendorName) as " + "TempVendorInfo";
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
				{
					strTemp += " ORDER BY VendorNumber";
				}
				else
				{
					strTemp += " ORDER BY VendorName";
				}
			}
			else
			{
				strTemp += " ORDER BY VendorNumber";
			}
			rsVendorInfo.OpenRecordset(strTemp);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, ID");
						rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
					}
					else
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, ID");
						rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
					}
					//goto CheckJournal;
				}
				//else
				{
					CheckJournal:
					;
					SavePrePaidJournalInfo();
					if (rsJournalInfo.EndOfFile() == true)
					{
						rsVendorInfo.MoveNext();
						CheckNextVendor:
						;
						if (rsVendorInfo.EndOfFile() == true)
						{
							eArgs.EOF = true;
							return;
						}
						else
						{
							if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, ID");
								rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
							}
							else
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, ID");
								rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
							}
							SavePrePaidJournalInfo();
							if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
							{
								eArgs.EOF = false;
								return;
							}
							else
							{
								rsVendorInfo.MoveNext();
								goto CheckNextVendor;
							}
						}
					}
					else
					{
						eArgs.EOF = false;
						return;
					}
				}
			}
			else
			{
				bool executeGetNextReprintVendor = false;
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) == "R")
					{
						rsJournalInfo.OpenRecordset(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
					}
					else
					{
						executeGetNextReprintVendor = true;
						goto GetNextReprintVendor;
					}
				}
				else
				{
					if (rsJournalInfo.EndOfFile() == true)
					{
						executeGetNextReprintVendor = true;
						goto GetNextReprintVendor;
					}
					else
					{
						eArgs.EOF = false;
						return;
					}
				}
				GetNextReprintVendor:
				;
				if (executeGetNextReprintVendor)
				{
					rsVendorInfo.MoveNext();
					if (rsVendorInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
						return;
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					else if (rsVendorInfo.Get_Fields("CheckNumber") > lngLastCheckToReprint)
					{
						eArgs.EOF = true;
						return;
					}
					else if (rsVendorInfo.Get_Fields_String("Status") == "P")
					{
						goto GetNextReprintVendor;
					}
					else
					{
						rsJournalInfo.OpenRecordset(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							eArgs.EOF = false;
							return;
						}
						else
						{
							goto GetNextReprintVendor;
						}
					}
				}
			}
		}

		private void SavePrePaidJournalInfo()
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			clsDRWrapper rsTotal = new clsDRWrapper();
			string strCheckSQL = "";
			clsDRWrapper rsStatusChange = new clsDRWrapper();
			rsCheckInfo.OmitNullsOnInsert = true;
			while (rsPrePaidJournalInfo.EndOfFile() != true)
			{
				if (FCConvert.ToString(rsPrePaidJournalInfo.Get_Fields_String("Status")) == "E")
				{
					bool executeAddNew = false;
					if (Information.IsDate(rsPrePaidJournalInfo.Get_Fields("CheckDate")))
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = " + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + " AND CheckDate = '" + rsPrePaidJournalInfo.Get_Fields_DateTime("CheckDate") + "'");
					}
					else
					{
						rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ID = 0");
						executeAddNew = true;
						goto AddNew;
					}
					if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
					{
						rsCheckInfo.Edit();
					}
					else
					{
						executeAddNew = true;
						goto AddNew;
					}
					AddNew:
					;
					if (executeAddNew)
					{
						rsCheckInfo.AddNew();
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsCheckInfo.Set_Fields("CheckNumber", rsPrePaidJournalInfo.Get_Fields("CheckNumber"));
					rsCheckInfo.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
					rsCheckInfo.Set_Fields("VendorName", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName"))));
					rsCheckInfo.Set_Fields("CheckAddress1", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))));
					rsCheckInfo.Set_Fields("CheckAddress2", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"))));
					rsCheckInfo.Set_Fields("CheckAddress3", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"))));
					rsCheckInfo.Set_Fields("CheckCity", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"))));
					rsCheckInfo.Set_Fields("CheckState", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))));
					rsCheckInfo.Set_Fields("CheckZip", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					rsCheckInfo.Set_Fields("CheckZip4", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					rsCheckInfo.Set_Fields("WarrantNumber", frmPrintChecks.InstancePtr.TempWarrant);
					rsTotal.OpenRecordset("SELECT SUM(Amount) as TotalAmt, SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsPrePaidJournalInfo.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [TotalAmt] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
					rsCheckInfo.Set_Fields("Amount", FCConvert.ToDecimal(rsCheckInfo.Get_Fields("Amount") + Conversion.Val(rsTotal.Get_Fields("TotalAmt")) - Conversion.Val(rsTotal.Get_Fields("TotalDiscount"))));
					rsCheckInfo.Set_Fields("CheckDate", frmPrintChecks.InstancePtr.txtPayDate.Text);
					strCheckSQL = rsPrePaidJournalInfo.Name();
					if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 1)
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "=", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + "' ORDER BY IsNull(Seperate, 0) DESC";
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "<", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + "' ORDER BY IsNull(Seperate, 0) DESC";
					}
					rsCheckInfo.Set_Fields("Journals", strCheckSQL);
					rsCheckInfo.Set_Fields("Status", "P");
					rsCheckInfo.Update(true);
					rsPrePaidJournalInfo.Edit();
					rsPrePaidJournalInfo.Set_Fields("Status", "C");
					rsPrePaidJournalInfo.Set_Fields("Warrant", frmPrintChecks.InstancePtr.TempWarrant);
					rsPrePaidJournalInfo.Set_Fields("CheckDate", frmPrintChecks.InstancePtr.txtPayDate.Text);
					rsPrePaidJournalInfo.Update(true);
				}
				rsPrePaidJournalInfo.MoveNext();
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		if (blnDonePreview)
		//		{
		//			this.Document.Print(false);
		//		}
		//		else
		//		{
		//			MessageBox.Show("You may not print checks until they have all finished processing.", "Checks Not Finished", MessageBoxButtons.OK, MessageBoxIcon.Information, modal:false);
		//		}
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			string strAmount = "";
			int intPlaceHolder = 0;
			int counter;
			int counter2;
			clsDRWrapper rsDiscount = new clsDRWrapper();
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			string strCheckSQL = "";
			clsDRWrapper rsDupChecks = new clsDRWrapper();
			clsDRWrapper rsCheckMessage = new clsDRWrapper();
			if (!rsVendorInfo.EndOfFile())
			{
				fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + "  " + rsVendorInfo.Get_Fields_String("VendorName");
				fldDate.Text = Strings.Format(frmPrintChecks.InstancePtr.txtPayDate.Text, "MM/dd/yy");
				fldCheck.Text = lngcheck.ToString();
				if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) != 0)
				{
					rsCheckMessage.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber"));
					if (rsCheckMessage.EndOfFile() != true && rsCheckMessage.BeginningOfFile() != true)
					{
						if (Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage"))) != "")
						{
							fldCheckMessage.Text = Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage")));
							fldBottomCheckMessage.Text = Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage")));
							if (FCConvert.ToBoolean(rsCheckMessage.Get_Fields_Boolean("OneTimeCheckMessage")))
							{
								rsCheckMessage.Edit();
								rsCheckMessage.Set_Fields("CheckMessage", "");
								rsCheckMessage.Set_Fields("OneTimeCheckMessage", false);
								rsCheckMessage.Update();
							}
						}
						else
						{
							fldCheckMessage.Text = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
							fldBottomCheckMessage.Text = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
						}
					}
					else
					{
						fldCheckMessage.Text = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
						fldBottomCheckMessage.Text = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
					}
				}
				else
				{
					fldCheckMessage.Text = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
					fldBottomCheckMessage.Text = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
				}
				lngHighCheck = lngcheck;
				fldBottomVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + "  " + rsVendorInfo.Get_Fields_String("VendorName");
				fldBottomDate.Text = frmPrintChecks.InstancePtr.txtPayDate.Text;
				fldBottomCheck.Text = lngcheck.ToString();
				lngcheck += 1;
				for (counter = 0; counter <= 9; counter++)
				{
					if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0)
					{
						if (rsJournalInfo.EndOfFile() != true)
						{
							rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							FillRows_2184(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(frmPrintChecks.InstancePtr.TempWarrant), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) * -1, "#,##0.00"), Strings.Format(Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")), "#,##0.00"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							curTotal += FCConvert.ToDecimal(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")));
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("CheckNumber", lngcheck - 1);
							rsJournalInfo.Set_Fields("Status", "C");
							rsJournalInfo.Set_Fields("Warrant", frmPrintChecks.InstancePtr.TempWarrant);
							rsJournalInfo.Set_Fields("CheckDate", frmPrintChecks.InstancePtr.txtPayDate.Text);
							rsJournalInfo.Update(true);
							if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("Seperate")))
							{
								for (counter2 = 1; counter2 <= 9; counter2++)
								{
									FillRows_2184(counter2, "", "", "", "", "", "");
								}
								rsJournalInfo.MoveNext();
								goto GetVendorInfo;
							}
						}
						else
						{
							FillRows_2184(counter, "", "", "", "", "", "");
						}
					}
					else
					{
						if (rsJournalInfo.EndOfFile() != true)
						{
							rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							FillRows_2184(counter, modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("Warrant"), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) * -1, "#,##0.00"), Strings.Format(Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")), "#,##0.00"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("CheckNumber", lngcheck - 1);
							rsJournalInfo.Set_Fields("CheckDate", frmPrintChecks.InstancePtr.txtPayDate.Text);
							rsJournalInfo.Update(true);
							if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("Seperate")))
							{
								for (counter2 = 1; counter2 <= 9; counter2++)
								{
									FillRows_2184(counter2, "", "", "", "", "", "");
								}
								rsJournalInfo.MoveNext();
								goto GetVendorInfo;
							}
						}
						else
						{
							FillRows_2184(counter, "", "", "", "", "", "");
						}
					}
					rsJournalInfo.MoveNext();
				}
				GetVendorInfo:
				;
				if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0)
				{
					curFooterAmount = curTotal;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curFooterAmount = FCConvert.ToDecimal(rsVendorInfo.Get_Fields("Amount"));
				}
				strFooterVendorName = FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName"));
				lngFooterVendorNumber = FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber"));
				strFooterAddress1 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
				strFooterAddress2 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
				strFooterAddress3 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
				strFooterCity = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
				strFooterState = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
				strFooterZip = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
				strFooterZip4 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
				if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 1)
				{
					strFooterJournals = rsJournalInfo.Name();
				}
				else
				{
					strFooterJournals = rsPrePaidJournalInfo.Name();
				}
				//Application.DoEvents();
				if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 1)
				{
					if (rsJournalInfo.EndOfFile() != true)
					{
						rsVendorInfo.MoveNext();
					}
				}
				if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 1)
				{
					curTotal = curFooterAmount;
				}
				fldAmount.Text = Strings.Format(curTotal, "#,##0.00");
				fldAmount.Text = Strings.StrDup(13 - fldAmount.Text.Length, "*") + fldAmount.Text;
				fldMuniName.Text = modGlobalConstants.Statics.MuniName;
				fldBottomMuniName.Text = fldMuniName.Text;
				fldBottomAmount.Text = fldAmount.Text;
				fldVendorName.Text = Strings.Trim(strFooterVendorName);
				fldAmount2.Text = "$" + fldAmount.Text;
				fldDate2.Text = Strings.Format(frmPrintChecks.InstancePtr.txtPayDate.Text, "MM/dd/yy");
				fldAddress1.Text = Strings.Trim(strFooterAddress1);
				fldAddress2.Text = Strings.Trim(strFooterAddress2);
				fldAddress3.Text = Strings.Trim(strFooterAddress3);
				fldAddress4.Text = Strings.Trim(strFooterAddress4);
				if (Strings.Trim(fldAddress3.Text) == "")
				{
					fldAddress3.Text = fldAddress4.Text;
					fldAddress4.Text = "";
				}
				if (Strings.Trim(fldAddress2.Text) == "")
				{
					fldAddress2.Text = fldAddress3.Text;
					fldAddress3.Text = "";
				}
				if (Strings.Trim(fldAddress1.Text) == "")
				{
					fldAddress1.Text = fldAddress2.Text;
					fldAddress2.Text = "";
				}
				if (curTotal != 0)
				{
					strAmount = modConvertAmountToString.ConvertAmountToString(curTotal);
					lblVoid.Visible = false;
				}
				else
				{
					strAmount = "ZERO 00/100";
					lblVoid.Visible = true;
				}
				if (strAmount.Length > 70)
				{
					intPlaceHolder = Strings.InStr(55, strAmount, " ", CompareConstants.vbBinaryCompare);
					fldTextAmount1.Text = Strings.Left(strAmount, intPlaceHolder) + Strings.StrDup(70 - Strings.Left(strAmount, intPlaceHolder).Length, "*");
					fldTextAmount2.Text = Strings.Right(strAmount, strAmount.Length - intPlaceHolder) + Strings.StrDup(70 - Strings.Right(strAmount, strAmount.Length - intPlaceHolder).Length, "*");
				}
				else
				{
					fldTextAmount1.Text = "";
					fldTextAmount2.Text = strAmount + Strings.StrDup(70 - strAmount.Length, "*");
				}
				rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ID = 0");
				rsCheckInfo.AddNew();
				rsCheckInfo.Set_Fields("CheckNumber", lngcheck - 1);
				rsCheckInfo.Set_Fields("VendorNumber", lngFooterVendorNumber);
				rsCheckInfo.Set_Fields("VendorName", Strings.Trim(strFooterVendorName));
				rsCheckInfo.Set_Fields("CheckAddress1", Strings.Trim(strFooterAddress1));
				rsCheckInfo.Set_Fields("CheckAddress2", Strings.Trim(strFooterAddress2));
				rsCheckInfo.Set_Fields("CheckAddress3", Strings.Trim(strFooterAddress3));
				rsCheckInfo.Set_Fields("CheckCity", Strings.Trim(strFooterCity));
				rsCheckInfo.Set_Fields("CheckState", Strings.Trim(strFooterState));
				rsCheckInfo.Set_Fields("CheckZip", Strings.Trim(strFooterZip));
				rsCheckInfo.Set_Fields("CheckZip4", Strings.Trim(strFooterZip4));
				rsCheckInfo.Set_Fields("WarrantNumber", frmPrintChecks.InstancePtr.TempWarrant);
				rsCheckInfo.Set_Fields("Amount", curTotal);
				rsCheckInfo.Set_Fields("CheckDate", frmPrintChecks.InstancePtr.txtPayDate.Text);
				strCheckSQL = strFooterJournals;
				if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 1)
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "=", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + FCConvert.ToString(lngcheck - 1) + "' ORDER BY IsNull(Seperate, 0) DESC";
				}
				else
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "<", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + FCConvert.ToString(lngcheck - 1) + "' ORDER BY IsNull(Seperate, 0) DESC";
				}
				rsCheckInfo.Set_Fields("Journals", strCheckSQL);
				rsCheckInfo.Set_Fields("Status", "R");
				rsCheckInfo.Update(true);
				curTotal = 0;
				if (lngFooterVendorNumber != 0)
				{
					rsCheckInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(lngFooterVendorNumber));
					if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
					{
						rsCheckInfo.Edit();
						rsCheckInfo.Set_Fields("LastPayment", DateTime.Today);
						rsCheckInfo.Set_Fields("CheckNumber", lngcheck - 1);
						rsCheckInfo.Update();
					}
				}
				if (frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 1)
				{
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsDupChecks.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + rsVendorInfo.Get_Fields("CheckNumber") + " AND WarrantNumber = " + FCConvert.ToString(frmPrintChecks.InstancePtr.TempWarrant));
					// AND ID > " & rsVendorInfo.Fields["ID"]
					if (rsDupChecks.EndOfFile() != true && rsDupChecks.BeginningOfFile() != true)
					{
						rsVendorInfo.Delete();
						rsVendorInfo.Update();
					}
					else
					{
						rsVendorInfo.Edit();
						rsVendorInfo.Set_Fields("Status", "V");
						rsVendorInfo.Update();
					}
				}
			}
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private void FillRows_2184(int intRow, string strWarr, string strDesc, string strRef, string curCredit, string curDisc, string curAmt)
		{
			FillRows(ref intRow, ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
		}

		private void FillRows(ref int intRow, ref string strWarr, ref string strDesc, ref string strRef, ref string curCredit, ref string curDisc, ref string curAmt)
		{
			switch (intRow)
			{
				case 0:
					{
						FillRow1(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 1:
					{
						FillRow2(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 2:
					{
						FillRow3(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 3:
					{
						FillRow4(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 4:
					{
						FillRow5(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 5:
					{
						FillRow6(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 6:
					{
						FillRow7(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 7:
					{
						FillRow8(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 8:
					{
						FillRow9(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 9:
					{
						FillRow10(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
			}
			//end switch
		}

		private void FillRow1(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant1.Text = strWarr;
			fldDescription1.Text = strDesc;
			fldReference1.Text = strRef;
			fldDiscount1.Text = strDisc;
			fldAmt1.Text = strAmt;
			fldBottomWarrant1.Text = strWarr;
			fldBottomDescription1.Text = strDesc;
			fldBottomReference1.Text = strRef;
			fldBottomDiscount1.Text = strDisc;
			fldBottomAmount1.Text = strAmt;
			fldCredit1.Text = strCredit;
			fldBottomCredit1.Text = strCredit;
		}

		private void FillRow2(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant2.Text = strWarr;
			fldDescription2.Text = strDesc;
			fldReference2.Text = strRef;
			fldDiscount2.Text = strDisc;
			fldAmt2.Text = strAmt;
			fldBottomWarrant2.Text = strWarr;
			fldBottomDescription2.Text = strDesc;
			fldBottomReference2.Text = strRef;
			fldBottomDiscount2.Text = strDisc;
			fldBottomAmount2.Text = strAmt;
			fldCredit2.Text = strCredit;
			fldBottomCredit2.Text = strCredit;
		}

		private void FillRow3(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant3.Text = strWarr;
			fldDescription3.Text = strDesc;
			fldReference3.Text = strRef;
			fldDiscount3.Text = strDisc;
			fldAmt3.Text = strAmt;
			fldBottomWarrant3.Text = strWarr;
			fldBottomDescription3.Text = strDesc;
			fldBottomReference3.Text = strRef;
			fldBottomDiscount3.Text = strDisc;
			fldBottomAmount3.Text = strAmt;
			fldCredit3.Text = strCredit;
			fldBottomCredit3.Text = strCredit;
		}

		private void FillRow4(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant4.Text = strWarr;
			fldDescription4.Text = strDesc;
			fldReference4.Text = strRef;
			fldDiscount4.Text = strDisc;
			fldAmt4.Text = strAmt;
			fldBottomWarrant4.Text = strWarr;
			fldBottomDescription4.Text = strDesc;
			fldBottomReference4.Text = strRef;
			fldBottomDiscount4.Text = strDisc;
			fldBottomAmount4.Text = strAmt;
			fldCredit4.Text = strCredit;
			fldBottomCredit4.Text = strCredit;
		}

		private void FillRow5(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant5.Text = strWarr;
			fldDescription5.Text = strDesc;
			fldReference5.Text = strRef;
			fldDiscount5.Text = strDisc;
			fldAmt5.Text = strAmt;
			fldBottomWarrant5.Text = strWarr;
			fldBottomDescription5.Text = strDesc;
			fldBottomReference5.Text = strRef;
			fldBottomDiscount5.Text = strDisc;
			fldBottomAmount5.Text = strAmt;
			fldCredit5.Text = strCredit;
			fldBottomCredit5.Text = strCredit;
		}

		private void FillRow6(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant6.Text = strWarr;
			fldDescription6.Text = strDesc;
			fldReference6.Text = strRef;
			fldDiscount6.Text = strDisc;
			fldAmt6.Text = strAmt;
			fldBottomWarrant6.Text = strWarr;
			fldBottomDescription6.Text = strDesc;
			fldBottomReference6.Text = strRef;
			fldBottomDiscount6.Text = strDisc;
			fldBottomAmount6.Text = strAmt;
			fldCredit6.Text = strCredit;
			fldBottomCredit6.Text = strCredit;
		}

		private void FillRow7(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant7.Text = strWarr;
			fldDescription7.Text = strDesc;
			fldReference7.Text = strRef;
			fldDiscount7.Text = strDisc;
			fldAmt7.Text = strAmt;
			fldBottomWarrant7.Text = strWarr;
			fldBottomDescription7.Text = strDesc;
			fldBottomReference7.Text = strRef;
			fldBottomDiscount7.Text = strDisc;
			fldBottomAmount7.Text = strAmt;
			fldCredit7.Text = strCredit;
			fldBottomCredit7.Text = strCredit;
		}

		private void FillRow8(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant8.Text = strWarr;
			fldDescription8.Text = strDesc;
			fldReference8.Text = strRef;
			fldDiscount8.Text = strDisc;
			fldAmt8.Text = strAmt;
			fldBottomWarrant8.Text = strWarr;
			fldBottomDescription8.Text = strDesc;
			fldBottomReference8.Text = strRef;
			fldBottomDiscount8.Text = strDisc;
			fldBottomAmount8.Text = strAmt;
			fldCredit8.Text = strCredit;
			fldBottomCredit8.Text = strCredit;
		}

		private void FillRow9(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant9.Text = strWarr;
			fldDescription9.Text = strDesc;
			fldReference9.Text = strRef;
			fldDiscount9.Text = strDisc;
			fldAmt9.Text = strAmt;
			fldBottomWarrant9.Text = strWarr;
			fldBottomDescription9.Text = strDesc;
			fldBottomReference9.Text = strRef;
			fldBottomDiscount9.Text = strDisc;
			fldBottomAmount9.Text = strAmt;
			fldCredit9.Text = strCredit;
			fldBottomCredit9.Text = strCredit;
		}

		private void FillRow10(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant10.Text = strWarr;
			fldDescription10.Text = strDesc;
			fldReference10.Text = strRef;
			fldDiscount10.Text = strDisc;
			fldAmt10.Text = strAmt;
			fldBottomWarrant10.Text = strWarr;
			fldBottomDescription10.Text = strDesc;
			fldBottomReference10.Text = strRef;
			fldBottomDiscount10.Text = strDisc;
			fldBottomAmount10.Text = strAmt;
			fldCredit10.Text = strCredit;
			fldBottomCredit10.Text = strCredit;
		}

		private void rptLaserChecksTop_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLaserChecksTop.Caption	= "Checks";
			//rptLaserChecksTop.Icon	= "rptLaserChecksTop.dsx":0000";
			//rptLaserChecksTop.Left	= 0;
			//rptLaserChecksTop.Top	= 0;
			//rptLaserChecksTop.Width	= 11880;
			//rptLaserChecksTop.Height	= 8595;
			//rptLaserChecksTop.StartUpPosition	= 3;
			//rptLaserChecksTop.SectionData	= "rptLaserChecksTop.dsx":058A;
			//End Unmaped Properties
		}
	}
}
