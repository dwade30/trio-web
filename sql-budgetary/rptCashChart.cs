﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCashChart.
	/// </summary>
	public partial class rptCashChart : BaseSectionReport
	{
		public static rptCashChart InstancePtr
		{
			get
			{
				return (rptCashChart)Sys.GetInstance(typeof(rptCashChart));
			}
		}
		//protected rptCashChart _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			Sys.ClearInstance(this);
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCashChart	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;

		public rptCashChart()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Cash Charting";
			if (frmCashChart.InstancePtr.mscInfo != null)
			{
				frmCashChart.InstancePtr.CopyChart(frmCashChart.InstancePtr.mscInfo, ref mscInfo);
				this.mscInfo.Height = 6.875F;
				this.mscInfo.Left = 0.0625F;
				this.mscInfo.Name = "mscInfo";
				this.mscInfo.Top = 0.125F;
				this.mscInfo.Width = 7.375F;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			// - "AutoDim"
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			//for (x = 0; x <= this.Toolbar.Tools.Count - 1; x++)
			//{
			//	if (this.Toolbar.Tools(x).Text == "Print...")
			//	{
			//		this.Toolbar.Tools(x).ID = 9950;
			//		this.Toolbar.Tools(x).Enabled = true;
			//	}
			//}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				Label6.Text = "Monthly Totals";
			}
			else
			{
				Label6.Text = "Cumulative Totals";
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmCashChart.InstancePtr.Show(App.MainForm);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//mscInfo.ChartAreas.Add(new GrapeCity.ActiveReports.Chart.ChartArea(true));
			//mscInfo.ChartAreas[0].Name = "test";
			//var source = frmCashChart.InstancePtr.mscInfo.ChartData;
			//for (int y = 1; y < source.GetLength(1); y++)
			//{
			//    int index = mscInfo.Series.Add(new GrapeCity.ActiveReports.Chart.Series());
			//    mscInfo.Series[index].Type = GrapeCity.ActiveReports.Chart.ChartType.Bar2D;
			//    //mscInfo.ChartAreas[0].Axes[y + 1].Position = 0;
			//}
			//mscInfo.ChartAreas[0].Axes[0].Position = 0;
			//mscInfo.ChartAreas[0].Axes[1].Position = 0;
			//mscInfo.ChartAreas[0].Axes[2].Position = 0;
			//mscInfo.ChartAreas[0].Axes[3].Position = 0;
			//mscInfo.ChartAreas[0].Axes[4].Position = 0;
			//for (int i = 0; i < source.GetLength(0); i++)
			//{
			//    if (source[i, 0] != null)
			//    {
			//        mscInfo.ChartAreas[0].Axes[0].Labels.Add(source[i, 0].ToString());
			//        if (source.GetLength(1) > 1)
			//        {
			//            if (source.GetLength(1) >= 1)
			//            {
			//                GrapeCity.ActiveReports.Chart.DataPoint point = new GrapeCity.ActiveReports.Chart.DataPoint();
			//                point.XValue = source[i, 0].ToString();
			//                GrapeCity.ActiveReports.Chart.DoubleArray values = null;
			//                values = new GrapeCity.ActiveReports.Chart.DoubleArray(new double[] { FCConvert.ToDouble(source[i, 1]) });
			//                point.YValues = values;
			//                mscInfo.Series[0].Points.Add(point);
			//            }
			//            if (source.GetLength(1) >= 2)
			//            {
			//                GrapeCity.ActiveReports.Chart.DataPoint point = new GrapeCity.ActiveReports.Chart.DataPoint();
			//                point.XValue = source[i, 0].ToString();
			//                GrapeCity.ActiveReports.Chart.DoubleArray values = null;
			//                values = new GrapeCity.ActiveReports.Chart.DoubleArray(new double[] { FCConvert.ToDouble(source[i, 2]) });
			//                point.YValues = values;
			//                mscInfo.Series[1].Points.Add(point);
			//            }
			//        }
			//    }
			//}
			//FC:TODO:AM
			//int counter;    // - "AutoDim"
			//mscInfo.ChartData = frmCashChart.InstancePtr.mscInfo.ChartData;
			//mscInfo.Title = frmCashChart.InstancePtr.mscInfo.Title;
			//mscInfo.RowCount = frmCashChart.InstancePtr.mscInfo.RowCount; // Set the number of rows. This must be done before
			//                                                              // setting chart data.
			//mscInfo.ColumnCount = frmCashChart.InstancePtr.mscInfo.ColumnCount; // Two columns. The first shows the miles per
			//                                                                    // gallon, the second the gallons used.
			//mscInfo.ColumnLabelCount = frmCashChart.InstancePtr.mscInfo.ColumnLabelCount;
			//for (counter = 1; counter <= mscInfo.ColumnLabelCount; counter++)
			//{
			//    mscInfo.Column = counter;
			//    frmCashChart.InstancePtr.mscInfo.Column = FCConvert.ToInt16(counter);
			//    mscInfo.ColumnLabel = frmCashChart.InstancePtr.mscInfo.ColumnLabel;
			//    mscInfo.Plot.SeriesCollection[counter].Pen.VtColor.red = frmCashChart.InstancePtr.mscInfo.Plot.SeriesCollection[counter].Pen.VtColor.red;
			//    mscInfo.Plot.SeriesCollection[counter].Pen.VtColor.green = frmCashChart.InstancePtr.mscInfo.Plot.SeriesCollection[counter].Pen.VtColor.green;
			//    mscInfo.Plot.SeriesCollection[counter].Pen.VtColor.blue = frmCashChart.InstancePtr.mscInfo.Plot.SeriesCollection[counter].Pen.VtColor.blue;
			//    mscInfo.Plot.SeriesCollection[counter].Pen.Style = frmCashChart.InstancePtr.mscInfo.Plot.SeriesCollection[counter].Pen.Style;
			//}
			//mscInfo.AllowDithering = true; // Set this to False if your color monitor
			//                               // uses only 8 bits.
			//for (counter = 1; counter <= mscInfo.Plot.Axis(VtChAxisIdY, 1).LabelLevelCount; counter++)
			//{
			//    mscInfo.Plot.Axis(VtChAxisIdY, 1).Labels(counter).format = "#,##0.00";
			//}
			//for (counter = 1; counter <= mscInfo.Plot.Axis(VtChAxisIdY2, 1).LabelLevelCount; counter++)
			//{
			//    mscInfo.Plot.Axis(VtChAxisIdY2, 1).Labels(counter).format = "#,##0.00";
			//}
			//mscInfo.Legend.Location.LocationType = frmCashChart.InstancePtr.mscInfo.Legend.Location.LocationType;
			//mscInfo.chartType = frmCashChart.InstancePtr.mscInfo.chartType;
			//mscInfo.Refresh();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	int intReturn;
		//	ActiveReportsRTFExport.ARExportRTF ArTemp = new ActiveReportsRTFExport.ARExportRTF();
		//	string temp = "";
		//	string strDirectory = "";
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		Information.Err(ex).Clear();
		//              App.MainForm.CommonDialog1.Flags = 0;
		//		App.MainForm.CommonDialog1.CancelError = true;
		//		/*? On Error Resume Next  */
		//		App.MainForm.CommonDialog1.ShowPrinter();
		//		if (Information.Err(ex).Number == 0)
		//		{
		//                  if ((App.MainForm.CommonDialog1.Flags & FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile) == FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile)
		//                  {
		//                      strDirectory = Application.StartupPath;
		//                  }
		//			Information.Err(ex).Clear();
		//                  App.MainForm.CommonDialog1.Flags = 0;
		//			App.MainForm.CommonDialog1.CancelError = true;
		//			/*? On Error Resume Next  */
		//			App.MainForm.CommonDialog1.Filter = "*.doc";
		//			App.MainForm.CommonDialog1.ShowSave();
		//			if (Information.Err(ex).Number == 0)
		//			{
		//				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building Report");
		//				temp = App.MainForm.CommonDialog1.FileName;
		//				if (Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare) == 0)
		//				{
		//					temp += ".doc";
		//				}
		//				else
		//				{
		//					temp = Strings.Left(temp, Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare)) + "doc";
		//				}
		//			}
		//			else
		//			{
		//				Application.StartupPath = strDirectory;
		//				return;
		//			}
		//			ArTemp = new ActiveReportsRTFExport.ARExportRTF;
		//			this.Run(false);
		//			ArTemp.FileName = temp;
		//			//Application.DoEvents();
		//			this.Export(ArTemp);
		//			frmWait.InstancePtr.Unload();
		//			CheckIt2:;
		//			if (modBudgetaryMaster.JobComplete_2("rptCashChart"))
		//			{
		//				// do nothing
		//			}
		//			else
		//			{
		//				goto CheckIt2;
		//			}
		//			Application.StartupPath = strDirectory;
		//		}
		//		else
		//		{
		//			this.Document.Print(false);
		//		}
		//	}
		//	else
		//	{
		//		return;
		//	}
		//}
		private void rptCashChart_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCashChart.Caption	= "Cash Charting";
			//rptCashChart.Icon	= "rptCashChart.dsx":0000";
			//rptCashChart.Left	= 0;
			//rptCashChart.Top	= 0;
			//rptCashChart.Width	= 8520;
			//rptCashChart.Height	= 8235;
			//rptCashChart.StartUpPosition	= 3;
			//rptCashChart.SectionData	= "rptCashChart.dsx":058A;
			//End Unmaped Properties
		}
	}
}
