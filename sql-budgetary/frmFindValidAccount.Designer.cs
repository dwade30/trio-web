﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmFindValidAccount.
	/// </summary>
	partial class frmFindValidAccount : BaseForm
	{
		public fecherFoundation.FCGrid vs2;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmFindValidAccount));
			this.vs2 = new fecherFoundation.FCGrid();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// vs2
			//
			this.vs2.Name = "vs2";
			this.vs2.Enabled = true;
			this.vs2.TabStop = false;
			this.vs2.TabIndex = 0;
			this.vs2.Location = new System.Drawing.Point(4, 33);
			this.vs2.Size = new System.Drawing.Size(602, 470);
			this.vs2.Cols = 6;
			this.vs2.Editable = FCGrid.EditableSettings.flexEDNone;
			this.vs2.FixedCols = 0;
			this.vs2.FixedRows = 1;
			this.vs2.Rows = 50;
			this.vs2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			//this.vs2.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vs2.OcxState")));
			this.vs2.DoubleClick += new System.EventHandler(this.vs2_DblClick);
			this.vs2.KeyDown += new KeyEventHandler(this.vs2_KeyDownEvent);
			this.vs2.KeyPress += new KeyPressEventHandler(this.vs2_KeyPressEvent);
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			//
			// mnuProcessSave
			//
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuProcessQuit
			//
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuProcess
			});
			//
			// frmFindValidAccount
			//
			this.ClientSize = new System.Drawing.Size(610, 496);
			this.ClientArea.Controls.Add(this.vs2);
			this.Menu = this.MainMenu1;
			this.Name = "frmFindValidAccount";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Activated += new System.EventHandler(this.frmFindValidAccount_Activated);
			this.Load += new System.EventHandler(this.frmFindValidAccount_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFindValidAccount_KeyPress);
			this.Resize += new System.EventHandler(this.frmFindValidAccount_Resize);
			this.Text = "Valid Accounts";
			this.vs2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs2)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
