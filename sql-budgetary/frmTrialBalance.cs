﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmTrialBalance.
	/// </summary>
	public partial class frmTrialBalance : BaseForm
	{
		public frmTrialBalance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTrialBalance InstancePtr
		{
			get
			{
				return (frmTrialBalance)Sys.GetInstance(typeof(frmTrialBalance));
			}
		}

		protected frmTrialBalance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController bactController = new cBDAccountController();
		private cBDAccountController bactController_AutoInitialized;

		private cBDAccountController bactController
		{
			get
			{
				if (bactController_AutoInitialized == null)
				{
					bactController_AutoInitialized = new cBDAccountController();
				}
				return bactController_AutoInitialized;
			}
			set
			{
				bactController_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection listFunds = new cGenericCollection();
		private cGenericCollection listFunds_AutoInitialized;

		private cGenericCollection listFunds
		{
			get
			{
				if (listFunds_AutoInitialized == null)
				{
					listFunds_AutoInitialized = new cGenericCollection();
				}
				return listFunds_AutoInitialized;
			}
			set
			{
				listFunds_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cTrialBalanceView theView = new cTrialBalanceView();
		private cTrialBalanceView theView_AutoInitialized;

		private cTrialBalanceView theView
		{
			get
			{
				if (theView_AutoInitialized == null)
				{
					theView_AutoInitialized = new cTrialBalanceView();
				}
				return theView_AutoInitialized;
			}
			set
			{
				theView_AutoInitialized = value;
			}
		}

		private bool boolLoading;

		private void chkIncludeZeroAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				theView.ShowZeroAccounts = chkIncludeZeroAccounts.CheckState == CheckState.Checked;
			}
		}

		private void cmbFundEnd_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (cmbFundEnd.SelectedIndex >= 0)
				{
					int lngId = 0;
					lngId = cmbFundEnd.ItemData(cmbFundEnd.SelectedIndex);
					theView.SetCurrentEndFund(lngId);
				}
			}
		}

		private void cmbFundStart_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (cmbFundStart.SelectedIndex >= 0)
				{
					int lngId = 0;
					lngId = cmbFundStart.ItemData(cmbFundStart.SelectedIndex);
					theView.SetCurrentStartFund(lngId);
				}
			}
		}

		private void cmbPeriodEnd_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				theView.EndPeriod = FCConvert.ToInt16(cmbPeriodEnd.SelectedIndex + 1);
			}
		}

		private void cmbPeriodStart_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				theView.StartPeriod = FCConvert.ToInt16(cmbPeriodStart.SelectedIndex + 1);
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdExport_Click(object sender, System.EventArgs e)
		{
			ExportRaw();
		}

		private void ExportRaw()
		{
			this.ShowWait();
			FCUtils.StartTask(this, () =>
		   {
			   this.UpdateWait("Loading Data ...");
			   theView.ExportReport();
			   this.EndWait();
		   });
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			PrintReport();
		}

		private void frmTrialBalance_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTrialBalance_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTrialBalance.FillStyle	= 0;
			//frmTrialBalance.ScaleWidth	= 6210;
			//frmTrialBalance.ScaleHeight	= 4590;
			//frmTrialBalance.LinkTopic	= "Form2";
			//frmTrialBalance.LockControls	= -1  'True;
			//frmTrialBalance.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoading = true;
			FillFundCombos();
			FillPeriodCombos();
			cmbAllFunds.SelectedIndex = 0;
			cmbAllPeriods.SelectedIndex = 0;
			boolLoading = false;
			RefreshView();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			PrintReport();
		}

		private void PrintReport()
		{
			this.ShowWait();
			FCUtils.StartTask(this, () =>
			{
				this.UpdateWait("Loading Data ...");
				theView.PrintReport();
				this.EndWait();
			});
		}

		private void RefreshView()
		{
			boolLoading = true;
			if (theView.ShowAllFunds)
			{
				cmbAllFunds.SelectedIndex = 0;
			}
			else
			{
				theView.ShowAllFunds = false;
			}
			if (theView.ShowAllPeriods)
			{
				cmbAllPeriods.SelectedIndex = 0;
			}
			else
			{
				cmbAllPeriods.SelectedIndex = 1;
			}
			if (theView.ShowRangeOfFunds)
			{
				cmbAllFunds.SelectedIndex = 1;
			}
			else
			{
				cmbAllFunds.SelectedIndex = 0;
			}
			if (theView.ShowRangeOfPeriods)
			{
				cmbAllPeriods.SelectedIndex = 1;
			}
			else
			{
				cmbAllPeriods.SelectedIndex = 0;
			}
			if (theView.ShowZeroAccounts)
			{
				chkIncludeZeroAccounts.CheckState = CheckState.Checked;
			}
			else
			{
				chkIncludeZeroAccounts.CheckState = CheckState.Unchecked;
			}
			if (theView.StartPeriod >= 1 && theView.StartPeriod <= 12)
			{
				cmbPeriodStart.SelectedIndex = theView.StartPeriod - 1;
			}
			if (theView.EndPeriod >= 1 && theView.EndPeriod <= 12)
			{
				cmbPeriodEnd.SelectedIndex = theView.EndPeriod - 1;
			}
			int x;
			if (theView.ShowRangeOfFunds)
			{
				cmbFundEnd.Enabled = true;
				cmbFundStart.Enabled = true;
				//lblfundto.Visible = true;
			}
			else
			{
				cmbFundEnd.Enabled = false;
				cmbFundStart.Enabled = false;
				//lblfundto.Visible = false;
			}
			if (theView.ShowRangeOfPeriods)
			{
				cmbPeriodEnd.Enabled = true;
				cmbPeriodStart.Enabled = true;
				//lblperiodto.Visible = true;
			}
			else
			{
				cmbPeriodEnd.Enabled = false;
				cmbPeriodStart.Enabled = false;
				//lblperiodto.Visible = false;
			}
			if (!(theView.StartFund == null))
			{
				for (x = 0; x <= cmbFundStart.Items.Count - 1; x++)
				{
					if (cmbFundStart.ItemData(x) == theView.StartFund.ID)
					{
						cmbFundStart.SelectedIndex = x;
						break;
					}
				}
			}
			else
			{
				cmbFundStart.SelectedIndex = -1;
			}
			if (!(theView.EndFund == null))
			{
				for (x = 0; x <= cmbFundEnd.Items.Count - 1; x++)
				{
					if (cmbFundEnd.ItemData(x) == theView.EndFund.ID)
					{
						cmbFundEnd.SelectedIndex = x;
						break;
					}
				}
			}
			else
			{
				cmbFundEnd.SelectedIndex = -1;
			}
			boolLoading = false;
		}

		private void FillFundCombos()
		{
			theView.Funds.MoveFirst();
			cFund tFund;
			cmbFundStart.Clear();
			while (theView.Funds.IsCurrent())
			{
				tFund = (cFund)theView.Funds.GetCurrentItem();
				cmbFundStart.AddItem(tFund.Fund + "-" + tFund.Description);
				cmbFundStart.ItemData(cmbFundStart.NewIndex, tFund.ID);
				cmbFundEnd.AddItem(tFund.Fund + "-" + tFund.Description);
				cmbFundEnd.ItemData(cmbFundEnd.NewIndex, tFund.ID);
				theView.Funds.MoveNext();
			}
		}

		private void FillPeriodCombos()
		{
			int x;
			cmbPeriodEnd.Clear();
			cmbPeriodStart.Clear();
			string strTemp = "";
			for (x = 1; x <= 12; x++)
			{
				switch (x)
				{
					case 1:
						{
							strTemp = "January";
							break;
						}
					case 2:
						{
							strTemp = "February";
							break;
						}
					case 3:
						{
							strTemp = "March";
							break;
						}
					case 4:
						{
							strTemp = "April";
							break;
						}
					case 5:
						{
							strTemp = "May";
							break;
						}
					case 6:
						{
							strTemp = "June";
							break;
						}
					case 7:
						{
							strTemp = "July";
							break;
						}
					case 8:
						{
							strTemp = "August";
							break;
						}
					case 9:
						{
							strTemp = "September";
							break;
						}
					case 10:
						{
							strTemp = "October";
							break;
						}
					case 11:
						{
							strTemp = "November";
							break;
						}
					case 12:
						{
							strTemp = "December";
							break;
						}
				}
				//end switch
				cmbPeriodStart.AddItem(strTemp);
				cmbPeriodEnd.AddItem(strTemp);
			}
		}

		private void optAllFunds_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (cmbAllFunds.SelectedIndex == 0)
				{
					theView.ShowAllFunds = true;
				}
				else
				{
					theView.ShowRangeOfFunds = true;
				}
				RefreshView();
			}
		}

		private void optAllPeriods_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (cmbAllPeriods.SelectedIndex == 0)
				{
					theView.ShowAllPeriods = true;
				}
				else
				{
					theView.ShowRangeOfPeriods = true;
				}
				RefreshView();
			}
		}
		//private void optFundRange_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    if (!boolLoading)
		//    {
		//        theView.ShowRangeOfFunds = true;
		//        RefreshView();
		//    }
		//}
		//private void optPeriodRange_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    if (!boolLoading)
		//    {
		//        theView.ShowRangeOfPeriods = true;
		//        RefreshView();
		//    }
		//}
	}
}
