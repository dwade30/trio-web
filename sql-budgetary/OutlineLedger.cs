﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmOutlineLedgerQuery.
	/// </summary>
	public partial class frmOutlineLedgerQuery : BaseForm
	{
		public frmOutlineLedgerQuery()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmOutlineLedgerQuery InstancePtr
		{
			get
			{
				return (frmOutlineLedgerQuery)Sys.GetInstance(typeof(frmOutlineLedgerQuery));
			}
		}

		protected frmOutlineLedgerQuery _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		[DllImport("user32")]
		private static extern int GetCursorPos(ref POINTAPI lpPoint);

		[StructLayout(LayoutKind.Sequential)]
		private struct POINTAPI
		{
			public int x;
			public int y;
		};

		string length = "";
		// holds format account lengths
		int NumberOfFunds;
		string[] DeletedFunds = new string[20 + 1];
		int FundLength;
		int YearLength;
		bool CollapsedFlag;
		int AcctLength;
		// holds division length
		int NumberCol;
		// column in the flex grid that has the database key
		int FundCol;
		int AcctCol;
		// column in the grid that holds the division number
		int ShortCol;
		// column in the grid that holds the short description
		int LongCol;
		// column in the grid that holds the long description
		int YearCol;
		int TaxCol;
		int AccountTypeCol;
		// column that shows if you have a PUC chart of accounts to let you select Asset, Liability, or Fund Balance
		float AcctPercent;
		float FundPercent;
		bool EditFlag;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		bool blnUnload;
		bool blnDirty;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Frame1.Visible = false;
			vs1.Enabled = true;
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			int counter = 0;
			int counter2 = 0;
			string temp = "";
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDelete.Text == "")
			{
				MessageBox.Show("You must enter the Number of the Fund you wish to delete", "Invalid Fund Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			answer = MessageBox.Show("Are you sure you wish to Delete Fund " + txtDelete.Text + "?", "Delete Fund?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.Yes)
			{
				if (CheckFundUsed_2(Strings.Trim(txtDelete.Text)) == false)
				{
					vs1.Enabled = true;
					counter = 1;
					temp = Strings.Mid(vs1.TextMatrix(counter, FundCol), 1, FundLength);
					while (temp != txtDelete.Text)
					{
						counter += 1;
						if (counter == vs1.Rows)
						{
							break;
						}
						temp = Strings.Mid(vs1.TextMatrix(counter, FundCol), 1, FundLength);
					}
					if (counter == vs1.Rows)
					{
						MessageBox.Show("No Fund found with the Fund Number of " + txtDelete.Text, "No Matches Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtDelete.Text = "";
						Frame1.Visible = false;
						return;
					}
					else
					{
						vs1.RemoveItem(counter);
						if (vs1.Rows == 1)
						{
							counter2 = 0;
							while (DeletedFunds[counter2] != "")
							{
								counter2 += 1;
								if (counter2 == 20)
								{
									return;
								}
							}
							DeletedFunds[counter2] = txtDelete.Text;
							txtDelete.Text = "";
							Frame1.Visible = false;
							cmdFundDelete.Enabled = false;
							vs1_Collapsed();
							return;
						}
						else
						{
							if (counter == vs1.Rows)
							{
								counter2 = 0;
								while (DeletedFunds[counter2] != "")
								{
									counter2 += 1;
									if (counter2 == 20)
									{
										return;
									}
								}
								DeletedFunds[counter2] = txtDelete.Text;
								txtDelete.Text = "";
								Frame1.Visible = false;
								vs1_Collapsed();
								return;
							}
							//FC:FINAL:RPU: #i644- RowOutlineLevel can't be 0
							//while (vs1.RowOutlineLevel(counter) != 0)
							while (vs1.RowOutlineLevel(counter) != 1)
							{
								vs1.RemoveItem(counter);
								if (vs1.Rows == 1)
								{
									cmdFundDelete.Enabled = false;
									break;
								}
								else if (counter == vs1.Rows)
								{
									break;
								}
							}
						}
						counter2 = 0;
						while (DeletedFunds[counter2] != "")
						{
							counter2 += 1;
							if (counter2 == 20)
							{
								return;
							}
						}
						DeletedFunds[counter2] = txtDelete.Text;
						txtDelete.Text = "";
						Frame1.Visible = false;
						vs1_Collapsed();
					}
				}
				else
				{
					MessageBox.Show("You may not delete this Fund because it it has a balance, is used in one or more journal entries, or one or more Departments are setup with this fund.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				txtDelete.Focus();
				txtDelete.SelectionStart = 0;
				txtDelete.SelectionLength = 1;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void frmOutlineLedgerQuery_Activated(object sender, System.EventArgs e)
		{
			int temp;
			int temp2;
			int temp3;
			string tempAcct;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			NumberCol = 1;
			YearCol = 4;
			AcctCol = 3;
			// initialize the columns
			ShortCol = 5;
			LongCol = 6;
			FundCol = 2;
			TaxCol = 7;
			AccountTypeCol = 8;
			FCUtils.EraseSafe(DeletedFunds);
			//FC:FINAl:SBE - #i632 - initialize array with empty string
			for (int i = 0; i < DeletedFunds.Length; i++)
			{
				DeletedFunds[i] = "";
			}
			YearLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 5, 2)))));
			// initialize department length
			FundLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 1, 2)))));
			AcctLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 3, 2)))));
			// initialize division lngth
			vs1.ColWidth(0, 300);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(FundCol, 900);
			FundPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(FundCol)) / vs1.WidthOriginal);
			if (YearLength == 0)
			{
				vs1.ColWidth(YearCol, 0);
			}
			else
			{
				vs1.ColWidth(YearCol, 600);
				// set column widths
			}
			vs1.ColWidth(ShortCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
			if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + 1200));
				vs1.ColWidth(TaxCol, 100);
			}
			else
			{
				vs1.ColWidth(LongCol, FCConvert.ToInt32(vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + (vs1.Width * 0.2))));
				vs1.ColWidth(TaxCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			}
			vs1.ColWidth(AccountTypeCol, 100);
			if (AcctLength < 5)
			{
				vs1.ColWidth(AcctCol, 600);
				AcctPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(AcctCol)) / vs1.WidthOriginal);
			}
			else
			{
				vs1.ColWidth(AcctCol, AcctLength * 140 + 50);
				AcctPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(AcctCol)) / vs1.WidthOriginal);
			}
			if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				vs1.ColHidden(AccountTypeCol, true);
			}
			else
			{
				vs1.ColHidden(AccountTypeCol, false);
				vs1.ColComboList(AccountTypeCol, "#1;A" + "\t" + "Asset|#2;L" + "\t" + "Liability|#3;F" + "\t" + "Fund Balance");
			}
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vs1.TextMatrix(0, FundCol, "Fund");
			vs1.TextMatrix(0, AcctCol, "Acct");
			// set column headings
			vs1.TextMatrix(0, ShortCol, "Short Desc");
			vs1.TextMatrix(0, LongCol, "Long Desc");
			vs1.TextMatrix(0, YearCol, "Suff");
			vs1.TextMatrix(0, TaxCol, "1099?");
			vs1.TextMatrix(0, AccountTypeCol, "Type");
			rs.OpenRecordset("SELECT * FROM LedgerTitles");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				vs1.Rows = rs.RecordCount() + 1;
			}
			else
			{
				cmdFundDelete.Enabled = false;
				vs1.Rows = 1;
			}
			for (temp = 1; temp <= vs1.Rows - 1; temp++)
			{
				vs1.TextMatrix(temp, NumberCol, "0");
			}
			vs1.Visible = true;
			vs1.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			// allow the user to resize the columns
			temp2 = 1;
			tempAcct = modValidateAccount.GetFormat("0", ref AcctLength);
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + tempAcct + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				NumberOfFunds = rs.RecordCount();
				for (temp = 1; temp <= rs.RecordCount(); temp++)
				{
                    vs1.TextMatrix(temp2, FundCol, rs.Get_Fields("Fund") + " -");
					vs1.TextMatrix(temp2, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
					vs1.TextMatrix(temp2, LongCol, (rs.Get_Fields_String("LongDescription") == null ? "" : rs.Get_Fields_String("LongDescription")));
					vs1.TextMatrix(temp2, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					vs1.RowOutlineLevel(temp2, 1);
					vs1.IsSubtotal(temp2, true);
					temp2 += 1;
                    rs2.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + rs.Get_Fields("Fund") + "' AND NOT Account = '" + tempAcct + "' ORDER BY Account, Year");
					if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
					{
						rs2.MoveLast();
						rs2.MoveFirst();
						for (temp3 = 1; temp3 <= rs2.RecordCount(); temp3++)
						{
							vs1.RowOutlineLevel(temp2, 2);
                            vs1.TextMatrix(temp2, AcctCol, FCConvert.ToString(rs2.Get_Fields("account")));
							if (modAccountTitle.Statics.YearFlag)
							{
								// do nothing
							}
							else
							{
                                vs1.TextMatrix(temp2, YearCol, FCConvert.ToString(rs2.Get_Fields("Year")));
							}
							vs1.TextMatrix(temp2, ShortCol, FCConvert.ToString(rs2.Get_Fields_String("ShortDescription")));
							vs1.TextMatrix(temp2, LongCol, FCConvert.ToString(rs2.Get_Fields_String("LongDescription")));
							vs1.TextMatrix(temp2, NumberCol, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
                            if (Strings.Trim(rs2.Get_Fields("Tax") + "") != "")
							{
                                vs1.TextMatrix(temp2, TaxCol, FCConvert.ToString(rs2.Get_Fields("Tax")));
							}
							else
							{
								vs1.TextMatrix(temp2, TaxCol, "N");
							}
							if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
							{
								if (Strings.Trim(rs2.Get_Fields_String("GLAccountType") + "") != "")
								{
									vs1.TextMatrix(temp2, AccountTypeCol, FCConvert.ToString(rs2.Get_Fields_String("GLAccountType")));
								}
								else
								{
									vs1.TextMatrix(temp2, AccountTypeCol, "1");
								}
							}
							temp2 += 1;
							rs2.MoveNext();
						}
					}
					rs.MoveNext();
				}
			}
			//FC:FINAL:DDU:#2946 - aligned columns
			for (temp = 1; temp <= temp2 - 1; temp++)
			{
				if (vs1.RowOutlineLevel(temp) == 1)
				{
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, YearCol, temp2 - 1, YearCol, 5);
					vs1.IsCollapsed(temp, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			vs1.ColAlignment(FundCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AcctCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1_Collapsed();
            modColorScheme.ColorGrid(vs1);
			vs1.Focus();
			if (vs1.Rows > 1)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vs1.Rows - 1, 0, Color.White);
			}
			frmOutlineLedgerQuery.InstancePtr.Refresh();
			// refresh the form
		}

		private void frmOutlineLedgerQuery_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int NextOpenRow = 0;
			if (KeyCode == Keys.Tab)
			{
				if (vs1.Col == LongCol)
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						KeyCode = (Keys)0;
						if (vs1.RowOutlineLevel(vs1.Row) == 1 && vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							NextOpenRow = vs1.Row + 1;
							while (vs1.RowOutlineLevel(NextOpenRow) != 0)
							{
								NextOpenRow += 1;
								if (NextOpenRow == vs1.Rows)
								{
									break;
								}
							}
						}
						else
						{
							NextOpenRow = vs1.Row + 1;
						}
						if (NextOpenRow == vs1.Rows)
						{
							return;
						}
						if (vs1.RowOutlineLevel(NextOpenRow) == 1)
						{
							if (vs1.TextMatrix(NextOpenRow, FundCol) != "")
							{
								EditFlag = true;
								vs1.Select(NextOpenRow, ShortCol);
								EditFlag = false;
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								EditFlag = true;
								vs1.Select(NextOpenRow, FundCol);
								EditFlag = false;
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						else
						{
							if (vs1.TextMatrix(NextOpenRow, AcctCol) != "")
							{
								if (modAccountTitle.Statics.YearFlag)
								{
									EditFlag = true;
									vs1.Select(NextOpenRow, ShortCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									if (vs1.TextMatrix(NextOpenRow, YearCol) != "")
									{
										EditFlag = true;
										vs1.Select(NextOpenRow, ShortCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										EditFlag = true;
										vs1.Select(NextOpenRow, YearCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
							}
							else
							{
								EditFlag = true;
								vs1.Select(NextOpenRow, AcctCol);
								EditFlag = false;
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
			}
			if (KeyCode == Keys.F10)
			{
				// if the user saves
				KeyCode = (Keys)0;
			}
		}

		private void frmOutlineLedgerQuery_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOutlineLedgerQuery.FillStyle	= 0;
			//frmOutlineLedgerQuery.ScaleWidth	= 9045;
			//frmOutlineLedgerQuery.ScaleHeight	= 7410;
			//frmOutlineLedgerQuery.LinkTopic	= "Form2";
			//frmOutlineLedgerQuery.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			length = modAccountTitle.Statics.Ledger;
			blnDirty = false;
			modGlobalFunctions.SetTRIOColors(this);
			vs1.AddExpandButton(1);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (e.CloseReason != FCCloseReason.FormCode)
			{
				if (blnDirty)
				{
					ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
				}
			}
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void frmOutlineLedgerQuery_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmOutlineLedgerQuery.InstancePtr.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmOutlineLedgerQuery_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(FundCol, FCConvert.ToInt32(vs1.WidthOriginal * FundPercent));
			if (!modAccountTitle.Statics.YearFlag)
			{
				vs1.ColWidth(YearCol, FCConvert.ToInt32(vs1.WidthOriginal * FundPercent));
			}
			vs1.ColWidth(AcctCol, FCConvert.ToInt32(vs1.WidthOriginal * AcctPercent));
			vs1.ColWidth(ShortCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
			if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + 1200));
				vs1.ColWidth(TaxCol, 100);
			}
			else
			{
				vs1.ColWidth(LongCol, FCConvert.ToInt32(vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + (vs1.WidthOriginal * 0.2))));
				vs1.ColWidth(TaxCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			}
			vs1.ColWidth(AccountTypeCol, 100);
			Frame1.CenterToContainer(this.ClientArea);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuFundAdd_Click(object sender, System.EventArgs e)
		{
			string tempFund = "";
			int counter;
			int intAddCounter = 0;
			vs1.Rows += 1;
			//FC:FINAL:DDU:#2947 - changed row color
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, FundCol, vs1.Rows - 1, vs1.Cols - 1, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, FundCol, vs1.Rows - 1, vs1.Cols - 1, 0x80000018);
			vs1.RowOutlineLevel(vs1.Rows - 1, 1);
			vs1.IsSubtotal(vs1.Rows - 1, true);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, Color.White);
			vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
			cmdFundDelete.Enabled = true;
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM' OR Code = 'EC' OR Code = 'RC' OR Code = 'EO' OR Code = 'FB' OR Code = 'PE' ORDER BY Row");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				intAddCounter = 0;
				for (counter = rs.RecordCount(); counter >= 1; counter--)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (Conversion.Val(rs.Get_Fields("account")) != 0)
					{
						vs1.Rows += 1;
						intAddCounter += 1;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						vs1.TextMatrix(vs1.Rows - 1, AcctCol, FCConvert.ToString(rs.Get_Fields("account")));
						vs1.TextMatrix(vs1.Rows - 1, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
						vs1.TextMatrix(vs1.Rows - 1, LongCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
						vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
						if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							if (intAddCounter == 1)
							{
								vs1.TextMatrix(vs1.Rows - 1, AccountTypeCol, "1");
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, AccountTypeCol, "3");
							}
						}
						if (modAccountTitle.Statics.YearFlag)
						{
							// do nothing
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, YearCol, "00");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 2);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, Color.White);
					}
					rs.MoveNext();
				}
			}
			if (vs1.Rows > intAddCounter + 1)
			{
				vs1.IsCollapsed(vs1.Rows - (intAddCounter + 1), FCGrid.CollapsedSettings.flexOutlineCollapsed);
			}
			vs1_Collapsed();
			vs1.Focus();
			if (vs1.Rows > intAddCounter + 1)
			{
				vs1.Select(vs1.Rows - (intAddCounter + 1), FundCol);
			}
			vs1.EditCell();
			//Application.DoEvents();
			vs1.EditMaxLength = 0;
		}

		private void mnuFundDelete_Click(object sender, System.EventArgs e)
		{
			Frame1.Visible = true;
			txtDelete.Focus();
			vs1.Enabled = false;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void txtDelete_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				txtDelete_Validate(false);
				cmdDelete_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDelete_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp;
			temp = txtDelete.Text;
			txtDelete.Text = modValidateAccount.GetFormat(temp, ref FundLength);
		}

		public void txtDelete_Validate(bool Cancel)
		{
			txtDelete_Validating(txtDelete, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				blnDirty = true;
			}
		}

		private void vs1_ClickEvent(object sender, DataGridViewCellEventArgs e)
		{
            //POINTAPI MousePosition = new POINTAPI();
            // vbPorter upgrade warning: PassFail As bool	OnWriteFCConvert.ToInt32(
            //bool PassFail;
            //PassFail = FCConvert.ToBoolean(GetCursorPos(ref MousePosition));
            //if (MousePosition.x * FCScreen.TwipsPerPixelX < vs1.Left + vs1.ColWidth(0))
            //{
			//	vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
			//	return;
			//}
			if (vs1.Col == NumberCol)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
				return;
			}
			if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.Col == AcctCol && vs1.TextMatrix(vs1.Row, FundCol) == "")
				{
					vs1.Select(vs1.Row, FundCol);
				}
				else if (vs1.Col == AcctCol || vs1.Col == YearCol)
				{
					vs1.Select(vs1.Row, ShortCol);
				}
				if (vs1.Col == FundCol && vs1.TextMatrix(vs1.Row, FundCol) == "")
				{
					// do nothing
				}
				else if (vs1.Col == FundCol)
				{
					vs1.Select(vs1.Row, ShortCol);
				}
				if (vs1.Col > LongCol)
				{
					vs1.Select(vs1.Row, LongCol);
				}
			}
			else
			{
				if (vs1.Col == FundCol)
				{
					if (vs1.TextMatrix(vs1.Row, AcctCol) == "")
					{
						vs1.Select(vs1.Row, AcctCol);
					}
					else
					{
						if (modAccountTitle.Statics.YearFlag)
						{
							vs1.Select(vs1.Row, ShortCol);
						}
						else
						{
							if (vs1.TextMatrix(vs1.Row, YearCol) == "")
							{
								vs1.Select(vs1.Row, YearCol);
							}
							else
							{
								vs1.Select(vs1.Row, ShortCol);
							}
						}
					}
				}
				if (vs1.Col == TaxCol)
				{
					// If vs1.TextMatrix(vs1.Row, ObjCol) <> "" Then
					vs1.EditCell();
					if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
					{
						vs1.EditText = "N";
					}
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
					vs1.EditMaxLength = 1;
					// Exit Sub
					// Else
					// vs1.Col = ObjCol
					// vs1.EditCell
					// vs1.EditSelStart = 0
					// End If
				}
				if (vs1.Col == AcctCol)
				{
					if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
					{
						if (modAccountTitle.Statics.YearFlag)
						{
							vs1.Select(vs1.Row, ShortCol);
						}
						else
						{
							if (vs1.TextMatrix(vs1.Row, YearCol) == "")
							{
								vs1.Select(vs1.Row, YearCol);
							}
							else
							{
								vs1.Select(vs1.Row, ShortCol);
							}
						}
					}
				}
				if (vs1.Col == YearCol)
				{
					if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
					{
						vs1.Select(vs1.Row, ShortCol);
					}
				}
			}
			if (vs1.Col == ShortCol)
			{
				vs1.EditMaxLength = 12;
			}
			else if (vs1.Col == LongCol)
			{
				vs1.EditMaxLength = 30;
			}
			vs1.Select(vs1.Row, vs1.Col);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			// give the cell focus
			vs1.EditSelStart = 0;
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			//FC:FINAL:CHN - issue #1220: Incorrect Grid working. Fixed using rows and columns numbers on correct values.
			vs1_Collapsed(e);
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			//FC:FINAL:CHN - issue #1220: Incorrect Grid working. Fixed using rows and columns numbers on correct values.
			vs1_Collapsed(e);
		}

		private void vs1_Collapsed(DataGridViewRowEventArgs e = null)
		{
			int counter;
			int rows = 0;
			int height;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineExpanded)
				{
					rows += 1;
				}
				else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed && vs1.RowOutlineLevel(counter) == 1)
				{
					rows += 1;
					counter += 1;
					//FC:FINAL:BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
					//while (vs1.RowOutlineLevel(counter) != 0)
					while (vs1.RowOutlineLevel(counter) != 1)
					{
						counter += 1;
						if (counter == vs1.Rows)
						{
							break;
						}
					}
					counter -= 1;
				}
			}
			height = (rows + 1) * vs1.RowHeight(0);
			if (height < frmOutlineLedgerQuery.InstancePtr.Height * 0.7375)
			{
				if (vs1.Height != height)
				{
					//FC:FINAL:ASZ - use anchors: vs1.Height = height + 75;
					if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
					{
						vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + 1400));
					}
					else
					{
						vs1.ColWidth(LongCol, FCConvert.ToInt32(vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + (vs1.WidthOriginal * 0.22))));
					}
				}
			}
			else
			{
				//FC:FINAL:ASZ - use anchors: vs1.Height = FCConvert.ToInt32(frmOutlineLedgerQuery.InstancePtr.Height * 0.7375 + 75);
				if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
				{
					vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + 1400));
				}
				else
				{
					vs1.ColWidth(LongCol, FCConvert.ToInt32(vs1.WidthOriginal - (vs1.ColWidth(FundCol) + vs1.ColWidth(AcctCol) + vs1.ColWidth(0) + vs1.ColWidth(YearCol) + vs1.ColWidth(ShortCol) + (vs1.WidthOriginal * 0.22))));
				}
			}
			//FC:FINAL:CHN - issue #1220: Incorrect Grid working. Fixed using rows and columns numbers on correct values.
			int row = e != null ? vs1.GetFlexRowIndex(e.RowIndex) : vs1.Row;
			counter = row;
			//FC:FINAL:BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
			//while (vs1.RowOutlineLevel(counter) != 0)
			while (counter > 0 && vs1.RowOutlineLevel(counter) != 1)
			{
                
				counter -= 1;
			}
			if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				vs1.Row = counter;
			}
			else
			{
				// do nothing
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// vbPorter upgrade warning: Check As string	OnWrite(DialogResult)
			int Check = 0;
			int counter = 0;
			string tempFund = "";
			string tempAcct = "";
			string tempYear = "";
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Insert)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						MessageBox.Show("You must open up a Fund to add an Account to it", "Unable to Create an Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
						return;
					}
					else if (vs1.TextMatrix(vs1.Row, FundCol) == "")
					{
						MessageBox.Show("You must have a Short Description and a Number for a Fund before you can add an Account", "Unable to Create an Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
						return;
					}
					else
					{
						if (vs1.Row == vs1.Rows - 1)
						{
							vs1.Rows += 1;
							vs1.Select(vs1.Row + 1, AcctCol);
							vs1.EditText = "";
							vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
							vs1.RowOutlineLevel(vs1.Row, 2);
							vs1.TextMatrix(vs1.Row, NumberCol, "0");
							vs1_Collapsed();
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							vs1.Select(vs1.Row, AcctCol);
							vs1.EditCell();
							vs1.EditSelStart = 0;
							return;
						}
					}
				}
				if (vs1.Row == vs1.Rows - 1)
				{
					vs1.Rows += 1;
				}
				else
				{
					vs1.AddItem("", vs1.Row + 1);
					// add a row in
				}
				vs1.Select(vs1.Row + 1, AcctCol);
				vs1.EditText = "";
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
				vs1.RowOutlineLevel(vs1.Row, 2);
				vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
				vs1_Collapsed();
				counter = vs1.Row - 1;
				//FC:FINAL:BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
				//while (vs1.RowOutlineLevel(counter) != 0)
				while (vs1.RowOutlineLevel(counter) != 1)
				{
					counter -= 1;
				}
				vs1.Select(vs1.Row, AcctCol);
				vs1.EditCell();
				vs1.EditSelStart = 0;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
			else if (KeyCode == Keys.Delete)
			{
				// if they hit the delete key
				if (vs1.TextMatrix(vs1.Row, AcctCol) != "" && vs1.TextMatrix(vs1.Row, ShortCol) != "")
				{
					if (vs1.RowOutlineLevel(vs1.Row) > 1)
					{
						Check = FCConvert.ToInt32(MessageBox.Show("Are You Sure You Want To Delete This Record?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
						// make sure they want to delete
						if (Check == FCConvert.ToInt32(DialogResult.Yes))
						{
							// if they do
							if (modAccountTitle.Statics.YearFlag)
							{
								tempAcct = vs1.TextMatrix(vs1.Row, AcctCol);
							}
							else
							{
								tempAcct = vs1.TextMatrix(vs1.Row, AcctCol) + "-" + vs1.TextMatrix(vs1.Row, YearCol);
							}
							if (CheckLedgerUsed(ref tempAcct) == false)
							{
								if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, NumberCol)) != 0)
								{
									rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, NumberCol))));
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
										tempAcct = FCConvert.ToString(rs.Get_Fields("account"));
										// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
										tempYear = FCConvert.ToString(rs.Get_Fields("Year"));
										// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
										tempFund = FCConvert.ToString(rs.Get_Fields("Fund"));
										rs.Delete();
										rs.Update();
									}
									if (modAccountTitle.Statics.YearFlag)
									{
										rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' and FirstAccountField = '" + tempFund + "' and SecondAccountField = '" + tempAcct + "'");
										if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
										{
											rs.Delete();
											rs.Update();
										}
									}
									else
									{
										rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' and FirstAccountField = '" + tempFund + "' and SecondAccountField = '" + tempAcct + "' and ThirdAccountField = '" + tempYear + "'");
										if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
										{
											rs.Delete();
											rs.Update();
										}
									}
								}
								vs1.RemoveItem(vs1.Row);
								vs1_Collapsed();
								counter = vs1.Row;
								//FC:FINAL:BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
								//while (vs1.RowOutlineLevel(counter) != 0)
								while (vs1.RowOutlineLevel(counter) != 1)
								{
									counter -= 1;
								}
							}
							else
							{
								MessageBox.Show("You may not delete this Ledger Account because it it has a balance or is used in one or more journal entries.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(vs1.Row) > 1)
					{
						vs1.RemoveItem(vs1.Row);
						vs1_Collapsed();
						counter = vs1.Row - 1;
						//FC:FINAL:BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
						//while (vs1.RowOutlineLevel(counter) != 0)
						while (vs1.RowOutlineLevel(counter) != 1)
						{
							counter -= 1;
						}
					}
				}
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
		}

        private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyDown -= vs1_KeyDownEdit;
            e.Control.KeyDown += vs1_KeyDownEdit;
        }

        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			int counter = 0;
			int NextOpenRow;
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == FundCol)
			{
				// if they are in the division field
				if (vs1.EditText.Length == FundLength)
				{
					// if the cell is the length of the division number
					if (vs1.EditSelStart == FundLength)
					{
						// dont move the cursor
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
						{
							KeyCode = 0;
							// dont send the key through
							vs1.Col = ShortCol;
							// send the user to the next column
						}
					}
				}
			}
			if (vs1.Col == AcctCol)
			{
				// if they are in the division field
				if (vs1.EditText.Length == AcctLength)
				{
					// if the cell is the length of the division number
					if (vs1.EditSelStart == AcctLength)
					{
						// dont move the cursor
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
						{
							KeyCode = 0;
							// dont send the key through
							vs1.Col = YearCol;
							// send the user to the next column
						}
					}
				}
			}
			if (vs1.Col == YearCol)
			{
				if (vs1.EditText.Length == YearLength)
				{
					if (vs1.EditSelStart == YearLength)
					{
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
						{
							KeyCode = 0;
							vs1.Col = ShortCol;
						}
					}
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					counter = vs1.Row + 1;
					//FC:FINAL:BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
					//while (vs1.RowOutlineLevel(counter) != 0)
					while (vs1.RowOutlineLevel(counter) != 1)
					{
						counter += 1;
						if (counter < vs1.Rows - 1)
						{
							// do nothing
						}
						else
						{
							break;
						}
					}
				}
				else
				{
					counter = vs1.Row + 1;
				}
				if (counter > vs1.Rows - 1)
				{
					return;
				}
				if (vs1.Col == FundCol || vs1.Col == AcctCol)
				{
					if (vs1.RowOutlineLevel(counter) > 1)
					{
						if (vs1.TextMatrix(counter, AcctCol) != "")
						{
							if (modAccountTitle.Statics.YearFlag)
							{
								KeyCode = 0;
								EditFlag = true;
								vs1.Select(counter, ShortCol);
								EditFlag = false;
								vs1.EditCell();
								vs1.EditSelStart = 0;
								Support.SendKeys("{LEFT}", false);
							}
							else
							{
								if (vs1.TextMatrix(counter, YearCol) != "")
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(counter, ShortCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
									Support.SendKeys("{LEFT}", false);
								}
								else
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(counter, YearCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
									Support.SendKeys("{LEFT}", false);
								}
							}
						}
						else
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(counter, AcctCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							Support.SendKeys("{LEFT}", false);
						}
					}
					else
					{
						if (vs1.TextMatrix(counter, FundCol) != "")
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(counter, ShortCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							Support.SendKeys("{LEFT}", false);
						}
						else
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(counter, FundCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							Support.SendKeys("{LEFT}", false);
						}
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(counter) > 1)
					{
						if (vs1.TextMatrix(counter, AcctCol) != "")
						{
							if (modAccountTitle.Statics.YearFlag)
							{
								if (vs1.Col != YearCol)
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(counter, vs1.Col);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
									Support.SendKeys("{LEFT}", false);
								}
							}
							else
							{
								if (vs1.TextMatrix(counter, YearCol) != "")
								{
									if (vs1.Col != YearCol)
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, vs1.Col);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
										Support.SendKeys("{LEFT}", false);
									}
									else
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, ShortCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
										Support.SendKeys("{LEFT}", false);
									}
								}
							}
						}
						else
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(counter, AcctCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							Support.SendKeys("{LEFT}", false);
						}
					}
					else
					{
						if (vs1.TextMatrix(counter, FundCol) != "")
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(counter, ShortCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							Support.SendKeys("{LEFT}", false);
						}
						else
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(counter, FundCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							Support.SendKeys("{LEFT}", false);
						}
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == ShortCol)
					{
						if (vs1.TextMatrix(vs1.Row, FundCol) != "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							vs1.Col = FundCol;
						}
					}
				}
				else
				{
					if (vs1.Col == ShortCol)
					{
						if (vs1.TextMatrix(vs1.Row, AcctCol) != "" && vs1.TextMatrix(vs1.Row, YearCol) != "")
						{
							KeyCode = 0;
						}
						else if (vs1.TextMatrix(vs1.Row, AcctCol) != "")
						{
							if (modAccountTitle.Statics.YearFlag)
							{
								KeyCode = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Col = YearCol;
							}
						}
						else
						{
							KeyCode = 0;
							vs1.Col = AcctCol;
						}
					}
					else if (vs1.Col == YearCol)
					{
						if (vs1.TextMatrix(vs1.Row, AcctCol) != "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							vs1.Col = AcctCol;
						}
					}
					else if (vs1.Col == AcctCol)
					{
						KeyCode = 0;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vs1.Row > 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row) == 1)
					{
						counter = vs1.Row - 1;
						//FC: FINAL: BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
						//while (vs1.RowOutlineLevel(counter) != 0)
						while (vs1.RowOutlineLevel(counter) != 1)
						{
							counter -= 1;
						}
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							// do nothing
						}
						else
						{
							counter = vs1.Row - 1;
						}
						if (counter == vs1.Row - 1)
						{
							if (vs1.Col == AcctCol || vs1.Col == FundCol)
							{
								if (vs1.RowOutlineLevel(vs1.Row - 1) == 1)
								{
									if (vs1.TextMatrix(vs1.Row - 1, FundCol) != "")
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, ShortCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, FundCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
								else
								{
									if (vs1.TextMatrix(vs1.Row - 1, AcctCol) != "")
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, ShortCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, AcctCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
							}
						}
						else
						{
							if (vs1.RowOutlineLevel(counter) > 1)
							{
								if (vs1.TextMatrix(counter, AcctCol) != "")
								{
									if (!modAccountTitle.Statics.YearFlag)
									{
										if (vs1.TextMatrix(counter, YearCol) != "")
										{
											KeyCode = 0;
											EditFlag = true;
											vs1.Select(counter, ShortCol);
											EditFlag = false;
											vs1.EditCell();
											vs1.EditSelStart = 0;
										}
										else
										{
											KeyCode = 0;
											EditFlag = true;
											vs1.Select(counter, YearCol);
											EditFlag = false;
											vs1.EditCell();
											vs1.EditSelStart = 0;
										}
									}
									else
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(counter, ShortCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
								else
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(counter, AcctCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
							else
							{
								if (vs1.TextMatrix(counter, FundCol) != "")
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(counter, ShortCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(counter, FundCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
						}
					}
					else
					{
						if (vs1.Col == AcctCol || vs1.Col == FundCol)
						{
							if (vs1.TextMatrix(vs1.Row - 1, AcctCol) != "")
							{
								if (!modAccountTitle.Statics.YearFlag)
								{
									if (vs1.TextMatrix(vs1.Row - 1, YearCol) != "")
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(vs1.Row - 1, ShortCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										EditFlag = true;
										vs1.Select(vs1.Row - 1, YearCol);
										EditFlag = false;
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
								else
								{
									KeyCode = 0;
									EditFlag = true;
									vs1.Select(vs1.Row - 1, ShortCol);
									EditFlag = false;
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
							else
							{
								KeyCode = 0;
								EditFlag = true;
								vs1.Select(vs1.Row - 1, AcctCol);
								EditFlag = false;
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == FundCol)
					{
						if (vs1.EditText == "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							EditFlag = true;
							vs1.Select(vs1.Row, ShortCol);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
					}
				}
				else
				{
					if (vs1.Col == AcctCol)
					{
						if (vs1.EditText == "")
						{
							KeyCode = 0;
						}
						else if (!modAccountTitle.Statics.YearFlag)
						{
							if (vs1.TextMatrix(vs1.Row, YearCol) == "")
							{
								KeyCode = 0;
								vs1.Col = YearCol;
							}
						}
						else
						{
							KeyCode = 0;
							vs1.Col = ShortCol;
						}
					}
				}
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				Close();
			}
			else if (KeyCode == Keys.Insert)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						MessageBox.Show("You must open up a Fund to add an Account to it", "Unable to Create an Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
						return;
					}
					else if (vs1.TextMatrix(vs1.Row, FundCol) == "")
					{
						MessageBox.Show("You must have a Short Description and a Number for a Fund before you can add an Account", "Unable to Create an Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
						return;
					}
					else
					{
						if (vs1.Row == vs1.Rows - 1)
						{
							vs1.Rows += 1;
							EditFlag = true;
							vs1.Select(vs1.Row + 1, AcctCol);
							vs1.EditMaxLength = AcctLength;
							EditFlag = false;
							vs1.EditText = "";
							vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
							vs1.RowOutlineLevel(vs1.Row, 2);
							vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
							vs1.TextMatrix(vs1.Row, TaxCol, "N");
							if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
							{
								vs1.TextMatrix(vs1.Row, AccountTypeCol, "1");
							}
							vs1_Collapsed();
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							return;
						}
					}
				}
				if (vs1.Row == vs1.Rows - 1)
				{
					vs1.Rows += 1;
				}
				else
				{
					vs1.AddItem("", vs1.Row + 1);
					// add a row in
				}
				EditFlag = true;
				vs1.Select(vs1.Row + 1, AcctCol);
				vs1.EditMaxLength = AcctLength;
				EditFlag = false;
				vs1.EditText = "";
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
				vs1.RowOutlineLevel(vs1.Row, 2);
				vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
				vs1_Collapsed();
				counter = vs1.Row - 1;
				//FC: FINAL: BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
				//while (vs1.RowOutlineLevel(counter) != 0)
				while (vs1.RowOutlineLevel(counter) != 1)
				{
					counter -= 1;
				}
				vs1.EditCell();
				vs1.EditSelStart = 0;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.Col == ShortCol)
			{
				vs1.EditMaxLength = 12;
			}
			else if (vs1.Col == LongCol)
			{
				vs1.EditMaxLength = 30;
			}
			else if (vs1.Col == FundCol)
			{
				vs1.EditMaxLength = FundLength;
			}
			else if (vs1.Col == AcctCol)
			{
				vs1.EditMaxLength = AcctLength;
			}
			if (!EditFlag)
			{
				if (vs1.Col == NumberCol)
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
					return;
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if ((vs1.Col == AcctCol || vs1.Col == YearCol) && vs1.TextMatrix(vs1.Row, FundCol) == "")
					{
						EditFlag = true;
						vs1.Select(vs1.Row, FundCol);
						EditFlag = false;
					}
					else if (vs1.Col == AcctCol || vs1.Col == YearCol)
					{
						EditFlag = true;
						vs1.Select(vs1.Row, ShortCol);
						EditFlag = false;
					}
					if (vs1.Col == FundCol && vs1.TextMatrix(vs1.Row, FundCol) == "")
					{
						// do nothing
					}
					else if (vs1.Col == FundCol)
					{
						EditFlag = true;
						vs1.Select(vs1.Row, ShortCol);
						EditFlag = false;
					}
					if (vs1.Col > LongCol)
					{
						EditFlag = true;
						vs1.Select(vs1.Row, LongCol);
						EditFlag = false;
					}
				}
				else
				{
					if (vs1.Col == FundCol)
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							if (vs1.TextMatrix(vs1.Row, AcctCol) == "")
							{
								EditFlag = true;
								vs1.Select(vs1.Row, AcctCol);
								EditFlag = false;
							}
						}
						else if (vs1.TextMatrix(vs1.Row, YearCol) == "")
						{
							EditFlag = true;
							vs1.Select(vs1.Row, YearCol);
							EditFlag = false;
						}
						else
						{
							EditFlag = true;
							vs1.Select(vs1.Row, ShortCol);
							EditFlag = false;
						}
					}
					if (vs1.Col == AcctCol || vs1.Col == YearCol)
					{
						if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
						{
							if (!modAccountTitle.Statics.YearFlag)
							{
								if (vs1.TextMatrix(vs1.Row, YearCol) != "")
								{
									EditFlag = true;
									vs1.Select(vs1.Row, ShortCol);
									EditFlag = false;
								}
								else
								{
									EditFlag = true;
									vs1.Select(vs1.Row, YearCol);
									EditFlag = false;
								}
							}
							else
							{
								EditFlag = true;
								vs1.Select(vs1.Row, ShortCol);
								EditFlag = false;
							}
						}
					}
					else
					{
						if (vs1.TextMatrix(vs1.Row, AcctCol) == "")
						{
							EditFlag = true;
							vs1.Select(vs1.Row, AcctCol);
							EditFlag = false;
						}
						else if (!modAccountTitle.Statics.YearFlag)
						{
							if (vs1.TextMatrix(vs1.Row, YearCol) == "")
							{
								// EditFlag = True
								// vs1.Select vs1.Row, ShortCol
								// EditFlag = False
								// Else
								EditFlag = true;
								vs1.Select(vs1.Row, YearCol);
								EditFlag = false;
							}
						}
					}
				}
				if (vs1.Col == TaxCol)
				{
					vs1.EditCell();
					if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
					{
						vs1.EditText = "N";
					}
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
					vs1.EditMaxLength = 1;
					return;
				}
				if (vs1.Col == AccountTypeCol)
				{
					vs1.EditMaxLength = 0;
				}
				vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				EditFlag = true;
				vs1.Select(vs1.Row, vs1.Col);
				EditFlag = false;
				vs1.EditCell();
				// give the cell focus
				vs1.EditSelStart = 0;
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:CHN - issue #1220: Incorrect Grid working. Fixed using rows and columns numbers on correct values.
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			int counter = 0;
			int counter2 = 0;
			bool flag;
			string search = "";
			if (col == AcctCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref AcctLength);
					}
				}
				if (modAccountTitle.Statics.YearFlag)
				{
					if (vs1.EditText.Length == AcctLength && AcctLength != 0)
					{
						// if there is something in the field
						counter = row - 1;
						//FC: FINAL: BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
						//while (vs1.RowOutlineLevel(counter) != 0)
						while (vs1.RowOutlineLevel(counter) != 1)
						{
							counter -= 1;
						}
						if (row == vs1.Rows - 1)
						{
							counter2 = row;
						}
						else
						{
							counter2 = row + 1;
						}
						//FC: FINAL: BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
						//while (vs1.RowOutlineLevel(counter2) != 0)
						while (vs1.RowOutlineLevel(counter2) != 1)
						{
							counter2 += 1;
							if (counter2 == vs1.Rows)
							{
								counter2 -= 1;
								break;
							}
						}
						for (counter = counter; counter <= counter2 - 1; counter++)
						{
							// check to see if it is a duplicate division number
							if (counter == row)
							{
								// do nothing
							}
							else
							{
								if (vs1.EditText == vs1.TextMatrix(counter, col))
								{
									// if there is a match give an error
									MessageBox.Show("This Account Number is Not Allowable Because it Matches a Previously Existing Account Number", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									// cancel their action
									vs1.EditSelStart = 0;
									// highlight the field
									vs1.EditSelLength = vs1.EditText.Length;
									return;
								}
							}
						}
					}
				}
			}
			if (col == FundCol)
			{
				// if we are in the fund column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref FundLength);
						search = vs1.EditText;
						vs1.EditText = vs1.EditText + " -";
						rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE (Row = 10 OR Row = 32 OR Row = 41 OR Row = 50) AND Account = '" + search + "' ORDER BY Row");
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.MoveLast();
							rs.MoveFirst();
							vs1.TextMatrix(row, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
							vs1.TextMatrix(row, LongCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
							vs1.TextMatrix(row, NumberCol, FCConvert.ToString(0));
							vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row, 0, Color.White);
							switch (FCConvert.ToInt32(rs.Get_Fields_Int32("Row")))
							{
								case 10:
									{
										rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row = 12 OR Row = 13 OR Row = 14 OR Row = 15 OR Row = 16 OR Row = 18 OR Row = 19 OR Row = 20 OR Row = 21 OR Row = 22 OR Row = 24 OR Row = 25 OR Row = 26 OR Row = 27 OR Row = 28 OR Row = 29 ORDER BY Row DESC");
										if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
										{
											rs.MoveLast();
											rs.MoveFirst();
											while (rs.EndOfFile() != true)
											{
												// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
												if (Strings.Left(FCConvert.ToString(rs.Get_Fields("account")), 1) == "G")
												{
													vs1.AddItem("", row + 1);
													if (FCConvert.ToInt32(rs.Get_Fields_Int32("Row")) < 17)
													{
														vs1.TextMatrix(row + 1, ShortCol, "R/E " + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 3));
														vs1.TextMatrix(row + 1, LongCol, "R/E " + rs.Get_Fields_String("LongDescription"));
														vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
														if (!modAccountTitle.Statics.YearFlag)
														{
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength) + " -");
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, YearCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 5 + FundLength + AcctLength, YearLength));
														}
														else
														{
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength));
														}
														vs1.RowOutlineLevel(row + 1, 2);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row + 1, 0, Color.White);
													}
													else if (rs.Get_Fields_Int32("Row") < 23)
													{
														vs1.TextMatrix(row + 1, ShortCol, "P/P " + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 3));
														vs1.TextMatrix(row + 1, LongCol, "P/P " + rs.Get_Fields_String("LongDescription"));
														vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
														if (!modAccountTitle.Statics.YearFlag)
														{
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength) + " -");
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, YearCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 5 + FundLength + AcctLength, YearLength));
														}
														else
														{
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength));
														}
														vs1.RowOutlineLevel(row + 1, 2);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row + 1, 0, Color.White);
													}
													else
													{
														vs1.TextMatrix(row + 1, ShortCol, "T/L " + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 3));
														vs1.TextMatrix(row + 1, LongCol, "T/L " + rs.Get_Fields_String("LongDescription"));
														vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
														if (!modAccountTitle.Statics.YearFlag)
														{
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength) + " -");
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, YearCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 5 + FundLength + AcctLength, YearLength));
														}
														else
														{
															// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
															vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength));
														}
														vs1.RowOutlineLevel(row + 1, 2);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row + 1, 0, Color.White);
													}
												}
												rs.MoveNext();
											}
										}
										break;
									}
								case 32:
									{
										rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row = 33 OR Row = 34 OR Row = 35 OR Row = 36 OR Row = 37 OR Row = 38 OR Row = 39 ORDER BY Row DESC");
										if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
										{
											rs.MoveLast();
											rs.MoveFirst();
											while (rs.EndOfFile() != true)
											{
												// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
												if (Strings.Left(FCConvert.ToString(rs.Get_Fields("account")), 1) == "G")
												{
													vs1.AddItem("", row + 1);
													switch (FCConvert.ToInt32(rs.Get_Fields_Int32("Row")))
													{
														case 36:
															{
																vs1.TextMatrix(row + 1, ShortCol, Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 4));
																break;
															}
														default:
															{
																vs1.TextMatrix(row + 1, ShortCol, Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 3));
																break;
															}
													}
													//end switch
													vs1.TextMatrix(row + 1, LongCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
													vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
													if (!modAccountTitle.Statics.YearFlag)
													{
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength) + " -");
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, YearCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 5 + FundLength + AcctLength, YearLength));
													}
													else
													{
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength));
													}
													vs1.RowOutlineLevel(row + 1, 2);
													vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row + 1, 0, Color.White);
												}
												rs.MoveNext();
											}
										}
										break;
									}
								case 41:
									{
										rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row = 42 OR Row = 43 OR Row = 44 OR Row = 45 OR Row = 46 OR Row = 47 OR Row = 48 ORDER BY Row DESC");
										if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
										{
											rs.MoveLast();
											rs.MoveFirst();
											while (rs.EndOfFile() != true)
											{
												// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
												if (Strings.Left(FCConvert.ToString(rs.Get_Fields("account")), 1) == "G")
												{
													vs1.AddItem("", row + 1);
													switch (FCConvert.ToInt32(rs.Get_Fields_Int32("Row")))
													{
														case 44:
															{
																vs1.TextMatrix(row + 1, ShortCol, Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 4));
																break;
															}
														default:
															{
																vs1.TextMatrix(row + 1, ShortCol, Strings.Left(FCConvert.ToString(rs.Get_Fields_String("ShortDescription")), 3));
																break;
															}
													}
													//end switch
													vs1.TextMatrix(row + 1, LongCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
													vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
													if (!modAccountTitle.Statics.YearFlag)
													{
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength) + " -");
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, YearCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 5 + FundLength + AcctLength, YearLength));
													}
													else
													{
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, AcctCol, Strings.Mid(FCConvert.ToString(rs.Get_Fields("account")), 4 + FundLength, AcctLength));
													}
													vs1.RowOutlineLevel(row + 1, 2);
													vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row + 1, 0, Color.White);
												}
												rs.MoveNext();
											}
										}
										break;
									}
								case 50:
									{
										rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row = 51 OR Row = 52 ORDER BY Row DESC");
										if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
										{
											rs.MoveLast();
											rs.MoveFirst();
											while (rs.EndOfFile() != true)
											{
												// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
												if (FCConvert.ToString(rs.Get_Fields("account")) != "")
												{
													vs1.AddItem("", row + 1);
													vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
													if (modAccountTitle.Statics.YearFlag)
													{
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, AcctCol, FCConvert.ToString(rs.Get_Fields("account")));
													}
													else
													{
														// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
														vs1.TextMatrix(row + 1, AcctCol, rs.Get_Fields("account") + " -");
														vs1.TextMatrix(row + 1, YearCol, "00");
													}
													vs1.TextMatrix(row + 1, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
													vs1.TextMatrix(row + 1, LongCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
													vs1.RowOutlineLevel(row + 1, 2);
													vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row + 1, 0, Color.White);
												}
												rs.MoveNext();
											}
										}
										break;
									}
							}
							//end switch
							vs1.IsCollapsed(row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
						}
					}
				}
				if (vs1.EditText.Length == FundLength + 2)
				{
					// if there is something in the field
					for (counter = 1; counter <= vs1.Rows - 1; counter++)
					{
						// check to see if it is a duplicate division number
						if (vs1.RowOutlineLevel(counter) == 1)
						{
							if (counter == row)
							{
								// do nothing
							}
							else
							{
								if (vs1.EditText == vs1.TextMatrix(counter, col))
								{
									// if there is a match give an error
									MessageBox.Show("This Fund Number is Not Allowable Because it Matches a Previously Existing Fund Number", "Invalid Fund Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									// cancel their action
									vs1.EditSelStart = 0;
									// highlight the field
									vs1.EditSelLength = vs1.EditText.Length;
									return;
								}
							}
						}
					}
				}
			}
			if (col == TaxCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) && Strings.UCase(vs1.EditText) != "N")
					{
						MessageBox.Show("You may only enter a number from 1 to 9 or an N in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else if (Information.IsNumeric(vs1.EditText) && Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number from 1 to 9 or an N in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
						if (vs1.EditText == "n")
						{
							vs1.EditText = "N";
						}
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(row) == 1)
					{
						if (vs1.TextMatrix(row, FundCol) != "")
						{
							vs1.EditText = "N";
							vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
						}
					}
					else
					{
						if (vs1.TextMatrix(row, AcctCol) != "")
						{
							vs1.EditText = "N";
							vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
						}
					}
				}
			}
			if (col == AccountTypeCol)
			{
				vs1.TextMatrix(row, col, vs1.EditText);
			}
			if (modAccountTitle.Statics.YearFlag)
			{
				// do nothing
			}
			else
			{
				if (col == YearCol)
				{
					vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
					if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
					{
						MessageBox.Show("You May Only Enter a Number in This Field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						// highlight the field
						vs1.EditSelLength = vs1.EditText.Length;
						return;
					}
					else if (vs1.EditText == "")
					{
						//vs1.EditText = "00";
					}
					if (vs1.EditText.Length > 0)
					{
						// if there is something in that field
						if (Strings.UCase(vs1.EditText) != "AL")
						{
							vs1.EditText = Strings.Format(FCConvert.ToString(Conversion.Val(vs1.EditText)), "00");
						}
						else
						{
							vs1.EditText = Strings.UCase(vs1.EditText);
						}
					}
					if (vs1.EditText.Length == YearLength && YearLength != 0)
					{
						// if there is something in the field
						counter = row - 1;
						//FC: FINAL: BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
						//while (vs1.RowOutlineLevel(counter) != 0)
						while (vs1.RowOutlineLevel(counter) != 1)
						{
							counter -= 1;
						}
						if (row == vs1.Rows - 1)
						{
							counter2 = row;
						}
						else
						{
							counter2 = row + 1;
						}
						//FC: FINAL: BBE:#i644 - RowOutlineLevel index is 1 based can't be 0 
						//while (vs1.RowOutlineLevel(counter2) != 0)
						while (vs1.RowOutlineLevel(counter2) != 1)
						{
							counter2 += 1;
							if (counter2 == vs1.Rows)
							{
								counter2 -= 1;
								break;
							}
						}
						for (counter = counter; counter <= counter2 - 1; counter++)
						{
							// check to see if it is a duplicate division number
							if (counter == row)
							{
								// do nothing
							}
							else
							{
								if (vs1.EditText == vs1.TextMatrix(counter, col) && vs1.TextMatrix(row, AcctCol) == vs1.TextMatrix(counter, AcctCol))
								{
									// if there is a match give an error
									MessageBox.Show("This Account Number and Suffix is Not Allowable Because it Matches a Previously Existing Account Number and Suffix", "Invalid Division Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									// cancel their action
									vs1.EditSelStart = 0;
									// highlight the field
									vs1.EditSelLength = vs1.EditText.Length;
									return;
								}
							}
						}
					}
				}
			}
		}

		private void SaveAccountMasterInfo()
		{
			clsDRWrapper rs3 = new clsDRWrapper();
			bool flag = false;
			rs3.OmitNullsOnInsert = true;
			if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)) == 0)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			rs3.OpenRecordset("SELECT * FROM AccountMaster WHERE ID = 0");
			rs3.AddNew();
			rs3.Set_Fields("AccountType", "G");
			rs3.Set_Fields("DateCreated", DateTime.Today);
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			rs3.Set_Fields("FirstAccountField", rs.Get_Fields("Fund"));
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			rs3.Set_Fields("SecondAccountField", rs.Get_Fields("account"));
			if (!flag)
			{
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				rs3.Set_Fields("ThirdAccountField", rs.Get_Fields("Year"));
			}
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			rs3.Set_Fields("Account", modBudgetaryMaster.CreateAccount_2186("G", rs.Get_Fields("Fund"), rs.Get_Fields("account"), rs.Get_Fields("Year"), "", "", false));
			rs3.Set_Fields("Valid", 0);
			if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				rs3.Set_Fields("GLAccountType", rs.Get_Fields_String("GLAccountType"));
			}
			rs3.Update();
		}

		private void UpdateAccountMasterInfo()
		{
			clsDRWrapper rs3 = new clsDRWrapper();
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			rs3.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + modBudgetaryMaster.CreateAccount_2186("G", rs.Get_Fields("Fund"), rs.Get_Fields("account"), rs.Get_Fields("Year"), "", "", false) + "'");
			if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
			{
				rs3.Edit();
				if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
				{
					rs3.Set_Fields("GLAccountType", rs.Get_Fields_String("GLAccountType"));
				}
				rs3.Update();
			}
			else
			{
				SaveAccountMasterInfo();
			}
		}

		private bool CheckLedgerUsed(ref string strLedger)
		{
			bool CheckLedgerUsed = false;
			clsDRWrapper rs = new clsDRWrapper();
			string strAcct = "";
			int counter;
			CheckLedgerUsed = false;
			for (counter = vs1.Row; counter >= 1; counter--)
			{
				if (vs1.TextMatrix(counter, FundCol) != "")
				{
					strAcct = "G " + Strings.Left(vs1.TextMatrix(counter, FundCol), FundLength);
					strAcct += "-" + strLedger;
					break;
				}
			}
			rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE Account = '" + strAcct + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckLedgerUsed = true;
				return CheckLedgerUsed;
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + strAcct + "' AND CurrentBudget <> 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckLedgerUsed = true;
				return CheckLedgerUsed;
			}
			rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE Account = '" + strAcct + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckLedgerUsed = true;
				return CheckLedgerUsed;
			}
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE Account = '" + strAcct + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckLedgerUsed = true;
				return CheckLedgerUsed;
			}
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Account = '" + strAcct + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckLedgerUsed = true;
				return CheckLedgerUsed;
			}
			return CheckLedgerUsed;
		}

		private bool CheckFundUsed_2(string strFund)
		{
			return CheckFundUsed(ref strFund);
		}

		private bool CheckFundUsed(ref string strFund)
		{
			bool CheckFundUsed = false;
			clsDRWrapper rs = new clsDRWrapper();
			CheckFundUsed = false;
			rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Fund = '" + strFund + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckFundUsed = true;
				return CheckFundUsed;
			}
			rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(FundLength) + ") = '" + strFund + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckFundUsed = true;
				return CheckFundUsed;
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(FundLength) + ") = '" + strFund + "' AND CurrentBudget <> 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckFundUsed = true;
				return CheckFundUsed;
			}
			rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(FundLength) + ") = '" + strFund + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckFundUsed = true;
				return CheckFundUsed;
			}
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(FundLength) + ") = '" + strFund + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckFundUsed = true;
				return CheckFundUsed;
			}
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(FundLength) + ") = '" + strFund + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckFundUsed = true;
				return CheckFundUsed;
			}
			return CheckFundUsed;
		}

		private void SaveInfo()
		{
			int counter;
			bool flag = false;
			string tempFund = "";
			clsDRWrapper rsTesting = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				vs1.Select(0, 1);
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (Conversion.Val(vs1.TextMatrix(counter, FundCol)) == 0)
						{
							MessageBox.Show("There must be a fund number for each fund before you can save.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
						{
							MessageBox.Show("There must be a short description for each fund before you can save.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (modAccountTitle.Statics.YearFlag)
						{
							if (vs1.TextMatrix(counter, AcctCol) != "")
							{
								if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
								{
									flag = true;
									break;
								}
							}
						}
						else
						{
							if (vs1.TextMatrix(counter, AcctCol) != "" && vs1.TextMatrix(counter, YearCol) != "")
							{
								if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
								{
									flag = true;
									break;
								}
							}
						}
					}
				}
				if (flag)
				{
					MessageBox.Show("There must be a short description for every account before you can save", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.Refresh();
				counter = 0;
				while (DeletedFunds[counter] != "")
				{
					//Application.DoEvents();
					rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + DeletedFunds[counter] + "'");
					while (rs.EndOfFile() != true)
					{
						//Application.DoEvents();
						rs.Delete();
						rs.Update();
					}
					counter += 1;
				}
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) == 0)
						{
							// is this a new record
							rsTesting.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Strings.Mid(vs1.TextMatrix(counter, FundCol), 1, FundLength) + "' AND Account = '" + modValidateAccount.GetFormat("0", ref AcctLength) + "'");
							if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
							{
								MessageBox.Show("Saving this information would create duplicate entries for fund " + Strings.Mid(vs1.TextMatrix(counter, FundCol), 1, FundLength) + ".", "Duplicate Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								frmWait.InstancePtr.Unload();
								return;
							}
							rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE ID = 0");
							rs.AddNew();
							// if so add it
							tempFund = Strings.Mid(vs1.TextMatrix(counter, FundCol), 1, FundLength);
							rs.Set_Fields("Fund", tempFund);
							rs.Set_Fields("account", modValidateAccount.GetFormat("0", ref AcctLength));
							rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
							if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
							}
							else
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
							}
							rs.Update();
							// update the database
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
							rs.Edit();
							// if not a new record edit the existing record
							tempFund = Strings.Mid(vs1.TextMatrix(counter, FundCol), 1, FundLength);
							rs.Set_Fields("Fund", tempFund);
							rs.Set_Fields("account", modValidateAccount.GetFormat("0", ref AcctLength));
							rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
							if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
							}
							else
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
							}
							rs.Update();
							// update the database
						}
					}
					else
					{
						if (vs1.TextMatrix(counter, AcctCol) == "")
						{
							// if the row contains no data
							// do nothing
						}
						else
						{
							if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) == 0)
							{
								// is this a new record
								if (!StaticSettings.gGlobalBudgetaryAccountSettings.SuffixExistsInLedger())
                                {
									rsTesting.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + tempFund + "' AND Account = '" + vs1.TextMatrix(counter, AcctCol) + "'");
								}
								else
								{
									rsTesting.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + tempFund + "' AND Account = '" + vs1.TextMatrix(counter, AcctCol) + "' AND [Year] = '" + vs1.TextMatrix(counter, YearCol) + "'");
								}
								if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
								{
									MessageBox.Show("Saving this information would create duplicate entries for account " + tempFund + "-" + vs1.TextMatrix(counter, AcctCol) + ".", "Duplicate Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									frmWait.InstancePtr.Unload();
									return;
								}
								rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE ID = 0");
								rs.AddNew();
								// if so add it
								rs.Set_Fields("Fund", tempFund);
								rs.Set_Fields("account", vs1.TextMatrix(counter, AcctCol));
								// put information in the database
								rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
								if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
								{
									rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
								}
								else
								{
									rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
								}
								if (modAccountTitle.Statics.YearFlag)
								{
									rs.Set_Fields("account", vs1.TextMatrix(counter, AcctCol));
								}
								else
								{
									if (Strings.InStr(1, vs1.TextMatrix(counter, AcctCol), "-", CompareConstants.vbBinaryCompare) != 0)
									{
										rs.Set_Fields("account", Strings.Left(vs1.TextMatrix(counter, AcctCol), vs1.TextMatrix(counter, AcctCol).Length - 2));
									}
									rs.Set_Fields("Year", vs1.TextMatrix(counter, YearCol));
								}
								if (vs1.TextMatrix(counter, TaxCol) != "")
								{
									rs.Set_Fields("Tax", vs1.TextMatrix(counter, TaxCol));
								}
								else
								{
									rs.Set_Fields("Tax", "N");
								}
								if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
								{
									rs.Set_Fields("GLAccountType", vs1.TextMatrix(counter, AccountTypeCol));
								}
								SaveAccountMasterInfo();
								rs.Update();
								// update the database
								vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
							}
							else
							{
								rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
								rs.Edit();
								// if not a new record edit the existing record
								rs.Set_Fields("Fund", tempFund);
								rs.Set_Fields("account", vs1.TextMatrix(counter, AcctCol));
								// put information in the database
								rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
								if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
								{
									rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
								}
								else
								{
									rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
								}
								if (modAccountTitle.Statics.YearFlag)
								{
									// do nothing
								}
								else
								{
									rs.Set_Fields("Year", vs1.TextMatrix(counter, YearCol));
								}
								if (vs1.TextMatrix(counter, TaxCol) != "")
								{
									rs.Set_Fields("Tax", vs1.TextMatrix(counter, TaxCol));
								}
								else
								{
									rs.Set_Fields("Tax", "N");
								}
								if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
								{
									rs.Set_Fields("GLAccountType", vs1.TextMatrix(counter, AccountTypeCol));
								}
								if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
								{
									UpdateAccountMasterInfo();
								}
								// SaveAccountMasterInfo
								rs.Update();
								// update the database
							}
						}
					}
				}
				frmWait.InstancePtr.Unload();
				blnDirty = false;
				//Application.DoEvents();
				MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				if (blnUnload)
				{
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + Information.Err(ex).Description);
			}
		}
	}
}
