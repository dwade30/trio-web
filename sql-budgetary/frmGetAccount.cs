﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetAccount.
	/// </summary>
	public partial class frmGetAccount : BaseForm
	{
		public frmGetAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetAccount InstancePtr
		{
			get
			{
				return (frmGetAccount)Sys.GetInstance(typeof(frmGetAccount));
			}
		}

		protected frmGetAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool OKFlag;

		public void StartProgram()
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			//Application.DoEvents();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			modBudgetaryMaster.Statics.lngCurrentAccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
			// initialize the current account number
			CheckAgain:
			;
			rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				//FC:FINAL:SBE - #486 - force execution of Form_Load event. In VB6 it is executed when first property of the form is accessed
				frmVendorMaster.InstancePtr.LoadForm();
				frmVendorMaster.InstancePtr.chkAddress.CheckState = (rs.Get_Fields_Boolean("Same") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				//Application.DoEvents();
				frmVendorMaster.InstancePtr.lblRecordNumber.Text = Strings.Format(rs.Get_Fields_Int32("VendorNumber"), "00000");
				frmVendorMaster.InstancePtr.txtCheckName.Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
				frmVendorMaster.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
				frmVendorMaster.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
				// initialize the VendorMaster form with the data from that record
				frmVendorMaster.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
				frmVendorMaster.InstancePtr.txtCheckCity.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckCity")));
				frmVendorMaster.InstancePtr.txtCheckState.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckState")));
				frmVendorMaster.InstancePtr.txtCheckZip.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip")));
				frmVendorMaster.InstancePtr.txtCheckZip4.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4")));
				frmVendorMaster.InstancePtr.txtCorrespondCity.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CorrespondCity")));
				frmVendorMaster.InstancePtr.txtCorrespondState.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CorrespondState")));
				frmVendorMaster.InstancePtr.txtCorrespondZip.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CorrespondZip")));
				frmVendorMaster.InstancePtr.txtCorrespondZip4.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CorrespondZip4")));
				frmVendorMaster.InstancePtr.txtCorName.Text = FCConvert.ToString(rs.Get_Fields_String("CorrespondName"));
				frmVendorMaster.InstancePtr.txtCorAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CorrespondAddress1"));
				frmVendorMaster.InstancePtr.txtCorAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CorrespondAddress2"));
				frmVendorMaster.InstancePtr.txtCorAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CorrespondAddress3"));
				frmVendorMaster.InstancePtr.txtContact.Text = FCConvert.ToString(rs.Get_Fields_String("Contact"));
				frmVendorMaster.InstancePtr.FillAccountTypeCombo();
				if (rs.Get_Fields_Boolean("EFT") == true)
				{
					frmVendorMaster.InstancePtr.chkEFT.CheckState = CheckState.Checked;
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Prenote")))
					{
						frmVendorMaster.InstancePtr.chkPrenote.CheckState = CheckState.Checked;
					}
					else
					{
						frmVendorMaster.InstancePtr.chkPrenote.CheckState = CheckState.Unchecked;
					}
					if (FCConvert.ToString(rs.Get_Fields_String("AccountType")) == "S")
					{
						frmVendorMaster.InstancePtr.cboAccountType.SelectedIndex = 1;
					}
					else
					{
						frmVendorMaster.InstancePtr.cboAccountType.SelectedIndex = 0;
					}
					frmVendorMaster.InstancePtr.txtRoutingNumber.Text = FCConvert.ToString(rs.Get_Fields_String("RoutingNumber"));
					frmVendorMaster.InstancePtr.txtBankAccountNumber.Text = FCConvert.ToString(rs.Get_Fields_String("BankAccountNumber"));
				}
				else
				{
					frmVendorMaster.InstancePtr.chkEFT.CheckState = CheckState.Unchecked;
					frmVendorMaster.InstancePtr.chkPrenote.CheckState = CheckState.Checked;
					frmVendorMaster.InstancePtr.cboAccountType.SelectedIndex = -1;
					frmVendorMaster.InstancePtr.txtRoutingNumber.Text = "";
					frmVendorMaster.InstancePtr.txtBankAccountNumber.Text = "";
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("InsuranceRequired")))
				{
					frmVendorMaster.InstancePtr.chkInsuranceRequired.CheckState = CheckState.Checked;
					if (Information.IsDate(rs.Get_Fields("InsuranceVerifiedDate")))
					{
						frmVendorMaster.InstancePtr.txtInsuranceVerifiedDate.Text = Strings.Format(rs.Get_Fields_DateTime("InsuranceVerifiedDate"), "MM/dd/yyyy");
					}
					else
					{
						frmVendorMaster.InstancePtr.txtInsuranceVerifiedDate.Text = "";
					}
				}
				else
				{
					frmVendorMaster.InstancePtr.chkInsuranceRequired.CheckState = CheckState.Unchecked;
					frmVendorMaster.InstancePtr.txtInsuranceVerifiedDate.Text = "";
				}
				frmVendorMaster.InstancePtr.txtPhone.Text = Strings.Format(rs.Get_Fields_String("TelephoneNumber"), "(000)###-####");
				frmVendorMaster.InstancePtr.txtFax.Text = Strings.Format(rs.Get_Fields_String("FaxNumber"), "(000)###-####");
				frmVendorMaster.InstancePtr.txtExtension.Text = FCConvert.ToString(rs.Get_Fields_String("Extension"));
				frmVendorMaster.InstancePtr.txtStatus.Text = FCConvert.ToString(rs.Get_Fields_String("Status"));
				frmVendorMaster.InstancePtr.txtClass.Text = FCConvert.ToString(rs.Get_Fields_String("Class"));
				// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
				frmVendorMaster.InstancePtr.chk1099.CheckState = (rs.Get_Fields("1099") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				frmVendorMaster.InstancePtr.chkIncludeAllPayments.CheckState = (rs.Get_Fields_Boolean("IncludeAllPaymentsFor1099") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				frmVendorMaster.InstancePtr.Set1099CodeCombo(FCConvert.ToString(rs.Get_Fields_String("1099Code")));
				frmVendorMaster.InstancePtr.chkAttorney.CheckState = (rs.Get_Fields_Boolean("Attorney") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				frmVendorMaster.InstancePtr.txtTaxNumber.Text = FCConvert.ToString(rs.Get_Fields_String("TaxNumber"));
				frmVendorMaster.InstancePtr.txtEmail.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Email")));
				if (rs.Get_Fields_Boolean("1099UseCorrName") == true)
				{
					frmVendorMaster.InstancePtr.cmbCorrespondenceName.SelectedIndex = 0;
				}
				else
				{
					frmVendorMaster.InstancePtr.cmbCorrespondenceName.SelectedIndex = 1;
				}
				frmVendorMaster.InstancePtr.txtMessage.Text = FCConvert.ToString(rs.Get_Fields_String("Message"));
				frmVendorMaster.InstancePtr.chkOneTime.CheckState = (rs.Get_Fields_Boolean("OneTimeCheckMessage") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				frmVendorMaster.InstancePtr.txtCheckMessage.Text = FCConvert.ToString(rs.Get_Fields_String("CheckMessage"));
				frmVendorMaster.InstancePtr.txtDataMessage.Text = FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage"));
				frmVendorMaster.InstancePtr.txtDefaultDescription.Text = FCConvert.ToString(rs.Get_Fields_String("DefaultDescription"));
				frmVendorMaster.InstancePtr.txtDefaultCheckNumber.Text = FCConvert.ToString(rs.Get_Fields_String("DefaultCheckNumber"));
				if (!rs.IsFieldNull("LastPayment"))
					frmVendorMaster.InstancePtr.lblLastPaymentDate.Text = Strings.Format(rs.Get_Fields_DateTime("LastPayment"), "MM/dd/yyyy");
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rs.Get_Fields("CheckNumber")) != "")
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					frmVendorMaster.InstancePtr.lblCheckNumber.Text = FCConvert.ToString(rs.Get_Fields("CheckNumber"));
				GetAdjustments();
				// put adjustment data in the VendorMaster form
				GetDefaults();
				this.Unload();
				// put default information into the VendorMaster form
				frmVendorMaster.InstancePtr.Show(App.MainForm);
				// show the form
				modRegistry.SaveRegistryKey("CURRVNDR", FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount));
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				if (MessageBox.Show("No vendor found with this number.  Would you like to create a new vendor using this as the vendor number?", "Create Vendor?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsCheckInfo.OpenRecordset("SELECT * FROM VendorMaster");
					rsCheckInfo.AddNew();
					rsCheckInfo.Set_Fields("VendorNumber", modBudgetaryMaster.Statics.lngCurrentAccount);
					rsCheckInfo.Update(false);
					goto CheckAgain;
				}
				else
				{
					return;
				}
				// rsCheckInfo.OpenRecordset "SELECT TOP 1 * FROM VendorMaster ORDER BY VendorNumber DESC"
				// If rsCheckInfo.EndOfFile <> True And rsCheckInfo.BeginningOfFile <> True Then
				// If rsCheckInfo.Fields["VendorNumber"] > lngCurrentAccount Then
				// rsCheckInfo.AddNew
				// rsCheckInfo.Fields["VendorNumber"] = lngCurrentAccount
				// rsCheckInfo.Update False
				// GoTo CheckAgain
				// Else
				// Unload frmWait     'get rid of the wait form
				// MsgBox "No Vendor Found", vbExclamation, "Non-Existent Vendor"
				// Exit Sub
				// End If
				// End If
				// Unload frmWait     'get rid of the wait form
				// MsgBox "No Vendor Found", vbExclamation, "Non-Existent Vendor"
				// Exit Sub
			}
			//Close();
			frmWait.InstancePtr.Unload();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
        {
            //FC:FINAL:CHN: Incorrect code migration.
            //optHidden.Checked = true;
            // cmbSearchType.SelectedIndex = 3;
            cmbSearchType.Text = "";
            // clear the search info
            txtSearch.Text = "";
			vs1.Clear();
			vs1.TextMatrix(0, 0, "Record");
			vs1.TextMatrix(0, 1, "Name");
			vs1.TextMatrix(0, 2, "Address");
			vs1.TextMatrix(0, 3, "Contact");
			vs1.TextMatrix(0, 4, "Tax ID#");
			//FC:FINAL:DSE:#455 Grid design should match the other search grids
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.Rows - 1, 4, 8);
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			// make the list of records invisible
			Frame3.Visible = false;
			groupBox1.Visible = !Frame3.Visible;
			//FC:FINAL:BBE:#450 - set toolbar button visibility correctly
			cmdClear.Visible = !Frame3.Visible;
			cmdSearch.Visible = !Frame3.Visible;
			int intAccount;
			intAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
			// get the reocrd number
			if (intAccount != 0)
			{
				// if it is a valid account number
				modBudgetaryMaster.Statics.lngCurrentAccount = intAccount;
				// gets the Account Number for the item that is double clicked
				txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
				cmdGetAccountNumber_Click();
				// get the record
			}
			// clear the search
			cmdClear_Click();
		}

		public void cmdGet_Click()
		{
			cmdGet_Click(cmdGet, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtGetAccountNumber.Text) != 0)
			{
				// if there is a valid account number
				modBudgetaryMaster.Statics.lngCurrentAccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
				// set the account number to the one last used
				modBudgetaryMaster.Statics.blnEdit = true;
				// set the edit flag
				StartProgram();
				// call the procedure to retrieve the info
			}
			else
			{
				modBudgetaryMaster.Statics.blnEdit = false;
				// else show we are creating a new vendor account
				frmVendorMaster.InstancePtr.Show(App.MainForm);
				// show the blankform
				Close();
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			frmGetAccount.InstancePtr.Close(); // unload this form
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			Frame3.Visible = false;
			groupBox1.Visible = !Frame3.Visible;
			//FC:FINAL:BBE:#450 - set toolbar button visibility correctly
			cmdClear.Visible = !Frame3.Visible;
			cmdSearch.Visible = !Frame3.Visible;
			// make the listbox invisible
			cmdClear_Click();
			// clear the search
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string resp1 = "";
			string resp2 = "";
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (cmbSearchType.SelectedIndex != 0)
			{
				// make sure a search type is clicked
				if (cmbSearchType.SelectedIndex != 1)
				{
					if (cmbSearchType.SelectedIndex != 2)
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("You must Choose a Search Type (Name, Contact or Tax ID Number)", "Search Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						resp1 = "TaxID";
						// remember which type of search we are doing
					}
				}
				else
				{
					resp1 = "Contact";
				}
			}
			else
			{
				resp1 = "Name";
			}
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				resp2 = txtSearch.Text;
				// remember the search criteria
			}
			if (resp1 == "TaxID")
			{
				// do the right kind of search
				rs.OpenRecordset("SELECT CheckName as VendorName, * FROM VendorMaster WHERE TaxNumber like '" + resp2 + "%' ORDER BY TaxNumber");
			}
			else if (resp1 == "Contact")
			{
				rs.OpenRecordset("SELECT CheckName as VendorName, * FROM VendorMaster WHERE Contact like '" + resp2 + "%' ORDER BY Contact");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM (SELECT VendorNumber, CheckName as VendorName, Contact, TaxNumber, Status, CheckAddress1, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4 FROM VendorMaster WHERE CheckName like '" + modCustomReport.FixQuotes(resp2) + "%' UNION ALL SELECT VendorNumber, CorrespondName as VendorName, Contact, TaxNumber, Status, CheckAddress1, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4 FROM VendorMaster WHERE CorrespondName like '" + modCustomReport.FixQuotes(resp2) + "%' AND Same = 0 UNION ALL SELECT VendorNumber, CheckAddress1 as VendorName, Contact, TaxNumber, Status, CheckAddress1, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4 FROM VendorMaster WHERE upper(left(CheckAddress1, " + FCConvert.ToString(1 + resp2.Length) + ")) = '*" + modCustomReport.FixQuotes(Strings.UCase(resp2)) + "' AND upper(left(CheckName, " + FCConvert.ToString(resp2.Length) + ")) <> '" + modCustomReport.FixQuotes(Strings.UCase(resp2)) + "') as temp ORDER BY VendorName");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record
				rs.MoveLast();
				rs.MoveFirst();
				frmWait.InstancePtr.Unload();
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					cmdClear_Click();
					// clear the search
					txtGetAccountNumber.Text = FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber"));
					// retrieve the info
					modBudgetaryMaster.Statics.lngCurrentAccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
					// save the account number
					cmdGetAccountNumber_Click();
				}
				else
				{
					Fill_List(ref resp1);
					// else show the user all possible records
					frmWait.InstancePtr.Unload();
					Frame3.Visible = true;
					groupBox1.Visible = !Frame3.Visible;
					//FC:FINAL:BBE:#450 - set toolbar button visibility correctly
					cmdClear.Visible = !Frame3.Visible;
					cmdSearch.Visible = !Frame3.Visible;
					// make the list of records visible
					vs1.Select(1, vs1.Cols - 1, 1, 0);
					vs1.Focus();
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Records Found That Match The Criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetAccount_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			modBudgetaryMaster.Statics.lngCurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRVNDR"))));
			txtGetAccountNumber.Text = FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount);
			txtGetAccountNumber.Focus();
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
			lblLastAccount.Text = FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount);
			vs1.TextMatrix(0, 0, "Record");
			vs1.TextMatrix(0, 1, "Name");
			vs1.TextMatrix(0, 2, "Address");
			vs1.TextMatrix(0, 3, "Contact");
			vs1.TextMatrix(0, 4, "Tax ID#");
			//FC:FINAL:DSE:#455 Grid design should match the other search grids
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.Rows - 1, vs1.Cols - 1, 8);
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.4));
			vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, 4);
			vs1.ColDataType(4, FCGrid.DataTypeSettings.flexDTString);
		}

		private void frmGetAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Up)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{UP}", false);
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{DOWN}", false);
				}
			}
		}

		private void frmGetAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				//Application.DoEvents();
				if (frmGetAccount.InstancePtr.Visible == true)
				{
					if (frmGetAccount.InstancePtr.ActiveControl.GetName() == "vs1")
					{
						KeyAscii = (Keys)0;
						cmdGet_Click();
					}
					else
					{
						KeyAscii = (Keys)0;
						if (frmGetAccount.InstancePtr.ActiveControl.GetName() == "txtGetAccountNumber")
						{
							cmdGetAccountNumber_Click();
							return;
						}
						if (frmGetAccount.InstancePtr.ActiveControl.GetName() == "optSearchType")
						{
							OKFlag = true;
							optSearchType_Click(FCConvert.ToInt16(frmGetAccount.InstancePtr.ActiveControl.GetIndex()));
						}
						if (frmGetAccount.InstancePtr.ActiveControl.GetName() == "txtSearch")
							cmdSearch_Click();
					}
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				if (Frame3.Visible == true)
				{
					cmdClear_Click();
					Frame3.Visible = false;
					groupBox1.Visible = !Frame3.Visible;
					//FC:FINAL:BBE:#450 - set toolbar button visibility correctly
					cmdClear.Visible = !Frame3.Visible;
					cmdSearch.Visible = !Frame3.Visible;
				}
				else
				{
					frmGetAccount.InstancePtr.Close();
				}
			}
			else if (KeyAscii == Keys.F13)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void GetAdjustments()
		{
			rs.OpenRecordset("SELECT * FROM Adjustments WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there are any adjustments for the record we are retrieving
				rs.MoveLast();
				rs.MoveFirst();
				while (rs.EndOfFile() != true)
				{
					// get each adjustment there is
					frmVendorMaster.InstancePtr.txtAdjustment[rs.Get_Fields_Int32("AdjustmentNumber") - 1].Text = Strings.Format(rs.Get_Fields_Decimal("Adjustment"), "#,##0.00");
					frmVendorMaster.InstancePtr.lblPosNegValue[FCConvert.ToInt16(rs.Get_Fields_Int32("AdjustmentNumber") - 1)].Text = (rs.Get_Fields_Boolean("Positive") == true ? "+" : "-");
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					frmVendorMaster.InstancePtr.cboCode[FCConvert.ToInt16(rs.Get_Fields_Int32("AdjustmentNumber") - 1)].SelectedIndex = FCConvert.ToInt32(rs.Get_Fields("Code"));
					rs.MoveNext();
					// go to the next adjustment
				}
			}
		}

		private void GetDefaults()
		{
			rs.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is any default information
				rs.MoveLast();
				rs.MoveFirst();
				while (rs.EndOfFile() != true)
				{
					// get all the default information there is
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					frmVendorMaster.InstancePtr.vs1.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int32("DescriptionNumber")), 1, FCConvert.ToString(rs.Get_Fields("AccountNumber")));
					frmVendorMaster.InstancePtr.vs1.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int32("DescriptionNumber")), 2, FCConvert.ToString(rs.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
					frmVendorMaster.InstancePtr.vs1.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int32("DescriptionNumber")), 3, FCConvert.ToString(rs.Get_Fields("Tax")));
					if (!String.IsNullOrEmpty(rs.Get_Fields_String("Project")))
						frmVendorMaster.InstancePtr.vs1.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int32("DescriptionNumber")), 4, FCConvert.ToString(rs.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					frmVendorMaster.InstancePtr.vs1.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int32("DescriptionNumber")), 5, FCConvert.ToString(rs.Get_Fields("Amount")));
					rs.MoveNext();
				}
			}
		}

		private void Fill_List(ref string x)
		{
			int I;
			string strAddressLine = "";
			vs1.Rows = rs.RecordCount() + 1;
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				vs1.TextMatrix(I, 0, modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")), 5));
				vs1.TextMatrix(I, 1, FCConvert.ToString(rs.Get_Fields_String("VendorName")));
				vs1.TextMatrix(I, 2, "");
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"))) != "")
				{
					vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"))));
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"))) != "")
				{
					if (Strings.Trim(vs1.TextMatrix(I, 2)) == "")
					{
						vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"))));
					}
					else
					{
						vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + "\r\n" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"))));
					}
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"))) != "")
				{
					if (Strings.Trim(vs1.TextMatrix(I, 2)) == "")
					{
						vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"))));
					}
					else
					{
						vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + "\r\n" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"))));
					}
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckCity"))) != "" || Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckState"))) != "" || Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip"))) != "" || Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckState"))) != "")
					{
						strAddressLine = Strings.Trim(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckCity"))) + ", " + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckState")))) + " ";
					}
					else
					{
						strAddressLine = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckCity"))) + " ";
					}
					if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip"))) != "" && Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4"))) != "")
					{
						strAddressLine += Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4")));
					}
					else if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip"))) != "")
					{
						strAddressLine += Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip")));
					}
					if (Strings.Trim(vs1.TextMatrix(I, 2)) == "")
					{
						vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + strAddressLine);
					}
					else
					{
						vs1.TextMatrix(I, 2, vs1.TextMatrix(I, 2) + "\r\n" + strAddressLine);
					}
				}
				vs1.TextMatrix(I, 3, FCConvert.ToString(rs.Get_Fields_String("Contact")));
				vs1.TextMatrix(I, 4, FCConvert.ToString(rs.Get_Fields_String("TaxNumber")));
				if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "D")
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, I, 0, I, vs1.Cols - 1, Color.Red);
				}
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
			//FC:FINAL:DSE:#455 Grid design should match the other search grids
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.Rows - 1, vs1.Cols - 1, 8);
			vs1.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			vs1.AutoSize(0, vs1.Cols - 1);
		}

		private void frmGetAccount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetAccount.ScaleWidth	= 9045;
			//frmGetAccount.ScaleHeight	= 7275;
			//frmGetAccount.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmGetAccount_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.4));
			vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void optSearchType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (frmGetAccount.InstancePtr.Visible == true)
			{
				if (OKFlag)
				{
					txtSearch.Focus();
					OKFlag = false;
				}
			}
		}

		public void optSearchType_Click(short Index)
		{
			optSearchType_CheckedChanged(Index, cmbSearchType, new System.EventArgs());
		}

		private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_CheckedChanged(index, sender, e);
		}

		private void txtSearch_Enter(object sender, System.EventArgs e)
		{
			if (cmbSearchType.SelectedIndex != 0 && cmbSearchType.SelectedIndex != 1 && cmbSearchType.SelectedIndex != 2)
			{
				cmbSearchType.SelectedIndex = 0;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			EditFlag = true;
			vs1.Select(vs1.Row, vs1.Cols - 1, vs1.Row, 0);
			EditFlag = false;
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			// make the listbox invisible
			Frame3.Visible = false;
			groupBox1.Visible = !Frame3.Visible;
			//FC:FINAL:BBE:#450 - set toolbar button visibility correctly
			cmdClear.Visible = !Frame3.Visible;
			cmdSearch.Visible = !Frame3.Visible;
			int intAccount;
			// if there is a record selected
			intAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
			// get the reocrd number
			if (intAccount != 0)
			{
				// if it is a valid account number
				modBudgetaryMaster.Statics.lngCurrentAccount = intAccount;
				// gets the Account Number for the item that is double clicked
				txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
				cmdGetAccountNumber_Click();
				// get the record
			}
			// clear the search
			cmdClear_Click();
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			int keyAscii = Strings.Asc(e.KeyChar);
			// if there is a record selected
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modBudgetaryMaster.Statics.lngCurrentAccount = intAccount;
					// gets the Account Number for the item that is double clicked
					txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
					cmdGetAccountNumber_Click();
					// get the record
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				groupBox1.Visible = !Frame3.Visible;
				//FC:FINAL:BBE:#450 - set toolbar button visibility correctly
				cmdClear.Visible = !Frame3.Visible;
				cmdSearch.Visible = !Frame3.Visible;
				// make the listbox invisible
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				EditFlag = true;
				vs1.Select(vs1.Row, vs1.Cols - 1, vs1.Row, 0);
				EditFlag = false;
			}
		}

		private void optSearchType_MouseDown(int Index, object sender, System.EventArgs e)
		{
			//FC:TODO
			//MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			//int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			//float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
			//float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
			//if (Button == MouseButtonConstants.LeftButton && Shift == 0)
			//{
			//    OKFlag = true;
			//}
		}

		private void optSearchType_MouseDown(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_MouseDown(index, sender, e);
		}

		private void SetCustomFormColors()
		{
			Label3.BackColor = Color.Red;
		}
	}
}
