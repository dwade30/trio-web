﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurgeSelected.
	/// </summary>
	partial class frmPurgeSelected : BaseForm
	{
		public Global.T2KDateBox txtPurgeDate;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeSelected));
			this.txtPurgeDate = new Global.T2KDateBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdExpenseAdd = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPurgeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExpenseAdd)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 280);
			this.BottomPanel.Size = new System.Drawing.Size(648, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdExpenseAdd);
			this.ClientArea.Controls.Add(this.txtPurgeDate);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(648, 220);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(648, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(180, 30);
			this.HeaderText.Text = "Purge Selected";
			// 
			// txtPurgeDate
			// 
			this.txtPurgeDate.Location = new System.Drawing.Point(30, 66);
			this.txtPurgeDate.MaxLength = 10;
			this.txtPurgeDate.Name = "txtPurgeDate";
			this.txtPurgeDate.Size = new System.Drawing.Size(115, 40);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(500, 16);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "SELECT A DATE AND HIT F12.  ALL ITEMS ENTERED BEFORE THAT DATE WILL BE PURGED";
			// 
			// cmdExpenseAdd
			// 
			this.cmdExpenseAdd.AppearanceKey = "acceptButton";
			this.cmdExpenseAdd.Location = new System.Drawing.Point(30, 126);
			this.cmdExpenseAdd.Name = "cmdExpenseAdd";
			this.cmdExpenseAdd.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdExpenseAdd.Size = new System.Drawing.Size(110, 48);
			this.cmdExpenseAdd.TabIndex = 9;
			this.cmdExpenseAdd.Text = "Process";
			this.cmdExpenseAdd.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmPurgeSelected
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(648, 388);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPurgeSelected";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purge Selected";
			this.Load += new System.EventHandler(this.frmPurgeSelected_Load);
			this.Activated += new System.EventHandler(this.frmPurgeSelected_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeSelected_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPurgeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExpenseAdd)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdExpenseAdd;
	}
}
