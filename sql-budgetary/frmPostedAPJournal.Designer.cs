//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPostedAPJournal.
	/// </summary>
	partial class frmPostedAPJournal : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public fecherFoundation.FCFrame fraAccountBalance;
		public fecherFoundation.FCButton cmdBalanceOK;
		public fecherFoundation.FCLabel lblBalPendingYTDNet;
		public fecherFoundation.FCLabel lblBalBalance;
		public fecherFoundation.FCLabel lblBalPostedYTDNet;
		public fecherFoundation.FCLabel lblBalNetBudget;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblBalAccount;
		public fecherFoundation.FCFrame frmSearch;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCFrame frmInfo;
		public fecherFoundation.FCListBox lstRecords;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdRetrieve;
		public fecherFoundation.FCLabel lblVendorName;
		public fecherFoundation.FCLabel lblRecordNumber;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtCheck;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KOverTypeBox txtReference;
		public fecherFoundation.FCFrame fraBorder;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboFrequency;
		public Global.T2KDateBox txtUntil;
		public fecherFoundation.FCLabel lblFrequency;
		public fecherFoundation.FCLabel lblDays;
		public fecherFoundation.FCLabel lblUntil;
		public fecherFoundation.FCCheckBox chkSeperate;
		public Global.T2KDateBox txtPayable;
		public Global.T2KOverTypeBox txtPeriod;
		public fecherFoundation.FCLabel lblJournalNumber;
		public fecherFoundation.FCLabel lblPayable;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblPeriod;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public Global.T2KBackFillDecimal txtAmount2;
		public fecherFoundation.FCTextBox txtAmount;
		public fecherFoundation.FCCheckBox chkUseAltCashAccount;
		public fecherFoundation.FCFrame fraAltCashAccount;
		public FCGrid vsAltCashAccount;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblReadOnly;
		public Wisej.Web.Panel Shape2;
		public Wisej.Web.Panel Shape1;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblActualPayment;
		public fecherFoundation.FCLabel lblRemAmount;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblReference;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblCheck;
		public fecherFoundation.FCLabel lblRemaining;
		public fecherFoundation.FCLabel lblActual;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuViewAttachedDocuments;
		public fecherFoundation.FCToolStripMenuItem mnuFileSep2;
		public fecherFoundation.FCToolStripMenuItem mnuFileDisplayAmount;
		public fecherFoundation.FCToolStripMenuItem mnuFileVendorDetail;
		public fecherFoundation.FCToolStripMenuItem mnuFileSep3;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPostedAPJournal));
			this.fraAccountBalance = new fecherFoundation.FCFrame();
			this.cmdBalanceOK = new fecherFoundation.FCButton();
			this.lblBalPendingYTDNet = new fecherFoundation.FCLabel();
			this.lblBalBalance = new fecherFoundation.FCLabel();
			this.lblBalPostedYTDNet = new fecherFoundation.FCLabel();
			this.lblBalNetBudget = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblBalAccount = new fecherFoundation.FCLabel();
			this.frmSearch = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.frmInfo = new fecherFoundation.FCFrame();
			this.lstRecords = new fecherFoundation.FCListBox();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdRetrieve = new fecherFoundation.FCButton();
			this.lblVendorName = new fecherFoundation.FCLabel();
			this.lblRecordNumber = new fecherFoundation.FCLabel();
			this.txtAddress_0 = new Global.T2KOverTypeBox();
			this.txtVendor = new Global.T2KOverTypeBox();
			this.txtCheck = new Global.T2KOverTypeBox();
			this.txtDescription = new Global.T2KOverTypeBox();
			this.txtReference = new Global.T2KOverTypeBox();
			this.fraBorder = new fecherFoundation.FCFrame();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cboFrequency = new fecherFoundation.FCComboBox();
			this.txtUntil = new Global.T2KDateBox();
			this.lblFrequency = new fecherFoundation.FCLabel();
			this.lblDays = new fecherFoundation.FCLabel();
			this.lblUntil = new fecherFoundation.FCLabel();
			this.chkSeperate = new fecherFoundation.FCCheckBox();
			this.txtPayable = new Global.T2KDateBox();
			this.txtPeriod = new Global.T2KOverTypeBox();
			this.lblJournalNumber = new fecherFoundation.FCLabel();
			this.lblPayable = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.txtAddress_1 = new Global.T2KOverTypeBox();
			this.txtAddress_2 = new Global.T2KOverTypeBox();
			this.txtAddress_3 = new Global.T2KOverTypeBox();
			this.txtAmount2 = new Global.T2KBackFillDecimal();
			this.txtAmount = new fecherFoundation.FCTextBox();
			this.chkUseAltCashAccount = new fecherFoundation.FCCheckBox();
			this.fraAltCashAccount = new fecherFoundation.FCFrame();
			this.vsAltCashAccount = new fecherFoundation.FCGrid();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblReadOnly = new fecherFoundation.FCLabel();
			this.Shape2 = new Wisej.Web.Panel();
			this.Shape1 = new Wisej.Web.Panel();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblActualPayment = new fecherFoundation.FCLabel();
			this.lblRemAmount = new fecherFoundation.FCLabel();
			this.lblVendor = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblReference = new fecherFoundation.FCLabel();
			this.lblAmount = new fecherFoundation.FCLabel();
			this.lblCheck = new fecherFoundation.FCLabel();
			this.lblRemaining = new fecherFoundation.FCLabel();
			this.lblActual = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuViewAttachedDocuments = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileVendorDetail = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSep2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDisplayAmount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSep3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnFileVendorDetail = new fecherFoundation.FCButton();
			this.btnFileDisplayAmount = new fecherFoundation.FCButton();
			this.btnViewAttachedDocuments = new fecherFoundation.FCButton();
			this.cmdFileVendorDetail = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).BeginInit();
			this.fraAccountBalance.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmSearch)).BeginInit();
			this.frmSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmInfo)).BeginInit();
			this.frmInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBorder)).BeginInit();
			this.fraBorder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtUntil)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSeperate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseAltCashAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAltCashAccount)).BeginInit();
			this.fraAltCashAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAltCashAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileVendorDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDisplayAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnViewAttachedDocuments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileVendorDetail)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileVendorDetail);
			this.BottomPanel.Location = new System.Drawing.Point(0, 512);
			this.BottomPanel.Size = new System.Drawing.Size(1218, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAccountBalance);
			this.ClientArea.Controls.Add(this.frmSearch);
			this.ClientArea.Controls.Add(this.frmInfo);
			this.ClientArea.Controls.Add(this.txtAddress_0);
			this.ClientArea.Controls.Add(this.txtVendor);
			this.ClientArea.Controls.Add(this.txtCheck);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtReference);
			this.ClientArea.Controls.Add(this.lblVendor);
			this.ClientArea.Controls.Add(this.fraBorder);
			this.ClientArea.Controls.Add(this.txtAddress_1);
			this.ClientArea.Controls.Add(this.txtAddress_2);
			this.ClientArea.Controls.Add(this.txtAddress_3);
			this.ClientArea.Controls.Add(this.txtAmount2);
			this.ClientArea.Controls.Add(this.txtAmount);
			this.ClientArea.Controls.Add(this.chkUseAltCashAccount);
			this.ClientArea.Controls.Add(this.fraAltCashAccount);
			this.ClientArea.Controls.Add(this.txtZip4);
			this.ClientArea.Controls.Add(this.txtZip);
			this.ClientArea.Controls.Add(this.txtState);
			this.ClientArea.Controls.Add(this.txtCity);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblReadOnly);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Controls.Add(this.lblActualPayment);
			this.ClientArea.Controls.Add(this.lblRemAmount);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Controls.Add(this.lblReference);
			this.ClientArea.Controls.Add(this.lblAmount);
			this.ClientArea.Controls.Add(this.lblCheck);
			this.ClientArea.Controls.Add(this.lblRemaining);
			this.ClientArea.Controls.Add(this.lblActual);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Controls.Add(this.Shape2);
			this.ClientArea.Size = new System.Drawing.Size(1218, 452);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileVendorDetail);
			this.TopPanel.Controls.Add(this.btnViewAttachedDocuments);
			this.TopPanel.Controls.Add(this.btnFileDisplayAmount);
			this.TopPanel.Size = new System.Drawing.Size(1218, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileDisplayAmount, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnViewAttachedDocuments, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileVendorDetail, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(136, 30);
			this.HeaderText.Text = "AP Invoice";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraAccountBalance
			// 
			this.fraAccountBalance.Controls.Add(this.cmdBalanceOK);
			this.fraAccountBalance.Controls.Add(this.lblBalPendingYTDNet);
			this.fraAccountBalance.Controls.Add(this.lblBalBalance);
			this.fraAccountBalance.Controls.Add(this.lblBalPostedYTDNet);
			this.fraAccountBalance.Controls.Add(this.lblBalNetBudget);
			this.fraAccountBalance.Controls.Add(this.Label6);
			this.fraAccountBalance.Controls.Add(this.Label5);
			this.fraAccountBalance.Controls.Add(this.Label4);
			this.fraAccountBalance.Controls.Add(this.Label3);
			this.fraAccountBalance.Controls.Add(this.lblBalAccount);
			this.fraAccountBalance.Location = new System.Drawing.Point(30, 30);
			this.fraAccountBalance.Name = "fraAccountBalance";
			this.fraAccountBalance.Size = new System.Drawing.Size(316, 265);
			this.fraAccountBalance.TabIndex = 52;
			this.fraAccountBalance.Text = "Account Balance";
			this.ToolTip1.SetToolTip(this.fraAccountBalance, null);
			this.fraAccountBalance.Visible = false;
			// 
			// cmdBalanceOK
			// 
			this.cmdBalanceOK.AppearanceKey = "actionButton";
			this.cmdBalanceOK.Location = new System.Drawing.Point(66, 205);
			this.cmdBalanceOK.Name = "cmdBalanceOK";
			this.cmdBalanceOK.Size = new System.Drawing.Size(74, 40);
			this.cmdBalanceOK.TabIndex = 58;
			this.cmdBalanceOK.Text = "OK";
			this.ToolTip1.SetToolTip(this.cmdBalanceOK, null);
			this.cmdBalanceOK.Click += new System.EventHandler(this.cmdBalanceOK_Click);
			// 
			// lblBalPendingYTDNet
			// 
			this.lblBalPendingYTDNet.Location = new System.Drawing.Point(176, 135);
			this.lblBalPendingYTDNet.Name = "lblBalPendingYTDNet";
			this.lblBalPendingYTDNet.Size = new System.Drawing.Size(120, 15);
			this.lblBalPendingYTDNet.TabIndex = 62;
			this.lblBalPendingYTDNet.Text = "PENDING YTD NET";
			this.lblBalPendingYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalPendingYTDNet, null);
			// 
			// lblBalBalance
			// 
			this.lblBalBalance.Location = new System.Drawing.Point(176, 170);
			this.lblBalBalance.Name = "lblBalBalance";
			this.lblBalBalance.Size = new System.Drawing.Size(119, 15);
			this.lblBalBalance.TabIndex = 61;
			this.lblBalBalance.Text = "BALANCE";
			this.lblBalBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalBalance, null);
			// 
			// lblBalPostedYTDNet
			// 
			this.lblBalPostedYTDNet.Location = new System.Drawing.Point(176, 100);
			this.lblBalPostedYTDNet.Name = "lblBalPostedYTDNet";
			this.lblBalPostedYTDNet.Size = new System.Drawing.Size(120, 15);
			this.lblBalPostedYTDNet.TabIndex = 60;
			this.lblBalPostedYTDNet.Text = "POSTED YTD NET";
			this.lblBalPostedYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalPostedYTDNet, null);
			// 
			// lblBalNetBudget
			// 
			this.lblBalNetBudget.Location = new System.Drawing.Point(176, 65);
			this.lblBalNetBudget.Name = "lblBalNetBudget";
			this.lblBalNetBudget.Size = new System.Drawing.Size(116, 15);
			this.lblBalNetBudget.TabIndex = 59;
			this.lblBalNetBudget.Text = "99,999,999.00";
			this.lblBalNetBudget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalNetBudget, null);
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.Location = new System.Drawing.Point(20, 135);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(120, 15);
			this.Label6.TabIndex = 57;
			this.Label6.Text = "PENDING YTD NET";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(75, 170);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(65, 15);
			this.Label5.TabIndex = 56;
			this.Label5.Text = "BALANCE";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.Location = new System.Drawing.Point(26, 100);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(114, 15);
			this.Label4.TabIndex = 55;
			this.Label4.Text = "POSTED YTD NET";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(53, 65);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(87, 15);
			this.Label3.TabIndex = 54;
			this.Label3.Text = "NET BUDGET";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// lblBalAccount
			// 
			this.lblBalAccount.AutoSize = true;
			this.lblBalAccount.Location = new System.Drawing.Point(20, 30);
			this.lblBalAccount.Name = "lblBalAccount";
			this.lblBalAccount.Size = new System.Drawing.Size(69, 15);
			this.lblBalAccount.TabIndex = 53;
			this.lblBalAccount.Text = "ACCOUNT";
			this.lblBalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblBalAccount, null);
			// 
			// frmSearch
			// 
			this.frmSearch.Controls.Add(this.cmdCancel);
			this.frmSearch.Controls.Add(this.cmdSearch);
			this.frmSearch.Controls.Add(this.txtSearch);
			this.frmSearch.Location = new System.Drawing.Point(30, 30);
			this.frmSearch.Name = "frmSearch";
			this.frmSearch.Size = new System.Drawing.Size(302, 150);
			this.frmSearch.TabIndex = 32;
			this.frmSearch.Text = "Vendor Search";
			this.ToolTip1.SetToolTip(this.frmSearch, null);
			this.frmSearch.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(136, 90);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(96, 40);
			this.cmdCancel.TabIndex = 35;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(20, 90);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(96, 40);
			this.cmdSearch.TabIndex = 34;
			this.cmdSearch.Text = "Search";
			this.ToolTip1.SetToolTip(this.cmdSearch, null);
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(20, 30);
			this.txtSearch.MaxLength = 35;
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(262, 40);
			this.txtSearch.TabIndex = 33;
			this.ToolTip1.SetToolTip(this.txtSearch, null);
			// 
			// frmInfo
			// 
			this.frmInfo.Controls.Add(this.lstRecords);
			this.frmInfo.Controls.Add(this.cmdReturn);
			this.frmInfo.Controls.Add(this.cmdRetrieve);
			this.frmInfo.Controls.Add(this.lblVendorName);
			this.frmInfo.Controls.Add(this.lblRecordNumber);
			this.frmInfo.Location = new System.Drawing.Point(30, 30);
			this.frmInfo.Name = "frmInfo";
			this.frmInfo.Size = new System.Drawing.Size(447, 347);
			this.frmInfo.TabIndex = 36;
			this.ToolTip1.SetToolTip(this.frmInfo, null);
			this.frmInfo.Visible = false;
			// 
			// lstRecords
			// 
			this.lstRecords.BackColor = System.Drawing.SystemColors.Window;
			this.lstRecords.Location = new System.Drawing.Point(20, 65);
			this.lstRecords.Name = "lstRecords";
			this.lstRecords.Size = new System.Drawing.Size(407, 202);
			this.lstRecords.TabIndex = 37;
			this.ToolTip1.SetToolTip(this.lstRecords, null);
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(190, 287);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(94, 40);
			this.cmdReturn.TabIndex = 41;
			this.cmdReturn.Text = "Cancel ";
			this.ToolTip1.SetToolTip(this.cmdReturn, null);
			// 
			// cmdRetrieve
			// 
			this.cmdRetrieve.AppearanceKey = "actionButton";
			this.cmdRetrieve.Location = new System.Drawing.Point(20, 287);
			this.cmdRetrieve.Name = "cmdRetrieve";
			this.cmdRetrieve.Size = new System.Drawing.Size(150, 40);
			this.cmdRetrieve.TabIndex = 40;
			this.cmdRetrieve.Text = "Retrieve Record";
			this.ToolTip1.SetToolTip(this.cmdRetrieve, null);
			// 
			// lblVendorName
			// 
			this.lblVendorName.Location = new System.Drawing.Point(158, 30);
			this.lblVendorName.Name = "lblVendorName";
			this.lblVendorName.Size = new System.Drawing.Size(42, 14);
			this.lblVendorName.TabIndex = 39;
			this.lblVendorName.Text = "NAME";
			this.lblVendorName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblVendorName, null);
			// 
			// lblRecordNumber
			// 
			this.lblRecordNumber.Location = new System.Drawing.Point(20, 30);
			this.lblRecordNumber.Name = "lblRecordNumber";
			this.lblRecordNumber.Size = new System.Drawing.Size(68, 14);
			this.lblRecordNumber.TabIndex = 38;
			this.lblRecordNumber.Text = "VENDOR #";
			this.lblRecordNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblRecordNumber, null);
			// 
			// txtAddress_0
			// 
			this.txtAddress_0.Location = new System.Drawing.Point(860, 50);
			this.txtAddress_0.MaxLength = 50;
			this.txtAddress_0.Name = "txtAddress_0";
			this.txtAddress_0.Size = new System.Drawing.Size(331, 40);
			this.txtAddress_0.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtAddress_0, null);
			// 
			// txtVendor
			// 
			this.txtVendor.Location = new System.Drawing.Point(770, 50);
			this.txtVendor.MaxLength = 5;
			this.txtVendor.Name = "txtVendor";
			this.txtVendor.Size = new System.Drawing.Size(80, 40);
			this.txtVendor.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtVendor, "Enter E for Encumbrance List, S for Vendor Search, or A for Add Vendor");
			// 
			// txtCheck
			// 
			this.txtCheck.Location = new System.Drawing.Point(770, 300);
			this.txtCheck.MaxLength = 6;
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Size = new System.Drawing.Size(203, 40);
			this.txtCheck.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.txtCheck, null);
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(155, 250);
			this.txtDescription.MaxLength = 25;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(506, 40);
			this.txtDescription.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			// 
			// txtReference
			// 
			this.txtReference.Location = new System.Drawing.Point(155, 300);
			this.txtReference.MaxLength = 15;
			this.txtReference.Name = "txtReference";
			this.txtReference.Size = new System.Drawing.Size(161, 40);
			this.txtReference.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtReference, null);
			// 
			// fraBorder
			// 
			this.fraBorder.AppearanceKey = "groupBoxNoBorders";
			this.fraBorder.Controls.Add(this.Frame1);
			this.fraBorder.Controls.Add(this.chkSeperate);
			this.fraBorder.Controls.Add(this.txtPayable);
			this.fraBorder.Controls.Add(this.txtPeriod);
			this.fraBorder.Controls.Add(this.lblJournalNumber);
			this.fraBorder.Controls.Add(this.lblPayable);
			this.fraBorder.Controls.Add(this.lblJournal);
			this.fraBorder.Controls.Add(this.lblPeriod);
			this.fraBorder.Location = new System.Drawing.Point(10, 45);
			this.fraBorder.Name = "fraBorder";
			this.fraBorder.Size = new System.Drawing.Size(659, 197);
			this.fraBorder.TabIndex = 42;
			this.ToolTip1.SetToolTip(this.fraBorder, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cboFrequency);
			this.Frame1.Controls.Add(this.txtUntil);
			this.Frame1.Controls.Add(this.lblFrequency);
			this.Frame1.Controls.Add(this.lblDays);
			this.Frame1.Controls.Add(this.lblUntil);
			this.Frame1.Location = new System.Drawing.Point(353, 57);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(298, 130);
			this.Frame1.TabIndex = 46;
			this.Frame1.Text = "Auto Payment";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// cboFrequency
			// 
			this.cboFrequency.BackColor = System.Drawing.SystemColors.Window;
			this.cboFrequency.Items.AddRange(new object[] {
            " ",
            "7",
            "14",
            "21",
            "30",
            "60",
            "90",
            "120",
            "150",
            "180",
            "365"});
			this.cboFrequency.Location = new System.Drawing.Point(129, 30);
			this.cboFrequency.Name = "cboFrequency";
			this.cboFrequency.Size = new System.Drawing.Size(113, 40);
			this.cboFrequency.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cboFrequency, null);
			this.cboFrequency.Enter += new System.EventHandler(this.cboFrequency_Enter);
			// 
			// txtUntil
			// 
			this.txtUntil.Location = new System.Drawing.Point(129, 80);
			this.txtUntil.Mask = "##/##/####";
			this.txtUntil.MaxLength = 10;
			this.txtUntil.Name = "txtUntil";
			this.txtUntil.Size = new System.Drawing.Size(115, 40);
			this.txtUntil.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtUntil, null);
			// 
			// lblFrequency
			// 
			this.lblFrequency.AutoSize = true;
			this.lblFrequency.Location = new System.Drawing.Point(20, 44);
			this.lblFrequency.Name = "lblFrequency";
			this.lblFrequency.Size = new System.Drawing.Size(85, 15);
			this.lblFrequency.TabIndex = 49;
			this.lblFrequency.Text = "FREQUENCY";
			this.lblFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblFrequency, null);
			// 
			// lblDays
			// 
			this.lblDays.AutoSize = true;
			this.lblDays.Location = new System.Drawing.Point(248, 44);
			this.lblDays.Name = "lblDays";
			this.lblDays.Size = new System.Drawing.Size(41, 15);
			this.lblDays.TabIndex = 48;
			this.lblDays.Text = "DAYS";
			this.lblDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblDays, null);
			// 
			// lblUntil
			// 
			this.lblUntil.AutoSize = true;
			this.lblUntil.Location = new System.Drawing.Point(20, 94);
			this.lblUntil.Name = "lblUntil";
			this.lblUntil.Size = new System.Drawing.Size(43, 15);
			this.lblUntil.TabIndex = 47;
			this.lblUntil.Text = "UNTIL";
			this.lblUntil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblUntil, null);
			// 
			// chkSeperate
			// 
			this.chkSeperate.Location = new System.Drawing.Point(353, 20);
			this.chkSeperate.Name = "chkSeperate";
			this.chkSeperate.Size = new System.Drawing.Size(145, 27);
			this.chkSeperate.TabIndex = 2;
			this.chkSeperate.Text = "Separate Check";
			this.ToolTip1.SetToolTip(this.chkSeperate, null);
			this.chkSeperate.CheckedChanged += new System.EventHandler(this.chkSeperate_CheckedChanged);
			// 
			// txtPayable
			// 
			this.txtPayable.Location = new System.Drawing.Point(145, 95);
			this.txtPayable.Mask = "##/##/####";
			this.txtPayable.MaxLength = 10;
			this.txtPayable.Name = "txtPayable";
			this.txtPayable.Size = new System.Drawing.Size(161, 40);
			this.txtPayable.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtPayable, null);
			// 
			// txtPeriod
			// 
			this.txtPeriod.Location = new System.Drawing.Point(145, 45);
			this.txtPeriod.MaxLength = 2;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Size = new System.Drawing.Size(161, 40);
			this.txtPeriod.TabIndex = 47;
			this.ToolTip1.SetToolTip(this.txtPeriod, null);
			// 
			// lblJournalNumber
			// 
			this.lblJournalNumber.AutoSize = true;
			this.lblJournalNumber.Location = new System.Drawing.Point(145, 20);
			this.lblJournalNumber.Name = "lblJournalNumber";
			this.lblJournalNumber.Size = new System.Drawing.Size(14, 15);
			this.lblJournalNumber.TabIndex = 63;
			this.lblJournalNumber.Text = "0";
			this.lblJournalNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblJournalNumber, null);
			// 
			// lblPayable
			// 
			this.lblPayable.AutoSize = true;
			this.lblPayable.Location = new System.Drawing.Point(20, 109);
			this.lblPayable.Name = "lblPayable";
			this.lblPayable.Size = new System.Drawing.Size(64, 15);
			this.lblPayable.TabIndex = 45;
			this.lblPayable.Text = "PAYABLE";
			this.lblPayable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblPayable, null);
			// 
			// lblJournal
			// 
			this.lblJournal.AutoSize = true;
			this.lblJournal.Location = new System.Drawing.Point(20, 20);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(65, 15);
			this.lblJournal.TabIndex = 44;
			this.lblJournal.Text = "JOURNAL";
			this.lblJournal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblJournal, null);
			// 
			// lblPeriod
			// 
			this.lblPeriod.AutoSize = true;
			this.lblPeriod.Location = new System.Drawing.Point(20, 59);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(101, 15);
			this.lblPeriod.TabIndex = 43;
			this.lblPeriod.Text = "ACCTG PERIOD";
			this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblPeriod, null);
			// 
			// txtAddress_1
			// 
			this.txtAddress_1.Location = new System.Drawing.Point(860, 100);
			this.txtAddress_1.MaxLength = 35;
			this.txtAddress_1.Name = "txtAddress_1";
			this.txtAddress_1.Size = new System.Drawing.Size(331, 40);
			this.txtAddress_1.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtAddress_1, null);
			// 
			// txtAddress_2
			// 
			this.txtAddress_2.Location = new System.Drawing.Point(860, 150);
			this.txtAddress_2.MaxLength = 35;
			this.txtAddress_2.Name = "txtAddress_2";
			this.txtAddress_2.Size = new System.Drawing.Size(331, 40);
			this.txtAddress_2.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtAddress_2, null);
			// 
			// txtAddress_3
			// 
			this.txtAddress_3.Location = new System.Drawing.Point(860, 200);
			this.txtAddress_3.MaxLength = 35;
			this.txtAddress_3.Name = "txtAddress_3";
			this.txtAddress_3.Size = new System.Drawing.Size(331, 40);
			this.txtAddress_3.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtAddress_3, null);
			// 
			// txtAmount2
			// 
			this.txtAmount2.Location = new System.Drawing.Point(414, 300);
			this.txtAmount2.MaxLength = 14;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Size = new System.Drawing.Size(247, 40);
			this.txtAmount2.TabIndex = 17;
			this.ToolTip1.SetToolTip(this.txtAmount2, null);
			this.txtAmount2.Visible = false;
			// 
			// txtAmount
			// 
			this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAmount.Location = new System.Drawing.Point(414, 300);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(247, 40);
			this.txtAmount.TabIndex = 16;
			this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAmount, null);
			// 
			// chkUseAltCashAccount
			// 
			this.chkUseAltCashAccount.Location = new System.Drawing.Point(699, 184);
			this.chkUseAltCashAccount.Name = "chkUseAltCashAccount";
			this.chkUseAltCashAccount.Size = new System.Drawing.Size(125, 27);
			this.chkUseAltCashAccount.TabIndex = 19;
			this.chkUseAltCashAccount.Text = "Alt Cash Acct";
			this.ToolTip1.SetToolTip(this.chkUseAltCashAccount, null);
			this.chkUseAltCashAccount.Visible = false;
			this.chkUseAltCashAccount.CheckedChanged += new System.EventHandler(this.chkUseAltCashAccount_CheckedChanged);
			// 
			// fraAltCashAccount
			// 
			this.fraAltCashAccount.Controls.Add(this.vsAltCashAccount);
			this.fraAltCashAccount.Enabled = false;
			this.fraAltCashAccount.Location = new System.Drawing.Point(988, 302);
			this.fraAltCashAccount.Name = "fraAltCashAccount";
			this.fraAltCashAccount.ShowCheckBox = true;
			this.fraAltCashAccount.Size = new System.Drawing.Size(195, 90);
			this.fraAltCashAccount.TabIndex = 51;
			this.fraAltCashAccount.Text = "Alt Cash Acct";
			this.ToolTip1.SetToolTip(this.fraAltCashAccount, null);
			this.fraAltCashAccount.EnabledChanged += new System.EventHandler(this.fraAltCashAccount_EnabledChanged);
			// 
			// vsAltCashAccount
			// 
			this.vsAltCashAccount.Cols = 1;
			this.vsAltCashAccount.ColumnHeadersVisible = false;
			this.vsAltCashAccount.FixedCols = 0;
			this.vsAltCashAccount.FixedRows = 0;
			this.vsAltCashAccount.Location = new System.Drawing.Point(20, 30);
			this.vsAltCashAccount.Name = "vsAltCashAccount";
			this.vsAltCashAccount.RowHeadersVisible = false;
			this.vsAltCashAccount.Size = new System.Drawing.Size(156, 42);
			this.vsAltCashAccount.TabIndex = 20;
			this.ToolTip1.SetToolTip(this.vsAltCashAccount, null);
			// 
			// txtZip4
			// 
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.Location = new System.Drawing.Point(1116, 250);
			this.txtZip4.MaxLength = 4;
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(75, 40);
			this.txtZip4.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtZip4, null);
			// 
			// txtZip
			// 
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.Location = new System.Drawing.Point(1035, 250);
			this.txtZip.MaxLength = 5;
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(75, 40);
			this.txtZip.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.txtZip, null);
			// 
			// txtState
			// 
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.Location = new System.Drawing.Point(979, 250);
			this.txtState.MaxLength = 2;
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(50, 40);
			this.txtState.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtState, null);
			// 
			// txtCity
			// 
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.Location = new System.Drawing.Point(860, 250);
			this.txtCity.MaxLength = 35;
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(113, 40);
			this.txtCity.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtCity, null);
			// 
			// vs1
			// 
			this.vs1.ExtendLastCol = true;
			this.vs1.Location = new System.Drawing.Point(30, 400);
			this.vs1.Name = "vs1";
			this.vs1.Size = new System.Drawing.Size(1158, 219);
			this.vs1.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.vs1, null);
			// 
			// lblReadOnly
			// 
			this.lblReadOnly.AutoSize = true;
			this.lblReadOnly.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblReadOnly.Location = new System.Drawing.Point(30, 30);
			this.lblReadOnly.Name = "lblReadOnly";
			this.lblReadOnly.Size = new System.Drawing.Size(76, 15);
			this.lblReadOnly.TabIndex = 50;
			this.lblReadOnly.Text = "VIEW ONLY";
			this.lblReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblReadOnly, null);
			// 
			// Shape2
			// 
			this.Shape2.BackColor = System.Drawing.Color.Transparent;
			this.Shape2.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.Shape2.Location = new System.Drawing.Point(326, 347);
			this.Shape2.Name = "Shape2";
			this.Shape2.Size = new System.Drawing.Size(335, 20);
			this.Shape2.TabIndex = 53;
			this.ToolTip1.SetToolTip(this.Shape2, null);
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.Transparent;
			this.Shape1.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.Shape1.Location = new System.Drawing.Point(20, 347);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(296, 20);
			this.Shape1.TabIndex = 54;
			this.ToolTip1.SetToolTip(this.Shape1, null);
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(30, 375);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(631, 15);
			this.lblExpense.TabIndex = 31;
			this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.ToolTip1.SetToolTip(this.lblExpense, "Right click to see account balance");
			this.lblExpense.MouseUp += new Wisej.Web.MouseEventHandler(this.lblExpense_MouseUp);
			// 
			// lblActualPayment
			// 
			this.lblActualPayment.Location = new System.Drawing.Point(565, 350);
			this.lblActualPayment.Name = "lblActualPayment";
			this.lblActualPayment.Size = new System.Drawing.Size(86, 16);
			this.lblActualPayment.TabIndex = 30;
			this.lblActualPayment.Text = "0.00";
			this.lblActualPayment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblActualPayment, null);
			// 
			// lblRemAmount
			// 
			this.lblRemAmount.Location = new System.Drawing.Point(218, 350);
			this.lblRemAmount.Name = "lblRemAmount";
			this.lblRemAmount.Size = new System.Drawing.Size(86, 16);
			this.lblRemAmount.TabIndex = 29;
			this.lblRemAmount.Text = "0.00";
			this.lblRemAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblRemAmount, null);
			// 
			// lblVendor
			// 
			this.lblVendor.AutoSize = true;
			this.lblVendor.Location = new System.Drawing.Point(684, 64);
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Size = new System.Drawing.Size(60, 15);
			this.lblVendor.TabIndex = 28;
			this.lblVendor.Text = "VENDOR";
			this.lblVendor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblVendor, null);
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Location = new System.Drawing.Point(30, 256);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(92, 15);
			this.lblDescription.TabIndex = 27;
			this.lblDescription.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// lblReference
			// 
			this.lblReference.AutoSize = true;
			this.lblReference.Location = new System.Drawing.Point(30, 314);
			this.lblReference.Name = "lblReference";
			this.lblReference.Size = new System.Drawing.Size(84, 15);
			this.lblReference.TabIndex = 26;
			this.lblReference.Text = "REFERENCE";
			this.lblReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblReference, null);
			// 
			// lblAmount
			// 
			this.lblAmount.AutoSize = true;
			this.lblAmount.Location = new System.Drawing.Point(336, 314);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(61, 15);
			this.lblAmount.TabIndex = 25;
			this.lblAmount.Text = "AMOUNT";
			this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblAmount, null);
			// 
			// lblCheck
			// 
			this.lblCheck.AutoSize = true;
			this.lblCheck.Location = new System.Drawing.Point(684, 314);
			this.lblCheck.Name = "lblCheck";
			this.lblCheck.Size = new System.Drawing.Size(58, 15);
			this.lblCheck.TabIndex = 24;
			this.lblCheck.Text = "CHECK#";
			this.lblCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblCheck, null);
			// 
			// lblRemaining
			// 
			this.lblRemaining.AutoSize = true;
			this.lblRemaining.Location = new System.Drawing.Point(30, 350);
			this.lblRemaining.Name = "lblRemaining";
			this.lblRemaining.Size = new System.Drawing.Size(134, 15);
			this.lblRemaining.TabIndex = 23;
			this.lblRemaining.Text = "REMAINING AMOUNT";
			this.lblRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblRemaining, null);
			// 
			// lblActual
			// 
			this.lblActual.AutoSize = true;
			this.lblActual.Location = new System.Drawing.Point(336, 350);
			this.lblActual.Name = "lblActual";
			this.lblActual.Size = new System.Drawing.Size(119, 15);
			this.lblActual.TabIndex = 21;
			this.lblActual.Text = "ACTUAL PAYMENT";
			this.lblActual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblActual, null);
			// 
			// MainMenu1
			// 
			this.MainMenu1.Name = null;
			// 
			// mnuViewAttachedDocuments
			// 
			this.mnuViewAttachedDocuments.Index = 0;
			this.mnuViewAttachedDocuments.Name = "mnuViewAttachedDocuments";
			this.mnuViewAttachedDocuments.Text = "Attached Documents";
			this.mnuViewAttachedDocuments.Click += new System.EventHandler(this.mnuViewAttachedDocuments_Click);
			// 
			// mnuFileVendorDetail
			// 
			this.mnuFileVendorDetail.Index = 3;
			this.mnuFileVendorDetail.Name = "mnuFileVendorDetail";
			this.mnuFileVendorDetail.Shortcut = Wisej.Web.Shortcut.F9;
			this.mnuFileVendorDetail.Text = "Vendor Detail";
			this.mnuFileVendorDetail.Click += new System.EventHandler(this.mnuFileVendorDetail_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuViewAttachedDocuments,
            this.mnuFileSep2,
            this.mnuFileDisplayAmount,
            this.mnuFileVendorDetail,
            this.mnuFileSep3,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSep2
			// 
			this.mnuFileSep2.Index = 1;
			this.mnuFileSep2.Name = "mnuFileSep2";
			this.mnuFileSep2.Text = "-";
			// 
			// mnuFileDisplayAmount
			// 
			this.mnuFileDisplayAmount.Index = 2;
			this.mnuFileDisplayAmount.Name = "mnuFileDisplayAmount";
			this.mnuFileDisplayAmount.Text = "Display Journal Amount";
			this.mnuFileDisplayAmount.Click += new System.EventHandler(this.mnuFileDisplayAmount_Click);
			// 
			// mnuFileSep3
			// 
			this.mnuFileSep3.Index = 4;
			this.mnuFileSep3.Name = "mnuFileSep3";
			this.mnuFileSep3.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnFileVendorDetail
			// 
			this.btnFileVendorDetail.AppearanceKey = "acceptButton";
			this.btnFileVendorDetail.Location = new System.Drawing.Point(452, 30);
			this.btnFileVendorDetail.Name = "btnFileVendorDetail";
			this.btnFileVendorDetail.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileVendorDetail.Size = new System.Drawing.Size(141, 48);
			this.btnFileVendorDetail.Text = "Vendor Detail";
			this.btnFileVendorDetail.Click += new System.EventHandler(this.mnuFileVendorDetail_Click);
			// 
			// btnFileDisplayAmount
			// 
			this.btnFileDisplayAmount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileDisplayAmount.Location = new System.Drawing.Point(1020, 28);
			this.btnFileDisplayAmount.Name = "btnFileDisplayAmount";
			this.btnFileDisplayAmount.Size = new System.Drawing.Size(159, 24);
			this.btnFileDisplayAmount.TabIndex = 1;
			this.btnFileDisplayAmount.Text = "Display Journal Amount";
			this.btnFileDisplayAmount.Click += new System.EventHandler(this.mnuFileDisplayAmount_Click);
			// 
			// btnViewAttachedDocuments
			// 
			this.btnViewAttachedDocuments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnViewAttachedDocuments.Location = new System.Drawing.Point(873, 28);
			this.btnViewAttachedDocuments.Name = "btnViewAttachedDocuments";
			this.btnViewAttachedDocuments.Size = new System.Drawing.Size(144, 24);
			this.btnViewAttachedDocuments.TabIndex = 2;
			this.btnViewAttachedDocuments.Text = "Attached Documents";
			this.btnViewAttachedDocuments.Click += new System.EventHandler(this.mnuViewAttachedDocuments_Click);
			// 
			// cmdFileVendorDetail
			// 
			this.cmdFileVendorDetail.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileVendorDetail.Location = new System.Drawing.Point(764, 28);
			this.cmdFileVendorDetail.Name = "cmdFileVendorDetail";
			this.cmdFileVendorDetail.Shortcut = Wisej.Web.Shortcut.F9;
			this.cmdFileVendorDetail.Size = new System.Drawing.Size(103, 24);
			this.cmdFileVendorDetail.TabIndex = 3;
			this.cmdFileVendorDetail.Text = "Vendor Detail";
			this.cmdFileVendorDetail.Click += new System.EventHandler(this.mnuFileVendorDetail_Click);
			// 
			// frmPostedAPJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1218, 620);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPostedAPJournal";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "AP Invoice";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmPostedAPJournal_Load);
			this.Activated += new System.EventHandler(this.frmPostedAPJournal_Activated);
			this.Resize += new System.EventHandler(this.frmPostedAPJournal_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPostedAPJournal_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPostedAPJournal_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).EndInit();
			this.fraAccountBalance.ResumeLayout(false);
			this.fraAccountBalance.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmSearch)).EndInit();
			this.frmSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmInfo)).EndInit();
			this.frmInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBorder)).EndInit();
			this.fraBorder.ResumeLayout(false);
			this.fraBorder.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtUntil)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSeperate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseAltCashAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAltCashAccount)).EndInit();
			this.fraAltCashAccount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAltCashAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileVendorDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDisplayAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnViewAttachedDocuments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileVendorDetail)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnFileVendorDetail;
		private FCButton btnFileDisplayAmount;
		private FCButton btnViewAttachedDocuments;
		private FCButton cmdFileVendorDetail;
	}
}