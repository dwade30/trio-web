﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBDAccountDetail.
	/// </summary>
	partial class frmBDAccountDetail : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPeriodStart;
		public fecherFoundation.FCComboBox cmbPeriodEnd;
		public FCGrid GridDetails;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblperiodto;
		public fecherFoundation.FCLabel lblAccountDescription;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblAccountLabel;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBDAccountDetail));
			this.cmbPeriodStart = new fecherFoundation.FCComboBox();
			this.cmbPeriodEnd = new fecherFoundation.FCComboBox();
			this.GridDetails = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblperiodto = new fecherFoundation.FCLabel();
			this.lblAccountDescription = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.lblAccountLabel = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDetails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPeriodStart);
			this.ClientArea.Controls.Add(this.cmbPeriodEnd);
			this.ClientArea.Controls.Add(this.GridDetails);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblperiodto);
			this.ClientArea.Controls.Add(this.lblAccountDescription);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.lblAccountLabel);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(171, 30);
			this.HeaderText.Text = "Account Detail";
			// 
			// cmbPeriodStart
			// 
			this.cmbPeriodStart.AutoSize = false;
			this.cmbPeriodStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriodStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPeriodStart.FormattingEnabled = true;
			this.cmbPeriodStart.Location = new System.Drawing.Point(150, 102);
			this.cmbPeriodStart.Name = "cmbPeriodStart";
			this.cmbPeriodStart.Size = new System.Drawing.Size(177, 40);
			this.cmbPeriodStart.TabIndex = 5;
			this.cmbPeriodStart.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodStart_SelectedIndexChanged);
			// 
			// cmbPeriodEnd
			// 
			this.cmbPeriodEnd.AutoSize = false;
			this.cmbPeriodEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriodEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPeriodEnd.FormattingEnabled = true;
			this.cmbPeriodEnd.Location = new System.Drawing.Point(381, 102);
			this.cmbPeriodEnd.Name = "cmbPeriodEnd";
			this.cmbPeriodEnd.Size = new System.Drawing.Size(177, 40);
			this.cmbPeriodEnd.TabIndex = 4;
			this.cmbPeriodEnd.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodEnd_SelectedIndexChanged);
			// 
			// GridDetails
			// 
			this.GridDetails.AllowSelection = false;
			this.GridDetails.AllowUserToResizeColumns = false;
			this.GridDetails.AllowUserToResizeRows = false;
			this.GridDetails.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridDetails.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDetails.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDetails.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDetails.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDetails.BackColorSel = System.Drawing.Color.Empty;
			this.GridDetails.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDetails.ColumnHeadersHeight = 30;
			this.GridDetails.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDetails.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDetails.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDetails.ExtendLastCol = true;
			this.GridDetails.FixedCols = 0;
			this.GridDetails.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDetails.FrozenCols = 0;
			this.GridDetails.GridColor = System.Drawing.Color.Empty;
			this.GridDetails.Location = new System.Drawing.Point(30, 172);
			this.GridDetails.Name = "GridDetails";
			this.GridDetails.ReadOnly = true;
			this.GridDetails.RowHeadersVisible = false;
			this.GridDetails.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDetails.RowHeightMin = 0;
			this.GridDetails.Rows = 1;
			this.GridDetails.ShowColumnVisibilityMenu = false;
			this.GridDetails.Size = new System.Drawing.Size(1018, 296);
			this.GridDetails.StandardTab = true;
			this.GridDetails.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDetails.TabIndex = 3;
			this.GridDetails.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridDetails_MouseMoveEvent);
			this.GridDetails.DoubleClick += new System.EventHandler(this.GridDetails_DblClick);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 116);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(50, 16);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "PERIODS";
			// 
			// lblperiodto
			// 
			this.lblperiodto.Location = new System.Drawing.Point(345, 115);
			this.lblperiodto.Name = "lblperiodto";
			this.lblperiodto.Size = new System.Drawing.Size(22, 14);
			this.lblperiodto.TabIndex = 6;
			this.lblperiodto.Text = "TO";
			// 
			// lblAccountDescription
			// 
			this.lblAccountDescription.Location = new System.Drawing.Point(100, 66);
			this.lblAccountDescription.Name = "lblAccountDescription";
			this.lblAccountDescription.Size = new System.Drawing.Size(522, 16);
			this.lblAccountDescription.TabIndex = 2;
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(100, 30);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(262, 16);
			this.lblAccount.TabIndex = 1;
			// 
			// lblAccountLabel
			// 
			this.lblAccountLabel.Location = new System.Drawing.Point(30, 30);
			this.lblAccountLabel.Name = "lblAccountLabel";
			this.lblAccountLabel.Size = new System.Drawing.Size(86, 16);
			this.lblAccountLabel.TabIndex = 0;
			this.lblAccountLabel.Text = "ACCOUNT";
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(489, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(78, 48);
			this.cmdPrint.TabIndex = 2;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// frmBDAccountDetail
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBDAccountDetail";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Account Detail";
			this.Load += new System.EventHandler(this.frmBDAccountDetail_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBDAccountDetail_KeyDown);
			this.Resize += new System.EventHandler(this.frmBDAccountDetail_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDetails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdPrint;
	}
}
