﻿//Fecher vbPorter - Version 1.0.0.27
using System.Runtime.InteropServices;

namespace TWBD0000
{
	public class modHancockCountyPayrollImport
	{
		//=========================================================
		[StructLayout(LayoutKind.Sequential)]
		public struct HancockCountyPayrollRecord
		{
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string CompanyCode;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
			public string Date;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
			public string Division;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
			public string Branch;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
			public string Department;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
			public string Account;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
			public string Hours;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
			public string DebitAmount;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
			public string CreditAmount;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string BlankOrCRLF;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public HancockCountyPayrollRecord(int unusedParam)
			{
				this.CompanyCode = new string(' ', 4);
				this.Date = new string(' ', 8);
				this.Division = new string(' ', 6);
				this.Branch = new string(' ', 6);
				this.Department = new string(' ', 6);
				this.Account = new string(' ', 15);
				this.Hours = new string(' ', 14);
				this.DebitAmount = new string(' ', 14);
				this.CreditAmount = new string(' ', 14);
				this.BlankOrCRLF = new string(' ', 2);
			}
		};

		public class StaticVariables
		{
			public HancockCountyPayrollRecord HCPR = new HancockCountyPayrollRecord(0);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
