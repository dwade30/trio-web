﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeCheckStatus.
	/// </summary>
	partial class frmChangeCheckStatus : BaseForm
	{
		public fecherFoundation.FCFrame fraCheck;
		public fecherFoundation.FCTextBox txtCheck;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCGrid vs1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangeCheckStatus));
			this.fraCheck = new fecherFoundation.FCFrame();
			this.txtCheck = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.vs1 = new fecherFoundation.FCGrid();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.vs1Header = new Wisej.Web.Panel();
			this.vs1HeaderText = new Wisej.Web.Label();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCheck)).BeginInit();
			this.fraCheck.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.vs1Header.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 540);
			this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vs1Header);
			this.ClientArea.Controls.Add(this.fraCheck);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Size = new System.Drawing.Size(1068, 480);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1068, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(168, 30);
			this.HeaderText.Text = "Update Status";
			// 
			// fraCheck
			// 
			this.fraCheck.AppearanceKey = "groupBoxNoBorders";
			this.fraCheck.BackColor = System.Drawing.Color.FromName("@window");
			this.fraCheck.Controls.Add(this.cmdProcessSave);
			this.fraCheck.Controls.Add(this.txtCheck);
			this.fraCheck.Controls.Add(this.Label1);
			this.fraCheck.Location = new System.Drawing.Point(30, 30);
			this.fraCheck.Name = "fraCheck";
			this.fraCheck.Size = new System.Drawing.Size(305, 186);
			this.fraCheck.TabIndex = 4;
			// 
			// txtCheck
			// 
			this.txtCheck.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheck.Location = new System.Drawing.Point(20, 66);
			this.txtCheck.MaxLength = 6;
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Size = new System.Drawing.Size(104, 40);
			this.txtCheck.TabIndex = 6;
			this.txtCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCheck_KeyPress);
			// 
			// Label1
			// 
			this.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(265, 16);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "PLEASE ENTER THE STARTING CHECK NUMBER";
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 9;
			this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vs1.FixedCols = 0;
			this.vs1.Location = new System.Drawing.Point(30, 60);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 50;
			this.vs1.Size = new System.Drawing.Size(1008, 405);
			this.vs1.StandardTab = false;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 2;
			this.vs1.Visible = false;
			this.vs1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(20, 127);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(104, 48);
			this.cmdProcessSave.TabIndex = 3;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// vs1Header
			// 
			this.vs1Header.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
			this.vs1Header.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.vs1Header.Controls.Add(this.vs1HeaderText);
			this.vs1Header.Location = new System.Drawing.Point(30, 31);
			this.vs1Header.Name = "vs1Header";
			this.vs1Header.Size = new System.Drawing.Size(1008, 30);
			this.vs1Header.TabIndex = 2;
			this.vs1Header.Text = "-- STATUS --";
			this.vs1Header.Visible = false;
			// 
			// vs1HeaderText
			// 
			this.vs1HeaderText.Location = new System.Drawing.Point(258, 0);
			this.vs1HeaderText.Name = "vs1HeaderText";
			this.vs1HeaderText.Size = new System.Drawing.Size(124, 29);
			this.vs1HeaderText.TabIndex = 0;
			this.vs1HeaderText.Text = "-- STATUS --";
			this.vs1HeaderText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmChangeCheckStatus
            // 
            this.AcceptButton = this.cmdProcessSave;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1068, 648);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmChangeCheckStatus";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Update Status";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmChangeCheckStatus_Load);
			this.Activated += new System.EventHandler(this.frmChangeCheckStatus_Activated);
			this.Resize += new System.EventHandler(this.frmChangeCheckStatus_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChangeCheckStatus_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChangeCheckStatus_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCheck)).EndInit();
			this.fraCheck.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.vs1Header.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
		private Panel vs1Header;
		private Label vs1HeaderText;
		private System.ComponentModel.IContainer components;
	}
}
