﻿//Fecher vbPorxter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetGJCorrDataEntry.
	/// </summary>
	partial class frmGetGJCorrDataEntry : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReturn;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCComboBox cboSavePeriod;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCLabel lblSavePeriod;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public fecherFoundation.FCFrame fraReturnMultipleChecks;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCGrid vsReturnMultiple;
		public fecherFoundation.FCCheckBox chkCreateNewJournal;
		public fecherFoundation.FCTextBox txtJournalDesc;
		public fecherFoundation.FCFrame fraMultipleChecks;
		public fecherFoundation.FCButton cmdGetCheck;
		public fecherFoundation.FCButton cmdReturnCheck;
		public fecherFoundation.FCGrid vsMultipleChecks;
		public Global.T2KDateBox txtCheckDate;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public Global.T2KOverTypeBox txtCheck;
		public fecherFoundation.FCLabel lblCheckNumber;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblCheckDate;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetGJCorrDataEntry));
            this.cmbReturn = new fecherFoundation.FCComboBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.cmdGet = new fecherFoundation.FCButton();
            this.vs1 = new fecherFoundation.FCGrid();
            this.fraJournalSave = new fecherFoundation.FCFrame();
            this.cboSavePeriod = new fecherFoundation.FCComboBox();
            this.txtJournalDescription = new fecherFoundation.FCTextBox();
            this.cmdCancelSave = new fecherFoundation.FCButton();
            this.cmdOKSave = new fecherFoundation.FCButton();
            this.cboSaveJournal = new fecherFoundation.FCComboBox();
            this.lblSavePeriod = new fecherFoundation.FCLabel();
            this.lblJournalDescription = new fecherFoundation.FCLabel();
            this.lblJournalSave = new fecherFoundation.FCLabel();
            this.lblSaveInstructions = new fecherFoundation.FCLabel();
            this.fraReturnMultipleChecks = new fecherFoundation.FCFrame();
            this.txtJournalDesc = new fecherFoundation.FCTextBox();
            this.cmdClearAll = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.vsReturnMultiple = new fecherFoundation.FCGrid();
            this.chkCreateNewJournal = new fecherFoundation.FCCheckBox();
            this.fraMultipleChecks = new fecherFoundation.FCFrame();
            this.cmdGetCheck = new fecherFoundation.FCButton();
            this.cmdReturnCheck = new fecherFoundation.FCButton();
            this.vsMultipleChecks = new fecherFoundation.FCGrid();
            this.txtCheckDate = new Global.T2KDateBox();
            this.cmdGetAccountNumber = new fecherFoundation.FCButton();
            this.txtCheck = new Global.T2KOverTypeBox();
            this.lblCheckNumber = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblCheckDate = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
            this.fraJournalSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReturnMultipleChecks)).BeginInit();
            this.fraReturnMultipleChecks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnMultiple)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateNewJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMultipleChecks)).BeginInit();
            this.fraMultipleChecks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturnCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMultipleChecks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(922, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraReturnMultipleChecks);
            this.ClientArea.Controls.Add(this.fraJournalSave);
            this.ClientArea.Controls.Add(this.txtCheckDate);
            this.ClientArea.Controls.Add(this.cmbReturn);
            this.ClientArea.Controls.Add(this.txtCheck);
            this.ClientArea.Controls.Add(this.lblCheckNumber);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblCheckDate);
            this.ClientArea.Controls.Add(this.fraMultipleChecks);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Size = new System.Drawing.Size(922, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(922, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(184, 30);
            this.HeaderText.Text = "AP Corrections ";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbReturn
            // 
            this.cmbReturn.Items.AddRange(new object[] {
            "Check Return (Single)",
            "Check Return (Multiple)",
            "Wrong Acct Number",
            "Split Amount",
            "Wrong Project"});
            this.cmbReturn.Location = new System.Drawing.Point(30, 66);
            this.cmbReturn.Name = "cmbReturn";
            this.cmbReturn.Size = new System.Drawing.Size(287, 40);
            this.cmbReturn.TabIndex = 24;
            this.cmbReturn.Text = "Check Return (Single)";
            this.ToolTip1.SetToolTip(this.cmbReturn, null);
            this.cmbReturn.SelectedIndexChanged += new System.EventHandler(this.optReturn_CheckedChanged);
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.White;
            this.Frame3.Controls.Add(this.cmdReturn);
            this.Frame3.Controls.Add(this.cmdGet);
            this.Frame3.Controls.Add(this.vs1);
            this.Frame3.Location = new System.Drawing.Point(30, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(862, 369);
            this.Frame3.TabIndex = 8;
            this.Frame3.Text = "Multiple Records";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            this.Frame3.Visible = false;
            // 
            // cmdReturn
            // 
            this.cmdReturn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(20, 311);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(216, 40);
            this.cmdReturn.TabIndex = 11;
            this.cmdReturn.Text = "Return to Search Screen";
            this.ToolTip1.SetToolTip(this.cmdReturn, null);
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // cmdGet
            // 
            this.cmdGet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdGet.AppearanceKey = "actionButton";
            this.cmdGet.Location = new System.Drawing.Point(245, 311);
            this.cmdGet.Name = "cmdGet";
            this.cmdGet.Size = new System.Drawing.Size(145, 40);
            this.cmdGet.TabIndex = 10;
            this.cmdGet.Text = "Retrieve Record";
            this.ToolTip1.SetToolTip(this.cmdGet, null);
            this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 6;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(20, 30);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 50;
            this.vs1.Size = new System.Drawing.Size(822, 259);
            this.vs1.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.vs1, null);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
            // 
            // fraJournalSave
            // 
            this.fraJournalSave.BackColor = System.Drawing.Color.White;
            this.fraJournalSave.Controls.Add(this.cboSavePeriod);
            this.fraJournalSave.Controls.Add(this.txtJournalDescription);
            this.fraJournalSave.Controls.Add(this.cmdCancelSave);
            this.fraJournalSave.Controls.Add(this.cmdOKSave);
            this.fraJournalSave.Controls.Add(this.cboSaveJournal);
            this.fraJournalSave.Controls.Add(this.lblSavePeriod);
            this.fraJournalSave.Controls.Add(this.lblJournalDescription);
            this.fraJournalSave.Controls.Add(this.lblJournalSave);
            this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
            this.fraJournalSave.Location = new System.Drawing.Point(30, 30);
            this.fraJournalSave.Name = "fraJournalSave";
            this.fraJournalSave.Size = new System.Drawing.Size(862, 306);
            this.fraJournalSave.TabIndex = 23;
            this.fraJournalSave.Text = "Save Journal";
            this.ToolTip1.SetToolTip(this.fraJournalSave, null);
            this.fraJournalSave.Visible = false;
            // 
            // cboSavePeriod
            // 
            this.cboSavePeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavePeriod.Location = new System.Drawing.Point(166, 186);
            this.cboSavePeriod.Name = "cboSavePeriod";
            this.cboSavePeriod.Size = new System.Drawing.Size(255, 40);
            this.cboSavePeriod.TabIndex = 37;
            this.ToolTip1.SetToolTip(this.cboSavePeriod, null);
            // 
            // txtJournalDescription
            // 
            this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtJournalDescription.Location = new System.Drawing.Point(166, 126);
            this.txtJournalDescription.MaxLength = 100;
            this.txtJournalDescription.Name = "txtJournalDescription";
            this.txtJournalDescription.Size = new System.Drawing.Size(255, 40);
            this.txtJournalDescription.TabIndex = 27;
            this.ToolTip1.SetToolTip(this.txtJournalDescription, null);
            // 
            // cmdCancelSave
            // 
            this.cmdCancelSave.AppearanceKey = "actionButton";
            this.cmdCancelSave.Location = new System.Drawing.Point(433, 246);
            this.cmdCancelSave.Name = "cmdCancelSave";
            this.cmdCancelSave.Size = new System.Drawing.Size(75, 40);
            this.cmdCancelSave.TabIndex = 26;
            this.cmdCancelSave.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancelSave, null);
            this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
            // 
            // cmdOKSave
            // 
            this.cmdOKSave.AppearanceKey = "actionButton";
            this.cmdOKSave.Location = new System.Drawing.Point(348, 246);
            this.cmdOKSave.Name = "cmdOKSave";
            this.cmdOKSave.Size = new System.Drawing.Size(70, 40);
            this.cmdOKSave.TabIndex = 25;
            this.cmdOKSave.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdOKSave, null);
            this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
            // 
            // cboSaveJournal
            // 
            this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
            this.cboSaveJournal.Location = new System.Drawing.Point(165, 66);
            this.cboSaveJournal.Name = "cboSaveJournal";
            this.cboSaveJournal.Size = new System.Drawing.Size(255, 40);
            this.cboSaveJournal.TabIndex = 24;
            this.ToolTip1.SetToolTip(this.cboSaveJournal, null);
            this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
            this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
            // 
            // lblSavePeriod
            // 
            this.lblSavePeriod.BackColor = System.Drawing.Color.Transparent;
            this.lblSavePeriod.Location = new System.Drawing.Point(20, 200);
            this.lblSavePeriod.Name = "lblSavePeriod";
            this.lblSavePeriod.Size = new System.Drawing.Size(53, 16);
            this.lblSavePeriod.TabIndex = 38;
            this.lblSavePeriod.Text = "PERIOD";
            this.ToolTip1.SetToolTip(this.lblSavePeriod, null);
            // 
            // lblJournalDescription
            // 
            this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalDescription.Location = new System.Drawing.Point(20, 140);
            this.lblJournalDescription.Name = "lblJournalDescription";
            this.lblJournalDescription.Size = new System.Drawing.Size(91, 16);
            this.lblJournalDescription.TabIndex = 30;
            this.lblJournalDescription.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.lblJournalDescription, null);
            // 
            // lblJournalSave
            // 
            this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalSave.Location = new System.Drawing.Point(20, 80);
            this.lblJournalSave.Name = "lblJournalSave";
            this.lblJournalSave.Size = new System.Drawing.Size(58, 16);
            this.lblJournalSave.TabIndex = 29;
            this.lblJournalSave.Text = "JOURNAL";
            this.ToolTip1.SetToolTip(this.lblJournalSave, null);
            // 
            // lblSaveInstructions
            // 
            this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
            this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblSaveInstructions.Name = "lblSaveInstructions";
            this.lblSaveInstructions.Size = new System.Drawing.Size(826, 16);
            this.lblSaveInstructions.TabIndex = 28;
            this.lblSaveInstructions.Text = "PLEASE SELECT THE JOURNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION F" +
    "OR THE JOURNAL, AND CLICK THE OK BUTTON";
            this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblSaveInstructions, null);
            // 
            // fraReturnMultipleChecks
            // 
            this.fraReturnMultipleChecks.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraReturnMultipleChecks.AppearanceKey = "groupBoxNoBorders";
            this.fraReturnMultipleChecks.BackColor = System.Drawing.Color.White;
            this.fraReturnMultipleChecks.Controls.Add(this.txtJournalDesc);
            this.fraReturnMultipleChecks.Controls.Add(this.cmdClearAll);
            this.fraReturnMultipleChecks.Controls.Add(this.cmdSelectAll);
            this.fraReturnMultipleChecks.Controls.Add(this.vsReturnMultiple);
            this.fraReturnMultipleChecks.Controls.Add(this.chkCreateNewJournal);
            this.fraReturnMultipleChecks.Location = new System.Drawing.Point(30, 30);
            this.fraReturnMultipleChecks.Name = "fraReturnMultipleChecks";
            this.fraReturnMultipleChecks.Size = new System.Drawing.Size(862, 487);
            this.fraReturnMultipleChecks.TabIndex = 19;
            this.fraReturnMultipleChecks.Text = "Return Multiple Checks";
            this.ToolTip1.SetToolTip(this.fraReturnMultipleChecks, null);
            this.fraReturnMultipleChecks.Visible = false;
            // 
            // txtJournalDesc
            // 
            this.txtJournalDesc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.txtJournalDesc.BackColor = System.Drawing.SystemColors.Window;
            this.txtJournalDesc.Location = new System.Drawing.Point(348, 375);
            this.txtJournalDesc.MaxLength = 25;
            this.txtJournalDesc.Name = "txtJournalDesc";
            this.txtJournalDesc.Size = new System.Drawing.Size(330, 40);
            this.txtJournalDesc.TabIndex = 33;
            this.ToolTip1.SetToolTip(this.txtJournalDesc, null);
            // 
            // cmdClearAll
            // 
            this.cmdClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdClearAll.AppearanceKey = "actionButton";
            this.cmdClearAll.Location = new System.Drawing.Point(307, 437);
            this.cmdClearAll.Name = "cmdClearAll";
            this.cmdClearAll.Size = new System.Drawing.Size(110, 40);
            this.cmdClearAll.TabIndex = 35;
            this.cmdClearAll.Text = "Clear All";
            this.ToolTip1.SetToolTip(this.cmdClearAll, null);
            this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(185, 437);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(110, 40);
            this.cmdSelectAll.TabIndex = 34;
            this.cmdSelectAll.Text = "Select All";
            this.ToolTip1.SetToolTip(this.cmdSelectAll, null);
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // vsReturnMultiple
            // 
            this.vsReturnMultiple.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsReturnMultiple.Cols = 5;
            this.vsReturnMultiple.FixedCols = 0;
            this.vsReturnMultiple.Location = new System.Drawing.Point(0, 30);
            this.vsReturnMultiple.Name = "vsReturnMultiple";
            this.vsReturnMultiple.RowHeadersVisible = false;
            this.vsReturnMultiple.Rows = 1;
            this.vsReturnMultiple.Size = new System.Drawing.Size(842, 315);
            this.vsReturnMultiple.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.vsReturnMultiple, null);
            this.vsReturnMultiple.Click += new System.EventHandler(this.vsReturnMultiple_ClickEvent);
            this.vsReturnMultiple.KeyDown += new Wisej.Web.KeyEventHandler(this.vsReturnMultiple_KeyDownEvent);
            // 
            // chkCreateNewJournal
            // 
            this.chkCreateNewJournal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.chkCreateNewJournal.Checked = true;
            this.chkCreateNewJournal.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkCreateNewJournal.Location = new System.Drawing.Point(0, 380);
            this.chkCreateNewJournal.Name = "chkCreateNewJournal";
            this.chkCreateNewJournal.Size = new System.Drawing.Size(327, 27);
            this.chkCreateNewJournal.TabIndex = 31;
            this.chkCreateNewJournal.Text = "Create New Journal for Returned Checks";
            this.ToolTip1.SetToolTip(this.chkCreateNewJournal, "Select to create a new unprocessed AP journal for the voided checks");
            this.chkCreateNewJournal.CheckedChanged += new System.EventHandler(this.chkCreateNewJournal_CheckedChanged);
            // 
            // fraMultipleChecks
            // 
            this.fraMultipleChecks.Controls.Add(this.cmdGetCheck);
            this.fraMultipleChecks.Controls.Add(this.cmdReturnCheck);
            this.fraMultipleChecks.Controls.Add(this.vsMultipleChecks);
            this.fraMultipleChecks.Location = new System.Drawing.Point(30, 30);
            this.fraMultipleChecks.Name = "fraMultipleChecks";
            this.fraMultipleChecks.Size = new System.Drawing.Size(862, 403);
            this.fraMultipleChecks.TabIndex = 12;
            this.fraMultipleChecks.Text = "Multiple Checks";
            this.ToolTip1.SetToolTip(this.fraMultipleChecks, null);
            this.fraMultipleChecks.Visible = false;
            // 
            // cmdGetCheck
            // 
            this.cmdGetCheck.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdGetCheck.AppearanceKey = "actionButton";
            this.cmdGetCheck.ForeColor = System.Drawing.Color.White;
            this.cmdGetCheck.Location = new System.Drawing.Point(258, 343);
            this.cmdGetCheck.Name = "cmdGetCheck";
            this.cmdGetCheck.Size = new System.Drawing.Size(151, 40);
            this.cmdGetCheck.TabIndex = 15;
            this.cmdGetCheck.Text = "Retrieve Record";
            this.ToolTip1.SetToolTip(this.cmdGetCheck, null);
            this.cmdGetCheck.Click += new System.EventHandler(this.cmdGetCheck_Click);
            // 
            // cmdReturnCheck
            // 
            this.cmdReturnCheck.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdReturnCheck.AppearanceKey = "actionButton";
            this.cmdReturnCheck.ForeColor = System.Drawing.Color.White;
            this.cmdReturnCheck.Location = new System.Drawing.Point(20, 343);
            this.cmdReturnCheck.Name = "cmdReturnCheck";
            this.cmdReturnCheck.Size = new System.Drawing.Size(232, 40);
            this.cmdReturnCheck.TabIndex = 14;
            this.cmdReturnCheck.Text = "Return to Search Screen";
            this.ToolTip1.SetToolTip(this.cmdReturnCheck, null);
            this.cmdReturnCheck.Click += new System.EventHandler(this.cmdReturnCheck_Click);
            // 
            // vsMultipleChecks
            // 
            this.vsMultipleChecks.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsMultipleChecks.Cols = 4;
            this.vsMultipleChecks.FixedCols = 0;
            this.vsMultipleChecks.Location = new System.Drawing.Point(20, 30);
            this.vsMultipleChecks.Name = "vsMultipleChecks";
            this.vsMultipleChecks.RowHeadersVisible = false;
            this.vsMultipleChecks.Rows = 1;
            this.vsMultipleChecks.Size = new System.Drawing.Size(822, 293);
            this.vsMultipleChecks.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.vsMultipleChecks, null);
            this.vsMultipleChecks.DoubleClick += new System.EventHandler(this.vsMultipleChecks_DblClick);
            this.vsMultipleChecks.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsMultipleChecks_KeyPressEvent);
            // 
            // txtCheckDate
            // 
            this.txtCheckDate.Location = new System.Drawing.Point(175, 126);
            this.txtCheckDate.MaxLength = 10;
            this.txtCheckDate.Name = "txtCheckDate";
            this.txtCheckDate.Size = new System.Drawing.Size(115, 40);
            this.txtCheckDate.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtCheckDate, null);
            this.txtCheckDate.Visible = false;
            // 
            // cmdGetAccountNumber
            // 
            this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
            this.cmdGetAccountNumber.Location = new System.Drawing.Point(360, 30);
            this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
            this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdGetAccountNumber.Size = new System.Drawing.Size(100, 48);
            this.cmdGetAccountNumber.TabIndex = 1;
            this.cmdGetAccountNumber.Text = "Process";
            this.ToolTip1.SetToolTip(this.cmdGetAccountNumber, null);
            this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // txtCheck
            // 
            this.txtCheck.Location = new System.Drawing.Point(175, 126);
            this.txtCheck.MaxLength = 10;
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(142, 40);
            this.txtCheck.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtCheck, null);
            // 
            // lblCheckNumber
            // 
            this.lblCheckNumber.Location = new System.Drawing.Point(30, 140);
            this.lblCheckNumber.Name = "lblCheckNumber";
            this.lblCheckNumber.Size = new System.Drawing.Size(52, 20);
            this.lblCheckNumber.TabIndex = 6;
            this.lblCheckNumber.Text = "CHECK #";
            this.ToolTip1.SetToolTip(this.lblCheckNumber, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(506, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "SELECT THE TYPE OF CORRECTION TO PERFORM AND THEN ENTER THE CHECK NUMBER";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // lblCheckDate
            // 
            this.lblCheckDate.Location = new System.Drawing.Point(30, 140);
            this.lblCheckDate.Name = "lblCheckDate";
            this.lblCheckDate.Size = new System.Drawing.Size(80, 18);
            this.lblCheckDate.TabIndex = 17;
            this.lblCheckDate.Text = "CHECK DATE";
            this.ToolTip1.SetToolTip(this.lblCheckDate, null);
            this.lblCheckDate.Visible = false;
            // 
            // frmGetGJCorrDataEntry
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(922, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmGetGJCorrDataEntry";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "AP Corrections ";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmGetGJCorrDataEntry_Load);
            this.Activated += new System.EventHandler(this.frmGetGJCorrDataEntry_Activated);
            this.Resize += new System.EventHandler(this.frmGetGJCorrDataEntry_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetGJCorrDataEntry_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
            this.fraJournalSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReturnMultipleChecks)).EndInit();
            this.fraReturnMultipleChecks.ResumeLayout(false);
            this.fraReturnMultipleChecks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnMultiple)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateNewJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMultipleChecks)).EndInit();
            this.fraMultipleChecks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturnCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMultipleChecks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
