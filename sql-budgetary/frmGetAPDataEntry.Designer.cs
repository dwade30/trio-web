﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetAPDataEntry.
	/// </summary>
	partial class frmGetAPDataEntry : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCComboBox cboEntry;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblLastAccount;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetAPDataEntry));
            this.cmbSearchType = new fecherFoundation.FCComboBox();
            this.lblSearchType = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.vs1 = new fecherFoundation.FCGrid();
            this.cmdGet = new fecherFoundation.FCButton();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.lblSearchInfo = new fecherFoundation.FCLabel();
            this.cboEntry = new fecherFoundation.FCComboBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblLastAccount = new fecherFoundation.FCLabel();
            this.cmdGetAccountNumber = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.btnUpdateJournalInformation = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpdateJournalInformation)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
            this.BottomPanel.Location = new System.Drawing.Point(0, 536);
            this.BottomPanel.Size = new System.Drawing.Size(677, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cboEntry);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblLastAccount);
            this.ClientArea.Size = new System.Drawing.Size(677, 476);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnUpdateJournalInformation);
            this.TopPanel.Size = new System.Drawing.Size(677, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnUpdateJournalInformation, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(162, 30);
            this.HeaderText.Text = "Get Invoice Entry";
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.AutoSize = false;
            this.cmbSearchType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSearchType.FormattingEnabled = true;
            this.cmbSearchType.Items.AddRange(new object[] {
            "Journal Number",
            "Vendor Number",
            "Vendor Name",
            "Description ",
            "Reference"});
            this.cmbSearchType.Location = new System.Drawing.Point(20, 66);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(212, 40);
            this.cmbSearchType.TabIndex = 1;
            this.cmbSearchType.SelectedIndexChanged += new System.EventHandler(this.optSearchType_MouseDown);
            // 
            // lblSearchType
            // 
            this.lblSearchType.Location = new System.Drawing.Point(20, 30);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(70, 16);
            this.lblSearchType.TabIndex = 0;
            this.lblSearchType.Text = "SEARCH BY";
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.White;
            this.Frame3.Controls.Add(this.vs1);
            this.Frame3.Controls.Add(this.cmdGet);
            this.Frame3.Controls.Add(this.cmdReturn);
            this.Frame3.Location = new System.Drawing.Point(30, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(631, 400);
            this.Frame3.TabIndex = 6;
            this.Frame3.Text = "Multiple Records";
            this.Frame3.Visible = false;
            // 
            // vs1
            // 
            this.vs1.AllowSelection = false;
            this.vs1.AllowUserToResizeColumns = false;
            this.vs1.AllowUserToResizeRows = false;
            this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
            this.vs1.BackColorBkg = System.Drawing.Color.Empty;
            this.vs1.BackColorFixed = System.Drawing.Color.Empty;
            this.vs1.BackColorSel = System.Drawing.Color.Empty;
            this.vs1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vs1.Cols = 7;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vs1.ColumnHeadersHeight = 30;
            this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
            this.vs1.DragIcon = null;
            this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vs1.FixedCols = 0;
            this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
            this.vs1.FrozenCols = 0;
            this.vs1.GridColor = System.Drawing.Color.Empty;
            this.vs1.GridColorFixed = System.Drawing.Color.Empty;
            this.vs1.Location = new System.Drawing.Point(20, 30);
            this.vs1.Name = "vs1";
            this.vs1.OutlineCol = 0;
            this.vs1.ReadOnly = true;
            this.vs1.RowHeadersVisible = false;
            this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vs1.RowHeightMin = 0;
            this.vs1.Rows = 50;
            this.vs1.ScrollTipText = null;
            this.vs1.ShowColumnVisibilityMenu = false;
            this.vs1.Size = new System.Drawing.Size(591, 293);
            this.vs1.StandardTab = true;
            this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vs1.TabIndex = 0;
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
            this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // cmdGet
            // 
            this.cmdGet.AppearanceKey = "actionButton";
            this.cmdGet.Location = new System.Drawing.Point(248, 343);
            this.cmdGet.Name = "cmdGet";
            this.cmdGet.Size = new System.Drawing.Size(183, 40);
            this.cmdGet.TabIndex = 2;
            this.cmdGet.Text = "Retrieve Record";
            this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(20, 343);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(208, 40);
            this.cmdReturn.TabIndex = 1;
            this.cmdReturn.Text = "Return to Search Screen";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmdClear);
            this.Frame2.Controls.Add(this.cmbSearchType);
            this.Frame2.Controls.Add(this.lblSearchType);
            this.Frame2.Controls.Add(this.cmdSearch);
            this.Frame2.Controls.Add(this.txtSearch);
            this.Frame2.Controls.Add(this.lblSearchInfo);
            this.Frame2.Location = new System.Drawing.Point(30, 162);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(252, 282);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Update";
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.Location = new System.Drawing.Point(20, 222);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(114, 40);
            this.cmdClear.TabIndex = 4;
            this.cmdClear.Text = "Clear Search";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(154, 222);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(78, 40);
            this.cmdSearch.TabIndex = 5;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = false;
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            clientEvent1.Event = "keypress";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.txtSearch).Add(clientEvent1);
            this.txtSearch.LinkItem = null;
            this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSearch.LinkTopic = null;
            this.txtSearch.Location = new System.Drawing.Point(20, 162);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(212, 40);
            this.txtSearch.TabIndex = 3;
            this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            // 
            // lblSearchInfo
            // 
            this.lblSearchInfo.Location = new System.Drawing.Point(20, 126);
            this.lblSearchInfo.Name = "lblSearchInfo";
            this.lblSearchInfo.Size = new System.Drawing.Size(148, 16);
            this.lblSearchInfo.TabIndex = 2;
            this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
            // 
            // cboEntry
            // 
            this.cboEntry.AutoSize = false;
            this.cboEntry.BackColor = System.Drawing.SystemColors.Window;
            this.cboEntry.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboEntry.FormattingEnabled = true;
            this.cboEntry.Location = new System.Drawing.Point(281, 102);
            this.cboEntry.Name = "cboEntry";
            this.cboEntry.Size = new System.Drawing.Size(330, 40);
            this.cboEntry.TabIndex = 4;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 116);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(215, 16);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "ENTRIES WILL BE ADDED TO JOURNAL";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 66);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(402, 16);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "SELECT A JOURNAL NUMBER OR ENTER 0 TO CREATE A NEW JOURNAL";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(170, 16);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "LAST JOURNAL ACCESSED";
            // 
            // lblLastAccount
            // 
            this.lblLastAccount.Location = new System.Drawing.Point(281, 30);
            this.lblLastAccount.Name = "lblLastAccount";
            this.lblLastAccount.Size = new System.Drawing.Size(70, 16);
            this.lblLastAccount.TabIndex = 1;
            // 
            // cmdGetAccountNumber
            // 
            this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
            this.cmdGetAccountNumber.Location = new System.Drawing.Point(272, 30);
            this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
            this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdGetAccountNumber.Size = new System.Drawing.Size(95, 48);
            this.cmdGetAccountNumber.TabIndex = 0;
            this.cmdGetAccountNumber.Text = "Process";
            this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // btnUpdateJournalInformation
            // 
            this.btnUpdateJournalInformation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnUpdateJournalInformation.AppearanceKey = "toolbarButton";
            this.btnUpdateJournalInformation.Location = new System.Drawing.Point(868, 29); ;
            this.btnUpdateJournalInformation.Name = "btnUpdateJournalInformation";
            this.btnUpdateJournalInformation.Size = new System.Drawing.Size(180, 24);
            this.btnUpdateJournalInformation.TabIndex = 2;
            this.btnUpdateJournalInformation.Text = "Update Journal Information";
            this.btnUpdateJournalInformation.Click += new System.EventHandler(this.btnUpdateJournalInformation_Click);
            // 
            // frmGetAPDataEntry
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(677, 644);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmGetAPDataEntry";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Get Invoice Entry";
            this.Load += new System.EventHandler(this.frmGetAPDataEntry_Load);
            this.Activated += new System.EventHandler(this.frmGetAPDataEntry_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetAPDataEntry_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetAPDataEntry_KeyPress);
            this.Resize += new System.EventHandler(this.frmGetAPDataEntry_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpdateJournalInformation)).EndInit();
            this.ResumeLayout(false);

		}

        


        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdGetAccountNumber;
		private JavaScript javaScript1;
        private FCButton btnUpdateJournalInformation;
    }
}
