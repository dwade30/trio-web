﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptOutstandingEncumbrances.
	/// </summary>
	public partial class rptOutstandingEncumbrances : BaseSectionReport
	{
		public static rptOutstandingEncumbrances InstancePtr
		{
			get
			{
				return (rptOutstandingEncumbrances)Sys.GetInstance(typeof(rptOutstandingEncumbrances));
			}
		}

		protected rptOutstandingEncumbrances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOutstandingEncumbrances	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;

		public rptOutstandingEncumbrances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outstanding Encumbrance List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			PageCounter = 0;
			curTotal = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsInfo.OpenRecordset("SELECT *, EncumbranceDetail.Amount as EncumbranceDetailAmount, EncumbranceDetail.Adjustments as EncumbranceDetailAdjustments, EncumbranceDetail.Liquidated as EncumbranceDetailLiquidated, EncumbranceDetail.Description as EncumbranceDetailDescription FROM EncumbranceDetail INNER JOIN Encumbrances ON EncumbranceDetail.EncumbranceID = Encumbrances.ID WHERE Status = 'P' AND IsNull(Project, '') <> 'CTRL' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No encumbrance information found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"));
			fldDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				fldVendor.Text = Strings.Format(rsInfo.Get_Fields_Int32("VendorNumber"), "00000") + " " + rsVendorInfo.Get_Fields_String("CheckName");
			}
			else
			{
				fldVendor.Text = Strings.Format(rsInfo.Get_Fields_Int32("VendorNumber"), "00000") + " UNKNOWN";
			}
			// TODO Get_Fields: Field [EncumbranceDetailDescription] not found!! (maybe it is an alias?)
			fldDescription.Text = FCConvert.ToString(rsInfo.Get_Fields("EncumbranceDetailDescription"));
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			fldReference.Text = FCConvert.ToString(rsInfo.Get_Fields("PO"));
			// TODO Get_Fields: Field [EncumbranceDetailAmount] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EncumbranceDetailAdjustments] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EncumbranceDetailLiquidated] not found!! (maybe it is an alias?)
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields("EncumbranceDetailAmount") + rsInfo.Get_Fields("EncumbranceDetailAdjustments") - rsInfo.Get_Fields("EncumbranceDetailLiquidated"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rsInfo.Get_Fields("Account"));
			// TODO Get_Fields: Field [EncumbranceDetailAmount] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EncumbranceDetailAdjustments] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EncumbranceDetailLiquidated] not found!! (maybe it is an alias?)
			curTotal += rsInfo.Get_Fields("EncumbranceDetailAmount") + rsInfo.Get_Fields("EncumbranceDetailAdjustments") - rsInfo.Get_Fields("EncumbranceDetailLiquidated");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
