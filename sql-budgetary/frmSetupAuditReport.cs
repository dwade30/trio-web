﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupAuditReport.
	/// </summary>
	public partial class frmSetupAuditReport : BaseForm
	{
		public frmSetupAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: add code from Load
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadBankCombo();
			LoadScreenCombo();
			//FC:FINAL:ASZ: default selection - All
			cmbBanksAll.SelectedIndex = 0;
			cmbScreenAll.SelectedIndex = 0;
			cmbDateAll.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupAuditReport InstancePtr
		{
			get
			{
				return (frmSetupAuditReport)Sys.GetInstance(typeof(frmSetupAuditReport));
			}
		}

		protected frmSetupAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmSetupAuditReport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAuditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAuditReport.FillStyle	= 0;
			//frmSetupAuditReport.ScaleWidth	= 9045;
			//frmSetupAuditReport.ScaleHeight	= 7215;
			//frmSetupAuditReport.LinkTopic	= "Form2";
			//frmSetupAuditReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//FC:FINAL:ASZ: moved code to Constructor
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
			//LoadBankCombo();
			//LoadScreenCombo();
		}

		private void frmSetupAuditReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptAuditReport.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			rptAuditReport.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadBankCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			cboBanks.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT UserField1 FROM AuditChanges ORDER BY UserField1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsBankInfo.OpenRecordset("SELECT * FROM Banks");
				do
				{
					if (rsBankInfo.FindFirstRecord("ID", Conversion.Val(rsInfo.Get_Fields_String("UserField1"))))
					{
						cboBanks.AddItem(rsInfo.Get_Fields_String("UserField1") + " - " + Strings.Trim(FCConvert.ToString(rsBankInfo.Get_Fields_String("Name"))));
					}
					else
					{
						cboBanks.AddItem(rsInfo.Get_Fields_String("UserField1") + " - UNKNOWN");
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadScreenCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboScreen.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT Location FROM AuditChanges ORDER BY Location");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboScreen.AddItem(rsInfo.Get_Fields_String("Location"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void optDateAll_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: 
			//fraDateRange.Enabled = false;
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//Label2.Enabled = false;
			//txtLowDate.Enabled = false;
			//txtHighDate.Enabled = false;
			//txtLowDate.Text = "";
			//txtHighDate.Text = "";
			if (sender == cmbDateAll)
			{
				switch (cmbDateAll.SelectedIndex)
				{
					case 0:
						{
							Label2.Enabled = false;
							txtLowDate.Enabled = false;
							txtHighDate.Enabled = false;
							txtLowDate.Text = "";
							txtHighDate.Text = "";
							break;
						}
					case 1:
						{
							optDateRange_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void optDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: 
			//fraDateRange.Enabled = true;
			Label2.Enabled = true;
			txtLowDate.Enabled = true;
			txtHighDate.Enabled = true;
		}

		private void optBanksAll_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: 
			//fraSelectedBank.Enabled = false;
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//cboBanks.Enabled = false;
			//cboBanks.SelectedIndex = -1;
			if (sender == cmbBanksAll)
			{
				switch (cmbBanksAll.SelectedIndex)
				{
					case 0:
						{
							cboBanks.Enabled = false;
							cboBanks.SelectedIndex = -1;
							break;
						}
					case 1:
						{
							optBanksSelected_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void optBanksSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: 
			//fraSelectedBank.Enabled = true;
			cboBanks.Enabled = true;
			if (cboBanks.Items.Count > 0)
			{
				cboBanks.SelectedIndex = 0;
			}
		}

		private void optScreenAll_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: 
			//fraSelectedScreen.Enabled = false;            
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//cboScreen.Enabled = false;
			//cboScreen.SelectedIndex = -1;
			if (sender == cmbScreenAll)
			{
				switch (cmbScreenAll.SelectedIndex)
				{
					case 0:
						{
							cboScreen.Enabled = false;
							cboScreen.SelectedIndex = -1;
							break;
						}
					case 1:
						{
							optScreenSelected_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void optScreenSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: 
			//fraSelectedScreen.Enabled = true;            
			cboScreen.Enabled = true;
			if (cboScreen.Items.Count > 0)
			{
				cboScreen.SelectedIndex = 0;
			}
		}
	}
}
