﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEditReportSetup.
	/// </summary>
	public partial class frmEditReportSetup : BaseForm
	{
		public frmEditReportSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbName.SelectedIndex = 0;
            this.txtYear.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditReportSetup InstancePtr
		{
			get
			{
				return (frmEditReportSetup)Sys.GetInstance(typeof(frmEditReportSetup));
			}
		}

		protected frmEditReportSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/29/02
		// This screen will be sed to get information for the 1099
		// Edit Report
		// ********************************************************
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(mnuFileExit, new System.EventArgs());
		}

		private void frmEditReportSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtYear.Text = FCConvert.ToString(DateTime.Now.Year);
			txtAmount.Text = "0.00";
			txtYear.SelectionStart = 0;
			txtYear.SelectionLength = 4;
			txtYear.Focus();
			this.Refresh();
		}

		private void frmEditReportSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEditReportSetup_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
            SetupFormTypeCombo();
            cboTaxFormType.SelectedIndex = 0;
        }

        private void SetupFormTypeCombo()
        {
            cboTaxFormType.Items.Clear();
            cboTaxFormType.Items.Add(new GenericDescriptionPair<TaxFormType>() { ID = TaxFormType.All, Description = "All" });
            cboTaxFormType.Items.Add(new GenericDescriptionPair<TaxFormType>() { ID = TaxFormType.MISC1099, Description = "1099 MISC" });
            cboTaxFormType.Items.Add(new GenericDescriptionPair<TaxFormType>() { ID = TaxFormType.NEC1099, Description = "1099 NEC" });
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTest = new clsDRWrapper();
			DialogResult ans;
            var setupInfo = new EditReportSetupInfo();

            var selectedItem = (GenericDescriptionPair<TaxFormType>)cboTaxFormType.SelectedItem;

            setupInfo.FormType = selectedItem.ID;
            setupInfo.MinimumAmount = txtAmount.Text.ToDecimalValue();
            setupInfo.SortOption = cmbName.Text == "Name" ? VendorSortOption.VendorName : VendorSortOption.VendorNumber;
            setupInfo.Year = txtYear.Text.ToIntegerValue();

			rsTest.OpenRecordset("SELECT * FROM VendorMaster WHERE Attorney = 1");
			if (rsTest.EndOfFile() != true && rsTest.BeginningOfFile() != true)
			{
                var report = new rpt1099EditReport(setupInfo);
                frmReportViewer.InstancePtr.Init(report);
			}
			else
			{
				ans = MessageBox.Show("None of your vendors have been selected as being an attorney.  Do you wish to proceed?", "No Attorney", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
                    var report = new rpt1099EditReport(setupInfo);
                    frmReportViewer.InstancePtr.Init(report);
				}
			}
			Close();
		}
    }
}
