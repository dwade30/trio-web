﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmQueryJournal.
	/// </summary>
	public partial class frmQueryJournal : BaseForm
	{
		public frmQueryJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbAll.SelectedIndex = 0;
			cboCompare.SelectedIndex = 0;
			cboWarrantCompare.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmQueryJournal InstancePtr
		{
			get
			{
				return (frmQueryJournal)Sys.GetInstance(typeof(frmQueryJournal));
			}
		}

		protected frmQueryJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			txtWarrant.Text = "";
			txtJournal.Text = "";
			txtVendor.Text = "";
			cboCompare.SelectedIndex = 1;
			txtDescription.Text = "";
			txtReference.Text = "";
			chkE.CheckState = CheckState.Unchecked;
			chkC.CheckState = CheckState.Unchecked;
			chkW.CheckState = CheckState.Unchecked;
			chkP.CheckState = CheckState.Unchecked;
			cboWarrantCompare.Focus();
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void cmdView_Click(object sender, System.EventArgs e)
		{
			string strSearch;
			bool CriteriaFlag;
			bool StatusFlag = false;
			string strStatus = "";
			CriteriaFlag = false;
			strSearch = "SELECT * FROM APJournal WHERE Description <> 'Control Entries' AND ";
			if (chkE.CheckState == CheckState.Checked || chkC.CheckState == CheckState.Checked || chkW.CheckState == CheckState.Checked || chkP.CheckState == CheckState.Checked || chkR.CheckState == CheckState.Checked || chkX.CheckState == CheckState.Checked || chkV.CheckState == CheckState.Checked || cmbAll.SelectedIndex == 0)
			{
				if (txtWarrant.Text != "")
				{
					CriteriaFlag = true;
					if (cboWarrantCompare.SelectedIndex == 0)
					{
						strSearch += "Warrant > '" + FCConvert.ToString(Conversion.Val(txtWarrant.Text)) + "' ";
					}
					else if (cboWarrantCompare.SelectedIndex == 1)
					{
						strSearch += "Warrant = '" + FCConvert.ToString(Conversion.Val(txtWarrant.Text)) + "' ";
					}
					else
					{
						strSearch += "Warrant < '" + FCConvert.ToString(Conversion.Val(txtWarrant.Text)) + "' ";
					}
				}
				if (txtJournal.Text != "")
				{
					if (CriteriaFlag)
					{
						if (cboCompare.SelectedIndex == 0)
						{
							strSearch += "AND JournalNumber > " + FCConvert.ToString(Conversion.Val(txtJournal.Text)) + " ";
						}
						else if (cboCompare.SelectedIndex == 1)
						{
							strSearch += "AND JournalNumber = " + FCConvert.ToString(Conversion.Val(txtJournal.Text)) + " ";
						}
						else
						{
							strSearch += "AND JournalNumber < " + FCConvert.ToString(Conversion.Val(txtJournal.Text)) + " ";
						}
					}
					else
					{
						CriteriaFlag = true;
						if (cboCompare.SelectedIndex == 0)
						{
							strSearch += "JournalNumber > " + FCConvert.ToString(Conversion.Val(txtJournal.Text)) + " ";
						}
						else if (cboCompare.SelectedIndex == 1)
						{
							strSearch += "JournalNumber = " + FCConvert.ToString(Conversion.Val(txtJournal.Text)) + " ";
						}
						else
						{
							strSearch += "JournalNumber < " + FCConvert.ToString(Conversion.Val(txtJournal.Text)) + " ";
						}
					}
				}
				if (txtVendor.Text != "")
				{
					if (CriteriaFlag)
					{
						strSearch += "AND VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)) + " ";
					}
					else
					{
						CriteriaFlag = true;
						strSearch += "VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)) + " ";
					}
				}
				if (txtDescription.Text != "")
				{
					if (CriteriaFlag)
					{
						strSearch += "AND Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "' ";
					}
					else
					{
						CriteriaFlag = true;
						strSearch += "Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "' ";
					}
				}
				if (txtReference.Text != "")
				{
					if (CriteriaFlag)
					{
						strSearch += "AND Reference = '" + modCustomReport.FixQuotes(txtReference.Text) + "' ";
					}
					else
					{
						CriteriaFlag = true;
						strSearch += "Reference = '" + modCustomReport.FixQuotes(txtReference.Text) + "' ";
					}
				}
				strStatus = "";
				if ((chkE.CheckState == CheckState.Checked && chkC.CheckState == CheckState.Checked && chkW.CheckState == CheckState.Checked && chkP.CheckState == CheckState.Checked && chkR.CheckState == CheckState.Checked && chkX.CheckState == CheckState.Checked && chkV.CheckState == CheckState.Checked) || cmbAll.SelectedIndex == 0)
				{
					// do nothing
				}
				else
				{
					if (CriteriaFlag)
					{
						strStatus = "AND ";
					}
					else
					{
						CriteriaFlag = true;
					}
					if (chkE.CheckState == CheckState.Checked)
					{
						StatusFlag = true;
						strStatus += "Status = 'E' ";
					}
					if (chkV.CheckState == CheckState.Checked)
					{
						if (StatusFlag)
						{
							strStatus += "OR Status = 'V' ";
						}
						else
						{
							StatusFlag = true;
							strStatus += "Status = 'V' ";
						}
					}
					if (chkC.CheckState == CheckState.Checked)
					{
						if (StatusFlag)
						{
							strStatus += "OR Status = 'C' ";
						}
						else
						{
							StatusFlag = true;
							strStatus += "Status = 'C' ";
						}
					}
					if (chkW.CheckState == CheckState.Checked)
					{
						if (StatusFlag)
						{
							strStatus += "OR Status = 'W' ";
						}
						else
						{
							StatusFlag = true;
							strStatus += "Status = 'W' ";
						}
					}
					if (chkP.CheckState == CheckState.Checked)
					{
						if (StatusFlag)
						{
							strStatus += "OR Status = 'P' ";
						}
						else
						{
							StatusFlag = true;
							strStatus += "Status = 'P' ";
						}
					}
					if (chkR.CheckState == CheckState.Checked)
					{
						if (StatusFlag)
						{
							strStatus += "OR Status = 'R' ";
						}
						else
						{
							StatusFlag = true;
							strStatus += "Status = 'R' ";
						}
					}
					if (chkX.CheckState == CheckState.Checked)
					{
						if (StatusFlag)
						{
							strStatus += "OR Status = 'X' ";
						}
						else
						{
							StatusFlag = true;
							strStatus += "Status = 'X' ";
						}
					}
					strSearch += strStatus;
				}
				if (CriteriaFlag == false)
				{
					strSearch = "SELECT * FROM APJournal WHERE Description <> 'Control Entries' ORDER BY JournalNumber, VendorNumber";
				}
				else
				{
					strSearch += "ORDER BY JournalNumber, VendorNumber";
				}
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset(strSearch);
				if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
				{
					// FC: FINAL: KV: IIT807 + FC - 8697
					this.Hide();
					modBudgetaryAccounting.Statics.SearchResults.MoveLast();
					modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
					cmdClear_Click();
					frmViewJournals.InstancePtr.Show(App.MainForm);
				}
				else
				{
					MessageBox.Show("No Records Found that Match the Criteria", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("You must have at least 1 status checked before you can view results", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void frmQueryJournal_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			cboWarrantCompare.SelectedIndex = 1;
			cboCompare.SelectedIndex = 1;
			cboWarrantCompare.Focus();
			this.Refresh();
		}

		private void frmQueryJournal_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmQueryJournal.FillStyle	= 0;
			//frmQueryJournal.ScaleWidth	= 9045;
			//frmQueryJournal.ScaleHeight	= 7275;
			//frmQueryJournal.LinkTopic	= "Form2";
			//frmQueryJournal.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmQueryJournal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			chkC.CheckState = CheckState.Unchecked;
			chkE.CheckState = CheckState.Unchecked;
			chkP.CheckState = CheckState.Unchecked;
			chkW.CheckState = CheckState.Unchecked;
			chkV.CheckState = CheckState.Unchecked;
			chkR.CheckState = CheckState.Unchecked;
			chkX.CheckState = CheckState.Unchecked;
			fraSelectStatus.Enabled = false;
		}

		private void optSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectStatus.Enabled = true;
		}

		private void txtJournal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtJournal.Text != "")
			{
				if (!Information.IsNumeric(txtJournal.Text))
				{
					MessageBox.Show("You may only enter a number in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtJournal.Text = "";
				}
				else
				{
					temp = txtJournal.Text;
					txtJournal.Text = modValidateAccount.GetFormat_6(temp, 4);
				}
			}
		}

		private void txtReference_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtReference.Text != "")
			{
				if (!Information.IsNumeric(txtReference.Text))
				{
					MessageBox.Show("You may only enter a number in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtReference.Text = "";
				}
			}
		}

		private void txtVendor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtVendor.Text != "")
			{
				if (!Information.IsNumeric(txtVendor.Text))
				{
					MessageBox.Show("You may only enter a number in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtVendor.Text = "";
				}
				else
				{
					temp = txtVendor.Text;
					txtVendor.Text = modValidateAccount.GetFormat_6(temp, 4);
				}
			}
		}

		private void txtWarrant_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtWarrant.Text != "")
			{
				if (!Information.IsNumeric(txtWarrant.Text))
				{
					MessageBox.Show("You may only enter a number in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtWarrant.Text = "";
				}
				else
				{
					temp = txtWarrant.Text;
					txtWarrant.Text = modValidateAccount.GetFormat_6(temp, 4);
				}
			}
		}

		private void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAll.SelectedIndex == 0)
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.SelectedIndex == 1)
			{
				optSelect_CheckedChanged(sender, e);
			}
		}
	}
}
