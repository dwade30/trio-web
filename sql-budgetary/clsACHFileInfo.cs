﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class clsACHFileInfo
	{
		//=========================================================
		private string strFileName = "";
		private string strEffectiveEntryDate = "";
		private bool boolBalanced;
		private bool boolRegular;
		private bool boolPreNote;
		private bool boolForcePreNote;
		// Private tACHSetup As New clsACHSetup
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHEmployerInfo eInfo = new clsACHEmployerInfo();
		private clsACHEmployerInfo eInfo_AutoInitialized;

		private clsACHEmployerInfo eInfo
		{
			get
			{
				if (eInfo_AutoInitialized == null)
				{
					eInfo_AutoInitialized = new clsACHEmployerInfo();
				}
				return eInfo_AutoInitialized;
			}
			set
			{
				eInfo_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHInfo destInfo = new clsACHInfo();
		private clsACHInfo destInfo_AutoInitialized;

		private clsACHInfo destInfo
		{
			get
			{
				if (destInfo_AutoInitialized == null)
				{
					destInfo_AutoInitialized = new clsACHInfo();
				}
				return destInfo_AutoInitialized;
			}
			set
			{
				destInfo_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHOriginatorInfo origInfo = new clsACHOriginatorInfo();
		private clsACHOriginatorInfo origInfo_AutoInitialized;

		private clsACHOriginatorInfo origInfo
		{
			get
			{
				if (origInfo_AutoInitialized == null)
				{
					origInfo_AutoInitialized = new clsACHOriginatorInfo();
				}
				return origInfo_AutoInitialized;
			}
			set
			{
				origInfo_AutoInitialized = value;
			}
		}

		public bool ForcePreNote
		{
			get
			{
				bool ForcePreNote = false;
				ForcePreNote = boolForcePreNote;
				return ForcePreNote;
			}
			set
			{
				boolForcePreNote = value;
			}
		}

		public bool Regular
		{
			set
			{
				boolRegular = value;
			}
			get
			{
				bool Regular = false;
				Regular = boolRegular;
				return Regular;
			}
		}

		public bool PreNote
		{
			set
			{
				boolPreNote = value;
			}
			get
			{
				bool PreNote = false;
				PreNote = boolPreNote;
				return PreNote;
			}
		}

		public clsACHOriginatorInfo OriginInfo()
		{
			clsACHOriginatorInfo OriginInfo = null;
			OriginInfo = origInfo;
			return OriginInfo;
		}

		public clsACHInfo DestinationInfo()
		{
			clsACHInfo DestinationInfo = null;
			DestinationInfo = destInfo;
			return DestinationInfo;
		}

		public clsACHEmployerInfo EmployerInfo()
		{
			clsACHEmployerInfo EmployerInfo = null;
			EmployerInfo = eInfo;
			return EmployerInfo;
		}

		public string FileName
		{
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
			set
			{
				strFileName = value;
			}
		}

		public string EffectiveEntryDate
		{
			get
			{
				string EffectiveEntryDate = "";
				EffectiveEntryDate = strEffectiveEntryDate;
				return EffectiveEntryDate;
			}
			set
			{
				strEffectiveEntryDate = value;
			}
		}

		public bool Balanced
		{
			get
			{
				bool Balanced = false;
				Balanced = boolBalanced;
				return Balanced;
			}
			set
			{
				boolBalanced = value;
			}
		}
	}
}
