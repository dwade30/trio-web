﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpenseDetailVendorSummary.
	/// </summary>
	public partial class frmExpenseDetailVendorSummary : BaseForm
	{
		public frmExpenseDetailVendorSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExpenseDetailVendorSummary InstancePtr
		{
			get
			{
				return (frmExpenseDetailVendorSummary)Sys.GetInstance(typeof(frmExpenseDetailVendorSummary));
			}
		}

		protected frmExpenseDetailVendorSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		clsDRWrapper rs4 = new clsDRWrapper();
		clsDRWrapper rs5 = new clsDRWrapper();
		clsDRWrapper rs6 = new clsDRWrapper();
		clsDRWrapper rs7 = new clsDRWrapper();
		clsDRWrapper UsedAccounts = new clsDRWrapper();
		bool NetBudgetFlag;
		bool YTDDCFlag;
		bool YTDNetFlag;
		bool BalanceFlag;
		bool PendingDetailFlag;
		bool PendingSummaryFlag;
		int CurrentRow;
		string CurrentDepartment = "";
		string CurrentExpense = "";
		string CurrentDivision = "";
		string CurrentObject = "";
		int CurrentCol;
		int PostedCol;
		int TransCol;
		int PeriodCol;
		int RCBCol;
		int JournalCol;
		int DescriptionCol;
		int WarrantCol;
		int CheckCol;
		int VendorCol;
		int NetBudgetCol;
		int YTDDebitCol;
		int YTDCreditCol;
		int YTDNetCol;
		int BalanceCol;
		int EncumbranceCol;
		int PendingCol;
		string strPeriodCheck;
		string[] strBalances = null;
		bool blnRunCollapseEvent;
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		clsDRWrapper rsDeptSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsDivSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsExpSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsObjSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsDeptBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsDivBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsExpBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsObjBudgetInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curNonActivityTotals As Decimal	OnWrite(short, Decimal)
		Decimal[] curNonActivityTotals = new Decimal[3 + 1];

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptExpenseDetailVendorSummary.InstancePtr);
		}

		private void frmExpenseDetailVendorSummary_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			clsDRWrapper Descriptions = new clsDRWrapper();
			int LMonth = 0;
			int HMonth = 0;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rs.OpenRecordset("SELECT * FROM ExpenseDetailFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			lblTitle.Text = "Department(s)";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				lblRangeDept.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp");
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
			{
				lblTitle.Text = "Accounts";
				lblRangeDept.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount");
			}
			else
			{
				lblRangeDept.Text = "ALL";
			}
			// lblMonthLabel.Visible = True
			strPeriodCheck = "AND";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Building Report", true);
			frmWait.InstancePtr.Show();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnFromPreview)
			{
				FormatGrid();
				frmWait.InstancePtr.prgProgress.Value = 20;
				frmWait.InstancePtr.Refresh();
				PrepareTemp();
				frmWait.InstancePtr.prgProgress.Value = 40;
				frmWait.InstancePtr.Refresh();
				// troges126
				modBudgetaryAccounting.CalculateAccountInfo();
				// CalculateAccountInfo False, True, True, "E"
				// frmWait.prgProgress.Value = 60
				// frmWait.Refresh
				// CalculateAccountInfo True, True, True, "E"
				frmWait.InstancePtr.prgProgress.Value = 80;
				frmWait.InstancePtr.Refresh();
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
				{
					// rsDeptSummaryInfo.OpenRecordset "SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department"
					// rsDivSummaryInfo.OpenRecordset "SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department, Division"
					// rsExpSummaryInfo.OpenRecordset "SELECT Department, Division, Expense, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department, Division, Expense"
					// rsObjSummaryInfo.OpenRecordset "SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department, Division, Expense, Object"
					rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period < 0 GROUP BY Department");
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						rsExpSummaryInfo.OpenRecordset("SELECT Department, Expense, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period < 0 GROUP BY Department, Expense");
						rsObjSummaryInfo.OpenRecordset("SELECT Department, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period < 0 GROUP BY Department, Expense, Object");
					}
					else
					{
						rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period < 0 GROUP BY Department, Division");
						rsExpSummaryInfo.OpenRecordset("SELECT Department, Division, Expense, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period < 0 GROUP BY Department, Division, Expense");
						rsObjSummaryInfo.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period < 0 GROUP BY Department, Division, Expense, Object");
					}
					if (PendingDetailFlag)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM ExpenseDetailInfo ORDER BY Account, VendorNumber");
					}
					else
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE Status = 'P' ORDER BY Account, VendorNumber");
					}
				}
				else
				{
					LMonth = modBudgetaryMaster.Statics.FirstMonth;
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) == LMonth)
					{
						HMonth = -1;
						LMonth = -1;
					}
					else
					{
						if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) == 1)
						{
							HMonth = 12;
						}
						else
						{
							HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						}
					}
					if (LMonth > HMonth)
					{
						strPeriodCheck = "OR";
					}
					else
					{
						strPeriodCheck = "AND";
					}
					rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " GROUP BY Department");
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						rsExpSummaryInfo.OpenRecordset("SELECT Department, Expense, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " GROUP BY Department, Expense");
						rsObjSummaryInfo.OpenRecordset("SELECT Department, Expense, Object, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " GROUP BY Department, Expense, Object");
					}
					else
					{
						rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " GROUP BY Department, Division");
						rsExpSummaryInfo.OpenRecordset("SELECT Department, Division, Expense, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " GROUP BY Department, Division, Expense");
						rsObjSummaryInfo.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " GROUP BY Department, Division, Expense, Object");
					}
					LMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
					}
					else
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
					}
					if (LMonth > HMonth)
					{
						strPeriodCheck = "OR";
					}
					else
					{
						strPeriodCheck = "AND";
					}
					if (PendingDetailFlag)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " ORDER BY Account, VendorNumber");
					}
					else
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Status = 'P' ORDER BY Account, VendorNumber");
					}
				}
				rsDeptBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department");
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					rsExpBudgetInfo.OpenRecordset("SELECT Department, Expense, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Expense");
					rsObjBudgetInfo.OpenRecordset("SELECT Department, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Expense, Object");
				}
				else
				{
					rsDivBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division");
					rsExpBudgetInfo.OpenRecordset("SELECT Department, Division, Expense, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division, Expense");
					rsObjBudgetInfo.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division, Expense, Object");
				}
				DepartmentReport();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Calculating Totals";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				FillInInformation();
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				GetRowSubtotals();
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				GetCompleteTotals();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Formatting Report";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				SetColors();
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.RowHeightMax = 250;
					//FC:FINAL:BBE:#528 - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 8);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					//FC:FINAL:BBE:#528 - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 10);
				}
				else
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_String("FontName"));
					//FC:FINAL:BBE:#528 - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields("FontSize"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Bold"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontItalic, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Italic"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontStrikethru, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("StrikeThru"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontUnderline, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Underline"));
					vs1.AutoSize(1, vs1.Cols - 1, false, 300);
					vs1.ExtendLastCol = false;
				}
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				blnRunCollapseEvent = false;
				for (counter = 4; counter >= 0; counter--)
				{
					for (counter2 = 2; counter2 <= vs1.Rows - 1; counter2++)
					{
						//Application.DoEvents();
						if (vs1.RowOutlineLevel(counter2) == counter)
						{
							if (vs1.IsSubtotal(counter2))
							{
								vs1.IsCollapsed(counter2, FCGrid.CollapsedSettings.flexOutlineCollapsed);
							}
						}
					}
				}
				blnRunCollapseEvent = true;
				vs1_Collapsed();
			}
			else
			{
				modBudgetaryMaster.Statics.blnFromPreview = false;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Use")) == "P")
			{
				cmdPrint.Enabled = true;
			}
			this.Refresh();
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Unload();
			vs1.Visible = true;
			lblTitle.Visible = true;
			lblRangeDept.Visible = true;
			lblMonthLabel.Visible = true;
			lblMonths.Visible = true;
		}

		private void frmExpenseDetailVendorSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExpenseDetailVendorSummary.FillStyle	= 0;
			//frmExpenseDetailVendorSummary.ScaleWidth	= 9480;
			//frmExpenseDetailVendorSummary.ScaleHeight	= 7530;
			//frmExpenseDetailVendorSummary.AutoRedraw	= -1  'True;
			//frmExpenseDetailVendorSummary.LinkTopic	= "Form2";
			//frmExpenseDetailVendorSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DeleteTemp();
			frmExpenseDetailSelect.InstancePtr.Show(App.MainForm);
		}

		private void frmExpenseDetailVendorSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptExpenseDetailVendorSummary.InstancePtr);
		}

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			//rptExpenseDetailVendorSummary.InstancePtr.Hide();
			modDuplexPrinting.DuplexPrintReport(rptExpenseDetailVendorSummary.InstancePtr);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool FirstFlag = false;
			bool SecondFlag = false;
			bool ThirdFlag = false;
			bool FourthFlag = false;
			bool FifthFlag;
			int counter2;
			// vbPorter upgrade warning: TempInfo As Decimal	OnWrite(short, string)
			Decimal TempInfo;
			int TempRow = 0;
			string TempArrayInfo = "";
			TempInfo = 0;
			if (blnRunCollapseEvent)
			{
				for (counter = 2; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 0)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FirstFlag = true;
							if (strBalances[counter] == "B0")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E0");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									// If counter2 = BalanceCol And TempInfo = 0 Then
									// vs1.TextMatrix(TempRow, counter2) = ""
									// Else
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									// End If
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							FirstFlag = false;
							if (strBalances[counter] == "E0")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B0");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (FirstFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							SecondFlag = true;
							if (strBalances[counter] == "B1")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E1");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									// If counter2 = BalanceCol And TempInfo = 0 Then
									// vs1.TextMatrix(TempRow, counter2) = ""
									// Else
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									// End If
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							SecondFlag = false;
							if (strBalances[counter] == "E1")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B1");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 2)
					{
						if (FirstFlag == true || SecondFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							ThirdFlag = true;
							if (strBalances[counter] == "B2")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E2");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									// If counter2 = BalanceCol And TempInfo = 0 Then
									// vs1.TextMatrix(TempRow, counter2) = ""
									// Else
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									// End If
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							ThirdFlag = false;
							if (strBalances[counter] == "E2")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B2");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 3)
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FourthFlag = true;
							if (strBalances[counter] == "B3")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E3");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									// If counter2 = BalanceCol And TempInfo = 0 Then
									// vs1.TextMatrix(TempRow, counter2) = ""
									// Else
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									// End If
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							FourthFlag = false;
							if (strBalances[counter] == "E3")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B3");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true || FourthFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				// If rs.Fields["Font"] = "S" Then
				height = (rows + 2) * 250 + 300;
				if (height < frmExpenseSummary.InstancePtr.Height * 0.6645)
				{
					if (vs1.Height != height)
					{
						vs1.Height = height + 75;
					}
				}
				else
				{
					if (vs1.Height < frmExpenseSummary.InstancePtr.Height * 0.6645)
					{
						vs1.Height = FCConvert.ToInt32(frmExpenseSummary.InstancePtr.Height * 0.6645 + 75);
					}
				}
				// Else
				// height = (rows + 2) * vs1.RowHeight(0)
				// If height < frmExpenseSummary.height * 0.6645 Then
				// If vs1.height <> height Then
				// vs1.height = height + 75
				// End If
				// Else
				// If vs1.height < frmExpenseSummary.height * 0.6645 Then
				// vs1.height = frmExpenseSummary.height * 0.6645 + 75
				// End If
				// End If
				// End If
			}
		}

		private void FormatGrid()
		{
			int counter;
			vs1.MergeRow(0, true);
			vs1.Cols = 20;
            vs1.AddExpandButton();
            vs1.RowHeadersWidth = 10;
            vs1.TextMatrix(0, 1, "Account------------");
			vs1.TextMatrix(1, 1, "  Vendor--------");
			DescriptionCol = 1;
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
                //FC:FINAL:AM:#3104 - increase column width
                //vs1.ColWidth(DescriptionCol, 2400 + 1200);
                vs1.ColWidth(DescriptionCol, 3800 + 1200);
            }
			else
			{
                //vs1.ColWidth(DescriptionCol, 2800 + 1400);
                vs1.ColWidth(DescriptionCol, 3800 + 1200);
            }
			counter = 2;
			vs1.TextMatrix(0, counter, "Current");
			vs1.TextMatrix(1, counter, "Budget");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter, 1400 + 700);
			}
			else
			{
				vs1.ColWidth(counter, 1600 + 800);
			}
			vs1.ColFormat(counter, "#,##0.00");
            vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
            NetBudgetFlag = true;
			NetBudgetCol = counter;
			vs1.MergeCol(NetBudgetCol, false);
			counter += 1;
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
			{
				vs1.TextMatrix(0, counter, "");
				vs1.TextMatrix(0, counter + 1, "");
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1400 + 700);
				}
				else
				{
					vs1.ColWidth(counter, 1600 + 800);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1400 + 700);
				}
				else
				{
					vs1.ColWidth(counter + 1, 1600 + 800);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
                vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDDCFlag = true;
				YTDDebitCol = counter;
				YTDCreditCol = counter + 1;
				vs1.MergeCol(YTDDebitCol, false);
				vs1.MergeCol(YTDCreditCol, false);
				counter += 2;
			}
			else
			{
				YTDDCFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDNet")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "");
				}
				else
				{
					vs1.TextMatrix(0, counter, "");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1400 + 700);
				}
				else
				{
					vs1.ColWidth(counter, 1600 + 800);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDNetFlag = true;
				YTDNetCol = counter;
				vs1.MergeCol(YTDNetCol, false);
				counter += 1;
			}
			else
			{
				YTDNetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("PendingDetail")))
			{
				PendingDetailFlag = true;
				PendingSummaryFlag = false;
			}
			else if (rs.Get_Fields_Boolean("PendingSummary"))
			{
				PendingSummaryFlag = true;
				PendingDetailFlag = false;
			}
			else
			{
				PendingDetailFlag = false;
				PendingSummaryFlag = false;
			}
			vs1.TextMatrix(0, counter, "Unexpended");
			vs1.TextMatrix(1, counter, "Balance");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter, 1400 + 700);
			}
			else
			{
				vs1.ColWidth(counter, 1600 + 800);
			}
			vs1.ColFormat(counter, "#,##0.00");
            vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
            BalanceFlag = true;
			BalanceCol = counter;
			vs1.MergeCol(BalanceCol, false);
			counter += 1;
			vs1.Cols = counter;
			//FC:FINAL:DDU:#3105 - fixed columns alignment
			//if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			//{
			//	for (int i = 0; i <= 9; i++)
			//	{
			//		vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//	}
			//	for (int i = 10; i <= vs1.Cols - 1; i++)
			//	{
			//		vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//	}
			//	//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 1, 9, 4);
			//	//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 10, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//}
			//else
			//{
			//	for (int i = 0; i <= 1; i++)
			//	{
			//		vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//	}
			//	for (int i = 2; i <= vs1.Cols - 1; i++)
			//	{
			//		vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//	}
			//	//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 1, 3, 4);
			//	//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 1, vs1.Cols - 1, true);
		}

		private string Expense(ref string x)
		{
			string Expense = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Expense = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			else
			{
				Expense = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			return Expense;
		}

		private string Department(ref string x)
		{
			string Department = "";
			Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return Department;
		}

		private string Division(ref string x)
		{
			string Division = "";
			Division = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			return Division;
		}

		private string Object(ref string x)
		{
			string Object = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Object = Strings.Mid(x, 6 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			else
			{
				Object = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			return Object;
		}

		private string CalculateMonth(int x)
		{
			string CalculateMonth = "";
			switch (x)
			{
				case 1:
					{
						CalculateMonth = "January";
						break;
					}
				case 2:
					{
						CalculateMonth = "February";
						break;
					}
				case 3:
					{
						CalculateMonth = "March";
						break;
					}
				case 4:
					{
						CalculateMonth = "April";
						break;
					}
				case 5:
					{
						CalculateMonth = "May";
						break;
					}
				case 6:
					{
						CalculateMonth = "June";
						break;
					}
				case 7:
					{
						CalculateMonth = "July";
						break;
					}
				case 8:
					{
						CalculateMonth = "August";
						break;
					}
				case 9:
					{
						CalculateMonth = "September";
						break;
					}
				case 10:
					{
						CalculateMonth = "October";
						break;
					}
				case 11:
					{
						CalculateMonth = "November";
						break;
					}
				case 12:
					{
						CalculateMonth = "December";
						break;
					}
			}
			//end switch
			return CalculateMonth;
		}

		private void DepartmentReport()
		{
			clsDRWrapper rsDepartment = new clsDRWrapper();
			clsDRWrapper rsDivision = new clsDRWrapper();
			clsDRWrapper rsExpense = new clsDRWrapper();
			clsDRWrapper rsObject = new clsDRWrapper();
			clsDRWrapper ExpObjDescriptions = new clsDRWrapper();
			clsDRWrapper DeptDivDescriptions = new clsDRWrapper();
			ExpObjDescriptions.OpenRecordset("SELECT * FROM ExpObjTitles ORDER BY Expense, Object");
			DeptDivDescriptions.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department, Division");
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				rsDivision.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "'");
				if (rsDivision.EndOfFile() != true && rsDivision.BeginningOfFile() != true)
				{
					rsDivision.MoveLast();
					rsDivision.MoveFirst();
					vs1.Rows += 1;
					// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
					//vs1.MergeRow(vs1.Rows - 1, true);
					if (DeptDivDescriptions.FindFirstRecord2("Department, Division", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
					{
						if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + "UNKNOWN");
					}
					vs1.RowOutlineLevel(vs1.Rows - 1, 0);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
					vs1.IsSubtotal(vs1.Rows - 1, true);
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						rsDivision.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField");
						if (rsDivision.EndOfFile() != true && rsDivision.BeginningOfFile() != true)
						{
							rsDivision.MoveLast();
							rsDivision.MoveFirst();
							while (rsDivision.EndOfFile() != true)
							{
								vs1.Rows += 1;
								// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
								//vs1.MergeRow(vs1.Rows - 1, true);
								if (DeptDivDescriptions.FindFirstRecord2("Department, Division", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rsDivision.Get_Fields_String("SecondAccountField"), ","))
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								vs1.IsSubtotal(vs1.Rows - 1, true);
								rsExpense.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
								if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
								{
									rsExpense.MoveLast();
									rsExpense.MoveFirst();
									while (rsExpense.EndOfFile() != true)
									{
										vs1.Rows += 1;
										// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
										//vs1.MergeRow(vs1.Rows - 1, true);
										if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
										}
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
										if (!modAccountTitle.Statics.ObjFlag)
										{
											// get object information----------------------
											vs1.IsSubtotal(vs1.Rows - 1, true);
											rsObject.OpenRecordset("SELECT DISTINCT FourthAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rsExpense.Get_Fields_String("ThirdAccountField") + "'ORDER BY FourthAccountField");
											if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
											{
												rsObject.MoveLast();
												rsObject.MoveFirst();
												while (rsObject.EndOfFile() != true)
												{
													vs1.Rows += 1;
													// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
													//vs1.MergeRow(vs1.Rows - 1, true);
													if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + rsObject.Get_Fields_String("FourthAccountField"), ","))
													{
														if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
														}
													}
													else
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + "UNKNOWN");
													}
													vs1.RowOutlineLevel(vs1.Rows - 1, 3);
													vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
													CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), rsObject.Get_Fields_String("FourthAccountField")));
													rsObject.MoveNext();
												}
											}
											// get object information-------------------------
											vs1.Rows += 1;
											vs1.RowOutlineLevel(vs1.Rows - 1, 3);
											vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
											vs1.IsSubtotal(vs1.Rows - 1, true);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
										}
										else
										{
											CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), ""));
										}
										rsExpense.MoveNext();
									}
								}
								rsDivision.MoveNext();
								vs1.Rows += 1;
								vs1.RowOutlineLevel(vs1.Rows - 1, 2);
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
								vs1.IsSubtotal(vs1.Rows - 1, true);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
							}
						}
					}
					else
					{
						rsExpense.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField");
						if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
						{
							rsExpense.MoveLast();
							rsExpense.MoveFirst();
							while (rsExpense.EndOfFile() != true)
							{
								vs1.Rows += 1;
								// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
								//vs1.MergeRow(vs1.Rows - 1, true);
								if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
								if (!modAccountTitle.Statics.ObjFlag)
								{
									// Get Object Information------------------------
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rsObject.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rsExpense.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
									if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
									{
										rsObject.MoveLast();
										rsObject.MoveFirst();
										while (rsObject.EndOfFile() != true)
										{
											vs1.Rows += 1;
											// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
											//vs1.MergeRow(vs1.Rows - 1, true);
											if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + rsObject.Get_Fields_String("ThirdAccountField"), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
											CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rsExpense.Get_Fields_String("SecondAccountField"), rsObject.Get_Fields_String("ThirdAccountField"), ""));
											rsObject.MoveNext();
										}
									}
									// Get Object Info-------------------------------------
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								}
								else
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rsExpense.Get_Fields_String("SecondAccountField"), "", ""));
								}
								rsExpense.MoveNext();
							}
						}
					}
					vs1.Rows += 1;
					vs1.RowOutlineLevel(vs1.Rows - 1, 1);
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
					vs1.IsSubtotal(vs1.Rows - 1, true);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				// more than 1 department
				rsDepartment.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND FirstAccountField <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "'");
				if (rsDepartment.EndOfFile() != true && rsDepartment.BeginningOfFile() != true)
				{
					rsDepartment.MoveLast();
					rsDepartment.MoveFirst();
					while (rsDepartment.EndOfFile() != true)
					{
						vs1.Rows += 1;
						// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
						//vs1.MergeRow(vs1.Rows - 1, true);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rsDepartment.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							rsDivision.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rsDivision.EndOfFile() != true && rsDivision.BeginningOfFile() != true)
							{
								rsDivision.MoveLast();
								rsDivision.MoveFirst();
								while (rsDivision.EndOfFile() != true)
								{
									vs1.Rows += 1;
									// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
									//vs1.MergeRow(vs1.Rows - 1, true);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rsDepartment.Get_Fields_String("FirstAccountField") + "," + rsDivision.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rsExpense.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
									if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
									{
										rsExpense.MoveLast();
										rsExpense.MoveFirst();
										while (rsExpense.EndOfFile() != true)
										{
											vs1.Rows += 1;
											// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
											//vs1.MergeRow(vs1.Rows - 1, true);
											if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											if (!modAccountTitle.Statics.ObjFlag)
											{
												// Get Object Info---------------------------------------
												vs1.IsSubtotal(vs1.Rows - 1, true);
												rsObject.OpenRecordset("SELECT DISTINCT FourthAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rsExpense.Get_Fields_String("ThirdAccountField") + "'ORDER BY FourthAccountField");
												if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
												{
													rsObject.MoveLast();
													rsObject.MoveFirst();
													while (rsObject.EndOfFile() != true)
													{
														vs1.Rows += 1;
														// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
														//vs1.MergeRow(vs1.Rows - 1, true);
														if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + rsObject.Get_Fields_String("FourthAccountField"), ","))
														{
															if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
															}
															else
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
															}
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + "UNKNOWN");
														}
														vs1.RowOutlineLevel(vs1.Rows - 1, 3);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
														CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), rsObject.Get_Fields_String("FourthAccountField")));
														rsObject.MoveNext();
													}
												}
												// Get Object Info---------------------------------
												vs1.Rows += 1;
												vs1.RowOutlineLevel(vs1.Rows - 1, 3);
												vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
												vs1.IsSubtotal(vs1.Rows - 1, true);
												vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
											}
											else
											{
												CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), ""));
											}
											rsExpense.MoveNext();
										}
									}
									rsDivision.MoveNext();
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								}
							}
						}
						else
						{
							rsExpense.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
							{
								rsExpense.MoveLast();
								rsExpense.MoveFirst();
								while (rsExpense.EndOfFile() != true)
								{
									vs1.Rows += 1;
									// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
									//vs1.MergeRow(vs1.Rows - 1, true);
									if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									if (!modAccountTitle.Statics.ObjFlag)
									{
										// Get Object Info--------------------------------
										vs1.IsSubtotal(vs1.Rows - 1, true);
										rsObject.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsExpense.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
										if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
										{
											rsObject.MoveLast();
											rsObject.MoveFirst();
											while (rsObject.EndOfFile() != true)
											{
												vs1.Rows += 1;
												// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
												//vs1.MergeRow(vs1.Rows - 1, true);
												if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + rsObject.Get_Fields_String("ThirdAccountField"), ","))
												{
													if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
													}
													else
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
													}
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
												}
												vs1.RowOutlineLevel(vs1.Rows - 1, 2);
												vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
												CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsExpense.Get_Fields_String("SecondAccountField"), rsObject.Get_Fields_String("ThirdAccountField"), ""));
												rsObject.MoveNext();
											}
										}
										// Get Object Info-------------------------------
										vs1.Rows += 1;
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
										vs1.IsSubtotal(vs1.Rows - 1, true);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									}
									else
									{
										CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsExpense.Get_Fields_String("SecondAccountField"), "", ""));
									}
									rsExpense.MoveNext();
								}
							}
						}
						rsDepartment.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "A")
			{
				// All Departments
				rsDepartment.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'E'");
				if (rsDepartment.EndOfFile() != true && rsDepartment.BeginningOfFile() != true)
				{
					rsDepartment.MoveLast();
					rsDepartment.MoveFirst();
					while (rsDepartment.EndOfFile() != true)
					{
						vs1.Rows += 1;
						// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
						//vs1.MergeRow(vs1.Rows - 1, true);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rsDepartment.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							rsDivision.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rsDivision.EndOfFile() != true && rsDivision.BeginningOfFile() != true)
							{
								rsDivision.MoveLast();
								rsDivision.MoveFirst();
								while (rsDivision.EndOfFile() != true)
								{
									vs1.Rows += 1;
									// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
									//vs1.MergeRow(vs1.Rows - 1, true);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rsDepartment.Get_Fields_String("FirstAccountField") + "," + rsDivision.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rsExpense.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
									if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
									{
										rsExpense.MoveLast();
										rsExpense.MoveFirst();
										while (rsExpense.EndOfFile() != true)
										{
											vs1.Rows += 1;
											// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
											//vs1.MergeRow(vs1.Rows - 1, true);
											if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											if (!modAccountTitle.Statics.ObjFlag)
											{
												// Get Object Info-------------------------
												vs1.IsSubtotal(vs1.Rows - 1, true);
												rsObject.OpenRecordset("SELECT Distinct FourthAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rsExpense.Get_Fields_String("ThirdAccountField") + "'ORDER BY FourthAccountField");
												if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
												{
													rsObject.MoveLast();
													rsObject.MoveFirst();
													while (rsObject.EndOfFile() != true)
													{
														vs1.Rows += 1;
														// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
														//vs1.MergeRow(vs1.Rows - 1, true);
														if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + rsObject.Get_Fields_String("FourthAccountField"), ","))
														{
															if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
															}
															else
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
															}
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + "UNKNOWN");
														}
														vs1.RowOutlineLevel(vs1.Rows - 1, 3);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
														CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), rsObject.Get_Fields_String("FourthAccountField")));
														rsObject.MoveNext();
													}
												}
												// Get Object Info--------------------------
												vs1.Rows += 1;
												vs1.RowOutlineLevel(vs1.Rows - 1, 3);
												vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
												vs1.IsSubtotal(vs1.Rows - 1, true);
												vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
											}
											else
											{
												CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), ""));
											}
											rsExpense.MoveNext();
										}
									}
									rsDivision.MoveNext();
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								}
							}
						}
						else
						{
							rsExpense.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
							{
								rsExpense.MoveLast();
								rsExpense.MoveFirst();
								while (rsExpense.EndOfFile() != true)
								{
									vs1.Rows += 1;
									// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
									//vs1.MergeRow(vs1.Rows - 1, true);
									if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									if (!modAccountTitle.Statics.ObjFlag)
									{
										// Get Object Info-------------------------------
										vs1.IsSubtotal(vs1.Rows - 1, true);
										rsObject.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsExpense.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
										if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
										{
											rsObject.MoveLast();
											rsObject.MoveFirst();
											while (rsObject.EndOfFile() != true)
											{
												vs1.Rows += 1;
												// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
												//vs1.MergeRow(vs1.Rows - 1, true);
												if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + rsObject.Get_Fields_String("ThirdAccountField"), ","))
												{
													if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
													}
													else
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
													}
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
												}
												vs1.RowOutlineLevel(vs1.Rows - 1, 2);
												vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
												CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsExpense.Get_Fields_String("SecondAccountField"), rsObject.Get_Fields_String("ThirdAccountField"), ""));
												rsObject.MoveNext();
											}
										}
										// Get Object Info---------------------------------
										vs1.Rows += 1;
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
										vs1.IsSubtotal(vs1.Rows - 1, true);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									}
									else
									{
										CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsExpense.Get_Fields_String("SecondAccountField"), "", ""));
									}
									rsExpense.MoveNext();
								}
							}
						}
						rsDepartment.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
					}
				}
			}
			else
			{
				// range of accounts
				rsDepartment.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'E' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "'");
				if (rsDepartment.EndOfFile() != true && rsDepartment.BeginningOfFile() != true)
				{
					rsDepartment.MoveLast();
					rsDepartment.MoveFirst();
					while (rsDepartment.EndOfFile() != true)
					{
						vs1.Rows += 1;
						// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
						//vs1.MergeRow(vs1.Rows - 1, true);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rsDepartment.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rsDepartment.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							rsDivision.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField");
							if (rsDivision.EndOfFile() != true && rsDivision.BeginningOfFile() != true)
							{
								rsDivision.MoveLast();
								rsDivision.MoveFirst();
								while (rsDivision.EndOfFile() != true)
								{
									vs1.Rows += 1;
									// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
									//vs1.MergeRow(vs1.Rows - 1, true);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rsDepartment.Get_Fields_String("FirstAccountField") + "," + rsDivision.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsDivision.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rsExpense.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY ThirdAccountField");
									if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
									{
										rsExpense.MoveLast();
										rsExpense.MoveFirst();
										while (rsExpense.EndOfFile() != true)
										{
											vs1.Rows += 1;
											// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
											//vs1.MergeRow(vs1.Rows - 1, true);
											if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsExpense.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											if (!modAccountTitle.Statics.ObjFlag)
											{
												// Get Object Info--------------------------------
												vs1.IsSubtotal(vs1.Rows - 1, true);
												rsObject.OpenRecordset("SELECT DISTINCT FourthAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsDivision.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rsExpense.Get_Fields_String("ThirdAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY FourthAccountField");
												if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
												{
													rsObject.MoveLast();
													rsObject.MoveFirst();
													while (rsObject.EndOfFile() != true)
													{
														vs1.Rows += 1;
														// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
														//vs1.MergeRow(vs1.Rows - 1, true);
														if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("ThirdAccountField") + "," + rsObject.Get_Fields_String("FourthAccountField"), ","))
														{
															if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
															}
															else
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
															}
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "            " + rsObject.Get_Fields_String("FourthAccountField") + " - " + "UNKNOWN");
														}
														vs1.RowOutlineLevel(vs1.Rows - 1, 3);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
														CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), rsObject.Get_Fields_String("FourthAccountField")));
														rsObject.MoveNext();
													}
												}
												// Get Object Info--------------------------------
												vs1.Rows += 1;
												vs1.RowOutlineLevel(vs1.Rows - 1, 3);
												vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
												vs1.IsSubtotal(vs1.Rows - 1, true);
												vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
											}
											else
											{
												CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsDivision.Get_Fields_String("SecondAccountField"), rsExpense.Get_Fields_String("ThirdAccountField"), ""));
											}
											rsExpense.MoveNext();
										}
									}
									rsDivision.MoveNext();
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								}
							}
						}
						else
						{
							rsExpense.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField");
							if (rsExpense.EndOfFile() != true && rsExpense.BeginningOfFile() != true)
							{
								rsExpense.MoveLast();
								rsExpense.MoveFirst();
								while (rsExpense.EndOfFile() != true)
								{
									vs1.Rows += 1;
									// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
									//vs1.MergeRow(vs1.Rows - 1, true);
									if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rsExpense.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									if (!modAccountTitle.Statics.ObjFlag)
									{
										// Get Object Info----------------------------------
										vs1.IsSubtotal(vs1.Rows - 1, true);
										rsObject.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'E' AND FirstAccountField = '" + rsDepartment.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rsExpense.Get_Fields_String("SecondAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY ThirdAccountField");
										if (rsObject.EndOfFile() != true && rsObject.BeginningOfFile() != true)
										{
											rsObject.MoveLast();
											rsObject.MoveFirst();
											while (rsObject.EndOfFile() != true)
											{
												vs1.Rows += 1;
												// FC:FINAL:VGE - #805 Skipping merging cells to allow zeroes
												//vs1.MergeRow(vs1.Rows - 1, true);
												if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rsExpense.Get_Fields_String("SecondAccountField") + "," + rsObject.Get_Fields_String("ThirdAccountField"), ","))
												{
													if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
													}
													else
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
													}
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rsObject.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
												}
												vs1.RowOutlineLevel(vs1.Rows - 1, 2);
												vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
												CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsExpense.Get_Fields_String("SecondAccountField"), rsObject.Get_Fields_String("ThirdAccountField"), ""));
												rsObject.MoveNext();
											}
										}
										// Get Object Info--------------------------
										vs1.Rows += 1;
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
										vs1.IsSubtotal(vs1.Rows - 1, true);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									}
									else
									{
										CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("E", rsDepartment.Get_Fields_String("FirstAccountField"), rsExpense.Get_Fields_String("SecondAccountField"), "", ""));
									}
									rsExpense.MoveNext();
								}
							}
						}
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
						rsDepartment.MoveNext();
					}
				}
			}
		}

		private void PrepareTemp()
		{
			string TempFirst = "";
			string TempSecond = "";
			string TempThird = "";
			string TempFourth = "";
			string strSQL1;
			string strSQL2;
			string strSQL3 = "";
			string strSQL4 = "";
			string strSQL5 = "";
			string strSQL6;
			string strTotalSQL;
			string strTotalSQL2;
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			string strSQLFields;
			// Dim rsNetActivity As New clsDRWrapper
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			DeleteTemp();
			strSQL1 = "AccountType, Account, ";
			strSQL2 = "substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) AS FirstAccountField, ";
			strSQLFields = "AccountType, Account, FirstAccountField";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2))) AS SecondAccountField, ";
				strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2))) AS ThirdAccountField, ";
				strSQLFields += ", SecondAccountfield, ThirdAccountField";
				if (!modAccountTitle.Statics.ObjFlag)
				{
					strSQL5 = "substring(Account, 6 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 7, 2))) AS FourthAccountField, ";
					strSQLFields += ", FourthAccountField";
				}
				else
				{
					strSQL5 = "";
				}
			}
			else
			{
				strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2))) AS SecondAccountField, ";
				strSQLFields += ", SecondAccountfield";
				if (!modAccountTitle.Statics.ObjFlag)
				{
					strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 7, 2))) AS ThirdAccountField, ";
					strSQLFields += ", ThirdAccountField";
				}
				else
				{
					strSQL4 = "";
				}
				strSQL5 = "";
			}
			strSQL6 = "left(Account, 1) AS AccountType, Account, ";
			strTotalSQL = "SELECT DISTINCT " + strSQL1 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL = Strings.Left(strTotalSQL, strTotalSQL.Length - 2);
			strTotalSQL2 = "SELECT DISTINCT " + strSQL6 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL2 = Strings.Left(strTotalSQL2, strTotalSQL2.Length - 2);
			// rs3.CreateStoredProcedure "ExpenseAccounts", strTotalSQL2 & " FROM APJournalDetail WHERE left(Account, 1) = 'E' UNION " & strTotalSQL2 & " FROM EncumbranceDetail WHERE left(Account, 1) = 'E' UNION " & strTotalSQL2 & " FROM JournalEntries WHERE left(Account, 1) = 'E' UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'E' AND CurrentBudget <> 0 ORDER BY Account" 'UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'E'
			rs3.Execute("INSERT INTO Temp (" + strSQLFields + ") SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'E' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'E' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'E' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'E' AND CurrentBudget <> 0 ) as temp", "Budgetary");
			// UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'E'
			// If YTDNetFlag Then
			// rsNetActivity.OpenRecordset "SELECT Account FROM (SELECT Account, Amount FROM JournalEntries WHERE substring(Account, 1, 1) = 'E' AND (Period >= " & LowDate & " " & strPeriodCheck & " Period <= " & HighDate & ") UNION SELECT APJournalDetail.Account AS Account, APJournalDetail.Amount as Amount FROM APJournalDetail INNER JOIN APJournal ON (APJournal.ID = APJournalDetail.APJournalID) WHERE substring(Account, 1, 1) = 'E' AND (Period >= " & LowDate & " " & strPeriodCheck & " Period <= " & HighDate & ")) GROUP BY Account HAVING SUM(Amount) = 0"
			// If rsNetActivity.EndOfFile <> True And rsNetActivity.BeginningOfFile <> True Then
			// Do
			// rs3.Execute "DELETE FROM Temp WHERE Account = '" & rsNetActivity.Fields["Account"] & "'"
			// rsNetActivity.MoveNext
			// Loop While rsNetActivity.EndOfFile <> True
			// End If
			// End If
			strPeriodCheck = strPeriodCheckHolder;
		}

		private void DeleteTemp()
		{
			rs3.Execute("DELETE FROM Temp", "Budgetary");
		}

		private void FillInInformation()
		{
			double temp = 0;
			int temp2;
			bool RemoveFlag;
			int lngBeginningRow = 0;
			int lngCounter;
			frmWait.InstancePtr.prgProgress.Value = 0;
			frmWait.InstancePtr.Refresh();
			//FC:FINAL:BBE:#528 - font size used in original for samll row height
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 7);
			lngCounter = 2;
			for (CurrentRow = 2; CurrentRow <= vs1.Rows - 1; CurrentRow++)
			{
				//Application.DoEvents();
				if (CurrentRow >= lngCounter + 20)
				{
					lngCounter = CurrentRow;
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CurrentRow) / (vs1.Rows - 1)) * 100);
					frmWait.InstancePtr.Refresh();
				}
				if (!IsTotalRow(CurrentRow) && !IsDetailRow(CurrentRow))
				{
					if (vs1.RowOutlineLevel(CurrentRow) == 0)
					{
						CurrentDepartment = Strings.Left(vs1.TextMatrix(CurrentRow, 1), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
						CurrentDivision = "";
						CurrentExpense = "";
						CurrentObject = "";
					}
					else if (vs1.RowOutlineLevel(CurrentRow) == 1)
					{
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							CurrentDivision = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
							CurrentExpense = "";
							CurrentObject = "";
						}
						else
						{
							CurrentExpense = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
							CurrentObject = "";
						}
					}
					else if (vs1.RowOutlineLevel(CurrentRow) == 2)
					{
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							CurrentExpense = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
							CurrentObject = "";
						}
						else
						{
							CurrentObject = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
						}
					}
					else if (vs1.RowOutlineLevel(CurrentRow) == 3)
					{
						if (!modAccountTitle.Statics.ExpDivFlag && !modAccountTitle.Statics.ObjFlag)
						{
							CurrentObject = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
						}
					}
					if (NetBudgetFlag)
					{
						vs1.TextMatrix(CurrentRow, NetBudgetCol, Strings.Format(GetNetBudget(), "#,##0.00"));
					}
					if (YTDDCFlag)
					{
						vs1.TextMatrix(CurrentRow, YTDDebitCol, Strings.Format(GetYTDDebit(), "#,##0.00"));
						vs1.TextMatrix(CurrentRow, YTDCreditCol, Strings.Format(GetYTDCredit(), "#,##0.00"));
					}
					if (YTDNetFlag)
					{
						vs1.TextMatrix(CurrentRow, YTDNetCol, Strings.Format(GetYTDNet(), "#,##0.00"));
					}
					if (YTDDCFlag)
					{
						temp = FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, NetBudgetCol)) - FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDDebitCol)) + FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDCreditCol));
					}
					else
					{
						temp = FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, NetBudgetCol)) - FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDNetCol));
					}
					if (temp < 0)
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, BalanceCol, Color.Red);
					}
					if (temp > GetNetBudget())
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, BalanceCol, Color.Blue);
					}
					vs1.TextMatrix(CurrentRow, BalanceCol, FCConvert.ToString(temp));
				}
				else if (IsTotalRow(CurrentRow))
				{
					lngBeginningRow = FindBeginningBalanceRow(ref CurrentRow);
					// If vs1.TextMatrix(CurrentRow, NetBudgetCol) <> "" Then
					// vs1.TextMatrix(CurrentRow, NetBudgetCol) = format(CCur(vs1.TextMatrix(lngBeginningRow, NetBudgetCol)) + CCur(vs1.TextMatrix(CurrentRow, NetBudgetCol)), "#,##0.00")
					// Else
					// vs1.TextMatrix(CurrentRow, NetBudgetCol) = format(CCur(vs1.TextMatrix(lngBeginningRow, NetBudgetCol)), "#,##0.00")
					// End If
					if (YTDDCFlag == true)
					{
						if (vs1.TextMatrix(CurrentRow, YTDDebitCol) != "")
						{
							vs1.TextMatrix(CurrentRow, YTDDebitCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDDebitCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, YTDDebitCol)), "#,##0.00"));
						}
						else
						{
							vs1.TextMatrix(CurrentRow, YTDDebitCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDDebitCol)), "#,##0.00"));
						}
						if (vs1.TextMatrix(CurrentRow, YTDCreditCol) != "")
						{
							vs1.TextMatrix(CurrentRow, YTDCreditCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDCreditCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, YTDCreditCol)), "#,##0.00"));
						}
						else
						{
							vs1.TextMatrix(CurrentRow, YTDCreditCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDCreditCol)), "#,##0.00"));
						}
					}
					else
					{
						if (vs1.TextMatrix(CurrentRow, YTDNetCol) != "")
						{
							vs1.TextMatrix(CurrentRow, YTDNetCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDNetCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, YTDNetCol)), "#,##0.00"));
						}
						else
						{
							vs1.TextMatrix(CurrentRow, YTDNetCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDNetCol)), "#,##0.00"));
						}
					}
				}
			}
		}

		private double GetNetBudget()
		{
			double GetNetBudget = 0;
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							//FC:FINAL:MSH - Issue #841: can't implicitly convert decimal to double. Added FCConvert.ToDouble
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else if (CurrentExpense == "")
				{
					if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
					{
						if (rsDivBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							//FC:FINAL:MSH - Issue #841: can't implicitly convert decimal to double. Added FCConvert.ToDouble
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDivBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsDivSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsDivBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							//FC:FINAL:MSH - Issue #841: can't inplicitly convert decimal to double. Added FCConvert.ToDouble
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDivBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsExpSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsExpSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else
				{
					if (rsObjSummaryInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
					{
						if (rsObjBudgetInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsObjBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsObjSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsObjSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsObjBudgetInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsObjBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
			}
			else
			{
				if (CurrentExpense == "")
				{
					if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsExpSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsExpSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else
				{
					if (rsObjSummaryInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
					{
						if (rsObjBudgetInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsObjBudgetInfo.Get_Fields("OriginalBudgetTotal") - rsObjSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsObjSummaryInfo.Get_Fields("BudgetAdjustmentsTotal")) * -1;
						}
					}
					else
					{
						if (rsObjBudgetInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							//FC:FINAL:MSH - can't implicitly convert decimal to double. Added FCConvert.ToDouble. (same with issue #841)
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsObjBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
			}
			return GetNetBudget;
		}

		private double GetYTDDebit()
		{
			double GetYTDDebit = 0;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
			{
				GetYTDDebit = 0;
				return GetYTDDebit;
			}
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsDeptSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsDeptSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (CurrentExpense == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsDivSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsDivSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsExpSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsExpSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsObjSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsObjSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			else
			{
				if (CurrentExpense == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsDeptSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsDeptSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsExpSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsExpSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = (double)rsObjSummaryInfo.Get_Fields("PostedDebitsTotal") + (double)rsObjSummaryInfo.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit()
		{
			double GetYTDCredit = 0;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
			{
				GetYTDCredit = 0;
				return GetYTDCredit;
			}
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (CurrentExpense == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsExpSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsObjSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			else
			{
				if (CurrentExpense == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsExpSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsObjSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			return GetYTDCredit;
		}

		private double GetYTDNet()
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit() - GetYTDCredit();
			return GetYTDNet;
		}

		private short LowMonthCalc(string x)
		{
			short LowMonthCalc = 0;
			if (x == "January")
			{
				LowMonthCalc = 1;
			}
			else if (x == "February")
			{
				LowMonthCalc = 2;
			}
			else if (x == "March")
			{
				LowMonthCalc = 3;
			}
			else if (x == "April")
			{
				LowMonthCalc = 4;
			}
			else if (x == "May")
			{
				LowMonthCalc = 5;
			}
			else if (x == "June")
			{
				LowMonthCalc = 6;
			}
			else if (x == "July")
			{
				LowMonthCalc = 7;
			}
			else if (x == "August")
			{
				LowMonthCalc = 8;
			}
			else if (x == "September")
			{
				LowMonthCalc = 9;
			}
			else if (x == "October")
			{
				LowMonthCalc = 10;
			}
			else if (x == "November")
			{
				LowMonthCalc = 11;
			}
			else if (x == "December")
			{
				LowMonthCalc = 12;
			}
			return LowMonthCalc;
		}

		private short HighMonthCalc(string x)
		{
			short HighMonthCalc = 0;
			if (x == "January")
			{
				HighMonthCalc = 1;
			}
			else if (x == "February")
			{
				HighMonthCalc = 2;
			}
			else if (x == "March")
			{
				HighMonthCalc = 3;
			}
			else if (x == "April")
			{
				HighMonthCalc = 4;
			}
			else if (x == "May")
			{
				HighMonthCalc = 5;
			}
			else if (x == "June")
			{
				HighMonthCalc = 6;
			}
			else if (x == "July")
			{
				HighMonthCalc = 7;
			}
			else if (x == "August")
			{
				HighMonthCalc = 8;
			}
			else if (x == "September")
			{
				HighMonthCalc = 9;
			}
			else if (x == "October")
			{
				HighMonthCalc = 10;
			}
			else if (x == "November")
			{
				HighMonthCalc = 11;
			}
			else if (x == "December")
			{
				HighMonthCalc = 12;
			}
			return HighMonthCalc;
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private double GetPending()
		{
			double GetPending = 0;
			GetPending = GetPendingDebits() - GetPendingCredits();
			return GetPending;
		}

		private void GetCompleteTotals()
		{
			int counter;
			int counter2;
			double total = 0;
			vs1.Rows += 1;
			vs1.TextMatrix(vs1.Rows - 1, 1, "Final Totals");
			for (counter = NetBudgetCol; counter <= vs1.Cols - 1; counter++)
			{
				total = 0;
				for (counter2 = 2; counter2 <= vs1.Rows - 2; counter2++)
				{
					if (IsTotalRow(counter2) && vs1.RowOutlineLevel(counter2) == 1)
					{
						total += FCConvert.ToDouble(vs1.TextMatrix(counter2, counter));
					}
					// FC:FINAL:VGE - #805 Setting zeros to parent rows.
					if (vs1.TextMatrix(counter2, 1).Contains("-"))
					{
						vs1.TextMatrix(counter2, counter, "0.00");
					}
				}
				vs1.TextMatrix(vs1.Rows - 1, counter, FCConvert.ToString(total));
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.Blue);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			vs1.RowOutlineLevel(vs1.Rows - 1, 0);
			vs1.IsSubtotal(vs1.Rows - 1, true);
		}

		private void SetColors()
		{
			int counter;
            //modColorScheme.ColorGrid(vs1, 2, -1, 0, -1, true);
            //for (counter = 2; counter <= vs1.Rows - 2; counter++)
            //{
            //    vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(vs1.RowOutlineLevel(counter)));
            //}
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(99));
            modColorScheme.ColorGrid(vs1, 2, vs1.Rows - 2, 0, vs1.Cols - 1);
            modColorScheme.ColorGrid(vs1, vs1.Rows - 1, vs1.Rows - 1, 0, vs1.Cols - 1, true);

		}

		private void CreateDetailInfo_2(string strAccount)
		{
			CreateDetailInfo(ref strAccount);
		}

		private void CreateDetailInfo(ref string strAccount)
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			int HighDate = 0;
			int LowDate;
			// vbPorter upgrade warning: curDebitTotal As Decimal	OnWrite(short, Decimal)
			Decimal curDebitTotal;
			// vbPorter upgrade warning: curCreditTotal As Decimal	OnWrite(short, Decimal)
			Decimal curCreditTotal;
			// vbPorter upgrade warning: curNetTotal As Decimal	OnWrite(short, Decimal)
			Decimal curNetTotal;
			// vbPorter upgrade warning: curBudgetTotal As Decimal	OnWrite(short, Decimal)
			Decimal curBudgetTotal;
			// vbPorter upgrade warning: curOtherDebitTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOtherDebitTotal;
			// vbPorter upgrade warning: curOtherCreditTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOtherCreditTotal;
			// vbPorter upgrade warning: curOtherNetTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOtherNetTotal;
			// vbPorter upgrade warning: curOtherBudgetTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOtherBudgetTotal;
			// vbPorter upgrade warning: curTotalDebitTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotalDebitTotal;
			// vbPorter upgrade warning: curTotalCreditTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotalCreditTotal;
			// vbPorter upgrade warning: curTotalNetTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotalNetTotal;
			// vbPorter upgrade warning: curTotalBudgetTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotalBudgetTotal;
			double dblPendingTotal = 0;
			bool blnRecordsFound;
			int lngVendorNumber;
			string strVendorName;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				HighDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
			}
			else
			{
				HighDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				if (HighDate == 1)
				{
					HighDate = 12;
				}
				else
				{
					HighDate -= 1;
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) != "A")
			{
				LowDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			else
			{
				LowDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			}
			blnRecordsFound = false;
			curDebitTotal = 0;
			curCreditTotal = 0;
			curNetTotal = 0;
			curBudgetTotal = 0;
			curTotalDebitTotal = 0;
			curTotalCreditTotal = 0;
			curTotalNetTotal = 0;
			curTotalBudgetTotal = 0;
			curOtherDebitTotal = 0;
			curOtherCreditTotal = 0;
			curOtherNetTotal = 0;
			curOtherBudgetTotal = 0;
			lngVendorNumber = 0;
			strVendorName = "";
			CurrentDepartment = Department(ref strAccount);
			CurrentDivision = Division(ref strAccount);
			CurrentExpense = Expense(ref strAccount);
			CurrentObject = Object(ref strAccount);
			strAccount = Strings.Left(strAccount, strAccount.Length - 1);
			if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
			{
				vs1.IsSubtotal(vs1.Rows - 1, true);
				CheckAgain:
				;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) == strAccount)
				{
					lngVendorNumber = FCConvert.ToInt32(rsDetailInfo.Get_Fields_Int32("VendorNumber"));
					do
					{
						blnRecordsFound = true;
						if (lngVendorNumber != FCConvert.ToInt32(rsDetailInfo.Get_Fields_Int32("VendorNumber")))
						{
							if (lngVendorNumber != 0)
							{
								vs1.Rows += 1;
								if (lngVendorNumber != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(lngVendorNumber));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, Strings.Format(lngVendorNumber, "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, Strings.Format(lngVendorNumber, "00000") + " - UNKNOWN");
									}
								}
								if (curBudgetTotal != 0)
								{
									vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curBudgetTotal, "#,##0.00"));
								}
								if (YTDDCFlag)
								{
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotal, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotal, "#,##0.00"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curNetTotal, "#,##0.00"));
								}
								if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
								{
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
								}
								else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
								{
									vs1.RowOutlineLevel(vs1.Rows - 1, 3);
								}
								else
								{
									vs1.RowOutlineLevel(vs1.Rows - 1, 4);
								}
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
								curTotalBudgetTotal += curBudgetTotal;
								curTotalDebitTotal += curDebitTotal;
								curTotalCreditTotal += curCreditTotal;
								curTotalNetTotal += curNetTotal;
								curBudgetTotal = 0;
								curDebitTotal = 0;
								curCreditTotal = 0;
								curNetTotal = 0;
							}
							else
							{
								curTotalBudgetTotal += curBudgetTotal;
								curTotalDebitTotal += curDebitTotal;
								curTotalCreditTotal += curCreditTotal;
								curTotalNetTotal += curNetTotal;
								curOtherBudgetTotal = curBudgetTotal;
								curOtherDebitTotal = curDebitTotal;
								curOtherCreditTotal = curCreditTotal;
								curOtherNetTotal = curNetTotal;
								curBudgetTotal = 0;
								curDebitTotal = 0;
								curCreditTotal = 0;
								curNetTotal = 0;
							}
							lngVendorNumber = FCConvert.ToInt32(rsDetailInfo.Get_Fields_Int32("VendorNumber"));
						}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A")
						{
							if (YTDDCFlag)
							{
								// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
								if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curDebitTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
									// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
								}
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
								else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curDebitTotal += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance"));
									// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
								}
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curCreditTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
									// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curCreditTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
								if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curNetTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
								}
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
								else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curNetTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
								}
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curNetTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curNetTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
								}
							}
						}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsDetailInfo.Get_Fields("Type") == "E")
						{
							if (YTDDCFlag)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curDebitTotal += rsDetailInfo.Get_Fields("Amount");
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCreditTotal += rsDetailInfo.Get_Fields("Amount");
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curNetTotal += rsDetailInfo.Get_Fields("Amount");
							}
						}
						else
						{
                            if (YTDDCFlag)
                            {
                                // if the amount is more than 0 and an RCB of anything but c it is a debit
                                if (rsDetailInfo.Get_Fields("Amount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
                                {
                                    if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curBudgetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curDebitTotal += rsDetailInfo.Get_Fields("Amount");
                                    }
                                    // if the amount is less than 0 and an RCB of C then it is a debit
                                }
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                else if (rsDetailInfo.Get_Fields("Amount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
                                {
                                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                    curDebitTotal += rsDetailInfo.Get_Fields("Amount");
                                    // if the amount is more than 0 and an RCB of C than it is a Credit
                                }
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                else if (rsDetailInfo.Get_Fields("Amount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
                                {
                                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                    curCreditTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
                                    // if the amount is less than 0 and it is a general journal entry with an RCB of anything but C then it is a Credit
                                }
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                else if (rsDetailInfo.Get_Fields("Amount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
                                {
                                    if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curBudgetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
                                    }
                                    else
                                    {
                                        if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "E")
                                        {
                                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                            curDebitTotal += rsDetailInfo.Get_Fields("Amount");
                                        }
                                        else
                                        {
                                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                            curCreditTotal += rsDetailInfo.Get_Fields("Amount");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                if (rsDetailInfo.Get_Fields("Amount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
                                {
                                    if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curBudgetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curNetTotal += rsDetailInfo.Get_Fields("Amount");
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                else if (rsDetailInfo.Get_Fields("Amount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
                                {
                                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                    curNetTotal += rsDetailInfo.Get_Fields("Amount");
                                }
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                else if (rsDetailInfo.Get_Fields("Amount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
                                {
                                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                    curNetTotal += rsDetailInfo.Get_Fields("Amount");
                                }
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                else if (rsDetailInfo.Get_Fields("Amount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
                                {
                                    if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curBudgetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                        curNetTotal += rsDetailInfo.Get_Fields("Amount");
                                    }
                                }
                            }
						}
						rsDetailInfo.MoveNext();
						if (rsDetailInfo.EndOfFile())
						{
							break;
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					}
					while (rsDetailInfo.Get_Fields("Account") == strAccount);
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				else if (string.Compare(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), strAccount) < 0)
				{
					rsDetailInfo.MoveNext();
					goto CheckAgain;
				}
			}
			else
			{
				if (PendingSummaryFlag)
				{
					vs1.IsSubtotal(vs1.Rows - 1, true);
				}
			}
			if (curBudgetTotal != 0 || curDebitTotal != 0 || curCreditTotal != 0 || curNetTotal != 0)
			{
				if (lngVendorNumber == 0)
				{
					curTotalBudgetTotal += curBudgetTotal;
					curTotalDebitTotal += curDebitTotal;
					curTotalCreditTotal += curCreditTotal;
					curTotalNetTotal += curNetTotal;
					curOtherBudgetTotal = curBudgetTotal;
					curOtherDebitTotal = curDebitTotal;
					curOtherCreditTotal = curCreditTotal;
					curOtherNetTotal = curNetTotal;
					curBudgetTotal = 0;
					curDebitTotal = 0;
					curCreditTotal = 0;
					curNetTotal = 0;
				}
				else
				{
					vs1.Rows += 1;
					if (lngVendorNumber != 0)
					{
						rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(lngVendorNumber));
						if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
						{
							vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, Strings.Format(lngVendorNumber, "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, Strings.Format(lngVendorNumber, "00000") + " - UNKNOWN");
						}
					}
					if (curBudgetTotal != 0)
					{
						vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curBudgetTotal, "#,##0.00"));
					}
					if (YTDDCFlag)
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotal, "#,##0.00"));
						vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotal, "#,##0.00"));
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curNetTotal, "#,##0.00"));
					}
					if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 2);
					}
					else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 3);
					}
					else
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 4);
					}
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
					curTotalBudgetTotal += curBudgetTotal;
					curTotalDebitTotal += curDebitTotal;
					curTotalCreditTotal += curCreditTotal;
					curTotalNetTotal += curNetTotal;
					curBudgetTotal = 0;
					curDebitTotal = 0;
					curCreditTotal = 0;
					curNetTotal = 0;
				}
			}
			if (curOtherBudgetTotal != 0 || curOtherDebitTotal != 0 || curOtherCreditTotal != 0 || curOtherNetTotal != 0)
			{
				vs1.Rows += 1;
				vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Other");
				if (YTDDCFlag)
				{
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curOtherDebitTotal, "#,##0.00"));
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curOtherCreditTotal * -1, "#,##0.00"));
				}
				else
				{
					vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curOtherNetTotal, "#,##0.00"));
				}
				vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curOtherBudgetTotal, "#,##0.00"));
				if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				}
				else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 3);
				}
				else
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 4);
				}
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			}
			if (PendingSummaryFlag)
			{
				dblPendingTotal = GetPending();
				if (dblPendingTotal != 0)
				{
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Pending Activity");
					if (YTDDCFlag)
					{
						if (dblPendingTotal > 0)
						{
							vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(dblPendingTotal, "#,##0.00"));
							vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
							curDebitTotal += FCConvert.ToDecimal(dblPendingTotal);
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(dblPendingTotal * -1, "#,##0.00"));
							vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
							curCreditTotal += FCConvert.ToDecimal(dblPendingTotal);
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(dblPendingTotal, "#,##0.00"));
						curNetTotal += FCConvert.ToDecimal(dblPendingTotal);
					}
					if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 2);
					}
					else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 3);
					}
					else
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 4);
					}
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				}
			}
			if (blnRecordsFound || PendingSummaryFlag)
			{
				vs1.Rows += 1;
				if (!modAccountTitle.Statics.ObjFlag)
				{
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Object......");
				}
				else
				{
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Expense.....");
				}
				if (YTDDCFlag)
				{
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curTotalDebitTotal, "#,##0.00"));
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curTotalCreditTotal * -1, "#,##0.00"));
				}
				else
				{
					vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curTotalNetTotal, "#,##0.00"));
				}
				vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curTotalBudgetTotal, "#,##0.00"));
				if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				}
				else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 3);
				}
				else
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 4);
				}
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				vs1.Rows += 1;
				if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				}
				else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 3);
				}
				else
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 4);
				}
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			}
		}

		public bool IsTotalRow(int lngRow)
		{
			bool IsTotalRow = false;
			if (vs1.TextMatrix(lngRow, DescriptionCol) == "Expense....." || vs1.TextMatrix(lngRow, DescriptionCol) == "Object......" || vs1.TextMatrix(lngRow, DescriptionCol) == "Division...." || vs1.TextMatrix(lngRow, DescriptionCol) == "Department..")
			{
				IsTotalRow = true;
			}
			else
			{
				IsTotalRow = false;
			}
			return IsTotalRow;
		}

		public bool IsDetailRow(int lngRow)
		{
			bool IsDetailRow = false;
			if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
			{
				if (vs1.RowOutlineLevel(lngRow) == 2)
				{
					IsDetailRow = true;
				}
				else
				{
					IsDetailRow = false;
				}
			}
			else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
			{
				if (vs1.RowOutlineLevel(lngRow) == 3)
				{
					IsDetailRow = true;
				}
				else
				{
					IsDetailRow = false;
				}
			}
			else
			{
				if (vs1.RowOutlineLevel(lngRow) == 4)
				{
					IsDetailRow = true;
				}
				else
				{
					IsDetailRow = false;
				}
			}
			return IsDetailRow;
		}

		public bool IsLowRow(int lngRow)
		{
			bool IsLowRow = false;
			if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
			{
				if (vs1.RowOutlineLevel(lngRow) == 1)
				{
					IsLowRow = true;
				}
				else
				{
					IsLowRow = false;
				}
			}
			else if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.ObjFlag)
			{
				if (vs1.RowOutlineLevel(lngRow) == 2)
				{
					IsLowRow = true;
				}
				else
				{
					IsLowRow = false;
				}
			}
			else
			{
				if (vs1.RowOutlineLevel(lngRow) == 3)
				{
					IsLowRow = true;
				}
				else
				{
					IsLowRow = false;
				}
			}
			return IsLowRow;
		}

		private void GetRowSubtotals()
		{
			// vbPorter upgrade warning: curTotals As Decimal	OnWrite(short, Decimal)
			Decimal[,] curTotals = new Decimal[3 + 1, 3 + 1];
			int counter;
			int counter2;
			int temp = 0;
			for (counter = 0; counter <= 3; counter++)
			{
				for (counter2 = 0; counter2 <= 3; counter2++)
				{
					curTotals[counter, counter2] = 0;
				}
			}
			strBalances = new string[vs1.Rows + 1 + 1];
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (!IsTotalRow(counter) && !IsDetailRow(counter))
				{
					strBalances[counter] = "B" + FCConvert.ToString(vs1.RowOutlineLevel(counter));
				}
				else if (IsTotalRow(counter))
				{
					strBalances[counter] = "E" + FCConvert.ToString(vs1.RowOutlineLevel(counter) - 1);
				}
				else
				{
					strBalances[counter] = "";
				}
			}
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (IsTotalRow(counter) && IsDetailRow(counter))
				{
					temp = FindBeginningBalanceRow(ref counter);
					if (vs1.TextMatrix(counter, NetBudgetCol) != "")
					{
						vs1.TextMatrix(counter, NetBudgetCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(temp, NetBudgetCol)) + FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)), "#,##0.00"));
					}
					else
					{
						vs1.TextMatrix(counter, NetBudgetCol, vs1.TextMatrix(temp, NetBudgetCol));
					}
					if (YTDNetFlag)
					{
						vs1.TextMatrix(counter, BalanceCol, FCConvert.ToString(FCConvert.ToDouble(vs1.TextMatrix(counter, NetBudgetCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, YTDNetCol))));
					}
					else
					{
						vs1.TextMatrix(counter, BalanceCol, FCConvert.ToString(FCConvert.ToDouble(vs1.TextMatrix(counter, NetBudgetCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, YTDDebitCol)) + FCConvert.ToDouble(vs1.TextMatrix(counter, YTDCreditCol))));
					}
				}
			}
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (IsTotalRow(counter) && IsDetailRow(counter))
				{
					for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
					{
						// If counter2 = NetBudgetCol Or BalanceCol Then
						temp = FindBeginningBalanceRow(ref counter);
						curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol] += (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) - FCConvert.ToDecimal(vs1.TextMatrix(temp, counter2)));
						// Else
						// curTotals(vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol) = curTotals(vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol) + vs1.TextMatrix(counter, counter2)
						// End If
					}
				}
				else if (IsTotalRow(counter))
				{
					temp = FindBeginningBalanceRow(ref counter);
					// SetNonActivityTotals vs1.RowOutlineLevel(counter), temp + 1, counter - 1
					for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
					{
						if (vs1.TextMatrix(temp, counter2) != "")
						{
							vs1.TextMatrix(counter, counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol] + FCConvert.ToDecimal(vs1.TextMatrix(temp, counter2))));
							curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol] += curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol];
							curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] = 0;
						}
						else
						{
							vs1.TextMatrix(counter, counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol]));
							curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol] += curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol];
							curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] = 0;
						}
					}
				}
			}
		}

		private int FindBeginningBalanceRow(ref int lngRow)
		{
			int FindBeginningBalanceRow = 0;
			int tempLevel;
			int counter;
			tempLevel = vs1.RowOutlineLevel(lngRow) - 1;
			for (counter = lngRow - 1; counter >= 2; counter--)
			{
				if (vs1.RowOutlineLevel(counter) == tempLevel)
				{
					FindBeginningBalanceRow = counter;
					break;
				}
			}
			return FindBeginningBalanceRow;
		}

		private int FindEndingBalanceRow_6(int lngRow, string strBalance)
		{
			return FindEndingBalanceRow(lngRow, ref strBalance);
		}

		private int FindEndingBalanceRow(int lngRow, ref string strBalance)
		{
			int FindEndingBalanceRow = 0;
			string tempLevel;
			int counter;
			tempLevel = strBalance;
			for (counter = lngRow + 1; counter <= vs1.Rows - 1; counter++)
			{
				if (strBalances[counter] == tempLevel)
				{
					FindEndingBalanceRow = counter;
					break;
				}
			}
			return FindEndingBalanceRow;
		}

		private double GetPendingCredits()
		{
			double GetPendingCredits = 0;
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else if (CurrentExpense == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsExpSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsObjSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			else
			{
				if (CurrentExpense == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsExpSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsObjSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			return GetPendingCredits;
		}

		private double GetPendingDebits()
		{
			double GetPendingDebits = 0;
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsDeptSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else if (CurrentExpense == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsDivSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsExpSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsObjSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			else
			{
				if (CurrentExpense == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsDeptSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else if (CurrentObject == "")
				{
					if (rsExpSummaryInfo.EndOfFile() != true && rsExpSummaryInfo.BeginningOfFile() != true)
					{
						if (rsExpSummaryInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsExpSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else
				{
					if (rsObjSummaryInfo.EndOfFile() != true && rsObjSummaryInfo.BeginningOfFile() != true)
					{
						if (rsObjSummaryInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsObjSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			return GetPendingDebits;
		}

		private void SetNonActivityTotals(ref short intOutlineLevel, ref int intStart, ref int intEnd)
		{
			int counter;
			int counter2;
			for (counter = 0; counter <= 3; counter++)
			{
				curNonActivityTotals[counter] = 0;
			}
			for (counter = intStart; counter <= intEnd; counter++)
			{
				if (strBalances[counter] == "B" + FCConvert.ToString(intOutlineLevel))
				{
					if (strBalances[counter + 1] == "B" + FCConvert.ToString(intOutlineLevel) || counter == intEnd)
					{
						for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
						{
							if (vs1.TextMatrix(counter, counter2) != "")
							{
								curNonActivityTotals[counter2 - NetBudgetCol] += FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
							}
						}
					}
				}
			}
		}
	}
}
