﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreateCustomCheckFormat.
	/// </summary>
	public partial class frmCreateCustomCheckFormat : BaseForm
	{
		public frmCreateCustomCheckFormat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblField = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblField.AddControlArrayElement(lblField_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: moved code from Load
			int counter;
			clsDRWrapper rsCheckItems = new clsDRWrapper();
			int lngDefaultID;
			boolLoading = true;
			boolFirstTime = true;
			KeyCol = 0;
			DescriptionCol = 1;
			IncludeCol = 2;
			XPositionCol = 3;
			YPositionCol = 4;
			XPositionDefailtCol = 5;
			YPositionDefailtCol = 6;
			vsCheckFields.ColHidden(KeyCol, true);
			vsCheckFields.ColHidden(XPositionDefailtCol, true);
			vsCheckFields.ColHidden(YPositionDefailtCol, true);
			vsCheckFields.ColDataType(XPositionCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(YPositionCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(XPositionDefailtCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(YPositionDefailtCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(IncludeCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsCheckFields.TextMatrix(0, DescriptionCol, "Check Field");
			vsCheckFields.TextMatrix(0, IncludeCol, "Show");
			vsCheckFields.TextMatrix(0, XPositionCol, "Horz Pos");
			vsCheckFields.TextMatrix(0, YPositionCol, "Vert Pos");
			vsCheckFields.TextMatrix(0, XPositionDefailtCol, "Def Horz Pos");
			vsCheckFields.TextMatrix(0, YPositionDefailtCol, "Def Vert Pos");
			vsCheckFields.ColAlignment(IncludeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// show a border between the titles and the data input section of the grid
			//vsCheckFields.Select(0, 0, 0, vsCheckFields.Cols - 1);
			//vsCheckFields.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			rsCheckItems.OpenRecordset("SELECT * FROM CustomChecks where rtrim(upper(description)) = 'DEFAULT'");
			lngDefaultID = FCConvert.ToInt32(rsCheckItems.Get_Fields_Int32("id"));
			LoadcmbCustomType();
			//FC:FINAL:DSE #i593 code is in Load event
			//cmbCustomType.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			boolDataChanged = false;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreateCustomCheckFormat InstancePtr
		{
			get
			{
				return (frmCreateCustomCheckFormat)Sys.GetInstance(typeof(frmCreateCustomCheckFormat));
			}
		}

		protected frmCreateCustomCheckFormat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           11/17/03
		// This form will be used by towns to setup custom checks in
		// case the pre defined checks that TRIO has created doesn't
		// quite match the format of the town checks
		// ********************************************************
		string strCheckField = "";
		int KeyCol;
		int DescriptionCol;
		int IncludeCol;
		int XPositionCol;
		int YPositionCol;
		int XPositionDefailtCol;
		int YPositionDefailtCol;
		double dblProportion;
		bool blnUnload;
		bool blnEdit;
		bool boolFirstTime;
		bool boolLoading;
		bool boolDataChanged;

		private void cboCheckPosition_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void chkDoubleStub_CheckedChanged(object sender, System.EventArgs e)
		{
			LoadCheckPosition();
			boolDataChanged = true;
		}

		private void chkTwoStubs_CheckedChanged(object sender, System.EventArgs e)
		{
			LoadCheckPosition();
			boolDataChanged = true;
		}

		private void cmbCustomType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngFormatID;
			int counter;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			blnEdit = true;
			LoadCustomCheckInfo();
			LoadCustomCheckFields(lngFormatID);
			blnEdit = false;
			clsLoad.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToString(clsLoad.Get_Fields_String("ScannedCheckFile")) != "")
				{
					if (File.Exists(FCConvert.ToString(clsLoad.Get_Fields_String("ScannedCheckFile"))))
					{
						imgCheck.Image = FCUtils.LoadPicture(FCConvert.ToString(clsLoad.Get_Fields_String("ScannedCheckFile")));
					}
					else
					{
						imgCheck.Image = ImageList1.Images[0];
					}
				}
				else
				{
					imgCheck.Image = ImageList1.Images[0];
				}
			}
			else
			{
				imgCheck.Image = ImageList1.Images[0];
			}
			if (boolFirstTime)
			{
				clsLoad.OpenRecordset("SELECT COUNT(Description) as TotalCount FROM CustomCheckFields where formatid = " + FCConvert.ToString(lngFormatID));
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					for (counter = 1; counter <= FCConvert.ToInt32(clsLoad.Get_Fields("TotalCount")) - 1; counter++)
					{
						lblField.Load(FCConvert.ToInt16(counter));
					}
					dblProportion = imgCheck.WidthOriginal / 6120.0;
					SetupFieldsFirstTime();
				}
				boolFirstTime = false;
			}
			else
			{
				SetupFields();
			}
			boolDataChanged = false;
		}

		private void frmCreateCustomCheckFormat_Activated(object sender, System.EventArgs e)
		{
			int lngFormatID = 0;
			if (boolLoading)
			{
				// have to do this so the font will resize correctly when the form first comes up
				// otherwise the font will be wrong until you change from laser to dot matrix or change formats
				boolLoading = false;
				//Application.DoEvents();
				if (cmbCustomType.SelectedIndex >= 0)
				{
					lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
					LoadCheckPosition();
					LoadCustomCheckFields(lngFormatID);
					SetupFields();
				}
				boolDataChanged = false;
			}
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void ResizevsCheckFields()
		{
			int lngGridWidth = 0;
			lngGridWidth = vsCheckFields.WidthOriginal;
			vsCheckFields.ColWidth(DescriptionCol, FCConvert.ToInt32(0.5 * lngGridWidth));
			vsCheckFields.ColWidth(IncludeCol, FCConvert.ToInt32(0.1 * lngGridWidth));
			vsCheckFields.ColWidth(XPositionCol, FCConvert.ToInt32(0.18 * lngGridWidth));
			vsCheckFields.ColWidth(YPositionCol, FCConvert.ToInt32(0.18 * lngGridWidth));
			// .ColWidth(XPositionDefailtCol) = 0.157 * lngGridWidth
		}

		private void frmCreateCustomCheckFormat_Load(object sender, System.EventArgs e)
		{
			cmbCustomType.SelectedIndex = 0;
		}

		private void frmCreateCustomCheckFormat_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCreateCustomCheckFormat_Resize(object sender, System.EventArgs e)
		{
			dblProportion = imgCheck.WidthOriginal / 6120.0;
			ResizevsCheckFields();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int lngFormatID;
			clsDRWrapper clsSave = new clsDRWrapper();
			if (cmbCustomType.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a format before you can delete one", "No Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbCustomType.SelectedIndex == 0 || Strings.Trim(Strings.UCase(cmbCustomType.Text)) == "DEFAULT")
			{
				// these should be the same. Default should be item 0
				MessageBox.Show("You cannot delete the default format", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (MessageBox.Show("This will delete this format." + "\r\n" + "Do you wish to continue?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			// delete it
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			clsSave.Execute("delete from customcheckfields where formatid = " + FCConvert.ToString(lngFormatID), "Budgetary");
			clsSave.Execute("delete from customchecks where id = " + FCConvert.ToString(lngFormatID), "Budgetary");
			// now reload cmbcustomtype
			LoadcmbCustomType();
			// select it
			cmbCustomType.SelectedIndex = 0;
		}

		private void mnuFileLoadImage_Click(object sender, System.EventArgs e)
		{
			string strCurDir;
			string strFileName = "";
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsImageInfo = new clsDRWrapper();
			string strDestinationPath;
			int lngFormatID;
			strCurDir = FCFileSystem.Statics.UserDataFolder;
			PickAgain:
			;
			Information.Err().Clear();
			// dlg1.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			//- dlg1.CancelError = true;
			/*? On Error Resume Next  */
			dlg1.Filter = "*.jpg, *.gif, *.bmp";
			dlg1.InitDir = FCFileSystem.Statics.UserDataFolder;
			try
			{
				dlg1.ShowOpen();
			}
			catch
			{
			}
			if (Information.Err().Number == 0)
			{
				strFileName = dlg1.FileName;
				if (Strings.UCase(Strings.Right(strFileName, 3)) != "JPG" && Strings.UCase(Strings.Right(strFileName, 3)) != "GIF" && Strings.UCase(Strings.Right(strFileName, 3)) != "BMP")
				{
					ans = MessageBox.Show("You must select a picture file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						goto PickAgain;
					}
					else
					{
						return;
					}
				}
			}
			else
			{
				return;
			}
			/*? On Error GoTo 0 */
			FCFileSystem.ChDrive(strCurDir);
			//Application.StartupPath = strCurDir;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			rsImageInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsImageInfo.EndOfFile() != true && rsImageInfo.BeginningOfFile() != true)
			{
				rsImageInfo.Edit();
			}
			else
			{
				rsImageInfo.AddNew();
			}
			strDestinationPath = strCurDir;
			if (Strings.Right(strDestinationPath, 1) != "\\")
			{
				strDestinationPath += "\\CheckIcons";
			}
			else
			{
				strDestinationPath += "CheckIcons";
			}
			if (!Directory.Exists(strDestinationPath))
			{
				Directory.CreateDirectory(strDestinationPath);
			}
			File.Copy(strFileName, strDestinationPath + "\\" + Strings.Right(strFileName, strFileName.Length - Strings.InStrRev(strFileName, "\\", -1, CompareConstants.vbTextCompare/*?*/)), true);
			rsImageInfo.Set_Fields("ScannedCheckFile", strDestinationPath + "\\" + Strings.Right(strFileName, strFileName.Length - Strings.InStrRev(strFileName, "\\", -1, CompareConstants.vbTextCompare/*?*/)));
			rsImageInfo.Update(true);
			imgCheck.Image = FCUtils.LoadPicture(strDestinationPath + "\\" + Strings.Right(strFileName, strFileName.Length - Strings.InStrRev(strFileName, "\\", -1, CompareConstants.vbTextCompare/*?*/)));
			//Application.DoEvents();
			imgCheck.Visible = true;
			this.Refresh();
		}

		private void mnuFileResetDefaults_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsCheckFields.Rows - 1; counter++)
			{
				vsCheckFields.TextMatrix(counter, XPositionCol, vsCheckFields.TextMatrix(counter, XPositionDefailtCol));
				vsCheckFields.TextMatrix(counter, YPositionCol, vsCheckFields.TextMatrix(counter, YPositionDefailtCol));
			}
			SetupFields();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		public void mnuFileSave_Click()
		{
			mnuFileSave_Click(null, new System.EventArgs());
		}

		private void mnuFileUnloadImage_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsImageInfo = new clsDRWrapper();
			int lngFormatID;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			rsImageInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsImageInfo.EndOfFile() != true && rsImageInfo.BeginningOfFile() != true)
			{
				rsImageInfo.Edit();
			}
			else
			{
				return;
			}
			rsImageInfo.Set_Fields("ScannedCheckFile", "");
			rsImageInfo.Update();
			imgCheck.Image = ImageList1.Images[0];
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			object strNewFormat = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngFormatID = 0;
			int lngCurrentID = 0;
			int x;
			// creates a new type that is the same as the current selected one
			Retry:
			;
			if (frmInput.InstancePtr.Init(ref strNewFormat, "New Format", "Enter a name for the new format", 2160, false, modGlobalConstants.InputDTypes.idtString))
			{
				strNewFormat = Strings.Trim(FCConvert.ToString(strNewFormat));
				if (strNewFormat != string.Empty)
				{
					if (Strings.UCase(FCConvert.ToString(strNewFormat)) == "DEFAULT")
					{
						MessageBox.Show("There can be only one default format." + "\r\n" + "Please choose another name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
						goto Retry;
					}
					lngCurrentID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
					clsLoad.OpenRecordset("select * from customchecks where id = " + FCConvert.ToString(lngCurrentID));
					clsSave.OpenRecordset("select * from customchecks where id = -1");
					clsSave.AddNew();
					clsSave.Set_Fields("description", strNewFormat);
					if (cmbCheckType.SelectedIndex == 0)
					{
						clsSave.Set_Fields("CheckType", "L");
					}
					else if (cmbCheckType.SelectedIndex == 1)
					{
						clsSave.Set_Fields("CheckType", "M");
					}
					
					clsSave.Set_Fields("scannedcheckfile", clsLoad.Get_Fields_String("scannedcheckfile"));
					clsSave.Set_Fields("CheckPosition", Strings.Left(cboCheckPosition.Text, 1));
					clsSave.Set_Fields("UseDoubleStub", (chkDoubleStub.CheckState == CheckState.Checked && cmbCheckType.SelectedIndex == 0));
					clsSave.Update();
					lngFormatID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
					// add this to the combobox
					cmbCustomType.AddItem(FCConvert.ToString(strNewFormat));
					cmbCustomType.ItemData(cmbCustomType.NewIndex, lngFormatID);
					// now copy and save current info
					clsLoad.OpenRecordset("select * from customcheckfields where formatid = " + FCConvert.ToString(lngCurrentID));
					clsSave.OpenRecordset("select * from customcheckfields where formatid = -1");
					while (!clsLoad.EndOfFile())
					{
						clsSave.AddNew();
						clsSave.Set_Fields("formatid", lngFormatID);
						clsSave.Set_Fields("description", clsLoad.Get_Fields_String("description"));
						clsSave.Set_Fields("controlname", clsLoad.Get_Fields_String("controlname"));
						clsSave.Set_Fields("checktype", clsLoad.Get_Fields_String("checktype"));
						clsSave.Set_Fields("defaultxposition", clsLoad.Get_Fields_Double("defaultxposition"));
						clsSave.Set_Fields("defaultyposition", clsLoad.Get_Fields_Double("defaultyposition"));
						clsSave.Set_Fields("fieldheight", FCConvert.ToString(clsLoad.Get_Fields_Int16("fieldheight")));
						clsSave.Set_Fields("fieldwidth", FCConvert.ToString(clsLoad.Get_Fields_Int16("fieldwidth")));
						for (x = 1; x <= vsCheckFields.Rows - 1; x++)
						{
							if (vsCheckFields.TextMatrix(x, KeyCol) == FCConvert.ToString(clsLoad.Get_Fields_Int32("id")))
							{
								clsSave.Set_Fields("Include", vsCheckFields.TextMatrix(x, IncludeCol));
								clsSave.Set_Fields("XPosition", vsCheckFields.TextMatrix(x, XPositionCol));
								clsSave.Set_Fields("YPosition", vsCheckFields.TextMatrix(x, YPositionCol));
							}
						}
						// x
						clsSave.Update();
						clsLoad.MoveNext();
					}
					//Application.DoEvents();
					// now load it since the id numbers won't match for the customcheckfields records
					for (x = 0; x <= cmbCustomType.Items.Count - 1; x++)
					{
						if (cmbCustomType.ItemData(x) == lngFormatID)
						{
							cmbCustomType.SelectedIndex = x;
							return;
						}
					}
					// x
				}
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// Unload rptSampleCheck
			// rptSampleCheck.PrintReport (False)
			if (cmbCustomType.SelectedIndex < 0)
				return;
			modCustomChecks.Statics.intCustomCheckFormatToUse = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			if (boolDataChanged)
			{
				mnuFileSave_Click();
			}
			rptNewSampleCheck.InstancePtr.Init(modCustomChecks.Statics.intCustomCheckFormatToUse, this.Modal);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(null, new System.EventArgs());
		}

		private void LoadCheckPosition()
		{
			string strTemp;
			strTemp = cboCheckPosition.Text + " ";
			strTemp = Strings.Left(strTemp, 1);
			if ((cmbCheckType.SelectedIndex == 0 && chkDoubleStub.CheckState != CheckState.Checked) || (cmbCheckType.SelectedIndex == 1 && chkTwoStubs.CheckState == CheckState.Checked))
			{
				cboCheckPosition.Clear();
				cboCheckPosition.AddItem("Top Section");
				cboCheckPosition.AddItem("Middle Section");
				cboCheckPosition.AddItem("Bottom Section");
				if (Strings.UCase(strTemp) == "T")
				{
					cboCheckPosition.SelectedIndex = 0;
				}
				else if (Strings.UCase(strTemp) == "M")
				{
					cboCheckPosition.SelectedIndex = 1;
				}
				else if (Strings.UCase(strTemp) == "B")
				{
					cboCheckPosition.SelectedIndex = 2;
				}
				else
				{
					cboCheckPosition.SelectedIndex = 0;
				}
			}
			else if (cmbCheckType.SelectedIndex == 1)
			{
				cboCheckPosition.Clear();
				cboCheckPosition.AddItem("Middle Section");
				cboCheckPosition.SelectedIndex = 0;
			}
			else
			{
				cboCheckPosition.Clear();
				cboCheckPosition.AddItem("Top Section");
				cboCheckPosition.AddItem("Bottom Section");
				if (Strings.UCase(strTemp) == "T")
				{
					cboCheckPosition.SelectedIndex = 0;
				}
				else if (Strings.UCase(strTemp) == "B")
				{
					cboCheckPosition.SelectedIndex = 1;
				}
				else if (Strings.UCase(strTemp) == "M")
				{
					cboCheckPosition.SelectedIndex = 1;
				}
				else
				{
					cboCheckPosition.SelectedIndex = 0;
				}
			}
		}

		private void LoadCustomCheckFields(int lngFormatID)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int intEmployeeRow;
			intEmployeeRow = -1;

			rsInfo.OpenRecordset("Select * from customcheckfields where formatid = " + FCConvert.ToString(lngFormatID) + " order by description");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				vsCheckFields.Rows = 1;
				do
				{
					//vsCheckFields.AddItem("");
					vsCheckFields.Rows++;
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields_String("Description")));
					if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) == "Employee Number")
						intEmployeeRow = vsCheckFields.Rows - 1;
					if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("Include")))
					{
						vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, IncludeCol, FCConvert.ToString(true));
					}
					else
					{
						vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, IncludeCol, FCConvert.ToString(false));
					}
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, XPositionCol, FCConvert.ToString(rsInfo.Get_Fields_Double("XPosition")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, YPositionCol, FCConvert.ToString(rsInfo.Get_Fields_Double("YPosition")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, XPositionDefailtCol, FCConvert.ToString(rsInfo.Get_Fields_Double("DefaultXPosition")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, YPositionDefailtCol, FCConvert.ToString(rsInfo.Get_Fields_Double("DefaultYPosition")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				// CHANGE MADE ON 8/10/2004 BY MATTHEW
				// THIS WILL MOVE THE EMPLOYEE NAME FIELD OPTION TO THE BOTTOM IF THIS
				// MODULE IS PAYROLL
				if (Strings.UCase(App.EXEName) == "TWPY0000")
				{
					if (intEmployeeRow >= 0)
					{
						int inta;
						//vsCheckFields.AddItem("");
						vsCheckFields.Rows++;
						for (inta = 0; inta <= vsCheckFields.Cols - 1; inta++)
						{
							vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, inta, vsCheckFields.TextMatrix(intEmployeeRow, inta));
						}
						vsCheckFields.RemoveItem(intEmployeeRow);
					}
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void optLaser_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			chkDoubleStub.Visible = true;
			chkTwoStubs.Visible = false;
			LoadCheckPosition();
			LoadCustomCheckFields(lngFormatID);
			if (!blnEdit)
			{
				SetupFields();
			}
		}

		private void LoadCustomCheckInfo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			rsInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsInfo.Get_Fields_String("CheckType")) == "L")
				{
					cmbCheckType.SelectedIndex = 0;
					chkDoubleStub.Visible = true;
					chkTwoStubs.Visible = false;
					chkTwoStubs.CheckState = CheckState.Unchecked;
					if (rsInfo.Get_Fields_Boolean("UseDoubleStub") == true)
					{
						chkDoubleStub.CheckState = CheckState.Checked;
					}
					else
					{
						chkDoubleStub.CheckState = CheckState.Unchecked;
					}
					txtVoidAfter.Text = FCConvert.ToString(rsInfo.Get_Fields_String("VoidAfterMessage"));
					LoadCheckPosition();
					if (chkDoubleStub.CheckState == CheckState.Unchecked)
					{
						if (FCConvert.ToString(rsInfo.Get_Fields_String("CheckPosition")) == "T")
						{
							cboCheckPosition.SelectedIndex = 0;
						}
						else if (FCConvert.ToString(rsInfo.Get_Fields_String("CheckPosition")) == "M")
						{
							cboCheckPosition.SelectedIndex = 1;
						}
						else if (FCConvert.ToString(rsInfo.Get_Fields_String("CheckPosition")) == "B")
						{
							cboCheckPosition.SelectedIndex = 2;
						}
					}
					else
					{
						if (Strings.UCase(FCConvert.ToString(rsInfo.Get_Fields_String("checkposition"))) == "T")
						{
							cboCheckPosition.SelectedIndex = 0;
						}
						else if (Strings.UCase(FCConvert.ToString(rsInfo.Get_Fields_String("checkposition"))) == "B")
						{
							cboCheckPosition.SelectedIndex = 1;
						}
						else
						{
							cboCheckPosition.SelectedIndex = 0;
						}
					}
				}
				else if (FCConvert.ToString(rsInfo.Get_Fields_String("CheckType")) == "M")
				{
					cmbCheckType.SelectedIndex = 1;
					chkDoubleStub.Visible = false;
					chkTwoStubs.Visible = false;
					chkTwoStubs.CheckState = CheckState.Unchecked;
					chkDoubleStub.CheckState = CheckState.Unchecked;
					txtVoidAfter.Text = FCConvert.ToString(rsInfo.Get_Fields_String("VoidAfterMessage"));
					LoadCheckPosition();
					cboCheckPosition.SelectedIndex = 0;
				}
			}
			else
			{
				LoadCheckPosition();
				cboCheckPosition.SelectedIndex = 0;
			}
		}

		private void SaveInfo()
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngFormatID;
			cboCheckPosition.Focus();
			if (cmbCustomType.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a format to save", "No Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			if (cboCheckPosition.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a check position before you may continue.", "Invalid Check Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboCheckPosition.Focus();
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsInfo.Edit();
			}
			else
			{
				rsInfo.AddNew();
			}
			if (cmbCheckType.SelectedIndex == 0)
			{
				rsInfo.Set_Fields("CheckType", "L");
			}
			else if (cmbCheckType.SelectedIndex == 1)
			{
				rsInfo.Set_Fields("CheckType", "M");
			}

			rsInfo.Set_Fields("CheckPosition", Strings.Left(cboCheckPosition.Text, 1));
			rsInfo.Set_Fields("UseDoubleStub", (chkDoubleStub.CheckState == CheckState.Checked && cmbCheckType.SelectedIndex == 0));
			rsInfo.Set_Fields("UseTwoStubs", (chkTwoStubs.CheckState == CheckState.Checked && cmbCheckType.SelectedIndex == 1));
			rsInfo.Set_Fields("VoidAfterMessage", Strings.Trim(txtVoidAfter.Text));
			rsInfo.Update();
			for (counter = 1; counter <= vsCheckFields.Rows - 1; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE formatid = " + FCConvert.ToString(lngFormatID) + " and ID = " + vsCheckFields.TextMatrix(counter, KeyCol));
				rsInfo.Edit();
				//FC:FINAL:SBE - #719 - TextMatrix returns True/False string. Use CBool method to convrt cell value to boolean
				//rsInfo.Set_Fields("Include", vsCheckFields.TextMatrix(counter, IncludeCol) == "0" ? false : true);
				rsInfo.Set_Fields("Include", fecherFoundation.FCConvert.CBool(vsCheckFields.TextMatrix(counter, IncludeCol)));
				rsInfo.Set_Fields("XPosition", vsCheckFields.TextMatrix(counter, XPositionCol));
				rsInfo.Set_Fields("YPosition", vsCheckFields.TextMatrix(counter, YPositionCol));
				rsInfo.Update(false);
			}
			// 
			boolDataChanged = false;
			MessageBox.Show("Check format was saved successfully.", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				mnuProcessQuit_Click();
			}
		}

		private void optMailer_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			chkDoubleStub.Visible = false;
			chkTwoStubs.Visible = false;
			LoadCheckPosition();
			LoadCustomCheckFields(lngFormatID);
			if (!blnEdit)
			{
				SetupFields();
			}
		}

		private void vsCheckFields_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void vsCheckFields_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (vsCheckFields.Row > 0)
			{
				boolDataChanged = true;
			}
		}

		private void vsCheckFields_RowColChange(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#i655 - don't edit the fixed columns
			//if (vsCheckFields.Col != IncludeCol)
			if (vsCheckFields.Col >= vsCheckFields.FixedCols && vsCheckFields.Col != IncludeCol)
			{
				vsCheckFields.EditCell();
			}
			SetupFields();
		}

		private void vsCheckFields_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{TAB}", false);
			}
		}

		private void SetupFields()
		{
			int counter;
			dblProportion = FCConvert.ToDouble(imgCheck.WidthOriginal) / 6120;
			for (counter = 1; counter <= vsCheckFields.Rows - 1; counter++)
			{
				lblField[FCConvert.ToInt16(counter - 1)].Visible = FCConvert.CBool(vsCheckFields.TextMatrix(counter, IncludeCol)) && !vsCheckFields.RowHidden(counter);
				lblField[FCConvert.ToInt16(counter - 1)].TopOriginal = FCConvert.ToInt32(imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, YPositionCol)) * 120 * dblProportion));
				lblField[FCConvert.ToInt16(counter - 1)].LeftOriginal = FCConvert.ToInt32(imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, XPositionCol)) * 77 * dblProportion));
				lblField[FCConvert.ToInt16(counter - 1)].Text = vsCheckFields.TextMatrix(counter, DescriptionCol);
				lblField[FCConvert.ToInt16(counter - 1)].ForeColor = Color.Blue;
				lblField[FCConvert.ToInt16(counter - 1)].BringToFront();
                //FC:FINAL:AM:#4588 - decrease the font size
                //lblField[FCConvert.ToInt16(counter - 1)].SetFontSize(FCConvert.ToSingle(Strings.Format(6 * dblProportion, "0.0")));
                lblField[FCConvert.ToInt16(counter - 1)].SetFontSize(FCConvert.ToSingle(Strings.Format(4 * dblProportion, "0.0")));
            }
		}

		private void SetupFieldsFirstTime()
		{
			int counter;
			clsDRWrapper rsFieldInfo = new clsDRWrapper();
			dblProportion = imgCheck.WidthOriginal / 6120.0;
			for (counter = 1; counter <= vsCheckFields.Rows - 1; counter++)
			{
                lblField[FCConvert.ToInt16(counter - 1)].Capitalize = false;
                lblField[FCConvert.ToInt16(counter - 1)].Visible = FCConvert.CBool(vsCheckFields.TextMatrix(counter, IncludeCol)) && !vsCheckFields.RowHidden(counter);
				lblField[FCConvert.ToInt16(counter - 1)].TopOriginal = FCConvert.ToInt32(imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, YPositionCol)) * 120 * dblProportion));
				lblField[FCConvert.ToInt16(counter - 1)].LeftOriginal = FCConvert.ToInt32(imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, XPositionCol)) * 77 * dblProportion));
				lblField[FCConvert.ToInt16(counter - 1)].Text = vsCheckFields.TextMatrix(counter, DescriptionCol);
				rsFieldInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = '" + vsCheckFields.TextMatrix(counter, DescriptionCol) + "'");
				if (rsFieldInfo.EndOfFile() != true && rsFieldInfo.BeginningOfFile() != true)
				{
					if (Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) > 0 && Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) > 0)
					{
                        //FC:FINAL:AM:#4588 - decrease the height
                        //lblField[FCConvert.ToInt16(counter - 1)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) * (imgCheck.HeightOriginal / 5025.0) * dblProportion);
                        lblField[FCConvert.ToInt16(counter - 1)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) * (imgCheck.HeightOriginal / 15000.0) * dblProportion);
                        //FC:FINAL:AM:#3145 - make fields smaller
                        //lblField[FCConvert.ToInt16(counter - 1)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) * (imgCheck.WidthOriginal / 10800.0) * dblProportion);
                        lblField[FCConvert.ToInt16(counter - 1)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) * (imgCheck.WidthOriginal / 25000.0) * dblProportion);
                        lblField[FCConvert.ToInt16(counter - 1)].BorderStyle = 1;
					}
				}
                //FC:FINAL:AM:#4588 - decrease the font size
                //lblField[FCConvert.ToInt16(counter - 1)].SetFontSize(FCConvert.ToSingle(Strings.Format(6 * dblProportion, "0.0")));
                lblField[FCConvert.ToInt16(counter - 1)].SetFontSize(FCConvert.ToSingle(Strings.Format(4 * dblProportion, "0.0")));
                lblField[FCConvert.ToInt16(counter - 1)].ForeColor = Color.Blue;
				lblField[FCConvert.ToInt16(counter - 1)].BringToFront();
            }
		}

		private void vsCheckFields_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsCheckFields.IsCurrentCellInEditMode)
			{
				double dblProportion;
				//FC:FINAL:MSH - save and use correct indexes of the cell
				//int row = vsCheckFields.Row, col = vsCheckFields.Col;
				int row = vsCheckFields.GetFlexRowIndex(e.RowIndex);
				int col = vsCheckFields.GetFlexColIndex(e.ColumnIndex);
				boolDataChanged = true;
				if (Strings.Trim(vsCheckFields.EditText) == "")
				{
					vsCheckFields.EditText = "0";
				}
				if (!Information.IsNumeric(vsCheckFields.EditText))
				{
					MessageBox.Show("You may only enter a number in these fields.", "Invalid Position Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
				dblProportion = FCConvert.ToDouble(imgCheck.WidthOriginal) / 6120;
				if (col == YPositionCol)
				{
					if (vsCheckFields.TextMatrix(row, DescriptionCol) == "MICR Line")
					{
						// do nothing
					}
					else
					{
						if (imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.EditText) * 120 * dblProportion) > imgCheck.TopOriginal + imgCheck.HeightOriginal || imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.EditText) * 120 * dblProportion) + lblField[FCConvert.ToInt16(row - 1)].HeightOriginal < imgCheck.TopOriginal)
						{
							MessageBox.Show("Using this value would place the field off the check.", "Invalid Position Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
					}
				}
				else if (col == XPositionCol)
				{
					if (imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.EditText) * 77 * dblProportion) > imgCheck.LeftOriginal + imgCheck.WidthOriginal || imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.EditText) * 77 * dblProportion) + lblField[FCConvert.ToInt16(row - 1)].WidthOriginal < imgCheck.LeftOriginal)
					{
						MessageBox.Show("Using this value would place the field off the check.", "Invalid Position Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						return;
					}
				}
				SetupFields();
			}
		}

		private void SetCustomFormColors()
		{
			lblField[0].ForeColor = Color.Blue;
		}

		private void vsCheckFields_ClickEvent(object sender, System.EventArgs e)
		{
			// FC:FINAL:VGE - #812 Click event stores previous columns in many cases. Moving logic to RowColChanged
			/*if (vsCheckFields.Row > 0)
            {
                // FC:FINAL:VGE - #812 Condition added to avoid switching with position edit.
                if (vsCheckFields.Col != XPositionCol && vsCheckFields.Col != YPositionCol)
                {
                    if (FCUtils.CBool(vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol)) == false)
                    {
                        vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(true));
                    }
                    else
                    {
                        vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(false));
                    }
                }
            }
            SetupFields();*/
		}

		private void vsCheckFields_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				keyAscii = 0;
				if (FCUtils.CBool(vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol)) == false)
				{
					vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(true));
				}
				else
				{
					vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(false));
				}
			}
			SetupFields();
		}

		private void LoadcmbCustomType()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbCustomType.Clear();
			cmbCustomType.AddItem("Default");
			clsLoad.OpenRecordset("select * from customchecks where rtrim(upper(description)) = 'DEFAULT'");
			cmbCustomType.ItemData(0, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id")));
			clsLoad.OpenRecordset("select * from customchecks where upper(rtrim(Description)) <> 'DEFAULT' order by description");
			while (!clsLoad.EndOfFile())
			{
				cmbCustomType.AddItem(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				cmbCustomType.ItemData(cmbCustomType.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id")));
				clsLoad.MoveNext();
			}
		}

		private void cmbCheckType_CheckedChanged(object sender, EventArgs e)
		{
			if (cmbCheckType.SelectedIndex == 1)
			{
				optMailer_CheckedChanged(sender, e);
			}
			else if (cmbCheckType.SelectedIndex == 0)
			{
				optLaser_CheckedChanged(sender, e);
			}
		}

		private void vsCheckFields_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			// FC:FINAL:VGE - #812 Logic moved from vsCheckFields_ClickEvent. 
			if (e.ColumnIndex + 1 == IncludeCol)
			{
				if (FCUtils.CBool(vsCheckFields.TextMatrix(e.RowIndex + 1, IncludeCol)) == false)
				{
					vsCheckFields.TextMatrix(e.RowIndex + 1, IncludeCol, FCConvert.ToString(true));
				}
				else
				{
					vsCheckFields.TextMatrix(e.RowIndex + 1, IncludeCol, FCConvert.ToString(false));
				}
				SetupFields();
			}
		}
	}
}
