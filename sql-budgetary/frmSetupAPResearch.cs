﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupAPResearch.
	/// </summary>
	public partial class frmSetupAPResearch : BaseForm
	{
		public frmSetupAPResearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbLineItem.SelectedIndex = 1;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupAPResearch InstancePtr
		{
			get
			{
				return (frmSetupAPResearch)Sys.GetInstance(typeof(frmSetupAPResearch));
			}
		}

		protected frmSetupAPResearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmSetupAPResearch_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAPResearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAPResearch.FillStyle	= 0;
			//frmSetupAPResearch.ScaleWidth	= 3885;
			//frmSetupAPResearch.ScaleHeight	= 2175;
			//frmSetupAPResearch.LinkTopic	= "Form2";
			//frmSetupAPResearch.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSetupAPResearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmAPResearch.InstancePtr.Show(App.MainForm);
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			mnuProcessSave_Click(mnuProcessSave, System.EventArgs.Empty);
		}
	}
}
