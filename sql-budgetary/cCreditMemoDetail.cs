﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cCreditMemoDetail
	{
		//=========================================================
		private bool boolUpdated;
		private bool boolDeleted;
		private int lngRecordID;
		private int lngCreditMemoID;
		private string str1099 = string.Empty;
		private string strAccount = string.Empty;
		private double dblDetailAmount;
		private string strProject = string.Empty;
		private double dblAdjustments;
		private double dblLiquidated;

		public int CreditMemoID
		{
			set
			{
				lngCreditMemoID = value;
				IsUpdated = true;
			}
			get
			{
				int CreditMemoID = 0;
				CreditMemoID = lngCreditMemoID;
				return CreditMemoID;
			}
		}

		public string Ten99
		{
			set
			{
				str1099 = value;
				IsUpdated = true;
			}
			get
			{
				string Ten99 = "";
				Ten99 = str1099;
				return Ten99;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double Amount
		{
			set
			{
				dblDetailAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblDetailAmount;
				return Amount;
			}
		}

		public string Project
		{
			set
			{
				strProject = value;
				IsUpdated = true;
			}
			get
			{
				string Project = "";
				Project = strProject;
				return Project;
			}
		}

		public double Adjustments
		{
			set
			{
				dblAdjustments = value;
				IsUpdated = true;
			}
			get
			{
				double Adjustments = 0;
				Adjustments = dblAdjustments;
				return Adjustments;
			}
		}

		public double Liquidated
		{
			set
			{
				dblLiquidated = value;
				IsUpdated = true;
			}
			get
			{
				double Liquidated = 0;
				Liquidated = dblLiquidated;
				return Liquidated;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
