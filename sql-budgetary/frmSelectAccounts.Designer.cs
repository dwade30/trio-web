﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSelectAccounts.
	/// </summary>
	partial class frmSelectAccounts : BaseForm
	{
		public FCGrid vsAccounts;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCButton cmdUnselectAll;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectAccounts));
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.cmdSelectAll = new fecherFoundation.FCButton();
			this.cmdUnselectAll = new fecherFoundation.FCButton();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnselectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 352);
			this.BottomPanel.Size = new System.Drawing.Size(498, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsAccounts);
			this.ClientArea.Size = new System.Drawing.Size(498, 292);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSelectAll);
			this.TopPanel.Controls.Add(this.cmdUnselectAll);
			this.TopPanel.Size = new System.Drawing.Size(498, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdUnselectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSelectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(189, 30);
			this.HeaderText.Text = "Select Accounts";
			// 
			// vsAccounts
			// 
			this.vsAccounts.AllowSelection = false;
			this.vsAccounts.AllowUserToResizeColumns = false;
			this.vsAccounts.AllowUserToResizeRows = false;
			this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccounts.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsAccounts.ColumnHeadersHeight = 30;
			this.vsAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccounts.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccounts.FixedCols = 0;
			this.vsAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.FrozenCols = 0;
			this.vsAccounts.GridColor = System.Drawing.Color.Empty;
			this.vsAccounts.Location = new System.Drawing.Point(30, 30);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.ReadOnly = true;
			this.vsAccounts.RowHeadersVisible = false;
			this.vsAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccounts.RowHeightMin = 0;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.ShowColumnVisibilityMenu = false;
			this.vsAccounts.Size = new System.Drawing.Size(440, 233);
			this.vsAccounts.StandardTab = true;
			this.vsAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAccounts.TabIndex = 0;
			this.vsAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAccounts_KeyDownEvent);
			this.vsAccounts.Click += new System.EventHandler(this.vsAccounts_ClickEvent);
			// 
			// cmdSelectAll
			// 
			this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSelectAll.AppearanceKey = "toolbarButton";
			this.cmdSelectAll.Location = new System.Drawing.Point(292, 29);
			this.cmdSelectAll.Name = "cmdSelectAll";
			this.cmdSelectAll.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdSelectAll.Size = new System.Drawing.Size(74, 24);
			this.cmdSelectAll.TabIndex = 1;
			this.cmdSelectAll.Text = "Select All";
			this.cmdSelectAll.Click += new System.EventHandler(this.mnuSelectAll_Click);
			// 
			// cmdUnselectAll
			// 
			this.cmdUnselectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdUnselectAll.AppearanceKey = "toolbarButton";
			this.cmdUnselectAll.Location = new System.Drawing.Point(370, 29);
			this.cmdUnselectAll.Name = "cmdUnselectAll";
			this.cmdUnselectAll.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdUnselectAll.Size = new System.Drawing.Size(89, 24);
			this.cmdUnselectAll.TabIndex = 2;
			this.cmdUnselectAll.Text = "Unselect All";
			this.cmdUnselectAll.Click += new System.EventHandler(this.mnuUnselectAll_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(180, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmSelectAccounts
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(498, 460);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSelectAccounts";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Accounts";
			this.Load += new System.EventHandler(this.frmSelectAccounts_Load);
			this.Activated += new System.EventHandler(this.frmSelectAccounts_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSelectAccounts_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnselectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
