﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrantPreview.
	/// </summary>
	public partial class frmWarrantPreview : BaseForm
	{
		public frmWarrantPreview()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWarrantPreview InstancePtr
		{
			get
			{
				return (frmWarrantPreview)Sys.GetInstance(typeof(frmWarrantPreview));
			}
		}

		protected frmWarrantPreview _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/17/02
		// This form will be used by towns to select Unposted AP
		// Journals to run a Warrant Preview report on
		// ********************************************************
		int PostCol;
		int JournalCol;
		int JournalDescriptionCol;
		int PeriodCol;
		int AmountCol;
		int CountCol;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsJournals = new clsDRWrapper();
		clsDRWrapper rsJournals_AutoInitialized;

		clsDRWrapper rsJournals
		{
			get
			{
				if (rsJournals_AutoInitialized == null)
				{
					rsJournals_AutoInitialized = new clsDRWrapper();
				}
				return rsJournals_AutoInitialized;
			}
			set
			{
				rsJournals_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsAPJournal = new clsDRWrapper();
		clsDRWrapper rsAPJournal_AutoInitialized;

		clsDRWrapper rsAPJournal
		{
			get
			{
				if (rsAPJournal_AutoInitialized == null)
				{
					rsAPJournal_AutoInitialized = new clsDRWrapper();
				}
				return rsAPJournal_AutoInitialized;
			}
			set
			{
				rsAPJournal_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsEntries = new clsDRWrapper();
		clsDRWrapper rsEntries_AutoInitialized;

		clsDRWrapper rsEntries
		{
			get
			{
				if (rsEntries_AutoInitialized == null)
				{
					rsEntries_AutoInitialized = new clsDRWrapper();
				}
				return rsEntries_AutoInitialized;
			}
			set
			{
				rsEntries_AutoInitialized = value;
			}
		}

		public int intPreviewChoice;
		string strAccount = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cSettingUtility setUtil = new cSettingUtility();
		private cSettingUtility setUtil_AutoInitialized;

		private cSettingUtility setUtil
		{
			get
			{
				if (setUtil_AutoInitialized == null)
				{
					setUtil_AutoInitialized = new cSettingUtility();
				}
				return setUtil_AutoInitialized;
			}
			set
			{
				setUtil_AutoInitialized = value;
			}
		}

		private void cmdChoose_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (cboPreviewAccount.SelectedIndex > -1)
			{
				intPreviewChoice = cboPreviewAccount.SelectedIndex + 1;
                PrintReport();
            }
			else
			{
				MessageBox.Show("You must make a selection before you may proceed with this Report.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

        private WarrantPreviewReportOptions BuildReportOptions()
        {
            var options = new WarrantPreviewReportOptions();

            options.AccountTitleOption = (WarrantAccountTitleOption)intPreviewChoice;
            options.PayDate = txtPayDate.Text.ToDate();
            options.EnteredOrder = chkEnteredOrder.Checked;

            for (int counter = 1; counter <= vsJournals.Rows - 1; counter++)
            {
                if (FCUtils.CBool(vsJournals.TextMatrix(counter, 0)))
                {
					options.SelectedJournals.Add(vsJournals.TextMatrix(counter, 1).ToIntegerValue());
                }
            }

            return options;
        }

		private void cmdPost_Click(object sender, System.EventArgs e)
		{
			int answer;
			int counter;
			string strJournals = "";
			bool blnSelected;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			
			blnSelected = false;
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(counter, 0)) == true)
				{
					blnSelected = true;
					rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))));
					if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
					{
						do
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
							{
								if (FCConvert.ToInt32(rsJournalInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsJournalInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										// do nothing
									}
									else
									{
										MessageBox.Show("Vendor " + modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalInfo.Get_Fields_Int32("VendorNumber")), 5) + " has been deleted.  You will need to go enter the vendor back into the system before you may run your warrant preview.  If you remember entering the journal for this vendor please call TRIO and describe the steps you went through so we can prevent this from happening in the future.", "Enter Vendor Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
										return;
									}
								}
							}
							else
							{
								rsJournalInfo.Delete();
								rsJournalInfo.Update();
							}
							rsJournalInfo.MoveNext();
						}
						while (rsJournalInfo.EndOfFile() != true);
					}
				}
			}
			if (!blnSelected)
			{
				MessageBox.Show("You must first select 1 or more journals to run a Warrant Preview on before you may continue.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (intPreviewChoice == 5)
			{
				Label1.Visible = false;
				vsJournals.Visible = false;
				chkEnteredOrder.Visible = false;
				//cmdPost.Visible = false;
				cmdSelect.Visible = false;
				//cmdQuit.Visible = false;
				cboPreviewAccount.SelectedIndex = 1;
				fraChoose.Visible = true;
				return;
			}
			else
            {
                intPreviewChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("PreviewPrint"))));
				PrintReport();
            }
		}

        private void PrintReport()
        {
            clsDRWrapper rsTemp = new clsDRWrapper();

			// Get the Credit Memo Receivable Account
			rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CR'");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				strAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
			}
			else
			{
				MessageBox.Show("You must set up a Credit Memo Receivable Account before you may continue.", "No Credit Memo Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			if (!UpdateCreditMemos())
			{
				return;
			}

            var options = BuildReportOptions();
			var report = new FCSectionReport();

            if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("LandscapeWarrant")))
            {
                report = new rptWarrantPreviewLandscape(options);
            }
            else
            {
                report = new rptWarrantPreview(options);
            }

			if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
			{
                frmReportViewer.InstancePtr.Init(report, modReplaceWorkFiles.StripColon(Strings.Trim(((modRegistry.GetRegistryKey("PrinterDeviceName")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : ""))));
			}
			else
			{
                frmReportViewer.InstancePtr.Init(report);
            }
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			int intTotalCount = 0;
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
			Decimal curTotalAmount;
			int counter = 0;
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			rsJournals.OpenRecordset("SELECT * FROM Budgetary");
			rsJournals.Edit();
			rsJournals.Set_Fields("PayDate", txtPayDate.Text);
			rsJournals.Update(true);
			rsJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') ORDER BY JournalNumber, Period");
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				rsJournals.MoveLast();
				rsJournals.MoveFirst();
				counter = 1;
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE Payable <= '" + txtPayDate.Text + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' or Status = 'V')");
					if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
					{
						intTotalCount = 0;
						curTotalAmount = 0;
						while (rsAPJournal.EndOfFile() != true)
						{
							// Dave 6/2/06
							// If rsAPJournal.Fields["Period"] <> rsJournals.Fields["Period"] Then
							// rsCheckInfo.OpenRecordset ("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') ORDER BY JournalNumber, Period")  'AND Period = " & rsAPJournal.Fields["Period"] & "  Dave 6/2/06
							// If rsCheckInfo.EndOfFile <> True And rsCheckInfo.BeginningOfFile <> True Then
							// GoTo CheckNextJournal
							// End If
							// End If
							rsEntries.OpenRecordset("SELECT SUM(Discount) as TotalDiscount, SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
							if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
							{
								// TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
								intTotalCount += FCConvert.ToInt32(rsEntries.Get_Fields("TotalEntries"));
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								curTotalAmount += FCConvert.ToDecimal(rsEntries.Get_Fields_Decimal("TotalAmount") - rsEntries.Get_Fields("TotalDiscount"));
							}
							CheckNextJournal:
							;
							rsAPJournal.MoveNext();
						}
						vsJournals.TextMatrix(counter, PostCol, FCConvert.ToString(false));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						vsJournals.TextMatrix(counter, JournalCol, modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournals.Get_Fields("JournalNumber")), 4));
						vsJournals.TextMatrix(counter, JournalDescriptionCol, FCConvert.ToString(rsJournals.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						vsJournals.TextMatrix(counter, PeriodCol, FCConvert.ToString(rsJournals.Get_Fields("Period")));
						vsJournals.TextMatrix(counter, AmountCol, Strings.Format(curTotalAmount, "#,##0.00"));
						vsJournals.TextMatrix(counter, CountCol, FCConvert.ToString(intTotalCount));
						counter += 1;
					}
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
				vsJournals.Rows = counter;
			}
			else
			{
				vsJournals.Rows = 1;
			}
			//FC:FINAl:BBE - grid is anchored
			//if (vsJournals.Rows >= 15)
			//{
			//    vsJournals.Height = 15 * vsJournals.RowHeight(0) + 75;
			//}
			//else
			//{
			//    vsJournals.Height = vsJournals.Rows * vsJournals.RowHeight(0) + 75;
			//}
			Label1.Visible = true;
			vsJournals.Visible = true;
			chkEnteredOrder.Visible = true;
			//cmdPost.Visible = true;
			cmdSelect.Visible = true;
			//cmdQuit.Visible = true;
			fraPayDate.Visible = false;
			vsJournals.Focus();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(btnProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				vsJournals.TextMatrix(counter, PostCol, FCConvert.ToString(true));
			}
		}

		private void frmWarrantPreview_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rsTemp.OpenRecordset("SELECT * FROM Budgetary");
			txtPayDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPayDate.Focus();
			this.Refresh();
		}

		private void frmWarrantPreview_Load(object sender, System.EventArgs e)
		{
			PostCol = 0;
			JournalCol = 1;
			JournalDescriptionCol = 2;
			PeriodCol = 3;
			AmountCol = 4;
			CountCol = 5;
			vsJournals.TextMatrix(0, PostCol, "Select");
			vsJournals.TextMatrix(0, JournalCol, "Journal");
			vsJournals.TextMatrix(0, JournalDescriptionCol, "Description");
			vsJournals.TextMatrix(0, PeriodCol, "Period");
			vsJournals.TextMatrix(0, AmountCol, "Amount");
			vsJournals.TextMatrix(0, CountCol, "Entries");
			vsJournals.ColWidth(PostCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1));
			vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.15));
			vsJournals.ColWidth(JournalDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.4));
			vsJournals.ColWidth(PeriodCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.15));
			vsJournals.ColWidth(AmountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.2));
			vsJournals.ColWidth(CountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1));
			vsJournals.ColHidden(PeriodCol, true);
			vsJournals.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
            vsJournals.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			intPreviewChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("PreviewPrint"))));
			if (intPreviewChoice == 0)
			{
				intPreviewChoice = 5;
			}
		}

		private void frmWarrantPreview_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmWarrantPreview_Resize(object sender, System.EventArgs e)
		{
			vsJournals.ColWidth(PostCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1));
			vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.15));
			vsJournals.ColWidth(JournalDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.4));
			vsJournals.ColWidth(AmountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.2));
			vsJournals.ColWidth(CountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1));
			fraChoose.CenterToContainer(this.ClientArea);
			fraPayDate.CenterToContainer(this.ClientArea);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsJournals_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsJournals.Row > 0)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, PostCol)) == true)
				{
					vsJournals.TextMatrix(vsJournals.Row, PostCol, FCConvert.ToString(false));
				}
				else
				{
					vsJournals.TextMatrix(vsJournals.Row, PostCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsJournals_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, PostCol)) == true)
				{
					vsJournals.TextMatrix(vsJournals.Row, PostCol, FCConvert.ToString(false));
				}
				else
				{
					vsJournals.TextMatrix(vsJournals.Row, PostCol, FCConvert.ToString(true));
				}
			}
		}

		private bool UpdateCreditMemos()
		{
			bool UpdateCreditMemos = false;
			int counter;
			clsDRWrapper rsStatus = new clsDRWrapper();
			Decimal curRemovedMemos;
			Decimal curAddedMemos;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			string strMessage;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			UpdateCreditMemos = true;
			strMessage = "";
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (FCConvert.CBool(vsJournals.TextMatrix(counter, PostCol)))
				{
					curRemovedMemos = modBudgetaryMaster.RemoveExistingCreditMemos_2(FCConvert.ToInt32(FCConvert.ToDouble(vsJournals.TextMatrix(counter, JournalCol))));
					curAddedMemos = AddAvailableCreditMemos_2(FCConvert.ToInt32(FCConvert.ToDouble(vsJournals.TextMatrix(counter, JournalCol))));
					rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))));
					// & " AND Period = " & Val(vsJournals.TextMatrix(counter, PeriodCol))  Dave 6/2/06
					if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
					{
						rsJournalInfo.Edit();
						//FC:FINAL:MSH - Issue #801: 'Convert.To...' added for vsJournals.TextMatrix(), 
						// because vsJournals.TextMatrix() is string and after adding curRemovedMemos will be in incorrect format.
						rsJournalInfo.Set_Fields("TotalAmount", FCConvert.ToDecimal(vsJournals.TextMatrix(counter, AmountCol)) + curRemovedMemos);
						rsJournalInfo.Set_Fields("TotalCreditMemo", curAddedMemos);
						strMessage += "Journal " + vsJournals.TextMatrix(counter, JournalCol) + " - " + "\r\n" + "    Entered Amount: " + Strings.StrDup(13 - Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00").Length, " ") + Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00") + "\r\n" + "    Cred Mem Amount: " + Strings.StrDup(13 - Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00").Length, " ") + Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00") + "\r\n" + "-------------------------------------------------------" + "\r\n" + "    Net Amount: " + Strings.StrDup(19 - Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount") - rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00").Length, " ") + Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount") - rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00") + "\r\n";
						rsJournalInfo.Update();
					}
				}
			}
			answer = MessageBox.Show("You are running a warrant preview for" + "\r\n" + "\r\n" + strMessage + "\r\n" + "Is this correct?", "Correct Amounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.Yes)
			{
				for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsJournals.TextMatrix(counter, 0)) == true)
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))));
						// & " AND Period = " & Val(vsJournals.TextMatrix(counter, PeriodCol))
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							modBudgetaryMaster.WriteAuditRecord_8("Accepted Journal " + vsJournals.TextMatrix(counter, JournalCol) + " for " + Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00") + " to be run through the AP process.", "Warrant Preview");
						}
					}
				}
			}
			else
			{
				for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsJournals.TextMatrix(counter, 0)) == true)
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))));
						// & " AND Period = " & Val(vsJournals.TextMatrix(counter, PeriodCol))
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("TotalAmount", 0);
							rsJournalInfo.Update();
						}
					}
				}
				UpdateCreditMemos = false;
				return UpdateCreditMemos;
			}
			return UpdateCreditMemos;
		}

		private Decimal AddAvailableCreditMemos_2(int lngJournal)
		{
			return AddAvailableCreditMemos(ref lngJournal);
		}

		private Decimal AddAvailableCreditMemos(ref int lngJournal)
		{
			Decimal AddAvailableCreditMemos = 0;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsCreditInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsAP = new clsDRWrapper();
			int counter = 0;
			modBudgetaryAccounting.FundType[] ftFundData = null;
			modBudgetaryAccounting.FundType[] ftAccountData = null;
			rsInfo.OpenRecordset("SELECT VendorNumber FROM APJournal WHERE rtrim(Isnull(CheckNumber, '')) = '' AND Isnull(Seperate, 0) = 0 AND VendorNumber <> 0 AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " GROUP BY VendorNumber ORDER BY VendorNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsCreditInfo.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'P' AND VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber") + " AND IsNull(Amount, 0) - IsNull(Liquidated, 0) + IsNull(Adjustments, 0) > 0 ORDER BY CreditMemoDate");
					if (rsCreditInfo.EndOfFile() != true && rsCreditInfo.BeginningOfFile() != true)
					{
						rsAP.OpenRecordset("SELECT * FROM APJournal WHERE rtrim(Isnull(CheckNumber, '')) = '' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber") + " AND Isnull(Seperate, 0) = 0");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID IN (SELECT ID FROM APJournal WHERE rtrim(Isnull(CheckNumber, '')) = '' AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber") + " AND Isnull(Seperate, 0) = 0)");
						counter = 0;
						do
						{
							Array.Resize(ref ftAccountData, counter + 1);
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							ftAccountData[counter].Account = FCConvert.ToString(rsDetailInfo.Get_Fields("Account"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							ftAccountData[counter].Amount = FCConvert.ToDecimal(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
							ftAccountData[counter].AcctType = "A";
							ftAccountData[counter].Description = FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description"));
							counter += 1;
							rsDetailInfo.MoveNext();
						}
						while (rsDetailInfo.EndOfFile() != true);
						ftFundData = modBudgetaryAccounting.CalcFundCash(ref ftAccountData);
						ftAccountData = new modBudgetaryAccounting.FundType[0 + 1];
						for (counter = 1; counter <= 999; counter++)
						{
							if (ftFundData[counter].Amount > 0)
							{
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								AddAvailableCreditMemos += AddCreditMemo_654(lngJournal, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")), FCConvert.ToInt16(counter), ftFundData[counter].Amount, rsAP.Get_Fields_DateTime("Payable"), FCConvert.ToInt16(rsAP.Get_Fields("Period")));
							}
						}
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			rsInfo.OpenRecordset("SELECT * FROM APJournal WHERE rtrim(Isnull(CheckNumber, '')) = '' AND Isnull(Seperate, 0) = 1 AND VendorNumber <> 0 AND (Status = 'E' or Status = 'V') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " ORDER BY VendorNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsCreditInfo.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'P' AND VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber") + " AND IsNull(Amount, 0) - IsNull(Liquidated, 0) + IsNull(Adjustments, 0) > 0 ORDER BY CreditMemoDate");
					if (rsCreditInfo.EndOfFile() != true && rsCreditInfo.BeginningOfFile() != true)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsInfo.Get_Fields_Int32("ID"));
						counter = 0;
						do
						{
							Array.Resize(ref ftAccountData, counter + 1);
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							ftAccountData[counter].Account = FCConvert.ToString(rsDetailInfo.Get_Fields("Account"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							ftAccountData[counter].Amount = FCConvert.ToDecimal(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
							ftAccountData[counter].AcctType = "A";
							ftAccountData[counter].Description = FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description"));
							counter += 1;
							rsDetailInfo.MoveNext();
						}
						while (rsDetailInfo.EndOfFile() != true);
						ftFundData = modBudgetaryAccounting.CalcFundCash(ref ftAccountData);
						ftAccountData = new modBudgetaryAccounting.FundType[0 + 1];
						for (counter = 1; counter <= 999; counter++)
						{
							if (ftFundData[counter].Amount > 0)
							{
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								AddAvailableCreditMemos += AddCreditMemo_2112(lngJournal, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")), FCConvert.ToInt16(counter), ftFundData[counter].Amount, rsInfo.Get_Fields_DateTime("Payable"), FCConvert.ToInt16(rsInfo.Get_Fields("Period")), FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
							}
						}
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			return AddAvailableCreditMemos;
		}
		// vbPorter upgrade warning: intFund As short	OnWriteFCConvert.ToInt32(
		private Decimal AddCreditMemo_654(int lngJournal, int lngVendor, short intFund, Decimal curAmount, DateTime datPayable, short intPeriod, int lngSepRecNum = 0, bool blnSchool = false)
		{
			return AddCreditMemo(ref lngJournal, ref lngVendor, ref intFund, ref curAmount, ref datPayable, ref intPeriod, lngSepRecNum, blnSchool);
		}

		private Decimal AddCreditMemo_2112(int lngJournal, int lngVendor, short intFund, Decimal curAmount, DateTime datPayable, short intPeriod, int lngSepRecNum = 0, bool blnSchool = false)
		{
			return AddCreditMemo(ref lngJournal, ref lngVendor, ref intFund, ref curAmount, ref datPayable, ref intPeriod, lngSepRecNum, blnSchool);
		}

		private Decimal AddCreditMemo(ref int lngJournal, ref int lngVendor, ref short intFund, ref Decimal curAmount, ref DateTime datPayable, ref short intPeriod, int lngSepRecNum = 0, bool blnSchool = false)
		{
			Decimal AddCreditMemo = 0;
			clsDRWrapper rsCredit = new clsDRWrapper();
			clsDRWrapper rsAP = new clsDRWrapper();
			// vbPorter upgrade warning: curRemAmount As Decimal	OnWrite(Decimal, short)
			Decimal curRemAmount;
			clsDRWrapper rsDetail = new clsDRWrapper();
			clsDRWrapper rsCreditDetail = new clsDRWrapper();
			// vbPorter upgrade warning: curDetailAmount As Decimal	OnWrite(short, Decimal)
			Decimal curDetailAmount;
			curRemAmount = curAmount;
			rsCredit.OmitNullsOnInsert = true;
			rsDetail.OmitNullsOnInsert = true;
			rsAP.OmitNullsOnInsert = true;
			rsCredit.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'P' AND VendorNumber = " + FCConvert.ToString(lngVendor) + " AND Fund = '" + Strings.Format(intFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "' AND IsNull(Amount, 0) - IsNull(Liquidated, 0) + IsNull(Adjustments, 0) > 0 ORDER BY CreditMemoDate");
			if (rsCredit.EndOfFile() != true && rsCredit.BeginningOfFile() != true)
			{
				do
				{
					rsAP.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
					rsAP.AddNew();
					rsAP.Set_Fields("JournalNumber", lngJournal);
					rsAP.Set_Fields("VendorNumber", lngVendor);
					rsAP.Set_Fields("Description", rsCredit.Get_Fields_String("Description"));
					rsAP.Set_Fields("Reference", rsCredit.Get_Fields_String("MemoNumber"));
					rsAP.Set_Fields("Seperate", false);
					rsAP.Set_Fields("Returned", "N");
					rsAP.Set_Fields("Status", "E");
					rsAP.Set_Fields("CreditMemoRecord", rsCredit.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (curRemAmount > rsCredit.Get_Fields("Amount") - rsCredit.Get_Fields_Decimal("Liquidated") + rsCredit.Get_Fields_Decimal("Adjustments"))
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsAP.Set_Fields("Amount", FCConvert.ToDecimal(rsCredit.Get_Fields("Amount") - rsCredit.Get_Fields_Decimal("Liquidated") + rsCredit.Get_Fields_Decimal("Adjustments")) * -1);
					}
					else
					{
						rsAP.Set_Fields("Amount", curRemAmount * -1);
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					AddCreditMemo += (FCConvert.ToDecimal(rsAP.Get_Fields("Amount")) * -1);
					rsAP.Set_Fields("Payable", datPayable);
					rsAP.Set_Fields("Period", intPeriod);
					rsAP.Set_Fields("SeperateCheckRecord", lngSepRecNum);
					rsAP.Update();
					rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
					rsDetail.AddNew();
					rsDetail.Set_Fields("APJournalID", rsAP.Get_Fields_Int32("ID"));
					rsDetail.Set_Fields("Description", "Credit Memo");
					if (modAccountTitle.Statics.YearFlag)
					{
						rsDetail.Set_Fields("Account", "G " + Strings.Format(intFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strAccount);
					}
					else
					{
						rsDetail.Set_Fields("Account", "G " + Strings.Format(intFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strAccount + "-00");
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					rsDetail.Set_Fields("Amount", rsAP.Get_Fields("Amount"));
					rsDetail.Set_Fields("1099", "D");
					rsDetail.Update();
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curDetailAmount = FCConvert.ToDecimal(rsAP.Get_Fields("Amount"));
					rsCreditDetail.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE Amount + Adjustments - Liquidated > 0 AND CreditMemoID = " + rsCredit.Get_Fields_Int32("ID"));
					if (rsCreditDetail.EndOfFile() != true && rsCreditDetail.BeginningOfFile() != true)
					{
						do
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							if (curDetailAmount * -1 <= (rsCreditDetail.Get_Fields("Amount") + rsCreditDetail.Get_Fields_Decimal("Adjustments") - rsCreditDetail.Get_Fields_Decimal("Liquidated")))
							{
								rsCreditDetail.Edit();
								rsCreditDetail.Set_Fields("Liquidated", rsCreditDetail.Get_Fields_Decimal("Liquidated") - curDetailAmount);
								curDetailAmount = 0;
								rsCreditDetail.Update();
								break;
							}
							else
							{
								rsCreditDetail.Edit();
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDetailAmount += (rsCreditDetail.Get_Fields("Amount") + rsCreditDetail.Get_Fields_Decimal("Adjustments") - rsCreditDetail.Get_Fields_Decimal("Liquidated"));
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								rsCreditDetail.Set_Fields("Liquidated", rsCreditDetail.Get_Fields_Decimal("Liquidated") + (rsCreditDetail.Get_Fields("Amount") + rsCreditDetail.Get_Fields_Decimal("Adjustments") - rsCreditDetail.Get_Fields_Decimal("Liquidated")));
								rsCreditDetail.Update();
							}
							rsCreditDetail.MoveNext();
						}
						while (rsCreditDetail.EndOfFile() != true);
					}
					rsCredit.Edit();
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (curRemAmount > rsCredit.Get_Fields("Amount") - rsCredit.Get_Fields_Decimal("Liquidated") + rsCredit.Get_Fields_Decimal("Adjustments"))
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curRemAmount += FCConvert.ToDecimal(rsCredit.Get_Fields("Amount") - rsCredit.Get_Fields_Decimal("Liquidated") + rsCredit.Get_Fields_Decimal("Adjustments")) * -1;
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsCredit.Set_Fields("Liquidated", rsCredit.Get_Fields_Decimal("Liquidated") + rsCredit.Get_Fields("Amount") - rsCredit.Get_Fields_Decimal("Liquidated") + rsCredit.Get_Fields_Decimal("Adjustments"));
					}
					else
					{
						rsCredit.Set_Fields("Liquidated", rsCredit.Get_Fields_Decimal("Liquidated") + curRemAmount);
						curRemAmount = 0;
					}
					rsCredit.Update();
					if (curRemAmount == 0)
					{
						break;
					}
					rsCredit.MoveNext();
				}
				while (rsCredit.EndOfFile() != true);
			}
			return AddCreditMemo;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			if (fraPayDate.Visible == true)
			{
				cmdProcess_Click(btnProcess, EventArgs.Empty);
			}
			else if (vsJournals.Visible == true)
			{
				cmdPost_Click(btnProcess, EventArgs.Empty);
			}
			else if (fraChoose.Visible == true)
			{
				cmdChoose_Click(btnProcess, EventArgs.Empty);
			}
		}
	}
}
