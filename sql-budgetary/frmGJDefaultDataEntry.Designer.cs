//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGJDefaultDataEntry.
	/// </summary>
	partial class frmGJDefaultDataEntry : BaseForm
	{
		public FCGrid vs1;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblNet;
		public fecherFoundation.FCLabel lblNetTotal;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGJDefaultDataEntry));
			this.vs1 = new fecherFoundation.FCGrid();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblNet = new fecherFoundation.FCLabel();
			this.lblNetTotal = new fecherFoundation.FCLabel();
			this.btnProcessDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblNet);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.lblNetTotal);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessDelete);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(251, 30);
			this.HeaderText.Text = "Edit GJ Default Types";
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 7;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 0;
			this.vs1.Location = new System.Drawing.Point(30, 120);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 16;
			this.vs1.Size = new System.Drawing.Size(1018, 337);
			this.vs1.StandardTab = false;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 5;
			this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.Vs1_EditingControlShowing1);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// txtDescription
			// 
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(130, 30);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(400, 40);
			this.txtDescription.TabIndex = 4;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(33, 16);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "TITLE";
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(30, 90);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(1018, 18);
			this.lblExpense.TabIndex = 2;
			this.lblExpense.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblNet
			// 
			this.lblNet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.lblNet.Location = new System.Drawing.Point(534, 474);
			this.lblNet.Name = "lblNet";
			this.lblNet.Size = new System.Drawing.Size(106, 16);
			this.lblNet.TabIndex = 1;
			this.lblNet.Text = "= NET ENTRY";
			// 
			// lblNetTotal
			// 
			this.lblNetTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.lblNetTotal.Location = new System.Drawing.Point(420, 474);
			this.lblNetTotal.Name = "lblNetTotal";
			this.lblNetTotal.Size = new System.Drawing.Size(110, 16);
			this.lblNetTotal.Text = "$0.00";
			this.lblNetTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnProcessDelete
			// 
			this.btnProcessDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessDelete.Enabled = false;
			this.btnProcessDelete.Location = new System.Drawing.Point(1022, 27);
			this.btnProcessDelete.Name = "btnProcessDelete";
			this.btnProcessDelete.Size = new System.Drawing.Size(91, 24);
			this.btnProcessDelete.TabIndex = 1;
			this.btnProcessDelete.Text = "Delete Entry";
			this.btnProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(489, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmGJDefaultDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGJDefaultDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit GJ Default Types";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmGJDefaultDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmGJDefaultDataEntry_Activated);
			this.Resize += new System.EventHandler(this.frmGJDefaultDataEntry_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGJDefaultDataEntry_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGJDefaultDataEntry_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}

      
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton btnProcessDelete;
	}
}