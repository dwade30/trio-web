//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetBreakdown.
	/// </summary>
	partial class frmBudgetBreakdown : BaseForm
	{
		public fecherFoundation.FCComboBox cmbExpense;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCComboBox cmbTown;
		public fecherFoundation.FCLabel lblTown;
		public fecherFoundation.FCComboBox cmbPercent;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCFrame fraAccountType;
		public Global.T2KBackFillDecimal txtTotalBudget;
		public fecherFoundation.FCGrid vsBreakdown;
		public fecherFoundation.FCGrid vsAccounts;
		public fecherFoundation.FCGrid vsBreakDown2;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessEven;
		public fecherFoundation.FCToolStripMenuItem mnuProcessClear;
		public fecherFoundation.FCToolStripMenuItem mnuProcessCalcPercent;
		public fecherFoundation.FCToolStripMenuItem mnuProcessCalcAmounts;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator3;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBudgetBreakdown));
			this.cmbExpense = new fecherFoundation.FCComboBox();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.cmbTown = new fecherFoundation.FCComboBox();
			this.lblTown = new fecherFoundation.FCLabel();
			this.cmbPercent = new fecherFoundation.FCComboBox();
			this.lblPercent = new fecherFoundation.FCLabel();
			this.fraAccountType = new fecherFoundation.FCFrame();
			this.txtTotalBudget = new Global.T2KBackFillDecimal();
			this.vsBreakdown = new fecherFoundation.FCGrid();
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.vsBreakDown2 = new fecherFoundation.FCGrid();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.lblAmount = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessEven = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessCalcPercent = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessCalcAmounts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.btnApplyEven = new fecherFoundation.FCButton();
			this.btnClearBreakdown = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).BeginInit();
			this.fraAccountType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsBreakdown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsBreakDown2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnApplyEven)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClearBreakdown)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 781);
			this.BottomPanel.Size = new System.Drawing.Size(590, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAccountType);
			this.ClientArea.Controls.Add(this.cmbPercent);
			this.ClientArea.Controls.Add(this.lblPercent);
			this.ClientArea.Controls.Add(this.txtTotalBudget);
			this.ClientArea.Controls.Add(this.vsBreakdown);
			this.ClientArea.Controls.Add(this.vsAccounts);
			this.ClientArea.Controls.Add(this.vsBreakDown2);
			this.ClientArea.Controls.Add(this.Label12);
			this.ClientArea.Controls.Add(this.Label10);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.lblAmount);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.Label11);
			this.ClientArea.Size = new System.Drawing.Size(610, 444);
			this.ClientArea.Controls.SetChildIndex(this.Label11, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label8, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblAmount, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblTitle, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label10, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label12, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsBreakDown2, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsAccounts, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsBreakdown, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtTotalBudget, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPercent, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbPercent, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraAccountType, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnClearBreakdown);
			this.TopPanel.Controls.Add(this.btnApplyEven);
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnApplyEven, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnClearBreakdown, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(313, 30);
			this.HeaderText.Text = "Monthly Budget Breakdown";
			// 
			// cmbExpense
			// 
			this.cmbExpense.Items.AddRange(new object[] {
            "Expense Accounts",
            "Revenue Accounts"});
			this.cmbExpense.Location = new System.Drawing.Point(183, 30);
			this.cmbExpense.Name = "cmbExpense";
			this.cmbExpense.Size = new System.Drawing.Size(180, 40);
			this.cmbExpense.TabIndex = 26;
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(20, 44);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(93, 16);
			this.lblExpense.TabIndex = 27;
			this.lblExpense.Text = "ACCOUNT TYPE";
			// 
			// cmbTown
			// 
			this.cmbTown.Items.AddRange(new object[] {
            "Town"});
			this.cmbTown.Location = new System.Drawing.Point(183, 30);
			this.cmbTown.Name = "cmbTown";
			this.cmbTown.Size = new System.Drawing.Size(180, 40);
			this.cmbTown.TabIndex = 28;
			this.cmbTown.Visible = false;
			// 
			// lblTown
			// 
			this.lblTown.Location = new System.Drawing.Point(20, 44);
			this.lblTown.Name = "lblTown";
			this.lblTown.Size = new System.Drawing.Size(32, 16);
			this.lblTown.TabIndex = 29;
			this.lblTown.Text = "TYPE";
			this.lblTown.Visible = false;
			// 
			// cmbPercent
			// 
			this.cmbPercent.Items.AddRange(new object[] {
            "Amount",
            "Percent",
            "Show Both"});
			this.cmbPercent.Location = new System.Drawing.Point(195, 325);
			this.cmbPercent.Name = "cmbPercent";
			this.cmbPercent.Size = new System.Drawing.Size(150, 40);
			this.cmbPercent.TabIndex = 21;
			this.cmbPercent.Text = "Amount";
			this.cmbPercent.Visible = false;
			this.cmbPercent.SelectedIndexChanged += new System.EventHandler(this.cmbPercent_SelectedIndexChanged);
			// 
			// lblPercent
			// 
			this.lblPercent.Location = new System.Drawing.Point(30, 339);
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.Size = new System.Drawing.Size(98, 16);
			this.lblPercent.TabIndex = 22;
			this.lblPercent.Text = "BREAKDOWN BY";
			this.lblPercent.Visible = false;
			// 
			// fraAccountType
			// 
			this.fraAccountType.Controls.Add(this.lblExpense);
			this.fraAccountType.Controls.Add(this.cmbTown);
			this.fraAccountType.Controls.Add(this.lblTown);
			this.fraAccountType.Controls.Add(this.cmbExpense);
			this.fraAccountType.Location = new System.Drawing.Point(30, 66);
			this.fraAccountType.Name = "fraAccountType";
			this.fraAccountType.Size = new System.Drawing.Size(383, 90);
			this.fraAccountType.TabIndex = 20;
			this.fraAccountType.Text = "Account Type";
			// 
			// txtTotalBudget
			// 
			this.txtTotalBudget.Location = new System.Drawing.Point(195, 385);
			this.txtTotalBudget.MaxLength = 13;
			this.txtTotalBudget.Name = "txtTotalBudget";
			this.txtTotalBudget.Size = new System.Drawing.Size(150, 22);
			this.txtTotalBudget.Visible = false;
			this.txtTotalBudget.Validating += new System.ComponentModel.CancelEventHandler(this.txtTotalBudget_Validate);
			// 
			// vsBreakdown
			// 
			this.vsBreakdown.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsBreakdown.Cols = 6;
			this.vsBreakdown.FixedCols = 0;
			this.vsBreakdown.Location = new System.Drawing.Point(120, 485);
			this.vsBreakdown.Name = "vsBreakdown";
			this.vsBreakdown.RowHeadersVisible = false;
			this.vsBreakdown.Rows = 3;
			this.vsBreakdown.Size = new System.Drawing.Size(381, 120);
			this.vsBreakdown.StandardTab = false;
			this.vsBreakdown.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsBreakdown.TabIndex = 4;
			this.vsBreakdown.Visible = false;
			this.vsBreakdown.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsBreakdown_KeyPressEdit);
			this.vsBreakdown.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsBreakdown_AfterEdit);
			this.vsBreakdown.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsBreakdown_ValidateEdit);
			this.vsBreakdown.CurrentCellChanged += new System.EventHandler(this.vsBreakdown_RowColChange);
			this.vsBreakdown.Enter += new System.EventHandler(this.vsBreakdown_Enter);
			this.vsBreakdown.Click += new System.EventHandler(this.vsBreakdown_ClickEvent);
			// 
			// vsAccounts
			// 
			this.vsAccounts.FixedCols = 0;
			this.vsAccounts.Location = new System.Drawing.Point(30, 106);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.RowHeadersVisible = false;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.Size = new System.Drawing.Size(315, 200);
			this.vsAccounts.TabIndex = 5;
			this.vsAccounts.Visible = false;
			this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
			// 
			// vsBreakDown2
			// 
			this.vsBreakDown2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsBreakDown2.Cols = 6;
			this.vsBreakDown2.FixedCols = 0;
			this.vsBreakDown2.Location = new System.Drawing.Point(120, 625);
			this.vsBreakDown2.Name = "vsBreakDown2";
			this.vsBreakDown2.RowHeadersVisible = false;
			this.vsBreakDown2.Rows = 3;
			this.vsBreakDown2.Size = new System.Drawing.Size(381, 120);
			this.vsBreakDown2.StandardTab = false;
			this.vsBreakDown2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsBreakDown2.TabIndex = 6;
			this.vsBreakDown2.Visible = false;
			this.vsBreakDown2.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsBreakDown2_KeyPressEdit);
			this.vsBreakDown2.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsBreakDown2_AfterEdit);
			this.vsBreakDown2.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsBreakDown2_ValidateEdit);
			this.vsBreakDown2.CurrentCellChanged += new System.EventHandler(this.vsBreakDown2_RowColChange);
			this.vsBreakDown2.Enter += new System.EventHandler(this.vsBreakDown2_Enter);
			this.vsBreakDown2.Click += new System.EventHandler(this.vsBreakDown2_ClickEvent);
			// 
			// Label12
			// 
			this.Label12.AutoSize = true;
			this.Label12.Location = new System.Drawing.Point(121, 30);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(760, 16);
			this.Label12.TabIndex = 27;
			this.Label12.Text = "THE BREAKDOWN AMOUNTS ARE SAVED AS PERCENTAGES; AMOUNTS COULD CHANGE SLIGHTLY AFT" +
    "ER SAVING";
			// 
			// Label10
			// 
			this.Label10.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.Label10.Location = new System.Drawing.Point(30, 30);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(67, 16);
			this.Label10.TabIndex = 26;
			this.Label10.Text = "ATTENTION";
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblTitle.Location = new System.Drawing.Point(30, 445);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(216, 20);
			this.lblTitle.TabIndex = 18;
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTitle.Visible = false;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(267, 71);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(75, 16);
			this.Label4.TabIndex = 17;
			this.Label4.Text = "BREAKDOWN";
			this.Label4.Visible = false;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(80, 71);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(98, 16);
			this.Label3.TabIndex = 16;
			this.Label3.Text = "NO BREAKDOWN";
			this.Label3.Visible = false;
			// 
			// Label2
			// 
			this.Label2.BackColor = System.Drawing.Color.FromArgb(0, 0, 255);
			this.Label2.Location = new System.Drawing.Point(213, 66);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(30, 20);
			this.Label2.TabIndex = 15;
			this.Label2.Visible = false;
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.Label1.Location = new System.Drawing.Point(30, 66);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(30, 20);
			this.Label1.TabIndex = 14;
			this.Label1.Visible = false;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 765);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(120, 16);
			this.Label5.TabIndex = 13;
			this.Label5.Text = "REMAINING AMOUNT";
			this.Label5.Visible = false;
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(185, 765);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(120, 16);
			this.lblAmount.TabIndex = 12;
			this.lblAmount.Text = " ";
			this.lblAmount.Visible = false;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 530);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(55, 16);
			this.Label6.TabIndex = 11;
			this.Label6.Text = "AMOUNT";
			this.Label6.Visible = false;
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(30, 570);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(55, 16);
			this.Label7.TabIndex = 10;
			this.Label7.Text = "PERCENT";
			this.Label7.Visible = false;
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 670);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(55, 16);
			this.Label8.TabIndex = 9;
			this.Label8.Text = "AMOUNT";
			this.Label8.Visible = false;
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 710);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(55, 16);
			this.Label9.TabIndex = 8;
			this.Label9.Text = "PERCENT";
			this.Label9.Visible = false;
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(30, 399);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(90, 16);
			this.Label11.TabIndex = 7;
			this.Label11.Text = "TOTAL BUDGET";
			this.Label11.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessEven,
            this.mnuProcessClear,
            this.mnuProcessCalcPercent,
            this.mnuProcessCalcAmounts,
            this.mnuSeperator3,
            this.mnuProcessSave,
            this.mnuFileSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessEven
			// 
			this.mnuProcessEven.Enabled = false;
			this.mnuProcessEven.Index = 0;
			this.mnuProcessEven.Name = "mnuProcessEven";
			this.mnuProcessEven.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuProcessEven.Text = "Apply Evenly";
			this.mnuProcessEven.Click += new System.EventHandler(this.mnuProcessEven_Click);
			// 
			// mnuProcessClear
			// 
			this.mnuProcessClear.Enabled = false;
			this.mnuProcessClear.Index = 1;
			this.mnuProcessClear.Name = "mnuProcessClear";
			this.mnuProcessClear.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuProcessClear.Text = "Clear Breakdown";
			this.mnuProcessClear.Click += new System.EventHandler(this.mnuProcessClear_Click);
			// 
			// mnuProcessCalcPercent
			// 
			this.mnuProcessCalcPercent.Enabled = false;
			this.mnuProcessCalcPercent.Index = 2;
			this.mnuProcessCalcPercent.Name = "mnuProcessCalcPercent";
			this.mnuProcessCalcPercent.Text = "Calculate Percentages";
			this.mnuProcessCalcPercent.Visible = false;
			this.mnuProcessCalcPercent.Click += new System.EventHandler(this.mnuProcessCalcPercent_Click);
			// 
			// mnuProcessCalcAmounts
			// 
			this.mnuProcessCalcAmounts.Enabled = false;
			this.mnuProcessCalcAmounts.Index = 3;
			this.mnuProcessCalcAmounts.Name = "mnuProcessCalcAmounts";
			this.mnuProcessCalcAmounts.Text = "Calculate Amounts";
			this.mnuProcessCalcAmounts.Visible = false;
			this.mnuProcessCalcAmounts.Click += new System.EventHandler(this.mnuProcessCalcAmounts_Click);
			// 
			// mnuSeperator3
			// 
			this.mnuSeperator3.Index = 4;
			this.mnuSeperator3.Name = "mnuSeperator3";
			this.mnuSeperator3.Text = "-";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Enabled = false;
			this.mnuProcessSave.Index = 5;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuProcessSave.Text = "Save";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Enabled = false;
			this.mnuFileSave.Index = 6;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Save & Exit";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 7;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 8;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(255, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(104, 48);
			this.btnProcess.TabIndex = 5;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// btnApplyEven
			// 
			this.btnApplyEven.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnApplyEven.Location = new System.Drawing.Point(359, 29);
			this.btnApplyEven.Name = "btnApplyEven";
			this.btnApplyEven.Shortcut = Wisej.Web.Shortcut.F3;
			this.btnApplyEven.Size = new System.Drawing.Size(96, 24);
			this.btnApplyEven.TabIndex = 2;
			this.btnApplyEven.Text = "Apply Evenly";
			this.btnApplyEven.Visible = false;
			this.btnApplyEven.Click += new System.EventHandler(this.btnApplyEven_Click);
			// 
			// btnClearBreakdown
			// 
			this.btnClearBreakdown.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnClearBreakdown.Location = new System.Drawing.Point(459, 29);
			this.btnClearBreakdown.Name = "btnClearBreakdown";
			this.btnClearBreakdown.Shortcut = Wisej.Web.Shortcut.F4;
			this.btnClearBreakdown.Size = new System.Drawing.Size(121, 24);
			this.btnClearBreakdown.TabIndex = 3;
			this.btnClearBreakdown.Text = "Clear Breakdown";
			this.btnClearBreakdown.Visible = false;
			this.btnClearBreakdown.Click += new System.EventHandler(this.btnClearBreakdown_Click);
			// 
			// frmBudgetBreakdown
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 504);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBudgetBreakdown";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Monthly Budget Breakdown";
			this.Load += new System.EventHandler(this.frmBudgetBreakdown_Load);
			this.Activated += new System.EventHandler(this.frmBudgetBreakdown_Activated);
			this.Resize += new System.EventHandler(this.frmBudgetBreakdown_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBudgetBreakdown_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBudgetBreakdown_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).EndInit();
			this.fraAccountType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsBreakdown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsBreakDown2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnApplyEven)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClearBreakdown)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
		private FCButton btnClearBreakdown;
		private FCButton btnApplyEven;
	}
}