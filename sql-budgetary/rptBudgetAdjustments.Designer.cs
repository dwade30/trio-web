﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBudgetAdjustments.
	/// </summary>
	partial class rptBudgetAdjustments
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBudgetAdjustments));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExtraInfo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldDate,
				this.fldJournal,
				this.fldDescription,
				this.fldDebits,
				this.fldCredits
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.MultiLine = false;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 3.4375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 3.5F;
			this.fldDate.MultiLine = false;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.63375F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 4.15625F;
			this.fldJournal.MultiLine = false;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldJournal.Text = "Field1";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.3125F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 4.53125F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; white-space: nowrap; ddo" + "-char-set: 1";
			this.fldDescription.Text = "Field1";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.5625F;
			// 
			// fldDebits
			// 
			this.fldDebits.Height = 0.1875F;
			this.fldDebits.Left = 6.125F;
			this.fldDebits.MultiLine = false;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldDebits.Text = "Field1";
			this.fldDebits.Top = 0F;
			this.fldDebits.Width = 0.90625F;
			// 
			// fldCredits
			// 
			this.fldCredits.Height = 0.1875F;
			this.fldCredits.Left = 7.09375F;
			this.fldCredits.MultiLine = false;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCredits.Text = "Field1";
			this.fldCredits.Top = 0F;
			this.fldCredits.Width = 0.875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblTitle1,
				this.lblAccount,
				this.Label16,
				this.Label17,
				this.lblExtraInfo,
				this.Line1,
				this.lblTitle2,
				this.Label18,
				this.Label19
			});
			this.PageHeader.Height = 0.9166667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Budget / Beg Bal  Adjustments";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.84375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.1875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 1.5F;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblTitle1.Text = "Expense";
			this.lblTitle1.Top = 0.21875F;
			this.lblTitle1.Width = 4.84375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.03125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Text = "Account ---------------------------";
			this.lblAccount.Top = 0.71875F;
			this.lblAccount.Width = 3.09375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.5F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label16.Text = "Date";
			this.Label16.Top = 0.71875F;
			this.Label16.Width = 0.59375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.15625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label17.Text = "Jrnl";
			this.Label17.Top = 0.71875F;
			this.Label17.Width = 0.3125F;
			// 
			// lblExtraInfo
			// 
			this.lblExtraInfo.Height = 0.1875F;
			this.lblExtraInfo.HyperLink = null;
			this.lblExtraInfo.Left = 7.09375F;
			this.lblExtraInfo.Name = "lblExtraInfo";
			this.lblExtraInfo.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblExtraInfo.Text = "Credits";
			this.lblExtraInfo.Top = 0.71875F;
			this.lblExtraInfo.Width = 0.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.90625F;
			this.Line1.Width = 7.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 0.90625F;
			this.Line1.Y2 = 0.90625F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 1.5F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblTitle2.Text = "Expense";
			this.lblTitle2.Top = 0.40625F;
			this.lblTitle2.Width = 4.84375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 6.125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label18.Text = "Debits";
			this.Label18.Top = 0.71875F;
			this.Label18.Width = 0.90625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4.53125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "Description----------- ";
			this.Label19.Top = 0.71875F;
			this.Label19.Width = 1.5625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.fldDebitTotal,
				this.fldCreditTotal,
				this.Line2
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 5.40625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Field1.Text = "Totals";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 0.65625F;
			// 
			// fldDebitTotal
			// 
			this.fldDebitTotal.Height = 0.1875F;
			this.fldDebitTotal.Left = 6.125F;
			this.fldDebitTotal.Name = "fldDebitTotal";
			this.fldDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDebitTotal.Text = "Field1";
			this.fldDebitTotal.Top = 0.03125F;
			this.fldDebitTotal.Width = 0.90625F;
			// 
			// fldCreditTotal
			// 
			this.fldCreditTotal.Height = 0.1875F;
			this.fldCreditTotal.Left = 7.09375F;
			this.fldCreditTotal.Name = "fldCreditTotal";
			this.fldCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCreditTotal.Text = "Field1";
			this.fldCreditTotal.Top = 0.03125F;
			this.fldCreditTotal.Width = 0.875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 5.40625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 2.5625F;
			this.Line2.X1 = 5.40625F;
			this.Line2.X2 = 7.96875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// rptBudgetAdjustments
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.989583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptBudgetAdjustments_ReportEndedAndCanceled);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExtraInfo;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
