﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeExpenseDetailVendorSummary.
	/// </summary>
	partial class frmCustomizeExpenseDetailVendorSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbWide;
		public fecherFoundation.FCLabel lblWide;
		public fecherFoundation.FCComboBox cmbShortDescriptions;
		public fecherFoundation.FCLabel lblShortDescriptions;
		public fecherFoundation.FCComboBox cmbPendingDetail;
		public fecherFoundation.FCLabel lblPendingDetail;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCFrame fraPreferences;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeExpenseDetailVendorSummary));
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.cmbWide = new fecherFoundation.FCComboBox();
            this.lblWide = new fecherFoundation.FCLabel();
            this.cmbShortDescriptions = new fecherFoundation.FCComboBox();
            this.lblShortDescriptions = new fecherFoundation.FCLabel();
            this.cmbPendingDetail = new fecherFoundation.FCComboBox();
            this.lblPendingDetail = new fecherFoundation.FCLabel();
            this.cdgFont = new fecherFoundation.FCCommonDialog();
            this.fraPreferences = new fecherFoundation.FCFrame();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).BeginInit();
            this.fraPreferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPreferences);
            this.ClientArea.Controls.Add(this.cmbPendingDetail);
            this.ClientArea.Controls.Add(this.lblPendingDetail);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(472, 30);
            this.HeaderText.Text = "Expense Vendor Summary Report Format";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "Print",
            "Display"});
            this.cmbPrint.Location = new System.Drawing.Point(477, 90);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(148, 40);
            this.ToolTip1.SetToolTip(this.cmbPrint, null);
            // 
            // lblPrint
            // 
            this.lblPrint.AutoSize = true;
            this.lblPrint.Location = new System.Drawing.Point(331, 104);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(88, 15);
            this.lblPrint.TabIndex = 1;
            this.lblPrint.Text = "REPORT USE";
            this.lblPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblPrint, null);
            // 
            // cmbWide
            // 
            this.cmbWide.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
            this.cmbWide.Location = new System.Drawing.Point(477, 30);
            this.cmbWide.Name = "cmbWide";
            this.cmbWide.Size = new System.Drawing.Size(148, 40);
            this.cmbWide.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cmbWide, null);
            // 
            // lblWide
            // 
            this.lblWide.AutoSize = true;
            this.lblWide.Location = new System.Drawing.Point(331, 44);
            this.lblWide.Name = "lblWide";
            this.lblWide.Size = new System.Drawing.Size(93, 15);
            this.lblWide.TabIndex = 5;
            this.lblWide.Text = "PAPER WIDTH";
            this.lblWide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblWide, null);
            // 
            // cmbShortDescriptions
            // 
            this.cmbShortDescriptions.Items.AddRange(new object[] {
            "Short Descriptions",
            "Long Descriptions"});
            this.cmbShortDescriptions.Location = new System.Drawing.Point(133, 30);
            this.cmbShortDescriptions.Name = "cmbShortDescriptions";
            this.cmbShortDescriptions.Size = new System.Drawing.Size(180, 40);
            this.cmbShortDescriptions.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.cmbShortDescriptions, null);
            // 
            // lblShortDescriptions
            // 
            this.lblShortDescriptions.AutoSize = true;
            this.lblShortDescriptions.Location = new System.Drawing.Point(20, 44);
            this.lblShortDescriptions.Name = "lblShortDescriptions";
            this.lblShortDescriptions.Size = new System.Drawing.Size(100, 15);
            this.lblShortDescriptions.TabIndex = 7;
            this.lblShortDescriptions.Text = "DESCRIPTIONS";
            this.ToolTip1.SetToolTip(this.lblShortDescriptions, null);
            // 
            // cmbPendingDetail
            // 
            this.cmbPendingDetail.Items.AddRange(new object[] {
            "Show Detail",
            "Show Summary Only",
            "Do Not Include" });
            this.cmbPendingDetail.Location = new System.Drawing.Point(203, 273);
            this.cmbPendingDetail.Name = "cmbPendingDetail";
            this.cmbPendingDetail.Size = new System.Drawing.Size(211, 40);
            this.cmbPendingDetail.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.cmbPendingDetail, null);
            // 
            // lblPendingDetail
            // 
            this.lblPendingDetail.AutoSize = true;
            this.lblPendingDetail.Location = new System.Drawing.Point(30, 287);
            this.lblPendingDetail.Name = "lblPendingDetail";
            this.lblPendingDetail.Size = new System.Drawing.Size(123, 15);
            this.lblPendingDetail.TabIndex = 20;
            this.lblPendingDetail.Text = "PENDING ACTIVITY";
            this.lblPendingDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblPendingDetail, null);
            // 
            // cdgFont
            // 
            this.cdgFont.Name = "cdgFont";
            this.cdgFont.Size = new System.Drawing.Size(0, 0);
            this.ToolTip1.SetToolTip(this.cdgFont, null);
            // 
            // fraPreferences
            // 
            this.fraPreferences.AppearanceKey = "groupBoxLeftBorder";
            this.fraPreferences.Controls.Add(this.cmbPrint);
            this.fraPreferences.Controls.Add(this.lblPrint);
            this.fraPreferences.Controls.Add(this.cmbWide);
            this.fraPreferences.Controls.Add(this.lblWide);
            this.fraPreferences.Controls.Add(this.cmbShortDescriptions);
            this.fraPreferences.Controls.Add(this.lblShortDescriptions);
            this.fraPreferences.Location = new System.Drawing.Point(30, 100);
            this.fraPreferences.Name = "fraPreferences";
            this.fraPreferences.Size = new System.Drawing.Size(645, 141);
            this.fraPreferences.TabIndex = 18;
            this.fraPreferences.Text = "General Preferences";
            this.ToolTip1.SetToolTip(this.fraPreferences, null);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(195, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(307, 40);
            this.txtDescription.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(124, 18);
            this.lblDescription.TabIndex = 15;
            this.lblDescription.Text = "REPORT FORMAT";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(80, 48);
            this.cmdProcessSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmCustomizeExpenseDetailVendorSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomizeExpenseDetailVendorSummary";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Expense - Vendor Summary Report Format";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomizeExpenseDetailVendorSummary_Load);
            this.Activated += new System.EventHandler(this.frmCustomizeExpenseDetailVendorSummary_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeExpenseDetailVendorSummary_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeExpenseDetailVendorSummary_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).EndInit();
            this.fraPreferences.ResumeLayout(false);
            this.fraPreferences.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
