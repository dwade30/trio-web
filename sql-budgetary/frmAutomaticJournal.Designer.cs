﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAutomaticJournals.
	/// </summary>
	partial class frmAutomaticJournals : BaseForm
	{
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsJournals = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.btnFileComplete = new fecherFoundation.FCButton();
			this.btnFileDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileComplete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileComplete);
			this.BottomPanel.Location = new System.Drawing.Point(0, 387);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsJournals);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(610, 327);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnFileDelete);
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(284, 30);
			this.HeaderText.Text = "Build Automatic Journals";
			// 
			// vsJournals
			// 
			this.vsJournals.AllowSelection = false;
			this.vsJournals.AllowUserToResizeColumns = false;
			this.vsJournals.AllowUserToResizeRows = false;
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsJournals.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsJournals.BackColorBkg = System.Drawing.Color.Empty;
			this.vsJournals.BackColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.BackColorSel = System.Drawing.Color.Empty;
			this.vsJournals.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsJournals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsJournals.ColumnHeadersHeight = 30;
			this.vsJournals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsJournals.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsJournals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsJournals.FixedCols = 0;
			this.vsJournals.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.FrozenCols = 0;
			this.vsJournals.GridColor = System.Drawing.Color.Empty;
			this.vsJournals.Location = new System.Drawing.Point(30, 66);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.ReadOnly = true;
			this.vsJournals.RowHeadersVisible = false;
			this.vsJournals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsJournals.RowHeightMin = 0;
			this.vsJournals.Rows = 50;
			this.vsJournals.ShowColumnVisibilityMenu = false;
			this.vsJournals.Size = new System.Drawing.Size(550, 230);
			this.vsJournals.StandardTab = true;
			this.vsJournals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsJournals.TabIndex = 0;
			this.vsJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsJournals_KeyDownEvent);
			this.vsJournals.Click += new System.EventHandler(this.vsJournals_ClickEvent);
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(284, 16);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "PLEASE SELECT THE JOURNAL(S) TO BE CREATED. ";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnFileComplete
			// 
			this.btnFileComplete.AppearanceKey = "acceptButton";
			this.btnFileComplete.Location = new System.Drawing.Point(233, 30);
			this.btnFileComplete.Name = "btnFileComplete";
			this.btnFileComplete.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileComplete.Size = new System.Drawing.Size(152, 48);
			this.btnFileComplete.TabIndex = 0;
			this.btnFileComplete.Text = "Create Journals";
			this.btnFileComplete.Click += new System.EventHandler(this.mnuFileComplete_Click);
			// 
			// btnFileDelete
			// 
			this.btnFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileDelete.AppearanceKey = "toolbarButton";
			this.btnFileDelete.Location = new System.Drawing.Point(518, 29);
			this.btnFileDelete.Name = "btnFileDelete";
			this.btnFileDelete.Size = new System.Drawing.Size(62, 24);
			this.btnFileDelete.TabIndex = 1;
			this.btnFileDelete.Text = "Delete";
			this.btnFileDelete.Visible = false;
			// 
			// frmAutomaticJournals
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(610, 495);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmAutomaticJournals";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Build Automatic Journals";
			this.Load += new System.EventHandler(this.frmAutomaticJournals_Load);
			this.Activated += new System.EventHandler(this.frmAutomaticJournals_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAutomaticJournals_KeyPress);
			this.Resize += new System.EventHandler(this.frmAutomaticJournals_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileComplete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnFileComplete;
		private FCButton btnFileDelete;
	}
}
