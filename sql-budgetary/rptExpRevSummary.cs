﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptExpRevSummary.
	/// </summary>
	public partial class rptExpRevSummary : BaseSectionReport
	{
		public static rptExpRevSummary InstancePtr
		{
			get
			{
				return (rptExpRevSummary)Sys.GetInstance(typeof(rptExpRevSummary));
			}
		}

		protected rptExpRevSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsDepartmentInfo.Dispose();
				rsDivisionInfo.Dispose();
				rsActivityDetail.Dispose();
				rsYTDActivity.Dispose();
				rsCurrentActivity.Dispose();
				rsExpBudgetInfo.Dispose();
				rsRevActivityDetail.Dispose();
				rsRevBudgetInfo.Dispose();
				rsRevCurrentActivity.Dispose();
				rsRevYTDActivity.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExpRevSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         12/13/02
		// This form will be used to print the Exp / Rev Summary
		// report
		// ********************************************************
		int PageCounter;
		// variable used for showing the page number in the page header section
		bool DeptBreakFlag;
		// variable for page breaks when reporting on a new department
		string strDepartmentRange = "";
		// reporting range A - All, D - Department Range, S - Single Department
		string strLowDepartment = "";
		string strHighDepartment = "";
		string strLowFund = "";
		string strHighFund = "";
		string strLowDetail = "";
		// lowesty detail to report on De - Department, Di - Division
		string strDateRange = "";
		// reporting range A - All, M - Month Range, S - Single Month
		int intLowDate;
		int intHighDate;
		bool blnIncEnc;
		// should encumbrance money be calculated into YTD amounts
		bool blnIncPending;
		// should pending activity be included in YTD amounts
		clsDRWrapper rsCurrentActivity = new clsDRWrapper();
		// recordset holds money activity within date rnage for selected departments in expenses
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		// recordset holds monthly money activity within date rnage for selected departments in expenses
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		// recordset holds YTD money activity selected departments in expenses
		clsDRWrapper rsRevCurrentActivity = new clsDRWrapper();
		// recordset holds money activity within date rnage for selected departments in revenues
		clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
		// recordset holds monthly money activity within date rnage for selected departments in revenues
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		// recordset holds YTD money activity selected departments in revenues
		clsDRWrapper rsExpBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsRevBudgetInfo = new clsDRWrapper();
		string strPeriodCheck = "";
		// AND or OR depending on if intLowDate > intHighDate
		string strExpOrRev = "";
		// R or E telling program whether we are reporting on Revenues or Expenses at the moment
		int intCurrentMonth;
		bool blnFirstRecord;
		// used in fetchdata to let us know not to move forward in the recordset on the first pass
		clsDRWrapper rsDepartmentInfo = new clsDRWrapper();
		// holds departments we are reporting on in report
		clsDRWrapper rsDivisionInfo = new clsDRWrapper();
		// holds division info for departments we are reporting on
		// vbPorter upgrade warning: curRevBudget As Decimal	OnWrite(Decimal, short)
		public Decimal curRevBudget;
		// vbPorter upgrade warning: curExpBudget As Decimal	OnWrite(Decimal, short)
		public Decimal curExpBudget;
		// vbPorter upgrade warning: curRevCurrentNet As Decimal	OnWrite(Decimal, short)
		public Decimal curRevCurrentNet;
		// vbPorter upgrade warning: curExpCurrentNet As Decimal	OnWrite(Decimal, short)
		public Decimal curExpCurrentNet;
		// currency values used to store totals for group footers
		// vbPorter upgrade warning: curRevYTDNet As Decimal	OnWrite(Decimal, short)
		public Decimal curRevYTDNet;
		// vbPorter upgrade warning: curExpYTDNet As Decimal	OnWrite(Decimal, short)
		public Decimal curExpYTDNet;
		// vbPorter upgrade warning: curRevBalance As Decimal	OnWrite(Decimal, short)
		public Decimal curRevBalance;
		// vbPorter upgrade warning: curExpBalance As Decimal	OnWrite(Decimal, short)
		public Decimal curExpBalance;
		string CurrentDepartment = "";
		// current department we are reporting on
		string CurrentDivision = "";
		// current division we are reporting on
		string strTitleToShow = "";

		public rptExpRevSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Exp / Rev Summary";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// set up binder values for groups in report
			this.Fields.Add("ExpRevBinder");
			this.Fields.Add("DepartmentBinder");
			this.Fields.Add("DeptBinder2");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			object strDeptHolder;
			// - "AutoDim"
			// is this the first record
			if (blnFirstRecord)
			{
				CheckDept:
				;
				// if low detail is department then check to see that there is information for this department for the type of information we are reporting on
				if (strExpOrRev == "E")
				{
					rsDivisionInfo.OpenRecordset("SELECT * FROM ExpenseReportInfo WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' order by department, division, expense, object");
				}
				else
				{
					rsDivisionInfo.OpenRecordset("SELECT * FROM RevenueReportInfo WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' order by department, division, revenue");
				}
				// if there is information
				if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
				{
					// set current department we are reporting for and set binder values
					CurrentDepartment = FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("Department"));
					CurrentDivision = "";
					this.Fields["DepartmentBinder"].Value = rsDepartmentInfo.Get_Fields_String("Department");
					this.Fields["ExpRevBinder"].Value = strExpOrRev;
					this.Fields["DeptBinder2"].Value = rsDepartmentInfo.Get_Fields_String("Department");
					eArgs.EOF = false;
					blnFirstRecord = false;
				}
				else
				{
					// if no information then if we are reporting on revenues try to report on expenses for this department
					if (strExpOrRev == "R")
					{
						strExpOrRev = "E";
						// if reporting on revenues see if there is information on expenses for this department
						goto CheckDept;
					}
					else
					{
						// if reporting on expenses go to the next department and report on revenues
						rsDepartmentInfo.MoveNext();
						// if there are no more dpeartments to report on then end the report
						if (rsDepartmentInfo.EndOfFile())
						{
							eArgs.EOF = true;
							return;
						}
						else
						{
							// otherwise check to see that there is information to report on for this department for revenues
							strExpOrRev = "R";
							goto CheckDept;
						}
					}
				}
			}
			else
			{
				// if departments are the lowest detail level to report on then if we just reported on revenues report on expenses
				if (strExpOrRev == "R")
				{
					strExpOrRev = "E";
				}
				else
				{
					// if we just reported on expenses move to next department and report on revenues
					strExpOrRev = "R";
					strDeptHolder = rsDepartmentInfo.Get_Fields_String("Department");
					rsDepartmentInfo.MoveNext();
					// if no more departments then end report
					if (rsDepartmentInfo.EndOfFile())
					{
						eArgs.EOF = true;
						return;
					}
				}
				CheckDept2:
				;
				// make sure there is information for the type we are reporting on
				if (strExpOrRev == "E")
				{
					rsDivisionInfo.OpenRecordset("SELECT * FROM ExpenseReportInfo WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' order by department, division, expense, object");
				}
				else
				{
					rsDivisionInfo.OpenRecordset("SELECT * FROM RevenueReportInfo WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' order by department, division, revenue");
				}
				// if there is information
				if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
				{
					// update current department, division, and binder values
					CurrentDepartment = FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("Department"));
					CurrentDivision = "";
					this.Fields["DepartmentBinder"].Value = rsDepartmentInfo.Get_Fields_String("Department");
					this.Fields["ExpRevBinder"].Value = strExpOrRev;
					this.Fields["DeptBinder2"].Value = rsDepartmentInfo.Get_Fields_String("Department");
					eArgs.EOF = false;
				}
				else
				{
					// if no informationon revneues try geting information on epxenses
					if (strExpOrRev == "R")
					{
						strExpOrRev = "E";
						goto CheckDept2;
					}
					else
					{
						// if no expense information go to next department and try to get revenue information
						rsDepartmentInfo.MoveNext();
						// if no more departments then end report
						if (rsDepartmentInfo.EndOfFile())
						{
							eArgs.EOF = true;
							return;
						}
						else
						{
							// get rev information on department
							strExpOrRev = "R";
							goto CheckDept2;
						}
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (!DeptBreakFlag)
			{
				GroupFooter3.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			else
			{
				GroupFooter3.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			blnFirstRecord = true;
			// If strLowDetail = "De" Then
			// lblDeptDiv = "Department"
			// Else
			// lblDeptDiv = "Dept \ Div"
			// End If
			if (strDepartmentRange == "SD")
			{
				lblDeptRange.Text = "Department(s): " + strLowDepartment;
			}
			else if (strDepartmentRange == "SF")
			{
				lblDeptRange.Text = "Fund(s): " + strLowFund;
			}
			else if (strDepartmentRange == "FD")
			{
				lblDeptRange.Text = "Department(s) in Fund: " + strLowFund;
			}
			else if (strDepartmentRange == "D")
			{
				lblDeptRange.Text = "Department(s): " + strLowDepartment + " - " + strHighDepartment;
			}
			else if (strDepartmentRange == "F")
			{
				lblDeptRange.Text = "Fund(s): " + strLowFund + " - " + strHighFund;
			}
			else
			{
				lblDeptRange.Text = "ALL Departments";
			}
			if (strDateRange == "S")
			{
				lblDateRange.Text = MonthCalc(intLowDate);
			}
			else if (strDateRange == "M")
			{
				lblDateRange.Text = MonthCalc(intLowDate) + " to " + MonthCalc(intHighDate);
			}
			else
			{
				lblDateRange.Text = "ALL Months";
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//clsDRWrapper rsDivisionTitle = new clsDRWrapper();
			// if lowest detail level is division
			bool executeGetTotals = false;
			if (strLowDetail != "De")
			{
				if (strExpOrRev == "E")
				{
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						if (strLowDetail == "Di")
						{
							subLowerLevelReport = null;
							executeGetTotals = true;
							goto GetTotals;
						}
						else
						{
                            var subReport = new srptExpRevSummaryExpLevel();
							subReport.Init(strLowDetail, strDateRange, blnIncEnc, blnIncPending, CurrentDepartment, null, FCConvert.ToInt16(intLowDate), FCConvert.ToInt16(intHighDate));
							subLowerLevelReport.Report = subReport;
						}
					}
					else
					{
                        var subReport = new srptExpRevSummaryDivLevel();
						subReport.Init(strLowDetail, strExpOrRev, strDateRange, blnIncEnc, blnIncPending, CurrentDepartment, FCConvert.ToInt16(intLowDate), FCConvert.ToInt16(intHighDate));
						subLowerLevelReport.Report = subReport;
					}
				}
				else
				{
					if (modAccountTitle.Statics.RevDivFlag)
					{
						if (strLowDetail == "Di")
						{
							subLowerLevelReport = null;
							executeGetTotals = true;
							goto GetTotals;
						}
						else
						{
                            var subReport = new srptExpRevSummaryRevLevel();
                            subReport.Init(strDateRange, blnIncEnc, blnIncPending, CurrentDepartment, null, FCConvert.ToInt16(intLowDate), FCConvert.ToInt16(intHighDate));
							subLowerLevelReport.Report = subReport;
						}
					}
					else
					{
                        var subReport = new srptExpRevSummaryDivLevel();
                        subReport.Init(strLowDetail, strExpOrRev, strDateRange, blnIncEnc, blnIncPending, CurrentDepartment, FCConvert.ToInt16(intLowDate), FCConvert.ToInt16(intHighDate));
						subLowerLevelReport.Report = subReport;
					}
				}
			}
			else
			{
				executeGetTotals = true;
				goto GetTotals;
			}
			GetTotals:
			;
			if (executeGetTotals)
			{
				// if department is low detail level then show no information
				subLowerLevelReport = null;
				// update totals
				if (strExpOrRev == "E")
				{
					curExpBudget += FCConvert.ToDecimal(GetNetBudget());
					curExpCurrentNet += FCConvert.ToDecimal(GetCurrentNet());
					curExpYTDNet += FCConvert.ToDecimal(GetYTDNet());
					curExpBalance += FCConvert.ToDecimal(GetBalance());
				}
				else
				{
					curRevBudget += FCConvert.ToDecimal(GetNetBudget());
					curRevCurrentNet += FCConvert.ToDecimal(GetCurrentNet());
					curRevYTDNet += FCConvert.ToDecimal(GetYTDNet());
					// Dave 4/9/03
					curRevBalance += FCConvert.ToDecimal(GetBalance());
				}
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			string strHolder = "";
			// if we are reporting on expenses
			if (lblExpRev.Text == "E X P E N S E S")
			{
				// show total information
				fldExpRevTotal.Text = "Expense Total";
				fldExpRevBudget.Text = Strings.Format(curExpBudget, "#,##0.00");
				fldExpRevCurrentMonth.Text = Strings.Format(curExpCurrentNet, "#,##0.00");
				fldExpRevYTD.Text = Strings.Format(curExpYTDNet, "#,##0.00");
				fldExpRevBalance.Text = Strings.Format(curExpBalance, "#,##0.00");
				if (curExpBudget != 0)
				{
					fldExpRevSpent.Text = Strings.Format(((curExpBudget - curExpBalance) / curExpBudget) * 100, "0.00");
				}
				else
				{
					fldExpRevSpent.Text = "0.00";
				}
			}
			else
			{
				fldExpRevTotal.Text = "Revenue Total";
				fldExpRevBudget.Text = Strings.Format(curRevBudget, "#,##0.00");
				fldExpRevCurrentMonth.Text = Strings.Format(curRevCurrentNet, "#,##0.00");
				fldExpRevYTD.Text = Strings.Format(curRevYTDNet, "#,##0.00");
				fldExpRevBalance.Text = Strings.Format(curRevBalance, "#,##0.00");
				if (curRevBudget != 0)
				{
					fldExpRevSpent.Text = Strings.Format(((curRevBudget - curRevBalance) / curRevBudget) * 100, "0.00");
				}
				else
				{
					fldExpRevSpent.Text = "0.00";
				}
			}
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			// show net profit or loss for department by using totals for expense and revenue sections
			if (curRevBudget - curExpBudget < 0)
			{
				fldDeptTotalBudget.Text = "(" + Strings.Format((curRevBudget - curExpBudget) * -1, "#,##0.00") + ")";
			}
			else
			{
				fldDeptTotalBudget.Text = Strings.Format(curRevBudget - curExpBudget, "#,##0.00");
			}
			if (curRevCurrentNet - curExpCurrentNet < 0)
			{
				fldDeptTotalCurrentMonth.Text = "(" + Strings.Format((curRevCurrentNet - curExpCurrentNet) * -1, "#,##0.00") + ")";
			}
			else
			{
				fldDeptTotalCurrentMonth.Text = Strings.Format(curRevCurrentNet - curExpCurrentNet, "#,##0.00");
			}
			if (curRevYTDNet - curExpYTDNet < 0)
			{
				fldDeptTotalYTD.Text = "(" + Strings.Format((curRevYTDNet - curExpYTDNet) * -1, "#,##0.00") + ")";
			}
			else
			{
				fldDeptTotalYTD.Text = Strings.Format(curRevYTDNet - curExpYTDNet, "#,##0.00");
			}
			if ((curRevBalance - curExpBalance) < 0)
			{
				fldDeptTotalBalance.Text = Strings.Format((curRevBalance - curExpBalance) * -1, "#,##0.00");
			}
			else
			{
				fldDeptTotalBalance.Text = "(" + Strings.Format((curRevBalance - curExpBalance), "#,##0.00") + ")";
			}
			// reset totals
			curExpBudget = 0;
			curExpCurrentNet = 0;
			curExpYTDNet = 0;
			curExpBalance = 0;
			curRevBudget = 0;
			curRevCurrentNet = 0;
			curRevYTDNet = 0;
			curRevBalance = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsDepartmentTitle = new clsDRWrapper())
            {
                // if department is lowest detail level then only show the title for revenues not expenses also
                if (strExpOrRev == "E" && strLowDetail == "De")
                {
                    if (curRevBudget != 0 || curRevCurrentNet != 0 || curRevYTDNet != 0 || curRevBalance != 0)
                    {
                        fldDeptTitle.Text = "";
                        goto DontShowTitleTag;
                    }
                }

                // get department title
                if (modAccountTitle.Statics.ExpDivFlag)
                {
                    rsDepartmentTitle.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" +
                                                    CurrentDepartment + "'");
                }
                else
                {
                    rsDepartmentTitle.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" +
                                                    CurrentDepartment + "' AND Division = '" +
                                                    modValidateAccount.GetFormat_6("0",
                                                        FCConvert.ToInt16(
                                                            Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "'");
                }

                if (rsDepartmentTitle.EndOfFile() != true && rsDepartmentTitle.BeginningOfFile() != true)
                {
                    fldDeptTitle.Text = CurrentDepartment + "  " +
                                        rsDepartmentTitle.Get_Fields_String("ShortDescription");
                    strTitleToShow = CurrentDepartment + "  " + rsDepartmentTitle.Get_Fields_String("ShortDescription");
                }
                else
                {
                    fldDeptTitle.Text = CurrentDepartment + "  UNKNOWN";
                    strTitleToShow = CurrentDepartment + "  UNKNOWN";
                }

                DontShowTitleTag:
                ;
            }
        }

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			if (strExpOrRev == "R")
			{
				lblExpRev.Text = "R E V E N U E S";
			}
			else
			{
				lblExpRev.Text = "E X P E N S E S";
			}
			if (strLowDetail == "De")
			{
				lblExpRev.Visible = false;
			}
			else
			{
				lblExpRev.Visible = true;
			}
		}

		private void GroupHeader4_Format(object sender, EventArgs e)
		{
			if (this.PageNumber == 1 || DeptBreakFlag)
			{
				fldDeptTitle2.Text = "";
				fldDeptTitle2.Visible = false;
			}
			else
			{
				fldDeptTitle2.Text = strTitleToShow + " CONT'D";
				fldDeptTitle2.Visible = true;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}
		// vbPorter upgrade warning: intLowDept As short	OnWrite(string)
		// vbPorter upgrade warning: intHighDept As short	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intLowMonth As short	OnWrite(string, double)
		// vbPorter upgrade warning: intHighMonth As short	OnWrite(string, double)
		public void Init(string strDeptRange, string strLowestDetail, string strMonthRange, bool blnIncludeEncumbrances, bool blnIncludePendingInformation, bool blnPageBreak, short intLowDept = 0, short intHighDept = 0, short intLowMonth = 0, short intHighMonth = 0)
		{
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Building Report", true);
			frmWait.InstancePtr.Show();
			//this.Refresh();
			// initialize all variables for this report
			strDepartmentRange = strDeptRange;
			strLowDetail = strLowestDetail;
			strDateRange = strMonthRange;
			blnIncEnc = blnIncludeEncumbrances;
			blnIncPending = blnIncludePendingInformation;
			DeptBreakFlag = blnPageBreak;
			if (strDepartmentRange == "A")
			{
				strLowDepartment = modValidateAccount.GetFormat_6(FCConvert.ToString(1), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Exp, 2)));
				strHighDepartment = Strings.StrDup(FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)), "9");
				strLowFund = modValidateAccount.GetFormat_6(FCConvert.ToString(1), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
				strHighFund = Strings.StrDup(FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)), "9");
			}
			else if (strDepartmentRange == "D")
			{
				strLowDepartment = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Exp, 2)));
				strHighDepartment = modValidateAccount.GetFormat_6(FCConvert.ToString(intHighDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Exp, 2)));
			}
			else if (strDepartmentRange == "FD")
			{
				strLowFund = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
				strHighFund = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
			}
			else if (strDepartmentRange == "F")
			{
				strLowFund = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
				strHighFund = modValidateAccount.GetFormat_6(FCConvert.ToString(intHighDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
			}
			else if (strDepartmentRange == "SF")
			{
				strLowFund = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
				strHighFund = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
			}
			else
			{
				strLowDepartment = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Exp, 2)));
				strHighDepartment = modValidateAccount.GetFormat_6(FCConvert.ToString(intLowDept), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Exp, 2)));
			}
			if (strDateRange == "A")
			{
				intLowDate = modBudgetaryMaster.Statics.FirstMonth;
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					intHighDate = 12;
				}
				else
				{
					intHighDate = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else if (strDateRange == "M")
			{
				intLowDate = intLowMonth;
				intHighDate = intHighMonth;
			}
			else
			{
				intLowDate = intLowMonth;
				intHighDate = intLowMonth;
			}
			if (intLowDate > intHighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			frmWait.InstancePtr.prgProgress.Value = 10;
			frmWait.InstancePtr.Refresh();
			// get totals information for expenses
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();
			// CalculateAccountInfo True, True, True, "E"
			// frmWait.prgProgress.Value = 40
			// frmWait.Refresh
			// get totals information for revenues
			// CalculateAccountInfo True, True, True, "R"
			frmWait.InstancePtr.prgProgress.Value = 70;
			frmWait.InstancePtr.Refresh();
			// put information into recordsets
			RetrieveInfo();
			// check to see if there are any departments with activity to report on
			if (strDepartmentRange == "SF" || strDepartmentRange == "F")
			{
				rsDepartmentInfo.OpenRecordset("SELECT DISTINCT DepartmentQuery.Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) AS DepartmentQuery INNER JOIN DeptDivTitles ON DepartmentQuery.Department = DeptDivTitles.Department WHERE Fund >= '" + strLowFund + "' AND Fund <= '" + strHighFund + "' order by departmentquery.department");
			}
			else if (strDepartmentRange == "FD")
			{
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					rsDepartmentInfo.OpenRecordset("SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) as tbl1 WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + strLowFund + "') order by department ");
				}
				else
				{
					rsDepartmentInfo.OpenRecordset("SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) as tbl1 WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + "' AND Fund = '" + strLowFund + "') order by department");
				}
			}
			else
			{
				rsDepartmentInfo.OpenRecordset("SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo WHERE Department >= '" + strLowDepartment + "' AND Department <= '" + strHighDepartment + "' UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo WHERE Department >= '" + strLowDepartment + "' AND Department <= '" + strHighDepartment + "') AS DepartmentQuery order by department");
			}
			if (rsDepartmentInfo.EndOfFile() != true && rsDepartmentInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No information found.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
				return;
			}
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Unload();
			strExpOrRev = "R";
			// depending on what the user selected print the report or preview it
			if (frmExpRevSummary.InstancePtr.blnPrint)
			{
				rptExpRevSummary.InstancePtr.PrintReport();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
			//Application.DoEvents();
			frmExpRevSummary.InstancePtr.Unload();
		}

		private void RetrieveInfo()
		{
			int HighDate;
			int LowDate;
			string strPeriodCheckHolder;
			HighDate = intHighDate;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (strLowDetail == "Di")
			{
				rsYTDActivity.OpenRecordset("SELECT Department, Division, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division order by  Department, Division");
				rsRevYTDActivity.OpenRecordset("SELECT Department, Division, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division order by  Department, Division");
				rsExpBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division order by  Department, Division");
				rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department, Division order by  Department, Division");
				strPeriodCheck = strPeriodCheckHolder;
				rsCurrentActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division order BY Department, Division");
				rsActivityDetail.OpenRecordset("SELECT Period, Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Period  order BY Department, Division, Period");
				rsRevCurrentActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division order BY Department, Division");
				rsRevActivityDetail.OpenRecordset("SELECT Period, Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Period order BY Department, Division, Period");
			}
			else
			{
				rsYTDActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department order by department");
				rsRevYTDActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department order by department");
				rsExpBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department order by department");
				rsRevBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department order by department");
				strPeriodCheck = strPeriodCheckHolder;
				rsCurrentActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department order by department");
				rsActivityDetail.OpenRecordset("SELECT Period, Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Period order by department,period");
				rsRevCurrentActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department order by department");
				rsRevActivityDetail.OpenRecordset("SELECT Period, Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Period order by department,period");
			}
		}

		private decimal GetCurrentDebits()
		{
			decimal GetCurrentDebits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetCurrentDebits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetCurrentDebits = 0;
							}
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (CurrentDivision == "")
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
								}
								else
								{
									GetCurrentDebits = 0;
								}
							}
						}
						else
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
								}
								else
								{
									GetCurrentDebits = 0;
								}
							}
						}
					}
					else
					{
						GetCurrentDebits = 0;
					}
				}
			}
			else
			{
				GetCurrentDebits = 0;
			}
			return GetCurrentDebits;
		}

		private decimal GetCurrentCredits()
		{
			decimal GetCurrentCredits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetCurrentCredits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetCurrentCredits = 0;
							}
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (CurrentDivision == "")
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
								}
								else
								{
									GetCurrentCredits = 0;
								}
							}
						}
						else
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
								}
								else
								{
									GetCurrentCredits = 0;
								}
							}
						}
					}
					else
					{
						GetCurrentCredits = 0;
					}
				}
			}
			else
			{
				GetCurrentCredits = 0;
			}
			return GetCurrentCredits;
		}

		private decimal GetCurrentNet()
		{
			decimal GetCurrentNet = 0;
			if (strExpOrRev == "E")
			{
				GetCurrentNet = GetCurrentDebits() + GetCurrentCredits();
			}
			else
			{
				GetCurrentNet = (GetCurrentDebits() + GetCurrentCredits()) * -1;
			}
			return GetCurrentNet;
		}

		private decimal GetYTDDebit()
		{
			decimal GetYTDDebit = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
			}
			if (blnIncEnc)
			{
				GetYTDDebit += GetEncumbrance();
			}
			if (blnIncPending)
			{
				GetYTDDebit += GetPendingDebits();
			}
			return GetYTDDebit;
		}

		private decimal GetYTDCredit()
		{
			decimal GetYTDCredit = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
			}
			if (blnIncPending)
			{
				GetYTDCredit += GetPendingCredits();
			}
			return GetYTDCredit;
		}

		private decimal GetYTDNet()
		{
			decimal GetYTDNet = 0;
			if (strExpOrRev == "E")
			{
				GetYTDNet = GetYTDDebit() + GetYTDCredit();
			}
			else
			{
				GetYTDNet = (GetYTDDebit() + GetYTDCredit()) * -1;
			}
			return GetYTDNet;
		}

		private decimal GetEncumbrance()
		{
			decimal GetEncumbrance = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
			}
			return GetEncumbrance;
		}

		private decimal GetPending()
		{
			decimal GetPending = 0;
			if (strExpOrRev == "E")
			{
				GetPending = GetPendingDebits() + GetPendingCredits();
			}
			else
			{
				GetPending = (GetPendingDebits() + GetPendingCredits()) * -1;
			}
			return GetPending;
		}

		private decimal GetBalance()
		{
			decimal GetBalance = 0;
			if (strExpOrRev == "E")
			{
				GetBalance = GetNetBudget() - (GetYTDDebit() + GetYTDCredit());
			}
			else
			{
				GetBalance = GetNetBudget() - ((GetYTDDebit() + GetYTDCredit()) * -1);
			}
			return GetBalance;
		}

		private decimal GetSpent()
		{
			decimal GetSpent = 0;
			int temp;
			decimal temp2;
			decimal temp3;
			temp2 = GetBalance();
			temp3 = GetNetBudget();
			if (temp3 == 0)
			{
				GetSpent = 0;
				return GetSpent;
			}
			if (temp2 > temp3)
			{
				GetSpent = (temp3 - temp2) / temp3;
				return GetSpent;
			}
			GetSpent = (temp3 - temp2) / temp3;
			return GetSpent;
		}

		private decimal GetPendingCredits()
		{
			decimal GetPendingCredits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (CurrentDivision == "")
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
						else
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			else
			{
				GetPendingCredits = 0;
			}
			return GetPendingCredits;
		}

		private decimal GetPendingDebits()
		{
			decimal GetPendingDebits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (CurrentDivision == "")
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
						else
						{
							if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
							}
							else
							{
								if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			else
			{
				GetPendingDebits = 0;
			}
			return GetPendingDebits;
		}

		private decimal GetOriginalBudget()
		{
			decimal GetOriginalBudget = 0;
			if (strExpOrRev == "E")
			{
				if (rsExpBudgetInfo.EndOfFile() != true && rsExpBudgetInfo.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							if (rsExpBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							if (rsExpBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
				}
				else
				{
					GetOriginalBudget = 0;
				}
			}
			else
			{
				if (rsRevBudgetInfo.EndOfFile() != true && rsRevBudgetInfo.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							if (rsRevBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							if (rsRevBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
				}
				else
				{
					GetOriginalBudget = 0;
				}
			}
			return GetOriginalBudget;
		}

		private decimal GetBudgetAdjustments()
		{
			decimal GetBudgetAdjustments = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") * -1;
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") * -1;
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
			}
			return GetBudgetAdjustments;
		}

		private decimal GetNetBudget()
		{
			decimal GetNetBudget = 0;
			GetNetBudget = GetOriginalBudget() + GetBudgetAdjustments();
			return GetNetBudget;
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		
	}
}
