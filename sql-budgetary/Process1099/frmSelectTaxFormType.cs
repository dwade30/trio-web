﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Extensions;

namespace TWBD0000.Process1099
{
    /// <summary>
    /// Summary description for frmTellerID.
    /// </summary>
    public partial class frmSelectTaxFormType : BaseForm, IModalView<ISelectTaxFormTypeViewModel>
    {
        public frmSelectTaxFormType()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
           
        }

        public frmSelectTaxFormType(ISelectTaxFormTypeViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }


        private void InitializeComponentEx()
        {
            this.cmdSave.Click += CmdSave_Click;
            this.Load += FrmGetTellerID_Load;
        }

        private void FrmGetTellerID_Load(object sender, EventArgs e)
        {
            SetFormTypeOptions();
            cboFormType.SelectedIndex = 0;
        }

        private void SetFormTypeOptions()
        {
            cboFormType.Items.Clear();
            foreach (var formType in ViewModel.FormTypeOptions())
            {
                cboFormType.Items.Add(formType);
            }
        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            SelectFormType();
        }

        private void SelectFormType()
        {
            var selectedItem = (GenericDescriptionPair<TaxFormType>)cboFormType.SelectedItem;

            ViewModel.ChosenFormType = selectedItem.ID;
            ViewModel.CancelSelected = false;
            Close();
        }

        public ISelectTaxFormTypeViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ViewModel.CancelSelected = true;
            Close();
        }
    }

}
