﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Messaging;

namespace TWBD0000.Process1099
{
    public class SelectTaxFormTypeHandler : CommandHandler<SelectTaxFormType, (bool Success, TaxFormType SelectedType)>
    {
        private IModalView<ISelectTaxFormTypeViewModel> selectTaxFormView;
        public SelectTaxFormTypeHandler(IModalView<ISelectTaxFormTypeViewModel> selectTaxFormView)
        {
            this.selectTaxFormView = selectTaxFormView;
        }
        protected override (bool Success, TaxFormType SelectedType) Handle(SelectTaxFormType command)
        {
            selectTaxFormView.ShowModal();
            if (!selectTaxFormView.ViewModel.CancelSelected)
            {
                return (Success: true, SelectedType: selectTaxFormView.ViewModel.ChosenFormType);
            }

            return (Success: false, SelectedType: selectTaxFormView.ViewModel.ChosenFormType);
        }
    }
}
