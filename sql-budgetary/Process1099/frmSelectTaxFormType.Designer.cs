﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWBD0000.Process1099
{
    /// <summary>
    /// Summary description for frmTellerID.
    /// </summary>
    partial class frmSelectTaxFormType : BaseForm
    {

        private System.ComponentModel.Container components = null;

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSave = new fecherFoundation.FCButton();
            this.btnCancel = new fecherFoundation.FCButton();
            this.cboFormType = new Wisej.Web.ComboBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnCancel);
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(10, 77);
            this.BottomPanel.Size = new System.Drawing.Size(296, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ClientArea.Controls.Add(this.cboFormType);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(316, 197);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboFormType, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(316, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 28);
            this.HeaderText.Text = "Select 1099 Form Type";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(49, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "OK";
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "acceptButton";
            this.btnCancel.Location = new System.Drawing.Point(147, 30);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnCancel.Size = new System.Drawing.Size(100, 48);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cboFormType
            // 
            this.cboFormType.AutoSize = false;
            this.cboFormType.DisplayMember = "Description";
            this.cboFormType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboFormType.Location = new System.Drawing.Point(64, 37);
            this.cboFormType.Name = "cboFormType";
            this.cboFormType.Size = new System.Drawing.Size(188, 40);
            this.cboFormType.TabIndex = 1004;
            this.cboFormType.ValueMember = "ID";
            // 
            // frmSelectTaxFormType
            // 
            this.ClientSize = new System.Drawing.Size(316, 257);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSelectTaxFormType";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "1099 Form Type";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private FCButton cmdSave;
        private FCButton btnCancel;
        private ComboBox cboFormType;
    }
}
