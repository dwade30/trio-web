﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreateCustomCheckFormat.
	/// </summary>
	partial class frmCreateCustomCheckFormat : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCheckType;
		public fecherFoundation.FCLabel lblCheckType;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblField;
		public fecherFoundation.FCComboBox cboCheckPosition;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid vsCheckFields;
		public fecherFoundation.FCCheckBox chkDoubleStub;
		public fecherFoundation.FCComboBox cmbCustomType;
		public fecherFoundation.FCCheckBox chkTwoStubs;
		public fecherFoundation.FCTextBox txtVoidAfter;
		public Wisej.Web.ImageList ImageList1;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCPictureBox imgCheck;
		public fecherFoundation.FCLabel Label2;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFileResetDefaults;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileLoadImage;
		public fecherFoundation.FCToolStripMenuItem mnuFileUnloadImage;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCLabel lblField_0;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateCustomCheckFormat));
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.cmbCheckType = new fecherFoundation.FCComboBox();
			this.lblCheckType = new fecherFoundation.FCLabel();
			this.cboCheckPosition = new fecherFoundation.FCComboBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.vsCheckFields = new fecherFoundation.FCGrid();
			this.chkDoubleStub = new fecherFoundation.FCCheckBox();
			this.cmbCustomType = new fecherFoundation.FCComboBox();
			this.chkTwoStubs = new fecherFoundation.FCCheckBox();
			this.txtVoidAfter = new fecherFoundation.FCTextBox();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.imgCheck = new fecherFoundation.FCPictureBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileResetDefaults = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileLoadImage = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileUnloadImage = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.lblField_0 = new fecherFoundation.FCLabel();
			this.btnProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCheckFields)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDoubleStub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTwoStubs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbCheckType);
			this.ClientArea.Controls.Add(this.lblCheckType);
			this.ClientArea.Controls.Add(this.cboCheckPosition);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.chkDoubleStub);
			this.ClientArea.Controls.Add(this.cmbCustomType);
			this.ClientArea.Controls.Add(this.chkTwoStubs);
			this.ClientArea.Controls.Add(this.txtVoidAfter);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.imgCheck);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblField_0);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(244, 30);
			this.HeaderText.Text = "Custom Check Setup";
			// 
			// cmbCheckType
			// 
			this.cmbCheckType.Items.AddRange(new object[] {
            "Laser",
            "Mailer"});
			this.cmbCheckType.Location = new System.Drawing.Point(198, 80);
			this.cmbCheckType.Name = "cmbCheckType";
			this.cmbCheckType.Size = new System.Drawing.Size(179, 40);
			this.cmbCheckType.SelectedIndexChanged += new System.EventHandler(this.cmbCheckType_CheckedChanged);
			// 
			// lblCheckType
			// 
			this.lblCheckType.AutoSize = true;
			this.lblCheckType.Location = new System.Drawing.Point(30, 94);
			this.lblCheckType.Name = "lblCheckType";
			this.lblCheckType.Size = new System.Drawing.Size(93, 16);
			this.lblCheckType.TabIndex = 1;
			this.lblCheckType.Text = "CHECK TYPE";
			// 
			// cboCheckPosition
			// 
			this.cboCheckPosition.BackColor = System.Drawing.SystemColors.Window;
			this.cboCheckPosition.Location = new System.Drawing.Point(198, 130);
			this.cboCheckPosition.Name = "cboCheckPosition";
			this.cboCheckPosition.Size = new System.Drawing.Size(179, 40);
			this.cboCheckPosition.TabIndex = 7;
			this.cboCheckPosition.SelectedIndexChanged += new System.EventHandler(this.cboCheckPosition_SelectedIndexChanged);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = " groupBoxNoBorder";
			this.Frame2.Controls.Add(this.vsCheckFields);
			this.Frame2.Location = new System.Drawing.Point(448, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(600, 227);
			this.Frame2.TabIndex = 14;
			this.Frame2.Text = "Setup Custom Check Fields";
			// 
			// vsCheckFields
			// 
			this.vsCheckFields.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsCheckFields.Cols = 7;
			this.vsCheckFields.FixedCols = 2;
			this.vsCheckFields.FrozenCols = 1;
			this.vsCheckFields.Location = new System.Drawing.Point(0, 30);
			this.vsCheckFields.Name = "vsCheckFields";
			this.vsCheckFields.Rows = 1;
			this.vsCheckFields.Size = new System.Drawing.Size(600, 197);
			this.vsCheckFields.StandardTab = false;
			this.vsCheckFields.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsCheckFields.TabIndex = 10;
			this.vsCheckFields.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsCheckFields_KeyDownEdit);
			this.vsCheckFields.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsCheckFields_CellChanged);
			this.vsCheckFields.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsCheckFields_BeforeEdit);
			this.vsCheckFields.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsCheckFields_ValidateEdit);
			this.vsCheckFields.CurrentCellChanged += new System.EventHandler(this.vsCheckFields_RowColChange);
			this.vsCheckFields.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.vsCheckFields_CellClick);
			this.vsCheckFields.Click += new System.EventHandler(this.vsCheckFields_ClickEvent);
			this.vsCheckFields.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsCheckFields_KeyPressEvent);
			// 
			// chkDoubleStub
			// 
			this.chkDoubleStub.Location = new System.Drawing.Point(30, 180);
			this.chkDoubleStub.Name = "chkDoubleStub";
			this.chkDoubleStub.Size = new System.Drawing.Size(164, 23);
			this.chkDoubleStub.TabIndex = 8;
			this.chkDoubleStub.Text = "Use double-height stub";
			this.chkDoubleStub.CheckedChanged += new System.EventHandler(this.chkDoubleStub_CheckedChanged);
			// 
			// cmbCustomType
			// 
			this.cmbCustomType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCustomType.Location = new System.Drawing.Point(198, 30);
			this.cmbCustomType.Name = "cmbCustomType";
			this.cmbCustomType.Size = new System.Drawing.Size(179, 40);
			this.cmbCustomType.TabIndex = 3;
			this.cmbCustomType.SelectedIndexChanged += new System.EventHandler(this.cmbCustomType_SelectedIndexChanged);
			// 
			// chkTwoStubs
			// 
			this.chkTwoStubs.Location = new System.Drawing.Point(30, 180);
			this.chkTwoStubs.Name = "chkTwoStubs";
			this.chkTwoStubs.Size = new System.Drawing.Size(110, 23);
			this.chkTwoStubs.TabIndex = 2;
			this.chkTwoStubs.Text = "Use two stubs";
			this.chkTwoStubs.CheckedChanged += new System.EventHandler(this.chkTwoStubs_CheckedChanged);
			// 
			// txtVoidAfter
			// 
			this.txtVoidAfter.BackColor = System.Drawing.SystemColors.Window;
			this.txtVoidAfter.Location = new System.Drawing.Point(198, 217);
			this.txtVoidAfter.Name = "txtVoidAfter";
			this.txtVoidAfter.Size = new System.Drawing.Size(179, 40);
			this.txtVoidAfter.TabIndex = 9;
			this.txtVoidAfter.Text = "Void after 180 days";
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry2});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 256);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
			// 
			// dlg1
			// 
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 144);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(176, 16);
			this.Label1.TabIndex = 15;
			this.Label1.Text = "CHECK POSITION";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(50, 16);
			this.Label3.TabIndex = 13;
			this.Label3.Text = "FORMAT";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 231);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(128, 16);
			this.Label4.TabIndex = 12;
			this.Label4.Text = "VOID AFTER MESSAGE";
			// 
			// imgCheck
			// 
			this.imgCheck.Image = ((System.Drawing.Image)(resources.GetObject("imgCheck.Image")));
			this.imgCheck.Location = new System.Drawing.Point(30, 304);
			this.imgCheck.Name = "imgCheck";
			this.imgCheck.Size = new System.Drawing.Size(1018, 336);
			this.imgCheck.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 276);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(1018, 14);
			this.Label2.Text = "PREVIEW";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileResetDefaults,
            this.mnuPrint,
            this.mnuFileLoadImage,
            this.mnuFileUnloadImage,
            this.mnuNew,
            this.mnuDelete});
			this.MainMenu1.Name = null;
			// 
			// mnuFileResetDefaults
			// 
			this.mnuFileResetDefaults.Index = 0;
			this.mnuFileResetDefaults.Name = "mnuFileResetDefaults";
			this.mnuFileResetDefaults.Text = "Set Fields to Default Position";
			this.mnuFileResetDefaults.Click += new System.EventHandler(this.mnuFileResetDefaults_Click);
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 1;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print Sample Check";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuFileLoadImage
			// 
			this.mnuFileLoadImage.Index = 2;
			this.mnuFileLoadImage.Name = "mnuFileLoadImage";
			this.mnuFileLoadImage.Text = "Load Scanned Check Image";
			this.mnuFileLoadImage.Click += new System.EventHandler(this.mnuFileLoadImage_Click);
			// 
			// mnuFileUnloadImage
			// 
			this.mnuFileUnloadImage.Index = 3;
			this.mnuFileUnloadImage.Name = "mnuFileUnloadImage";
			this.mnuFileUnloadImage.Text = "Unload Scanned Check Image";
			this.mnuFileUnloadImage.Click += new System.EventHandler(this.mnuFileUnloadImage_Click);
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 4;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 5;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Format";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// lblField_0
			// 
			this.lblField_0.BackColor = System.Drawing.Color.Transparent;
			this.lblField_0.Location = new System.Drawing.Point(408, 6);
			this.lblField_0.Name = "lblField_0";
			this.lblField_0.Size = new System.Drawing.Size(86, 13);
			this.lblField_0.TabIndex = 1;
			this.lblField_0.Text = "LABEL3";
			this.lblField_0.Visible = false;
			// 
			// btnProcessSave
			// 
			this.btnProcessSave.AppearanceKey = "acceptButton";
			this.btnProcessSave.Location = new System.Drawing.Point(489, 30);
			this.btnProcessSave.Name = "btnProcessSave";
			this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessSave.Size = new System.Drawing.Size(80, 48);
			this.btnProcessSave.TabIndex = 1;
			this.btnProcessSave.Text = "Save";
			this.btnProcessSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmCreateCustomCheckFormat
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCreateCustomCheckFormat";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Custom Check Setup";
			this.Load += new System.EventHandler(this.frmCreateCustomCheckFormat_Load);
			this.Activated += new System.EventHandler(this.frmCreateCustomCheckFormat_Activated);
			this.Resize += new System.EventHandler(this.frmCreateCustomCheckFormat_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreateCustomCheckFormat_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsCheckFields)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDoubleStub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTwoStubs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcessSave;
	}
}
