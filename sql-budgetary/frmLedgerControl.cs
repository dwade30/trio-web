﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web.Ext.CustomProperties;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerControl.
	/// </summary>
	public partial class frmLedgerControl : BaseForm
	{
		public frmLedgerControl()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLedgerControl InstancePtr
		{
			get
			{
				return (frmLedgerControl)Sys.GetInstance(typeof(frmLedgerControl));
			}
		}

		protected frmLedgerControl _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// Key for Codes for Control Accounts
		// Accounts                                       Town Codes         School Codes
		// -------------------------------------------------------------------------------
		// Miscellaneous                                      CM                 SCM
		// Accounts Payable                                   CA                 SCA
		// Payroll                                            CP                 SCP
		// Expense Control                                    EC                 SEC
		// Revenue Control                                    RC                 SRC
		// Encumbrance Control                                EO                 SEO
		// Unliquidated Encumbrances                          UE                 SUE
		// Fund Balance                                       FB                 SFB
		// Credit Memo Receivable                             CR                 SCR
		// EOY Accounts Payable                               AP                 SAP
		// Real Estate
		// Commitment                                     RC
		// Personal Property
		// Commitment                                     PC
		// Real Estate
		// Supplemental                                   RS
		// Personal Property
		// Supplemental                                   PS
		// Real Estate
		// Receivable                                     RR
		// Personal Property
		// Receivable                                     PR
		// Tax Aquired Receivable                             TR
		// Tax Acquired Lien Receivable                       LV
		// Lien Receivable                                    LR
		// UT Water Receivable                                WR
		// UT Sewer Receivable                                SR
		// UT Water Lien Receivable                           WL
		// UT Sewer Lien Receivable                           SL
		// UT Water Tax Acquired Receivable                   WT
		// UT Sewer Tax Acquired Receivable                   ST
		// UT Water Lien Tax Acquired Receivable              WA
		// UT Sewer Lien Tax Acquired Receivable              SA
		// Due To/From
		// Cash Fund                                      CF                 SCF
		// Due To                                         DT                 SDT
		// Due From                                       DF                 SDF
		int NameCol;
		// column in the flexgrid that holds the account names
		string length = "";
		// variable that holds the account length information
		int AccountCol;
		// column in the flexgrid that holds the account information
		// vbPorter upgrade warning: FundLength As short --> As int	OnWrite(string)
		int FundLength;
		// length of the fund numbers
		// vbPorter upgrade warning: AcctLength As short --> As int	OnWrite(string)
		int AcctLength;
		// length of the account numbers
		// Dim SchoolAcctLength As Integer   'length of the school account numbers
		// Dim SchoolFundLength As Integer
		bool VerifyFlag;
		clsDRWrapper rs = new clsDRWrapper();
		int CurrentRow;
		int CurrentSchoolRow;
		bool blnUnload;

		private void frmLedgerControl_Activated(object sender, System.EventArgs e)
		{
			int counter;
			// loop counter variable
			if (modGlobal.FormExist(this))
			{
				return;
			}
			NameCol = 1;
			// initialize columns
			AccountCol = 2;
			FundLength = FCConvert.ToInt32(Strings.Mid(length, 1, 2));
			// initialize the account lengths
			AcctLength = FCConvert.ToInt32(Strings.Mid(length, 3, 2));
			// SchoolAcctLength = Mid(SchoolLedger, 3, 2)
			// SchoolFundLength = Mid(SchoolLedger, 1, 2)
			vs1.Visible = true;
			// make the flexgrid visible
			vs1.ColWidth(0, 200);
			vs1.ColWidth(NameCol, FCConvert.ToInt32(0.6 * vs1.WidthOriginal));
			// set the column widths
			vs1.ColWidth(AccountCol, 100);
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			// disable highlighting
			vs1.MergeRow(0, true);
			vs1.MergeRow(1, true);
			vs1.MergeRow(5, true);
			vs1.MergeRow(14, true);
			// set rows to be able to merge
			vs1.MergeRow(32, true);
			vs1.TextMatrix(0, NameCol, " ");
			vs1.TextMatrix(0, AccountCol, " ");
			// put headings in the flexgrid and set the outline levels
			vs1.TextMatrix(1, NameCol, "Cash Accounts");
			//vs1.TextMatrix(1, AccountCol, "Cash Accounts");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, NameCol, 1, AccountCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, NameCol, 1, AccountCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, NameCol, 1, AccountCol, 5);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 1, NameCol, 1, NameCol, true);
			vs1.RowOutlineLevel(1, 1);
			vs1.IsSubtotal(1, true);
			for (counter = 2; counter <= 4; counter++)
			{
				vs1.RowOutlineLevel(counter, 2);
			}
			vs1.TextMatrix(2, NameCol, "Miscellaneous Cash");
			vs1.TextMatrix(3, NameCol, "Accounts Payable");
			vs1.TextMatrix(4, NameCol, "Payroll");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 2, NameCol, 4, NameCol, true);
			vs1.TextMatrix(5, NameCol, "Control Accounts");
			//vs1.TextMatrix(5, AccountCol, "Control Accounts");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 5, NameCol, 5, AccountCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, NameCol, 5, AccountCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 5, NameCol, 5, AccountCol, 5);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 5, NameCol, 5, NameCol, true);
			vs1.RowOutlineLevel(5, 1);
			vs1.IsSubtotal(5, true);
			for (counter = 6; counter <= 13; counter++)
			{
				vs1.RowOutlineLevel(counter, 2);
			}
			vs1.TextMatrix(6, NameCol, "Expense Control");
			vs1.TextMatrix(7, NameCol, "Revenue Control");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 6, NameCol, 7, NameCol, true);
			vs1.TextMatrix(8, NameCol, "Encumbrance Control");
			vs1.TextMatrix(9, NameCol, "EOY Unliquidated Encumbrances");
			vs1.TextMatrix(10, NameCol, "Fund Balance");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 10, NameCol, 10, NameCol, true);
			vs1.TextMatrix(11, NameCol, "Credit Memo Receivable");
			vs1.TextMatrix(12, NameCol, "EOY Accounts Payable");
			vs1.TextMatrix(13, NameCol, "Past Year Enc Control");
			vs1.TextMatrix(14, NameCol, "Commitment Accounts");
			//vs1.TextMatrix(14, AccountCol, "Commitment Accounts");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 14, NameCol, 14, AccountCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 14, NameCol, 14, AccountCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 14, NameCol, 14, AccountCol, 5);
			vs1.RowOutlineLevel(14, 1);
			vs1.IsSubtotal(14, true);
			vs1.RowOutlineLevel(15, 2);
			vs1.TextMatrix(15, NameCol, "R / E Commitment");
			vs1.RowOutlineLevel(16, 2);
			vs1.TextMatrix(16, NameCol, "P / P Commitment");
			vs1.RowOutlineLevel(17, 2);
			vs1.TextMatrix(17, NameCol, "R / E Supplemental");
			vs1.RowOutlineLevel(18, 2);
			vs1.TextMatrix(18, NameCol, "P / P Supplemental");
			vs1.RowOutlineLevel(19, 2);
			vs1.TextMatrix(19, NameCol, "R / E Receivable");
			vs1.RowOutlineLevel(20, 2);
			vs1.TextMatrix(20, NameCol, "P / P Receivable");
			vs1.RowOutlineLevel(21, 2);
			vs1.TextMatrix(21, NameCol, "Lien Receivable");
			vs1.RowOutlineLevel(22, 2);
			vs1.TextMatrix(22, NameCol, "Tax Aquired Receivable");
			vs1.RowOutlineLevel(23, 2);
			vs1.TextMatrix(23, NameCol, "Lien Tax Aquired Receivable");
			vs1.RowOutlineLevel(24, 2);
			vs1.TextMatrix(24, NameCol, "UT Water Receivable");
			vs1.RowOutlineLevel(25, 2);
			vs1.TextMatrix(25, NameCol, "UT Sewer Receivable");
			vs1.RowOutlineLevel(26, 2);
			vs1.TextMatrix(26, NameCol, "UT Water Lien Receivable");
			vs1.RowOutlineLevel(27, 2);
			vs1.TextMatrix(27, NameCol, "UT Sewer Lien Receivable");
			vs1.RowOutlineLevel(28, 2);
			vs1.TextMatrix(28, NameCol, "UT Water Tax Acquired Receivable");
			vs1.RowOutlineLevel(29, 2);
			vs1.TextMatrix(29, NameCol, "UT Water Lien Tax Acquired Receivable");
			vs1.RowOutlineLevel(30, 2);
			vs1.TextMatrix(30, NameCol, "UT Sewer Tax Acquired Receivable");
			vs1.RowOutlineLevel(31, 2);
			vs1.TextMatrix(31, NameCol, "UT Sewer Lien Tax Acquired Receivable");
			vs1.TextMatrix(32, NameCol, "Due To / From");
			//vs1.TextMatrix(32, AccountCol, "Due To / From");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 32, NameCol, 32, AccountCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 32, NameCol, 32, AccountCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 32, NameCol, 32, AccountCol, 5);
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y")
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 32, NameCol, 35, NameCol, true);
			}
			vs1.RowOutlineLevel(32, 1);
			vs1.IsSubtotal(32, true);
			for (counter = 33; counter <= 35; counter++)
			{
				vs1.RowOutlineLevel(counter, 2);
			}
			vs1.TextMatrix(33, NameCol, "Cash Fund");
			vs1.TextMatrix(34, NameCol, "Due To");
			vs1.TextMatrix(35, NameCol, "Due From");
			// collapse all the rows
			for (counter = 1; counter <= 35; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 2)
				{
					vs1.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			for (counter = 1; counter <= 35; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					vs1.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			// put headings in the flexgrid and set the outline levels
			tabStandardAccounts.SelectedIndex = 0;
			tabStandardAccounts.TabPages[0].Enabled = true;
			//FC:FINAL:AM:#i641 - can't disable a tabpage; hide it instead
			//tabStandardAccounts.TabPages[1].Enabled = false;
			tabStandardAccounts.TabPages[1].Visible = false;
			// collapse all the rows
			LoadData();
			vs1.Enabled = true;
            modColorScheme.ColorGrid(vs1);
			// enable the flexgrid
			//FC:FINAL:MSH - Issue #745: assign a handler for the table EditControl
			this.vs1.EditingControlShowing -= vs1_EditingControlShowing;
			this.vs1.EditingControlShowing += vs1_EditingControlShowing;
			frmLedgerControl.InstancePtr.Refresh();
			// refresh the form
		}
		//FC:FINAL:MSH - Issue #745: add handler if edit control of vs1 table is activated
		private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - Issue #745: in VB vs1_KeyDownEdit event is executed when key pressed in edit area
				e.Control.KeyDown -= vs1_KeyDownEdit;
				//e.Control.KeyPress -= vs1_KeyPressEdit;
				e.Control.KeyUp -= vs1_KeyUpEdit;
				e.Control.KeyDown += vs1_KeyDownEdit;
				//e.Control.KeyPress += vs1_KeyPressEdit;
				e.Control.KeyUp += vs1_KeyUpEdit;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (AccountCol == -1 || AccountCol == col)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					if (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18)
					{
						customClientData["GRID7Light_Col"] = col;
					}
					else
					{
						customClientData["GRID7Light_Col"] = 9999;
					}
					customClientData["intAcctCol"] = AccountCol;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void frmLedgerControl_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Tab)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{DOWN}", false);
			}
			else if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
				return;
			}
			if (this.ActiveControl.GetName() == "vs1")
			{
				if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmLedgerControl_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLedgerControl.FillStyle	= 0;
			//frmLedgerControl.ScaleWidth	= 9045;
			//frmLedgerControl.ScaleHeight	= 7335;
			//frmLedgerControl.LinkTopic	= "Form2";
			//frmLedgerControl.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			length = modAccountTitle.Statics.Ledger;
			// get the account lengths from the work record
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:AM: add the expand button
			vs1.AddExpandButton();
		}

		private void frmLedgerControl_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(NameCol, FCConvert.ToInt32(0.6 * vs1.WidthOriginal));
			// set the column width
			vs1_Collapsed();
			// recalculate the size of the flexgrid to be proportional to the form
		}

		private void frmLedgerControl_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Return)
			{
				if (frmLedgerControl.InstancePtr.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
			// go to the menu
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
			{
				vs1.EditMaxLength = 23;
			}
		}

		private void vs1_ClickEvent(object sender, DataGridViewCellEventArgs e)
		{
			//FC:FINAL:AM:#i641 - let the default action when clicking on the collapse/expand button
			if (e.Role == "open" || e.Role == "close")
			{
				return;
			}
			switch (vs1.Row)
			{
			// if we click on a heading the expand or collapse it
				case 1:
				case 5:
				case 14:
				case 32:
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
						}
						else
						{
							vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
						}
						vs1.Select(1, AccountCol);
						vs1.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case 2:
				case 3:
				case 4:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 30:
				case 31:
				case 34:
				case 35:
					{
						// if we click on an account row then give it focus
						vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vs1.Col = AccountCol;
						vs1.EditCell();
						vs1.EditSelStart = 0;
						if (vs1.TextMatrix(vs1.Row, vs1.Col) == modValidateAccount.GetFormat("0", ref AcctLength))
						{
							vs1.EditText = "";
						}
						vs1.EditMaxLength = AcctLength;
						break;
					}
				case 33:
					{
						// if we click on a fund row then give it focus
						vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vs1.Col = AccountCol;
						vs1.EditCell();
						vs1.EditSelStart = 0;
						if (vs1.TextMatrix(vs1.Row, vs1.Col) == modValidateAccount.GetFormat("0", ref FundLength))
						{
							vs1.EditText = "";
						}
						vs1.EditMaxLength = FundLength;
						break;
					}
				default:
					{
						// case actual account row
						vs1.Col = AccountCol;
						break;
					}
			}
			//end switch
		}

		private void vs1_Collapsed()
		{
			int counter;
			int rows = 0;
			int counter2 = 0;
			int height;
			// set the height of the flexgrid to perfectly match the rows that are shown
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						if (counter == vs1.Rows - 1)
						{
							break;
						}
						else
						{
							counter2 = counter + 1;
							while (vs1.RowOutlineLevel(counter2) != 0)
							{
								counter2 += 1;
								if (counter2 == vs1.Rows - 1)
								{
									break;
								}
							}
							counter = counter2 - 1;
						}
					}
					else
					{
						rows += 1;
					}
				}
				else if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						if (counter == vs1.Rows - 1)
						{
							break;
						}
						else
						{
							counter2 = counter + 1;
							while (vs1.RowOutlineLevel(counter2) > 1)
							{
								counter2 += 1;
								if (counter2 == vs1.Rows - 1)
								{
									break;
								}
							}
							counter = counter2 - 1;
						}
					}
					else
					{
						rows += 1;
					}
				}
				else
				{
					rows += 1;
				}
			}
			//FC:FINAL:BB - use Anchoring
			//height = rows * vs1.RowHeight(0) + 75;
			//if (height > 0.77 * frmLedgerControl.InstancePtr.HeightOriginal)
			//{
			//    vs1.Height = FCConvert.ToInt32(0.77 * frmLedgerControl.InstancePtr.HeightOriginal);
			//}
			//else
			//{
			//    vs1.Height = height;
			//}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			int temp = 0;
			if (CurrentRow == vs1.Row)
			{
				temp = GetNextRow();
				if (temp != 0)
				{
					vs1.Row = temp;
				}
			}
			else
			{
				vs1_RowColChange(sender, e);
			}
			if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
			{
				modNewAccountBox.SetGridFormat(vs1, vs1.Row, vs1.Col, false, "", "R");
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int OldRow = 0;
			int temp = 0;
			if (KeyCode == Keys.Return)
			{
				// if they hit the enter key on a heading then open up the heading and give focus to the first thing inside it
				KeyCode = 0;
				if (vs1.RowOutlineLevel(vs1.Row) < 2)
				{
                    //FC:FINAL:AM:#3149 - expand row
                    if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                    {
                        vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
                    }
                    OldRow = vs1.Row;
					vs1.Row = OldRow + 1;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				if (vs1.Row == vs1.Rows - 1)
				{
					// do nothing
				}
				else
				{
					if (vs1.RowOutlineLevel(vs1.Row + 1) < 2)
					{
						// if we are moving to a heading
						temp = GetNextRow();
						// look for next available row to go to
						if (temp == 0)
						{
							// if none are available then do nothing
							// do nothing
						}
						else
						{
							vs1.Row = temp;
							// else go to that row
						}
					}
					else
					{
						vs1.Row += 1;
						// else move to the next row
					}
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			int counter;
			int temp = 0;
			string temp2 = "";
			Keys KeyCode = e.KeyCode;
			//if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
			//{
			//	string temp1 = vs1.EditText;
			//	modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, KeyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp1, vs1.EditSelLength);
			//	vs1.EditText = temp1;
			//}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				// if enter is hit then make it act like the down arrow
				Support.SendKeys("{DOWN}", false);
			}
			else if (KeyCode == Keys.Left)
			{
				// if the left key is hit and we are at the beginning of the cell then do nothing
				if (vs1.EditSelStart == 0)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				// if down is hit and we are at the bottom do nothing
				KeyCode = 0;
				if (vs1.Row == vs1.Rows - 1)
				{
					Support.SendKeys("{LEFT}", false);
				}
				else
				{
					if (vs1.RowOutlineLevel(vs1.Row + 1) < 2)
					{
						// if we are moving to a heading
						temp = GetNextRow();
						// look for next available row to go to
						if (temp == 0)
						{
							// if none are available then do nothing
							Support.SendKeys("{LEFT}", false);
						}
						else
						{
							vs1.Row = temp;
							// else go to that row
							Support.SendKeys("{LEFT}", false);
						}
					}
					else
					{
						vs1.Row += 1;
						// else move to the next row
						Support.SendKeys("{LEFT}", false);
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 0)
				{
					// if we are at the first row then do nothing
					KeyCode = 0;
				}
				else
				{
					if (vs1.RowOutlineLevel(vs1.Row - 1) < 2)
					{
						// if we are moving to a heading
						temp = GetPreviousRow();
						// find the next available row
						if (temp == 0)
						{
							// if there is none then do nothing
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							// else move to the row
							vs1.Row = temp;
						}
					}
					else
					{
						KeyCode = 0;
						// else move up one row
						vs1.Row -= 1;
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				// if the right key is pushed and we are at the end of the cell then do nothing
				if (vs1.TextMatrix(vs1.Row, vs1.Col).Length == vs1.EditSelStart)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
			{
				modNewAccountBox.CheckAccountKeyPress(vs1, vs1.Row, vs1.Col, FCConvert.ToInt16(keyAscii), vs1.EditSelStart, vs1.EditText);
			}
		}

		private void vs1_KeyUpEdit(object sender, KeyEventArgs e)
		{
            Keys KeyCode = e.KeyCode;
            if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
            {
                string temp1 = vs1.EditText;
                modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, KeyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp1, vs1.EditSelLength);
                vs1.EditText = temp1;
            }
   //         Keys KeyCode = e.KeyCode;
			//if (vs1.Col == AccountCol && (vs1.Row == 15 || vs1.Row == 16 || vs1.Row == 17 || vs1.Row == 18))
			//{
			//	if (FCConvert.ToInt32(KeyCode) != 40 && FCConvert.ToInt32(KeyCode) != 38)
			//	{
			//		// up and down arrows
			//		modNewAccountBox.CheckAccountKeyCode(vs1, vs1.Row, vs1.Col, FCConvert.ToInt32(KeyCode), 0, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
			//	}
			//}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.Col == AccountCol)
			{
				switch (vs1.Row)
				{
					case 2:
					case 3:
					case 4:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 19:
					case 20:
					case 21:
					case 22:
					case 23:
					case 24:
					case 25:
					case 26:
					case 27:
					case 28:
					case 29:
					case 30:
					case 31:
					case 34:
					case 35:
						{
							vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vs1.EditMaxLength = AcctLength;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							if (vs1.TextMatrix(vs1.Row, vs1.Col) == modValidateAccount.GetFormat("0", ref AcctLength))
							{
								vs1.EditText = "";
							}
							break;
						}
					case 33:
						{
							vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vs1.EditMaxLength = FundLength;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							if (vs1.TextMatrix(vs1.Row, vs1.Col) == modValidateAccount.GetFormat("0", ref FundLength))
							{
								vs1.EditText = "";
							}
							break;
						}
					case 15:
					case 16:
					case 17:
					case 18:
						{
							// actual account rows
							modNewAccountBox.SetGridFormat(vs1, vs1.Row, vs1.Col, true, "", "R");
							vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
							break;
						}
				}
				//end switch
			}
			else
			{
				vs1.Col = AccountCol;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetNextRow()
		{
			short GetNextRow = 0;
			int counter;
			bool HeadingFlag = false;
			bool SubFlag = false;
			counter = vs1.Row + 1;
			while (counter < vs1.Rows)
			{
				if (vs1.RowOutlineLevel(counter) == 2)
				{
					if (HeadingFlag || SubFlag)
					{
						// do nothing
					}
					else
					{
						break;
					}
				}
				else if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (HeadingFlag)
					{
						// do nothing
					}
					else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						SubFlag = true;
					}
					else
					{
						SubFlag = false;
					}
				}
				else
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						HeadingFlag = true;
					}
					else
					{
						HeadingFlag = false;
						SubFlag = false;
					}
				}
				counter += 1;
				if (counter == vs1.Rows)
				{
					GetNextRow = 0;
					return GetNextRow;
				}
			}
			if (counter == vs1.Rows)
			{
				GetNextRow = 0;
			}
			else
			{
				GetNextRow = FCConvert.ToInt16(counter);
			}
			return GetNextRow;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetPreviousRow()
		{
			short GetPreviousRow = 0;
			int counter;
			int TempRow;
			TempRow = 0;
			counter = vs1.Row - 1;
			while (counter >= 1)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (TempRow == 0)
					{
						TempRow = counter;
					}
					else
					{
						// do nothing
					}
				}
				else
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						TempRow = 0;
					}
					else
					{
						if (TempRow != 0)
						{
							break;
						}
					}
				}
				counter -= 1;
				if (counter == 0)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						GetPreviousRow = 0;
					}
					else
					{
						GetPreviousRow = FCConvert.ToInt16(TempRow);
					}
					return GetPreviousRow;
				}
			}
			GetPreviousRow = FCConvert.ToInt16(TempRow);
			return GetPreviousRow;
		}
		// validates funds and accounts
		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			string temp2 = "";
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			switch (row)
			{
				case 2:
				case 3:
				case 4:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 30:
				case 31:
				case 34:
				case 35:
					{
						if (!Information.IsNumeric(vs1.EditText) && vs1.EditText.Length != 0)
						{
							MessageBox.Show("You may only enter a number in this field", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
						}
						else
						{
							temp2 = vs1.EditText;
							vs1.EditText = modValidateAccount.GetFormat(temp2, ref AcctLength);
						}
						break;
					}
				case 33:
					{
						if (!Information.IsNumeric(vs1.EditText) && vs1.EditText.Length != 0)
						{
							MessageBox.Show("You may only enter a number in this field", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
						}
						else
						{
							temp2 = vs1.EditText;
							vs1.EditText = modValidateAccount.GetFormat(temp2, ref FundLength);
						}
						break;
					}
				case 15:
				case 16:
				case 17:
				case 18:
					{
						e.Cancel = modNewAccountBox.CheckAccountValidate(vs1, row, col, e.Cancel);
						break;
					}
			}
			//end switch
		}

		private void LoadData()
		{
			int counter;
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'T' ORDER BY Row");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (counter == FCConvert.ToInt32(rs.Get_Fields_Int32("Row")))
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter, AccountCol, FCConvert.ToString(rs.Get_Fields("account")));
						rs.MoveNext();
					}
				}
			}
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'S' ORDER BY Row");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
			}
		}

		private void SaveInfo()
		{
			int counter;
			clsDRWrapper rsPYInfo = new clsDRWrapper();
			clsDRWrapper rsCRInfo = new clsDRWrapper();
			bool blnPYUpdate;
			bool blnCRUpdate;
			string strCRTypes;
			blnPYUpdate = false;
			blnCRUpdate = false;
			strCRTypes = "";
			vs1.Select(0, 1);
			// Town Accounts
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 0)
				{
					// do nothing
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'T' AND Row = " + FCConvert.ToString(counter));
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.Edit();
						rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
						switch (counter)
						{
							case 2:
								{
									rs.Set_Fields("ShortDescription", "Misc");
									break;
								}
							case 4:
								{
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolPY && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsPYInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'", "TWPY0000.vb1");
											if (rsPYInfo.EndOfFile() != true && rsPYInfo.BeginningOfFile() != true)
											{
												rsPYInfo.Edit();
												if (!modAccountTitle.Statics.YearFlag)
												{
													// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
													if (FCConvert.ToString(rsPYInfo.Get_Fields("Account")) != "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol) + "-00")
													{
														rsPYInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol) + "-00");
														blnPYUpdate = true;
													}
												}
												else
												{
													// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
													if (FCConvert.ToString(rsPYInfo.Get_Fields("Account")) != "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol))
													{
														rsPYInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol));
														blnPYUpdate = true;
													}
												}
												rsPYInfo.Update();
											}
										}
									}
									break;
								}
							case 8:
								{
									rs.Set_Fields("ShortDescription", "Encum Offset");
									break;
								}
							case 9:
								{
									rs.Set_Fields("ShortDescription", "Unliq Encum");
									break;
								}
							case 19:
								{
									// RE Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 90", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "90, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "90, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 20:
								{
									// PP Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 92", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "92, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "92, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 21:
								{
									// Lien Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 91", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "91, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "91, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 22:
								{
									// Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Tax Aqr Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 890", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "890, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "890, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 23:
								{
									// Lien Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Ln Tx Aq Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 891", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "891, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "891, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 24:
								{
									// UT Water Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 93", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "93, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "93, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 25:
								{
									// UT Sewer Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 94", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "94, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "94, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 26:
								{
									// UT Water Lien Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 96", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "96, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "96, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 27:
								{
									// UT Sewer Lien Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 95", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "95, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "95, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 28:
								{
									// UT Water Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Wt Tx Aq Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 893", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "893, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "893, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 29:
								{
									// UT Water Lien Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "W Ln Tx Aq R");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 896", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "896, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "896, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 30:
								{
									// UT Sewer Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Sw Tx Aq Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 894", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "894, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "894, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 31:
								{
									// UT Sewer Lien Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "S Ln Tx Aq R");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 895", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "895, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "895, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							default:
								{
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									break;
								}
						}
						//end switch
						switch (counter)
						{
							case 22:
								{
									rs.Set_Fields("LongDescription", "Tax Aquired Receivable");
									break;
								}
							case 23:
								{
									rs.Set_Fields("LongDescription", "Lien Tax Aquired Receiv");
									break;
								}
							case 28:
								{
									rs.Set_Fields("LongDescription", "Water Tax Aquired Receiv");
									break;
								}
							case 29:
								{
									rs.Set_Fields("LongDescription", "Wat Lien Tax Aquired Rec");
									break;
								}
							case 30:
								{
									rs.Set_Fields("LongDescription", "Sewer Tax Aquired Receiv");
									break;
								}
							case 31:
								{
									rs.Set_Fields("LongDescription", "Sew Lien Tax Aquired Rec");
									break;
								}
							default:
								{
									rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, NameCol));
									break;
								}
						}
						//end switch
						rs.Update();
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM StandardAccounts");
						rs.AddNew();
						rs.Set_Fields("Row", counter);
						rs.Set_Fields("Type", "T");
						rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
						switch (counter)
						{
							case 2:
								{
									rs.Set_Fields("ShortDescription", "Misc");
									break;
								}
							case 4:
								{
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolPY && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsPYInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'", "TWPY0000.vb1");
											if (rsPYInfo.EndOfFile() != true && rsPYInfo.BeginningOfFile() != true)
											{
												rsPYInfo.Edit();
												if (!modAccountTitle.Statics.YearFlag)
												{
													// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
													if (FCConvert.ToString(rsPYInfo.Get_Fields("Account")) != "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol) + "-00")
													{
														rsPYInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol) + "-00");
														blnPYUpdate = true;
													}
												}
												else
												{
													// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
													if (FCConvert.ToString(rsPYInfo.Get_Fields("Account")) != "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol))
													{
														rsPYInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6("1", FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + vs1.TextMatrix(counter, AccountCol));
														blnPYUpdate = true;
													}
												}
												rsPYInfo.Update();
											}
										}
									}
									break;
								}
							case 8:
								{
									rs.Set_Fields("ShortDescription", "Encum Offset");
									break;
								}
							case 9:
								{
									rs.Set_Fields("ShortDescription", "Unliq Encum");
									break;
								}
							case 19:
								{
									// RE Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 90", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "90, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "90, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 20:
								{
									// PP Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 92", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "92, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "92, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 21:
								{
									// Lien Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 91", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "91, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "91, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 22:
								{
									// Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Tax Aqr Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 890", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "890, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "890, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 23:
								{
									// Lien Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Ln Tx Aq Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 891", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "891, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "891, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 24:
								{
									// UT Water Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 93", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "93, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "93, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 25:
								{
									// UT Sewer Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 94", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "94, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "94, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 26:
								{
									// UT Water Lien Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 96", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "96, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "96, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 27:
								{
									// UT Sewer Lien Receivable
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 95", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "95, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "95, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 28:
								{
									// UT Water Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Wt Tx Aq Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 893", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "893, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "893, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 29:
								{
									// UT Water Lien Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "W Ln Tx Aq R");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 896", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "896, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "896, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 30:
								{
									// UT Sewer Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "Sw Tx Aq Rec");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 894", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "894, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "894, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							case 31:
								{
									// UT Sewer Lien Tax Acquired Receivable
									rs.Set_Fields("ShortDescription", "S Ln Tx Aq R");
									if (Conversion.Val(vs1.TextMatrix(counter, AccountCol)) != 0)
									{
										if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
										{
											rsCRInfo.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 895", "TWCR0000.vb1");
											if (rsCRInfo.EndOfFile() != true && rsCRInfo.BeginningOfFile() != true)
											{
												do
												{
													if (Strings.Trim(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1"))) != "")
													{
														if (Strings.Left(FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")), 1) != "M")
														{
															rsCRInfo.Edit();
															if (!modAccountTitle.Statics.YearFlag)
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol) + "-" + modBudgetaryAccounting.GetSuffix(rsCRInfo.Get_Fields_String("Account1")));
																	blnCRUpdate = true;
																	strCRTypes += "895, ";
																}
															}
															else
															{
																if (FCConvert.ToString(rsCRInfo.Get_Fields_String("Account1")) != "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol))
																{
																	rsCRInfo.Set_Fields("Account1", "G " + modBudgetaryAccounting.GetFund_2(rsCRInfo.Get_Fields_String("Account1")) + "-" + vs1.TextMatrix(counter, AccountCol));
																	blnCRUpdate = true;
																	strCRTypes += "895, ";
																}
															}
															rsCRInfo.Update();
														}
													}
													rsCRInfo.MoveNext();
												}
												while (rsCRInfo.EndOfFile() != true);
											}
										}
									}
									break;
								}
							default:
								{
									if (vs1.TextMatrix(counter, NameCol).Length <= 12)
									{
										rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, NameCol));
									}
									else
									{
										rs.Set_Fields("ShortDescription", Strings.Left(vs1.TextMatrix(counter, NameCol), 12));
									}
									break;
								}
						}
						//end switch
						switch (counter)
						{
							case 22:
								{
									rs.Set_Fields("LongDescription", "Tax Aquired Receivable");
									break;
								}
							case 23:
								{
									rs.Set_Fields("LongDescription", "Lien Tax Aquired Receiv");
									break;
								}
							case 28:
								{
									rs.Set_Fields("LongDescription", "Water Tax Aquired Receiv");
									break;
								}
							case 29:
								{
									rs.Set_Fields("LongDescription", "Wat Lien Tax Aquired Rec");
									break;
								}
							case 30:
								{
									rs.Set_Fields("LongDescription", "Sewer Tax Aquired Receiv");
									break;
								}
							case 31:
								{
									rs.Set_Fields("LongDescription", "Sew Lien Tax Aquired Rec");
									break;
								}
							default:
								{
									rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, NameCol));
									break;
								}
						}
						//end switch
						rs.Update();
					}
				}
			}
			if (vs1.TextMatrix(32, AccountCol) != "" && Conversion.Val(vs1.TextMatrix(32, AccountCol)) != 0)
			{
				modBudgetaryAccounting.UpdateBDVariable("CashDue", vs1.TextMatrix(32, AccountCol));
				if (vs1.TextMatrix(33, AccountCol) != "")
				{
					modBudgetaryAccounting.UpdateBDVariable("DueFrom", vs1.TextMatrix(33, AccountCol));
				}
				if (vs1.TextMatrix(34, AccountCol) != "")
				{
					modBudgetaryAccounting.UpdateBDVariable("DueTo", vs1.TextMatrix(34, AccountCol));
				}
			}
			modBudgetaryAccounting.GetControlAccounts();
			if (strCRTypes != "")
			{
				strCRTypes = Strings.Left(strCRTypes, strCRTypes.Length - 2);
			}
			if (blnPYUpdate && blnCRUpdate)
			{
				MessageBox.Show("Save successful!  Payroll Cash Account and Cash Receipts Type(s): " + strCRTypes + " have been updated.", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else if (blnPYUpdate)
			{
				MessageBox.Show("Save successful!  Payroll Cash Account has been updated.", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else if (blnCRUpdate)
			{
				MessageBox.Show("Save successful!  Cash Receipts Type(s): " + strCRTypes + " have been updated.", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			if (blnUnload)
			{
				Close();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
	}
}
