//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeProjectDetail.
	/// </summary>
	partial class frmCustomizeProjectDetail : BaseForm
	{
		public fecherFoundation.FCComboBox cmbYTDDebitsCredits;
		public fecherFoundation.FCLabel lblYTDDebitsCredits;
		public fecherFoundation.FCComboBox cmbPendingDetail;
		public fecherFoundation.FCLabel lblPendingDetail;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeProjectDetail));
            this.cmbYTDDebitsCredits = new fecherFoundation.FCComboBox();
            this.lblYTDDebitsCredits = new fecherFoundation.FCLabel();
            this.cmbPendingDetail = new fecherFoundation.FCComboBox();
            this.lblPendingDetail = new fecherFoundation.FCLabel();
            this.cdgFont = new fecherFoundation.FCCommonDialog();
            this.fraInformation = new fecherFoundation.FCFrame();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
            this.fraInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 390);
            this.BottomPanel.Size = new System.Drawing.Size(627, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Controls.Add(this.fraInformation);
            this.ClientArea.Size = new System.Drawing.Size(627, 330);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(627, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(324, 30);
            this.HeaderText.Text = "Project Detail Report Format";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbYTDDebitsCredits
            // 
            this.cmbYTDDebitsCredits.Items.AddRange(new object[] {
            "Debits / Credits",
            "Net Total"});
            this.cmbYTDDebitsCredits.Location = new System.Drawing.Point(205, 30);
            this.cmbYTDDebitsCredits.Name = "cmbYTDDebitsCredits";
            this.cmbYTDDebitsCredits.Size = new System.Drawing.Size(200, 40);
            this.cmbYTDDebitsCredits.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbYTDDebitsCredits, null);
            // 
            // lblYTDDebitsCredits
            // 
            this.lblYTDDebitsCredits.Location = new System.Drawing.Point(20, 44);
            this.lblYTDDebitsCredits.Name = "lblYTDDebitsCredits";
            this.lblYTDDebitsCredits.Size = new System.Drawing.Size(150, 16);
            this.lblYTDDebitsCredits.TabIndex = 3;
            this.lblYTDDebitsCredits.Text = "ACCOUNT INFORMATION";
            this.ToolTip1.SetToolTip(this.lblYTDDebitsCredits, null);
            // 
            // cmbPendingDetail
            // 
            this.cmbPendingDetail.Items.AddRange(new object[] {
            "Show Detail",
            "Do Not Include",
            "Show Summary Only"});
            this.cmbPendingDetail.Location = new System.Drawing.Point(205, 90);
            this.cmbPendingDetail.Name = "cmbPendingDetail";
            this.cmbPendingDetail.Size = new System.Drawing.Size(200, 40);
            this.ToolTip1.SetToolTip(this.cmbPendingDetail, null);
            // 
            // lblPendingDetail
            // 
            this.lblPendingDetail.Location = new System.Drawing.Point(20, 99);
            this.lblPendingDetail.Name = "lblPendingDetail";
            this.lblPendingDetail.Size = new System.Drawing.Size(115, 16);
            this.lblPendingDetail.TabIndex = 1;
            this.lblPendingDetail.Text = "PENDING ACTIVITY";
            this.ToolTip1.SetToolTip(this.lblPendingDetail, null);
            // 
            // cdgFont
            // 
            this.cdgFont.Name = "cdgFont";
            this.cdgFont.Size = new System.Drawing.Size(0, 0);
            this.ToolTip1.SetToolTip(this.cdgFont, null);
            // 
            // fraInformation
            // 
            this.fraInformation.Controls.Add(this.cmbPendingDetail);
            this.fraInformation.Controls.Add(this.lblPendingDetail);
            this.fraInformation.Controls.Add(this.cmbYTDDebitsCredits);
            this.fraInformation.Controls.Add(this.lblYTDDebitsCredits);
            this.fraInformation.Location = new System.Drawing.Point(30, 83);
            this.fraInformation.Name = "fraInformation";
            this.fraInformation.Size = new System.Drawing.Size(425, 150);
            this.fraInformation.TabIndex = 10;
            this.fraInformation.Text = "Information To Be Reported";
            this.ToolTip1.SetToolTip(this.fraInformation, null);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(154, 13);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(200, 40);
            this.txtDescription.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 30);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(135, 16);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Text = "REPORT FORMAT";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(238, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(90, 48);
            this.cmdSave.TabIndex = 4;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmCustomizeProjectDetail
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(627, 498);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCustomizeProjectDetail";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Project Detail Report Format";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomizeProjectDetail_Load);
            this.Activated += new System.EventHandler(this.frmCustomizeProjectDetail_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeProjectDetail_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeProjectDetail_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
            this.fraInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdSave;
	}
}