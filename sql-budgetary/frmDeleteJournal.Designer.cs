﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmDeleteJournal.
	/// </summary>
	partial class frmDeleteJournal : BaseForm
	{
		public fecherFoundation.FCComboBox cboJournals;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeleteJournal));
			this.cboJournals = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 293);
			this.BottomPanel.Size = new System.Drawing.Size(598, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdFilePrint);
			this.ClientArea.Controls.Add(this.cboJournals);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(598, 233);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(598, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(296, 30);
			this.HeaderText.Text = "Delete Unposted Journals";
			// 
			// cboJournals
			// 
			this.cboJournals.AutoSize = false;
			this.cboJournals.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournals.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournals.FormattingEnabled = true;
			this.cboJournals.Location = new System.Drawing.Point(30, 60);
			this.cboJournals.Name = "cboJournals";
			this.cboJournals.Size = new System.Drawing.Size(331, 40);
			this.cboJournals.TabIndex = 1;
			this.cboJournals.DropDown += new System.EventHandler(this.cboJournals_DropDown);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(306, 16);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "SELECT A JOURNAL AND CLICK THE DELETE BUTTON";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 120);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(56, 17);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "WARNING";
			// 
			// Label2
			// 
			this.Label2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Label2.Location = new System.Drawing.Point(113, 120);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(412, 16);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "ONCE DELETED, JOURNAL INFORMATION IS NO LONGER IN THE SYSTEM";
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.AppearanceKey = "acceptButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(30, 150);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(134, 48);
			this.cmdFilePrint.TabIndex = 4;
			this.cmdFilePrint.Text = "Delete Journal";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// frmDeleteJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(598, 401);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmDeleteJournal";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Delete Unposted Journals";
			this.Load += new System.EventHandler(this.frmDeleteJournal_Load);
			this.Activated += new System.EventHandler(this.frmDeleteJournal_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeleteJournal_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdFilePrint;
	}
}
