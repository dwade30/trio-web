﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreateACHFiles.
	/// </summary>
	partial class frmCreateACHFiles : BaseForm
	{
		public fecherFoundation.FCTextBox txtFileName;
		public Global.T2KDateBox T2KEffectiveEntryDate;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateACHFiles));
            this.txtFileName = new fecherFoundation.FCTextBox();
            this.T2KEffectiveEntryDate = new Global.T2KDateBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmbACH = new fecherFoundation.FCComboBox();
            this.fraACHFile = new fecherFoundation.FCFrame();
            this.lblJournal = new fecherFoundation.FCLabel();
            this.cboJournal = new fecherFoundation.FCComboBox();
            this.fraPrenoteFile = new fecherFoundation.FCFrame();
            this.lblBank = new fecherFoundation.FCLabel();
            this.cboBanks = new fecherFoundation.FCComboBox();
            this.lblAllEFTVendors = new fecherFoundation.FCLabel();
            this.cmbAllEFTVendors = new fecherFoundation.FCComboBox();
            this.lblACH = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KEffectiveEntryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraACHFile)).BeginInit();
            this.fraACHFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPrenoteFile)).BeginInit();
            this.fraPrenoteFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 469);
            this.BottomPanel.Size = new System.Drawing.Size(609, 94);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblACH);
            this.ClientArea.Controls.Add(this.cmbACH);
            this.ClientArea.Controls.Add(this.fraPrenoteFile);
            this.ClientArea.Controls.Add(this.fraACHFile);
            this.ClientArea.Controls.Add(this.txtFileName);
            this.ClientArea.Controls.Add(this.T2KEffectiveEntryDate);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Size = new System.Drawing.Size(609, 409);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(609, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(123, 30);
            this.HeaderText.Text = "ACH Files";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileName.Location = new System.Drawing.Point(268, 90);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(323, 40);
            this.txtFileName.TabIndex = 3;
            this.txtFileName.Text = "APACHExp.txt";
            this.ToolTip1.SetToolTip(this.txtFileName, "The filename for entries not in separate files");
            // 
            // T2KEffectiveEntryDate
            // 
            this.T2KEffectiveEntryDate.Location = new System.Drawing.Point(268, 38);
            this.T2KEffectiveEntryDate.Mask = "##/##/####";
            this.T2KEffectiveEntryDate.MaxLength = 10;
            this.T2KEffectiveEntryDate.Name = "T2KEffectiveEntryDate";
            this.T2KEffectiveEntryDate.Size = new System.Drawing.Size(115, 40);
            this.T2KEffectiveEntryDate.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.T2KEffectiveEntryDate, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 104);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(177, 16);
            this.Label8.TabIndex = 4;
            this.Label8.Text = "GROUPED ENTRIES FILE NAME";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(150, 16);
            this.Label7.TabIndex = 1;
            this.Label7.Text = "EFFECTIVE ENTRY DATE";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(203, 23);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(174, 48);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmbACH
            // 
            this.cmbACH.Items.AddRange(new object[] {
            "ACH File",
            "Prenote File"});
            this.cmbACH.Location = new System.Drawing.Point(268, 150);
            this.cmbACH.Name = "cmbACH";
            this.cmbACH.Size = new System.Drawing.Size(323, 40);
            this.cmbACH.TabIndex = 10;
            this.cmbACH.SelectedIndexChanged += new System.EventHandler(this.cmbACH_SelectedIndexChanged);
            // 
            // fraACHFile
            // 
            this.fraACHFile.AppearanceKey = "groupBoxNoBorders";
            this.fraACHFile.Controls.Add(this.lblJournal);
            this.fraACHFile.Controls.Add(this.cboJournal);
            this.fraACHFile.Location = new System.Drawing.Point(30, 340);
            this.fraACHFile.Name = "fraACHFile";
            this.fraACHFile.Size = new System.Drawing.Size(561, 65);
            this.fraACHFile.TabIndex = 5;
            // 
            // lblJournal
            // 
            this.lblJournal.Name = "lblJournal";
            this.lblJournal.Size = new System.Drawing.Size(130, 15);
            this.lblJournal.TabIndex = 12;
            this.lblJournal.Text = "SELECT AP JOURNAL";
            // 
            // cboJournal
            // 
            this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
            this.cboJournal.Location = new System.Drawing.Point(0, 25);
            this.cboJournal.Name = "cboJournal";
            this.cboJournal.Size = new System.Drawing.Size(561, 40);
            this.cboJournal.TabIndex = 6;
            // 
            // fraPrenoteFile
            // 
            this.fraPrenoteFile.AppearanceKey = "groupBoxNoBorders";
            this.fraPrenoteFile.Controls.Add(this.lblBank);
            this.fraPrenoteFile.Controls.Add(this.cboBanks);
            this.fraPrenoteFile.Controls.Add(this.lblAllEFTVendors);
            this.fraPrenoteFile.Controls.Add(this.cmbAllEFTVendors);
            this.fraPrenoteFile.Location = new System.Drawing.Point(30, 210);
            this.fraPrenoteFile.Name = "fraPrenoteFile";
            this.fraPrenoteFile.Size = new System.Drawing.Size(561, 100);
            this.fraPrenoteFile.TabIndex = 9;
            // 
            // lblBank
            // 
            this.lblBank.Anchor = Wisej.Web.AnchorStyles.Left;
            this.lblBank.Location = new System.Drawing.Point(0, 74);
            this.lblBank.Name = "lblBank";
            this.lblBank.Size = new System.Drawing.Size(88, 16);
            this.lblBank.TabIndex = 14;
            this.lblBank.Text = "SELECT BANK";
            // 
            // cboBanks
            // 
            this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
            this.cboBanks.Location = new System.Drawing.Point(238, 60);
            this.cboBanks.Name = "cboBanks";
            this.cboBanks.Size = new System.Drawing.Size(323, 40);
            this.cboBanks.TabIndex = 13;
            // 
            // lblAllEFTVendors
            // 
            this.lblAllEFTVendors.Location = new System.Drawing.Point(0, 14);
            this.lblAllEFTVendors.Name = "lblAllEFTVendors";
            this.lblAllEFTVendors.Size = new System.Drawing.Size(66, 16);
            this.lblAllEFTVendors.TabIndex = 15;
            this.lblAllEFTVendors.Text = "VENDORS";
            // 
            // cmbAllEFTVendors
            // 
            this.cmbAllEFTVendors.Items.AddRange(new object[] {
            "All EFT Vendors",
            "Only EFT Vendors marked as Prenote"});
            this.cmbAllEFTVendors.Location = new System.Drawing.Point(238, 0);
            this.cmbAllEFTVendors.Name = "cmbAllEFTVendors";
            this.cmbAllEFTVendors.Size = new System.Drawing.Size(323, 40);
            this.cmbAllEFTVendors.TabIndex = 14;
            // 
            // lblACH
            // 
            this.lblACH.Location = new System.Drawing.Point(30, 164);
            this.lblACH.Name = "lblACH";
            this.lblACH.Size = new System.Drawing.Size(66, 16);
            this.lblACH.TabIndex = 16;
            this.lblACH.Text = "FILE TYPE";
            // 
            // frmCreateACHFiles
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(609, 563);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCreateACHFiles";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "ACH Files";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmCreateACHFiles_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCreateACHFiles_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KEffectiveEntryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraACHFile)).EndInit();
            this.fraACHFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraPrenoteFile)).EndInit();
            this.fraPrenoteFile.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
        public FCLabel lblACH;
        public FCComboBox cmbACH;
        public FCFrame fraPrenoteFile;
        public FCLabel lblBank;
        public FCComboBox cboBanks;
        public FCLabel lblAllEFTVendors;
        public FCComboBox cmbAllEFTVendors;
        public FCFrame fraACHFile;
        public FCLabel lblJournal;
        public FCComboBox cboJournal;
    }
}
