﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChooseImage.
	/// </summary>
	partial class frmChooseImage : BaseForm
	{
		public fecherFoundation.FCComboBox cmbShow;
		public fecherFoundation.FCCheckBox chkUseTownSeal;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPictureBox Image1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseImage));
            this.cmbShow = new fecherFoundation.FCComboBox();
            this.chkUseTownSeal = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.cmdSave = new fecherFoundation.FCButton();
            this.upload1 = new Wisej.Web.Upload();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseTownSeal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 487);
            this.BottomPanel.Size = new System.Drawing.Size(1088, 13);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.upload1);
            this.ClientArea.Controls.Add(this.chkUseTownSeal);
            this.ClientArea.Controls.Add(this.cmbShow);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Image1);
            this.ClientArea.Size = new System.Drawing.Size(1088, 427);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(209, 30);
            this.HeaderText.Text = "Select Town Logo";
            // 
            // cmbShow
            // 
            this.cmbShow.Items.AddRange(new object[] {
            "Use Logo",
            "Don\'t use Logo"});
            this.cmbShow.Location = new System.Drawing.Point(412, 30);
            this.cmbShow.Name = "cmbShow";
            this.cmbShow.Size = new System.Drawing.Size(310, 40);
            this.cmbShow.TabIndex = 11;
            // 
            // chkUseTownSeal
            // 
            this.chkUseTownSeal.AutoSize = false;
            this.chkUseTownSeal.Location = new System.Drawing.Point(30, 90);
            this.chkUseTownSeal.Name = "chkUseTownSeal";
            this.chkUseTownSeal.Size = new System.Drawing.Size(139, 26);
            this.chkUseTownSeal.TabIndex = 10;
            this.chkUseTownSeal.Text = "Use Town Seal";
            this.chkUseTownSeal.CheckedChanged += new System.EventHandler(this.chkUseTownSeal_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(320, 16);
            this.Label1.TabIndex = 9;
            this.Label1.Text = "THIS OPTION ONLY APPLIES TO LASER FORMAT CHECKS";
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Enabled = false;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(30, 219);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(92, 96);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 13;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 353);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // upload1
            // 
            this.upload1.Location = new System.Drawing.Point(30, 147);
            this.upload1.Name = "upload1";
            this.upload1.Size = new System.Drawing.Size(320, 40);
            this.upload1.TabIndex = 14;
            this.upload1.Text = "Select Logo";
            this.upload1.Uploaded += new Wisej.Web.UploadedEventHandler(this.upload1_Uploaded);
            // 
            // frmChooseImage
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1088, 500);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmChooseImage";
            this.Text = "Select Town Logo";
            this.Load += new System.EventHandler(this.frmChooseImage_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChooseImage_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseTownSeal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private Upload upload1;
	}
}
