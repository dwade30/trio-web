﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReportSetup.
	/// </summary>
	partial class frmReportSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbExpSummary;
		public fecherFoundation.FCFrame fraReportType;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblType;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReportSetup));
			this.cmbExpSummary = new fecherFoundation.FCComboBox();
			this.fraReportType = new fecherFoundation.FCFrame();
			this.cmdOK = new fecherFoundation.FCButton();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblType = new fecherFoundation.FCLabel();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdChange = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReportType)).BeginInit();
			this.fraReportType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdChange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraReportType);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblType);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdChange);
			this.TopPanel.Controls.SetChildIndex(this.cmdChange, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(157, 30);
			this.HeaderText.Text = "Report Setup";
			// 
			// cmbExpSummary
			// 
			this.cmbExpSummary.AutoSize = false;
			this.cmbExpSummary.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbExpSummary.FormattingEnabled = true;
			this.cmbExpSummary.Items.AddRange(new object[] {
				"Town Expense Summary",
				"Town Revenue Summary",
				"Town G/L Summary",
				"Town Expense Detail",
				"Town Revenue Detail",
				"Town G/L Detail",
				"Project Summary",
				"Project Detail"
			});
			this.cmbExpSummary.Location = new System.Drawing.Point(20, 30);
			this.cmbExpSummary.Name = "cmbExpSummary";
			this.cmbExpSummary.Size = new System.Drawing.Size(300, 40);
			this.cmbExpSummary.TabIndex = 9;
			// 
			// fraReportType
			// 
			this.fraReportType.Controls.Add(this.cmbExpSummary);
			this.fraReportType.Controls.Add(this.cmdOK);
			this.fraReportType.Location = new System.Drawing.Point(30, 30);
			this.fraReportType.Name = "fraReportType";
			this.fraReportType.Size = new System.Drawing.Size(340, 150);
			this.fraReportType.TabIndex = 0;
			this.fraReportType.Text = "Report Type";
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "acceptButton";
			this.cmdOK.Location = new System.Drawing.Point(20, 80);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(90, 48);
			this.cmdOK.TabIndex = 7;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 30);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 50;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1018, 438);
			this.vs1.StandardTab = true;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 9;
			this.vs1.Visible = false;
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			// 
			// lblType
			// 
			this.lblType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblType.Location = new System.Drawing.Point(141, 86);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(335, 26);
			this.lblType.TabIndex = 10;
			this.lblType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Enabled = false;
			this.cmdNew.Location = new System.Drawing.Point(790, 27);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(42, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdCreate_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Enabled = false;
			this.cmdDelete.Location = new System.Drawing.Point(835, 27);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(57, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdChange
			// 
			this.cmdChange.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdChange.AppearanceKey = "toolbarButton";
			this.cmdChange.Enabled = false;
			this.cmdChange.Location = new System.Drawing.Point(895, 27);
			this.cmdChange.Name = "cmdChange";
			this.cmdChange.Size = new System.Drawing.Size(142, 24);
			this.cmdChange.TabIndex = 3;
			this.cmdChange.Text = "Change Report Type";
			this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(489, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmReportSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReportSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Report Setup";
			this.Load += new System.EventHandler(this.frmReportSetup_Load);
			this.Activated += new System.EventHandler(this.frmReportSetup_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReportSetup_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReportSetup_KeyPress);
			this.Resize += new System.EventHandler(this.frmReportSetup_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReportType)).EndInit();
			this.fraReportType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdChange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdNew;
		private FCButton cmdDelete;
		private FCButton cmdChange;
	}
}
