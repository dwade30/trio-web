﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeExpenseSummary.
	/// </summary>
	public partial class frmCustomizeExpenseSummary : BaseForm
	{
		public frmCustomizeExpenseSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeExpenseSummary InstancePtr
		{
			get
			{
				return (frmCustomizeExpenseSummary)Sys.GetInstance(typeof(frmCustomizeExpenseSummary));
			}
		}

		protected frmCustomizeExpenseSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int Choices;
		bool FormatFlag;
		bool PendingFlag;
		bool OutstandingFlag;
		int PendingChoice;
		int MaxItems;
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		bool UseMax;
		//bool LargeFont;
		bool SpecificFontFlag;
		public bool FromExp;
		bool blnSavedFormat;
		string strWidth;

		private void chkAdjustments_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkAdjustments.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkAdjustments.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkBalance_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkBalance.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkBalance.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkBudget_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkBudget.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkBudget.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkNetBudget_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkNetBudget.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkNetBudget.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkPercent_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkPercent.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkPercent.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkSelectedDebitsCredits_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkSelectedDebitsCredits.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems - 1)
						{
							Choices += 2;
						}
						else
						{
							FormatFlag = true;
							chkSelectedDebitsCredits.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 2;
					}
				}
				else
				{
					Choices -= 2;
				}
			}
			ShowItems();
		}

		private void chkSelectedNet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkSelectedNet.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkSelectedNet.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkYTDDebitsCredits_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkYTDDebitsCredits.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems - 1)
						{
							Choices += 2;
						}
						else
						{
							FormatFlag = true;
							chkYTDDebitsCredits.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 2;
					}
				}
				else
				{
					Choices -= 2;
				}
			}
			ShowItems();
		}

		private void chkYTDNet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkYTDNet.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkYTDNet.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

	
		private void frmCustomizeExpenseSummary_Activated(object sender, System.EventArgs e)
		{
            if (!modGlobal.FormExist(this))
            {
                Choices = 0;
                OutstandingFlag = false;
                PendingFlag = false;
                PendingChoice = 2;
                txtDescription.Focus();
                UseMax = true;
                SpecificFontFlag = false;
                MaxItems = 5;
                strWidth = "N";
                this.Refresh();
                if (!modBudgetaryMaster.Statics.blnExpenseSummaryEdit &&
                    !modBudgetaryMaster.Statics.blnExpenseSummaryReportEdit)
                {
                    // do nothing
                }
                else
                {
                    if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
                    {
                        // do nothing
                    }
                    else
                    {
                        if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Use")) ==
                            "P")
                        {
                            cmbPrint.SelectedIndex = 0;
                        }
                        else
                        {
                            cmbPrint.SelectedIndex = 1;
                            lblNumber.Text = "All";
                            UseMax = false;
                        }
                        if (FCConvert.ToString(
                                modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("PaperWidth")) == "N")
                        {
                            cmbLandscape.SelectedIndex = 1;
                            strWidth = "N";
                        }
                        else
                        {
                            cmbLandscape.SelectedIndex = 0;
                            strWidth = "L";
                        }
                        cmbShortDescriptions.SelectedIndex = FCConvert.ToString(
                                                                 modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("DescriptionLength")) ==
                                                             "L" ? 0 : 1;
                        txtDescription.Text =
                            FCConvert.ToString(
                                modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
                        bool optShowOutstandingEncumbrance =
                            FCConvert.ToBoolean(
                                modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("OSEncumbrance"));
                        cmbShowOutstandingEncumbrance.SelectedIndex = optShowOutstandingEncumbrance ? 0 : 1;                       
                        bool optShowPending =
                            FCConvert.ToBoolean(
                                modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("SeperatePending"));
                        bool optIncludePending =
                            FCConvert.ToBoolean(
                                modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("IncludePending"));
                        if (!optShowPending && !optIncludePending)
                        {
                            cmbShowPending.SelectedIndex = 2;
                        }
                        else if (optShowPending)
                        {
                            cmbShowPending.SelectedIndex = 0;
                        }
                        else
                        {
                            cmbShowPending.SelectedIndex = 1;
                        }
                        cmbYTD.SelectedIndex = FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MTDBudget")) ? 1 : 0;
                        chkBudget.CheckState = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("Budget")) ? CheckState.Checked : CheckState.Unchecked;
                        chkAdjustments.CheckState = FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("BudgetAdjustment")) ? CheckState.Checked : CheckState.Unchecked;
                        chkNetBudget.CheckState = FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("NetBudget")) ? CheckState.Checked : CheckState.Unchecked;
                        chkSelectedDebitsCredits.CheckState = FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CurrentDebitCredit")) ? CheckState.Checked : CheckState.Unchecked;
                        chkSelectedNet.CheckState = FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CurrentNet")) ? CheckState.Checked : CheckState.Unchecked;
                        chkYTDDebitsCredits.CheckState = FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDDebitCredit")) ? CheckState.Checked : CheckState.Unchecked;
                        chkYTDNet.CheckState = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDNet")) ? CheckState.Checked : CheckState.Unchecked;
                        // TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
                        chkBalance.CheckState = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Balance")) ? CheckState.Checked : CheckState.Unchecked;
                        chkPercent.CheckState = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("Spent")) ? CheckState.Checked : CheckState.Unchecked;
                    }
                }
            }
        }

		private void frmCustomizeExpenseSummary_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeExpenseSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeExpenseSummary.FillStyle	= 0;
			//frmCustomizeExpenseSummary.ScaleWidth	= 9300;
			//frmCustomizeExpenseSummary.ScaleHeight	= 7380;
			//frmCustomizeExpenseSummary.LinkTopic	= "Form2";
			//frmCustomizeExpenseSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromExp)
			{
				FromExp = false;
				frmExpenseSummarySelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnExpenseSummaryReportEdit)
					{
						modBudgetaryMaster.Statics.blnExpenseSummaryReportEdit = false;
						frmExpenseSummarySelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmExpenseSummarySelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnExpenseSummaryReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnExpenseSummaryEdit)
			{
				modBudgetaryMaster.Statics.blnExpenseSummaryEdit = false;
				frmGetExpenseSummary.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeExpenseSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Choices == 0)
			{
				MessageBox.Show("You must make at least one selection before you can save this format", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkPercent.CheckState == CheckState.Checked)
			{
				if (chkNetBudget.CheckState == CheckState.Unchecked && (chkBudget.CheckState == CheckState.Unchecked || chkAdjustments.CheckState == CheckState.Unchecked))
				{
					MessageBox.Show("You must include Net Budget and YTD Net in this report if you wish to show Percent Spent", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else if (chkBalance.CheckState == CheckState.Unchecked && chkYTDNet.CheckState == CheckState.Unchecked && chkYTDDebitsCredits.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must include Net Budget and YTD Net in this report if you wish to show Percent Spent", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			rs.OpenRecordset("SELECT * FROM ExpenseSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("Budget", chkBudget.CheckState == CheckState.Checked);
					rs.Set_Fields("MTDBudget", cmbYTD.SelectedIndex == 1);
					rs.Set_Fields("BudgetAdjustment", chkAdjustments.CheckState == CheckState.Checked);
					rs.Set_Fields("NetBudget", chkNetBudget.CheckState == CheckState.Checked);
					rs.Set_Fields("CurrentDebitCredit", chkSelectedDebitsCredits.CheckState == CheckState.Checked);
					rs.Set_Fields("CurrentNet", chkSelectedNet.CheckState == CheckState.Checked);
					rs.Set_Fields("YTDDebitCredit", chkYTDDebitsCredits.CheckState == CheckState.Checked);
					rs.Set_Fields("YTDNet", chkYTDNet.CheckState == CheckState.Checked);
                    rs.Set_Fields("DescriptionLength", cmbShortDescriptions.SelectedIndex == 0 ? "L" : "S");
                    rs.Set_Fields("OSEncumbrance", cmbShowOutstandingEncumbrance.SelectedIndex == 0);
					rs.Set_Fields("SeperatePending", cmbShowPending.SelectedIndex == 0);
					rs.Set_Fields("IncludePending", cmbShowPending.SelectedIndex == 1);
					rs.Set_Fields("Balance", chkBalance.CheckState == CheckState.Checked);
					rs.Set_Fields("Spent", chkPercent.CheckState == CheckState.Checked);
					rs.Set_Fields("Printer", "O");
					rs.Set_Fields("Font", "S");
                    rs.Set_Fields("PaperWidth", cmbLandscape.SelectedIndex == 1 ? "N" : "L");
                    rs.Set_Fields("Use", cmbPrint.SelectedIndex == 0 ? "P" : "D");
                    rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExpenseSummaryFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("MTDBudget", cmbYTD.SelectedIndex == 1);
				rs.Set_Fields("Budget", chkBudget.CheckState == CheckState.Checked);
				rs.Set_Fields("BudgetAdjustment", chkAdjustments.CheckState == CheckState.Checked);
				rs.Set_Fields("NetBudget", chkNetBudget.CheckState == CheckState.Checked);
				rs.Set_Fields("CurrentDebitCredit", chkSelectedDebitsCredits.CheckState == CheckState.Checked);
				rs.Set_Fields("CurrentNet", chkSelectedNet.CheckState == CheckState.Checked);
				rs.Set_Fields("YTDDebitCredit", chkYTDDebitsCredits.CheckState == CheckState.Checked);
				rs.Set_Fields("YTDNet", chkYTDNet.CheckState == CheckState.Checked);
                rs.Set_Fields("DescriptionLength", cmbShortDescriptions.SelectedIndex == 0 ? "L" : "S");
                rs.Set_Fields("OSEncumbrance", cmbShowOutstandingEncumbrance.SelectedIndex == 0);
				rs.Set_Fields("SeperatePending", cmbShowPending.SelectedIndex == 0);
				rs.Set_Fields("IncludePending", cmbShowPending.SelectedIndex == 2);
				rs.Set_Fields("Balance", chkBalance.CheckState == CheckState.Checked);
				rs.Set_Fields("Spent", chkPercent.CheckState == CheckState.Checked);
				rs.Set_Fields("Printer", "O");
				rs.Set_Fields("Font", "S");
                rs.Set_Fields("PaperWidth", cmbLandscape.SelectedIndex == 1 ? "N" : "L");
                rs.Set_Fields("Use", cmbPrint.SelectedIndex == 0 ? "P" : "D");
                rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}

		private void optLandscape_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbLandscape.SelectedIndex == 0)
			{
                if (!FormatFlag)
                {
                    MaxItems += 2;
                    strWidth = "L";
                }

                ShowItems();
			}
			else if (this.cmbLandscape.SelectedIndex == 1)
			{
				if (!FormatFlag)
				{
					if (UseMax)
					{
                        if (Choices < MaxItems - 1)
                        {
                            MaxItems -= 2;
                            strWidth = "N";
                        }
                        else
                        {
                            //FormatFlag = true;
                            //optNarrow.Checked = false;
                            //optLandscape.Checked = true;
                            cmbLandscape.SelectedIndex = 0;
                            FormatFlag = false;
                            MessageBox.Show(
                                "You must take " + FCConvert.ToString(Choices - (MaxItems - 2)) +
                                " items out of the report before you can make this change", "Too Many Items in Report",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
					else
                    {
                        MaxItems -= 2;
                        strWidth = "N";
                    }
				}
				ShowItems();
			}
		}

		private void optPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbPrint.SelectedIndex == 0)
			{
				if (!FormatFlag)
				{
					if (Choices <= MaxItems)
					{
						UseMax = true;
					}
					else
					{
						FormatFlag = true;
						//optPrint.Checked = false;
						//optDisplay.Checked = true;
						cmbPrint.SelectedIndex = 1;
						FormatFlag = false;
						MessageBox.Show("You must take " + FCConvert.ToString(Choices - MaxItems) + " items out of the report before you can make this change", "Too Many Items in Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				ShowItems();
			}
			else if (this.cmbPrint.SelectedIndex == 1)
			{
				if (!FormatFlag)
				{
					if (!SpecificFontFlag)
					{
						lblNumber.Text = "All";
						UseMax = false;
					}
				}
			}
		}

		private void optShortDescriptions_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbShortDescriptions.SelectedIndex == 1)
			{
				if (!FormatFlag)
				{
					MaxItems += 1;
					ShowItems();
				}
			}
			else if (this.cmbShortDescriptions.SelectedIndex == 0)
			{
				if (!FormatFlag)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							MaxItems -= 1;
						}
						else
						{
							FormatFlag = true;
							cmbShortDescriptions.SelectedIndex = 1;
							FormatFlag = false;
							MessageBox.Show("You must take 1 item out of this report format to make this change", "Too Many Items in Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						MaxItems -= 1;
					}
				}
				ShowItems();
			}
		}

		private void optShowOutstanding_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbShowOutstandingEncumbrance.SelectedIndex == 0)
			{
				if (!OutstandingFlag)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							OutstandingFlag = true;
							Choices += 1;
						}
						else
						{
							cmbShowOutstandingEncumbrance.SelectedIndex = 1;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				ShowItems();
			}
			else if (this.cmbShowOutstandingEncumbrance.SelectedIndex == 1)
			{
				if (OutstandingFlag)
				{
					OutstandingFlag = false;
					Choices -= 1;
				}
				ShowItems();
			}
		}

		private void optShowPending_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbShowPending.SelectedIndex == 0)
			{
				if (!PendingFlag)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							PendingFlag = true;
							Choices += 1;
							PendingChoice = 1;
						}
						else
						{
							if (PendingChoice == 2)
							{
								cmbShowPending.SelectedIndex = 2;
							}
							else
							{
								cmbShowPending.SelectedIndex = 1;
							}
						}
					}
					else
					{
						PendingFlag = true;
						Choices += 1;
						PendingChoice = 1;
					}
				}
				ShowItems();
			}
			else if (this.cmbShowPending.SelectedIndex == 1)
			{
				if (PendingFlag)
				{
					PendingFlag = false;
					Choices -= 1;
				}
				PendingChoice = 2;
				ShowItems();
			}
			else if (this.cmbShowPending.SelectedIndex == 2)
			{
				if (PendingFlag)
				{
					PendingFlag = false;
					Choices -= 1;
				}
				PendingChoice = 3;
				ShowItems();
			}
		}

		private void ShowItems()
		{
			if (cmbPrint.SelectedIndex != 1)
			{
				lblNumber.Text = FCConvert.ToString(MaxItems - Choices);
			}
		}

		private void SetCustomFormColors()
		{
			lblNumber.ForeColor = ColorTranslator.FromOle(0x800080);
		}
	}
}
