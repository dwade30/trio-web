﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomBudgetReport.
	/// </summary>
	public partial class frmCustomBudgetReport : BaseForm
	{
		public frmCustomBudgetReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbPortrait.SelectedIndex = 0;
			cmbReport.SelectedIndex = 0;
			//FC:FINAL:DDU:#2979 - align columns left
			vsWhere.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsWhere.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomBudgetReport InstancePtr
		{
			get
			{
				return (frmCustomBudgetReport)Sys.GetInstance(typeof(frmCustomBudgetReport));
			}
		}

		protected frmCustomBudgetReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomReport.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomReport, "Births")
		//
		//
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		ListViewItem temp = null;
		string strTemp = "";
		bool boolSaveReport;
		int intChoices;
		public bool blnPrint;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (cmbReport.SelectedIndex == 2)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsDelete.Execute("Delete from CustomBudgetReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				rs.OpenRecordset("SELECT * FROM CustomBudgetReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rs.Get_Fields_String("Orientation")) == "L")
					{
						cmbPortrait.SelectedIndex = 1;
					}
					else
					{
						cmbPortrait.SelectedIndex = 0;
					}
					ShowTownOptions();
					if (FCConvert.ToString(rs.Get_Fields_String("ReportingRange")) == "A")
					{
						vsWhere.TextMatrix(3, 0, "Department(s)");
						modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else if (rs.Get_Fields_String("ReportingRange") == "D")
					{
						vsWhere.TextMatrix(3, 0, "Department(s)");
						modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, Color.White);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else if (rs.Get_Fields_String("ReportingRange") == "S")
					{
						vsWhere.TextMatrix(3, 0, "Department(s)");
						modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else
					{
						vsWhere.TextMatrix(3, 0, "Fund");
						modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund", "ShortDescription", "Fund", true);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					vsWhere.TextMatrix(0, 1, FCConvert.ToString(rs.Get_Fields_String("TownSchool")));
					vsWhere.TextMatrix(1, 1, FCConvert.ToString(rs.Get_Fields_String("AccountTypes")));
					vsWhere.TextMatrix(2, 1, FCConvert.ToString(rs.Get_Fields_String("ReportingRange")));
					vsWhere.TextMatrix(3, 1, FCConvert.ToString(rs.Get_Fields_String("LowDept")));
					vsWhere.TextMatrix(3, 2, FCConvert.ToString(rs.Get_Fields_String("HighDept")));
					vsWhere.TextMatrix(4, 1, FCConvert.ToString(rs.Get_Fields_String("CurrentYear")));
					vsWhere.TextMatrix(5, 1, FCConvert.ToString(rs.Get_Fields_String("BudgetYear")));
					vsWhere.TextMatrix(6, 1, FCConvert.ToString(rs.Get_Fields_String("DeptPageBreak")));
					vsWhere.TextMatrix(7, 1, FCConvert.ToString(rs.Get_Fields_String("DivPageBreak")));
					vsWhere.TextMatrix(8, 1, FCConvert.ToString(rs.Get_Fields_String("DeptTotals")));
					vsWhere.TextMatrix(9, 1, FCConvert.ToString(rs.Get_Fields_String("DivTotals")));
					vsWhere.TextMatrix(10, 1, FCConvert.ToString(rs.Get_Fields_String("ExpTotals")));
					vsWhere.TextMatrix(11, 1, FCConvert.ToString(rs.Get_Fields_String("Comments")));
					vsWhere.TextMatrix(12, 1, FCConvert.ToString(rs.Get_Fields_String("ReportTitle")));
					vsWhere.TextMatrix(13, 1, FCConvert.ToString(rs.Get_Fields_String("IncludeBudgetAdjustments")));
					vsWhere.TextMatrix(14, 1, FCConvert.ToString(rs.Get_Fields_Int32("StartingPage")));
					vsWhere.TextMatrix(15, 1, FCConvert.ToString(rs.Get_Fields_String("IncludeCarryForward")));
					vsWhere.TextMatrix(16, 1, FCConvert.ToString(rs.Get_Fields_String("ExpLowestDetailLevel")));
					vsWhere.TextMatrix(17, 1, FCConvert.ToString(rs.Get_Fields_String("RevLowestDetailLevel")));
					lstFields.SetSelected(0, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ThreeYearBudget")));
					lstFields.SetSelected(1, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ThreeYearActual")));
					lstFields.SetSelected(2, FCConvert.ToBoolean(rs.Get_Fields_Boolean("TwoYearBudget")));
					lstFields.SetSelected(3, FCConvert.ToBoolean(rs.Get_Fields_Boolean("TwoYearActual")));
					lstFields.SetSelected(4, FCConvert.ToBoolean(rs.Get_Fields_Boolean("OneYearBudget")));
					lstFields.SetSelected(5, FCConvert.ToBoolean(rs.Get_Fields_Boolean("OneYearActual")));
					// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
					lstFields.SetSelected(6, FCConvert.ToBoolean(rs.Get_Fields("CurrentBudget")));
					lstFields.SetSelected(7, FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentYTD")));
					lstFields.SetSelected(8, FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentBalance")));
					// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
					lstFields.SetSelected(9, FCConvert.ToBoolean(rs.Get_Fields("InitialRequest")));
					// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
					lstFields.SetSelected(10, FCConvert.ToBoolean(rs.Get_Fields("ManagerRequest")));
					// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
					lstFields.SetSelected(11, FCConvert.ToBoolean(rs.Get_Fields("CommitteeRequest")));
					// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
					lstFields.SetSelected(12, FCConvert.ToBoolean(rs.Get_Fields("ElectedRequest")));
					lstFields.SetSelected(13, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ApprovedAmounts")));
					lstFields.SetSelected(14, FCConvert.ToBoolean(rs.Get_Fields_Boolean("InitReqVsCurBudMon")));
					lstFields.SetSelected(15, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ManReqVsCurBudMon")));
					lstFields.SetSelected(16, FCConvert.ToBoolean(rs.Get_Fields_Boolean("CommReqVsCurBudMon")));
					lstFields.SetSelected(17, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ElecReqVsCurBudMon")));
					lstFields.SetSelected(18, FCConvert.ToBoolean(rs.Get_Fields_Boolean("AppAmtVsCurBudMon")));
					lstFields.SetSelected(19, FCConvert.ToBoolean(rs.Get_Fields_Boolean("InitReqVsCurBudPer")));
					lstFields.SetSelected(20, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ManReqVsCurBudPer")));
					lstFields.SetSelected(21, FCConvert.ToBoolean(rs.Get_Fields_Boolean("CommReqVsCurBudPer")));
					lstFields.SetSelected(22, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ElecReqVsCurBudPer")));
					lstFields.SetSelected(23, FCConvert.ToBoolean(rs.Get_Fields_Boolean("AppAmtVsCurBudPer")));
					lstFields.SetSelected(24, FCConvert.ToBoolean(rs.Get_Fields_Boolean("InitReqVsLastYrBudMon")));
					lstFields.SetSelected(25, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ManReqVsLastYrBudMon")));
					lstFields.SetSelected(26, FCConvert.ToBoolean(rs.Get_Fields_Boolean("CommReqVsLastYrBudMon")));
					lstFields.SetSelected(27, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ElecReqVsLastYrBudMon")));
					lstFields.SetSelected(28, FCConvert.ToBoolean(rs.Get_Fields_Boolean("AppAmtVsLastYrBudMon")));
					lstFields.SetSelected(29, FCConvert.ToBoolean(rs.Get_Fields_Boolean("InitReqVsLastYrBudPer")));
					lstFields.SetSelected(30, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ManReqVsLastYrBudPer")));
					lstFields.SetSelected(31, FCConvert.ToBoolean(rs.Get_Fields_Boolean("CommReqVsLastYrBudPer")));
					lstFields.SetSelected(32, FCConvert.ToBoolean(rs.Get_Fields_Boolean("ElecReqVsLastYrBudPer")));
					lstFields.SetSelected(33, FCConvert.ToBoolean(rs.Get_Fields_Boolean("AppAmtVsLastYrBudPer")));
					lstFields.SetSelected(34, FCConvert.ToBoolean(rs.Get_Fields_Boolean("BlankInitialRequest")));
					lstFields.SetSelected(35, FCConvert.ToBoolean(rs.Get_Fields_Boolean("BlankManagerRequest")));
					lstFields.SetSelected(36, FCConvert.ToBoolean(rs.Get_Fields_Boolean("BlankCommitteeRequest")));
					lstFields.SetSelected(37, FCConvert.ToBoolean(rs.Get_Fields_Boolean("BlankElectedRequest")));
					lstFields.SetSelected(38, FCConvert.ToBoolean(rs.Get_Fields_Boolean("BlankApprovedAmount")));
					// TODO Get_Fields: Field [ThreeYearVsTwoYearActMon] not found!! (maybe it is an alias?)
					lstFields.SetSelected(39, FCConvert.ToBoolean(rs.Get_Fields("ThreeYearVsTwoYearActMon")));
					// TODO Get_Fields: Field [ThreeYearVsTwoYearActPer] not found!! (maybe it is an alias?)
					lstFields.SetSelected(40, FCConvert.ToBoolean(rs.Get_Fields("ThreeYearVsTwoYearActPer")));
					// TODO Get_Fields: Field [TwoYearVsOneYearActMon] not found!! (maybe it is an alias?)
					lstFields.SetSelected(41, FCConvert.ToBoolean(rs.Get_Fields("TwoYearVsOneYearActMon")));
					// TODO Get_Fields: Field [TwoYearVsOneYearActPer] not found!! (maybe it is an alias?)
					lstFields.SetSelected(42, FCConvert.ToBoolean(rs.Get_Fields("TwoYearVsOneYearActPer")));
					// TODO Get_Fields: Field [OneYearVsCurrentActMon] not found!! (maybe it is an alias?)
					lstFields.SetSelected(43, FCConvert.ToBoolean(rs.Get_Fields("OneYearVsCurrentActMon")));
					// TODO Get_Fields: Field [OneYearVsCurrentActPer] not found!! (maybe it is an alias?)
					lstFields.SetSelected(44, FCConvert.ToBoolean(rs.Get_Fields("OneYearVsCurrentActPer")));
				}
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			string strReturn;
			clsDRWrapper rsSave = new clsDRWrapper();
			int intRow;
			int intCol;
			clsDRWrapper RSLayout = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report", null);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from CustomBudgetReports where Description = '" + strReturn + "'", modGlobal.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					ans = MessageBox.Show("A report by that name already exists. Do you wish to overwrite this report?", "Overwrite Report?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						rsSave.Edit();
					}
					else
					{
						return;
					}
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Description", Strings.Trim(strReturn));
				if (cmbPortrait.SelectedIndex == 1)
				{
					rsSave.Set_Fields("Orientation", "L");
				}
				else
				{
					rsSave.Set_Fields("Orientation", "P");
				}
				rsSave.Set_Fields("TownSchool", vsWhere.TextMatrix(0, 1));
				rsSave.Set_Fields("AccountTypes", vsWhere.TextMatrix(1, 1));
				rsSave.Set_Fields("ReportingRange", vsWhere.TextMatrix(2, 1));
				rsSave.Set_Fields("LowDept", vsWhere.TextMatrix(3, 1));
				rsSave.Set_Fields("HighDept", vsWhere.TextMatrix(3, 2));
				rsSave.Set_Fields("CurrentYear", vsWhere.TextMatrix(4, 1));
				rsSave.Set_Fields("BudgetYear", vsWhere.TextMatrix(5, 1));
				if (vsWhere.TextMatrix(0, 1) == "T")
				{
					rsSave.Set_Fields("DeptPageBreak", vsWhere.TextMatrix(6, 1));
					rsSave.Set_Fields("DivPageBreak", vsWhere.TextMatrix(7, 1));
					rsSave.Set_Fields("DeptTotals", vsWhere.TextMatrix(8, 1));
					rsSave.Set_Fields("DivTotals", vsWhere.TextMatrix(9, 1));
					rsSave.Set_Fields("ExpTotals", vsWhere.TextMatrix(10, 1));
					rsSave.Set_Fields("Comments", vsWhere.TextMatrix(11, 1));
					rsSave.Set_Fields("ReportTitle", vsWhere.TextMatrix(12, 1));
					rsSave.Set_Fields("IncludeBudgetAdjustments", vsWhere.TextMatrix(13, 1));
					rsSave.Set_Fields("StartingPage", vsWhere.TextMatrix(14, 1));
					rsSave.Set_Fields("IncludeCarryForward", vsWhere.TextMatrix(15, 1));
					rsSave.Set_Fields("ExpLowestDetailLevel", vsWhere.TextMatrix(16, 1));
					rsSave.Set_Fields("RevLowestDetailLevel", vsWhere.TextMatrix(17, 1));
				}
				else
				{
					rsSave.Set_Fields("DeptPageBreak", vsWhere.TextMatrix(6, 1));
					rsSave.Set_Fields("DivPageBreak", vsWhere.TextMatrix(7, 1));
					rsSave.Set_Fields("FunctionPageBreak", vsWhere.TextMatrix(8, 1));
					rsSave.Set_Fields("DeptTotals", vsWhere.TextMatrix(9, 1));
					rsSave.Set_Fields("DivTotals", vsWhere.TextMatrix(10, 1));
					rsSave.Set_Fields("ExpTotals", vsWhere.TextMatrix(11, 1));
					rsSave.Set_Fields("ObjTotals", vsWhere.TextMatrix(12, 1));
					rsSave.Set_Fields("Comments", vsWhere.TextMatrix(13, 1));
					rsSave.Set_Fields("ReportTitle", vsWhere.TextMatrix(14, 1));
					rsSave.Set_Fields("IncludeBudgetAdjustments", vsWhere.TextMatrix(15, 1));
					rsSave.Set_Fields("StartingPage", vsWhere.TextMatrix(16, 1));
					rsSave.Set_Fields("IncludeCarryForward", vsWhere.TextMatrix(17, 1));
				}
				rsSave.Set_Fields("ThreeYearBudget", lstFields.Selected(0) == true);
				rsSave.Set_Fields("ThreeYearActual", lstFields.Selected(1) == true);
				rsSave.Set_Fields("TwoYearBudget", lstFields.Selected(2) == true);
				rsSave.Set_Fields("TwoYearActual", lstFields.Selected(3) == true);
				rsSave.Set_Fields("OneYearBudget", lstFields.Selected(4) == true);
				rsSave.Set_Fields("OneYearActual", lstFields.Selected(5) == true);
				rsSave.Set_Fields("CurrentBudget", lstFields.Selected(6) == true);
				rsSave.Set_Fields("CurrentYTD", lstFields.Selected(7) == true);
				rsSave.Set_Fields("CurrentBalance", lstFields.Selected(8) == true);
				rsSave.Set_Fields("InitialRequest", lstFields.Selected(9) == true);
				rsSave.Set_Fields("ManagerRequest", lstFields.Selected(10) == true);
				rsSave.Set_Fields("CommitteeRequest", lstFields.Selected(11) == true);
				rsSave.Set_Fields("ElectedRequest", lstFields.Selected(12) == true);
				rsSave.Set_Fields("ApprovedAmounts", lstFields.Selected(13) == true);
				rsSave.Set_Fields("InitReqVsCurBudMon", lstFields.Selected(14) == true);
				rsSave.Set_Fields("ManReqVsCurBudMon", lstFields.Selected(15) == true);
				rsSave.Set_Fields("CommReqVsCurBudMon", lstFields.Selected(16) == true);
				rsSave.Set_Fields("ElecReqVsCurBudMon", lstFields.Selected(17) == true);
				rsSave.Set_Fields("AppAmtVsCurBudMon", lstFields.Selected(18) == true);
				rsSave.Set_Fields("InitReqVsCurBudPer", lstFields.Selected(19) == true);
				rsSave.Set_Fields("ManReqVsCurBudPer", lstFields.Selected(20) == true);
				rsSave.Set_Fields("CommReqVsCurBudPer", lstFields.Selected(21) == true);
				rsSave.Set_Fields("ElecReqVsCurBudPer", lstFields.Selected(22) == true);
				rsSave.Set_Fields("AppAmtVsCurBudPer", lstFields.Selected(23) == true);
				rsSave.Set_Fields("InitReqVsLastYrBudMon", lstFields.Selected(24) == true);
				rsSave.Set_Fields("ManReqVsLastYrBudMon", lstFields.Selected(25) == true);
				rsSave.Set_Fields("CommReqVsLastYrBudMon", lstFields.Selected(26) == true);
				rsSave.Set_Fields("ElecReqVsLastYrBudMon", lstFields.Selected(27) == true);
				rsSave.Set_Fields("AppAmtVsLastYrBudMon", lstFields.Selected(28) == true);
				rsSave.Set_Fields("InitReqVsLastYrBudPer", lstFields.Selected(29) == true);
				rsSave.Set_Fields("ManReqVsLastYrBudPer", lstFields.Selected(30) == true);
				rsSave.Set_Fields("CommReqVsLastYrBudPer", lstFields.Selected(31) == true);
				rsSave.Set_Fields("ElecReqVsLastYrBudPer", lstFields.Selected(32) == true);
				rsSave.Set_Fields("AppAmtVsLastYrBudPer", lstFields.Selected(33) == true);
				rsSave.Set_Fields("BlankInitialRequest", lstFields.Selected(34) == true);
				rsSave.Set_Fields("BlankManagerRequest", lstFields.Selected(35) == true);
				rsSave.Set_Fields("BlankCommitteeRequest", lstFields.Selected(36) == true);
				rsSave.Set_Fields("BlankElectedRequest", lstFields.Selected(37) == true);
				rsSave.Set_Fields("BlankApprovedAmount", lstFields.Selected(38) == true);
				rsSave.Set_Fields("ThreeYearVsTwoYearActMon", lstFields.Selected(39) == true);
				rsSave.Set_Fields("ThreeYearVsTwoYearActPer", lstFields.Selected(40) == true);
				rsSave.Set_Fields("TwoYearVsOneYearActMon", lstFields.Selected(41) == true);
				rsSave.Set_Fields("TwoYearVsOneYearActPer", lstFields.Selected(42) == true);
				rsSave.Set_Fields("OneYearVsCurrentActMon", lstFields.Selected(43) == true);
				rsSave.Set_Fields("OneYearVsCurrentActPer", lstFields.Selected(44) == true);
				rsSave.Update();
				LoadCombo();
				MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(mnuClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			int counter;
			string temp = "";
			bool blnFieldsSelected;
			bool blnExpenseAccountsExist;
			bool blnRevenueAccountsExist;
			clsDRWrapper rsInfoExists = new clsDRWrapper();
			string strTempAccount = "";
			string strAcctsToShow;
			for (counter = 0; counter <= vsWhere.Rows - 1; counter++)
			{
				if (vsWhere.TextMatrix(counter, 1) == "" && vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 1) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					if ((vsWhere.TextMatrix(0, 1) == "T" && counter != 12) || (vsWhere.TextMatrix(0, 1) == "S" && counter != 14))
					{
						MessageBox.Show("You must fill out all of the selection criteria before you may print this report.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 2) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					// do nothing
				}
				else
				{
					if (vsWhere.TextMatrix(counter, 2) == "")
					{
						MessageBox.Show("You must fill out all the selection criteria before you may print this report.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			if (vsWhere.TextMatrix(2, 1) == "D" || (vsWhere.TextMatrix(0, 1) == "S" && vsWhere.TextMatrix(2, 1) == "F"))
			{
				if (fecherFoundation.Strings.CompareString(vsWhere.TextMatrix(3, 1), vsWhere.TextMatrix(3, 2), true) >= 0)
				{
					if (vsWhere.TextMatrix(0, 1) == "T")
					{
						MessageBox.Show("Your ending department must be greater than your beginning department.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("Your ending fund must be greater than your beginning fund.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					return;
				}
			}
			if (vsWhere.TextMatrix(0, 1) == "T")
			{
				if (Conversion.Val(vsWhere.TextMatrix(14, 1)) == 0)
				{
					vsWhere.TextMatrix(14, 1, "1");
				}
			}
			else
			{
				if (Conversion.Val(vsWhere.TextMatrix(16, 1)) == 0)
				{
					vsWhere.TextMatrix(16, 1, "1");
				}
			}
			blnFieldsSelected = false;
			for (counter = 0; counter <= lstFields.Items.Count - 1; counter++)
			{
				if (lstFields.Selected(counter))
				{
					blnFieldsSelected = true;
				}
			}
			if (!blnFieldsSelected)
			{
				MessageBox.Show("You must select 1 or more fields before you may print this report.", "Select Fields", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (lstFields.Selected(6))
			{
				if (vsWhere.TextMatrix(4, 1) == vsWhere.TextMatrix(5, 1))
				{
					MessageBox.Show("You may not select the current budget if you are budgeting for this year.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					lstFields.SetSelected(6, false);
					return;
				}
			}
			blnExpenseAccountsExist = false;
			blnRevenueAccountsExist = false;
			if (vsWhere.TextMatrix(2, 1) == "A")
			{
				if (vsWhere.TextMatrix(0, 1) == "T")
				{
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnExpenseAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnExpenseAccountsExist = false;
					}
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnRevenueAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnRevenueAccountsExist = false;
					}
				}
				else
				{
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'P'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnExpenseAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnExpenseAccountsExist = false;
					}
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'V'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnRevenueAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnRevenueAccountsExist = false;
					}
				}
			}
			else if ((vsWhere.TextMatrix(0, 1) == "T" && vsWhere.TextMatrix(2, 1) == "D") || (vsWhere.TextMatrix(0, 1) == "S" && vsWhere.TextMatrix(2, 1) == "F"))
			{
				if (vsWhere.TextMatrix(0, 1) == "T")
				{
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' AND FirstAccountField >= '" + vsWhere.TextMatrix(3, 1) + "' AND FirstAccountField <= '" + vsWhere.TextMatrix(3, 2) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnExpenseAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnExpenseAccountsExist = false;
					}
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField >= '" + vsWhere.TextMatrix(3, 1) + "' AND FirstAccountField <= '" + vsWhere.TextMatrix(3, 2) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnRevenueAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnRevenueAccountsExist = false;
					}
				}
				else
				{
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'P' AND FirstAccountField >= '" + vsWhere.TextMatrix(3, 1) + "' AND FirstAccountField <= '" + vsWhere.TextMatrix(3, 2) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnExpenseAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnExpenseAccountsExist = false;
					}
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'V' AND FirstAccountField >= '" + vsWhere.TextMatrix(3, 1) + "' AND FirstAccountField <= '" + vsWhere.TextMatrix(3, 2) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnRevenueAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnRevenueAccountsExist = false;
					}
				}
			}
			else if (vsWhere.TextMatrix(2, 1) == "S")
			{
				if (vsWhere.TextMatrix(0, 1) == "T")
				{
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' AND FirstAccountField = '" + vsWhere.TextMatrix(3, 1) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnExpenseAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnExpenseAccountsExist = false;
					}
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + vsWhere.TextMatrix(3, 1) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnRevenueAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnRevenueAccountsExist = false;
					}
				}
				else
				{
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'P' AND FirstAccountField = '" + vsWhere.TextMatrix(3, 1) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnExpenseAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnExpenseAccountsExist = false;
					}
					rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'V' AND FirstAccountField = '" + vsWhere.TextMatrix(3, 1) + "'");
					if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
					{
						do
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								blnRevenueAccountsExist = true;
								break;
							}
							rsInfoExists.MoveNext();
						}
						while (rsInfoExists.EndOfFile() != true);
					}
					else
					{
						blnRevenueAccountsExist = false;
					}
				}
			}
			else
			{
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' AND FirstAccountField IN (SELECT DISTINCT Department FROM DeptDivTitles WHERE Fund = '" + vsWhere.TextMatrix(3, 1) + "')");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					do
					{
						strTempAccount = modBudgetaryMaster.CreateAccount_242("E", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
						strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
						if (BudgetAccountExists(strTempAccount))
						{
							blnExpenseAccountsExist = true;
							break;
						}
						rsInfoExists.MoveNext();
					}
					while (rsInfoExists.EndOfFile() != true);
				}
				else
				{
					blnExpenseAccountsExist = false;
				}
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField IN (SELECT DISTINCT Department FROM DeptDivTitles WHERE Fund = '" + vsWhere.TextMatrix(3, 1) + "')");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					do
					{
						strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsInfoExists.Get_Fields_String("FirstAccountField"), rsInfoExists.Get_Fields_String("SecondAccountField"), rsInfoExists.Get_Fields_String("ThirdAccountField"), rsInfoExists.Get_Fields_String("FourthAccountField"));
						strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
						if (BudgetAccountExists(strTempAccount))
						{
							blnRevenueAccountsExist = true;
							break;
						}
						rsInfoExists.MoveNext();
					}
					while (rsInfoExists.EndOfFile() != true);
				}
				else
				{
					blnRevenueAccountsExist = false;
				}
			}
			strAcctsToShow = vsWhere.TextMatrix(1, 1);
			if (strAcctsToShow == "E")
			{
				if (blnExpenseAccountsExist)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else if (strAcctsToShow == "R")
			{
				if (blnRevenueAccountsExist)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else
			{
				if (blnRevenueAccountsExist || blnExpenseAccountsExist)
				{
					if (strAcctsToShow == "B")
					{
						if (blnRevenueAccountsExist == false)
						{
							strAcctsToShow = "E";
						}
						else if (blnExpenseAccountsExist == false)
						{
							strAcctsToShow = "R";
						}
					}
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (vsWhere.TextMatrix(0, 1) == "T")
			{
				if (strAcctsToShow == "E" || strAcctsToShow == "B")
				{
					if (blnExpenseAccountsExist)
					{
						if (vsWhere.TextMatrix(2, 1) == "A")
						{
							temp = "A";
							rptCustomBudgetExpenseReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1));
						}
						else if (vsWhere.TextMatrix(2, 1) == "D")
						{
							temp = "R";
							rptCustomBudgetExpenseReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1), vsWhere.TextMatrix(3, 2));
						}
						else if (vsWhere.TextMatrix(2, 1) == "S")
						{
							temp = "D";
							rptCustomBudgetExpenseReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1));
						}
						else
						{
							temp = "F";
							rptCustomBudgetExpenseReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1));
						}
					}
					else
					{
						if (vsWhere.TextMatrix(2, 1) == "A")
						{
							temp = "A";
							rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), "R", vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1));
						}
						else if (vsWhere.TextMatrix(2, 1) == "D")
						{
							temp = "R";
							rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), "R", vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1), vsWhere.TextMatrix(3, 2));
						}
						else if (vsWhere.TextMatrix(2, 1) == "S")
						{
							temp = "D";
							rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), "R", vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1));
						}
						else
						{
							temp = "F";
							rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), "R", vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1));
						}
					}
				}
				else
				{
					if (vsWhere.TextMatrix(2, 1) == "A")
					{
						temp = "A";
						rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1));
					}
					else if (vsWhere.TextMatrix(2, 1) == "D")
					{
						temp = "R";
						rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1), vsWhere.TextMatrix(3, 2));
					}
					else if (vsWhere.TextMatrix(2, 1) == "S")
					{
						temp = "D";
						rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1));
					}
					else
					{
						temp = "F";
						rptCustomBudgetRevenueReport.InstancePtr.Init(vsWhere.TextMatrix(13, 1) == "Y", vsWhere.TextMatrix(12, 1), FCConvert.ToInt32(vsWhere.TextMatrix(5, 1)), FCConvert.ToInt32(vsWhere.TextMatrix(4, 1)), strAcctsToShow, vsWhere.TextMatrix(11, 1), vsWhere.TextMatrix(8, 1) == "Y", vsWhere.TextMatrix(9, 1) == "Y", vsWhere.TextMatrix(10, 1) == "Y", vsWhere.TextMatrix(6, 1) == "Y", vsWhere.TextMatrix(7, 1) == "Y", temp, FCConvert.ToInt16(vsWhere.TextMatrix(14, 1)), vsWhere.TextMatrix(15, 1) == "Y", vsWhere.TextMatrix(16, 1), vsWhere.TextMatrix(17, 1), vsWhere.TextMatrix(3, 1));
					}
				}
				// 
			}
			//Application.DoEvents();
			if (!blnPrint)
			{
				//FC:FINAL:MSH - Issue #620: 'Close' replaced by 'Hide' method, because after using 'Close' all items in 'lstFields' are deleted and can't be used in rptCustomBudgetRevenueReport.
				//this.Close();
				this.Hide();
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			int intRow;
			int intCol;
			string[] strSelectedFields = new string[500 + 1];
			vsWhere.Select(0, 0);
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			// GET THE FIELD NAMES THAT THE USER HAS SELECTED
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				strSelectedFields[intCounter] = string.Empty;
			}
			for (intRow = 1; intRow <= vsLayout.Rows - 1; intRow++)
			{
				for (intCol = 0; intCol <= vsLayout.Cols - 1; intCol++)
				{
					for (intCounter = 0; intCounter <= 499; intCounter++)
					{
						if (strSelectedFields[intCounter] == string.Empty)
						{
							modCustomReport.Statics.intNumberOfSQLFields += 1;
							strSelectedFields[intCounter] = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							break;
						}
						else if (strSelectedFields[intCounter] == vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol))
						{
							goto NextCell;
						}
					}
					NextCell:
					;
				}
			}
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				if (strSelectedFields[intCounter] == string.Empty)
				{
					goto Continue;
				}
				else
				{
					for (intRow = 0; intRow <= lstFields.Items.Count - 1; intRow++)
					{
						if (lstFields.Items[intRow].Text == strSelectedFields[intCounter])
						{
							if (modCustomReport.Statics.strCustomSQL != string.Empty)
								modCustomReport.Statics.strCustomSQL += ", ";
							modCustomReport.Statics.strCustomSQL += modCustomReport.Statics.strFields[lstFields.ItemData(intRow)];
							break;
						}
					}
				}
			}
			Continue:
			;
			// CHECK TO SEE IF ANY FIELDS WERE SELCTED. IF THEY WERE THEN THE
			// STRCUSTOMSQL WOULD HAVE SOME DATA IN IT
			if (Strings.Trim(modCustomReport.Statics.strCustomSQL) == string.Empty)
			{
				modCustomReport.Statics.intNumberOfSQLFields = -1;
				return;
			}
			// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
			modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
			BuildWhereParameter();
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			BuildSortParameter();
		}

		public void BuildSortParameter()
		{
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					strSort += FCConvert.ToString(modCustomReport.CheckForAS_6(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1));
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			if (Strings.Trim(strSort) != string.Empty)
			{
				modCustomReport.Statics.strCustomSQL += " Order by" + strSort;
			}
		}

		public void BuildWhereParameter()
		{
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			// CLEAR THE VARIABLES
			strWhere = " ";
			// GET THE FIELDS TO FILTER BY
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
				{
					case modCustomReport.GRIDTEXT:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
							// THE POUND SYMBOL WRAPPED AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
									// FIELD SO WE NOW HAVE A DATE RANGE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and " + Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
									// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "'";
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
							// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 3) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " = " + vsWhere.TextMatrix(intCounter, 3);
							}
							break;
						}
					case modCustomReport.GRIDCOMBOTEXT:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 1) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + " AND " + modCustomReport.Statics.strFields[intCounter] + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1);
								}
							}
							break;
						}
					case modCustomReport.GRIDTEXTRANGE:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + modCustomReport.Statics.strFields[intCounter] + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "%'";
								}
							}
							break;
						}
				}
				//end switch
			}
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS
			// TO THE SQL STATEMENT
			if (strWhere != " ")
			{
				modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere;
			}
		}

		private void frmCustomBudgetReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomBudgetReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCustomBudgetReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomBudgetReport.ScaleWidth	= 8955;
			//frmCustomBudgetReport.ScaleHeight	= 7395;
			//frmCustomBudgetReport.LinkTopic	= "Form1";
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			int counter;
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION
			Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			//Image1.Width = 1440 * 14;
			Image1.Image = ImageList1.Images[0];
			//HScroll1.Minimum = 0;
			//if (Image1.Width > cmbPortrait.Width)
			//{
			//    HScroll1.Maximum = Image1.Width - cmbPortrait.Width;
			//}
			//else
			//{
			//    HScroll1.Enabled = false;
			//}
			//Line1.X1 = Image1.Left + (1440 * 8) - 900;
			//Line1.X2 = Image1.Left + (1440 * 8) - 900;
			//Line1.Y2 = vsLayout.Top + vsLayout.Height;
			intChoices = 0;
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = 720 + (-HScroll1.Value + 30);
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = 720 + (-HScroll1.Value + 30);
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("Select * from CustomBudgetReports ORDER BY Description", modGlobal.DEFAULTDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("Description"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}
		//private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		private void lstFields_SelectedIndexChanged(object sender, ItemCheckedEventArgs e)
		{
			int counter;
			int intCommentRow = 0;
			intChoices = 0;
			for (counter = 0; counter <= lstFields.Items.Count - 1; counter++)
			{
				if (lstFields.Selected(counter))
				{
					intChoices += 1;
				}
			}
			if (vsWhere.TextMatrix(0, 1) == "T")
			{
				intCommentRow = 11;
			}
			else
			{
				intCommentRow = 13;
			}
			if (intChoices > 8 || (intChoices > 5 && vsWhere.TextMatrix(intCommentRow, 1) == "S"))
			{
				lstFields.SetSelected(lstFields.ListIndex, false);
			}
			// If lstFields.ListIndex < 0 Then Exit Sub
			// If vsLayout.Row < 1 Then vsLayout.Row = 1
			// 
			// vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col) = lstFields.List(lstFields.ListIndex)
			// vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col) = lstFields.ItemData(lstFields.ListIndex)
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		ListViewItem temp = lstSort.Items[lstSort.SelectedIndex];
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart] = temp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		lstSort.SetSelected(lstSort.ListIndex, true);
		//		lstSort.SetSelected(intStart, true);
		//	}
		//}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			lstFields.Focus();
			blnPrint = true;
			cmdPrint_Click();
		}

		private void mnuPreview_Click(object sender, System.EventArgs e)
		{
			lstFields.Focus();
			blnPrint = false;
			cmdPrint_Click();
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			else
			{
				cboSavedReport.Visible = false;
			}
			cmdAdd.Visible = Index == 0;
			// Dave 03/04/05
			// fraSort.Enabled = Index = 0
			// fraFields.Enabled = Index = 0
			// fraWhere.Enabled = Index = 0
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
					{
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

		private void vsWhere_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsWhere.IsCurrentCellInEditMode)
			{
				if (vsWhere.Row == 0)
				{
					if (vsWhere.EditText == "T")
					{
						ShowTownOptions();
					}
				}
				else if (vsWhere.Row == 2)
				{
					if (vsWhere.EditText == "A")
					{
						if (vsWhere.TextMatrix(0, 1) == "T")
						{
							vsWhere.TextMatrix(3, 0, "Department(s)");
							modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
						}
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else if (vsWhere.EditText == "D" || (vsWhere.TextMatrix(0, 1) == "S" && vsWhere.EditText == "F"))
					{
						if (vsWhere.TextMatrix(0, 1) == "T")
						{
							vsWhere.TextMatrix(3, 0, "Department(s)");
							modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
						}
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, Color.White);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else if (vsWhere.EditText == "S")
					{
						if (vsWhere.TextMatrix(0, 1) == "T")
						{
							vsWhere.TextMatrix(3, 0, "Department(s)");
							modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
						}
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else
					{
						vsWhere.TextMatrix(3, 0, "Fund");
						modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund", "ShortDescription", "Fund", true);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
				}
			}
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
			{
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
			if (vsWhere.Col == 2)
			{
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, 1) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsWhere.Select(4, 1);
					}
					else
					{
						vsWhere.Col = 1;
					}
				}
			}
			else if (vsWhere.Col == 1)
			{
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsWhere.Select(4, 1);
				}
				else
				{
					vsWhere.Col = 1;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			int intCommentRow = 0;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(row, col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!modCustomReport.IsValidDate(vsWhere.EditText))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
						break;
					}
			}
			//end switch
			if (vsWhere.TextMatrix(0, 1) == "T")
			{
				intCommentRow = 11;
			}
			else
			{
				intCommentRow = 13;
			}
			if (row == intCommentRow)
			{
				if (vsWhere.EditText == "S")
				{
					if (intChoices > 5)
					{
						MessageBox.Show("You may only select 5 items to show if you wish to use this option.  Please remove some items before selecting this.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
					}
				}
			}
		}

		private void SetCustomFormColors()
		{
			Label3.ForeColor = ColorTranslator.FromOle(0xC00000);
			Label2.ForeColor = ColorTranslator.FromOle(0xC00000);
			Label1.ForeColor = ColorTranslator.FromOle(0xC00000);
		}

		public void ShowTownOptions()
		{
			int intCounter;
			vsWhere.Rows = 2;
			modCustomReport.Statics.strWhereType[0] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[1] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[2] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[3] = FCConvert.ToString(modCustomReport.GRIDCOMBOTEXT);
			modCustomReport.Statics.strWhereType[4] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[5] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[6] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[7] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[8] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[9] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[10] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[11] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[12] = FCConvert.ToString(modCustomReport.GRIDTEXT);
			modCustomReport.Statics.strWhereType[13] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[14] = FCConvert.ToString(modCustomReport.GRIDTEXT);
			modCustomReport.Statics.strWhereType[15] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[16] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.Statics.strWhereType[17] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
			modCustomReport.LoadGridCellAsCombo(this, 1, "#0;B" + "\t" + "Both|#1;E" + "\t" + "Expense|#2;R" + "\t" + "Revenue");
			modCustomReport.LoadGridCellAsCombo(this, 2, "#0;A" + "\t" + "All|#1;D" + "\t" + "Department Range|#2;S" + "\t" + "Single Department|#3;F" + "\t" + "Single Fund");
			modCustomReport.LoadGridCellAsCombo_1701(this, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department", "ShortDescription", "Department", true);
			strTemp = "";
			for (intCounter = DateTime.Today.Year - 4; intCounter <= DateTime.Today.Year + 4; intCounter++)
			{
				strTemp += FCConvert.ToString(intCounter) + "|";
			}
			strTemp = Strings.Left(strTemp, strTemp.Length - 1);
			modCustomReport.LoadGridCellAsCombo(this, 4, strTemp);
			modCustomReport.LoadGridCellAsCombo(this, 5, strTemp);
			modCustomReport.LoadGridCellAsCombo(this, 6, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 7, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 8, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 9, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 10, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 11, "#0;B" + "\t" + "Print Below|#1;S" + "\t" + "Print on Same Line|#2;P" + "\t" + "Print Below Across Entire Page|#3;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 13, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			modCustomReport.LoadGridCellAsCombo(this, 15, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
			{
				modCustomReport.LoadGridCellAsCombo(this, 16, "#0;De" + "\t" + "Department|#1;Ex" + "\t" + "Expense");
			}
			else if (modAccountTitle.Statics.ExpDivFlag)
			{
				modCustomReport.LoadGridCellAsCombo(this, 16, "#0;De" + "\t" + "Department|#1;Ex" + "\t" + "Expense|#2;Ob" + "\t" + "Object");
			}
			else if (modAccountTitle.Statics.ObjFlag)
			{
				modCustomReport.LoadGridCellAsCombo(this, 16, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Ex" + "\t" + "Expense");
			}
			else
			{
				modCustomReport.LoadGridCellAsCombo(this, 16, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Ex" + "\t" + "Expense|#3;Ob" + "\t" + "Object");
			}
			if (modAccountTitle.Statics.RevDivFlag)
			{
				modCustomReport.LoadGridCellAsCombo(this, 17, "#0;De" + "\t" + "Department|#1;Re" + "\t" + "Revenue");
			}
			else
			{
				modCustomReport.LoadGridCellAsCombo(this, 17, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Re" + "\t" + "Revenue");
			}
			vsWhere.AddItem("Reporting Range");
			vsWhere.AddItem("Department(s)");
			vsWhere.AddItem("Current Year");
			vsWhere.AddItem("Budget Year");
			vsWhere.AddItem("Dept Page Break");
			vsWhere.AddItem("Div Page Break");
			vsWhere.AddItem("Department Totals");
			vsWhere.AddItem("Division Totals");
			vsWhere.AddItem("Expense Totals");
			vsWhere.AddItem("Comments");
			vsWhere.AddItem("Report Title");
			vsWhere.AddItem("Include Budget Adjustments");
			vsWhere.AddItem("Starting Page");
			vsWhere.AddItem("Include Carry Forwards");
			vsWhere.AddItem("Exp Lowest Detail Level");
			vsWhere.AddItem("Rev Lowest Detail Level");
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
				// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
				// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
				if (intCounter == 3)
				{
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[intCounter]) != modCustomReport.GRIDDATE && FCConvert.ToDouble(modCustomReport.Statics.strWhereType[intCounter]) != modCustomReport.GRIDNUMRANGE && FCConvert.ToDouble(modCustomReport.Statics.strWhereType[intCounter]) != modCustomReport.GRIDTEXTRANGE)
				{
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			vsWhere.TextMatrix(1, 1, "B");
			vsWhere.TextMatrix(2, 1, "A");
			vsWhere.TextMatrix(4, 1, FCConvert.ToString(FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy")).Year));
			vsWhere.TextMatrix(5, 1, FCConvert.ToString(FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy")).Year + 1));
			vsWhere.TextMatrix(6, 1, "Y");
			vsWhere.TextMatrix(7, 1, "Y");
			vsWhere.TextMatrix(8, 1, "Y");
			vsWhere.TextMatrix(9, 1, "Y");
			vsWhere.TextMatrix(10, 1, "Y");
			vsWhere.TextMatrix(11, 1, "B");
			vsWhere.TextMatrix(13, 1, "Y");
			vsWhere.TextMatrix(14, 1, "1");
			vsWhere.TextMatrix(15, 1, "Y");
			if (modAccountTitle.Statics.ObjFlag)
			{
				vsWhere.TextMatrix(16, 1, "Ex");
			}
			else
			{
				vsWhere.TextMatrix(16, 1, "Ob");
			}
			vsWhere.TextMatrix(17, 1, "Re");

			//FC:FINAL:DDU:#2980 - set columns width
			vsWhere.ColWidth(0, FCConvert.ToInt32(vsWhere.WidthOriginal * 0.5));
			vsWhere.ColWidth(1, FCConvert.ToInt32(vsWhere.WidthOriginal * 0.23));
			vsWhere.ColWidth(2, FCConvert.ToInt32(vsWhere.WidthOriginal * 0.23));
		}

		private bool BudgetAccountExists(string strAccount)
		{
			bool BudgetAccountExists = false;
			// check to see this is an account a budget is getting created for
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + strAccount + "'");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				BudgetAccountExists = true;
			}
			else
			{
				BudgetAccountExists = false;
			}
			return BudgetAccountExists;
		}
	}
}
