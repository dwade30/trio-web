﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreditMemoCorrections.
	/// </summary>
	public partial class frmCreditMemoCorrections : BaseForm
	{
		public frmCreditMemoCorrections()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreditMemoCorrections InstancePtr
		{
			get
			{
				return (frmCreditMemoCorrections)Sys.GetInstance(typeof(frmCreditMemoCorrections));
			}
		}

		protected frmCreditMemoCorrections _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int VendorCol;
		int NameCol;
		int DescriptionCol;
		int AmountCol;
		int ReferenceCol;
		float[] ColPercents = new float[6 + 1];
		float[] vs2ColPercents = new float[5 + 1];
		int AccountCol;
		int NumberCol;
		int DetailCol;
		int DetailAmountCol;
		int ProjectCol;
		int PayableCol;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs_AutoInitialized;

		clsDRWrapper rs
		{
			get
			{
				if (rs_AutoInitialized == null)
				{
					rs_AutoInitialized = new clsDRWrapper();
				}
				return rs_AutoInitialized;
			}
			set
			{
				rs_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsDetail = new clsDRWrapper();
		clsDRWrapper rsDetail_AutoInitialized;

		clsDRWrapper rsDetail
		{
			get
			{
				if (rsDetail_AutoInitialized == null)
				{
					rsDetail_AutoInitialized = new clsDRWrapper();
				}
				return rsDetail_AutoInitialized;
			}
			set
			{
				rsDetail_AutoInitialized = value;
			}
		}

		bool BadAmount;
		bool EditFlag;
		int JournalNumber;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper Master = new clsDRWrapper();
		clsDRWrapper Master_AutoInitialized;

		clsDRWrapper Master
		{
			get
			{
				if (Master_AutoInitialized == null)
				{
					Master_AutoInitialized = new clsDRWrapper();
				}
				return Master_AutoInitialized;
			}
			set
			{
				Master_AutoInitialized = value;
			}
		}

		int[] PeriodData = null;
		bool blnJournalLocked;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsOldGrid = new clsGridAccount();
		private clsGridAccount vsOldGrid_AutoInitialized;

		private clsGridAccount vsOldGrid
		{
			get
			{
				if (vsOldGrid_AutoInitialized == null)
				{
					vsOldGrid_AutoInitialized = new clsGridAccount();
				}
				return vsOldGrid_AutoInitialized;
			}
			set
			{
				vsOldGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsNewGrid = new clsGridAccount();
		private clsGridAccount vsNewGrid_AutoInitialized;

		private clsGridAccount vsNewGrid
		{
			get
			{
				if (vsNewGrid_AutoInitialized == null)
				{
					vsNewGrid_AutoInitialized = new clsGridAccount();
				}
				return vsNewGrid_AutoInitialized;
			}
			set
			{
				vsNewGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsBudgetaryPosting clsPostInfo_AutoInitialized;

		clsBudgetaryPosting clsPostInfo
		{
			get
			{
				if (clsPostInfo_AutoInitialized == null)
				{
					clsPostInfo_AutoInitialized = new clsBudgetaryPosting();
				}
				return clsPostInfo_AutoInitialized;
			}
			set
			{
				clsPostInfo_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		clsPostingJournalInfo clsJournalInfo_AutoInitialized;

		clsPostingJournalInfo clsJournalInfo
		{
			get
			{
				if (clsJournalInfo_AutoInitialized == null)
				{
					clsJournalInfo_AutoInitialized = new clsPostingJournalInfo();
				}
				return clsJournalInfo_AutoInitialized;
			}
			set
			{
				clsJournalInfo_AutoInitialized = value;
			}
		}

		private void cmdAccount_Click(object sender, System.EventArgs e)
		{
			lblOldValue.Text = "Old Account";
			lblNewValue.Text = "New Account";
			vsOldValue.TextMatrix(0, 0, vs2.TextMatrix(vs2.Row, AccountCol));
			vsNewAccount.Visible = true;
            vsNewAccount.BringToFront();
			txtNewValue.Visible = false;
			vsNewAccount.TextMatrix(0, 0, "");
			fraChange.Visible = true;
			vsNewAccount.Focus();
			fraUpdate.Visible = false;
		}

		private void cmdAmount_Click(object sender, System.EventArgs e)
		{
			lblOldValue.Text = "Old Amount";
			lblNewValue.Text = "New Amount";
			vsOldValue.TextMatrix(0, 0, vs2.TextMatrix(vs2.Row, DetailAmountCol));
			vsNewAccount.Visible = false;
			txtNewValue.Visible = true;
            txtNewValue.BringToFront();
			txtNewValue.Text = "";
			fraChange.Visible = true;
			txtNewValue.Focus();
			fraUpdate.Visible = false;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraUpdate.Visible = true;
			fraUpdate.CenterToParent();
			fraChange.Visible = false;
			cmdDeleteUpdate.Focus();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdDeleteUpdate_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: total As double	OnWrite(string)
			double total = 0;
			string AcctType = "";
			string AcctFund = "";
			string AcctDept = "";
			if (vs1.TextMatrix(vs1.Row, PayableCol) != "")
			{
				if (FCConvert.ToDecimal(vs2.TextMatrix(vs2.Row, DetailAmountCol)) > FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol)) - FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, PayableCol)))
				{
					MessageBox.Show("You may not delete this Credit Memo item because the amount is more then is remaining in the credit memo.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
			}
			answer = MessageBox.Show("Do you really want to Delete this line?", "Delete Account?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if (answer == DialogResult.Cancel)
			{
				cmdReturn_Click();
			}
			else if (answer == DialogResult.No)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					JournalNumber = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", JournalNumber);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "CM Adjust for " + Strings.Format(DateTime.Today, "MM/dd/yy"));
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("CreditMemoCorrection", true);
				Master.Set_Fields("Period", cboPeriod.Text);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalType = "AC";
				clsJournalInfo.JournalNumber = JournalNumber;
				clsJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt16(FCConvert.ToDouble(cboPeriod.Text)));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
				}
				rs.AddNew();
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("CheckDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("Description", vs1.TextMatrix(vs1.Row, DescriptionCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Set_Fields("Reference", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol))));
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("CreditMemoCorrection", true);
				rs.Set_Fields("CreditMemoRecord", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				rs.Update();
				rsDetail.OmitNullsOnInsert = true;
				rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
				rsDetail.AddNew();
				rsDetail.Set_Fields("APJournalID", rs.Get_Fields_Int32("ID"));
				rsDetail.Set_Fields("1099", "D");
				rsDetail.Set_Fields("account", vs2.TextMatrix(vs2.Row, AccountCol));
				rsDetail.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol))));
				rsDetail.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rsDetail.Update();
				modBudgetaryMaster.Statics.CurrentAPEntry = JournalNumber;
				modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - FCConvert.ToDecimal(vs2.TextMatrix(vs2.Row, DetailAmountCol)));
					rs.Update();
				}
				total = FCConvert.ToDouble(vs2.TextMatrix(vs2.Row, DetailAmountCol));
				vs2.RemoveItem(vs2.Row);
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", Conversion.Val(rs.Get_Fields_Decimal("Adjustments")) - total);
					rs.Update();
				}
				if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, AmountCol)) - total != 0)
				{
					vs1.TextMatrix(vs1.Row, AmountCol, Conversion.Val(vs1.TextMatrix(vs1.Row, AmountCol)) - total);
				}
				else
				{
					vs1.RemoveItem(vs1.Row);
				}
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(modBudgetaryMaster.Statics.CurrentAPEntry));
				vs1_Collapsed();
				vs2_Collapsed();
				cmdReturn_Click();
				// Dave 5/7/03
				vs1_RowColChange(vs1, EventArgs.Empty);
				if (!clsPostInfo.PostJournals())
				{
					MessageBox.Show("There was a problem posting journal " + FCConvert.ToString(JournalNumber) + ".  Please correct the issues and post the journal as soon as possible.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			fraUpdate.Visible = false;
			vs1.Enabled = true;
			vs2.Enabled = true;
			vs2.BackColorSel = SystemColors.Info;
			vs2.Focus();
		}

		public void cmdReturn_Click()
		{
			cmdReturn_Click(cmdReturn, new System.EventArgs());
		}

		private void cmdUpdate_Click(object sender, System.EventArgs e)
		{
			double total;
			clsDRWrapper rs2 = new clsDRWrapper();
			clsDRWrapper rs3 = new clsDRWrapper();
			int counter;
			double AmountChange = 0;
			string AcctType = "";
			string AcctFund = "";
			string AcctDept = "";
			string AcctType2 = "";
			string AcctFund2 = "";
			string AcctDept2 = "";
			int lngEncumbranceDetail = 0;
			int lngOldEncumbranceDetail;
			if (txtNewValue.Visible == true)
			{
				if (vs2.TextMatrix(vs2.Row, DetailAmountCol) != "")
				{
					if (Conversion.Val(vsOldValue.TextMatrix(0, 0)) - Conversion.Val(txtNewValue.Text) > Conversion.Val(vs1.TextMatrix(vs1.Row, AmountCol)) - Conversion.Val(vs1.TextMatrix(vs1.Row, PayableCol)))
					{
						MessageBox.Show("You cannot change this amount to less then what is curently left in the credit memo", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					JournalNumber = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", JournalNumber);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "CM Adjust for " + Strings.Format(DateTime.Today, "MM/dd/yy"));
				Master.Set_Fields("Period", cboPeriod.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("CreditMemoCorrection", true);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalType = "AC";
				clsJournalInfo.JournalNumber = JournalNumber;
				clsJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt16(FCConvert.ToDouble(cboPeriod.Text)));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				AmountChange = Conversion.Val(txtNewValue.Text) - Conversion.Val(vsOldValue.TextMatrix(0, 0));
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
				}
				rs.AddNew();
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("CheckDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("Description", vs1.TextMatrix(vs1.Row, DescriptionCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Set_Fields("Reference", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Amount", AmountChange * -1);
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("CreditMemoCorrection", true);
				rs.Set_Fields("CreditMemoRecord", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				rs.Update();
				rsDetail.OmitNullsOnInsert = true;
				rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
				rsDetail.AddNew();
				rsDetail.Set_Fields("APJournalID", rs.Get_Fields_Int32("ID"));
				rsDetail.Set_Fields("1099", "D");
				rsDetail.Set_Fields("account", vs2.TextMatrix(vs2.Row, AccountCol));
				rsDetail.Set_Fields("Amount", AmountChange * -1);
				rsDetail.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rsDetail.Update();
				modBudgetaryMaster.Statics.CurrentAPEntry = JournalNumber;
				modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", Conversion.Val(rs.Get_Fields_Decimal("Adjustments")) + AmountChange);
					rs.Update();
				}
				vs2.TextMatrix(vs2.Row, DetailAmountCol, txtNewValue.Text);
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", Conversion.Val(rs.Get_Fields_Decimal("Adjustments")) + AmountChange);
					rs.Update();
				}
				vs1.TextMatrix(vs1.Row, AmountCol, Conversion.Val(vs1.TextMatrix(vs1.Row, AmountCol)) + AmountChange);
				;
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(modBudgetaryMaster.Statics.CurrentAPEntry));
			}
			else
			{
				if (vsNewAccount.Visible == true)
				{
					for (counter = 1; counter <= vsNewAccount.TextMatrix(0, 0).Length; counter++)
					{
						if (Strings.Mid(vsNewAccount.TextMatrix(0, 0), counter, 1) == "_")
						{
							MessageBox.Show("You must enter a full account number before you may proceed", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsNewAccount.Focus();
							return;
						}
					}
					if (modBudgetaryAccounting.GetFundFromAccount(vsNewAccount.TextMatrix(0, 0)) != modBudgetaryAccounting.GetFundFromAccount(vsOldValue.TextMatrix(0, 0)))
					{
						MessageBox.Show("You may not change the fund on an existing credit memo.  If you wish to do this you must delete this credit memo and create a new one.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					JournalNumber = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", JournalNumber);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "CM Adjust for " + Strings.Format(DateTime.Today, "MM/dd/yy"));
				Master.Set_Fields("Period", cboPeriod.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("CreditMemoCorrection", true);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalType = "AC";
				clsJournalInfo.JournalNumber = JournalNumber;
				clsJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt16(FCConvert.ToDouble(cboPeriod.Text)));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
				}
				rs.AddNew();
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("CheckDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("Description", vs1.TextMatrix(vs1.Row, DescriptionCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Set_Fields("Reference", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Amount", 0);
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("CreditMemoCorrection", true);
				rs.Set_Fields("CreditMemoRecord", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				rs.Update();
				rsDetail.OmitNullsOnInsert = true;
				rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
				rsDetail.AddNew();
				rsDetail.Set_Fields("APJournalID", rs.Get_Fields_Int32("ID"));
				rsDetail.Set_Fields("1099", "D");
				rsDetail.Set_Fields("account", vs2.TextMatrix(vs2.Row, AccountCol));
				rsDetail.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol))));
				rsDetail.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rsDetail.Update();
				rsDetail.AddNew();
				rsDetail.Set_Fields("APJournalID", rs.Get_Fields_Int32("ID"));
				rsDetail.Set_Fields("1099", "D");
				rsDetail.Set_Fields("account", vsNewAccount.TextMatrix(0, 0));
				rsDetail.Set_Fields("Amount", Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol)) * -1);
				rsDetail.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rsDetail.Update();
				// Dave 5/7/03
				rs2.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs2.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE ID = 0");
					rs2.AddNew();
					rs2.Set_Fields("account", vsNewAccount.TextMatrix(0, 0));
					rs2.Set_Fields("Adjustments", vs2.TextMatrix(vs2.Row, DetailAmountCol));
					rs2.Set_Fields("Amount", 0);
					rs2.Set_Fields("CreditMemoID", rs.Get_Fields_Int32("VendorNumber"));
					// rs2.Fields["Description"] = rs.Fields["Description"]
					rs2.Set_Fields("Project", rs.Get_Fields_String("Project"));
					rs.Edit();
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - rs.Get_Fields("Amount"));
					lngOldEncumbranceDetail = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.Update();
				}
				vs2.TextMatrix(vs2.Row, AccountCol, vsNewAccount.TextMatrix(0, 0));
				// Dave 5/7/03
				rs2.Update();
				lngEncumbranceDetail = FCConvert.ToInt32(rs2.Get_Fields_Int32("ID"));
				vs2.TextMatrix(vs2.Row, 0, FCConvert.ToString(lngEncumbranceDetail));
				modBudgetaryMaster.Statics.CurrentAPEntry = JournalNumber;
				modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(modBudgetaryMaster.Statics.CurrentAPEntry));
			}
			fraChange.Visible = false;
			vs1.Enabled = true;
			vs2.Enabled = true;
			vs2.BackColorSel = SystemColors.Info;
			vs2.Focus();
			if (!clsPostInfo.PostJournals())
			{
				MessageBox.Show("There was a problem posting journal " + FCConvert.ToString(JournalNumber) + ".  Please correct the issues and post the journal as soon as possible.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdUpdate_Click()
		{
			cmdUpdate_Click(cmdUpdate, new System.EventArgs());
		}

		private void frmCreditMemoCorrections_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1_RowColChange(vs1, EventArgs.Empty);
		}

		private void frmCreditMemoCorrections_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSOLDVALUE")
			{
				modNewAccountBox.CheckFormKeyDown(vsOldValue, vsOldValue.Row, vsOldValue.Col, KeyCode, Shift, vsOldValue.EditSelStart, vsOldValue.EditText, vsOldValue.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSNEWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsNewAccount, vsNewAccount.Row, vsNewAccount.Col, KeyCode, Shift, vsNewAccount.EditSelStart, vsNewAccount.EditText, vsNewAccount.EditSelLength);
			}
		}

		private void frmCreditMemoCorrections_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreditMemoCorrections.FillStyle	= 0;
			//frmCreditMemoCorrections.ScaleWidth	= 9045;
			//frmCreditMemoCorrections.ScaleHeight	= 7590;
			//frmCreditMemoCorrections.LinkTopic	= "Form2";
			//frmCreditMemoCorrections.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int CurrJournal;
			int counter;
			double temp = 0;
			double temp2 = 0;
			EditFlag = true;
			blnJournalLocked = false;
			JournalNumber = 0;
			NumberCol = 0;
			DetailCol = 1;
			AccountCol = 2;
			ProjectCol = 3;
			DetailAmountCol = 4;
			VendorCol = 1;
			NameCol = 2;
			DescriptionCol = 3;
			ReferenceCol = 4;
			AmountCol = 5;
			PayableCol = 6;
			vs1.TextMatrix(0, VendorCol, "Vdr #");
			vs1.TextMatrix(0, NameCol, "Vendor Name");
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, ReferenceCol, "Memo #");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, PayableCol, "Pending A / P");
			vs2.TextMatrix(0, DetailCol, "Description");
			vs2.TextMatrix(0, AccountCol, "Account");
			vs2.TextMatrix(0, DetailAmountCol, "Amount");
			vs2.TextMatrix(0, ProjectCol, "Project");
			//FC:FINAL:DDU:#2899 - aligned columns
			vs1.ColAlignment(VendorCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(NameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(PayableCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs2.ColAlignment(DetailCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs2.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs2.ColAlignment(DetailAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs2.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ForeColorSel = Color.Black;
			vs2.ForeColorSel = Color.Black;
			ColPercents[0] = 0;
			ColPercents[1] = 0.07057221f;
			ColPercents[2] = 0.3002835f;
			ColPercents[3] = 0.2152247f;
			ColPercents[4] = 0.1356311f;
			ColPercents[5] = 0.1288771f;
			ColPercents[6] = 0.1288771f;
			vs2ColPercents[0] = 0;
			vs2ColPercents[1] = 0.3391759f;
			vs2ColPercents[2] = 0.3082659f;
			vs2ColPercents[3] = 0.10494805f;
			vs2ColPercents[4] = 0.2191473f;
			vsOldGrid.GRID7Light = vsOldValue;
			vsOldGrid.DefaultAccountType = "E";
			vsOldGrid.AccountCol = -1;
			vsNewGrid.GRID7Light = vsNewAccount;
			vsNewGrid.DefaultAccountType = "E";
			vsNewGrid.AccountCol = -1;
            //FC:FINAL:AM:#2928 - extend the columns
            vsOldValue.ExtendLastCol = true;
            vsNewAccount.ExtendLastCol = true;
            //FC:FINAL:AM:#2928 - hide the columns here not in resize
            vs1.ColHidden(0, true);
            vs2.ColHidden(0, true);
			counter = 0;
			modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'P'");
			if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveLast();
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				PeriodData = new int[modBudgetaryAccounting.Statics.SearchResults.RecordCount() + 1];
				vs1.ColFormat(AmountCol, "#,##0.00");
				vs1.ColFormat(PayableCol, "#,##0.00");
				vs2.ColFormat(DetailAmountCol, "#,##0.00");
				while (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
				{
					if (!(modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Adjustments")))
					{
						temp = Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Adjustments"));
					}
					else
					{
						temp = 0;
					}
					if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Liquidated"))
					{
						temp2 = Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Liquidated"));
					}
					else
					{
						temp2 = 0;
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount")) + temp - temp2 != 0)
					{
						vs1.Rows += 1;
						//FC:FINAL:DSE #i610 Set current row too
						vs1.Row = 1;
						//FC:FINAL:BBE:#528 - remove colors
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, PayableCol, 0x80000017);
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, PayableCol, 0x80000018);
						vs1.TextMatrix(vs1.Rows - 1, VendorCol, modValidateAccount.GetFormat_6(Conversion.Str(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")), 5));
						rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
						vs1.TextMatrix(vs1.Rows - 1, NameCol, FCConvert.ToString(rs.Get_Fields_String("CheckName")));
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description")));
						vs1.TextMatrix(vs1.Rows - 1, ReferenceCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("MemoNumber")));
						if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Adjustments"))
						{
							temp = Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Adjustments"));
						}
						else
						{
							temp = 0;
						}
						if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Liquidated"))
						{
							temp2 = Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Liquidated"));
						}
						else
						{
							temp2 = 0;
						}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount")) + temp - temp2));
						vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")));
						CalculatePending();
						vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Rows - 1, AmountCol)) + Conversion.Val(vs1.TextMatrix(vs1.Rows - 1, PayableCol))));
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						PeriodData[counter] = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period"));
						counter += 1;
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					}
				}
				//FC:FINAL:DDU:#2899 - aligned columns
				//FC:FINAL:BBE:#528 - font size used in original for samll row height
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, VendorCol, vs1.Rows - 1, PayableCol, 8);
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, VendorCol, 0, PayableCol, 4);
				//if (vs1.Rows > 1)
				//{
				//	vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, ReferenceCol, vs1.Rows - 1, ReferenceCol, 1);
				//}
				//FC:FINAL:BBE:#528 - font size used in original for samll row height
				//vs2.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, DetailCol, vs2.Rows - 1, DetailAmountCol, 8);
				//vs2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DetailCol, 0, DetailAmountCol, 4);
				vs1.ColAlignment(VendorCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(NameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(ReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs1.ColAlignment(PayableCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs2.ColAlignment(DetailCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs2.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs2.ColAlignment(DetailAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs2.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1_RowColChange(vs1, EventArgs.Empty);
				cboPeriod.SelectedIndex = PeriodData[0] - 1;
			}
			else
			{
				cboPeriod.SelectedIndex = DateTime.Today.Month - 1;
			}
			vs1_Collapsed();
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomColors();
			EditFlag = false;
		}

		private void frmCreditMemoCorrections_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 6; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * ColPercents[counter]));
			}
			//FC:FINAL:DSE #i601 Exception if width or height is 0
			if (vs2.Height != 0 && vs2.Width != 0)
			{
				for (counter = 0; counter <= 4; counter++)
				{
					vs2.ColWidth(counter, FCConvert.ToInt32(vs2.WidthOriginal * vs2ColPercents[counter]));
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
			if (JournalNumber != 0)
			{
				MessageBox.Show("Adjusting entries in AC Journal " + FCConvert.ToString(JournalNumber) + ".  Post the journal as soon as possible.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void frmCreditMemoCorrections_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				if (fraChange.Visible == true)
				{
					KeyAscii = (Keys)0;
					cmdCancel_Click();
				}
				else if (fraUpdate.Visible == true)
				{
					KeyAscii = (Keys)0;
					cmdReturn_Click();
				}
				else
				{
					KeyAscii = (Keys)0;
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() == "vs1")
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click(sender, e);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsNewAccount_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				vsNewAccount_Validating_2(false);
				if (!BadAmount)
				{
					cmdUpdate_Click();
				}
			}
		}

		private void vsNewAccount_Validating_2(bool Cancel)
		{
			vsNewAccount_Validating(vsNewAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vsNewAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsNewAccount.TextMatrix(0, 0).Length; counter++)
			{
				if (Strings.Mid(vsNewAccount.TextMatrix(0, 0), counter, 1) == "_")
				{
					BadAmount = true;
					return;
				}
			}
			if (modValidateAccount.AccountValidate(vsNewAccount.TextMatrix(0, 0)))
			{
				BadAmount = false;
			}
			else
			{
				MessageBox.Show("This is not a valid account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				vsNewAccount.TextMatrix(0, 0, "");
				vsNewAccount.EditSelLength = 1;
				BadAmount = true;
			}
		}

		private void txtNewValue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				txtNewValue_Validating_2(false);
				if (!BadAmount)
				{
					cmdUpdate_Click();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNewValue_Validating_2(bool Cancel)
		{
			txtNewValue_Validating(txtNewValue, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtNewValue_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int temp = 0;
			if ((!Information.IsNumeric(txtNewValue.Text) && txtNewValue.Text != "") || Information.IsNumeric(txtNewValue.Text) && Conversion.Val(txtNewValue.Text) < 0)
			{
				MessageBox.Show("You must enter a number greater then 0 in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtNewValue.Text = "";
				BadAmount = true;
			}
			else if (txtNewValue.Text == "")
			{
				txtNewValue.Text = "0";
				BadAmount = false;
			}
			else
			{
				temp = Strings.InStr(txtNewValue.Text, ".", CompareConstants.vbBinaryCompare);
				if (temp > 0)
				{
					if (txtNewValue.Text.Length > temp + 2)
					{
						txtNewValue.Text = Strings.Left(txtNewValue.Text, temp + 2);
					}
				}
				if (vs2.TextMatrix(vs2.Row, DetailAmountCol) != "")
				{
					if (Conversion.Val(vsOldValue.TextMatrix(0, 0)) - Conversion.Val(txtNewValue.Text) > Conversion.Val(vs1.TextMatrix(vs1.Row, AmountCol)) - Conversion.Val(vs1.TextMatrix(vs1.Row, PayableCol)))
					{
						MessageBox.Show("You cannot change this amount to less then what is curently left in the credit memo", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				BadAmount = false;
			}
		}

		public void txtNewValue_Validate(ref bool Cancel)
		{
			txtNewValue_Validating(txtNewValue, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vs1_AfterSort(object sender, EventArgs e)
		{
			vs1_RowColChange(sender, e);
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int height;
			//FC:FINAl:SBE - fixed height on web version
			//height = vs1.Rows * vs1.RowHeight(0);
			//if (height < this.Height * 0.4)
			//{
			//    if (vs1.Height != height)
			//    {
			//        vs1.Height = height + 75;
			//    }
			//}
			//else
			//{
			//    if (vs1.Height < this.Height * 0.4 + 75)
			//    {
			//        vs1.Height = FCConvert.ToInt32(this.Height * 0.4 + 75);
			//    }
			//}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			vs1.BackColorSel = SystemColors.Info;
			vs2.BackColorSel = SystemColors.Control;
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			double temp;
			double temp2;
			clsDRWrapper rsAdjustments = new clsDRWrapper();
			vs2.Rows = 1;
			rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE CreditMemoID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				cboPeriod.SelectedIndex = PeriodData[vs1.Row - 1] - 1;
				while (rs.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (Conversion.Val(rs.Get_Fields("Amount")) + Conversion.Val(rs.Get_Fields_Decimal("Adjustments")) - Conversion.Val(rs.Get_Fields_Decimal("Liquidated")) > 0)
					{
						vs2.Rows += 1;
						vs2.TextMatrix(vs2.Rows - 1, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						vs2.TextMatrix(vs2.Rows - 1, DetailCol, "Credit Memo");
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, AccountCol, FCConvert.ToString(rs.Get_Fields("account")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, DetailAmountCol, Conversion.Val(rs.Get_Fields("Amount") + rs.Get_Fields_Decimal("Adjustments") - rs.Get_Fields_Decimal("Liquidated")));
						if (modBudgetaryMaster.Statics.ProjectFlag)
						{
							vs2.TextMatrix(vs2.Rows - 1, ProjectCol, FCConvert.ToString(rs.Get_Fields_String("Project")));
						}
					}
					rs.MoveNext();
				}
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                //FC:FINAL:CHN: Incorrect Converting.
                // vs1.TextMatrix(vs1.Row, AmountCol, rs.Get_Fields("Amount") + rs.Get_Fields_Decimal("Adjustments") - rs.Get_Fields_Decimal("Liquidated") + vs1.TextMatrix(vs1.Row, PayableCol));
                vs1.TextMatrix(vs1.Row, AmountCol, rs.Get_Fields("Amount") + rs.Get_Fields_Decimal("Adjustments") - rs.Get_Fields_Decimal("Liquidated") + FCConvert.ToDecimal(Conversion.CDbl(vs1.TextMatrix(vs1.Row, PayableCol))));
            }
            // vs2.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, DetailCol, vs2.rows - 1, DetailPayableCol) = 8
            vs2_Collapsed();
			rsAdjustments.OpenRecordset("SELECT * FROM APJournal WHERE Status <> 'P' AND CreditMemoCorrection = 1 AND Reference = '" + vs1.TextMatrix(vs1.Row, ReferenceCol) + "'");
			if (EditFlag == false)
			{
				if (rsAdjustments.EndOfFile() != true && rsAdjustments.BeginningOfFile() != true)
				{
					MessageBox.Show("Credit Memo Information is not accurate.  There are one or more Crdit Memo Adjustment Journal Entries that have not been posted yet for this credit memo.", "Credit Memo Information Not Current", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs2_Collapsed();
		}

		private void RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs2_Collapsed();
		}

		private void vs2_Collapsed()
		{
			if (vs2.Rows >= 8)
			{
				//vs2.Height = 8 * vs2.RowHeight(0) + 75;
				//vs2.Height = 312;
			}
			else
			{
				//vs2.Height = (vs2.Rows - 1) * 40 + 32;
			}
		}

		private void vs2_DblClick(object sender, System.EventArgs e)
		{
			if (vs2.Rows > 1 && vs2.MouseRow > 0)
			{
				vs2.BackColorSel = SystemColors.Control;
				fraUpdate.Visible = true;
				fraUpdate.CenterToParent();
				cmdDeleteUpdate.Focus();
				vs1.Enabled = false;
				vs2.Enabled = false;
			}
		}

		private void vs2_Enter(object sender, System.EventArgs e)
		{
			vs2.BackColorSel = SystemColors.Info;
			vs1.BackColorSel = SystemColors.Control;
		}

		private void vs2_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				vs2.BackColorSel = SystemColors.Control;
				fraUpdate.Visible = true;
				fraUpdate.CenterToParent();
				cmdDeleteUpdate.Focus();
				vs1.Enabled = false;
				vs2.Enabled = false;
			}
		}

		private double CalculateTotal()
		{
			double CalculateTotal = 0;
			int counter;
			CalculateTotal = 0;
			for (counter = 1; counter <= vs2.Rows - 1; counter++)
			{
				CalculateTotal += FCConvert.ToDouble(vs2.TextMatrix(counter, DetailAmountCol));
			}
			return CalculateTotal;
		}

		private void CalculatePending()
		{
			double total = 0;
			int counter;
			rs.OpenRecordset("SELECT SUM(Amount) as TotalAmount FROM APJournal WHERE CreditMemoCorrection = 0 AND CreditMemoRecord = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " AND Status <> 'P'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				total = 0;
				total += Conversion.Val(rs.Get_Fields_Decimal("TotalAmount"));
				vs1.TextMatrix(vs1.Rows - 1, PayableCol, FCConvert.ToString(total * -1));
			}
		}

		private void SetCustomColors()
		{
			lblInstruct.ForeColor = Color.Red;
		}
	}
}
