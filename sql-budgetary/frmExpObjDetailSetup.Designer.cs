﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpObjDetailSetup.
	/// </summary>
	partial class frmExpObjDetailSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRangeObject;
		public fecherFoundation.FCLabel lblRangeObject;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbTrans;
		public fecherFoundation.FCLabel lblTrans;
		public fecherFoundation.FCFrame fraObjects;
		public fecherFoundation.FCComboBox cboBeginningObject;
		public fecherFoundation.FCComboBox cboSingleObject;
		public fecherFoundation.FCComboBox cboEndingObject;
		public fecherFoundation.FCComboBox cboExpense;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public Global.T2KDateBox txtSingleDate;
		public fecherFoundation.FCLabel lblDateTo;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboDeptDivPriority;
		public fecherFoundation.FCComboBox cboJournalNumberPriority;
		public fecherFoundation.FCComboBox cboPeriodPriority;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpObjDetailSetup));
			this.cmbRangeObject = new fecherFoundation.FCComboBox();
			this.lblRangeObject = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbTrans = new fecherFoundation.FCComboBox();
			this.lblTrans = new fecherFoundation.FCLabel();
			this.fraObjects = new fecherFoundation.FCFrame();
			this.cboBeginningObject = new fecherFoundation.FCComboBox();
			this.cboSingleObject = new fecherFoundation.FCComboBox();
			this.cboEndingObject = new fecherFoundation.FCComboBox();
			this.cboExpense = new fecherFoundation.FCComboBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Line1 = new fecherFoundation.FCLine();
			this.fraMonths = new fecherFoundation.FCFrame();
			this.cboEndingMonth = new fecherFoundation.FCComboBox();
			this.cboBeginningMonth = new fecherFoundation.FCComboBox();
			this.cboSingleMonth = new fecherFoundation.FCComboBox();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.txtSingleDate = new Global.T2KDateBox();
			this.lblDateTo = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboDeptDivPriority = new fecherFoundation.FCComboBox();
			this.cboJournalNumberPriority = new fecherFoundation.FCComboBox();
			this.cboPeriodPriority = new fecherFoundation.FCComboBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraObjects)).BeginInit();
			this.fraObjects.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
			this.fraMonths.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSingleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 389);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraObjects);
			this.ClientArea.Controls.Add(this.fraMonths);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Size = new System.Drawing.Size(610, 329);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(263, 30);
			this.HeaderText.Text = "Exp / Obj Detail Report";
			// 
			// cmbRangeObject
			// 
			this.cmbRangeObject.AutoSize = false;
			this.cmbRangeObject.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRangeObject.FormattingEnabled = true;
			this.cmbRangeObject.Items.AddRange(new object[] {
				"All",
				"Single Object",
				"Range of Objects"
			});
			this.cmbRangeObject.Location = new System.Drawing.Point(113, 90);
			this.cmbRangeObject.Name = "cmbRangeObject";
			this.cmbRangeObject.Size = new System.Drawing.Size(200, 40);
			this.cmbRangeObject.TabIndex = 30;
			this.cmbRangeObject.SelectedIndexChanged += new System.EventHandler(this.cmbRangeObject_SelectedIndexChanged);
			// 
			// lblRangeObject
			// 
			this.lblRangeObject.Location = new System.Drawing.Point(20, 104);
			this.lblRangeObject.Name = "lblRangeObject";
			this.lblRangeObject.Size = new System.Drawing.Size(51, 15);
			this.lblRangeObject.TabIndex = 31;
			this.lblRangeObject.Text = "RANGE";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Single Month",
				"Range of Months"
			});
			this.cmbRange.Location = new System.Drawing.Point(140, 90);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(200, 40);
			this.cmbRange.TabIndex = 2;
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
			// 
			// lblRange
			// 
			this.lblRange.Location = new System.Drawing.Point(20, 104);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(50, 16);
			this.lblRange.TabIndex = 3;
			this.lblRange.Text = "RANGE";
			// 
			// cmbTrans
			// 
			this.cmbTrans.AutoSize = false;
			this.cmbTrans.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTrans.FormattingEnabled = true;
			this.cmbTrans.Items.AddRange(new object[] {
				"Period",
				"Posted Date",
				"Trans Date"
			});
			this.cmbTrans.Location = new System.Drawing.Point(140, 30);
			this.cmbTrans.Name = "cmbTrans";
			this.cmbTrans.Size = new System.Drawing.Size(200, 40);
			this.cmbTrans.TabIndex = 0;
			this.cmbTrans.SelectedIndexChanged += new System.EventHandler(this.optTrans_CheckedChanged);
			// 
			// lblTrans
			// 
			this.lblTrans.Location = new System.Drawing.Point(20, 44);
			this.lblTrans.Name = "lblTrans";
			this.lblTrans.Size = new System.Drawing.Size(85, 16);
			this.lblTrans.TabIndex = 1;
			this.lblTrans.Text = "SELECT DATE";
			// 
			// fraObjects
			// 
			this.fraObjects.Controls.Add(this.cboBeginningObject);
			this.fraObjects.Controls.Add(this.cmbRangeObject);
			this.fraObjects.Controls.Add(this.lblRangeObject);
			this.fraObjects.Controls.Add(this.cboSingleObject);
			this.fraObjects.Controls.Add(this.cboEndingObject);
			this.fraObjects.Controls.Add(this.cboExpense);
			this.fraObjects.Controls.Add(this.lblTo);
			this.fraObjects.Controls.Add(this.Label1);
			this.fraObjects.Controls.Add(this.Line1);
			this.fraObjects.Location = new System.Drawing.Point(30, 30);
			this.fraObjects.Name = "fraObjects";
			this.fraObjects.Size = new System.Drawing.Size(500, 210);
			this.fraObjects.TabIndex = 22;
			this.fraObjects.Text = "Accounts To Report";
			// 
			// cboBeginningObject
			// 
			this.cboBeginningObject.AutoSize = false;
			this.cboBeginningObject.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningObject.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningObject.FormattingEnabled = true;
			this.cboBeginningObject.Location = new System.Drawing.Point(20, 150);
			this.cboBeginningObject.Name = "cboBeginningObject";
			this.cboBeginningObject.Size = new System.Drawing.Size(200, 40);
			this.cboBeginningObject.TabIndex = 29;
			this.cboBeginningObject.Visible = false;
			// 
			// cboSingleObject
			// 
			this.cboSingleObject.AutoSize = false;
			this.cboSingleObject.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleObject.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleObject.FormattingEnabled = true;
			this.cboSingleObject.Location = new System.Drawing.Point(113, 150);
			this.cboSingleObject.Name = "cboSingleObject";
			this.cboSingleObject.Size = new System.Drawing.Size(200, 40);
			this.cboSingleObject.TabIndex = 25;
			this.cboSingleObject.Visible = false;
			// 
			// cboEndingObject
			// 
			this.cboEndingObject.AutoSize = false;
			this.cboEndingObject.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingObject.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingObject.FormattingEnabled = true;
			this.cboEndingObject.Location = new System.Drawing.Point(280, 150);
			this.cboEndingObject.Name = "cboEndingObject";
			this.cboEndingObject.Size = new System.Drawing.Size(200, 40);
			this.cboEndingObject.TabIndex = 24;
			this.cboEndingObject.Visible = false;
			// 
			// cboExpense
			// 
			this.cboExpense.AutoSize = false;
			this.cboExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboExpense.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboExpense.FormattingEnabled = true;
			this.cboExpense.Location = new System.Drawing.Point(113, 30);
			this.cboExpense.Name = "cboExpense";
			this.cboExpense.Size = new System.Drawing.Size(200, 40);
			this.cboExpense.TabIndex = 23;
			this.cboExpense.SelectedIndexChanged += new System.EventHandler(this.cboExpense_SelectedIndexChanged);
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(240, 162);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(20, 16);
			this.lblTo.TabIndex = 31;
			this.lblTo.Text = "TO";
			this.lblTo.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(58, 16);
			this.Label1.TabIndex = 30;
			this.Label1.Text = "EXPENSE";
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.BorderWidth = ((short)(1));
			this.Line1.LineWidth = 0;
			this.Line1.Location = new System.Drawing.Point(0, 0);
			this.Line1.Name = "Line1";
			this.Line1.Size = new System.Drawing.Size(100, 10);
			this.Line1.X1 = 0F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// fraMonths
			// 
			this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
			this.fraMonths.Controls.Add(this.cmbTrans);
			this.fraMonths.Controls.Add(this.lblTrans);
			this.fraMonths.Controls.Add(this.cmbRange);
			this.fraMonths.Controls.Add(this.lblRange);
			this.fraMonths.Controls.Add(this.cboEndingMonth);
			this.fraMonths.Controls.Add(this.cboBeginningMonth);
			this.fraMonths.Controls.Add(this.cboSingleMonth);
			this.fraMonths.Controls.Add(this.txtStartDate);
			this.fraMonths.Controls.Add(this.txtEndDate);
			this.fraMonths.Controls.Add(this.txtSingleDate);
			this.fraMonths.Controls.Add(this.lblDateTo);
			this.fraMonths.Location = new System.Drawing.Point(30, 260);
			this.fraMonths.Name = "fraMonths";
			this.fraMonths.Size = new System.Drawing.Size(360, 210);
			this.fraMonths.TabIndex = 7;
			this.fraMonths.Text = "Date\'s To Report";
			// 
			// cboEndingMonth
			// 
			this.cboEndingMonth.AutoSize = false;
			this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingMonth.FormattingEnabled = true;
			this.cboEndingMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboEndingMonth.Location = new System.Drawing.Point(210, 150);
			this.cboEndingMonth.Name = "cboEndingMonth";
			this.cboEndingMonth.Size = new System.Drawing.Size(130, 40);
			this.cboEndingMonth.TabIndex = 17;
			this.cboEndingMonth.Visible = false;
			// 
			// cboBeginningMonth
			// 
			this.cboBeginningMonth.AutoSize = false;
			this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningMonth.FormattingEnabled = true;
			this.cboBeginningMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboBeginningMonth.Location = new System.Drawing.Point(20, 150);
			this.cboBeginningMonth.Name = "cboBeginningMonth";
			this.cboBeginningMonth.Size = new System.Drawing.Size(130, 40);
			this.cboBeginningMonth.TabIndex = 16;
			this.cboBeginningMonth.Visible = false;
			// 
			// cboSingleMonth
			// 
			this.cboSingleMonth.AutoSize = false;
			this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleMonth.FormattingEnabled = true;
			this.cboSingleMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboSingleMonth.Location = new System.Drawing.Point(140, 150);
			this.cboSingleMonth.Name = "cboSingleMonth";
			this.cboSingleMonth.Size = new System.Drawing.Size(130, 40);
			this.cboSingleMonth.TabIndex = 13;
			this.cboSingleMonth.Visible = false;
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(20, 150);
			this.txtStartDate.Mask = "00/00/0000";
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(130, 40);
			this.txtStartDate.TabIndex = 8;
			this.txtStartDate.Text = "  /  /";
			this.txtStartDate.Visible = false;
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(210, 150);
			this.txtEndDate.Mask = "00/00/0000";
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(130, 40);
			this.txtEndDate.TabIndex = 19;
			this.txtEndDate.Text = "  /  /";
			this.txtEndDate.Visible = false;
			// 
			// txtSingleDate
			// 
			this.txtSingleDate.Location = new System.Drawing.Point(140, 150);
			this.txtSingleDate.Mask = "00/00/0000";
			this.txtSingleDate.MaxLength = 10;
			this.txtSingleDate.Name = "txtSingleDate";
			this.txtSingleDate.Size = new System.Drawing.Size(130, 40);
			this.txtSingleDate.TabIndex = 20;
			this.txtSingleDate.Text = "  /  /";
			this.txtSingleDate.Visible = false;
			// 
			// lblDateTo
			// 
			this.lblDateTo.BackColor = System.Drawing.Color.Transparent;
			this.lblDateTo.Location = new System.Drawing.Point(170, 164);
			this.lblDateTo.Name = "lblDateTo";
			this.lblDateTo.Size = new System.Drawing.Size(20, 16);
			this.lblDateTo.TabIndex = 21;
			this.lblDateTo.Text = "TO";
			this.lblDateTo.Visible = false;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboDeptDivPriority);
			this.Frame2.Controls.Add(this.cboJournalNumberPriority);
			this.Frame2.Controls.Add(this.cboPeriodPriority);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Controls.Add(this.Label3);
			this.Frame2.Controls.Add(this.Label4);
			this.Frame2.Location = new System.Drawing.Point(30, 490);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(250, 210);
			this.Frame2.TabIndex = 0;
			this.Frame2.Text = "Order By Priority";
			// 
			// cboDeptDivPriority
			// 
			this.cboDeptDivPriority.AutoSize = false;
			this.cboDeptDivPriority.BackColor = System.Drawing.SystemColors.Window;
			this.cboDeptDivPriority.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboDeptDivPriority.FormattingEnabled = true;
			this.cboDeptDivPriority.Items.AddRange(new object[] {
				"1",
				"2",
				"3"
			});
			this.cboDeptDivPriority.Location = new System.Drawing.Point(20, 30);
			this.cboDeptDivPriority.Name = "cboDeptDivPriority";
			this.cboDeptDivPriority.Size = new System.Drawing.Size(60, 40);
			this.cboDeptDivPriority.TabIndex = 3;
			// 
			// cboJournalNumberPriority
			// 
			this.cboJournalNumberPriority.AutoSize = false;
			this.cboJournalNumberPriority.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournalNumberPriority.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournalNumberPriority.FormattingEnabled = true;
			this.cboJournalNumberPriority.Items.AddRange(new object[] {
				"1",
				"2",
				"3"
			});
			this.cboJournalNumberPriority.Location = new System.Drawing.Point(20, 90);
			this.cboJournalNumberPriority.Name = "cboJournalNumberPriority";
			this.cboJournalNumberPriority.Size = new System.Drawing.Size(60, 40);
			this.cboJournalNumberPriority.TabIndex = 2;
			// 
			// cboPeriodPriority
			// 
			this.cboPeriodPriority.AutoSize = false;
			this.cboPeriodPriority.BackColor = System.Drawing.SystemColors.Window;
			this.cboPeriodPriority.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPeriodPriority.FormattingEnabled = true;
			this.cboPeriodPriority.Items.AddRange(new object[] {
				"1",
				"2",
				"3"
			});
			this.cboPeriodPriority.Location = new System.Drawing.Point(20, 150);
			this.cboPeriodPriority.Name = "cboPeriodPriority";
			this.cboPeriodPriority.Size = new System.Drawing.Size(60, 40);
			this.cboPeriodPriority.TabIndex = 1;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(100, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(115, 16);
			this.Label2.TabIndex = 6;
			this.Label2.Text = "JOURNAL NUMBER";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(100, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(65, 16);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "DEPT / DIV";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(100, 164);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(130, 16);
			this.Label4.TabIndex = 4;
			this.Label4.Text = "ACCOUNTING PERIOD";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(259, 30);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(100, 48);
			this.cmdPreview.TabIndex = 0;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// frmExpObjDetailSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 497);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmExpObjDetailSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Exp / Obj Detail Report";
			this.Load += new System.EventHandler(this.frmExpObjDetailSetup_Load);
			this.Activated += new System.EventHandler(this.frmExpObjDetailSetup_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExpObjDetailSetup_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraObjects)).EndInit();
			this.fraObjects.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
			this.fraMonths.ResumeLayout(false);
			this.fraMonths.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSingleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdPreview;
	}
}
