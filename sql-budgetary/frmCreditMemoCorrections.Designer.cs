﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreditMemoCorrections.
	/// </summary>
	partial class frmCreditMemoCorrections : BaseForm
	{
		public fecherFoundation.FCFrame fraUpdate;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdAmount;
		public fecherFoundation.FCButton cmdAccount;
		public fecherFoundation.FCButton cmdDeleteUpdate;
		public fecherFoundation.FCComboBox cboPeriod;
		public fecherFoundation.FCFrame fraChange;
		public FCGrid vsNewAccount;
		public FCGrid vsOldValue;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdUpdate;
		public fecherFoundation.FCTextBox txtNewValue;
		public fecherFoundation.FCLabel lblNewValue;
		public fecherFoundation.FCLabel lblOldValue;
		public fecherFoundation.FCGrid vs2;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCLabel lblEntries;
		public fecherFoundation.FCLabel lblDetails;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreditMemoCorrections));
			this.fraUpdate = new fecherFoundation.FCFrame();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdAmount = new fecherFoundation.FCButton();
			this.cmdAccount = new fecherFoundation.FCButton();
			this.cmdDeleteUpdate = new fecherFoundation.FCButton();
			this.cboPeriod = new fecherFoundation.FCComboBox();
			this.fraChange = new fecherFoundation.FCFrame();
			this.vsNewAccount = new fecherFoundation.FCGrid();
			this.vsOldValue = new fecherFoundation.FCGrid();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdUpdate = new fecherFoundation.FCButton();
			this.txtNewValue = new fecherFoundation.FCTextBox();
			this.lblNewValue = new fecherFoundation.FCLabel();
			this.lblOldValue = new fecherFoundation.FCLabel();
			this.vs2 = new fecherFoundation.FCGrid();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblInstruct = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.lblEntries = new fecherFoundation.FCLabel();
			this.lblDetails = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcessAccounts = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraUpdate)).BeginInit();
			this.fraUpdate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChange)).BeginInit();
			this.fraChange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsNewAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsOldValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraUpdate);
			this.ClientArea.Controls.Add(this.cboPeriod);
			this.ClientArea.Controls.Add(this.fraChange);
			this.ClientArea.Controls.Add(this.vs2);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblInstruct);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Controls.Add(this.lblEntries);
			this.ClientArea.Controls.Add(this.lblDetails);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessAccounts);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessAccounts, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(286, 30);
			this.HeaderText.Text = "Credit Memo Corrections";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraUpdate
			// 
			this.fraUpdate.BackColor = System.Drawing.Color.White;
			this.fraUpdate.Controls.Add(this.cmdReturn);
			this.fraUpdate.Controls.Add(this.cmdAmount);
			this.fraUpdate.Controls.Add(this.cmdAccount);
			this.fraUpdate.Controls.Add(this.cmdDeleteUpdate);
			this.fraUpdate.Location = new System.Drawing.Point(672, 0);
			this.fraUpdate.Name = "fraUpdate";
			this.fraUpdate.Size = new System.Drawing.Size(274, 240);
			this.fraUpdate.TabIndex = 4;
			this.fraUpdate.Text = "Update Choices";
			this.ToolTip1.SetToolTip(this.fraUpdate, null);
			this.fraUpdate.Visible = false;
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(20, 180);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(234, 40);
			this.cmdReturn.TabIndex = 8;
			this.cmdReturn.Text = "Return to List";
			this.ToolTip1.SetToolTip(this.cmdReturn, null);
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// cmdAmount
			// 
			this.cmdAmount.AppearanceKey = "actionButton";
			this.cmdAmount.Location = new System.Drawing.Point(20, 130);
			this.cmdAmount.Name = "cmdAmount";
			this.cmdAmount.Size = new System.Drawing.Size(234, 40);
			this.cmdAmount.TabIndex = 7;
			this.cmdAmount.Text = "Change Amount";
			this.ToolTip1.SetToolTip(this.cmdAmount, null);
			this.cmdAmount.Click += new System.EventHandler(this.cmdAmount_Click);
			// 
			// cmdAccount
			// 
			this.cmdAccount.AppearanceKey = "actionButton";
			this.cmdAccount.Location = new System.Drawing.Point(20, 80);
			this.cmdAccount.Name = "cmdAccount";
			this.cmdAccount.Size = new System.Drawing.Size(234, 40);
			this.cmdAccount.TabIndex = 6;
			this.cmdAccount.Text = "Change Account";
			this.ToolTip1.SetToolTip(this.cmdAccount, null);
			this.cmdAccount.Click += new System.EventHandler(this.cmdAccount_Click);
			// 
			// cmdDeleteUpdate
			// 
			this.cmdDeleteUpdate.AppearanceKey = "actionButton";
			this.cmdDeleteUpdate.Location = new System.Drawing.Point(20, 30);
			this.cmdDeleteUpdate.Name = "cmdDeleteUpdate";
			this.cmdDeleteUpdate.Size = new System.Drawing.Size(234, 40);
			this.cmdDeleteUpdate.TabIndex = 5;
			this.cmdDeleteUpdate.Text = "Delete Line Item";
			this.ToolTip1.SetToolTip(this.cmdDeleteUpdate, null);
			this.cmdDeleteUpdate.Click += new System.EventHandler(this.cmdDeleteUpdate_Click);
			// 
			// cboPeriod
			// 
			this.cboPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.cboPeriod.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
			this.cboPeriod.Location = new System.Drawing.Point(141, 66);
			this.cboPeriod.Name = "cboPeriod";
			this.cboPeriod.Size = new System.Drawing.Size(178, 40);
			this.cboPeriod.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.cboPeriod, null);
			// 
			// fraChange
			// 
			this.fraChange.BackColor = System.Drawing.Color.White;
			this.fraChange.Controls.Add(this.vsNewAccount);
			this.fraChange.Controls.Add(this.vsOldValue);
			this.fraChange.Controls.Add(this.cmdCancel);
			this.fraChange.Controls.Add(this.cmdUpdate);
			this.fraChange.Controls.Add(this.txtNewValue);
			this.fraChange.Controls.Add(this.lblNewValue);
			this.fraChange.Controls.Add(this.lblOldValue);
			this.fraChange.Location = new System.Drawing.Point(672, 280);
			this.fraChange.Name = "fraChange";
			this.fraChange.Size = new System.Drawing.Size(375, 210);
			this.fraChange.TabIndex = 9;
			this.fraChange.Text = "Update Value";
			this.ToolTip1.SetToolTip(this.fraChange, null);
			this.fraChange.Visible = false;
			// 
			// vsNewAccount
			// 
			this.vsNewAccount.Cols = 1;
			this.vsNewAccount.ColumnHeadersVisible = false;
			this.vsNewAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsNewAccount.FixedCols = 0;
			this.vsNewAccount.FixedRows = 0;
			this.vsNewAccount.Location = new System.Drawing.Point(168, 90);
			this.vsNewAccount.Name = "vsNewAccount";
			this.vsNewAccount.ReadOnly = false;
			this.vsNewAccount.RowHeadersVisible = false;
			this.vsNewAccount.Rows = 1;
			this.vsNewAccount.Size = new System.Drawing.Size(187, 42);
			this.vsNewAccount.TabIndex = 19;
			this.ToolTip1.SetToolTip(this.vsNewAccount, null);
			this.vsNewAccount.Validating += new System.ComponentModel.CancelEventHandler(this.vsNewAccount_Validating);
			this.vsNewAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsNewAccount_KeyPressEvent);
			// 
			// vsOldValue
			// 
			this.vsOldValue.Cols = 1;
			this.vsOldValue.ColumnHeadersVisible = false;
			this.vsOldValue.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsOldValue.FixedCols = 0;
			this.vsOldValue.FixedRows = 0;
			this.vsOldValue.Location = new System.Drawing.Point(168, 30);
			this.vsOldValue.Name = "vsOldValue";
			this.vsOldValue.ReadOnly = false;
			this.vsOldValue.RowHeadersVisible = false;
			this.vsOldValue.Rows = 1;
			this.vsOldValue.Size = new System.Drawing.Size(187, 42);
			this.vsOldValue.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.vsOldValue, null);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(150, 150);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(100, 40);
			this.cmdCancel.TabIndex = 14;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdUpdate
			// 
			this.cmdUpdate.AppearanceKey = "actionButton";
			this.cmdUpdate.Location = new System.Drawing.Point(20, 150);
			this.cmdUpdate.Name = "cmdUpdate";
			this.cmdUpdate.Size = new System.Drawing.Size(100, 40);
			this.cmdUpdate.TabIndex = 13;
			this.cmdUpdate.Text = "Update";
			this.ToolTip1.SetToolTip(this.cmdUpdate, null);
			this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
			// 
			// txtNewValue
			// 
			this.txtNewValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtNewValue.Location = new System.Drawing.Point(168, 90);
			this.txtNewValue.Name = "txtNewValue";
			this.txtNewValue.Size = new System.Drawing.Size(187, 40);
			this.txtNewValue.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.txtNewValue, null);
			this.txtNewValue.Visible = false;
			this.txtNewValue.Validating += new System.ComponentModel.CancelEventHandler(this.txtNewValue_Validating);
			this.txtNewValue.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewValue_KeyPress);
			// 
			// lblNewValue
			// 
			this.lblNewValue.Location = new System.Drawing.Point(20, 104);
			this.lblNewValue.Name = "lblNewValue";
			this.lblNewValue.Size = new System.Drawing.Size(100, 20);
			this.lblNewValue.TabIndex = 11;
			this.lblNewValue.Text = "NEW ACCOUNT";
			this.ToolTip1.SetToolTip(this.lblNewValue, null);
			// 
			// lblOldValue
			// 
			this.lblOldValue.Location = new System.Drawing.Point(20, 44);
			this.lblOldValue.Name = "lblOldValue";
			this.lblOldValue.Size = new System.Drawing.Size(100, 20);
			this.lblOldValue.TabIndex = 10;
			this.lblOldValue.Text = "OLD ACCOUNT";
			this.ToolTip1.SetToolTip(this.lblOldValue, null);
			// 
			// vs2
			// 
			this.vs2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs2.Cols = 5;
			this.vs2.FixedCols = 0;
			this.vs2.Location = new System.Drawing.Point(30, 410);
			this.vs2.Name = "vs2";
			this.vs2.RowHeadersVisible = false;
			this.vs2.Rows = 1;
			this.vs2.Size = new System.Drawing.Size(1018, 73);
			this.vs2.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vs2, "Double click on the line you wish to make the correction to.");
			this.vs2.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.RowExpanded);
			this.vs2.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.RowCollapsed);
			this.vs2.Enter += new System.EventHandler(this.vs2_Enter);
			this.vs2.DoubleClick += new System.EventHandler(this.vs2_DblClick);
			this.vs2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs2_KeyPressEvent);
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 7;
			this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vs1.Location = new System.Drawing.Point(30, 162);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 1;
			this.vs1.Size = new System.Drawing.Size(1018, 192);
			this.ToolTip1.SetToolTip(this.vs1, "Click on a journal to view entries");
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.Sorted += new System.EventHandler(this.vs1_AfterSort);
			this.vs1.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowExpanded);
			this.vs1.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowCollapsed);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			// 
			// lblInstruct
			// 
			this.lblInstruct.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblInstruct.Location = new System.Drawing.Point(30, 30);
			this.lblInstruct.Name = "lblInstruct";
			this.lblInstruct.Size = new System.Drawing.Size(412, 16);
			this.lblInstruct.TabIndex = 17;
			this.lblInstruct.Text = "ANY CHANGES MADE IN THIS SCREEN WILL TAKE EFFECT IMMEDIATELY";
			this.lblInstruct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblInstruct, null);
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(30, 80);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(53, 16);
			this.lblPeriod.TabIndex = 16;
			this.lblPeriod.Text = "PERIOD";
			this.ToolTip1.SetToolTip(this.lblPeriod, null);
			// 
			// lblEntries
			// 
			this.lblEntries.Location = new System.Drawing.Point(30, 126);
			this.lblEntries.Name = "lblEntries";
			this.lblEntries.Size = new System.Drawing.Size(111, 16);
			this.lblEntries.TabIndex = 3;
			this.lblEntries.Text = "CREDIT MEMOS";
			this.lblEntries.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblEntries, null);
			// 
			// lblDetails
			// 
			this.lblDetails.Location = new System.Drawing.Point(30, 374);
			this.lblDetails.Name = "lblDetails";
			this.lblDetails.Size = new System.Drawing.Size(67, 16);
			this.lblDetails.TabIndex = 2;
			this.lblDetails.Text = "DETAILS";
			this.lblDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblDetails, null);
			// 
			// btnProcessAccounts
			// 
			this.btnProcessAccounts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessAccounts.Enabled = false;
			this.btnProcessAccounts.Location = new System.Drawing.Point(967, 29);
			this.btnProcessAccounts.Name = "btnProcessAccounts";
			this.btnProcessAccounts.Size = new System.Drawing.Size(145, 24);
			this.btnProcessAccounts.TabIndex = 1;
			this.btnProcessAccounts.Text = "Show Valid Accounts";
			this.btnProcessAccounts.Visible = false;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(472, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(130, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save & Exit";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmCreditMemoCorrections
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCreditMemoCorrections";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Credit Memo Corrections";
			this.ToolTip1.SetToolTip(this, null);
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmCreditMemoCorrections_Load);
			this.Activated += new System.EventHandler(this.frmCreditMemoCorrections_Activated);
			this.Resize += new System.EventHandler(this.frmCreditMemoCorrections_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCreditMemoCorrections_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreditMemoCorrections_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraUpdate)).EndInit();
			this.fraUpdate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChange)).EndInit();
			this.fraChange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsNewAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsOldValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcessAccounts;
		private FCButton cmdSave;
	}
}
