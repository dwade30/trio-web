﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmTaxTitles.
	/// </summary>
	public partial class frmTaxTitles : BaseForm
	{
		public frmTaxTitles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxTitles InstancePtr
		{
			get
			{
				return (frmTaxTitles)Sys.GetInstance(typeof(frmTaxTitles));
			}
		}

		protected frmTaxTitles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/19/02
		// This form will be uesd by towns to setup the 1099 title
		// categories
		// ********************************************************
		int NameCol;
		int CategoryCol;
		private int FormCol;
		private string MiscFormCategories;
		private string NECFormCategories;   
		clsDRWrapper rs = new clsDRWrapper();
		bool blnUnload;

		private void frmTaxTitles_Activated(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsAddTaxTitle = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			NameCol = 1;
			FormCol = 2;
			CategoryCol = 3;
			MiscFormCategories = "#1;1" + "\t" + "Rents|#2;2" + "\t" + "Royalties|#3;3" + "\t" + "Other Income|#4;4" +
								 "\t" + "Federal Income Tax Withheld|#5;5" + "\t" + "Fishing Boat Proceeds|#6;6" +
								 "\t" + "Medical and Health Care Payments|#14;14" + "\t" +
								 "Gross Proceeds Paid to an Attorney";
			NECFormCategories = "#1;1" + "\t" + "Nonemployee Compensation|#4;4" + "\t" + "Federal Income Tax Withheld";

			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
			vs1.ColWidth(NameCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.6));
			vs1.ColWidth(FormCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.ColWidth(CategoryCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.01));

			vs1.EditMaxLength = 30;

			for (counter = 1; counter <= 9; counter++)
			{
				vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
			}
			vs1.TextMatrix(0, 0, "Code");
			vs1.TextMatrix(0, NameCol, "Tax Title");
			vs1.TextMatrix(0, FormCol, "Form");
			vs1.TextMatrix(0, CategoryCol, "Box");

			vs1.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColComboList(CategoryCol, MiscFormCategories);
			vs1.ColComboList(FormCol, "MISC|NEC");

			vs1.ColAlignment(CategoryCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// open the database and put 1099 information into the grid
			rs.OpenRecordset("SELECT * FROM TaxTitles ORDER BY TaxCode");
			rsAddTaxTitle.OpenRecordset("SELECT * FROM TaxTitles WHERE ID = 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (counter = 1; counter <= 9; counter++)
				{
					if (rs.EndOfFile() != true)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("TaxCode")) != counter)
						{
							rsAddTaxTitle.AddNew();
							rsAddTaxTitle.Set_Fields("TaxCode", counter);
							rsAddTaxTitle.Update();
						}
						else
						{
							rs.MoveNext();
						}
					}
					else
					{
						rsAddTaxTitle.AddNew();
						rsAddTaxTitle.Set_Fields("TaxCode", counter);
						rsAddTaxTitle.Update();
					}
				}
				rs.OpenRecordset("SELECT * FROM TaxTitles ORDER BY TaxCode");
				for (counter = 1; counter <= 9; counter++)
				{
					vs1.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("TaxCode")));
					vs1.TextMatrix(counter, NameCol, FCConvert.ToString(rs.Get_Fields_String("TaxDescription")));
					// TODO Get_Fields: Check the table for the column [Category] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, FormCol, FCConvert.ToString(rs.Get_Fields("FormName")));
					vs1.TextMatrix(counter, CategoryCol, FCConvert.ToString(rs.Get_Fields("Category")));
					if (counter < 9)
					{
						rs.MoveNext();
					}
				}
			}
			Form_Resize();
			vs1.Select(1, NameCol);
			vs1.EditCell();
			vs1.EditSelStart = 0;
			this.Refresh();
		}

		private void frmTaxTitles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				// save all 1099 informartion
				KeyCode = (Keys)0;
			}
		}

		private void frmTaxTitles_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmTaxTitles_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
			vs1.ColWidth(NameCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.6));
			vs1.ColWidth(FormCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.ColWidth(CategoryCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.01));
		}

		public void Form_Resize()
		{
			frmTaxTitles_Resize(this, new System.EventArgs());
		}

		private void frmTaxTitles_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			vs1.EditCell();
			vs1.EditSelStart = 0;
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{DOWN}", false);
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			
			if (vs1.Col == CategoryCol || vs1.Col == FormCol)
			{
				ReloadCategoryCombo(vs1.Row);
			}
			vs1.EditCell();
			vs1.EditSelStart = 0;
		}

		private void ReloadCategoryCombo(int gridRow)
		{
			if (vs1.TextMatrix(gridRow, FormCol) == "NEC")
			{
				vs1.ColComboList(CategoryCol, NECFormCategories);
			}
			else
			{
				vs1.ColComboList(CategoryCol, MiscFormCategories);
			}
		}

		private bool DataValid()
		{
			for (int counter = 1; counter < vs1.Rows; counter++)
			{
				if (vs1.TextMatrix(counter, FormCol) == "NEC" && (vs1.TextMatrix(counter, CategoryCol).ToIntegerValue() != 1 && vs1.TextMatrix(counter, CategoryCol).ToIntegerValue() != 4))
					return false;
			}

			return true;
		}


		private void SaveInfo()
		{
			int counter;

			if (DataValid())
			{
				vs1.Select(1, NameCol);
				rs.OpenRecordset("SELECT * FROM TaxTitles ORDER BY TaxCode");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					for (counter = 1; counter <= 9; counter++)
					{
						rs.Edit();
						rs.Set_Fields("TaxCode", vs1.TextMatrix(counter, 0));
						rs.Set_Fields("TaxDescription", vs1.TextMatrix(counter, NameCol));
						rs.Set_Fields("FormName", vs1.TextMatrix(counter, FormCol));
						if (Conversion.Val(vs1.TextMatrix(counter, CategoryCol)) != 0)
						{
							rs.Set_Fields("Category", vs1.TextMatrix(counter, CategoryCol));
						}
						rs.Update();
						rs.MoveNext();
					}
				}
				else
				{
					for (counter = 1; counter <= 9; counter++)
					{
						rs.AddNew();
						rs.Set_Fields("TaxCode", vs1.TextMatrix(counter, 0));
						rs.Set_Fields("TaxDescription", vs1.TextMatrix(counter, NameCol));
						rs.Set_Fields("FormName", vs1.TextMatrix(counter, FormCol));
						if (Conversion.Val(vs1.TextMatrix(counter, CategoryCol)) != 0)
						{
							rs.Set_Fields("Category", vs1.TextMatrix(counter, CategoryCol));
						}
						rs.Update();
					}
				}
				if (blnUnload)
				{
					Close();
				}
				else
				{
					MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
	}
}
