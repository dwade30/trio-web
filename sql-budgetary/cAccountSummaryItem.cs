﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cAccountSummaryItem
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strShortDescription = string.Empty;
		private string strAccount = string.Empty;
		private double dblDebits;
		private double dblCredits;
		private double dblBeginningBalance;
		private double dblEndBalance;
		private double dblNet;
		private double dblBalanceAdjustments;
		private string strFund = string.Empty;
		private string strAcctType = "";
		private double dblBudgetAdjustment;
		private double[] dblPeriodCredits = new double[12 + 1];
		private double[] dblPeriodDebits = new double[12 + 1];
		private double[] dblPeriodNet = new double[12 + 1];
		private double[] dblNetBegin = new double[12 + 1];
		private double[] dblNetEnd = new double[12 + 1];
		private double dblBeginningNet;
		private bool boolInclude;
		private string[] strSegments = new string[4 + 1];

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public bool Include
		{
			set
			{
				boolInclude = value;
			}
			get
			{
				bool Include = false;
				Include = boolInclude;
				return Include;
			}
		}
		// vbPorter upgrade warning: intSegment As Variant, short --> As int
		public string GetSegment(int intSegment)
		{
			string GetSegment = "";
			if (intSegment > 0 && intSegment < 5)
			{
				GetSegment = strSegments[FCConvert.ToInt16(intSegment) - 1];
			}
			return GetSegment;
		}

		public void SetSegment(int intSegment, string strValue)
		{
			if (intSegment > 0 && intSegment < 5)
			{
				strSegments[intSegment - 1] = strValue;
			}
		}

		public double BeginningNet
		{
			get
			{
				double BeginningNet = 0;
				BeginningNet = dblBeginningNet;
				return BeginningNet;
			}
			set
			{
				dblBeginningNet = value;
			}
		}

		public void AppendCreditsForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblPeriodCredits[intPeriod] += dblAmount;
			}
		}

		public void AppendDebitsForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblPeriodDebits[intPeriod] += dblAmount;
			}
		}

		public void AppendNetForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblPeriodNet[intPeriod] += dblAmount;
			}
		}

		public void AppendNetBeginForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblNetBegin[intPeriod] += dblAmount;
			}
		}

		public void AppendNetEndForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblNetEnd[intPeriod] += dblAmount;
			}
		}

		public void SetCreditsForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblPeriodCredits[intPeriod] = dblAmount;
			}
		}

		public double GetCreditsForPeriod(int intPeriod)
		{
			double GetCreditsForPeriod = 0;
			if (intPeriod > 0 && intPeriod < 13)
			{
				GetCreditsForPeriod = dblPeriodCredits[intPeriod];
			}
			return GetCreditsForPeriod;
		}

		public void SetDebitsForPeriod(int intPeriod, ref double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblPeriodDebits[intPeriod] = dblAmount;
			}
		}

		public double GetDebitsForPeriod(int intPeriod)
		{
			double GetDebitsForPeriod = 0;
			if (intPeriod > 0 && intPeriod < 13)
			{
				GetDebitsForPeriod = dblPeriodDebits[intPeriod];
			}
			return GetDebitsForPeriod;
		}

		public void SetNetForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblPeriodNet[intPeriod] = dblAmount;
			}
		}

		public double GetNetForPeriod(int intPeriod)
		{
			double GetNetForPeriod = 0;
			if (intPeriod > 0 && intPeriod < 13)
			{
				GetNetForPeriod = dblPeriodNet[intPeriod];
			}
			return GetNetForPeriod;
		}

		public void SetNetBeginForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblNetBegin[intPeriod] = dblAmount;
			}
		}

		public double GetNetBeginForPeriod(int intPeriod)
		{
			double GetNetBeginForPeriod = 0;
			if (intPeriod > 0 && intPeriod < 13)
			{
				GetNetBeginForPeriod = dblNetBegin[intPeriod];
			}
			return GetNetBeginForPeriod;
		}

		public void SetNetEndForPeriod(int intPeriod, double dblAmount)
		{
			if (intPeriod > 0 && intPeriod < 13)
			{
				dblNetEnd[intPeriod] = dblAmount;
			}
		}

		public double GetNetEndForPeriod(int intPeriod)
		{
			double GetNetEndForPeriod = 0;
			if (intPeriod > 0 && intPeriod < 13)
			{
				GetNetEndForPeriod = dblNetEnd[intPeriod];
			}
			return GetNetEndForPeriod;
		}

		public double BudgetAdjustment
		{
			set
			{
				dblBudgetAdjustment = value;
			}
			get
			{
				double BudgetAdjustment = 0;
				BudgetAdjustment = dblBudgetAdjustment;
				return BudgetAdjustment;
			}
		}

		public bool IsGeneralLedger
		{
			get
			{
				bool IsGeneralLedger = false;
				IsGeneralLedger = IsAsset || IsFundBalance || IsLiability;
				return IsGeneralLedger;
			}
		}

		public bool IsRevenue
		{
			get
			{
				bool IsRevenue = false;
				IsRevenue = Strings.LCase(strAcctType) == "r";
				return IsRevenue;
			}
		}

		public bool IsExpense
		{
			get
			{
				bool IsExpense = false;
				IsExpense = Strings.LCase(strAcctType) == "e";
				return IsExpense;
			}
		}

		public bool IsFundBalance
		{
			get
			{
				bool IsFundBalance = false;
				IsFundBalance = Strings.LCase(strAcctType) == "f";
				return IsFundBalance;
			}
		}

		public bool IsLiability
		{
			get
			{
				bool IsLiability = false;
				IsLiability = Strings.LCase(strAcctType) == "l";
				return IsLiability;
			}
		}

		public bool IsAsset
		{
			get
			{
				bool IsAsset = false;
				IsAsset = Strings.LCase(strAcctType) == "a";
				return IsAsset;
			}
		}

		public string AccountType
		{
			set
			{
				strAcctType = value;
			}
			get
			{
				string AccountType = "";
				AccountType = strAcctType;
				return AccountType;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double Debits
		{
			set
			{
				dblDebits = value;
			}
			get
			{
				double Debits = 0;
				Debits = dblDebits;
				return Debits;
			}
		}

		public double Credits
		{
			set
			{
				dblCredits = value;
			}
			get
			{
				double Credits = 0;
				Credits = dblCredits;
				return Credits;
			}
		}

		public double BeginningBalance
		{
			set
			{
				dblBeginningBalance = value;
			}
			get
			{
				double BeginningBalance = 0;
				BeginningBalance = dblBeginningBalance;
				return BeginningBalance;
			}
		}

		public double EndBalance
		{
			set
			{
				dblEndBalance = value;
			}
			get
			{
				double EndBalance = 0;
				EndBalance = dblEndBalance;
				return EndBalance;
			}
		}

		public double Net
		{
			set
			{
				dblNet = value;
			}
			get
			{
				double Net = 0;
				Net = dblNet;
				return Net;
			}
		}

		public void SetTotalsByPeriodRange(int intStart, int intEnd)
		{
			// recalc the beginning net, ending net, net, credits and debits to reflect only the period range
			if (intStart > 0 && intStart < 13 && intEnd > 0 && intEnd < 13)
			{
				int intCount = 0;
				intCount = intStart;
				double dblTempNet = 0;
				double dblTempCreds = 0;
				double dblTempDebs = 0;
				dblBeginningNet = GetNetBeginForPeriod(intStart);
				dblEndBalance = GetNetEndForPeriod(intEnd);
				while (intCount != intEnd)
				{
					dblTempNet += GetNetForPeriod(intCount);
					dblTempCreds += GetCreditsForPeriod(intCount);
					dblTempDebs += GetDebitsForPeriod(intCount);
					intCount += 1;
					if (intCount > 12)
					{
						intCount = 1;
					}
				}
				dblTempNet += GetNetForPeriod(intEnd);
				dblTempCreds += GetCreditsForPeriod(intEnd);
				dblTempDebs += GetDebitsForPeriod(intEnd);
				dblCredits = dblTempCreds;
				dblDebits = dblTempDebs;
				dblNet = dblTempNet;
			}
		}

		public void CalcNet()
		{
			Net = Debits - Credits;
		}

		public void CalcEndBalance()
		{
			if (!IsExpense)
			{
				EndBalance = BeginningBalance + BudgetAdjustment + (Debits - Credits);
			}
		}
	}
}
