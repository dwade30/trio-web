﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptJournalAccountDetail.
	/// </summary>
	partial class rptJournalAccountDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptJournalAccountDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldGrandTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotalDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDeptRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalDebit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDebit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPeriod,
				this.fldJournal,
				this.fldDate,
				this.fldDescription,
				this.fldRCB,
				this.fldType,
				this.fldCredit,
				this.fldDebit
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.1875F;
			this.fldPeriod.Left = 0.4375F;
			this.fldPeriod.MultiLine = false;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldPeriod.Text = "Field36";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.25F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 0.75F;
			this.fldJournal.MultiLine = false;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldJournal.Text = "Field36";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.34375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 1.90625F;
			this.fldDate.MultiLine = false;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldDate.Text = "Field36";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.59375F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 2.5625F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldDescription.Text = "Field36";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.71875F;
			// 
			// fldRCB
			// 
			this.fldRCB.Height = 0.1875F;
			this.fldRCB.Left = 1.15625F;
			this.fldRCB.MultiLine = false;
			this.fldRCB.Name = "fldRCB";
			this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldRCB.Text = "Field36";
			this.fldRCB.Top = 0F;
			this.fldRCB.Width = 0.3125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 1.5F;
			this.fldType.MultiLine = false;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldType.Text = "Field36";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// fldCredit
			// 
			this.fldCredit.Height = 0.1875F;
			this.fldCredit.Left = 6.40625F;
			this.fldCredit.MultiLine = false;
			this.fldCredit.Name = "fldCredit";
			this.fldCredit.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCredit.Text = "Field36";
			this.fldCredit.Top = 0F;
			this.fldCredit.Width = 1.03125F;
			// 
			// fldDebit
			// 
			this.fldDebit.Height = 0.1875F;
			this.fldDebit.Left = 5.25F;
			this.fldDebit.MultiLine = false;
			this.fldDebit.Name = "fldDebit";
			this.fldDebit.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldDebit.Text = "Field36";
			this.fldDebit.Top = 0F;
			this.fldDebit.Width = 1.03125F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldGrandTotalCredit,
				this.Label9,
				this.fldGrandTotalDebit,
				this.Line4
			});
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldGrandTotalCredit
			// 
			this.fldGrandTotalCredit.Height = 0.1875F;
			this.fldGrandTotalCredit.Left = 6.40625F;
			this.fldGrandTotalCredit.Name = "fldGrandTotalCredit";
			this.fldGrandTotalCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGrandTotalCredit.Text = "Field37";
			this.fldGrandTotalCredit.Top = 0.0625F;
			this.fldGrandTotalCredit.Width = 1.03125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.34375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label9.Text = "Final Total";
			this.Label9.Top = 0.0625F;
			this.Label9.Width = 0.8125F;
			// 
			// fldGrandTotalDebit
			// 
			this.fldGrandTotalDebit.Height = 0.1875F;
			this.fldGrandTotalDebit.Left = 5.25F;
			this.fldGrandTotalDebit.Name = "fldGrandTotalDebit";
			this.fldGrandTotalDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGrandTotalDebit.Text = "Field37";
			this.fldGrandTotalDebit.Top = 0.0625F;
			this.fldGrandTotalDebit.Width = 1.03125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 4.84375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.0625F;
			this.Line4.Width = 2.65625F;
			this.Line4.X1 = 4.84375F;
			this.Line4.X2 = 7.5F;
			this.Line4.Y1 = 0.0625F;
			this.Line4.Y2 = 0.0625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblDateRange,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblJournal,
				this.Line1,
				this.lblDate,
				this.lblPeriod,
				this.lblDescription,
				this.lblType,
				this.lblRCB,
				this.lblAmount,
				this.lblDeptRange,
				this.Field35
			});
			this.PageHeader.Height = 1.020833F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Journal Account Detail Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.5F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.5F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label6";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 4.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblJournal
			// 
			this.lblJournal.Height = 0.1875F;
			this.lblJournal.Left = 0.75F;
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblJournal.Tag = " ";
			this.lblJournal.Text = "Jrnl";
			this.lblJournal.Top = 0.6875F;
			this.lblJournal.Width = 0.34375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.Left = 1.90625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblDate.Tag = " ";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.6875F;
			this.lblDate.Width = 0.59375F;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Height = 0.1875F;
			this.lblPeriod.Left = 0.4375F;
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblPeriod.Tag = " ";
			this.lblPeriod.Text = "Per";
			this.lblPeriod.Top = 0.6875F;
			this.lblPeriod.Width = 0.25F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.Left = 2.5625F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblDescription.Tag = " ";
			this.lblDescription.Text = "Description";
			this.lblDescription.Top = 0.6875F;
			this.lblDescription.Width = 1.71875F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.Left = 1.5F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblType.Tag = " ";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.6875F;
			this.lblType.Width = 0.34375F;
			// 
			// lblRCB
			// 
			this.lblRCB.Height = 0.1875F;
			this.lblRCB.Left = 1.15625F;
			this.lblRCB.Name = "lblRCB";
			this.lblRCB.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblRCB.Tag = " ";
			this.lblRCB.Text = "RCB";
			this.lblRCB.Top = 0.6875F;
			this.lblRCB.Width = 0.3125F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.Left = 6.40625F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblAmount.Tag = " ";
			this.lblAmount.Text = "Credit";
			this.lblAmount.Top = 0.6875F;
			this.lblAmount.Width = 1.03125F;
			// 
			// lblDeptRange
			// 
			this.lblDeptRange.Height = 0.1875F;
			this.lblDeptRange.HyperLink = null;
			this.lblDeptRange.Left = 1.5F;
			this.lblDeptRange.Name = "lblDeptRange";
			this.lblDeptRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDeptRange.Text = "Label6";
			this.lblDeptRange.Top = 0.40625F;
			this.lblDeptRange.Width = 4.5F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.1875F;
			this.Field35.Left = 5.25F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field35.Tag = " ";
			this.Field35.Text = "Debit";
			this.Field35.Top = 0.6875F;
			this.Field35.Width = 1.03125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.1979167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// lblAccount
			// 
			this.lblAccount.DataField = "GroupTitle";
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblAccount.Text = null;
			this.lblAccount.Top = 0F;
			this.lblAccount.Width = 7.4375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalCredit,
				this.Label8,
				this.fldTotalDebit,
				this.Line3
			});
			this.GroupFooter1.Height = 0.1770833F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// fldTotalCredit
			// 
			this.fldTotalCredit.Height = 0.1875F;
			this.fldTotalCredit.Left = 6.40625F;
			this.fldTotalCredit.Name = "fldTotalCredit";
			this.fldTotalCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalCredit.Text = "Field37";
			this.fldTotalCredit.Top = 0F;
			this.fldTotalCredit.Width = 1.03125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.71875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label8.Text = "Total";
			this.Label8.Top = 0F;
			this.Label8.Width = 0.5F;
			// 
			// fldTotalDebit
			// 
			this.fldTotalDebit.Height = 0.1875F;
			this.fldTotalDebit.Left = 5.25F;
			this.fldTotalDebit.Name = "fldTotalDebit";
			this.fldTotalDebit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalDebit.Text = "Field37";
			this.fldTotalDebit.Top = 0F;
			this.fldTotalDebit.Width = 1.03125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.78125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.71875F;
			this.Line3.X1 = 4.78125F;
			this.Line3.X2 = 7.5F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// rptJournalAccountDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptJournalAccountDetail_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotalDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDebit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebit;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotalDebit;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblJournal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeptRange;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDebit;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
	}
}
