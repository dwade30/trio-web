﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorExtract.
	/// </summary>
	public partial class frmVendorExtract : BaseForm
	{
		public frmVendorExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVendorExtract InstancePtr
		{
			get
			{
				return (frmVendorExtract)Sys.GetInstance(typeof(frmVendorExtract));
			}
		}

		protected frmVendorExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         1/31/03
		// This form will be used to select which items of data you
		// would like extracted for the vendor database extract
		// ********************************************************
		private void frmVendorExtract_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmVendorExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVendorExtract.FillStyle	= 0;
			//frmVendorExtract.ScaleWidth	= 5880;
			//frmVendorExtract.ScaleHeight	= 3810;
			//frmVendorExtract.LinkTopic	= "Form2";
			//frmVendorExtract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmVendorExtract_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			bool blnItemsSelected;
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			int intItems;
			int intItemsWritten = 0;
			blnItemsSelected = false;
			intItems = 0;
			if (chkAddress.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkCheckName.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkClass.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkComments.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkEmail.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkContact.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkCorrName.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkStatus.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkTelephone.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (chkVendorNumber.CheckState == CheckState.Checked)
			{
				blnItemsSelected = true;
				intItems += 1;
			}
			if (blnItemsSelected == false)
			{
				MessageBox.Show("You must select at least 1 item of data before you may continue", "Select Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Creating Extract";
			frmWait.InstancePtr.Refresh();
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, "VdExtrct.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				PrintTitles(ref intItems);
				do
				{
					//Application.DoEvents();
					intItemsWritten = 0;
					if (chkAddress.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("CheckAddress1"), rsVendorInfo.Get_Fields_String("CheckAddress2"), rsVendorInfo.Get_Fields_String("CheckAddress3"), rsVendorInfo.Get_Fields_String("CheckCity"), rsVendorInfo.Get_Fields_String("CheckState"), rsVendorInfo.Get_Fields_String("CheckZip"), rsVendorInfo.Get_Fields_String("CheckZip4"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("CheckAddress1"), rsVendorInfo.Get_Fields_String("CheckAddress2"), rsVendorInfo.Get_Fields_String("CheckAddress3"), rsVendorInfo.Get_Fields_String("CheckCity"), rsVendorInfo.Get_Fields_String("CheckState"), rsVendorInfo.Get_Fields_String("CheckZip"), rsVendorInfo.Get_Fields_String("CheckZip4"));
						}
					}
					if (chkCheckName.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("CheckName"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("CheckName"));
						}
					}
					if (chkClass.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("Class"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("Class"));
						}
					}
					if (chkComments.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("Message"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("Message"));
						}
					}
					if (chkEmail.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("Email"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("Email"));
						}
					}
					if (chkContact.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("Contact"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("Contact"));
						}
					}
					if (chkCorrName.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("CorrespondName"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("CorrespondName"));
						}
					}
					if (chkStatus.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("Status"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("Status"));
						}
					}
					if (chkTelephone.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsVendorInfo.Get_Fields_String("TelephoneNumber"));
						}
						else
						{
							FCFileSystem.Write(1, rsVendorInfo.Get_Fields_String("TelephoneNumber"));
						}
					}
					if (chkVendorNumber.CheckState == CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, modValidateAccount.GetFormat_6(FCConvert.ToString(rsVendorInfo.Get_Fields_Int32("VendorNumber")), 5));
						}
						else
						{
							FCFileSystem.Write(1, modValidateAccount.GetFormat_6(FCConvert.ToString(rsVendorInfo.Get_Fields_Int32("VendorNumber")), 5));
						}
					}
					rsVendorInfo.MoveNext();
				}
				while (rsVendorInfo.EndOfFile() != true);
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				//MessageBox.Show(FCFileSystem.Statics.UserDataFolder + "\\VdExtrct.txt has been created");
				FCUtils.Download(System.IO.Path.Combine(FCFileSystem.Statics.UserDataFolder, "VdExtrct.txt"));
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Vendor Information Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			FCFileSystem.FileClose(1);
		}
		// vbPorter upgrade warning: intTotal As short	OnWriteFCConvert.ToInt32(
		private void PrintTitles(ref int intTotal)
		{
			int intItems;
			intItems = 0;
			if (chkAddress.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Address1", "Address2", "Address3", "Address4");
				}
				else
				{
					FCFileSystem.Write(1, "Address1", "Address2", "Address3", "Address4");
				}
			}
			if (chkCheckName.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Check Name");
				}
				else
				{
					FCFileSystem.Write(1, "Check Name");
				}
			}
			if (chkClass.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Class");
				}
				else
				{
					FCFileSystem.Write(1, "Class");
				}
			}
			if (chkComments.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Comments");
				}
				else
				{
					FCFileSystem.Write(1, "Comments");
				}
			}
			if (chkEmail.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "E-Mail Address");
				}
				else
				{
					FCFileSystem.Write(1, "E-Mail Address");
				}
			}
			if (chkContact.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Contact Name");
				}
				else
				{
					FCFileSystem.Write(1, "Contact Name");
				}
			}
			if (chkCorrName.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Correspondence Name");
				}
				else
				{
					FCFileSystem.Write(1, "Correspondence Name");
				}
			}
			if (chkStatus.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Status");
				}
				else
				{
					FCFileSystem.Write(1, "Status");
				}
			}
			if (chkTelephone.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Telephone #");
				}
				else
				{
					FCFileSystem.Write(1, "Telephone #");
				}
			}
			if (chkVendorNumber.CheckState == CheckState.Checked)
			{
				intItems += 1;
				if (intItems == intTotal)
				{
					FCFileSystem.WriteLine(1, "Vendor Number");
				}
				else
				{
					FCFileSystem.Write(1, "Vendor Number");
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
