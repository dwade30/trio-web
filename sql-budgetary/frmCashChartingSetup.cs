﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCashChartingSetup.
	/// </summary>
	public partial class frmCashChartingSetup : BaseForm
	{
		public frmCashChartingSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			cmbYTDTotals.SelectedIndex = 1;
			cmbRange.SelectedIndex = 2;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCashChartingSetup InstancePtr
		{
			get
			{
				return (frmCashChartingSetup)Sys.GetInstance(typeof(frmCashChartingSetup));
			}
		}

		protected frmCashChartingSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         9/6/02
		// This form will be used by towns to setup a cash charting
		// graph for them to view or print out
		// ********************************************************
		private struct GraphItem
		{
			public string Title;
			public string Type;
			public string AccountSelectionType;
			public string Low;
			public string High;
			public string SelectedAccounts;
			public string SelectedLiabilities;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public GraphItem(int unusedParam)
			{
				this.Title = string.Empty;
				this.Type = string.Empty;
				this.AccountSelectionType = string.Empty;
				this.Low = string.Empty;
				this.High = string.Empty;
				this.SelectedAccounts = string.Empty;
				this.SelectedLiabilities = string.Empty;
			}
		};

		bool blnEdit;
		int intSelected;
		GraphItem[] grpItems = null;
		int intItems;
		bool blnDirty;
		int KeyCol;
		int LineCol;
		int TypeCol;
		int TitleCol;
		public bool blnReportAlreadyExists;
		public int OldChartKey;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		private clsGridAccount vsLowGrid_AutoInitialized;

		private clsGridAccount vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		private clsGridAccount vsHighGrid_AutoInitialized;

		private clsGridAccount vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowRevGrid = new clsGridAccount();
		private clsGridAccount vsLowRevGrid_AutoInitialized;

		private clsGridAccount vsLowRevGrid
		{
			get
			{
				if (vsLowRevGrid_AutoInitialized == null)
				{
					vsLowRevGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowRevGrid_AutoInitialized;
			}
			set
			{
				vsLowRevGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighRevGrid = new clsGridAccount();
		private clsGridAccount vsHighRevGrid_AutoInitialized;

		private clsGridAccount vsHighRevGrid
		{
			get
			{
				if (vsHighRevGrid_AutoInitialized == null)
				{
					vsHighRevGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighRevGrid_AutoInitialized;
			}
			set
			{
				vsHighRevGrid_AutoInitialized = value;
			}
		}

		private void cboCashSelection_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int counter;
			lblAccountTitle.Text = "";
			if (cboCashSelection.SelectedIndex == 1)
			{
				// Net Amount
				lblLiability.Visible = true;
				vsSelectedLiabilities.Visible = true;
				cmdLiabilityClear.Visible = true;
				cmdLiabilitySelectAll.Visible = true;
				vsSelectedCash.Focus();
			}
			else if (cboCashSelection.SelectedIndex == 0)
			{
				// Total Amount
				lblLiability.Visible = false;
				vsSelectedLiabilities.Visible = false;
				for (counter = 1; counter <= vsSelectedLiabilities.Rows - 1; counter++)
				{
					vsSelectedLiabilities.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cmdLiabilityClear.Visible = false;
				cmdLiabilitySelectAll.Visible = false;
				vsSelectedCash.Focus();
			}
		}

		private void cboExpenseSelection_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int counter;
			lblAccountTitle.Text = "";
			if (cboExpenseSelection.SelectedIndex == 0)
			{
				// All Accounts
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboSingleDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = false;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = false;
				cmdExpenseClear.Visible = false;
				cmdExpenseSelectAll.Visible = false;
				fraExpenseAccountRange.Visible = false;
				cmdCreate.Focus();
			}
			else if (cboExpenseSelection.SelectedIndex == 1)
			{
				// Single Department
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = true;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = false;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = false;
				cmdExpenseClear.Visible = false;
				cmdExpenseSelectAll.Visible = false;
				fraExpenseAccountRange.Visible = true;
				fraExpenseAccountRange.Text = "Single Department";
				cboSingleDept.Focus();
			}
			else if (cboExpenseSelection.SelectedIndex == 2)
			{
				// Single Expense
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboSingleDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = false;
				cboSingleExpense.Visible = true;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = false;
				cmdExpenseClear.Visible = false;
				cmdExpenseSelectAll.Visible = false;
				fraExpenseAccountRange.Visible = true;
				fraExpenseAccountRange.Text = "Single Expense";
				cboSingleExpense.Focus();
			}
			else if (cboExpenseSelection.SelectedIndex == 3)
			{
				// Range of Deaprtments
				cboSingleDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = true;
				cboEndingDept.Visible = true;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = true;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = false;
				cmdExpenseClear.Visible = false;
				cmdExpenseSelectAll.Visible = false;
				fraExpenseAccountRange.Visible = true;
				fraExpenseAccountRange.Text = "Department Range";
				cboBeginningDept.Focus();
			}
			else if (cboExpenseSelection.SelectedIndex == 4)
			{
				// Range of Expenses
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboSingleDept.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = true;
				cboEndingExpense.Visible = true;
				lblTo[2].Visible = true;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = false;
				cmdExpenseClear.Visible = false;
				cmdExpenseSelectAll.Visible = false;
				fraExpenseAccountRange.Visible = true;
				fraExpenseAccountRange.Text = "Expense Range";
				cboBeginningExpense.Focus();
			}
			else if (cboExpenseSelection.SelectedIndex == 5)
			{
				// Range of Accounts
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboSingleDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = true;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = true;
				vsLowAccount.Visible = true;
				vsSelectedExpenses.Visible = false;
				cmdExpenseClear.Visible = false;
				cmdExpenseSelectAll.Visible = false;
				fraExpenseAccountRange.Visible = true;
				fraExpenseAccountRange.Text = "Account Range";
				vsLowAccount.Focus();
			}
			else if (cboExpenseSelection.SelectedIndex == 6)
			{
				// Selected Accounts
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboSingleDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = false;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = true;
				cmdExpenseClear.Visible = true;
				cmdExpenseSelectAll.Visible = true;
				fraExpenseAccountRange.Visible = false;
				vsSelectedExpenses.Focus();
			}
		}

		private void cboGraphType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboGraphType.SelectedIndex == 0)
			{
				fraExpense.Visible = true;
				cboExpenseSelection.SelectedIndex = 0;
				cboExpenseSelection.Focus();
				fraRevenue.Visible = false;
				fraCash.Visible = false;
				fraTax.Visible = false;
			}
			else if (cboGraphType.SelectedIndex == 1)
			{
				fraExpense.Visible = false;
				fraRevenue.Visible = true;
				cboRevenueSelection.SelectedIndex = 0;
				cboRevenueSelection.Focus();
				fraCash.Visible = false;
				fraTax.Visible = false;
			}
			else if (cboGraphType.SelectedIndex == 2)
			{
				fraExpense.Visible = false;
				fraRevenue.Visible = false;
				fraCash.Visible = true;
				cboCashSelection.SelectedIndex = 0;
				cboCashSelection.Focus();
				fraTax.Visible = false;
			}
			else if (cboGraphType.SelectedIndex == 3)
			{
				fraExpense.Visible = false;
				fraRevenue.Visible = false;
				fraCash.Visible = false;
				fraTax.Visible = true;
				vsSelectedTax.Focus();
			}
			ClearFrames();
		}

		private void cboRevenueSelection_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int counter;
			lblAccountTitle.Text = "";
			if (cboRevenueSelection.SelectedIndex == 0)
			{
				// All Accounts
				cboBeginningRev.SelectedIndex = -1;
				cboEndingRev.SelectedIndex = -1;
				cboSingleRev.SelectedIndex = -1;
				vsHighRevAccount.TextMatrix(0, 0, "");
				vsLowRevAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningRev.Visible = false;
				cboEndingRev.Visible = false;
				lblTo[0].Visible = false;
				cboSingleRev.Visible = false;
				vsHighRevAccount.Visible = false;
				vsLowRevAccount.Visible = false;
				vsSelectedRevenues.Visible = false;
				cmdRevenueClear.Visible = false;
				cmdRevenueSelectAll.Visible = false;
				fraRevenueAccountRange.Visible = false;
				cmdCreate.Focus();
			}
			else if (cboRevenueSelection.SelectedIndex == 1)
			{
				// Single Department
				cboBeginningRev.SelectedIndex = -1;
				cboEndingRev.SelectedIndex = -1;
				vsHighRevAccount.TextMatrix(0, 0, "");
				vsLowRevAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningRev.Visible = false;
				cboEndingRev.Visible = false;
				lblTo[0].Visible = false;
				cboSingleRev.Visible = true;
				vsHighRevAccount.Visible = false;
				vsLowRevAccount.Visible = false;
				vsSelectedRevenues.Visible = false;
				cmdRevenueClear.Visible = false;
				cmdRevenueSelectAll.Visible = false;
				fraRevenueAccountRange.Visible = true;
				fraRevenueAccountRange.Text = "Single Department";
				cboSingleRev.Focus();
			}
			else if (cboRevenueSelection.SelectedIndex == 2)
			{
				// Range of Departments
				cboSingleRev.SelectedIndex = -1;
				vsHighRevAccount.TextMatrix(0, 0, "");
				vsLowRevAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningRev.Visible = true;
				cboEndingRev.Visible = true;
				lblTo[0].Visible = true;
				cboSingleRev.Visible = false;
				vsHighRevAccount.Visible = false;
				vsLowRevAccount.Visible = false;
				vsSelectedRevenues.Visible = false;
				cmdRevenueClear.Visible = false;
				cmdRevenueSelectAll.Visible = false;
				fraRevenueAccountRange.Visible = true;
				fraRevenueAccountRange.Text = "Department Range";
				cboBeginningRev.Focus();
			}
			else if (cboRevenueSelection.SelectedIndex == 3)
			{
				// Range of Accounts
				cboBeginningRev.SelectedIndex = -1;
				cboEndingRev.SelectedIndex = -1;
				cboSingleRev.SelectedIndex = -1;
				for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningRev.Visible = false;
				cboEndingRev.Visible = false;
				cboSingleRev.Visible = false;
				vsHighRevAccount.Visible = true;
				vsLowRevAccount.Visible = true;
				lblTo[0].Visible = true;
				vsSelectedRevenues.Visible = false;
				cmdRevenueClear.Visible = false;
				cmdRevenueSelectAll.Visible = false;
				fraRevenueAccountRange.Visible = true;
				fraRevenueAccountRange.Text = "Account Range";
				vsLowRevAccount.Focus();
			}
			else if (cboRevenueSelection.SelectedIndex == 4)
			{
				// Selected Accounts
				cboBeginningRev.SelectedIndex = -1;
				cboEndingRev.SelectedIndex = -1;
				cboSingleRev.SelectedIndex = -1;
				vsHighRevAccount.TextMatrix(0, 0, "");
				vsLowRevAccount.TextMatrix(0, 0, "");
				cboBeginningRev.Visible = false;
				cboEndingRev.Visible = false;
				lblTo[0].Visible = false;
				cboSingleRev.Visible = false;
				vsHighRevAccount.Visible = false;
				vsLowRevAccount.Visible = false;
				vsSelectedRevenues.Visible = true;
				cmdRevenueClear.Visible = true;
				cmdRevenueSelectAll.Visible = true;
				fraRevenueAccountRange.Visible = false;
				vsSelectedRevenues.Focus();
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			blnEdit = false;
			fraGraphItem.Visible = true;
		}

		private void cmdAddExisting_Click(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (cboExistingItems.SelectedIndex > -1)
			{
				for (counter = 1; counter <= vsGraph.Rows - 1; counter++)
				{
					if (cboExistingItems.Text == vsGraph.TextMatrix(counter, TitleCol))
					{
						MessageBox.Show("You can't add this item because it is already in the chart", "Unable to Add Item", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			rsInfo.OpenRecordset("SELECT * FROM GraphItems WHERE Title = '" + cboExistingItems.Text + "'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				intItems += 1;
				Array.Resize(ref grpItems, intItems + 1);
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				grpItems[intItems - 1] = new GraphItem(0);
				grpItems[intItems - 1].Title = FCConvert.ToString(rsInfo.Get_Fields_String("Title"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				grpItems[intItems - 1].Type = FCConvert.ToString(rsInfo.Get_Fields("Type"));
				grpItems[intItems - 1].AccountSelectionType = FCConvert.ToString(rsInfo.Get_Fields_String("AccountSelectionType"));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				grpItems[intItems - 1].Low = FCConvert.ToString(rsInfo.Get_Fields("Low"));
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				grpItems[intItems - 1].High = FCConvert.ToString(rsInfo.Get_Fields("High"));
				grpItems[intItems - 1].SelectedAccounts = FCConvert.ToString(rsInfo.Get_Fields_String("SelectedAccounts"));
				grpItems[intItems - 1].SelectedLiabilities = FCConvert.ToString(rsInfo.Get_Fields_String("SelectedLiabilities"));
				vsGraph.Rows += 1;
				vsGraph.TextMatrix(vsGraph.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
				vsGraph.TextMatrix(vsGraph.Rows - 1, LineCol, Strings.Trim((vsGraph.Rows - 1).ToString()));
				vsGraph.TextMatrix(vsGraph.Rows - 1, TypeCol, grpItems[intItems - 1].Type);
				vsGraph.TextMatrix(vsGraph.Rows - 1, TitleCol, grpItems[intItems - 1].Title);
				vsGraph.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsGraph.Rows - 1, 0, vsGraph.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				blnDirty = true;
				cboExistingItems.SelectedIndex = -1;
				fraExistingItem.Visible = false;
			}
			else
			{
				MessageBox.Show("Item could not be found", "Unable to Find Item", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			txtTitle.Text = "";
			cboGraphType.SelectedIndex = -1;
			fraExpense.Visible = false;
			fraRevenue.Visible = false;
			fraCash.Visible = false;
			fraTax.Visible = false;
			ClearFrames();
			fraGraphItem.Visible = false;
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdCancelAdd_Click(object sender, System.EventArgs e)
		{
			cboExistingItems.SelectedIndex = -1;
			fraExistingItem.Visible = false;
		}

		private void cmdCancelSelection_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("If you exit this form, all of your information will be lost.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					Close();
				}
			}
			else
			{
				Close();
			}
		}

		private void cmdCashClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
			{
				vsSelectedCash.TextMatrix(counter, 0, FCConvert.ToString(false));
			}
		}

		private void cmdCashSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
			{
				vsSelectedCash.TextMatrix(counter, 0, FCConvert.ToString(true));
			}
		}

		private void cmdCreate_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnSelected = false;
			clsDRWrapper rsGraphItem = new clsDRWrapper();
			int lngKey = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (Strings.Trim(txtTitle.Text) == "")
			{
				MessageBox.Show("You must enter a title for this item before you may proceed", "No Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			blnDirty = true;
			if (cboGraphType.Items[cboGraphType.SelectedIndex].ToString() == "Town Expense")
			{
				// Expense Item
				if (cboExpenseSelection.SelectedIndex == 0)
				{
					// All Accounts
					// no need to check anything
				}
				else if (cboExpenseSelection.SelectedIndex == 1)
				{
					// Single Department
					if (cboSingleDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must select a department before you may proceed", "No Department Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboExpenseSelection.SelectedIndex == 2)
				{
					// Single Expense
					if (cboSingleExpense.SelectedIndex == -1)
					{
						MessageBox.Show("You must select an expense before you may proceed", "No Expense Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboExpenseSelection.SelectedIndex == 3)
				{
					// Range of Deaprtments
					if (cboBeginningDept.SelectedIndex == -1 || cboEndingDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must select a range of departments before you may proceed", "No Department Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (cboBeginningDept.SelectedIndex >= cboEndingDept.SelectedIndex)
					{
						MessageBox.Show("Your beginning department must be lower then your ending department before you may proceed", "Invalid Department Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboExpenseSelection.SelectedIndex == 4)
				{
					// Range of Expenses
					if (cboBeginningExpense.SelectedIndex == -1 || cboEndingExpense.SelectedIndex == -1)
					{
						MessageBox.Show("You must select a range of expenses before you may proceed", "No Expense Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (cboBeginningExpense.SelectedIndex >= cboEndingExpense.SelectedIndex)
					{
						MessageBox.Show("Your beginning expense must be lower then your ending expense before you may proceed", "Invalid Expense Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboExpenseSelection.SelectedIndex == 5)
				{
					// Range of Accounts
					if (Strings.InStr(1, "_", vsLowAccount.Text, CompareConstants.vbBinaryCompare) != 0 || Strings.InStr(1, "_", vsHighAccount.Text, CompareConstants.vbBinaryCompare) != 0)
					{
						MessageBox.Show("You must enter complete account numbers before you may proceed", "Invalid Account Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) >= 0)
					{
						MessageBox.Show("Your low account number must be less than your high account number before you may proceed", "Invalid Account Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboExpenseSelection.SelectedIndex == 6)
				{
					// Selected Accounts
					blnSelected = false;
					for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
					{
						if (FCUtils.CBool(vsSelectedExpenses.TextMatrix(counter, 0)) == true)
						{
							blnSelected = true;
						}
					}
					if (!blnSelected)
					{
						MessageBox.Show("You must select at least one expense account before you may proceed", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			else if (cboGraphType.Items[cboGraphType.SelectedIndex].ToString() == "Town Revenue")
			{
				// Revenue Item
				if (cboRevenueSelection.SelectedIndex == 0)
				{
					// All Accounts
					// Nothing to check
				}
				else if (cboRevenueSelection.SelectedIndex == 1)
				{
					// Single Department
					if (cboSingleRev.SelectedIndex == -1)
					{
						MessageBox.Show("You must select a department before you may proceed", "No Department Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboRevenueSelection.SelectedIndex == 2)
				{
					// Range of Departments
					if (cboBeginningRev.SelectedIndex == -1 || cboEndingRev.SelectedIndex == -1)
					{
						MessageBox.Show("You must select a range of departments before you may proceed", "No Department Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (cboBeginningRev.SelectedIndex >= cboEndingRev.SelectedIndex)
					{
						MessageBox.Show("Your beginning department must be lower then your ending department before you may proceed", "Invalid Department Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboRevenueSelection.SelectedIndex == 3)
				{
					// Range of Accounts
					if (Strings.InStr(1, "_", vsLowRevAccount.Text, CompareConstants.vbBinaryCompare) != 0 || Strings.InStr(1, "_", vsHighRevAccount.Text, CompareConstants.vbBinaryCompare) != 0)
					{
						MessageBox.Show("You must enter complete account numbers before you may proceed", "Invalid Account Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (fecherFoundation.Strings.CompareString(vsLowRevAccount.TextMatrix(0, 0), vsHighRevAccount.TextMatrix(0, 0), true) >= 0)
					{
						MessageBox.Show("Your low account number must be less than your high account number before you may proceed", "Invalid Account Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboRevenueSelection.SelectedIndex == 4)
				{
					// Selected Accounts
					blnSelected = false;
					for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
					{
						if (FCUtils.CBool(vsSelectedRevenues.TextMatrix(counter, 0)) == true)
						{
							blnSelected = true;
						}
					}
					if (!blnSelected)
					{
						MessageBox.Show("You must select at least one revenue account before you may proceed", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			else if (cboGraphType.Items[cboGraphType.SelectedIndex].ToString() == "Town Cash")
			{
				// Cash Item
				if (cboCashSelection.SelectedIndex == 1)
				{
					// Net Amount
					blnSelected = false;
					for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
					{
						if (FCUtils.CBool(vsSelectedCash.TextMatrix(counter, 0)) == true)
						{
							blnSelected = true;
						}
					}
					if (!blnSelected)
					{
						MessageBox.Show("You must select at least one cash account before you may proceed", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					blnSelected = false;
					for (counter = 1; counter <= vsSelectedLiabilities.Rows - 1; counter++)
					{
						if (FCUtils.CBool(vsSelectedLiabilities.TextMatrix(counter, 0)) == true)
						{
							blnSelected = true;
						}
					}
					if (!blnSelected)
					{
						MessageBox.Show("You must select at least one liability account before you may proceed", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cboCashSelection.SelectedIndex == 0)
				{
					// Total Amount
					blnSelected = false;
					for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
					{
						if (FCUtils.CBool(vsSelectedCash.TextMatrix(counter, 0)) == true)
						{
							blnSelected = true;
						}
					}
					if (!blnSelected)
					{
						MessageBox.Show("You must select at least one cash account before you may proceed", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			else if (cboGraphType.Items[cboGraphType.SelectedIndex].ToString() == "Town Tax Receivables")
			{
				// Tax Item
				blnSelected = false;
				for (counter = 1; counter <= vsSelectedTax.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsSelectedTax.TextMatrix(counter, 0)) == true)
					{
						blnSelected = true;
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least one tax receivable account before you may proceed", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				// None Selected
				MessageBox.Show("You must select a type and enter the information for that type before you may proceed", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (blnEdit)
			{
				if (cboGraphType.SelectedIndex == 0)
				{
					SaveExpenseItem(FCConvert.ToInt16(intSelected));
				}
				else if (cboGraphType.SelectedIndex == 1)
				{
					SaveRevenueItem(ref intSelected);
				}
				else if (cboGraphType.SelectedIndex == 2)
				{
					SaveCashItem(ref intSelected);
				}
				else if (cboGraphType.SelectedIndex == 3)
				{
					SaveTaxItem(ref intSelected);
				}
				rsGraphItem.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + FCConvert.ToString(Conversion.Val(vsGraph.TextMatrix(intSelected + 1, KeyCol))));
				rsGraphItem.Edit();
				rsGraphItem.Set_Fields("Title", grpItems[intSelected].Title);
				rsGraphItem.Set_Fields("Type", grpItems[intSelected].Type);
				rsGraphItem.Set_Fields("AccountSelectionType", grpItems[intSelected].AccountSelectionType);
				rsGraphItem.Set_Fields("Low", grpItems[intSelected].Low);
				rsGraphItem.Set_Fields("High", grpItems[intSelected].High);
				rsGraphItem.Set_Fields("SelectedAccounts", grpItems[intSelected].SelectedAccounts);
				rsGraphItem.Set_Fields("SelectedLiabilities", grpItems[intSelected].SelectedLiabilities);
				rsGraphItem.Update(true);
				vsGraph.TextMatrix(intSelected + 1, TypeCol, grpItems[intSelected].Type);
				vsGraph.TextMatrix(intSelected + 1, TitleCol, grpItems[intSelected].Title);
			}
			else
			{
				rsGraphItem.OpenRecordset("SELECT * FROM GraphItems WHERE Title = '" + Strings.Trim(txtTitle.Text) + "'");
				if (rsGraphItem.EndOfFile() != true && rsGraphItem.BeginningOfFile() != true)
				{
					ans = MessageBox.Show("A Graph Item already exists with this name.  Do you wish to overwrite this record?", "Overwrite Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						rsGraphItem.Edit();
					}
					else
					{
						return;
					}
				}
				else
				{
					rsGraphItem.AddNew();
				}
				intItems += 1;
				Array.Resize(ref grpItems, intItems + 1);
				if (cboGraphType.SelectedIndex == 0)
				{
					SaveExpenseItem_2(FCConvert.ToInt16(intItems - 1));
				}
				else if (cboGraphType.SelectedIndex == 1)
				{
					SaveRevenueItem_2(FCConvert.ToInt16(intItems - 1));
				}
				else if (cboGraphType.SelectedIndex == 2)
				{
					SaveCashItem_2(FCConvert.ToInt16(intItems - 1));
				}
				else if (cboGraphType.SelectedIndex == 3)
				{
					SaveTaxItem_2(FCConvert.ToInt16(intItems - 1));
				}
				rsGraphItem.Set_Fields("Title", grpItems[intItems - 1].Title);
				rsGraphItem.Set_Fields("Type", grpItems[intItems - 1].Type);
				rsGraphItem.Set_Fields("AccountSelectionType", grpItems[intItems - 1].AccountSelectionType);
				rsGraphItem.Set_Fields("Low", grpItems[intItems - 1].Low);
				rsGraphItem.Set_Fields("High", grpItems[intItems - 1].High);
				rsGraphItem.Set_Fields("SelectedAccounts", grpItems[intItems - 1].SelectedAccounts);
				rsGraphItem.Set_Fields("SelectedLiabilities", grpItems[intItems - 1].SelectedLiabilities);
				rsGraphItem.Update(true);
				lngKey = FCConvert.ToInt32(rsGraphItem.Get_Fields_Int32("ID"));
				vsGraph.Rows += 1;
				vsGraph.TextMatrix(vsGraph.Rows - 1, KeyCol, FCConvert.ToString(lngKey));
				vsGraph.TextMatrix(vsGraph.Rows - 1, LineCol, Strings.Trim((vsGraph.Rows - 1).ToString()));
				vsGraph.TextMatrix(vsGraph.Rows - 1, TypeCol, grpItems[intItems - 1].Type);
				vsGraph.TextMatrix(vsGraph.Rows - 1, TitleCol, grpItems[intItems - 1].Title);
				vsGraph.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsGraph.Rows - 1, 0, vsGraph.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			}
			blnEdit = false;
			intSelected = -1;
			cmdCancel_Click();
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			int counter2;
			if (vsGraph.Row >= 1)
			{
				ans = MessageBox.Show("Are you sure you wish to remove this item?", "Delete Graph Item?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					vsGraph.RemoveItem(vsGraph.Row);
					if (vsGraph.Rows > 1)
					{
						for (counter = vsGraph.Row; counter <= vsGraph.Rows - 1; counter++)
						{
							if (counter < intItems)
							{
								grpItems[counter - 1].AccountSelectionType = grpItems[counter].AccountSelectionType;
								grpItems[counter - 1].High = grpItems[counter].High;
								grpItems[counter - 1].Low = grpItems[counter].Low;
								grpItems[counter - 1].SelectedAccounts = grpItems[counter].SelectedAccounts;
								grpItems[counter - 1].SelectedLiabilities = grpItems[counter].SelectedLiabilities;
								grpItems[counter - 1].Title = grpItems[counter].Title;
								grpItems[counter - 1].Type = grpItems[counter].Type;
							}
							vsGraph.TextMatrix(counter, LineCol, Strings.Trim(counter.ToString()));
						}
					}
					intItems -= 1;
					Array.Resize(ref grpItems, intItems + 1);
				}
			}
		}

		private void cmdExpenseClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
			{
				vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
			}
		}

		private void cmdExpenseSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
			{
				vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(true));
			}
		}

		private void cmdLiabilityClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedLiabilities.Rows - 1; counter++)
			{
				vsSelectedLiabilities.TextMatrix(counter, 0, FCConvert.ToString(false));
			}
		}

		private void cmdLiabilitySelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedLiabilities.Rows - 1; counter++)
			{
				vsSelectedLiabilities.TextMatrix(counter, 0, FCConvert.ToString(true));
			}
		}

		private void cmdRevenueClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
			{
				vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(false));
			}
		}

		private void cmdRevenueSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
			{
				vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(true));
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void cmdGetExisting_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			cboExistingItems.Clear();
			rs.OpenRecordset("SELECT * FROM GraphItems ORDER BY ID");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					cboExistingItems.AddItem(rs.Get_Fields_String("Title"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			else
			{
				MessageBox.Show("There are no existing graph items to add", "No Existing Items", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			fraExistingItem.Visible = true;
			cboExistingItems.Focus();
		}

		private void cmdTaxClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedTax.Rows - 1; counter++)
			{
				vsSelectedTax.TextMatrix(counter, 0, FCConvert.ToString(false));
			}
		}

		private void cmdTaxSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsSelectedTax.Rows - 1; counter++)
			{
				vsSelectedTax.TextMatrix(counter, 0, FCConvert.ToString(true));
			}
		}

		private void frmCashChartingSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCashChartingSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuProcessSave_Click();
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowAccount, vsLowAccount.Row, vsLowAccount.Col, KeyCode, Shift, vsLowAccount.EditSelStart, vsLowAccount.EditText, vsLowAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsHighAccount, vsHighAccount.Row, vsHighAccount.Col, KeyCode, Shift, vsHighAccount.EditSelStart, vsHighAccount.EditText, vsHighAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWREVACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowRevAccount, vsLowRevAccount.Row, vsLowRevAccount.Col, KeyCode, Shift, vsLowRevAccount.EditSelStart, vsLowRevAccount.EditText, vsLowRevAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHREVACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsHighRevAccount, vsHighRevAccount.Row, vsHighRevAccount.Col, KeyCode, Shift, vsHighRevAccount.EditSelStart, vsHighRevAccount.EditText, vsHighRevAccount.EditSelLength);
			}
		}

		private void frmCashChartingSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCashChartingSetup.FillStyle	= 0;
			//frmCashChartingSetup.ScaleWidth	= 9045;
			//frmCashChartingSetup.ScaleHeight	= 7500;
			//frmCashChartingSetup.LinkTopic	= "Form2";
			//frmCashChartingSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter = 0;
			clsDRWrapper rs = new clsDRWrapper();
			string strAccount = "";
			clsDRWrapper rsRange = new clsDRWrapper();
			KeyCol = 0;
			LineCol = 1;
			TypeCol = 2;
			TitleCol = 3;
			blnDirty = false;
			vsGraph.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, KeyCol, 0, vsGraph.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsGraph.TextMatrix(0, LineCol, "Item");
			vsGraph.TextMatrix(0, TypeCol, "Type");
			vsGraph.TextMatrix(0, TitleCol, "Title");
			vsGraph.ColWidth(KeyCol, 0);
			//FC:FINAL:DDU:#i742 set cols width to fit well in datagridview
			vsGraph.ColWidth(LineCol, FCConvert.ToInt32(0.1 * vsGraph.WidthOriginal));
			vsGraph.ColWidth(TypeCol, FCConvert.ToInt32(0.1 * vsGraph.WidthOriginal));
			vsGraph.ColWidth(TitleCol, FCConvert.ToInt32(0.79 * vsGraph.WidthOriginal));
			vsSelectedExpenses.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSelectedRevenues.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSelectedCash.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSelectedLiabilities.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSelectedTax.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSelectedExpenses.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSelectedRevenues.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSelectedCash.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSelectedLiabilities.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSelectedTax.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSelectedExpenses.TextMatrix(0, 0, "Select");
			vsSelectedExpenses.TextMatrix(0, 1, "Account");
			vsSelectedRevenues.TextMatrix(0, 0, "Select");
			vsSelectedRevenues.TextMatrix(0, 1, "Account");
			vsSelectedCash.TextMatrix(0, 0, "Select");
			vsSelectedCash.TextMatrix(0, 1, "Account");
			vsSelectedLiabilities.TextMatrix(0, 0, "Select");
			vsSelectedLiabilities.TextMatrix(0, 1, "Account");
			vsSelectedTax.TextMatrix(0, 0, "Select");
			vsSelectedTax.TextMatrix(0, 1, "Account");
			//FC:FINAL:DDU:#i742 set cols width to fit well in datagridview
			vsSelectedExpenses.ColWidth(0, FCConvert.ToInt32(0.07 * vsSelectedExpenses.WidthOriginal));
			vsSelectedExpenses.ColWidth(1, FCConvert.ToInt32(0.91 * vsSelectedExpenses.WidthOriginal));
			vsSelectedRevenues.ColWidth(0, FCConvert.ToInt32(0.07 * vsSelectedRevenues.WidthOriginal));
			vsSelectedRevenues.ColWidth(1, FCConvert.ToInt32(0.91 * vsSelectedRevenues.WidthOriginal));
			vsSelectedCash.ColWidth(0, FCConvert.ToInt32(0.14 * vsSelectedCash.WidthOriginal));
			vsSelectedCash.ColWidth(1, FCConvert.ToInt32(0.82 * vsSelectedCash.WidthOriginal));
			vsSelectedLiabilities.ColWidth(0, FCConvert.ToInt32(0.14 * vsSelectedLiabilities.WidthOriginal));
			vsSelectedLiabilities.ColWidth(1, FCConvert.ToInt32(0.82 * vsSelectedLiabilities.WidthOriginal));
			vsSelectedTax.ColWidth(0, FCConvert.ToInt32(0.07 * vsSelectedTax.WidthOriginal));
			vsSelectedTax.ColWidth(1, FCConvert.ToInt32(0.91 * vsSelectedTax.WidthOriginal));
            //FC:FINAL:BBE:#497 - correct the column width
            vsHighAccount.ExtendLastCol = true;
            vsHighRevAccount.ExtendLastCol = true;
			vsLowAccount.ExtendLastCol = true;
            vsLowRevAccount.ExtendLastCol = true;
            //cboBeginningDept.Left = FCConvert.ToInt32(cboBeginningDept.Left + cboBeginningDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
            //cboBeginningRev.Left = FCConvert.ToInt32(cboBeginningRev.Left + cboBeginningRev.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
            //cboBeginningExpense.Left = FCConvert.ToInt32(cboBeginningExpense.Left + cboBeginningExpense.Width - (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) * 115 + 350));
            //cboSingleDept.Left = FCConvert.ToInt32(cboSingleDept.Left + (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
            //cboSingleRev.Left = FCConvert.ToInt32(cboSingleRev.Left + (cboSingleRev.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
            //cboSingleExpense.Left = FCConvert.ToInt32(cboSingleExpense.Left + (cboSingleExpense.Width - (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) * 115 + 350)) - (0.5 * (cboSingleExpense.Width - (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) * 115 + 350))));
            //cboBeginningDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
            //cboEndingDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
            //cboSingleDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
            //cboBeginningRev.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
            //cboEndingRev.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
            //cboSingleRev.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
            //cboBeginningExpense.Width = FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) * 115 + 350);
            //cboEndingExpense.Width = FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) * 115 + 350);
            //cboSingleExpense.Width = FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) * 115 + 350);
            if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboBeginningDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboBeginningRev.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingRev.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleRev.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Object = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Right(modAccountTitle.Statics.Exp, 2))) + "' ORDER BY Expense");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboBeginningExpense.AddItem(rs.Get_Fields_String("Expense") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingExpense.AddItem(rs.Get_Fields_String("Expense") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleExpense.AddItem(rs.Get_Fields_String("Expense") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			FillGraphItemsCombo();
			vsLowGrid.GRID7Light = vsLowAccount;
			vsLowGrid.DefaultAccountType = "E";
			vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsHighGrid.GRID7Light = vsHighAccount;
			vsHighGrid.DefaultAccountType = "E";
			vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsLowRevGrid.GRID7Light = vsLowRevAccount;
			vsLowRevGrid.DefaultAccountType = "R";
			vsLowRevGrid.AccountCol = -1;
			vsLowRevGrid.Validation = false;
			vsHighRevGrid.GRID7Light = vsHighRevAccount;
			vsHighRevGrid.DefaultAccountType = "R";
			vsHighRevGrid.AccountCol = -1;
			vsHighRevGrid.Validation = false;
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					strAccount = modBudgetaryMaster.CreateAccount_242(rs.Get_Fields_String("AccountType"), rs.Get_Fields_String("FirstAccountField"), rs.Get_Fields_String("SecondAccountField"), rs.Get_Fields_String("ThirdAccountField"), rs.Get_Fields_String("FourthAccountField"));
					strAccount = Strings.Left(strAccount, strAccount.Length - 1);
					vsSelectedExpenses.AddItem(false + "\t" + strAccount);
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					strAccount = modBudgetaryMaster.CreateAccount_242(rs.Get_Fields_String("AccountType"), rs.Get_Fields_String("FirstAccountField"), rs.Get_Fields_String("SecondAccountField"), rs.Get_Fields_String("ThirdAccountField"), rs.Get_Fields_String("FourthAccountField"));
					strAccount = Strings.Left(strAccount, strAccount.Length - 1);
					vsSelectedRevenues.AddItem(false + "\t" + strAccount);
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				rsRange.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'L'");
				if (rsRange.EndOfFile() != true && rsRange.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					if (Conversion.Val(rsRange.Get_Fields("Low")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' AND convert(int, SecondAccountField) < " + FCConvert.ToString(Conversion.Val(rsRange.Get_Fields("Low"))) + " ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
					}
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
				}
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' AND GLAccountType = 'A' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					strAccount = modBudgetaryMaster.CreateAccount_242(rs.Get_Fields_String("AccountType"), rs.Get_Fields_String("FirstAccountField"), rs.Get_Fields_String("SecondAccountField"), rs.Get_Fields_String("ThirdAccountField"), rs.Get_Fields_String("FourthAccountField"));
					strAccount = Strings.Left(strAccount, strAccount.Length - 1);
					vsSelectedCash.AddItem(false + "\t" + strAccount);
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				if (Conversion.Val(rsRange.Get_Fields("Low")) != 0 && Conversion.Val(rsRange.Get_Fields("High")) != 0)
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' AND convert(int, SecondAccountField) >= " + FCConvert.ToString(Conversion.Val(rsRange.Get_Fields("Low"))) + " AND convert(int, SecondAccountField) <= " + FCConvert.ToString(Conversion.Val(rsRange.Get_Fields("High"))) + " ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
				}
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' AND GLAccountType = 'L' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					strAccount = modBudgetaryMaster.CreateAccount_242(rs.Get_Fields_String("AccountType"), rs.Get_Fields_String("FirstAccountField"), rs.Get_Fields_String("SecondAccountField"), rs.Get_Fields_String("ThirdAccountField"), rs.Get_Fields_String("FourthAccountField"));
					strAccount = Strings.Left(strAccount, strAccount.Length - 1);
					vsSelectedLiabilities.AddItem(false + "\t" + strAccount);
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					strAccount = modBudgetaryMaster.CreateAccount_242(rs.Get_Fields_String("AccountType"), rs.Get_Fields_String("FirstAccountField"), rs.Get_Fields_String("SecondAccountField"), rs.Get_Fields_String("ThirdAccountField"), rs.Get_Fields_String("FourthAccountField"));
					strAccount = Strings.Left(strAccount, strAccount.Length - 1);
					vsSelectedTax.AddItem(false + "\t" + strAccount);
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			intItems = 0;
			if (blnReportAlreadyExists)
			{
				LoadChart();
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCashChartingSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraGraphItem.Visible == true)
				{
					cmdCancel_Click();
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(cmdProcessSave, new System.EventArgs());
		}

		private void cboBeginningDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboBeginningExpense_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningExpense.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingExpense_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingExpense.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleExpense_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleExpense.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboBeginningRev_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningRev.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingRev_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingRev.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleRev_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleRev.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void ClearFrames()
		{
			int counter;
			lblAccountTitle.Text = "";
			if (fraExpense.Visible == false)
			{
				cboExpenseSelection.SelectedIndex = -1;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboSingleDept.SelectedIndex = -1;
				cboBeginningExpense.SelectedIndex = -1;
				cboEndingExpense.SelectedIndex = -1;
				cboSingleExpense.SelectedIndex = -1;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboSingleDept.Visible = false;
				cboBeginningExpense.Visible = false;
				cboEndingExpense.Visible = false;
				lblTo[2].Visible = false;
				cboSingleExpense.Visible = false;
				vsHighAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsSelectedExpenses.Visible = false;
				fraExpenseAccountRange.Visible = false;
			}
			if (fraRevenue.Visible == false)
			{
				cboRevenueSelection.SelectedIndex = -1;
				cboBeginningRev.SelectedIndex = -1;
				cboEndingRev.SelectedIndex = -1;
				cboSingleRev.SelectedIndex = -1;
				vsHighRevAccount.TextMatrix(0, 0, "");
				vsLowRevAccount.TextMatrix(0, 0, "");
				for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				cboBeginningRev.Visible = false;
				cboEndingRev.Visible = false;
				lblTo[0].Visible = false;
				cboSingleRev.Visible = false;
				vsHighRevAccount.Visible = false;
				vsLowRevAccount.Visible = false;
				vsSelectedRevenues.Visible = false;
				fraRevenueAccountRange.Visible = false;
			}
			if (fraCash.Visible == false)
			{
				cboCashSelection.SelectedIndex = -1;
				lblLiability.Visible = false;
				for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
				{
					vsSelectedCash.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				for (counter = 1; counter <= vsSelectedLiabilities.Rows - 1; counter++)
				{
					vsSelectedLiabilities.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
				vsSelectedLiabilities.Visible = false;
			}
			if (fraTax.Visible == false)
			{
				for (counter = 1; counter <= vsSelectedTax.Rows - 1; counter++)
				{
					vsSelectedTax.TextMatrix(counter, 0, FCConvert.ToString(false));
				}
			}
		}

		private void optAllMonths_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningMonth.Visible = false;
			cboBeginningMonth.SelectedIndex = -1;
			cboEndingMonth.Visible = false;
			cboEndingMonth.SelectedIndex = -1;
			lblTo[1].Visible = false;
			cboSingleMonth.SelectedIndex = -1;
			cboSingleMonth.Visible = false;
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningMonth.Visible = true;
			cboEndingMonth.Visible = true;
			lblTo[1].Visible = true;
			cboSingleMonth.Visible = false;
			cboBeginningMonth.SelectedIndex = modBudgetaryMaster.Statics.FirstMonth - 1;
			cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
		}

		private void optSingle_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningMonth.Visible = false;
			cboBeginningMonth.SelectedIndex = -1;
			cboEndingMonth.Visible = false;
			cboEndingMonth.SelectedIndex = -1;
			lblTo[1].Visible = false;
			cboSingleMonth.Visible = true;
			cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
		}

		private void txtChartTitle_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTitle_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsGraph_DblClick(object sender, System.EventArgs e)
		{
			if (vsGraph.MouseRow > 0)
			{
				blnEdit = true;
				intSelected = vsGraph.Row - 1;
				fraGraphItem.Visible = true;
				txtTitle.Text = grpItems[intSelected].Title;
				string vbPorterVar = grpItems[intSelected].Type;
				if (vbPorterVar == "E")
				{
					SetGraphTypeCombo(ref grpItems[intSelected].Type);
					cboGraphType.SelectedIndex = 0;
					FillExpenseItem();
				}
				else if (vbPorterVar == "R")
				{
					SetGraphTypeCombo(ref grpItems[intSelected].Type);
					FillRevenueItem();
				}
				else if (vbPorterVar == "C")
				{
					SetGraphTypeCombo(ref grpItems[intSelected].Type);
					FillCashItem();
				}
				else if (vbPorterVar == "T")
				{
					SetGraphTypeCombo(ref grpItems[intSelected].Type);
					FillTaxItem();
				}
			}
		}

		private void vsSelectedExpenses_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsSelectedExpenses.Row > 0)
			{
				if (FCUtils.CBool(vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 0)) != true)
				{
					vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 0, FCConvert.ToString(true));
				}
				else
				{
					vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 0, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedExpenses_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsSelectedExpenses.Row > 0)
				{
					if (FCUtils.CBool(vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 0)) != true)
					{
						vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 0, FCConvert.ToString(true));
					}
					else
					{
						vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 0, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsSelectedRevenues_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsSelectedRevenues.Row > 0)
			{
				if (FCUtils.CBool(vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 0)) != true)
				{
					vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 0, FCConvert.ToString(true));
				}
				else
				{
					vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 0, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedRevenues_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsSelectedRevenues.Row > 0)
				{
					if (FCUtils.CBool(vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 0)) != true)
					{
						vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 0, FCConvert.ToString(true));
					}
					else
					{
						vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 0, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsSelectedCash_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsSelectedCash.Row > 0)
			{
				if (FCUtils.CBool(vsSelectedCash.TextMatrix(vsSelectedCash.Row, 0)) != true)
				{
					vsSelectedCash.TextMatrix(vsSelectedCash.Row, 0, FCConvert.ToString(true));
				}
				else
				{
					vsSelectedCash.TextMatrix(vsSelectedCash.Row, 0, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedCash_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsSelectedCash.Row > 0)
				{
					if (FCUtils.CBool(vsSelectedCash.TextMatrix(vsSelectedCash.Row, 0)) != true)
					{
						vsSelectedCash.TextMatrix(vsSelectedCash.Row, 0, FCConvert.ToString(true));
					}
					else
					{
						vsSelectedCash.TextMatrix(vsSelectedCash.Row, 0, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsSelectedLiabilities_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsSelectedLiabilities.Row > 0)
			{
				if (FCUtils.CBool(vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 0)) != true)
				{
					vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 0, FCConvert.ToString(true));
				}
				else
				{
					vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 0, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedLiabilities_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsSelectedLiabilities.Row > 0)
				{
					if (FCUtils.CBool(vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 0)) != true)
					{
						vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 0, FCConvert.ToString(true));
					}
					else
					{
						vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 0, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsSelectedTax_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsSelectedTax.Row > 0)
			{
				if (FCUtils.CBool(vsSelectedTax.TextMatrix(vsSelectedTax.Row, 0)) != true)
				{
					vsSelectedTax.TextMatrix(vsSelectedTax.Row, 0, FCConvert.ToString(true));
				}
				else
				{
					vsSelectedTax.TextMatrix(vsSelectedTax.Row, 0, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedTax_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsSelectedTax.Row > 0)
				{
					if (FCUtils.CBool(vsSelectedTax.TextMatrix(vsSelectedTax.Row, 0)) != true)
					{
						vsSelectedTax.TextMatrix(vsSelectedTax.Row, 0, FCConvert.ToString(true));
					}
					else
					{
						vsSelectedTax.TextMatrix(vsSelectedTax.Row, 0, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsSelectedTax_RowColChange(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedTax.TextMatrix(vsSelectedTax.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedCash_RowColChange(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedCash.TextMatrix(vsSelectedCash.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedLiabilities_RowColChange(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedRevenues_RowColChange(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedExpenses_RowColChange(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedExpenses_Enter(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedExpenses.TextMatrix(vsSelectedExpenses.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedRevenues_Enter(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedRevenues.TextMatrix(vsSelectedRevenues.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedCash_Enter(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedCash.TextMatrix(vsSelectedCash.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedLiabilities_Enter(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedLiabilities.TextMatrix(vsSelectedLiabilities.Row, 1), ref lblAccountTitle);
		}

		private void vsSelectedTax_Enter(object sender, System.EventArgs e)
		{
			modAccountTitle.DetermineAccountTitle(vsSelectedTax.TextMatrix(vsSelectedTax.Row, 1), ref lblAccountTitle);
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void SaveExpenseItem_2(short intIndex)
		{
			SaveExpenseItem(intIndex);
		}

		private void SaveExpenseItem(short intIndex)
		{
			int counter;
			string strAccounts = "";
			if (cboExpenseSelection.SelectedIndex == 0)
			{
				// All Accounts
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "ALL";
				grpItems[intIndex].Low = "";
				grpItems[intIndex].High = "";
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboExpenseSelection.SelectedIndex == 1)
			{
				// Single Department
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "SD";
				grpItems[intIndex].Low = Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
				grpItems[intIndex].High = "";
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboExpenseSelection.SelectedIndex == 2)
			{
				// Single Expense
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "SE";
				grpItems[intIndex].Low = Strings.Left(cboSingleExpense.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
				grpItems[intIndex].High = "";
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboExpenseSelection.SelectedIndex == 3)
			{
				// Range of Deaprtments
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "RD";
				grpItems[intIndex].Low = Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
				grpItems[intIndex].High = Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboExpenseSelection.SelectedIndex == 4)
			{
				// Range of Expenses
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "RE";
				grpItems[intIndex].Low = Strings.Left(cboBeginningExpense.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
				grpItems[intIndex].High = Strings.Left(cboEndingExpense.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboExpenseSelection.SelectedIndex == 5)
			{
				// Range of Accounts
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "RA";
				grpItems[intIndex].Low = vsLowAccount.Text;
				grpItems[intIndex].High = vsHighAccount.Text;
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboExpenseSelection.SelectedIndex == 6)
			{
				// Selected Accounts
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "E";
				grpItems[intIndex].AccountSelectionType = "SA";
				grpItems[intIndex].Low = "";
				grpItems[intIndex].High = "";
				strAccounts = "";
				for (counter = 1; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsSelectedExpenses.TextMatrix(counter, 0)) == true)
					{
						strAccounts += vsSelectedExpenses.TextMatrix(counter, 1) + "  ";
					}
				}
				strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
				grpItems[intIndex].SelectedAccounts = strAccounts;
				grpItems[intIndex].SelectedLiabilities = "";
			}
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void SaveRevenueItem_2(int intIndex)
		{
			SaveRevenueItem(ref intIndex);
		}

		private void SaveRevenueItem(ref int intIndex)
		{
			int counter;
			string strAccounts = "";
			if (cboRevenueSelection.SelectedIndex == 0)
			{
				// All Accounts
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "R";
				grpItems[intIndex].AccountSelectionType = "ALL";
				grpItems[intIndex].Low = "";
				grpItems[intIndex].High = "";
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboRevenueSelection.SelectedIndex == 1)
			{
				// Single Department
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "R";
				grpItems[intIndex].AccountSelectionType = "SD";
				grpItems[intIndex].Low = Strings.Left(cboSingleRev.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))));
				grpItems[intIndex].High = "";
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboRevenueSelection.SelectedIndex == 2)
			{
				// Range of Departments
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "R";
				grpItems[intIndex].AccountSelectionType = "RD";
				grpItems[intIndex].Low = Strings.Left(cboBeginningRev.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))));
				grpItems[intIndex].High = Strings.Left(cboEndingRev.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))));
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboRevenueSelection.SelectedIndex == 3)
			{
				// Range of Accounts
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "R";
				grpItems[intIndex].AccountSelectionType = "RA";
				grpItems[intIndex].Low = vsLowRevAccount.Text;
				grpItems[intIndex].High = vsHighRevAccount.Text;
				grpItems[intIndex].SelectedAccounts = "";
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboRevenueSelection.SelectedIndex == 4)
			{
				// Selected Accounts
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "R";
				grpItems[intIndex].AccountSelectionType = "SA";
				grpItems[intIndex].Low = "";
				grpItems[intIndex].High = "";
				strAccounts = "";
				for (counter = 1; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsSelectedRevenues.TextMatrix(counter, 0)) == true)
					{
						strAccounts += vsSelectedRevenues.TextMatrix(counter, 1) + "  ";
					}
				}
				strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
				grpItems[intIndex].SelectedAccounts = strAccounts;
				grpItems[intIndex].SelectedLiabilities = "";
			}
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void SaveCashItem_2(int intIndex)
		{
			SaveCashItem(ref intIndex);
		}

		private void SaveCashItem(ref int intIndex)
		{
			int counter;
			string strAccounts = "";
			if (cboCashSelection.SelectedIndex == 0)
			{
				// Total Amount
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "C";
				grpItems[intIndex].AccountSelectionType = "TA";
				grpItems[intIndex].Low = "";
				grpItems[intIndex].High = "";
				strAccounts = "";
				for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsSelectedCash.TextMatrix(counter, 0)) == true)
					{
						strAccounts += vsSelectedCash.TextMatrix(counter, 1) + "  ";
					}
				}
				strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
				grpItems[intIndex].SelectedAccounts = strAccounts;
				grpItems[intIndex].SelectedLiabilities = "";
			}
			else if (cboCashSelection.SelectedIndex == 1)
			{
				// Net Amount
				grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
				grpItems[intIndex].Type = "C";
				grpItems[intIndex].AccountSelectionType = "NA";
				grpItems[intIndex].Low = "";
				grpItems[intIndex].High = "";
				strAccounts = "";
				for (counter = 1; counter <= vsSelectedCash.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsSelectedCash.TextMatrix(counter, 0)) == true)
					{
						strAccounts += vsSelectedCash.TextMatrix(counter, 1) + "  ";
					}
				}
				strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
				grpItems[intIndex].SelectedAccounts = strAccounts;
				strAccounts = "";
				for (counter = 1; counter <= vsSelectedLiabilities.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsSelectedLiabilities.TextMatrix(counter, 0)) == true)
					{
						strAccounts += vsSelectedLiabilities.TextMatrix(counter, 1) + "  ";
					}
				}
				strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
				grpItems[intIndex].SelectedLiabilities = strAccounts;
			}
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void SaveTaxItem_2(int intIndex)
		{
			SaveTaxItem(ref intIndex);
		}

		private void SaveTaxItem(ref int intIndex)
		{
			int counter;
			string strAccounts;
			grpItems[intIndex].Title = Strings.Trim(txtTitle.Text);
			grpItems[intIndex].Type = "T";
			grpItems[intIndex].AccountSelectionType = "SA";
			grpItems[intIndex].Low = "";
			grpItems[intIndex].High = "";
			strAccounts = "";
			for (counter = 1; counter <= vsSelectedTax.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsSelectedTax.TextMatrix(counter, 0)) == true)
				{
					strAccounts += vsSelectedTax.TextMatrix(counter, 1) + "  ";
				}
			}
			strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
			grpItems[intIndex].SelectedAccounts = strAccounts;
			grpItems[intIndex].SelectedLiabilities = "";
		}

		private void FillExpenseItem()
		{
			string[] strAccounts = null;
			int counter;
			int intAccount = 0;
			string vbPorterVar = grpItems[intSelected].AccountSelectionType;
			if (vbPorterVar == "ALL")
			{
				cboExpenseSelection.SelectedIndex = 0;
			}
			else if (vbPorterVar == "SD")
			{
				cboExpenseSelection.SelectedIndex = 1;
				cboSingleDept.SelectedIndex = GetListIndex(ref cboSingleDept, ref grpItems[intSelected].Low);
			}
			else if (vbPorterVar == "SE")
			{
				cboExpenseSelection.SelectedIndex = 2;
				cboSingleExpense.SelectedIndex = GetListIndex(ref cboSingleExpense, ref grpItems[intSelected].Low);
			}
			else if (vbPorterVar == "RD")
			{
				cboExpenseSelection.SelectedIndex = 3;
				cboBeginningDept.SelectedIndex = GetListIndex(ref cboBeginningDept, ref grpItems[intSelected].Low);
				cboEndingDept.SelectedIndex = GetListIndex(ref cboEndingDept, ref grpItems[intSelected].High);
			}
			else if (vbPorterVar == "RE")
			{
				cboExpenseSelection.SelectedIndex = 4;
				cboBeginningExpense.SelectedIndex = GetListIndex(ref cboBeginningExpense, ref grpItems[intSelected].Low);
				cboEndingExpense.SelectedIndex = GetListIndex(ref cboEndingExpense, ref grpItems[intSelected].High);
			}
			else if (vbPorterVar == "RA")
			{
				cboExpenseSelection.SelectedIndex = 5;
				vsLowAccount.TextMatrix(0, 0, grpItems[intSelected].Low);
				vsHighAccount.TextMatrix(0, 0, grpItems[intSelected].High);
			}
			else if (vbPorterVar == "SA")
			{
				cboExpenseSelection.SelectedIndex = 6;
				strAccounts = Strings.Split(grpItems[intSelected].SelectedAccounts, "  ", -1, CompareConstants.vbBinaryCompare);
				intAccount = 0;
				for (counter = 0; counter <= vsSelectedExpenses.Rows - 1; counter++)
				{
					if (intAccount <= Information.UBound(strAccounts, 1))
					{
						if (Strings.Trim(strAccounts[intAccount]) == vsSelectedExpenses.TextMatrix(counter, 1))
						{
							vsSelectedExpenses.TextMatrix(counter, 0, FCConvert.ToString(true));
							intAccount += 1;
						}
					}
					else
					{
						break;
					}
				}
				if (vsSelectedExpenses.Rows > 1)
				{
					vsSelectedExpenses.Row = 1;
				}
			}
		}

		private void FillRevenueItem()
		{
			string[] strAccounts = null;
			int counter;
			int intAccount = 0;
			string vbPorterVar = grpItems[intSelected].AccountSelectionType;
			if (vbPorterVar == "ALL")
			{
				cboRevenueSelection.SelectedIndex = 0;
			}
			else if (vbPorterVar == "SD")
			{
				cboRevenueSelection.SelectedIndex = 1;
				cboSingleRev.SelectedIndex = GetListIndex(ref cboSingleRev, ref grpItems[intSelected].Low);
			}
			else if (vbPorterVar == "RD")
			{
				cboRevenueSelection.SelectedIndex = 2;
				cboBeginningRev.SelectedIndex = GetListIndex(ref cboBeginningRev, ref grpItems[intSelected].Low);
				cboEndingRev.SelectedIndex = GetListIndex(ref cboEndingRev, ref grpItems[intSelected].High);
			}
			else if (vbPorterVar == "RA")
			{
				cboRevenueSelection.SelectedIndex = 3;
				vsLowRevAccount.TextMatrix(0, 0, grpItems[intSelected].Low);
				vsHighRevAccount.TextMatrix(0, 0, grpItems[intSelected].High);
			}
			else if (vbPorterVar == "SA")
			{
				cboRevenueSelection.SelectedIndex = 4;
				strAccounts = Strings.Split(grpItems[intSelected].SelectedAccounts, "  ", -1, CompareConstants.vbBinaryCompare);
				intAccount = 0;
				for (counter = 0; counter <= vsSelectedRevenues.Rows - 1; counter++)
				{
					if (intAccount <= Information.UBound(strAccounts, 1))
					{
						if (Strings.Trim(strAccounts[intAccount]) == vsSelectedRevenues.TextMatrix(counter, 1))
						{
							vsSelectedRevenues.TextMatrix(counter, 0, FCConvert.ToString(true));
							intAccount += 1;
						}
					}
					else
					{
						break;
					}
				}
				if (vsSelectedRevenues.Rows > 1)
				{
					vsSelectedRevenues.Row = 1;
				}
			}
		}

		private void FillCashItem()
		{
			string[] strAccounts = null;
			int counter;
			int intAccount = 0;
			string vbPorterVar = grpItems[intSelected].AccountSelectionType;
			if (vbPorterVar == "TA")
			{
				cboCashSelection.SelectedIndex = 0;
				strAccounts = Strings.Split(grpItems[intSelected].SelectedAccounts, "  ", -1, CompareConstants.vbBinaryCompare);
				intAccount = 0;
				for (counter = 0; counter <= vsSelectedCash.Rows - 1; counter++)
				{
					if (intAccount <= Information.UBound(strAccounts, 1))
					{
						if (Strings.Trim(strAccounts[intAccount]) == vsSelectedCash.TextMatrix(counter, 1))
						{
							vsSelectedCash.TextMatrix(counter, 0, FCConvert.ToString(true));
							intAccount += 1;
						}
					}
					else
					{
						break;
					}
				}
				if (vsSelectedCash.Rows > 1)
				{
					vsSelectedCash.Row = 1;
				}
			}
			else if (vbPorterVar == "NA")
			{
				cboCashSelection.SelectedIndex = 1;
				strAccounts = Strings.Split(grpItems[intSelected].SelectedAccounts, "  ", -1, CompareConstants.vbBinaryCompare);
				intAccount = 0;
				for (counter = 0; counter <= vsSelectedCash.Rows - 1; counter++)
				{
					if (intAccount <= Information.UBound(strAccounts, 1))
					{
						if (Strings.Trim(strAccounts[intAccount]) == vsSelectedCash.TextMatrix(counter, 1))
						{
							vsSelectedCash.TextMatrix(counter, 0, FCConvert.ToString(true));
							intAccount += 1;
						}
					}
					else
					{
						break;
					}
				}
				if (vsSelectedCash.Rows > 1)
				{
					vsSelectedCash.Row = 1;
				}
				strAccounts = Strings.Split(grpItems[intSelected].SelectedLiabilities, "  ", -1, CompareConstants.vbBinaryCompare);
				intAccount = 0;
				for (counter = 0; counter <= vsSelectedLiabilities.Rows - 1; counter++)
				{
					if (intAccount <= Information.UBound(strAccounts, 1))
					{
						if (Strings.Trim(strAccounts[intAccount]) == vsSelectedLiabilities.TextMatrix(counter, 1))
						{
							vsSelectedLiabilities.TextMatrix(counter, 0, FCConvert.ToString(true));
							intAccount += 1;
						}
					}
					else
					{
						break;
					}
				}
				if (vsSelectedLiabilities.Rows > 1)
				{
					vsSelectedLiabilities.Row = 1;
				}
			}
		}

		private void FillTaxItem()
		{
			string[] strAccounts = null;
			int counter;
			int intAccount;
			strAccounts = Strings.Split(grpItems[intSelected].SelectedAccounts, "  ", -1, CompareConstants.vbBinaryCompare);
			intAccount = 0;
			for (counter = 0; counter <= vsSelectedTax.Rows - 1; counter++)
			{
				if (intAccount <= Information.UBound(strAccounts, 1))
				{
					if (Strings.Trim(strAccounts[intAccount]) == vsSelectedTax.TextMatrix(counter, 1))
					{
						vsSelectedTax.TextMatrix(counter, 0, FCConvert.ToString(true));
						intAccount += 1;
					}
				}
				else
				{
					break;
				}
			}
			if (vsSelectedTax.Rows > 1)
			{
				vsSelectedTax.Row = 1;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetListIndex(ref FCComboBox cboInfo, ref string strItem)
		{
			short GetListIndex = 0;
			int counter;
			GetListIndex = -1;
			for (counter = 0; counter <= cboInfo.Items.Count; counter++)
			{
				if (Strings.Left(cboInfo.Text, strItem.Length) == strItem)
				{
					GetListIndex = FCConvert.ToInt16(counter);
					return GetListIndex;
				}
			}
			return GetListIndex;
		}

		private void LoadChart()
		{
			clsDRWrapper rsChartInfo = new clsDRWrapper();
			clsDRWrapper rsChartItems = new clsDRWrapper();
			clsDRWrapper rsItemDetail = new clsDRWrapper();
			rsChartInfo.OpenRecordset("SELECT * FROM CashCharts WHERE ID = " + FCConvert.ToString(OldChartKey));
			txtChartTitle.Text = FCConvert.ToString(rsChartInfo.Get_Fields_String("Title"));
			if (FCConvert.ToString(rsChartInfo.Get_Fields_String("SelectedMonths")) == "S")
			{
				cmbRange.SelectedIndex = 1;
				cboSingleMonth.SelectedIndex = FCConvert.ToInt32(rsChartInfo.Get_Fields_Int32("BegMonth")) - 1;
			}
			else if (rsChartInfo.Get_Fields_String("SelectedMonths") == "R")
			{
				cmbRange.SelectedIndex = 0;
				cboBeginningMonth.SelectedIndex = FCConvert.ToInt32(rsChartInfo.Get_Fields_Int32("BegMonth")) - 1;
				cboEndingMonth.SelectedIndex = FCConvert.ToInt32(rsChartInfo.Get_Fields_Int32("EndMonth")) - 1;
			}
			else
			{
				cmbRange.SelectedIndex = 2;
			}
			if (FCConvert.ToString(rsChartInfo.Get_Fields_String("Totals")) == "M")
			{
				cmbYTDTotals.SelectedIndex = 1;
			}
			else
			{
				cmbYTDTotals.SelectedIndex = 0;
			}
			rsChartItems.OpenRecordset("SELECT * FROM ItemsInChart WHERE ChartKey = " + FCConvert.ToString(OldChartKey));
			if (rsChartItems.EndOfFile() != true && rsChartItems.BeginningOfFile() != true)
			{
				do
				{
					rsItemDetail.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + rsChartItems.Get_Fields_Int32("GraphItemID"));
					intItems += 1;
					Array.Resize(ref grpItems, intItems + 1);
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					grpItems[intItems - 1] = new GraphItem(0);
					grpItems[intItems - 1].Title = FCConvert.ToString(rsItemDetail.Get_Fields_String("Title"));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					grpItems[intItems - 1].Type = FCConvert.ToString(rsItemDetail.Get_Fields("Type"));
					grpItems[intItems - 1].AccountSelectionType = FCConvert.ToString(rsItemDetail.Get_Fields_String("AccountSelectionType"));
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					grpItems[intItems - 1].Low = FCConvert.ToString(rsItemDetail.Get_Fields("Low"));
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					grpItems[intItems - 1].High = FCConvert.ToString(rsItemDetail.Get_Fields("High"));
					grpItems[intItems - 1].SelectedAccounts = FCConvert.ToString(rsItemDetail.Get_Fields_String("SelectedAccounts"));
					grpItems[intItems - 1].SelectedLiabilities = FCConvert.ToString(rsItemDetail.Get_Fields_String("SelectedLiabilities"));
					vsGraph.Rows += 1;
					vsGraph.TextMatrix(vsGraph.Rows - 1, KeyCol, FCConvert.ToString(rsItemDetail.Get_Fields_Int32("ID")));
					vsGraph.TextMatrix(vsGraph.Rows - 1, LineCol, Strings.Trim((vsGraph.Rows - 1).ToString()));
					vsGraph.TextMatrix(vsGraph.Rows - 1, TypeCol, grpItems[intItems - 1].Type);
					vsGraph.TextMatrix(vsGraph.Rows - 1, TitleCol, grpItems[intItems - 1].Title);
					vsGraph.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsGraph.Rows - 1, 0, vsGraph.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					rsChartItems.MoveNext();
				}
				while (rsChartItems.EndOfFile() != true);
			}
		}

		private void SaveInfo()
		{
			clsDRWrapper rsGraphItems = new clsDRWrapper();
			clsDRWrapper rsCashChart = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int lngChartKey = 0;
			int counter;
			if (Strings.Trim(txtChartTitle.Text) == "")
			{
				MessageBox.Show("You must enter a chart title before you may proceed", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (vsGraph.Rows == 1)
			{
				MessageBox.Show("You must enter at least one graph item before you may proceed.", "No Information Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbRange.SelectedIndex == 0)
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Chart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Chart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Chart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			rsCashChart.OpenRecordset("SELECT * FROM CashCharts WHERE Title = '" + Strings.Trim(txtChartTitle.Text) + "'");
			if (rsCashChart.EndOfFile() != true && rsCashChart.BeginningOfFile() != true)
			{
				ans = MessageBox.Show("A Cash Chart with this title already exists.  Do you wish to overwrite it?", "Overwrite Chart?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					rsCashChart.Edit();
				}
				else
				{
					return;
				}
			}
			else
			{
				rsCashChart.AddNew();
			}
			rsCashChart.Set_Fields("Title", Strings.Trim(txtChartTitle.Text));
			if (cmbRange.SelectedIndex == 1)
			{
				rsCashChart.Set_Fields("SelectedMonths", "S");
				rsCashChart.Set_Fields("BegMonth", cboSingleMonth.SelectedIndex + 1);
			}
			else if (cmbRange.SelectedIndex == 0)
			{
				rsCashChart.Set_Fields("SelectedMonths", "R");
				rsCashChart.Set_Fields("BegMonth", cboBeginningMonth.SelectedIndex + 1);
				rsCashChart.Set_Fields("EndMonth", cboEndingMonth.SelectedIndex + 1);
			}
			else
			{
				rsCashChart.Set_Fields("SelectedMonths", "A");
			}
			if (cmbYTDTotals.SelectedIndex == 1)
			{
				rsCashChart.Set_Fields("Totals", "M");
			}
			else
			{
				rsCashChart.Set_Fields("Totals", "Y");
			}
			rsCashChart.Update(true);
			lngChartKey = FCConvert.ToInt32(rsCashChart.Get_Fields_Int32("ID"));
			rsCashChart.Execute("DELETE FROM ItemsInChart WHERE Chartkey = " + FCConvert.ToString(lngChartKey), "Budgetary");
			rsCashChart.OpenRecordset("SELECT * FROM ItemsInChart WHERE ID = 0");
			for (counter = 1; counter <= vsGraph.Rows - 1; counter++)
			{
				rsCashChart.AddNew();
				rsCashChart.Set_Fields("GraphItemID", FCConvert.ToString(Conversion.Val(vsGraph.TextMatrix(counter, KeyCol))));
				rsCashChart.Set_Fields("Chartkey", lngChartKey);
				rsCashChart.Update();
			}
			if (!blnReportAlreadyExists)
			{
				frmCashChartSelect.InstancePtr.FillReport(true);
			}
			else
			{
				blnReportAlreadyExists = false;
				OldChartKey = 0;
			}
			frmCashChartSelect.InstancePtr.Show(App.MainForm);
			//FC:FINAL:MSH - issue #778: show the combobox and the checkbox after saving the results of editing
			frmCashChartSelect.InstancePtr.cboReports.Visible = true;
			frmCashChartSelect.InstancePtr.chkDefault.Visible = true;
			Close();
		}

		private void FillGraphItemsCombo()
		{
			cboGraphType.Clear();
			cboGraphType.AddItem("Town Expense");
			cboGraphType.AddItem("Town Revenue");
			cboGraphType.AddItem("Town Cash");
			cboGraphType.AddItem("Town Tax Receivables");
		}

		private void SetGraphTypeCombo(ref string strType)
		{
			int counter;
			cboGraphType.SelectedIndex = -1;
			if (strType == "E")
			{
				for (counter = 0; counter <= cboGraphType.Items.Count - 1; counter++)
				{
					if (cboGraphType.Items[counter].ToString() == "Town Expense")
					{
						cboGraphType.SelectedIndex = counter;
						break;
					}
				}
			}
			else if (strType == "R")
			{
				for (counter = 0; counter <= cboGraphType.Items.Count - 1; counter++)
				{
					if (cboGraphType.Items[counter].ToString() == "Town Revenue")
					{
						cboGraphType.SelectedIndex = counter;
						break;
					}
				}
			}
			else if (strType == "C")
			{
				for (counter = 0; counter <= cboGraphType.Items.Count - 1; counter++)
				{
					if (cboGraphType.Items[counter].ToString() == "Town Cash")
					{
						cboGraphType.SelectedIndex = counter;
						break;
					}
				}
			}
			else if (strType == "T")
			{
				for (counter = 0; counter <= cboGraphType.Items.Count - 1; counter++)
				{
					if (cboGraphType.Items[counter].ToString() == "Town Tax Receivables")
					{
						cboGraphType.SelectedIndex = counter;
						break;
					}
				}
			}
		}

		private void frmCashChartingSetup_Resize(object sender, EventArgs e)
		{
			fraExistingItem.CenterToContainer(this.ClientArea);
		}

		private void cmbRange_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				optSingle_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 2)
			{
				optAllMonths_CheckedChanged(sender, e);
			}
		}
	}
}
