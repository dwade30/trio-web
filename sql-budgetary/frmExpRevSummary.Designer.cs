﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpRevSummary.
	/// </summary>
	partial class frmExpRevSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.ImageList ImageList1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdFilePrint;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCButton cmdExportData;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpRevSummary));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.lblReport = new fecherFoundation.FCLabel();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Line1 = new fecherFoundation.FCLine();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.cmdPreview = new fecherFoundation.FCButton();
            this.cmdExportData = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            this.vsLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExportData)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 750);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Size = new System.Drawing.Size(1078, 690);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdExportData);
            this.TopPanel.Controls.Add(this.cmdFilePrint);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdExportData, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(217, 30);
            this.HeaderText.Text = "Exp/Rev Summary";
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(133, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(221, 40);
            this.cmbReport.TabIndex = 11;
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(20, 44);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(59, 15);
            this.lblReport.TabIndex = 12;
            this.lblReport.Text = "REPORT";
            this.lblReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 481);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(683, 187);
            this.fraWhere.TabIndex = 11;
            this.fraWhere.Text = "Select Search Criteria";
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(20, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.ShowFocusCell = false;
            this.vsWhere.Size = new System.Drawing.Size(643, 137);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 12;
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmdAdd);
            this.Frame2.Controls.Add(this.cmbReport);
            this.Frame2.Controls.Add(this.lblReport);
            this.Frame2.Controls.Add(this.cboSavedReport);
            this.Frame2.Enabled = false;
            this.Frame2.Location = new System.Drawing.Point(634, 241);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(374, 210);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Report";
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Enabled = false;
            this.cmdAdd.Location = new System.Drawing.Point(20, 150);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(261, 40);
            this.cmdAdd.TabIndex = 10;
            this.cmdAdd.Text = "Add Custom Report to Library";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 90);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(334, 40);
            this.cboSavedReport.TabIndex = 8;
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Enabled = false;
            this.fraSort.Location = new System.Drawing.Point(332, 241);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(272, 210);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(232, 160);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Enabled = false;
            this.fraFields.Location = new System.Drawing.Point(30, 241);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(272, 210);
            this.fraFields.Text = "Fields To Display On Report";
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.CheckBoxes = true;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(232, 160);
            this.lstFields.Style = 1;
            this.lstFields.TabIndex = 2;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // Frame3
            // 
            this.Frame3.AppearanceKey = "groupBoxNoBorders";
            this.Frame3.Controls.Add(this.Label2);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.Image1);
            this.Frame3.Controls.Add(this.fraMessage);
            this.Frame3.Controls.Add(this.vsLayout);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(1039, 190);
            this.Frame3.TabIndex = 16;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(49, 72);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(9, 82);
            this.Label2.TabIndex = 20;
            this.Label2.Text = "M A R G I N";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 72);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(9, 58);
            this.Label1.TabIndex = 19;
            this.Label1.Text = "L E F T";
            // 
            // Image1
            // 
            this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(78, 30);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(941, 22);
            this.Image1.TabIndex = 23;
            // 
            // fraMessage
            // 
            this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left)
            | Wisej.Web.AnchorStyles.Right)));
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(78, 72);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(941, 92);
            this.fraMessage.TabIndex = 22;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(24, 39);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(593, 16);
            this.Label3.TabIndex = 23;
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            // 
            // vsLayout
            // 
            this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsLayout.Controls.Add(this.Line1);
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.Location = new System.Drawing.Point(74, 71);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.ReadOnly = false;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.Rows = 1;
            this.vsLayout.ShowFocusCell = false;
            this.vsLayout.Size = new System.Drawing.Size(958, 88);
            this.vsLayout.TabIndex = 21;
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.Line1.LineWidth = 2;
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(2, 360);
            this.Line1.Y2 = 360F;
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry2});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            //this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuLayout});
            this.MainMenu1.Name = null;
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = 0;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(826, 27);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(139, 24);
            this.cmdClear.TabIndex = 3;
            this.cmdClear.Text = "Clear Search Criteria";
            this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrint.Location = new System.Drawing.Point(1028, 27);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Size = new System.Drawing.Size(42, 24);
            this.cmdFilePrint.TabIndex = 2;
            this.cmdFilePrint.Text = "Print";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // cmdPreview
            // 
            this.cmdPreview.AppearanceKey = "acceptButton";
            this.cmdPreview.Location = new System.Drawing.Point(274, 30);
            this.cmdPreview.Name = "cmdPreview";
            this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPreview.Size = new System.Drawing.Size(137, 48);
            this.cmdPreview.Text = "Print / Preview ";
            this.cmdPreview.Click += new System.EventHandler(this.mnuPreview_Click);
            // 
            // cmdExportData
            // 
            this.cmdExportData.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdExportData.Location = new System.Drawing.Point(970, 27);
            this.cmdExportData.Name = "cmdExportData";
            this.cmdExportData.Size = new System.Drawing.Size(55, 24);
            this.cmdExportData.TabIndex = 1;
            this.cmdExportData.Text = "Export";
            this.cmdExportData.Click += new System.EventHandler(this.mnuExportData_Click);
            // 
            // frmExpRevSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 858);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmExpRevSummary";
            this.Text = "Exp / Rev Summary";
            this.Load += new System.EventHandler(this.frmExpRevSummary_Load);
            this.Resize += new System.EventHandler(this.frmExpRevSummary_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExpRevSummary_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            this.vsLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExportData)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
