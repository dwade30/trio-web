﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using SharedApplication.Budgetary.Enums;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEncumbranceDataEntry.
	/// </summary>
	public partial class frmEncumbranceDataEntry : BaseForm
	{
		public frmEncumbranceDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtTownAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_0, 0);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_1, 1);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_2, 2);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_3, 3);
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEncumbranceDataEntry InstancePtr
		{
			get
			{
				return (frmEncumbranceDataEntry)Sys.GetInstance(typeof(frmEncumbranceDataEntry));
			}
		}

		protected frmEncumbranceDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int DescriptionCol;
		int AccountCol;
		int ProjectCol;
		int AmountCol;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		bool SearchFlag;
		int NumberCol;
		// vbPorter upgrade warning: TotalAmount As double	OnWrite(short, Decimal)
		double TotalAmount;
		// vbPorter upgrade warning: AmountToPay As double	OnWrite(short, string)
		double AmountToPay;
		bool EditFlag;
		string ErrorString = "";
		bool BadAccountFlag;
		int CurrJournal;
		public int VendorNumber;
		public bool OldJournal;
		public int OldJournalNumber;
		// vbPorter upgrade warning: OldPeriod As short --> As int	OnWrite(string)
		public int OldPeriod;
		public int FirstRecordShown;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		bool blnUnload;
		public bool blnFromVendorMaster;
		public bool blnFromVendor;
		bool blnJournalLocked;
		string strComboList = "";
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsGridAccount vsGrid = new clsGridAccount();
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;
		bool openFormWhenThisClose = false;

		public void Init(ref cEncumbrance enc)
		{
			frmEncumbranceDataEntry.InstancePtr.OldJournal = true;
			if (!(enc == null))
			{
				OldJournal = true;
				OldJournalNumber = enc.JournalNumber;
				OldPeriod = enc.Period;
				modBudgetaryMaster.Statics.CurrentEncEntry = enc.JournalNumber;
				modRegistry.SaveRegistryKey("CURRENCJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentEncEntry));
				txtVendor.Text = modValidateAccount.GetFormat_6(enc.VendorNumber.ToString(), 5);
				if (enc.VendorNumber == 0)
				{
					txtAddress[0].Text = enc.TempVendorName;
					txtAddress[1].Text = enc.TempVendorAddress1;
					txtAddress[2].Text = enc.TempVendorAddress2;
					txtAddress[3].Text = enc.TempVendorAddress3;
					txtCity.Text = enc.TempVendorCity;
					txtState.Text = enc.TempVendorState;
					txtZip.Text = enc.TempVendorZip;
					txtZip4.Text = enc.TempVendorZip4;
				}
				else
				{
					txtAddress[0].Text = enc.Vendor.CheckName;
					txtAddress[1].Text = enc.Vendor.CheckAddress1;
					txtAddress[2].Text = enc.Vendor.CheckAddress2;
					txtAddress[3].Text = enc.Vendor.CheckAddress3;
					txtCity.Text = enc.Vendor.CheckCity;
					txtState.Text = enc.Vendor.CheckState;
					txtZip.Text = enc.Vendor.CheckState;
					txtZip4.Text = enc.Vendor.CheckZip4;
				}
				if (Information.IsDate(enc.EncumbrancesDate))
				{
					txtDate.Text = enc.EncumbrancesDate;
				}
				txtPeriod.Text = FCConvert.ToString(enc.Period);
				txtDescription.Text = enc.Description;
				txtAmount.Text = Strings.Format(enc.EncumbranceAmount, "#,###,##0.00");
				txtPO.Text = enc.PO;
				VendorNumber = enc.ID;
				FirstRecordShown = enc.ID;
				modBudgetaryMaster.Statics.blnEncEdit = true;
				enc.EncumbranceDetails.MoveFirst();
				if (enc.EncumbranceDetails.ItemCount() > 15)
				{
					vs1.Rows = enc.EncumbranceDetails.ItemCount() + 1;
				}
				int counter = 0;
				int intMax = 0;
				cEncumbranceDetail encDetail;
				counter = 1;
				while (enc.EncumbranceDetails.IsCurrent())
				{
					encDetail = (cEncumbranceDetail)enc.EncumbranceDetails.GetCurrentItem();
					vs1.TextMatrix(counter, 0, FCConvert.ToString(encDetail.ID));
					vs1.TextMatrix(counter, 1, encDetail.Description);
					vs1.TextMatrix(counter, 2, encDetail.Account);
					vs1.TextMatrix(counter, 3, encDetail.Project);
					vs1.TextMatrix(counter, 4, FCConvert.ToString(encDetail.Amount));
					enc.EncumbranceDetails.MoveNext();
					counter += 1;
				}
				if (counter < vs1.Rows - 1)
				{
					intMax = counter;
					for (counter = intMax; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					}
				}
			}
			else
			{
			}
			this.Show(App.MainForm);
		}

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
		}

		private void cmdBalanceOK_Click(object sender, System.EventArgs e)
		{
			fraAccountBalance.Visible = false;
			vs1.Focus();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			int counter;
			txtSearch.Text = "";
			frmSearch.Visible = false;
			cboJournal.Enabled = true;
			txtVendor.Enabled = true;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = true;
			}
			txtCity.Enabled = true;
			txtState.Enabled = true;
			txtZip.Enabled = true;
			txtZip4.Enabled = true;
			txtDate.Enabled = true;
			txtDescription.Enabled = true;
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
			{
				txtPO.Enabled = false;
			}
			else
			{
				txtPO.Enabled = true;
			}
			txtAmount.Enabled = true;
			vs1.Enabled = true;
			txtVendor.Focus();
		}

		private void cmdCancelPrint_Click(object sender, System.EventArgs e)
		{
			fraTownInfo.Visible = false;
		}

		public void cmdCancelPrint_Click()
		{
			cmdCancelPrint_Click(cmdCancelPrint, new System.EventArgs());
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
			{
				modBudgetaryMaster.UnlockPO();
				txtPO.Text = "Auto";
			}
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			modRegistry.SaveRegistryKey("TownName", txtTownName.Text);
			modRegistry.SaveRegistryKey("TownAddress1", txtTownAddress[0].Text);
			modRegistry.SaveRegistryKey("TownAddress2", txtTownAddress[1].Text);
			modRegistry.SaveRegistryKey("TownAddress3", txtTownAddress[2].Text);
			modRegistry.SaveRegistryKey("TownAddress4", txtTownAddress[3].Text);
			cmdCancelPrint_Click();
			//Application.DoEvents();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptPurchaseOrder.InstancePtr);
		}

		private void cmdRetrieve_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: tempAccount As int	OnWrite(string)
			int tempAccount = 0;
			int counter;
			string temp = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			if (lstRecords.Items[lstRecords.SelectedIndex].Text.Length > 0)
			{
				// if a record is highlighted
				tempAccount = FCConvert.ToInt32(Strings.Mid(lstRecords.Items[lstRecords.SelectedIndex].Text, 1, 5));
				// get the account number of that record
			}
			if (tempAccount != 0)
			{
				// if there is a valid account number
				rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(tempAccount));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// if there is a record
					rs.MoveLast();
					rs.MoveFirst();
					cboJournal.Enabled = true;
					txtVendor.Enabled = true;
					for (counter = 0; counter <= 3; counter++)
					{
						txtAddress[counter].Enabled = true;
					}
					txtCity.Enabled = true;
					txtState.Enabled = true;
					txtZip.Enabled = true;
					txtZip4.Enabled = true;
					txtDate.Enabled = true;
					txtDescription.Enabled = true;
					if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
					{
						txtPO.Enabled = false;
					}
					else
					{
						txtPO.Enabled = true;
					}
					txtAmount.Enabled = true;
					vs1.Enabled = true;
					temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
					txtVendor.Text = Strings.Format(temp, "0000");
					txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
					txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
					txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
					txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
					txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("CheckCity"));
					txtState.Text = FCConvert.ToString(rs.Get_Fields_String("CheckState"));
					txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip"));
					txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip4"));
					lstRecords.Clear();
					// clear the list box
					frmInfo.Visible = false;
					// make the list of records invisible
					if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
					{
						MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtDescription.Focus();
				}
			}
		}

		public void cmdRetrieve_Click()
		{
			cmdRetrieve_Click(cmdRetrieve, new System.EventArgs());
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			int counter;
			frmInfo.Visible = false;
			lstRecords.Clear();
			cboJournal.Enabled = true;
			txtVendor.Enabled = true;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = true;
			}
			txtCity.Enabled = true;
			txtState.Enabled = true;
			txtZip.Enabled = true;
			txtZip4.Enabled = true;
			txtDate.Enabled = true;
			txtDescription.Enabled = true;
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
			{
				txtPO.Enabled = false;
			}
			else
			{
				txtPO.Enabled = true;
			}
			txtAmount.Enabled = true;
			vs1.Enabled = true;
			txtVendor.Focus();
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			frmSearch.Visible = false;
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmSearch.Visible = true;
				txtSearch.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM (SELECT *, CheckName as VendorSearchName FROM VendorMaster WHERE CheckName like '" + modCustomReport.FixQuotes(txtSearch.Text) + "%' AND Status <> 'D' UNION ALL SELECT *, CheckAddress1 as VendorSearchName FROM VendorMaster WHERE upper(left(CheckAddress1, " + FCConvert.ToString(1 + txtSearch.Text.Length) + ")) = '*" + modCustomReport.FixQuotes(Strings.UCase(txtSearch.Text)) + "' AND upper(left(CheckName, " + FCConvert.ToString(txtSearch.Text.Length) + ")) <> '" + modCustomReport.FixQuotes(Strings.UCase(txtSearch.Text)) + "' AND Status <> 'D') as temp ORDER BY VendorSearchName");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					txtSearch.Text = "";
					frmWait.InstancePtr.Unload();
					cboJournal.Enabled = true;
					txtVendor.Enabled = true;
					for (counter = 0; counter <= 3; counter++)
					{
						txtAddress[counter].Enabled = true;
					}
					txtCity.Enabled = true;
					txtState.Enabled = true;
					txtZip.Enabled = true;
					txtZip4.Enabled = true;
					txtDate.Enabled = true;
					txtDescription.Enabled = true;
					if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
					{
						txtPO.Enabled = false;
					}
					else
					{
						txtPO.Enabled = true;
					}
					txtAmount.Enabled = true;
					vs1.Enabled = true;
					temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
					txtVendor.Text = Strings.Format(temp, "00000");
					txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
					txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
					txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
					txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
					txtCity.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckCity")));
					txtState.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckState")));
					txtZip.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip")));
					txtZip4.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4")));
					if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
					{
						MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtDescription.Focus();
				}
				else
				{
					txtSearch.Text = "";
					Fill_List();
					frmWait.InstancePtr.Unload();
					frmInfo.Visible = true;
					// make the list of records visible
					// show listbox with all records
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Records Found That Match That Name", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboJournal.Enabled = true;
				txtVendor.Enabled = true;
				for (counter = 0; counter <= 3; counter++)
				{
					txtAddress[counter].Enabled = true;
				}
				txtCity.Enabled = true;
				txtState.Enabled = true;
				txtZip.Enabled = true;
				txtZip4.Enabled = true;
				txtDate.Enabled = true;
				txtDescription.Enabled = true;
				if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
				{
					txtPO.Enabled = false;
				}
				else
				{
					txtPO.Enabled = true;
				}
				txtAmount.Enabled = true;
				vs1.Enabled = true;
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmEncumbranceDataEntry_Activated(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsProjects = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (!blnFromVendorMaster)
			{
				if (!SearchFlag)
				{
					NumberCol = 0;
					DescriptionCol = 1;
					AccountCol = 2;
					ProjectCol = 3;
					AmountCol = 4;
					vs1.ColWidth(NumberCol, 0);
					vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3709));
					vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3176));
					vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0819));
					vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2000));
					//FC:FINAL:DDU:#2896 - aligned columns
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, AmountCol, 4);
					vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vs1.TextMatrix(0, DescriptionCol, "Description");
					vs1.TextMatrix(0, AccountCol, "Account");
					vs1.TextMatrix(0, ProjectCol, "Proj");
					vs1.TextMatrix(0, AmountCol, "Amount");
					vs1.ColFormat(AmountCol, "#,##0.00");
					strComboList = "# ; " + "\t" + " |";
					if (modBudgetaryMaster.Statics.ProjectFlag)
					{
						rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
						if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
						{
							do
							{
								strComboList += "" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
								rsProjects.MoveNext();
							}
							while (rsProjects.EndOfFile() != true);
							strComboList = Strings.Left(strComboList, strComboList.Length - 1);
						}
						vs1.ColComboList(ProjectCol, strComboList);
					}
					FillJournalCombo();
					if (!modBudgetaryMaster.Statics.blnEncEdit)
					{
						for (counter = 1; counter <= vs1.Rows - 1; counter++)
						{
							vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
						}
						txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
						txtPeriod.Text = CalculatePeriod();
						if (!OldJournal)
						{
							cboJournal.SelectedIndex = 0;
							cboSaveJournal.SelectedIndex = 0;
						}
						else
						{
							SetCombo(OldJournalNumber, OldPeriod);
						}
						AmountToPay = 0;
						TotalAmount = 0;
						if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
						{
							txtPO.Text = "Auto";
							txtPO.Enabled = false;
						}
					}
					else
					{
						if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
						{
							txtPO.Enabled = false;
						}
						FillJournalCombo();
						SetCombo(OldJournalNumber, OldPeriod);
						AmountToPay = FCConvert.ToDouble(txtAmount.Text);
						CalculateTotals();
						lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
					}
					txtAmount.Focus();
					txtVendor.Focus();
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					Form_Resize();
					CurrJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					// initialize current journal
					this.Refresh();
				}
				else
				{
					SearchFlag = false;
				}
			}
			else
			{
				blnFromVendorMaster = false;
			}
		}

		private void frmEncumbranceDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmEncumbranceDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEncumbranceDataEntry.FillStyle	= 0;
			//frmEncumbranceDataEntry.ScaleWidth	= 9300;
			//frmEncumbranceDataEntry.ScaleHeight	= 7410;
			//frmEncumbranceDataEntry.LinkTopic	= "Form2";
			//frmEncumbranceDataEntry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			blnJournalLocked = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = 2;
			vsGrid.AllowSplits = modRegionalTown.IsRegionalTown();
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			RetrieveInfo();
			blnPostJournal = false;
			if (txtAmount.Text == "")
			{
				txtAmount.Text = Strings.Format(0, "#,##.00");
			}
		}

		private void frmEncumbranceDataEntry_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3709));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3176));
			vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0819));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1187));
			fraTownInfo.CenterToContainer(this.ClientArea);
			fraAccountBalance.CenterToContainer(this.ClientArea);
			fraJournalSave.CenterToContainer(this.ClientArea);
			frmInfo.CenterToContainer(this.ClientArea);
			frmSearch.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmEncumbranceDataEntry_Resize(this, new System.EventArgs());
		}

		private void FrmEncumbranceDataEntry_FormClosed(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			if (openFormWhenThisClose)
			{
				frmGetEncDataEntry.InstancePtr.Show(App.MainForm);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			//FC:FINAL:DSE:#558 Hide the current form before opening the new one, so that the focus would be on the newly opened form
			//this.Hide();
			openFormWhenThisClose = true;
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void frmEncumbranceDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					KeyAscii = (Keys)0;
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmEncumbranceDataEntry.InstancePtr.ActiveControl.GetName() != "vs1" && frmEncumbranceDataEntry.InstancePtr.ActiveControl.GetName() != "txtSearch")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void lblExpense_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)((FCConvert.ToInt32(e.Button) / 0x100000));
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			string strAcct = "";
			if (Button == MouseButtonConstants.RightButton)
			{
				strAcct = FCConvert.ToString(vs1.TextMatrix(vs1.Row, AccountCol));
				if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(strAcct, 1) != "E" && Strings.Left(strAcct, 1) != "P")
					{
						// do nothing
					}
					else
					{
						lblBalAccount.Text = "Account: " + strAcct;
						lblBalNetBudget.Text = Strings.Format(GetOriginalBudget(strAcct) - GetBudgetAdjustments(strAcct), "#,##0.00");
						lblBalPostedYTDNet.Text = Strings.Format(GetYTDNet(strAcct) + GetEncumbrance(strAcct), "#,##0.00");
						lblBalPendingYTDNet.Text = Strings.Format(GetPendingYTDNet(strAcct), "#,##0.00");
						lblBalBalance.Text = Strings.Format(FCConvert.ToDouble(lblBalNetBudget.Text) - FCConvert.ToDouble(lblBalPostedYTDNet.Text) - FCConvert.ToDouble(lblBalPendingYTDNet.Text), "#,##0.00");
						fraAccountBalance.Visible = true;
					}
				}
			}
		}

		private void lstRecords_DoubleClick(object sender, System.EventArgs e)
		{
			cmdRetrieve_Click();
		}

		private void mnuFileAddVendor_Click(object sender, System.EventArgs e)
		{
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			modBudgetaryMaster.Statics.blnEdit = false;
			// else show we are creating a new vendor account
			modBudgetaryMaster.Statics.blnFromEnc = true;
			frmVendorMaster.InstancePtr.Show(App.MainForm);
			// show the blankform
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool NoSaveFlag = false;
			vs1.Col = 1;
			if (this.ActiveControl.GetName() != "txtAddress")
			{
				txtDescription.Focus();
			}
			else
			{
				txtAddress_Validate_8(FCConvert.ToInt16(this.ActiveControl.GetIndex()), false);
			}
			//Application.DoEvents();
			if (txtVendor.Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			else if (Conversion.Val(txtVendor.Text) == 0 && txtAddress[0].Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtAddress[0].Focus();
				return;
			}
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must fill in the Description before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (txtPeriod.Text == "")
			{
				MessageBox.Show("You must fill in the Period before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPeriod.Focus();
				return;
			}
			if (txtPO.Text == "")
			{
				MessageBox.Show("You must fill in the PO Number before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPO.Focus();
				return;
			}
			CalculateTotals();
			if (FCConvert.ToDouble(Strings.Format((AmountToPay - TotalAmount), "####.00")) != 0)
			{
				MessageBox.Show("The Remaining Amount must be 0.00 before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtAmount.Text == "" || txtAmount.Text == ".00" || txtAmount.Text == "0.00")
			{
				MessageBox.Show("There must be an Amount to Pay in a Journal Entry", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToString(vs1.TextMatrix(counter, AmountCol)) != "0")
				{
					if (FCConvert.ToString(vs1.TextMatrix(counter, AccountCol)) == "" || FCConvert.ToString(vs1.TextMatrix(counter, DescriptionCol)) == "")
					{
						NoSaveFlag = true;
						break;
					}
				}
			}
			if (NoSaveFlag == true)
			{
				NoSaveFlag = false;
				MessageBox.Show("Each entry must have a Description, Account, Project Number, and an Amount to be valid", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				txtTownName.Text = modRegistry.GetRegistryKey("TownName");
				txtTownAddress[0].Text = modRegistry.GetRegistryKey("TownAddress1");
				txtTownAddress[1].Text = modRegistry.GetRegistryKey("TownAddress2");
				txtTownAddress[2].Text = modRegistry.GetRegistryKey("TownAddress3");
				txtTownAddress[3].Text = modRegistry.GetRegistryKey("TownAddress4");
				fraTownInfo.Visible = true;
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int temp = 0;
			if (FCConvert.ToString(vs1.TextMatrix(vs1.Row, DescriptionCol)) == "" && FCConvert.ToString(vs1.TextMatrix(vs1.Row, AccountCol)) == "" && FCConvert.ToString(vs1.TextMatrix(vs1.Row, ProjectCol)) == "" && FCConvert.ToString(vs1.TextMatrix(vs1.Row, AmountCol)) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, AmountCol, vs1.Row, NumberCol);
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					//FC:FINAL:VGE - #563 Data from selected field is being carried over to row bellow. Disabling data grid to prevent.
					vs1.Enabled = false;
					DeleteFlag = true;
					temp = vs1.Row;
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
					}
					else
					{
						vs1.Row -= 1;
					}
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					CalculateTotals();
					lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
					DeleteFlag = false;
					//FC:FINAL:VGE - #563 Reenabling data grid.
					vs1.Enabled = true;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DescriptionCol);
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DescriptionCol);
				}
			}
		}

		private void mnuProcessDeleteEntry_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			clsDRWrapper rsDetail = new clsDRWrapper();
			int lngRecordToDelete = 0;
			clsDRWrapper rsJournalEntriesLeft = new clsDRWrapper();
			// make sure they want to delete this item
			counter = MessageBox.Show("Are you sure you want to delete this journal entry?", "Delete This Journal Entry?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (counter == DialogResult.Yes)
			{
				rsDetail.Execute("DELETE FROM EncumbranceDetail WHERE EncumbranceID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"), "Budgetary");
				if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
					{
						lngRecordToDelete = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
						if (lngRecordToDelete == FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")))
						{
							modBudgetaryAccounting.Statics.SearchResults.MoveNext();
							FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
						}
						else
						{
							FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							do
							{
								modBudgetaryAccounting.Statics.SearchResults.MoveNext();
							}
							while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != lngRecordToDelete);
						}
						modBudgetaryAccounting.Statics.SearchResults.Delete();
						modBudgetaryAccounting.Statics.SearchResults.Update();
						modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
						GetJournalData();
						btnProcessPreviousEntry.Enabled = false;
						if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
						{
							btnProcessNextEntry.Enabled = true;
						}
						else
						{
							btnProcessNextEntry.Enabled = false;
						}
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.Delete();
						modBudgetaryAccounting.Statics.SearchResults.Update();
						if (btnProcessNextEntry.Enabled)
						{
							if (btnProcessPreviousEntry.Enabled == false)
							{
								mnuProcessNextEntry_Click();
								btnProcessPreviousEntry.Enabled = false;
							}
							else
							{
								mnuProcessNextEntry_Click();
							}
						}
						else
						{
							mnuProcessPreviousEntry_Click();
							btnProcessNextEntry.Enabled = false;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					rsJournalEntriesLeft.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber") + " AND Status = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status") + "' AND ID <> " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
					if (rsJournalEntriesLeft.EndOfFile() != true && rsJournalEntriesLeft.BeginningOfFile() != true)
					{
						// do nothing
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						rsDetail.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status") + "' AND JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
						if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
						{
							rsDetail.Edit();
							rsDetail.Set_Fields("Status", "D");
							rsDetail.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
							rsDetail.Set_Fields("StatusChangeDate", DateTime.Today);
							rsDetail.Update();
						}
					}
					modBudgetaryAccounting.Statics.SearchResults.Delete();
					modBudgetaryAccounting.Statics.SearchResults.Update();
					//Application.DoEvents();
					frmGetEncDataEntry.InstancePtr.FillJournals();
					mnuProcessQuit_Click();
				}
			}
		}

		private void mnuProcessNextEntry_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 1;
			//Application.DoEvents();
			txtVendor.Focus();
			if (VendorNumber == FirstRecordShown)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
					{
						btnProcessNextEntry.Enabled = false;
					}
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
				}
			}
			else
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					}
					else
					{
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					}
				}
				else
				{
					btnProcessNextEntry.Enabled = false;
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
				}
			}
			if (btnProcessPreviousEntry.Enabled == false)
			{
				btnProcessPreviousEntry.Enabled = true;
			}
			GetJournalData();
		}

		public void mnuProcessNextEntry_Click()
		{
			mnuProcessNextEntry_Click(btnProcessNextEntry, new System.EventArgs());
		}

		private void mnuProcessPreviousEntry_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 1;
			//Application.DoEvents();
			txtVendor.Focus();
			modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
			if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				btnProcessPreviousEntry.Enabled = false;
			}
			else
			{
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						btnProcessPreviousEntry.Enabled = false;
					}
				}
			}
			if (btnProcessNextEntry.Enabled == false)
			{
				btnProcessNextEntry.Enabled = true;
			}
			GetJournalData();
		}

		public void mnuProcessPreviousEntry_Click()
		{
			mnuProcessPreviousEntry_Click(btnProcessPreviousEntry, new System.EventArgs());
		}

		private void mnuProcessQuit_Click()
		{
			Close();
		}
		//FC:FINAL:MSH - Issue #818 - method for handling 'Save' button and 'F12' hotkey pressing
		private void cmdProcessSave_Click(object sender, System.EventArgs e)
		{
			mnuProcessSave_Click();
		}

		private void mnuProcessSave_Click()
		{
			blnUnload = true;
			SaveInfo();
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			int counter;
			lblExpense.Text = "";
			frmSearch.Visible = true;
			cboJournal.Enabled = false;
			txtVendor.Enabled = false;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = false;
			}
			txtCity.Enabled = false;
			txtState.Enabled = false;
			txtZip.Enabled = false;
			txtZip4.Enabled = false;
			txtDate.Enabled = false;
			txtDescription.Enabled = false;
			txtPO.Enabled = false;
			txtAmount.Enabled = false;
			vs1.Enabled = false;
			txtSearch.Focus();
		}

		public void mnuProcessSearch_Click()
		{
			mnuProcessSearch_Click(btnProcessSearch, new System.EventArgs());
		}

		private void txtAddress_Enter(int Index, object sender, System.EventArgs e)
		{
			int counter;
			int tempIndex;
			lblExpense.Text = "";
			if (!SearchFlag)
			{
				if (txtVendor.Text == "")
				{
					txtVendor.Text = "0000";
				}
				else if (Conversion.Val(txtVendor.Text) != 0)
				{
					txtDescription.Focus();
				}
				else
				{
					if (Index > 0)
					{
						if (txtAddress[Index - 1].Text == "")
						{
							for (counter = Index - 1; counter >= 0; counter--)
							{
								if (txtAddress[counter].Text == "")
								{
									tempIndex = counter;
								}
							}
							txtCity.Focus();
						}
					}
				}
			}
		}

		private void txtAddress_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - if the text field is empty, sender will be equal 0 (Int32 type), not 'T2KOverTypeBox' type (same with issue #795)
			if (sender.GetType() == typeof(T2KOverTypeBox))
			{
				int index = txtAddress.IndexOf((Global.T2KOverTypeBox)sender);
				txtAddress_Enter(index, sender, e);
			}
		}

		private void txtAddress_Validate_8(short Index, bool Cancel)
		{
			txtAddress_Validate(Index, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtAddress_Validate(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			int counter;
			if (Index < 3)
			{
				if (Strings.Trim(txtAddress[Index + 1].Text) != "")
				{
					if (txtAddress[Index].Text == "")
					{
						for (counter = Index + 1; counter <= 3; counter++)
						{
							txtAddress[counter - 1].Text = txtAddress[counter].Text;
						}
						txtAddress[3].Text = "";
					}
				}
			}
		}

		private void txtAddress_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//FC:FINAL:MSH - if the text field is empty, sender will be equal 0 (Int32 type), not 'T2KOverTypeBox' type (same with issue #795)
			if (sender.GetType() == typeof(T2KOverTypeBox))
			{
				int index = txtAddress.IndexOf((Global.T2KOverTypeBox)sender);
				txtAddress_Validate(index, sender, e);
			}
		}

		private void txtAmount_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAmount.Text = "";
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
		}

		private void txtAmount_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtAmount.Text != "")
			{
				AmountToPay = FCConvert.ToDouble(txtAmount.Text);
				lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
			}
			else
			{
				AmountToPay = 0;
				lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
			}
		}

		private void txtCity_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
				return;
			}
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtDate.Text == "00/00/00")
			{
				txtDate.Text = FCConvert.ToString(DateTime.Today);
			}
		}

		private void txtDescription_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
		}

		private void txtDate_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
		}

		private void txtPeriod_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
			}
			else
			{
				txtPeriod.SelectionStart = 0;
				txtPeriod.SelectionLength = txtPeriod.Text.Length;
			}
			lblExpense.Text = "";
		}

		private void txtPO_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdSearch_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtState_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
				return;
			}
		}

		private void txtVendor_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#568 - leave the buttons enabled all the time
			//btnProcessSearch.Enabled = true;
			//btnFileAddVendor.Enabled = true;
			lblExpense.Text = "";
		}

		private void txtVendor_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.A)
			{
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				modBudgetaryMaster.Statics.blnEdit = false;
				// else show we are creating a new vendor account
				modBudgetaryMaster.Statics.blnFromEnc = true;
				frmVendorMaster.InstancePtr.Show(App.MainForm);
				// show the blankform
			}
			else if (KeyCode == Keys.S)
			{
				KeyCode = 0;
				mnuProcessSearch_Click();
			}
		}

		private void txtVendor_Leave(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#568 - leave the buttons enabled all the time
			//btnProcessSearch.Enabled = false;
			//btnFileAddVendor.Enabled = false;
		}

		public void txtVendor_Validate(bool validate)
		{
			txtVendor_Validate(txtVendor, new System.ComponentModel.CancelEventArgs(validate));
		}

		public void txtVendor_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			//FC:FINAL:DDU:#1461 - preventing validating textbox
			if (!(txtVendor.Text.ToUpper().Contains("S") || txtVendor.Text.ToUpper().Contains("A")))
			{
				if ((txtVendor.SelectionStart > 0 && Conversion.Val(txtVendor.Text) != 0) || blnFromVendor)
				{
					blnFromVendor = false;
					if (!Information.IsNumeric(txtVendor.Text))
					{
						MessageBox.Show("You may only enter an ID in this field", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtVendor.Text = "";
						e.Cancel = true;
					}
					else
					{
						temp = txtVendor.Text;
						txtVendor.Text = Strings.Format(temp, "00000");
                        if (!GetVendorInfo())
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            //FC:FINAL:AM:#4351 - set the focu
                            this.txtDescription.Focus();
                        }
					}
				}
				else
				{
					if (!SearchFlag)
					{
						if (txtVendor.Text == "" || Conversion.Val(txtVendor.Text) == 0)
						{
							txtVendor.Text = "0000";
							txtAddress[0].Focus();
						}
					}
				}
			}
		}

		private bool GetVendorInfo()
		{
			bool GetVendorInfo = false;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter;
			int counter2;
			string strLabel = "";
			counter = 1;
			rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)) + "AND Status <> 'D'");
			if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "S")
				{
					MessageBox.Show("This vendor may not be used at this time because it has a status of S", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtVendor.Text = "";
					GetVendorInfo = false;
					return GetVendorInfo;
				}
				txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
				txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
				txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
				txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
				txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("CheckCity"));
				txtState.Text = FCConvert.ToString(rs.Get_Fields_String("CheckState"));
				txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip"));
				txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip4"));
				for (counter = counter; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 1, "");
					vs1.TextMatrix(counter, 2, "");
					vs1.TextMatrix(counter, 3, "");
				}
				CalculateTotals();
				lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
				if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
				{
					MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				GetVendorInfo = true;
			}
			else
			{
				MessageBox.Show("There is no Vendor that matches that number", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Text = "";
				GetVendorInfo = false;
			}
			return GetVendorInfo;
		}

		private void Fill_List()
		{
			string temp;
			int I;
			//FC:FINAL:BBE:#584 - wisej do not supported spaces and tabs in ListViewItems. Use colums instead.
			lstRecords.Columns.Clear();
			lstRecords.Columns.Add("VendorNumber", 120);
			lstRecords.Columns.Add("VendorName", lstRecords.Width - lstRecords.Columns[0].Width - 20);
			temp = "";
			// this fills the listbox with the account information so that the user can dblclick
			// an account to choose it
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				//Application.DoEvents();
				if (!rs.IsFieldNull("VendorNumber"))
				{
					temp = FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber"));
					temp = Strings.Format(temp, "00000");
					temp += "\t";
				}
				if (FCConvert.ToString(rs.Get_Fields_String("CheckName")) != "")
					temp += rs.Get_Fields_String("CheckName") + " // " + rs.Get_Fields_String("CheckAddress1");
				if (I < rs.RecordCount())
					rs.MoveNext();
				lstRecords.AddItem(temp);
				// go to the next record
			}
			//FC:FINAL:DDU:#1459 - prevent user to resize columns
			for (int i = 0; i < lstRecords.Columns.Count; i++)
			{
				lstRecords.Columns[i].Resizable = false;
			}
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
				return;
			}
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtZip4_Enter(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
				return;
			}
		}

		private void txtZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				// vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
				Decimal curBalance;
				if (vs1.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
				if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
				{
					if (Strings.InStr(1, vs1.EditText, "_", CompareConstants.vbBinaryCompare) == 0)
					{
						if (Strings.Left(vs1.EditText, 1) != "E" && Strings.Left(vs1.EditText, 1) != "P")
						{
							// do nothing
						}
						else
						{
							curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.EditText) - GetBudgetAdjustments(vs1.EditText));
							curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.EditText) + GetEncumbrance(vs1.EditText));
							curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.EditText));
							lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
						}
					}
				}
			}
		}

        //FC:FINAl:SBE - #1457 - Enter event cannot be used to disable focus of grid, when cell is already in edit mode. Disable editing the grid using the same conditions from Enter event
        private void vs1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (txtVendor.Text == "")
            {
                txtVendor.Text = "0000";
            }
            if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
            {
                e.Cancel = true;
                txtAddress[0].Focus();
            }
        }

        private void vs1_Enter(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
			Decimal curBalance;
			//FC:FINAL:DDU:#1457 - prevent user from inserting data in grid
			if (txtVendor.Text == "")
			{
				txtVendor.Text = "0000";
			}
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
                if (vs1.IsCurrentCellInEditMode)
                {
                    vs1.EndEdit();
                }
				txtAddress[0].Focus();
				return;
			}
			mnuProcessDelete.Enabled = true;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
			{
				if (Strings.InStr(1, vs1.TextMatrix(vs1.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "E" && Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "P")
					{
						// do nothing
					}
					else
					{
						curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.TextMatrix(vs1.Row, AccountCol)) - GetBudgetAdjustments(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)) + GetEncumbrance(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)));
						lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
					}
				}
			}
			if (frmEncumbranceDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DescriptionCol;
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			else if (vs1.Col != AccountCol)
			{
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					if (vs1.Col == DescriptionCol)
					{
						if (vs1.EditText == "")
						{
							vs1.EditText = txtDescription.Text;
						}
					}
					vs1.Col += 1;
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.Row += 1;
						vs1.Col = 0;
					}
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
			Decimal curBalance;
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
				return;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
			{
				if (Strings.InStr(1, vs1.TextMatrix(vs1.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "E" && Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "P")
					{
						// do nothing
					}
					else
					{
						curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.TextMatrix(vs1.Row, AccountCol)) - GetBudgetAdjustments(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)) + GetEncumbrance(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)));
						lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
					}
				}
			}
			if (vs1.Col == NumberCol)
			{
				vs1.Col = 1;
			}
			else if (vs1.Col == DescriptionCol)
			{
				vs1.EditMaxLength = 25;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else if (vs1.Col == ProjectCol)
			{
				if (modBudgetaryMaster.Statics.ProjectFlag && strComboList != "")
				{
					vs1.EditCell();
				}
				else
				{
					vs1.Col += 1;
				}
			}
			else if (vs1.Col == AmountCol)
			{
				vs1.EditMaxLength = 10;
				if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
				return;
			}
			if (vs1.Col != AccountCol)
			{
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					if (vs1.Col == AmountCol)
					{
						if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
						{
							vs1.EditCell();
							vs1.EditSelStart = 0;
							vs1.EditSelLength = 1;
						}
						else
						{
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == AccountCol && KeyCode != Keys.F9)
			{
				if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
				{
					KeyCode = 0;
					vs1.Col -= 1;
				}
				else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
				{
					KeyCode = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					if (vs1.Col == DescriptionCol)
					{
						if (vs1.EditText == "")
						{
							vs1.EditText = txtDescription.Text;
						}
					}
					vs1.Col += 1;
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.Row += 1;
						vs1.Col = 0;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (modBudgetaryMaster.Statics.ProjectFlag == false || strComboList == "")
				{
					if (vs1.Col == AmountCol)
					{
						if (vs1.EditSelStart == 0)
						{
							KeyCode = 0;
							vs1.Col = AccountCol;
						}
					}
				}
			}
			else if (vs1.Col == AmountCol)
			{
				if (KeyCode == Keys.C)
				{
					KeyCode = 0;
					vs1.EditText = "";
					Support.SendKeys("{BACKSPACE}", false);
				}
				if (KeyCode == Keys.Insert)
				{
					KeyCode = 0;
					if (vs1.Col == AmountCol)
					{
						if (Information.IsNumeric(vs1.TextMatrix(vs1.Row, vs1.Col)))
						{
							vs1.EditText = FCConvert.ToString(FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) + FCConvert.ToDecimal(lblRemAmount.Text));
						}
						else
						{
							vs1.EditText = FCConvert.ToString(FCConvert.ToDecimal(lblRemAmount.Text));
						}
					}
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string temp2 = "";
			float percent;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == AmountCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
				else if (vs1.EditText == "")
				{
					vs1.EditText = "0";
				}
				else
				{
					temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
					if (temp > 0)
					{
						if (vs1.EditText.Length > temp + 2)
						{
							vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
						}
					}
					if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
					{
						if (!ValidateBalance_6(vs1.TextMatrix(row, AccountCol), FCConvert.ToDecimal(vs1.EditText)))
						{
							e.Cancel = true;
							vs1.EditText = "";
							return;
						}
					}
					CalculateTotals();
					lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
				}
			}
			else if (col == ProjectCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Project Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
			}
			else if (col == AccountCol)
			{
				if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
				{
					if (!ValidateBalance_6(vs1.EditText, FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol))))
					{
						e.Cancel = true;
						vs1.EditText = "";
						return;
					}
				}
			}
		}

		private void CalculateTotals()
		{
			int counter;
			TotalAmount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (counter == vs1.Row)
				{
					if (vs1.Col == AmountCol)
					{
						if (Strings.Trim(vs1.EditText) != "")
						{
							TotalAmount += FCConvert.ToDouble(vs1.EditText);
						}
					}
					else
					{
						if (Strings.Trim(vs1.TextMatrix(counter, AmountCol)) != "")
						{
							TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
						}
					}
				}
				else
				{
					if (Strings.Trim(vs1.TextMatrix(counter, AmountCol)) != "")
					{
						TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
					}
				}
			}
		}

		private void SaveDetails()
		{
			int counter;
			rs.OmitNullsOnInsert = true;
			if (modBudgetaryMaster.Statics.blnEncEdit)
			{
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(VendorNumber));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						rs.Delete();
						rs.Update();
					}
				}
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToString(vs1.TextMatrix(counter, AmountCol)) != "0")
				{
					rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = 0");
					rs.AddNew();
					// if so add it
					rs.Set_Fields("EncumbranceID", VendorNumber);
					rs.Set_Fields("Description", FCConvert.ToString(vs1.TextMatrix(counter, DescriptionCol)));
					rs.Set_Fields("account", FCConvert.ToString(vs1.TextMatrix(counter, AccountCol)));
					rs.Set_Fields("Amount", FCConvert.ToString(vs1.TextMatrix(counter, AmountCol)));
					rs.Set_Fields("Project", FCConvert.ToString(vs1.TextMatrix(counter, ProjectCol)));
					rs.Update();
					// update the database
				}
				else
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) != 0)
					{
						rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
						rs.Delete();
						rs.Update();
					}
				}
			}
		}

		private void GetJournalData()
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			SetCombo_6(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber")), FCConvert.ToInt16(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period")));
			txtVendor.Text = modValidateAccount.GetFormat_6(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"), 5);
			if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")) == 0)
			{
				txtAddress[0].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorName"));
				txtAddress[1].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress1"));
				txtAddress[2].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress2"));
				txtAddress[3].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress3"));
				txtCity.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorCity"));
				txtState.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorState"));
				txtZip.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorZip"));
				txtZip4.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorZip4"));
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
				txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
				txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
				txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
				txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
				txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
				txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
				txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
				txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
			}
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("EncumbrancesDate") != null)
				txtDate.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("EncumbrancesDate"));
			txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			txtPeriod.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period"), "00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			txtAmount.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount"), "#,##0.00");
			AmountToPay = FCConvert.ToDouble(txtAmount.Text);
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PO") != null)
			{
				// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
				txtPO.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PO"));
			}
			else
			{
				txtPO.Text = "";
			}
			VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
			GetJournalDetails();
		}

		private void ClearData()
		{
			int counter;
			txtVendor.Text = "";
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Text = "";
			}
			txtCity.Text = "";
			txtState.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			txtAmount.Text = "";
			txtDescription.Text = "";
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
			{
				txtPO.Text = "Auto";
			}
			else
			{
				txtPO.Text = "";
			}
			VendorNumber = 0;
			AmountToPay = 0;
			TotalAmount = 0;
			txtDate.Text = FCConvert.ToString(DateTime.Today);
			vs1.Clear();
			lblExpense.Text = "";
			lblRemAmount.Text = "0.00";
			vs1.Rows = 16;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
			}
			//FC:FINAL:DDU:#2896 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, AmountCol, 4);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, ProjectCol, "Proj");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.Select(1, 1);
		}

		private void GetJournalDetails()
		{
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			rs2.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				if (rs2.RecordCount() > 15)
				{
					vs1.Rows = rs2.RecordCount() + 1;
				}
				else
				{
					vs1.Rows = 16;
				}
				counter = 1;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
					vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("account")));
					vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields("Amount")));
					rs2.MoveNext();
					counter += 1;
				}
				if (counter < vs1.Rows - 1)
				{
					for (counter = counter; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 1, "");
						vs1.TextMatrix(counter, 2, "");
						vs1.TextMatrix(counter, 3, "");
					}
				}
			}
		}
		// vbPorter upgrade warning: intPeriod As short	OnWriteFCConvert.ToInt32(
		private void SetCombo_6(int x, short intPeriod)
		{
			SetCombo(x, intPeriod);
		}

		private void SetCombo(int x, int intPeriod)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(intPeriod), 2) == Strings.Mid(cboJournal.Items[counter].ToString(), 19, 2))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			rs.OmitNullsOnInsert = true;
			txtVendor.Focus();
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "EN");
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Status = 'P'");
				if (Master.BeginningOfFile() != true && Master.EndOfFile() != true)
				{
					MessageBox.Show("Changes to this journal cannot be saved because it has been posted.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
					Close();
					return;
				}
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(txtPeriod.Text))
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", "Add Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (VendorNumber != 0)
							{
								rsDeleteInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(OldJournalNumber) + " AND Period = " + FCConvert.ToString(OldPeriod) + " AND ID <> " + FCConvert.ToString(VendorNumber));
								if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
								{
									// do nothing
								}
								else
								{
									rsDeleteInfo.Execute("DELETE FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(OldJournalNumber) + " AND Period = " + FCConvert.ToString(OldPeriod), "Budgetary");
									cboSaveJournal.Items.RemoveAt(cboSaveJournal.SelectedIndex);
									cboJournal.Items.RemoveAt(cboJournal.SelectedIndex);
								}
							}
							for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
							{
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
								{
									cboSaveJournal.SelectedIndex = counter;
									cboJournal.SelectedIndex = counter;
									break;
								}
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
					else
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  Do you wish to use this Journal Number?", "Update Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (Strings.Trim(txtJournalDescription.Text) != "" || VendorNumber != 0)
							{
								if (VendorNumber != 0)
								{
									rsDeleteInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(OldJournalNumber) + " AND Period = " + FCConvert.ToString(OldPeriod) + " AND ID <> " + FCConvert.ToString(VendorNumber));
									if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
									{
										if (Strings.Trim(txtJournalDescription.Text) == "")
										{
											fraJournalSave.Visible = true;
											lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
											lblJournalSave.Visible = false;
											cboSaveJournal.Visible = false;
											txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Encumbrance";
											lblJournalDescription.Visible = true;
											txtJournalDescription.Visible = true;
											txtJournalDescription.Focus();
											txtJournalDescription.SelectionStart = 0;
											txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
											return;
										}
										else
										{
											Master.AddNew();
											Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
											Master.Set_Fields("Status", "E");
											Master.Set_Fields("Description", txtJournalDescription.Text);
											Master.Set_Fields("Type", "EN");
											Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
											Master.Update();
											cboJournal.Clear();
											cboSaveJournal.Clear();
											FillJournalCombo();
											for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
											{
												// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
												if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
												{
													cboSaveJournal.SelectedIndex = counter;
													cboJournal.SelectedIndex = counter;
													break;
												}
											}
											Master.Reset();
										}
									}
									else
									{
										Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
										Master.Edit();
										Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
										Master.Update();
										cboJournal.Clear();
										cboSaveJournal.Clear();
										FillJournalCombo();
										for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
										{
											// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
											if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
											{
												cboSaveJournal.SelectedIndex = counter;
												cboJournal.SelectedIndex = counter;
												break;
											}
										}
										Master.Reset();
									}
								}
								else
								{
									Master.AddNew();
									Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
									Master.Set_Fields("Status", "E");
									Master.Set_Fields("Description", txtJournalDescription.Text);
									Master.Set_Fields("Type", "EN");
									Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
									Master.Update();
									cboJournal.Clear();
									cboSaveJournal.Clear();
									FillJournalCombo();
									for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
									{
										// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
										if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
										{
											cboSaveJournal.SelectedIndex = counter;
											cboJournal.SelectedIndex = counter;
											break;
										}
									}
									Master.Reset();
								}
							}
							else
							{
								fraJournalSave.Visible = true;
								lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
								lblJournalSave.Visible = false;
								cboSaveJournal.Visible = false;
								txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Encumbrance";
								lblJournalDescription.Visible = true;
								txtJournalDescription.Visible = true;
								txtJournalDescription.Focus();
								txtJournalDescription.SelectionStart = 0;
								txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
								return;
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			clsJournalInfo.JournalType = "EN";
			clsJournalInfo.Period = FCConvert.ToString(Conversion.Val(txtPeriod.Text));
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			if (VendorNumber != 0)
			{
				OldPeriod = FCConvert.ToInt32(txtPeriod.Text);
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(VendorNumber));
				rs.Edit();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					rs.Set_Fields("JournalNumber", TempJournal);
				}
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
				{
					rs.Set_Fields("TempVendorName", txtAddress[0].Text);
					rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
					rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
					rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
					rs.Set_Fields("TempVendorCity", Strings.Trim(txtCity.Text));
					rs.Set_Fields("TempVendorState", Strings.Trim(txtState.Text));
					rs.Set_Fields("TempVendorZip", Strings.Trim(txtZip.Text));
					rs.Set_Fields("TempVendorZip4", Strings.Trim(txtZip4.Text));
				}
				rs.Set_Fields("EncumbrancesDate", txtDate.Text);
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				rs.Set_Fields("PO", txtPO.Text);
				rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
				VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.Set_Fields("Status", "E");
				rs.Update();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = 0");
				rs.AddNew();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					rs.Set_Fields("JournalNumber", TempJournal);
				}
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
				{
					rs.Set_Fields("TempVendorName", txtAddress[0].Text);
					rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
					rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
					rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
					rs.Set_Fields("TempVendorCity", Strings.Trim(txtCity.Text));
					rs.Set_Fields("TempVendorState", Strings.Trim(txtState.Text));
					rs.Set_Fields("TempVendorZip", Strings.Trim(txtZip.Text));
					rs.Set_Fields("TempVendorZip4", Strings.Trim(txtZip4.Text));
				}
				rs.Set_Fields("EncumbrancesDate", txtDate.Text);
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				rs.Set_Fields("PO", txtPO.Text);
				rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
				rs.Set_Fields("Status", "E");
				rs.Update();
				VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			SaveDetails();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				modRegistry.SaveRegistryKey("CURRENCJRNL", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryMaster.Statics.CurrentEncEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				modRegistry.SaveRegistryKey("CURRENCJRNL", FCConvert.ToString(TempJournal));
				modBudgetaryMaster.Statics.CurrentEncEntry = TempJournal;
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
				FillJournalCombo();
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
			{
				MessageBox.Show("This entry was saved with PO# " + txtPO.Text, "Info Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				modBudgetaryMaster.IncrementPO();
				modBudgetaryMaster.UnlockPO();
			}
			if (!modBudgetaryMaster.Statics.blnEncEdit)
			{
				fraJournalSave.Visible = false;
				if (cboSaveJournal.SelectedIndex == 0)
				{
					FillJournalCombo();
					cboJournal.SelectedIndex = 1;
					cboSaveJournal.SelectedIndex = 1;
					txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Encumbrance";
				}
				ClearData();
				txtAmount.Focus();
				txtVendor.Focus();
				if (blnUnload)
				{
					if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptEncumbrance))
					{
						if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							blnPostJournal = true;
						}
					}
					else
					{
						blnPostJournal = false;
						if (cboSaveJournal.SelectedIndex != 0)
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
						}
						else
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
						}
					}
					Close();
				}
			}
			else
			{
				if (btnProcessNextEntry.Enabled == false || blnUnload)
				{
					if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptEncumbrance))
					{
						if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							blnPostJournal = true;
						}
					}
					else
					{
						blnPostJournal = false;
						if (cboSaveJournal.SelectedIndex != 0)
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
						}
						else
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
						}
					}
					Close();
				}
				else
				{
					blnPostJournal = false;
					mnuProcessNextEntry_Click();
				}
			}
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.Width = 90;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'EN' ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					while (cboJournal.ListCount < counter)
					{
						cboJournal.AddItem("");
					}
					while (cboSaveJournal.ListCount < counter)
					{
						cboSaveJournal.AddItem("");
					}
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			bool NoSaveFlag = false;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			int TempPO = 0;
			vs1.Col = 1;
			if (this.ActiveControl.GetName() != "txtAddress")
			{
				txtDescription.Focus();
			}
			else
			{
				txtAddress_Validate_8(FCConvert.ToInt16(this.ActiveControl.GetIndex()), false);
			}
			//Application.DoEvents();
			if (txtVendor.Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			else if (Conversion.Val(txtVendor.Text) == 0 && txtAddress[0].Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtAddress[0].Focus();
				return;
			}
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must fill in the Description before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (txtPeriod.Text == "")
			{
				MessageBox.Show("You must fill in the Period before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPeriod.Focus();
				return;
			}
			if (txtPO.Text == "")
			{
				MessageBox.Show("You must fill in the PO Number before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPO.Focus();
				return;
			}
			CalculateTotals();
			if (FCConvert.ToDouble(Strings.Format((AmountToPay - TotalAmount), "####.00")) != 0)
			{
				MessageBox.Show("The Remaining Amount must be 0.00 before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtAmount.Text == "" || txtAmount.Text == ".00" || txtAmount.Text == "0.00")
			{
				MessageBox.Show("There must be an Amount to Pay in a Journal Entry", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				//FC:FINAL:VGE - #567. Strings like "0.0" and "0,0" etc. are qualifying as Zero but are not equal to "0" as strings. Replaced with decimal condition
				//if (FCConvert.ToString(vs1.TextMatrix(counter, AmountCol)) != "0")
				if (FCUtils.ParseOrDefault(vs1.TextMatrix(counter, AmountCol)) != 0.0M)
				{
					if (FCConvert.ToString(vs1.TextMatrix(counter, AccountCol)) == "" || FCConvert.ToString(vs1.TextMatrix(counter, DescriptionCol)) == "")
					{
						NoSaveFlag = true;
						break;
					}
					if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
					{
						answer = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.No)
						{
							vs1.Select(counter, AccountCol);
							vs1.Focus();
							return;
						}
						else
						{
							modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "Encumbrance Data Entry");
						}
					}
				}
			}
			if (NoSaveFlag == true)
			{
				NoSaveFlag = false;
				MessageBox.Show("Each entry must have a Description, Account, Project Number, and an Amount to be valid", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (cboJournal.SelectedIndex == 0)
				{
					if (modBudgetaryAccounting.LockJournal() == false)
					{
						MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					blnJournalLocked = true;
					Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						TempJournal = 1;
					}
					if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
					{
						if (modBudgetaryMaster.LockPO() == false)
						{
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						else
						{
							TempPO = modBudgetaryMaster.GetNextPO();
						}
					}
					answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (answer == DialogResult.Cancel)
					{
						modBudgetaryMaster.UnlockPO();
						modBudgetaryAccounting.UnlockJournal();
						blnJournalLocked = false;
						if (Master.IsntAnything())
						{
							// do nothing
						}
						else
						{
							Master.Reset();
						}
						return;
					}
					else if (answer == DialogResult.No)
					{
						if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
						{
							txtPO.Text = FCConvert.ToString(TempPO);
						}
						fraJournalSave.Visible = true;
						lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
						lblJournalSave.Visible = true;
						cboSaveJournal.Visible = true;
						//FC:FINAL:DDU:#2872 - show textbox when selecting item in combobox
						cboSaveJournal.Text = "";
						lblJournalDescription.Visible = false;
						txtJournalDescription.Visible = false;
						cboSaveJournal.Focus();
						if (Master.IsntAnything())
						{
							// do nothing
						}
						else
						{
							Master.Reset();
						}
						return;
					}
					else
					{
						if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
						{
							txtPO.Text = FCConvert.ToString(TempPO);
						}
						fraJournalSave.Visible = true;
						lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
						lblJournalSave.Visible = false;
						cboSaveJournal.Visible = false;
						txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Encumbrance";
						lblJournalDescription.Visible = true;
						txtJournalDescription.Visible = true;
						txtJournalDescription.Focus();
						txtJournalDescription.SelectionStart = 0;
						txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
						return;
					}
				}
				else
				{
					if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")))
					{
						if (modBudgetaryMaster.LockPO() == false)
						{
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						else
						{
							txtPO.Text = FCConvert.ToString(modBudgetaryMaster.GetNextPO());
						}
					}
					SaveJournal();
				}
			}
		}

		private double GetOriginalBudget(string strAcct)
		{
			double GetOriginalBudget = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
				GetOriginalBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal"));
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private double GetBudgetAdjustments(string strAcct)
		{
			double GetBudgetAdjustments = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
				GetBudgetAdjustments = Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private double GetPendingYTDDebit(string strAcct)
		{
			double GetPendingYTDDebit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
				GetPendingYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PendingDebitsTotal"));
			}
			else
			{
				GetPendingYTDDebit = 0;
			}
			return GetPendingYTDDebit;
		}

		private double GetPendingYTDCredit(string strAcct)
		{
			double GetPendingYTDCredit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
				GetPendingYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PendingCreditsTotal")) * -1;
			}
			else
			{
				GetPendingYTDCredit = 0;
			}
			return GetPendingYTDCredit;
		}

		private double GetPendingYTDNet(string strAcct)
		{
			double GetPendingYTDNet = 0;
			GetPendingYTDNet = GetPendingYTDDebit(strAcct) - GetPendingYTDCredit(strAcct);
			return GetPendingYTDNet;
		}

		private double GetYTDDebit(string strAcct)
		{
			double GetYTDDebit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
				GetYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
			}
			else
			{
				GetYTDDebit = 0;
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit(string strAcct)
		{
			double GetYTDCredit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
				GetYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
			}
			else
			{
				GetYTDCredit = 0;
			}
			return GetYTDCredit;
		}

		private double GetYTDNet(string strAcct)
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit(strAcct) - GetYTDCredit(strAcct);
			return GetYTDNet;
		}

		private double GetEncumbrance(string strAcct)
		{
			double GetEncumbrance = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
				GetEncumbrance = Conversion.Val(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheck = "";
			string strTable;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			strTable = "ExpenseReportInfo";
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
		}
		// vbPorter upgrade warning: curAmt As Decimal	OnWrite(double, Decimal)
		// vbPorter upgrade warning: 'Return' As bool	OnWriteFCConvert.ToInt16
		private bool ValidateBalance_6(string strAcct, Decimal curAmt)
		{
			return ValidateBalance(ref strAcct, ref curAmt);
		}

		private bool ValidateBalance(ref string strAcct, ref Decimal curAmt)
		{
			bool ValidateBalance = false;
			int intLevel = 0;
			// vbPorter upgrade warning: curNetBudget As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curNetBudget;
			// vbPorter upgrade warning: curYTDPostedNet As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curYTDPostedNet;
			// vbPorter upgrade warning: curYTDPendingNet As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curYTDPendingNet;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
			{
				intLevel = 1;
			}
			else
			{
				intLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("BalanceValidation"))));
			}
			if (Strings.Left(strAcct, 1) != "E")
			{
				ValidateBalance = FCConvert.ToBoolean(1);
			}
			else
			{
				curNetBudget = FCConvert.ToDecimal(GetOriginalBudget(strAcct) - GetBudgetAdjustments(strAcct));
				curYTDPostedNet = FCConvert.ToDecimal(GetYTDNet(strAcct) + GetEncumbrance(strAcct));
				curYTDPendingNet = FCConvert.ToDecimal(GetPendingYTDNet(strAcct));
				if (curNetBudget - curYTDPostedNet - curYTDPendingNet - curAmt < 0)
				{
					if (intLevel == 1)
					{
						MessageBox.Show("Net Budget: " + Strings.Format(curNetBudget, "#,##0.00") + "\r\n" + "Posted YTD Net: " + Strings.Format(curYTDPostedNet, "#,##0.00") + "\r\n" + "Pending Net Activity: " + Strings.Format(curYTDPendingNet, "#,##0.00") + "\r\n" + "\r\n" + "You may not enter this amount because it would put this account over budget.", "Account Over Budget", MessageBoxButtons.OK, MessageBoxIcon.Information);
						ValidateBalance = FCConvert.ToBoolean(0);
					}
					else if (intLevel == 2)
					{
						ans = MessageBox.Show("Net Budget: " + Strings.Format(curNetBudget, "#,##0.00") + "\r\n" + "Posted YTD Net: " + Strings.Format(curYTDPostedNet, "#,##0.00") + "\r\n" + "Pending Net Activity: " + Strings.Format(curYTDPendingNet, "#,##0.00") + "\r\n" + "\r\n" + "Entering this amount would put this account over budget.  Do you wish to continue?", "Account Over Budget", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							ValidateBalance = FCConvert.ToBoolean(1);
						}
						else
						{
							ValidateBalance = FCConvert.ToBoolean(0);
						}
					}
					else
					{
						ValidateBalance = FCConvert.ToBoolean(1);
					}
				}
				else
				{
					ValidateBalance = FCConvert.ToBoolean(1);
				}
			}
			return ValidateBalance;
		}
	}
}
