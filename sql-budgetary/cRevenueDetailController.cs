﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;

namespace TWBD0000
{
	public class cRevenueDetailController : cReportDataExporter
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private int intFormatType;
		// 0 = CSV, 1 = Tab
		private bool boolUseDivision;

		public short FormatType
		{
			set
			{
				intFormatType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FormatType = 0;
				FormatType = FCConvert.ToInt16(intFormatType);
				return FormatType;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}
		// vbPorter upgrade warning: theReport As cDetailsReport	OnRead(cRevenueDetailReport)
		public void ExportData(ref cDetailsReport theReport, string strFileName)
		{
			ClearErrors();
			StreamWriter ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				string strPath;
				string strSeparator = "";
				string strQ = "";
				// vbPorter upgrade warning: revReport As cRevenueDetailReport	OnWrite(cDetailsReport)
				cRevenueDetailReport revReport;
				revReport = (cRevenueDetailReport)theReport;
				strPath = Path.GetDirectoryName(strFileName);
				if (!Directory.Exists(strPath))
				{
					Information.Err().Raise(9999, null, "Path not found", null, null);
				}
				if (intFormatType == 1)
				{
					strSeparator = "\t";
					strQ = "";
				}
				else
				{
					strSeparator = ",";
					strQ = FCConvert.ToString(Convert.ToChar(34));
				}
				cRevenueDetailItem revItem;
				string strLine;
				ts = new StreamWriter(strFileName);
				strLine = strQ + "Account" + strQ + strSeparator + strQ + "Department" + strQ;
				if (boolUseDivision)
				{
					strLine += strSeparator + strQ + "Division" + strQ;
				}
				strLine += strSeparator + strQ + "Revenue" + strQ;
				strLine += strSeparator + strQ + "Fund" + strQ;
				strLine += strSeparator + strQ + "Date Posted" + strQ;
				strLine += strSeparator + strQ + "Transaction Date" + strQ;
				strLine += strSeparator + strQ + "Warrant" + strQ;
				strLine += strSeparator + strQ + "Journal Type" + strQ;
				strLine += strSeparator + strQ + "Journal Number" + strQ;
				strLine += strSeparator + strQ + "Description" + strQ;
				if (revReport.ShowJournalDetail)
				{
					strLine += strSeparator + strQ + "Period" + strQ;
					strLine += strSeparator + strQ + "RCB" + strQ;
					strLine += strSeparator + strQ + "Check" + strQ;
					strLine += strSeparator + strQ + "Vendor" + strQ;
				}
				strLine += strSeparator + strQ + "Current Budget" + strQ;
				strLine += strSeparator + strQ + "Debits" + strQ;
				strLine += strSeparator + strQ + "Credits" + strQ;
				strLine += strSeparator + strQ + "Uncollected Balance" + strQ;
				ts.WriteLine(strLine);
				theReport.Details.MoveFirst();
				while (theReport.Details.IsCurrent())
				{
					strLine = "";
					revItem = (cRevenueDetailItem)theReport.Details.GetCurrentItem();
					strLine = strQ + revItem.Account + strQ;
					strLine += strSeparator + strQ + revItem.Department + strQ;
					if (boolUseDivision)
					{
						strLine += strSeparator + strQ + revItem.Division + strQ;
					}
					strLine += strSeparator + strQ + revItem.Revenue + strQ;
					strLine += strSeparator + strQ + revItem.Fund + strQ;
					strLine += strSeparator + revItem.JournalDate;
					strLine += strSeparator + revItem.TransactionDate;
					strLine += strSeparator + strQ + revItem.JournalType + strQ;
					strLine += strSeparator + FCConvert.ToString(revItem.JournalNumber);
					strLine += strSeparator + strQ + revItem.Description + strQ;
					if (revReport.ShowJournalDetail)
					{
						strLine += strSeparator + FCConvert.ToString(revItem.Period);
						strLine += strSeparator + strQ + revItem.RCB + strQ;
						strLine += strSeparator + revItem.Warrant;
						strLine += strSeparator + FCConvert.ToString(revItem.CheckNumber);
						strLine += strSeparator + strQ + revItem.Vendor + strQ;
					}
					strLine += strSeparator + FCConvert.ToString(revItem.CurrentBudget);
					strLine += strSeparator + FCConvert.ToString(revItem.Debit);
					strLine += strSeparator + FCConvert.ToString(revItem.Credit);
					strLine += strSeparator + FCConvert.ToString(revItem.UncollectedBalance);
					ts.WriteLine(strLine);
					theReport.Details.MoveNext();
				}
				ts.Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				if (!(ts == null))
				{
					ts.Close();
				}
			}
		}

		public cRevenueDetailController() : base()
		{
			boolUseDivision = !modAccountTitle.Statics.RevDivFlag;
		}
	}
}
