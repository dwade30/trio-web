﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAccountResearch.
	/// </summary>
	public partial class frmAccountResearch : BaseForm
	{
		public frmAccountResearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_3, 3);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.cmbSingleAccount.SelectedIndex = 1;
			this.cmbLedger.SelectedIndex = 0;
			this.cmbDebitsCredits.SelectedIndex = 0;
			this.cmbDetail.SelectedIndex = 1;
			this.cmbAllJournals.SelectedIndex = 0;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.vsLowAccount.ExtendLastCol = true;
			this.vsHighAccount.ExtendLastCol = true;
            this.vsSingleAccount.ExtendLastCol = true;
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAccountResearch InstancePtr
		{
			get
			{
				return (frmAccountResearch)Sys.GetInstance(typeof(frmAccountResearch));
			}
		}

		protected frmAccountResearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         06/14/2002
		// This form will be used to set up what the user wants to
		// view in the Current Account Status report
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//public clsDRWrapper rsAccountInfo = new clsDRWrapper();
		public clsDRWrapper rsAccountInfo_AutoInitialized;

		public clsDRWrapper rsAccountInfo
		{
			get
			{
				if (rsAccountInfo_AutoInitialized == null)
				{
					rsAccountInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsAccountInfo_AutoInitialized;
			}
			set
			{
				rsAccountInfo_AutoInitialized = value;
			}
		}

		public bool blnHaveData;
		string strDefaultType;
		public string strSql = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		private clsGridAccount vsLowGrid_AutoInitialized;

		private clsGridAccount vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		private clsGridAccount vsHighGrid_AutoInitialized;

		private clsGridAccount vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsSingleGrid = new clsGridAccount();
		private clsGridAccount vsSingleGrid_AutoInitialized;

		private clsGridAccount vsSingleGrid
		{
			get
			{
				if (vsSingleGrid_AutoInitialized == null)
				{
					vsSingleGrid_AutoInitialized = new clsGridAccount();
				}
				return vsSingleGrid_AutoInitialized;
			}
			set
			{
				vsSingleGrid_AutoInitialized = value;
			}
		}

		private void chkDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDateRange.CheckState == CheckState.Checked)
			{
				fraDateRange.Enabled = true;
				txtLowDate.Focus();
			}
			else
			{
				fraDateRange.Enabled = false;
				txtLowDate.Text = "";
				txtHighDate.Text = "";
			}
		}

		private void chkMonthRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkMonthRange.CheckState == CheckState.Checked)
			{
				fraMonthRange.Enabled = true;
				cboLowMonth.SelectedIndex = 0;
				cboHighMonth.SelectedIndex = 0;
				cboLowMonth.Focus();
			}
			else
			{
				fraMonthRange.Enabled = false;
				cboLowMonth.SelectedIndex = -1;
				cboHighMonth.SelectedIndex = -1;
			}
		}

		private void frmAccountResearch_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vsSingleAccount.Focus();
			this.Refresh();
		}

		private void frmAccountResearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowAccount, vsLowAccount.Row, vsLowAccount.Col, KeyCode, Shift, vsLowAccount.EditSelStart, vsLowAccount.EditText, vsLowAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsHighAccount, vsHighAccount.Row, vsHighAccount.Col, KeyCode, Shift, vsHighAccount.EditSelStart, vsHighAccount.EditText, vsHighAccount.EditSelLength);
				}
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSSINGLEACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsSingleAccount, vsSingleAccount.Row, vsSingleAccount.Col, KeyCode, Shift, vsSingleAccount.EditSelStart, vsSingleAccount.EditText, vsSingleAccount.EditSelLength);
				}
			}
		}

		private void frmAccountResearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAccountResearch.FillStyle	= 0;
			//frmAccountResearch.ScaleWidth	= 9300;
			//frmAccountResearch.ScaleHeight	= 7500;
			//frmAccountResearch.LinkTopic	= "Form2";
			//frmAccountResearch.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rs = new clsDRWrapper();
			int counter = 0;
			vsLowGrid.GRID7Light = vsLowAccount;
			vsHighGrid.GRID7Light = vsHighAccount;
			vsSingleGrid.GRID7Light = vsSingleAccount;
		
			vsLowGrid.DefaultAccountType = "E";
			vsHighGrid.DefaultAccountType = "E";
			vsSingleGrid.DefaultAccountType = "E";
			strDefaultType = "E";
		
			vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsLowGrid.OnlyAllowDefaultType = true;
			vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsHighGrid.OnlyAllowDefaultType = true;
			vsSingleGrid.AccountCol = -1;
			vsSingleGrid.OnlyAllowDefaultType = true;
			//cboBeginningDept.Left = FCConvert.ToInt32(cboBeginningDept.Left + cboBeginningDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
			//cboSingleDept.Left = FCConvert.ToInt32(cboSingleDept.Left + (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
			//cboBeginningDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboEndingDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboSingleDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					//cboBeginningDept.Items[counter] = rs.Get_Fields("Department") + " - " + rs.Get_Fields("ShortDescription");
					//cboEndingDept.Items[counter] = rs.Get_Fields("Department") + " - " + rs.Get_Fields("ShortDescription");
					//cboSingleDept.Items[counter] = rs.Get_Fields("Department") + " - " + rs.Get_Fields("ShortDescription");
					cboBeginningDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			//cboBeginningFund.Left = cboBeginningFund.Left + cboBeginningFund.Width - (3 * 115 + 350);
			//cboSingleFund.Left = FCConvert.ToInt32(cboSingleFund.Left + (cboSingleFund.Width - (3 * 115 + 350)) - (0.5 * (cboSingleFund.Width - (3 * 115 + 350))));
			//cboBeginningFund.Width = 3 * 115 + 350;
			//cboEndingFund.Width = 3 * 115 + 350;
			//cboSingleFund.Width = 3 * 115 + 350;
			FillTownFunds();
			rs.OpenRecordset("SELECT DISTINCT JournalNumber FROM JournalMaster ORDER BY JournalNumber");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboStartJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("JournalNumber")), 4));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboEndJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("JournalNumber")), 4));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			blnHaveData = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmAccountResearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbLedger.SelectedIndex == 0)
			{
				cmbLedger.Focus();
			}
			else if (cmbLedger.SelectedIndex == 2)
			{
				cmbLedger.Focus();
			}
			else
			{
				cmbLedger.Focus();
			}
			if (TestValidInfo())
			{
                //FC:FINAL:AM:#3128 - don't close the form because the values will be lost
                //this.Close();
                frmReportViewer.InstancePtr.Init(rptAccountResearch.InstancePtr);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbLedger.SelectedIndex == 0)
			{
				cmbLedger.Focus();
			}
			else if (cmbLedger.SelectedIndex == 2)
			{
				cmbLedger.Focus();
			}
			else
			{
				cmbLedger.Focus();
			}
			if (TestValidInfo())
			{
				modDuplexPrinting.DuplexPrintReport(rptAccountResearch.InstancePtr);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAllJournals_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAllJournals.SelectedIndex == 0)
			{
				fraJournalRange.Enabled = false;
				cboStartJournal.SelectedIndex = -1;
				cboEndJournal.SelectedIndex = -1;
			}
			else if (cmbAllJournals.SelectedIndex == 1)
			{
				fraJournalRange.Enabled = true;
				cboStartJournal.SelectedIndex = 0;
				cboEndJournal.SelectedIndex = 0;
				cboStartJournal.Focus();
			}
		}

		private void optExpense_CheckedChanged(object sender, System.EventArgs e)
		{
			//optSingleDepartment.Text = "Single Department";
			if (!cmbSingleAccount.Items.Contains("Single Department"))
			{
				cmbSingleAccount.Items.Insert(3, "Single Department");
			}
			int index = cmbSingleAccount.Items.IndexOf("Single Fund");
			if (index != -1)
			{
				cmbSingleAccount.Items.RemoveAt(index);
			}
			//optDepartmentRange.Text = "Department Range";
			if (!cmbSingleAccount.Items.Contains("Department Range"))
			{
				cmbSingleAccount.Items.Insert(4, "Department Range");
			}
			index = cmbSingleAccount.Items.IndexOf("Fund Range");
			if (index != -1)
			{
				cmbSingleAccount.Items.RemoveAt(index);
			}
			cmbSingleAccount.SelectedIndex = 1;
			strDefaultType = "E";
			vsLowGrid.DefaultAccountType = "E";
			vsHighGrid.DefaultAccountType = "E";
			vsSingleGrid.DefaultAccountType = "E";
			vsLowAccount.TextMatrix(0, 0, "");
			vsHighAccount.TextMatrix(0, 0, "");
			vsSingleAccount.TextMatrix(0, 0, "");
			vsSingleAccount.Focus();
			Support.SendKeys("{E}", false);
			blnHaveData = false;
		}

		private void optLedger_CheckedChanged(object sender, System.EventArgs e)
		{
			//optSingleDepartment.Text = "Single Fund";
			if (!cmbSingleAccount.Items.Contains("Single Fund"))
			{
				cmbSingleAccount.Items.Insert(3, "Single Fund");
			}
			int index = cmbSingleAccount.Items.IndexOf("Single Department");
			if (index != -1)
			{
				cmbSingleAccount.Items.RemoveAt(index);
			}
			//optDepartmentRange.Text = "Fund Range";
			if (!cmbSingleAccount.Items.Contains("Fund Range"))
			{
				cmbSingleAccount.Items.Insert(4, "Fund Range");
			}
			index = cmbSingleAccount.Items.IndexOf("Department Range");
			if (index != -1)
			{
				cmbSingleAccount.Items.RemoveAt(index);
			}
			cmbSingleAccount.SelectedIndex = 1;
			strDefaultType = "G";
			vsLowGrid.DefaultAccountType = "G";
			vsHighGrid.DefaultAccountType = "G";
			vsSingleGrid.DefaultAccountType = "G";
			vsLowAccount.TextMatrix(0, 0, "");
			vsHighAccount.TextMatrix(0, 0, "");
			vsSingleAccount.TextMatrix(0, 0, "");
			vsSingleAccount.Focus();
			Support.SendKeys("{G}", false);
			blnHaveData = false;
		}

		private void optRevenue_CheckedChanged(object sender, System.EventArgs e)
		{
			//optSingleDepartment.Text = "Single Department";
			if (!cmbSingleAccount.Items.Contains("Single Department"))
			{
				cmbSingleAccount.Items.Insert(3, "Single Department");
			}
			int index = cmbSingleAccount.Items.IndexOf("Single Fund");
			if (index != -1)
			{
				cmbSingleAccount.Items.RemoveAt(index);
			}
			//optDepartmentRange.Text = "Department Range";
			if (!cmbSingleAccount.Items.Contains("Department Range"))
			{
				cmbSingleAccount.Items.Insert(4, "Department Range");
			}
			index = cmbSingleAccount.Items.IndexOf("Fund Range");
			if (index != -1)
			{
				cmbSingleAccount.Items.RemoveAt(index);
			}
			cmbSingleAccount.SelectedIndex = 1;
			strDefaultType = "R";
			vsLowGrid.DefaultAccountType = "R";
			vsHighGrid.DefaultAccountType = "R";
			vsSingleGrid.DefaultAccountType = "R";
			vsLowAccount.TextMatrix(0, 0, "");
			vsHighAccount.TextMatrix(0, 0, "");
			vsSingleAccount.TextMatrix(0, 0, "");
			vsSingleAccount.Focus();
			Support.SendKeys("{R}", false);
			blnHaveData = false;
		}

		private void optSingleAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbSingleAccount.SelectedIndex == 1)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsSingleAccount.Visible = true;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				vsSingleAccount.Focus();
			}
			else if (cmbSingleAccount.SelectedIndex == 0)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
			}
			else if (cmbSingleAccount.SelectedIndex == 2)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = true;
				vsHighAccount.Visible = true;
				lblTo[1].Visible = true;
				vsLowAccount.Focus();
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				if (cmbLedger.SelectedIndex == 2)
				{
					FillTownFunds();
					cboSingleFund.Visible = true;
					cboSingleFund.Focus();
				}
				else
				{
					cboSingleDept.Visible = true;
					cboSingleDept.Focus();
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 4)
			{
				lblTo[2].Visible = true;
				cboSingleDept.Visible = false;
				cboSingleFund.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				if (cmbLedger.SelectedIndex == 2)
				{
					FillTownFunds();
					cboBeginningFund.Visible = true;
					cboEndingFund.Visible = true;
					cboBeginningFund.Focus();
				}
				else
				{
					cboBeginningDept.Visible = true;
					cboEndingDept.Visible = true;
					cboBeginningDept.Focus();
				}
			}
		}

		private void SetCustomFormColors()
		{
			//lblTitle.ForeColor = Color.Blue;
		}

		private void cboBeginningDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboBeginningFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void optTown_Click()
		{
			if (cmbLedger.SelectedIndex == 0)
			{
				vsLowGrid.DefaultAccountType = "E";
				vsHighGrid.DefaultAccountType = "E";
				vsSingleGrid.DefaultAccountType = "E";
				strDefaultType = "E";
				optExpense_CheckedChanged(cmbLedger, EventArgs.Empty);
			}
			else if (cmbLedger.SelectedIndex == 1)
			{
				vsLowGrid.DefaultAccountType = "R";
				vsHighGrid.DefaultAccountType = "R";
				vsSingleGrid.DefaultAccountType = "R";
				strDefaultType = "R";
				optRevenue_CheckedChanged(cmbLedger, EventArgs.Empty);
			}
			else
			{
				vsLowGrid.DefaultAccountType = "G";
				vsHighGrid.DefaultAccountType = "G";
				vsSingleGrid.DefaultAccountType = "G";
				strDefaultType = "G";
				optLedger_CheckedChanged(cmbLedger, EventArgs.Empty);
			}
		}

		private void vsHighAccount_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsHighAccount.IsCurrentCellInEditMode)
			{
				modAccountTitle.DetermineAccountTitle(vsHighAccount.TextMatrix(0, 0), ref lblTitle);
				HeaderText.Text = (lblTitle.Text != string.Empty) ? lblTitle : HeaderText.Text;
			}
		}

		private void vsHighAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			if (vsHighAccount.EditSelStart == 0)
			{
				if (cmbLedger.SelectedIndex == 0)
				{
					if (keyAscii == 71 || keyAscii == 103 || keyAscii == 82 || keyAscii == 114 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
					{
						keyAscii = 0;
					}
				}
				else if (cmbLedger.SelectedIndex == 1)
				{
					if (keyAscii == 71 || keyAscii == 103 || keyAscii == 69 || keyAscii == 101 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
					{
						keyAscii = 0;
					}
				}
				else
				{
					if (keyAscii == 82 || keyAscii == 114 || keyAscii == 69 || keyAscii == 101 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
					{
						keyAscii = 0;
					}
				}
			}
		}

		private void vsLowAccount_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsLowAccount.IsCurrentCellInEditMode)
			{
				modAccountTitle.DetermineAccountTitle(vsLowAccount.TextMatrix(0, 0), ref lblTitle);
				HeaderText.Text = (lblTitle.Text != string.Empty) ? lblTitle : HeaderText.Text;
			}
		}

		private void vsLowAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			if (vsLowAccount.EditSelStart == 0)
			{
				if (keyAscii == 71 || keyAscii == 103)
				{
					cmbLedger.SelectedIndex = 2;
				}
				else if (keyAscii == 69 || keyAscii == 101)
				{
					cmbLedger.SelectedIndex = 0;
				}
				else if (keyAscii == 82 || keyAscii == 114)
				{
					cmbLedger.SelectedIndex = 1;
				}
				else if (keyAscii == 76 || keyAscii == 108)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 80 || keyAscii == 112)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 86 || keyAscii == 118)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109)
				{
					keyAscii = 0;
				}
			}
		}

		private void vsSingleAccount_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsSingleAccount.IsCurrentCellInEditMode)
			{
				modAccountTitle.DetermineAccountTitle(vsSingleAccount.EditText, ref lblTitle);
				HeaderText.Text = (lblTitle.Text != string.Empty) ? lblTitle : HeaderText.Text;
			}
		}

		private void vsSingleAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			if (vsSingleAccount.EditSelStart == 0)
			{
				if (keyAscii == 71 || keyAscii == 103)
				{
					cmbLedger.SelectedIndex = 2;
				}
				else if (keyAscii == 69 || keyAscii == 101)
				{
					cmbLedger.SelectedIndex = 0;
				}
				else if (keyAscii == 82 || keyAscii == 114)
				{
					cmbLedger.SelectedIndex = 1;
				}
				else if (keyAscii == 76 || keyAscii == 108)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 80 || keyAscii == 112)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 86 || keyAscii == 118)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109)
				{
					keyAscii = 0;
				}
			}
		}

		private void CreateSQL()
		{
			strSql = "";
			if (cmbSingleAccount.SelectedIndex == 0)
			{
				// do nothing
			}
			else if (cmbSingleAccount.SelectedIndex == 2)
			{
				strSql += "(Account >= '" + vsLowAccount.TextMatrix(0, 0) + "' AND Account <= '" + vsHighAccount.TextMatrix(0, 0) + "') AND ";
			}
			else if (cmbSingleAccount.SelectedIndex == 1)
			{
				strSql += "(Account = '" + vsSingleAccount.TextMatrix(0, 0) + "') AND ";
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					strSql += "(Fund = '" + Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "') AND ";
				}
				else
				{
					strSql += "(Department = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "') AND ";
				}
			}
			else
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					strSql += "(Fund >= '" + Strings.Left(cboBeginningFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' AND Fund <= '" + Strings.Left(cboEndingFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "') AND ";
				}
				else
				{
					strSql += "(Department >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND Department <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "') AND ";
				}
			}
			if (chkDateRange.CheckState == CheckState.Checked)
			{
				strSql += "(TransDate >= '" + txtLowDate.Text + "' AND TransDate <= '" + txtHighDate.Text + "') AND ";
			}
			if (chkMonthRange.CheckState == CheckState.Checked)
			{
				strSql += "(Period >= " + FCConvert.ToString(cboLowMonth.SelectedIndex + 1) + " AND Period <= " + FCConvert.ToString(cboHighMonth.SelectedIndex + 1) + ") AND ";
			}
			if (cmbAllJournals.SelectedIndex == 1)
			{
				strSql += "(JournalNumber >= " + FCConvert.ToString(Conversion.Val(Strings.Left(cboStartJournal.Text, 4))) + " And JournalNumber <= " + FCConvert.ToString(Conversion.Val(Strings.Left(cboEndJournal.Text, 4))) + ") And ";
			}
			strSql += "(";
			if (chkAP.CheckState == CheckState.Checked)
			{
				strSql += "Type = 'A' OR ";
			}
			if (chkEN.CheckState == CheckState.Checked)
			{
				strSql += "Type = 'E' OR ";
			}
			if (chkCR.CheckState == CheckState.Checked)
			{
				strSql += "Type = 'W' OR Type = 'C' OR ";
			}
			if (chkCD.CheckState == CheckState.Checked)
			{
				strSql += "Type = 'D' OR ";
			}
			if (chkGJ.CheckState == CheckState.Checked)
			{
				strSql += "Type = 'G' OR ";
			}
			if (chkPY.CheckState == CheckState.Checked)
			{
				strSql += "Type = 'P' OR ";
			}
			strSql = Strings.Left(strSql, strSql.Length - 4) + ")";
		}

		private bool TestValidInfo()
		{
			bool TestValidInfo = false;
			TestValidInfo = false;
			if (chkAP.CheckState == CheckState.Unchecked && chkEN.CheckState == CheckState.Unchecked && chkCR.CheckState == CheckState.Unchecked && chkCD.CheckState == CheckState.Unchecked && chkGJ.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked)
			{
				MessageBox.Show("You must select at least one type of journal before you may continue", "Invalid Journal Types", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return TestValidInfo;
			}
			if (chkDateRange.CheckState == CheckState.Checked)
			{
				if (!Information.IsDate(txtLowDate.Text) || !Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("There is an invalid date in your date range.  Please enter a valid date range then try again.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
				if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("Your low date must be earlier than your high date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(FCConvert.ToDateTime(txtLowDate.Text).Month), FCConvert.ToInt16(FCConvert.ToDateTime(txtHighDate.Text).Month)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
			}
			if (chkMonthRange.Checked == true)
			{
				if (cboLowMonth.SelectedIndex == -1 || cboHighMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of accounting months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboLowMonth.SelectedIndex + 1), FCConvert.ToInt16(cboHighMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
			}
			if (cmbSingleAccount.SelectedIndex == 2)
			{
				if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.TextMatrix(0, 0) == "" || Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.TextMatrix(0, 0) == "")
				{
					MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
				if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) > 0)
				{
					MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return TestValidInfo;
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 1)
			{
				if (Strings.InStr(1, vsSingleAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("You must enter a valid account before you may continue", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsSingleAccount.Focus();
					return TestValidInfo;
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					if (cboSingleFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Fund you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return TestValidInfo;
					}
				}
				else
				{
					if (cboSingleDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Department you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return TestValidInfo;
					}
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 4)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					if (cboBeginningFund.SelectedIndex == -1 || cboEndingFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Funds you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return TestValidInfo;
					}
					if (cboBeginningFund.SelectedIndex > cboEndingFund.SelectedIndex)
					{
						MessageBox.Show("Your beginning Fund must be lower then your ending Fund", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return TestValidInfo;
					}
				}
				else
				{
					if (cboBeginningDept.SelectedIndex == -1 || cboEndingDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Departments you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return TestValidInfo;
					}
					if (cboBeginningDept.SelectedIndex > cboEndingDept.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return TestValidInfo;
					}
				}
			}
			if (cmbAllJournals.SelectedIndex == 1)
			{
				if (Conversion.Val(Strings.Left(cboStartJournal.Text, 4)) > Conversion.Val(Strings.Left(cboEndJournal.Text, 4)))
				{
					MessageBox.Show("Your beginning journal must be lower than your ending journal.", "Invalid Journal Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return TestValidInfo;
				}
			}
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();
			// If optExpense Then
			// CalculateAccountInfo False, True, True, "E"
			// ElseIf optRevenue Then
			// CalculateAccountInfo False, True, True, "R"
			// Else
			// CalculateAccountInfo False, True, True, "G"
			// End If
			CreateSQL();
			if (cmbLedger.SelectedIndex == 0)
			{
				if (Strings.Trim(strSql) == "")
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM ExpenseDetailInfo ORDER BY Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM ExpenseDetailInfo WHERE " + strSql + " ORDER BY Account");
				}
			}
			else if (cmbLedger.SelectedIndex == 1)
			{
				if (Strings.Trim(strSql) == "")
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM RevenueDetailInfo ORDER BY Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM RevenueDetailInfo WHERE " + strSql + " ORDER BY Account");
				}
			}
			else
			{
				if (Strings.Trim(strSql) == "")
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM LedgerDetailInfo ORDER BY Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM LedgerDetailInfo WHERE " + strSql + " ORDER BY Account");
				}
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				TestValidInfo = true;
			}
			else
			{
				MessageBox.Show("No accounts found that match the criteria you specified", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return TestValidInfo;
			}
			return TestValidInfo;
		}

		private void FillTownFunds()
		{
			clsDRWrapper rs = new clsDRWrapper();
			int counter = 0;
			cboBeginningFund.Clear();
			cboEndingFund.Clear();
			cboSingleFund.Clear();
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					//cboBeginningFund.Items[counter] = rs.Get_Fields("Fund") + " - " + rs.Get_Fields("ShortDescription");
					//cboEndingFund.Items[counter] = rs.Get_Fields("Fund") + " - " + rs.Get_Fields("ShortDescription");
					//cboSingleFund.Items[counter] = rs.Get_Fields("Fund") + " - " + rs.Get_Fields("ShortDescription");
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboBeginningFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboEndingFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboSingleFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
		}

		private void cmbLedger_SelectedIndexChanged(object sender, EventArgs e)
		{
			optTown_Click();
		}
	}
}
