//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGJCorrDataEntry.
	/// </summary>
	partial class frmGJCorrDataEntry : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtCheck;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KOverTypeBox txtReference;
		public Global.T2KBackFillDecimal txtAmount;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public fecherFoundation.FCFrame fraBorder;
		public fecherFoundation.FCComboBox cboPeriod;
		public fecherFoundation.FCComboBox cboJournal;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblReference;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblCheck;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGJCorrDataEntry));
			this.fraJournalSave = new fecherFoundation.FCFrame();
			this.txtJournalDescription = new fecherFoundation.FCTextBox();
			this.cmdCancelSave = new fecherFoundation.FCButton();
			this.cmdOKSave = new fecherFoundation.FCButton();
			this.cboSaveJournal = new fecherFoundation.FCComboBox();
			this.lblJournalDescription = new fecherFoundation.FCLabel();
			this.lblJournalSave = new fecherFoundation.FCLabel();
			this.lblSaveInstructions = new fecherFoundation.FCLabel();
			this.txtAddress_0 = new Global.T2KOverTypeBox();
			this.txtVendor = new Global.T2KOverTypeBox();
			this.txtCheck = new Global.T2KOverTypeBox();
			this.txtDescription = new Global.T2KOverTypeBox();
			this.txtReference = new Global.T2KOverTypeBox();
			this.txtAmount = new Global.T2KBackFillDecimal();
			this.txtAddress_1 = new Global.T2KOverTypeBox();
			this.txtAddress_2 = new Global.T2KOverTypeBox();
			this.txtAddress_3 = new Global.T2KOverTypeBox();
			this.fraBorder = new fecherFoundation.FCFrame();
			this.cboPeriod = new fecherFoundation.FCComboBox();
			this.cboJournal = new fecherFoundation.FCComboBox();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblVendor = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblReference = new fecherFoundation.FCLabel();
			this.lblAmount = new fecherFoundation.FCLabel();
			this.lblCheck = new fecherFoundation.FCLabel();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
			this.fraJournalSave.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBorder)).BeginInit();
			this.fraBorder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtAddress_0);
			this.ClientArea.Controls.Add(this.txtVendor);
			this.ClientArea.Controls.Add(this.txtCheck);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtReference);
			this.ClientArea.Controls.Add(this.txtAmount);
			this.ClientArea.Controls.Add(this.txtAddress_1);
			this.ClientArea.Controls.Add(this.txtAddress_2);
			this.ClientArea.Controls.Add(this.txtAddress_3);
			this.ClientArea.Controls.Add(this.fraBorder);
			this.ClientArea.Controls.Add(this.txtCity);
			this.ClientArea.Controls.Add(this.txtState);
			this.ClientArea.Controls.Add(this.txtZip);
			this.ClientArea.Controls.Add(this.txtZip4);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Controls.Add(this.lblVendor);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Controls.Add(this.lblReference);
			this.ClientArea.Controls.Add(this.lblAmount);
			this.ClientArea.Controls.Add(this.lblCheck);
			this.ClientArea.Controls.Add(this.fraJournalSave);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(172, 30);
			this.HeaderText.Text = "AP Correction";
			// 
			// fraJournalSave
			// 
			this.fraJournalSave.BackColor = System.Drawing.Color.White;
			this.fraJournalSave.Controls.Add(this.txtJournalDescription);
			this.fraJournalSave.Controls.Add(this.cmdCancelSave);
			this.fraJournalSave.Controls.Add(this.cmdOKSave);
			this.fraJournalSave.Controls.Add(this.cboSaveJournal);
			this.fraJournalSave.Controls.Add(this.lblJournalDescription);
			this.fraJournalSave.Controls.Add(this.lblJournalSave);
			this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
			this.fraJournalSave.Location = new System.Drawing.Point(30, 30);
			this.fraJournalSave.Name = "fraJournalSave";
			this.fraJournalSave.Size = new System.Drawing.Size(1018, 490);
			this.fraJournalSave.TabIndex = 0;
			this.fraJournalSave.Text = "Save Journal";
			this.fraJournalSave.Visible = false;
			// 
			// txtJournalDescription
			// 
			this.txtJournalDescription.MaxLength = 100;
			this.txtJournalDescription.AutoSize = false;
			this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournalDescription.Location = new System.Drawing.Point(168, 116);
			this.txtJournalDescription.Name = "txtJournalDescription";
			this.txtJournalDescription.Size = new System.Drawing.Size(331, 40);
			this.txtJournalDescription.TabIndex = 2;
			// 
			// cmdCancelSave
			// 
			this.cmdCancelSave.AppearanceKey = "actionButton";
			this.cmdCancelSave.Location = new System.Drawing.Point(502, 166);
			this.cmdCancelSave.Name = "cmdCancelSave";
			this.cmdCancelSave.Size = new System.Drawing.Size(100, 40);
			this.cmdCancelSave.TabIndex = 6;
			this.cmdCancelSave.Text = "Cancel";
			this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
			// 
			// cmdOKSave
			// 
			this.cmdOKSave.AppearanceKey = "actionButton";
			this.cmdOKSave.Location = new System.Drawing.Point(396, 166);
			this.cmdOKSave.Name = "cmdOKSave";
			this.cmdOKSave.Size = new System.Drawing.Size(100, 40);
			this.cmdOKSave.TabIndex = 4;
			this.cmdOKSave.Text = "OK";
			this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
			// 
			// cboSaveJournal
			// 
			this.cboSaveJournal.AutoSize = false;
			this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboSaveJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSaveJournal.FormattingEnabled = true;
			this.cboSaveJournal.Location = new System.Drawing.Point(168, 66);
			this.cboSaveJournal.Name = "cboSaveJournal";
			this.cboSaveJournal.Size = new System.Drawing.Size(331, 40);
			this.cboSaveJournal.TabIndex = 1;
			this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
			this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
			// 
			// lblJournalDescription
			// 
			this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalDescription.Location = new System.Drawing.Point(20, 130);
			this.lblJournalDescription.Name = "lblJournalDescription";
			this.lblJournalDescription.Size = new System.Drawing.Size(78, 16);
			this.lblJournalDescription.TabIndex = 7;
			this.lblJournalDescription.Text = "DESCRIPTION";
			// 
			// lblJournalSave
			// 
			this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalSave.Location = new System.Drawing.Point(20, 80);
			this.lblJournalSave.Name = "lblJournalSave";
			this.lblJournalSave.Size = new System.Drawing.Size(63, 16);
			this.lblJournalSave.TabIndex = 5;
			this.lblJournalSave.Text = "JOURNAL";
			// 
			// lblSaveInstructions
			// 
			this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
			this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblSaveInstructions.Name = "lblSaveInstructions";
			this.lblSaveInstructions.Size = new System.Drawing.Size(978, 16);
			this.lblSaveInstructions.TabIndex = 3;
			this.lblSaveInstructions.Text = "PLEASE SELECT THE JOURNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION F" + "OR THE JOURNAL, AND CLICK THE OK BUTTON";
			// 
			// txtAddress_0
			// 
			this.txtAddress_0.MaxLength = 35;
			this.txtAddress_0.AutoSize = false;
			this.txtAddress_0.Enabled = false;
			this.txtAddress_0.Location = new System.Drawing.Point(649, 30);
			this.txtAddress_0.Name = "txtAddress_0";
			this.txtAddress_0.Size = new System.Drawing.Size(360, 40);
			this.txtAddress_0.TabIndex = 12;
			// 
			// txtVendor
			// 
			this.txtVendor.MaxLength = 5;
			this.txtVendor.AutoSize = false;
			this.txtVendor.Enabled = false;
			this.txtVendor.Location = new System.Drawing.Point(558, 30);
			this.txtVendor.Name = "txtVendor";
			this.txtVendor.Size = new System.Drawing.Size(88, 40);
			this.txtVendor.TabIndex = 13;
			// 
			// txtCheck
			// 
			this.txtCheck.MaxLength = 6;
			this.txtCheck.AutoSize = false;
			this.txtCheck.Enabled = false;
			this.txtCheck.Location = new System.Drawing.Point(142, 320);
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Size = new System.Drawing.Size(300, 40);
			this.txtCheck.TabIndex = 14;
			// 
			// txtDescription
			// 
			this.txtDescription.MaxLength = 25;
			this.txtDescription.AutoSize = false;
			this.txtDescription.Enabled = false;
			this.txtDescription.Location = new System.Drawing.Point(142, 170);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(300, 40);
			this.txtDescription.TabIndex = 15;
			// 
			// txtReference
			// 
			this.txtReference.MaxLength = 15;
			this.txtReference.AutoSize = false;
			this.txtReference.Enabled = false;
			this.txtReference.Location = new System.Drawing.Point(142, 220);
			this.txtReference.Name = "txtReference";
			this.txtReference.Size = new System.Drawing.Size(300, 40);
			this.txtReference.TabIndex = 16;
			// 
			// txtAmount
			// 
			this.txtAmount.MaxLength = 14;
			this.txtAmount.Enabled = false;
			this.txtAmount.Location = new System.Drawing.Point(142, 270);
			this.txtAmount.MaxLength = 14;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(300, 40);
			this.txtAmount.TabIndex = 18;
			// 
			// txtAddress_1
			// 
			this.txtAddress_1.MaxLength = 35;
			this.txtAddress_1.AutoSize = false;
			this.txtAddress_1.Enabled = false;
			this.txtAddress_1.Location = new System.Drawing.Point(649, 80);
			this.txtAddress_1.Name = "txtAddress_1";
			this.txtAddress_1.Size = new System.Drawing.Size(360, 40);
			this.txtAddress_1.TabIndex = 19;
			// 
			// txtAddress_2
			// 
			this.txtAddress_2.MaxLength = 35;
			this.txtAddress_2.AutoSize = false;
			this.txtAddress_2.Enabled = false;
			this.txtAddress_2.Location = new System.Drawing.Point(649, 130);
			this.txtAddress_2.Name = "txtAddress_2";
			this.txtAddress_2.Size = new System.Drawing.Size(360, 40);
			this.txtAddress_2.TabIndex = 20;
			// 
			// txtAddress_3
			// 
			this.txtAddress_3.MaxLength = 35;
			this.txtAddress_3.AutoSize = false;
			this.txtAddress_3.Enabled = false;
			this.txtAddress_3.Location = new System.Drawing.Point(649, 180);
			this.txtAddress_3.Name = "txtAddress_3";
			this.txtAddress_3.Size = new System.Drawing.Size(360, 40);
			this.txtAddress_3.TabIndex = 21;
			// 
			// fraBorder
			// 
			this.fraBorder.Controls.Add(this.cboPeriod);
			this.fraBorder.Controls.Add(this.cboJournal);
			this.fraBorder.Controls.Add(this.lblJournal);
			this.fraBorder.Controls.Add(this.lblPeriod);
			this.fraBorder.Location = new System.Drawing.Point(30, 30);
			this.fraBorder.Name = "fraBorder";
			this.fraBorder.Size = new System.Drawing.Size(412, 130);
			this.fraBorder.TabIndex = 8;
			// 
			// cboPeriod
			// 
			this.cboPeriod.AutoSize = false;
			this.cboPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.cboPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPeriod.Enabled = false;
			this.cboPeriod.FormattingEnabled = true;
			this.cboPeriod.Items.AddRange(new object[] {
				"01",
				"02",
				"03",
				"04",
				"05",
				"06",
				"07",
				"08",
				"09",
				"10",
				"11",
				"12"
			});
			this.cboPeriod.Location = new System.Drawing.Point(150, 70);
			this.cboPeriod.Name = "cboPeriod";
			this.cboPeriod.Size = new System.Drawing.Size(242, 40);
			this.cboPeriod.TabIndex = 32;
			this.cboPeriod.Enter += new System.EventHandler(this.cboPeriod_Enter);
			// 
			// cboJournal
			// 
			this.cboJournal.AutoSize = false;
			this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournal.FormattingEnabled = true;
			this.cboJournal.Location = new System.Drawing.Point(150, 20);
			this.cboJournal.Name = "cboJournal";
			this.cboJournal.Size = new System.Drawing.Size(242, 40);
			this.cboJournal.TabIndex = 9;
			this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
			this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
			this.cboJournal.Enter += new System.EventHandler(this.cboJournal_Enter);
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(20, 34);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(63, 16);
			this.lblJournal.TabIndex = 11;
			this.lblJournal.Text = "JOURNAL";
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(20, 84);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(95, 16);
			this.lblPeriod.TabIndex = 10;
			this.lblPeriod.Text = "ACCTG PERIOD";
			// 
			// txtCity
			// 
			this.txtCity.MaxLength = 35;
			this.txtCity.Appearance = 1;
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.Color.FromName("@window");
			//this.txtCity.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtCity.Enabled = false;
			this.txtCity.Location = new System.Drawing.Point(649, 230);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(150, 40);
			this.txtCity.TabIndex = 28;
			// 
			// txtState
			// 
			this.txtState.MaxLength = 2;
			this.txtState.Appearance = 1;
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.Color.FromName("@window");
			//this.txtState.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtState.Enabled = false;
			this.txtState.Location = new System.Drawing.Point(802, 230);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(55, 40);
			this.txtState.TabIndex = 29;
			// 
			// txtZip
			// 
			this.txtZip.MaxLength = 5;
			this.txtZip.Appearance = 1;
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.Color.FromName("@window");
			//this.txtZip.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtZip.Enabled = false;
			this.txtZip.Location = new System.Drawing.Point(863, 230);
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(73, 40);
			this.txtZip.TabIndex = 30;
			// 
			// txtZip4
			// 
			this.txtZip4.MaxLength = 4;
			this.txtZip4.Appearance = 1;
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.Color.FromName("@window");
			//this.txtZip4.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtZip4.Enabled = false;
			this.txtZip4.Location = new System.Drawing.Point(939, 230);
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(73, 40);
			this.txtZip4.TabIndex = 31;
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 396);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 16;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1018, 96);
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 17;
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			// 
			// lblExpense
			// 
			this.lblExpense.BackColor = System.Drawing.Color.FromName("@window");
			this.lblExpense.Location = new System.Drawing.Point(30, 370);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(655, 16);
			this.lblExpense.TabIndex = 27;
			// 
			// lblVendor
			// 
			this.lblVendor.Location = new System.Drawing.Point(473, 44);
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Size = new System.Drawing.Size(50, 16);
			this.lblVendor.TabIndex = 26;
			this.lblVendor.Text = "VENDOR";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 184);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(77, 16);
			this.lblDescription.TabIndex = 25;
			this.lblDescription.Text = "DESCRIPTION";
			// 
			// lblReference
			// 
			this.lblReference.Location = new System.Drawing.Point(30, 234);
			this.lblReference.Name = "lblReference";
			this.lblReference.Size = new System.Drawing.Size(87, 16);
			this.lblReference.TabIndex = 24;
			this.lblReference.Text = "REFERENCE";
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(30, 284);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(75, 16);
			this.lblAmount.TabIndex = 23;
			this.lblAmount.Text = "AMOUNT";
			// 
			// lblCheck
			// 
			this.lblCheck.Location = new System.Drawing.Point(30, 334);
			this.lblCheck.Name = "lblCheck";
			this.lblCheck.Size = new System.Drawing.Size(58, 16);
			this.lblCheck.TabIndex = 22;
			this.lblCheck.Text = "CHECK#";
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(262, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(103, 48);
			this.cmdProcessSave.TabIndex = 3;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmGJCorrDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGJCorrDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "AP Correction";
			this.Load += new System.EventHandler(this.frmGJCorrDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmGJCorrDataEntry_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGJCorrDataEntry_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGJCorrDataEntry_KeyPress);
			this.Resize += new System.EventHandler(this.frmGJCorrDataEntry_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
			this.fraJournalSave.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBorder)).EndInit();
			this.fraBorder.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdProcessSave;
	}
}