﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmTrialBalance.
	/// </summary>
	partial class frmTrialBalance : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAllPeriods;
		public fecherFoundation.FCComboBox cmbAllFunds;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdExport;
		public fecherFoundation.FCCheckBox chkIncludeZeroAccounts;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cmbPeriodEnd;
		public fecherFoundation.FCComboBox cmbPeriodStart;
		public fecherFoundation.FCLabel lblperiodto;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbFundEnd;
		public fecherFoundation.FCComboBox cmbFundStart;
		public fecherFoundation.FCLabel lblfundto;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTrialBalance));
			this.cmbAllPeriods = new fecherFoundation.FCComboBox();
			this.cmbAllFunds = new fecherFoundation.FCComboBox();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdExport = new fecherFoundation.FCButton();
			this.chkIncludeZeroAccounts = new fecherFoundation.FCCheckBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cmbPeriodEnd = new fecherFoundation.FCComboBox();
			this.cmbPeriodStart = new fecherFoundation.FCComboBox();
			this.lblperiodto = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmbFundEnd = new fecherFoundation.FCComboBox();
			this.cmbFundStart = new fecherFoundation.FCComboBox();
			this.lblfundto = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.btnSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeZeroAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 520);
			this.BottomPanel.Size = new System.Drawing.Size(863, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.chkIncludeZeroAccounts);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(863, 460);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdExport);
			this.TopPanel.Size = new System.Drawing.Size(863, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdExport, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(156, 30);
			this.HeaderText.Text = "Trial Balance";
			// 
			// cmbAllPeriods
			// 
			this.cmbAllPeriods.AutoSize = false;
			this.cmbAllPeriods.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllPeriods.FormattingEnabled = true;
			this.cmbAllPeriods.Items.AddRange(new object[] {
				"All periods",
				"Range of periods"
			});
			this.cmbAllPeriods.Location = new System.Drawing.Point(20, 30);
			this.cmbAllPeriods.Name = "cmbAllPeriods";
			this.cmbAllPeriods.Size = new System.Drawing.Size(350, 40);
			this.cmbAllPeriods.TabIndex = 11;
			this.cmbAllPeriods.SelectedIndexChanged += new System.EventHandler(this.optAllPeriods_CheckedChanged);
			// 
			// cmbAllFunds
			// 
			this.cmbAllFunds.AutoSize = false;
			this.cmbAllFunds.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllFunds.FormattingEnabled = true;
			this.cmbAllFunds.Items.AddRange(new object[] {
				"All funds",
				"Range of funds"
			});
			this.cmbAllFunds.Location = new System.Drawing.Point(20, 30);
			this.cmbAllFunds.Name = "cmbAllFunds";
			this.cmbAllFunds.Size = new System.Drawing.Size(350, 40);
			this.cmbAllFunds.TabIndex = 8;
			this.cmbAllFunds.SelectedIndexChanged += new System.EventHandler(this.optAllFunds_CheckedChanged);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "toolbarButton";
			this.cmdCancel.Location = new System.Drawing.Point(425, 370);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(100, 26);
			this.cmdCancel.TabIndex = 15;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Visible = false;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(669, 27);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(104, 24);
			this.cmdPrint.TabIndex = 14;
			this.cmdPrint.Text = "Print / Preview";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdExport
			// 
			this.cmdExport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdExport.AppearanceKey = "toolbarButton";
			this.cmdExport.Location = new System.Drawing.Point(776, 27);
			this.cmdExport.Name = "cmdExport";
			this.cmdExport.Size = new System.Drawing.Size(53, 24);
			this.cmdExport.TabIndex = 13;
			this.cmdExport.Text = "Export";
			this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
			// 
			// chkIncludeZeroAccounts
			// 
			this.chkIncludeZeroAccounts.Location = new System.Drawing.Point(30, 370);
			this.chkIncludeZeroAccounts.Name = "chkIncludeZeroAccounts";
			this.chkIncludeZeroAccounts.Size = new System.Drawing.Size(249, 27);
			this.chkIncludeZeroAccounts.TabIndex = 12;
			this.chkIncludeZeroAccounts.Text = "Include zero balance accounts";
			this.chkIncludeZeroAccounts.CheckedChanged += new System.EventHandler(this.chkIncludeZeroAccounts_CheckedChanged);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cmbPeriodEnd);
			this.Frame2.Controls.Add(this.cmbAllPeriods);
			this.Frame2.Controls.Add(this.cmbPeriodStart);
			this.Frame2.Controls.Add(this.lblperiodto);
			this.Frame2.Location = new System.Drawing.Point(30, 200);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(800, 150);
			this.Frame2.TabIndex = 1;
			this.Frame2.Text = "Periods To Report";
			// 
			// cmbPeriodEnd
			// 
			this.cmbPeriodEnd.AutoSize = false;
			this.cmbPeriodEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriodEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPeriodEnd.FormattingEnabled = true;
			this.cmbPeriodEnd.Location = new System.Drawing.Point(426, 90);
			this.cmbPeriodEnd.Name = "cmbPeriodEnd";
			this.cmbPeriodEnd.Size = new System.Drawing.Size(350, 40);
			this.cmbPeriodEnd.TabIndex = 10;
			this.cmbPeriodEnd.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodEnd_SelectedIndexChanged);
			// 
			// cmbPeriodStart
			// 
			this.cmbPeriodStart.AutoSize = false;
			this.cmbPeriodStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriodStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPeriodStart.FormattingEnabled = true;
			this.cmbPeriodStart.Location = new System.Drawing.Point(20, 90);
			this.cmbPeriodStart.Name = "cmbPeriodStart";
			this.cmbPeriodStart.Size = new System.Drawing.Size(350, 40);
			this.cmbPeriodStart.TabIndex = 9;
			this.cmbPeriodStart.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodStart_SelectedIndexChanged);
			// 
			// lblperiodto
			// 
			this.lblperiodto.Location = new System.Drawing.Point(391, 104);
			this.lblperiodto.Name = "lblperiodto";
			this.lblperiodto.Size = new System.Drawing.Size(17, 14);
			this.lblperiodto.TabIndex = 11;
			this.lblperiodto.Text = "TO";
			this.lblperiodto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmbFundEnd);
			this.Frame1.Controls.Add(this.cmbAllFunds);
			this.Frame1.Controls.Add(this.cmbFundStart);
			this.Frame1.Controls.Add(this.lblfundto);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(800, 150);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Funds To Report";
			// 
			// cmbFundEnd
			// 
			this.cmbFundEnd.AutoSize = false;
			this.cmbFundEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFundEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFundEnd.FormattingEnabled = true;
			this.cmbFundEnd.Location = new System.Drawing.Point(426, 90);
			this.cmbFundEnd.Name = "cmbFundEnd";
			this.cmbFundEnd.Size = new System.Drawing.Size(350, 40);
			this.cmbFundEnd.TabIndex = 7;
			this.cmbFundEnd.SelectedIndexChanged += new System.EventHandler(this.cmbFundEnd_SelectedIndexChanged);
			// 
			// cmbFundStart
			// 
			this.cmbFundStart.AutoSize = false;
			this.cmbFundStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFundStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFundStart.FormattingEnabled = true;
			this.cmbFundStart.Location = new System.Drawing.Point(20, 90);
			this.cmbFundStart.Name = "cmbFundStart";
			this.cmbFundStart.Size = new System.Drawing.Size(350, 40);
			this.cmbFundStart.TabIndex = 6;
			this.cmbFundStart.SelectedIndexChanged += new System.EventHandler(this.cmbFundStart_SelectedIndexChanged);
			// 
			// lblfundto
			// 
			this.lblfundto.Location = new System.Drawing.Point(391, 104);
			this.lblfundto.Name = "lblfundto";
			this.lblfundto.Size = new System.Drawing.Size(17, 14);
			this.lblfundto.TabIndex = 8;
			this.lblfundto.Text = "TO";
			this.lblfundto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// btnSaveContinue
			// 
			this.btnSaveContinue.AppearanceKey = "acceptButton";
			this.btnSaveContinue.Location = new System.Drawing.Point(324, 30);
			this.btnSaveContinue.Name = "btnSaveContinue";
			this.btnSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSaveContinue.Size = new System.Drawing.Size(158, 48);
			this.btnSaveContinue.TabIndex = 0;
			this.btnSaveContinue.Text = "Save & Continue";
			this.btnSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmTrialBalance
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(863, 628);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTrialBalance";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Trial Balance";
			this.Load += new System.EventHandler(this.frmTrialBalance_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTrialBalance_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeZeroAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private fecherFoundation.FCButton btnSaveContinue;
	}
}
