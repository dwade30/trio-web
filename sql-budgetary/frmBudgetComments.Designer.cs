//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetComments.
	/// </summary>
	partial class frmBudgetComments : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRevenue;
		public fecherFoundation.FCLabel lblRevenue;
		public fecherFoundation.FCTextBox txtComment;
		public fecherFoundation.FCGrid vsAccounts;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBudgetComments));
            this.cmbRevenue = new fecherFoundation.FCComboBox();
            this.lblRevenue = new fecherFoundation.FCLabel();
            this.txtComment = new fecherFoundation.FCTextBox();
            this.vsAccounts = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 530);
            this.BottomPanel.Size = new System.Drawing.Size(590, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbRevenue);
            this.ClientArea.Controls.Add(this.lblRevenue);
            this.ClientArea.Controls.Add(this.txtComment);
            this.ClientArea.Controls.Add(this.vsAccounts);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.lblTitle);
            this.ClientArea.Size = new System.Drawing.Size(610, 438);
            this.ClientArea.Controls.SetChildIndex(this.lblTitle, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsAccounts, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblRevenue, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbRevenue, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(610, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(202, 28);
            this.HeaderText.Text = "Budget Comments";
            // 
            // cmbRevenue
            // 
            this.cmbRevenue.Items.AddRange(new object[] {
            "Revenue Accounts",
            "Expense Accounts"});
            this.cmbRevenue.Location = new System.Drawing.Point(195, 30);
            this.cmbRevenue.Name = "cmbRevenue";
            this.cmbRevenue.Size = new System.Drawing.Size(180, 40);
            this.cmbRevenue.TabIndex = 10;
            // 
            // lblRevenue
            // 
            this.lblRevenue.Location = new System.Drawing.Point(30, 44);
            this.lblRevenue.Name = "lblRevenue";
            this.lblRevenue.Size = new System.Drawing.Size(93, 16);
            this.lblRevenue.TabIndex = 11;
            this.lblRevenue.Text = "ACCOUNT TYPE";
            // 
            // txtComment
            // 
            this.txtComment.AcceptsReturn = true;
            this.txtComment.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(30, 470);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(505, 60);
            this.txtComment.TabIndex = 1;
            this.txtComment.Visible = false;
            // 
            // vsAccounts
            // 
            this.vsAccounts.FixedCols = 0;
            this.vsAccounts.Location = new System.Drawing.Point(30, 110);
            this.vsAccounts.Name = "vsAccounts";
            this.vsAccounts.RowHeadersVisible = false;
            this.vsAccounts.Rows = 1;
            this.vsAccounts.Size = new System.Drawing.Size(220, 300);
            this.vsAccounts.TabIndex = 2;
            this.vsAccounts.Visible = false;
            this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(30, 20);
            this.Label1.TabIndex = 7;
            this.Label1.Visible = false;
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.Color.FromArgb(0, 0, 255);
            this.Label2.Location = new System.Drawing.Point(30, 70);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(30, 20);
            this.Label2.TabIndex = 6;
            this.Label2.Visible = false;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(80, 33);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(82, 16);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "NO COMMENT";
            this.Label3.Visible = false;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(80, 73);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(62, 16);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "COMMENT";
            this.Label4.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblTitle.Location = new System.Drawing.Point(30, 430);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(505, 20);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTitle.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.mnuFileSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuProcessSave.Text = "Save";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 1;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSave.Text = "Save & Exit";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(237, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(104, 48);
            this.btnProcess.TabIndex = 4;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmBudgetComments
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(610, 498);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBudgetComments";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Budget Comments";
            this.Load += new System.EventHandler(this.frmBudgetComments_Load);
            this.Activated += new System.EventHandler(this.frmBudgetComments_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBudgetComments_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBudgetComments_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
	}
}