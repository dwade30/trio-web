﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupProjects.
	/// </summary>
	public partial class frmSetupProjects : BaseForm
	{
		public frmSetupProjects()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupProjects InstancePtr
		{
			get
			{
				return (frmSetupProjects)Sys.GetInstance(typeof(frmSetupProjects));
			}
		}

		protected frmSetupProjects _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int KeyCol;
		int CodeCol;
		int ShortCol;
		int LongCol;
		int ProjectedCostCol;
		int FundCol;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnUnload;

		private void frmSetupProjects_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupProjects_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupProjects.FillStyle	= 0;
			//frmSetupProjects.ScaleWidth	= 9045;
			//frmSetupProjects.ScaleHeight	= 7365;
			//frmSetupProjects.LinkTopic	= "Form2";
			//frmSetupProjects.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			KeyCol = 0;
			CodeCol = 1;
			ShortCol = 2;
			LongCol = 3;
			ProjectedCostCol = 4;
			FundCol = 5;
			vsProjects.TextMatrix(0, CodeCol, "Proj #");
			vsProjects.TextMatrix(0, ShortCol, "Short Desc");
			vsProjects.TextMatrix(0, LongCol, "Long Desc");
			vsProjects.TextMatrix(0, ProjectedCostCol, "Projected Cost");
			vsProjects.TextMatrix(0, FundCol, "Fund");
			//FC:FINAL:DDU:#2926 - aligned columns
			vsProjects.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsProjects.ColAlignment(ShortCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsProjects.ColAlignment(LongCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsProjects.ColAlignment(ProjectedCostCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsProjects.ColAlignment(FundCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsProjects.ColHidden(KeyCol, true);
			vsProjects.ColFormat(ProjectedCostCol, "#,##0.00");
			vsProjects.ColWidth(CodeCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.07));
			vsProjects.ColWidth(ShortCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.24));
			vsProjects.ColWidth(LongCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.4));
			vsProjects.ColWidth(ProjectedCostCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.18));
			vsProjects.ColWidth(FundCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.06));
			LoadProjectsGrid();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSetupProjects_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSetupProjects_Resize(object sender, System.EventArgs e)
		{
			vsProjects.ColWidth(CodeCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.07));
			vsProjects.ColWidth(ShortCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.24));
			vsProjects.ColWidth(LongCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.4));
			vsProjects.ColWidth(ProjectedCostCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.18));
			vsProjects.ColWidth(FundCol, FCConvert.ToInt32(vsProjects.WidthOriginal * 0.06));
		}

		private void mnuFileAdd_Click(object sender, System.EventArgs e)
		{
			vsProjects.Rows += 1;
			vsProjects.TextMatrix(vsProjects.Rows - 1, KeyCol, FCConvert.ToString(0));
			vsProjects.Select(vsProjects.Rows - 1, CodeCol);
			// vsProjects.EditCell
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool blnUsed = false;
			// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
			DialogResult intAnswer;
			if (FCConvert.ToDouble(vsProjects.TextMatrix(vsProjects.Row, KeyCol)) != 0)
			{
				blnUsed = false;
				rsCheck.OpenRecordset("SELECT * FROM APJournalDetail WHERE Project = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "'");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					blnUsed = true;
				}
				rsCheck.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE Project = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "'");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					blnUsed = true;
				}
				rsCheck.OpenRecordset("SELECT * FROM JournalEntries WHERE Project = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "'");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					blnUsed = true;
				}
				if (blnUsed)
				{
					MessageBox.Show("This project code is currently being used in Budgetary so it may not be deleted.", "Unable To Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (modGlobalConstants.Statics.gboolPY && modGlobalConstants.Statics.gstrArchiveYear == "")
				{
					blnUsed = false;
					rsCheck.OpenRecordset("SELECT * FROM tblPayrollDistribution WHERE Project = " + vsProjects.TextMatrix(vsProjects.Row, KeyCol), "TWPY0000.vb1");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						blnUsed = true;
					}
					// rsCheck.OpenRecordset "SELECT * FROM tblCheckDetail WHERE Project = " & vsProjects.TextMatrix(vsProjects.Row, KeyCol), "TWPY0000.vb1"
					// If rsCheck.EndOfFile <> True And rsCheck.BeginningOfFile <> True Then
					// blnUsed = True
					// End If
					rsCheck.OpenRecordset("SELECT * FROM tblTempPayProcess WHERE Project = " + vsProjects.TextMatrix(vsProjects.Row, KeyCol), "TWPY0000.vb1");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						blnUsed = true;
					}
					if (blnUsed)
					{
						MessageBox.Show("This project code is currently being used in Payroll so it may not be deleted.", "Unable To Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gstrArchiveYear == "")
				{
					blnUsed = false;
					rsCheck.OpenRecordset("SELECT * FROM Archive WHERE Project1 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project2 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project3 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project4 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project5 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project6 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "'", "TWCR0000.vb1");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						blnUsed = true;
					}
					// rsCheck.OpenRecordset "SELECT * FROM LastYearArchive WHERE Project1 = '" & vsProjects.TextMatrix(vsProjects.Row, CodeCol) & "' OR Project2 = '" & vsProjects.TextMatrix(vsProjects.Row, CodeCol) & "' OR Project3 = '" & vsProjects.TextMatrix(vsProjects.Row, CodeCol) & "' OR Project4 = '" & vsProjects.TextMatrix(vsProjects.Row, CodeCol) & "' OR Project5 = '" & vsProjects.TextMatrix(vsProjects.Row, CodeCol) & "' OR Project6 = '" & vsProjects.TextMatrix(vsProjects.Row, CodeCol) & "'", "TWCR0000.vb1"
					// If rsCheck.EndOfFile <> True And rsCheck.BeginningOfFile <> True Then
					// blnUsed = True
					// End If
					rsCheck.OpenRecordset("SELECT * FROM Type WHERE Project1 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project2 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project3 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project4 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project5 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "' OR Project6 = '" + vsProjects.TextMatrix(vsProjects.Row, CodeCol) + "'", "TWCR0000.vb1");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						blnUsed = true;
					}
					if (blnUsed)
					{
						MessageBox.Show("This project code is currently being used in Cash Receipts so it may not be deleted.", "Unable To Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				intAnswer = MessageBox.Show("Once this project is deleted you will not be able to get the information back.  Do you wish to continue?", "Delete Project?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intAnswer == DialogResult.Yes)
				{
					rsCheck.Execute("DELETE FROM ProjectMaster WHERE ID = " + vsProjects.TextMatrix(vsProjects.Row, KeyCol), "Budgetary");
					vsProjects.RemoveItem(vsProjects.Row);
					MessageBox.Show("Project deleted successfully!", "Project Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				vsProjects.RemoveItem(vsProjects.Row);
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadProjectsGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			rsInfo.OpenRecordset("SELECT * FROM ProjectMaster ORDER BY ProjectCode");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsProjects.Rows += 1;
					vsProjects.TextMatrix(vsProjects.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vsProjects.TextMatrix(vsProjects.Rows - 1, CodeCol, Strings.Format(rsInfo.Get_Fields_String("ProjectCode"), "0000"));
					vsProjects.TextMatrix(vsProjects.Rows - 1, LongCol, Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("LongDescription"))));
					vsProjects.TextMatrix(vsProjects.Rows - 1, ShortCol, Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("ShortDescription"))));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					vsProjects.TextMatrix(vsProjects.Rows - 1, FundCol, modValidateAccount.GetFormat_6(rsInfo.Get_Fields("Fund"), FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					vsProjects.TextMatrix(vsProjects.Rows - 1, ProjectedCostCol, Strings.Format(rsInfo.Get_Fields_Decimal("ProjectedCost"), "#,##0.00"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void SaveInfo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			if (vsProjects.Col < vsProjects.Cols - 1)
			{
				vsProjects.Select(vsProjects.Row, vsProjects.Col + 1);
			}
			else
			{
				vsProjects.Select(vsProjects.Row, vsProjects.Col - 1);
			}
			for (counter = 1; counter <= vsProjects.Rows - 1; counter++)
			{
				if (Conversion.Val(vsProjects.TextMatrix(counter, CodeCol)) == 0)
				{
					MessageBox.Show("You must enter a project code before you may save.", "Invalid Project Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsProjects.Select(counter, CodeCol);
					return;
				}
				if (Strings.Trim(vsProjects.TextMatrix(counter, ShortCol)) == "")
				{
					MessageBox.Show("You must enter a short description before you may save.", "Invalid Short Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsProjects.Select(counter, ShortCol);
					return;
				}
				if (Conversion.Val(vsProjects.TextMatrix(counter, FundCol)) == 0)
				{
					MessageBox.Show("You must enter a fund before you may save.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsProjects.Select(counter, FundCol);
					return;
				}
				if (Strings.Trim(vsProjects.TextMatrix(counter, ProjectedCostCol)) == "")
				{
					MessageBox.Show("You must enter a projected cost before you may save.", "Invalid Projected Cost", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsProjects.Select(counter, ProjectedCostCol);
					return;
				}
				else if (FCConvert.ToDecimal(vsProjects.TextMatrix(counter, ProjectedCostCol)) == 0)
				{
					MessageBox.Show("You must enter a projected cost before you may save.", "Invalid Projected Cost", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsProjects.Select(counter, ProjectedCostCol);
					return;
				}
			}
			for (counter = 1; counter <= vsProjects.Rows - 1; counter++)
			{
				if (FCConvert.ToDouble(vsProjects.TextMatrix(counter, KeyCol)) == 0)
				{
					rsInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE ID = 0");
					rsInfo.AddNew();
				}
				else
				{
					rsInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE ID = " + vsProjects.TextMatrix(counter, KeyCol));
					rsInfo.Edit();
				}
				rsInfo.Set_Fields("ProjectCode", vsProjects.TextMatrix(counter, CodeCol));
				rsInfo.Set_Fields("ShortDescription", Strings.Trim(vsProjects.TextMatrix(counter, ShortCol)));
				if (Strings.Trim(vsProjects.TextMatrix(counter, LongCol)) == "")
				{
					rsInfo.Set_Fields("LongDescription", Strings.Trim(vsProjects.TextMatrix(counter, ShortCol)));
				}
				else
				{
					rsInfo.Set_Fields("LongDescription", Strings.Trim(vsProjects.TextMatrix(counter, LongCol)));
				}
				rsInfo.Set_Fields("Fund", vsProjects.TextMatrix(counter, FundCol));
				rsInfo.Set_Fields("ProjectedCost", vsProjects.TextMatrix(counter, ProjectedCostCol));
				rsInfo.Update();
				if (FCConvert.ToDouble(vsProjects.TextMatrix(counter, KeyCol)) == 0)
				{
					vsProjects.TextMatrix(counter, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
				}
			}
			MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				Close();
			}
		}

		private void vsProjects_RowColChange(object sender, System.EventArgs e)
		{
			if (vsProjects.Col == CodeCol)
			{
				vsProjects.EditMaxLength = 4;
			}
			else if (vsProjects.Col == ShortCol)
			{
				vsProjects.EditMaxLength = 12;
			}
			else if (vsProjects.Col == LongCol)
			{
				vsProjects.EditMaxLength = 25;
			}
			else if (vsProjects.Col == FundCol)
			{
				vsProjects.EditMaxLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))));
			}
			else
			{
				vsProjects.EditMaxLength = 0;
			}
			if (vsProjects.Row > 0)
			{
				vsProjects.EditCell();
			}
		}

		private void vsProjects_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int intCounter;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsProjects.GetFlexRowIndex(e.RowIndex);
			int col = vsProjects.GetFlexColIndex(e.ColumnIndex);
			if (col == CodeCol)
			{
				if (!Information.IsNumeric(vsProjects.EditText) && Strings.Trim(vsProjects.EditText) != "")
				{
					MessageBox.Show("You may only enter a numeric project number.", "Invalid Project Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
				else
				{
					for (intCounter = 1; intCounter <= vsProjects.Rows - 1; intCounter++)
					{
						if (intCounter != row)
						{
							if (Conversion.Val(vsProjects.TextMatrix(intCounter, CodeCol)) == Conversion.Val(vsProjects.EditText))
							{
								MessageBox.Show("There is already a project with this code.  You must change this code before you may continue.", "Duplicate Project Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
								e.Cancel = true;
								return;
							}
						}
					}
					vsProjects.EditText = modValidateAccount.GetFormat_6(vsProjects.EditText, 4);
				}
			}
			else if (col == FundCol)
			{
				if (!Information.IsNumeric(vsProjects.EditText))
				{
					MessageBox.Show("You may only enter a numeric fund.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
				else
				{
					vsProjects.EditText = modValidateAccount.GetFormat_6(vsProjects.EditText, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
				}
			}
			else if (col == ProjectedCostCol)
			{
				if (!Information.IsNumeric(vsProjects.EditText))
				{
					MessageBox.Show("You may only enter a numeric value for the projected cost.", "Invalid Projected Cost", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
			}
		}
	}
}
