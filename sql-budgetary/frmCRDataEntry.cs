﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCRDataEntry.
	/// </summary>
	public partial class frmCRDataEntry : BaseForm
	{
		public frmCRDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCRDataEntry InstancePtr
		{
			get
			{
				return (frmCRDataEntry)Sys.GetInstance(typeof(frmCRDataEntry));
			}
		}

		protected frmCRDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumberCol;
		int DateCol;
		int DescriptionCol;
		int AccountCol;
		int ProjCol;
		int AmountCol;
		public double TotalAmount;
		public int ChosenJournal;
		// vbPorter upgrade warning: ChosenPeriod As short --> As int	OnWrite(string, int)
		public int ChosenPeriod;
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		string ErrorString = "";
		bool BadAccountFlag;
		bool blnUnload;
		public bool blnWindowsCashReceipts;
		bool blnUnchanged;
		int intCurIndex;
		bool blnJournalLocked;
		string strComboList;
		clsGridAccount vsGrid = new clsGridAccount();
		bool blnDirty;
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;
		bool showFormWhenThisClosed = false;

		private void frmCRDataEntry_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtPeriod.Focus();
			txtPeriod.SelectionStart = 0;
			txtPeriod.SelectionLength = txtPeriod.Text.Length;
			this.Refresh();
		}

		private void frmCRDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmCRDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCRDataEntry.FillStyle	= 0;
			//frmCRDataEntry.ScaleWidth	= 9300;
			//frmCRDataEntry.ScaleHeight	= 7350;
			//frmCRDataEntry.LinkTopic	= "Form2";
			//frmCRDataEntry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsProjects = new clsDRWrapper();
			blnJournalLocked = false;
			NumberCol = 0;
			DateCol = 1;
			DescriptionCol = 2;
			AccountCol = 3;
			ProjCol = 4;
			AmountCol = 5;
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "R";
			vsGrid.AccountCol = FCConvert.ToInt16(AccountCol);
			intCurIndex = 0;
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.32 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.29 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.068 * vs1.WidthOriginal));
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, DateCol, "Date");
			vs1.TextMatrix(0, ProjCol, "Proj");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, AmountCol, "Credits");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DateCol, 0, AmountCol, 4);
			vs1.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColEditMask(DateCol, "0#/0#/0#");
			vs1.ExtendLastCol = true;
			strComboList = "# ; " + "\t" + " |";
			if (modBudgetaryMaster.Statics.ProjectFlag)
			{
				rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
				if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
				{
					do
					{
						strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
						rsProjects.MoveNext();
					}
					while (rsProjects.EndOfFile() != true);
					strComboList = Strings.Left(strComboList, strComboList.Length - 1);
				}
				vs1.ColComboList(ProjCol, strComboList);
			}
			FillJournalCombo();
			if (!modBudgetaryMaster.Statics.blnCREdit)
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, DescriptionCol, "");
					vs1.TextMatrix(counter, ProjCol, "");
					vs1.TextMatrix(counter, AccountCol, "");
					vs1.TextMatrix(counter, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
					vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
				}
				txtPeriod.Text = CalculatePeriod();
				if (ChosenJournal == 0)
				{
					cboJournal.SelectedIndex = 0;
					// put journal number in the text box
					cboSaveJournal.SelectedIndex = 0;
				}
				else
				{
					SetCombo(ChosenJournal, ref ChosenPeriod);
					txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2);
				}
			}
			else
			{
				SetCombo(ChosenJournal, ref ChosenPeriod);
				txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2);
				CalculateTotals();
				lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
			}
			blnDirty = false;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			//Application.DoEvents();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			//FC:FINAL:BBE:#436 - attach key event handlers to grid editor
			this.vs1.EditingControlShowing += vs1_EditingControlShowing;
		}

		private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.KeyDown -= new KeyEventHandler(vs1_KeyDownEdit);
				//e.Control.KeyPress -= new KeyPressEventHandler(vs1_KeyPressEdit);
				//e.Control.KeyUp -= new KeyEventHandler(vs1_KeyUpEdit);
				e.Control.KeyDown += new KeyEventHandler(vs1_KeyDownEdit);
				//e.Control.KeyPress += new KeyPressEventHandler(vs1_KeyPressEdit);
				//e.Control.KeyUp += new KeyEventHandler(vs1_KeyUpEdit);
			}
		}

		private void frmCRDataEntry_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.32 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.29 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.068 * vs1.WidthOriginal));
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				//FC:FINAl:SBE - grid is anchored
				//vs1.RowHeight(counter, FCConvert.ToInt32(vs1.Height * 0.06));
			}
		}

		private void FrmCRDataEntry_FormClosed(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			if (showFormWhenThisClosed)
			{
				frmGetCRDataEntry.InstancePtr.Show(App.MainForm);
			}
		}

		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("You have changed, but not saved some of the data on this screen.  Would you like to save now?", "Save Now?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
				if (ans == DialogResult.Yes)
				{
					e.Cancel = true;
					mnuProcessSave_Click();
					return;
				}
				else if (ans == DialogResult.No)
				{
					if (blnJournalLocked)
					{
						modBudgetaryAccounting.UnlockJournal();
					}
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					showFormWhenThisClosed = true;
				}
				else
				{
					e.Cancel = true;
					return;
				}
			}
			else
			{
				if (blnJournalLocked)
				{
					modBudgetaryAccounting.UnlockJournal();
				}
				//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
				showFormWhenThisClosed = true;
			}
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void frmCRDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmCRDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
				{
					if (frmCRDataEntry.InstancePtr.ActiveControl.GetName() == "cboJournal")
					{
						KeyAscii = (Keys)0;
						vs1.Focus();
					}
					else
					{
						KeyAscii = (Keys)0;
						Support.SendKeys("{TAB}", false);
					}
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int temp = 0;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && Convert.ToDateTime(vs1.TextMatrix(vs1.Row, DateCol)).ToOADate() == DateTime.Today.ToOADate() && vs1.TextMatrix(vs1.Row, ProjCol) == "" && vs1.TextMatrix(vs1.Row, AmountCol) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, AmountCol, vs1.Row, NumberCol);
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					if (blnWindowsCashReceipts)
					{
						modGlobalFunctions.AddCYAEntry_26("BD", "Deleted entry to account " + vs1.TextMatrix(vs1.Row, AccountCol) + " for the amount of " + Strings.Format(vs1.TextMatrix(vs1.Row, AmountCol), "#,##0.00") + " in a Windows Cash Receipts Journal", "Cash Receipts Data Entry");
					}
					DeleteFlag = true;
					temp = vs1.Row;
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
					}
					else
					{
						vs1.Row -= 1;
					}
					if (Conversion.Val(vs1.TextMatrix(temp, NumberCol)) != 0)
					{
						rsDelete.Execute("DELETE FROM JournalEntries WHERE ID = " + vs1.TextMatrix(temp, NumberCol), "Budgetary");
					}
					vs1.Col = NumberCol;
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DateCol, FCConvert.ToString(DateTime.Today));
					vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					CalculateTotals();
					lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
					DeleteFlag = false;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DateCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(vs1.Row, DateCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(btnProcessSave, new System.EventArgs());
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void txtPeriod_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtPeriod_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void txtPeriod_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPeriod_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtPeriod.Text == "")
			{
				txtPeriod.Text = CalculatePeriod();
			}
			else if (Conversion.Val(txtPeriod.Text) == 0)
			{
				txtPeriod.Text = CalculatePeriod();
			}
			else if (!Information.IsNumeric(txtPeriod.Text))
			{
				MessageBox.Show("You must enter a number in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else if (Conversion.Val(txtPeriod.Text) > 12)
			{
				MessageBox.Show("You must enter a number less then 12 in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else
			{
				temp = txtPeriod.Text;
				txtPeriod.Text = modValidateAccount.GetFormat_6(temp, 2);
			}
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vs1.Col == AccountCol)
			{
				vs1.EditMaxLength = 23;
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				blnUnchanged = false;
				blnDirty = true;
				if (vs1.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			blnUnchanged = true;
			if (frmCRDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, AccountCol), ref lblExpense);
			btnProcessDelete.Enabled = true;
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DateCol;
			}
			else if (vs1.Col != AccountCol)
			{
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			blnUnchanged = true;
			modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, AccountCol), ref lblExpense);
			if (vs1.Col == NumberCol)
			{
				modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, AccountCol), ref lblExpense);
				vs1.Col = DateCol;
			}
			else if (vs1.Col == ProjCol)
			{
				if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
				{
					vs1.Col += 1;
				}
				else
				{
					modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, AccountCol), ref lblExpense);
					vs1.EditCell();
				}
			}
			else if (vs1.Col == DescriptionCol)
			{
				vs1.EditMaxLength = 25;
				modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, AccountCol), ref lblExpense);
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else
			{
				modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, AccountCol), ref lblExpense);
				if (vs1.Col == AmountCol)
				{
					vs1.EditMaxLength = 13;
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			blnUnchanged = true;
			if (vs1.Col != AccountCol)
			{
				if (vs1.Col == AmountCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			//FC:FINAL:AM:#436 - moved code from form KeyDown (event not fired when the focus is in the grid)
			if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
			{
				modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, e.Shift ? 1 : 0, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
				vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
			}
			bool blnLocalUnchanged;
			blnLocalUnchanged = blnUnchanged;
			if (vs1.Col == AccountCol && KeyCode != Keys.F9)
			{
				if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
				{
					KeyCode = 0;
					vs1.Col -= 1;
				}
				else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
				{
					KeyCode = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					vs1.Col += 1;
					if (vs1.Col - 1 == DateCol || vs1.Col - 1 == DescriptionCol)
					{
						if (vs1.Row > 1)
						{
							if (blnLocalUnchanged)
							{
								vs1.TextMatrix(vs1.Row, vs1.Col - 1, vs1.TextMatrix(vs1.Row - 1, vs1.Col - 1));
							}
						}
					}
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
						vs1.Row += 1;
						vs1.Col = DateCol;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (vs1.Col == AmountCol)
				{
					if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
					{
						KeyCode = 0;
						vs1.Col -= 2;
					}
				}
			}
			else if (vs1.Col == AmountCol)
			{
				if (KeyCode == Keys.C)
				{
					// if the c key is hit then erase the value
					KeyCode = 0;
					vs1.EditText = "";
					Support.SendKeys("{BACKSPACE}", false);
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string holder = "";
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == DateCol)
			{
				if (!Information.IsDate(vs1.EditText))
				{
					MessageBox.Show("You must enter a valid date in this field", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
			}
			else if (col == AmountCol)
			{
				if (Strings.Trim(vs1.EditText) != "")
				{
					if (!Information.IsNumeric(vs1.EditText))
					{
						MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditText = "";
					}
					else
					{
						temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
						if (temp != 0)
						{
							if (vs1.EditText.Length > temp + 2)
							{
								vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
							}
						}
						CalculateTotals();
						lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
					}
				}
				else
				{
					if (Strings.Trim(vs1.EditText) == "")
					{
						vs1.EditText = "0";
					}
				}
			}
		}

		public void CalculateTotals()
		{
			int counter;
			TotalAmount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (counter == vs1.Row)
				{
					if (vs1.Col == AmountCol)
					{
						TotalAmount += Conversion.Val(vs1.EditText);
					}
					else
					{
						TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
					}
				}
				else
				{
					TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
				}
			}
		}
		// vbPorter upgrade warning: intPeriod As short	OnWriteFCConvert.ToInt32(
		private void SetCombo(int x, ref int intPeriod)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(intPeriod), 2) == Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			int counter;
			int ans;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			int lngCheckJournal = 0;
			txtPeriod.Focus();
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "CR");
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Status = 'P'");
				if (Master.BeginningOfFile() != true && Master.EndOfFile() != true)
				{
					MessageBox.Show("Changes to this journal cannot be saved because it has been posted.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
					blnDirty = false;
					Close();
					return;
				}
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				bool executeAddTag = false;
				if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
				{
					executeAddTag = true;
					goto AddTag;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(txtPeriod.Text))
				{
					executeAddTag = true;
					goto AddTag;
				}
				else
				{
					if (modBudgetaryMaster.Statics.blnCREdit)
					{
						if (ChosenJournal != Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)))
						{
							rsDeleteInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(ChosenJournal) + " AND Period = " + FCConvert.ToString(ChosenPeriod));
							if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
							{
								rsDeleteInfo.Edit();
								rsDeleteInfo.Set_Fields("Status", "D");
								rsDeleteInfo.Update();
							}
							for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
							{
								if (modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenJournal), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2) == Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2))
								{
									cboSaveJournal.Items.RemoveAt(counter);
									cboJournal.Items.RemoveAt(counter);
								}
							}
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
					Master.Edit();
					Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
					Master.Update();
					cboJournal.Clear();
					cboSaveJournal.Clear();
					FillJournalCombo();
					for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
						{
							cboSaveJournal.SelectedIndex = counter;
							cboJournal.SelectedIndex = counter;
							break;
						}
					}
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			if (cboSaveJournal.SelectedIndex != 0)
			{
				modRegistry.SaveRegistryKey("CURRCRJRNL", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryMaster.Statics.CurrentCREntry = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				modRegistry.SaveRegistryKey("CURRCRJRNL", FCConvert.ToString(TempJournal));
				modBudgetaryMaster.Statics.CurrentCREntry = TempJournal;
			}
			if (modBudgetaryMaster.Statics.blnCREdit)
			{
				// rs.OpenRecordset ("SELECT * FROM JournalEntries WHERE (Type = 'C' OR Type = 'W') and JournalNumber = " & ChosenJournal & " AND Period = " & ChosenPeriod & " AND Status = 'E'")
				// If rs.EndOfFile <> True And rs.BeginningOfFile <> True Then
				// rs.MoveLast
				// rs.MoveFirst
				// Do Until rs.EndOfFile
				// rs.Delete
				// rs.MoveNext
				// Loop
				// End If
				ChosenPeriod = FCConvert.ToInt32(txtPeriod.Text);
			}
			else if (modBudgetaryMaster.Statics.blnCREdit)
			{
				ChosenPeriod = FCConvert.ToInt32(txtPeriod.Text);
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			if (!blnWindowsCashReceipts)
			{
				clsJournalInfo.JournalType = "CR";
			}
			else
			{
				clsJournalInfo.JournalType = "CW";
			}
			clsJournalInfo.Period = txtPeriod.Text;
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			rs.OmitNullsOnInsert = true;
			if (!blnWindowsCashReceipts)
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.TextMatrix(counter, AmountCol) != "0")
					{
						if (Conversion.Val(vs1.TextMatrix(counter, NumberCol)) == 0)
						{
							rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
							rs.AddNew();
							rs.Set_Fields("RCB", "R");
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = " + vs1.TextMatrix(counter, NumberCol));
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.Edit();
							}
							else
							{
								rs.AddNew();
								rs.Set_Fields("RCB", "R");
							}
						}
						rs.Set_Fields("Type", "C");
						rs.Set_Fields("JournalEntriesDate", vs1.TextMatrix(counter, DateCol));
						rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
						if (cboSaveJournal.SelectedIndex != 0)
						{
							rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
						}
						else
						{
							rs.Set_Fields("JournalNumber", TempJournal);
						}
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngCheckJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
						rs.Set_Fields("Period", txtPeriod.Text);
						rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjCol));
						rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
						rs.Set_Fields("Amount", FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) * -1);
						rs.Set_Fields("Status", "E");
						rs.Update();
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					}
				}
			}
			else
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.TextMatrix(counter, AmountCol) != "0")
					{
						if (Conversion.Val(vs1.TextMatrix(counter, NumberCol)) == 0)
						{
							rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
							rs.AddNew();
							rs.Set_Fields("RCB", "R");
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = " + vs1.TextMatrix(counter, NumberCol));
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.Edit();
							}
							else
							{
								rs.AddNew();
								rs.Set_Fields("RCB", "R");
							}
						}
						rs.Set_Fields("Type", "W");
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						rs.Set_Fields("JournalEntriesDate", vs1.TextMatrix(counter, DateCol));
						rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
						if (cboSaveJournal.SelectedIndex != 0)
						{
							rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
						}
						else
						{
							rs.Set_Fields("JournalNumber", TempJournal);
						}
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngCheckJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
						rs.Set_Fields("Period", txtPeriod.Text);
						rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjCol));
						rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
						rs.Set_Fields("Amount", FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) * -1);
						rs.Set_Fields("Status", "E");
						rs.Update();
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					}
					else
					{
						if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) != 0)
						{
							rs.Execute(("DELETE FROM JournalEntries WHERE ID = " + vs1.TextMatrix(counter, NumberCol)), "Budgetary");
						}
					}
				}
			}
			blnDirty = false;
			if (blnUnload)
			{
				if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCashReceipts))
				{
					if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						blnPostJournal = true;
					}
				}
				else
				{
					blnPostJournal = false;
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
				}
				Close();
			}
			else
			{
				cboJournal.Clear();
				cboSaveJournal.Clear();
				FillJournalCombo();
				for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
				{
					if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == lngCheckJournal)
					{
						cboSaveJournal.SelectedIndex = counter;
						cboJournal.SelectedIndex = counter;
						break;
					}
				}
				txtJournalDescription.Text = "";
				fraJournalSave.Visible = false;
				MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.Width = 90;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND (Type = 'CR' OR Type = 'CW') ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			bool blnData;
			txtPeriod.Focus();
			blnData = false;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) != 0)
				{
					blnData = true;
					if (vs1.TextMatrix(counter, DescriptionCol) == "" || vs1.TextMatrix(counter, AccountCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
				{
					ans = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						vs1.Select(counter, AccountCol);
						vs1.Focus();
						return;
					}
					else
					{
						modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "Cash Receipts Data Entry");
					}
				}
			}
			if (!blnData)
			{
				MessageBox.Show("You must make at least one entry before you may save this", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cboJournal.SelectedIndex == 0)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Cancel)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else if (answer == DialogResult.No)
				{
					//FC:FINAL:RPU: #i560 Center the frame to the ClientArea before show it
					fraJournalSave.CenterToContainer(this.ClientArea);
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
					lblJournalSave.Visible = true;
					cboSaveJournal.Visible = true;
					lblJournalDescription.Visible = false;
					txtJournalDescription.Visible = false;
					cboSaveJournal.Focus();
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else
				{
					//FC:FINAL:RPU: #i560 Center the frame to the ClientArea before show it
					fraJournalSave.CenterToContainer(this.ClientArea);
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
					lblJournalSave.Visible = false;
					cboSaveJournal.Visible = false;
					lblJournalDescription.Visible = true;
					txtJournalDescription.Visible = true;
					txtJournalDescription.Focus();
					return;
				}
			}
			else
			{
				SaveJournal();
			}
		}

		public void Init(ref cJournal journ)
		{
			if (!(journ == null))
			{
				modBudgetaryMaster.Statics.blnCREdit = true;
				ChosenJournal = journ.JournalNumber;
				ChosenPeriod = journ.Period;
				if (journ.JournalEntries.ItemCount() > 15)
				{
					vs1.Rows = journ.JournalEntries.ItemCount() + 1;
				}
				if (journ.JournalType == "CW")
				{
					blnWindowsCashReceipts = true;
				}
				else
				{
					blnWindowsCashReceipts = false;
				}
				txtPeriod.Text = modValidateAccount.GetFormat_6(journ.Period.ToString(), 2);
				journ.JournalEntries.MoveFirst();
				int counter = 0;
				counter = 0;
				cJournalEntry jEntry;
				while (journ.JournalEntries.IsCurrent())
				{
					counter += 1;
					jEntry = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
					vs1.TextMatrix(counter, 0, FCConvert.ToString(jEntry.ID));
					vs1.TextMatrix(counter, 1, Strings.Format(jEntry.JournalEntriesDate, "MM/dd/yyyy"));
					vs1.TextMatrix(counter, 2, jEntry.Description);
					vs1.TextMatrix(counter, 3, jEntry.Account);
					vs1.TextMatrix(counter, 4, jEntry.Project);
					vs1.TextMatrix(counter, 5, FCConvert.ToString(jEntry.Amount * -1));
					journ.JournalEntries.MoveNext();
				}
				if (journ.JournalEntries.ItemCount() < 15)
				{
					for (counter = journ.JournalEntries.ItemCount() + 1; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 0, "0");
						vs1.TextMatrix(counter, 1, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
						vs1.TextMatrix(counter, 5, "0");
					}
				}
				CalculateTotals();
				lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
				modRegistry.SaveRegistryKey("CURRCRJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCREntry));
				cboJournal.Enabled = false;
			}
			else
			{
				modBudgetaryMaster.Statics.blnCREdit = false;
				ChosenJournal = 0;
				ChosenPeriod = 0;
			}
			this.Show(App.MainForm);
		}
	}
}
