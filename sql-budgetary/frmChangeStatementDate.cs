﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeStatementDate.
	/// </summary>
	public partial class frmChangeStatementDate : BaseForm
	{
		public frmChangeStatementDate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChangeStatementDate InstancePtr
		{
			get
			{
				return (frmChangeStatementDate)Sys.GetInstance(typeof(frmChangeStatementDate));
			}
		}

		protected frmChangeStatementDate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/30/02
		// This form will be used by the customers to select a
		// statement date to use while doing the check rec process
		// ********************************************************
		DateTime datSelectedDate;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdCalendar2_Click(object sender, System.EventArgs e)
		{
			if (!Information.IsDate(txtStatementDate.Text))
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Today);
			}
			else
			{
				frmCalender.InstancePtr.Init(ref datSelectedDate, DateAndTime.DateValue(txtStatementDate.Text));
			}
			txtStatementDate.Text = Strings.Format(datSelectedDate, "MM/dd/yyyy");
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSaveInfo = new clsDRWrapper();
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			int x;
			if (Information.IsDate(txtStatementDate.Text))
			{
				modBudgetaryAccounting.UpdateBDVariable("StatementDate", txtStatementDate.Text);
				rsBankInfo.OpenRecordset("SELECT * FROM Banks WHERE CurrentBank = 1");
				if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
				{
					rsBankInfo.Edit();
					rsBankInfo.Set_Fields("StatementDate", txtStatementDate.Text);
					rsBankInfo.Update();
				}
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Check Rec Change Statement Date", "", "", "");
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				rsSaveInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '1' OR Status = '2'");
				if (rsSaveInfo.EndOfFile() != true && rsSaveInfo.BeginningOfFile() != true)
				{
					do
					{
						if (FCConvert.ToString(rsSaveInfo.Get_Fields_String("Status")) == "1")
						{
							if (rsSaveInfo.Get_Fields_DateTime("CheckDate") <= DateAndTime.DateValue(txtStatementDate.Text))
							{
								if (FCConvert.ToBoolean(rsSaveInfo.Get_Fields_Boolean("DirectDeposit")))
								{
									rsSaveInfo.Edit();
									rsSaveInfo.Set_Fields("Status", "3");
									rsSaveInfo.Set_Fields("StatusDate", DateTime.Today);
									rsSaveInfo.Update(true);
								}
								else
								{
									rsSaveInfo.Edit();
									rsSaveInfo.Set_Fields("Status", "2");
									rsSaveInfo.Set_Fields("StatusDate", DateTime.Today);
									rsSaveInfo.Update(true);
								}
							}
						}
						else
						{
							if (rsSaveInfo.Get_Fields_DateTime("CheckDate") > DateAndTime.DateValue(txtStatementDate.Text))
							{
								rsSaveInfo.Edit();
								rsSaveInfo.Set_Fields("Status", "1");
								rsSaveInfo.Set_Fields("StatusDate", DateTime.Today);
								rsSaveInfo.Update(true);
							}
						}
						rsSaveInfo.MoveNext();
					}
					while (rsSaveInfo.EndOfFile() != true);
				}
				Close();
			}
			else
			{
				MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void frmChangeStatementDate_Activated(object sender, System.EventArgs e)
		{
			string tempDate;
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			tempDate = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("StatementDate"));
			if (tempDate != "" && Information.IsDate(tempDate))
			{
				lblOldDate.Text = Strings.Format(tempDate, "MM/dd/yyyy");
				txtStatementDate.Text = Strings.Format(tempDate, "MM/dd/yyyy");
			}
			else
			{
				lblOldDate.Text = "00/00/00";
			}
			this.Refresh();
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
			FillControlInformationClass();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			//FC:FINAL:DDU:#2997 - set cursor at start of date
			txtStatementDate.SelectionStart = 0;
			// ---------------------------------------------------------------------
		}

		private void frmChangeStatementDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmChangeStatementDate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChangeStatementDate.FillStyle	= 0;
			//frmChangeStatementDate.ScaleWidth	= 3885;
			//frmChangeStatementDate.ScaleHeight	= 2280;
			//frmChangeStatementDate.LinkTopic	= "Form2";
			//frmChangeStatementDate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:DSE #i623 Objects should be created manualy
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStatementDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Statement Date";
			intTotalNumberOfControls += 1;
		}
	}
}
