﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmOutlineRevQuery.
	/// </summary>
	partial class frmOutlineRevQuery : BaseForm
	{
		public fecherFoundation.FCGrid vs1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutlineRevQuery));
            this.vs1 = new fecherFoundation.FCGrid();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.cmdRowsDelete = new fecherFoundation.FCButton();
            this.cmdRowsAdd = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRowsDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRowsAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 597);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 606);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdRowsDelete);
            this.TopPanel.Controls.Add(this.cmdRowsAdd);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRowsAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRowsDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(164, 28);
            this.HeaderText.Text = "Revenue Titles";
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 8;
            this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.vs1.ExtendLastCol = true;
            this.vs1.Location = new System.Drawing.Point(30, 30);
            this.vs1.Name = "vs1";
            this.vs1.Size = new System.Drawing.Size(1028, 546);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 0;
            this.vs1.Visible = false;
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_CellValueChanged);
            this.vs1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vs1_CellFormattingEvent);
            this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_BeforeEdit);
            this.vs1.CellEnter += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_CellEnter);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.SelectionChanged += new System.EventHandler(this.vs1_SelectionChanged);
            this.vs1.CellDoubleClick += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_CellDoubleClick);
            this.vs1.Click += new System.EventHandler(this.vs1_Click);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPress);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(488, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(103, 48);
            this.cmdProcessSave.TabIndex = 2;
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // cmdRowsDelete
            // 
            this.cmdRowsDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRowsDelete.Location = new System.Drawing.Point(993, 29);
            this.cmdRowsDelete.Name = "cmdRowsDelete";
            this.cmdRowsDelete.Size = new System.Drawing.Size(119, 24);
            this.cmdRowsDelete.TabIndex = 11;
            this.cmdRowsDelete.Text = "Delete Revenue";
            this.cmdRowsDelete.Visible = false;
            this.cmdRowsDelete.Click += new System.EventHandler(this.mnuRowsDelete_Click);
            // 
            // cmdRowsAdd
            // 
            this.cmdRowsAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRowsAdd.Location = new System.Drawing.Point(883, 29);
            this.cmdRowsAdd.Name = "cmdRowsAdd";
            this.cmdRowsAdd.Size = new System.Drawing.Size(104, 24);
            this.cmdRowsAdd.TabIndex = 10;
            this.cmdRowsAdd.Text = "Add Revenue";
            this.cmdRowsAdd.Visible = false;
            this.cmdRowsAdd.Click += new System.EventHandler(this.mnuRowsAdd_Click);
            // 
            // frmOutlineRevQuery
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmOutlineRevQuery";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Revenue Titles";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmOutlineRevQuery_Load);
            this.Activated += new System.EventHandler(this.frmOutlineRevQuery_Activated);
            this.Click += new System.EventHandler(this.frmOutlineRevQuery_Click);
            this.Resize += new System.EventHandler(this.frmOutlineRevQuery_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOutlineRevQuery_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRowsDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRowsAdd)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
		public FCButton cmdRowsDelete;
		public FCButton cmdRowsAdd;
	}
}
