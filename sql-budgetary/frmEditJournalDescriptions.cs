﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEditJournalDescriptions.
	/// </summary>
	public partial class frmEditJournalDescriptions : BaseForm
	{
		public frmEditJournalDescriptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditJournalDescriptions InstancePtr
		{
			get
			{
				return (frmEditJournalDescriptions)Sys.GetInstance(typeof(frmEditJournalDescriptions));
			}
		}

		protected frmEditJournalDescriptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int intJournalCol;
		int intDescriptionCol;
		int intOriginalDescriptionCol;
		clsDRWrapper rsTitles = new clsDRWrapper();

		private void frmEditJournalDescriptions_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmEditJournalDescriptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditJournalDescriptions.FillStyle	= 0;
			//frmEditJournalDescriptions.ScaleWidth	= 9045;
			//frmEditJournalDescriptions.ScaleHeight	= 7290;
			//frmEditJournalDescriptions.LinkTopic	= "Form2";
			//frmEditJournalDescriptions.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			intJournalCol = 0;
			intOriginalDescriptionCol = 1;
			intDescriptionCol = 2;
			vsJournals.TextMatrix(0, intJournalCol, "Journal Number");
			vsJournals.TextMatrix(0, intDescriptionCol, "Description");
			vsJournals.ColAlignment(intJournalCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsJournals.ColHidden(intOriginalDescriptionCol, true);
			vsJournals.ColWidth(intJournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.3));
			vsJournals.ColWidth(intDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.6));
			vsJournals.EditMaxLength = 100;
			rsTitles.OpenRecordset("SELECT DISTINCT JournalNumber, Description FROM JournalMaster WHERE Status <> 'D' and Status <> 'P' ORDER BY JournalNumber, Description");
			if (rsTitles.EndOfFile() != true && rsTitles.BeginningOfFile() != true)
			{
				do
				{
					// 
					vsJournals.Rows += 1;
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vsJournals.TextMatrix(vsJournals.Rows - 1, intJournalCol, Strings.Format(rsTitles.Get_Fields("JournalNumber"), "0000"));
					vsJournals.TextMatrix(vsJournals.Rows - 1, intDescriptionCol, FCConvert.ToString(rsTitles.Get_Fields_String("Description")));
					vsJournals.TextMatrix(vsJournals.Rows - 1, intOriginalDescriptionCol, FCConvert.ToString(rsTitles.Get_Fields_String("Description")));
					rsTitles.MoveNext();
				}
				while (rsTitles.EndOfFile() != true);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmEditJournalDescriptions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEditJournalDescriptions_Resize(object sender, System.EventArgs e)
		{
			vsJournals.ColWidth(intJournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.3));
			vsJournals.ColWidth(intDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.6));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			this.Focus();
			if (SaveData())
			{
				//FC:FINAL:DDU:#2883 - don't close form when save is done and show message box when finished
				MessageBox.Show("Save successful!", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Close();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			int counter;
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (Strings.Trim(vsJournals.TextMatrix(counter, intDescriptionCol)) == "" && Strings.Trim(vsJournals.TextMatrix(counter, intDescriptionCol)) != Strings.Trim(vsJournals.TextMatrix(counter, intOriginalDescriptionCol)))
				{
					MessageBox.Show("You must have a description entered for each journal before you may continue.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveData = false;
					vsJournals.TopRow = counter;
					vsJournals.Select(counter, intDescriptionCol);
					return SaveData;
				}
			}
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (Strings.Trim(vsJournals.TextMatrix(counter, intDescriptionCol)) != Strings.Trim(vsJournals.TextMatrix(counter, intOriginalDescriptionCol)))
				{
					rsUpdate.Execute("UPDATE JournalMaster SET Description = '" + Strings.Trim(vsJournals.TextMatrix(counter, intDescriptionCol)) + "' WHERE JournalNumber = " + vsJournals.TextMatrix(counter, intJournalCol), "TWBD0000.vb1");
				}
			}
			SaveData = true;
			return SaveData;
		}

		private void vsJournals_RowColChange(object sender, System.EventArgs e)
		{
			if (vsJournals.Col == intDescriptionCol && vsJournals.Row > 0)
			{
				vsJournals.EditCell();
			}
		}
	}
}
