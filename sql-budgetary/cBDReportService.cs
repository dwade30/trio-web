﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWBD0000
{
	public class cBDReportService
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController bactController = new cBDAccountController();
		private cBDAccountController bactController_AutoInitialized;

		private cBDAccountController bactController
		{
			get
			{
				if (bactController_AutoInitialized == null)
				{
					bactController_AutoInitialized = new cBDAccountController();
				}
				return bactController_AutoInitialized;
			}
			set
			{
				bactController_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cJournalController journController = new cJournalController();
		private cJournalController journController_AutoInitialized;

		private cJournalController journController
		{
			get
			{
				if (journController_AutoInitialized == null)
				{
					journController_AutoInitialized = new cJournalController();
				}
				return journController_AutoInitialized;
			}
			set
			{
				journController_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cAPJournalController apjournController = new cAPJournalController();
		private cAPJournalController apjournController_AutoInitialized;

		private cAPJournalController apjournController
		{
			get
			{
				if (apjournController_AutoInitialized == null)
				{
					apjournController_AutoInitialized = new cAPJournalController();
				}
				return apjournController_AutoInitialized;
			}
			set
			{
				apjournController_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cEncumbranceController enController = new cEncumbranceController();
		private cEncumbranceController enController_AutoInitialized;

		private cEncumbranceController enController
		{
			get
			{
				if (enController_AutoInitialized == null)
				{
					enController_AutoInitialized = new cEncumbranceController();
				}
				return enController_AutoInitialized;
			}
			set
			{
				enController_AutoInitialized = value;
			}
		}

		private string strLastError = "";
		private int lngLastError;
		private cBDSettings bdSettings;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDSettingsController bdSetCont = new cBDSettingsController();
		private cBDSettingsController bdSetCont_AutoInitialized;

		private cBDSettingsController bdSetCont
		{
			get
			{
				if (bdSetCont_AutoInitialized == null)
				{
					bdSetCont_AutoInitialized = new cBDSettingsController();
				}
				return bdSetCont_AutoInitialized;
			}
			set
			{
				bdSetCont_AutoInitialized = value;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public void ShowTrialBalanceReport(ref cTrialBalanceReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				//frmBusy bbusy = new frmBusy();
				//bbusy.StartBusy();
				//bbusy.BringToFront();
				//bbusy.Show(FCForm.FormShowEnum.Modeless);
				FillTrialBalanceReport(ref theReport);
				//bbusy.StopBusy();
				//bbusy.Unload();
				/*- bbusy = null; */
				if (theReport.Funds.ItemCount() > 0)
				{
					if (!HadError)
					{
						rptTrialBalance.InstancePtr.Init(ref theReport);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void ExportTrialBalanceCSV(ref cTrialBalanceReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				//frmBusy bbusy = new frmBusy();
				//bbusy.StartBusy();
				//bbusy.BringToFront();
				//bbusy.Show(FCForm.FormShowEnum.Modeless);
				FillTrialBalanceReport(ref theReport);
				//bbusy.StopBusy();
				//bbusy.Unload();
				/*- bbusy = null; */
				if (theReport.Funds.ItemCount() > 0)
				{
					if (!HadError)
					{
						// Call rptTrialBalance.Init(theReport)
						cTrialBalanceController tbc = new cTrialBalanceController();
						tbc.FormatType = 0;
						// csv
						frmReportDataExport rdeForm = new frmReportDataExport("Trial Balance");
						cReportExportView reView = new cReportExportView();
						reView.SetReport(theReport);
						rdeForm.SetViewModel(ref reView);
						reView.SetExporter(tbc);
						rdeForm.Show(FCForm.FormShowEnum.Modal);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void ShowAccountInquiryReport(ref cBDAccountInquiryReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				rptBDAccountInquiry.InstancePtr.Init(ref theReport);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void ShowAccountDetailReport(ref cBDAccountDetailReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				rptBDAccountDetail.InstancePtr.Init(ref theReport);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void FillTrialBalanceReport(ref cTrialBalanceReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection collFunds;
				bool boolPeriodRange = false;
				int intFirstPeriod;
				int intLastPeriod = 0;
				intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				if (intFirstPeriod == 1)
				{
					intLastPeriod = 12;
				}
				else
				{
					intLastPeriod = intFirstPeriod - 1;
				}
				if (theReport.StartPeriod > 0 && theReport.StartPeriod != intFirstPeriod)
				{
					boolPeriodRange = true;
				}
				if (theReport.EndPeriod > 0 && theReport.EndPeriod != intLastPeriod)
				{
					boolPeriodRange = true;
				}
				theReport.Funds.ClearList();
				if (Conversion.Val(theReport.EndingFund) > 0)
				{
					collFunds = bactController.GetFundsByFundRange(theReport.BeginningFund, theReport.EndingFund);
				}
				else
				{
					collFunds = bactController.GetFunds();
				}
				if (bactController.HadError)
				{
					SetError(bactController.LastErrorNumber, bactController.LastErrorMessage);
					return;
				}
				cFund tFund;
				cAccountReportGroup fundGroup;
				cAccountReportGroup expenseGroup;
				cAccountReportGroup revenueGroup;
				cAccountReportGroup assetGroup;
				cAccountReportGroup liabilityGroup;
				cAccountReportGroup fundbalanceGroup;
				cAccountReportGroup zeroFundGroup = new cAccountReportGroup();
				cGenericCollection listSummaries;
				cAccountReportGroup zerofundExpenses = new cAccountReportGroup();
				cAccountReportGroup zerofundRevenues = new cAccountReportGroup();
				cAccountReportGroup zerofundAssets = new cAccountReportGroup();
				cAccountReportGroup zerofundLiabilities = new cAccountReportGroup();
				cAccountReportGroup zerofundFundBalances = new cAccountReportGroup();
				cAccountReportGroup zerofundUnknowns = new cAccountReportGroup();
				listSummaries = GetAccountSummaries(false, true);
				if (HadError)
				{
					return;
				}
				zerofundExpenses.GroupValue = "Expenses0";
				zerofundExpenses.Description = "Expenses";
				zerofundRevenues.GroupValue = "Revenue0";
				zerofundRevenues.Description = "Revenues";
				zerofundAssets.GroupValue = "Asset0";
				zerofundAssets.Description = "Assets";
				zerofundLiabilities.GroupValue = "Liability0";
				zerofundLiabilities.Description = "Liabilities";
				zerofundFundBalances.GroupValue = "FundBalance0";
				zerofundFundBalances.Description = "Fund Balances";
				zerofundUnknowns.GroupValue = "Unknown0";
				zerofundUnknowns.Description = "Invalid Accounts";
				zeroFundGroup.GroupValue = "0";
				zeroFundGroup.Description = "Unknown";
				cAccountSummaryItem currSummaryItem;
				collFunds.MoveFirst();
				while (collFunds.IsCurrent())
				{
					//Application.DoEvents();
					tFund = (cFund)collFunds.GetCurrentItem();
					fundGroup = new cAccountReportGroup();
					fundGroup.Description = tFund.Description;
					fundGroup.GroupValue = tFund.Fund;
					expenseGroup = new cAccountReportGroup();
					expenseGroup.GroupValue = "Expenses" + tFund.Fund;
					expenseGroup.Description = "Expense";
					revenueGroup = new cAccountReportGroup();
					revenueGroup.GroupValue = "Revenue" + tFund.Fund;
					revenueGroup.Description = "Revenues";
					assetGroup = new cAccountReportGroup();
					assetGroup.GroupValue = "Asset" + tFund.Fund;
					assetGroup.Description = "Assets";
					liabilityGroup = new cAccountReportGroup();
					liabilityGroup.GroupValue = "Liability" + tFund.Fund;
					liabilityGroup.Description = "Liabilities";
					fundbalanceGroup = new cAccountReportGroup();
					fundbalanceGroup.GroupValue = "FundBalance" + tFund.Fund;
					fundbalanceGroup.Description = "Fund Balances";
					listSummaries.MoveFirst();
					while (listSummaries.IsCurrent())
					{
						//Application.DoEvents();
						currSummaryItem = (cAccountSummaryItem)listSummaries.GetCurrentItem();
						// If currSummaryItem.IsExpense Or currSummaryItem.IsRevenue Then
						// currSummaryItem.BeginningBalance = 0
						// currSummaryItem.BeginningNet = 0
						// currSummaryItem.BudgetAdjustment = 0
						// End If
						if (boolPeriodRange)
						{
							currSummaryItem.SetTotalsByPeriodRange(theReport.StartPeriod, theReport.EndPeriod);
						}
						if (currSummaryItem.Fund == tFund.Fund)
						{
							if (theReport.IncludeAccountsWithNoActivity || currSummaryItem.BeginningNet != 0 || currSummaryItem.Debits != 0 || currSummaryItem.Credits != 0)
							{
								if (currSummaryItem.IsExpense)
								{
									expenseGroup.Accounts.AddItem(currSummaryItem);
									expenseGroup.Credits += currSummaryItem.Credits;
									expenseGroup.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsRevenue)
								{
									revenueGroup.Accounts.AddItem(currSummaryItem);
									revenueGroup.Credits += currSummaryItem.Credits;
									revenueGroup.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsAsset)
								{
									assetGroup.Accounts.AddItem(currSummaryItem);
									assetGroup.Credits += currSummaryItem.Credits;
									assetGroup.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsLiability)
								{
									liabilityGroup.Accounts.AddItem(currSummaryItem);
									liabilityGroup.Credits += currSummaryItem.Credits;
									liabilityGroup.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsFundBalance)
								{
									fundbalanceGroup.Accounts.AddItem(currSummaryItem);
									fundbalanceGroup.Credits += currSummaryItem.Credits;
									fundbalanceGroup.Debits += currSummaryItem.Debits;
								}
							}
							listSummaries.RemoveCurrent();
						}
						//else if (currSummaryItem.Fund == "")
						// FC:FINAL:VGE - #661 'Fund' can be NULL. Condition replaced with 'IsNullOrEmpty'
						else if (string.IsNullOrEmpty(currSummaryItem.Fund))
						{
							if ((currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment) != 0 || currSummaryItem.Credits != 0 || currSummaryItem.Debits != 0)
							{
								if (currSummaryItem.IsExpense)
								{
									zerofundExpenses.Accounts.AddItem(currSummaryItem);
									zerofundExpenses.Credits += currSummaryItem.Credits;
									zerofundExpenses.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsRevenue)
								{
									zerofundRevenues.Accounts.AddItem(currSummaryItem);
									zerofundRevenues.Credits += currSummaryItem.Credits;
									zerofundRevenues.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsAsset)
								{
									zerofundAssets.Accounts.AddItem(currSummaryItem);
									zerofundAssets.Credits += currSummaryItem.Credits;
									zerofundAssets.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsLiability)
								{
									zerofundLiabilities.Accounts.AddItem(currSummaryItem);
									zerofundLiabilities.Credits += currSummaryItem.Credits;
									zerofundLiabilities.Debits += currSummaryItem.Debits;
								}
								else if (currSummaryItem.IsFundBalance)
								{
									zerofundFundBalances.Accounts.AddItem(currSummaryItem);
									zerofundFundBalances.Credits += currSummaryItem.Credits;
									zerofundFundBalances.Debits += currSummaryItem.Debits;
								}
								else
								{
									zerofundUnknowns.Accounts.AddItem(currSummaryItem);
									zerofundUnknowns.Credits += currSummaryItem.Credits;
									zerofundUnknowns.Debits += currSummaryItem.Debits;
								}
							}
							listSummaries.RemoveCurrent();
						}
						listSummaries.MoveNext();
					}
					if (expenseGroup.Accounts.ItemCount() > 0)
					{
						fundGroup.Accounts.AddItem(expenseGroup);
					}
					if (revenueGroup.Accounts.ItemCount() > 0)
					{
						fundGroup.Accounts.AddItem(revenueGroup);
					}
					if (assetGroup.Accounts.ItemCount() > 0)
					{
						fundGroup.Accounts.AddItem(assetGroup);
					}
					if (liabilityGroup.Accounts.ItemCount() > 0)
					{
						fundGroup.Accounts.AddItem(liabilityGroup);
					}
					if (fundbalanceGroup.Accounts.ItemCount() > 0)
					{
						fundGroup.Accounts.AddItem(fundbalanceGroup);
					}
					if (fundGroup.Accounts.ItemCount() > 0)
					{
						theReport.Funds.AddItem(fundGroup);
					}
					collFunds.MoveNext();
				}
				if (zerofundExpenses.Accounts.ItemCount() > 0)
				{
					zeroFundGroup.Accounts.AddItem(zerofundExpenses);
				}
				if (zerofundRevenues.Accounts.ItemCount() > 0)
				{
					zeroFundGroup.Accounts.AddItem(zerofundRevenues);
				}
				if (zerofundAssets.Accounts.ItemCount() > 0)
				{
					zeroFundGroup.Accounts.AddItem(zerofundAssets);
				}
				if (zerofundLiabilities.Accounts.ItemCount() > 0)
				{
					zeroFundGroup.Accounts.AddItem(zerofundLiabilities);
				}
				if (zerofundFundBalances.Accounts.ItemCount() > 0)
				{
					zeroFundGroup.Accounts.AddItem(zerofundFundBalances);
				}
				if (zerofundUnknowns.Accounts.ItemCount() > 0)
				{
					zeroFundGroup.Accounts.AddItem(zerofundUnknowns);
				}
				if (zeroFundGroup.Accounts.ItemCount() > 0)
				{
					theReport.Funds.MoveFirst();
					theReport.Funds.InsertItemBefore(zeroFundGroup);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}
		// Public Sub FillTrialBalanceReport(ByRef theReport As cTrialBalanceReport)
		// ClearErrors
		// On Error GoTo ErrorHandler
		// Dim collFunds As cGenericCollection
		// Dim intFirstPeriod As Integer
		// intFirstPeriod = Val(GetBDVariable("FiscalStart"))
		// theReport.Funds.ClearList
		// If theReport.EndingFund > 0 Then
		// Set collFunds = bactController.GetFundsByFundRange(theReport.BeginningFund, theReport.EndingFund)
		// Else
		// Set collFunds = bactController.GetFunds
		// End If
		// Dim tFund As cFund
		// Dim fundGroup As cAccountReportGroup
		// Dim fundAccounts As cGenericCollection
		// Dim fundRG As cGLFundReportGroup
		// Dim repItem As cAccountReportItem
		// Dim strAccount As String
		// Dim aRepGroup As cAccountReportGroup
		// Dim anAccount As cBDAccount
		// Dim aRev As cRevTitle
		// Dim accountsWithActivity As cERGDictionaryGroup
		// Set accountsWithActivity = GetAccountsWithActivity(theReport.StartPeriod, theReport.EndPeriod, False)
		// collFunds.MoveFirst
		// Dim aTitle As cBDAccountTitle
		// Do While collFunds.IsCurrent
		// DoEvents
		// Set tFund = collFunds.GetCurrentItem
		// Set fundGroup = New cAccountReportGroup
		// fundGroup.Description = tFund.Description
		// fundGroup.GroupValue = tFund.Fund
		//
		// Set aRepGroup = New cAccountReportGroup
		// aRepGroup.Description = "Expenses"
		// aRepGroup.GroupValue = "Expenses"
		// Set fundAccounts = bactController.GetExpenseAccountsByFund(tFund.Fund)
		// fundAccounts.MoveFirst
		// Do While fundAccounts.IsCurrent
		// DoEvents
		// Set anAccount = fundAccounts.GetCurrentItem
		// strAccount = anAccount.Account
		// Set repItem = Nothing
		// If accountsWithActivity.Expenses.Exists(strAccount) Then
		// Set repItem = accountsWithActivity.Expenses[strAccount]
		// End If
		// If Not repItem Is Nothing Or theReport.IncludeAccountsWithNoActivity Then
		// If repItem Is Nothing Then
		// Set repItem = New cAccountReportItem
		// repItem.Account = strAccount
		// End If
		// repItem.Description = anAccount.Description
		// aRepGroup.Credits = aRepGroup.Credits + repItem.Credits
		// aRepGroup.Debits = aRepGroup.Debits + repItem.Debits
		// Call aRepGroup.Accounts.AddItem(repItem)
		// End If
		//
		// fundAccounts.MoveNext
		// Loop
		// Call fundGroup.Accounts.AddItem(aRepGroup)
		//
		// Set aRepGroup = New cAccountReportGroup
		// aRepGroup.Description = "Revenues"
		// aRepGroup.GroupValue = "Revenues"
		// Set fundAccounts = bactController.GetRevenueAccountTitlesByFund(tFund.Fund)
		// fundAccounts.MoveFirst
		// Do While fundAccounts.IsCurrent
		// DoEvents
		// Set aRev = fundAccounts.GetCurrentItem
		// strAccount = "R " & aRev.Department
		// If Val(aRev.Division) > 0 Then
		// strAccount = strAccount & "-" & aRev.Division
		// End If
		// strAccount = strAccount & "-" & aRev.Revenue
		// Set repItem = Nothing
		// If accountsWithActivity.Revenues.Exists(strAccount) Then
		// Set repItem = accountsWithActivity.Revenues[strAccount]
		// End If
		// If Not repItem Is Nothing Or theReport.IncludeAccountsWithNoActivity Then
		// If repItem Is Nothing Then
		// Set repItem = New cAccountReportItem
		// repItem.Account = strAccount
		// End If
		// repItem.Description = aRev.LongDescription
		// aRepGroup.Credits = aRepGroup.Credits + repItem.Credits
		// aRepGroup.Debits = aRepGroup.Debits + repItem.Debits
		// Call aRepGroup.Accounts.AddItem(repItem)
		// End If
		// fundAccounts.MoveNext
		// Loop
		// Call fundGroup.Accounts.AddItem(aRepGroup)
		//
		//
		// Set aRepGroup = New cAccountReportGroup
		// aRepGroup.Description = "Assets"
		// aRepGroup.GroupValue = "Assets"
		// Set fundAccounts = bactController.GetAssetLedgerTitlesByFund(tFund.Fund)
		// fundAccounts.MoveFirst
		//
		// Do While fundAccounts.IsCurrent
		// DoEvents
		// Set aTitle = fundAccounts.GetCurrentItem
		// strAccount = "G " & aTitle.Fund & "-" & aTitle.Account
		// If Trim(aTitle.Suffix) <> "" Then
		// strAccount = strAccount & "-" & aTitle.Suffix
		// End If
		// Set repItem = Nothing
		// If accountsWithActivity.GeneralLedgers.Exists(strAccount) Then
		// Set repItem = accountsWithActivity.GeneralLedgers[strAccount]
		// End If
		// If Not repItem Is Nothing Or theReport.IncludeAccountsWithNoActivity Then
		// If repItem Is Nothing Then
		// Set repItem = New cAccountReportItem
		// repItem.Account = strAccount
		// End If
		// repItem.Description = aTitle.Description
		// aRepGroup.Credits = aRepGroup.Credits + repItem.Credits
		// aRepGroup.Debits = aRepGroup.Debits + repItem.Debits
		// figure out beginning balance etc.
		// Call aRepGroup.Accounts.AddItem(repItem)
		// End If
		//
		// fundAccounts.MoveNext
		// Loop
		// fundGroup.Credits = fundGroup.Credits + aRepGroup.Credits
		// fundGroup.Debits = fundGroup.Debits + aRepGroup.Debits
		// Call fundGroup.Accounts.AddItem(aRepGroup)
		//
		// Set aRepGroup = New cAccountReportGroup
		// aRepGroup.Description = "Liabilities"
		// aRepGroup.GroupValue = "Liabilities"
		// Set fundAccounts = bactController.GetLiabilityLedgerTitlesByFund(tFund.Fund)
		// fundAccounts.MoveFirst
		// Do While fundAccounts.IsCurrent
		// DoEvents
		// Set aTitle = fundAccounts.GetCurrentItem
		// strAccount = "G " & aTitle.Fund & "-" & aTitle.Account
		// If Trim(aTitle.Suffix) <> "" Then
		// strAccount = strAccount & "-" & aTitle.Suffix
		// End If
		// Set repItem = Nothing
		// If accountsWithActivity.GeneralLedgers.Exists(strAccount) Then
		// Set repItem = accountsWithActivity.GeneralLedgers[strAccount]
		// End If
		// If Not repItem Is Nothing Or theReport.IncludeAccountsWithNoActivity Then
		// If repItem Is Nothing Then
		// Set repItem = New cAccountReportItem
		// repItem.Account = strAccount
		// End If
		// repItem.Description = aTitle.Description
		// aRepGroup.Credits = aRepGroup.Credits + repItem.Credits
		// aRepGroup.Debits = aRepGroup.Debits + repItem.Debits
		// Call aRepGroup.Accounts.AddItem(repItem)
		// End If
		// fundAccounts.MoveNext
		// Loop
		// fundGroup.Credits = fundGroup.Credits + aRepGroup.Credits
		// fundGroup.Debits = fundGroup.Debits + aRepGroup.Debits
		// Call fundGroup.Accounts.AddItem(aRepGroup)
		//
		// Set aRepGroup = New cAccountReportGroup
		// aRepGroup.Description = "Fund Balance"
		// aRepGroup.GroupValue = "FundBalance"
		// Set fundAccounts = bactController.GetFundBalanceLedgerTitlesByFund(tFund.Fund)
		// fundAccounts.MoveFirst
		// Do While fundAccounts.IsCurrent
		// DoEvents
		// Set aTitle = fundAccounts.GetCurrentItem
		// strAccount = "G " & aTitle.Fund & "-" & aTitle.Account
		// If Trim(aTitle.Suffix) <> "" Then
		// strAccount = strAccount & "-" & aTitle.Suffix
		// End If
		// Set repItem = Nothing
		// If accountsWithActivity.GeneralLedgers.Exists(strAccount) Then
		// Set repItem = accountsWithActivity.GeneralLedgers[strAccount]
		// End If
		// If Not repItem Is Nothing Or theReport.IncludeAccountsWithNoActivity Then
		// If repItem Is Nothing Then
		// Set repItem = New cAccountReportItem
		// repItem.Account = strAccount
		// End If
		// repItem.Description = aTitle.Description
		// aRepGroup.Credits = aRepGroup.Credits + repItem.Credits
		// aRepGroup.Debits = aRepGroup.Debits + repItem.Debits
		// Call aRepGroup.Accounts.AddItem(repItem)
		// End If
		// fundAccounts.MoveNext
		// Loop
		// fundGroup.Credits = fundGroup.Credits + aRepGroup.Credits
		// fundGroup.Debits = fundGroup.Debits + aRepGroup.Debits
		// Call fundGroup.Accounts.AddItem(aRepGroup)
		//
		// Call theReport.Funds.AddItem(fundGroup)
		// collFunds.MoveNext
		// Loop
		//
		//
		// Exit Sub
		// ErrorHandler:
		// Call SetError(Err.Number, Err.Description)
		// End Sub
		// vbPorter upgrade warning: boolIncludePending As Variant --> As bool
		public cGenericCollection GetAccountSummaries(bool boolIncludePending, bool boolExcludeBudgets)
		{
			cGenericCollection GetAccountSummaries = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				Dictionary<object, object> dAccounts = new Dictionary<object, object>();
				cGenericCollection collJournals;
				int intFirstPeriod;
				int intStartPeriod;
				int intEndPeriod = 0;
				int intLastPeriod;
				cJournal journ;
				cAPJournal aJourn;
				cGenericCollection listapJournals;
				double dblCredit;
				double dblDebit;
				cAPJournalDetail currAPDetail;
				cJournalEntry currJEntry;
				// vbPorter upgrade warning: currSummaryItem As cAccountSummaryItem	OnWrite(cAccountSummaryItem, object)
				cAccountSummaryItem currSummaryItem;
				cGenericCollection listSummaries = new cGenericCollection();
				Dictionary<object, object> dictSummaries = new Dictionary<object, object>();
				cGenericCollection listDeptDivs;
				bool boolExpensesUseDivision;
				bool boolRevenuesUseDivision;
				cGenericCollection listRevTitles;
				cGenericCollection listLedgerTitles;
				cGenericCollection listExpObjs;
				bool boolUseObject;
				int intCurrentPeriod = 0;
				boolUseObject = !modAccountTitle.Statics.ObjFlag;
				boolExpensesUseDivision = false;
				boolRevenuesUseDivision = false;
				if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) > 0)
				{
					boolExpensesUseDivision = true;
				}
				if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) > 0)
				{
					boolRevenuesUseDivision = true;
				}
				intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				intStartPeriod = intFirstPeriod;
				if (intStartPeriod == 1)
				{
					intEndPeriod = 12;
				}
				else
				{
					intEndPeriod = intStartPeriod - 1;
				}
				intLastPeriod = intEndPeriod;
				dAccounts = bactController.GetAccountsDictionary();
				if (bactController.HadError)
				{
					SetError(bactController.LastErrorNumber, bactController.LastErrorMessage);
					return GetAccountSummaries;
				}
				collJournals = journController.GetFullJournalsByPeriod(intStartPeriod, intEndPeriod, boolIncludePending);
				if (journController.HadError)
				{
					SetError(journController.LastErrorNumber, journController.LastErrorMessage);
					return GetAccountSummaries;
				}
				collJournals.MoveFirst();
				while (collJournals.IsCurrent())
				{
					//Application.DoEvents();
					journ = (cJournal)collJournals.GetCurrentItem();
					intCurrentPeriod = journ.Period;
					if ((Strings.LCase(journ.JournalType) == "ac") || (Strings.LCase(journ.JournalType) == "ap"))
					{
						listapJournals = apjournController.GetFullAPJournalsByJournalNumber(journ.JournalNumber);
						if (apjournController.HadError)
						{
							SetError(apjournController.LastErrorNumber, apjournController.LastErrorMessage);
							return GetAccountSummaries;
						}
						listapJournals.MoveFirst();
						while (listapJournals.IsCurrent())
						{
							//Application.DoEvents();
							aJourn = (cAPJournal)listapJournals.GetCurrentItem();
							aJourn.JournalEntries.MoveFirst();
							while (aJourn.JournalEntries.IsCurrent())
							{
								//Application.DoEvents();
								currAPDetail = (cAPJournalDetail)aJourn.JournalEntries.GetCurrentItem();
								dblCredit = 0;
								dblDebit = 0;
								if (!dictSummaries.ContainsKey(currAPDetail.Account))
								{
									currSummaryItem = new cAccountSummaryItem();
									currSummaryItem.Account = currAPDetail.Account;
									dictSummaries.Add(currSummaryItem.Account, currSummaryItem);
								}
								else
								{
									currSummaryItem = (cAccountSummaryItem)dictSummaries[currAPDetail.Account];
								}
								if (Strings.LCase(currAPDetail.RCB) != "e")
								{
									if (currAPDetail.IsBudgetAdjustment())
									{
										currSummaryItem.BudgetAdjustment += currAPDetail.Amount;
									}
									else if (currAPDetail.IsCredit())
									{
										if (!currAPDetail.IsCorrection())
										{
											currSummaryItem.Credits += Math.Abs(currAPDetail.Amount);
											currSummaryItem.AppendCreditsForPeriod(intCurrentPeriod, Math.Abs(currAPDetail.Amount));
										}
										else
										{
											currSummaryItem.Credits -= Math.Abs(currAPDetail.Amount);
											currSummaryItem.AppendCreditsForPeriod(intCurrentPeriod, -Math.Abs(currAPDetail.Amount));
										}
									}
									else
									{
										if (!currAPDetail.IsCorrection())
										{
											currSummaryItem.Debits += Math.Abs(currAPDetail.Amount - currAPDetail.Discount);
											currSummaryItem.AppendDebitsForPeriod(intCurrentPeriod, Math.Abs(currAPDetail.Amount - currAPDetail.Discount));
										}
										else
										{
											currSummaryItem.Debits -= Math.Abs(currAPDetail.Amount - currAPDetail.Discount);
											currSummaryItem.AppendDebitsForPeriod(intCurrentPeriod, -Math.Abs(currAPDetail.Amount - currAPDetail.Discount));
										}
									}
								}
								aJourn.JournalEntries.MoveNext();
							}
							listapJournals.MoveNext();
						}
					}
					else
					{
						journ.JournalEntries.MoveFirst();
						while (journ.JournalEntries.IsCurrent())
						{
							//Application.DoEvents();
							currJEntry = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
							if (!dictSummaries.ContainsKey(currJEntry.Account))
							{
								currSummaryItem = new cAccountSummaryItem();
								currSummaryItem.Account = currJEntry.Account;
								dictSummaries.Add(currSummaryItem.Account, currSummaryItem);
							}
							else
							{
								currSummaryItem = (cAccountSummaryItem)dictSummaries[currJEntry.Account];
							}
							if (Strings.LCase(currJEntry.RCB) != "e")
							{
								if (currJEntry.IsBudgetAdjustment())
								{
									currSummaryItem.BudgetAdjustment += currJEntry.Amount;
								}
								else if (currJEntry.IsCredit())
								{
									if (!currJEntry.IsCorrection())
									{
										currSummaryItem.Credits += Math.Abs(currJEntry.Amount);
										currSummaryItem.AppendCreditsForPeriod(intCurrentPeriod, Math.Abs(currJEntry.Amount));
									}
									else
									{
										currSummaryItem.Credits -= Math.Abs(currJEntry.Amount);
										currSummaryItem.AppendCreditsForPeriod(intCurrentPeriod, -Math.Abs(currJEntry.Amount));
									}
								}
								else
								{
									if (!currJEntry.IsCorrection())
									{
										currSummaryItem.Debits += Math.Abs(currJEntry.Amount);
										currSummaryItem.AppendDebitsForPeriod(intCurrentPeriod, Math.Abs(currJEntry.Amount));
									}
									else
									{
										currSummaryItem.Debits -= Math.Abs(currJEntry.Amount);
										currSummaryItem.AppendDebitsForPeriod(intCurrentPeriod, -Math.Abs(currJEntry.Amount));
									}
								}
							}
							else
							{
								if (currJEntry.Amount < 0 && Strings.LCase(currJEntry.JournalType) == "p")
								{
									currSummaryItem.Debits += Math.Abs(currJEntry.Amount);
									currSummaryItem.AppendDebitsForPeriod(intCurrentPeriod, Math.Abs(currJEntry.Amount));
								}
							}
							journ.JournalEntries.MoveNext();
						}
					}
					collJournals.MoveNext();
				}
				int lngCount;
				int lngMax;
				// vbPorter upgrade warning: accts As object	OnRead(cBDAccount, cAccountSummaryItem)
				//FC:FINAL:ASZ: object accts;
				Dictionary<object, object>.ValueCollection accts;
				cBDAccount currAccount;
				cDeptDivTitle currDept;
				cDeptDivTitle CurrDivision;
				cDeptDivTitle currDeptDiv;
				cRevTitle rTitle;
				cExpenseObjectTitle eObTitle;
				string strExpense = "";
				string strObject = "";
				string strExpenseDescription = "";
				string strObjectDescription = "";
				string strShortExpDescription = "";
				string strShortObjectDescription = "";
				cBDAccountTitle lTitle;
				int tempCount;
				listLedgerTitles = bactController.GetLedgerTitles();
				if (bactController.HadError)
				{
					SetError(bactController.LastErrorNumber, bactController.LastErrorMessage);
					return GetAccountSummaries;
				}
				listRevTitles = bactController.GetRevenueTitles();
				if (bactController.HadError)
				{
					SetError(bactController.LastErrorNumber, bactController.LastErrorMessage);
					return GetAccountSummaries;
				}
				listDeptDivs = bactController.GetDeptDivs();
				if (bactController.HadError)
				{
					SetError(bactController.LastErrorNumber, bactController.LastErrorMessage);
					return GetAccountSummaries;
				}
				listExpObjs = bactController.GetExpenseObjectTitles();
				if (bactController.HadError)
				{
					SetError(bactController.LastErrorNumber, bactController.LastErrorMessage);
					return GetAccountSummaries;
				}
				lngMax = dAccounts.Count - 1;
				accts = dAccounts.Values;
				foreach (object obj in accts)
				{
					//Application.DoEvents();                    
					currAccount = (cBDAccount)obj;
					if (dictSummaries.ContainsKey(currAccount.Account))
					{
						currSummaryItem = (cAccountSummaryItem)dictSummaries[currAccount.Account];
					}
					else
					{
						currSummaryItem = new cAccountSummaryItem();
						currSummaryItem.Account = currAccount.Account;
						dictSummaries.Add(currSummaryItem.Account, currSummaryItem);
					}
					currSummaryItem.AccountType = Strings.UCase(currAccount.AccountType);
					currSummaryItem.Net = currSummaryItem.Debits - currSummaryItem.Credits;
					for (tempCount = 1; tempCount <= 12; tempCount++)
					{
						currSummaryItem.SetNetForPeriod(tempCount, currSummaryItem.GetDebitsForPeriod(tempCount) - currSummaryItem.GetCreditsForPeriod(tempCount));
					}
					if (currSummaryItem.AccountType == "G")
					{
						if (currAccount.IsAsset())
						{
							currSummaryItem.AccountType = "A";
						}
						else if (currAccount.IsLiability())
						{
							currSummaryItem.AccountType = "L";
						}
						else
						{
							currSummaryItem.AccountType = "F";
						}
					}
					currSummaryItem.SetSegment(1, Strings.Trim(currAccount.FirstAccountField));
					currSummaryItem.SetSegment(2, Strings.Trim(currAccount.SecondAccountField));
					currSummaryItem.SetSegment(3, Strings.Trim(currAccount.ThirdAccountField));
					currSummaryItem.SetSegment(4, Strings.Trim(currAccount.FourthAccountField));
					if (boolExcludeBudgets && (currAccount.IsExpense() || currAccount.IsRevenue()))
					{
						currSummaryItem.BudgetAdjustment = 0;
						currSummaryItem.BeginningBalance = 0;
					}
					else
					{
						currSummaryItem.BeginningBalance = currAccount.CurrentBudget;
						currSummaryItem.BeginningNet = currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment;
						currSummaryItem.SetNetBeginForPeriod(intFirstPeriod, currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment);
					}
					if (!currAccount.IsExpense())
					{
						currSummaryItem.EndBalance = currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment + currSummaryItem.Net;
						currSummaryItem.SetNetEndForPeriod(intFirstPeriod, currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment + currSummaryItem.GetNetForPeriod(intFirstPeriod));
					}
					else
					{
						currSummaryItem.EndBalance = currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment - currSummaryItem.Net;
						currSummaryItem.SetNetEndForPeriod(intFirstPeriod, currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment - currSummaryItem.GetNetForPeriod(intFirstPeriod));
					}
					// Call currSummaryItem.SetNetEndForPeriod(intFirstPeriod, currSummaryItem.BeginningBalance + currSummaryItem.BudgetAdjustment + currSummaryItem.GetNetForPeriod(intFirstPeriod))
					// Call currSummaryItem.SetNetEndForPeriod(intLastPeriod, currSummaryItem.EndBalance)
					tempCount = intFirstPeriod + 1;
					if (tempCount > 12)
					{
						tempCount = 1;
					}
					while (tempCount != intFirstPeriod)
					{
						if (tempCount > 1)
						{
							currSummaryItem.SetNetBeginForPeriod(tempCount, currSummaryItem.GetNetEndForPeriod(tempCount - 1));
						}
						else
						{
							currSummaryItem.SetNetBeginForPeriod(tempCount, currSummaryItem.GetNetEndForPeriod(12));
						}
						if (!currSummaryItem.IsExpense)
						{
							currSummaryItem.SetNetEndForPeriod(tempCount, currSummaryItem.GetNetBeginForPeriod(tempCount) + currSummaryItem.GetNetForPeriod(tempCount));
						}
						else
						{
							currSummaryItem.SetNetEndForPeriod(tempCount, currSummaryItem.GetNetBeginForPeriod(tempCount) - currSummaryItem.GetNetForPeriod(tempCount));
						}
						tempCount += 1;
						if (tempCount > 12)
						{
							tempCount = 1;
						}
					}
					if (Strings.LCase(currAccount.AccountType) == "g")
					{
						currSummaryItem.Fund = currAccount.FirstAccountField;
						// currSummaryItem.Description = currAccount.Description
						listLedgerTitles.MoveFirst();
						while (listLedgerTitles.IsCurrent())
						{
							lTitle = (cBDAccountTitle)listLedgerTitles.GetCurrentItem();
							if (currAccount.Account == "G " + lTitle.Fund + "-" + lTitle.Account + "-" + lTitle.Suffix)
							{
								currSummaryItem.Description = lTitle.Description;
								currSummaryItem.ShortDescription = lTitle.ShortDescription;
								break;
							}
							listLedgerTitles.MoveNext();
						}
					}
					else if (currAccount.IsExpense())
					{
						listDeptDivs.MoveFirst();
						currDept = null;
						CurrDivision = null;
						while (listDeptDivs.IsCurrent())
						{
							//Application.DoEvents();
							currDeptDiv = (cDeptDivTitle)listDeptDivs.GetCurrentItem();
							if (currDeptDiv.Department == currAccount.FirstAccountField)
							{
								if (Conversion.Val(currDeptDiv.Division) == 0)
								{
									currDept = currDeptDiv;
									if (!boolExpensesUseDivision)
									{
										currSummaryItem.Fund = currDept.Fund;
										break;
									}
									else
									{
										if (!(CurrDivision == null))
										{
											break;
										}
									}
								}
								else
								{
									if (boolExpensesUseDivision)
									{
										if (currDeptDiv.Division == currAccount.SecondAccountField)
										{
											CurrDivision = currDeptDiv;
											// currSummaryItem.Fund = currDeptDiv.Fund
											currSummaryItem.Fund = currDept.Fund;
											if (!(currDept == null))
											{
												break;
											}
										}
									}
								}
							}
							listDeptDivs.MoveNext();
						}
						if (!(currDept == null))
						{
							currSummaryItem.Description = currDept.LongDescription;
							currSummaryItem.ShortDescription = currDept.ShortDescription;
							if (boolExpensesUseDivision && !(CurrDivision == null))
							{
								currSummaryItem.Description = currSummaryItem.Description + " / " + CurrDivision.LongDescription;
								currSummaryItem.ShortDescription = currSummaryItem.ShortDescription + " / " + CurrDivision.ShortDescription;
							}
						}
						listExpObjs.MoveFirst();
						strExpenseDescription = "";
						strObjectDescription = "";
						strShortExpDescription = "";
						strShortObjectDescription = "";
						while (listExpObjs.IsCurrent())
						{
							//Application.DoEvents();
							eObTitle = (cExpenseObjectTitle)listExpObjs.GetCurrentItem();
							if (boolExpensesUseDivision)
							{
								strExpense = currAccount.ThirdAccountField;
								strObject = currAccount.FourthAccountField;
							}
							else
							{
								strExpense = currAccount.SecondAccountField;
								strObject = currAccount.ThirdAccountField;
							}
							if (strExpense == eObTitle.Expense)
							{
								if (Conversion.Val(eObTitle.ExpenseObject) == 0)
								{
									strExpenseDescription = eObTitle.LongDescription;
									strShortExpDescription = eObTitle.ShortDescription;
									if (!boolUseObject)
									{
										break;
									}
								}
								if (boolUseObject)
								{
									if (strObject == eObTitle.ExpenseObject)
									{
										strObjectDescription = eObTitle.LongDescription;
										strShortObjectDescription = eObTitle.ShortDescription;
										if (strExpenseDescription != "")
										{
											break;
										}
									}
								}
							}
							listExpObjs.MoveNext();
						}
						currSummaryItem.Description = currSummaryItem.Description + " - " + strExpenseDescription;
						currSummaryItem.ShortDescription = currSummaryItem.ShortDescription + " - " + strShortExpDescription;
						if (strObjectDescription != "")
						{
							currSummaryItem.Description = currSummaryItem.Description + " / " + strObjectDescription;
						}
						if (strShortObjectDescription != "")
						{
							currSummaryItem.ShortDescription = currSummaryItem.ShortDescription + " / " + strShortObjectDescription;
						}
					}
					else
					{
						// revenue
						listDeptDivs.MoveFirst();
						currDept = null;
						CurrDivision = null;
						while (listDeptDivs.IsCurrent())
						{
							//Application.DoEvents();
							currDeptDiv = (cDeptDivTitle)listDeptDivs.GetCurrentItem();
							if (currDeptDiv.Department == currAccount.FirstAccountField)
							{
								if (Conversion.Val(currDeptDiv.Division) == 0)
								{
									currDept = currDeptDiv;
									if (!boolRevenuesUseDivision)
									{
										currSummaryItem.Fund = currDept.Fund;
										break;
									}
									else
									{
										if (!(CurrDivision == null))
										{
											break;
										}
									}
								}
								else
								{
									if (boolRevenuesUseDivision)
									{
										if (currDeptDiv.Division == currAccount.SecondAccountField)
										{
											CurrDivision = currDeptDiv;
											// currSummaryItem.Fund = currDeptDiv.Fund
											currSummaryItem.Fund = currDept.Fund;
											if (!(currDept == null))
											{
												break;
											}
										}
									}
								}
							}
							listDeptDivs.MoveNext();
						}
						if (!(currDept == null))
						{
							currSummaryItem.Description = currDept.LongDescription;
							currSummaryItem.ShortDescription = currDept.ShortDescription;
							if (boolRevenuesUseDivision && !(CurrDivision == null))
							{
								currSummaryItem.Description = currSummaryItem.Description + " / " + CurrDivision.LongDescription;
								currSummaryItem.ShortDescription = currSummaryItem.ShortDescription + " / " + CurrDivision.ShortDescription;
							}
						}
						listRevTitles.MoveFirst();
						while (listRevTitles.IsCurrent())
						{
							//Application.DoEvents();
							rTitle = (cRevTitle)listRevTitles.GetCurrentItem();
							if (rTitle.Department == currAccount.FirstAccountField)
							{
								if (boolRevenuesUseDivision)
								{
									if (rTitle.Division == currAccount.SecondAccountField)
									{
										if (rTitle.Revenue == currAccount.ThirdAccountField)
										{
											currSummaryItem.Description = currSummaryItem.Description + " - " + rTitle.LongDescription;
											currSummaryItem.ShortDescription = currSummaryItem.ShortDescription + " - " + rTitle.ShortDescription;
											break;
										}
									}
								}
								else
								{
									if (rTitle.Revenue == currAccount.SecondAccountField)
									{
										currSummaryItem.Description = currSummaryItem.Description + " - " + rTitle.LongDescription;
										currSummaryItem.ShortDescription = currSummaryItem.ShortDescription + " - " + rTitle.ShortDescription;
										break;
									}
								}
							}
							listRevTitles.MoveNext();
						}
					}
					listSummaries.AddItem(currSummaryItem);
					dictSummaries.Remove(currSummaryItem.Account);
				}
				if (dictSummaries.Count > 0)
				{
					lngMax = dictSummaries.Count - 1;
					accts = dictSummaries.Values;
					foreach (object obj in accts)
					{
						currSummaryItem = (cAccountSummaryItem)obj;
						listSummaries.AddItem(currSummaryItem);
					}
				}
				dictSummaries.Clear();
				dictSummaries = null;
				GetAccountSummaries = listSummaries;
				return GetAccountSummaries;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetAccountSummaries;
		}
		// Private Function GetAccountsWithActivity(ByVal intStartPeriod As Integer, ByVal intEndPeriod As Integer, ByVal boolIncludePending As Boolean) As cERGDictionaryGroup
		//
		// ClearErrors
		// On Error GoTo ErrorHandler
		//
		// Dim returnGroup As New cERGDictionaryGroup
		// Dim listJournals As cGenericCollection
		// Set listJournals = journController.GetJournalBriefInfosByPeriodRange(intStartPeriod, intEndPeriod, True, boolIncludePending)
		// listJournals.MoveFirst
		// Dim currJourninfo As cJournalBriefInfo
		// Dim listJournalEntries As cGenericCollection
		// Dim listAPJournalEntries As cGenericCollection
		// Dim listapJournals As cGenericCollection
		//
		// Dim ldSummaryinfo As cLedgerDetailSummaryInfo
		// Dim exSummaryInfo As cExpenseDetailSummaryInfo
		// Dim rvSummaryInfo As cRevenueDetailSummaryInfo
		// Dim currJournEntry As cJournalEntry
		// Dim currAPDetail As cAPJournalDetail
		// Dim currAPJournal As cAPJournal
		// Dim strTemp As String
		// Dim dblCredit As Double
		// Dim dblDebit As Double
		// Dim currAcctItem As cAccountReportItem
		//
		// Do While listJournals.IsCurrent
		// DoEvents
		// Set currJourninfo = listJournals.GetCurrentItem
		// Select Case LCase(currJourninfo.JournalType)
		// Case "ap"
		// Set listapJournals = apjournController.GetFullAPJournalsByJournalNumber(currJourninfo.JournalNumber)
		// listapJournals.MoveFirst
		// Do While listapJournals.IsCurrent
		// Set currAPJournal = listapJournals.GetCurrentItem
		// currAPJournal.JournalEntries.MoveFirst
		// Do While currAPJournal.JournalEntries.IsCurrent
		// DoEvents
		// dblCredit = 0
		// dblDebit = 0
		// Set currAPDetail = currAPJournal.JournalEntries.GetCurrentItem
		// strTemp = LCase(Left(currAPDetail.Account & " ", 1))
		// Select Case strTemp
		// Case "g"
		// If Not returnGroup.GeneralLedgers.Exists(currAPDetail.Account) Then
		// Set currAcctItem = New cAccountReportItem
		// currAcctItem.Account = currAPDetail.Account
		// Set returnGroup.GeneralLedgers[currAcctItem.Account] = currAcctItem
		// Else
		// Set currAcctItem = returnGroup.GeneralLedgers[currAPDetail.Account]
		// End If
		// If currAPDetail.IsCredit Then
		// dblCredit = Abs(currAPDetail.Amount)
		// Else
		// dblDebit = Abs(currAPDetail.Amount)
		// End If
		// currAcctItem.Credits = currAcctItem.Credits + dblCredit
		// currAcctItem.Debits = currAcctItem.Debits + dblDebit
		// Case "e"
		// If Not returnGroup.Expenses.Exists(currAPDetail.Account) Then
		// Set currAcctItem = New cAccountReportItem
		// currAcctItem.Account = currAPDetail.Account
		// Set returnGroup.Expenses[currAcctItem.Account] = currAcctItem
		// Else
		// Set currAcctItem = returnGroup.Expenses[currAPDetail.Account]
		// End If
		// If currAPDetail.IsCredit Then
		// dblCredit = Abs(currAPDetail.Amount)
		// Else
		// dblDebit = Abs(currAPDetail.Amount)
		// End If
		// currAcctItem.Credits = currAcctItem.Credits + dblCredit
		// currAcctItem.Debits = currAcctItem.Debits + dblDebit
		// Case "r"
		// If Not returnGroup.Revenues.Exists(currAPDetail.Account) Then
		// Set currAcctItem = New cAccountReportItem
		// currAcctItem.Account = currAPDetail.Account
		// Set returnGroup.Revenues[currAcctItem.Account] = currAcctItem
		// Else
		// Set currAcctItem = returnGroup.Revenues[currAPDetail.Account]
		// End If
		// If currAPDetail.IsCredit Then
		// dblCredit = Abs(currAPDetail.Amount)
		// Else
		// dblDebit = Abs(currAPDetail.Amount)
		// End If
		// currAcctItem.Credits = currAcctItem.Credits + dblCredit
		// currAcctItem.Debits = currAcctItem.Debits + dblDebit
		// End Select
		// currAPJournal.JournalEntries.MoveNext
		// Loop
		// listapJournals.MoveNext
		// Loop
		// Case "en"
		// Case "gj", "py", "cw", "cr"
		// Set listJournalEntries = journController.GetJournalEntriesByNumber(currJourninfo.JournalNumber)
		// listJournalEntries.MoveFirst
		// Do While listJournalEntries.IsCurrent
		// DoEvents
		// dblCredit = 0
		// dblDebit = 0
		// Set currJournEntry = listJournalEntries.GetCurrentItem
		// strTemp = LCase(Left(currJournEntry.Account & " ", 1))
		// Select Case strTemp
		// Case "g"
		// If Not returnGroup.GeneralLedgers.Exists(currJournEntry.Account) Then
		// Set currAcctItem = New cAccountReportItem
		// currAcctItem.Account = currJournEntry.Account
		// Set returnGroup.GeneralLedgers[currAcctItem.Account] = currAcctItem
		// Else
		// Set currAcctItem = returnGroup.GeneralLedgers[currJournEntry.Account]
		// End If
		// If currJournEntry.RCB <> "B" And currJournEntry.RCB <> "E" Then '(currJournEntry.RCB <> "E" Or currJournEntry.JournalType = "P") Then
		// If currJournEntry.IsCredit Then
		// dblCredit = Abs(currJournEntry.Amount)
		// Else
		// dblDebit = Abs(currJournEntry.Amount)
		// End If
		//
		// currAcctItem.Credits = currAcctItem.Credits + dblCredit
		// currAcctItem.Debits = currAcctItem.Debits + dblDebit
		// ElseIf currJournEntry.RCB = "E" Then
		// ldSummaryinfo.EncumbranceDebits = ldSummaryinfo.EncumbranceDebits + currJournEntry.Amount
		// ldSummaryinfo.EncumbranceActivity = ldSummaryinfo.EncumbranceActivity + currJournEntry.Amount
		// End If
		// Case "e"
		// If Not returnGroup.Expenses.Exists(currJournEntry.Account) Then
		// Set currAcctItem = New cAccountReportItem
		// currAcctItem.Account = currJournEntry.Account
		// Set returnGroup.Expenses[currJournEntry.Account] = currAcctItem
		// Else
		// Set currAcctItem = returnGroup.Expenses[currJournEntry.Account]
		// End If
		// If currJournEntry.RCB <> "B" And currJournEntry.RCB <> "E" Then
		// If currJournEntry.IsCredit Then
		// dblCredit = Abs(currJournEntry.Amount)
		// Else
		// dblDebit = Abs(currJournEntry.Amount)
		// End If
		// currAcctItem.Credits = currAcctItem.Credits + dblCredit
		// currAcctItem.Debits = currAcctItem.Debits + dblDebit
		// ElseIf currJournEntry.RCB = "E" Then
		// exSummaryInfo.EncumbranceActivity = exSummaryInfo.EncumbranceActivity + currJournEntry.Amount
		// End If
		// Case "r"
		// If Not returnGroup.Revenues.Exists(currJournEntry.Account) Then
		// Set currAcctItem = New cAccountReportItem
		// currAcctItem.Account = currJournEntry.Account
		// Set returnGroup.Revenues[currJournEntry.Account] = currAcctItem
		// Else
		// Set currAcctItem = returnGroup.Revenues[currJournEntry.Account]
		// End If
		// If currJournEntry.RCB <> "B" And currJournEntry.RCB <> "E" Then
		// If currJournEntry.IsCredit Then
		// dblCredit = Abs(currJournEntry.Amount)
		// Else
		// dblDebit = Abs(currJournEntry.Amount)
		// End If
		// currAcctItem.Credits = currAcctItem.Credits + dblCredit
		// currAcctItem.Debits = currAcctItem.Debits + dblDebit
		// ElseIf currJournEntry.RCB = "E" Then
		// rvSummaryInfo.EncumbranceActivity = rvSummaryInfo.EncumbranceActivity + currJournEntry.Amount
		// End If
		// End Select
		//
		//
		// listJournalEntries.MoveNext
		// Loop
		// End Select
		// listJournals.MoveNext
		// Loop
		// now add accounts with no activity
		// now calculate beginning balances etc
		// now order accounts
		//
		// Set GetAccountsWithActivity = returnGroup
		// Exit Function
		// ErrorHandler:
		// Call SetError(Err.Number, Err.Description)
		// End Function
		// Public Sub test(ByVal intStartPeriod As Integer, ByVal intEndPeriod As Integer)
		// get accounts with activity first
		//
		// ClearErrors
		// On Error GoTo ErrorHandler
		// Dim listExpenseAccounts As New Dictionary
		// Dim listRevenueAccounts As New Dictionary
		// Dim listLedgerAccounts As New Dictionary
		//
		// Dim listJournals As cGenericCollection
		// Set listJournals = journController.GetJournalBriefInfosByPeriodRange(intStartPeriod, intEndPeriod, True, False)
		// listJournals.MoveFirst
		// Dim currJourninfo As cJournalBriefInfo
		// Dim listJournalEntries As cGenericCollection
		// Dim listAPJournalEntries As cGenericCollection
		// Dim listapJournals As cGenericCollection
		//
		// Dim ldSummaryinfo As cLedgerDetailSummaryInfo
		// Dim exSummaryInfo As cExpenseDetailSummaryInfo
		// Dim rvSummaryInfo As cRevenueDetailSummaryInfo
		// Dim currJournEntry As cJournalEntry
		// Dim currAPDetail As cAPJournalDetail
		// Dim currAPJournal As cAPJournal
		// Dim strTemp As String
		// Dim dblCredit As Double
		// Dim dblDebit As Double
		// Dim dblEncumbrance As Double
		// Dim dblEncumbranceCredit As Double
		// Dim dblEncumbranceDebit As Double
		// Dim bbusy As New frmBusy
		// bbusy.StartBusy
		// bbusy.Message = ""
		// bbusy.Show , MDIParent
		// Do While listJournals.IsCurrent
		// DoEvents
		// Set currJourninfo = listJournals.GetCurrentItem
		// Select Case LCase(currJourninfo.JournalType)
		// Case "ap"
		// Set listapJournals = apjournController.GetFullAPJournalsByJournalNumber(currJourninfo.JournalNumber)
		// listapJournals.MoveFirst
		// Do While listapJournals.IsCurrent
		// Set currAPJournal = listapJournals.GetCurrentItem
		// currAPJournal.JournalEntries.MoveFirst
		// Do While currAPJournal.JournalEntries.IsCurrent
		// DoEvents
		// dblCredit = 0
		// dblDebit = 0
		// dblEncumbrance = 0
		// dblEncumbranceCredit = 0
		// dblEncumbranceDebit = 0
		// Set currAPDetail = currAPJournal.JournalEntries.GetCurrentItem
		// strTemp = LCase(Left(currAPDetail.Account & " ", 1))
		// Select Case strTemp
		// Case "g"
		// If Not listLedgerAccounts.Exists(currAPDetail.Account) Then
		// Set ldSummaryinfo = New cLedgerDetailSummaryInfo
		// ldSummaryinfo.Account = currAPDetail.Account
		// ldSummaryinfo.Fund = GetFundFromAccount(currAPDetail.Account)
		// ldSummaryinfo.Suffix = GetSuffix(ldSummaryinfo.Account)
		// ldSummaryinfo.LedgerAccount = GetAccountNumber(ldSummaryinfo.Account)
		// ldSummaryinfo.Period = currJourninfo.Period
		// Set listLedgerAccounts[ldSummaryinfo.Account] = ldSummaryinfo
		// Else
		// Set ldSummaryinfo = listLedgerAccounts[currAPDetail.Account]
		// End If
		// dblEncumbrance = Abs(currAPDetail.Encumbrance)
		// If currAPDetail.IsCredit Then
		// dblCredit = Abs(currAPDetail.Amount)
		// Else
		// dblDebit = Abs(currAPDetail.Amount)
		// End If
		// dblEncumbrance = Abs(currAPDetail.Encumbrance)
		// If LCase(currAPJournal.Status) = "p" Then
		// ldSummaryinfo.PostedCredits = ldSummaryinfo.PostedCredits + dblCredit
		// ldSummaryinfo.PostedDebits = ldSummaryinfo.PostedDebits + dblDebit
		// ldSummaryinfo.EncumbranceActivity = ldSummaryinfo.EncumbranceActivity + dblEncumbrance
		// ldSummaryinfo.EncumbranceDebits = ldSummaryinfo.EncumbranceDebits + dblEncumbrance
		// Else
		// ldSummaryinfo.PendingCredits = ldSummaryinfo.PendingCredits + dblCredit
		// ldSummaryinfo.PendingDebits = ldSummaryinfo.PendingDebits + dblDebit
		// ldSummaryinfo.EncumbranceDebits = ldSummaryinfo.EncumbranceDebits + dblEncumbrance
		// End If
		// Case "e"
		// If Not listExpenseAccounts.Exists(currAPDetail.Account) Then
		// Set exSummaryInfo = New cExpenseDetailSummaryInfo
		// exSummaryInfo.Account = currAPDetail.Account
		// exSummaryInfo.AccountObject = GetObject(currAPDetail.Account)
		// exSummaryInfo.Department = GetDepartment(exSummaryInfo.Account)
		// exSummaryInfo.Division = GetExpDivision(exSummaryInfo.Account)
		// exSummaryInfo.Expense = GetExpense(exSummaryInfo.Account)
		// exSummaryInfo.Period = currJourninfo.Period
		// Set listExpenseAccounts[exSummaryInfo.Account] = exSummaryInfo
		// Else
		// Set exSummaryInfo = listExpenseAccounts[currAPDetail.Account]
		// End If
		// dblEncumbrance = Abs(currAPDetail.Encumbrance)
		// If currAPDetail.IsCredit Then
		// dblCredit = Abs(currAPDetail.Amount)
		// Else
		// dblDebit = Abs(currAPDetail.Amount)
		// End If
		// If LCase(currAPJournal.Status) = "p" Then
		// exSummaryInfo.PostedCredits = exSummaryInfo.PostedCredits + dblCredit
		// exSummaryInfo.PostedDebits = exSummaryInfo.PostedDebits + dblDebit
		// exSummaryInfo.EncumbranceActivity = exSummaryInfo.EncumbranceActivity + dblEncumbrance
		// Else
		// exSummaryInfo.PendingCredits = exSummaryInfo.PendingCredits + dblCredit
		// exSummaryInfo.PendingDebits = exSummaryInfo.PendingDebits + dblDebit
		// End If
		// Case "r"
		// If Not listRevenueAccounts.Exists(currAPDetail.Account) Then
		// Set rvSummaryInfo = New cRevenueDetailSummaryInfo
		// rvSummaryInfo.Account = currAPDetail.Account
		// rvSummaryInfo.Department = GetDepartment(rvSummaryInfo.Account)
		// rvSummaryInfo.Division = GetRevDivision(rvSummaryInfo.Account)
		// rvSummaryInfo.Period = currJourninfo.Period
		// rvSummaryInfo.Revenue = GetRevenue(rvSummaryInfo.Account)
		// Set listRevenueAccounts[rvSummaryInfo.Account] = rvSummaryInfo
		// Else
		// Set rvSummaryInfo = listRevenueAccounts[currAPDetail.Account]
		// End If
		// dblEncumbrance = Abs(currAPDetail.Encumbrance)
		// If currAPDetail.IsCredit Then
		// dblCredit = Abs(currAPDetail.Amount)
		// Else
		// dblDebit = Abs(currAPDetail.Amount)
		// End If
		// If LCase(currAPJournal.Status) = "p" Then
		// rvSummaryInfo.PostedCredits = rvSummaryInfo.PostedCredits + dblCredit
		// rvSummaryInfo.PostedDebits = rvSummaryInfo.PostedDebits + dblDebit
		// rvSummaryInfo.EncumbranceActivity = rvSummaryInfo.EncumbranceActivity + dblEncumbrance
		// Else
		// rvSummaryInfo.PendingCredits = rvSummaryInfo.PendingCredits + dblCredit
		// rvSummaryInfo.PendingDebits = rvSummaryInfo.PendingDebits + dblDebit
		// End If
		// End Select
		// currAPJournal.JournalEntries.MoveNext
		// Loop
		// listapJournals.MoveNext
		// Loop
		// Case "en"
		// Case "gj", "py", "cw", "cr"
		// Set listJournalEntries = journController.GetJournalEntriesByNumber(currJourninfo.JournalNumber)
		// listJournalEntries.MoveFirst
		// Do While listJournalEntries.IsCurrent
		// DoEvents
		// dblCredit = 0
		// dblDebit = 0
		// dblEncumbrance = 0
		// dblEncumbranceDebit = 0
		// dblEncumbranceCredit = 0
		// Set currJournEntry = listJournalEntries.GetCurrentItem
		// strTemp = LCase(Left(currJournEntry.Account & " ", 1))
		// Select Case strTemp
		// Case "g"
		// If Not listLedgerAccounts.Exists(currJournEntry.Account) Then
		// Set ldSummaryinfo = New cLedgerDetailSummaryInfo
		// ldSummaryinfo.Account = currJournEntry.Account
		// ldSummaryinfo.Fund = GetFundFromAccount(ldSummaryinfo.Account)
		// ldSummaryinfo.Suffix = GetSuffix(ldSummaryinfo.Account)
		// ldSummaryinfo.LedgerAccount = GetAccountNumber(ldSummaryinfo.Account)
		// ldSummaryinfo.Period = currJourninfo.Period
		// Set listLedgerAccounts[ldSummaryinfo.Account] = ldSummaryinfo
		// Else
		// Set ldSummaryinfo = listLedgerAccounts[currJournEntry.Account]
		// End If
		// If currJournEntry.RCB <> "B" And currJournEntry.RCB <> "E" Then '(currJournEntry.RCB <> "E" Or currJournEntry.JournalType = "P") Then
		// If currJournEntry.IsCredit Then
		// dblCredit = Abs(currJournEntry.Amount)
		// Else
		// dblDebit = Abs(currJournEntry.Amount)
		// End If
		//
		// If LCase(currJournEntry.Status) = "p" Then
		// ldSummaryinfo.PostedCredits = ldSummaryinfo.PostedCredits + dblCredit
		// ldSummaryinfo.PostedDebits = ldSummaryinfo.PostedDebits + dblDebit
		// Else
		// ldSummaryinfo.PendingCredits = ldSummaryinfo.PendingCredits + dblCredit
		// ldSummaryinfo.PendingDebits = ldSummaryinfo.PendingDebits + dblDebit
		// End If
		// ElseIf currJournEntry.RCB = "E" Then
		// ldSummaryinfo.EncumbranceDebits = ldSummaryinfo.EncumbranceDebits + currJournEntry.Amount
		// ldSummaryinfo.EncumbranceActivity = ldSummaryinfo.EncumbranceActivity + currJournEntry.Amount
		// End If
		// Case "e"
		// If Not listExpenseAccounts.Exists(currJournEntry.Account) Then
		// Set exSummaryInfo = New cExpenseDetailSummaryInfo
		// exSummaryInfo.Account = currJournEntry.Account
		// exSummaryInfo.AccountObject = GetObject(currJournEntry.Account)
		// exSummaryInfo.Department = GetDepartment(currJournEntry.Account)
		// exSummaryInfo.Division = GetExpDivision(currJournEntry.Account)
		// exSummaryInfo.Expense = GetExpense(currJournEntry.Account)
		// exSummaryInfo.Period = currJourninfo.Period
		// Set listExpenseAccounts[currJournEntry.Account] = exSummaryInfo
		// Else
		// Set exSummaryInfo = listExpenseAccounts[currJournEntry.Account]
		// End If
		// If currJournEntry.RCB <> "B" And currJournEntry.RCB <> "E" Then
		// If currJournEntry.IsCredit Then
		// dblCredit = Abs(currJournEntry.Amount)
		// Else
		// dblDebit = Abs(currJournEntry.Amount)
		// End If
		// If LCase(currJournEntry.Status) = "p" Then
		// exSummaryInfo.PostedCredits = exSummaryInfo.PostedCredits + dblCredit
		// exSummaryInfo.PostedDebits = exSummaryInfo.PostedDebits + dblDebit
		// Else
		// exSummaryInfo.PendingCredits = exSummaryInfo.PendingCredits + dblCredit
		// exSummaryInfo.PendingDebits = exSummaryInfo.PendingDebits + dblDebit
		// End If
		// ElseIf currJournEntry.RCB = "E" Then
		// exSummaryInfo.EncumbranceActivity = exSummaryInfo.EncumbranceActivity + currJournEntry.Amount
		// End If
		// Case "r"
		// If Not listRevenueAccounts.Exists(currJournEntry.Account) Then
		// Set rvSummaryInfo = New cRevenueDetailSummaryInfo
		// rvSummaryInfo.Account = currJournEntry.Account
		// rvSummaryInfo.Department = GetDepartment(currJournEntry.Account)
		// rvSummaryInfo.Division = GetRevDivision(currJournEntry.Account)
		// rvSummaryInfo.Period = currJourninfo.Period
		// rvSummaryInfo.Revenue = GetRevenue(currJournEntry.Account)
		// Set listRevenueAccounts[currJournEntry.Account] = rvSummaryInfo
		// Else
		// Set rvSummaryInfo = listRevenueAccounts[currJournEntry.Account]
		// End If
		// If currJournEntry.RCB <> "B" And currJournEntry.RCB <> "E" Then
		// If currJournEntry.IsCredit Then
		// dblCredit = Abs(currJournEntry.Amount)
		// Else
		// dblDebit = Abs(currJournEntry.Amount)
		// End If
		// If LCase(currJournEntry.Status) = "p" Then
		// rvSummaryInfo.PostedCredits = rvSummaryInfo.PostedCredits + dblCredit
		// rvSummaryInfo.PostedDebits = rvSummaryInfo.PostedDebits + dblDebit
		// Else
		// rvSummaryInfo.PendingCredits = rvSummaryInfo.PendingCredits + dblCredit
		// rvSummaryInfo.PendingDebits = rvSummaryInfo.PendingDebits + dblDebit
		// End If
		// ElseIf currJournEntry.RCB = "E" Then
		// rvSummaryInfo.EncumbranceActivity = rvSummaryInfo.EncumbranceActivity + currJournEntry.Amount
		// End If
		// End Select
		//
		//
		// listJournalEntries.MoveNext
		// Loop
		// End Select
		// listJournals.MoveNext
		// Loop
		// now add accounts with no activity
		// now calculate beginning balances etc
		// now order accounts
		//
		// bbusy.StopBusy
		// Unload bbusy
		// Set bbusy = Nothing
		// Exit Sub
		// ErrorHandler:
		// bbusy.StopBusy
		// Unload bbusy
		// Set bbusy = Nothing
		// Call SetError(Err.Number, Err.Description)
		// End Sub
		public void FillBDAccountDetailReport(ref cBDAccountDetailReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection collReturn = new cGenericCollection();
				cAccountDetailSummaryInfo detInfo;
				cGenericCollection collJournals;
				cJournalBriefInfo journInfo;
				cJournal journ;
				cAPJournal apJourn;
				cJournalEntry jEntry;
				cAPJournalDetail aEntry;
				Dictionary<object, object> summGroups = new Dictionary<object, object>();
				// vbPorter upgrade warning: asummGroup As cBDAccountSummaryGroup	OnWrite(cBDAccountSummaryGroup, object)
				cBDAccountSummaryGroup asummGroup;
				cGenericCollection collAps;
				int intStartPeriod;
				int intEndPeriod;
				string strAccount = "";
				bool boolPosted;
				bool boolUnposted;
				cAccountSummaryItem acctSumm;
				Dictionary<object, object> acctDict = new Dictionary<object, object>();
				// vbPorter upgrade warning: bAccount As cBDAccount	OnWrite(object)
				cBDAccount bAccount;
				cGenericCollection collEncumbrances;
				cEncumbrance enc;
				cEncumbranceDetail encDetail;
				intStartPeriod = theReport.StartPeriod;
				intEndPeriod = theReport.EndPeriod;
				boolPosted = theReport.IncludePosted;
				boolUnposted = theReport.IncludeUnposted;
				theReport.Accounts.MoveFirst();
				acctDict = bactController.GetFullAccountsDictionary();
				while (theReport.Accounts.IsCurrent())
				{
					//Application.DoEvents();
					strAccount = (string)theReport.Accounts.GetCurrentItem();
					if (acctDict.ContainsKey(strAccount))
					{
						bAccount = (cBDAccount)acctDict[strAccount];
						asummGroup = new cBDAccountSummaryGroup();
						asummGroup.Account = bAccount;
						summGroups.Add(strAccount, asummGroup);
					}
					theReport.Accounts.MoveNext();
				}
				collJournals = journController.GetJournalBriefInfosByPeriodRange(intStartPeriod, intEndPeriod, boolPosted, boolUnposted);
				if (journController.HadError)
				{
					SetError(journController.LastErrorNumber, journController.LastErrorMessage);
					return;
				}
				if (!(collJournals == null))
				{
					collJournals.MoveFirst();
					while (collJournals.IsCurrent())
					{
						//Application.DoEvents();
						journInfo = (cJournalBriefInfo)collJournals.GetCurrentItem();
						if ((Strings.LCase(journInfo.JournalType) == "ap") || (Strings.LCase(journInfo.JournalType) == "ac"))
						{
							collAps = apjournController.GetFullAPJournalsByJournalNumber(journInfo.JournalNumber);
							if (apjournController.HadError)
							{
								SetError(apjournController.LastErrorNumber, apjournController.LastErrorMessage);
								return;
							}
							collAps.MoveFirst();
							while (collAps.IsCurrent())
							{
								//Application.DoEvents();
								apJourn = (cAPJournal)collAps.GetCurrentItem();
								apJourn.JournalEntries.MoveFirst();
								while (apJourn.JournalEntries.IsCurrent())
								{
									//Application.DoEvents();
									aEntry = (cAPJournalDetail)apJourn.JournalEntries.GetCurrentItem();
									if (summGroups.ContainsKey(aEntry.Account))
									{
										asummGroup = (cBDAccountSummaryGroup)summGroups[aEntry.Account];
										detInfo = new cAccountDetailSummaryInfo();
										detInfo.Account = aEntry.Account;
										detInfo.Check = apJourn.CheckNumber;
										detInfo.Description = aEntry.Description;
										detInfo.EntryDate = apJourn.PayableDate;
										detInfo.JournalNumber = journInfo.JournalNumber;
										detInfo.JournalType = journInfo.JournalType;
										detInfo.Period = journInfo.Period;
										detInfo.ParentID = apJourn.ID;
										if (aEntry.IsCredit())
										{
											if (!aEntry.IsCorrection())
											{
												detInfo.Credits = Math.Abs(aEntry.Amount);
											}
											else
											{
												detInfo.Credits = -Math.Abs(aEntry.Amount);
											}
										}
										else
										{
											if (!aEntry.IsCorrection())
											{
												detInfo.Debits = Math.Abs(aEntry.Amount - aEntry.Discount);
											}
											else
											{
												detInfo.Debits = -(Math.Abs(aEntry.Amount - aEntry.Discount));
											}
										}
										asummGroup.Details.AddItem(detInfo);
									}
									apJourn.JournalEntries.MoveNext();
								}
								collAps.MoveNext();
							}
						}
						else if (Strings.LCase(journInfo.JournalType) == "en")
						{
							collEncumbrances = enController.GetFullEncumbrancesByJournalNumber(journInfo.JournalNumber);
							if (enController.HadError)
							{
								SetError(enController.LastErrorNumber, enController.LastErrorMessage);
								return;
							}
							collEncumbrances.MoveFirst();
							while (collEncumbrances.IsCurrent())
							{
								//Application.DoEvents();
								enc = (cEncumbrance)collEncumbrances.GetCurrentItem();
								enc.EncumbranceDetails.MoveFirst();
								while (enc.EncumbranceDetails.IsCurrent())
								{
									//Application.DoEvents();
									encDetail = (cEncumbranceDetail)enc.EncumbranceDetails.GetCurrentItem();
									if (summGroups.ContainsKey(encDetail.Account))
									{
										asummGroup = (cBDAccountSummaryGroup)summGroups[encDetail.Account];
										detInfo = new cAccountDetailSummaryInfo();
										detInfo.Account = encDetail.Account;
										detInfo.Description = encDetail.Description;
										detInfo.EntryDate = enc.EncumbrancesDate;
										detInfo.JournalNumber = journInfo.JournalNumber;
										detInfo.JournalType = journInfo.JournalType;
										detInfo.Period = journInfo.Period;
										detInfo.ParentID = enc.ID;
										if (encDetail.IsDebit())
										{
											detInfo.Debits = Math.Abs(encDetail.Amount);
										}
										else
										{
											detInfo.Credits = Math.Abs(encDetail.Amount);
										}
										asummGroup.Details.AddItem(detInfo);
									}
									enc.EncumbranceDetails.MoveNext();
								}
								collEncumbrances.MoveNext();
							}
						}
						else if ((Strings.LCase(journInfo.JournalType) == "gj") || (Strings.LCase(journInfo.JournalType) == "py") || (Strings.LCase(journInfo.JournalType) == "cw") || (Strings.LCase(journInfo.JournalType) == "cr"))
						{
							journ = journController.GetFullJournal(journInfo.ID);
							if (journController.HadError)
							{
								SetError(journController.LastErrorNumber, journController.LastErrorMessage);
								return;
							}
							journ.JournalEntries.MoveFirst();
							while (journ.JournalEntries.IsCurrent())
							{
								//Application.DoEvents();
								jEntry = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
								if (summGroups.ContainsKey(jEntry.Account))
								{
									asummGroup = (cBDAccountSummaryGroup)summGroups[jEntry.Account];
									detInfo = new cAccountDetailSummaryInfo();
									detInfo.Account = jEntry.Account;
									detInfo.Check = jEntry.CheckNumber;
									detInfo.Description = jEntry.Description;
									detInfo.EntryDate = jEntry.JournalEntriesDate;
									detInfo.JournalNumber = journ.JournalNumber;
									detInfo.JournalType = journ.JournalType;
									detInfo.Period = journ.Period;
									detInfo.ParentID = journ.ID;
									if (!jEntry.IsCorrection())
									{
										if (jEntry.IsCredit())
										{
											detInfo.Credits = Math.Abs(jEntry.Amount);
										}
										else
										{
											detInfo.Debits = Math.Abs(jEntry.Amount);
										}
									}
									else
									{
										if (jEntry.IsCredit())
										{
											detInfo.Credits = -Math.Abs(jEntry.Amount);
										}
										else
										{
											detInfo.Credits = -Math.Abs(jEntry.Amount);
										}
									}
									asummGroup.Details.AddItem(detInfo);
								}
								journ.JournalEntries.MoveNext();
							}
						}
						collJournals.MoveNext();
					}
					//FC:FINAL:ASZ: Begin
					//int x;
					//object ary = null;
					//ary = summGroups.Values;
					//theReport.AccountSummaries.ClearList();
					//for (x = 0; x <= Information.UBound((object[])ary, 1); x++)                    
					//{
					//    theReport.AccountSummaries.AddItem(((object[])ary)[x]);
					//}
					Dictionary<object, object>.ValueCollection ary;
					ary = summGroups.Values;
					theReport.AccountSummaries.ClearList();
					foreach (object obj in ary)
					{
						theReport.AccountSummaries.AddItem(obj);
					}
					//FC:FINAL:ASZ: End
					summGroups.Clear();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void FillBDAccountStatusReport(ref cBDAccountStatusReport theReport)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(theReport == null))
				{
					theReport.Accounts.ClearList();
					switch (theReport.AccountRangeType)
					{
						case 0:
							{
								// all
								theReport.Accounts = bactController.GetAccounts();
								break;
							}
						case 1:
							{
								// single
								theReport.Accounts.AddItem(bactController.GetAccountByAccount(theReport.Account));
								break;
							}
						case 2:
							{
								// range of accounts
								break;
							}
						case 3:
							{
								// department
								break;
							}
						case 4:
							{
								// department range
								break;
							}
					}
					//end switch
				}
				else
				{
					Information.Err().Raise(9999, null, "Null report object in Fill Account Status Report", null, null);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void FillExpRevSummaryReport(ref cExpRevSummaryReport theReport)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsDepts = new clsDRWrapper();
				string strDepartmentRange;
				cGenericCollection collFunds = new cGenericCollection();
				strDepartmentRange = theReport.RangeType;
				if (!(theReport == null))
				{
					if ((Strings.UCase(theReport.RangeType) == "SF") || (Strings.UCase(theReport.RangeType) == "F") || (Strings.UCase(theReport.RangeType) == "FD") || (Strings.UCase(theReport.RangeType) == "A"))
					{
						FillExpRevSummaryReportByFund(ref theReport);
					}
					else
					{
						FillExpRevSummaryReportByDepartment(ref theReport);
					}
					// If strDepartmentRange = "SF" Or strDepartmentRange = "F" Then
					// Call rsDepts.OpenRecordset("SELECT DISTINCT DepartmentQuery.Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) AS DepartmentQuery INNER JOIN DeptDivTitles ON DepartmentQuery.Department = DeptDivTitles.Department WHERE Fund >= '" & theReport.RangeStart & "' AND Fund <= '" & theReport.RangeEnd & "' order by departmentquery.department", "Budgetary")
					// Set collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeEnd)
					// ElseIf strDepartmentRange = "FD" Then
					// If ExpDivFlag Then
					// Call rsDepts.OpenRecordset("SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) as tbl1 WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" & theReport.RangeStart & "') order by department ", "Budgetary")
					// Else
					// rsDepartmentInfo.OpenRecordset "SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) as tbl1 WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Division = '" & GetFormat("0", Val(Mid(Exp, 3, 2))) & "' AND Fund = '" & theReport.RangeStart & "') order by department"
					// End If
					// Set collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeStart)
					// ElseIf strDepartmentRange <> "A" Then
					// rsDepartmentInfo.OpenRecordset "SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo WHERE Department >= '" & theReport.RangeStart & "' AND Department <= '" & theReport.RangeEnd & "' UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo WHERE Department >= '" & theReport.RangeStart & "' AND Department <= '" & theReport.RangeEnd & "') AS DepartmentQuery order by department"
					// Else
					// rsDepartmentInfo.OpenRecordset "SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo  UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo ) AS DepartmentQuery order by department"
					// End If
				}
				else
				{
					Information.Err().Raise(9999, null, "Null report object in Fill Expense Revenue Summary Report", null, null);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}
		// Private Sub FillExpRevSummaryReportByFund(ByRef theReport As cExpRevSummaryReport)
		// Dim collFunds As cGenericCollection
		// Dim collDepts As cGenericCollection
		// Dim boolUseExpDiv As Boolean
		// Dim boolUseExpObj As Boolean
		// Dim boolUseRevDiv As Boolean
		//
		// boolUseExpObj = Not ObjFlag
		// boolUseExpDiv = Not ExpDivFlag
		// boolUseRevDiv = Not RevDivFlag
		//
		// Dim rsDepts As New clsDRWrapper
		// If theReport.RangeType = "SF" Then
		// Call rsDepts.OpenRecordset("SELECT DISTINCT DepartmentQuery.Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) AS DepartmentQuery INNER JOIN DeptDivTitles ON DepartmentQuery.Department = DeptDivTitles.Department WHERE Fund >= '" & theReport.RangeStart & "' AND Fund <= '" & theReport.RangeEnd & "' order by departmentquery.department", "Budgetary")
		// Set collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeStart)
		// ElseIf theReport.RangeType = "F" Then
		// Call rsDepts.OpenRecordset("SELECT DISTINCT DepartmentQuery.Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) AS DepartmentQuery INNER JOIN DeptDivTitles ON DepartmentQuery.Department = DeptDivTitles.Department WHERE Fund >= '" & theReport.RangeStart & "' AND Fund <= '" & theReport.RangeEnd & "' order by departmentquery.department", "Budgetary")
		// Set collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeEnd)
		// Else
		// FD
		// If ExpDivFlag Then
		// Call rsDepts.OpenRecordset("SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) as tbl1 WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" & theReport.RangeStart & "') order by department ", "Budgetary")
		// Else
		// rsDepartmentInfo.OpenRecordset "SELECT DISTINCT Department FROM (SELECT DISTINCT Department FROM ExpenseReportInfo UNION ALL SELECT DISTINCT Department FROM RevenueReportInfo) as tbl1 WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Division = '" & GetFormat("0", Val(Mid(Exp, 3, 2))) & "' AND Fund = '" & theReport.RangeStart & "') order by department"
		// End If
		// Set collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeStart)
		// End If
		// collFunds.MoveFirst
		// Dim strCurrentFund As String
		// Dim strCurrentDepartment As String
		// Dim currFund As cFund
		// Dim currDep As cDeptDivTitle
		// Dim rsTemp As New clsDRWrapper
		// Dim exrevitem As cExpRevSummaryItem
		//
		// If strExpOrRev = "R" Then
		// rsTestData.OpenRecordset "SELECT * FROM RevenueReportInfo WHERE Department = '" & rsDepartmentInfo.Fields["Department"] & "' AND Division = '" & rsDivisionInfo.Fields["Division"] & "'"
		// Else
		// rsTestData.OpenRecordset "SELECT * FROM ExpenseReportInfo WHERE Department = '" & rsDepartmentInfo.Fields["Department"] & "' AND Division = '" & rsDivisionInfo.Fields["Division"] & "'"
		// End If
		// Do While collFunds.IsCurrent
		// DoEvents
		// Set currDep = collFunds.GetCurrentItem
		//
		// rsDepts.MoveFirst
		// Set collDepts = bactController.GetDeptDivsByFund(currFund.Fund, True)
		// If Not collDepts Is Nothing Then
		// collDepts.MoveFirst
		// Do While collDepts.IsCurrent
		// DoEvents
		// Set currDep = collDepts.GetCurrentItem
		// Call rsTemp.OpenRecordset("select * from revenuereportinfo where Department = '" & currDep.Department & "' order by department,division,revenue", "Budgetary")
		// Do While Not rsTemp.EndOfFile
		// DoEvents
		// Set exrevitem = New cExpRevSummaryItem
		// exrevitem.Fund = currFund.Fund
		// exrevitem.Department = currDep.Department
		// exrevitem.Division = rsTemp.Fields["division"]
		// exrevitem.Revenue = rsTemp.Fields["revenue"]
		// exrevitem.AccountType = "R"
		// If boolUseRevDiv Then
		// exrevitem.Account = "R " & exrevitem.Department & "-" & exrevitem.Division & "-" & exrevitem.Revenue
		// Else
		// exrevitem.Account = "R " & exrevitem.Department & "-" & exrevitem.Revenue
		// End If
		// rsTemp.MoveNext
		// Loop
		// Call rsTemp.OpenRecordset("select * from expensereportinfo where Department = '" & currDep.Department & "' order by department, division,expense,object", "Budgetary")
		// Do While Not rsTemp.EndOfFile
		// DoEvents
		// rsTemp.MoveNext
		// Loop
		// collDepts.MoveNext
		// Loop
		// ElseIf bactController.HadError Then
		// Call Err.Raise(bactController.LastErrorNumber, , bactController.LastErrorMessage)
		// Exit Sub
		// End If
		//
		// collFunds.MoveNext
		// Loop
		// End Sub
		private void FillExpRevSummaryReportByFund(ref cExpRevSummaryReport theReport)
		{
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection listSummaries;
				cGenericCollection collFunds;
				cGenericCollection collDepts;
				Dictionary<object, cGenericCollection> dictExpenses = new Dictionary<object, cGenericCollection>();
				Dictionary<object, cGenericCollection> dictRevenues = new Dictionary<object, cGenericCollection>();
				cFund tFund;
				cDeptDivTitle dd;
				cExpRevSummaryItem exrevitem;
				string strDept = "";
				cGenericCollection tempCollection;
				bool boolUseExpDiv;
				bool boolUseExpObj;
				bool booluseRevDiv;
				int intFirstPeriod;
				int intEndPeriod = 0;
				int intLastPeriod;
				int intStartPeriod;
				intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				intStartPeriod = intFirstPeriod;
				if (intStartPeriod == 1)
				{
					intEndPeriod = 12;
				}
				else
				{
					intEndPeriod = intStartPeriod - 1;
				}
				intLastPeriod = intEndPeriod;
				if (Strings.LCase(theReport.DateRangeType) != "a")
				{
					intStartPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(theReport.DateRangeStart)));
					intEndPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(theReport.DateRangeEnd)));
				}
				boolUseExpObj = !modAccountTitle.Statics.ObjFlag;
				boolUseExpDiv = !modAccountTitle.Statics.ExpDivFlag;
				booluseRevDiv = !modAccountTitle.Statics.RevDivFlag;
				listSummaries = GetAccountSummaries(theReport.IncludePending, false);
				if (HadError)
				{
					Information.Err().Raise(LastErrorNumber, null, LastErrorMessage, null, null);
				}
				if (theReport.RangeType == "SF")
				{
					collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeStart);
				}
				else if (theReport.RangeType == "F")
				{
					collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeEnd);
				}
				else if (theReport.RangeType == "FD")
				{
					// FD
					collFunds = bactController.GetFundsByFundRange(theReport.RangeStart, theReport.RangeStart);
				}
				else
				{
					collFunds = bactController.GetFunds();
				}
				cAccountSummaryItem currSummaryItem;
				cGenericCollection expAccts;
				listSummaries.MoveFirst();
				while (listSummaries.IsCurrent())
				{
					//Application.DoEvents();
					currSummaryItem = (cAccountSummaryItem)listSummaries.GetCurrentItem();
					if (currSummaryItem.IsExpense)
					{
						strDept = currSummaryItem.GetSegment(1);
						if (dictExpenses.ContainsKey(strDept))
						{
							tempCollection = dictExpenses[strDept];
							tempCollection.AddItem(currSummaryItem);
						}
						else
						{
							tempCollection = new cGenericCollection();
							tempCollection.AddItem(currSummaryItem);
							dictExpenses.Add(strDept, tempCollection);
						}
					}
					else if (currSummaryItem.IsRevenue)
					{
						strDept = currSummaryItem.GetSegment(1);
						if (dictRevenues.ContainsKey(strDept))
						{
							tempCollection = dictRevenues[strDept];
							tempCollection.AddItem(currSummaryItem);
						}
						else
						{
							tempCollection = new cGenericCollection();
							tempCollection.AddItem(currSummaryItem);
							dictRevenues.Add(strDept, tempCollection);
						}
						//FC:TODO:AM - check if this is correct
						//dictRevenues.Add(currSummaryItem.Account, currSummaryItem);
					}
					listSummaries.MoveNext();
				}
				collFunds.MoveFirst();
				while (collFunds.IsCurrent())
				{
					//Application.DoEvents();
					tFund = (cFund)collFunds.GetCurrentItem();
					collDepts = bactController.GetDeptDivsByFund(tFund.Fund, true);
					collDepts.MoveFirst();
					while (collDepts.IsCurrent())
					{
						dd = (cDeptDivTitle)collDepts.GetCurrentItem();
						if (dictRevenues.ContainsKey(dd.Department))
						{
							tempCollection = dictRevenues[dd.Department];
							tempCollection.MoveFirst();
							while (tempCollection.IsCurrent())
							{
								//Application.DoEvents();
								exrevitem = new cExpRevSummaryItem();
								exrevitem.Fund = dd.Fund;
								exrevitem.Department = dd.Department;
								exrevitem.AccountType = "R";
								exrevitem.Expense = "";
								exrevitem.ExpenseObject = "";
								currSummaryItem = (cAccountSummaryItem)tempCollection.GetCurrentItem();
								if (booluseRevDiv)
								{
									exrevitem.Division = currSummaryItem.GetSegment(2);
									exrevitem.Revenue = currSummaryItem.GetSegment(3);
								}
								else
								{
									exrevitem.Revenue = currSummaryItem.GetSegment(2);
								}
								exrevitem.Account = currSummaryItem.Account;
								exrevitem.Balance = currSummaryItem.GetNetEndForPeriod(intEndPeriod);
								exrevitem.Budget = currSummaryItem.BeginningNet;
								exrevitem.YTD = -1 * (exrevitem.Balance - currSummaryItem.GetNetBeginForPeriod(intFirstPeriod));
								exrevitem.CurrentMonth = -1 * (currSummaryItem.GetNetEndForPeriod(intEndPeriod) - currSummaryItem.GetNetBeginForPeriod(intStartPeriod));
								exrevitem.Description = currSummaryItem.Description;
								if (exrevitem.Budget > 0)
								{
									exrevitem.PercentUsed = FCConvert.ToDouble(Strings.Format(((exrevitem.Budget - exrevitem.Balance) / exrevitem.Budget) * 100, "0.00"));
								}
								else
								{
									exrevitem.PercentUsed = 0;
								}
								theReport.Details.AddItem(exrevitem);
								tempCollection.MoveNext();
							}
						}
						collDepts.MoveNext();
					}
					collDepts.MoveFirst();
					while (collDepts.IsCurrent())
					{
						//Application.DoEvents();
						dd = (cDeptDivTitle)collDepts.GetCurrentItem();
						if (dictExpenses.ContainsKey(dd.Department))
						{
							tempCollection = dictExpenses[dd.Department];
							tempCollection.MoveFirst();
							while (tempCollection.IsCurrent())
							{
								//Application.DoEvents();
								currSummaryItem = (cAccountSummaryItem)tempCollection.GetCurrentItem();
								exrevitem = new cExpRevSummaryItem();
								exrevitem.Fund = tFund.Fund;
								exrevitem.Department = dd.Department;
								if (boolUseExpDiv)
								{
									exrevitem.Division = currSummaryItem.GetSegment(2);
									exrevitem.Expense = currSummaryItem.GetSegment(3);
									if (boolUseExpObj)
									{
										exrevitem.ExpenseObject = currSummaryItem.GetSegment(4);
									}
								}
								else
								{
									exrevitem.Expense = currSummaryItem.GetSegment(2);
									if (boolUseExpObj)
									{
										exrevitem.ExpenseObject = currSummaryItem.GetSegment(3);
									}
								}
								exrevitem.Account = currSummaryItem.Account;
								exrevitem.AccountType = "E";
								exrevitem.Balance = currSummaryItem.GetNetEndForPeriod(intEndPeriod);
								exrevitem.Budget = currSummaryItem.BeginningNet;
								exrevitem.YTD = -1 * (exrevitem.Balance - currSummaryItem.GetNetBeginForPeriod(intFirstPeriod));
								exrevitem.CurrentMonth = -1 * (currSummaryItem.GetNetEndForPeriod(intEndPeriod) - currSummaryItem.GetNetBeginForPeriod(intStartPeriod));
								exrevitem.Revenue = "";
								exrevitem.Description = currSummaryItem.Description;
								if (exrevitem.Budget > 0)
								{
									exrevitem.PercentUsed = FCConvert.ToDouble(Strings.Format(100 * ((exrevitem.Budget - exrevitem.Balance) / exrevitem.Budget), "0.00"));
								}
								else
								{
									exrevitem.PercentUsed = 0;
								}
								theReport.Details.AddItem(exrevitem);
								tempCollection.MoveNext();
							}
						}
						collDepts.MoveNext();
					}
					collFunds.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private void FillExpRevSummaryReportByDepartment(ref cExpRevSummaryReport theReport)
		{
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection collDepts;
				cGenericCollection listSummaries;
				Dictionary<object, cGenericCollection> dictExpenses = new Dictionary<object, cGenericCollection>();
				Dictionary<object, cGenericCollection> dictRevenues = new Dictionary<object, cGenericCollection>();
				bool boolUseExpObj;
				bool boolUseExpDiv;
				bool booluseRevDiv;
				cGenericCollection tempCollection;
				cDeptDivTitle dd;
				cExpRevSummaryItem exrevitem;
				string strDept = "";
				int intFirstPeriod;
				int intEndPeriod = 0;
				int intLastPeriod;
				int intStartPeriod;
				intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				intStartPeriod = intFirstPeriod;
				if (intStartPeriod == 1)
				{
					intEndPeriod = 12;
				}
				else
				{
					intEndPeriod = intStartPeriod - 1;
				}
				intLastPeriod = intEndPeriod;
				if (Strings.LCase(theReport.DateRangeType) != "a")
				{
					intStartPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(theReport.DateRangeStart)));
					intEndPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(theReport.DateRangeEnd)));
				}
				boolUseExpObj = !modAccountTitle.Statics.ObjFlag;
				boolUseExpDiv = !modAccountTitle.Statics.ExpDivFlag;
				booluseRevDiv = !modAccountTitle.Statics.RevDivFlag;
				listSummaries = GetAccountSummaries(theReport.IncludePending, false);
				if (HadError)
				{
					Information.Err().Raise(LastErrorNumber, null, LastErrorMessage, null, null);
				}
				if (theReport.RangeType == "SD")
				{
					collDepts = bactController.GetDeptDivsByRange(theReport.RangeStart, theReport.RangeStart, true);
				}
				else
				{
					collDepts = bactController.GetDeptDivsByRange(theReport.RangeStart, theReport.RangeEnd, true);
				}
				if (bactController.HadError)
				{
					Information.Err().Raise(bactController.LastErrorNumber, null, bactController.LastErrorMessage, null, null);
				}
				cAccountSummaryItem currSummaryItem;
				cGenericCollection expAccts;
				listSummaries.MoveFirst();
				while (listSummaries.IsCurrent())
				{
					//Application.DoEvents();
					currSummaryItem = (cAccountSummaryItem)listSummaries.GetCurrentItem();
					strDept = currSummaryItem.GetSegment(1);
					if (Strings.CompareString(strDept, theReport.RangeStart, true) >= 0 || Strings.CompareString(strDept, theReport.RangeEnd, true) <= 0)
					{
						if (currSummaryItem.IsExpense)
						{
							if (dictExpenses.ContainsKey(strDept))
							{
								tempCollection = dictExpenses[strDept];
								tempCollection.AddItem(currSummaryItem);
							}
							else
							{
								tempCollection = new cGenericCollection();
								tempCollection.AddItem(currSummaryItem);
								dictExpenses.Add(strDept, tempCollection);
							}
						}
						else if (currSummaryItem.IsRevenue)
						{
							if (dictRevenues.ContainsKey(strDept))
							{
								tempCollection = dictRevenues[strDept];
								tempCollection.AddItem(currSummaryItem);
							}
							else
							{
								tempCollection = new cGenericCollection();
								tempCollection.AddItem(currSummaryItem);
								dictRevenues.Add(strDept, tempCollection);
							}
							//FC:TODO:AM - check if this is correct
							//dictRevenues.Add(currSummaryItem.Account, currSummaryItem);
						}
					}
					listSummaries.MoveNext();
				}
				collDepts.MoveFirst();
				while (collDepts.IsCurrent())
				{
					dd = (cDeptDivTitle)collDepts.GetCurrentItem();
					if (dictRevenues.ContainsKey(dd.Department))
					{
						tempCollection = dictRevenues[dd.Department];
						tempCollection.MoveFirst();
						while (tempCollection.IsCurrent())
						{
							//Application.DoEvents();
							exrevitem = new cExpRevSummaryItem();
							exrevitem.Fund = dd.Fund;
							exrevitem.Department = dd.Department;
							exrevitem.AccountType = "R";
							exrevitem.Expense = "";
							exrevitem.ExpenseObject = "";
							currSummaryItem = (cAccountSummaryItem)tempCollection.GetCurrentItem();
							if (booluseRevDiv)
							{
								exrevitem.Division = currSummaryItem.GetSegment(2);
								exrevitem.Revenue = currSummaryItem.GetSegment(3);
							}
							else
							{
								exrevitem.Revenue = currSummaryItem.GetSegment(2);
							}
							exrevitem.Account = currSummaryItem.Account;
							exrevitem.Balance = currSummaryItem.GetNetEndForPeriod(intEndPeriod);
							exrevitem.Budget = currSummaryItem.BeginningNet;
							exrevitem.YTD = -1 * (exrevitem.Balance - currSummaryItem.GetNetBeginForPeriod(intFirstPeriod));
							exrevitem.CurrentMonth = -1 * (currSummaryItem.GetNetEndForPeriod(intEndPeriod) - currSummaryItem.GetNetBeginForPeriod(intStartPeriod));
							exrevitem.Description = currSummaryItem.Description;
							if (exrevitem.Budget > 0)
							{
								exrevitem.PercentUsed = FCConvert.ToDouble(Strings.Format(((exrevitem.Budget - exrevitem.Balance) / exrevitem.Budget) * 100, "0.00"));
							}
							else
							{
								exrevitem.PercentUsed = 0;
							}
							theReport.Details.AddItem(exrevitem);
							tempCollection.MoveNext();
						}
					}
					if (dictExpenses.ContainsKey(dd.Department))
					{
						tempCollection = dictExpenses[dd.Department];
						tempCollection.MoveFirst();
						while (tempCollection.IsCurrent())
						{
							//Application.DoEvents();
							currSummaryItem = (cAccountSummaryItem)tempCollection.GetCurrentItem();
							exrevitem = new cExpRevSummaryItem();
							exrevitem.Fund = dd.Fund;
							exrevitem.Department = dd.Department;
							if (boolUseExpDiv)
							{
								exrevitem.Division = currSummaryItem.GetSegment(2);
								exrevitem.Expense = currSummaryItem.GetSegment(3);
								if (boolUseExpObj)
								{
									exrevitem.ExpenseObject = currSummaryItem.GetSegment(4);
								}
							}
							else
							{
								exrevitem.Expense = currSummaryItem.GetSegment(2);
								if (boolUseExpObj)
								{
									exrevitem.ExpenseObject = currSummaryItem.GetSegment(3);
								}
							}
							exrevitem.Account = currSummaryItem.Account;
							exrevitem.AccountType = "E";
							exrevitem.Balance = currSummaryItem.GetNetEndForPeriod(intEndPeriod);
							exrevitem.Budget = currSummaryItem.BeginningNet;
							exrevitem.YTD = -1 * (exrevitem.Balance - currSummaryItem.GetNetBeginForPeriod(intFirstPeriod));
							exrevitem.CurrentMonth = -1 * (currSummaryItem.GetNetEndForPeriod(intEndPeriod) - currSummaryItem.GetNetBeginForPeriod(intStartPeriod));
							exrevitem.Revenue = "";
							exrevitem.Description = currSummaryItem.Description;
							if (exrevitem.Budget > 0)
							{
								exrevitem.PercentUsed = FCConvert.ToDouble(Strings.Format(((exrevitem.Budget - exrevitem.Balance) / exrevitem.Budget) * 100, "0.00"));
							}
							else
							{
								exrevitem.PercentUsed = 0;
							}
							theReport.Details.AddItem(exrevitem);
							tempCollection.MoveNext();
						}
					}
					collDepts.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}
		// Public Sub CalculateSummaryDetailInfo(Optional ByVal strArchivePath As String = "")
		// ClearErrors
		// On Error GoTo ErrorHandler
		//
		// Dim boolExpensesUseDivision As Boolean
		// Dim boolRevenuesUseDivision As Boolean
		// Dim boolUseObject As Boolean
		// Dim collJournals As cGenericCollection
		// Dim collAPJournals As cGenericCollection
		// Dim collEncumbrances As cGenericCollection
		// Dim boolIncludePending As Boolean
		// Dim intFirstPeriod As Integer
		// Dim intStartPeriod As Integer
		// Dim intEndPeriod As Integer
		// Dim intLastPeriod As Integer
		// Dim sdo As New cSummaryDetailObject
		// Dim aryOrderMonth(12) As Integer
		// Dim tmpAry() As String
		// Dim x As Integer
		// Dim intTemp As Integer
		// Dim boolUseProjects As Boolean
		// Dim dictAccounts As New Dictionary
		// Dim dExpenseSummaries As New Dictionary
		// Dim dRevenueSummaries As New Dictionary
		// Dim dLedgerSummaries As New Dictionary
		// Dim tempAccount As cBDAccount
		//
		// Set dictAccounts = bactController.GetAccountsDictionary(strArchivePath)
		// boolUseProjects = False
		// boolIncludePending = True
		// boolUseObject = Not ObjFlag
		// boolUseObject = bdSettings.ExpensesUseObject
		// boolExpensesUseDivision = bdSettings.ExpensesUseDivision
		// boolRevenuesUseDivision = bdSettings.RevenuesUseDivision
		//
		// If Val(Mid(Exp, 3, 2)) > 0 Then
		// boolExpensesUseDivision = True
		// End If
		// If Val(Mid(Rev, 3, 2)) > 0 Then
		// boolRevenuesUseDivision = True
		// End If
		// intFirstPeriod = Val(GetBDVariable("FiscalStart"))
		// intFirstPeriod = Val(bdSettings.FiscalStart)
		// intStartPeriod = intFirstPeriod
		// If intStartPeriod = 1 Then
		// intEndPeriod = 12
		// Else
		// intEndPeriod = intStartPeriod - 1
		// End If
		// intLastPeriod = intEndPeriod
		// intTemp = intFirstPeriod
		// For x = 0 To 11
		// aryOrderMonth(intTemp - 1) = x + 1
		// intTemp = intTemp + 1
		// If intTemp > 12 Then
		// intTemp = 1
		// End If
		// Next
		//
		//
		// Set collJournals = journController.GetFullJournalsByPeriod(intStartPeriod, intEndPeriod, boolIncludePending, strArchivePath)
		// If journController.HadError Then
		// Call SetError(journController.LastErrorNumber, journController.LastErrorMessage)
		// Exit Sub
		// End If
		// collJournals.MoveFirst
		// Dim journ As cJournal
		// Dim jDet As cJournalEntry
		// Dim ldi As cLedgerDetailInfo
		// Dim rdi As cRevenueDetailInfo
		// Dim edi As cExpenseDetailInfo
		// Dim collAPJourns As cGenericCollection
		// Dim collENJourns As cGenericCollection
		// Dim apJourn As cAPJournal
		// Dim apDet As cAPJournalDetail
		// Dim enJourn As cEncumbrance
		// Dim enDet As cEncumbranceDetail
		// Dim lri As cLedgerReportInfo
		// Dim eri As cExpenseReportInfo
		// Dim rri As cRevenueReportInfo
		// Dim tempDict As Dictionary
		//
		// Do While collJournals.IsCurrent
		// Set journ = collJournals.GetCurrentItem
		// journ.JournalEntries.MoveFirst
		// If LCase(journ.JournalType) = "ap" Or LCase(journ.JournalType) = "ac" Then
		// Set collAPJourns = apjournController.GetFullAPJournalsByJournalNumber(journ.JournalNumber, strArchivePath)
		// collAPJourns.MoveFirst
		// Do While collAPJourns.IsCurrent
		// 100                Set apJourn = collAPJourns.GetCurrentItem
		// apJourn.JournalEntries.MoveFirst
		// Do While apJourn.JournalEntries.IsCurrent
		// DoEvents
		// 150                    Set apDet = apJourn.JournalEntries.GetCurrentItem
		// tmpAry = Split(Mid(apDet.Account, 3), "-")
		// Select Case LCase(Left(apDet.Account, 1))
		// Case "g"
		// make detail record
		// 160                            Set ldi = New cLedgerDetailInfo
		// ldi.Account = apDet.Account
		// ldi.Fund = tmpAry(0)
		// ldi.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// ldi.Suffix = tmpAry(2)
		// End If
		// ldi.Description = apDet.Description
		// ldi.Amount = apDet.Amount
		// ldi.CheckNumber = apJourn.CheckNumber
		// ldi.Description = apDet.Description
		// ldi.Encumbrance = apDet.Encumbrance
		// ldi.EncumbranceKey = apDet.EncumbranceDetailRecord
		// ldi.Discount = apDet.Discount
		// ldi.JournalNumber = journ.JournalNumber
		// ldi.JournalType = "A"
		// ldi.Period = journ.Period
		// ldi.OrderMonth = aryOrderMonth(ldi.Period - 1)
		// ldi.PostedDate = apJourn.PostedDate
		// ldi.Project = apDet.Project
		// ldi.RCB = apDet.RCB
		// ldi.Status = journ.Status
		// ldi.TransactionDate = apJourn.CheckDate
		// ldi.VendorNumber = Val(apJourn.VendorNumber)
		// ldi.Warrant = Val(apJourn.Warrant)
		// Call sdo.LedgerDetails.AddItem(ldi)
		// make summary record
		// If dLedgerSummaries.Exists(apDet.Account) Then
		// Set tempDict = dLedgerSummaries[apDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set lri = tempDict[journ.Period]
		// Else
		// Set lri = New cLedgerReportInfo
		// lri.Account = apDet.Account
		// lri.Fund = tmpAry(0)
		// lri.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// lri.Suffix = tmpAry(2)
		// End If
		// lri.Period = journ.Period
		// Call tempDict.Add(journ.Period, lri)
		// End If
		// Else
		// Set lri = New cLedgerReportInfo
		// lri.Account = apDet.Account
		// lri.Fund = tmpAry(0)
		// lri.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// lri.Suffix = tmpAry(2)
		// End If
		// lri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, lri)
		// Call dLedgerSummaries.Add(apDet.Account, tempDict)
		// End If
		//
		// If journ.IsPosted Then
		// If apDet.IsCredit Then
		// lri.PostedCredits = lri.PostedCredits + apDet.Amount - apDet.Discount
		// If apDet.Encumbrance <> 0 Then
		// lri.EncumbranceCredits = lri.EncumbranceCredits + apDet.Encumbrance * -1
		// End If
		// Else
		// lri.PostedDebits = lri.PostedDebits + apDet.Amount - apDet.Discount
		// If apDet.Encumbrance <> 0 Then
		// lri.EncumbranceCredits = lri.EncumbranceCredits + apDet.Encumbrance * -1
		// End If
		// End If
		// If apDet.Encumbrance <> 0 Then
		// lri.EncumbranceActivity = lri.EncumbranceActivity + apDet.Encumbrance * -1
		// End If
		// Else
		// If apDet.IsCredit Then
		// lri.PendingCredits = lri.PendingCredits + apDet.Amount - apDet.Discount
		// Else
		// lri.PendingDebits = lri.PendingDebits + apDet.Amount - apDet.Discount
		// End If
		// If apDet.Encumbrance <> 0 Then
		// lri.EncumbranceActivity = lri.EncumbranceActivity - apDet.Encumbrance '* -1
		// End If
		// End If
		//
		// Case "e"
		// 200                            Set edi = New cExpenseDetailInfo
		// edi.Account = apDet.Account
		// edi.Amount = apDet.Amount
		// edi.CheckNumber = apJourn.CheckNumber
		// edi.Status = journ.Status
		// edi.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// edi.Division = tmpAry(1)
		// edi.Expense = tmpAry(2)
		// If boolUseObject Then
		// edi.ExpenseObject = tmpAry(3)
		// End If
		// Else
		// edi.Expense = tmpAry(1)
		// If boolUseObject Then
		// edi.ExpenseObject = tmpAry(2)
		// End If
		// End If
		// edi.Description = apDet.Description
		// edi.Discount = apDet.Discount
		// edi.Encumbrance = apDet.Encumbrance
		// edi.EncumbranceKey = apDet.EncumbranceDetailRecord
		// edi.JournalNumber = journ.JournalNumber
		// edi.JournalType = "A"
		// edi.Period = journ.Period
		// edi.OrderMonth = aryOrderMonth(edi.Period - 1)
		// edi.PostedDate = apJourn.PostedDate
		// edi.Project = apDet.Project
		// edi.RCB = apDet.RCB
		// edi.TransactionDate = apJourn.CheckDate
		// edi.VendorNumber = apJourn.VendorNumber
		// edi.Warrant = Val(apJourn.Warrant)
		// Call sdo.ExpenseDetails.AddItem(edi)
		// make summary record
		// If dExpenseSummaries.Exists(apDet.Account) Then
		// Set tempDict = dExpenseSummaries[apDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set eri = tempDict[journ.Period]
		// Else
		// Set eri = New cExpenseReportInfo
		// eri.Account = apDet.Account
		// eri.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// eri.Division = tmpAry(1)
		// eri.Expense = tmpAry(2)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(3)
		// End If
		// Else
		// eri.Expense = tmpAry(1)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(2)
		// End If
		// End If
		// eri.Period = journ.Period
		// Call tempDict.Add(journ.Period, eri)
		// End If
		// Else
		// Set eri = New cExpenseReportInfo
		// eri.Account = apDet.Account
		// eri.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// eri.Division = tmpAry(1)
		// eri.Expense = tmpAry(2)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(3)
		// End If
		// Else
		// eri.Expense = tmpAry(1)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(2)
		// End If
		// End If
		// eri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, eri)
		// Call dExpenseSummaries.Add(apDet.Account, tempDict)
		// End If
		//
		// If journ.IsPosted Then
		// If apDet.IsCredit Then
		// eri.PostedCredits = eri.PostedCredits + apDet.Amount - apDet.Discount
		// Else
		// eri.PostedDebits = eri.PostedDebits + apDet.Amount - apDet.Discount
		// If apDet.Encumbrance <> 0 Then
		// eri.EncumbranceDebits = eri.EncumbranceDebits - apDet.Encumbrance
		// End If
		// End If
		// If apDet.Encumbrance <> 0 Then
		// eri.EncumbranceActivity = eri.EncumbranceActivity - apDet.Encumbrance
		// End If
		// Else
		// If apDet.IsCredit Then
		// eri.PendingCredits = eri.PendingCredits + apDet.Amount - apDet.Discount
		// Else
		// eri.PendingDebits = eri.PendingDebits + apDet.Amount - apDet.Discount
		// If apDet.Encumbrance <> 0 Then
		// apdet.encumbrance * -1
		// eri.PendingEncumbranceDebits = eri.PendingEncumbranceDebits - apDet.Encumbrance
		// eri.PendingEncumbranceActivity = eri.PendingEncumbranceActivity - apDet.Encumbrance
		// End If
		// End If
		// End If
		// Case "r"
		// 300                            Set rdi = New cRevenueDetailInfo
		// rdi.Account = apDet.Account
		// rdi.Amount = apDet.Amount
		// rdi.Status = journ.Status
		// rdi.CheckNumber = apJourn.CheckNumber
		// rdi.Description = apDet.Description
		// rdi.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rdi.Division = tmpAry(1)
		// rdi.Revenue = tmpAry(2)
		// Else
		// rdi.Revenue = tmpAry(1)
		// End If
		// rdi.Discount = apDet.Discount
		// rdi.JournalNumber = journ.JournalNumber
		// rdi.Encumbrance = apDet.Encumbrance
		// rdi.EncumbranceKey = apDet.EncumbranceDetailRecord
		// rdi.JournalType = "A"
		// rdi.Period = journ.Period
		// rdi.OrderMonth = aryOrderMonth(rdi.Period - 1)
		// rdi.PostedDate = apJourn.PostedDate
		// rdi.Project = apDet.Project
		// rdi.RCB = apDet.RCB
		// rdi.TransactionDate = apJourn.CheckDate
		// rdi.VendorNumber = Val(apJourn.VendorNumber)
		// rdi.Warrant = Val(apJourn.Warrant)
		// Call sdo.RevenueDetails.AddItem(rdi)
		// make summary record
		// If dRevenueSummaries.Exists(apDet.Account) Then
		// Set tempDict = dRevenueSummaries[apDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set rri = tempDict[journ.Period]
		// Else
		// Set rri = New cRevenueReportInfo
		// rri.Account = jDet.Account
		// rri.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rri.Division = tmpAry(1)
		// rri.Revenue = tmpAry(2)
		// Else
		// rri.Revenue = tmpAry(1)
		// End If
		// lri.Period = journ.Period
		// Call tempDict.Add(journ.Period, rri)
		// End If
		// Else
		// Set rri = New cRevenueReportInfo
		// rri.Account = apDet.Account
		// rri.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rri.Division = tmpAry(1)
		// rri.Revenue = tmpAry(2)
		// Else
		// rri.Revenue = tmpAry(1)
		// End If
		// rri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, rri)
		// Call dRevenueSummaries.Add(apDet.Account, tempDict)
		// End If
		// If journ.IsPosted Then
		// If apDet.IsCredit Then
		// rri.PostedCredits = rri.PostedCredits + apDet.Amount - apDet.Discount
		// Else
		// rri.PostedDebits = rri.PostedDebits + apDet.Amount - apDet.Discount
		// If apDet.Encumbrance <> 0 Then
		// rri.EncumbranceDebits = rri.EncumbranceDebits - apDet.Encumbrance
		// End If
		// End If
		// If apDet.Encumbrance <> 0 Then
		// rri.EncumbranceActivity = rri.EncumbranceActivity - apDet.Encumbrance
		// End If
		// Else
		// If apDet.IsCredit Then
		// rri.PendingCredits = rri.PendingCredits + apDet.Amount - apDet.Discount
		// Else
		// rri.PendingDebits = rri.PendingDebits + apDet.Amount - apDet.Discount
		// If apDet.Encumbrance <> 0 Then
		// rri.EncumbranceDebits = rri.EncumbranceDebits - apDet.Encumbrance
		// End If
		// End If
		// If apDet.Encumbrance <> 0 Then
		// rri.EncumbranceActivity = rri.EncumbranceActivity - apDet.Encumbrance
		// End If
		// End If
		// End Select
		// apJourn.JournalEntries.MoveNext
		// Loop
		// collAPJourns.MoveNext
		// Loop
		// ElseIf LCase(journ.JournalType) = "en" Then
		// 400            Set collENJourns = enController.GetFullEncumbrancesByJournalNumber(journ.JournalNumber, strArchivePath)
		// collENJourns.MoveFirst
		// Do While collENJourns.IsCurrent
		// 450                Set enJourn = collENJourns.GetCurrentItem
		// enJourn.EncumbranceDetails.MoveFirst
		// Do While enJourn.EncumbranceDetails.IsCurrent
		// DoEvents
		// 500                    Set enDet = enJourn.EncumbranceDetails.GetCurrentItem
		// tmpAry = Split(Mid(enDet.Account, 3), "-")
		// Select Case LCase(Left(enDet.Account, 1))
		// Case "g"
		// make detail record
		// Set ldi = New cLedgerDetailInfo
		// ldi.Account = enDet.Account
		// ldi.Fund = tmpAry(0)
		// ldi.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// ldi.Suffix = tmpAry(2)
		// End If
		// ldi.Description = enDet.Description
		// ldi.JournalNumber = journ.JournalNumber
		// ldi.JournalType = "E"
		// ldi.Period = journ.Period
		// ldi.OrderMonth = aryOrderMonth(ldi.Period - 1)
		// ldi.PostedDate = enJourn.PostedDate
		// ldi.Project = enDet.Project
		// ldi.Status = journ.Status
		// ldi.TransactionDate = enJourn.EncumbrancesDate
		// ldi.VendorNumber = enJourn.VendorNumber
		// ldi.Adjustments = enDet.Adjustments
		// ldi.Liquidated = enDet.LiquidatedAmount
		// ldi.EncumbranceKey = enDet.EncumbranceID
		// ldi.Amount = enDet.Amount
		// Call sdo.LedgerDetails.AddItem(ldi)
		// make summary record
		// If dLedgerSummaries.Exists(enDet.Account) Then
		// Set tempDict = dLedgerSummaries[enDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set lri = tempDict[journ.Period]
		// Else
		// Set lri = New cLedgerReportInfo
		// lri.Account = enDet.Account
		// lri.Fund = tmpAry(0)
		// lri.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// lri.Suffix = tmpAry(2)
		// End If
		// lri.Period = journ.Period
		// Call tempDict.Add(journ.Period, lri)
		// End If
		// Else
		// Set lri = New cLedgerReportInfo
		// lri.Account = enDet.Account
		// lri.Fund = tmpAry(0)
		// lri.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// lri.Suffix = tmpAry(2)
		// End If
		// lri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, lri)
		// Call dLedgerSummaries.Add(enDet.Account, tempDict)
		// End If
		// Case "e"
		// make detail record
		// Set edi = New cExpenseDetailInfo
		// edi.Account = enDet.Account
		// edi.Amount = enDet.Amount
		// edi.Status = journ.Status
		// edi.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// edi.Division = tmpAry(1)
		// edi.Expense = tmpAry(2)
		// If boolUseObject Then
		// edi.ExpenseObject = tmpAry(3)
		// End If
		// Else
		// edi.Expense = tmpAry(1)
		// If boolUseObject Then
		// edi.ExpenseObject = tmpAry(2)
		// End If
		// End If
		// edi.Description = enDet.Description
		// edi.JournalNumber = journ.JournalNumber
		// edi.JournalType = "E"
		// edi.Period = journ.Period
		// edi.OrderMonth = aryOrderMonth(edi.Period - 1)
		// edi.PostedDate = enJourn.PostedDate
		// edi.Project = enDet.Project
		// edi.RCB = "E"
		// edi.TransactionDate = enJourn.EncumbrancesDate
		// edi.VendorNumber = enJourn.VendorNumber
		// edi.Warrant = 0
		// edi.Liquidated = enDet.LiquidatedAmount
		// edi.EncumbranceKey = enDet.EncumbranceID
		// edi.Adjustments = enDet.Adjustments
		// Call sdo.ExpenseDetails.AddItem(edi)
		// Case "r"
		// make detail record
		// Set rdi = New cRevenueDetailInfo
		// rdi.Account = enDet.Account
		// rdi.Amount = enDet.Amount
		// rdi.Status = journ.Status
		// rdi.Description = enDet.Description
		// rdi.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rdi.Division = tmpAry(1)
		// rdi.Revenue = tmpAry(2)
		// Else
		// rdi.Revenue = tmpAry(1)
		// End If
		// rdi.JournalNumber = journ.JournalNumber
		// rdi.JournalType = "E"
		// rdi.Period = journ.Period
		// rdi.OrderMonth = aryOrderMonth(rdi.Period - 1)
		// rdi.PostedDate = enJourn.PostedDate
		// rdi.Project = enDet.Project
		// rdi.RCB = "E"
		// rdi.TransactionDate = enJourn.EncumbrancesDate
		// rdi.VendorNumber = enJourn.VendorNumber
		// rdi.Warrant = 0
		// rdi.EncumbranceKey = enDet.EncumbranceID
		// rdi.Adjustments = enDet.Adjustments
		// rdi.Liquidated = enDet.LiquidatedAmount
		// Call sdo.RevenueDetails.AddItem(rdi)
		// End Select
		// enJourn.EncumbranceDetails.MoveNext
		// Loop
		// collENJourns.MoveNext
		// Loop
		// Else
		// 1000            Do While journ.JournalEntries.IsCurrent
		// DoEvents
		// 1010                Set jDet = journ.JournalEntries.GetCurrentItem
		// Select Case LCase(Left(jDet.Account, 1))
		// Case "g"
		// 1100                        tmpAry = Split(Mid(jDet.Account, 3), "-")
		// make detail record
		// Set ldi = New cLedgerDetailInfo
		// ldi.Account = jDet.Account
		// ldi.Fund = tmpAry(0)
		// ldi.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// ldi.Suffix = tmpAry(2)
		// End If
		// ldi.Description = jDet.Description
		// ldi.CheckNumber = jDet.CheckNumber
		// ldi.JournalNumber = journ.JournalNumber
		// ldi.JournalType = jDet.JournalType
		// ldi.Period = journ.Period
		// ldi.OrderMonth = aryOrderMonth(ldi.Period - 1)
		// ldi.PostedDate = jDet.PostedDate
		// ldi.Project = jDet.Project
		// ldi.RCB = jDet.RCB
		// ldi.Status = journ.Status
		// ldi.TransactionDate = jDet.JournalEntriesDate
		// ldi.VendorNumber = jDet.VendorNumber
		// ldi.Warrant = jDet.WarrantNumber
		// ldi.Amount = jDet.Amount
		// Call sdo.LedgerDetails.AddItem(ldi)
		// make summary record
		//
		// If dLedgerSummaries.Exists(jDet.Account) Then
		// Set tempDict = dLedgerSummaries[jDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set lri = tempDict[journ.Period]
		// Else
		// Set lri = New cLedgerReportInfo
		// lri.Account = jDet.Account
		// lri.Fund = tmpAry(0)
		// lri.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// lri.Suffix = tmpAry(2)
		// End If
		// lri.Period = journ.Period
		// Call tempDict.Add(journ.Period, lri)
		// End If
		// Else
		// Set lri = New cLedgerReportInfo
		// lri.Account = jDet.Account
		// lri.Fund = tmpAry(0)
		// lri.LedgerAccount = tmpAry(1)
		// If UBound(tmpAry) > 1 Then
		// lri.Suffix = tmpAry(2)
		// End If
		// lri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, lri)
		// Call dLedgerSummaries.Add(jDet.Account, tempDict)
		// End If
		//
		// If journ.IsPosted Then
		// If jDet.IsBudgetAdjustment Then
		// If journ.IsAutomaticJournal Then
		// lri.AutoBudgetAdjustments = lri.AutoBudgetAdjustments + jDet.Amount
		// End If
		// lri.BudgetAdjustments = lri.BudgetAdjustments + jDet.Amount
		// If jDet.CarryForward Then
		// lri.CarryForward = lri.CarryForward + jDet.Amount
		// End If
		// Else
		// If jDet.RCB <> "E" Then
		// If jDet.IsCredit Then
		// lri.PostedCredits = lri.PostedCredits + jDet.Amount
		// Else
		// If Not jDet.IsPayrollContract Then
		// lri.PostedDebits = lri.PostedDebits + jDet.Amount
		// Else
		// lri.PostedDebits = lri.PostedDebits - jDet.Amount
		// End If
		// End If
		// Else
		// If jDet.JournalType = "G" Or jDet.JournalType = "P" Then
		// lri.EncumbranceActivity = lri.EncumbranceActivity + jDet.Amount
		// If jDet.IsCredit Then
		// lri.EncumbranceCredits = lri.EncumbranceCredits + jDet.Amount
		// Else
		// lri.EncumbranceDebits = lri.EncumbranceDebits + jDet.Amount
		// End If
		// End If
		// End If
		// End If
		// Else
		// If jDet.IsCredit Then
		// lri.PendingCredits = lri.PendingCredits + jDet.Amount
		// Else
		// If Not jDet.IsPayrollContract Then
		// lri.PendingDebits = lri.PendingDebits + jDet.Amount
		// Else
		// lri.PendingDebits = lri.PendingDebits - jDet.Amount
		// End If
		// End If
		// End If
		// Case "e"
		// 1400                        tmpAry = Split(Mid(jDet.Account, 3), "-")
		// make detail record
		// Set edi = New cExpenseDetailInfo
		// edi.Account = jDet.Account
		// edi.Amount = jDet.Amount
		// edi.CheckNumber = jDet.CheckNumber
		// edi.Status = journ.Status
		// edi.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// edi.Division = tmpAry(1)
		// edi.Expense = tmpAry(2)
		// If boolUseObject Then
		// edi.ExpenseObject = tmpAry(3)
		// End If
		// Else
		// edi.Expense = tmpAry(1)
		// If boolUseObject Then
		// edi.ExpenseObject = tmpAry(2)
		// End If
		// End If
		// edi.Description = jDet.Description
		// edi.JournalNumber = journ.JournalNumber
		// edi.JournalType = jDet.JournalType
		// edi.Period = journ.Period
		// edi.OrderMonth = aryOrderMonth(edi.Period - 1)
		// edi.PostedDate = jDet.PostedDate
		// edi.Project = jDet.Project
		// edi.RCB = jDet.RCB
		// edi.TransactionDate = jDet.JournalEntriesDate
		// edi.VendorNumber = jDet.VendorNumber
		// edi.Warrant = jDet.WarrantNumber
		// Call sdo.ExpenseDetails.AddItem(edi)
		// make summary record
		// If dExpenseSummaries.Exists(jDet.Account) Then
		// Set tempDict = dExpenseSummaries[jDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set eri = tempDict[journ.Period]
		// Else
		// Set eri = New cExpenseReportInfo
		// eri.Account = jDet.Account
		// eri.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// eri.Division = tmpAry(1)
		// eri.Expense = tmpAry(2)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(3)
		// End If
		// Else
		// eri.Expense = tmpAry(1)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(2)
		// End If
		// End If
		// eri.Period = journ.Period
		// Call tempDict.Add(journ.Period, eri)
		// End If
		// Else
		// Set eri = New cExpenseReportInfo
		// eri.Account = jDet.Account
		// eri.Department = tmpAry(0)
		// If boolExpensesUseDivision Then
		// eri.Division = tmpAry(1)
		// eri.Expense = tmpAry(2)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(3)
		// End If
		// Else
		// eri.Expense = tmpAry(1)
		// If boolUseObject Then
		// eri.AccountObject = tmpAry(2)
		// End If
		// End If
		// eri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, eri)
		// Call dExpenseSummaries.Add(jDet.Account, tempDict)
		// End If
		//
		// If journ.IsPosted Then
		// If jDet.IsBudgetAdjustment Then
		// If journ.IsAutomaticJournal Then
		// eri.AutoBudgetAdjustments = eri.AutoBudgetAdjustments + jDet.Amount
		// End If
		// eri.BudgetAdjustments = eri.BudgetAdjustments + jDet.Amount
		// If jDet.CarryForward Then
		// eri.CarryForward = eri.CarryForward + jDet.Amount
		// End If
		// Else
		// If jDet.RCB <> "E" Then
		// If jDet.IsCredit Then
		// eri.PostedCredits = eri.PostedCredits + jDet.Amount
		// Else
		// eri.PostedDebits = eri.PostedDebits + jDet.Amount
		// End If
		// Else
		// eri.EncumbranceActivity = eri.EncumbranceActivity + jDet.Amount
		// End If
		// End If
		// Else
		// If jDet.IsCredit Then
		// eri.PendingCredits = eri.PendingCredits + jDet.Amount
		// Else
		// eri.PendingDebits = eri.PendingDebits + jDet.Amount
		// End If
		// End If
		// Case "r"
		//
		// 1500                        tmpAry = Split(Mid(jDet.Account, 3), "-")
		// make detail record
		// Set rdi = New cRevenueDetailInfo
		// rdi.Account = jDet.Account
		// rdi.Amount = jDet.Amount
		// rdi.Status = journ.Status
		// rdi.CheckNumber = jDet.CheckNumber
		// rdi.Description = jDet.Description
		// rdi.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rdi.Division = tmpAry(1)
		// rdi.Revenue = tmpAry(2)
		// Else
		// rdi.Revenue = tmpAry(1)
		// End If
		// rdi.JournalNumber = journ.JournalNumber
		// rdi.JournalType = jDet.JournalType
		// rdi.Period = jDet.Period
		// rdi.OrderMonth = aryOrderMonth(rdi.Period - 1)
		// rdi.PostedDate = jDet.PostedDate
		// rdi.Project = jDet.Project
		// rdi.RCB = jDet.RCB
		// rdi.TransactionDate = jDet.JournalEntriesDate
		// rdi.VendorNumber = jDet.VendorNumber
		// rdi.Warrant = jDet.WarrantNumber
		// Call sdo.RevenueDetails.AddItem(rdi)
		// make summary record
		// If dRevenueSummaries.Exists(jDet.Account) Then
		// Set tempDict = dRevenueSummaries[jDet.Account]
		// If tempDict.Exists(journ.Period) Then
		// Set rri = tempDict[journ.Period]
		// Else
		// Set rri = New cRevenueReportInfo
		// rri.Account = jDet.Account
		// rri.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rri.Division = tmpAry(1)
		// rri.Revenue = tmpAry(2)
		// Else
		// rri.Revenue = tmpAry(1)
		// End If
		// lri.Period = journ.Period
		// Call tempDict.Add(journ.Period, rri)
		// End If
		// Else
		// Set rri = New cRevenueReportInfo
		// rri.Account = jDet.Account
		// rri.Department = tmpAry(0)
		// If boolRevenuesUseDivision Then
		// rri.Division = tmpAry(1)
		// rri.Revenue = tmpAry(2)
		// Else
		// rri.Revenue = tmpAry(1)
		// End If
		// rri.Period = journ.Period
		// Set tempDict = New Dictionary
		// Call tempDict.Add(journ.Period, rri)
		// Call dRevenueSummaries.Add(jDet.Account, tempDict)
		// End If
		// If journ.IsPosted Then
		// If jDet.IsBudgetAdjustment Then
		// If journ.IsAutomaticJournal Then
		// rri.AutoBudgetAdjustments = rri.AutoBudgetAdjustments + jDet.Amount
		// End If
		// rri.BudgetAdjustments = rri.BudgetAdjustments + jDet.Amount
		// If jDet.CarryForward Then
		// rri.CarryForward = rri.CarryForward + jDet.Amount
		// End If
		// Else
		// If Not jDet.RCB = "E" Then
		// If jDet.IsCredit Then
		// rri.PostedCredits = rri.PostedCredits + jDet.Amount
		// Else
		// rri.PostedDebits = rri.PostedDebits + jDet.Amount
		// End If
		// Else
		// rri.EncumbranceActivity = rri.EncumbranceActivity + jDet.Amount
		// End If
		// End If
		// Else
		// If jDet.IsCredit Then
		// rri.PendingCredits = rri.PendingCredits + jDet.Amount
		// Else
		// rri.PendingDebits = rri.PendingDebits + jDet.Amount
		// End If
		// End If
		// End Select
		//
		// journ.JournalEntries.MoveNext
		// Loop
		// End If
		// collJournals.MoveNext
		// Loop
		// 1600    Set collJournals = Nothing
		//
		// sdo.ExpenseDetails.MoveFirst
		// sdo.LedgerDetails.MoveFirst
		// sdo.RevenueDetails.MoveFirst
		// Dim aryLSums
		// Dim aryAccts
		// Dim aryKeys
		// Dim dblOriginalBudget As Double
		// Dim aryAccountMaster
		//
		// aryAccountMaster = dictAccounts.Items
		//
		// aryAccts = dLedgerSummaries.Items
		// aryKeys = dLedgerSummaries.Keys
		//
		// Dim rsSave As New clsDRWrapper
		// If strArchivePath <> "" Then
		// rsSave.GroupName = strArchivePath
		// End If
		// Call rsSave.Execute("delete from ledgerreportinfo", "Budgetary")
		// Call rsSave.Execute("delete from expensereportinfo", "Budgetary")
		// Call rsSave.Execute("delete from revenuereportinfo", "Budgetary")
		// Call rsSave.Execute("delete from ledgerdetailinfo", "Budgetary")
		// Call rsSave.Execute("delete from expensedetailinfo", "Budgetary")
		// Call rsSave.Execute("delete from revenuedetailinfo", "Budgetary")
		//
		// Dim summDict As Dictionary
		// Dim y As Integer
		// Call rsSave.OpenRecordset("select * from ledgerreportinfo where id = -1", "Budgetary")
		// For x = 0 To UBound(aryAccts)
		// Set summDict = aryAccts(x)
		// aryLSums = summDict.Items
		// dblOriginalBudget = 0
		// If dictAccounts.Exists(aryKeys(x)) Then
		// Set tempAccount = dictAccounts[aryKeys(x])
		// dblOriginalBudget = tempAccount.CurrentBudget
		// End If
		// For y = 0 To UBound(aryLSums)
		// DoEvents
		// Set lri = aryLSums(y)
		// rsSave.AddNew
		// rsSave.Fields["Account"] = lri.Account
		// rsSave.Fields["Period"] = lri.Period
		// rsSave.Fields["Fund"] = lri.Fund
		// rsSave.Fields["LedgerAccount"] = lri.LedgerAccount
		// rsSave.Fields["Suffix"] = lri.Suffix
		// rsSave.Fields["BudgetAdjustments"] = lri.BudgetAdjustments
		// If y > 0 Then
		// dblOriginalBudget = 0
		// End If
		// lri.OriginalBudget = dblOriginalBudget
		// rsSave.Fields["OriginalBudget"] = lri.OriginalBudget
		// rsSave.Fields["postedDebits"] = lri.PostedDebits
		// rsSave.Fields["PostedCredits"] = lri.PostedCredits
		// rsSave.Fields["EncumbranceDebits"] = lri.EncumbranceDebits
		// rsSave.Fields["EncumbActivity"] = lri.EncumbranceActivity
		// rsSave.Fields["PendingDebits"] = lri.PendingDebits
		// rsSave.Fields["PendingCredits"] = lri.PendingCredits
		// rsSave.Fields["EncumbranceCredits"] = lri.EncumbranceCredits
		// rsSave.Fields["Project"] = lri.Project
		// rsSave.Fields["AutomaticBudgetAdjustments"] = lri.AutoBudgetAdjustments
		// rsSave.Fields["PendingEncumbActivity"] = lri.PendingEncumbranceActivity
		// rsSave.Fields["PendingEncumbranceDebits"] = lri.PendingEncumbranceDebits
		// rsSave.Fields["CarryForward"] = lri.CarryForward
		// rsSave.Update
		// Next
		// Next
		// For x = 0 To UBound(aryAccountMaster)
		// Set tempAccount = aryAccountMaster(x)
		// If Not dLedgerSummaries.Exists(tempAccount.Account) And tempAccount.IsLedger Then
		// rsSave.AddNew
		//
		//
		// rsSave.Fields["Account"] = tempAccount.Account
		// rsSave.Fields["Period"] = 0
		// rsSave.Fields["Fund"] = tempAccount.FirstAccountField
		// rsSave.Fields["LedgerAccount"] = tempAccount.SecondAccountField
		// If bdSettings.LedgersUseSuffix Then
		// rsSave.Fields["Suffix"] = tempAccount.ThirdAccountField
		// Else
		// rsSave.Fields["suffix"] = ""
		// End If
		//
		// rsSave.Fields["BudgetAdjustments"] = 0
		//
		// rsSave.Fields["OriginalBudget"] = tempAccount.CurrentBudget
		// rsSave.Fields["postedDebits"] = 0
		// rsSave.Fields["PostedCredits"] = 0
		// rsSave.Fields["EncumbranceDebits"] = 0
		// rsSave.Fields["EncumbActivity"] = 0
		// rsSave.Fields["PendingDebits"] = 0
		// rsSave.Fields["PendingCredits"] = 0
		// rsSave.Fields["EncumbranceCredits"] = 0
		// rsSave.Fields["Project"] = ""
		// rsSave.Fields["AutomaticBudgetAdjustments"] = 0
		// rsSave.Fields["PendingEncumbActivity"] = 0
		// rsSave.Fields["PendingEncumbranceDebits"] = 0
		// rsSave.Fields["CarryForward"] = 0
		//
		//
		// End If
		// Next
		// Call rsSave.BulkCopy("LedgerReportInfo", "Budgetary", True)
		//
		// aryAccts = dRevenueSummaries.Items
		// aryKeys = dRevenueSummaries.Keys
		//
		// Call rsSave.OpenRecordset("select * from revenuereportinfo where id = -1", "Budgetary")
		// For x = 0 To UBound(aryAccts)
		// Set summDict = aryAccts(x)
		// aryLSums = summDict.Items
		// dblOriginalBudget = 0
		// If dictAccounts.Exists(aryKeys(x)) Then
		// Set tempAccount = dictAccounts[aryKeys(x])
		// dblOriginalBudget = tempAccount.CurrentBudget
		// End If
		// For y = 0 To UBound(aryLSums)
		// DoEvents
		// Set rri = aryLSums(y)
		// rsSave.AddNew
		// rsSave.Fields["account"] = rri.Account
		// rsSave.Fields["period"] = rri.Period
		// rsSave.Fields["department"] = rri.Department
		// rsSave.Fields["Division"] = rri.Division
		// rsSave.Fields["Revenue"] = rri.Revenue
		// rsSave.Fields["BudgetAdjustments"] = rri.BudgetAdjustments
		// If y > 0 Then
		// dblOriginalBudget = 0
		// End If
		// rri.OriginalBudget = dblOriginalBudget
		// rsSave.Fields["OriginalBudget"] = rri.OriginalBudget
		// rsSave.Fields["PostedDebits"] = rri.PostedDebits
		// rsSave.Fields["PostedCredits"] = rri.PostedCredits
		// rsSave.Fields["PendingDebits"] = rri.PendingDebits
		// rsSave.Fields["PendingCredits"] = rri.PendingCredits
		// rsSave.Fields["EncumbranceDebits"] = rri.EncumbranceDebits
		// rsSave.Fields["EncumbranceCredits"] = rri.EncumbranceCredits
		// rsSave.Fields["MonthlyBudget"] = rri.MonthlyBudget
		// rsSave.Fields["MonthlyBudgetAdjustments"] = rri.MonthlyBudgetAdjustments
		// rsSave.Fields["Project"] = rri.Project
		// rsSave.Fields["AutomaticBudgetAdjustments"] = rri.AutoBudgetAdjustments
		// rsSave.Fields["PendingEncumbActivity"] = rri.PendingEncumbranceActivity
		// rsSave.Fields["PendingEncumbranceDebits"] = rri.PendingEncumbranceDebits
		// rsSave.Fields["CarryForward"] = rri.CarryForward
		// rsSave.Update
		// Next
		// Next
		// For x = 0 To UBound(aryAccountMaster)
		// Set tempAccount = aryAccountMaster(x)
		// If Not dRevenueSummaries.Exists(tempAccount.Account) And tempAccount.IsRevenue Then
		// rsSave.AddNew
		// rsSave.Fields["Department"] = tempAccount.FirstAccountField
		// If bdSettings.RevenuesUseDivision Then
		// rsSave.Fields["Division"] = tempAccount.SecondAccountField
		// rsSave.Fields["Revenue"] = tempAccount.ThirdAccountField
		//
		// Else
		// rsSave.Fields["Division"] = ""
		// rsSave.Fields["Revenue"] = tempAccount.SecondAccountField
		// End If
		// rsSave.Fields["Account"] = tempAccount.Account
		// rsSave.Fields["period"] = 0
		// rsSave.Fields["originalbudget"] = tempAccount.CurrentBudget
		// rsSave.Fields["posteddebits"] = 0
		// rsSave.Fields["postedcredits"] = 0
		// rsSave.Fields["Pendingdebits"] = 0
		// rsSave.Fields["pendingcredits"] = 0
		// rsSave.Fields["EncumbranceDebits"] = 0
		// rsSave.Fields["encumbrancecredits"] = 0
		// rsSave.Fields["MonthlyBUdget"] = 0
		// rsSave.Fields["MonthlyBudgetAdjustments"] = 0
		// rsSave.Fields["project"] = ""
		// rsSave.Fields["AutomaticBudgetAdjustments"] = 0
		// rsSave.Fields["PendingEncumbActivity"] = 0
		// rsSave.Fields["PendingEncumbranceDebits"] = 0
		// rsSave.Fields["carryforward"] = 0
		// End If
		// Next
		// Call rsSave.BulkCopy("RevenueReportInfo", "Budgetary", True)
		//
		// aryAccts = dExpenseSummaries.Items
		// aryKeys = dExpenseSummaries.Keys
		// Call rsSave.OpenRecordset("select * from ExpenseReportInfo where id = -1", "Budgetary")
		// For x = 0 To UBound(aryAccts)
		// Set summDict = aryAccts(x)
		// aryLSums = summDict.Items
		// dblOriginalBudget = 0
		//
		// If dictAccounts.Exists(aryKeys(x)) Then
		// Set tempAccount = dictAccounts[aryKeys(x])
		// dblOriginalBudget = tempAccount.CurrentBudget
		// End If
		// For y = 0 To UBound(aryLSums)
		// DoEvents
		// If y > 0 Then
		// dblOriginalBudget = 0
		// End If
		// Set eri = aryLSums(y)
		// rsSave.AddNew
		// rsSave.Fields["account"] = eri.Account
		// rsSave.Fields["period"] = eri.Period
		// rsSave.Fields["department"] = eri.Department
		// rsSave.Fields["Division"] = eri.Division
		// rsSave.Fields["Expense"] = eri.Expense
		// rsSave.Fields["Object"] = eri.AccountObject
		// rsSave.Fields["BudgetAdjustments"] = eri.BudgetAdjustments
		// eri.OriginalBudget = dblOriginalBudget
		// rsSave.Fields["OriginalBudget"] = eri.OriginalBudget
		// rsSave.Fields["PostedDebits"] = eri.PostedDebits
		// rsSave.Fields["PostedCredits"] = eri.PostedCredits
		// rsSave.Fields["PendingDebits"] = eri.PendingDebits
		// rsSave.Fields["PendingCredits"] = eri.PendingCredits
		// rsSave.Fields["EncumbranceDebits"] = eri.EncumbranceDebits
		// rsSave.Fields["EncumbranceCredits"] = eri.EncumbranceCredits
		// rsSave.Fields["MonthlyBudget"] = eri.MonthlyBudget
		// rsSave.Fields["MonthlyBudgetAdjustments"] = eri.MonthlyBudgetAdjustments
		// rsSave.Fields["Project"] = eri.Project
		// rsSave.Fields["AutomaticBudgetAdjustments"] = eri.AutoBudgetAdjustments
		// rsSave.Fields["PendingEncumbActivity"] = eri.PendingEncumbranceActivity
		// rsSave.Fields["PendingEncumbranceDebits"] = eri.PendingEncumbranceDebits
		// rsSave.Fields["CarryForward"] = eri.CarryForward
		// rsSave.Update
		// Next
		// Next
		// For x = 0 To UBound(aryAccountMaster)
		// Set tempAccount = aryAccountMaster(x)
		// If Not dExpenseSummaries.Exists(tempAccount.Account) And tempAccount.IsExpense Then
		// rsSave.AddNew
		// rsSave.Fields["Department"] = tempAccount.FirstAccountField
		// If bdSettings.ExpensesUseDivision Then
		// rsSave.Fields["Division"] = tempAccount.SecondAccountField
		// rsSave.Fields["Expense"] = tempAccount.ThirdAccountField
		// If bdSettings.ExpensesUseObject Then
		// rsSave.Fields["object"] = tempAccount.FourthAccountField
		// Else
		// rsSave.Fields["object"] = ""
		// End If
		// Else
		// rsSave.Fields["Division"] = ""
		// rsSave.Fields["expense"] = tempAccount.SecondAccountField
		// If bdSettings.ExpensesUseObject Then
		// rsSave.Fields["object"] = tempAccount.ThirdAccountField
		// Else
		// rsSave.Fields["object"] = ""
		// End If
		// End If
		// rsSave.Fields["Account"] = tempAccount.Account
		// rsSave.Fields["period"] = 0
		// rsSave.Fields["originalbudget"] = tempAccount.CurrentBudget
		// rsSave.Fields["posteddebits"] = 0
		// rsSave.Fields["postedcredits"] = 0
		// rsSave.Fields["Pendingdebits"] = 0
		// rsSave.Fields["pendingcredits"] = 0
		// rsSave.Fields["EncumbranceDebits"] = 0
		// rsSave.Fields["encumbrancecredits"] = 0
		// rsSave.Fields["MonthlyBUdget"] = 0
		// rsSave.Fields["MonthlyBudgetAdjustments"] = 0
		// rsSave.Fields["project"] = ""
		// rsSave.Fields["carryforward"] = 0
		// End If
		// Next
		// Call rsSave.BulkCopy("ExpenseReportInfo", "Budgetary", True)
		//
		// Call rsSave.OpenRecordset("select * from expenseDetailInfo where id = -1", "Budgetary")
		// Do While sdo.ExpenseDetails.IsCurrent
		// DoEvents
		// Set edi = sdo.ExpenseDetails.GetCurrentItem
		// rsSave.AddNew
		// rsSave.Fields["Account"] = edi.Account
		// rsSave.Fields["Period"] = edi.Period
		// rsSave.Fields["Department"] = edi.Department
		// rsSave.Fields["Division"] = edi.Division
		// rsSave.Fields["Expense"] = edi.Expense
		// rsSave.Fields["Object"] = edi.ExpenseObject
		// rsSave.Fields["Status"] = edi.Status
		// rsSave.Fields["PostedDate"] = edi.PostedDate
		// rsSave.Fields["TransDate"] = edi.TransactionDate
		// rsSave.Fields["RCB"] = edi.RCB
		// rsSave.Fields["JournalNumber"] = edi.JournalNumber
		// rsSave.Fields["Description"] = edi.Description
		// rsSave.Fields["Warrant"] = edi.Warrant
		// rsSave.Fields["VendorNumber"] = edi.VendorNumber
		// rsSave.Fields["CheckNumber"] = edi.CheckNumber
		// rsSave.Fields["Amount"] = edi.Amount
		// rsSave.Fields["Discount"] = edi.Discount
		// rsSave.Fields["Encumbrance"] = edi.Encumbrance
		// rsSave.Fields["Adjustments"] = edi.Adjustments
		// rsSave.Fields["Liquidated"] = edi.Liquidated
		// rsSave.Fields["Type"] = edi.JournalType
		// rsSave.Fields["OrderMonth"] = edi.OrderMonth
		// rsSave.Fields["Project"] = edi.Project
		// rsSave.Fields["encumbrancekey"] = edi.EncumbranceKey
		// rsSave.Update
		// sdo.ExpenseDetails.MoveNext
		// Loop
		// Call rsSave.BulkCopy("ExpenseDetailInfo", "Budgetary", True)
		//
		// Call rsSave.OpenRecordset("Select * from revenuedetailinfo where id = -1", "Budgetary")
		// Do While sdo.RevenueDetails.IsCurrent
		// DoEvents
		// Set rdi = sdo.RevenueDetails.GetCurrentItem
		// rsSave.AddNew
		// rsSave.Fields["account"] = rdi.Account
		// rsSave.Fields["period"] = rdi.Period
		// rsSave.Fields["Department"] = rdi.Department
		// rsSave.Fields["Division"] = rdi.Division
		// rsSave.Fields["Revenue"] = rdi.Revenue
		// rsSave.Fields["Status"] = rdi.Status
		// rsSave.Fields["PostedDate"] = rdi.PostedDate
		// rsSave.Fields["TransDate"] = rdi.TransactionDate
		// rsSave.Fields["RCB"] = rdi.RCB
		// rsSave.Fields["JournalNumber"] = rdi.JournalNumber
		// rsSave.Fields["Description"] = rdi.Description
		// rsSave.Fields["Warrant"] = rdi.Warrant
		// rsSave.Fields["CheckNumber"] = rdi.CheckNumber
		// rsSave.Fields["VendorNumber"] = rdi.VendorNumber
		// rsSave.Fields["Amount"] = rdi.Amount
		// rsSave.Fields["Discount"] = rdi.Discount
		// rsSave.Fields["Encumbrance"] = rdi.Encumbrance
		// rsSave.Fields["Adjustments"] = rdi.Adjustments
		// rsSave.Fields["Liquidated"] = rdi.Liquidated
		// rsSave.Fields["Type"] = rdi.JournalType
		// rsSave.Fields["OrderMonth"] = rdi.OrderMonth
		// rsSave.Fields["Project"] = rdi.Project
		// rsSave.Fields["EncumbranceKey"] = rdi.EncumbranceKey
		//
		// rsSave.Update
		// sdo.RevenueDetails.MoveNext
		// Loop
		// Call rsSave.BulkCopy("RevenueDetailInfo", "Budgetary", True)
		// Call rsSave.OpenRecordset("select * from ledgerdetailinfo where id = -1", "Budgetary")
		// Do While sdo.LedgerDetails.IsCurrent
		// DoEvents
		// Set ldi = sdo.LedgerDetails.GetCurrentItem
		// rsSave.AddNew
		// rsSave.Fields["account"] = ldi.Account
		// rsSave.Fields["Period"] = ldi.Period
		// rsSave.Fields["Fund"] = ldi.Fund
		// rsSave.Fields["Acct"] = ldi.LedgerAccount
		// rsSave.Fields["Suffix"] = ldi.Suffix
		// rsSave.Fields["Status"] = ldi.Status
		// rsSave.Fields["PostedDate"] = ldi.PostedDate
		// rsSave.Fields["TransDate"] = ldi.TransactionDate
		// rsSave.Fields["RCB"] = ldi.RCB
		// rsSave.Fields["JournalNumber"] = ldi.JournalNumber
		// rsSave.Fields["Description"] = ldi.Description
		// rsSave.Fields["Warrant"] = ldi.Warrant
		// rsSave.Fields["CheckNumber"] = ldi.CheckNumber
		// rsSave.Fields["VendorNumber"] = ldi.VendorNumber
		// rsSave.Fields["Amount"] = ldi.Amount
		// rsSave.Fields["Discount"] = ldi.Discount
		// rsSave.Fields["Encumbrance"] = ldi.Encumbrance
		// rsSave.Fields["Adjustments"] = ldi.Adjustments
		// rsSave.Fields["Liquidated"] = ldi.Liquidated
		// rsSave.Fields["Type"] = ldi.JournalType
		// rsSave.Fields["OrderMonth"] = ldi.OrderMonth
		// rsSave.Fields["Project"] = ldi.Project
		// rsSave.Fields["EncumbranceKey"] = ldi.EncumbranceKey
		// rsSave.Update
		// sdo.LedgerDetails.MoveNext
		// Loop
		// Call rsSave.BulkCopy("LedgerDetailInfo", "Budgetary", True)
		//
		// Exit Sub
		// ErrorHandler:
		// Call SetError(Err.Number, Err.Description)
		// End Sub
		public cBDReportService() : base()
		{
			bdSettings = bdSetCont.GetBDSettings();
		}
	}
}
