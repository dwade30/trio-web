﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGJCorrDataEntry.
	/// </summary>
	public partial class frmGJCorrDataEntry : BaseForm
	{
		public frmGJCorrDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGJCorrDataEntry InstancePtr
		{
			get
			{
				return (frmGJCorrDataEntry)Sys.GetInstance(typeof(frmGJCorrDataEntry));
			}
		}

		protected frmGJCorrDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int TempCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		string ErrorString = "";
		bool BadAccountFlag;
		int DescriptionCol;
		int AccountCol;
		int AmountCol;
		int DiscountCol;
		int EncumbranceCol;
		int ProjectCol;
		int TaxCol;
		int CorrectedCol;
		int NumberCol;
		int NewAccountCol;
		public bool blnReturn;
		public int CorrRecordNumber;
		int VendorNumber;
		bool blnStopSave;
		bool blnJournalLocked;
		clsGridAccount vsGrid = new clsGridAccount();
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;

		private void cboPeriod_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void frmGJCorrDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			string strLabel = "";
			if (modGlobal.FormExist(this))
			{
				return;
			}
			FillJournalCombo();
			cboJournal.SelectedIndex = 0;
			cboSaveJournal.SelectedIndex = 0;
			if (vs1.Rows <= 13)
			{
				//FC:FINAL:DSE:#535 Top row header is smaller than the rest of rows
				//vs1.HeightOriginal = (vs1.RowHeight(0) * vs1.Rows) + 75;
				vs1.HeightOriginal = (vs1.RowHeight(0) + vs1.RowHeight(1) * (vs1.Rows - 1)) + 75;
			}
			else
			{
				//FC:FINAL:DSE:#535 Top row header is smaller than the rest of rows
				//vs1.HeightOriginal = (vs1.RowHeight(0) * 13) + 75;
				vs1.HeightOriginal = (vs1.RowHeight(0) + vs1.RowHeight(1) * 12) + 75;
			}
			//FC:FINAL:DSE:#537 Save frame should cover the entire window
			this.fraJournalSave.Height = this.vs1.Top + this.vs1.Height;
			if (blnReturn)
			{
				vs1.Enabled = false;
			}
			else
			{
				vs1.Enabled = true;
				vs1.Col = NewAccountCol;
				vs1.Focus();
			}
			//Application.DoEvents();
			this.Refresh();
		}

		private void frmGJCorrDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == NewAccountCol && vs1.Row > 0 && KeyCode != Keys.F9)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmGJCorrDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGJCorrDataEntry.FillStyle	= 0;
			//frmGJCorrDataEntry.ScaleWidth	= 9300;
			//frmGJCorrDataEntry.ScaleHeight	= 7350;
			//frmGJCorrDataEntry.LinkTopic	= "Form2";
			//frmGJCorrDataEntry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			blnJournalLocked = false;
			if (blnReturn)
			{
				vs1.Cols = 9;
				vs1.ExtendLastCol = false;
			}
			else
			{
				vs1.ExtendLastCol = true;
				vs1.Cols = 10;
				NewAccountCol = 9;
				vs1.ColWidth(NewAccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1723939));
				vs1.TextMatrix(0, NewAccountCol, "New Account");
				vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, NewAccountCol, true);
			}
			CorrectedCol = 0;
			NumberCol = 1;
			ProjectCol = 2;
			TaxCol = 3;
			DescriptionCol = 4;
			AccountCol = 5;
			AmountCol = 6;
			DiscountCol = 7;
			EncumbranceCol = 8;
			vs1.ColWidth(CorrectedCol, 0);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(TaxCol, 0);
			vs1.ColWidth(ProjectCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2378979));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2223939));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(EncumbranceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, 4);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, DiscountCol, "Disc");
			vs1.TextMatrix(0, EncumbranceCol, "Enc");
            //FC:FINAL:BSE:#4223 column alignment
            vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(NewAccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(DiscountCol, "#,###.00");
			// set column formats
			vs1.ColFormat(EncumbranceCol, "#,###.00");
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = FCConvert.ToInt16(NewAccountCol);
			vsGrid.Validation = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			blnPostJournal = false;
		}

		private void frmGJCorrDataEntry_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(CorrectedCol, 0);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(TaxCol, 0);
			vs1.ColWidth(ProjectCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2378979));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2223939));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(EncumbranceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			if (!blnReturn)
			{
				vs1.ColWidth(NewAccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1723939));
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
			frmGetGJCorrDataEntry.InstancePtr.Show(App.MainForm);
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void frmGJCorrDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmGJCorrDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
				{
					if (frmGJCorrDataEntry.InstancePtr.ActiveControl.GetName() == "cboJournal")
					{
						KeyAscii = (Keys)0;
						vs1.Focus();
					}
					else
					{
						KeyAscii = (Keys)0;
						Support.SendKeys("{TAB}", false);
					}
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			bool blnShouldSave = false;
			clsDRWrapper rsAltCashInfo = new clsDRWrapper();
			bool blnWrongFund = false;
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			bool blnCreditMemoUsed = false;
			// blnStopSave = False
			// vs1_ValidateEdit vs1.Row, vs1.Col, False
			// cboJournal.SetFocus
			Support.SendKeys("{TAB}", false);
			//Application.DoEvents();
			if (blnStopSave)
			{
				return;
			}
			rsAltCashInfo.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(CorrRecordNumber));
			if (!blnReturn)
			{
				blnShouldSave = false;
				blnWrongFund = false;
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (Strings.InStr(1, vs1.TextMatrix(counter, NewAccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						if (Strings.Trim(vs1.TextMatrix(counter, NewAccountCol)) != "")
						{
							blnShouldSave = true;
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							rsCreditMemo.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + rsAltCashInfo.Get_Fields("JournalNumber") + " AND VendorNumber = " + rsAltCashInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = '" + rsAltCashInfo.Get_Fields("CheckNumber") + "' AND IsNull(CreditMemoRecord, 0) <> 0");
							if (rsCreditMemo.EndOfFile() != true && rsCreditMemo.BeginningOfFile() != true)
							{
								blnCreditMemoUsed = true;
							}
							else
							{
								blnCreditMemoUsed = false;
							}
							if (FCConvert.ToBoolean(rsAltCashInfo.Get_Fields_Boolean("UseAlternateCash")) || blnCreditMemoUsed)
							{
								if (modBudgetaryMaster.GetAccountFund(vs1.TextMatrix(counter, NewAccountCol)) != modBudgetaryMaster.GetAccountFund(vs1.TextMatrix(counter, AccountCol)))
								{
									blnShouldSave = false;
									blnWrongFund = true;
									break;
								}
							}
						}
					}
					if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, NewAccountCol)))
					{
						answer = MessageBox.Show("Account " + vs1.TextMatrix(counter, NewAccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.No)
						{
							vs1.Select(counter, NewAccountCol);
							vs1.Focus();
							return;
						}
						else
						{
							modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "AP Cortrection Data Entry");
						}
					}
				}
				if (!blnShouldSave)
				{
					if (blnWrongFund)
					{
						if (blnCreditMemoUsed)
						{
							MessageBox.Show("Because the AP entry you are trying to correct used a credit memo the fund of the new account must match the fund of the old account.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("Because the AP entry you are trying to correct used an alternate cash account the fund of the new account must match the fund of the old account.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						MessageBox.Show("You must change at least one account before you may save.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					return;
				}
			}
			if (cboJournal.SelectedIndex == 0)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Cancel)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else if (answer == DialogResult.No)
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
					lblJournalSave.Visible = true;
					cboSaveJournal.Visible = true;
					lblJournalDescription.Visible = false;
					txtJournalDescription.Visible = false;
					cboSaveJournal.Focus();
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
					lblJournalSave.Visible = false;
					cboSaveJournal.Visible = false;
					lblJournalDescription.Visible = true;
					txtJournalDescription.Visible = true;
					txtJournalDescription.Focus();
					return;
				}
			}
			else
			{
				SaveJournal();
			}
		}

		public void mnuProcessSave_Click()
		{
			//FC:FINAL:ASZ: mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
			mnuProcessSave_Click(null, new System.EventArgs());
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				if (vs1.Col == NewAccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, NewAccountCol));
			if (vs1.Col == NewAccountCol)
			{
				// if we are in the first column
				vs1.Focus();
				vs1.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				if (vs1.Col < DescriptionCol)
				{
					vs1.Col = DescriptionCol;
				}
				vs1.Editable = FCGrid.EditableSettings.flexEDNone;
				vs1.Focus();
			}
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:DSE:#537 Fix combo size
			cboSaveJournal.Width = 90;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			clsDRWrapper rsEncData = new clsDRWrapper();
			txtVendor.Focus();
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			bool executeAddTag = false;
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(cboPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(cboSaveJournal.ItemData(cboSaveJournal.SelectedIndex)), "Budgetary");
				if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
				{
					executeAddTag = true;
					goto AddTag;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(cboPeriod.Text))
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(cboPeriod.Text)));
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(cboSaveJournal.ItemData(cboSaveJournal.SelectedIndex)) + ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", "Add Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
							{
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)) == Master.Get_Fields("JournalNumber") && cboSaveJournal.ItemData(cboSaveJournal.SelectedIndex) == Conversion.Val(cboPeriod.Text))
								{
									cboSaveJournal.SelectedIndex = counter;
									cboJournal.SelectedIndex = counter;
									break;
								}
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
					else
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(cboSaveJournal.ItemData(cboSaveJournal.SelectedIndex)) + ".  Do you wish to use this Journal Number?", "Update Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (Strings.Trim(txtJournalDescription.Text) != "")
							{
								executeAddTag = true;
								goto AddTag;
							}
							else
							{
								fraJournalSave.Visible = true;
								lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
								lblJournalSave.Visible = false;
								cboSaveJournal.Visible = false;
								lblJournalDescription.Visible = true;
								txtJournalDescription.Visible = true;
								txtJournalDescription.Focus();
								return;
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					Master.AddNew();
					Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("Description", txtJournalDescription.Text);
					Master.Set_Fields("Type", "AC");
					Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(cboPeriod.Text)));
					Master.Update();
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			clsJournalInfo.JournalType = "AC";
			clsJournalInfo.Period = cboPeriod.Text;
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			rs.OmitNullsOnInsert = true;
			rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
			rs.AddNew();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				rs.Set_Fields("JournalNumber", TempJournal);
			}
			rs.Set_Fields("VendorNumber", txtVendor.Text);
			if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
			{
				rs.Set_Fields("TempVendorName", txtAddress[0].Text);
				rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
				rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
				rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
				rs.Set_Fields("TempVendorCity", txtCity.Text);
				rs.Set_Fields("TempVendorState", txtState.Text);
				rs.Set_Fields("TempVendorZip", txtZip.Text);
				rs.Set_Fields("TempVendorZip4", txtZip4.Text);
			}
			rs.Set_Fields("Period", cboPeriod.Text);
			rs.Set_Fields("Description", txtDescription.Text);
			rs.Set_Fields("Reference", txtReference.Text);
			if (blnReturn)
			{
				rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text) * -1);
				rs.Set_Fields("Returned", "D");
			}
			else
			{
				rs.Set_Fields("Amount", 0);
			}
			rs.Set_Fields("CheckNumber", txtCheck.Text);
			rs.Set_Fields("Status", "E");
			// If blnReturn Then
			rsEncData.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(CorrRecordNumber));
			if (rsEncData.EndOfFile() != true && rsEncData.BeginningOfFile() != true)
			{
				// rs.Fields["EncumbranceRecord"] = Val(rsEncData.Fields["EncumbranceRecord"])
				rs.Set_Fields("UseAlternateCash", rsEncData.Get_Fields_Boolean("UseAlternateCash"));
				rs.Set_Fields("AlternateCashAccount", rsEncData.Get_Fields_String("AlternateCashAccount"));
				rs.Set_Fields("CheckDate", rsEncData.Get_Fields_DateTime("CheckDate"));
				rs.Set_Fields("Payable", rsEncData.Get_Fields_DateTime("CheckDate"));
				// rsEncData.Edit
				// rsEncData.Fields["Returned"] = "R"
				// rsEncData.Update
			}
			// End If
			rs.Update();
			VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			SaveDetails();
			if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptAccountsPayableCorrections))
			{
				if (MessageBox.Show("Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					blnPostJournal = true;
				}
				else
				{
					blnPostJournal = false;
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
				}
			}
			else
			{
				blnPostJournal = false;
				if (cboSaveJournal.SelectedIndex != 0)
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
				}
			}
			Close();
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				while (!rsJournalPeriod.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "Per " + rsJournalPeriod.Get_Fields("Period") + " - " + rsJournalPeriod.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [period] and replace with corresponding Get_Field method
					cboJournal.ItemData(cboJournal.NewIndex, FCConvert.ToInt32(Conversion.Val(rsJournalPeriod.Get_Fields("period"))));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "Per " + rsJournalPeriod.Get_Fields("Period") + " - " + rsJournalPeriod.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [period] and replace with corresponding Get_Field method
					cboSaveJournal.ItemData(cboSaveJournal.NewIndex, FCConvert.ToInt32(Conversion.Val(rsJournalPeriod.Get_Fields("period"))));
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SaveDetails()
		{
			int counter;
			Decimal TotalAmount;
			clsDRWrapper rsCorrect = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsEncInfo = new clsDRWrapper();
			rsDetailInfo.OmitNullsOnInsert = true;
			if (blnReturn)
			{
				rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					// save the grid information
					rsDetailInfo.AddNew();
					// if so add it
					rsDetailInfo.Set_Fields("VendorNumber", VendorNumber);
					if (("VOID - " + vs1.TextMatrix(counter, DescriptionCol)).Length > 25)
					{
						rsDetailInfo.Set_Fields("Description", "VOID - " + Strings.Left(vs1.TextMatrix(counter, DescriptionCol), 18));
					}
					else
					{
						rsDetailInfo.Set_Fields("Description", "VOID - " + vs1.TextMatrix(counter, DescriptionCol));
					}
					rsDetailInfo.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
					rsDetailInfo.Set_Fields("Amount", (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) * -1));
					rsDetailInfo.Set_Fields("Discount", (FCConvert.ToDecimal(vs1.TextMatrix(counter, DiscountCol)) * -1));
					rsDetailInfo.Set_Fields("Encumbrance", (FCConvert.ToDecimal(vs1.TextMatrix(counter, EncumbranceCol)) * -1));
					rsDetailInfo.Set_Fields("Project", vs1.TextMatrix(counter, ProjectCol));
					rsDetailInfo.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
					// TextMatrix(counter, TaxCol)
					rsDetailInfo.Set_Fields("RCB", "C");
					rsCorrect.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
					if (rsCorrect.EndOfFile() != true && rsCorrect.BeginningOfFile() != true)
					{
						rsDetailInfo.Set_Fields("EncumbranceDetailRecord", FCConvert.ToString(Conversion.Val(rsCorrect.Get_Fields_Int32("EncumbranceDetailRecord"))));
					}
					else
					{
						rsDetailInfo.Set_Fields("EncumbranceDetailRecord", 0);
					}
					rsDetailInfo.Update();
					// update the database
				}
			}
			else
			{
				rsDetailInfo.OmitNullsOnInsert = true;
				rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					// save the grid information
					if (vs1.TextMatrix(counter, NewAccountCol) != "")
					{
						if (modValidateAccount.AccountValidate(vs1.TextMatrix(counter, NewAccountCol)))
						{
							rsDetailInfo.AddNew();
							// if so add it
							rsDetailInfo.Set_Fields("APJournalID", VendorNumber);
							rsDetailInfo.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
							rsDetailInfo.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
							rsDetailInfo.Set_Fields("Amount", (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) * -1));
							rsDetailInfo.Set_Fields("Discount", (FCConvert.ToDecimal(vs1.TextMatrix(counter, DiscountCol)) * -1));
							// Dave 12/28/05 Dont want to reverse the encumbrance entries
							// rsDetailInfo.Fields["Encumbrance"] = (CCur(vs1.TextMatrix(counter, EncumbranceCol)) * -1)
							rsDetailInfo.Set_Fields("Encumbrance", 0);
							rsDetailInfo.Set_Fields("Project", vs1.TextMatrix(counter, ProjectCol));
							rsDetailInfo.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
							// TextMatrix(counter, TaxCol)
							rsDetailInfo.Set_Fields("RCB", "C");
							rsDetailInfo.Set_Fields("Corrected", true);
							rsDetailInfo.Update();
							// update the database
							rsDetailInfo.AddNew();
							// if so add it
							rsDetailInfo.Set_Fields("APJournalID", VendorNumber);
							rsDetailInfo.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
							rsDetailInfo.Set_Fields("account", vs1.TextMatrix(counter, NewAccountCol));
							rsDetailInfo.Set_Fields("Amount", vs1.TextMatrix(counter, AmountCol));
							rsDetailInfo.Set_Fields("Discount", vs1.TextMatrix(counter, DiscountCol));
							// Dave 12/28/05 Dont want to reverse the encumbrance entries
							// rsDetailInfo.Fields["Encumbrance"] = vs1.TextMatrix(counter, EncumbranceCol)
							rsDetailInfo.Set_Fields("Encumbrance", 0);
							rsDetailInfo.Set_Fields("Project", vs1.TextMatrix(counter, ProjectCol));
							rsDetailInfo.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
							// TextMatrix(counter, TaxCol)
							rsDetailInfo.Set_Fields("RCB", "R");
							rsDetailInfo.Set_Fields("Corrected", false);
							rsDetailInfo.Update();
							// update the database
							rsCorrect.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
							if (rsCorrect.EndOfFile() != true && rsCorrect.BeginningOfFile() != true)
							{
								rsCorrect.Edit();
								rsCorrect.Set_Fields("Corrected", true);
								//FC:FINAL:ASZ: rsCorrect.Set_Fields("CorrectedAmount", Conversion.Val(rsCorrect.Get_Fields("CorrectedAmount"))) + (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) * -1));
								rsCorrect.Set_Fields("CorrectedAmount", rsCorrect.Get_Fields_Decimal("CorrectedAmount") + (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) * -1));
								rsCorrect.Update();
							}
						}
					}
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == NewAccountCol)
			{
				e.Cancel = modNewAccountBox.CheckAccountValidate(vs1, row, col, e.Cancel);
			}
			blnStopSave = e.Cancel;
		}
	}
}
