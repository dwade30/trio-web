﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetPurchaseOrders.
	/// </summary>
	partial class frmGetPurchaseOrders : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdGet;
		public FCGrid vs1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetPurchaseOrders));
			this.cmbSearchType = new fecherFoundation.FCComboBox();
			this.lblSearchType = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdGet = new fecherFoundation.FCButton();
			this.vs1 = new fecherFoundation.FCGrid();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
			this.BottomPanel.Location = new System.Drawing.Point(0, 522);
			this.BottomPanel.Size = new System.Drawing.Size(1102, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.txtGetAccountNumber);
			this.ClientArea.Controls.Add(this.lblLastAccount);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1102, 462);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1102, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(244, 30);
			this.HeaderText.Text = "Get Purchase Orders";
			// 
			// cmbSearchType
			// 
			this.cmbSearchType.AutoSize = false;
			this.cmbSearchType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSearchType.FormattingEnabled = true;
			this.cmbSearchType.Items.AddRange(new object[] {
				"Purchase Order ",
				"Vendor Number",
				"Vendor Name",
				"Description ",
				"Department"
			});
			this.cmbSearchType.Location = new System.Drawing.Point(231, 30);
			this.cmbSearchType.Name = "cmbSearchType";
			this.cmbSearchType.Size = new System.Drawing.Size(370, 40);
			this.cmbSearchType.TabIndex = 13;
			this.cmbSearchType.SelectedIndexChanged += new System.EventHandler(this.optSearchType_MouseDown);
			// 
			// lblSearchType
			// 
			this.lblSearchType.AutoSize = true;
			this.lblSearchType.Location = new System.Drawing.Point(20, 44);
			this.lblSearchType.Name = "lblSearchType";
			this.lblSearchType.Size = new System.Drawing.Size(79, 15);
			this.lblSearchType.TabIndex = 14;
			this.lblSearchType.Text = "SEARCH BY";
			// 
			// Frame3
			// 
			this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Frame3.BackColor = System.Drawing.Color.White;
			this.Frame3.Controls.Add(this.cmdReturn);
			this.Frame3.Controls.Add(this.cmdGet);
			this.Frame3.Controls.Add(this.vs1);
			this.Frame3.Controls.Add(this.Label3);
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Location = new System.Drawing.Point(30, 126);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(1042, 482);
			this.Frame3.TabIndex = 0;
			this.Frame3.Text = "Multiple Records";
			this.Frame3.Visible = false;
			// 
			// cmdReturn
			// 
			this.cmdReturn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(20, 428);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(220, 40);
			this.cmdReturn.TabIndex = 3;
			this.cmdReturn.Text = "Return to Search Screen";
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// cmdGet
			// 
			this.cmdGet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdGet.AppearanceKey = "actionButton";
			this.cmdGet.Location = new System.Drawing.Point(260, 428);
			this.cmdGet.Name = "cmdGet";
			this.cmdGet.Size = new System.Drawing.Size(155, 40);
			this.cmdGet.TabIndex = 2;
			this.cmdGet.Text = "Retrieve Record";
			this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(20, 30);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 50;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1002, 388);
			this.vs1.StandardTab = true;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vs1.TabIndex = 1;
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
			// 
			// Label3
			// 
			this.Label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Label3.BorderStyle = 1;
			this.Label3.Location = new System.Drawing.Point(20, 104);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(23, 20);
			this.Label3.TabIndex = 5;
			this.Label3.Visible = false;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(113, 104);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(117, 18);
			this.Label4.TabIndex = 4;
			this.Label4.Text = "DELETED VENDOR";
			this.Label4.Visible = false;
			// 
			// Frame2
			// 
			this.Frame2.BackColor = System.Drawing.Color.White;
			this.Frame2.Controls.Add(this.txtSearch);
			this.Frame2.Controls.Add(this.cmbSearchType);
			this.Frame2.Controls.Add(this.lblSearchType);
			this.Frame2.Controls.Add(this.cmdSearch);
			this.Frame2.Controls.Add(this.cmdClear);
			this.Frame2.Controls.Add(this.lblSearchInfo);
			this.Frame2.Location = new System.Drawing.Point(30, 126);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(621, 210);
			this.Frame2.TabIndex = 9;
			this.Frame2.Text = "Search";
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(231, 90);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(370, 40);
			this.txtSearch.TabIndex = 12;
			this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(381, 150);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(87, 40);
			this.cmdSearch.TabIndex = 11;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.AppearanceKey = "actionButton";
			this.cmdClear.Location = new System.Drawing.Point(231, 150);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(130, 40);
			this.cmdClear.TabIndex = 10;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.AutoSize = true;
			this.lblSearchInfo.Location = new System.Drawing.Point(20, 104);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(165, 15);
			this.lblSearchInfo.TabIndex = 13;
			this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "toolbarButton";
			this.cmdQuit.Location = new System.Drawing.Point(209, 233);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(100, 26);
			this.cmdQuit.TabIndex = 8;
			this.cmdQuit.Text = "Quit";
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.AutoSize = false;
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(543, 66);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(108, 40);
			this.txtGetAccountNumber.TabIndex = 7;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(423, 30);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(100, 48);
			this.cmdGetAccountNumber.TabIndex = 6;
			this.cmdGetAccountNumber.Text = "Process";
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblLastAccount.Location = new System.Drawing.Point(256, 30);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(55, 16);
			this.lblLastAccount.TabIndex = 16;
			// 
			// Label2
			// 
			this.Label2.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(229, 16);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "LAST PURCHASE ORDER ACCESSED ...";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 80);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(461, 16);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "ENTER THE PURCHASE ORDER NUMBER OR 0 TO ADD A NEW PURCHASE ORDER";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			// 
			// frmGetPurchaseOrders
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1102, 630);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGetPurchaseOrders";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Get Purchase Orders";
			this.Load += new System.EventHandler(this.frmGetPurchaseOrders_Load);
			this.Activated += new System.EventHandler(this.frmGetPurchaseOrders_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetPurchaseOrders_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetPurchaseOrders_KeyPress);
			this.Resize += new System.EventHandler(this.frmGetPurchaseOrders_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
