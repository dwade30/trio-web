﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrantPreview.
	/// </summary>
	partial class frmWarrantPreview : BaseForm
	{
		public fecherFoundation.FCCheckBox chkEnteredOrder;
		public fecherFoundation.FCFrame fraChoose;
		public fecherFoundation.FCComboBox cboPreviewAccount;
		public fecherFoundation.FCLabel lblTownAccounts;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCFrame fraPayDate;
		public Global.T2KDateBox txtPayDate;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWarrantPreview));
			this.chkEnteredOrder = new fecherFoundation.FCCheckBox();
			this.fraChoose = new fecherFoundation.FCFrame();
			this.cboPreviewAccount = new fecherFoundation.FCComboBox();
			this.lblTownAccounts = new fecherFoundation.FCLabel();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.fraPayDate = new fecherFoundation.FCFrame();
			this.txtPayDate = new Global.T2KDateBox();
			this.lblInstruct = new fecherFoundation.FCLabel();
			this.vsJournals = new fecherFoundation.FCGrid();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEnteredOrder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChoose)).BeginInit();
			this.fraChoose.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).BeginInit();
			this.fraPayDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Controls.Add(this.cmdSelect);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPayDate);
			this.ClientArea.Controls.Add(this.fraChoose);
			this.ClientArea.Controls.Add(this.chkEnteredOrder);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.vsJournals);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(194, 30);
			this.HeaderText.Text = "Warrant Preview";
			// 
			// chkEnteredOrder
			// 
			this.chkEnteredOrder.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.chkEnteredOrder.Location = new System.Drawing.Point(30, 483);
			this.chkEnteredOrder.Name = "chkEnteredOrder";
			this.chkEnteredOrder.Size = new System.Drawing.Size(274, 27);
			this.chkEnteredOrder.TabIndex = 14;
			this.chkEnteredOrder.Text = "Display Preview by Entered Order";
			this.chkEnteredOrder.Visible = false;
			// 
			// fraChoose
			// 
			this.fraChoose.Controls.Add(this.cboPreviewAccount);
			this.fraChoose.Controls.Add(this.lblTownAccounts);
			this.fraChoose.Location = new System.Drawing.Point(30, 23);
			this.fraChoose.Name = "fraChoose";
			this.fraChoose.Size = new System.Drawing.Size(487, 124);
			this.fraChoose.TabIndex = 10;
			this.fraChoose.Text = "Please Select A Reporting Option";
			this.fraChoose.Visible = false;
			// 
			// cboPreviewAccount
			// 
			this.cboPreviewAccount.AutoSize = false;
			this.cboPreviewAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboPreviewAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPreviewAccount.FormattingEnabled = true;
			this.cboPreviewAccount.Items.AddRange(new object[] {
				"1 - Do Not Print Account Titles",
				"2 - Print Dept/Div - Exp/Obj // Dept/Div - Rev Titles",
				"3 - Print Dept/Div - Dept/Div Titles",
				"4 - Print Exp/Obj - Rev Titles"
			});
			this.cboPreviewAccount.Location = new System.Drawing.Point(20, 64);
			this.cboPreviewAccount.Name = "cboPreviewAccount";
			this.cboPreviewAccount.Size = new System.Drawing.Size(440, 40);
			this.cboPreviewAccount.TabIndex = 12;
			// 
			// lblTownAccounts
			// 
			this.lblTownAccounts.Location = new System.Drawing.Point(20, 30);
			this.lblTownAccounts.Name = "lblTownAccounts";
			this.lblTownAccounts.Size = new System.Drawing.Size(113, 14);
			this.lblTownAccounts.TabIndex = 13;
			this.lblTownAccounts.Text = "TOWN ACCOUNTS";
			this.lblTownAccounts.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTownAccounts.Visible = false;
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "acceptButton";
			this.cmdSelect.Location = new System.Drawing.Point(160, 30);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(100, 48);
			this.cmdSelect.TabIndex = 9;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Visible = false;
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// fraPayDate
			// 
			this.fraPayDate.Controls.Add(this.txtPayDate);
			this.fraPayDate.Controls.Add(this.lblInstruct);
			this.fraPayDate.Location = new System.Drawing.Point(30, 28);
			this.fraPayDate.Name = "fraPayDate";
			this.fraPayDate.Size = new System.Drawing.Size(220, 130);
			this.fraPayDate.TabIndex = 0;
			this.fraPayDate.Text = "Pay Date";
			// 
			// txtPayDate
			// 
			this.txtPayDate.Location = new System.Drawing.Point(20, 70);
			this.txtPayDate.Mask = "00/00/0000";
			this.txtPayDate.MaxLength = 10;
			this.txtPayDate.Name = "txtPayDate";
			this.txtPayDate.Size = new System.Drawing.Size(115, 40);
			this.txtPayDate.TabIndex = 2;
			this.txtPayDate.Text = "  /  /";
			// 
			// lblInstruct
			// 
			this.lblInstruct.Location = new System.Drawing.Point(20, 30);
			this.lblInstruct.Name = "lblInstruct";
			this.lblInstruct.Size = new System.Drawing.Size(175, 20);
			this.lblInstruct.TabIndex = 3;
			this.lblInstruct.Text = "PLEASE ENTER THE PAY DATE";
			this.lblInstruct.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// vsJournals
			// 
			this.vsJournals.AllowSelection = false;
			this.vsJournals.AllowUserToResizeColumns = false;
			this.vsJournals.AllowUserToResizeRows = false;
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsJournals.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsJournals.BackColorBkg = System.Drawing.Color.Empty;
			this.vsJournals.BackColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.BackColorSel = System.Drawing.Color.Empty;
			this.vsJournals.Cols = 6;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsJournals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsJournals.ColumnHeadersHeight = 30;
			this.vsJournals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsJournals.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsJournals.DragIcon = null;
			this.vsJournals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsJournals.FixedCols = 0;
			this.vsJournals.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.FrozenCols = 0;
			this.vsJournals.GridColor = System.Drawing.Color.Empty;
			this.vsJournals.GridColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.Location = new System.Drawing.Point(30, 70);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.ReadOnly = true;
			this.vsJournals.RowHeadersVisible = false;
			this.vsJournals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsJournals.RowHeightMin = 0;
			this.vsJournals.Rows = 50;
			this.vsJournals.ScrollTipText = null;
			this.vsJournals.ShowColumnVisibilityMenu = false;
			this.vsJournals.Size = new System.Drawing.Size(1018, 404);
			this.vsJournals.StandardTab = true;
			this.vsJournals.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsJournals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsJournals.TabIndex = 4;
			this.vsJournals.Visible = false;
			this.vsJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsJournals_KeyDownEvent);
			this.vsJournals.Click += new System.EventHandler(this.vsJournals_ClickEvent);
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(0, 111);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(66, 24);
			this.lblTitle.TabIndex = 8;
			this.lblTitle.Text = "LABEL2";
			this.lblTitle.Visible = false;
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(811, 34);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "PLEASE SELECT THE JOURNAL(S) YOU WISH TO PREVIEW AND THEN CLICK THE PROCESS BUTTO" + "N TO CONTINUE";
			this.Label1.Visible = false;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(100, 48);
			this.btnProcess.TabIndex = 12;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmWarrantPreview
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmWarrantPreview";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Warrant Preview";
			this.Load += new System.EventHandler(this.frmWarrantPreview_Load);
			this.Activated += new System.EventHandler(this.frmWarrantPreview_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmWarrantPreview_KeyPress);
			this.Resize += new System.EventHandler(this.frmWarrantPreview_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEnteredOrder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChoose)).EndInit();
			this.fraChoose.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).EndInit();
			this.fraPayDate.ResumeLayout(false);
			this.fraPayDate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCButton btnProcess;
	}
}
