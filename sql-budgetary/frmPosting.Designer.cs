﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPosting.
	/// </summary>
	partial class frmPosting : BaseForm
	{
		public fecherFoundation.FCGrid vsCheckDates;
		public fecherFoundation.FCCheckBox chkBreak;
		public fecherFoundation.FCButton cmdPost;
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPosting));
			this.vsCheckDates = new fecherFoundation.FCGrid();
			this.chkBreak = new fecherFoundation.FCCheckBox();
			this.cmdPost = new fecherFoundation.FCButton();
			this.vsJournals = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCheckDates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBreak)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPost);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsCheckDates);
			this.ClientArea.Controls.Add(this.chkBreak);
			this.ClientArea.Controls.Add(this.vsJournals);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(182, 30);
			this.HeaderText.Text = "Journal Posting";
			// 
			// vsCheckDates
			// 
			this.vsCheckDates.AllowSelection = false;
			this.vsCheckDates.AllowUserToResizeColumns = false;
			this.vsCheckDates.AllowUserToResizeRows = false;
			this.vsCheckDates.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCheckDates.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCheckDates.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCheckDates.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCheckDates.BackColorSel = System.Drawing.Color.Empty;
			this.vsCheckDates.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCheckDates.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsCheckDates.ColumnHeadersHeight = 30;
			this.vsCheckDates.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsCheckDates.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCheckDates.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsCheckDates.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCheckDates.FixedCols = 0;
			this.vsCheckDates.FixedRows = 0;
			this.vsCheckDates.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCheckDates.FrozenCols = 0;
			this.vsCheckDates.GridColor = System.Drawing.Color.Empty;
			this.vsCheckDates.Location = new System.Drawing.Point(997, 7);
			this.vsCheckDates.Name = "vsCheckDates";
			this.vsCheckDates.ReadOnly = true;
			this.vsCheckDates.RowHeadersVisible = false;
			this.vsCheckDates.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCheckDates.Rows = 50;
			this.vsCheckDates.ShowColumnVisibilityMenu = false;
			this.vsCheckDates.Size = new System.Drawing.Size(37, 39);
			this.vsCheckDates.StandardTab = true;
			this.vsCheckDates.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsCheckDates.TabIndex = 5;
			this.vsCheckDates.Visible = false;
			// 
			// chkBreak
			// 
			this.chkBreak.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.chkBreak.Checked = true;
			this.chkBreak.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkBreak.Location = new System.Drawing.Point(30, 390);
			this.chkBreak.Name = "chkBreak";
			this.chkBreak.Size = new System.Drawing.Size(252, 26);
			this.chkBreak.TabIndex = 4;
			this.chkBreak.Text = "Page break after each journal?";
			// 
			// cmdPost
			// 
			this.cmdPost.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdPost.AppearanceKey = "acceptButton";
			this.cmdPost.Location = new System.Drawing.Point(427, 30);
			this.cmdPost.Name = "cmdPost";
			this.cmdPost.Size = new System.Drawing.Size(140, 48);
			this.cmdPost.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPost.TabIndex = 1;
			this.cmdPost.Text = "Post Journal(s)";
			this.cmdPost.Click += new System.EventHandler(this.cmdPost_Click);
			// 
			// vsJournals
			// 
			this.vsJournals.AllowSelection = false;
			this.vsJournals.AllowUserToResizeColumns = false;
			this.vsJournals.AllowUserToResizeRows = false;
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsJournals.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsJournals.BackColorBkg = System.Drawing.Color.Empty;
			this.vsJournals.BackColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.BackColorSel = System.Drawing.Color.Empty;
			this.vsJournals.Cols = 6;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsJournals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsJournals.ColumnHeadersHeight = 30;
			this.vsJournals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsJournals.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsJournals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsJournals.FixedCols = 0;
			this.vsJournals.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.FrozenCols = 0;
			this.vsJournals.GridColor = System.Drawing.Color.Empty;
			this.vsJournals.Location = new System.Drawing.Point(30, 62);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.ReadOnly = true;
			this.vsJournals.RowHeadersVisible = false;
			this.vsJournals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsJournals.Rows = 50;
			this.vsJournals.ShowColumnVisibilityMenu = false;
			this.vsJournals.Size = new System.Drawing.Size(1018, 308);
			this.vsJournals.StandardTab = true;
			this.vsJournals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsJournals.TabIndex = 2;
			this.vsJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsJournals_KeyDownEvent);
			this.vsJournals.Click += new System.EventHandler(this.vsJournals_ClickEvent);
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(640, 16);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "PLEASE SELECT THE JOURNAL(S) TO BE POSTED AND THEN CLICK THE POST JOURNAL(S) BUTT" + "ON TO CONTINUE";
			// 
			// frmPosting
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPosting";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Journal Posting";
			this.Load += new System.EventHandler(this.frmPosting_Load);
			this.Activated += new System.EventHandler(this.frmPosting_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPosting_KeyPress);
			this.Resize += new System.EventHandler(this.frmPosting_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCheckDates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBreak)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
