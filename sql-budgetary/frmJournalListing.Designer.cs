//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmJournalListing.
	/// </summary>
	partial class frmJournalListing : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCGrid vsWhere;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJournalListing));
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.cmbReport = new fecherFoundation.FCComboBox();
			this.lblReport = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraMessage = new fecherFoundation.FCFrame();
			this.Label3 = new fecherFoundation.FCLabel();
			this.vsLayout = new fecherFoundation.FCGrid();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraFields = new fecherFoundation.FCFrame();
			this.lstFields = new fecherFoundation.FCDraggableListBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboSavedReport = new fecherFoundation.FCComboBox();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.fraWhere = new fecherFoundation.FCFrame();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.cmdClear = new fecherFoundation.FCButton();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
			this.fraMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
			this.vsLayout.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 682);
			this.BottomPanel.Size = new System.Drawing.Size(1026, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Size = new System.Drawing.Size(1026, 622);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(1026, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(250, 30);
			this.HeaderText.Text = "Journal Summary List";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbReport
			// 
			this.cmbReport.AutoSize = false;
			this.cmbReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbReport.FormattingEnabled = true;
			this.cmbReport.Items.AddRange(new object[] {
				"Create New Report",
				"Show Saved Report",
				"Delete Saved Report"
			});
			this.cmbReport.Location = new System.Drawing.Point(89, 30);
			this.cmbReport.Name = "cmbReport";
			this.cmbReport.Size = new System.Drawing.Size(193, 40);
			this.cmbReport.TabIndex = 1;
			this.cmbReport.Text = "Create New Report";
			this.ToolTip1.SetToolTip(this.cmbReport, null);
			this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
			// 
			// lblReport
			// 
			this.lblReport.Location = new System.Drawing.Point(20, 44);
			this.lblReport.Name = "lblReport";
			this.lblReport.Size = new System.Drawing.Size(50, 16);
			this.lblReport.TabIndex = 0;
			this.lblReport.Text = "REPORT";
			this.ToolTip1.SetToolTip(this.lblReport, null);
			// 
			// Frame3
			// 
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.fraMessage);
			this.Frame3.Controls.Add(this.vsLayout);
			this.Frame3.Controls.Add(this.Image1);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Controls.Add(this.Label2);
			this.Frame3.Location = new System.Drawing.Point(0, 0);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(1023, 224);
			this.Frame3.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// fraMessage
			// 
			this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraMessage.AppearanceKey = "groupBoxNoBorders";
			this.fraMessage.Controls.Add(this.Label3);
			this.fraMessage.Location = new System.Drawing.Point(69, 72);
			this.fraMessage.Name = "fraMessage";
			this.fraMessage.Size = new System.Drawing.Size(934, 133);
			this.fraMessage.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.fraMessage, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(97, 39);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(734, 21);
			this.Label3.TabIndex = 0;
			this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// vsLayout
			// 
			this.vsLayout.AllowSelection = false;
			this.vsLayout.AllowUserToResizeColumns = false;
			this.vsLayout.AllowUserToResizeRows = false;
			this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsLayout.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLayout.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLayout.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLayout.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.BackColorSel = System.Drawing.Color.Empty;
			this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsLayout.Cols = 0;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLayout.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsLayout.ColumnHeadersHeight = 30;
			this.vsLayout.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLayout.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsLayout.DragIcon = null;
			this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLayout.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLayout.Enabled = false;
			this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.vsLayout.ExtendLastCol = true;
			this.vsLayout.FixedCols = 0;
			this.vsLayout.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.FrozenCols = 0;
			this.vsLayout.GridColor = System.Drawing.Color.Empty;
			this.vsLayout.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.Location = new System.Drawing.Point(69, 72);
			this.vsLayout.Margin = new Wisej.Web.Padding(0);
			this.vsLayout.Name = "vsLayout";
			this.vsLayout.RowHeadersVisible = false;
			this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLayout.RowHeightMin = 0;
			this.vsLayout.Rows = 1;
			this.vsLayout.ScrollTipText = null;
			this.vsLayout.ShowColumnVisibilityMenu = false;
			this.vsLayout.ShowFocusCell = false;
			this.vsLayout.Size = new System.Drawing.Size(934, 133);
			this.vsLayout.StandardTab = true;
			this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLayout.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLayout.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.vsLayout, null);
			this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
			this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
			// 
			// Image1
			// 
			this.Image1.AllowDrop = true;
			this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.DrawStyle = ((short)(0));
			this.Image1.DrawWidth = ((short)(1));
			this.Image1.FillStyle = ((short)(1));
			this.Image1.FontTransparent = true;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(70, 30);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(933, 22);
			this.Image1.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.Image1, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 72);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(9, 60);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "L E F T";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(49, 72);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(9, 86);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "M A R G I N";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstFields);
			this.fraFields.Location = new System.Drawing.Point(30, 224);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(300, 210);
			this.fraFields.TabIndex = 1;
			this.fraFields.Text = "Fields To Display";
			this.ToolTip1.SetToolTip(this.fraFields, "Double Click to select all");
			this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
			// 
			// lstFields
			// 
			this.lstFields.Appearance = 0;
			this.lstFields.BackColor = System.Drawing.SystemColors.Window;
			this.lstFields.CheckBoxes = true;
			this.lstFields.Location = new System.Drawing.Point(20, 30);
			this.lstFields.MultiSelect = 0;
			this.lstFields.Name = "lstFields";
			this.lstFields.Size = new System.Drawing.Size(260, 160);
			this.lstFields.Sorted = false;
			this.lstFields.Style = 1;
			this.lstFields.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lstFields, null);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(331, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(123, 48);
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print Preview";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
			this.cmdPrint.Click += new System.EventHandler(this.mnuPreview_Click);
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Enabled = false;
			this.fraSort.Location = new System.Drawing.Point(350, 224);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(300, 210);
			this.fraSort.TabIndex = 2;
			this.fraSort.Text = "Fields To Sort By";
			this.ToolTip1.SetToolTip(this.fraSort, null);
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.Appearance = 0;
			this.lstSort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.lstSort.CheckBoxes = true;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.MultiSelect = 0;
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(260, 160);
			this.lstSort.Sorted = false;
			this.lstSort.Style = 1;
			this.lstSort.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lstSort, null);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboSavedReport);
			this.Frame2.Controls.Add(this.cmbReport);
			this.Frame2.Controls.Add(this.lblReport);
			this.Frame2.Controls.Add(this.cmdAdd);
			this.Frame2.Enabled = false;
			this.Frame2.Location = new System.Drawing.Point(670, 224);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(302, 210);
			this.Frame2.TabIndex = 3;
			this.Frame2.Text = "Report";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// cboSavedReport
			// 
			this.cboSavedReport.AutoSize = false;
			this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
			this.cboSavedReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSavedReport.Enabled = false;
			this.cboSavedReport.FormattingEnabled = true;
			this.cboSavedReport.Location = new System.Drawing.Point(20, 90);
			this.cboSavedReport.Name = "cboSavedReport";
			this.cboSavedReport.Size = new System.Drawing.Size(262, 40);
			this.cboSavedReport.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.cboSavedReport, null);
			this.cboSavedReport.Visible = false;
			this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
			// 
			// cmdAdd
			// 
			this.cmdAdd.AppearanceKey = "actionButton";
			this.cmdAdd.Enabled = false;
			this.cmdAdd.Location = new System.Drawing.Point(20, 150);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(262, 40);
			this.cmdAdd.TabIndex = 3;
			this.cmdAdd.Text = "Add Custom Report to Library";
			this.ToolTip1.SetToolTip(this.cmdAdd, null);
			this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(30, 454);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(942, 200);
			this.fraWhere.TabIndex = 4;
			this.fraWhere.Text = "Search Criteria";
			this.ToolTip1.SetToolTip(this.fraWhere, null);
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// vsWhere
			// 
			this.vsWhere.AllowSelection = false;
			this.vsWhere.AllowUserToResizeColumns = false;
			this.vsWhere.AllowUserToResizeRows = false;
			this.vsWhere.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsWhere.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsWhere.BackColorBkg = System.Drawing.Color.Empty;
			this.vsWhere.BackColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.BackColorSel = System.Drawing.Color.Empty;
			this.vsWhere.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsWhere.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsWhere.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsWhere.ColumnHeadersHeight = 30;
			this.vsWhere.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsWhere.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsWhere.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsWhere.DragIcon = null;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.FrozenCols = 0;
			this.vsWhere.GridColor = System.Drawing.Color.Empty;
			this.vsWhere.GridColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.Location = new System.Drawing.Point(19, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsWhere.RowHeightMin = 0;
			this.vsWhere.Rows = 0;
			this.vsWhere.ScrollTipText = null;
			this.vsWhere.ShowColumnVisibilityMenu = false;
			this.vsWhere.ShowFocusCell = false;
			this.vsWhere.Size = new System.Drawing.Size(903, 150);
			this.vsWhere.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.vsWhere, null);
			this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
			this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
			this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
			this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
			this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
			this.vsWhere.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsWhere_MouseMoveEvent);
			this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(850, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(140, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "Clear Search Criteria";
			this.ToolTip1.SetToolTip(this.cmdClear, null);
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1
			});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuFileSeperator2,
				this.mnuFilePrint,
				this.mnuPreview,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Search Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 2;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuPreview
			// 
			this.mnuPreview.Index = 3;
			this.mnuPreview.Name = "mnuPreview";
			this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPreview.Text = "Print / Preview";
			this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 4;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 5;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuLayout
			// 
			this.mnuLayout.Enabled = false;
			this.mnuLayout.Index = -1;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddRow,
				this.mnuAddColumn,
				this.mnuDeleteRow,
				this.mnuDeleteColumn
			});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Layout";
			this.mnuLayout.Visible = false;
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = 1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 3;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
			// 
			// frmJournalListing
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1026, 790);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmJournalListing";
			this.Text = "Journal Summary List";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmJournalListing_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmJournalListing_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmJournalListing_KeyPress);
			this.Resize += new System.EventHandler(this.frmJournalListing_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
			this.fraMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
			this.vsLayout.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}