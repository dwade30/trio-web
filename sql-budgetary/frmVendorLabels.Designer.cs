﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorLabels.
	/// </summary>
	partial class frmVendorLabels : BaseForm
	{
		public fecherFoundation.FCComboBox cmbStatusSelect;
		public fecherFoundation.FCLabel lblStatusSelect;
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCComboBox cmbVendorJournal;
		public fecherFoundation.FCLabel lblVendorJournal;
		public fecherFoundation.FCComboBox cmbSpecific;
		public fecherFoundation.FCLabel lblSpecific;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkClassCode;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraStatus;
		public fecherFoundation.FCCheckBox chkActive;
		public fecherFoundation.FCCheckBox chkSuspended;
		public fecherFoundation.FCCheckBox chkDeleted;
		public fecherFoundation.FCFrame fraJournal;
		public fecherFoundation.FCComboBox cboJournal;
		public fecherFoundation.FCFrame fraWarrant;
		public fecherFoundation.FCComboBox cboWarrant;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboLabelType;
		public fecherFoundation.FCLabel lblLabelDescription;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraClassCodes;
		public fecherFoundation.FCFrame fraSpecificClassCodes;
		public fecherFoundation.FCCheckBox chkClassCode_19;
		public fecherFoundation.FCCheckBox chkClassCode_18;
		public fecherFoundation.FCCheckBox chkClassCode_17;
		public fecherFoundation.FCCheckBox chkClassCode_16;
		public fecherFoundation.FCCheckBox chkClassCode_15;
		public fecherFoundation.FCCheckBox chkClassCode_14;
		public fecherFoundation.FCCheckBox chkClassCode_13;
		public fecherFoundation.FCCheckBox chkClassCode_12;
		public fecherFoundation.FCCheckBox chkClassCode_11;
		public fecherFoundation.FCCheckBox chkClassCode_10;
		public fecherFoundation.FCCheckBox chkClassCode_9;
		public fecherFoundation.FCCheckBox chkClassCode_8;
		public fecherFoundation.FCCheckBox chkClassCode_7;
		public fecherFoundation.FCCheckBox chkClassCode_6;
		public fecherFoundation.FCCheckBox chkClassCode_5;
		public fecherFoundation.FCCheckBox chkClassCode_4;
		public fecherFoundation.FCCheckBox chkClassCode_3;
		public fecherFoundation.FCCheckBox chkClassCode_2;
		public fecherFoundation.FCCheckBox chkClassCode_1;
		public fecherFoundation.FCCheckBox chkClassCode_0;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVendorLabels));
			this.cmbStatusSelect = new fecherFoundation.FCComboBox();
			this.lblStatusSelect = new fecherFoundation.FCLabel();
			this.cmbName = new fecherFoundation.FCComboBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.cmbVendorJournal = new fecherFoundation.FCComboBox();
			this.lblVendorJournal = new fecherFoundation.FCLabel();
			this.cmbSpecific = new fecherFoundation.FCComboBox();
			this.lblSpecific = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraStatus = new fecherFoundation.FCFrame();
			this.chkActive = new fecherFoundation.FCCheckBox();
			this.chkSuspended = new fecherFoundation.FCCheckBox();
			this.chkDeleted = new fecherFoundation.FCCheckBox();
			this.fraJournal = new fecherFoundation.FCFrame();
			this.cboJournal = new fecherFoundation.FCComboBox();
			this.fraWarrant = new fecherFoundation.FCFrame();
			this.cboWarrant = new fecherFoundation.FCComboBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboLabelType = new fecherFoundation.FCComboBox();
			this.lblLabelDescription = new fecherFoundation.FCLabel();
			this.fraRange = new fecherFoundation.FCFrame();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraClassCodes = new fecherFoundation.FCFrame();
			this.fraSpecificClassCodes = new fecherFoundation.FCFrame();
			this.chkClassCode_19 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_18 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_17 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_16 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_15 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_14 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_13 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_12 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_11 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_10 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_9 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_8 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_7 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_6 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_5 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_4 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_3 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_2 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_1 = new fecherFoundation.FCCheckBox();
			this.chkClassCode_0 = new fecherFoundation.FCCheckBox();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).BeginInit();
			this.fraStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSuspended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournal)).BeginInit();
			this.fraJournal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWarrant)).BeginInit();
			this.fraWarrant.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).BeginInit();
			this.fraClassCodes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificClassCodes)).BeginInit();
			this.fraSpecificClassCodes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 460);
			this.BottomPanel.Size = new System.Drawing.Size(805, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.fraJournal);
			this.ClientArea.Controls.Add(this.fraWarrant);
			this.ClientArea.Controls.Add(this.cmbName);
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbVendorJournal);
			this.ClientArea.Controls.Add(this.lblVendorJournal);
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Controls.Add(this.fraClassCodes);
			this.ClientArea.Size = new System.Drawing.Size(805, 400);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(805, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(142, 30);
			this.HeaderText.Text = "Print Labels";
			// 
			// cmbStatusSelect
			// 
			this.cmbStatusSelect.Items.AddRange(new object[] {
            "All",
            "Selected Status"});
			this.cmbStatusSelect.Location = new System.Drawing.Point(147, 30);
			this.cmbStatusSelect.Name = "cmbStatusSelect";
			this.cmbStatusSelect.Size = new System.Drawing.Size(160, 40);
			this.cmbStatusSelect.TabIndex = 47;
			this.cmbStatusSelect.SelectedIndexChanged += new System.EventHandler(this.optStatusSelect_CheckedChanged);
			// 
			// lblStatusSelect
			// 
			this.lblStatusSelect.Location = new System.Drawing.Point(20, 44);
			this.lblStatusSelect.Name = "lblStatusSelect";
			this.lblStatusSelect.Size = new System.Drawing.Size(92, 16);
			this.lblStatusSelect.TabIndex = 48;
			this.lblStatusSelect.Text = "SELECT STATUS";
			// 
			// cmbName
			// 
			this.cmbName.Items.AddRange(new object[] {
            "Name",
            "Number"});
			this.cmbName.Location = new System.Drawing.Point(157, 306);
			this.cmbName.Name = "cmbName";
			this.cmbName.Size = new System.Drawing.Size(200, 40);
			this.cmbName.TabIndex = 44;
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(30, 320);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(63, 16);
			this.lblName.TabIndex = 45;
			this.lblName.Text = "ORDER BY";
			// 
			// cmbVendorJournal
			// 
			this.cmbVendorJournal.Items.AddRange(new object[] {
            "Complete List",
            "Range of Vendors ",
            "Vendors in Warrant",
            "Vendors in Journal"});
			this.cmbVendorJournal.Location = new System.Drawing.Point(157, 246);
			this.cmbVendorJournal.Name = "cmbVendorJournal";
			this.cmbVendorJournal.Size = new System.Drawing.Size(200, 40);
			this.cmbVendorJournal.TabIndex = 46;
			this.cmbVendorJournal.SelectedIndexChanged += new System.EventHandler(this.optVendorJournal_CheckedChanged);
			// 
			// lblVendorJournal
			// 
			this.lblVendorJournal.Location = new System.Drawing.Point(30, 260);
			this.lblVendorJournal.Name = "lblVendorJournal";
			this.lblVendorJournal.Size = new System.Drawing.Size(92, 16);
			this.lblVendorJournal.TabIndex = 47;
			this.lblVendorJournal.Text = "LABEL OPTIONS";
			// 
			// cmbSpecific
			// 
			this.cmbSpecific.Items.AddRange(new object[] {
            "All",
            "Selected"});
			this.cmbSpecific.Location = new System.Drawing.Point(179, 30);
			this.cmbSpecific.Name = "cmbSpecific";
			this.cmbSpecific.Size = new System.Drawing.Size(122, 40);
			this.cmbSpecific.TabIndex = 4;
			this.cmbSpecific.SelectedIndexChanged += new System.EventHandler(this.optSpecific_CheckedChanged);
			// 
			// lblSpecific
			// 
			this.lblSpecific.Location = new System.Drawing.Point(20, 44);
			this.lblSpecific.Name = "lblSpecific";
			this.lblSpecific.Size = new System.Drawing.Size(88, 16);
			this.lblSpecific.TabIndex = 5;
			this.lblSpecific.Text = "SELECT CODES";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.fraStatus);
			this.Frame3.Controls.Add(this.cmbStatusSelect);
			this.Frame3.Controls.Add(this.lblStatusSelect);
			this.Frame3.Location = new System.Drawing.Point(30, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(327, 196);
			this.Frame3.TabIndex = 43;
			this.Frame3.Text = "Status Selection";
			// 
			// fraStatus
			// 
			this.fraStatus.AppearanceKey = "groupBoxNoBorders";
			this.fraStatus.Controls.Add(this.chkActive);
			this.fraStatus.Controls.Add(this.chkSuspended);
			this.fraStatus.Controls.Add(this.chkDeleted);
			this.fraStatus.Enabled = false;
			this.fraStatus.Location = new System.Drawing.Point(0, 70);
			this.fraStatus.Name = "fraStatus";
			this.fraStatus.Size = new System.Drawing.Size(150, 126);
			this.fraStatus.TabIndex = 46;
			// 
			// chkActive
			// 
			this.chkActive.AutoSize = false;
			this.chkActive.Enabled = false;
			this.chkActive.Location = new System.Drawing.Point(20, 20);
			this.chkActive.Name = "chkActive";
			this.chkActive.Size = new System.Drawing.Size(74, 24);
			this.chkActive.TabIndex = 49;
			this.chkActive.Text = "Active";
			// 
			// chkSuspended
			// 
			this.chkSuspended.AutoSize = false;
			this.chkSuspended.Enabled = false;
			this.chkSuspended.Location = new System.Drawing.Point(20, 54);
			this.chkSuspended.Name = "chkSuspended";
			this.chkSuspended.Size = new System.Drawing.Size(110, 24);
			this.chkSuspended.TabIndex = 48;
			this.chkSuspended.Text = "Suspended";
			// 
			// chkDeleted
			// 
			this.chkDeleted.AutoSize = false;
			this.chkDeleted.Enabled = false;
			this.chkDeleted.Location = new System.Drawing.Point(20, 88);
			this.chkDeleted.Name = "chkDeleted";
			this.chkDeleted.Size = new System.Drawing.Size(84, 24);
			this.chkDeleted.TabIndex = 47;
			this.chkDeleted.Text = "Deleted";
			// 
			// fraJournal
			// 
			this.fraJournal.Controls.Add(this.cboJournal);
			this.fraJournal.Location = new System.Drawing.Point(30, 366);
			this.fraJournal.Name = "fraJournal";
			this.fraJournal.Size = new System.Drawing.Size(130, 90);
			this.fraJournal.TabIndex = 40;
			this.fraJournal.Text = "Select Journal";
			this.fraJournal.Visible = false;
			// 
			// cboJournal
			// 
			this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.Location = new System.Drawing.Point(20, 30);
			this.cboJournal.Name = "cboJournal";
			this.cboJournal.Size = new System.Drawing.Size(90, 40);
			this.cboJournal.TabIndex = 41;
			this.cboJournal.Text = "Combo1";
			this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
			// 
			// fraWarrant
			// 
			this.fraWarrant.Controls.Add(this.cboWarrant);
			this.fraWarrant.Location = new System.Drawing.Point(30, 366);
			this.fraWarrant.Name = "fraWarrant";
			this.fraWarrant.Size = new System.Drawing.Size(130, 90);
			this.fraWarrant.TabIndex = 38;
			this.fraWarrant.Text = "Select Warrant";
			this.fraWarrant.Visible = false;
			// 
			// cboWarrant
			// 
			this.cboWarrant.BackColor = System.Drawing.SystemColors.Window;
			this.cboWarrant.Location = new System.Drawing.Point(20, 30);
			this.cboWarrant.Name = "cboWarrant";
			this.cboWarrant.Size = new System.Drawing.Size(90, 40);
			this.cboWarrant.TabIndex = 39;
			this.cboWarrant.Text = "Combo1";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboLabelType);
			this.Frame2.Controls.Add(this.lblLabelDescription);
			this.Frame2.Location = new System.Drawing.Point(377, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(365, 196);
			this.Frame2.TabIndex = 32;
			this.Frame2.Text = "Select Label Type";
			// 
			// cboLabelType
			// 
			this.cboLabelType.BackColor = System.Drawing.SystemColors.Window;
			this.cboLabelType.Location = new System.Drawing.Point(20, 30);
			this.cboLabelType.Name = "cboLabelType";
			this.cboLabelType.Size = new System.Drawing.Size(327, 40);
			this.cboLabelType.TabIndex = 33;
			this.cboLabelType.SelectedIndexChanged += new System.EventHandler(this.cboLabelType_SelectedIndexChanged);
			// 
			// lblLabelDescription
			// 
			this.lblLabelDescription.Location = new System.Drawing.Point(20, 90);
			this.lblLabelDescription.Name = "lblLabelDescription";
			this.lblLabelDescription.Size = new System.Drawing.Size(327, 92);
			this.lblLabelDescription.TabIndex = 42;
			// 
			// fraRange
			// 
			this.fraRange.Controls.Add(this.cboStart);
			this.fraRange.Controls.Add(this.cboEnd);
			this.fraRange.Controls.Add(this.Label1);
			this.fraRange.Location = new System.Drawing.Point(30, 366);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(460, 90);
			this.fraRange.TabIndex = 4;
			this.fraRange.Text = "Select Range";
			this.fraRange.Visible = false;
			// 
			// cboStart
			// 
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.Location = new System.Drawing.Point(20, 30);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(190, 40);
			this.cboStart.TabIndex = 6;
			// 
			// cboEnd
			// 
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.Location = new System.Drawing.Point(250, 30);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(190, 40);
			this.cboEnd.TabIndex = 5;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(220, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(20, 16);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraClassCodes
			// 
			this.fraClassCodes.Controls.Add(this.fraSpecificClassCodes);
			this.fraClassCodes.Controls.Add(this.cmbSpecific);
			this.fraClassCodes.Controls.Add(this.lblSpecific);
			this.fraClassCodes.Location = new System.Drawing.Point(30, 476);
			this.fraClassCodes.Name = "fraClassCodes";
			this.fraClassCodes.Size = new System.Drawing.Size(700, 352);
			this.fraClassCodes.Text = "Class Codes To Print";
			// 
			// fraSpecificClassCodes
			// 
			this.fraSpecificClassCodes.AppearanceKey = "groupBoxNoBorders";
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_19);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_18);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_17);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_16);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_15);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_14);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_13);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_12);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_11);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_10);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_9);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_8);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_7);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_6);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_5);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_4);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_3);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_2);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_1);
			this.fraSpecificClassCodes.Controls.Add(this.chkClassCode_0);
			this.fraSpecificClassCodes.Enabled = false;
			this.fraSpecificClassCodes.Location = new System.Drawing.Point(0, 70);
			this.fraSpecificClassCodes.Name = "fraSpecificClassCodes";
			this.fraSpecificClassCodes.Size = new System.Drawing.Size(699, 282);
			this.fraSpecificClassCodes.TabIndex = 3;
			// 
			// chkClassCode_19
			// 
			this.chkClassCode_19.Location = new System.Drawing.Point(460, 200);
			this.chkClassCode_19.Name = "chkClassCode_19";
			this.chkClassCode_19.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_19.TabIndex = 31;
			// 
			// chkClassCode_18
			// 
			this.chkClassCode_18.Location = new System.Drawing.Point(460, 164);
			this.chkClassCode_18.Name = "chkClassCode_18";
			this.chkClassCode_18.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_18.TabIndex = 30;
			// 
			// chkClassCode_17
			// 
			this.chkClassCode_17.Location = new System.Drawing.Point(460, 128);
			this.chkClassCode_17.Name = "chkClassCode_17";
			this.chkClassCode_17.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_17.TabIndex = 29;
			// 
			// chkClassCode_16
			// 
			this.chkClassCode_16.Location = new System.Drawing.Point(460, 92);
			this.chkClassCode_16.Name = "chkClassCode_16";
			this.chkClassCode_16.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_16.TabIndex = 28;
			// 
			// chkClassCode_15
			// 
			this.chkClassCode_15.Location = new System.Drawing.Point(460, 56);
			this.chkClassCode_15.Name = "chkClassCode_15";
			this.chkClassCode_15.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_15.TabIndex = 27;
			// 
			// chkClassCode_14
			// 
			this.chkClassCode_14.Location = new System.Drawing.Point(460, 20);
			this.chkClassCode_14.Name = "chkClassCode_14";
			this.chkClassCode_14.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_14.TabIndex = 26;
			// 
			// chkClassCode_13
			// 
			this.chkClassCode_13.Location = new System.Drawing.Point(230, 236);
			this.chkClassCode_13.Name = "chkClassCode_13";
			this.chkClassCode_13.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_13.TabIndex = 25;
			// 
			// chkClassCode_12
			// 
			this.chkClassCode_12.Location = new System.Drawing.Point(230, 200);
			this.chkClassCode_12.Name = "chkClassCode_12";
			this.chkClassCode_12.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_12.TabIndex = 24;
			// 
			// chkClassCode_11
			// 
			this.chkClassCode_11.Location = new System.Drawing.Point(230, 164);
			this.chkClassCode_11.Name = "chkClassCode_11";
			this.chkClassCode_11.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_11.TabIndex = 23;
			// 
			// chkClassCode_10
			// 
			this.chkClassCode_10.Location = new System.Drawing.Point(230, 128);
			this.chkClassCode_10.Name = "chkClassCode_10";
			this.chkClassCode_10.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_10.TabIndex = 22;
			// 
			// chkClassCode_9
			// 
			this.chkClassCode_9.Location = new System.Drawing.Point(230, 92);
			this.chkClassCode_9.Name = "chkClassCode_9";
			this.chkClassCode_9.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_9.TabIndex = 21;
			// 
			// chkClassCode_8
			// 
			this.chkClassCode_8.Location = new System.Drawing.Point(230, 56);
			this.chkClassCode_8.Name = "chkClassCode_8";
			this.chkClassCode_8.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_8.TabIndex = 20;
			// 
			// chkClassCode_7
			// 
			this.chkClassCode_7.Location = new System.Drawing.Point(230, 20);
			this.chkClassCode_7.Name = "chkClassCode_7";
			this.chkClassCode_7.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_7.TabIndex = 19;
			// 
			// chkClassCode_6
			// 
			this.chkClassCode_6.Location = new System.Drawing.Point(20, 236);
			this.chkClassCode_6.Name = "chkClassCode_6";
			this.chkClassCode_6.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_6.TabIndex = 18;
			// 
			// chkClassCode_5
			// 
			this.chkClassCode_5.Location = new System.Drawing.Point(20, 200);
			this.chkClassCode_5.Name = "chkClassCode_5";
			this.chkClassCode_5.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_5.TabIndex = 17;
			// 
			// chkClassCode_4
			// 
			this.chkClassCode_4.Location = new System.Drawing.Point(20, 164);
			this.chkClassCode_4.Name = "chkClassCode_4";
			this.chkClassCode_4.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_4.TabIndex = 16;
			// 
			// chkClassCode_3
			// 
			this.chkClassCode_3.Location = new System.Drawing.Point(20, 128);
			this.chkClassCode_3.Name = "chkClassCode_3";
			this.chkClassCode_3.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_3.TabIndex = 15;
			// 
			// chkClassCode_2
			// 
			this.chkClassCode_2.Location = new System.Drawing.Point(20, 92);
			this.chkClassCode_2.Name = "chkClassCode_2";
			this.chkClassCode_2.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_2.TabIndex = 14;
			// 
			// chkClassCode_1
			// 
			this.chkClassCode_1.Location = new System.Drawing.Point(20, 56);
			this.chkClassCode_1.Name = "chkClassCode_1";
			this.chkClassCode_1.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_1.TabIndex = 13;
			// 
			// chkClassCode_0
			// 
			this.chkClassCode_0.Location = new System.Drawing.Point(20, 20);
			this.chkClassCode_0.Name = "chkClassCode_0";
			this.chkClassCode_0.Size = new System.Drawing.Size(22, 26);
			this.chkClassCode_0.TabIndex = 12;
			// 
			// dlg1
			// 
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnPrint
			// 
			this.btnPrint.AppearanceKey = "acceptButton";
			this.btnPrint.Location = new System.Drawing.Point(353, 30);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnPrint.Size = new System.Drawing.Size(73, 48);
			this.btnPrint.TabIndex = 2;
			this.btnPrint.Text = "Print";
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// frmVendorLabels
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(805, 568);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVendorLabels";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Labels";
			this.Load += new System.EventHandler(this.frmVendorLabels_Load);
			this.Activated += new System.EventHandler(this.frmVendorLabels_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVendorLabels_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).EndInit();
			this.fraStatus.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSuspended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournal)).EndInit();
			this.fraJournal.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraWarrant)).EndInit();
			this.fraWarrant.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).EndInit();
			this.fraClassCodes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificClassCodes)).EndInit();
			this.fraSpecificClassCodes.ResumeLayout(false);
			this.fraSpecificClassCodes.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClassCode_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrint)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnPrint;
	}
}
