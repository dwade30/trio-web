﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevenueRanges.
	/// </summary>
	partial class frmRevenueRanges : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtLevel1Total;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtLevel1Description;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtLevel2Range;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtLevel2Description;
		public fecherFoundation.FCComboBox cmbDivision;
		public fecherFoundation.FCComboBox cmbDepartment;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdCreate;
		public fecherFoundation.FCTextBox txtLevel1Total_0;
		public fecherFoundation.FCTextBox txtLevel1Total_1;
		public fecherFoundation.FCTextBox txtLevel1Total_2;
		public fecherFoundation.FCTextBox txtLevel1Total_3;
		public fecherFoundation.FCTextBox txtLevel1Total_4;
		public fecherFoundation.FCTextBox txtLevel1Total_5;
		public fecherFoundation.FCTextBox txtLevel1Total_6;
		public fecherFoundation.FCTextBox txtLevel1Total_7;
		public fecherFoundation.FCTextBox txtLevel1Total_8;
		public fecherFoundation.FCTextBox txtLevel1Total_9;
		public fecherFoundation.FCTextBox txtLevel1Description_0;
		public fecherFoundation.FCTextBox txtLevel1Description_1;
		public fecherFoundation.FCTextBox txtLevel1Description_2;
		public fecherFoundation.FCTextBox txtLevel1Description_3;
		public fecherFoundation.FCTextBox txtLevel1Description_4;
		public fecherFoundation.FCTextBox txtLevel1Description_5;
		public fecherFoundation.FCTextBox txtLevel1Description_6;
		public fecherFoundation.FCTextBox txtLevel1Description_7;
		public fecherFoundation.FCTextBox txtLevel1Description_8;
		public fecherFoundation.FCTextBox txtLevel1Description_9;
		public fecherFoundation.FCTextBox txtLevel2Range_0;
		public fecherFoundation.FCTextBox txtLevel2Range_1;
		public fecherFoundation.FCTextBox txtLevel2Range_2;
		public fecherFoundation.FCTextBox txtLevel2Range_3;
		public fecherFoundation.FCTextBox txtLevel2Range_4;
		public fecherFoundation.FCTextBox txtLevel2Description_0;
		public fecherFoundation.FCTextBox txtLevel2Description_1;
		public fecherFoundation.FCTextBox txtLevel2Description_2;
		public fecherFoundation.FCTextBox txtLevel2Description_3;
		public fecherFoundation.FCTextBox txtLevel2Description_4;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblDivisionName;
		public fecherFoundation.FCLabel lblDivision;
		public fecherFoundation.FCLabel lblLevel2Range;
		public fecherFoundation.FCLabel lblDepartment;
		public fecherFoundation.FCLabel lblDepartmentName;
		public fecherFoundation.FCLabel lblLevel1;
		public fecherFoundation.FCLabel lblLevel2Name;
		public fecherFoundation.FCLabel lblLevel2;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRevenueRanges));
			this.cmbDivision = new fecherFoundation.FCComboBox();
			this.cmbDepartment = new fecherFoundation.FCComboBox();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdCreate = new fecherFoundation.FCButton();
			this.txtLevel1Total_0 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_1 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_2 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_3 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_4 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_5 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_6 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_7 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_8 = new fecherFoundation.FCTextBox();
			this.txtLevel1Total_9 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_0 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_1 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_2 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_3 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_4 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_5 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_6 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_7 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_8 = new fecherFoundation.FCTextBox();
			this.txtLevel1Description_9 = new fecherFoundation.FCTextBox();
			this.txtLevel2Range_0 = new fecherFoundation.FCTextBox();
			this.txtLevel2Description_0 = new fecherFoundation.FCTextBox();
			this.txtLevel2Range_1 = new fecherFoundation.FCTextBox();
			this.txtLevel2Range_2 = new fecherFoundation.FCTextBox();
			this.txtLevel2Range_3 = new fecherFoundation.FCTextBox();
			this.txtLevel2Range_4 = new fecherFoundation.FCTextBox();
			this.txtLevel2Description_1 = new fecherFoundation.FCTextBox();
			this.txtLevel2Description_2 = new fecherFoundation.FCTextBox();
			this.txtLevel2Description_3 = new fecherFoundation.FCTextBox();
			this.txtLevel2Description_4 = new fecherFoundation.FCTextBox();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblDivisionName = new fecherFoundation.FCLabel();
			this.lblDivision = new fecherFoundation.FCLabel();
			this.lblLevel2Range = new fecherFoundation.FCLabel();
			this.lblDepartment = new fecherFoundation.FCLabel();
			this.lblDepartmentName = new fecherFoundation.FCLabel();
			this.lblLevel1 = new fecherFoundation.FCLabel();
			this.lblLevel2Name = new fecherFoundation.FCLabel();
			this.lblLevel2 = new fecherFoundation.FCLabel();
			this.btnProcessView = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbDivision);
			this.ClientArea.Controls.Add(this.cmbDepartment);
			this.ClientArea.Controls.Add(this.cmdDelete);
			this.ClientArea.Controls.Add(this.cmdCreate);
			this.ClientArea.Controls.Add(this.txtLevel1Total_0);
			this.ClientArea.Controls.Add(this.txtLevel1Total_1);
			this.ClientArea.Controls.Add(this.txtLevel1Total_2);
			this.ClientArea.Controls.Add(this.txtLevel1Total_3);
			this.ClientArea.Controls.Add(this.txtLevel1Total_4);
			this.ClientArea.Controls.Add(this.txtLevel1Total_5);
			this.ClientArea.Controls.Add(this.txtLevel1Total_6);
			this.ClientArea.Controls.Add(this.txtLevel1Total_7);
			this.ClientArea.Controls.Add(this.txtLevel1Total_8);
			this.ClientArea.Controls.Add(this.txtLevel1Total_9);
			this.ClientArea.Controls.Add(this.txtLevel1Description_0);
			this.ClientArea.Controls.Add(this.txtLevel1Description_1);
			this.ClientArea.Controls.Add(this.txtLevel1Description_2);
			this.ClientArea.Controls.Add(this.txtLevel1Description_3);
			this.ClientArea.Controls.Add(this.txtLevel1Description_4);
			this.ClientArea.Controls.Add(this.txtLevel1Description_5);
			this.ClientArea.Controls.Add(this.txtLevel1Description_6);
			this.ClientArea.Controls.Add(this.txtLevel1Description_7);
			this.ClientArea.Controls.Add(this.txtLevel1Description_8);
			this.ClientArea.Controls.Add(this.txtLevel1Description_9);
			this.ClientArea.Controls.Add(this.txtLevel2Range_0);
			this.ClientArea.Controls.Add(this.txtLevel2Range_1);
			this.ClientArea.Controls.Add(this.txtLevel2Range_2);
			this.ClientArea.Controls.Add(this.txtLevel2Range_3);
			this.ClientArea.Controls.Add(this.txtLevel2Range_4);
			this.ClientArea.Controls.Add(this.txtLevel2Description_0);
			this.ClientArea.Controls.Add(this.txtLevel2Description_1);
			this.ClientArea.Controls.Add(this.txtLevel2Description_2);
			this.ClientArea.Controls.Add(this.txtLevel2Description_3);
			this.ClientArea.Controls.Add(this.txtLevel2Description_4);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblDivisionName);
			this.ClientArea.Controls.Add(this.lblDivision);
			this.ClientArea.Controls.Add(this.lblLevel2Range);
			this.ClientArea.Controls.Add(this.lblDepartment);
			this.ClientArea.Controls.Add(this.lblDepartmentName);
			this.ClientArea.Controls.Add(this.lblLevel1);
			this.ClientArea.Controls.Add(this.lblLevel2Name);
			this.ClientArea.Controls.Add(this.lblLevel2);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessView);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessView, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(264, 30);
			this.HeaderText.Text = "Revenue Total Ranges";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbDivision
			// 
			this.cmbDivision.AutoSize = false;
			this.cmbDivision.BackColor = System.Drawing.SystemColors.Window;
			this.cmbDivision.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDivision.FormattingEnabled = true;
			this.cmbDivision.Location = new System.Drawing.Point(624, 30);
			this.cmbDivision.Name = "cmbDivision";
			this.cmbDivision.Size = new System.Drawing.Size(300, 40);
			this.cmbDivision.Sorted = true;
			this.cmbDivision.TabIndex = 40;
			this.ToolTip1.SetToolTip(this.cmbDivision, null);
			this.cmbDivision.Visible = false;
			this.cmbDivision.SelectedIndexChanged += new System.EventHandler(this.cmbDivision_SelectedIndexChanged);
			this.cmbDivision.DropDown += new System.EventHandler(this.cmbDivision_DropDown);
			this.cmbDivision.Enter += new System.EventHandler(this.cmbDivision_Enter);
			this.cmbDivision.Validating += new System.ComponentModel.CancelEventHandler(this.cmbDivision_Validating);
			// 
			// cmbDepartment
			// 
			this.cmbDepartment.AutoSize = false;
			this.cmbDepartment.BackColor = System.Drawing.SystemColors.Window;
			this.cmbDepartment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDepartment.FormattingEnabled = true;
			this.cmbDepartment.Location = new System.Drawing.Point(176, 30);
			this.cmbDepartment.Name = "cmbDepartment";
			this.cmbDepartment.Size = new System.Drawing.Size(300, 40);
			this.cmbDepartment.Sorted = true;
			this.cmbDepartment.TabIndex = 38;
			this.ToolTip1.SetToolTip(this.cmbDepartment, null);
			this.cmbDepartment.Visible = false;
			this.cmbDepartment.SelectedIndexChanged += new System.EventHandler(this.cmbDepartment_SelectedIndexChanged);
			this.cmbDepartment.DropDown += new System.EventHandler(this.cmbDepartment_DropDown);
			this.cmbDepartment.Enter += new System.EventHandler(this.cmbDepartment_Enter);
			this.cmbDepartment.Validating += new System.ComponentModel.CancelEventHandler(this.cmbDepartment_Validating);
			// 
			// cmdDelete
			// 
			this.cmdDelete.AppearanceKey = "actionButton";
			this.cmdDelete.Location = new System.Drawing.Point(30, 474);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(160, 48);
			this.cmdDelete.TabIndex = 37;
			this.cmdDelete.Text = "Delete Subranges";
			this.ToolTip1.SetToolTip(this.cmdDelete, null);
			this.cmdDelete.Visible = false;
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdCreate
			// 
			this.cmdCreate.AppearanceKey = "actionButton";
			this.cmdCreate.Location = new System.Drawing.Point(30, 474);
			this.cmdCreate.Name = "cmdCreate";
			this.cmdCreate.Size = new System.Drawing.Size(160, 48);
			this.cmdCreate.TabIndex = 35;
			this.cmdCreate.Text = "Create Subranges ";
			this.ToolTip1.SetToolTip(this.cmdCreate, null);
			this.cmdCreate.Visible = false;
			this.cmdCreate.Click += new System.EventHandler(this.cmdCreate_Click);
			// 
			// txtLevel1Total_0
			// 
			this.txtLevel1Total_0.AutoSize = false;
			this.txtLevel1Total_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_0.Location = new System.Drawing.Point(30, 214);
			this.txtLevel1Total_0.Name = "txtLevel1Total_0";
			this.txtLevel1Total_0.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_0.TabIndex = 0;
			this.txtLevel1Total_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_0, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_0.Visible = false;
			this.txtLevel1Total_0.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_0.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_1
			// 
			this.txtLevel1Total_1.AutoSize = false;
			this.txtLevel1Total_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_1.Location = new System.Drawing.Point(30, 264);
			this.txtLevel1Total_1.Name = "txtLevel1Total_1";
			this.txtLevel1Total_1.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_1.TabIndex = 2;
			this.txtLevel1Total_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_1, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_1.Visible = false;
			this.txtLevel1Total_1.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_1.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_2
			// 
			this.txtLevel1Total_2.AutoSize = false;
			this.txtLevel1Total_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_2.Location = new System.Drawing.Point(30, 314);
			this.txtLevel1Total_2.Name = "txtLevel1Total_2";
			this.txtLevel1Total_2.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_2.TabIndex = 4;
			this.txtLevel1Total_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_2, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_2.Visible = false;
			this.txtLevel1Total_2.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_2.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_3
			// 
			this.txtLevel1Total_3.AutoSize = false;
			this.txtLevel1Total_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_3.Location = new System.Drawing.Point(30, 364);
			this.txtLevel1Total_3.Name = "txtLevel1Total_3";
			this.txtLevel1Total_3.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_3.TabIndex = 6;
			this.txtLevel1Total_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_3, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_3.Visible = false;
			this.txtLevel1Total_3.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_3.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_4
			// 
			this.txtLevel1Total_4.AutoSize = false;
			this.txtLevel1Total_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_4.Location = new System.Drawing.Point(30, 414);
			this.txtLevel1Total_4.Name = "txtLevel1Total_4";
			this.txtLevel1Total_4.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_4.TabIndex = 8;
			this.txtLevel1Total_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_4, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_4.Visible = false;
			this.txtLevel1Total_4.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_4.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_5
			// 
			this.txtLevel1Total_5.AutoSize = false;
			this.txtLevel1Total_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_5.Location = new System.Drawing.Point(370, 214);
			this.txtLevel1Total_5.Name = "txtLevel1Total_5";
			this.txtLevel1Total_5.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_5.TabIndex = 10;
			this.txtLevel1Total_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_5, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_5.Visible = false;
			this.txtLevel1Total_5.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_5.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_6
			// 
			this.txtLevel1Total_6.AutoSize = false;
			this.txtLevel1Total_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_6.Location = new System.Drawing.Point(370, 264);
			this.txtLevel1Total_6.Name = "txtLevel1Total_6";
			this.txtLevel1Total_6.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_6.TabIndex = 12;
			this.txtLevel1Total_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_6, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_6.Visible = false;
			this.txtLevel1Total_6.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_6.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_7
			// 
			this.txtLevel1Total_7.AutoSize = false;
			this.txtLevel1Total_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_7.Location = new System.Drawing.Point(370, 314);
			this.txtLevel1Total_7.Name = "txtLevel1Total_7";
			this.txtLevel1Total_7.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_7.TabIndex = 14;
			this.txtLevel1Total_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_7, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_7.Visible = false;
			this.txtLevel1Total_7.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_7.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_8
			// 
			this.txtLevel1Total_8.AutoSize = false;
			this.txtLevel1Total_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_8.Location = new System.Drawing.Point(370, 364);
			this.txtLevel1Total_8.Name = "txtLevel1Total_8";
			this.txtLevel1Total_8.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_8.TabIndex = 16;
			this.txtLevel1Total_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_8, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_8.Visible = false;
			this.txtLevel1Total_8.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_8.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Total_9
			// 
			this.txtLevel1Total_9.AutoSize = false;
			this.txtLevel1Total_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Total_9.Location = new System.Drawing.Point(370, 414);
			this.txtLevel1Total_9.Name = "txtLevel1Total_9";
			this.txtLevel1Total_9.Size = new System.Drawing.Size(100, 40);
			this.txtLevel1Total_9.TabIndex = 18;
			this.txtLevel1Total_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel1Total_9, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel1Total_9.Visible = false;
			this.txtLevel1Total_9.Enter += new System.EventHandler(this.txtLevel1Total_Enter);
			this.txtLevel1Total_9.TextChanged += new System.EventHandler(this.txtLevel1Total_TextChanged);
			this.txtLevel1Total_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Total_Validating);
			// 
			// txtLevel1Description_0
			// 
			this.txtLevel1Description_0.AutoSize = false;
			this.txtLevel1Description_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_0.Location = new System.Drawing.Point(160, 214);
			this.txtLevel1Description_0.Name = "txtLevel1Description_0";
			this.txtLevel1Description_0.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_0.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_0, null);
			this.txtLevel1Description_0.Visible = false;
			this.txtLevel1Description_0.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_0.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_1
			// 
			this.txtLevel1Description_1.AutoSize = false;
			this.txtLevel1Description_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_1.Location = new System.Drawing.Point(160, 264);
			this.txtLevel1Description_1.Name = "txtLevel1Description_1";
			this.txtLevel1Description_1.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_1.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_1, null);
			this.txtLevel1Description_1.Visible = false;
			this.txtLevel1Description_1.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_1.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_2
			// 
			this.txtLevel1Description_2.AutoSize = false;
			this.txtLevel1Description_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_2.Location = new System.Drawing.Point(160, 314);
			this.txtLevel1Description_2.Name = "txtLevel1Description_2";
			this.txtLevel1Description_2.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_2.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_2, null);
			this.txtLevel1Description_2.Visible = false;
			this.txtLevel1Description_2.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_2.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_3
			// 
			this.txtLevel1Description_3.AutoSize = false;
			this.txtLevel1Description_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_3.Location = new System.Drawing.Point(160, 364);
			this.txtLevel1Description_3.Name = "txtLevel1Description_3";
			this.txtLevel1Description_3.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_3.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_3, null);
			this.txtLevel1Description_3.Visible = false;
			this.txtLevel1Description_3.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_3.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_4
			// 
			this.txtLevel1Description_4.AutoSize = false;
			this.txtLevel1Description_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_4.Location = new System.Drawing.Point(160, 414);
			this.txtLevel1Description_4.Name = "txtLevel1Description_4";
			this.txtLevel1Description_4.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_4.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_4, null);
			this.txtLevel1Description_4.Visible = false;
			this.txtLevel1Description_4.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_4.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_5
			// 
			this.txtLevel1Description_5.AutoSize = false;
			this.txtLevel1Description_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_5.Location = new System.Drawing.Point(500, 214);
			this.txtLevel1Description_5.Name = "txtLevel1Description_5";
			this.txtLevel1Description_5.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_5.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_5, null);
			this.txtLevel1Description_5.Visible = false;
			this.txtLevel1Description_5.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_5.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_6
			// 
			this.txtLevel1Description_6.AutoSize = false;
			this.txtLevel1Description_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_6.Location = new System.Drawing.Point(500, 264);
			this.txtLevel1Description_6.Name = "txtLevel1Description_6";
			this.txtLevel1Description_6.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_6.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_6, null);
			this.txtLevel1Description_6.Visible = false;
			this.txtLevel1Description_6.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_6.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_7
			// 
			this.txtLevel1Description_7.AutoSize = false;
			this.txtLevel1Description_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_7.Location = new System.Drawing.Point(500, 314);
			this.txtLevel1Description_7.Name = "txtLevel1Description_7";
			this.txtLevel1Description_7.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_7.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_7, null);
			this.txtLevel1Description_7.Visible = false;
			this.txtLevel1Description_7.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_7.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_8
			// 
			this.txtLevel1Description_8.AutoSize = false;
			this.txtLevel1Description_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_8.Location = new System.Drawing.Point(500, 364);
			this.txtLevel1Description_8.Name = "txtLevel1Description_8";
			this.txtLevel1Description_8.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_8.TabIndex = 17;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_8, null);
			this.txtLevel1Description_8.Visible = false;
			this.txtLevel1Description_8.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_8.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel1Description_9
			// 
			this.txtLevel1Description_9.AutoSize = false;
			this.txtLevel1Description_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel1Description_9.Location = new System.Drawing.Point(500, 414);
			this.txtLevel1Description_9.Name = "txtLevel1Description_9";
			this.txtLevel1Description_9.Size = new System.Drawing.Size(150, 40);
			this.txtLevel1Description_9.TabIndex = 19;
			this.ToolTip1.SetToolTip(this.txtLevel1Description_9, null);
			this.txtLevel1Description_9.Visible = false;
			this.txtLevel1Description_9.Enter += new System.EventHandler(this.txtLevel1Description_Enter);
			this.txtLevel1Description_9.TextChanged += new System.EventHandler(this.txtLevel1Description_TextChanged);
			this.txtLevel1Description_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel1Description_Validating);
			// 
			// txtLevel2Range_0
			// 
			this.txtLevel2Range_0.AutoSize = false;
			this.txtLevel2Range_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Range_0.Location = new System.Drawing.Point(30, 669);
			this.txtLevel2Range_0.Name = "txtLevel2Range_0";
			this.txtLevel2Range_0.Size = new System.Drawing.Size(100, 40);
			this.txtLevel2Range_0.TabIndex = 20;
			this.txtLevel2Range_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel2Range_0, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel2Range_0.Visible = false;
			this.txtLevel2Range_0.TextChanged += new System.EventHandler(this.txtLevel2Range_TextChanged);
			this.txtLevel2Range_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Range_Validating);
			// 
			// txtLevel2Description_0
			// 
			this.txtLevel2Description_0.AutoSize = false;
			this.txtLevel2Description_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Description_0.Location = new System.Drawing.Point(170, 669);
			this.txtLevel2Description_0.Name = "txtLevel2Description_0";
			this.txtLevel2Description_0.Size = new System.Drawing.Size(150, 40);
			this.txtLevel2Description_0.TabIndex = 21;
			this.ToolTip1.SetToolTip(this.txtLevel2Description_0, null);
			this.txtLevel2Description_0.Visible = false;
			this.txtLevel2Description_0.TextChanged += new System.EventHandler(this.txtLevel2Description_TextChanged);
			this.txtLevel2Description_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Description_Validating);
			// 
			// txtLevel2Range_1
			// 
			this.txtLevel2Range_1.AutoSize = false;
			this.txtLevel2Range_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Range_1.Location = new System.Drawing.Point(30, 719);
			this.txtLevel2Range_1.Name = "txtLevel2Range_1";
			this.txtLevel2Range_1.Size = new System.Drawing.Size(100, 40);
			this.txtLevel2Range_1.TabIndex = 22;
			this.txtLevel2Range_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel2Range_1, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel2Range_1.Visible = false;
			this.txtLevel2Range_1.TextChanged += new System.EventHandler(this.txtLevel2Range_TextChanged);
			this.txtLevel2Range_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Range_Validating);
			// 
			// txtLevel2Range_2
			// 
			this.txtLevel2Range_2.AutoSize = false;
			this.txtLevel2Range_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Range_2.Location = new System.Drawing.Point(30, 769);
			this.txtLevel2Range_2.Name = "txtLevel2Range_2";
			this.txtLevel2Range_2.Size = new System.Drawing.Size(100, 40);
			this.txtLevel2Range_2.TabIndex = 24;
			this.txtLevel2Range_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel2Range_2, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel2Range_2.Visible = false;
			this.txtLevel2Range_2.TextChanged += new System.EventHandler(this.txtLevel2Range_TextChanged);
			this.txtLevel2Range_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Range_Validating);
			// 
			// txtLevel2Range_3
			// 
			this.txtLevel2Range_3.AutoSize = false;
			this.txtLevel2Range_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Range_3.Location = new System.Drawing.Point(31, 819);
			this.txtLevel2Range_3.Name = "txtLevel2Range_3";
			this.txtLevel2Range_3.Size = new System.Drawing.Size(100, 40);
			this.txtLevel2Range_3.TabIndex = 26;
			this.txtLevel2Range_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel2Range_3, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel2Range_3.Visible = false;
			this.txtLevel2Range_3.TextChanged += new System.EventHandler(this.txtLevel2Range_TextChanged);
			this.txtLevel2Range_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Range_Validating);
			// 
			// txtLevel2Range_4
			// 
			this.txtLevel2Range_4.AutoSize = false;
			this.txtLevel2Range_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Range_4.Location = new System.Drawing.Point(31, 869);
			this.txtLevel2Range_4.Name = "txtLevel2Range_4";
			this.txtLevel2Range_4.Size = new System.Drawing.Size(100, 40);
			this.txtLevel2Range_4.TabIndex = 28;
			this.txtLevel2Range_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtLevel2Range_4, "To Delete a Range simply erase the value in this field and hit Enter");
			this.txtLevel2Range_4.Visible = false;
			this.txtLevel2Range_4.TextChanged += new System.EventHandler(this.txtLevel2Range_TextChanged);
			this.txtLevel2Range_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Range_Validating);
			// 
			// txtLevel2Description_1
			// 
			this.txtLevel2Description_1.AutoSize = false;
			this.txtLevel2Description_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Description_1.Location = new System.Drawing.Point(170, 719);
			this.txtLevel2Description_1.Name = "txtLevel2Description_1";
			this.txtLevel2Description_1.Size = new System.Drawing.Size(150, 40);
			this.txtLevel2Description_1.TabIndex = 23;
			this.ToolTip1.SetToolTip(this.txtLevel2Description_1, null);
			this.txtLevel2Description_1.Visible = false;
			this.txtLevel2Description_1.TextChanged += new System.EventHandler(this.txtLevel2Description_TextChanged);
			this.txtLevel2Description_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Description_Validating);
			// 
			// txtLevel2Description_2
			// 
			this.txtLevel2Description_2.AutoSize = false;
			this.txtLevel2Description_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Description_2.Location = new System.Drawing.Point(170, 769);
			this.txtLevel2Description_2.Name = "txtLevel2Description_2";
			this.txtLevel2Description_2.Size = new System.Drawing.Size(150, 40);
			this.txtLevel2Description_2.TabIndex = 25;
			this.ToolTip1.SetToolTip(this.txtLevel2Description_2, null);
			this.txtLevel2Description_2.Visible = false;
			this.txtLevel2Description_2.TextChanged += new System.EventHandler(this.txtLevel2Description_TextChanged);
			this.txtLevel2Description_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Description_Validating);
			// 
			// txtLevel2Description_3
			// 
			this.txtLevel2Description_3.AutoSize = false;
			this.txtLevel2Description_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Description_3.Location = new System.Drawing.Point(170, 819);
			this.txtLevel2Description_3.Name = "txtLevel2Description_3";
			this.txtLevel2Description_3.Size = new System.Drawing.Size(150, 40);
			this.txtLevel2Description_3.TabIndex = 27;
			this.ToolTip1.SetToolTip(this.txtLevel2Description_3, null);
			this.txtLevel2Description_3.Visible = false;
			this.txtLevel2Description_3.TextChanged += new System.EventHandler(this.txtLevel2Description_TextChanged);
			this.txtLevel2Description_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Description_Validating);
			// 
			// txtLevel2Description_4
			// 
			this.txtLevel2Description_4.AutoSize = false;
			this.txtLevel2Description_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtLevel2Description_4.Location = new System.Drawing.Point(170, 869);
			this.txtLevel2Description_4.Name = "txtLevel2Description_4";
			this.txtLevel2Description_4.Size = new System.Drawing.Size(150, 40);
			this.txtLevel2Description_4.TabIndex = 29;
			this.ToolTip1.SetToolTip(this.txtLevel2Description_4, null);
			this.txtLevel2Description_4.Visible = false;
			this.txtLevel2Description_4.TextChanged += new System.EventHandler(this.txtLevel2Description_TextChanged);
			this.txtLevel2Description_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel2Description_Validating);
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(170, 645);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(85, 14);
			this.Label6.TabIndex = 47;
			this.Label6.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(370, 190);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(60, 14);
			this.Label3.TabIndex = 44;
			this.Label3.Text = "REVENUE";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(160, 190);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(85, 14);
			this.Label2.TabIndex = 43;
			this.Label2.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 190);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(60, 14);
			this.Label1.TabIndex = 42;
			this.Label1.Text = "REVENUE";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// lblDivisionName
			// 
			this.lblDivisionName.Location = new System.Drawing.Point(30, 113);
			this.lblDivisionName.Name = "lblDivisionName";
			this.lblDivisionName.Size = new System.Drawing.Size(260, 23);
			this.lblDivisionName.TabIndex = 41;
			this.ToolTip1.SetToolTip(this.lblDivisionName, null);
			this.lblDivisionName.Visible = false;
			// 
			// lblDivision
			// 
			this.lblDivision.Location = new System.Drawing.Point(504, 44);
			this.lblDivision.Name = "lblDivision";
			this.lblDivision.Size = new System.Drawing.Size(53, 15);
			this.lblDivision.TabIndex = 39;
			this.lblDivision.Text = "DIVISION";
			this.ToolTip1.SetToolTip(this.lblDivision, null);
			this.lblDivision.Visible = false;
			// 
			// lblLevel2Range
			// 
			this.lblLevel2Range.Location = new System.Drawing.Point(30, 540);
			this.lblLevel2Range.Name = "lblLevel2Range";
			this.lblLevel2Range.Size = new System.Drawing.Size(165, 23);
			this.lblLevel2Range.TabIndex = 36;
			this.ToolTip1.SetToolTip(this.lblLevel2Range, null);
			// 
			// lblDepartment
			// 
			this.lblDepartment.Location = new System.Drawing.Point(30, 44);
			this.lblDepartment.Name = "lblDepartment";
			this.lblDepartment.Size = new System.Drawing.Size(80, 15);
			this.lblDepartment.TabIndex = 34;
			this.lblDepartment.Text = "DEPARTMENT";
			this.ToolTip1.SetToolTip(this.lblDepartment, null);
			this.lblDepartment.Visible = false;
			// 
			// lblDepartmentName
			// 
			this.lblDepartmentName.Location = new System.Drawing.Point(30, 85);
			this.lblDepartmentName.Name = "lblDepartmentName";
			this.lblDepartmentName.Size = new System.Drawing.Size(260, 23);
			this.lblDepartmentName.TabIndex = 33;
			this.ToolTip1.SetToolTip(this.lblDepartmentName, null);
			this.lblDepartmentName.Visible = false;
			// 
			// lblLevel1
			// 
			this.lblLevel1.Location = new System.Drawing.Point(30, 155);
			this.lblLevel1.Name = "lblLevel1";
			this.lblLevel1.Size = new System.Drawing.Size(138, 15);
			this.lblLevel1.TabIndex = 32;
			this.lblLevel1.Text = "LEVEL 1 TOTAL AFTER";
			this.ToolTip1.SetToolTip(this.lblLevel1, null);
			this.lblLevel1.Visible = false;
			// 
			// lblLevel2Name
			// 
			this.lblLevel2Name.Location = new System.Drawing.Point(30, 568);
			this.lblLevel2Name.Name = "lblLevel2Name";
			this.lblLevel2Name.Size = new System.Drawing.Size(165, 23);
			this.lblLevel2Name.TabIndex = 31;
			this.ToolTip1.SetToolTip(this.lblLevel2Name, null);
			this.lblLevel2Name.Visible = false;
			// 
			// lblLevel2
			// 
			this.lblLevel2.Location = new System.Drawing.Point(30, 610);
			this.lblLevel2.Name = "lblLevel2";
			this.lblLevel2.Size = new System.Drawing.Size(130, 15);
			this.lblLevel2.TabIndex = 30;
			this.lblLevel2.Text = "LEVEL 2 TOTAL AFTER";
			this.ToolTip1.SetToolTip(this.lblLevel2, null);
			this.lblLevel2.Visible = false;
			// 
			// btnProcessView
			// 
			this.btnProcessView.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessView.AppearanceKey = "toolbarButton";
			this.btnProcessView.Location = new System.Drawing.Point(886, 28);
			this.btnProcessView.Name = "btnProcessView";
			this.btnProcessView.Size = new System.Drawing.Size(162, 24);
			this.btnProcessView.TabIndex = 1;
			this.btnProcessView.Text = "View Revenue Accounts";
			this.ToolTip1.SetToolTip(this.btnProcessView, null);
			this.btnProcessView.Click += new System.EventHandler(this.btnProcessView_Click);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 645);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(60, 14);
			this.Label5.TabIndex = 46;
			this.Label5.Text = "REVENUE";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(500, 190);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(85, 14);
			this.Label4.TabIndex = 45;
			this.Label4.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(489, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmRevenueRanges
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmRevenueRanges";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Revenue Total Ranges";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmRevenueRanges_Load);
			this.Activated += new System.EventHandler(this.frmRevenueRanges_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRevenueRanges_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRevenueRanges_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton btnProcessView;
	}
}
