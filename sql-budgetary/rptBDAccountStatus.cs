﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBDAccountStatus.
	/// </summary>
	public partial class rptBDAccountStatus : BaseSectionReport
	{
		public static rptBDAccountStatus InstancePtr
		{
			get
			{
				return (rptBDAccountStatus)Sys.GetInstance(typeof(rptBDAccountStatus));
			}
		}

		protected rptBDAccountStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBDAccountStatus	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		cBDAccountStatusReport aStatReport;
		//frmBusy bWait = null;

		public rptBDAccountStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Status";
		}

		public cBDAccountStatusReport AccountStatusReport
		{
			get
			{
				cBDAccountStatusReport AccountStatusReport = null;
				AccountStatusReport = aStatReport;
				return AccountStatusReport;
			}
		}

		public void Init(cBDAccountStatusReport statRpt)
		{
			//bWait = new frmBusy();
			//bWait.Message = "Please wait, creating report";
			//bWait.StartBusy();
			//bWait.Show(FCForm.FormShowEnum.Modeless);
			aStatReport = statRpt;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !aStatReport.Accounts.IsCurrent();
			if (!eArgs.EOF)
			{
				eArgs.EOF = !aStatReport.Accounts.IsCurrent();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			aStatReport.Accounts.MoveFirst();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (aStatReport.Accounts.IsCurrent())
			{
				string strRange = "";
				srptBDAccountStatusDetail sRpt = new srptBDAccountStatusDetail();
				switch (aStatReport.ActivityRangeType)
				{
					case 0:
						{
							strRange = "";
							break;
						}
					case 1:
						{
							break;
						}
					case 2:
						{
							strRange = FCConvert.ToString(aStatReport.StartPeriod) + "  -  " + FCConvert.ToString(aStatReport.EndPeriod);
							break;
						}
				}
				//end switch
				sRpt.Init(ref aStatReport);
				subAccountInfo.Report = sRpt;
				aStatReport.Accounts.MoveNext();
			}
			else
			{
				subAccountInfo.Report = null;
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			subAccountInfo.Report = null;
			//bWait.StopBusy();
			//bWait.Unload();
			/*- bWait = null; */
		}

		private void rptBDAccountStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBDAccountStatus.Caption	= "Account Status";
			//rptBDAccountStatus.Left	= 0;
			//rptBDAccountStatus.Top	= 0;
			//rptBDAccountStatus.Width	= 19245;
			//rptBDAccountStatus.Height	= 8445;
			//rptBDAccountStatus.StartUpPosition	= 3;
			//rptBDAccountStatus.SectionData	= "rptBDAccountStatus.dsx":0000;
			//End Unmaped Properties
		}
	}
}
