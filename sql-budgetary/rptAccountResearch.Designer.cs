﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAccountResearch.
	/// </summary>
	partial class rptAccountResearch
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAccountResearch));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblJournal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVendor = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRCBType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDebits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCredits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblJournalOptions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblEncumbrance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLiquidated = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLiquidatedCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccountTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldHeaderDebitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderCreditsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderEncumbranceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncumbrance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkLiquidated = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.fldLiquidatedCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldEncumbranceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDebitsFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditsFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldEncumbranceFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRCBType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournalOptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEncumbrance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLiquidated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLiquidatedCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderDebitsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderCreditsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderEncumbranceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbrance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLiquidated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLiquidatedCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbranceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitsFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditsFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbranceFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPeriod,
				this.fldJournal,
				this.fldDate,
				this.fldVendor,
				this.fldDescription,
				this.fldRCB,
				this.fldDebits,
				this.fldCredits,
				this.fldType,
				this.fldCheck,
				this.fldEncumbrance,
				this.chkLiquidated,
				this.fldLiquidatedCheck
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAcct,
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblDateRange,
				this.lblAccount,
				this.lblPeriod,
				this.lblJournal,
				this.lblDate,
				this.lblVendor,
				this.lblDescription,
				this.lblRCBType,
				this.lblDebits,
				this.lblCredits,
				this.Line1,
				this.lblCheck,
				this.lblJournalOptions,
				this.lblEncumbrance,
				this.lblLiquidated,
				this.lblLiquidatedCheck
			});
			this.PageHeader.Height = 1.041667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.fldDebitsFinalTotal,
				this.fldCreditsFinalTotal,
				this.Line4,
				this.fldEncumbranceFinalTotal
			});
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccountTitle,
				this.fldHeaderDebitsTotal,
				this.fldHeaderCreditsTotal,
				this.Binder,
				this.fldHeaderEncumbranceTotal
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.2604167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label21,
				this.fldDebitTotal,
				this.fldCreditTotal,
				this.Line3,
				this.fldEncumbranceTotal
			});
			this.GroupFooter1.Height = 0.2604167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblAcct.Text = "Account";
			this.lblAcct.Top = 0.84375F;
			this.lblAcct.Width = 3.65625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = " Account Research";
			this.Label1.Top = 0F;
			this.Label1.Width = 10.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 9.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 9.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 2.8125F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label8";
			this.lblDateRange.Top = 0.1875F;
			this.lblDateRange.Width = 4.84375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 2.3125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.lblAccount.Text = "Label8";
			this.lblAccount.Top = 0.375F;
			this.lblAccount.Width = 5.8125F;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Height = 0.1875F;
			this.lblPeriod.HyperLink = null;
			this.lblPeriod.Left = 0.125F;
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblPeriod.Text = "Per";
			this.lblPeriod.Top = 0.84375F;
			this.lblPeriod.Width = 0.25F;
			// 
			// lblJournal
			// 
			this.lblJournal.Height = 0.1875F;
			this.lblJournal.HyperLink = null;
			this.lblJournal.Left = 0.4375F;
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblJournal.Text = "Jrnl";
			this.lblJournal.Top = 0.84375F;
			this.lblJournal.Width = 0.375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.34375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.84375F;
			this.lblDate.Width = 0.4375F;
			// 
			// lblVendor
			// 
			this.lblVendor.Height = 0.1875F;
			this.lblVendor.HyperLink = null;
			this.lblVendor.Left = 1.84375F;
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblVendor.Text = "Vendor---------";
			this.lblVendor.Top = 0.84375F;
			this.lblVendor.Width = 2.09375F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 4.03125F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblDescription.Text = "Description--------";
			this.lblDescription.Top = 0.84375F;
			this.lblDescription.Width = 1.71875F;
			// 
			// lblRCBType
			// 
			this.lblRCBType.Height = 0.1875F;
			this.lblRCBType.HyperLink = null;
			this.lblRCBType.Left = 7.0625F;
			this.lblRCBType.Name = "lblRCBType";
			this.lblRCBType.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblRCBType.Text = "RCB / Typ";
			this.lblRCBType.Top = 0.84375F;
			this.lblRCBType.Width = 0.6875F;
			// 
			// lblDebits
			// 
			this.lblDebits.Height = 0.1875F;
			this.lblDebits.HyperLink = null;
			this.lblDebits.Left = 7.78125F;
			this.lblDebits.Name = "lblDebits";
			this.lblDebits.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblDebits.Text = "Debits";
			this.lblDebits.Top = 0.84375F;
			this.lblDebits.Width = 0.84375F;
			// 
			// lblCredits
			// 
			this.lblCredits.Height = 0.1875F;
			this.lblCredits.HyperLink = null;
			this.lblCredits.Left = 8.6875F;
			this.lblCredits.Name = "lblCredits";
			this.lblCredits.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblCredits.Text = "Credits";
			this.lblCredits.Top = 0.84375F;
			this.lblCredits.Width = 0.84375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.03125F;
			this.Line1.Width = 10.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10.5F;
			this.Line1.Y1 = 1.03125F;
			this.Line1.Y2 = 1.03125F;
			// 
			// lblCheck
			// 
			this.lblCheck.Height = 0.1875F;
			this.lblCheck.HyperLink = null;
			this.lblCheck.Left = 0.875F;
			this.lblCheck.Name = "lblCheck";
			this.lblCheck.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblCheck.Text = "Check";
			this.lblCheck.Top = 0.84375F;
			this.lblCheck.Width = 0.4375F;
			// 
			// lblJournalOptions
			// 
			this.lblJournalOptions.Height = 0.1875F;
			this.lblJournalOptions.HyperLink = null;
			this.lblJournalOptions.Left = 2.3125F;
			this.lblJournalOptions.Name = "lblJournalOptions";
			this.lblJournalOptions.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.lblJournalOptions.Text = "Label8";
			this.lblJournalOptions.Top = 0.5625F;
			this.lblJournalOptions.Width = 5.8125F;
			// 
			// lblEncumbrance
			// 
			this.lblEncumbrance.Height = 0.1875F;
			this.lblEncumbrance.HyperLink = null;
			this.lblEncumbrance.Left = 9.625F;
			this.lblEncumbrance.Name = "lblEncumbrance";
			this.lblEncumbrance.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblEncumbrance.Text = "Enc";
			this.lblEncumbrance.Top = 0.84375F;
			this.lblEncumbrance.Width = 0.84375F;
			// 
			// lblLiquidated
			// 
			this.lblLiquidated.Height = 0.1875F;
			this.lblLiquidated.HyperLink = null;
			this.lblLiquidated.Left = 5.78125F;
			this.lblLiquidated.Name = "lblLiquidated";
			this.lblLiquidated.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblLiquidated.Text = "Liq?";
			this.lblLiquidated.Top = 0.84375F;
			this.lblLiquidated.Width = 0.3125F;
			// 
			// lblLiquidatedCheck
			// 
			this.lblLiquidatedCheck.Height = 0.1875F;
			this.lblLiquidatedCheck.HyperLink = null;
			this.lblLiquidatedCheck.Left = 6.21875F;
			this.lblLiquidatedCheck.Name = "lblLiquidatedCheck";
			this.lblLiquidatedCheck.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblLiquidatedCheck.Text = "Liq Check";
			this.lblLiquidatedCheck.Top = 0.84375F;
			this.lblLiquidatedCheck.Width = 0.78125F;
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.Height = 0.19F;
			this.lblAccountTitle.HyperLink = null;
			this.lblAccountTitle.Left = 0F;
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblAccountTitle.Text = "Label31";
			this.lblAccountTitle.Top = 0.0625F;
			this.lblAccountTitle.Width = 7.09375F;
			// 
			// fldHeaderDebitsTotal
			// 
			this.fldHeaderDebitsTotal.Height = 0.1875F;
			this.fldHeaderDebitsTotal.Left = 7.78125F;
			this.fldHeaderDebitsTotal.Name = "fldHeaderDebitsTotal";
			this.fldHeaderDebitsTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldHeaderDebitsTotal.Text = "Field7";
			this.fldHeaderDebitsTotal.Top = 0.03125F;
			this.fldHeaderDebitsTotal.Width = 0.84375F;
			// 
			// fldHeaderCreditsTotal
			// 
			this.fldHeaderCreditsTotal.Height = 0.1875F;
			this.fldHeaderCreditsTotal.Left = 8.6875F;
			this.fldHeaderCreditsTotal.Name = "fldHeaderCreditsTotal";
			this.fldHeaderCreditsTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldHeaderCreditsTotal.Text = "Field8";
			this.fldHeaderCreditsTotal.Top = 0.03125F;
			this.fldHeaderCreditsTotal.Width = 0.84375F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.0625F;
			this.Binder.Left = 5.625F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.78125F;
			// 
			// fldHeaderEncumbranceTotal
			// 
			this.fldHeaderEncumbranceTotal.Height = 0.1875F;
			this.fldHeaderEncumbranceTotal.Left = 9.625F;
			this.fldHeaderEncumbranceTotal.Name = "fldHeaderEncumbranceTotal";
			this.fldHeaderEncumbranceTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldHeaderEncumbranceTotal.Text = "Field8";
			this.fldHeaderEncumbranceTotal.Top = 0.03125F;
			this.fldHeaderEncumbranceTotal.Width = 0.84375F;
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.1875F;
			this.fldPeriod.Left = 0.125F;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldPeriod.Text = "Field1";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.25F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 0.4375F;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldJournal.Text = "Field2";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 1.34375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldDate.Text = "Field3";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.53125F;
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.1875F;
			this.fldVendor.Left = 1.90625F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldVendor.Text = "Field4";
			this.fldVendor.Top = 0F;
			this.fldVendor.Width = 2.0625F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 4.03125F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDescription.Text = "Field5";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.71875F;
			// 
			// fldRCB
			// 
			this.fldRCB.Height = 0.1875F;
			this.fldRCB.Left = 7.125F;
			this.fldRCB.Name = "fldRCB";
			this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldRCB.Text = "Field6";
			this.fldRCB.Top = 0F;
			this.fldRCB.Width = 0.1875F;
			// 
			// fldDebits
			// 
			this.fldDebits.Height = 0.1875F;
			this.fldDebits.Left = 7.78125F;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDebits.Text = "Field7";
			this.fldDebits.Top = 0F;
			this.fldDebits.Width = 0.84375F;
			// 
			// fldCredits
			// 
			this.fldCredits.Height = 0.1875F;
			this.fldCredits.Left = 8.6875F;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCredits.Text = "Field8";
			this.fldCredits.Top = 0F;
			this.fldCredits.Width = 0.84375F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 7.40625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = "Field6";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 0.90625F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = "Field2";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.4375F;
			// 
			// fldEncumbrance
			// 
			this.fldEncumbrance.Height = 0.1875F;
			this.fldEncumbrance.Left = 9.625F;
			this.fldEncumbrance.Name = "fldEncumbrance";
			this.fldEncumbrance.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldEncumbrance.Text = "Field8";
			this.fldEncumbrance.Top = 0F;
			this.fldEncumbrance.Width = 0.84375F;
			// 
			// chkLiquidated
			// 
			this.chkLiquidated.Height = 0.125F;
			this.chkLiquidated.Left = 5.78125F;
			this.chkLiquidated.Name = "chkLiquidated";
			this.chkLiquidated.Style = "text-align: right";
			this.chkLiquidated.Text = null;
			this.chkLiquidated.Top = 0.03125F;
			this.chkLiquidated.Width = 0.1875F;
			// 
			// fldLiquidatedCheck
			// 
			this.fldLiquidatedCheck.Height = 0.1875F;
			this.fldLiquidatedCheck.Left = 6.21875F;
			this.fldLiquidatedCheck.Name = "fldLiquidatedCheck";
			this.fldLiquidatedCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldLiquidatedCheck.Text = "Field3";
			this.fldLiquidatedCheck.Top = 0F;
			this.fldLiquidatedCheck.Width = 0.78125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 7.15625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Label21.Text = "Totals--";
			this.Label21.Top = 0.0625F;
			this.Label21.Width = 0.59375F;
			// 
			// fldDebitTotal
			// 
			this.fldDebitTotal.Height = 0.1875F;
			this.fldDebitTotal.Left = 7.78125F;
			this.fldDebitTotal.Name = "fldDebitTotal";
			this.fldDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDebitTotal.Text = "Field1";
			this.fldDebitTotal.Top = 0.0625F;
			this.fldDebitTotal.Width = 0.84375F;
			// 
			// fldCreditTotal
			// 
			this.fldCreditTotal.Height = 0.1875F;
			this.fldCreditTotal.Left = 8.6875F;
			this.fldCreditTotal.Name = "fldCreditTotal";
			this.fldCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCreditTotal.Text = "Field2";
			this.fldCreditTotal.Top = 0.0625F;
			this.fldCreditTotal.Width = 0.84375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 6.4375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.03125F;
			this.Line3.Width = 4.0625F;
			this.Line3.X1 = 6.4375F;
			this.Line3.X2 = 10.5F;
			this.Line3.Y1 = 0.03125F;
			this.Line3.Y2 = 0.03125F;
			// 
			// fldEncumbranceTotal
			// 
			this.fldEncumbranceTotal.Height = 0.1875F;
			this.fldEncumbranceTotal.Left = 9.625F;
			this.fldEncumbranceTotal.Name = "fldEncumbranceTotal";
			this.fldEncumbranceTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldEncumbranceTotal.Text = "99,999,999.00";
			this.fldEncumbranceTotal.Top = 0.0625F;
			this.fldEncumbranceTotal.Width = 0.84375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 6.65625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Label22.Text = "Final Totals--";
			this.Label22.Top = 0.0625F;
			this.Label22.Width = 1.09375F;
			// 
			// fldDebitsFinalTotal
			// 
			this.fldDebitsFinalTotal.Height = 0.1875F;
			this.fldDebitsFinalTotal.Left = 7.78125F;
			this.fldDebitsFinalTotal.Name = "fldDebitsFinalTotal";
			this.fldDebitsFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDebitsFinalTotal.Text = "Field1";
			this.fldDebitsFinalTotal.Top = 0.0625F;
			this.fldDebitsFinalTotal.Width = 0.84375F;
			// 
			// fldCreditsFinalTotal
			// 
			this.fldCreditsFinalTotal.Height = 0.1875F;
			this.fldCreditsFinalTotal.Left = 8.6875F;
			this.fldCreditsFinalTotal.Name = "fldCreditsFinalTotal";
			this.fldCreditsFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCreditsFinalTotal.Text = "Field2";
			this.fldCreditsFinalTotal.Top = 0.0625F;
			this.fldCreditsFinalTotal.Width = 0.84375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 6.4375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.03125F;
			this.Line4.Width = 4.0625F;
			this.Line4.X1 = 6.4375F;
			this.Line4.X2 = 10.5F;
			this.Line4.Y1 = 0.03125F;
			this.Line4.Y2 = 0.03125F;
			// 
			// fldEncumbranceFinalTotal
			// 
			this.fldEncumbranceFinalTotal.Height = 0.1875F;
			this.fldEncumbranceFinalTotal.Left = 9.625F;
			this.fldEncumbranceFinalTotal.Name = "fldEncumbranceFinalTotal";
			this.fldEncumbranceFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldEncumbranceFinalTotal.Text = "Field2";
			this.fldEncumbranceFinalTotal.Top = 0.0625F;
			this.fldEncumbranceFinalTotal.Width = 0.84375F;
			// 
			// rptAccountResearch
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.51042F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRCBType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournalOptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEncumbrance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLiquidated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLiquidatedCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderDebitsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderCreditsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderEncumbranceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbrance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLiquidated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLiquidatedCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbranceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitsFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditsFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbranceFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbrance;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkLiquidated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLiquidatedCheck;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblJournal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVendor;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRCBType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDebits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblJournalOptions;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEncumbrance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLiquidated;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLiquidatedCheck;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitsFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditsFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbranceFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderDebitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderCreditsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderEncumbranceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbranceTotal;
	}
}
