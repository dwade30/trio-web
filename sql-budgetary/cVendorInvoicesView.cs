﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;

namespace TWBD0000
{
	public class cVendorInvoicesView
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cLinkedDocumentController docCont = new cLinkedDocumentController();
		//private cLinkedDocumentController docCont_AutoInitialized;

		//private cLinkedDocumentController docCont
		//{
		//	get
		//	{
		//		if (docCont_AutoInitialized == null)
		//		{
		//			docCont_AutoInitialized = new cLinkedDocumentController();
		//		}
		//		return docCont_AutoInitialized;
		//	}
		//	set
		//	{
		//		docCont_AutoInitialized = value;
		//	}
		//}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cAPJournalController apCont = new cAPJournalController();
		private cAPJournalController apCont_AutoInitialized;

		private cAPJournalController apCont
		{
			get
			{
				if (apCont_AutoInitialized == null)
				{
					apCont_AutoInitialized = new cAPJournalController();
				}
				return apCont_AutoInitialized;
			}
			set
			{
				apCont_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cVendorController vendCont = new cVendorController();
		private cVendorController vendCont_AutoInitialized;

		private cVendorController vendCont
		{
			get
			{
				if (vendCont_AutoInitialized == null)
				{
					vendCont_AutoInitialized = new cVendorController();
				}
				return vendCont_AutoInitialized;
			}
			set
			{
				vendCont_AutoInitialized = value;
			}
		}

		private cVendor vend;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection invoiceColl = new cGenericCollection();
		private cGenericCollection invoiceColl_AutoInitialized;

		private cGenericCollection invoiceColl
		{
			get
			{
				if (invoiceColl_AutoInitialized == null)
				{
					invoiceColl_AutoInitialized = new cGenericCollection();
				}
				return invoiceColl_AutoInitialized;
			}
			set
			{
				invoiceColl_AutoInitialized = value;
			}
		}

		public delegate void ViewChangedEventHandler();

		public event ViewChangedEventHandler ViewChanged;

		public cGenericCollection Invoices
		{
			get
			{
				cGenericCollection Invoices = null;
				Invoices = invoiceColl;
				return Invoices;
			}
		}

		public cVendor Vendor
		{
			get
			{
				cVendor Vendor = null;
				Vendor = vend;
				return Vendor;
			}
		}

		public void LoadVendor(int lngVendorID)
		{
			RefreshAll(lngVendorID);
		}

		private void RefreshVendor(int lngID)
		{
			vend = vendCont.GetVendor(lngID);
			if (vendCont.HadError)
			{
				Information.Err().Raise(vendCont.LastErrorNumber, null, vendCont.LastErrorMessage, null, null);
			}
		}

		private void RefreshInvoices()
		{
			if (!(vend == null))
			{
				invoiceColl.ClearList();
				invoiceColl = null;
				invoiceColl = apCont.GetAPJournalsByVendorNumber(vend.VendorNumber);
				if (apCont.HadError)
				{
					Information.Err().Raise(apCont.LastErrorNumber, null, apCont.LastErrorMessage, null, null);
				}
			}
		}

		private void RefreshAll(int lngVendorID)
		{
			RefreshVendor(lngVendorID);
			RefreshInvoices();
			if (this.ViewChanged != null)
				this.ViewChanged();
		}

		public void ViewInvoice(int lngInvoiceID)
		{
			if (lngInvoiceID > 0)
			{
				cAPJournal aJourn;
				aJourn = apCont.GetFullAPJournal(lngInvoiceID);
				aJourn.Vendor = vend;
				frmPostedAPJournal.InstancePtr.Init(ref aJourn);
			}
		}
	}
}
