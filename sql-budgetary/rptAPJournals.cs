﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAPJournals.
	/// </summary>
	public partial class rptAPJournals : BaseSectionReport
	{
		public static rptAPJournals InstancePtr
		{
			get
			{
				return (rptAPJournals)Sys.GetInstance(typeof(rptAPJournals));
			}
		}

		protected rptAPJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsGeneralInfo.Dispose();
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAPJournals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitTotal;
		// vbPorter upgrade warning: curCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditTotal;
		// vbPorter upgrade warning: curEncTotal As Decimal	OnWrite(short, Decimal)
		Decimal curEncTotal;
		bool blnFirstControl;

		public rptAPJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsInfo.EndOfFile())
			{
				TryAgain:
				;
				rsGeneralInfo.MoveNext();
				if (rsGeneralInfo.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
					rsInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsGeneralInfo.Get_Fields_Int32("ID"));
					if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
					{
						rsInfo.MoveLast();
						rsInfo.MoveFirst();
					}
					else
					{
						goto TryAgain;
					}
				}
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsDesc = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			rsDesc.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)));
			if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
			{
				lblDescription.Text = rsDesc.Get_Fields_String("Description");
			}
			else
			{
				lblDescription.Text = "UNKNOWN";
			}
			Label6.Text = "Journal No. " + this.UserData + "       Post Date: " + DateTime.Today.ToShortDateString() + "       Type: " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 3);
			if (frmPosting.InstancePtr.vsCheckDates.TextMatrix(rptPosting.InstancePtr.intRowHolder, 0) != "")
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND CheckDate = '" + frmPosting.InstancePtr.vsCheckDates.TextMatrix(rptPosting.InstancePtr.intRowHolder, 0) + "' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " ORDER BY ID");
			}
			else
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " ORDER BY ID");
			}
			rsInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsGeneralInfo.Get_Fields_Int32("ID"));
			if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
			{
				rsInfo.MoveLast();
				rsInfo.MoveFirst();
			}
			if (rsGeneralInfo.EndOfFile() != true && rsGeneralInfo.BeginningOfFile() != true)
			{
				rsGeneralInfo.MoveLast();
				rsGeneralInfo.MoveFirst();
			}
			curDebitTotal = 0;
			curCreditTotal = 0;
			curEncTotal = 0;
			blnFirstControl = false;
			Line2.Visible = false;
			rsDesc.Dispose();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnFirstControl == false)
			{
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
				{
					Line2.Visible = true;
					blnFirstControl = true;
				}
			}
			else
			{
				if (Line2.Visible == true)
				{
					Line2.Visible = false;
				}
			}
			Field3.Text = rsInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			Field6.Text = rsInfo.Get_Fields_String("Account");
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			Field1.Text = Strings.Format(rsGeneralInfo.Get_Fields("Period"), "00");
			if (rsInfo.Get_Fields_Decimal("Encumbrance") > 0)
			{
				Field8.Text = Strings.Format(rsInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
			}
			else
			{
				Field8.Text = "";
			}
			curEncTotal += rsInfo.Get_Fields_Decimal("Encumbrance");
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			Field12.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields("CheckNumber"));
			Field2.Text = Strings.Format(rsGeneralInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yyyy");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			if (rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount") - rsInfo.Get_Fields_Decimal("Encumbrance") < 0)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				Field14.Text = Strings.Format(((rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount") - rsInfo.Get_Fields_Decimal("Encumbrance")) * -1), "#,##0.00");
				Field7.Text = "";
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curCreditTotal += ((rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount") - rsInfo.Get_Fields_Decimal("Encumbrance")) * -1);
				}
			}
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			else if (rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount") - rsInfo.Get_Fields_Decimal("Encumbrance") > 0)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				Field7.Text = Strings.Format(rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount") - rsInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
				Field14.Text = "";
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curDebitTotal += (rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount") - rsInfo.Get_Fields_Decimal("Encumbrance"));
				}
			}
			else
			{
				Field7.Text = "";
				Field14.Text = "";
			}
			Field13.Text = rsGeneralInfo.Get_Fields_String("Reference");
			// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
			Field10.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields("Warrant"));
			Field9.Text = rsInfo.Get_Fields_String("Project");
			if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
			{
				Field11.Text = "";
			}
			else
			{
				Field11.Text = modValidateAccount.GetFormat_6(rsGeneralInfo.Get_Fields_Int32("VendorNumber"), 5);
			}
			rsInfo.MoveNext();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (curEncTotal != 0)
			{
				if (frmPosting.InstancePtr.chkBreak.CheckState == Wisej.Web.CheckState.Checked)
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
				}
				else
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				srptEncLiquid.Report = rptEncumbranceLiquidation.InstancePtr;
				srptEncLiquid.Report.UserData = this.UserData;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				srptEncLiquid = null;
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			if (curDebitTotal != 0)
			{
				fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			}
			else
			{
				fldDebitTotal.Text = "";
			}
			if (curCreditTotal != 0)
			{
				fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			}
			else
			{
				fldCreditTotal.Text = "";
			}
			if (curEncTotal > 0)
			{
				fldEncTotal.Text = Strings.Format(curEncTotal, "#,##0.00");
			}
			else
			{
				fldEncTotal.Text = "";
			}
			rptSubTotals.Report = rptJournalTotals.InstancePtr;
			rptSubTotals.Report.UserData = this.UserData;
		}

		private void rptAPJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAPJournals.Icon	= "rptAPJournals.dsx":0000";
			//rptAPJournals.Left	= 0;
			//rptAPJournals.Top	= 0;
			//rptAPJournals.Width	= 11880;
			//rptAPJournals.Height	= 8595;
			//rptAPJournals.StartUpPosition	= 3;
			//rptAPJournals.SectionData	= "rptAPJournals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
