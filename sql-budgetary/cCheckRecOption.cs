﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWBD0000
{
    public class cCheckRecOption
    {
        private String startDate = "";
        private String endDate = "";
        public String FileName { get; set; } = "";
        public String StartDate
        {
            get
            {
                return startDate; 

            }
            set
            {
                DateTime parsedDate;
                if (DateTime.TryParse(value, out parsedDate))
                {
                    startDate = value;
                }
            }
        }

        public String EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                DateTime parsedDate;
                if (DateTime.TryParse(value, out parsedDate))
                {
                    endDate = value;
                }
            }
        }
    }
}
