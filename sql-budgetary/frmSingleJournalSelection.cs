﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSingleJournalSelection.
	/// </summary>
	public partial class frmSingleJournalSelection : BaseForm
	{
		public frmSingleJournalSelection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public string strReportType = "";

		private void CancelButton_Click(object sender, System.EventArgs e)
		{
			if (strReportType == "A")
			{
				modBudgetaryAccounting.Statics.lngSelectedJournal = -1;
			}
			Close();
		}

		private void cboJournals_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ToolTip1.SetToolTip(cboJournals, cboJournals.Text);
		}

		private void frmSingleJournalSelection_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsJournals = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (strReportType == "P")
			{
				rsJournals.OpenRecordset("SELECT DISTINCT JournalNumber, Description FROM JournalMaster WHERE Status = 'P' ORDER BY JournalNumber");
			}
			else if (strReportType == "A")
			{
				rsJournals.OpenRecordset("SELECT DISTINCT JournalNumber, Description FROM JournalMaster WHERE Status <> 'D' ORDER BY JournalNumber");
			}
			else
			{
				rsJournals.OpenRecordset("SELECT DISTINCT JournalNumber, Description FROM JournalMaster WHERE Status <> 'P' AND Status <> 'D' ORDER BY JournalNumber");
			}
			cboJournals.Clear();
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboJournals.AddItem(modValidateAccount.GetFormat_6(rsJournals.Get_Fields("JournalNumber"), 4) + " " + rsJournals.Get_Fields_String("Description"));
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
				cboJournals.SelectedIndex = 0;
				ToolTip1.SetToolTip(cboJournals, cboJournals.Text);
			}
			cboJournals.Focus();
		}

		private void frmSingleJournalSelection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSingleJournalSelection.ScaleWidth	= 3915;
			//frmSingleJournalSelection.ScaleHeight	= 2310;
			//frmSingleJournalSelection.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsJournalCheck = new clsDRWrapper();
			if (cboJournals.SelectedIndex != -1)
			{
				if (strReportType == "P")
				{
					rsJournalCheck.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				}
				else if (strReportType == "A")
				{
					modBudgetaryAccounting.Statics.lngSelectedJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
					Close();
					return;
				}
				else
				{
					rsJournalCheck.OpenRecordset("SELECT * FROM JournalMaster WHERE Status <> 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				}
				if (rsJournalCheck.EndOfFile() != true && rsJournalCheck.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					if (strReportType == "P")
					{
						MessageBox.Show("There are no posted journals that match the number you entered.  Please enter a valid journal ID to continue.", "No Journal Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("There are no unposted journals that match the number you entered.  Please enter a valid journal ID to continue.", "No Journal Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					return;
				}
			}
			else
			{
				MessageBox.Show("You must enter a journal number before you may proceed.", "No Journal Entered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			this.Hide();
			if (strReportType == "P")
			{
				frmReportViewer.InstancePtr.Init(rptPostedJournal.InstancePtr);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(rptUnpostedJournal.InstancePtr);
			}
		}
	}
}
