//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAccountSetup.
	/// </summary>
	partial class frmAccountSetup : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtInfo;
		public fecherFoundation.FCCheckBox chkPUCChartOfAccounts;
		public fecherFoundation.FCComboBox cboAccountTypes;
		public fecherFoundation.FCFrame frame1;
		public fecherFoundation.FCTextBox txtInfo_2;
		public fecherFoundation.FCTextBox txtInfo_1;
		public fecherFoundation.FCTextBox txtInfo_0;
		public fecherFoundation.FCTextBox txtInfo_3;
		public fecherFoundation.FCLabel lblExpExample;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblExpDiv;
		public fecherFoundation.FCLabel lblExpDept;
		public fecherFoundation.FCLabel lblExpenditureDemo;
		public fecherFoundation.FCLabel lblObject;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtInfo_5;
		public fecherFoundation.FCTextBox txtInfo_4;
		public fecherFoundation.FCTextBox txtInfo_6;
		public fecherFoundation.FCLabel lblRevExample;
		public fecherFoundation.FCLabel lblRevRevenue;
		public fecherFoundation.FCLabel lblRevDiv;
		public fecherFoundation.FCLabel lblRevDept;
		public fecherFoundation.FCLabel lblRevenueDemo;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtInfo_8;
		public fecherFoundation.FCTextBox txtInfo_7;
		public fecherFoundation.FCTextBox txtInfo_9;
		public fecherFoundation.FCLabel lblLedgerExample;
		public fecherFoundation.FCLabel lblFund;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblLedgerDemo;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountSetup));
            this.chkPUCChartOfAccounts = new fecherFoundation.FCCheckBox();
            this.cboAccountTypes = new fecherFoundation.FCComboBox();
            this.frame1 = new fecherFoundation.FCFrame();
            this.txtInfo_2 = new fecherFoundation.FCTextBox();
            this.txtInfo_1 = new fecherFoundation.FCTextBox();
            this.txtInfo_0 = new fecherFoundation.FCTextBox();
            this.txtInfo_3 = new fecherFoundation.FCTextBox();
            this.lblExpExample = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblExpense = new fecherFoundation.FCLabel();
            this.lblExpDiv = new fecherFoundation.FCLabel();
            this.lblExpDept = new fecherFoundation.FCLabel();
            this.lblExpenditureDemo = new fecherFoundation.FCLabel();
            this.lblObject = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtInfo_5 = new fecherFoundation.FCTextBox();
            this.txtInfo_4 = new fecherFoundation.FCTextBox();
            this.txtInfo_6 = new fecherFoundation.FCTextBox();
            this.lblRevExample = new fecherFoundation.FCLabel();
            this.lblRevRevenue = new fecherFoundation.FCLabel();
            this.lblRevDiv = new fecherFoundation.FCLabel();
            this.lblRevDept = new fecherFoundation.FCLabel();
            this.lblRevenueDemo = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtInfo_8 = new fecherFoundation.FCTextBox();
            this.txtInfo_7 = new fecherFoundation.FCTextBox();
            this.txtInfo_9 = new fecherFoundation.FCTextBox();
            this.lblLedgerExample = new fecherFoundation.FCLabel();
            this.lblFund = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblLedgerDemo = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.btnFileSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPUCChartOfAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frame1)).BeginInit();
            this.frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnFileSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 524);
            this.BottomPanel.Size = new System.Drawing.Size(840, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkPUCChartOfAccounts);
            this.ClientArea.Controls.Add(this.cboAccountTypes);
            this.ClientArea.Controls.Add(this.frame1);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(840, 464);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(840, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(198, 30);
            this.HeaderText.Text = "Account Formats";
            // 
            // chkPUCChartOfAccounts
            // 
            this.chkPUCChartOfAccounts.Location = new System.Drawing.Point(30, 408);
            this.chkPUCChartOfAccounts.Name = "chkPUCChartOfAccounts";
            this.chkPUCChartOfAccounts.Size = new System.Drawing.Size(174, 24);
            this.chkPUCChartOfAccounts.TabIndex = 32;
            this.chkPUCChartOfAccounts.Text = "PUC Chart of Accounts";
            // 
            // cboAccountTypes
            // 
            this.cboAccountTypes.BackColor = System.Drawing.SystemColors.Window;
            this.cboAccountTypes.Items.AddRange(new object[] {
            "1  -  Town",
            "2  -  School",
            "3  -  Both"});
            this.cboAccountTypes.Location = new System.Drawing.Point(219, 30);
            this.cboAccountTypes.Name = "cboAccountTypes";
            this.cboAccountTypes.Size = new System.Drawing.Size(331, 40);
            this.cboAccountTypes.TabIndex = 30;
            this.cboAccountTypes.SelectedIndexChanged += new System.EventHandler(this.cboAccountTypes_SelectedIndexChanged);
            // 
            // frame1
            // 
            this.frame1.Controls.Add(this.txtInfo_2);
            this.frame1.Controls.Add(this.txtInfo_1);
            this.frame1.Controls.Add(this.txtInfo_0);
            this.frame1.Controls.Add(this.txtInfo_3);
            this.frame1.Controls.Add(this.lblExpExample);
            this.frame1.Controls.Add(this.Label6);
            this.frame1.Controls.Add(this.lblExpense);
            this.frame1.Controls.Add(this.lblExpDiv);
            this.frame1.Controls.Add(this.lblExpDept);
            this.frame1.Controls.Add(this.lblExpenditureDemo);
            this.frame1.Controls.Add(this.lblObject);
            this.frame1.Location = new System.Drawing.Point(30, 80);
            this.frame1.Name = "frame1";
            this.frame1.Size = new System.Drawing.Size(250, 308);
            this.frame1.TabIndex = 10;
            this.frame1.Text = "Expense";
            // 
            // txtInfo_2
            // 
            this.txtInfo_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_2.Location = new System.Drawing.Point(160, 150);
            this.txtInfo_2.MaxLength = 2;
            this.txtInfo_2.Name = "txtInfo_2";
            this.txtInfo_2.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_2.TabIndex = 13;
            this.txtInfo_2.Text = "0";
            this.txtInfo_2.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_2.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_1
            // 
            this.txtInfo_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_1.Location = new System.Drawing.Point(160, 90);
            this.txtInfo_1.MaxLength = 2;
            this.txtInfo_1.Name = "txtInfo_1";
            this.txtInfo_1.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_1.TabIndex = 12;
            this.txtInfo_1.Text = "0";
            this.txtInfo_1.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_1.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_0
            // 
            this.txtInfo_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_0.Location = new System.Drawing.Point(160, 30);
            this.txtInfo_0.MaxLength = 2;
            this.txtInfo_0.Name = "txtInfo_0";
            this.txtInfo_0.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_0.TabIndex = 11;
            this.txtInfo_0.Text = "0";
            this.txtInfo_0.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_0.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_3
            // 
            this.txtInfo_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_3.Location = new System.Drawing.Point(160, 210);
            this.txtInfo_3.MaxLength = 2;
            this.txtInfo_3.Name = "txtInfo_3";
            this.txtInfo_3.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_3.TabIndex = 15;
            this.txtInfo_3.Text = "0";
            this.txtInfo_3.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_3.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // lblExpExample
            // 
            this.lblExpExample.Location = new System.Drawing.Point(12, 267);
            this.lblExpExample.Name = "lblExpExample";
            this.lblExpExample.Size = new System.Drawing.Size(74, 18);
            this.lblExpExample.TabIndex = 27;
            this.lblExpExample.Text = "EXAMPLE";
            this.lblExpExample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(277, 110);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(86, 22);
            this.Label6.TabIndex = 24;
            this.Label6.Text = "OBJECT";
            // 
            // lblExpense
            // 
            this.lblExpense.Location = new System.Drawing.Point(20, 164);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(86, 22);
            this.lblExpense.TabIndex = 22;
            this.lblExpense.Text = "EXPENSE";
            this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExpDiv
            // 
            this.lblExpDiv.Location = new System.Drawing.Point(20, 104);
            this.lblExpDiv.Name = "lblExpDiv";
            this.lblExpDiv.Size = new System.Drawing.Size(86, 22);
            this.lblExpDiv.TabIndex = 20;
            this.lblExpDiv.Text = "DIVISION";
            this.lblExpDiv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExpDept
            // 
            this.lblExpDept.Location = new System.Drawing.Point(20, 44);
            this.lblExpDept.Name = "lblExpDept";
            this.lblExpDept.Size = new System.Drawing.Size(86, 22);
            this.lblExpDept.TabIndex = 18;
            this.lblExpDept.Text = "DEPARTMENT";
            this.lblExpDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExpenditureDemo
            // 
            this.lblExpenditureDemo.Location = new System.Drawing.Point(93, 267);
            this.lblExpenditureDemo.Name = "lblExpenditureDemo";
            this.lblExpenditureDemo.Size = new System.Drawing.Size(137, 18);
            this.lblExpenditureDemo.TabIndex = 16;
            this.lblExpenditureDemo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblObject
            // 
            this.lblObject.Location = new System.Drawing.Point(20, 224);
            this.lblObject.Name = "lblObject";
            this.lblObject.Size = new System.Drawing.Size(86, 22);
            this.lblObject.TabIndex = 14;
            this.lblObject.Text = "OBJECT";
            this.lblObject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtInfo_5);
            this.Frame2.Controls.Add(this.txtInfo_4);
            this.Frame2.Controls.Add(this.txtInfo_6);
            this.Frame2.Controls.Add(this.lblRevExample);
            this.Frame2.Controls.Add(this.lblRevRevenue);
            this.Frame2.Controls.Add(this.lblRevDiv);
            this.Frame2.Controls.Add(this.lblRevDept);
            this.Frame2.Controls.Add(this.lblRevenueDemo);
            this.Frame2.Location = new System.Drawing.Point(300, 80);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(250, 248);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Revenue";
            // 
            // txtInfo_5
            // 
            this.txtInfo_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_5.Location = new System.Drawing.Point(160, 90);
            this.txtInfo_5.MaxLength = 2;
            this.txtInfo_5.Name = "txtInfo_5";
            this.txtInfo_5.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_5.TabIndex = 19;
            this.txtInfo_5.Text = "0";
            this.txtInfo_5.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_5.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_4
            // 
            this.txtInfo_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_4.Location = new System.Drawing.Point(160, 30);
            this.txtInfo_4.LockedOriginal = true;
            this.txtInfo_4.MaxLength = 2;
            this.txtInfo_4.Name = "txtInfo_4";
            this.txtInfo_4.ReadOnly = true;
            this.txtInfo_4.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_4.TabIndex = 17;
            this.txtInfo_4.TabStop = false;
            this.txtInfo_4.Text = "0";
            this.txtInfo_4.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_4.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_6
            // 
            this.txtInfo_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_6.Location = new System.Drawing.Point(160, 150);
            this.txtInfo_6.MaxLength = 2;
            this.txtInfo_6.Name = "txtInfo_6";
            this.txtInfo_6.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_6.TabIndex = 21;
            this.txtInfo_6.Text = "0";
            this.txtInfo_6.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_6.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // lblRevExample
            // 
            this.lblRevExample.Location = new System.Drawing.Point(20, 210);
            this.lblRevExample.Name = "lblRevExample";
            this.lblRevExample.Size = new System.Drawing.Size(58, 18);
            this.lblRevExample.TabIndex = 28;
            this.lblRevExample.Text = "EXAMPLE";
            this.lblRevExample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRevRevenue
            // 
            this.lblRevRevenue.Location = new System.Drawing.Point(20, 164);
            this.lblRevRevenue.Name = "lblRevRevenue";
            this.lblRevRevenue.Size = new System.Drawing.Size(86, 22);
            this.lblRevRevenue.TabIndex = 9;
            this.lblRevRevenue.Text = "REVENUE";
            this.lblRevRevenue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRevDiv
            // 
            this.lblRevDiv.Location = new System.Drawing.Point(20, 104);
            this.lblRevDiv.Name = "lblRevDiv";
            this.lblRevDiv.Size = new System.Drawing.Size(86, 22);
            this.lblRevDiv.TabIndex = 8;
            this.lblRevDiv.Text = "DIVISION";
            this.lblRevDiv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRevDept
            // 
            this.lblRevDept.Location = new System.Drawing.Point(20, 44);
            this.lblRevDept.Name = "lblRevDept";
            this.lblRevDept.Size = new System.Drawing.Size(86, 22);
            this.lblRevDept.TabIndex = 7;
            this.lblRevDept.Text = "DEPARTMENT";
            this.lblRevDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRevenueDemo
            // 
            this.lblRevenueDemo.Location = new System.Drawing.Point(93, 210);
            this.lblRevenueDemo.Name = "lblRevenueDemo";
            this.lblRevenueDemo.Size = new System.Drawing.Size(137, 18);
            this.lblRevenueDemo.TabIndex = 6;
            this.lblRevenueDemo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtInfo_8);
            this.Frame3.Controls.Add(this.txtInfo_7);
            this.Frame3.Controls.Add(this.txtInfo_9);
            this.Frame3.Controls.Add(this.lblLedgerExample);
            this.Frame3.Controls.Add(this.lblFund);
            this.Frame3.Controls.Add(this.lblAccount);
            this.Frame3.Controls.Add(this.lblYear);
            this.Frame3.Controls.Add(this.lblLedgerDemo);
            this.Frame3.Location = new System.Drawing.Point(570, 80);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(250, 248);
            this.Frame3.Text = "General Ledger";
            // 
            // txtInfo_8
            // 
            this.txtInfo_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_8.Location = new System.Drawing.Point(160, 86);
            this.txtInfo_8.MaxLength = 2;
            this.txtInfo_8.Name = "txtInfo_8";
            this.txtInfo_8.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_8.TabIndex = 25;
            this.txtInfo_8.Text = "0";
            this.txtInfo_8.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_8.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_7
            // 
            this.txtInfo_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_7.Location = new System.Drawing.Point(160, 25);
            this.txtInfo_7.MaxLength = 2;
            this.txtInfo_7.Name = "txtInfo_7";
            this.txtInfo_7.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_7.TabIndex = 23;
            this.txtInfo_7.Text = "0";
            this.txtInfo_7.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_7.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // txtInfo_9
            // 
            this.txtInfo_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo_9.Location = new System.Drawing.Point(160, 146);
            this.txtInfo_9.MaxLength = 2;
            this.txtInfo_9.Name = "txtInfo_9";
            this.txtInfo_9.Size = new System.Drawing.Size(70, 40);
            this.txtInfo_9.TabIndex = 26;
            this.txtInfo_9.Text = "0";
            this.txtInfo_9.Enter += new System.EventHandler(this.txtInfo_Enter);
            this.txtInfo_9.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            this.txtInfo_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtInfo_Validating);
            // 
            // lblLedgerExample
            // 
            this.lblLedgerExample.Location = new System.Drawing.Point(20, 210);
            this.lblLedgerExample.Name = "lblLedgerExample";
            this.lblLedgerExample.Size = new System.Drawing.Size(58, 22);
            this.lblLedgerExample.TabIndex = 29;
            this.lblLedgerExample.Text = "EXAMPLE";
            this.lblLedgerExample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFund
            // 
            this.lblFund.Location = new System.Drawing.Point(20, 34);
            this.lblFund.Name = "lblFund";
            this.lblFund.Size = new System.Drawing.Size(86, 22);
            this.lblFund.TabIndex = 4;
            this.lblFund.Text = "FUND";
            this.lblFund.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(20, 94);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(86, 22);
            this.lblAccount.TabIndex = 3;
            this.lblAccount.Text = "ACCOUNT";
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(20, 154);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(86, 22);
            this.lblYear.TabIndex = 2;
            this.lblYear.Text = "SUFFIX";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLedgerDemo
            // 
            this.lblLedgerDemo.Location = new System.Drawing.Point(114, 210);
            this.lblLedgerDemo.Name = "lblLedgerDemo";
            this.lblLedgerDemo.Size = new System.Drawing.Size(116, 22);
            this.lblLedgerDemo.TabIndex = 1;
            this.lblLedgerDemo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(151, 18);
            this.Label1.TabIndex = 31;
            this.Label1.Text = "ACCOUNT TYPES";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSaveExit,
            this.mnuSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 1;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit  ";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // mnuSeperator
            // 
            this.mnuSeperator.Index = 2;
            this.mnuSeperator.Name = "mnuSeperator";
            this.mnuSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 3;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // btnFileSaveExit
            // 
            this.btnFileSaveExit.AppearanceKey = "acceptButton";
            this.btnFileSaveExit.Location = new System.Drawing.Point(327, 30);
            this.btnFileSaveExit.Name = "btnFileSaveExit";
            this.btnFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnFileSaveExit.Size = new System.Drawing.Size(131, 48);
            this.btnFileSaveExit.Text = "Save";
            this.btnFileSaveExit.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmAccountSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(840, 632);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmAccountSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Account Formats";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmAccountSetup_Load);
            this.Activated += new System.EventHandler(this.frmAccountSetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAccountSetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAccountSetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPUCChartOfAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frame1)).EndInit();
            this.frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnFileSaveExit;
	}
}