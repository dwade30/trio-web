﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetProjectDetail.
	/// </summary>
	partial class frmGetProjectDetail : BaseForm
	{
		public fecherFoundation.FCComboBox cmbFormat;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCComboBox cmbEditLayout;
		public fecherFoundation.FCLabel lblEditLayout;
		public fecherFoundation.FCComboBox cmbEditFormat;
		public fecherFoundation.FCLabel lblEditFormat;
		public fecherFoundation.FCFrame fraEdit;
		public fecherFoundation.FCFrame fraLayout;
		public fecherFoundation.FCComboBox cboLayout;
		public fecherFoundation.FCComboBox cboLayoutDelete;
		public fecherFoundation.FCFrame fraFormat;
		public fecherFoundation.FCComboBox cboFormat;
		public fecherFoundation.FCComboBox cboFormatDelete;
		public fecherFoundation.FCButton cmdFileProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetProjectDetail));
			this.cmbFormat = new fecherFoundation.FCComboBox();
			this.lblFormat = new fecherFoundation.FCLabel();
			this.cmbEditLayout = new fecherFoundation.FCComboBox();
			this.lblEditLayout = new fecherFoundation.FCLabel();
			this.cmbEditFormat = new fecherFoundation.FCComboBox();
			this.lblEditFormat = new fecherFoundation.FCLabel();
			this.fraEdit = new fecherFoundation.FCFrame();
			this.fraLayout = new fecherFoundation.FCFrame();
			this.cboLayout = new fecherFoundation.FCComboBox();
			this.cboLayoutDelete = new fecherFoundation.FCComboBox();
			this.fraFormat = new fecherFoundation.FCFrame();
			this.cboFormat = new fecherFoundation.FCComboBox();
			this.cboFormatDelete = new fecherFoundation.FCComboBox();
			this.cmdFileProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEdit)).BeginInit();
			this.fraEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLayout)).BeginInit();
			this.fraLayout.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraFormat)).BeginInit();
			this.fraFormat.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 425);
			this.BottomPanel.Size = new System.Drawing.Size(834, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraEdit);
			this.ClientArea.Controls.Add(this.cmdFileProcess);
			this.ClientArea.Size = new System.Drawing.Size(834, 365);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(834, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(403, 30);
			this.HeaderText.Text = "Project Detail Report Customization";
			// 
			// cmbFormat
			// 
			this.cmbFormat.AutoSize = false;
			this.cmbFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormat.FormattingEnabled = true;
			this.cmbFormat.Items.AddRange(new object[] {
				"Report Format",
				"Selection Criteria"
			});
			this.cmbFormat.Location = new System.Drawing.Point(20, 30);
			this.cmbFormat.Name = "cmbFormat";
			this.cmbFormat.Size = new System.Drawing.Size(200, 40);
			this.cmbFormat.TabIndex = 10;
			this.cmbFormat.SelectedIndexChanged += new System.EventHandler(this.optFormat_CheckedChanged);
			// 
			// lblFormat
			// 
			this.lblFormat.AutoSize = true;
			this.lblFormat.Location = new System.Drawing.Point(20, 44);
			this.lblFormat.Name = "lblFormat";
			this.lblFormat.Size = new System.Drawing.Size(4, 14);
			this.lblFormat.TabIndex = 11;
			this.lblFormat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbEditLayout
			// 
			this.cmbEditLayout.AutoSize = false;
			this.cmbEditLayout.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbEditLayout.FormattingEnabled = true;
			this.cmbEditLayout.Items.AddRange(new object[] {
				"Create / Edit",
				"Delete"
			});
			this.cmbEditLayout.Location = new System.Drawing.Point(20, 90);
			this.cmbEditLayout.Name = "cmbEditLayout";
			this.cmbEditLayout.Size = new System.Drawing.Size(300, 40);
			this.cmbEditLayout.TabIndex = 14;
			this.cmbEditLayout.SelectedIndexChanged += new System.EventHandler(this.optEditLayout_CheckedChanged);
			// 
			// lblEditLayout
			// 
			this.lblEditLayout.AutoSize = true;
			this.lblEditLayout.Location = new System.Drawing.Point(9, 74);
			this.lblEditLayout.Name = "lblEditLayout";
			this.lblEditLayout.Size = new System.Drawing.Size(4, 14);
			this.lblEditLayout.TabIndex = 15;
			this.lblEditLayout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbEditFormat
			// 
			this.cmbEditFormat.AutoSize = false;
			this.cmbEditFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbEditFormat.FormattingEnabled = true;
			this.cmbEditFormat.Items.AddRange(new object[] {
				"Create / Edit",
				"Delete"
			});
			this.cmbEditFormat.Location = new System.Drawing.Point(20, 90);
			this.cmbEditFormat.Name = "cmbEditFormat";
			this.cmbEditFormat.Size = new System.Drawing.Size(300, 40);
			this.cmbEditFormat.TabIndex = 9;
			this.cmbEditFormat.SelectedIndexChanged += new System.EventHandler(this.optEditFormat_CheckedChanged);
			// 
			// lblEditFormat
			// 
			this.lblEditFormat.AutoSize = true;
			this.lblEditFormat.Location = new System.Drawing.Point(10, 74);
			this.lblEditFormat.Name = "lblEditFormat";
			this.lblEditFormat.Size = new System.Drawing.Size(4, 14);
			this.lblEditFormat.TabIndex = 10;
			this.lblEditFormat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraEdit
			// 
			this.fraEdit.AppearanceKey = "groupBoxLeftBorder";
			this.fraEdit.Controls.Add(this.fraLayout);
			this.fraEdit.Controls.Add(this.cmbFormat);
			this.fraEdit.Controls.Add(this.lblFormat);
			this.fraEdit.Controls.Add(this.fraFormat);
			this.fraEdit.Location = new System.Drawing.Point(30, 30);
			this.fraEdit.Name = "fraEdit";
			this.fraEdit.Size = new System.Drawing.Size(760, 260);
			this.fraEdit.TabIndex = 0;
			this.fraEdit.Text = "Edit Report Formats And Selection Criteria";
			// 
			// fraLayout
			// 
			this.fraLayout.Controls.Add(this.cboLayout);
			this.fraLayout.Controls.Add(this.cmbEditLayout);
			this.fraLayout.Controls.Add(this.lblEditLayout);
			this.fraLayout.Controls.Add(this.cboLayoutDelete);
			this.fraLayout.Location = new System.Drawing.Point(400, 90);
			this.fraLayout.Name = "fraLayout";
			this.fraLayout.Size = new System.Drawing.Size(340, 150);
			this.fraLayout.TabIndex = 9;
			this.fraLayout.Text = "Selection Criteria";
			// 
			// cboLayout
			// 
			this.cboLayout.AutoSize = false;
			this.cboLayout.BackColor = System.Drawing.SystemColors.Window;
			this.cboLayout.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboLayout.Enabled = false;
			this.cboLayout.FormattingEnabled = true;
			this.cboLayout.Location = new System.Drawing.Point(20, 30);
			this.cboLayout.Name = "cboLayout";
			this.cboLayout.Size = new System.Drawing.Size(300, 40);
			this.cboLayout.TabIndex = 13;
			// 
			// cboLayoutDelete
			// 
			this.cboLayoutDelete.AutoSize = false;
			this.cboLayoutDelete.BackColor = System.Drawing.SystemColors.Window;
			this.cboLayoutDelete.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboLayoutDelete.FormattingEnabled = true;
			this.cboLayoutDelete.Location = new System.Drawing.Point(20, 30);
			this.cboLayoutDelete.Name = "cboLayoutDelete";
			this.cboLayoutDelete.Size = new System.Drawing.Size(300, 40);
			this.cboLayoutDelete.TabIndex = 10;
			this.cboLayoutDelete.Visible = false;
			// 
			// fraFormat
			// 
			this.fraFormat.Controls.Add(this.cmbEditFormat);
			this.fraFormat.Controls.Add(this.lblEditFormat);
			this.fraFormat.Controls.Add(this.cboFormatDelete);
			this.fraFormat.Controls.Add(this.cboFormat);
			this.fraFormat.Location = new System.Drawing.Point(20, 90);
			this.fraFormat.Name = "fraFormat";
			this.fraFormat.Size = new System.Drawing.Size(340, 150);
			this.fraFormat.TabIndex = 4;
			this.fraFormat.Text = "Report Format";
			// 
			// cboFormat
			// 
			this.cboFormat.AutoSize = false;
			this.cboFormat.BackColor = System.Drawing.SystemColors.Window;
			this.cboFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboFormat.FormattingEnabled = true;
			this.cboFormat.Location = new System.Drawing.Point(20, 30);
			this.cboFormat.Name = "cboFormat";
			this.cboFormat.Size = new System.Drawing.Size(300, 40);
			this.cboFormat.TabIndex = 8;
			// 
			// cboFormatDelete
			// 
			this.cboFormatDelete.AutoSize = false;
			this.cboFormatDelete.BackColor = System.Drawing.SystemColors.Window;
			this.cboFormatDelete.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboFormatDelete.FormattingEnabled = true;
			this.cboFormatDelete.Location = new System.Drawing.Point(20, 30);
			this.cboFormatDelete.Name = "cboFormatDelete";
			this.cboFormatDelete.Size = new System.Drawing.Size(300, 40);
			this.cboFormatDelete.TabIndex = 5;
			this.cboFormatDelete.Visible = false;
			// 
			// cmdFileProcess
			// 
			this.cmdFileProcess.AppearanceKey = "acceptButton";
			this.cmdFileProcess.Location = new System.Drawing.Point(30, 320);
			this.cmdFileProcess.Name = "cmdFileProcess";
			this.cmdFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileProcess.Size = new System.Drawing.Size(110, 48);
			this.cmdFileProcess.TabIndex = 1;
			this.cmdFileProcess.Text = "Process";
			this.cmdFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
			// 
			// frmGetProjectDetail
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(834, 533);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmGetProjectDetail";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Project Detail Report Customization";
			this.Load += new System.EventHandler(this.frmGetProjectDetail_Load);
			this.Activated += new System.EventHandler(this.frmGetProjectDetail_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetProjectDetail_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEdit)).EndInit();
			this.fraEdit.ResumeLayout(false);
			this.fraEdit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLayout)).EndInit();
			this.fraLayout.ResumeLayout(false);
			this.fraLayout.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraFormat)).EndInit();
			this.fraFormat.ResumeLayout(false);
			this.fraFormat.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
