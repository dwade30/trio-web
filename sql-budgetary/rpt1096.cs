﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1096.
	/// </summary>
	public partial class rpt1096 : FCSectionReport
	{
		public static rpt1096 InstancePtr
		{
			get
			{
				return (rpt1096)Sys.GetInstance(typeof(rpt1096));
			}
		}

		protected rpt1096 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1096	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();

		public rpt1096()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "1099 Forms";
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			rsInfo.OpenRecordset("SELECT * FROM [1099Information]");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				rpt1096.InstancePtr.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strPhone;
			fldMunicipality.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Municipality")));
			fldAddress1.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address1")));
			fldAddress2.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address2")));
			if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip4"))) != "")
			{
				fldCityStateZip.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip"))) + "-" + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip4")));
			}
			else
			{
				fldCityStateZip.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip")));
			}
			fldContact.Text = rsInfo.Get_Fields_String("Contact");
			fldFederalID.Text = rsInfo.Get_Fields_String("FederalCode");
			fldEmail.Text = rsInfo.Get_Fields_String("Email");
			strPhone = FCConvert.ToString(rsInfo.Get_Fields_String("Phone")).Replace("(", "").Replace(")", "");
			if (Strings.InStr(1, strPhone, "000-0000", CompareConstants.vbBinaryCompare) == 0 && strPhone != "")
			{
				fldPhone.Text = Strings.Left(strPhone, 3) + "   " + Strings.Right(strPhone, strPhone.Length - 3);
			}
			else
			{
				fldPhone.Text = "";
			}
			strPhone = FCConvert.ToString(rsInfo.Get_Fields_String("Fax")).Replace("(", "").Replace(")", "");
			if (Strings.InStr(1, strPhone, "000-0000", CompareConstants.vbBinaryCompare) == 0 && strPhone != "")
			{
				fldFax.Text = Strings.Left(strPhone, 3) + "   " + Strings.Right(strPhone, strPhone.Length - 3);
			}
			else
			{
				fldFax.Text = "";
			}
			fldTotalAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmountReported"), "#,##0.00");
			fldNumberOfForms.Text = Strings.Format(rsInfo.Get_Fields_Int32("TotalNumberOfFormsPrinted"), "#,##0");
		}

		private void rpt1096_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt1096.Caption	= "1099 Forms";
			//rpt1096.Icon	= "rpt1096.dsx":0000";
			//rpt1096.Left	= 0;
			//rpt1096.Top	= 0;
			//rpt1096.Width	= 11880;
			//rpt1096.Height	= 8595;
			//rpt1096.StartUpPosition	= 3;
			//rpt1096.SectionData	= "rpt1096.dsx":058A;
			//End Unmaped Properties
		}
	}
}
