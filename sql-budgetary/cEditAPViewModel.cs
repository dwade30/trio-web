﻿using System;
using Global;
using fecherFoundation;
using SharedApplication.Extensions;

namespace TWBD0000
{
    public class cEditAPViewModel
    {
        public delegate void EditAPViewModelChangedEventHandler();

        public event EditAPViewModelChangedEventHandler BanksChanged;
        public event EditAPViewModelChangedEventHandler JournalChanged;
        public event EditAPViewModelChangedEventHandler JournalSaved;

        private cAPJournalMasterInfo apJournalMaster = new cAPJournalMasterInfo();
        private cAPJournalController apJournController = new cAPJournalController();
        private cJournalController journController = new cJournalController();
        private cBankController bankController = new cBankController();

        public Boolean BankIsVisible { get; set; } = false;
        public Boolean Cancelled { get; set; } = false;
        public Boolean PayableDateIsEditable { get; protected set; }
        public Boolean PeriodIsEditable { get; protected set; }
        public Boolean BankIsEditable { get; protected set; }

        public cAPJournalMasterInfo APJournalInfo
        {
            get { return apJournalMaster; }
        }

        public cEditAPViewModel()
        {
            LoadBanks();
        }
        public fecherFoundation.FCCollection Banks { get; private set; } = new FCCollection();
        public void AllowEditing()
        {
            PeriodIsEditable = true;
            PayableDateIsEditable = true;
            if (!BankIsVisible)
            {
                BankIsEditable = false;
            }
        }
        public void LoadAPJournalMaster(int journalNumber)
        {
            apJournalMaster = GetAPJournalMasterInfo(journalNumber);
            RaiseJournalChanged();
        }

        private cAPJournalMasterInfo GetAPJournalMasterInfo(int lngJournalNumber)
        {
            var apJournMast = new cAPJournalMasterInfo();
            PeriodIsEditable = true;
            BankIsEditable = true;
            PayableDateIsEditable = true;
            BankIsVisible = true;
            var bank = 0;
            if (lngJournalNumber > 0)
            {
                var masterJourn = journController.GetJournalByNumber(lngJournalNumber);
                if (journController.HadError)
                {
                    throw new Exception(journController.LastErrorMessage);
                }
                apJournMast.BankID = masterJourn.BankNumber;
                bank = Convert.ToInt32(modBudgetaryAccounting.GetBankVariable("APBank"));
                if ( bank > 0)
                {
                    BankIsEditable = false;
                }

                if (apJournMast.BankID == bank && bank > 0)
                {
                    BankIsVisible = false;
                }
                if (masterJourn.Status.ToLower() != "e" && masterJourn.Status.ToLower() != "v" && masterJourn.BankNumber > 0)
                {
                    BankIsEditable = false;
                }
                apJournMast.Description = masterJourn.Description;
                apJournMast.JournalMasterID = masterJourn.ID;
                apJournMast.JournalNumber = masterJourn.JournalNumber;
                apJournMast.Period = masterJourn.Period;
                if (masterJourn.Period > 0)
                {
                    PeriodIsEditable = false;
                }

                cAPJournal apJourn;
                var apJournals = apJournController.GetAPJournalsByJournalNumber(masterJourn.JournalNumber);
                if (apJournals != null)
                {
                    PeriodIsEditable = false;
                    apJournals.MoveFirst();
                    if (apJournals.ItemCount() > 0)
                    {
                        if (BankIsEditable && masterJourn.BankNumber > 0)
                        {
                            if (HasPrintedChecksYet(apJournals.Items))
                            {
                                BankIsEditable = false;
                            }
                        }
                        apJourn = (cAPJournal)apJournals.GetCurrentItem();
                        apJournMast.PayableDate = apJourn.PayableDate;
                        if (apJourn.PayableDate.IsDate())
                        {
                            PayableDateIsEditable = false;
                        }
                    }
                }
            }
            else
            {
                
                PeriodIsEditable = true;
                if (modGlobalConstants.Statics.gstrArchiveYear != "")
                {
                    if (modBudgetaryMaster.Statics.FirstMonth == 1)
                    {
                        apJournMast.Period = 12;
                    }
                    else
                    {
                        apJournMast.Period = modBudgetaryMaster.Statics.FirstMonth - 1;
                    }
                }
                else
                {
                    apJournMast.Period = DateTime.Now.Month;
                }
                apJournMast.PayableDate = DateTime.Now.ToString("MM/dd/yyyy");
                bank = Convert.ToInt32(modBudgetaryAccounting.GetBankVariable("APBank"));
                apJournMast.BankID = bank;
                if (bank > 0)
                {
                    BankIsEditable = false;
                    BankIsVisible = false;
                }
            }
            return apJournMast;
        }
        public Boolean AllValidated()
        {
            if (apJournalMaster.BankID < 1)
            {
                return false;
            }
            if (apJournalMaster.Period < 1)
            {
                return false;
            }
            if (String.IsNullOrWhiteSpace(apJournalMaster.Description))
            {
                return false;
            }
            if (!apJournalMaster.PayableDate.IsDate())
            {
                return false;
            }
            return true;
        }

        public void Save()
        {
            cJournal journ;
            journ = SetJournalFromAPJournalMasterInfo(ref apJournalMaster);
            journController.SaveJournal(ref journ);
            apJournalMaster.JournalMasterID = journ.ID;
            apJournalMaster.JournalNumber = journ.JournalNumber;
            apJournController.UpdateJournalPeriodAndPayable(ref apJournalMaster);
            RaiseJournalChanged();
            RaiseJournalSaved();
        }

        private cJournal SetJournalFromAPJournalMasterInfo(ref cAPJournalMasterInfo apJournalMasterInfo)
        {
            var journ = journController.GetJournalByNumber(apJournalMasterInfo.JournalNumber);
            if (journ == null)
            {
                var lngID = journController.CreateJournal(apJournalMasterInfo.Description, "AP", apJournalMasterInfo.Period);
                journ = journController.GetJournal(lngID);
            }
            FillJournalFromAPJournalMasterInfo(ref journ, apJournalMasterInfo);
            return journ;
        }

        private void FillJournalFromAPJournalMasterInfo(ref cJournal journ,cAPJournalMasterInfo apJournalMasterInfo)
        {
            journ.BankNumber = apJournalMasterInfo.BankID;
            journ.Description = apJournalMasterInfo.Description;
            journ.Period = Convert.ToInt16(apJournalMasterInfo.Period);
        }

        private void LoadBanks()
        {
            var bankCollection = bankController.GetBanks();
            Banks = bankCollection.Items;
            RaiseBanksChanged();
        }

        private void RaiseBanksChanged()
        {
            if (this.BanksChanged != null)
                this.BanksChanged();
        }

        private void RaiseJournalChanged()
        {
            if (this.JournalChanged != null)
            {
                this.JournalChanged();
            }
        }

        private void RaiseJournalSaved()
        {
            if (this.JournalSaved != null)
            {
                this.JournalSaved();
            }
        }

        private bool HasPrintedChecksYet(FCCollection apJournals)
        {
            foreach (cAPJournal journ in apJournals)
            {
                if (journ.PrintedIndividual && !String.IsNullOrWhiteSpace(journ.CheckNumber))
                {
                    return true;
                }                
            }
            return false;
        }
    }
}
