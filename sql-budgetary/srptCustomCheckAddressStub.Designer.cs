﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCustomCheckAddressStub.
	/// </summary>
	partial class srptCustomCheckAddressStub
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCustomCheckAddressStub));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldVendorName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.imgIcon = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.lblReturnName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldVendorName,
            this.fldAddress1,
            this.fldAddress3,
            this.fldAddress2,
            this.fldCheckNumber,
            this.imgIcon,
            this.lblReturnName,
            this.lblReturnAddress1,
            this.lblReturnAddress2,
            this.lblReturnAddress3,
            this.fldAddress4});
            this.Detail.Height = 3.5F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldVendorName
            // 
            this.fldVendorName.Height = 0.19F;
            this.fldVendorName.HyperLink = null;
            this.fldVendorName.Left = 2.25F;
            this.fldVendorName.Name = "fldVendorName";
            this.fldVendorName.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.fldVendorName.Tag = "text";
            this.fldVendorName.Text = "EMPLOYEE";
            this.fldVendorName.Top = 2.4375F;
            this.fldVendorName.Width = 4.34375F;
            // 
            // fldAddress1
            // 
            this.fldAddress1.Height = 0.19F;
            this.fldAddress1.HyperLink = null;
            this.fldAddress1.Left = 2.25F;
            this.fldAddress1.Name = "fldAddress1";
            this.fldAddress1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.fldAddress1.Tag = "TEXT";
            this.fldAddress1.Text = "TRIOVILLE";
            this.fldAddress1.Top = 2.625F;
            this.fldAddress1.Width = 4.375F;
            // 
            // fldAddress3
            // 
            this.fldAddress3.Height = 0.19F;
            this.fldAddress3.HyperLink = null;
            this.fldAddress3.Left = 2.25F;
            this.fldAddress3.Name = "fldAddress3";
            this.fldAddress3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.fldAddress3.Tag = "TEXT";
            this.fldAddress3.Text = "TRIOVILLE";
            this.fldAddress3.Top = 3F;
            this.fldAddress3.Width = 4.375F;
            // 
            // fldAddress2
            // 
            this.fldAddress2.Height = 0.19F;
            this.fldAddress2.HyperLink = null;
            this.fldAddress2.Left = 2.25F;
            this.fldAddress2.Name = "fldAddress2";
            this.fldAddress2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.fldAddress2.Tag = "TEXT";
            this.fldAddress2.Text = "TRIOVILLE";
            this.fldAddress2.Top = 2.8125F;
            this.fldAddress2.Width = 4.375F;
            // 
            // fldCheckNumber
            // 
            this.fldCheckNumber.Height = 0.28125F;
            this.fldCheckNumber.HyperLink = null;
            this.fldCheckNumber.Left = 4.75F;
            this.fldCheckNumber.Name = "fldCheckNumber";
            this.fldCheckNumber.Style = "font-size: 14.5pt; text-align: right";
            this.fldCheckNumber.Tag = "text";
            this.fldCheckNumber.Text = null;
            this.fldCheckNumber.Top = 1.8125F;
            this.fldCheckNumber.Width = 1.875F;
            // 
            // imgIcon
            // 
            this.imgIcon.Height = 0.96875F;
            this.imgIcon.HyperLink = null;
            this.imgIcon.ImageData = null;
            this.imgIcon.Left = 0.34375F;
            this.imgIcon.LineWeight = 1F;
            this.imgIcon.Name = "imgIcon";
            this.imgIcon.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.imgIcon.Top = 1F;
            this.imgIcon.Width = 0.96875F;
            // 
            // lblReturnName
            // 
            this.lblReturnName.Height = 0.19F;
            this.lblReturnName.Left = 1.375F;
            this.lblReturnName.Name = "lblReturnName";
            this.lblReturnName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.lblReturnName.Tag = "Large";
            this.lblReturnName.Text = "COUNTY OF HANCOCK";
            this.lblReturnName.Top = 1.09375F;
            this.lblReturnName.Width = 3.125F;
            // 
            // lblReturnAddress1
            // 
            this.lblReturnAddress1.Height = 0.19F;
            this.lblReturnAddress1.Left = 1.375F;
            this.lblReturnAddress1.Name = "lblReturnAddress1";
            this.lblReturnAddress1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.lblReturnAddress1.Tag = "Large";
            this.lblReturnAddress1.Text = "50 STATE STREET, SUITE #8";
            this.lblReturnAddress1.Top = 1.28125F;
            this.lblReturnAddress1.Width = 3.125F;
            // 
            // lblReturnAddress2
            // 
            this.lblReturnAddress2.Height = 0.19F;
            this.lblReturnAddress2.Left = 1.375F;
            this.lblReturnAddress2.Name = "lblReturnAddress2";
            this.lblReturnAddress2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.lblReturnAddress2.Tag = "Large";
            this.lblReturnAddress2.Text = "ELLSWORTH, ME 04605";
            this.lblReturnAddress2.Top = 1.46875F;
            this.lblReturnAddress2.Width = 3.125F;
            // 
            // lblReturnAddress3
            // 
            this.lblReturnAddress3.Height = 0.19F;
            this.lblReturnAddress3.Left = 1.375F;
            this.lblReturnAddress3.Name = "lblReturnAddress3";
            this.lblReturnAddress3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.lblReturnAddress3.Tag = "Large";
            this.lblReturnAddress3.Text = "(207) 667-8272";
            this.lblReturnAddress3.Top = 1.65625F;
            this.lblReturnAddress3.Width = 3.125F;
            // 
            // fldAddress4
            // 
            this.fldAddress4.Height = 0.19F;
            this.fldAddress4.HyperLink = null;
            this.fldAddress4.Left = 2.25F;
            this.fldAddress4.Name = "fldAddress4";
            this.fldAddress4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.fldAddress4.Tag = "TEXT";
            this.fldAddress4.Text = "TRIOVILLE";
            this.fldAddress4.Top = 3.1875F;
            this.fldAddress4.Width = 4.375F;
            // 
            // srptCustomCheckAddressStub
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.760417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label fldVendorName;
		private GrapeCity.ActiveReports.SectionReportModel.Label fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label fldCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgIcon;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label fldAddress4;
	}
}
