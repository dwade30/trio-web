﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Extensions;
using System;
using System.IO;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using TWSharedLibrary;
using DialogResult = Wisej.Web.DialogResult;
using Form = Wisej.Web.Form;
using Keys = Wisej.Web.Keys;
using MessageBox = Wisej.Web.MessageBox;
using MessageBoxButtons = Wisej.Web.MessageBoxButtons;
using MessageBoxDefaultButton = Wisej.Web.MessageBoxDefaultButton;
using MessageBoxIcon = Wisej.Web.MessageBoxIcon;

namespace TWBD0000
{
    public class cBDCommandHandler
    {
        //=========================================================
        public delegate void MenuChangedEventHandler(string strMenuName);

        public event MenuChangedEventHandler MenuChanged;

        #region Service Properties
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBDAccountService bdAccountService = new cBDAccountService();
        private cBDAccountService bdAccountService_AutoInitialized;

        private cBDAccountService bdAccountService
        {
            get
            {
                if (bdAccountService_AutoInitialized == null) bdAccountService_AutoInitialized = new cBDAccountService();

                return bdAccountService_AutoInitialized;
            }
            set
            {
                bdAccountService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBudgetService budgetService = new cBudgetService();
        private cBudgetService budgetService_AutoInitialized;

        private cBudgetService budgetService
        {
            get
            {
                if (budgetService_AutoInitialized == null) budgetService_AutoInitialized = new cBudgetService();

                return budgetService_AutoInitialized;
            }
            set
            {
                budgetService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cJournalService journService = new cJournalService();
        private cJournalService journService_AutoInitialized;

        private cJournalService journService
        {
            get
            {
                if (journService_AutoInitialized == null) journService_AutoInitialized = new cJournalService();

                return journService_AutoInitialized;
            }
            set
            {
                journService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private c1099Service ten99Service = new c1099Service();
        private c1099Service ten99Service_AutoInitialized;

        private c1099Service ten99Service
        {
            get
            {
                if (ten99Service_AutoInitialized == null) ten99Service_AutoInitialized = new c1099Service();

                return ten99Service_AutoInitialized;
            }
            set
            {
                ten99Service_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBankService bankService = new cBankService();
        private cBankService bankService_AutoInitialized;

        private cBankService bankService
        {
            get
            {
                if (bankService_AutoInitialized == null) bankService_AutoInitialized = new cBankService();

                return bankService_AutoInitialized;
            }
            set
            {
                bankService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cCheckRecService checkRecService = new cCheckRecService();
        private cCheckRecService checkRecService_AutoInitialized;

        private cCheckRecService checkRecService
        {
            get
            {
                if (checkRecService_AutoInitialized == null) checkRecService_AutoInitialized = new cCheckRecService();

                return checkRecService_AutoInitialized;
            }
            set
            {
                checkRecService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cOronoPayrollImportService oPayImportService = new cOronoPayrollImportService();
        private cOronoPayrollImportService oPayImportService_AutoInitialized;

        private cOronoPayrollImportService oPayImportService
        {
            get
            {
                if (oPayImportService_AutoInitialized == null) oPayImportService_AutoInitialized = new cOronoPayrollImportService();

                return oPayImportService_AutoInitialized;
            }
            set
            {
                oPayImportService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cCustomCheckRecService custCheckRecService = new cCustomCheckRecService();
        private cCustomCheckRecService custCheckRecService_AutoInitialized;

        private cCustomCheckRecService custCheckRecService
        {
            get
            {
                if (custCheckRecService_AutoInitialized == null) custCheckRecService_AutoInitialized = new cCustomCheckRecService();

                return custCheckRecService_AutoInitialized;
            }
            set
            {
                custCheckRecService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cCustomFunctionsService custFunctionService = new cCustomFunctionsService();
        private cCustomFunctionsService custFunctionService_AutoInitialized;

        private cCustomFunctionsService custFunctionService
        {
            get
            {
                if (custFunctionService_AutoInitialized == null) custFunctionService_AutoInitialized = new cCustomFunctionsService();

                return custFunctionService_AutoInitialized;
            }
            set
            {
                custFunctionService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBDSettings bdSettings = new cBDSettings();

        #endregion

        #region Misc Properties

        private cBDSettings bdSettings_AutoInitialized;

        private cBDSettings bdSettings
        {
            get
            {
                if (bdSettings_AutoInitialized == null) bdSettings_AutoInitialized = new cBDSettings();

                return bdSettings_AutoInitialized;
            }
            set
            {
                bdSettings_AutoInitialized = value;
            }
        }

        private cSettingsController setCont_AutoInitialized;

        private cSettingsController setCont
        {
            get
            {
                if (setCont_AutoInitialized == null) setCont_AutoInitialized = new cSettingsController();

                return setCont_AutoInitialized;
            }
            set
            {
                setCont_AutoInitialized = value;
            }
        }


        #endregion

        public void ExecuteMenuCommand(string strMenuName, int intCode)
        {
            var menuName = Strings.LCase(strMenuName);

            switch (menuName)
            {
                case "main":
                    ExecuteMainMenu(intCode);

                    break;
                case "payables":
	                ExecutePayablesMenu(intCode);

	                break;
                case "projects":
	                ExecuteProjectsMenu(intCode);

	                break;
                case "end of year":
	                ExecuteEndOfYearMenu(intCode);

	                break;
                case "accounts":
	                ExecuteAccountsMenu(intCode);

	                break;
                case "data":
                    ExecuteDataEntryMenu(intCode);

                    break;
                case "vendor":
                    ExecuteVendorMenu(intCode);

                    break;
                case "reports":
                    ExecuteReportsMenu(intCode);

                    break;
                case "file":
                    ExecuteFileMenu(intCode);

                    break;
                case "budget":
                    ExecuteBudgetMenu(intCode);

                    break;
                case "checkrec":
                    ExecuteCheckRecMenu(intCode);

                    break;
                case "summary":
                    ExecuteSummaryMenu(intCode);

                    break;
                case "account":
                    ExecuteAccountMenu(intCode);

                    break;
                case "formats":
                    ExecuteFormatsMenu(intCode);

                    break;
                case "detail":
                    ExecuteDetailMenu(intCode);

                    break;
                case "1099":
                    Execute1099Menu(intCode);

                    break;
                case "ap":
                    ExecuteAPMenu(intCode);

                    break;
                case "checkfile":
                    ExecuteCheckFileMenu(intCode);

                    break;
                case "encumbrancereports":
                    ExecuteEncumbranceReportsMenu(intCode);

                    break;
                case "committeerequest":
                    ExecuteCommitteeRequestMenu(intCode);

                    break;
                case "initialrequest":
                    ExecuteInitialRequestMenu(intCode);

                    break;
                case "managerrequest":
                    ExecuteManagerRequestMenu(intCode);

                    break;
                case "electedrequest":
                    ExecuteElectedRequestMenu(intCode);

                    break;
                case "eoyreports":
                    ExecuteEOYReportsMenu(intCode);

                    break;
                case "approvedamount":
                    ExecuteApprovedAmountMenu(intCode);

                    break;
                case "purge":
                    ExecutePurgeMenu(intCode);

                    break;
                case "custom":
                    ExecuteCustomMenu(intCode);

                    break;
            }
        }

        #region SubMenu Commands
        private void ExecuteCustomMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_OrringtonCustomCheckRecAP";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_OrringtonCustomCheckRecPY";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_SetupTownRevenueReport";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_ClintonCheckRecExport";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Form_CaribouCheckRecExport";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Form_HancockCountyPayrollImport";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_CalaisBDSetDBLocation";
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_LincolnCountyPayrollImport";
                        break;
                    }
                case 9:
                    {
                        strCommand = "BD_DaytonCustomImport";
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_Form_BoothbayCheckRecExport";
                        break;
                    }
                case 11:
                    {
                        strCommand = "BD_OronoPayrollImport";
                        break;
                    }
                case 12:
                    {
                        strCommand = "BD_SomersetCountyCustomCheckRecAP";
                        break;
                    }
                case 13:
                    {
                        strCommand = "BD_Form_DoverFoxcroftSimpleCheckRecExport";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteMainMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                {
                    strCommand = "BD_Form_Journals";
                    break;
                }
                case 2:
                {
	                strCommand = "BD_Form_AccountInquiry";
	                break;
                }
                case 3:
                {
                    strCommand = "BD_Menu_Payables";
                    break;
                }
                case 4:
                {
	                strCommand = "BD_Menu_CheckRec";
	                if (modGlobalConstants.Statics.gstrArchiveYear != "")
	                {
		                if (MessageBox.Show("You are currently working in an archive year. Check Reconciliation must always be performed in the current year. Would you like to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
			                App.MainForm.NavigationMenu.CurrentItem.AllowExpand = false;
		                else
			                App.MainForm.NavigationMenu.CurrentItem.AllowExpand = true;
	                }
	                break;
                }
                case 5:
                {
                    strCommand = "BD_Menu_Accounts";
                    break;
                }
                case 6:
                {
	                strCommand = "BD_Menu_Budget";
	                break;
                }
                case 7:
                {
	                strCommand = "BD_Menu_EOY";
	                break;
                }
                case 8:
                {
	                strCommand = "BD_Menu_Projects";
	                break;
                }
                case 9:
                {
                    strCommand = "BD_Menu_Reports";
                    break;
                }
                case 10:
                {
	                strCommand = "BD_Menu_File";
	                break;
                }


                
                
                
               
                //case 10:
                //    {
                //        strCommand = "BD_Form_DeleteJournal";
                //        break;
                //    }
               
                
                
                
                //case 16:
                //    {
                //        strCommand = "BD_Exit";
                //        break;
                //    }
                
                //case 18:
                //    {
                //        strCommand = "BD_Import_Custom";
                //        break;
                //    }
            }

            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteCheckRecMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = CheckForCurrentBank("BD_Form_CheckRecBalanceScreen");
                        break;
                    }
                case 2:
                    {
                        strCommand = CheckForCurrentBank("BD_Form_AddCheckDep");
                        break;
                    }
                case 3:
                    {
                        strCommand = CheckForCurrentBank("BD_Form_ChangeStatementDate");
                        break;
                    }
                case 4:
                    {
                        strCommand = CheckForCurrentBank("BD_Form_FlagCashed");
                        break;
                    }
                case 5:
                    {
                        strCommand = CheckForCurrentBank("BD_Menu_CheckFile");
                        break;
                    }
                case 6:
                    {
                        strCommand = CheckForCurrentBank("BD_PurgeChecks");
                        break;
                    }
                case 7:
                    {
                        strCommand = CheckForCurrentBank("BD_Form_ChangeCheckStatus");
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_Form_ChangeBank";
                        break;
                    }
                case 9:
                    {
                        strCommand = CheckForCurrentBank("BD_Form_ArchiveViewSelection");
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_Form_UpdateDeposit";
                        break;
                    }
                case 11:
                    {
                        strCommand = "BD_Form_PurgeSelected";
                        break;
                    }
                case 12:
                    {
                        strCommand = "BD_Form_SetupAuditReport";
                        break;
                    }
                case 13:
                    {
                        strCommand = "BD_PositivePayExport";
                        break;
                    }
                case 14:
                    {
                        App.MainForm.StatusBarText3 = "";
                        //MDIParent.InstancePtr.StatusBar1.Panels[3 - 1].Bevel = sbrNoBevel;
                        strCommand = "BD_Menu_Main";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecutePayablesMenu(int intCode)
        {
	        var strCommand = "";
	        switch (intCode)
	        {
		        case 1:
		        {
			        strCommand = "BD_Menu_AP";
			        break;
		        }
		        case 2:
		        {
			        strCommand = "BD_Menu_Vendor";
			        break;
		        }
		        case 3:
		        {
			        strCommand = "BD_Form_GetGJCorrDataEntry";
			        break;
		        }
		        case 4:
		        {
			        strCommand = "BD_Form_GetEncDataEntry";
			        break;
		        }
		        case 5:
		        {
			        strCommand = "BD_Form_EncumbranceUpdate";
			        break;
		        }
		        case 6:
		        {
			        strCommand = "BD_Form_GetCreditMemoDataEntry";
			        break;
		        }
		        case 7:
		        {
			        strCommand = "BD_Form_CreditMemoCorrections";
			        break;
		        }
		        case 8:
		        {
			        strCommand = "BD_Form_GetPurchaseOrders";
			        break;
		        }
                case 9:
		        {
			        strCommand = "BD_Form_QueryJournal";
			        break;
		        }
		        case 10:
		        {
			        strCommand = "BD_Form_SetupAPResearch";
			        break;
		        }
            }
	        if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteCheckFileMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_CheckFileBalanceScreen";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Report_CashedItemsOnly";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Report_CheckFileList";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_SetupAutoEntryReport";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Form_CheckListSelection";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Report_rptBalancingReport";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_Menu_CheckRec";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void Execute1099Menu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                {
                    strCommand = "BD_Form_SelectYear";
                    break;
                }
                case 2:
                {
                    strCommand = "BD_Report_1099EditReport";
                    break;
                }
                case 3:
                {
                    strCommand = "BD_Print1099s";
                    break;
                }
                case 4:
                {
                    strCommand = "BD_Print1096";
                    break;
                }
                case 5:
                {
                    strCommand = "BD_CreateElectronic1099File";
                    break;
                }
                case 6:
                {
                    strCommand = "BD_Archive1099ExtractFile";
                    break;
                }
                case 7:
                {
                    strCommand = "BD_Form_1099Restore";
                    break;
                }
                case 8:
                {
	                strCommand = "BD_Clear1099Adjustments";
	                break;
                }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteSummaryMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_ExpenseSummarySelect";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_RevenueSummarySelect";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_LedgerSummarySelect";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_ExpRevSummary";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Form_ExpenseDetailVendorSummarySelect";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Form_JournalAccountSummary";
                        break;
                    }
                
                case 13:
                    {
                        strCommand = "BD_Menu_Reports";
                        break;
                    }
                case 14:
                    {
                        strCommand = "BD_Form_TrialBalance";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteReportsMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Menu_Summary";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Menu_Detail";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_CurrentAccountStatus";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_ChartOfAccounts";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Form_BudgetAdjustments";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Form_VendorDetail";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_Form_MultipleJournalSelectionP";
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_Form_MultipleJournalSelectionU";
                        break;
                    }
                case 9:
                    {
                        strCommand = "BD_Form_CashChartSelect";
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_Menu_EncumbranceReports";
                        break;
                    }
                case 11:
                    {
                        strCommand = "BD_Form_JournalListing";
                        break;
                    }
                case 12:
                    {
                        strCommand = "BD_Form_CustomReportSelection";
                        break;
                    }
                case 13:
                    {
                        strCommand = "BD_Menu_Formats";
                        break;
                    }
                case 14:
                    {
                        strCommand = "BD_Report_ValidAccounts";
                        break;
                    }
                case 15:
                    {
                        strCommand = "BD_Form_AccountResearch";
                        break;
                    }
                case 16:
                    {
                        strCommand = "BD_Report_OutstandingCreditMemos";
                        break;
                    }
                case 19:
                    {
                        strCommand = "BD_Menu_Main";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteDataEntryMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                
                case 2:
                    {
                        strCommand = "BD_Form_GetCDDataEntry";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_GetCRDataEntry";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_GetGJDataEntry";
                        break;
                    }
                
                case 10:
                    {
                        strCommand = "BD_Form_AutomaticJournals";
                        break;
                    }
                case 11:
                    {
                        strCommand = "BD_Form_CompleteJournals";
                        break;
                    }
                
                case 13:
                    {
                        strCommand = "BD_Form_ReversingJournal";
                        break;
                    }
                case 14:
                    {
                        strCommand = "BD_Form_EditJournalDescriptions";
                        break;
                    }
                
                case 16:
                    {
                        strCommand = "BD_Menu_Main";
                        break;
                    }
                case 17:
                    {
                        strCommand = "BD_Form_Journals";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteVendorMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_GetVendor";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_VendorListSetup";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_ClassCodes";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_VendorLabels";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Form_VendorExtract";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_Report_PurgedVendors";
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_Report_VendorDefaultAccountInfo";
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_Menu_Main";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteFileMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_Customization";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_CDBS";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_BankNames";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_ForceCalculate";
                        break;
                    }
            }
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteDetailMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_ExpenseDetailSelect";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_RevenueDetailSelect";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_LedgerDetailSelect";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_JournalAccountReportsDetail";
                        break;
                    }
                
                case 9:
                    {
                        strCommand = "BD_Form_ExpObjDetailSetup";
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_Menu_Reports";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteCommitteeRequestMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_PrintCommitteeRequestWorksheets";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_EditCommitteeRequests";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_TransferCommittee";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Menu_Budget";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteInitialRequestMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_PrintInitialRequestWorksheets";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_EditInitialRequests";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_TransferInitial";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Menu_Budget";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteManagerRequestMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_PrintManagerRequestWorksheets";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_EditManagerRequests";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_TransferManager";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Menu_Budget";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteApprovedAmountMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_PrintApprovedAmountsWorksheets";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_EditApprovedAmounts";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Menu_Budget";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteEndOfYearMenu(int intCode)
        {
	        var strCommand = "";
	        switch (intCode)
	        {
		        case 1:
		        {
			        strCommand = "BD_Form_EOYProcessing";
			        break;
		        }
		        case 2:
		        {
			        strCommand = "BD_Menu_EOYReports";
			        break;
		        }
		        case 3:
		        {
			        strCommand = "BD_CreateOpeningAdjustments";
			        break;
		        }
                case 4:
                {
                    strCommand = "BD_Menu_1099";
                    break;
                }
		        case 5:
		        {
			        strCommand = "BD_Form_TaxTitles";
			        break;
		        }
            }
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteProjectsMenu(int intCode)
        {
	        var strCommand = "";
	        switch (intCode)
	        {
		        case 1:
		        {
			        strCommand = "BD_Form_SetupProjects";
			        break;
		        }
		        case 2:
		        {
			        strCommand = "BD_Form_ProjectSummarySelect";
			        break;
		        }
		        case 3:
		        {
			        strCommand = "BD_Form_ProjectDetailSelect";
			        break;
		        }
            }
	        if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteAccountsMenu(int intCode)
        {
	        var strCommand = "";
	        switch (intCode)
	        {
                case 1:
                {
                    strCommand = "BD_Menu_AccountSetup";
                    break;
                }
                case 2:
                {
	                strCommand = "BD_Form_ValidAccounts";
	                break;
                }
                case 3:
                {
	                strCommand = "BD_Form_LedgerControl";
	                break;
                }
                case 4:
                {
	                strCommand = "BD_Form_LedgerRanges";
	                break;
                }
            }
	        if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecutePurgeMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_PurgeChart";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_PurgeGraphItem";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Menu_File";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteEOYReportsMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Report_ClosingJournal";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Report_AccountCloseout";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Menu_Reports";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteElectedRequestMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_PrintElectedRequestWorksheets";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_EditElectedRequests";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_TransferElectedRequestToApproved";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Menu_Budget";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteEncumbranceReportsMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Report_OutstandingEncumbrances";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Report_LiquidatedEncumbrances";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_PrintPurchaseOrder";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_PrintPurchaseOrderSummary";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Menu_Reports";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteFormatsMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_GetExpenseSummary";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_GetRevenueSummary";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_GetLedgerSummary";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_GetProjectSummary";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Form_GetExpenseDetail";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Form_GetRevenueDetail";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_Form_GetLedgerDetail";
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_Form_GetProjectDetail";
                        break;
                    }
                case 9:
                    {
                        strCommand = "BD_Form_ReportSetup";
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_Menu_File";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteAPMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_PrintIndividualChecks";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_WarrantPreview";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_PrintChecks";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_CreateACHFiles";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Form_CheckRegister";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Form_Warrant";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_Form_WarrantRecap";
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_Menu_Main";
                        break;
                    }
                case 9:
                    {
                        strCommand = "BD_UpdateJournalInformation";
                        break;
                    }
                case 10:
	                {
		                strCommand = "BD_Form_GetAPDataEntry";
		                break;
	                }
            }
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteBudgetMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_ClearBudgetAmounts";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Menu_InitialRequest";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Menu_ManagerRequest";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Menu_CommitteeRequest";
                        break;
                    }
                case 5:
                    {
                        strCommand = "BD_Menu_ElectedRequest";
                        break;
                    }
                case 6:
                    {
                        strCommand = "BD_Menu_ApprovedAmount";
                        break;
                    }
                case 7:
                    {
                        strCommand = "BD_Form_CustomBudgetReport";
                        break;
                    }
                case 8:
                    {
                        strCommand = "BD_Form_BudgetComments";
                        break;
                    }
                case 9:
                    {
                        strCommand = "BD_Form_BudgetBreakdown";
                        break;
                    }
                case 10:
                    {
                        strCommand = "BD_TransferBudget";
                        break;
                    }
                case 11:
                    {
                        strCommand = "BD_CreateBudgetExtract";
                        break;
                    }
                case 12:
                    {
                        strCommand = "BD_Form_BudgetImport";
                        break;
                    }
                case 13:
                    {
                        strCommand = "BD_UpdateBudgetTable";
                        break;
                    }
                case 14:
                    {
                        strCommand = "BD_Form_UpdatePreviousSelectYear";
                        break;
                    }
                case 15:
                    {
                        strCommand = "BD_Menu_Main";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        private void ExecuteAccountMenu(int intCode)
        {
            var strCommand = "";
            switch (intCode)
            {
                case 1:
                    {
                        strCommand = "BD_Form_OutlineDeptQuery";
                        break;
                    }
                case 2:
                    {
                        strCommand = "BD_Form_OutlineExpQuery";
                        break;
                    }
                case 3:
                    {
                        strCommand = "BD_Form_OutlineRevQuery";
                        break;
                    }
                case 4:
                    {
                        strCommand = "BD_Form_OutlineLedgerQuery";
                        break;
                    }
                case 12:
                    {
                        strCommand = "BD_Menu_Main";
                        break;
                    }
            }
            //end switch
            if (strCommand != "") ExecuteCommand(strCommand);
        }

        #endregion

        public void ExecuteCommand(string strCommand)
        {
            var strTemp = "";
            DialogResult answer;
            var rsTemp = new clsDRWrapper();
            var ofd = new FCCommonDialog();

            var commandString = Strings.LCase(strCommand);

            try
            {
                switch (commandString)
                {
                    case "bd_import_custom":
                        {
                            if (MenuChanged != null)
                                MenuChanged("Custom");

                            break;
                        }

                    case "bd_form_journals":
                        frmJournals.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_getvendor":
                        frmGetAccount.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_main":
                        {
                            // MDIParent.SetMenu ("Main")
                            if (MenuChanged != null)
                                MenuChanged("Main");

                            break;
                        }
                    case "bd_menu_projects":
                    {
	                    if (MenuChanged != null)
		                    MenuChanged("Projects");

	                    break;
                    }
                    case "bd_menu_accounts":
                    {
	                    if (MenuChanged != null)
		                    MenuChanged("Accounts");

	                    break;
                    }
                    case "bd_menu_eoy":
                    {
	                    if (MenuChanged != null)
		                    MenuChanged("EOY");

	                    break;
                    }
                    case "bd_menu_payables":
                    {
	                    if (MenuChanged != null)
		                    MenuChanged("Payables");

	                    break;
                    }
                    case "bd_menu_data":
                        {
                            // MDIParent.SetMenu ("Data")
                            if (MenuChanged != null)
                                MenuChanged("Data");

                            break;
                        }

                    case "bd_menu_ap":
                        {
                            // MDIParent.SetMenu ("AP")
                            if (MenuChanged != null)
                                MenuChanged("AP");

                            break;
                        }

                    case "bd_form_posting":
                        {
                            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "CALAIS" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "NORWAY")
                            {
                                strTemp = setCont.GetSettingValue("WATERDBLOCATION", "", "", "", "");
                                if (Strings.Trim(strTemp) == "")
                                {
                                    MessageBox.Show("You must enter a water database before you may continue.", "Enter Database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                else
                                {
                                    if (!rsTemp.TableExists(strTemp + ".dbo.JournalEntries", "Budgetary"))
                                    {
                                        MessageBox.Show("Water database can't be found.  You must enter a valid database before you may proceed.", "Invalid Database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                            }
                            frmPosting.InstancePtr.Show(App.MainForm);

                            break;
                        }

                    case "bd_menu_reports":
                        {
                            // MDIParent.SetMenu ("Reports")
                            if (MenuChanged != null)
                                MenuChanged("Reports");

                            break;
                        }

                    case "bd_menu_vendor":
                        {
                            // MDIParent.SetMenu ("Vendor")
                            if (MenuChanged != null)
                                MenuChanged("Vendor");

                            break;
                        }

                    case "bd_menu_budget":
                        {
                            // MDIParent.SetMenu ("Budget")
                            if (MenuChanged != null)
                                MenuChanged("Budget");

                            break;
                        }

                    case "bd_menu_accountsetup":
                        {
                            //FC:FINAL:SBE - #843 - show hidden screen when Ctrl key is pressed
                            if ((Form.ModifierKeys & Keys.Control) == Keys.Control) frmPassword.InstancePtr.Show(App.MainForm);

                            // MDIParent.SetMenu ("Account")
                            if (MenuChanged != null)
                                MenuChanged("Account");

                            break;
                        }

                    case "bd_menu_formats":
                        {
                            // MDIParent.SetMenu ("Formats")
                            if (MenuChanged != null)
                                MenuChanged("Formats");

                            break;
                        }

                    case "bd_form_eoyprocessing":
                        frmEOYProcessing.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_deletejournal":
                        frmDeleteJournal.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_checkrec":
                        {
                            // MDIParent.SetMenu ("CheckRec")
                            if (MenuChanged != null)
                                MenuChanged("CheckRec");

                            break;
                        }

                    case "bd_form_queryjournal":
                        frmQueryJournal.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_setupapresearch":
                        frmSetupAPResearch.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_setupprojects":
                        frmSetupProjects.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_file":
                        {
                            // MDIParent.SetMenu ("File")
                            if (MenuChanged != null)
                                MenuChanged("File");

                            break;
                        }

                    case "bd_exit":
                        ExitApplication();

                        break;
                    case "bd_report_outstandingcreditmemos":
                        frmReportViewer.InstancePtr.Init(rptOutstandingCreditMemos.InstancePtr);

                        break;

                    case "bd_menu_eoyreports":
                        {
                            // MDIParent.SetMenu ("EOYReports")
                            if (MenuChanged != null)
                                MenuChanged("EOYReports");

                            break;
                        }

                    case "bd_form_getpurchaseorders":
                        frmGetPurchaseOrders.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_accountresearch":
                        frmAccountResearch.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_forcecalculate":
                        journService.ForceCalculate();

                        break;
                    case "bd_form_updatepreviousselectyear":
                        frmUpdatePreviousSelectYear.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_report_validaccounts":
                        frmReportViewer.InstancePtr.Init(rptValidAccounts.InstancePtr);

                        break;
                    case "bd_form_editjournaldescriptions":
                        frmEditJournalDescriptions.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_revertjournal":
                        {
                            object lngCode = 0;
                            if (frmInput.InstancePtr.Init(ref lngCode, "BD One Day Code", "Call TRIO for today's code", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, string.Empty, true))
                            {
                                if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngCode), "BD"))
                                {
                                    frmRevertJournal.InstancePtr.Show(App.MainForm);
                                }
                                else
                                {
                                    MessageBox.Show("Invalid Code", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }

                            break;
                        }

                    case "bd_menu_purge":
                        {
                            // MDIParent.SetMenu ("Purge")
                            if (MenuChanged != null)
                                MenuChanged("Purge");

                            break;
                        }

                    case "bd_form_reversingjournal":
                        frmReversingJournal.InstancePtr.Show(App.MainForm);
                        // Case "bd_menu_article"
                        // MDIParent.SetMenu ("Article")
                        break;
                    case "bd_updatebudgettable":
                        budgetService.UpdateBudgetTable();

                        break;
                    case "bd_form_budgetimport":
                        frmBudgetImport.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_setupauditreport":
                        frmSetupAuditReport.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_customreportselection":
                        frmCustomReportSelection.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_ledgercontrol":
                        frmLedgerControl.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_createopeningadjustments":
                        journService.CreateOpeningAdjustments();

                        break;
                    case "bd_form_completejournals":
                        frmCompleteJournals.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getgjdefault":
                        frmGetGJDefault.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_journallisting":
                        frmJournalListing.InstancePtr.Show(App.MainForm);
                        modCustomReport.SetFormFieldCaptions(frmJournalListing.InstancePtr, "JournalList");
                        //FC:FINAL:AM: moved code from Activate
                        frmJournalListing.InstancePtr.fraFields_DblClick();
                        frmJournalListing.InstancePtr.vsWhere.TextMatrix(0, 1, "A");
                        frmJournalListing.InstancePtr.vsWhere.TextMatrix(2, 1, "Y");

                        break;
                    case "bd_form_purgeselected":
                        frmPurgeSelected.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_chooseimage":
                        frmChooseImage.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_encumbrancereports":
                        {
                            // MDIParent.SetMenu ("EncumbranceReports")
                            if (MenuChanged != null)
                                MenuChanged("EncumbranceReports");

                            break;
                        }

                    case "bd_form_updatedeposit":
                        frmUpdateDeposit.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_automaticjournals":
                        BuildAutomaticJournals();

                        break;

                    case "bd_menu_1099":
                        {
                            // MDIParent.SetMenu ("1099")
                            if (MenuChanged != null)
                                MenuChanged("1099");

                            break;
                        }

                    case "bd_form_expobjdetailsetup":
                        frmExpObjDetailSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_budgetbreakdown":
                        frmBudgetBreakdown.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_reportsetup":
                        frmReportSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_report_vendordefaultaccountinfo":
                        frmReportViewer.InstancePtr.Init(rptVendorDefaultAccountInfo.InstancePtr);

                        break;
                    case "bd_form_cashchartselect":
                        frmCashChartSelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_creditmemocorrections":
                        frmCreditMemoCorrections.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_changebank":
                        frmChangeBank.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_archiveviewselection" when IsCurrentBank():
                        frmArchiveViewSelection.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_archiveviewselection":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;
                    case "bd_form_revenueranges":
                        frmRevenueRanges.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getcreditmemodataentry":
                        frmGetCreditMemoDataEntry.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_form_multiplejournalselectionu":
                        {
                            // frmMultipleJournalSelection.strReportType = "U"
                            // frmMultipleJournalSelection.Show , MDIParent
                            var journUnposted = new frmMultipleJournalSelection();
                            journUnposted.strReportType = "U";
                            journUnposted.Show(App.MainForm);

                            break;
                        }

                    case "bd_form_multiplejournalselectionp":
                        {
                            var journPosted = new frmMultipleJournalSelection();
                            // frmMultipleJournalSelection.strReportType = "P"
                            // frmMultipleJournalSelection.Show , MDIParent
                            journPosted.strReportType = "P";
                            journPosted.Show(App.MainForm);

                            break;
                        }

                    case "bd_form_getprojectdetail":
                        frmGetProjectDetail.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_budgetcomments":
                        frmBudgetComments.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_report_purgedvendors":
                        {
                            answer = MessageBox.Show("This process will purge any vendors in the system set to a status of deleted that have not been paid in this or last fiscal year.  Once this is done you will not be able to get any previous year information on these vendors.  Are you sure you wish to do this?", "Purge Vendors?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                            if (answer == DialogResult.Yes)
                            {
                                rsTemp.OpenRecordset("SELECT * FROM VendorMaster WHERE Status = 'D' AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM APJournal) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM Encumbrances) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM APJournalArchive) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM JournalEntries) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM CDJournalArchive) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM CreditMemo)");
                                if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                                    frmReportViewer.InstancePtr.Init(rptPurgedVendors.InstancePtr);
                                else
                                    MessageBox.Show("No Vendors found to purge.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            break;
                        }

                    case "bd_form_projectsummaryselect":
                        frmProjectSummarySelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_1099restore":
                        frm1099Restore.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_changecheckstatus" when IsCurrentBank():
                        frmChangeCheckStatus.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_changecheckstatus":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;
                    case "bd_form_getledgerdetail":
                        frmGetLedgerDetail.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_warrantrecap":
                        frmWarrantRecap.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_encumbranceupdate":
                        frmEncumbranceUpdate.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_validaccounts":

                        //! Load frmWait; // shwo the wait form
                        frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
                        frmWait.InstancePtr.Refresh();
                        frmWait.InstancePtr.Show();
                        frmValidAccounts.InstancePtr.Show(App.MainForm);
                        frmWait.InstancePtr.Refresh();
                        //Application.DoEvents();
                        break;
                    case "bd_form_custombudgetreport":
                        ShowCustomBudgetReport();

                        break;
                    case "bd_form_warrantmessage":
                        frmWarrantMessage.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getencdataentry":
                        frmGetEncDataEntry.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_vendorextract":
                        frmVendorExtract.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getrevenuedetail":
                        frmGetRevenueDetail.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_approvedamount":
                        {
                            // MDIParent.SetMenu ("ApprovedAmount")
                            if (MenuChanged != null)
                                MenuChanged("ApprovedAmount");

                            break;
                        }

                    case "bd_form_warrant":
                        frmWarrant.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_report_rptbalancingreport":
                        frmReportViewer.InstancePtr.Init(rptBalancingReport.InstancePtr);

                        break;
                    case "bd_form_journalaccountsummary":
                        frmJournalAccountReports.InstancePtr.blnDetail = false;
                        frmJournalAccountReports.InstancePtr.Show(App.MainForm);
                        modCustomReport.SetFormFieldCaptions(frmJournalAccountReports.InstancePtr, "JournalAccountSummary");
                        //FC:FINAL:AM:#i659 - moved code from activate
                        frmJournalAccountReports.InstancePtr.fraFields_DblClick();
                        frmJournalAccountReports.InstancePtr.vsWhere.TextMatrix(0, 1, "A");

                        break;
                    case "bd_form_vendordetail" when modBudgetaryMaster.JobComplete_2("frmVendorDetail"):
                        frmVendorDetail.InstancePtr.Show(App.MainForm);
                        modCustomReport.SetFormFieldCaptions(frmVendorDetail.InstancePtr, "VendorDetail");
                        frmVendorDetail.InstancePtr.vsWhere.TextMatrix(0, 1, "1");
                        frmVendorDetail.InstancePtr.vsWhere.TextMatrix(2, 1, "A");
                        frmVendorDetail.InstancePtr.vsWhere.TextMatrix(4, 1, "N");

                        break;
                    case "bd_form_vendordetail":
                        frmVendorDetail.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_archive1099extractfile":
                        ten99Service.ArchiveExtractFile();

                        break;
                    case "bd_form_banknames":
                        frmBankNames.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getgjcorrdataentry":
                        frmGetGJCorrDataEntry.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getexpensedetail":
                        frmGetExpenseDetail.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_electedrequest":
                        {
                            // MDIParent.SetMenu ("ElectedRequest")
                            if (MenuChanged != null)
                                MenuChanged("ElectedRequest");

                            break;
                        }

                    case "bd_form_checkregister":
                        frmCheckRegister.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_checklistselection":
                        frmCheckListSelection.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_projectdetailselect":
                        frmProjectDetailSelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_expensedetailvendorsummaryselect":
                        frmExpenseDetailVendorSummarySelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_createelectronic1099file":
                        frm1099Setup.InstancePtr.OptionType = "F";
                        frm1099Setup.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_clear1099adjustments":
                        {
                            answer = MessageBox.Show("Doing this will delete all your 1099 adjustment information.  Are you sure you wish to do this?", "Clear 1099 Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (answer == DialogResult.Yes)
                            {
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                                rsTemp.Execute("DELETE FROM Adjustments", "Budgetary");
                                FCGlobal.Screen.MousePointer = 0;
                                MessageBox.Show("1099 Adjustment Data Cleared Successfully");
                            }

                            break;
                        }

                    case "bd_form_budgetadjustments" when modBudgetaryMaster.JobComplete_2("frmBudgetAdjustments"):
                        frmBudgetAdjustments.InstancePtr.Show(App.MainForm);
                        modCustomReport.SetFormFieldCaptions(frmBudgetAdjustments.InstancePtr, "BudgetAdjustments");
                        frmBudgetAdjustments.InstancePtr.vsWhere.TextMatrix(0, 1, "A");
                        frmBudgetAdjustments.InstancePtr.vsWhere.TextMatrix(1, 1, "A");
                        frmBudgetAdjustments.InstancePtr.vsWhere.TextMatrix(2, 1, "");

                        break;
                    case "bd_form_budgetadjustments":
                        frmBudgetAdjustments.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_taxtitles":
                        frmTaxTitles.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getgjdataentry":
                        frmGetGJDataEntry.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_chartofaccounts":
                        frmChartOfAccounts.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_printpurchaseordersummary":
                        frmPrintPurchaseOrderSummary.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_vendorlabels":
                        frmVendorLabels.InstancePtr.Init();

                        break;
                    case "bd_form_outlineledgerquery":
                        frmOutlineLedgerQuery.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_committeerequest":
                        {
                            // MDIParent.SetMenu ("CommitteeRequest")
                            if (MenuChanged != null)
                                MenuChanged("CommitteeRequest");

                            break;
                        }

                    case "bd_form_getprojectsummary":
                        frmGetProjectSummary.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_createachfiles":
                        frmCreateACHFiles.InstancePtr.Init();

                        break;
                    case "bd_form_setupautoentryreport":
                        frmSetupAutoEntryReport.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_flagcashed" when IsCurrentBank():
                        frmFlagCashed.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_flagcashed":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;
                    case "bd_form_exprevsummary" when modGlobal.FormExist(frmExpRevSummary.InstancePtr):
                        frmExpRevSummary.InstancePtr.Show(App.MainForm);
                        return;
                    case "bd_form_exprevsummary":
                        frmExpRevSummary.InstancePtr.Show(App.MainForm);
                        modCustomReport.SetFormFieldCaptions(frmExpRevSummary.InstancePtr, "ExpRevSum");
                        // FC:FINAL:VGE - #693 Disabling ChangeEdit event during initialization. 
                        frmExpRevSummary.InstancePtr.cDBInit = true;
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(0, 1, "A");
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(2, 1, "Ob");
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(3, 1, "A");
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(5, 1, "Y");
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(6, 1, "N");
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(7, 1, "N");
                        frmExpRevSummary.InstancePtr.vsWhere.TextMatrix(8, 1, "De");
                        // FC:FINAL:VGE - #693 Disabling ChangeEdit event during initialization. 
                        frmExpRevSummary.InstancePtr.cDBInit = false;

                        break;
                    case "bd_form_journalaccountreportsdetail":
                        frmJournalAccountReports.InstancePtr.blnDetail = true;
                        frmJournalAccountReports.InstancePtr.Show(App.MainForm);
                        modCustomReport.SetFormFieldCaptions(frmJournalAccountReports.InstancePtr, "JournalAccountDetail");
                        //FC:FINAL:AM: moved code from activate
                        frmJournalAccountReports.InstancePtr.fraFields_DblClick();
                        frmJournalAccountReports.InstancePtr.vsWhere.TextMatrix(0, 1, "A");

                        break;
                    case "bd_print1096":
                        Print1096();

                        break;
                    case "bd_form_ledgerranges":
                        frmLedgerRanges.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getcrdataentry":
                        frmGetCRDataEntry.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_currentaccountstatus":
                        frmCurrentAccountStatus.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_printpurchaseorder":
                        frmPrintPurchaseOrder.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_classcodes":
                        frmClassCodes.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_outlinerevquery":
                        frmOutlineRevQuery.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getledgersummary":
                        frmGetLedgerSummary.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_managerrequest":
                        {
                            // MDIParent.SetMenu ("ManagerRequest")
                            if (MenuChanged != null)
                                MenuChanged("ManagerRequest");

                            break;
                        }

                    case "bd_form_transferinitial":
                        frmTransferInitial.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_transfermanager":
                        frmTransferManager.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_transfercommittee":
                        frmTransferCommittee.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_printchecks":
                        frmPrintChecks.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_changestatementdate" when IsCurrentBank():
                        frmChangeStatementDate.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_changestatementdate":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;
                    case "bd_form_ledgersummaryselect":
                        frmLedgerSummarySelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_ledgerdetailselecdt":
                        frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_print1099s":
                        frm1099Setup.InstancePtr.OptionType = "P";
                        frm1099Setup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_report_checkfilelist":
                        rptCheckFileList.InstancePtr.strTransactions = "O";
                        frmReportViewer.InstancePtr.Init(rptCheckFileList.InstancePtr);

                        break;
                    case "bd_transferelectedrequesttoapproved":
                        TransferElectedRequestToApproved();

                        break;
                    case "bd_form_getcddataentry":
                        frmGetCDDataEntry.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_cdbs":
                        CDBS();

                        break;

                    case "bd_menu_detail":
                        {
                            // MDIParent.SetMenu ("Detail")
                            if (MenuChanged != null)
                                MenuChanged("Detail");

                            break;
                        }

                    case "bd_report_accountcloseout":
                        frmReportViewer.InstancePtr.Init(AccountCloseoutReport.InstancePtr);

                        break;
                    case "bd_report_liquidatedencumbrances":
                        frmReportViewer.InstancePtr.Init(rptLiquidatedEncumbrances.InstancePtr);

                        break;
                    case "bd_form_vendorlistsetup":
                        frmVendorListSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_outlineexpquery":
                        frmOutlineExpQuery.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getrevenuesummary":
                        frmGetRevenueSummary.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_initialrequest":
                        {
                            // MDIParent.SetMenu ("InitialRequest")
                            if (MenuChanged != null)
                                MenuChanged("InitialRequest");

                            break;
                        }

                    case "bd_form_editinitialrequests":
                        modBudgetaryMaster.Statics.strBudgetFlag = "I";
                       // Sys.ClearInstance(frmBudgetRequestEdit.InstancePtr);
                        frmBudgetRequestEdit.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_editmanagerrequests":
                        modBudgetaryMaster.Statics.strBudgetFlag = "M";
                        frmBudgetRequestEdit.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_editcommitteerequests":
                        modBudgetaryMaster.Statics.strBudgetFlag = "C";
                        frmBudgetRequestEdit.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_editelectedrequests":
                        modBudgetaryMaster.Statics.strBudgetFlag = "E";
                        frmBudgetRequestEdit.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_editapprovedamounts":
                        modBudgetaryMaster.Statics.strBudgetFlag = "A";
                        frmBudgetRequestEdit.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_warrantpreview":
                        frmWarrantPreview.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_addcheckdep" when IsCurrentBank():
                        frmAddCheckDep.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_addcheckdep":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;
                    case "bd_report_casheditemsonly":
                        rptCheckFileList.InstancePtr.strTransactions = "C";
                        frmReportViewer.InstancePtr.Init(rptCheckFileList.InstancePtr);

                        break;
                    case "bd_report_1099editreport":
                        frm1099Setup.InstancePtr.OptionType = "E";
                        frm1099Setup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_revenuesummaryselect":
                        frmRevenueSummarySelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_revenuedetailselect":
                        frmRevenueDetailSelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_ledgerdetailselect":
                        frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_purgegraphitem":
                        frmPurgeGraphItem.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_form_customization":
                        {
                            var tfrmcust = new frmCustomization();
                            tfrmcust.Settings = bdSettings;
                            tfrmcust.Show(App.MainForm);

                            break;
                        }

                    case "bd_form_getapdataentry":
                        frmGetAPDataEntry.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_menu_summary":
                        {
                            // MDIParent.SetMenu ("Summary")
                            if (MenuChanged != null)
                                MenuChanged("Summary");

                            break;
                        }

                    case "bd_report_closingjournal":
						frmReportViewer.InstancePtr.Init(null, string.Empty, 0, false, false, "Pages", false, Path.Combine(FCFileSystem.Statics.UserDataFolder, Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) + "EOYClosingJournal.rdf"));

                        break;
                    case "bd_report_outstandingencumbrances":
                        frmReportViewer.InstancePtr.Init(rptOutstandingEncumbrances.InstancePtr);

                        break;
                    case "bd_form_outlinedeptquery":
                        frmOutlineDeptQuery.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_getexpensesummary":
                        frmGetExpenseSummary.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_clearbudgetamounts":
                        frmClearBudgetAmounts.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_printinitialrequestworksheets":
                        frmBudgetWorksheetSetup.InstancePtr.strReportType = "I";
                        frmBudgetWorksheetSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_printmanagerrequestworksheets":
                        frmBudgetWorksheetSetup.InstancePtr.strReportType = "M";
                        frmBudgetWorksheetSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_printcommitteerequestworksheets":
                        frmBudgetWorksheetSetup.InstancePtr.strReportType = "C";
                        frmBudgetWorksheetSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_printelectedrequestworksheets":
                        frmBudgetWorksheetSetup.InstancePtr.strReportType = "E";
                        frmBudgetWorksheetSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_printapprovedamountsworksheets":
                        frmBudgetWorksheetSetup.InstancePtr.strReportType = "A";
                        frmBudgetWorksheetSetup.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_printindividualchecks":
                        frmPrintIndividualChecks.InstancePtr.Unload();
                        frmPrintIndividualChecks.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_checkrecbalancescreen" when IsCurrentBank():
                        frmCheckBalance.InstancePtr.blnChangeBegBalance = true;
                        frmCheckBalance.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_checkrecbalancescreen":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;
                    case "bd_form_checkfilebalancescreen":
                        frmCheckBalance.InstancePtr.blnChangeBegBalance = false;
                        frmCheckBalance.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_selectyear":
                        frmSelectYear.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_expensesummaryselect":
                        frmExpenseSummarySelect.InstancePtr.Close();
                        frmExpenseSummarySelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_expensedetailselect":
                        frmExpenseDetailSelect.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_form_purgechart":
                        frmPurgeChart.InstancePtr.Show(App.MainForm);

                        break;

                    case "bd_purgechecks" when IsCurrentBank():
                        {
                            answer = MessageBox.Show("Are you sure you wish to purge " + App.MainForm.StatusBarText3, "Purge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (answer == DialogResult.No) return;
                            checkRecService.PurgeChecks();
                            if (checkRecService.HadError) MessageBox.Show("Error " + FCConvert.ToString(checkRecService.LastErrorNumber) + " " + checkRecService.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                            break;
                        }

                    case "bd_purgechecks":
                        ExecuteCommand("BD_Form_ChangeBank");

                        break;

                    case "bd_transferbudget":
                        {
                            answer = MessageBox.Show("Be sure you are in your current fiscal year before transferring budget figures to accounts. This process should only be completed once per year. Would you like to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                            if (answer == DialogResult.Yes) TRANSFERAPPROVEDTOBUDGET();

                            break;
                        }

                    case "bd_createbudgetextract":
                        CreateBudgetExtract();

                        break;
                    case "bd_oronopayrollimport":
                        oPayImportService.OronoPayrollImport();

                        break;
                    case "bd_form_setuptownrevenuereport":
                        frmSetupTownRevenueReport.InstancePtr.Show(App.MainForm);

                        break;
                    case "bd_positivepayexport":
                        cCheckRecOption cro;
                        var positivePayExportForm = new frmBoothBayCheckRecExport();
                        cro = positivePayExportForm.Init();
                        if (cro.StartDate.IsDate() && cro.EndDate.IsDate())
                        {
                            cro.FileName = "CheckRecExport.csv";
                            var positivePayCheckRec = new cCheckRecExportService();
                            positivePayCheckRec.ExportSimpleCheckRec(cro);
                            if (positivePayCheckRec.HadError)
                            {
                                if (positivePayCheckRec.LastErrorNumber == 10000)
                                    MessageBox.Show(positivePayCheckRec.LastErrorMessage, "No Records", MessageBoxButtons.OK);
                                else
                                    MessageBox.Show("Error " + positivePayCheckRec.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                MessageBox.Show(cro.FileName + " created", "File Created", MessageBoxButtons.OK, MessageBoxIcon.None);
                            }
                        }
                        break;

                    default:
                    {
                        switch (commandString)
                        {
                            //case "bd_form_cariboucheckrecexport":
                            //{
                            //    cCheckRecOption cro;
                            //    var caribouCheckRecExportForm = new frmBoothBayCheckRecExport();
                            //    cro = caribouCheckRecExportForm.Init();
                            //    if (cro.StartDate.IsDate() && cro.EndDate.IsDate())
                            //    {
                            //        cro.FileName = "CheckRecExport.csv";
                            //        var caribouCheckRec = new cCheckRecExportService();
                            //        caribouCheckRec.ExportSimpleCheckRec(cro);
                            //        if (caribouCheckRec.HadError)
                            //        {
                            //            if (caribouCheckRec.LastErrorNumber == 10000)
                            //                MessageBox.Show(caribouCheckRec.LastErrorMessage, "No Records", MessageBoxButtons.OK);
                            //            else
                            //                MessageBox.Show("Error " + caribouCheckRec.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            //        }
                            //        else
                            //        {
                            //            MessageBox.Show(cro.FileName + " created", "File Created", MessageBoxButtons.OK, MessageBoxIcon.None);
                            //        }
                            //    }

                            //    break;
                            //}

                            case "bd_form_boothbaycheckrecexport":
                            {
                                //frmBoothBayCheckRecExport.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
                                cCheckRecOption bro;
                                var bbcheckRecExportForm = new frmBoothBayCheckRecExport();
                                bro = bbcheckRecExportForm.Init();
                                DateTime parsedDate;
                                //TODO:
                                if (DateTime.TryParse(bro.StartDate, out parsedDate))
                                {
                                    bro.FileName = "CheckRecExport.txt";
                                    var bbayCheckRec = new cCheckRecExportService();
                                    bbayCheckRec.ExportBoothbayCheckRec(ref bro);
                                    if (bbayCheckRec.HadError)
                                    {
                                        if (bbayCheckRec.LastErrorNumber == 10000)
                                            MessageBox.Show(bbayCheckRec.LastErrorMessage, "No Records", MessageBoxButtons.OK);
                                        else
                                            MessageBox.Show("Error " + bbayCheckRec.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        MessageBox.Show(bro.FileName + " created", "File Created", MessageBoxButtons.OK, MessageBoxIcon.None);
                                    }
                                }

                                break;
                            }

                            default:
                                switch (commandString)
                                {
                                    case "bd_orringtoncustomcheckrecap":
                                        custCheckRecService.OrringtonCustomCheckRecAP();

                                        break;
                                    case "bd_somersetcountycustomcheckrecap":
                                        custCheckRecService.SomersetCustomCheckRecAP();

                                        break;
                                    case "bd_form_clintoncheckrecexport":
                                        frmClintonCheckRecExport.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);

                                        break;
                                    case "bd_orringtoncustomcheckrecpy":
                                        custCheckRecService.OrringtonCustomCheckRecPY();

                                        break;

                                    case "bd_form_doverfoxcroftsimplecheckrecexport":
                                        ExecuteCommand("bd_form_cariboucheckrecexport");

                                        break;

                                    case "bd_form_hancockcountypayrollimport":
                                    {
                                        //FC:TODO:PJ File-Handling (Options)
                                        //ofd.Options = vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir;
                                        if (ofd.ShowOpen())
                                        {
                                            frmHancockCountyPayrollImport.InstancePtr.strFileName = ofd.FileName;
                                            frmHancockCountyPayrollImport.InstancePtr.Show(App.MainForm);
                                        }

                                        break;
                                    }

                                    default:
                                        switch (commandString)
                                        {
                                            case "bd_calaisbdsetdblocation":
                                                custFunctionService.CalaisBDSetDBLocation();

                                                break;
                                            case "bd_daytoncustomimport":
                                                custFunctionService.DaytonImport();

                                                break;
                                            case "bd_lincolncountypayrollimport":
                                                custFunctionService.LincolnCountyPayrollImport();

                                                break;
                                            case "bd_menu_checkfile" when IsCurrentBank():
                                                MenuChanged?.Invoke("CheckFile");

                                                break;
                                            case "bd_menu_checkfile":
                                                ExecuteCommand("BD_Form_ChangeBank");

                                                break;

                                            default:
                                                switch (commandString)
                                                {
                                                    case "bd_form_trialbalance":
                                                        frmTrialBalance.InstancePtr.Show(App.MainForm);

                                                        break;
                                                    case "bd_form_accountinquiry":
                                                        frmBDAccountInquiry.InstancePtr.Show(App.MainForm);

                                                        break;

                                                    case "bd_updatejournalinformation":
                                                    {
                                                        int lngAPJournal;
                                                        var briefs = journService.GetUnpostedAPJournalBriefs();
                                                        lngAPJournal = frmChooseAPJournal.InstancePtr.Init(ref briefs);
                                                        if (lngAPJournal > 0)
                                                        {
                                                            var apView = new cEditAPViewModel();
                                                            apView.LoadAPJournalMaster(lngAPJournal);
                                                            apView.AllowEditing();
                                                            var EditAPJournal = new frmEditAPJournal();
                                                            EditAPJournal.InitializeScreen(ref apView);
                                                        }

                                                        break;
                                                    }
                                                }

                                                break;
                                        }

                                        break;
                                }

                                break;
                        }

                        break;
                    }
                }
            }
            finally
            {
                rsTemp.DisposeOf();
                ofd.Dispose();
            }

        }

        private void ExitApplication()
        {
            modBudgetaryMaster.Statics.gboolClosingProgram = true;
            modBlockEntry.WriteYY();
            //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
            //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
            App.MainForm.OpenModule("TWGNENTY");
            /*? On Error Resume Next  *//*? On Error GoTo 0 */
            foreach (Form f in FCGlobal.Statics.Forms) f.Close();
        }

        private void BuildAutomaticJournals()
        {
            var rsInfo = new clsDRWrapper();
            rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RM'");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
                {
                    MessageBox.Show("You must set up a Real Estate Commitment Account in file maintenance - Standard Accounts before you may continue.", "No Real Estate Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    FCGlobal.Screen.MousePointer = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("You must set up a Real Estate Commitment Account in file maintenance - Standard Accounts before you may continue.", "No Real Estate Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                FCGlobal.Screen.MousePointer = 0;
                return;
            }
            rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'PM'");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
                {
                    MessageBox.Show("You must set up a Personal Property Commitment Account in file maintenance - Standard Accounts before you may continue.", "No Personal Property Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    FCGlobal.Screen.MousePointer = 0;
                    return;
                }
            }
            else
            {
                MessageBox.Show("You must set up a Personal Property Commitment Account in file maintenance - Standard Accounts before you may continue.", "No Perosnal Property Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                FCGlobal.Screen.MousePointer = 0;
                return;
            }
            rsInfo.OpenRecordset("SELECT * FROM TaxCommitment");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                // do nothing
            }
            else
            {
                MessageBox.Show("There is no information found", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                FCGlobal.Screen.MousePointer = 0;
                return;
            }
            frmAutomaticJournals.InstancePtr.Show(App.MainForm);
            // show the blankform
        }

        private void ShowCustomBudgetReport()
        {
            frmCustomBudgetReport.InstancePtr.Unload();
            frmCustomBudgetReport.InstancePtr.Show(App.MainForm);
            modCustomReport.SetFormFieldCaptions(frmCustomBudgetReport.InstancePtr, "CustomBudget");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(0, 1, "T");
            frmCustomBudgetReport.InstancePtr.ShowTownOptions();
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(1, 1, "B");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(2, 1, "A");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(4, 1, FCConvert.ToString(FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy")).Year));
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(5, 1, FCConvert.ToString(FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy")).Year + 1));
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(6, 1, "Y");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(7, 1, "Y");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(8, 1, "Y");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(9, 1, "Y");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(10, 1, "Y");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(11, 1, "B");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(13, 1, "Y");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(14, 1, "1");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(15, 1, "Y");
            if (modAccountTitle.Statics.ObjFlag)
                frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(16, 1, "Ex");
            else
                frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(16, 1, "Ob");
            frmCustomBudgetReport.InstancePtr.vsWhere.TextMatrix(17, 1, "Re");
        }

        private string CheckForCurrentBank(string strCommand)
        {
            var CheckForCurrentBank = "";
            cBank bk;
            string strCommandToUse;
            strCommandToUse = strCommand;
            bk = bankService.GetCurrentBank();
            if (bk == null)
                strCommandToUse = "BD_Form_ChangeBank";
            else if (bk.ID == 0) strCommandToUse = "BD_Form_ChangeBank";
            CheckForCurrentBank = strCommandToUse;
            return CheckForCurrentBank;
        }

        private bool IsCurrentBank()
        {
            var IsCurrentBank = false;
            cBank bk;
            bk = bankService.GetCurrentBank();
            bool boolIsCurrent;
            boolIsCurrent = true;
            if (bk == null)
                boolIsCurrent = false;
            else if (bk.ID == 0) boolIsCurrent = false;
            IsCurrentBank = boolIsCurrent;
            return IsCurrentBank;
        }

        private void Print1096()
        {
            Information.Err().Clear();

            var result = StaticSettings.GlobalCommandDispatcher.Send(new SelectTaxFormType
            {
                ShowAllOption = false
            }).Result;

            if (result.Success)
            {
                var report = new rpt1096Laser(result.selectedType);
                frmReportViewer.InstancePtr.Init(report);
            }
            else
            {
                return;
            }
        }

        private void TransferElectedRequestToApproved()
        {
            var rsBudgetInfo = new clsDRWrapper();
            rsBudgetInfo.OpenRecordset("SELECT * FROM Budget");
            if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
            {
                do
                {
                    rsBudgetInfo.Edit();
                    // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                    rsBudgetInfo.Set_Fields("ApprovedAmount", FCConvert.ToString(Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest"))));
                    if (FCConvert.ToBoolean(rsBudgetInfo.Get_Fields_Boolean("PercentFlag")))
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        if (Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")) != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("JanBudget", rsBudgetInfo.Get_Fields_Double("JanPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("FebBudget", rsBudgetInfo.Get_Fields_Double("FebPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("MarBudget", rsBudgetInfo.Get_Fields_Double("MarPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("AprBudget", rsBudgetInfo.Get_Fields_Double("AprPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("MayBudget", rsBudgetInfo.Get_Fields_Double("MayPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("JuneBudget", rsBudgetInfo.Get_Fields_Double("JunePercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("JulyBudget", rsBudgetInfo.Get_Fields_Double("JulyPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("AugBudget", rsBudgetInfo.Get_Fields_Double("AugPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("SeptBudget", rsBudgetInfo.Get_Fields_Double("SeptPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("OctBudget", rsBudgetInfo.Get_Fields_Double("OctPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("NovBudget", rsBudgetInfo.Get_Fields_Double("NovPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            rsBudgetInfo.Set_Fields("DecBudget", rsBudgetInfo.Get_Fields_Double("DecPercent") * Conversion.Val(rsBudgetInfo.Get_Fields("ElectedRequest")));
                        }
                    rsBudgetInfo.Update(true);
                    rsBudgetInfo.MoveNext();
                }
                while (rsBudgetInfo.EndOfFile() != true);
                modGlobalFunctions.AddCYAEntry_8("BD", "Transfer Elected Budget Request Done");
            }
            MessageBox.Show("Elected Request Amounts Transferred Successfully!", "Successful Transfer", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void TRANSFERAPPROVEDTOBUDGET()
        {
            var rsBudgetInfo = new clsDRWrapper();
            var rsAccountInfo = new clsDRWrapper();
            bool blnAmountsExist;
            // vbPorter upgrade warning: answer As short, int --> As DialogResult
            DialogResult answer;
            // vbPorter upgrade warning: curRemaining As Decimal	OnWrite(short, Decimal)
            Decimal curRemaining;
            var Master = new clsDRWrapper();
            var JournalNumber = 0;
            var strRevCtrl = "";
            var strExpCtrl = "";
            var strFundBalance = "";
            modBudgetaryAccounting.FundType[] accts = null;
            modBudgetaryAccounting.FundType[] ExpTotals = null;
            modBudgetaryAccounting.FundType[] RevTotals = null;
            int counter;
            var strField1 = "";
            var strField2 = "";
            var strField3 = "";
            var strField4 = "";
            var strField5 = "";
            bool blnBudgetAmountsExist;
            var lngTempValidAcctCheck = 0;
            var clsPostInfo = new clsBudgetaryPosting();
            var clsJournalInfo = new clsPostingJournalInfo();
            blnAmountsExist = false;
            rsBudgetInfo.Execute("DELETE FROM Budget WHERE Left(Account, 1) = 'G' or Left(Account, 1) = 'L'", "Budgetary", false);
            rsBudgetInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' OR AccountType = 'R' or AccountType = 'P' OR AccountType = 'V'");
            while (rsBudgetInfo.EndOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("CurrentBudget")) > 0)
                {
                    blnAmountsExist = true;
                    break;
                }
                rsBudgetInfo.MoveNext();
            }
            if (blnAmountsExist)
            {
                answer = MessageBox.Show("There are already amounts in your current budget.  Do you wish to overwrite these amounts?", "Budget Amounts Already Exist", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (answer == DialogResult.No)
                {
                    return;
                }
                else
                {
                    // do nothing
                }
            }
            rsBudgetInfo.OpenRecordset("SELECT * FROM Budget");
            if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
            {
                lngTempValidAcctCheck = modValidateAccount.Statics.ValidAcctCheck;
                modValidateAccount.Statics.ValidAcctCheck = 3;
                do
                {
                    if (Conversion.Val(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount")) != 0)
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        if (modValidateAccount.AccountValidate(rsBudgetInfo.Get_Fields("Account")) == false)
                        {
                            modValidateAccount.Statics.ValidAcctCheck = lngTempValidAcctCheck;
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            MessageBox.Show("You must set up account " + rsBudgetInfo.Get_Fields("Account") + " before you may proceed with this process.", "Bad Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    rsBudgetInfo.MoveNext();
                }
                while (rsBudgetInfo.EndOfFile() != true);
                modValidateAccount.Statics.ValidAcctCheck = lngTempValidAcctCheck;
            }
            rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EC'");
            if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                strExpCtrl = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
                if (Conversion.Val(strExpCtrl) == 0)
                {
                    MessageBox.Show("You must set up a Town Expense Control Account before you may continue.", "No Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                MessageBox.Show("You must set up a Town Expense Control Account before you may continue.", "No Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RC'");
            if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                strRevCtrl = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
                if (Conversion.Val(strRevCtrl) == 0)
                {
                    MessageBox.Show("You must set up a Town Revenue Control Account before you may continue.", "No Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                MessageBox.Show("You must set up a Town Revenue Control Account before you may continue.", "No Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'FB'");
            if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                strFundBalance = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
                if (Conversion.Val(strFundBalance) == 0)
                {
                    MessageBox.Show("You must set up a Town Fund Balance Account before you may continue.", "No Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                MessageBox.Show("You must set up a Town Fund Balance Account before you may continue.", "No Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Transferring to Budget");
            frmWait.InstancePtr.Refresh();
            curRemaining = 0;
            counter = 0;
            rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E' and IsNull(ApprovedAmount, 0) <> 0");
            while (rsBudgetInfo.EndOfFile() != true)
            {
                //Application.DoEvents();
                Array.Resize(ref accts, counter + 1);
                //FC:FINAL:DSE Initialize string fields with empty value
                accts[counter].Init();
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                accts[counter].Account = FCConvert.ToString(rsBudgetInfo.Get_Fields("Account"));
                accts[counter].AcctType = "A";
                accts[counter].Amount = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
                accts[counter].Description = "Initial Budget";
                counter += 1;
                rsBudgetInfo.MoveNext();
            }
            ExpTotals = modBudgetaryAccounting.CalcFundCash(ref accts);
            counter = 0;
            accts = new modBudgetaryAccounting.FundType[counter + 1];
            //FC:FINAL:AM:#i681 - init the fields
            accts[0].Init();
            rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'R' and IsNull(ApprovedAmount, 0) <> 0");
            while (rsBudgetInfo.EndOfFile() != true)
            {
                //Application.DoEvents();
                Array.Resize(ref accts, counter + 1);
                //FC:FINAL:DSE Initialize string fields with empty value
                accts[counter].Init();
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                accts[counter].Account = FCConvert.ToString(rsBudgetInfo.Get_Fields("Account"));
                accts[counter].AcctType = "A";
                accts[counter].Amount = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
                accts[counter].Description = "Initial Budget";
                counter += 1;
                rsBudgetInfo.MoveNext();
            }
            RevTotals = modBudgetaryAccounting.CalcFundCash(ref accts);
            blnBudgetAmountsExist = false;
            for (counter = 1; counter <= 99; counter++)
                //Application.DoEvents();
                if (ExpTotals[counter].Amount != 0 || RevTotals[counter].Amount != 0)
                {
                    blnBudgetAmountsExist = true;
                    break;
                }
            if (blnBudgetAmountsExist == false)
            {
                frmWait.InstancePtr.Unload();
                //Application.DoEvents();
                MessageBox.Show("There are no budget amounts to transfer", "No Budget Amounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (modBudgetaryAccounting.LockJournal() == false)
            {
                frmWait.InstancePtr.Unload();
                //Application.DoEvents();
                MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
            if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
            {
                Master.MoveLast();
                Master.MoveFirst();
                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
            }
            else
            {
                JournalNumber = 1;
            }
            Master.AddNew();
            Master.Set_Fields("JournalNumber", JournalNumber);
            Master.Set_Fields("Status", "E");
            Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
            Master.Set_Fields("StatusChangeDate", DateTime.Today);
            Master.Set_Fields("Description", "Initial Budget");
            Master.Set_Fields("Type", "GJ");
            Master.Set_Fields("Period", modBudgetaryMaster.Statics.FirstMonth);
            clsJournalInfo = new clsPostingJournalInfo();
            clsJournalInfo.JournalNumber = JournalNumber;
            clsJournalInfo.JournalType = "GJ";
            clsJournalInfo.Period = FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth);
            clsJournalInfo.CheckDate = "";
            Master.Update();
            Master.Reset();
            modBudgetaryAccounting.UnlockJournal();
            rsAccountInfo.OmitNullsOnInsert = true;
            rsAccountInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
            for (counter = 1; counter <= 99; counter++)
                //Application.DoEvents();
                if (ExpTotals[counter].Amount != 0 || RevTotals[counter].Amount != 0)
                {
                    if (ExpTotals[counter].Amount != 0)
                    {
                        rsAccountInfo.AddNew();
                        rsAccountInfo.Set_Fields("JournalNumber", JournalNumber);
                        rsAccountInfo.Set_Fields("Description", "Initial Budget");
                        if (!modAccountTitle.Statics.YearFlag)
                            rsAccountInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strExpCtrl + "-00");
                        else
                            rsAccountInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strExpCtrl);
                        rsAccountInfo.Set_Fields("RCB", "R");
                        rsAccountInfo.Set_Fields("Amount", ExpTotals[counter].Amount * -1);
                        rsAccountInfo.Set_Fields("Status", "E");
                        rsAccountInfo.Set_Fields("Period", modBudgetaryMaster.Statics.FirstMonth);
                        if (modBudgetaryMaster.Statics.FirstMonth == 1)
                        {
                            if (DateTime.Today.Month > 10)
                                rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
                            else
                                rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }
                        else
                        {
                            rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }
                        rsAccountInfo.Set_Fields("Type", "G");
                        rsAccountInfo.Update();
                    }
                    if (RevTotals[counter].Amount != 0)
                    {
                        rsAccountInfo.AddNew();
                        rsAccountInfo.Set_Fields("JournalNumber", JournalNumber);
                        rsAccountInfo.Set_Fields("Description", "Initial Budget");
                        if (!modAccountTitle.Statics.YearFlag)
                            rsAccountInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strRevCtrl + "-00");
                        else
                            rsAccountInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strRevCtrl);
                        rsAccountInfo.Set_Fields("RCB", "R");
                        rsAccountInfo.Set_Fields("Amount", RevTotals[counter].Amount);
                        rsAccountInfo.Set_Fields("Status", "E");
                        rsAccountInfo.Set_Fields("Period", modBudgetaryMaster.Statics.FirstMonth);
                        if (modBudgetaryMaster.Statics.FirstMonth == 1)
                        {
                            if (DateTime.Today.Month > 10)
                                rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
                            else
                                rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }
                        else
                        {
                            rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }
                        rsAccountInfo.Set_Fields("Type", "G");
                        rsAccountInfo.Update();
                    }
                    if (ExpTotals[counter].Amount - RevTotals[counter].Amount != 0)
                    {
                        curRemaining = ExpTotals[counter].Amount - RevTotals[counter].Amount;
                        rsAccountInfo.AddNew();
                        rsAccountInfo.Set_Fields("JournalNumber", JournalNumber);
                        rsAccountInfo.Set_Fields("Description", "Initial Budget");
                        if (!modAccountTitle.Statics.YearFlag)
                            rsAccountInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strFundBalance + "-00");
                        else
                            rsAccountInfo.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + strFundBalance);
                        rsAccountInfo.Set_Fields("RCB", "R");
                        rsAccountInfo.Set_Fields("Amount", curRemaining);
                        rsAccountInfo.Set_Fields("Status", "E");
                        rsAccountInfo.Set_Fields("Period", modBudgetaryMaster.Statics.FirstMonth);
                        if (modBudgetaryMaster.Statics.FirstMonth == 1)
                        {
                            if (DateTime.Today.Month > 10)
                                rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
                            else
                                rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }
                        else
                        {
                            rsAccountInfo.Set_Fields("JournalEntriesDate", FCConvert.ToString(modBudgetaryMaster.Statics.FirstMonth) + "/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }
                        rsAccountInfo.Set_Fields("Type", "G");
                        rsAccountInfo.Update();
                    }
                }


            rsBudgetInfo.OpenRecordset("SELECT * FROM Budget");
            while (rsBudgetInfo.EndOfFile() != true)
            {
                //Application.DoEvents();
                strField1 = "";
                strField2 = "";
                strField3 = "";
                strField4 = "";
                strField5 = "";
                // If Val(rsBudgetInfo.Fields["ApprovedAmount"]) <> 0 Then
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                if (Strings.Left(FCConvert.ToString(rsBudgetInfo.Get_Fields("Account")), 1) == "E")
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    strField1 = Department_2(rsBudgetInfo.Get_Fields("Account"));
                    if (modAccountTitle.Statics.ExpDivFlag)
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        strField2 = Expense_2(rsBudgetInfo.Get_Fields("Account"));
                        if (!modAccountTitle.Statics.ObjFlag)
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strField3 = Object_2(rsBudgetInfo.Get_Fields("Account"));
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        strField2 = Division_2(rsBudgetInfo.Get_Fields("Account"));
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        strField3 = Expense_2(rsBudgetInfo.Get_Fields("Account"));
                        if (!modAccountTitle.Statics.ObjFlag)
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strField4 = Object_2(rsBudgetInfo.Get_Fields("Account"));
                    }
                }
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                else
                {
                    if (Strings.Left(FCConvert.ToString(rsBudgetInfo.Get_Fields("Account")), 1) == "R")
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        strField1 = Department_2(rsBudgetInfo.Get_Fields("Account"));
                        if (modAccountTitle.Statics.RevDivFlag)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strField2 = Revenue_2(rsBudgetInfo.Get_Fields("Account"));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strField2 = Division_2(rsBudgetInfo.Get_Fields("Account"));
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strField3 = Revenue_2(rsBudgetInfo.Get_Fields("Account"));
                        }
                    }
                }

                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + rsBudgetInfo.Get_Fields("Account") + "'");
                if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
                {
                    rsAccountInfo.Edit();
                    rsAccountInfo.Set_Fields("Valid", true);
                    rsAccountInfo.Set_Fields("CurrentBudget", rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
                    rsAccountInfo.Set_Fields("JanBudget", rsBudgetInfo.Get_Fields_Decimal("JanBudget"));
                    rsAccountInfo.Set_Fields("FebBudget", rsBudgetInfo.Get_Fields_Decimal("FebBudget"));
                    rsAccountInfo.Set_Fields("MarBudget", rsBudgetInfo.Get_Fields_Decimal("MarBudget"));
                    rsAccountInfo.Set_Fields("AprBudget", rsBudgetInfo.Get_Fields_Decimal("AprBudget"));
                    rsAccountInfo.Set_Fields("MayBudget", rsBudgetInfo.Get_Fields_Decimal("MayBudget"));
                    rsAccountInfo.Set_Fields("JuneBudget", rsBudgetInfo.Get_Fields_Decimal("JuneBudget"));
                    rsAccountInfo.Set_Fields("JulyBudget", rsBudgetInfo.Get_Fields_Decimal("JulyBudget"));
                    rsAccountInfo.Set_Fields("AugBudget", rsBudgetInfo.Get_Fields_Decimal("AugBudget"));
                    rsAccountInfo.Set_Fields("SeptBudget", rsBudgetInfo.Get_Fields_Decimal("SeptBudget"));
                    rsAccountInfo.Set_Fields("OctBudget", rsBudgetInfo.Get_Fields_Decimal("OctBudget"));
                    rsAccountInfo.Set_Fields("NovBudget", rsBudgetInfo.Get_Fields_Decimal("NovBudget"));
                    rsAccountInfo.Set_Fields("DecBudget", rsBudgetInfo.Get_Fields_Decimal("DecBudget"));
                    rsAccountInfo.Set_Fields("JanPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("JanPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("FebPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("FebPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("MarPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("MarPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("AprPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("AprPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("MayPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("MayPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("JunePercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("JunePercent"), "#.00"));
                    rsAccountInfo.Set_Fields("JulyPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("JulyPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("AugPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("AugPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("SeptPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("SeptPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("OctPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("OctPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("NovPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("NovPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("DecPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("DecPercent"), "#.00"));
                    rsAccountInfo.Update(true);
                }
                else
                {
                    rsAccountInfo.AddNew();
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsAccountInfo.Set_Fields("AccountType", Strings.Left(FCConvert.ToString(rsBudgetInfo.Get_Fields("Account")), 1));
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsAccountInfo.Set_Fields("Account", rsBudgetInfo.Get_Fields("Account"));
                    rsAccountInfo.Set_Fields("FirstAccountField", strField1);
                    rsAccountInfo.Set_Fields("SecondAccountField", strField2);
                    rsAccountInfo.Set_Fields("ThirdAccountField", strField3);
                    rsAccountInfo.Set_Fields("FourthAccountField", strField4);
                    rsAccountInfo.Set_Fields("FifthAccountField", strField5);
                    rsAccountInfo.Set_Fields("DateCreated", DateTime.Today);
                    rsAccountInfo.Set_Fields("Valid", true);
                    rsAccountInfo.Set_Fields("CurrentBudget", rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
                    rsAccountInfo.Set_Fields("JanBudget", rsBudgetInfo.Get_Fields_Decimal("JanBudget"));
                    rsAccountInfo.Set_Fields("FebBudget", rsBudgetInfo.Get_Fields_Decimal("FebBudget"));
                    rsAccountInfo.Set_Fields("MarBudget", rsBudgetInfo.Get_Fields_Decimal("MarBudget"));
                    rsAccountInfo.Set_Fields("AprBudget", rsBudgetInfo.Get_Fields_Decimal("AprBudget"));
                    rsAccountInfo.Set_Fields("MayBudget", rsBudgetInfo.Get_Fields_Decimal("MayBudget"));
                    rsAccountInfo.Set_Fields("JuneBudget", rsBudgetInfo.Get_Fields_Decimal("JuneBudget"));
                    rsAccountInfo.Set_Fields("JulyBudget", rsBudgetInfo.Get_Fields_Decimal("JulyBudget"));
                    rsAccountInfo.Set_Fields("AugBudget", rsBudgetInfo.Get_Fields_Decimal("AugBudget"));
                    rsAccountInfo.Set_Fields("SeptBudget", rsBudgetInfo.Get_Fields_Decimal("SeptBudget"));
                    rsAccountInfo.Set_Fields("OctBudget", rsBudgetInfo.Get_Fields_Decimal("OctBudget"));
                    rsAccountInfo.Set_Fields("NovBudget", rsBudgetInfo.Get_Fields_Decimal("NovBudget"));
                    rsAccountInfo.Set_Fields("DecBudget", rsBudgetInfo.Get_Fields_Decimal("DecBudget"));
                    rsAccountInfo.Set_Fields("JanPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("JanPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("FebPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("FebPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("MarPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("MarPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("AprPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("AprPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("MayPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("MayPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("JunePercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("JunePercent"), "#.00"));
                    rsAccountInfo.Set_Fields("JulyPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("JulyPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("AugPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("AugPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("SeptPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("SeptPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("OctPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("OctPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("NovPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("NovPercent"), "#.00"));
                    rsAccountInfo.Set_Fields("DecPercent", Strings.Format(rsBudgetInfo.Get_Fields_Double("DecPercent"), "#.00"));
                    rsAccountInfo.Update(true);
                }
                // End If
                rsBudgetInfo.MoveNext();
            }
            modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(JournalNumber), true);
            frmWait.InstancePtr.Unload();
            //Application.DoEvents();
            if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptBudgetTransfer))
            {
                if (MessageBox.Show("Budget Transfer Complete.  Journal " + FCConvert.ToString(JournalNumber) + " has been created to hold Budget Entries.  Would you like to post this journal?", "Successful Transfer", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    clsPostInfo.ClearJournals();
                    clsPostInfo.AllowPreview = true;
                    clsPostInfo.AddJournal(clsJournalInfo);
                    clsPostInfo.PostJournals();
                }
            }
            else
            {
                MessageBox.Show("Budget Transfer Complete.  Journal " + FCConvert.ToString(JournalNumber) + " has been created to hold Budget Entries", "Successful Transfer", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void CDBS()
        {
            var bDB = new cBudgetaryDB();
            cVersionInfo tVer;
            if (bDB.CheckVersion())
            {
                MessageBox.Show("Database Structure Check completed successfully. Checking archives now", "Successful",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                var util = new cArchiveUtility();
                var archives = util.GetArchiveGroupNames("Archive", 0);
                var wrapper = new clsDRWrapper();
                var liveGroup = wrapper.DefaultGroup;
                foreach (string archive in archives)
                {
                    wrapper.DefaultGroup = archive;
                    var archiveDB = new cBudgetaryDB();
                    archiveDB.CheckVersion();
                }

                wrapper.DefaultGroup = liveGroup;
                MessageBox.Show("Archive database structure checks completed successfully", "Successful");
            }
            else

            {
                MessageBox.Show("Error Number " + FCConvert.ToString(bDB.LastErrorNumber) + "  " + bDB.LastErrorMessage,
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                tVer = bDB.GetVersion();
                MessageBox.Show("Current DB Version is " + tVer.VersionString);
            }
    }

        private string Revenue_2(string x)
        {
            return Revenue(ref x);
        }

        private string Revenue(ref string x)
        {
            var Revenue = "";
            if (!modAccountTitle.Statics.RevDivFlag)
                Revenue = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
            else
                Revenue = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));

            return Revenue;
        }

        private string Expense_2(string x)
        {
            return Expense(ref x);
        }

        private string Expense(ref string x)
        {
            var Expense = "";
            if (!modAccountTitle.Statics.ExpDivFlag)
                Expense = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
            else
                Expense = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));

            return Expense;
        }

        private string Department_2(string x)
        {
            return Department(ref x);
        }

        private string Department(ref string x)
        {
            var Department = "";
            Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
            return Department;
        }

        private string Division_2(string x)
        {
            return Division(ref x);
        }

        private string Division(ref string x)
        {
            var Division = "";
            Division = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
            return Division;
        }

        private string Object_2(string x)
        {
            return Object(ref x);
        }

        private string Object(ref string x)
        {
            var Object = "";
            if (!modAccountTitle.Statics.ExpDivFlag)
                Object = Strings.Mid(x, 6 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
            else
                Object = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));

            return Object;
        }

        private void CreateBudgetExtract()
        {
            string strDirectory;
            var lngCounter = 0;
            var temp = "";
            var rsAccountInfo = new clsDRWrapper();
            var lngCurrent = 0;
            var rsLastYearInfo = new clsDRWrapper();
            var rsCurrentYearInfo = new clsDRWrapper();
            //FC:FINAL:AM:#i584 - download the file
            strDirectory = FCFileSystem.Statics.UserDataFolder;
            Information.Err().Clear();
            //App.MainForm.CommonDialog1.Flags = 0;  // - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
            //App.MainForm.CommonDialog1.CancelError = true;
            ///*? On Error Resume Next  */
            //App.MainForm.CommonDialog1.Filter = "*.txt";
            //App.MainForm.CommonDialog1.ShowSave();
            if (Information.Err().Number == 0)
            {
                lngCounter = 0;
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Extracting Data", true);
                frmWait.InstancePtr.Refresh();
                //temp = App.MainForm.CommonDialog1.FileName;
                //FC:FINAL:SBE - #762 - create folder if it does not exist
                temp = FCFileSystem.Statics.UserDataFolder;
                if (!Directory.Exists(temp)) Directory.CreateDirectory(temp);
                temp = Path.Combine(temp, "BDExtract.txt");
                //if (Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare) == 0)
                //{
                //    temp += ".txt";
                //}
                //else
                //{
                //    temp = Strings.Left(temp, Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare)) + "txt";
                //}
                //FCFileSystem.ChDrive(strDirectory);
                //Application.StartupPath = strDirectory;
                FCFileSystem.FileOpen(1, temp, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                rsAccountInfo.OpenRecordset("SELECT * FROM Budget ORDER BY Account");
                if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
                {
                    if (modBudgetaryMaster.Statics.FirstMonth != 1 && DateTime.Today.Month > modBudgetaryMaster.Statics.FirstMonth)
                        lngCurrent = DateTime.Today.Year + 1;
                    else
                        lngCurrent = DateTime.Today.Year;
                    FCFileSystem.WriteLine(1, "Account Number", "Account Description", "Initial Request", "Manager Request", "Committee Request", "Elected Request", "Approved Budget", "Breakdown By Amount or Percent", "January Amount", "January Percent", "February Amount", "February Percent", "March Amount", "March Percent", "April Amount", "April Percent", "May Amount", "May Percent", "June Amount", "June Percent", "July Amount", "July Percent", "August Amount", "August Percent", "September Amount", "September Percent", "October Amount", "October Percent", "November Amount", "November Percent", "December Amount", "December Percent", "Comments", "Last Year Budget", "Last Year Actual", "Current Year Budget", "Current Year Actual");
                    System.Collections.Generic.List<object> tempObjList = null;
                    do
                    {
                        //Application.DoEvents();
                        lngCounter += 1;
                        frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(lngCounter) / rsAccountInfo.RecordCount()) * 100);
                        frmWait.InstancePtr.Refresh();
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsLastYearInfo.OpenRecordset("SELECT TOP 1 * FROM PastBudgets WHERE Year = " + FCConvert.ToString(lngCurrent - 1) + " AND Account = '" + rsAccountInfo.Get_Fields("Account") + "' ORDER BY Year DESC");
                        // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                        rsCurrentYearInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + rsAccountInfo.Get_Fields("account") + "'");
                        //FC:FINAL:MSH - Issue #800: replaced to list of objects, which after we can save, because 'Write' and 'WriteLine' have the same
                        // functionality (the methods add '\r\n' to the file after of each item)
                        tempObjList = new System.Collections.Generic.List<object>();
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("Account"));
                        //FCFileSystem.Write(1, modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields("Account")));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("InitialRequest"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("ManagerRequest"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("CommitteeRequest"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("ElectedRequest"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("ApprovedAmount"));
                        //if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields("PercentFlag")))
                        //{
                        //    FCFileSystem.Write(1, "P");
                        //}
                        //else
                        //{
                        //    FCFileSystem.Write(1, "A");
                        //}
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("JanBudget"), rsAccountInfo.Get_Fields("JanPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("FebBudget"), rsAccountInfo.Get_Fields("FebPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("MarBudget"), rsAccountInfo.Get_Fields("MarPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("AprBudget"), rsAccountInfo.Get_Fields("AprPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("MayBudget"), rsAccountInfo.Get_Fields("MayPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("JuneBudget"), rsAccountInfo.Get_Fields("JunePercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("JulyBudget"), rsAccountInfo.Get_Fields("JulyPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("AugBudget"), rsAccountInfo.Get_Fields("AugPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("SeptBudget"), rsAccountInfo.Get_Fields("SeptPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("OctBudget"), rsAccountInfo.Get_Fields("OctPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("NovBudget"), rsAccountInfo.Get_Fields("NovPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("DecBudget"), rsAccountInfo.Get_Fields("DecPercent"));
                        //FCFileSystem.Write(1, rsAccountInfo.Get_Fields("Comments"));
                        //if (rsLastYearInfo.EndOfFile() != true && rsLastYearInfo.BeginningOfFile() != true)
                        //{
                        //    FCFileSystem.Write(1, rsLastYearInfo.Get_Fields("EndBudget"), rsLastYearInfo.Get_Fields("ActualSpent"));
                        //}
                        //else
                        //{
                        //    FCFileSystem.Write(1, 0, 0);
                        //}
                        //if (rsCurrentYearInfo.EndOfFile() != true && rsCurrentYearInfo.BeginningOfFile() != true)
                        //{
                        //    FCFileSystem.WriteLine(1, rsCurrentYearInfo.Get_Fields("CurrentBudget") + FCConvert.ToDecimal(GetBudgetAdjustments_2(rsAccountInfo.Get_Fields("Account"))), GetYTDNet_2(rsAccountInfo.Get_Fields("Account")));
                        //}
                        //else
                        //{
                        //    FCFileSystem.WriteLine(1, 0, 0);
                        //}
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        tempObjList.Add(rsAccountInfo.Get_Fields("Account"));
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        tempObjList.Add(modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields("Account")));
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        tempObjList.Add(rsAccountInfo.Get_Fields("InitialRequest"));
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        tempObjList.Add(rsAccountInfo.Get_Fields("ManagerRequest"));
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        tempObjList.Add(rsAccountInfo.Get_Fields("CommitteeRequest"));
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        tempObjList.Add(rsAccountInfo.Get_Fields("ElectedRequest"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("ApprovedAmount"));
                        if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("PercentFlag")))
                            tempObjList.Add("P");
                        else
                            tempObjList.Add("A");
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("JanBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("JanPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("FebBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("FebPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("MarBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("MarPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("AprBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("AprPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("MayBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("MayPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("JuneBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("JunePercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("JulyBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("JulyPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("AugBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("AugPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("SeptBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("SeptPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("OctBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("OctPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("NovBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("NovPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("DecBudget"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_Double("DecPercent"));
                        tempObjList.Add(rsAccountInfo.Get_Fields_String("Comments"));
                        if (rsLastYearInfo.EndOfFile() != true && rsLastYearInfo.BeginningOfFile() != true)
                        {
                            tempObjList.Add(rsLastYearInfo.Get_Fields_Decimal("EndBudget"));
                            tempObjList.Add(rsLastYearInfo.Get_Fields_Decimal("ActualSpent"));
                        }
                        else
                        {
                            tempObjList.Add(0);
                            tempObjList.Add(0);
                        }
                        if (rsCurrentYearInfo.EndOfFile() != true && rsCurrentYearInfo.BeginningOfFile() != true)
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            tempObjList.Add(rsCurrentYearInfo.Get_Fields("CurrentBudget") + FCConvert.ToDecimal(GetBudgetAdjustments_2(rsAccountInfo.Get_Fields("Account"))));
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            tempObjList.Add(GetYTDNet_2(rsAccountInfo.Get_Fields("Account")));
                        }
                        else
                        {
                            tempObjList.Add(0);
                            tempObjList.Add(0);
                        }
                        FCFileSystem.WriteLine(1, tempObjList.ToArray());
                        rsAccountInfo.MoveNext();
                    }
                    while (rsAccountInfo.EndOfFile() != true);
                    FCFileSystem.FileClose(1);
                    FCGlobal.Screen.MousePointer = 0;
                    frmWait.InstancePtr.Unload();
                    //Application.DoEvents();
                    MessageBox.Show("Extract Completed Successfully!", "Extract Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(temp);
                }
                else
                {
                    frmWait.InstancePtr.Unload();
                    //Application.DoEvents();
                    MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                FCFileSystem.ChDrive(strDirectory);
                //Application.StartupPath = strDirectory;
                return;
            }
            //Application.DoEvents();
            frmWait.InstancePtr.Unload();
            FCFileSystem.ChDrive(strDirectory);
            //Application.StartupPath = strDirectory;
        }

        public double GetYTDNet_2(string strAcct, string strPath = "")
        {
            return GetYTDNet(ref strAcct, strPath);
        }

        public double GetYTDNet(ref string strAcct, string strPath = "")
        {
            double GetYTDNet = 0;
            int temp;
            GetYTDNet = GetYTDDebit(ref strAcct, strPath) - GetYTDCredit(ref strAcct, strPath);
            return GetYTDNet;
        }

        private double GetYTDDebit(ref string strAcct, string strPath = "")
        {
            double GetYTDDebit = 0;
            var HighDate = 0;
            int LowDate;
            var strPeriodCheck = "";
            var rs2 = new clsDRWrapper();
            var strTable = "";
            if (strPath != "") rs2.GroupName = strPath;

            if (modBudgetaryMaster.Statics.FirstMonth == 1)
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(12));
            else
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(modBudgetaryMaster.Statics.FirstMonth - 1));
            LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
            if (LowDate > HighDate)
                strPeriodCheck = "OR";
            else
                strPeriodCheck = "AND";

            //FC:FINAL:BBE:#i750 - set missing table name in sql statement (same as in cJournalService)
            if (Strings.Left(strAcct, 1) == "E")
                strTable = "ExpenseReportInfo";
            // CalculateAccountInfo True, True, True, "E", strPath
            else if (Strings.Left(strAcct, 1) == "R")
                strTable = "RevenueReportInfo";
            // CalculateAccountInfo True, True, True, "R", strPath
            else if (Strings.Left(strAcct, 1) == "G")
                strTable = "LedgerReportInfo";
            // CalculateAccountInfo True, True, True, "G", strPath
            rs2.OpenRecordset("SELECT SUM(PostedDebits) AS PostedDebitsTotal FROM " + strTable + " WHERE Account = '" + strAcct + "' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account");
            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                GetYTDDebit = Conversion.Val(rs2.Get_Fields("PostedDebitsTotal"));
            else
                GetYTDDebit = 0;
            GetYTDDebit += GetEncumbrance(ref strAcct, strPath);
            return GetYTDDebit;
        }

        public double GetBudgetAdjustments_2(string strAcct)
        {
            return GetBudgetAdjustments(ref strAcct);
        }

        public double GetBudgetAdjustments(ref string strAcct)
        {
            double GetBudgetAdjustments = 0;
            int temp;
            int HighDate;
            int LowDate;
            int intExpStart;
            var strPeriodCheck = "";
            var rs2 = new clsDRWrapper();
            HighDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
            if (HighDate == 1)
                HighDate = 12;
            else
                HighDate -= 1;
            LowDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
            if (LowDate > HighDate)
                strPeriodCheck = "OR";
            else
                strPeriodCheck = "AND";
            rs2.OpenRecordset("SELECT SUM(Amount) AS BudAdj FROM JournalEntries WHERE Account = '" + strAcct + "' AND RCB = 'B' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
            // TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
            if (!(FCConvert.ToString(rs2.Get_Fields("BudAdj")) == ""))
                // TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
                GetBudgetAdjustments = FCConvert.ToDouble(rs2.Get_Fields("BudAdj")) * -1;
            else
                GetBudgetAdjustments = 0;

            return GetBudgetAdjustments;
        }

        private double GetYTDCredit(ref string strAcct, string strPath = "")
        {
            double GetYTDCredit = 0;
            var HighDate = 0;
            int LowDate;
            var strPeriodCheck = "";
            var rs2 = new clsDRWrapper();
            var strTable = "";
            if (strPath != "") rs2.GroupName = strPath;

            if (modBudgetaryMaster.Statics.FirstMonth == 1)
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(12));
            else
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(modBudgetaryMaster.Statics.FirstMonth - 1));
            LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
            if (LowDate > HighDate)
                strPeriodCheck = "OR";
            else
                strPeriodCheck = "AND";

            //FC:FINAL:BBE:#i750 - set missing table name in sql statement (same as in cJournalService)
            if (Strings.Left(strAcct, 1) == "E")
                strTable = "ExpenseReportInfo";
            // CalculateAccountInfo True, True, True, "E", strPath
            else if (Strings.Left(strAcct, 1) == "R")
                strTable = "RevenueReportInfo";
            // CalculateAccountInfo True, True, True, "R", strPath
            else if (Strings.Left(strAcct, 1) == "G")
                strTable = "LedgerReportInfo";
            // CalculateAccountInfo True, True, True, "G", strPath
            rs2.OpenRecordset("SELECT SUM(PostedCredits) AS PostedCreditsTotal FROM " + strTable + " WHERE Account = '" + strAcct + "' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account");
            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                GetYTDCredit = Conversion.Val(rs2.Get_Fields("PostedCreditsTotal")) * -1;
            else
                GetYTDCredit = 0;

            return GetYTDCredit;
        }

        private double GetEncumbrance(ref string strAcct, string strPath = "")
        {
            double GetEncumbrance = 0;
            var HighDate = 0;
            int LowDate;
            var strPeriodCheck = "";
            var rs2 = new clsDRWrapper();
            var strTable = "";
            if (strPath != "") rs2.GroupName = strPath;

            var firstMonth = modBudgetaryMaster.Statics.FirstMonth;

            HighDate = modBudgetaryAccounting.HighMonthCalc(firstMonth == 1
                                                                ? modBudgetaryAccounting.MonthCalc(12)
                                                                : modBudgetaryAccounting.MonthCalc(firstMonth - 1));

            LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(firstMonth));

            strPeriodCheck = LowDate > HighDate ? "OR" : "AND";

            var accountFirstCharacter = Strings.Left(strAcct, 1);
            switch (accountFirstCharacter)
            {
                case "E":
                    strTable = "ExpenseReportInfo";
                    // CalculateAccountInfo True, True, True, "E", strPath
                    break;
                case "R":
                    strTable = "RevenueReportInfo";
                    // CalculateAccountInfo True, True, True, "R", strPath
                    break;
                case "G":
                    strTable = "LedgerReportInfo";
                    // CalculateAccountInfo True, True, True, "G", strPath
                    break;
            }
            rs2.OpenRecordset($"SELECT SUM(EncumbActivity) AS EncumbActivityTotal FROM {strTable} WHERE Account = '{strAcct}' AND (Period >= {FCConvert.ToString(LowDate)}{strPeriodCheck} Period <= {FCConvert.ToString(HighDate)}) GROUP BY Account");
            GetEncumbrance = rs2.EndOfFile() != true && rs2.BeginningOfFile() != true
                ? (double)Conversion.Val(rs2.Get_Fields("EncumbActivityTotal"))
                : 0;

            return GetEncumbrance;
        }

        private void LoadSettings()
        {
            var setCont = new cBDSettingsController();
            bdSettings = setCont.GetBDSettings();
        }

        public cBDCommandHandler() : base()
        {
            LoadSettings();
        }
    }
}
