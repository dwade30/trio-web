﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEditJournalDescriptions.
	/// </summary>
	partial class frmEditJournalDescriptions : BaseForm
	{
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditJournalDescriptions));
			this.vsJournals = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 480);
			this.BottomPanel.Size = new System.Drawing.Size(660, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsJournals);
			this.ClientArea.Size = new System.Drawing.Size(660, 420);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(660, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(285, 30);
			this.HeaderText.Text = "Edit Journal Descriptions";
			// 
			// vsJournals
			// 
			this.vsJournals.AllowSelection = false;
			this.vsJournals.AllowUserToResizeColumns = false;
			this.vsJournals.AllowUserToResizeRows = false;
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsJournals.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsJournals.BackColorBkg = System.Drawing.Color.Empty;
			this.vsJournals.BackColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.BackColorSel = System.Drawing.Color.Empty;
			this.vsJournals.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsJournals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsJournals.ColumnHeadersHeight = 30;
			this.vsJournals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsJournals.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsJournals.DragIcon = null;
			this.vsJournals.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsJournals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsJournals.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsJournals.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.FrozenCols = 0;
			this.vsJournals.GridColor = System.Drawing.Color.Empty;
			this.vsJournals.GridColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.Location = new System.Drawing.Point(30, 30);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsJournals.RowHeightMin = 0;
			this.vsJournals.Rows = 1;
			this.vsJournals.ScrollTipText = null;
			this.vsJournals.ShowColumnVisibilityMenu = false;
			this.vsJournals.Size = new System.Drawing.Size(600, 360);
			this.vsJournals.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsJournals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsJournals.TabIndex = 0;
			this.vsJournals.CurrentCellChanged += new System.EventHandler(this.vsJournals_RowColChange);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnProcessSave
			// 
			this.btnProcessSave.AppearanceKey = "acceptButton";
			this.btnProcessSave.Location = new System.Drawing.Point(302, 30);
			this.btnProcessSave.Name = "btnProcessSave";
			this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessSave.Size = new System.Drawing.Size(120, 48);
			this.btnProcessSave.TabIndex = 0;
			this.btnProcessSave.Text = "Save & Exit";
			this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmEditJournalDescriptions
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(660, 588);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEditJournalDescriptions";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Journal Descriptions";
			this.Load += new System.EventHandler(this.frmEditJournalDescriptions_Load);
			this.Activated += new System.EventHandler(this.frmEditJournalDescriptions_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditJournalDescriptions_KeyPress);
			this.Resize += new System.EventHandler(this.frmEditJournalDescriptions_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcessSave;
	}
}
