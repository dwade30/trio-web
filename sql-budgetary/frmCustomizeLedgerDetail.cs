﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeLedgerDetail.
	/// </summary>
	public partial class frmCustomizeLedgerDetail : BaseForm
	{
		public frmCustomizeLedgerDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbLong.SelectedIndex = 0;
			this.cmbLandscape.Text = "Portrait";
			this.cmbDisplay.SelectedIndex = 0;
			this.cmbPendingDetail.SelectedIndex = 2;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeLedgerDetail InstancePtr
		{
			get
			{
				return (frmCustomizeLedgerDetail)Sys.GetInstance(typeof(frmCustomizeLedgerDetail));
			}
		}

		protected frmCustomizeLedgerDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		public bool FromLedger;
		bool blnSavedFormat;

		private void chkAllInclusive_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAllInclusive.CheckState == CheckState.Checked)
			{
				fraPreferences.Enabled = false;
				cmbLong.SelectedIndex = 1;
				cmbLandscape.Text = "Landscape";
				cmbDisplay.SelectedIndex = 0;
			}
			else
			{
				fraPreferences.Enabled = true;
			}
		}

		private void frmCustomizeLedgerDetail_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtDescription.Focus();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnLedgerDetailEdit && !modBudgetaryMaster.Statics.blnLedgerDetailReportEdit)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowAll")))
					{
						chkAllInclusive.CheckState = CheckState.Checked;
						fraPreferences.Enabled = false;
					}
					else
					{
						chkAllInclusive.CheckState = CheckState.Unchecked;
						fraPreferences.Enabled = true;
					}
					cmbDisplay.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Use")) == "P" ? 0 : 1;
					chkShowLiquidatedEncumbranceActivity.CheckState =
                        FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean(
                                "ShowLiquidatedEncumbranceActivity"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
					cmbLandscape.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("PaperWidth")) == "N" ? 0 : 1;
					cmbLong.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("DescriptionLength")) == "L" ? 1 : 0;
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					bool optPendingDetail = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingDetail"));
					bool optPendingSummary = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingSummary"));
					if (!optPendingDetail && !optPendingSummary)
					{
						cmbPendingDetail.SelectedIndex = 2;
					}
					else if (optPendingDetail)
					{
						cmbPendingDetail.SelectedIndex = 0;
					}
					else 
					{
						cmbPendingDetail.SelectedIndex = 1;
					}
				}
			}
		}

		private void frmCustomizeLedgerDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeLedgerDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeLedgerDetail.FillStyle	= 0;
			//frmCustomizeLedgerDetail.ScaleWidth	= 9300;
			//frmCustomizeLedgerDetail.ScaleHeight	= 7320;
			//frmCustomizeLedgerDetail.LinkTopic	= "Form2";
			//frmCustomizeLedgerDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromLedger)
			{
				FromLedger = false;
				frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnLedgerDetailReportEdit)
					{
						modBudgetaryMaster.Statics.blnLedgerDetailReportEdit = false;
						frmLedgerDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmLedgerDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnLedgerDetailReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnLedgerDetailEdit)
			{
				modBudgetaryMaster.Statics.blnLedgerDetailEdit = false;
				frmGetLedgerDetail.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeLedgerDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM LedgerDetailFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("YTDDebitCredit", true);
					rs.Set_Fields("YTDNet", false);
                    rs.Set_Fields("DescriptionLength", cmbLong.SelectedIndex == 1 ? "L" : "S");
                    rs.Set_Fields("ShowLiquidatedEncumbranceActivity",
                        chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
                    rs.Set_Fields("ShowAll", chkAllInclusive.CheckState == CheckState.Checked);
                    rs.Set_Fields("PendingDetail", cmbPendingDetail.SelectedIndex == 0);
					rs.Set_Fields("PendingSummary", cmbPendingDetail.SelectedIndex == 1);
					rs.Set_Fields("Printer", "O");
					rs.Set_Fields("Font", "S");
                    rs.Set_Fields("PaperWidth", cmbLandscape.SelectedIndex == 0 ? "N" : "L");
                    rs.Set_Fields("Use", cmbDisplay.SelectedIndex == 0 ? "P" : "D");
                    rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM LedgerDetailFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("YTDDebitCredit", true);
				rs.Set_Fields("YTDNet", false);
                rs.Set_Fields("DescriptionLength", cmbLong.SelectedIndex == 1 ? "L" : "S");
                rs.Set_Fields("ShowLiquidatedEncumbranceActivity",
                    chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
                rs.Set_Fields("ShowAll", chkAllInclusive.CheckState == CheckState.Checked);
                rs.Set_Fields("PendingDetail", cmbPendingDetail.SelectedIndex == 0);
				rs.Set_Fields("PendingSummary", cmbPendingDetail.SelectedIndex == 1);
				rs.Set_Fields("Printer", "O");
				rs.Set_Fields("Font", "S");
                rs.Set_Fields("PaperWidth", cmbLandscape.SelectedIndex == 0 ? "N" : "L");
                rs.Set_Fields("Use", cmbDisplay.SelectedIndex == 0 ? "P" : "D");
                rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}
	}
}
