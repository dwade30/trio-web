﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCorrectProject.
	/// </summary>
	public partial class frmCorrectProject : BaseForm
	{
		public frmCorrectProject()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: add code from Load
			string strComboList;
			clsDRWrapper rsProjects = new clsDRWrapper();
			vs1.ExtendLastCol = true;
			vs1.Cols = 10;
			NewProjectCol = 9;
			vs1.ColWidth(NewProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.TextMatrix(0, NewProjectCol, "New Project");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, NewProjectCol, true);
			CorrectedCol = 0;
			NumberCol = 1;
			ProjectCol = 5;
			TaxCol = 2;
			DescriptionCol = 3;
			AccountCol = 4;
			AmountCol = 6;
			DiscountCol = 7;
			EncumbranceCol = 8;
			vs1.ColWidth(CorrectedCol, 0);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(TaxCol, 0);
			vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.23));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.22));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(EncumbranceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, 4);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, ProjectCol, "Project");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, DiscountCol, "Disc");
			vs1.TextMatrix(0, EncumbranceCol, "Enc");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(DiscountCol, "#,###.00");
            // set column formats
            vs1.ColFormat(EncumbranceCol, "#,###.00");
            vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            strComboList = "# ; " + "\t" + " |";
			rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
			if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
			{
				do
				{
                    //FC:FINAL:AM:#4227 - no need for "'"
                    //strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
                    strComboList += rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
                    rsProjects.MoveNext();
				}
				while (rsProjects.EndOfFile() != true);
				strComboList = Strings.Left(strComboList, strComboList.Length - 1);
			}
			vs1.ColComboList(NewProjectCol, strComboList);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCorrectProject InstancePtr
		{
			get
			{
				return (frmCorrectProject)Sys.GetInstance(typeof(frmCorrectProject));
			}
		}

		protected frmCorrectProject _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int TempCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		string ErrorString = "";
		bool BadAccountFlag;
		int DescriptionCol;
		int AccountCol;
		int AmountCol;
		int DiscountCol;
		int EncumbranceCol;
		int ProjectCol;
		int TaxCol;
		int CorrectedCol;
		int NumberCol;
		int NewProjectCol;
		public int CorrRecordNumber;
		int VendorNumber;
		bool blnStopSave;

		private void cboPeriod_GotFocus()
		{
			lblExpense.Text = "";
		}

		private void frmCorrectProject_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			string strLabel = "";
			if (modGlobal.FormExist(this))
			{
				return;
			}

			vs1.Enabled = true;
			vs1.Col = NewProjectCol;
			vs1.Focus();
			//Application.DoEvents();
			this.Refresh();
		}

		private void frmCorrectProject_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCorrectProject_Load(object sender, System.EventArgs e)
		{

		}

		private void frmCorrectProject_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(CorrectedCol, 0);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(TaxCol, 0);
			vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.23));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.22));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(EncumbranceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(NewProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			frmGetGJCorrDataEntry.InstancePtr.Show(App.MainForm);
		}

		private void frmCorrectProject_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "vs1")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			int answer;
			bool blnShouldSave;
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			Support.SendKeys("{TAB}", false);
			//Application.DoEvents();
			if (blnStopSave)
			{
				return;
			}
			blnShouldSave = false;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, NewProjectCol) != "")
				{
					blnShouldSave = true;
					modGlobalFunctions.AddCYAEntry_8("BD", "Corrected project on AP Detail Record " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))) + ".  Old Project: " + vs1.TextMatrix(counter, ProjectCol) + "  New Project: " + vs1.TextMatrix(counter, NewProjectCol));
					rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.Edit();
						rs.Set_Fields("Project", Strings.Trim(vs1.TextMatrix(counter, NewProjectCol)));
						rs.Update();
					}
				}
			}
			if (!blnShouldSave)
			{
				MessageBox.Show("You must change at least one project before you may save.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Recalculating Expense Account Summary Information", true);
			frmWait.InstancePtr.Show();

			modBudgetaryAccounting.CalculateAccountInfo();

			frmWait.InstancePtr.prgProgress.Value = 90;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Finalizing Account Information";
            frmWait.InstancePtr.Refresh();
            frmWait.InstancePtr.Unload();
			Close();
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (vs1.Col == NewProjectCol)
			{
				// if we are in the first column
				vs1.Focus();
				vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				if (vs1.Col < DescriptionCol)
				{
					vs1.Col = DescriptionCol;
				}
				vs1.Editable = FCGrid.EditableSettings.flexEDNone;
				vs1.Focus();
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}
	}
}
