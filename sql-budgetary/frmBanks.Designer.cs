﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBankNames.
	/// </summary>
	partial class frmBankNames : BaseForm
	{
		public fecherFoundation.FCGrid vs1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBankNames));
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.vs1 = new fecherFoundation.FCGrid();
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 380);
            this.BottomPanel.Size = new System.Drawing.Size(610, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Size = new System.Drawing.Size(610, 320);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(610, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(218, 30);
            this.HeaderText.Text = "Enter Bank Names";
            // 
            // vs1
            // 
            this.vs1.AllowSelection = false;
            this.vs1.AllowUserToResizeColumns = false;
            this.vs1.AllowUserToResizeRows = false;
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
            this.vs1.BackColorBkg = System.Drawing.Color.Empty;
            this.vs1.BackColorFixed = System.Drawing.Color.Empty;
            this.vs1.BackColorSel = System.Drawing.Color.Empty;
            this.vs1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vs1.Cols = 7;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vs1.ColumnHeadersHeight = 30;
            this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
            this.vs1.DragIcon = null;
            this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
            this.vs1.FrozenCols = 0;
            this.vs1.GridColor = System.Drawing.Color.Empty;
            this.vs1.GridColorFixed = System.Drawing.Color.Empty;
            this.vs1.Location = new System.Drawing.Point(30, 30);
            this.vs1.Name = "vs1";
            this.vs1.OutlineCol = 0;
            this.vs1.ReadOnly = true;
            this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vs1.RowHeightMin = 0;
            this.vs1.Rows = 100;
            this.vs1.ScrollTipText = null;
            this.vs1.ShowColumnVisibilityMenu = false;
            this.vs1.Size = new System.Drawing.Size(550, 260);
            this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 0;
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(230, 30);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(80, 48);
            this.btnProcessSave.TabIndex = 0;
            this.btnProcessSave.Text = "Save";
            this.btnProcessSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmBankNames
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(610, 488);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBankNames";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Bank Names";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmBankNames_Load);
            this.Activated += new System.EventHandler(this.frmBankNames_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBankNames_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBankNames_KeyPress);
            this.Resize += new System.EventHandler(this.frmBankNames_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcessSave;
		private JavaScript javaScript1;
		Wisej.Web.JavaScript.ClientEvent clientEvent1;
        private System.ComponentModel.IContainer components;
    }
}
