﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCashChart.
	/// </summary>
	partial class frmCashChart : BaseForm
	{
		public fecherFoundation.FCFrame fraLineStyle;
		public fecherFoundation.FCTextBox txtColor;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdColor;
		public fecherFoundation.FCComboBox cboPenStyle;
		public FCCommonDialog dlgChart_Save;
		public Wisej.Web.ColorDialog dlgChart;
		public fecherFoundation.FCComboBox cboChartType;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCLabel lblChartType;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCashChart));
			this.fraLineStyle = new fecherFoundation.FCFrame();
			this.txtColor = new fecherFoundation.FCTextBox();
			this.cmdDone = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdColor = new fecherFoundation.FCButton();
			this.cboPenStyle = new fecherFoundation.FCComboBox();
			this.dlgChart_Save = new fecherFoundation.FCCommonDialog();
			this.dlgChart = new Wisej.Web.ColorDialog();
			this.cboChartType = new fecherFoundation.FCComboBox();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.lblChartType = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.gridSeries = new Wisej.Web.DataGridView();
			this.colSeriesName = new Wisej.Web.DataGridViewTextBoxColumn();
			this.colPenStyle = new Wisej.Web.DataGridViewComboBoxColumn();
			this.colLineColor = new Wisej.Web.DataGridViewButtonColumn();
			this.chartPreview = new Wisej.Web.PictureBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLineStyle)).BeginInit();
			this.fraLineStyle.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdColor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridSeries)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chartPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 524);
			this.BottomPanel.Size = new System.Drawing.Size(812, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chartPreview);
			this.ClientArea.Controls.Add(this.gridSeries);
			this.ClientArea.Controls.Add(this.fraLineStyle);
			this.ClientArea.Controls.Add(this.cboChartType);
			this.ClientArea.Controls.Add(this.lblChartType);
			this.ClientArea.Size = new System.Drawing.Size(812, 464);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(812, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(169, 30);
			this.HeaderText.Text = "Cash Charting";
			// 
			// fraLineStyle
			// 
			this.fraLineStyle.Controls.Add(this.txtColor);
			this.fraLineStyle.Controls.Add(this.cmdDone);
			this.fraLineStyle.Controls.Add(this.cmdCancel);
			this.fraLineStyle.Controls.Add(this.cmdColor);
			this.fraLineStyle.Controls.Add(this.cboPenStyle);
			this.fraLineStyle.Location = new System.Drawing.Point(54, 105);
			this.fraLineStyle.Name = "fraLineStyle";
			this.fraLineStyle.Size = new System.Drawing.Size(260, 261);
			this.fraLineStyle.TabIndex = 5;
			this.fraLineStyle.Text = "Change Line Style";
			this.fraLineStyle.Visible = false;
			// 
			// txtColor
			// 
			this.txtColor.AutoSize = false;
			this.txtColor.BackColor = System.Drawing.SystemColors.Window;
			this.txtColor.Location = new System.Drawing.Point(20, 150);
			this.txtColor.Name = "txtColor";
			this.txtColor.Size = new System.Drawing.Size(140, 40);
			this.txtColor.TabIndex = 10;
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "actionButton";
			this.cmdDone.Location = new System.Drawing.Point(20, 210);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(80, 40);
			this.cmdDone.TabIndex = 9;
			this.cmdDone.Text = "OK";
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(120, 210);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(80, 40);
			this.cmdCancel.TabIndex = 8;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdColor
			// 
			this.cmdColor.AppearanceKey = "actionButton";
			this.cmdColor.Location = new System.Drawing.Point(20, 90);
			this.cmdColor.Name = "cmdColor";
			this.cmdColor.Size = new System.Drawing.Size(140, 40);
			this.cmdColor.TabIndex = 7;
			this.cmdColor.Text = "Change Color";
			this.cmdColor.Click += new System.EventHandler(this.cmdColor_Click);
			// 
			// cboPenStyle
			// 
			this.cboPenStyle.AutoSize = false;
			this.cboPenStyle.BackColor = System.Drawing.SystemColors.Window;
			this.cboPenStyle.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPenStyle.FormattingEnabled = true;
			this.cboPenStyle.Items.AddRange(new object[] {
				"Solid",
				"Dashed",
				"Dotted",
				"Dash Dot",
				"Dash Dot Dot",
				"Ditted",
				"Dash Dit",
				"Dash Dit Dit"
			});
			this.cboPenStyle.Location = new System.Drawing.Point(20, 30);
			this.cboPenStyle.Name = "cboPenStyle";
			this.cboPenStyle.Size = new System.Drawing.Size(220, 40);
			this.cboPenStyle.TabIndex = 6;
			// 
			// dlgChart_Save
			// 
			this.dlgChart_Save.Color = System.Drawing.Color.Black;
			this.dlgChart_Save.DefaultExt = null;
			this.dlgChart_Save.FilterIndex = ((short)(0));
			this.dlgChart_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlgChart_Save.FontName = "Microsoft Sans Serif";
			this.dlgChart_Save.FontSize = 8.25F;
			this.dlgChart_Save.ForeColor = System.Drawing.Color.Black;
			this.dlgChart_Save.FromPage = 0;
			this.dlgChart_Save.Location = new System.Drawing.Point(0, 0);
			this.dlgChart_Save.Name = "dlgChart_Save";
			this.dlgChart_Save.PrinterSettings = null;
			this.dlgChart_Save.Size = new System.Drawing.Size(0, 0);
			this.dlgChart_Save.TabIndex = 0;
			this.dlgChart_Save.ToPage = 0;
			// 
			// cboChartType
			// 
			this.cboChartType.AutoSize = false;
			this.cboChartType.BackColor = System.Drawing.SystemColors.Window;
			this.cboChartType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboChartType.Enabled = false;
			this.cboChartType.FormattingEnabled = true;
			this.cboChartType.Location = new System.Drawing.Point(145, 30);
			this.cboChartType.Name = "cboChartType";
			this.cboChartType.Size = new System.Drawing.Size(173, 40);
			this.cboChartType.TabIndex = 2;
			this.cboChartType.SelectedIndexChanged += new System.EventHandler(this.cboChartType_SelectedIndexChanged);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Enabled = false;
			this.cmdPreview.Location = new System.Drawing.Point(324, 30);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(105, 48);
			this.cmdPreview.TabIndex = 1;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// lblChartType
			// 
			this.lblChartType.Location = new System.Drawing.Point(30, 44);
			this.lblChartType.Name = "lblChartType";
			this.lblChartType.Size = new System.Drawing.Size(80, 16);
			this.lblChartType.TabIndex = 4;
			this.lblChartType.Text = "CHART TYPE";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print / Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// gridSeries
			// 
			this.gridSeries.AllowUserToResizeRows = false;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridSeries.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridSeries.ColumnHeadersHeight = 30;
			this.gridSeries.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridSeries.Columns.AddRange(new Wisej.Web.DataGridViewColumn[] {
				this.colSeriesName,
				this.colPenStyle,
				this.colLineColor
			});
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridSeries.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridSeries.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridSeries.Location = new System.Drawing.Point(30, 105);
			this.gridSeries.Name = "gridSeries";
			this.gridSeries.RowHeadersVisible = false;
			this.gridSeries.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridSeries.ShowColumnVisibilityMenu = false;
			this.gridSeries.Size = new System.Drawing.Size(288, 305);
			this.gridSeries.StandardTab = true;
			this.gridSeries.TabIndex = 6;
			this.gridSeries.Visible = false;
			this.gridSeries.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridSeries_CellValueChanged);
			this.gridSeries.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.gridSeries_CellClick);
			// 
			// colSeriesName
			// 
			this.colSeriesName.HeaderText = "Series Name";
			this.colSeriesName.Name = "colSeriesName";
			this.colSeriesName.ReadOnly = true;
			this.colSeriesName.SortMode = Wisej.Web.DataGridViewColumnSortMode.Programmatic;
			// 
			// colPenStyle
			// 
			this.colPenStyle.HeaderText = "Pen Style";
			this.colPenStyle.Items.AddRange(new object[] {
				"None",
				"Solid",
				"Dash",
				"Dot",
				"DashDot",
				"DashDotDot"
			});
			this.colPenStyle.Name = "colPenStyle";
			this.colPenStyle.SortMode = Wisej.Web.DataGridViewColumnSortMode.Programmatic;
			// 
			// colLineColor
			// 
			this.colLineColor.HeaderText = "Color";
			this.colLineColor.Name = "colLineColor";
			this.colLineColor.ReadOnly = true;
			this.colLineColor.SortMode = Wisej.Web.DataGridViewColumnSortMode.Programmatic;
			this.colLineColor.Width = 70;
			// 
			// chartPreview
			// 
			this.chartPreview.Location = new System.Drawing.Point(335, 30);
			this.chartPreview.Name = "chartPreview";
			this.chartPreview.Size = new System.Drawing.Size(457, 380);
			this.chartPreview.SizeMode = Wisej.Web.PictureBoxSizeMode.AutoSize;
			this.chartPreview.TabIndex = 7;
			// 
			// frmCashChart
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(812, 632);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCashChart";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Cash Charting";
			this.Load += new System.EventHandler(this.frmCashChart_Load);
			this.Activated += new System.EventHandler(this.frmCashChart_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCashChart_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLineStyle)).EndInit();
			this.fraLineStyle.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdColor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridSeries)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chartPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private DataGridView gridSeries;
		private DataGridViewTextBoxColumn colSeriesName;
		private DataGridViewComboBoxColumn colPenStyle;
		private DataGridViewButtonColumn colLineColor;
		private PictureBox chartPreview;
	}
}
