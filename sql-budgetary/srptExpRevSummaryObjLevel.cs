﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptExpRevSummaryObjLevel.
	/// </summary>
	public partial class srptExpRevSummaryObjLevel : FCSectionReport
	{
		public static srptExpRevSummaryObjLevel InstancePtr
		{
			get
			{
				return (srptExpRevSummaryObjLevel)Sys.GetInstance(typeof(srptExpRevSummaryObjLevel));
			}
		}

		protected srptExpRevSummaryObjLevel _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsYTDActivity.Dispose();
				rsActivityDetail.Dispose();
				rsCurrentActivity.Dispose();
				rsExpBudgetInfo.Dispose();
				rsObjectInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptExpRevSummaryObjLevel	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strDateRange;
		// reporting range A - All, M - Month Range, S - Single Month
		int intLowDate;
		int intHighDate;
		bool blnIncEnc;
		// should encumbrance money be calculated into YTD amounts
		bool blnIncPending;
		// should pending activity be included in YTD amounts
		clsDRWrapper rsCurrentActivity = new clsDRWrapper();
		// recordset holds money activity within date rnage for selected departments in expenses
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		// recordset holds monthly money activity within date rnage for selected departments in expenses
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		// recordset holds YTD money activity selected departments in expenses
		clsDRWrapper rsExpBudgetInfo = new clsDRWrapper();
		string strPeriodCheck = "";
		// AND or OR depending on if intLowDate > intHighDate
		int intCurrentMonth;
		bool blnFirstRecord;
		// used in fetchdata to let us know not to move forward in the recordset on the first pass
		clsDRWrapper rsObjectInfo = new clsDRWrapper();
		// holds division info for departments we are reporting on
		string CurrentDepartment = "";
		// current department we are reporting on
		string CurrentDivision = "";
		// current division we are reporting on
		string CurrentExpense = "";
		string CurrentObject = "";
		string strParentReport;

		public srptExpRevSummaryObjLevel()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: blnAdd As Variant --> As bool
			bool blnAdd;
			// - "AutoDim"
			int counter;
			blnAdd = true;
			for (counter = 1; counter <= this.Fields.Count; counter++)
			{
				if (this.Fields[counter - 1].Name == "Binder")
				{
					blnAdd = false;
					break;
				}
			}
			if (blnAdd)
			{
				this.Fields.Add("Binder");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsObjectInfo.EndOfFile();
			}
			else
			{
				rsObjectInfo.MoveNext();
				eArgs.EOF = rsObjectInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				CurrentObject = FCConvert.ToString(rsObjectInfo.Get_Fields_String("Object"));
				this.Fields["Binder"].Value = rsObjectInfo.Get_Fields_String("Object");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsObjectTitle = new clsDRWrapper())
            {
                // get division title
                rsObjectTitle.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + CurrentExpense +
                                            "' AND Object = '" + CurrentObject + "'");
                // show information for division
                if (rsObjectTitle.EndOfFile() != true && rsObjectTitle.BeginningOfFile() != true)
                {
                    fldAccount.Text = CurrentObject + "  " + rsObjectTitle.Get_Fields_String("ShortDescription");
                }
                else
                {
                    fldAccount.Text = CurrentObject + "  UNKNOWN";
                }

                fldBudget.Text = Strings.Format(GetNetBudget(), "#,##0.00");
                fldCurrentMonth.Text = Strings.Format(GetCurrentNet(), "#,##0.00");
                fldYTD.Text = Strings.Format(GetYTDNet(), "#,##0.00");
                fldBalance.Text = Strings.Format(GetBalance(), "#,##0.00");
                fldSpent.Text = Strings.Format(GetSpent() * 100, "0.00");
            }
        }

		private void RetrieveInfo()
		{
			int HighDate;
			int LowDate;
			string strPeriodCheckHolder;
			HighDate = intHighDate;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rsYTDActivity.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division, Expense, Object");
			rsExpBudgetInfo.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division, Expense, Object");
			strPeriodCheck = strPeriodCheckHolder;
			rsCurrentActivity.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Expense, Object");
			rsActivityDetail.OpenRecordset("SELECT Period, Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Expense, Object, Period");
		}

		private decimal GetCurrentDebits()
		{
			decimal GetCurrentDebits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
			}
			else
			{
				GetCurrentDebits = 0;
			}
			return GetCurrentDebits;
		}

		private decimal GetCurrentCredits()
		{
			decimal GetCurrentCredits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
			}
			else
			{
				GetCurrentCredits = 0;
			}
			return GetCurrentCredits;
		}

		private decimal GetCurrentNet()
		{
			decimal GetCurrentNet = 0;
			GetCurrentNet = GetCurrentDebits() + GetCurrentCredits();
			return GetCurrentNet;
		}

		private decimal GetYTDDebit()
		{
			decimal GetYTDDebit = 0;
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
			}
			else
			{
				GetYTDDebit = 0;
			}
			if (blnIncEnc)
			{
				GetYTDDebit += GetEncumbrance();
			}
			if (blnIncPending)
			{
				GetYTDDebit += GetPendingDebits();
			}
			return GetYTDDebit;
		}

		private decimal GetYTDCredit()
		{
			decimal GetYTDCredit = 0;
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
			}
			else
			{
				GetYTDCredit = 0;
			}
			if (blnIncPending)
			{
				GetYTDCredit += GetPendingCredits();
			}
			return GetYTDCredit;
		}

		private decimal GetYTDNet()
		{
			decimal GetYTDNet = 0;
			GetYTDNet = GetYTDDebit() + GetYTDCredit();
			return GetYTDNet;
		}

		private decimal GetEncumbrance()
		{
			decimal GetEncumbrance = 0;
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private decimal GetPending()
		{
			decimal GetPending = 0;
			GetPending = GetPendingDebits() + GetPendingCredits();
			return GetPending;
		}

		private decimal GetBalance()
		{
			decimal GetBalance = 0;
			GetBalance = GetNetBudget() - (GetYTDDebit() + GetYTDCredit());
			return GetBalance;
		}

		private decimal GetSpent()
		{
			decimal GetSpent = 0;
			int temp;
			decimal temp2;
			decimal temp3;
			temp2 = GetBalance();
			temp3 = GetNetBudget();
			if (temp3 == 0)
			{
				GetSpent = 0;
				return GetSpent;
			}
			if (temp2 > temp3)
			{
				GetSpent = (temp3 - temp2) / temp3;
				return GetSpent;
			}
			GetSpent = (temp3 - temp2) / temp3;
			return GetSpent;
		}

		private decimal GetPendingCredits()
		{
			decimal GetPendingCredits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
			}
			else
			{
				GetPendingCredits = 0;
			}
			return GetPendingCredits;
		}

		private decimal GetPendingDebits()
		{
			decimal GetPendingDebits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
			}
			else
			{
				GetPendingDebits = 0;
			}
			return GetPendingDebits;
		}

		private decimal GetOriginalBudget()
		{
			decimal GetOriginalBudget = 0;
			if (rsExpBudgetInfo.EndOfFile() != true && rsExpBudgetInfo.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
					}
					else
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
					}
					else
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private decimal GetBudgetAdjustments()
		{
			decimal GetBudgetAdjustments = 0;
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (CurrentDivision == "")
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private decimal GetNetBudget()
		{
			decimal GetNetBudget = 0;
			GetNetBudget = GetOriginalBudget() - GetBudgetAdjustments();
			return GetNetBudget;
		}

		private string MonthCalc(int X)
		{
			string MonthCalc = "";
			switch (X)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}
		// vbPorter upgrade warning: intLowMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intHighMonth As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strMonthRange, ref bool blnIncludeEncumbrances, ref bool blnIncludePendingInformation, ref string strDept, ref string strExp, string strDiv = "", int intLowMonth = 0, int intHighMonth = 0, string strWhereToSendTotals = "DE")
		{
			// initialize all variables for this report
			strParentReport = strWhereToSendTotals;
			CurrentDepartment = strDept;
			CurrentDivision = strDiv;
			CurrentExpense = strExp;
			strDateRange = strMonthRange;
			blnIncEnc = blnIncludeEncumbrances;
			blnIncPending = blnIncludePendingInformation;
			if (strDateRange == "A")
			{
				intLowDate = modBudgetaryMaster.Statics.FirstMonth;
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					intHighDate = 12;
				}
				else
				{
					intHighDate = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else if (strDateRange == "M")
			{
				intLowDate = intLowMonth;
				intHighDate = intHighMonth;
			}
			else
			{
				intLowDate = intLowMonth;
				intHighDate = intLowMonth;
			}
			if (intLowDate > intHighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			// put information into recordsets
			RetrieveInfo();
			// check to see if there are any departments with activity to report on
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				rsObjectInfo.OpenRecordset("SELECT DISTINCT Object FROM ExpenseReportInfo WHERE Department = '" + CurrentDepartment + "' AND Expense = '" + CurrentExpense + "'");
			}
			else
			{
				rsObjectInfo.OpenRecordset("SELECT DISTINCT Object FROM ExpenseReportInfo WHERE Department = '" + CurrentDepartment + "' AND Division = '" + CurrentDivision + "' AND Expense = '" + CurrentExpense + "'");
			}
			if (rsObjectInfo.EndOfFile() != true && rsObjectInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
				return;
			}
		}

		

		private void srptExpRevSummaryObjLevel_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
