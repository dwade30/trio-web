﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptEncumbranceLiquidation.
	/// </summary>
	partial class rptEncumbranceLiquidation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEncumbranceLiquidation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLiquidated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRemaining = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLiquidated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemaining)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldVendor,
				this.fldReference,
				this.fldLiquidated,
				this.fldRemaining,
				this.fldAccount
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Line4
			});
			this.GroupHeader1.Height = 0.5520833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label6.Text = "Encumbrance Liquidation";
			this.Label6.Top = 0F;
			this.Label6.Width = 7.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Vendor";
			this.Label7.Top = 0.34375F;
			this.Label7.Width = 0.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.53125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Reference";
			this.Label8.Top = 0.34375F;
			this.Label8.Width = 0.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.75F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label9.Text = "Liquidated";
			this.Label9.Top = 0.34375F;
			this.Label9.Width = 1.03125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4.84375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Amt Left";
			this.Label10.Top = 0.34375F;
			this.Label10.Width = 1.0625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label11.Text = "Account";
			this.Label11.Top = 0.34375F;
			this.Label11.Width = 0.5625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.0625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.53125F;
			this.Line4.Width = 7.90625F;
			this.Line4.X1 = 0.0625F;
			this.Line4.X2 = 7.96875F;
			this.Line4.Y1 = 0.53125F;
			this.Line4.Y2 = 0.53125F;
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.1875F;
			this.fldVendor.Left = 0.0625F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldVendor.Text = "Field1";
			this.fldVendor.Top = 0F;
			this.fldVendor.Width = 2.375F;
			// 
			// fldReference
			// 
			this.fldReference.Height = 0.1875F;
			this.fldReference.Left = 2.53125F;
			this.fldReference.Name = "fldReference";
			this.fldReference.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldReference.Text = "Field1";
			this.fldReference.Top = 0F;
			this.fldReference.Width = 1.09375F;
			// 
			// fldLiquidated
			// 
			this.fldLiquidated.Height = 0.1875F;
			this.fldLiquidated.Left = 3.71875F;
			this.fldLiquidated.Name = "fldLiquidated";
			this.fldLiquidated.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldLiquidated.Text = "Field1";
			this.fldLiquidated.Top = 0F;
			this.fldLiquidated.Width = 1.0625F;
			// 
			// fldRemaining
			// 
			this.fldRemaining.Height = 0.1875F;
			this.fldRemaining.Left = 4.84375F;
			this.fldRemaining.Name = "fldRemaining";
			this.fldRemaining.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldRemaining.Text = "Field1";
			this.fldRemaining.Top = 0F;
			this.fldRemaining.Width = 1.0625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 6F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.9375F;
			// 
			// rptEncumbranceLiquidation
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.989583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLiquidated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemaining)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLiquidated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemaining;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
