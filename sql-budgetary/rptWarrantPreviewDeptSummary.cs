﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrantPreviewDeptSummary.
	/// </summary>
	public partial class rptWarrantPreviewDeptSummary : BaseSectionReport
	{
		public static rptWarrantPreviewDeptSummary InstancePtr
		{
			get
			{
				return (rptWarrantPreviewDeptSummary)Sys.GetInstance(typeof(rptWarrantPreviewDeptSummary));
			}
		}

		protected rptWarrantPreviewDeptSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrantPreviewDeptSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		// vbPorter upgrade warning: blnFirstRecord As short --> As int	OnWrite(bool)
		int blnFirstRecord;
		bool blnShowDivGroup;
		string strSQL = "";
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnShowingGLAccounts;
		// vbPorter upgrade warning: curDivisionTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDivisionTotal;
		// vbPorter upgrade warning: curDepartmentTotal As Decimal	OnWrite(Decimal, short)
		Decimal curDepartmentTotal;
		Decimal curFinalTotal;
		bool blnNewDept;
		bool blnNewDiv;
        private WarrantPreviewReportOptions reportOptions;

		public rptWarrantPreviewDeptSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptWarrantPreviewDeptSummary(WarrantPreviewReportOptions options) : this()
        {
            reportOptions = options;
        }

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Department Summary";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("DepartmentBinder");
			this.Fields.Add("DivisionBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (FCConvert.ToBoolean(blnFirstRecord))
			{
				blnFirstRecord = (false ? -1 : 0);
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					if (blnShowingGLAccounts == false)
					{
						blnShowingGLAccounts = true;
						rsInfo.OpenRecordset("SELECT APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournalDetail.Account as Account, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE " + strSQL + " AND Left(APJournalDetail.Account, 1) = 'G' ORDER BY APJournalDetail.Account");
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							eArgs.EOF = false;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				if (blnShowingGLAccounts)
				{
					if (FCConvert.ToString(this.Fields["DepartmentBinder"].Value) == "G/L Accounts")
					{
						blnNewDept = false;
					}
					else
					{
						blnNewDept = true;
					}
					this.Fields["DepartmentBinder"].Value = "G/L Accounts";
				}
				else
				{
					if (this.Fields["DepartmentBinder"].Value == rsInfo.Get_Fields_String("Dept"))
					{
						blnNewDept = false;
					}
					else
					{
						blnNewDept = true;
					}
					this.Fields["DepartmentBinder"].Value = rsInfo.Get_Fields_String("Dept");
					if (blnShowDivGroup)
					{
						if (this.Fields["DivisionBinder"].Value == rsInfo.Get_Fields_String("Div"))
						{
							blnNewDiv = false;
						}
						else
						{
							blnNewDiv = true;
						}
						this.Fields["DivisionBinder"].Value = rsInfo.Get_Fields_String("Div");
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnShowingGLAccounts = false;
			blnFirstRecord = (true ? -1 : 0);
			lblPayDate.Text = "Pay Date: " + reportOptions.PayDate.ToString("MM/dd/yyyy");
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				blnShowDivGroup = false;
				fldDivision.Visible = blnShowDivGroup;
				Line3.Visible = blnShowDivGroup;
				lblDivisionTotal.Visible = blnShowDivGroup;
				fldDivisionTotal.Visible = blnShowDivGroup;
			}
			else
			{
				blnShowDivGroup = true;
				fldDivision.Visible = blnShowDivGroup;
				Line3.Visible = blnShowDivGroup;
				lblDivisionTotal.Visible = blnShowDivGroup;
				fldDivisionTotal.Visible = blnShowDivGroup;
			}
			strSQL = "(APJournal.Status = 'E' or APJournal.Status = 'V') AND APJournal.JournalNumber IN (";
            foreach (var journalNumber in reportOptions.SelectedJournals)
            {
				strSQL += journalNumber + ", ";
			}

			strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ") AND APJournal.Payable <= '" + reportOptions.PayDate + "' ";
			if (blnShowDivGroup && !modAccountTitle.Statics.RevDivFlag)
			{
				rsInfo.OpenRecordset("SELECT APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournalDetail.Account as Account, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, substring(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2))) + ") AS Dept, substring(APJournalDetail.Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2))) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + ") AS Div FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE " + strSQL + " AND Left(APJournalDetail.Account, 1) <> 'G' AND Left(APJournalDetail.Account, 1) <> 'P' AND Left(APJournalDetail.Account, 1) <> 'V' AND Left(APJournalDetail.Account, 1) <> 'L' ORDER BY substring(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2))) + "), substring(APJournalDetail.Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2))) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + ")");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournalDetail.Account as Account, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, substring(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2))) + ") AS Dept, " + modBudgetaryAccounting.Statics.strZeroDiv + " as Div FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE " + strSQL + " AND Left(APJournalDetail.Account, 1) <> 'G' AND Left(APJournalDetail.Account, 1) <> 'P' AND Left(APJournalDetail.Account, 1) <> 'V' AND Left(APJournalDetail.Account, 1) <> 'L' ORDER BY substring(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2))) + ")");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				blnShowingGLAccounts = true;
				rsInfo.OpenRecordset("SELECT APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournalDetail.Account as Account, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE " + strSQL + " AND Left(APJournalDetail.Account, 1) = 'G' ORDER BY APJournalDetail.Account");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					this.Cancel();
					return;
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsVendorInfo = new clsDRWrapper())
            {
                if (rsInfo.Get_Fields_Int32("VendorNumber") != 0)
                {
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                               rsInfo.Get_Fields_Int32("VendorNumber"));
                    if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
                    {
                        fldVendor.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("VendorNumber"), 5) +
                                         " - " + rsVendorInfo.Get_Fields_String("CheckName");
                    }
                    else
                    {
                        fldVendor.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("VendorNumber"), 5) +
                                         " - UNKNOWN";
                    }
                }
                else
                {
                    if (FCConvert.ToString(rsInfo.Get_Fields_String("TempVendorName")) != "")
                    {
                        fldVendor.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("VendorNumber"), 5) +
                                         " - " + rsInfo.Get_Fields_String("TempVendorName");
                    }
                    else
                    {
                        fldVendor.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("VendorNumber"), 5) +
                                         " - UNKNOWN";
                    }
                }

                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                fldAmount.Text =
                    Strings.Format(rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount"), "#,##0.00");
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                fldAccount.Text = FCConvert.ToString(rsInfo.Get_Fields("Account")) + "  " +
                                  modAccountTitle.ReturnAccountDescription(
                                      FCConvert.ToString(rsInfo.Get_Fields("Account")));
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                curDivisionTotal += rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount");
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                curDepartmentTotal += rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount");
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                curFinalTotal += rsInfo.Get_Fields("Amount") - rsInfo.Get_Fields("Discount");
            }
        }

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (blnShowingGLAccounts)
			{
				lblDepartmentTotal.Text = "G/L Account Total";
			}
			fldDepartmentTotal.Text = Strings.Format(curDepartmentTotal, "#,##0.00");
			curDepartmentTotal = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldDivisionTotal.Text = Strings.Format(curDivisionTotal, "#,##0.00");
			curDivisionTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (blnNewDept)
			{
				fldDepartment.Text = DepartmentBinder.Text + " " + modAccountTitle.GetDepartmentName(DepartmentBinder.Text);
			}
			else
			{
				fldDepartment.Text = DepartmentBinder.Text + " " + modAccountTitle.GetDepartmentName(DepartmentBinder.Text) + " CONT'D";
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			if (blnShowDivGroup)
			{
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					if (blnNewDiv)
					{
						fldDivision.Text = DepartmentBinder.Text + "-" + DivisionBinder.Text + " " + modAccountTitle.GetDepartmentName(DepartmentBinder.Text) + " / " + modAccountTitle.GetExpDivisionName(DepartmentBinder.Text, DivisionBinder.Text);
					}
					else
					{
						fldDivision.Text = DepartmentBinder.Text + "-" + DivisionBinder.Text + " " + modAccountTitle.GetDepartmentName(DepartmentBinder.Text) + " / " + modAccountTitle.GetExpDivisionName(DepartmentBinder.Text, DivisionBinder.Text) + " CONT'D";
					}
				}
				else
				{
					if (blnNewDiv)
					{
						fldDivision.Text = DepartmentBinder.Text + "-" + DivisionBinder.Text + " " + modAccountTitle.GetDepartmentName(DepartmentBinder.Text) + " / NO DIVISION";
					}
					else
					{
						fldDivision.Text = DepartmentBinder.Text + "-" + DivisionBinder.Text + " " + modAccountTitle.GetDepartmentName(DepartmentBinder.Text) + " / NO DIVISION CONT'D";
					}
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		

		private void rptWarrantPreviewDeptSummary_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
