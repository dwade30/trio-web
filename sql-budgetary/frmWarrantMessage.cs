﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrantMessage.
	/// </summary>
	public partial class frmWarrantMessage : BaseForm
	{
		public frmWarrantMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.rtfWarrant = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_9, 9);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_8, 8);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_7, 7);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_6, 6);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_5, 5);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_4, 4);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_3, 3);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_2, 2);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_1, 1);
			this.rtfWarrant.AddControlArrayElement(rtfWarrant_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWarrantMessage InstancePtr
		{
			get
			{
				return (frmWarrantMessage)Sys.GetInstance(typeof(frmWarrantMessage));
			}
		}

		protected frmWarrantMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/17/2002
		// This form will be used to input a warrant message that
		// will be printed on all the towns warrants
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnOverType;
		bool blnUnload;
		bool blnDirty;

		private void frmWarrantMessage_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM WarrantMessage");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				for (counter = 0; counter <= 9; counter++)
				{
					rtfWarrant[FCConvert.ToInt16(counter)].Text = Strings.Mid(FCConvert.ToString(rsInfo.Get_Fields_String("Message")), (counter * 80) + 1, 80);
				}
			}
			else
			{
				for (counter = 0; counter <= 9; counter++)
				{
					rtfWarrant[FCConvert.ToInt16(counter)].Text = Strings.StrDup(80, " ");
				}
			}
			blnOverType = true;
			blnDirty = false;
			rtfWarrant[0].Focus();
			rtfWarrant[0].SelectionStart = 0;
			FCUtils.ApplicationUpdate(rtfWarrant[0]);
			this.Refresh();
		}

		private void frmWarrantMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmWarrantMessage.FillStyle	= 0;
			//frmWarrantMessage.ScaleWidth	= 9045;
			//frmWarrantMessage.ScaleHeight	= 6930;
			//frmWarrantMessage.LinkTopic	= "Form2";
			//frmWarrantMessage.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int lngGap;
			int counter;
			blnDirty = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:BBE - Redesign
			//lblInstructions.Left = FCConvert.ToInt32((this.WidthOriginal - lblInstructions.WidthOriginal) / 2.0);
			//for (counter = 0; counter <= 9; counter++)
			//{
			//    rtfWarrant[FCConvert.ToInt16(counter)].Left = FCConvert.ToInt32((this.WidthOriginal - rtfWarrant[FCConvert.ToInt16(counter)].WidthOriginal) / 2.0);
			//}
			//shpBorder.Left = rtfWarrant[0].Left - 30;
			for (counter = 0; counter <= 9; counter++)
			{
				Wisej.Core.DynamicObject rtfWarrantProperties = new Wisej.Core.DynamicObject();
				rtfWarrantProperties["blnOverType"] = true;
				customProperties1.SetCustomPropertiesValue(rtfWarrant[FCConvert.ToInt16(counter)], rtfWarrantProperties);
			}
		}

		private void frmWarrantMessage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuProcessQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("If you exit this form now your changes will be lost.  Do you wish to exit this form?", "Exit Form?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					//MDIParent.InstancePtr.GRID.Focus();
				}
				else
				{
					e.Cancel = true;
				}
			}
			else
			{
				//MDIParent.InstancePtr.GRID.Focus();
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			//mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void rtfWarrant_TextChanged(int Index, object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void rtfWarrant_TextChanged(object sender, System.EventArgs e)
		{
			int index = rtfWarrant.IndexOf((FCTextBox)sender);
			rtfWarrant_TextChanged(index, sender, e);
		}

		private void rtfWarrant_Enter(int Index, object sender, System.EventArgs e)
		{
			if (blnOverType)
			{
				//FC:FINAL:AM:#746 - restore the SelectionStart
				int temp = rtfWarrant[Index].SelectionStart;
				rtfWarrant[Index].SelectionLength = 1;
				rtfWarrant[Index].SelectionStart = temp;
			}
		}

		private void rtfWarrant_Enter(object sender, System.EventArgs e)
		{
			return;
			int index = rtfWarrant.IndexOf((FCTextBox)sender);
			rtfWarrant_Enter(index, sender, e);
		}

		private void rtfWarrant_KeyDown(int Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int temp = 0;
			if (KeyCode == Keys.Left)
			{
				if (blnOverType)
				{
					KeyCode = (Keys)0;
					if (rtfWarrant[Index].SelectionStart > 0)
					{
						rtfWarrant[Index].SelectionStart = rtfWarrant[Index].SelectionStart - 1;
					}
					else if (Index > 0)
					{
						rtfWarrant[FCConvert.ToInt16(Index - 1)].Focus();
						rtfWarrant[FCConvert.ToInt16(Index - 1)].SelectionStart = 79;
					}
					rtfWarrant[Index].SelectionLength = 1;
				}
				else
				{
					if (rtfWarrant[Index].SelectionStart == 0 && Index > 0)
					{
						rtfWarrant[FCConvert.ToInt16(Index - 1)].Focus();
						rtfWarrant[FCConvert.ToInt16(Index - 1)].SelectionStart = 80;
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (blnOverType)
				{
					KeyCode = (Keys)0;
					if (rtfWarrant[Index].SelectionStart < rtfWarrant[Index].Text.Length)
					{
						rtfWarrant[Index].SelectionStart = rtfWarrant[Index].SelectionStart + 1;
					}
					else
					{
						if (Index < 9)
						{
							rtfWarrant[FCConvert.ToInt16(Index + 1)].Focus();
							rtfWarrant[FCConvert.ToInt16(Index + 1)].SelectionStart = 0;
						}
					}
					rtfWarrant[Index].SelectionLength = 1;
				}
				else
				{
					if (rtfWarrant[Index].SelectionStart == rtfWarrant[Index].Text.Length && Index < 9)
					{
						rtfWarrant[FCConvert.ToInt16(Index + 1)].Focus();
						rtfWarrant[FCConvert.ToInt16(Index + 1)].SelectionStart = 0;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = (Keys)0;
				if (Index > 0)
				{
					temp = rtfWarrant[Index].SelectionStart;
					rtfWarrant[FCConvert.ToInt16(Index - 1)].Focus();
					rtfWarrant[FCConvert.ToInt16(Index - 1)].SelectionStart = temp;
					if (blnOverType)
						rtfWarrant[FCConvert.ToInt16(Index - 1)].SelectionLength = 1;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (Index < 9)
				{
					temp = rtfWarrant[Index].SelectionStart;
					rtfWarrant[FCConvert.ToInt16(Index + 1)].Focus();
					rtfWarrant[FCConvert.ToInt16(Index + 1)].SelectionStart = temp;
					if (blnOverType)
						rtfWarrant[FCConvert.ToInt16(Index + 1)].SelectionLength = 1;
				}
			}
			else if (KeyCode == Keys.Delete)
			{
				KeyCode = (Keys)0;
				temp = rtfWarrant[Index].SelectionStart;
				if (rtfWarrant[Index].SelectedText == "")
				{
					rtfWarrant[Index].SelectionLength = 1;
					rtfWarrant[Index].SelectedText = "";
				}
				else
				{
					rtfWarrant[Index].SelectedText = "";
				}
				if (rtfWarrant[Index].Text.Length < 80)
				{
					rtfWarrant[Index].Text = rtfWarrant[Index].Text + Strings.StrDup(80 - rtfWarrant[Index].Text.Length, " ");
				}
				rtfWarrant[Index].SelectionStart = temp;
				if (rtfWarrant[Index].SelectionStart < rtfWarrant[Index].Text.Length && blnOverType)
				{
					rtfWarrant[Index].SelectionLength = 1;
				}
			}
			else if (KeyCode == Keys.Insert)
			{
				KeyCode = (Keys)0;
				blnOverType = !blnOverType;
				if (blnOverType)
				{
					rtfWarrant[Index].SelectionLength = 1;
				}
				else
				{
					rtfWarrant[Index].SelectionLength = 0;
				}
			}
		}

		private void rtfWarrant_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			return;
			int index = rtfWarrant.IndexOf((FCTextBox)sender);
			rtfWarrant_KeyDown(index, sender, e);
		}

		private void rtfWarrant_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int sel = 0;
			if (blnOverType)
			{
				rtfWarrant[Index].SelectionLength = 1;
			}
			if (KeyAscii == Keys.Back)
			{
				if (blnOverType)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{LEFT}", false);
				}
				if (rtfWarrant[Index].Text.Length < 80)
				{
					rtfWarrant[Index].Text = rtfWarrant[Index].Text + Strings.StrDup(80 - rtfWarrant[Index].Text.Length, " ");
				}
			}
			else if (KeyAscii == Keys.Tab)
			{
				if (blnOverType)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{RIGHT}", false);
					Support.SendKeys("{RIGHT}", false);
					Support.SendKeys("{RIGHT}", false);
					Support.SendKeys("{RIGHT}", false);
					Support.SendKeys("{RIGHT}", false);
				}
				if (rtfWarrant[Index].Text.Length < 80)
				{
					rtfWarrant[Index].Text = rtfWarrant[Index].Text + Strings.StrDup(80 - rtfWarrant[Index].Text.Length, " ");
				}
			}
			if (rtfWarrant[Index].SelectionStart < rtfWarrant[Index].Text.Length)
			{
				if (!blnOverType)
				{
					if (KeyAscii >= Keys.Space && KeyAscii <= Keys.F15)
					{
						if (Strings.Right(rtfWarrant[Index].Text, 1) != " ")
						{
							ans = MessageBox.Show("You only have so much space for your warrant message.  By inserting this text you will be deleteing text as the bottom of your warrant message.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
							if (ans == DialogResult.Yes)
							{
								sel = rtfWarrant[Index].SelectionStart;
								rtfWarrant[Index].Text = Strings.Left(rtfWarrant[Index].Text, 79);
								rtfWarrant[Index].SelectionStart = sel;
							}
							else
							{
								KeyAscii = (Keys)0;
							}
						}
						else
						{
							sel = rtfWarrant[Index].SelectionStart;
							rtfWarrant[Index].Text = Strings.Left(rtfWarrant[Index].Text, 79);
							rtfWarrant[Index].SelectionStart = sel;
						}
					}
				}
			}
			else
			{
				if (Index < 9)
				{
					rtfWarrant[FCConvert.ToInt16(Index + 1)].Focus();
					rtfWarrant[FCConvert.ToInt16(Index + 1)].SelectionStart = 0;
					if (blnOverType)
					{
						rtfWarrant[FCConvert.ToInt16(Index + 1)].SelectionLength = 1;
					}
					Support.SendKeys(FCConvert.ToString(Convert.ToChar(KeyAscii)), false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void rtfWarrant_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			return;
			int index = rtfWarrant.IndexOf((FCTextBox)sender);
			rtfWarrant_KeyPress(index, sender, e);
		}

		private void SaveInfo()
		{
			string strMessage;
			int counter;
			strMessage = "";
			for (counter = 0; counter <= 9; counter++)
			{
				strMessage += rtfWarrant[FCConvert.ToInt16(counter)].Text + Strings.StrDup(80 - rtfWarrant[FCConvert.ToInt16(counter)].Text.Length, " ");
			}
			rsInfo.OpenRecordset("SELECT * FROM WarrantMessage");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsInfo.Edit();
				rsInfo.Set_Fields("Message", strMessage);
				rsInfo.Update();
			}
			else
			{
				rsInfo.AddNew();
				rsInfo.Set_Fields("Message", strMessage);
				rsInfo.Update();
			}
			blnDirty = false;
			if (blnUnload)
			{
				Close();
			}
			else
			{
				MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void rtfWarrant_KeyUp(int Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (blnOverType)
			{
				if (rtfWarrant[Index].SelectionLength == 0)
				{
					rtfWarrant[Index].SelectionLength = 1;
				}
			}
		}

		private void rtfWarrant_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			return;
			int index = rtfWarrant.IndexOf((FCTextBox)sender);
			rtfWarrant_KeyUp(index, sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
	}
}
