﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedAPI.PropertyTax.RealProperty
{
    public interface IRealProperty: ISearchResult
    {
        int Account { get; set; }
        string ParcelIdentifier { get; set; }
        int StreetNumber { get; set; }
        string StreetName { get; set; }
        string Name { get; set; }
        Guid AccountIdentifier { get; set; }
    }
}
