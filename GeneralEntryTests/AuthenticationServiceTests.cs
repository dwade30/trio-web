using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess.AccountsReceivable;
using SharedDataAccess.SystemSettings;
using TWGNENTY;
using TWGNENTY.Authentication;
using Xunit;

namespace GeneralEntryTests
{
	public class AuthenticationServiceTests
	{
		[Fact]
		public void GetAuthenticatedUserLockedOutUser()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = ""
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);
			var result = authenticationService.GetAuthenticatedUser("TST", "PASS");

			Assert.True(result.result == AuthenticationResult.LockedOut);
			Assert.True(result.security == null);
		}

		[Fact]
		public void GetAuthenticatedUserNoPassword()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = ""
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);
			var result = authenticationService.GetAuthenticatedUser("TST", "");

			Assert.True(result.result == AuthenticationResult.InvalidCredentials);
			Assert.True(result.security == null);
		}

		[Fact]
		public void GetAuthenticatedUserNoUser()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = ""
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);
			var result = authenticationService.GetAuthenticatedUser("", "PASS");

			Assert.True(result.result == AuthenticationResult.InvalidCredentials);
			Assert.True(result.security == null);
		}

		[Fact]
		public void GetAuthenticatedUserUserDoesNotExist()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = ""
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);
			var result = authenticationService.GetAuthenticatedUser("NO", "PASS");

			Assert.True(result.result == AuthenticationResult.InvalidCredentials);
			Assert.True(result.security == null);
		}

		[Fact]
		public void GetAuthenticatedUserInactiveUser()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				Inactive = true,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = ""
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);
			var result = authenticationService.GetAuthenticatedUser("TST", "PASS");

			Assert.True(result.result == AuthenticationResult.InactiveUser);
			Assert.True(result.security == null);
		}

		[Fact]
		public void GetAuthenticatedUserSuccess()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			
			string strCalcHash = "";

			CryptoUtility cryptoUtility = new CryptoUtility();

			strCalcHash = cryptoUtility.ComputeSHA512Hash("PASS", "");

			User user = new User()
			{
				Id = 1,
				UserID = "TST",
				Inactive = false,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = "",
				Password = strCalcHash
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);

			using (var systemSettingsContext = new SystemSettingsContext(GetDBOptions<SystemSettingsContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;

				systemSettingsContext.UserPermissions.Add(new UserPermission
				{
					UserID = user.Id,
					ModuleName = "GN",
					FunctionID = 1,
					Permission = "F"
				});

				systemSettingsContext.SaveChanges();

				var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
				var result = authenticationService.GetAuthenticatedUser("TST", "PASS");


				Assert.True(result.result == AuthenticationResult.Success);
				Assert.True(result.security == user);
			}
		}

		[Fact]
		public void GetAuthenticatedUserSuccessUnlockedUser()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			
			string strCalcHash = "";

			CryptoUtility cryptoUtility = new CryptoUtility();

			strCalcHash = cryptoUtility.ComputeSHA512Hash("PASS", "");

			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now.AddMinutes(-10),
				UserName = "TEST",
				Salt = "",
				Password = strCalcHash
			};

			userDataAccess.GetUserByUserId("TST").Returns(user);

			using (var systemSettingsContext = new SystemSettingsContext(GetDBOptions<SystemSettingsContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;

				systemSettingsContext.UserPermissions.Add(new UserPermission
				{
					UserID = user.Id,
					ModuleName = "GN",
					FunctionID = 1,
					Permission = "F"
				});

				systemSettingsContext.SaveChanges();

				var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
				var result = authenticationService.GetAuthenticatedUser("TST", "PASS");

				Assert.True(result.result == AuthenticationResult.Success);
				Assert.True(result.security == user);
			}
		}

		[Fact]
		public void DaysUntilPasswordExpiredNullUser()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				UserName = "TEST",
				Salt = ""
			};

			var result = authenticationService.DaysUntilPasswordExpired(null);

			Assert.True(result == 0);
		}

		[Fact]
		public void DaysUntilPasswordExpiredNullDateChanged()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				DateChanged = null,
				UserName = "TEST",
				Salt = ""
			};

			var result = authenticationService.DaysUntilPasswordExpired(user);

			Assert.True(result == 0);
		}

		[Fact]
		public void DaysUntilPasswordExpiredFrequencyZero()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				DateChanged = DateTime.Now,
				Frequency = 0,
				UserName = "TEST",
				Salt = ""
			};

			var result = authenticationService.DaysUntilPasswordExpired(user);

			Assert.True(result == 0);
		}

		[Fact]
		public void DaysUntilPasswordExpiredFrequencyLowerThanDaysPassed()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				DateChanged = DateTime.Now.AddDays(-10),
				Frequency = 1,
				UserName = "TEST",
				Salt = ""
			};

			var result = authenticationService.DaysUntilPasswordExpired(user);

			Assert.True(result == 0);
		}

		[Fact]
		public void DaysUntilPasswordExpiredFrequencyHigherThanDaysPassed()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);
			User user = new User()
			{
				UserID = "TST",
				LockedOut = true,
				LockoutDateTime = DateTime.Now,
				DateChanged = DateTime.Now.AddDays(-10),
				Frequency = 12,
				UserName = "TEST",
				Salt = ""
			};

			var result = authenticationService.DaysUntilPasswordExpired(user);

			Assert.True(result == 2);
		}

		[Fact]
		public void GetPasswordHashAndSaltReturnsCorrectHash()
		{
			var userDataAccess = Substitute.For<IUserDataAccess>();
			var systemSettingsContext = Substitute.For<ISystemSettingsContext>();
			var authenticationService = new AuthenticationService(userDataAccess, systemSettingsContext);

			string strCalcHash = "";

			CryptoUtility cryptoUtility = new CryptoUtility();

			var result = authenticationService.GetPasswordHashAndSalt("PASS");

			strCalcHash = cryptoUtility.ComputeSHA512Hash("PASS", result.salt);

			Assert.True(strCalcHash == result.password);
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
