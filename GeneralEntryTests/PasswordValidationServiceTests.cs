﻿using System;
using System.Collections.Generic;
using System.Text;
using NSubstitute;
using SharedApplication;
using SharedApplication.ClientSettings.Models;
using TWGNENTY;
using TWGNENTY.Authentication;
using Xunit;

namespace GeneralEntryTests
{
	public class PasswordValidationServiceTests
	{
		[Fact]
		public void IsValidPasswordCommonWord()
		{
			var passwordValidationService = new PasswordValidationService();
			
			var result = passwordValidationService.IsValidPassword("password", new List<PasswordHistory>());

			Assert.True(result == PasswordValidationResult.CommonPassword);
		}

		[Fact]
		public void IsValidPasswordPreviousPassword()
		{
			var passwordValidationService = new PasswordValidationService();

			string strCalcHash = "";

			CryptoUtility cryptoUtility = new CryptoUtility();

			strCalcHash = cryptoUtility.ComputeSHA512Hash("triotrio", "");

			var result = passwordValidationService.IsValidPassword("triotrio", new List<PasswordHistory>()
			{
				new PasswordHistory()
				{
					Password = strCalcHash,
					Salt = ""
				}
			});

			Assert.True(result == PasswordValidationResult.PreviousPassword);
		}

		[Fact]
		public void IsValidPasswordPasswordTooShort()
		{
			var passwordValidationService = new PasswordValidationService();

			var result = passwordValidationService.IsValidPassword("qm0!", new List<PasswordHistory>());

			Assert.True(result == PasswordValidationResult.DoesNotMeetMinimumRequirements);
		}

		[Fact]
		public void IsValidPasswordPasswordTooLong()
		{
			var passwordValidationService = new PasswordValidationService();

			var result = passwordValidationService.IsValidPassword("12345678901234567890123456789012345678901234567890123456789012345!", new List<PasswordHistory>());

			Assert.True(result == PasswordValidationResult.DoesNotMeetMinimumRequirements);
		}

		[Fact]
		public void IsValidPasswordGoodPassword()
		{
			var passwordValidationService = new PasswordValidationService();

			string strCalcHash = "";

			CryptoUtility cryptoUtility = new CryptoUtility();

			strCalcHash = cryptoUtility.ComputeSHA512Hash("triotrio", "");

			var result = passwordValidationService.IsValidPassword("triotrio1", new List<PasswordHistory>()
			{
				new PasswordHistory()
				{
					Password = strCalcHash,
					Salt = ""
				}
			});

			Assert.True(result == PasswordValidationResult.GoodPassword);
		}
	}
}
