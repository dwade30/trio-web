﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage4.
	/// </summary>
	partial class srptMVRPage4
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage4));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34f = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34g = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtOpenSpaceA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtOpenSpaceB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtOpenSpaceC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblDateRange1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label153 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label150 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label154 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label156 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label158 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34a1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label160 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label161 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34a2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label162 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label163 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label164 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label165 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label198 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label199 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label200 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label202 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtNewLine35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label211 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label213 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label215 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label216 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label220 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label221 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label222 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label223 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label224 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label225 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt39a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label226 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label227 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt39b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label228 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label230 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt39c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label231 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label233 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34f)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34g)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpenSpaceA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpenSpaceB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpenSpaceC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label153)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label150)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34a1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34a2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label198)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label199)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label200)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label201)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label202)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewLine35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label220)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label221)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label222)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label223)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label224)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label225)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label226)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label227)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label228)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label230)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label231)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label233)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label58,
            this.Shape9,
            this.txtLine34f,
            this.Label38,
            this.Label70,
            this.Label71,
            this.Label72,
            this.Shape10,
            this.txtLine34g,
            this.Label175,
            this.Label124,
            this.Label125,
            this.Label126,
            this.Shape15,
            this.txtOpenSpaceA,
            this.Label127,
            this.Label128,
            this.Shape16,
            this.txtOpenSpaceB,
            this.Label129,
            this.Label130,
            this.Shape17,
            this.txtOpenSpaceC,
            this.Label131,
            this.Label133,
            this.Label176,
            this.Line3,
            this.lblDateRange1,
            this.lblDateRange2,
            this.lblDateRange3,
            this.Label152,
            this.Line4,
            this.Label153,
            this.Label150,
            this.Label154,
            this.Label156,
            this.Label158,
            this.Shape23,
            this.txtLine34a1,
            this.Label159,
            this.Label160,
            this.Label161,
            this.Shape24,
            this.txtLine34a2,
            this.Label162,
            this.Shape25,
            this.txtLine34a,
            this.Label163,
            this.Label164,
            this.Label165,
            this.Shape31,
            this.txtLine34b,
            this.Label168,
            this.Label169,
            this.Label170,
            this.Shape32,
            this.txtLine34c,
            this.Label172,
            this.Label173,
            this.Label174,
            this.Shape33,
            this.txtLine34d,
            this.Label198,
            this.Label199,
            this.Label177,
            this.Shape34,
            this.txtLine34e,
            this.Label178,
            this.Label200,
            this.Label201,
            this.Label202,
            this.Label203,
            this.Label206,
            this.Label207,
            this.Line5,
            this.Label208,
            this.Shape36,
            this.txtNewLine35,
            this.Label209,
            this.lblYear1,
            this.Label211,
            this.Label212,
            this.Shape37,
            this.txtLine36,
            this.Label213,
            this.lblYear2,
            this.Label215,
            this.Label216,
            this.Shape38,
            this.txtLine37,
            this.Label217,
            this.Label219,
            this.Label220,
            this.Shape39,
            this.txtLine38,
            this.Label221,
            this.Label222,
            this.Label223,
            this.Label224,
            this.Label225,
            this.Shape40,
            this.txt39a,
            this.Label226,
            this.lblDateRange4,
            this.Label227,
            this.Shape41,
            this.txt39b,
            this.Label228,
            this.lblDateRange5,
            this.Label230,
            this.Shape42,
            this.txt39c,
            this.Label231,
            this.lblDateRange6,
            this.Label233});
			this.Detail.Height = 9.885417F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.625F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.375F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 1.4375F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.375F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.0625F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 5.375F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 9pt";
			this.Label58.Text = "40f";
			this.Label58.Top = 8.5625F;
			this.Label58.Width = 0.25F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1875F;
			this.Shape9.Left = 5.625F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 8.5625F;
			this.Shape9.Width = 1.3125F;
			// 
			// txtLine34f
			// 
			this.txtLine34f.Height = 0.1875F;
			this.txtLine34f.Left = 5.6875F;
			this.txtLine34f.Name = "txtLine34f";
			this.txtLine34f.Style = "font-family: \'Courier New\'; text-align: right; vertical-align: top";
			this.txtLine34f.Text = null;
			this.txtLine34f.Top = 8.5625F;
			this.txtLine34f.Width = 1.1875F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0.375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label38.Text = "g. Pipes, fixtures, conduits, buildings, pumping stations, and other facilities";
			this.Label38.Top = 8.8125F;
			this.Label38.Width = 4.25F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 0.465F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label70.Text = "of a public municipal corporation used for sewerage disposal if located";
			this.Label70.Top = 9F;
			this.Label70.Width = 4.75F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 0.465F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label71.Text = "outside the limits of the municipality  (§ 651(1)(G))";
			this.Label71.Top = 9.1875F;
			this.Label71.Width = 4.25F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 5.375F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-size: 9pt";
			this.Label72.Text = "40g";
			this.Label72.Top = 9.375F;
			this.Label72.Width = 0.25F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.1875F;
			this.Shape10.Left = 5.625F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 9.375F;
			this.Shape10.Width = 1.3125F;
			// 
			// txtLine34g
			// 
			this.txtLine34g.Height = 0.1875F;
			this.txtLine34g.Left = 5.6875F;
			this.txtLine34g.Name = "txtLine34g";
			this.txtLine34g.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34g.Text = " ";
			this.txtLine34g.Top = 9.375F;
			this.txtLine34g.Width = 1.1875F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-4-";
			this.Label175.Top = 9.625F;
			this.Label175.Width = 1F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1875F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-size: 9pt; text-align: left";
			this.Label124.Text = "34.";
			this.Label124.Top = 0.875F;
			this.Label124.Width = 0.25F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1875F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 0.375F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label125.Text = "Land withdrawn from Open Space Classification (36 M.R.S. § 1112)";
			this.Label125.Top = 0.875F;
			this.Label125.Width = 4.625F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 5.375F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-size: 9pt";
			this.Label126.Text = "34a";
			this.Label126.Top = 1.125F;
			this.Label126.Width = 0.25F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1875F;
			this.Shape15.Left = 5.625F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 1.125F;
			this.Shape15.Width = 1.3125F;
			// 
			// txtOpenSpaceA
			// 
			this.txtOpenSpaceA.Height = 0.1875F;
			this.txtOpenSpaceA.Left = 5.6875F;
			this.txtOpenSpaceA.Name = "txtOpenSpaceA";
			this.txtOpenSpaceA.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOpenSpaceA.Text = " ";
			this.txtOpenSpaceA.Top = 1.125F;
			this.txtOpenSpaceA.Width = 1.1875F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 0.375F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label127.Text = "a. Total number of parcels withdrawn from";
			this.Label127.Top = 1.125F;
			this.Label127.Width = 2.3125F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 5.375F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-size: 9pt";
			this.Label128.Text = "34b";
			this.Label128.Top = 1.375F;
			this.Label128.Width = 0.25F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.1875F;
			this.Shape16.Left = 5.625F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 1.375F;
			this.Shape16.Width = 1.3125F;
			// 
			// txtOpenSpaceB
			// 
			this.txtOpenSpaceB.Height = 0.1875F;
			this.txtOpenSpaceB.Left = 5.6875F;
			this.txtOpenSpaceB.Name = "txtOpenSpaceB";
			this.txtOpenSpaceB.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOpenSpaceB.Text = " ";
			this.txtOpenSpaceB.Top = 1.375F;
			this.txtOpenSpaceB.Width = 1.1875F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1875F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 0.375F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label129.Text = "b. Total number of acres withdrawn from";
			this.Label129.Top = 1.375F;
			this.Label129.Width = 2.25F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1875F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 5.375F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-size: 9pt";
			this.Label130.Text = "34c";
			this.Label130.Top = 1.8125F;
			this.Label130.Width = 0.25F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.1875F;
			this.Shape17.Left = 5.625F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 1.8125F;
			this.Shape17.Width = 1.3125F;
			// 
			// txtOpenSpaceC
			// 
			this.txtOpenSpaceC.Height = 0.1875F;
			this.txtOpenSpaceC.Left = 5.6875F;
			this.txtOpenSpaceC.Name = "txtOpenSpaceC";
			this.txtOpenSpaceC.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOpenSpaceC.Text = " ";
			this.txtOpenSpaceC.Top = 1.8125F;
			this.txtOpenSpaceC.Width = 1.1875F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1875F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 0.375F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label131.Text = "c. Total value of penalties assessed by the municipality due to the withdrawal";
			this.Label131.Top = 1.625F;
			this.Label131.Width = 3.75F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1875F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0.514F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label133.Text = "of classified Open Space land from";
			this.Label133.Top = 1.813F;
			this.Label133.Width = 2.4375F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 0.05199999F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label176.Text = "OPEN SPACE CONTINUED";
			this.Label176.Top = 0.625F;
			this.Label176.Width = 7.292F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.625F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 0.625F;
			this.Line3.Y2 = 0.625F;
			// 
			// lblDateRange1
			// 
			this.lblDateRange1.Height = 0.1875F;
			this.lblDateRange1.HyperLink = null;
			this.lblDateRange1.Left = 2.5F;
			this.lblDateRange1.Name = "lblDateRange1";
			this.lblDateRange1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange1.Text = "04/02/04 to 04/01/05";
			this.lblDateRange1.Top = 1.125F;
			this.lblDateRange1.Width = 2F;
			// 
			// lblDateRange2
			// 
			this.lblDateRange2.Height = 0.1875F;
			this.lblDateRange2.HyperLink = null;
			this.lblDateRange2.Left = 2.375F;
			this.lblDateRange2.Name = "lblDateRange2";
			this.lblDateRange2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange2.Text = "04/02/04 to 04/01/05";
			this.lblDateRange2.Top = 1.375F;
			this.lblDateRange2.Width = 2.0625F;
			// 
			// lblDateRange3
			// 
			this.lblDateRange3.Height = 0.1875F;
			this.lblDateRange3.HyperLink = null;
			this.lblDateRange3.Left = 2.231F;
			this.lblDateRange3.Name = "lblDateRange3";
			this.lblDateRange3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange3.Text = "04/02/04 through 04/01/05";
			this.lblDateRange3.Top = 1.812F;
			this.lblDateRange3.Width = 2.125F;
			// 
			// Label152
			// 
			this.Label152.Height = 0.1875F;
			this.Label152.HyperLink = null;
			this.Label152.Left = 1F;
			this.Label152.Name = "Label152";
			this.Label152.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label152.Text = "EXEMPT PROPERTY";
			this.Label152.Top = 4.625F;
			this.Label152.Width = 5.5F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 4.625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 4.625F;
			this.Line4.Y2 = 4.625F;
			// 
			// Label153
			// 
			this.Label153.Height = 0.1875F;
			this.Label153.HyperLink = null;
			this.Label153.Left = 1F;
			this.Label153.Name = "Label153";
			this.Label153.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold; text-align: " +
    "center";
			this.Label153.Text = "(36 M.R.S. §§ 651, 652, 653, 654-A, 656)";
			this.Label153.Top = 4.8125F;
			this.Label153.Width = 5.5F;
			// 
			// Label150
			// 
			this.Label150.Height = 0.1875F;
			this.Label150.HyperLink = null;
			this.Label150.Left = 0F;
			this.Label150.Name = "Label150";
			this.Label150.Style = "font-size: 9pt; text-align: left";
			this.Label150.Text = "40.";
			this.Label150.Top = 5.0625F;
			this.Label150.Width = 0.25F;
			// 
			// Label154
			// 
			this.Label154.Height = 0.1875F;
			this.Label154.HyperLink = null;
			this.Label154.Left = 0.375F;
			this.Label154.Name = "Label154";
			this.Label154.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label154.Text = "Enter the exempt value of all the following classes of property which are exempt " +
    "from property taxation by law.";
			this.Label154.Top = 5.0625F;
			this.Label154.Width = 5.8125F;
			// 
			// Label156
			// 
			this.Label156.Height = 0.1875F;
			this.Label156.HyperLink = null;
			this.Label156.Left = 0.375F;
			this.Label156.Name = "Label156";
			this.Label156.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label156.Text = "a.Public property  (§ 651(1)(A) and (B))";
			this.Label156.Top = 5.25F;
			this.Label156.Width = 4.5F;
			// 
			// Label158
			// 
			this.Label158.Height = 0.1875F;
			this.Label158.HyperLink = null;
			this.Label158.Left = 5.25F;
			this.Label158.Name = "Label158";
			this.Label158.Style = "font-size: 9pt";
			this.Label158.Text = "40a(1)";
			this.Label158.Top = 5.4375F;
			this.Label158.Width = 0.375F;
			// 
			// Shape23
			// 
			this.Shape23.Height = 0.1875F;
			this.Shape23.Left = 5.625F;
			this.Shape23.Name = "Shape23";
			this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape23.Top = 5.4375F;
			this.Shape23.Width = 1.3125F;
			// 
			// txtLine34a1
			// 
			this.txtLine34a1.Height = 0.1875F;
			this.txtLine34a1.Left = 5.6875F;
			this.txtLine34a1.Name = "txtLine34a1";
			this.txtLine34a1.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34a1.Text = " ";
			this.txtLine34a1.Top = 5.4375F;
			this.txtLine34a1.Width = 1.1875F;
			// 
			// Label159
			// 
			this.Label159.Height = 0.1875F;
			this.Label159.HyperLink = null;
			this.Label159.Left = 0.465F;
			this.Label159.Name = "Label159";
			this.Label159.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label159.Text = "(1) United States";
			this.Label159.Top = 5.4995F;
			this.Label159.Width = 1.6875F;
			// 
			// Label160
			// 
			this.Label160.Height = 0.1875F;
			this.Label160.HyperLink = null;
			this.Label160.Left = 0.465F;
			this.Label160.Name = "Label160";
			this.Label160.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label160.Text = "(2) State of Maine (excluding roads)";
			this.Label160.Top = 5.687F;
			this.Label160.Width = 1.875F;
			// 
			// Label161
			// 
			this.Label161.Height = 0.1875F;
			this.Label161.HyperLink = null;
			this.Label161.Left = 5.25F;
			this.Label161.Name = "Label161";
			this.Label161.Style = "font-size: 9pt";
			this.Label161.Text = "40a(2)";
			this.Label161.Top = 5.6875F;
			this.Label161.Width = 0.375F;
			// 
			// Shape24
			// 
			this.Shape24.Height = 0.1875F;
			this.Shape24.Left = 5.625F;
			this.Shape24.Name = "Shape24";
			this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape24.Top = 5.6875F;
			this.Shape24.Width = 1.3125F;
			// 
			// txtLine34a2
			// 
			this.txtLine34a2.Height = 0.1875F;
			this.txtLine34a2.Left = 5.6875F;
			this.txtLine34a2.Name = "txtLine34a2";
			this.txtLine34a2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34a2.Text = " ";
			this.txtLine34a2.Top = 5.6875F;
			this.txtLine34a2.Width = 1.1875F;
			// 
			// Label162
			// 
			this.Label162.Height = 0.1875F;
			this.Label162.HyperLink = null;
			this.Label162.Left = 5.375F;
			this.Label162.Name = "Label162";
			this.Label162.Style = "font-size: 9pt";
			this.Label162.Text = "40a";
			this.Label162.Top = 5.9375F;
			this.Label162.Width = 0.25F;
			// 
			// Shape25
			// 
			this.Shape25.Height = 0.1875F;
			this.Shape25.Left = 5.625F;
			this.Shape25.Name = "Shape25";
			this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape25.Top = 5.9375F;
			this.Shape25.Width = 1.3125F;
			// 
			// txtLine34a
			// 
			this.txtLine34a.Height = 0.1875F;
			this.txtLine34a.Left = 5.6875F;
			this.txtLine34a.Name = "txtLine34a";
			this.txtLine34a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34a.Text = " ";
			this.txtLine34a.Top = 5.9375F;
			this.txtLine34a.Width = 1.1875F;
			// 
			// Label163
			// 
			this.Label163.Height = 0.1875F;
			this.Label163.HyperLink = null;
			this.Label163.Left = 0.375F;
			this.Label163.Name = "Label163";
			this.Label163.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label163.Text = "b. Real estate owned by the Water Resources Board of the State of";
			this.Label163.Top = 6.1875F;
			this.Label163.Width = 4.5F;
			// 
			// Label164
			// 
			this.Label164.Height = 0.1875F;
			this.Label164.HyperLink = null;
			this.Label164.Left = 0.465F;
			this.Label164.Name = "Label164";
			this.Label164.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label164.Text = "New Hampshire located within this State  (§ 651(1)(B-1))";
			this.Label164.Top = 6.375F;
			this.Label164.Width = 4.5F;
			// 
			// Label165
			// 
			this.Label165.Height = 0.1875F;
			this.Label165.HyperLink = null;
			this.Label165.Left = 5.375F;
			this.Label165.Name = "Label165";
			this.Label165.Style = "font-size: 9pt";
			this.Label165.Text = "40b";
			this.Label165.Top = 6.375F;
			this.Label165.Width = 0.25F;
			// 
			// Shape31
			// 
			this.Shape31.Height = 0.1875F;
			this.Shape31.Left = 5.625F;
			this.Shape31.Name = "Shape31";
			this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape31.Top = 6.375F;
			this.Shape31.Width = 1.3125F;
			// 
			// txtLine34b
			// 
			this.txtLine34b.Height = 0.1875F;
			this.txtLine34b.Left = 5.6875F;
			this.txtLine34b.Name = "txtLine34b";
			this.txtLine34b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34b.Text = " ";
			this.txtLine34b.Top = 6.375F;
			this.txtLine34b.Width = 1.1875F;
			// 
			// Label168
			// 
			this.Label168.Height = 0.1875F;
			this.Label168.HyperLink = null;
			this.Label168.Left = 0.375F;
			this.Label168.Name = "Label168";
			this.Label168.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label168.Text = "c. Property of any public municipal corporation of this state (including county";
			this.Label168.Top = 6.625F;
			this.Label168.Width = 4.5625F;
			// 
			// Label169
			// 
			this.Label169.Height = 0.1875F;
			this.Label169.HyperLink = null;
			this.Label169.Left = 0.465F;
			this.Label169.Name = "Label169";
			this.Label169.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label169.Text = "property) appropriated to public uses  (§ 651(1)(D))";
			this.Label169.Top = 6.8125F;
			this.Label169.Width = 4.5F;
			// 
			// Label170
			// 
			this.Label170.Height = 0.1875F;
			this.Label170.HyperLink = null;
			this.Label170.Left = 5.375F;
			this.Label170.Name = "Label170";
			this.Label170.Style = "font-size: 9pt";
			this.Label170.Text = "40c";
			this.Label170.Top = 7F;
			this.Label170.Width = 0.25F;
			// 
			// Shape32
			// 
			this.Shape32.Height = 0.1875F;
			this.Shape32.Left = 5.625F;
			this.Shape32.Name = "Shape32";
			this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape32.Top = 7F;
			this.Shape32.Width = 1.3125F;
			// 
			// txtLine34c
			// 
			this.txtLine34c.Height = 0.1875F;
			this.txtLine34c.Left = 5.6875F;
			this.txtLine34c.Name = "txtLine34c";
			this.txtLine34c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34c.Text = " ";
			this.txtLine34c.Top = 7F;
			this.txtLine34c.Width = 1.1875F;
			// 
			// Label172
			// 
			this.Label172.Height = 0.1875F;
			this.Label172.HyperLink = null;
			this.Label172.Left = 0.375F;
			this.Label172.Name = "Label172";
			this.Label172.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label172.Text = "d. Pipes, fixtures, hydrants, conduits, gatehouses, pumping stations, reservoirs";
			this.Label172.Top = 7.3125F;
			this.Label172.Width = 4.8125F;
			// 
			// Label173
			// 
			this.Label173.Height = 0.1875F;
			this.Label173.HyperLink = null;
			this.Label173.Left = 0.465F;
			this.Label173.Name = "Label173";
			this.Label173.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label173.Text = "and dams of a public municipal corporation supplying water, power or light";
			this.Label173.Top = 7.5F;
			this.Label173.Width = 4.5F;
			// 
			// Label174
			// 
			this.Label174.Height = 0.1875F;
			this.Label174.HyperLink = null;
			this.Label174.Left = 5.375F;
			this.Label174.Name = "Label174";
			this.Label174.Style = "font-size: 9pt";
			this.Label174.Text = "40d";
			this.Label174.Top = 7.6875F;
			this.Label174.Width = 0.25F;
			// 
			// Shape33
			// 
			this.Shape33.Height = 0.1875F;
			this.Shape33.Left = 5.625F;
			this.Shape33.Name = "Shape33";
			this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape33.Top = 7.6875F;
			this.Shape33.Width = 1.3125F;
			// 
			// txtLine34d
			// 
			this.txtLine34d.Height = 0.1875F;
			this.txtLine34d.Left = 5.6875F;
			this.txtLine34d.Name = "txtLine34d";
			this.txtLine34d.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34d.Text = " ";
			this.txtLine34d.Top = 7.6875F;
			this.txtLine34d.Width = 1.1875F;
			// 
			// Label198
			// 
			this.Label198.Height = 0.1875F;
			this.Label198.HyperLink = null;
			this.Label198.Left = 0.375F;
			this.Label198.Name = "Label198";
			this.Label198.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; vertical-align: top";
			this.Label198.Text = "e. Airport or landing field of a public municipal corporation used for airport or" +
    "";
			this.Label198.Top = 7.9375F;
			this.Label198.Width = 4.8125F;
			// 
			// Label199
			// 
			this.Label199.Height = 0.1875F;
			this.Label199.HyperLink = null;
			this.Label199.Left = 0.465F;
			this.Label199.Name = "Label199";
			this.Label199.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label199.Text = "aeronautical purposes  (§ 651(1)(F))";
			this.Label199.Top = 8.125F;
			this.Label199.Width = 4.5F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1875F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 5.375F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-size: 9pt; vertical-align: top";
			this.Label177.Text = "40e";
			this.Label177.Top = 8.125F;
			this.Label177.Width = 0.25F;
			// 
			// Shape34
			// 
			this.Shape34.Height = 0.1875F;
			this.Shape34.Left = 5.625F;
			this.Shape34.Name = "Shape34";
			this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape34.Top = 8.125F;
			this.Shape34.Width = 1.3125F;
			// 
			// txtLine34e
			// 
			this.txtLine34e.Height = 0.1875F;
			this.txtLine34e.Left = 5.6875F;
			this.txtLine34e.Name = "txtLine34e";
			this.txtLine34e.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34e.Text = " ";
			this.txtLine34e.Top = 8.125F;
			this.txtLine34e.Width = 1.1875F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.1875F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 0.465F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: left";
			this.Label178.Text = "Total value of public property (40a(1) + 40a(2))";
			this.Label178.Top = 5.9375F;
			this.Label178.Width = 2.66F;
			// 
			// Label200
			// 
			this.Label200.Height = 0.1875F;
			this.Label200.HyperLink = null;
			this.Label200.Left = 0.465F;
			this.Label200.Name = "Label200";
			this.Label200.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: normal";
			this.Label200.Text = "(County, Municipal, Quasi-Municipal owned property)";
			this.Label200.Top = 7F;
			this.Label200.Width = 4.5F;
			// 
			// Label201
			// 
			this.Label201.Height = 0.1875F;
			this.Label201.HyperLink = null;
			this.Label201.Left = 0.465F;
			this.Label201.Name = "Label201";
			this.Label201.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label201.Text = "if located outside the limits of the municipality  (§ 651(1)(E))";
			this.Label201.Top = 7.6875F;
			this.Label201.Width = 4.5F;
			// 
			// Label202
			// 
			this.Label202.Height = 0.1875F;
			this.Label202.HyperLink = null;
			this.Label202.Left = 0.375F;
			this.Label202.Name = "Label202";
			this.Label202.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; vertical-align: top";
			this.Label202.Text = "f. Landing area of a privately owned airport when owner grants free use of that";
			this.Label202.Top = 8.375F;
			this.Label202.Width = 4.8125F;
			// 
			// Label203
			// 
			this.Label203.Height = 0.1875F;
			this.Label203.HyperLink = null;
			this.Label203.Left = 0.465F;
			this.Label203.Name = "Label203";
			this.Label203.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label203.Text = "landing area to the public  (§ 656(1)(C))";
			this.Label203.Top = 8.5625F;
			this.Label203.Width = 4.5F;
			// 
			// Label206
			// 
			this.Label206.Height = 0.1875F;
			this.Label206.HyperLink = null;
			this.Label206.Left = 1F;
			this.Label206.Name = "Label206";
			this.Label206.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label206.Text = "LAND CLASSIFIED UNDER THE WORKING WATERFRONT TAX LAW";
			this.Label206.Top = 2.0625F;
			this.Label206.Width = 5.5F;
			// 
			// Label207
			// 
			this.Label207.Height = 0.1875F;
			this.Label207.HyperLink = null;
			this.Label207.Left = 1F;
			this.Label207.Name = "Label207";
			this.Label207.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold; text-align: " +
    "center";
			this.Label207.Text = "(36 M.R.S. §§ 1131 - 1140-B)";
			this.Label207.Top = 2.25F;
			this.Label207.Width = 5.5F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.0625F;
			this.Line5.Width = 7.25F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.25F;
			this.Line5.Y1 = 2.0625F;
			this.Line5.Y2 = 2.0625F;
			// 
			// Label208
			// 
			this.Label208.Height = 0.1875F;
			this.Label208.HyperLink = null;
			this.Label208.Left = 5.375F;
			this.Label208.Name = "Label208";
			this.Label208.Style = "font-size: 9pt";
			this.Label208.Text = "35";
			this.Label208.Top = 2.5F;
			this.Label208.Width = 0.25F;
			// 
			// Shape36
			// 
			this.Shape36.Height = 0.1875F;
			this.Shape36.Left = 5.625F;
			this.Shape36.Name = "Shape36";
			this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape36.Top = 2.5F;
			this.Shape36.Width = 1.3125F;
			// 
			// txtNewLine35
			// 
			this.txtNewLine35.Height = 0.1875F;
			this.txtNewLine35.Left = 5.6875F;
			this.txtNewLine35.Name = "txtNewLine35";
			this.txtNewLine35.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtNewLine35.Text = " ";
			this.txtNewLine35.Top = 2.5F;
			this.txtNewLine35.Width = 1.1875F;
			// 
			// Label209
			// 
			this.Label209.Height = 0.1875F;
			this.Label209.HyperLink = null;
			this.Label209.Left = 0.375F;
			this.Label209.Name = "Label209";
			this.Label209.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label209.Text = "Number of parcels classified as Working Waterfront as of April 1,";
			this.Label209.Top = 2.5F;
			this.Label209.Width = 3.6875F;
			// 
			// lblYear1
			// 
			this.lblYear1.Height = 0.1875F;
			this.lblYear1.HyperLink = null;
			this.lblYear1.Left = 3.625F;
			this.lblYear1.Name = "lblYear1";
			this.lblYear1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear1.Text = "2004";
			this.lblYear1.Top = 2.5F;
			this.lblYear1.Width = 1.1875F;
			// 
			// Label211
			// 
			this.Label211.Height = 0.1875F;
			this.Label211.HyperLink = null;
			this.Label211.Left = 0F;
			this.Label211.Name = "Label211";
			this.Label211.Style = "font-size: 9pt; text-align: left";
			this.Label211.Text = "35.";
			this.Label211.Top = 2.5F;
			this.Label211.Width = 0.25F;
			// 
			// Label212
			// 
			this.Label212.Height = 0.1875F;
			this.Label212.HyperLink = null;
			this.Label212.Left = 5.375F;
			this.Label212.Name = "Label212";
			this.Label212.Style = "font-size: 9pt";
			this.Label212.Text = "36";
			this.Label212.Top = 2.75F;
			this.Label212.Width = 0.25F;
			// 
			// Shape37
			// 
			this.Shape37.Height = 0.1875F;
			this.Shape37.Left = 5.625F;
			this.Shape37.Name = "Shape37";
			this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape37.Top = 2.75F;
			this.Shape37.Width = 1.3125F;
			// 
			// txtLine36
			// 
			this.txtLine36.Height = 0.1875F;
			this.txtLine36.Left = 5.6875F;
			this.txtLine36.Name = "txtLine36";
			this.txtLine36.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine36.Text = " ";
			this.txtLine36.Top = 2.75F;
			this.txtLine36.Width = 1.1875F;
			// 
			// Label213
			// 
			this.Label213.Height = 0.1875F;
			this.Label213.HyperLink = null;
			this.Label213.Left = 0.375F;
			this.Label213.Name = "Label213";
			this.Label213.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label213.Text = "Number of acres first classified as Working Waterfront for tax year";
			this.Label213.Top = 2.75F;
			this.Label213.Width = 3.3125F;
			// 
			// lblYear2
			// 
			this.lblYear2.Height = 0.1875F;
			this.lblYear2.HyperLink = null;
			this.lblYear2.Left = 3.625F;
			this.lblYear2.Name = "lblYear2";
			this.lblYear2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear2.Text = "2004";
			this.lblYear2.Top = 2.75F;
			this.lblYear2.Width = 1.1875F;
			// 
			// Label215
			// 
			this.Label215.Height = 0.1875F;
			this.Label215.HyperLink = null;
			this.Label215.Left = 0F;
			this.Label215.Name = "Label215";
			this.Label215.Style = "font-size: 9pt; text-align: left";
			this.Label215.Text = "36.";
			this.Label215.Top = 2.75F;
			this.Label215.Width = 0.25F;
			// 
			// Label216
			// 
			this.Label216.Height = 0.1875F;
			this.Label216.HyperLink = null;
			this.Label216.Left = 5.375F;
			this.Label216.Name = "Label216";
			this.Label216.Style = "font-size: 9pt";
			this.Label216.Text = "37";
			this.Label216.Top = 3F;
			this.Label216.Width = 0.25F;
			// 
			// Shape38
			// 
			this.Shape38.Height = 0.1875F;
			this.Shape38.Left = 5.625F;
			this.Shape38.Name = "Shape38";
			this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape38.Top = 3F;
			this.Shape38.Width = 1.3125F;
			// 
			// txtLine37
			// 
			this.txtLine37.Height = 0.1875F;
			this.txtLine37.Left = 5.6875F;
			this.txtLine37.Name = "txtLine37";
			this.txtLine37.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine37.Text = " ";
			this.txtLine37.Top = 3F;
			this.txtLine37.Width = 1.1875F;
			// 
			// Label217
			// 
			this.Label217.Height = 0.1875F;
			this.Label217.HyperLink = null;
			this.Label217.Left = 0.375F;
			this.Label217.Name = "Label217";
			this.Label217.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label217.Text = "Total acreage of all land now classified as Working Waterfront";
			this.Label217.Top = 3F;
			this.Label217.Width = 3.4375F;
			// 
			// Label219
			// 
			this.Label219.Height = 0.1875F;
			this.Label219.HyperLink = null;
			this.Label219.Left = 0F;
			this.Label219.Name = "Label219";
			this.Label219.Style = "font-size: 9pt; text-align: left";
			this.Label219.Text = "37.";
			this.Label219.Top = 3F;
			this.Label219.Width = 0.25F;
			// 
			// Label220
			// 
			this.Label220.Height = 0.1875F;
			this.Label220.HyperLink = null;
			this.Label220.Left = 5.375F;
			this.Label220.Name = "Label220";
			this.Label220.Style = "font-size: 9pt";
			this.Label220.Text = "38";
			this.Label220.Top = 3.25F;
			this.Label220.Width = 0.25F;
			// 
			// Shape39
			// 
			this.Shape39.Height = 0.1875F;
			this.Shape39.Left = 5.625F;
			this.Shape39.Name = "Shape39";
			this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape39.Top = 3.25F;
			this.Shape39.Width = 1.3125F;
			// 
			// txtLine38
			// 
			this.txtLine38.Height = 0.1875F;
			this.txtLine38.Left = 5.6875F;
			this.txtLine38.Name = "txtLine38";
			this.txtLine38.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine38.Text = " ";
			this.txtLine38.Top = 3.25F;
			this.txtLine38.Width = 1.1875F;
			// 
			// Label221
			// 
			this.Label221.Height = 0.1875F;
			this.Label221.HyperLink = null;
			this.Label221.Left = 0.375F;
			this.Label221.Name = "Label221";
			this.Label221.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label221.Text = "Total valuation of all land now classified as Working Waterfront";
			this.Label221.Top = 3.25F;
			this.Label221.Width = 3.4375F;
			// 
			// Label222
			// 
			this.Label222.Height = 0.1875F;
			this.Label222.HyperLink = null;
			this.Label222.Left = 0F;
			this.Label222.Name = "Label222";
			this.Label222.Style = "font-size: 9pt; text-align: left";
			this.Label222.Text = "38.";
			this.Label222.Top = 3.25F;
			this.Label222.Width = 0.25F;
			// 
			// Label223
			// 
			this.Label223.Height = 0.1875F;
			this.Label223.HyperLink = null;
			this.Label223.Left = 0.375F;
			this.Label223.Name = "Label223";
			this.Label223.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label223.Text = "Land withdrawn from Working Waterfront classification (36 M.R.S. § 1138)";
			this.Label223.Top = 3.5F;
			this.Label223.Width = 3.8125F;
			// 
			// Label224
			// 
			this.Label224.Height = 0.1875F;
			this.Label224.HyperLink = null;
			this.Label224.Left = 0F;
			this.Label224.Name = "Label224";
			this.Label224.Style = "font-size: 9pt; text-align: left";
			this.Label224.Text = "39.";
			this.Label224.Top = 3.5F;
			this.Label224.Width = 0.25F;
			// 
			// Label225
			// 
			this.Label225.Height = 0.1875F;
			this.Label225.HyperLink = null;
			this.Label225.Left = 5.375F;
			this.Label225.Name = "Label225";
			this.Label225.Style = "font-size: 9pt";
			this.Label225.Text = "39a";
			this.Label225.Top = 3.6875F;
			this.Label225.Width = 0.25F;
			// 
			// Shape40
			// 
			this.Shape40.Height = 0.1875F;
			this.Shape40.Left = 5.625F;
			this.Shape40.Name = "Shape40";
			this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape40.Top = 3.6875F;
			this.Shape40.Width = 1.3125F;
			// 
			// txt39a
			// 
			this.txt39a.Height = 0.1875F;
			this.txt39a.Left = 5.6875F;
			this.txt39a.Name = "txt39a";
			this.txt39a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txt39a.Text = " ";
			this.txt39a.Top = 3.6875F;
			this.txt39a.Width = 1.1875F;
			// 
			// Label226
			// 
			this.Label226.Height = 0.1875F;
			this.Label226.HyperLink = null;
			this.Label226.Left = 0.375F;
			this.Label226.Name = "Label226";
			this.Label226.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label226.Text = "a.  Total number of parcels withdrawn from";
			this.Label226.Top = 3.6875F;
			this.Label226.Width = 2.1875F;
			// 
			// lblDateRange4
			// 
			this.lblDateRange4.Height = 0.1875F;
			this.lblDateRange4.HyperLink = null;
			this.lblDateRange4.Left = 2.5F;
			this.lblDateRange4.Name = "lblDateRange4";
			this.lblDateRange4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange4.Text = "04/02/04 to 04/01/05";
			this.lblDateRange4.Top = 3.6875F;
			this.lblDateRange4.Width = 1.8125F;
			// 
			// Label227
			// 
			this.Label227.Height = 0.1875F;
			this.Label227.HyperLink = null;
			this.Label227.Left = 5.375F;
			this.Label227.Name = "Label227";
			this.Label227.Style = "font-size: 9pt";
			this.Label227.Text = "39b";
			this.Label227.Top = 3.9375F;
			this.Label227.Width = 0.25F;
			// 
			// Shape41
			// 
			this.Shape41.Height = 0.1875F;
			this.Shape41.Left = 5.625F;
			this.Shape41.Name = "Shape41";
			this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape41.Top = 3.9375F;
			this.Shape41.Width = 1.3125F;
			// 
			// txt39b
			// 
			this.txt39b.Height = 0.1875F;
			this.txt39b.Left = 5.6875F;
			this.txt39b.Name = "txt39b";
			this.txt39b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txt39b.Text = " ";
			this.txt39b.Top = 3.9375F;
			this.txt39b.Width = 1.1875F;
			// 
			// Label228
			// 
			this.Label228.Height = 0.1875F;
			this.Label228.HyperLink = null;
			this.Label228.Left = 0.375F;
			this.Label228.Name = "Label228";
			this.Label228.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label228.Text = "b.  Total number of acres withdrawn from";
			this.Label228.Top = 3.9375F;
			this.Label228.Width = 2.1875F;
			// 
			// lblDateRange5
			// 
			this.lblDateRange5.Height = 0.1875F;
			this.lblDateRange5.HyperLink = null;
			this.lblDateRange5.Left = 2.4375F;
			this.lblDateRange5.Name = "lblDateRange5";
			this.lblDateRange5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange5.Text = "04/02/04 to 04/01/05";
			this.lblDateRange5.Top = 3.9375F;
			this.lblDateRange5.Width = 1.875F;
			// 
			// Label230
			// 
			this.Label230.Height = 0.1875F;
			this.Label230.HyperLink = null;
			this.Label230.Left = 5.375F;
			this.Label230.Name = "Label230";
			this.Label230.Style = "font-size: 9pt";
			this.Label230.Text = "39c";
			this.Label230.Top = 4.375F;
			this.Label230.Width = 0.25F;
			// 
			// Shape42
			// 
			this.Shape42.Height = 0.1875F;
			this.Shape42.Left = 5.625F;
			this.Shape42.Name = "Shape42";
			this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape42.Top = 4.375F;
			this.Shape42.Width = 1.3125F;
			// 
			// txt39c
			// 
			this.txt39c.Height = 0.1875F;
			this.txt39c.Left = 5.6875F;
			this.txt39c.Name = "txt39c";
			this.txt39c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txt39c.Text = " ";
			this.txt39c.Top = 4.375F;
			this.txt39c.Width = 1.1875F;
			// 
			// Label231
			// 
			this.Label231.Height = 0.1875F;
			this.Label231.HyperLink = null;
			this.Label231.Left = 0.375F;
			this.Label231.Name = "Label231";
			this.Label231.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label231.Text = "c.  Total value of penalties assessed by the municipality due to the withdrawal";
			this.Label231.Top = 4.1875F;
			this.Label231.Width = 4.1875F;
			// 
			// lblDateRange6
			// 
			this.lblDateRange6.Height = 0.1875F;
			this.lblDateRange6.HyperLink = null;
			this.lblDateRange6.Left = 2.644F;
			this.lblDateRange6.Name = "lblDateRange6";
			this.lblDateRange6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange6.Text = "04/02/04 to 04/01/05";
			this.lblDateRange6.Top = 4.375F;
			this.lblDateRange6.Width = 1.8125F;
			// 
			// Label233
			// 
			this.Label233.Height = 0.1875F;
			this.Label233.HyperLink = null;
			this.Label233.Left = 0.514F;
			this.Label233.Name = "Label233";
			this.Label233.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label233.Text = "of classified Working Waterfront land from";
			this.Label233.Top = 4.375F;
			this.Label233.Width = 2.125F;
			// 
			// srptMVRPage4
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34f)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34g)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpenSpaceA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpenSpaceB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpenSpaceC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label153)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label150)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34a1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34a2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label198)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label199)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label200)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label201)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label202)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewLine35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label220)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label221)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label222)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label223)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label224)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label225)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label226)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label227)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label228)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label230)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label231)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label233)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34f;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34g;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpenSpaceA;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpenSpaceB;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpenSpaceC;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label153;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label150;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label154;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label156;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label158;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34a1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label159;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label160;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label161;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34a2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label162;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label163;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label164;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label165;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label168;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label169;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label170;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label172;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label173;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label174;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34d;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label198;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label199;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34e;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label200;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label201;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label202;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label203;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label207;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label208;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewLine35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label211;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label213;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label215;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label216;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label219;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label220;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label221;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label222;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label223;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label224;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label225;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt39a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label226;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label227;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt39b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label228;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label230;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt39c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label231;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label233;
	}
}
