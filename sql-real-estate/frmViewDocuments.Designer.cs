﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmViewDocuments.
	/// </summary>
	partial class frmViewDocuments : BaseForm
	{
		public Wisej.Web.ImageList imgSmall;
		public Wisej.Web.ImageList imgLarge;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileAddDocument;
		public fecherFoundation.FCToolStripMenuItem mnuFileDeleteDocument;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoom;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomZoomIn;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomZoomOut;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomFitToWindow;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomAspectRatio;
		public fecherFoundation.FCToolStripMenuItem mnuFileRotate;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;
		private Rotation rotation1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewDocuments));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("imgSmall.Images"))), "Dummy");
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("imgLarge.Images"))), "Dummy");
			this.imgSmall = new Wisej.Web.ImageList(this.components);
			this.imgLarge = new Wisej.Web.ImageList(this.components);
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileAddDocument = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDeleteDocument = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileZoom = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileZoomZoomIn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileZoomZoomOut = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileZoomFitToWindow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileZoomAspectRatio = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileRotate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ImageViewer1 = new fecherFoundation.FCViewerPanel();
			this.picSmall = new fecherFoundation.FCPictureBox();
			this.ListView1 = new fecherFoundation.FCListView();
			this.picLarge = new fecherFoundation.FCPictureBox();
			this.lblCurrentPage = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.lblTotalPage = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.cmdFileRotate = new fecherFoundation.FCButton();
			this.cmdFileZoomFitToWindow = new fecherFoundation.FCButton();
			this.cmdFileZoomZoomOut = new fecherFoundation.FCButton();
			this.cmdFileZoomZoomIn = new fecherFoundation.FCButton();
			this.cmdFileDeleteDocument = new fecherFoundation.FCButton();
			this.cmdFileAddDocument = new fecherFoundation.FCButton();
			this.cmdAspectRatio = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.rotation1 = new Wisej.Web.Rotation(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picSmall)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picLarge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRotate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomFitToWindow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomOut)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomIn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteDocument)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAddDocument)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAspectRatio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 551);
			this.BottomPanel.Size = new System.Drawing.Size(1180, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.ImageViewer1);
			this.ClientArea.Controls.Add(this.picSmall);
			this.ClientArea.Controls.Add(this.ListView1);
			this.ClientArea.Controls.Add(this.picLarge);
			this.ClientArea.Controls.Add(this.lblCurrentPage);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.lblTotalPage);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(1180, 491);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileAddDocument);
			this.TopPanel.Controls.Add(this.cmdFileDeleteDocument);
			this.TopPanel.Controls.Add(this.cmdFileZoomZoomIn);
			this.TopPanel.Controls.Add(this.cmdFileZoomZoomOut);
			this.TopPanel.Controls.Add(this.cmdFileZoomFitToWindow);
			this.TopPanel.Controls.Add(this.cmdAspectRatio);
			this.TopPanel.Controls.Add(this.cmdFileRotate);
			this.TopPanel.Size = new System.Drawing.Size(1180, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileRotate, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAspectRatio, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileZoomFitToWindow, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileZoomZoomOut, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileZoomZoomIn, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDeleteDocument, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileAddDocument, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(276, 30);
			this.HeaderText.Text = "View Linked Documents";
			// 
			// imgSmall
			// 
			this.imgSmall.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1
			});
			this.imgSmall.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// imgLarge
			// 
			this.imgLarge.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry2
			});
			this.imgLarge.ImageSize = new System.Drawing.Size(32, 32);
			this.imgLarge.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileAddDocument,
				this.mnuFileDeleteDocument,
				this.mnuSeperator2,
				this.mnuFileZoom,
				this.mnuFileRotate,
				this.mnuSeperator,
				this.mnuFilePrint,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileAddDocument
			// 
			this.mnuFileAddDocument.Index = 0;
			this.mnuFileAddDocument.Name = "mnuFileAddDocument";
			this.mnuFileAddDocument.Shortcut = Wisej.Web.Shortcut.CtrlA;
			this.mnuFileAddDocument.Text = "Add Document";
			this.mnuFileAddDocument.Click += new System.EventHandler(this.mnuFileAddDocument_Click);
			// 
			// mnuFileDeleteDocument
			// 
			this.mnuFileDeleteDocument.Index = 1;
			this.mnuFileDeleteDocument.Name = "mnuFileDeleteDocument";
			this.mnuFileDeleteDocument.Shortcut = Wisej.Web.Shortcut.CtrlR;
			this.mnuFileDeleteDocument.Text = "Remove Document";
			this.mnuFileDeleteDocument.Click += new System.EventHandler(this.mnuFileDeleteDocument_Click);
			// 
			// mnuSeperator2
			// 
			this.mnuSeperator2.Index = 2;
			this.mnuSeperator2.Name = "mnuSeperator2";
			this.mnuSeperator2.Text = "-";
			// 
			// mnuFileZoom
			// 
			this.mnuFileZoom.Index = 3;
			this.mnuFileZoom.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileZoomZoomIn,
				this.mnuFileZoomZoomOut,
				this.mnuFileZoomFitToWindow,
				this.mnuFileZoomAspectRatio
			});
			this.mnuFileZoom.Name = "mnuFileZoom";
			this.mnuFileZoom.Text = "Zoom";
			// 
			// mnuFileZoomZoomIn
			// 
			this.mnuFileZoomZoomIn.Index = 0;
			this.mnuFileZoomZoomIn.Name = "mnuFileZoomZoomIn";
			this.mnuFileZoomZoomIn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuFileZoomZoomIn.Text = "Zoom In";
			this.mnuFileZoomZoomIn.Click += new System.EventHandler(this.mnuFileZoomZoomIn_Click);
			// 
			// mnuFileZoomZoomOut
			// 
			this.mnuFileZoomZoomOut.Index = 1;
			this.mnuFileZoomZoomOut.Name = "mnuFileZoomZoomOut";
			this.mnuFileZoomZoomOut.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuFileZoomZoomOut.Text = "Zoom Out";
			this.mnuFileZoomZoomOut.Click += new System.EventHandler(this.mnuFileZoomZoomOut_Click);
			// 
			// mnuFileZoomFitToWindow
			// 
			this.mnuFileZoomFitToWindow.Index = 2;
			this.mnuFileZoomFitToWindow.Name = "mnuFileZoomFitToWindow";
			this.mnuFileZoomFitToWindow.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuFileZoomFitToWindow.Text = "Fit To Window";
			this.mnuFileZoomFitToWindow.Click += new System.EventHandler(this.mnuFileZoomFitToWindow_Click);
			// 
			// mnuFileZoomAspectRatio
			// 
			this.mnuFileZoomAspectRatio.Index = 3;
			this.mnuFileZoomAspectRatio.Name = "mnuFileZoomAspectRatio";
			this.mnuFileZoomAspectRatio.Text = "Aspect Ratio";
			this.mnuFileZoomAspectRatio.Click += new System.EventHandler(this.mnuFileZoomAspectRatio_Click);
			// 
			// mnuFileRotate
			// 
			this.mnuFileRotate.Index = 4;
			this.mnuFileRotate.Name = "mnuFileRotate";
			this.mnuFileRotate.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuFileRotate.Text = "Rotate";
			this.mnuFileRotate.Click += new System.EventHandler(this.mnuFileRotate_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 7;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 8;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 9;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 10;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// ImageViewer1
			// 
			this.ImageViewer1.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.ImageViewer1.Location = new System.Drawing.Point(596, 70);
			this.ImageViewer1.Name = "ImageViewer1";
			this.ImageViewer1.Size = new System.Drawing.Size(415, 352);
			this.ImageViewer1.TabIndex = 10;
			// 
			// picSmall
			// 
			this.picSmall.AllowDrop = true;
			this.picSmall.BackColor = System.Drawing.SystemColors.Window;
			this.picSmall.DrawStyle = ((short)(0));
			this.picSmall.DrawWidth = ((short)(1));
			this.picSmall.FillColor = -2147483643;
			this.picSmall.FillStyle = ((short)(1));
			this.picSmall.FontTransparent = true;
			this.picSmall.Image = ((System.Drawing.Image)(resources.GetObject("picSmall.Image")));
			this.picSmall.Location = new System.Drawing.Point(734, 391);
			this.picSmall.Name = "picSmall";
			this.picSmall.Picture = ((System.Drawing.Image)(resources.GetObject("picSmall.Picture")));
			this.picSmall.Size = new System.Drawing.Size(16, 16);
			this.picSmall.TabIndex = 11;
			this.picSmall.Visible = false;
			// 
			// ListView1
			// 
			this.ListView1.BackColor = System.Drawing.SystemColors.Window;
			this.ListView1.HeaderStyle = Wisej.Web.ColumnHeaderStyle.None;
			this.ListView1.LabelEdit = true;
			this.ListView1.Location = new System.Drawing.Point(30, 20);
			this.ListView1.Name = "ListView1";
			this.ListView1.OLEDragMode = fecherFoundation.FCListView.OLEDragConstants.ccOLEDragManual;
			this.ListView1.OLEDropMode = fecherFoundation.FCListView.OLEDragConstants.ccOLEDragManual;
			this.ListView1.Size = new System.Drawing.Size(534, 402);
			this.ListView1.SortOrder = Wisej.Web.SortOrder.None;
			this.ListView1.TabIndex = 9;
			this.ListView1.View = Wisej.Web.View.Details;
			this.ListView1.SelectedIndexChanged += new EventHandler(ListView1_SelectedIndexChanged);
			// 
			// picLarge
			// 
			this.picLarge.AllowDrop = true;
			this.picLarge.BackColor = System.Drawing.SystemColors.Window;
			this.picLarge.DrawStyle = ((short)(0));
			this.picLarge.DrawWidth = ((short)(1));
			this.picLarge.FillColor = -2147483643;
			this.picLarge.FillStyle = ((short)(1));
			this.picLarge.FontTransparent = true;
			this.picLarge.Image = ((System.Drawing.Image)(resources.GetObject("picLarge.Image")));
			this.picLarge.Location = new System.Drawing.Point(784, 391);
			this.picLarge.Name = "picLarge";
			this.picLarge.Picture = ((System.Drawing.Image)(resources.GetObject("picLarge.Picture")));
			this.picLarge.Size = new System.Drawing.Size(32, 32);
			this.picLarge.TabIndex = 12;
			this.picLarge.Visible = false;
			// 
			// lblCurrentPage
			// 
			this.lblCurrentPage.Location = new System.Drawing.Point(895, 429);
			this.lblCurrentPage.Name = "lblCurrentPage";
			this.lblCurrentPage.Size = new System.Drawing.Size(29, 17);
			this.lblCurrentPage.TabIndex = 17;
			this.lblCurrentPage.Text = "0";
			this.lblCurrentPage.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.lblCurrentPage.Visible = false;
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(929, 429);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(9, 17);
			this.Label8.TabIndex = 16;
			this.Label8.Text = "/";
			this.Label8.Visible = false;
			// 
			// lblTotalPage
			// 
			this.lblTotalPage.Location = new System.Drawing.Point(941, 429);
			this.lblTotalPage.Name = "lblTotalPage";
			this.lblTotalPage.Size = new System.Drawing.Size(37, 17);
			this.lblTotalPage.TabIndex = 15;
			this.lblTotalPage.Text = "0";
			this.lblTotalPage.Visible = false;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(784, 429);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(125, 17);
			this.Label3.TabIndex = 14;
			this.Label3.Text = "TOTAL PAGES";
			this.Label3.Visible = false;
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(596, 20);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(415, 30);
			this.lblDescription.TabIndex = 13;
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cmdFileRotate
			// 
			this.cmdFileRotate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileRotate.AppearanceKey = "toolbarButton";
			this.cmdFileRotate.Location = new System.Drawing.Point(606, 29);
			this.cmdFileRotate.Name = "cmdFileRotate";
			this.cmdFileRotate.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdFileRotate.Size = new System.Drawing.Size(55, 24);
			this.cmdFileRotate.TabIndex = 23;
			this.cmdFileRotate.Text = "Rotate";
			this.cmdFileRotate.Click += new System.EventHandler(this.mnuFileRotate_Click);
			// 
			// cmdFileZoomFitToWindow
			// 
			this.cmdFileZoomFitToWindow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileZoomFitToWindow.AppearanceKey = "toolbarButton";
			this.cmdFileZoomFitToWindow.Location = new System.Drawing.Point(667, 29);
			this.cmdFileZoomFitToWindow.Name = "cmdFileZoomFitToWindow";
			this.cmdFileZoomFitToWindow.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdFileZoomFitToWindow.Size = new System.Drawing.Size(100, 24);
			this.cmdFileZoomFitToWindow.TabIndex = 22;
			this.cmdFileZoomFitToWindow.Text = "Fit To Window";
            this.cmdFileZoomFitToWindow.Visible = false;
            this.cmdFileZoomFitToWindow.Click += new System.EventHandler(this.mnuFileZoomFitToWindow_Click);
			// 
			// cmdFileZoomZoomOut
			// 
			this.cmdFileZoomZoomOut.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileZoomZoomOut.AppearanceKey = "toolbarButton";
			this.cmdFileZoomZoomOut.Location = new System.Drawing.Point(773, 29);
			this.cmdFileZoomZoomOut.Name = "cmdFileZoomZoomOut";
			this.cmdFileZoomZoomOut.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdFileZoomZoomOut.Size = new System.Drawing.Size(74, 24);
			this.cmdFileZoomZoomOut.TabIndex = 21;
			this.cmdFileZoomZoomOut.Text = "Zoom Out";
            this.cmdFileZoomZoomOut.Visible = false;
            this.cmdFileZoomZoomOut.Click += new System.EventHandler(this.mnuFileZoomZoomOut_Click);
			// 
			// cmdFileZoomZoomIn
			// 
			this.cmdFileZoomZoomIn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileZoomZoomIn.AppearanceKey = "toolbarButton";
			this.cmdFileZoomZoomIn.Location = new System.Drawing.Point(853, 29);
			this.cmdFileZoomZoomIn.Name = "cmdFileZoomZoomIn";
			this.cmdFileZoomZoomIn.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdFileZoomZoomIn.Size = new System.Drawing.Size(61, 24);
			this.cmdFileZoomZoomIn.TabIndex = 20;
			this.cmdFileZoomZoomIn.Text = "Zoom In";
            this.cmdFileZoomZoomIn.Visible = false;
            this.cmdFileZoomZoomIn.Click += new System.EventHandler(this.mnuFileZoomZoomIn_Click);
			// 
			// cmdFileDeleteDocument
			// 
			this.cmdFileDeleteDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDeleteDocument.AppearanceKey = "toolbarButton";
			this.cmdFileDeleteDocument.Location = new System.Drawing.Point(920, 29);
			this.cmdFileDeleteDocument.Name = "cmdFileDeleteDocument";
			this.cmdFileDeleteDocument.Shortcut = Wisej.Web.Shortcut.CtrlR;
			this.cmdFileDeleteDocument.Size = new System.Drawing.Size(127, 24);
			this.cmdFileDeleteDocument.TabIndex = 19;
			this.cmdFileDeleteDocument.Text = "Remove Document";
			this.cmdFileDeleteDocument.Click += new System.EventHandler(this.mnuFileDeleteDocument_Click);
			// 
			// cmdFileAddDocument
			// 
			this.cmdFileAddDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileAddDocument.AppearanceKey = "toolbarButton";
			this.cmdFileAddDocument.Location = new System.Drawing.Point(1053, 29);
			this.cmdFileAddDocument.Name = "cmdFileAddDocument";
			this.cmdFileAddDocument.Shortcut = Wisej.Web.Shortcut.CtrlA;
			this.cmdFileAddDocument.Size = new System.Drawing.Size(107, 24);
			this.cmdFileAddDocument.TabIndex = 18;
			this.cmdFileAddDocument.Text = "Add Document";
			this.cmdFileAddDocument.Click += new System.EventHandler(this.mnuFileAddDocument_Click);
			// 
			// cmdAspectRatio
			// 
			this.cmdAspectRatio.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAspectRatio.AppearanceKey = "toolbarButton";
			this.cmdAspectRatio.Location = new System.Drawing.Point(513, 29);
			this.cmdAspectRatio.Name = "cmdAspectRatio";
			this.cmdAspectRatio.Size = new System.Drawing.Size(87, 24);
			this.cmdAspectRatio.TabIndex = 26;
			this.cmdAspectRatio.Text = "Aspect Ratio";
			this.cmdAspectRatio.Click += new System.EventHandler(this.mnuFileZoomAspectRatio_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.AppearanceKey = "acceptButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(525, 30);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePrint.Size = new System.Drawing.Size(100, 48);
			this.cmdFilePrint.TabIndex = 0;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmViewDocuments
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1180, 659);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmViewDocuments";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "View Attached Documents";
			this.Load += new System.EventHandler(this.frmViewDocuments_Load);
			this.Activated += new System.EventHandler(this.frmViewDocuments_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmViewDocuments_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.picSmall)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picLarge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRotate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomFitToWindow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomOut)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomIn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteDocument)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAddDocument)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAspectRatio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCViewerPanel ImageViewer1;
		public FCPictureBox picSmall;
		public FCListView ListView1;
		public FCPictureBox picLarge;
		public FCLabel lblCurrentPage;
		public FCLabel Label8;
		public FCLabel lblTotalPage;
		public FCLabel Label3;
		public FCLabel lblDescription;
		private FCButton cmdFileZoomZoomOut;
		private FCButton cmdFileRotate;
		private FCButton cmdFileAddDocument;
		private FCButton cmdFileZoomFitToWindow;
		private FCButton cmdFileDeleteDocument;
		private FCButton cmdFileZoomZoomIn;
		private FCButton cmdAspectRatio;
		private FCButton cmdFilePrint;
	}
}
