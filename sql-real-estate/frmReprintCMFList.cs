﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmReprintCMFList.
	/// </summary>
	public partial class frmReprintCMFList : BaseForm
	{
		public frmReprintCMFList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprintCMFList InstancePtr
		{
			get
			{
				return (frmReprintCMFList)Sys.GetInstance(typeof(frmReprintCMFList));
			}
		}

		protected frmReprintCMFList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 10/13/2005
		// The user chooses from a list of cmf lists to reprint
		// ********************************************************
		const int CNSTGRIDCOLSESSIONID = 0;
		const int CNSTGRIDCOLDATETIME = 1;
		const int CNSTGRIDCOLSTARTCODE = 2;
		const int CNSTGRIDCOLENDCODE = 3;
		const int CNSTGRIDCOLCOMPUTERNAME = 4;

		private void frmReprintCMFList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmReprintCMFList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprintCMFList properties;
			//frmReprintCMFList.FillStyle	= 0;
			//frmReprintCMFList.ScaleWidth	= 9300;
			//frmReprintCMFList.ScaleHeight	= 7350;
			//frmReprintCMFList.LinkTopic	= "Form2";
			//frmReprintCMFList.LockControls	= true;
			//frmReprintCMFList.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void frmReprintCMFList_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 5;
			Grid.FixedRows = 1;
			Grid.FixedCols = 0;
			Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			Grid.ExtendLastCol = true;
			Grid.ColHidden(CNSTGRIDCOLSESSIONID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLDATETIME, "Time");
			Grid.TextMatrix(0, CNSTGRIDCOLSTARTCODE, "First Number");
			Grid.TextMatrix(0, CNSTGRIDCOLENDCODE, "Last Number");
			Grid.TextMatrix(0, CNSTGRIDCOLCOMPUTERNAME, "Computer");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLDATETIME, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSTARTCODE, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLENDCODE, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			int lngRow;
			strSQL = "select min(barcode) as mincode,max(barcode) as maxcode,sessionid,computername,cmfdatetime from cmfnumbers group by sessionid,computername,cmfdatetime order by cmfdatetime desc,sessionid,computername";
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSESSIONID, FCConvert.ToString(clsLoad.Get_Fields_Int32("sessionid")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDATETIME, FCConvert.ToString(clsLoad.Get_Fields_DateTime("cmfdatetime")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCOMPUTERNAME, FCConvert.ToString(clsLoad.Get_Fields_String("computername")));
				// TODO Get_Fields: Field [mincode] not found!! (maybe it is an alias?)
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSTARTCODE, FCConvert.ToString(clsLoad.Get_Fields("mincode")));
				// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
				Grid.TextMatrix(lngRow, CNSTGRIDCOLENDCODE, FCConvert.ToString(clsLoad.Get_Fields("maxcode")));
				clsLoad.MoveNext();
			}
		}

		private void mnuPurge_Click(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			if (Grid.Row < 1)
				return;
			if (MessageBox.Show("Are you sure you want to delete the selected list(s)?", "Purge List(s)?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				for (x = Grid.SelectedRows.Count - 1; x >= 0; x--)
				{
					lngRow = Grid.GetFlexRowIndex(Grid.SelectedRows[x].Index);
					modGlobalFunctions.AddCYAEntry_8("RE", "Purged Certified Mail List", Grid.TextMatrix(lngRow, CNSTGRIDCOLSTARTCODE), Grid.TextMatrix(lngRow, CNSTGRIDCOLENDCODE));
					clsSave.Execute("delete from cmfnumbers where sessionid = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLSESSIONID))) + " and computername = '" + Grid.TextMatrix(lngRow, CNSTGRIDCOLCOMPUTERNAME) + "'", modGlobalVariables.strREDatabase);
					Grid.RemoveItem(lngRow);
				}
				// x
			}
			//FC:FINAL:DSE unselect rows
			for(x = Grid.SelectedRows.Count - 1; x >= 0; x--)
			{
				Grid.SelectedRows[x].Selected = false;
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strList;
			int x;
			if (Grid.Row < 1)
				return;
			strList = "";
			for (x = 0; x <= Grid.SelectedRows.Count - 1; x++)
			{
				strList += Grid.TextMatrix(Grid.SelectedRows[x].Index, CNSTGRIDCOLSESSIONID) + ";" + Grid.TextMatrix(Grid.SelectedRows[x].Index, CNSTGRIDCOLCOMPUTERNAME) + ",";
			}
			// x
			if (strList != string.Empty)
			{
				strList = Strings.Mid(strList, 1, strList.Length - 1);
			}
			rptCMFList.InstancePtr.Init(strList, true);
		}
	}
}
