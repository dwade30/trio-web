﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNotUpdatedAccounts.
	/// </summary>
	public partial class rptNotUpdatedAccounts : BaseSectionReport
	{
		public rptNotUpdatedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Accounts Not Updated";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNotUpdatedAccounts InstancePtr
		{
			get
			{
				return (rptNotUpdatedAccounts)Sys.GetInstance(typeof(rptNotUpdatedAccounts));
			}
		}

		protected rptNotUpdatedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNotUpdatedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCRow >= frmImportCompetitorXF.InstancePtr.GridNotUpdated.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			txtTotal.Text = Strings.Format(frmImportCompetitorXF.InstancePtr.GridNotUpdated.Rows - 1, "#,###,###,##0");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtMapLot.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTMAPLOT));
			txtName.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTOWNER));
			txtTRIOAccount.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTTRIOACCOUNT));
			txtOtherAccount.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTVISIONACCOUNT));
			txtLocation.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREETNUM) + " " + frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREET));
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
