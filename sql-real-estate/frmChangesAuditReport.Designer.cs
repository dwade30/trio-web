﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmChangesAuditReport.
	/// </summary>
	partial class frmChangesAuditReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCFrame Frame1;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCComboBox cboScreen;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSaveContinue;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangesAuditReport));
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtLowDate = new Global.T2KDateBox();
            this.txtHighDate = new Global.T2KDateBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cboScreen = new fecherFoundation.FCComboBox();
            this.txtAccount = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 473);
            this.BottomPanel.Size = new System.Drawing.Size(534, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSaveContinue);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cboScreen);
            this.ClientArea.Controls.Add(this.txtAccount);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(534, 413);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(534, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(263, 30);
            this.HeaderText.Text = "Changes Audit Archive";
            // 
            // cmbOrder
            // 
            this.cmbOrder.Items.AddRange(new object[] {
            "Date",
            "User",
            "Description"});
            this.cmbOrder.Location = new System.Drawing.Point(156, 260);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(201, 40);
            this.cmbOrder.TabIndex = 6;
            this.cmbOrder.Text = "Date";
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(30, 274);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(72, 15);
            this.lblOrder.TabIndex = 5;
            this.lblOrder.Text = "ORDER BY";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtLowDate);
            this.Frame1.Controls.Add(this.txtHighDate);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Location = new System.Drawing.Point(30, 150);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(347, 90);
            this.Frame1.TabIndex = 4;
            this.Frame1.Text = "Date Range";
            // 
            // txtLowDate
            // 
            this.txtLowDate.Location = new System.Drawing.Point(20, 30);
            this.txtLowDate.Mask = "##/##/####";
            this.txtLowDate.Name = "txtLowDate";
            this.txtLowDate.Size = new System.Drawing.Size(125, 40);
            // 
            // txtHighDate
            // 
            this.txtHighDate.Location = new System.Drawing.Point(202, 30);
            this.txtHighDate.Mask = "##/##/####";
            this.txtHighDate.Name = "txtHighDate";
            this.txtHighDate.Size = new System.Drawing.Size(125, 40);
            this.txtHighDate.TabIndex = 1;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(164, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 17);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboScreen
            // 
            this.cboScreen.BackColor = System.Drawing.SystemColors.Window;
            this.cboScreen.Location = new System.Drawing.Point(156, 90);
            this.cboScreen.Name = "cboScreen";
            this.cboScreen.Size = new System.Drawing.Size(201, 40);
            this.cboScreen.TabIndex = 3;
            // 
            // txtAccount
            // 
            this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccount.Location = new System.Drawing.Point(156, 30);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(201, 40);
            this.txtAccount.TabIndex = 1;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 104);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(62, 17);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "SCREEN";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(62, 17);
            this.Label1.Text = "ACCOUNT";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(30, 347);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(186, 48);
            this.cmdSaveContinue.TabIndex = 7;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmChangesAuditReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(534, 483);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmChangesAuditReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Changes Audit Archive";
            this.Load += new System.EventHandler(this.frmChangesAuditReport_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChangesAuditReport_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
