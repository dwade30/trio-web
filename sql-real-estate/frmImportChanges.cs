﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportChanges.
	/// </summary>
	public partial class frmImportChanges : BaseForm
	{
		public frmImportChanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label8.AddControlArrayElement(Label8_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportChanges InstancePtr
		{
			get
			{
				return (frmImportChanges)Sys.GetInstance(typeof(frmImportChanges));
			}
		}

		protected frmImportChanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int lngAccount;
		int intCard;
		int lngMasterID;

		private struct PicInfo
		{
			public bool boolNew;
			// Is this a pic taken on the Pocket PC?
			public bool boolDeleted;
			// was the picture deleted?
			public bool boolChanged;
			// was the picture changed?
			public int SequenceNumber;
			// which picture number will this be in the account
			public bool boolIsTheDeleteCell;
			// if true this cell will deleted pics dropped on it
			public bool boolIstheAddCell;
			// if true this is the cell that will add or undelete
			// a picture when it is dropped on this cell
			public bool boolNotEmpty;
			public int lngID;
			public string FileName;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public PicInfo(int unusedParam)
			{
				this.boolNew = false;
				this.boolDeleted = false;
				this.boolChanged = false;
				this.SequenceNumber = 0;
				this.boolIsTheDeleteCell = false;
				this.boolIstheAddCell = false;
				this.boolNotEmpty = false;
				this.lngID = 0;
				this.FileName = string.Empty;
			}
		};

		bool boolDragDrop;
		// are we performing a drag and drop procedure
		PicInfo[,] aryPicInfo;
		// array is used backwards so we can redim it
		PicInfo[,] arySketchesInfo;
		int intSourceCol;
		int intSourceRow;
		bool boolSoundChanged;
		bool boolRecording;
		string strWavName = "";
		string strDataPath;

		public void Init(ref int Account, ref short CARD, ref string strPath)
		{
			clsDRWrapper drTemp = new clsDRWrapper();
			string strTemp;
			strTemp = drTemp.MakeODBCConnectionStringForAccess(strPath + "RETempIm.vb1");
			drTemp.AddConnection(strTemp, "RETempIm.vb1", FCConvert.ToInt32(dbConnectionTypes.ODBC));
			strTemp = drTemp.MakeODBCConnectionStringForAccess(strPath + "TWREHHex.vb1");
			drTemp.AddConnection(strTemp, "TWREHHex.vb1", FCConvert.ToInt32(dbConnectionTypes.ODBC));
			strDataPath = strPath;
			lngAccount = Account;
			intCard = CARD;
			boolDragDrop = false;
			if (FillInfo())
			{
				boolSoundChanged = false;
				boolRecording = false;
				this.Show(FCForm.FormShowEnum.Modal);
				// Me.Show , MDIParent
			}
			else
			{
				this.Unload();
			}
		}

		private void frmImportChanges_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportChanges properties;
			//frmImportChanges.ScaleWidth	= 9225;
			//frmImportChanges.ScaleHeight	= 7800;
			//frmImportChanges.LinkTopic	= "Form1";
			//frmImportChanges.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridMisc();
			SetupGridLand();
			SetupGridDwellings();
			SetupGridOutbuilding();
			SetupGridComms();
			SetupPicSketches(ref strDataPath);
		}

		private void frmImportChanges_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			boolDragDrop = false;
			framImage.Visible = false;
			framSketchDragIcon.Visible = false;
		}

		private void frmImportChanges_Resize(object sender, System.EventArgs e)
		{
			ResizeGridMisc();
			ResizeGridLand();
			ResizeDwellingGrids();
			ResizeGridOutbuilding();
			ResizeGridComms();
			ResizePicSketches();
		}

		private void Frame2_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int intNewPicMouseRow;
			int intNewPicMouseCol;
			intNewPicMouseRow = GridNewPics.MouseRow;
			intNewPicMouseCol = GridNewPics.MouseCol;
			framImage.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				if (intNewPicMouseCol >= 0)
				{
					GridNewPicsDrop(ref intSourceRow, ref intSourceCol, ref intNewPicMouseCol);
				}
			}
		}

		private void framImage_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			// If boolDragDrop Then
			// framImage.Left = X - (framImage.Width / 2)
			// framImage.Top = Y - (framImage.Height / 2)
			// DoEvents
			// End If
		}

		private void GridDwelling1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (GridDwelling1.Row == 0)
			{
				// If KeyCode = vbKeyUp Then
				// KeyCode = 0
				// GridDwelling3.Row = GridDwelling3.Rows - 1
				// GridDwelling3.SetFocus
				// GridDwelling3.EditCell
				// End If
			}
			else if (GridDwelling1.Row == GridDwelling1.Rows - 1)
			{
				// If KeyCode = vbKeyDown Or KeyCode = vbKeyReturn Then
				// KeyCode = 0
				// GridDwelling2.Row = 0
				// GridDwelling2.SetFocus
				// GridDwelling2.EditCell
				// End If
			}
			else if (e.KeyCode == Keys.Return)
			{
				KeyCode = 0;
				GridDwelling1.Row += 1;
			}
		}

		private void GridDwelling1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (GridDwelling1.Row == GridDwelling1.Rows - 1)
				{
					// GridDwelling2.Row = 0
					// SendKeys "{TAB}"
				}
				else
				{
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void GridDwelling1_RowColChange(object sender, System.EventArgs e)
		{
			if (GridDwelling1.MouseRow > 0)
			{
				if (GridDwelling1.MouseCol == 1)
				{
					GridDwelling1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					GridDwelling1.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}
		// vbPorter upgrade warning: srcRow As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: srcCol As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intDestCol As short	OnWriteFCConvert.ToInt32(
		private void GridNewPicsDrop(ref int srcRow, ref int srcCol, ref int intDestCol)
		{
			switch (intDestCol)
			{
				case 0:
					{
						// remove operation
						if (aryPicInfo[srcCol, srcRow].boolNew)
						{
							GridPics.TextMatrix(srcRow, srcCol, "New" + "\r\n" + "Removed");
							aryPicInfo[srcCol, srcRow].boolDeleted = true;
						}
						else
						{
							GridPics.TextMatrix(srcRow, srcCol, "Removed");
							aryPicInfo[srcCol, srcRow].boolDeleted = true;
						}
						break;
					}
				case 1:
					{
						// restore operation
						if (aryPicInfo[srcCol, srcRow].boolNew)
						{
							GridPics.TextMatrix(srcRow, srcCol, "New");
							aryPicInfo[srcCol, srcRow].boolDeleted = false;
						}
						else
						{
							GridPics.TextMatrix(srcRow, srcCol, "");
							aryPicInfo[srcCol, srcRow].boolDeleted = false;
						}
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: srcRow As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: srcCol As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intDestCol As short	OnWriteFCConvert.ToInt32(
		private void GridNewSketchesDrop(ref int srcRow, ref int srcCol, ref int intDestCol)
		{
			switch (intDestCol)
			{
				case 0:
					{
						// remove operation
						if (arySketchesInfo[srcCol, srcRow].boolNew)
						{
							GridSketches.TextMatrix(srcRow, srcCol, "New" + "\r\n" + "Removed");
							arySketchesInfo[srcCol, srcRow].boolDeleted = true;
						}
						else
						{
							GridSketches.TextMatrix(srcRow, srcCol, "Removed");
							arySketchesInfo[srcCol, srcRow].boolDeleted = true;
						}
						break;
					}
				case 1:
					{
						// restore operation
						if (arySketchesInfo[srcCol, srcRow].boolNew)
						{
							GridSketches.TextMatrix(srcRow, srcCol, "New");
							arySketchesInfo[srcCol, srcRow].boolDeleted = false;
						}
						else
						{
							GridSketches.TextMatrix(srcRow, srcCol, "");
							arySketchesInfo[srcCol, srcRow].boolDeleted = false;
						}
						break;
					}
			}
			//end switch
		}

		private void GridNewPics_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If boolDragDrop Then
			// framImage.Left = X - (framImage.Width / 2)
			// framImage.Top = Y - (framImage.Height / 2)
			// DoEvents
			// End If
			if (boolDragDrop)
			{
				framImage.Left = FCConvert.ToInt32((e.X + Frame2.Left + GridNewPics.Left) - (framImage.Width / 2.0));
				framImage.Top = FCConvert.ToInt32((e.Y + Frame2.Top + GridNewPics.Top) - (framImage.Height / 2.0));
				frmImportChanges.InstancePtr.Refresh();
			}
		}

		private void GridNewPics_MouseUpEvent(object sender, MouseEventArgs e)
		{
			int intDestCol = 0;
			framImage.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				GridNewPicsDrop(ref intSourceRow, ref intSourceCol, ref intDestCol);
			}
		}
		// Private Sub GridNewPics_OLEDragDrop(Data As FCGridCtl.vsDataObject, Effect As Long, ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
		// Dim intCol As Integer
		// Dim srcCol As Integer
		// Dim srcRow As Integer
		//
		// intCol = GridNewPics.MouseCol
		// srcCol = gridsketches.Col
		// srcRow = gridsketches.Row
		// Select Case intCol
		// Case 0
		// remove operation
		// If arysketchesinfo(srcCol, srcRow).boolNew Then
		// gridsketches.TextMatrix(srcRow, srcCol) = "New" & vbNewLine & "Removed"
		// arysketchesinfo(srcCol, srcRow).boolDeleted = True
		// Else
		// gridsketches.TextMatrix(srcRow, srcCol) = "Removed"
		// arysketchesinfo(srcCol, srcRow).boolDeleted = True
		// End If
		// Case 1
		// restore operation
		// If arysketchesinfo(srcCol, srcRow).boolNew Then
		// gridsketches.TextMatrix(srcRow, srcCol) = "New"
		// arysketchesinfo(srcCol, srcRow).boolDeleted = False
		// Else
		// gridsketches.TextMatrix(srcRow, srcCol) = ""
		// arysketchesinfo(srcCol, srcRow).boolDeleted = False
		// End If
		// End Select
		// Effect = vbDropEffectCopy
		//
		// GridDest.AddItem Data.GetData(vbCFText)
		// End Sub
		// vbPorter upgrade warning: intSrcRow As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intSrcCol As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intDestRow As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intDestCol As short	OnWriteFCConvert.ToInt32(
		private void GridSketchesDrop(ref int intSrcRow, ref int intSrcCol, ref int intDestRow, ref int intDestCol)
		{
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			PicInfo TempInfo = new PicInfo(0);
			string strTemp = "";
			// Effect = vbDropEffectCopy
			if ((intSrcCol != intDestCol) || (intSrcRow != intDestRow))
			{
				if (intDestCol >= 0 && intDestRow >= 0)
				{
					if (arySketchesInfo[intDestCol, intDestRow].boolNotEmpty)
					{
						// make sure this isn't an empty cell
						intResp = MessageBox.Show("Do you wish to swap position?" + "\r\n" + "If you answer no the dragged picture will replace this one.", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
						if (intResp != DialogResult.Cancel)
						{
							// just swap em
							TempInfo.boolChanged = arySketchesInfo[intDestCol, intDestRow].boolChanged;
							TempInfo.boolDeleted = arySketchesInfo[intDestCol, intDestRow].boolDeleted;
							TempInfo.boolIstheAddCell = false;
							TempInfo.boolIsTheDeleteCell = false;
							TempInfo.boolNew = arySketchesInfo[intDestCol, intDestRow].boolNew;
							TempInfo.boolNotEmpty = true;
							TempInfo.SequenceNumber = arySketchesInfo[intDestCol, intDestRow].SequenceNumber;
							arySketchesInfo[intDestCol, intDestRow].boolChanged = arySketchesInfo[intSrcCol, intSrcRow].boolChanged;
							arySketchesInfo[intDestCol, intDestRow].boolDeleted = arySketchesInfo[intSrcCol, intSrcRow].boolDeleted;
							arySketchesInfo[intDestCol, intDestRow].boolIstheAddCell = false;
							arySketchesInfo[intDestCol, intDestRow].boolIsTheDeleteCell = false;
							arySketchesInfo[intDestCol, intDestRow].boolNew = arySketchesInfo[intSrcCol, intSrcRow].boolNew;
							arySketchesInfo[intDestCol, intDestRow].boolNotEmpty = true;
							arySketchesInfo[intDestCol, intDestRow].SequenceNumber = arySketchesInfo[intSrcCol, intSrcRow].SequenceNumber;
							arySketchesInfo[intSrcCol, intSrcRow].boolChanged = TempInfo.boolChanged;
							arySketchesInfo[intSrcCol, intSrcRow].boolDeleted = TempInfo.boolDeleted;
							arySketchesInfo[intSrcCol, intSrcRow].boolIstheAddCell = false;
							arySketchesInfo[intSrcCol, intSrcRow].boolIsTheDeleteCell = false;
							arySketchesInfo[intSrcCol, intSrcRow].boolNew = TempInfo.boolNew;
							arySketchesInfo[intSrcCol, intSrcRow].boolNotEmpty = true;
							arySketchesInfo[intSrcCol, intSrcRow].SequenceNumber = TempInfo.SequenceNumber;
							// now switch the photos and textmatrix
							strTemp = GridSketches.TextMatrix(intDestRow, intDestCol);
							GridSketches.TextMatrix(intDestRow, intDestCol, GridSketches.TextMatrix(intSrcRow, intSrcCol));
							GridSketches.TextMatrix(intSrcRow, intSrcCol, strTemp);
							ImDragImage.Image = GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, intDestRow, intDestCol);
							GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, intDestRow, intDestCol, GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, intSrcRow, intSrcCol));
							GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, intSrcRow, intSrcCol, ImDragImage.Image);
							ImDragImage.Image = null;
							if (intResp == DialogResult.No)
							{
								// don't just swap em, replace it
								// source is now the replaced pic
								arySketchesInfo[intSrcCol, intSrcRow].boolChanged = true;
								if (arySketchesInfo[intSrcCol, intSrcRow].boolNew)
								{
									GridSketches.TextMatrix(intSrcRow, intSrcCol, "New" + "\r\n" + "Removed");
								}
								else
								{
									GridSketches.TextMatrix(intSrcRow, intSrcCol, "Removed");
								}
							}
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: intSrcRow As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intSrcCol As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intDestRow As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intDestCol As short	OnWriteFCConvert.ToInt32(
		private void GridPicsDrop(ref int intSrcRow, ref int intSrcCol, ref int intDestRow, ref int intDestCol)
		{
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			PicInfo TempInfo = new PicInfo(0);
			string strTemp = "";
			// Effect = vbDropEffectCopy
			if ((intSrcCol != intDestCol) || (intSrcRow != intDestRow))
			{
				if (intDestCol >= 0 && intDestRow >= 0)
				{
					if (aryPicInfo[intDestCol, intDestRow].boolNotEmpty)
					{
						// make sure this isn't an empty cell
						intResp = MessageBox.Show("Do you wish to swap position?" + "\r\n" + "If you answer no the dragged picture will replace this one.", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
						if (intResp != DialogResult.Cancel)
						{
							// just swap em
							TempInfo.boolChanged = aryPicInfo[intDestCol, intDestRow].boolChanged;
							TempInfo.boolDeleted = aryPicInfo[intDestCol, intDestRow].boolDeleted;
							TempInfo.boolIstheAddCell = false;
							TempInfo.boolIsTheDeleteCell = false;
							TempInfo.boolNew = aryPicInfo[intDestCol, intDestRow].boolNew;
							TempInfo.boolNotEmpty = true;
							TempInfo.SequenceNumber = aryPicInfo[intDestCol, intDestRow].SequenceNumber;
							TempInfo.lngID = aryPicInfo[intDestCol, intDestRow].lngID;
							TempInfo.FileName = aryPicInfo[intDestCol, intDestRow].FileName;
							aryPicInfo[intDestCol, intDestRow].boolChanged = aryPicInfo[intSrcCol, intSrcRow].boolChanged;
							aryPicInfo[intDestCol, intDestRow].boolDeleted = aryPicInfo[intSrcCol, intSrcRow].boolDeleted;
							aryPicInfo[intDestCol, intDestRow].boolIstheAddCell = false;
							aryPicInfo[intDestCol, intDestRow].boolIsTheDeleteCell = false;
							aryPicInfo[intDestCol, intDestRow].boolNew = aryPicInfo[intSrcCol, intSrcRow].boolNew;
							aryPicInfo[intDestCol, intDestRow].boolNotEmpty = true;
							aryPicInfo[intDestCol, intDestRow].SequenceNumber = aryPicInfo[intSrcCol, intSrcRow].SequenceNumber;
							aryPicInfo[intDestCol, intDestRow].lngID = aryPicInfo[intSrcCol, intSrcRow].lngID;
							aryPicInfo[intDestCol, intDestRow].FileName = aryPicInfo[intSrcCol, intSrcRow].FileName;
							aryPicInfo[intSrcCol, intSrcRow].boolChanged = TempInfo.boolChanged;
							aryPicInfo[intSrcCol, intSrcRow].boolDeleted = TempInfo.boolDeleted;
							aryPicInfo[intSrcCol, intSrcRow].boolIstheAddCell = false;
							aryPicInfo[intSrcCol, intSrcRow].boolIsTheDeleteCell = false;
							aryPicInfo[intSrcCol, intSrcRow].boolNew = TempInfo.boolNew;
							aryPicInfo[intSrcCol, intSrcRow].boolNotEmpty = true;
							aryPicInfo[intSrcCol, intSrcRow].SequenceNumber = TempInfo.SequenceNumber;
							aryPicInfo[intSrcCol, intSrcRow].lngID = TempInfo.lngID;
							aryPicInfo[intSrcCol, intSrcRow].FileName = TempInfo.FileName;
							// now switch the photos and textmatrix
							strTemp = GridPics.TextMatrix(intDestRow, intDestCol);
							GridPics.TextMatrix(intDestRow, intDestCol, GridPics.TextMatrix(intSrcRow, intSrcCol));
							GridPics.TextMatrix(intSrcRow, intSrcCol, strTemp);
							Image1.Image = GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, intDestRow, intDestCol);
							GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, intDestRow, intDestCol, GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, intSrcRow, intSrcCol));
							GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, intSrcRow, intSrcCol, Image1.Image);
							Image1.Image = null;
							if (intResp == DialogResult.No)
							{
								// don't just swap em, replace it
								// source is now the replaced pic
								aryPicInfo[intSrcCol, intSrcRow].boolChanged = true;
								if (aryPicInfo[intSrcCol, intSrcRow].boolNew)
								{
									GridPics.TextMatrix(intSrcRow, intSrcCol, "New" + "\r\n" + "Removed");
								}
								else
								{
									GridPics.TextMatrix(intSrcRow, intSrcCol, "Removed");
								}
								aryPicInfo[intSrcCol, intSrcRow].boolDeleted = true;
							}
						}
					}
				}
			}
		}

		private void GridNewSketches_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (boolDragDrop)
			{
				framSketchDragIcon.Left = FCConvert.ToInt32((e.X + framSketchMenu.Left + GridNewSketches.Left) - (framSketchDragIcon.Width / 2.0));
				framSketchDragIcon.Top = FCConvert.ToInt32((e.Y + framSketchMenu.Top + GridNewSketches.Top) - (framSketchDragIcon.Height / 2.0));
				frmImportChanges.InstancePtr.Refresh();
			}
		}

		private void GridNewSketches_MouseUpEvent(object sender, MouseEventArgs e)
		{
			int intDestCol = 0;
			framSketchDragIcon.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				GridNewSketchesDrop(ref intSourceRow, ref intSourceCol, ref intDestCol);
			}
		}

		private void GridPics_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If boolDragDrop Then
			// framImage.Left = X - (framImage.Width / 2)
			// framImage.Top = Y - (framImage.Height / 2)
			// End If
			if (boolDragDrop)
			{
				framImage.Left = FCConvert.ToInt32((e.X + Frame1.Left + GridPics.Left) - (framImage.Width / 2.0));
				framImage.Top = FCConvert.ToInt32((e.Y + Frame1.Top + GridPics.Top) - (framImage.Height / 2.0));
				frmImportChanges.InstancePtr.Refresh();
			}
		}

		private void GridPics_MouseUpEvent(object sender, MouseEventArgs e)
		{
			int intPicMouseRow;
			int intPicMouseCol;
			int intNewPicMouseRow;
			int intNewPicMouseCol;
			intPicMouseRow = GridPics.MouseRow;
			intPicMouseCol = GridPics.MouseCol;
			intNewPicMouseRow = GridNewPics.MouseRow;
			intNewPicMouseCol = GridNewPics.MouseCol;
			framImage.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				if (intPicMouseRow >= 0 && intPicMouseCol >= 0)
				{
					GridPicsDrop(ref intSourceRow, ref intSourceCol, ref intPicMouseRow, ref intPicMouseCol);
				}
				else if (intNewPicMouseRow >= 0 && intNewPicMouseCol >= 0)
				{
					GridNewPicsDrop(ref intSourceRow, ref intSourceCol, ref intNewPicMouseCol);
				}
			}
		}

		private void GridSketches_BeforeMouseDown(object sender, MouseEventArgs e)
		{
			//if (e.Cancel) return;
			// if the click was on the active cell, start dragging
			// If .MouseRow = .Row And .MouseCol = .Col Then
			GridSketches.Row = GridSketches.MouseRow;
			GridSketches.Col = GridSketches.MouseCol;
			if (GridSketches.Row >= 0 && GridSketches.Col >= 0)
			{
				if (arySketchesInfo[GridSketches.Col, GridSketches.Row].boolNotEmpty)
				{
					// use OLEDrag method to start manual OLE drag operation
					// this will fire the OLEStartDrag event, which we will use
					// to fill the DataObject with the data we want to drag.
					// .OLEDrag
					// Image1.Picture = .Cell(FCGrid.CellPropertySettings.flexcpPicture, .Row, .Col)
					// Image1.Visible = True
					// .Drag
					intSourceRow = GridSketches.Row;
					intSourceCol = GridSketches.Col;
					boolDragDrop = true;
					framSketchDragIcon.Visible = true;
					// Image2.Picture = .Cell(FCGrid.CellPropertySettings.flexcpPicture, .Row, .Col)
					ImDragImage.Image = GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, GridSketches.Row, GridSketches.Col);
					// Cancel = True
				}
				// tell grid control to ignore mouse movements until the
				// mouse button goes up again
				// Cancel = True
			}
		}

		private void GridSketches_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (boolDragDrop)
			{
				framSketchDragIcon.Left = FCConvert.ToInt32((e.X + framSketch.Left + GridSketches.Left) - (framSketchDragIcon.Width / 2.0));
				framSketchDragIcon.Top = FCConvert.ToInt32((e.Y + framSketch.Top + GridSketches.Top) - (framSketchDragIcon.Height / 2.0));
				frmImportChanges.InstancePtr.Refresh();
			}
		}

		private void GridSketches_MouseUpEvent(object sender, MouseEventArgs e)
		{
			int intPicMouseRow;
			int intPicMouseCol;
			int intNewPicMouseRow;
			int intNewPicMouseCol;
			intPicMouseRow = GridSketches.MouseRow;
			intPicMouseCol = GridSketches.MouseCol;
			intNewPicMouseRow = GridNewSketches.MouseRow;
			intNewPicMouseCol = GridNewSketches.MouseCol;
			framSketchDragIcon.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				if (intPicMouseRow >= 0 && intPicMouseCol >= 0)
				{
					GridSketchesDrop(ref intSourceRow, ref intSourceCol, ref intPicMouseRow, ref intPicMouseCol);
				}
				else if (intNewPicMouseRow >= 0 && intNewPicMouseCol >= 0)
				{
					GridNewSketchesDrop(ref intSourceRow, ref intSourceCol, ref intNewPicMouseCol);
				}
			}
		}
		// Private Sub GridPics_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
		// With GridPics
		//
		// if the click was on the active cell, start dragging
		// .Row = .MouseRow
		// .Col = .MouseCol
		// If .Row >= 0 And .Col >= 0 Then
		// If aryPicInfo(.Col, .Row).boolNotEmpty Then
		// use OLEDrag method to start manual OLE drag operation
		// this will fire the OLEStartDrag event, which we will use
		// to fill the DataObject with the data we want to drag.
		// .OLEDrag
		// End If
		// tell grid control to ignore mouse movements until the
		// mouse button goes up again
		// Cancel = True
		// End If
		// End With
		// End Sub
		private void Image2_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (boolDragDrop)
			{
				framImage.Left = FCConvert.ToInt32((x + framImage.Left) - (framImage.Width / 2.0));
				framImage.Top = FCConvert.ToInt32((y + framImage.Top) - (framImage.Height / 2.0));
				frmImportChanges.InstancePtr.Refresh();
			}
		}

		private void Image2_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int intPicMouseRow;
			int intPicMouseCol;
			int intNewPicMouseRow;
			int intNewPicMouseCol;
			intPicMouseRow = GridPics.MouseRow;
			intPicMouseCol = GridPics.MouseCol;
			intNewPicMouseRow = GridNewPics.MouseRow;
			intNewPicMouseCol = GridNewPics.MouseCol;
			framImage.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				if (intPicMouseRow >= 0 && intPicMouseCol >= 0)
				{
					GridPicsDrop(ref intSourceRow, ref intSourceCol, ref intPicMouseRow, ref intPicMouseCol);
				}
				else if (intNewPicMouseRow >= 0 && intNewPicMouseCol >= 0)
				{
					GridNewPicsDrop(ref intSourceRow, ref intSourceCol, ref intNewPicMouseCol);
				}
			}
		}

		private void ImDragImage_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (boolDragDrop)
			{
				framSketchDragIcon.Left = FCConvert.ToInt32((x + framSketchDragIcon.Left) - (framSketchDragIcon.Width / 2.0));
				framSketchDragIcon.Top = FCConvert.ToInt32((y + framSketchDragIcon.Top) - (framSketchDragIcon.Height / 2.0));
				frmImportChanges.InstancePtr.Refresh();
			}
		}

		private void ImDragImage_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int intPicMouseRow;
			int intPicMouseCol;
			int intNewPicMouseRow;
			int intNewPicMouseCol;
			intPicMouseRow = GridSketches.MouseRow;
			intPicMouseCol = GridSketches.MouseCol;
			intNewPicMouseRow = GridNewSketches.MouseRow;
			intNewPicMouseCol = GridNewSketches.MouseCol;
			framSketchDragIcon.Visible = false;
			if (boolDragDrop)
			{
				boolDragDrop = false;
				if (intPicMouseRow >= 0 && intPicMouseCol >= 0)
				{
					GridSketchesDrop(ref intSourceRow, ref intSourceCol, ref intPicMouseRow, ref intPicMouseCol);
				}
				else if (intNewPicMouseRow >= 0 && intNewPicMouseCol >= 0)
				{
					GridNewSketchesDrop(ref intSourceRow, ref intSourceCol, ref intNewPicMouseCol);
				}
			}
		}

		private void MMControl1_PlayCompleted(object sender, EventArgs e/* AxMCI.DmciEvents_PlayCompletedEvent e */)
		{
			MMControl1.Command = "Prev";
		}

		private void MMControl1_RecordClick(object sender, EventArgs e/* AxMCI.DmciEvents_RecordClickEvent e */)
		{
			// MMControl1.Notify = False
			// MMControl1.Wait = True
			// MMControl1.Shareable = False
			// MMControl1.FileName = App.Path & "\rehandheld\files\TempWav.wav"
			// MMControl1.Command = "Open"
			// boolRecording = True
		}

		private void MMControl1_RecordCompleted(object sender, EventArgs e/* AxMCI.DmciEvents_RecordCompletedEvent e */)
		{
			boolSoundChanged = true;
		}

		private void MMControl1_StopCompleted(object sender, EventArgs e/* AxMCI.DmciEvents_StopCompletedEvent e */)
		{
			string strTemp = "";
			// If boolRecording Then
			// MMControl1.Command = "Save"
			// Call File.Copy(MMControl1.FileName, strWavName, True)
			// strTemp = MMControl1.FileName
			// MMControl1.Command = "Close"
			// MMControl1.FileName = strWavName
			// MMControl1.Command = "Open"
			// Call fso.DeleteFile(strTemp, True)
			// End If
			boolRecording = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool FillInfo()
		{
			bool FillInfo = false;
			string strLocation = "";
			clsDRWrapper clsParcels = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsDwellComm = new clsDRWrapper();
			clsDRWrapper clsMast = new clsDRWrapper();
			clsDRWrapper clsOrigDwellComm = new clsDRWrapper();
			clsDRWrapper clsOut = new clsDRWrapper();
			clsDRWrapper clsOrigOut = new clsDRWrapper();
			clsDRWrapper clsLand = new clsDRWrapper();
			clsDRWrapper clsSounds = new clsDRWrapper();
			int intTemp = 0;
			string strTemp;
			int intSeq = 0;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				strTemp = clsTemp.MakeODBCConnectionStringForAccess(strDataPath + "RETempIm.vb1");
				clsTemp.AddConnection(strTemp, "RETempIm.vb1", FCConvert.ToInt32(dbConnectionTypes.ODBC));
				FillInfo = false;
				// clsParcels.Path = strDataPath
				// clsDwellComm.Path = strDataPath
				// clsOut.Path = strDataPath
				// clsLand.Path = strDataPath
				// clsSounds.Path = strDataPath
				clsParcels.OpenRecordset("select * from tblparcels where accountnumber = " + FCConvert.ToString(lngAccount) + " and cardnumber = " + FCConvert.ToString(intCard), "retempim.vb1");
				if (clsParcels.EndOfFile())
				{
					MessageBox.Show("Could not find a record for Account " + FCConvert.ToString(lngAccount) + " Card " + FCConvert.ToString(intCard), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					FillInfo = false;
					return FillInfo;
				}
				lngMasterID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsParcels.Get_Fields_Int32("masterid"))));
				clsMast.OpenRecordset("select * from master where id = " + FCConvert.ToString(lngMasterID), modGlobalVariables.strREDatabase);
				// TODO Get_Fields: Field [isdwelling] not found!! (maybe it is an alias?)
				if (clsParcels.Get_Fields("isdwelling") == true)
				{
					SSTab1.TabPages[1].Enabled = true;
					SSTab1.TabPages[1].Text = "Dwelling";
					framDwelling.Visible = true;
					framCommercial.Visible = false;
				}
				// TODO Get_Fields: Field [iscommercial] not found!! (maybe it is an alias?)
				else if (clsParcels.Get_Fields("iscommercial") == true)
				{
					SSTab1.TabPages[1].Enabled = true;
					SSTab1.TabPages[1].Text = "Commercial";
					framCommercial.Visible = true;
					framDwelling.Visible = false;
				}
				else
				{
					SSTab1.TabPages[1].Enabled = false;
					SSTab1.TabPages[1].Text = "";
					SSTab1.TabPages[1].Visible = false;
				}
				// fill property information first
				txtowner.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("owner")));
				// TODO Get_Fields: Field [secondowner] not found!! (maybe it is an alias?)
				txtSecondOwner.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields("secondowner")));
				txtAddress1.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("address1")));
				txtAddress2.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("address2")));
				txtCity.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("city")));
				clsTemp.OpenRecordset("select * from tblstates ORDER by id", modGlobalVariables.Statics.strGNDatabase);
				// Call clsTemp.FindFirstRecord("id", Val(clsParcels.Fields("state")))
				if (clsTemp.FindFirst("id = " + FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields_String("state")))))
				{
					// If Not clsTemp.NoMatch Then
					txtState.Text = FCConvert.ToString(clsTemp.Get_Fields_String("STATECODE"));
				}
				else
				{
					txtState.Text = "";
				}
				txtZip.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("zip")));
				txtZip4.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("zip4")));
				// TODO Get_Fields: Field [parcelid] not found!! (maybe it is an alias?)
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields("parcelid")));
				// TODO Get_Fields: Field [LegalRefNo] not found!! (maybe it is an alias?)
				txtRef1.Text = FCConvert.ToString(clsParcels.Get_Fields("LegalRefNo"));
				// TODO Get_Fields: Field [reference2] not found!! (maybe it is an alias?)
				txtRef2.Text = FCConvert.ToString(clsParcels.Get_Fields("reference2"));
				txtStreetNumber.Text = FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields_String("location")));
				if (Conversion.Val(clsParcels.Get_Fields_String("location")) > 0)
				{
					// txtStreetNumber.Text = Val(clsParcels.Fields("location"))
					if (Strings.InStr(1, FCConvert.ToString(clsParcels.Get_Fields_String("location")), " ", CompareConstants.vbTextCompare) > 0)
					{
						strLocation = Strings.Mid(FCConvert.ToString(clsParcels.Get_Fields_String("location")), Strings.InStr(1, FCConvert.ToString(clsParcels.Get_Fields_String("location")), " ", CompareConstants.vbTextCompare) + 1);
					}
					else
					{
						strLocation = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("location")));
					}
				}
				else
				{
					strLocation = Strings.Trim(FCConvert.ToString(clsParcels.Get_Fields_String("location")));
				}
				txtStreetName.Text = strLocation;
				// TODO Get_Fields: Field [ownerphone] not found!! (maybe it is an alias?)
				txtTelephone.Text = Strings.Format(clsParcels.Get_Fields("ownerphone"), "(000)000-0000");
				// now fill gridMisc
				GridMisc.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields_Int32("neighborhood"))));
				// TODO Get_Fields: Field [treegrowth] not found!! (maybe it is an alias?)
				GridMisc.TextMatrix(2, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("treegrowth"))));
				// TODO Get_Fields: Field [xcoord] not found!! (maybe it is an alias?)
				GridMisc.TextMatrix(3, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("xcoord"))));
				// TODO Get_Fields: Field [ycoord] not found!! (maybe it is an alias?)
				GridMisc.TextMatrix(4, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("ycoord"))));
				GridMisc.TextMatrix(5, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields_Int32("zone"))));
				// TODO Get_Fields: Field [secondaryzone] not found!! (maybe it is an alias?)
				GridMisc.TextMatrix(6, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("secondaryzone"))));
				// TODO Get_Fields: Field [topography1] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [topography2] not found!! (maybe it is an alias?)
				GridMisc.TextMatrix(7, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("topography1"))) + FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("topography2"))));
				// TODO Get_Fields: Field [utilities1] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [utilities2] not found!! (maybe it is an alias?)
				GridMisc.TextMatrix(8, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("utilities1"))) + FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("utilities2"))));
				// TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
				GridMisc.TextMatrix(9, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields("streetcode"))));
				GridMisc.TextMatrix(10, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields_String("open1"))));
				GridMisc.TextMatrix(11, 1, FCConvert.ToString(Conversion.Val(clsParcels.Get_Fields_String("open2"))));
				// Call clsMast.OpenRecordset("select * from master where rsaccount = " & lngAccount & " and rscard = " & intCard, strredatabase)
				if (!clsMast.EndOfFile())
				{
					GridMisc.TextMatrix(1, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("pineighborhood"))));
					GridMisc.TextMatrix(2, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("PISTREETCODE"))));
					// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
					GridMisc.TextMatrix(3, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("pixcoord"))));
					// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
					GridMisc.TextMatrix(4, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piycoord"))));
					GridMisc.TextMatrix(5, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("pizone"))));
					GridMisc.TextMatrix(6, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("piseczone"))));
					GridMisc.TextMatrix(7, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("pitopOgraphy1"))) + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("pitopography2"))));
					GridMisc.TextMatrix(8, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("piutilities1"))) + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("piutilities2"))));
					GridMisc.TextMatrix(9, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("pistreet"))));
					GridMisc.TextMatrix(10, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("piopen1"))));
					GridMisc.TextMatrix(11, 2, FCConvert.ToString(Conversion.Val(clsMast.Get_Fields_Int32("piopen2"))));
				}
				for (x = 1; x <= 11; x++)
				{
					if (GridMisc.TextMatrix(x, 1) == GridMisc.TextMatrix(x, 2))
					{
						GridMisc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 1, x, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
				// x
				// now fill land info
				clsLand.OpenRecordset("select * from tblland where masterid = " + FCConvert.ToString(lngMasterID) + " and (val(landunitsa & '') + val(landunitsb & '') > 0 or val(sequencenumber & '') > 0)   order by id", "retempim.vb1");
				intTemp = 0;
				intSeq = 0;
				if (!clsLand.EndOfFile())
				{
					while (!clsLand.EndOfFile())
					{
						intSeq += 1;
						intTemp = intSeq;
						// If Val(clsLand.Fields("sequencenumber")) = 0 Then
						// intTemp = intTemp + 1
						// Else
						// intTemp = Val(clsLand.Fields("sequencenumber"))
						// End If
						if (intTemp < 8)
						{
							// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
							if (Conversion.Val(clsLand.Get_Fields("landcode")) >= 11 && Conversion.Val(clsLand.Get_Fields("landcode")) <= 15)
							{
								// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [landunitsa] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [landunitsb] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [landinfluence] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [influencecode] not found!! (maybe it is an alias?)
								GridLand.AddItem("\t" + clsLand.Get_Fields("landcode") + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landunitsa"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landunitsb"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landinfluence"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("influencecode"))));
							}
							// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
							else if (Conversion.Val(clsLand.Get_Fields("landcode")) >= 16 && Conversion.Val(clsLand.Get_Fields("landcode")) <= 20)
							{
								// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [landunitsb] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [landinfluence] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [influencecode] not found!! (maybe it is an alias?)
								GridLand.AddItem("\t" + clsLand.Get_Fields("landcode") + "\t" + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landunitsb"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landinfluence"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("influencecode"))));
							}
								// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
								else if (Conversion.Val(clsLand.Get_Fields("landcode")) >= 21 && Conversion.Val(clsLand.Get_Fields("landcode")) <= 46)
							{
								// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [landunitsb] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [landinfluence] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [influencecode] not found!! (maybe it is an alias?)
								GridLand.AddItem("\t" + clsLand.Get_Fields("landcode") + "\t" + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landunitsb"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("landinfluence"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLand.Get_Fields("influencecode"))));
							}
							GridLand.IsSubtotal(GridLand.Rows - 1, true);
							// check the original files
							if (!clsMast.EndOfFile())
							{
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								if (Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) >= 0 && Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) <= 10)
								{
									// no land
									GridLand.AddItem("");
								}
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								else if (Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) >= 11 && Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) <= 15)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									GridLand.AddItem("\t" + clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE") + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "unitsa"))) + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "unitsb"))) + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "inf"))) + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "infcode"))));
								}
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									else if (Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) >= 16 && Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) <= 20)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									GridLand.AddItem("\t" + clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "type") + "\t" + "\t" + Strings.Format(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "unitsb")), "###,##0") + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "inf"))) + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "infcode"))));
								}
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										else if (Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) >= 21 && Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE")) <= 46)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strTemp = clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "unitsa") + clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "unitsb");
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									GridLand.AddItem("\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "TYPE"))) + "\t" + "\t" + Strings.Format(Conversion.Val(strTemp) / 100, "0.00") + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "inf"))) + "\t" + FCConvert.ToString(Conversion.Val(clsMast.Get_Fields("piland" + FCConvert.ToString(intTemp) + "infcode"))));
								}
								GridLand.RowOutlineLevel(GridLand.Rows - 1, 1);
								GridLand.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridLand.Rows - 1, 1, GridLand.Rows - 1, 5, modColorScheme.SetGridColor(1));
							}
						}
						clsLand.MoveNext();
					}
				}
				// TODO Get_Fields: Field [isdwelling] not found!! (maybe it is an alias?)
				if (clsParcels.Get_Fields("isdwelling") == true)
				{
					// fill the dwelling information
					clsDwellComm.OpenRecordset("select * from tbldwelling where masterid = " + FCConvert.ToString(lngMasterID), "retempim.vb1");
					if (!clsDwellComm.EndOfFile())
					{
						// TODO Get_Fields: Field [dwchanged] not found!! (maybe it is an alias?)
						if (clsDwellComm.Get_Fields("dwchanged") == true)
						{
							// data changed so let's look at it
							// TODO Get_Fields: Field [buildingstyle] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("buildingstyle"))));
							// TODO Get_Fields: Field [DwellingUnits] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(2, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("DwellingUnits"))));
							// TODO Get_Fields: Field [OtherUnits] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(3, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("OtherUnits"))));
							// TODO Get_Fields: Field [stories] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(4, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("stories"))));
							// TODO Get_Fields: Field [extprimwallid] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(5, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("extprimwallid"))));
							// TODO Get_Fields: Field [extroofcovid] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(6, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("extroofcovid"))));
							// TODO Get_Fields: Field [SFMasonryTrim] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(7, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("SFMasonryTrim"))));
							// TODO Get_Fields: Field [open3] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(8, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("open3"))));
							// TODO Get_Fields: Field [open4] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(9, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("open4"))));
							// TODO Get_Fields: Field [YrBuilt] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(10, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("YrBuilt"))));
							// TODO Get_Fields: Field [YearRemodeled] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(11, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("YearRemodeled"))));
							// TODO Get_Fields: Field [ExtFoundID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(12, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("ExtFoundID"))));
							// TODO Get_Fields: Field [Basement] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(13, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Basement"))));
							// TODO Get_Fields: Field [BasementGarage] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(14, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("BasementGarage"))));
							// TODO Get_Fields: Field [WetBasement] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(15, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("WetBasement"))));
							// TODO Get_Fields: Field [SFBasementLiving] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(16, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("SFBasementLiving"))));
							// TODO Get_Fields: Field [FinishedBasementGrade] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(17, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("FinishedBasementGrade"))));
							// TODO Get_Fields: Field [FinishedBasementFactor] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(18, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("FinishedBasementFactor"))));
							// TODO Get_Fields: Field [Open5] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(19, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Open5"))));
							// TODO Get_Fields: Field [HeatTypeID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(20, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("HeatTypeID"))));
							// TODO Get_Fields: Field [PercHeatedID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(21, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("PercHeatedID"))));
							// TODO Get_Fields: Field [ACID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(22, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("ACID"))));
							// TODO Get_Fields: Field [percentcooled] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(23, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("percentcooled"))));
							// TODO Get_Fields: Field [KRatingID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(24, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("KRatingID"))));
							// TODO Get_Fields: Field [BathStyle] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(25, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("BathStyle"))));
							// TODO Get_Fields: Field [NumRooms] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(26, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("NumRooms"))));
							// TODO Get_Fields: Field [NumBedrooms] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(27, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("NumBedrooms"))));
							// TODO Get_Fields: Field [FullBaths] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(28, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("FullBaths"))));
							// TODO Get_Fields: Field [12Baths] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(29, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("12Baths"))));
							// TODO Get_Fields: Field [OtherFixtures] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(30, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("OtherFixtures"))));
							// TODO Get_Fields: Field [Fireplaces] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(31, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Fireplaces"))));
							// TODO Get_Fields: Field [Layout] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(32, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Layout"))));
							// TODO Get_Fields: Field [Attic] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(33, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Attic"))));
							// TODO Get_Fields: Field [InsulID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(34, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("InsulID"))));
							// TODO Get_Fields: Field [PercentUnfinished] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(35, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("PercentUnfinished"))));
							// TODO Get_Fields: Field [GradeID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(36, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("GradeID"))));
							GridDwelling1.TextMatrix(37, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_Double("Factor"))));
							GridDwelling1.TextMatrix(38, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_Int32("SquareFootage"))));
							GridDwelling1.TextMatrix(39, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_String("Condition"))));
							// TODO Get_Fields: Field [PhysicalPercent] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(40, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("PhysicalPercent"))));
							// TODO Get_Fields: Field [FunctPerc] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(41, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("FunctPerc"))));
							// TODO Get_Fields: Field [FunctionalID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(42, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("FunctionalID"))));
							// TODO Get_Fields: Field [EconPerc] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(43, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("EconPerc"))));
							// TODO Get_Fields: Field [EconomicID] not found!! (maybe it is an alias?)
							GridDwelling1.TextMatrix(44, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("EconomicID"))));
							// open original data to compare
							clsOrigDwellComm.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
							if (!clsOrigDwellComm.EndOfFile())
							{
								// found some data so fill in the old column
								GridDwelling1.TextMatrix(1, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("distyle"))));
								GridDwelling1.TextMatrix(2, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diUnitsDWELLING"))));
								GridDwelling1.TextMatrix(3, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diUnitsother"))));
								GridDwelling1.TextMatrix(4, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("distories"))));
								GridDwelling1.TextMatrix(5, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diextwalls"))));
								GridDwelling1.TextMatrix(6, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diroof"))));
								GridDwelling1.TextMatrix(7, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diSFMasonry"))));
								GridDwelling1.TextMatrix(8, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diopen3"))));
								GridDwelling1.TextMatrix(9, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diopen4"))));
								GridDwelling1.TextMatrix(10, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diYearBuilt"))));
								GridDwelling1.TextMatrix(11, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diYearRemodel"))));
								GridDwelling1.TextMatrix(12, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diFoundation"))));
								GridDwelling1.TextMatrix(13, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dibsmt"))));
								GridDwelling1.TextMatrix(14, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diBsmtGar"))));
								GridDwelling1.TextMatrix(15, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diWetBsmt"))));
								GridDwelling1.TextMatrix(16, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diSFBsmtLiving"))));
								GridDwelling1.TextMatrix(17, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dibsmtfingrade1"))));
								GridDwelling1.TextMatrix(18, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dibsmtfingrade2"))));
								GridDwelling1.TextMatrix(19, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diOpen5"))));
								GridDwelling1.TextMatrix(20, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diHeat"))));
								GridDwelling1.TextMatrix(21, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diPctHeat"))));
								GridDwelling1.TextMatrix(22, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dicool"))));
								GridDwelling1.TextMatrix(23, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dipctcool"))));
								GridDwelling1.TextMatrix(24, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dikitchens"))));
								GridDwelling1.TextMatrix(25, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diBaths"))));
								GridDwelling1.TextMatrix(26, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("diRooms"))));
								GridDwelling1.TextMatrix(27, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIBedrooms"))));
								GridDwelling1.TextMatrix(28, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIFullBaths"))));
								GridDwelling1.TextMatrix(29, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIHalfBaths"))));
								GridDwelling1.TextMatrix(30, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIAddnFixtures"))));
								GridDwelling1.TextMatrix(31, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIFireplaces"))));
								GridDwelling1.TextMatrix(32, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DILayout"))));
								GridDwelling1.TextMatrix(33, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIAttic"))));
								GridDwelling1.TextMatrix(34, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIInsulation"))));
								GridDwelling1.TextMatrix(35, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIPctUnfinished"))));
								GridDwelling1.TextMatrix(36, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIGrade1"))));
								GridDwelling1.TextMatrix(37, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIGrade2"))));
								GridDwelling1.TextMatrix(38, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DISqft"))));
								GridDwelling1.TextMatrix(39, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DICondition"))));
								GridDwelling1.TextMatrix(40, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIPctPhys"))));
								GridDwelling1.TextMatrix(41, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIPctFunct"))));
								GridDwelling1.TextMatrix(42, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("DIFunctCode"))));
								GridDwelling1.TextMatrix(43, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dipctecon"))));
								GridDwelling1.TextMatrix(44, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("dieconcode"))));
							}
							for (x = 1; x <= 44; x++)
							{
								if (GridDwelling1.TextMatrix(x, 1) == GridDwelling1.TextMatrix(x, 2))
								{
									GridDwelling1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 1, x, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
							}
							// x
						}
						else
						{
							SSTab1.TabPages[1].Enabled = false;
						}
					}
					else
					{
						SSTab1.TabPages[1].Enabled = false;
					}
				}
				// TODO Get_Fields: Field [iscommercial] not found!! (maybe it is an alias?)
				else if (clsParcels.Get_Fields("iscommercial") == true)
				{
					clsDwellComm.OpenRecordset("select * from tblcommercial where masterid = " + FCConvert.ToString(lngMasterID) + " where not cmdeleted order by id", "retempim.vb1");
					intSeq = 0;
					if (!clsDwellComm.EndOfFile())
					{
						// If clsDwellComm.Fields("cmchanged") = True Then
						GridComm1.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_Int16("occupancycode"))));
						// TODO Get_Fields: Field [Numberdwellingunits] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(2, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Numberdwellingunits"))));
						GridComm1.TextMatrix(3, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_String("class"))));
						// TODO Get_Fields: Field [quality] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(4, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("quality"))));
						// TODO Get_Fields: Field [gradefactor] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(5, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("gradefactor"))));
						// TODO Get_Fields: Field [exteriorwalls] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(6, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("exteriorwalls"))));
						// TODO Get_Fields: Field [numberofstories] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(7, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("numberofstories"))));
						// TODO Get_Fields: Field [storiesheight] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(8, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("storiesheight"))));
						// TODO Get_Fields: Field [basefloorarea] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(9, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("basefloorarea"))));
						// TODO Get_Fields: Field [perimeterunits] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(10, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("perimeterunits"))));
						// TODO Get_Fields: Field [heatingcooling] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(11, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("heatingcooling"))));
						// TODO Get_Fields: Field [yearbuilt] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(12, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("yearbuilt"))));
						// TODO Get_Fields: Field [yearremodeled] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(13, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("yearremodeled"))));
						GridComm1.TextMatrix(14, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_String("condition"))));
						// TODO Get_Fields: Field [physicalpercent] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(15, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("physicalpercent"))));
						// TODO Get_Fields: Field [functional] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(16, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("functional"))));
						// TODO Get_Fields: Field [economic] not found!! (maybe it is an alias?)
						GridComm1.TextMatrix(17, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("economic"))));
						clsOrigDwellComm.OpenRecordset("select * from commercial where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
						if (!clsOrigDwellComm.EndOfFile())
						{
							GridComm1.TextMatrix(1, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("occ1"))));
							GridComm1.TextMatrix(2, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("Dwel1"))));
							GridComm1.TextMatrix(3, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C1class"))));
							GridComm1.TextMatrix(4, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C1quality"))));
							GridComm1.TextMatrix(5, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1grade"))));
							GridComm1.TextMatrix(6, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1extWalls"))));
							GridComm1.TextMatrix(7, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1stories"))));
							GridComm1.TextMatrix(8, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C1height"))));
							GridComm1.TextMatrix(9, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1floor"))));
							GridComm1.TextMatrix(10, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1perimeter"))));
							GridComm1.TextMatrix(11, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1heat"))));
							GridComm1.TextMatrix(12, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1built"))));
							GridComm1.TextMatrix(13, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1remodel"))));
							GridComm1.TextMatrix(14, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1condition"))));
							GridComm1.TextMatrix(15, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c1phys"))));
							GridComm1.TextMatrix(16, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C1funct"))));
							GridComm1.TextMatrix(17, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("CMecon"))));
						}
						for (x = 1; x <= 17; x++)
						{
							if (GridComm1.TextMatrix(x, 1) == GridComm1.TextMatrix(x, 2))
							{
								GridComm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 1, x, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
						}
						// x
						clsDwellComm.MoveNext();
						if (!clsDwellComm.EndOfFile())
						{
							GridComm2.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_Int16("occupancycode"))));
							// TODO Get_Fields: Field [Numberdwellingunits] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(2, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("Numberdwellingunits"))));
							GridComm2.TextMatrix(3, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_String("class"))));
							// TODO Get_Fields: Field [quality] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(4, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("quality"))));
							// TODO Get_Fields: Field [gradefactor] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(5, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("gradefactor"))));
							// TODO Get_Fields: Field [exteriorwalls] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(6, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("exteriorwalls"))));
							// TODO Get_Fields: Field [numberofstories] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(7, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("numberofstories"))));
							// TODO Get_Fields: Field [storiesheight] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(8, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("storiesheight"))));
							// TODO Get_Fields: Field [basefloorarea] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(9, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("basefloorarea"))));
							// TODO Get_Fields: Field [perimeterunits] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(10, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("perimeterunits"))));
							// TODO Get_Fields: Field [heatingcooling] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(11, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("heatingcooling"))));
							// TODO Get_Fields: Field [yearbuilt] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(12, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("yearbuilt"))));
							// TODO Get_Fields: Field [yearremodeled] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(13, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("yearremodeled"))));
							GridComm2.TextMatrix(14, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields_String("condition"))));
							// TODO Get_Fields: Field [physicalpercent] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(15, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("physicalpercent"))));
							// TODO Get_Fields: Field [functional] not found!! (maybe it is an alias?)
							GridComm2.TextMatrix(16, 1, FCConvert.ToString(Conversion.Val(clsDwellComm.Get_Fields("functional"))));
							if (!clsOrigDwellComm.EndOfFile())
							{
								GridComm2.TextMatrix(1, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("occ2"))));
								GridComm2.TextMatrix(2, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("Dwel2"))));
								GridComm2.TextMatrix(3, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C2class"))));
								GridComm2.TextMatrix(4, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C2quality"))));
								GridComm2.TextMatrix(5, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2grade"))));
								GridComm2.TextMatrix(6, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2extWalls"))));
								GridComm2.TextMatrix(7, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2stories"))));
								GridComm2.TextMatrix(8, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C2height"))));
								GridComm2.TextMatrix(9, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2floor"))));
								GridComm2.TextMatrix(10, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2perimeter"))));
								GridComm2.TextMatrix(11, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2heat"))));
								GridComm2.TextMatrix(12, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2built"))));
								GridComm2.TextMatrix(13, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2remodel"))));
								GridComm2.TextMatrix(14, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2condition"))));
								GridComm2.TextMatrix(15, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("c2phys"))));
								GridComm2.TextMatrix(16, 2, FCConvert.ToString(Conversion.Val(clsOrigDwellComm.Get_Fields_Int32("C2funct"))));
							}
							// gray out anything that hasn't changed
							for (x = 1; x <= 16; x++)
							{
								if (GridComm2.TextMatrix(x, 1) == GridComm2.TextMatrix(x, 2))
								{
									GridComm2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 1, x, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
							}
							// x
							// Else
							// no commercial changes
							// SSTab1.TabEnabled(1) = False
							// End If
						}
					}
				}
				clsDwellComm = null;
				clsOrigDwellComm = null;
				// fill outbuilding stuff
				clsOut.OpenRecordset("select * from tbloutbuilding where masterid = " + FCConvert.ToString(lngMasterID) + " and not yideleted order by id", "retempim.vb1");
				clsOrigOut.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				intSeq = 0;
				if (!clsOut.EndOfFile())
				{
					while (!clsOut.EndOfFile())
					{
						// If clsOut.Fields("YIChanged") = True Then
						intSeq += 1;
						if (intSeq < 11)
						{
							// TODO Get_Fields: Field [buildingtype] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [yrbuilt] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [qualityid] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [gradepercent] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [conditionid] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [depperc] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [functionalpercent] not found!! (maybe it is an alias?)
							GridOutbuilding.AddItem("\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("buildingtype"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("yrbuilt"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields_Int32("sqft"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("qualityid"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("gradepercent"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("conditionid"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("depperc"))) + "\t" + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("functionalpercent"))));
							// .IsSubtotal(1) = True
							// 
							// .RowOutlineLevel(2) = 1
							// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, 2, 8) = SetGridColor(1)
							GridOutbuilding.IsSubtotal(GridOutbuilding.Rows - 1, true);
							if (!clsOrigOut.EndOfFile())
							{
								// intTemp = Val(clsOut.Fields("seqnumber"))
								intTemp = intSeq;
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
								GridOutbuilding.AddItem("\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oitype" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oiyear" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oiunits" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oigradecd" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oigradepct" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oicond" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oipctphys" + FCConvert.ToString(intTemp)))) + "\t" + FCConvert.ToString(Conversion.Val(clsOrigOut.Get_Fields("oipctfunct" + FCConvert.ToString(intTemp)))));
							}
							else
							{
								GridOutbuilding.AddItem("");
							}
							GridOutbuilding.RowOutlineLevel(GridOutbuilding.Rows - 1, 1);
							GridOutbuilding.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridOutbuilding.Rows - 1, 1, GridOutbuilding.Rows - 1, 8, modColorScheme.SetGridColor(1));
						}
						clsOut.MoveNext();
					}
					// Else
					// SSTab1.TabEnabled(2) = False
					// End If
				}
				else
				{
					SSTab1.TabPages[2].Enabled = false;
				}
				strTemp = FCFileSystem.Dir(strDataPath + "files\\" + Strings.Format(lngAccount, "000000") + Strings.Format(intCard, "00") + ".wav", 0);
				// Call clsSounds.OpenRecordset("select * from tblpicturessketches where masterid = " & lngMasterID & " and pstype = 'S' and (psnew = true or pschanged = true) order by id ", "RETempIm.vb1")
				// If clsSounds.EndOfFile Then
				if (Strings.Trim(strTemp) == string.Empty)
				{
					// nothing new to deal with
					SSTab1.TabPages[5].Enabled = false;
				}
				else
				{
					SSTab1.TabPages[5].Enabled = true;
					// modSound.LoadSoundFile (app.path & "\rehandheld\files\" & clsSounds.Fields("id") & ".wav")
					MMControl1.FileName = strDataPath + "files\\" + strTemp;
					strWavName = MMControl1.FileName;
					MMControl1.Command = "Open";
					clsSounds.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and pstype = 'W' and (psdeleted = true)", "RETempIm.vb1");
					if (clsSounds.EndOfFile())
					{
						chkWavFile.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkWavFile.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				FillInfo = true;
				return FillInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Info", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillInfo;
		}

		private void SetupGridMisc()
		{
			GridMisc.TextMatrix(0, 0, "");
			GridMisc.TextMatrix(0, 1, "New");
			GridMisc.TextMatrix(0, 2, "Old");
			GridMisc.TextMatrix(1, 0, "Neighborhood");
			GridMisc.TextMatrix(2, 0, "Tree Growth (Year)");
			GridMisc.TextMatrix(3, 0, "X Coord");
			GridMisc.TextMatrix(4, 0, "Y Coord");
			GridMisc.TextMatrix(5, 0, "Zoning Use");
			GridMisc.TextMatrix(6, 0, "Secondary Zone");
			GridMisc.TextMatrix(7, 0, "Topography");
			GridMisc.TextMatrix(8, 0, "Utilities");
			GridMisc.TextMatrix(9, 0, "Street");
			GridMisc.TextMatrix(10, 0, "Open 1");
			GridMisc.TextMatrix(11, 0, "Open 2");
			// temporary
			// .TextMatrix(0, 1) = 40
			// .TextMatrix(1, 1) = 2001
			// .TextMatrix(4, 1) = 14
			// .TextMatrix(5, 1) = 52
			// .TextMatrix(6, 1) = 10
			// .TextMatrix(7, 1) = 10
			// .TextMatrix(8, 1) = 2
			// .TextMatrix(9, 1) = 2
			// .TextMatrix(10, 1) = 1281
		}

		private void SetupGridLand()
		{
			shapeLand.BackColor = ColorTranslator.FromOle(FCConvert.ToInt32(modColorScheme.SetGridColor(1)));
			GridLand.Rows = 1;
			GridLand.TextMatrix(0, 1, "Type");
			GridLand.TextMatrix(0, 2, "Units");
			GridLand.TextMatrix(0, 3, "Units");
			GridLand.TextMatrix(0, 4, "Influence");
			GridLand.TextMatrix(0, 5, "Code");
			// temporary
			// .IsSubtotal(1) = True
			// .RowOutlineLevel(2) = 1
			// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, 2, 5) = SetGridColor(1)
			// 
		}

		private void ResizeGridLand()
		{
			int GridWidth = 0;
			GridWidth = GridLand.WidthOriginal;
			GridLand.ColWidth(0, FCConvert.ToInt32(0.04 * GridWidth));
			GridLand.ColWidth(1, FCConvert.ToInt32(0.16 * GridWidth));
			GridLand.ColWidth(2, FCConvert.ToInt32(0.2 * GridWidth));
			GridLand.ColWidth(3, FCConvert.ToInt32(0.2 * GridWidth));
			GridLand.ColWidth(4, FCConvert.ToInt32(0.2 * GridWidth));
			GridLand.ColWidth(5, FCConvert.ToInt32(0.16 * GridWidth));
		}

		private void ResizeGridMisc()
		{
			int GridWidth = 0;
			GridWidth = GridMisc.WidthOriginal;
			GridMisc.ColWidth(0, FCConvert.ToInt32(0.6 * GridWidth));
			GridMisc.ColWidth(1, FCConvert.ToInt32(0.2 * GridWidth));
			GridMisc.ColWidth(2, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void SetupGridDwellings()
		{
			GridDwelling1.TextMatrix(0, 1, "New");
			GridDwelling1.TextMatrix(0, 2, "Old");
			GridDwelling1.TextMatrix(1, 0, "Building Style");
			GridDwelling1.TextMatrix(2, 0, "Dwelling Units");
			GridDwelling1.TextMatrix(3, 0, "Other Units");
			GridDwelling1.TextMatrix(4, 0, "Stories");
			GridDwelling1.TextMatrix(5, 0, "Exterior Walls");
			GridDwelling1.TextMatrix(6, 0, "Roof Surface");
			GridDwelling1.TextMatrix(7, 0, "S/F Masonry Trim");
			clsDRWrapper temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1520);
			modDataTypes.Statics.OUT = temp;
			if (Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "") != string.Empty)
			{
				GridDwelling1.TextMatrix(8, 0, modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "");
			}
			else
			{
				GridDwelling1.TextMatrix(8, 0, "Open 3");
			}
			temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1530);
			modDataTypes.Statics.CR = temp;
			if (Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "") != string.Empty)
			{
				GridDwelling1.TextMatrix(9, 0, modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "");
			}
			else
			{
				GridDwelling1.TextMatrix(9, 0, "Open 4");
			}
			GridDwelling1.TextMatrix(10, 0, "Year Built");
			GridDwelling1.TextMatrix(11, 0, "Year Remodeled");
			GridDwelling1.TextMatrix(12, 0, "Foundation");
			GridDwelling1.TextMatrix(13, 0, "Basement");
			GridDwelling1.TextMatrix(14, 0, "Bsmt Garage");
			GridDwelling1.TextMatrix(15, 0, "Wet Basement");
			GridDwelling1.TextMatrix(16, 0, "SF Basement Living");
			GridDwelling1.TextMatrix(17, 0, "Finished Bsmt Grade");
			GridDwelling1.TextMatrix(18, 0, "Finished Bsmt Factor");
			// 
			temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1570);
			modDataTypes.Statics.CR = temp;
			if (Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "") != string.Empty)
			{
				GridDwelling1.TextMatrix(19, 0, Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + ""));
			}
			else
			{
				GridDwelling1.TextMatrix(19, 0, "Open 5");
			}
			GridDwelling1.TextMatrix(20, 0, "Heat Type");
			GridDwelling1.TextMatrix(21, 0, "Percent Heated");
			GridDwelling1.TextMatrix(22, 0, "Cool Type");
			GridDwelling1.TextMatrix(23, 0, "Percent Cooled");
			GridDwelling1.TextMatrix(24, 0, "Kitchen Style");
			GridDwelling1.TextMatrix(25, 0, "Bath(s) Style");
			GridDwelling1.TextMatrix(26, 0, "No. of Rooms");
			GridDwelling1.TextMatrix(27, 0, "No. of Bedrooms");
			GridDwelling1.TextMatrix(28, 0, "No. of Full Baths");
			GridDwelling1.TextMatrix(29, 0, "No. of Half Baths");
			GridDwelling1.TextMatrix(30, 0, "No. of Additional Fixtures");
			GridDwelling1.TextMatrix(31, 0, "No. of Fireplaces");
			GridDwelling1.TextMatrix(32, 0, "Layout");
			GridDwelling1.TextMatrix(33, 0, "Attic");
			GridDwelling1.TextMatrix(34, 0, "Insulation");
			GridDwelling1.TextMatrix(35, 0, "Percent Unfinished");
			GridDwelling1.TextMatrix(36, 0, "Grade");
			GridDwelling1.TextMatrix(37, 0, "Factor");
			GridDwelling1.TextMatrix(38, 0, "Square Footage");
			GridDwelling1.TextMatrix(39, 0, "Condition");
			GridDwelling1.TextMatrix(40, 0, "Physical % Good");
			GridDwelling1.TextMatrix(41, 0, "Functional % Good");
			GridDwelling1.TextMatrix(42, 0, "Functional Code");
			GridDwelling1.TextMatrix(43, 0, "Economic % Good");
			GridDwelling1.TextMatrix(44, 0, "Economic Code");
		}

		private void ResizeDwellingGrids()
		{
			int GridWidth = 0;
			GridWidth = GridDwelling1.WidthOriginal;
			GridDwelling1.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.56));
			GridDwelling1.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.2));
			GridDwelling1.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.2));
			// .Height = .RowHeight(0) * GridDwelling1.Rows + 60
		}

		private void ResizePicSketches()
		{
			int GridWidth = 0;
			int GridHeight = 0;
			int x;
			GridHeight = GridPics.HeightOriginal;
			GridWidth = GridPics.WidthOriginal;
			GridPics.RowHeightMin = FCConvert.ToInt32(0.33 * GridHeight);
			GridPics.ColWidth(0, FCConvert.ToInt32(0.32 * GridWidth));
			GridPics.ColWidth(1, FCConvert.ToInt32(0.32 * GridWidth));
			GridPics.ColWidth(2, FCConvert.ToInt32(0.32 * GridWidth));
			framImage.Width = GridPics.ColWidth(0);
			framImage.Height = GridPics.RowHeightMin;
			Image2.SizeMode = PictureBoxSizeMode.StretchImage;
			Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			Image2.Width = framImage.Width;
			Image2.Height = framImage.Height;
			// Image1.Width = .ColWidth(0)
			// Image1.Height = .RowHeightMin
			// Image1.Stretch = True
			GridHeight = GridSketches.HeightOriginal;
			GridWidth = GridSketches.WidthOriginal;
			GridSketches.RowHeightMin = FCConvert.ToInt32(0.33 * GridHeight);
			GridSketches.ColWidth(0, FCConvert.ToInt32(0.32 * GridWidth));
			GridSketches.ColWidth(1, FCConvert.ToInt32(0.32 * GridWidth));
			GridSketches.ColWidth(2, FCConvert.ToInt32(0.32 * GridWidth));
			framSketchDragIcon.Width = GridSketches.ColWidth(0);
			framSketchDragIcon.Height = GridSketches.RowHeightMin;
			ImDragImage.SizeMode = PictureBoxSizeMode.StretchImage;
			SketchCopyImage.SizeMode = PictureBoxSizeMode.StretchImage;
			ImDragImage.Width = framSketchDragIcon.Width;
			ImDragImage.Height = framSketchDragIcon.Height;
			GridHeight = GridNewPics.HeightOriginal;
			GridWidth = GridNewPics.WidthOriginal;
			GridNewPics.RowHeightMin = GridHeight;
			GridNewPics.ColWidth(0, FCConvert.ToInt32(0.49 * GridWidth));
			GridNewPics.ColWidth(1, FCConvert.ToInt32(0.49 * GridWidth));
			// .ColWidth(2) = 0.32 * GridWidth
			GridHeight = GridNewSketches.HeightOriginal;
			GridWidth = GridNewSketches.WidthOriginal;
			GridNewSketches.RowHeightMin = GridHeight;
			GridNewSketches.ColWidth(0, FCConvert.ToInt32(0.49 * GridWidth));
			GridNewSketches.ColWidth(1, FCConvert.ToInt32(0.49 * GridWidth));
			// With GridPictures
			// GridWidth = .Width
			// GridHeight = .Height
			// .ColWidth(0) = 0.08 * GridWidth
			// 
			// .RowHeightMin = GridHeight * 0.7
			// 
			// If .Cols > 1 Then
			// .ColWidth(1) = 0.08 * GridWidth
			// .ColWidth(2) = .RowHeightMin * 1.5
			// .ColWidth(2) = (0.7 * GridHeight) * 1.5
			// End If
			// 
			// For x = 1 To .Rows - 1
			// .RowHeight(x) = 0.7 * GridHeight
			// Next x
			// End With
			// With GridSketches
			// GridWidth = .Width
			// GridHeight = .Height
			// .ColWidth(0) = 0.08 * GridWidth
			// .RowHeightMin = GridHeight * 0.7
			// If .Cols > 1 Then
			// .ColWidth(1) = 0.08 * GridWidth
			// .ColWidth(2) = (0.7 * GridHeight) * 1.5
			// End If
			// 
			// For x = 1 To .Rows - 1
			// .RowHeight(x) = 0.7 * GridHeight
			// Next x
			// End With
		}

		private void SetupPicSketches(ref string strPath)
		{
			//FileSystemObject fso = new FileSystemObject();
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsLoadFromHH = new clsDRWrapper();
			// vbPorter upgrade warning: intNumRows As short --> As int	OnWrite(int, double)
			int intNumRows = 0;
			int intNumPics = 0;
			int intTotalPics = 0;
			int x = 0;
			int y = 0;
			string strPicPath = "";
			string strSketchList = "";
			string[] strSketchListArray = null;
			int intSketchCount;
			int intSeqNumber = 0;
			bool boolSeqIsCorrect;
			clsDRWrapper clsTemp = new clsDRWrapper();
			boolSeqIsCorrect = false;
			string strTemp;
			strTemp = clsLoad.MakeODBCConnectionStringForAccess(strPath + "RETempIm.vb1");
			clsLoad.AddConnection(strTemp, "RETempIm.vb1", FCConvert.ToInt32(dbConnectionTypes.ODBC));
			// clsLoadFromHH.Path = strPath
			// clsTemp.Path = strPath
			clsLoadFromHH.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and (pstype = 'ps' or pstype = 'PS')", "retempim.vb1");
			if (clsLoadFromHH.EndOfFile())
			{
				clsLoad.OpenRecordset("select * from picturerecord where MRaccountnumber = " + FCConvert.ToString(lngAccount) + " and rscard = " + FCConvert.ToString(intCard) + " order by picnum", modGlobalVariables.strREDatabase);
				clsLoadFromHH.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and (pstype = 'P' or pstype = 'p') and (psnew = true or pschanged = true) order by id ", "RETempIm.vb1");
				clsTemp.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and (pstype = 'P' or pstype = 'p') and psnew = false and pschanged = false order by id", "retempim.vb1");
			}
			else
			{
				boolSeqIsCorrect = true;
				clsLoadFromHH.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and (pstype = 'p' or pstype = 'P') order by seqnumber", "retempim.vb1");
			}
			if (clsLoadFromHH.EndOfFile())
			{
				// nothing new to deal with
				clsLoad = null;
				clsLoadFromHH = null;
				SSTab1.TabPages[3].Enabled = false;
			}
			// .FixedRows = 1
			// 
			// .Cols = 5
			// .ColHidden(4) = True
			// .ColDataType(0) = flexDTBoolean
			// .ColDataType(1) = flexDTBoolean
			// .Rows = clsLoad.RecordCount + 1
			// .TextMatrix(0, 0) = "Delete"
			// .TextMatrix(0, 1) = "Import"
			// .TextMatrix(0, 3) = "Description"
			// .Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, 1, 2, .Rows - 1, 2) = flexPicAlignStretch
			// .ColHidden(4) = True
			// .ColDataType(0) = flexDTBoolean
			// .ColDataType(1) = flexDTBoolean
			// For x = 1 To .Rows - 1
			// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 1, x, 1) = TRIOCOLORGRAYBACKGROUND
			// try to load picture.
			// strPath = GetFullPictureName(clsLoad.Fields("picturelocation"))
			// If strPath <> vbNullString Then
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, x, 2) = LoadPicture(clsLoad.Fields("PICTURELOCATION"))
			// .TextMatrix(x, 3) = clsLoad.Fields("description")
			// .TextMatrix(x, 4) = clsLoad.Fields("picturelocation")
			// .TextMatrix(x, 0) = False
			// If UBound(FileNameList) >= 0 Then
			// For y = 0 To UBound(FileNameList)
			// If UCase(strPath) = UCase(FileNameList(y)) Then .TextMatrix(x, 0) = True
			// Next y
			// End If
			// Else
			// .TextMatrix(x, 0) = False
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, x, 1) = Nothing
			// .TextMatrix(x, 2) = "Could not load Picture"
			// End If
			// 
			// clsLoad.MoveNext
			// Next x
			// 
			// now load new pictures
			// temporary
			// .AddItem (False & vbTab & True & vbTab & vbTab & "New Photo" & vbNewLine & "Front View 1/20/2003")
			// .AddItem (False & vbTab & True & vbTab & vbTab & "New Photo" & vbNewLine & "Side View 1/20/2003")
			// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, .Rows - 2, 0, .Rows - 1, 0) = TRIOCOLORGRAYBACKGROUND
			// .Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, .Rows - 2, 2, .Rows - 1, 2) = flexPicAlignStretch
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, .Rows - 2, 2) = LoadPicture(App.Path & "\rehandheld\25-145.jpg")
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, .Rows - 1, 2) = LoadPicture(App.Path & "\rehandheld\25-39.jpg")
			// End If
			// End With
			// 
			// With GridSketches
			// Call clsLoadFromHH.OpenRecordset("select * from tblpicturessketches where masterid = " & lngMasterID & " and pstype = 'S' and ((psnew = true) or (pschanged = true)) order by id ", "RETempIm.vb1")
			// If clsLoadFromHH.EndOfFile Then
			// .Rows = 1
			// .Cols = 1
			// .ColDataType(0) = flexDTString
			// .TextMatrix(0, 0) = "No changes in sketches for this card"
			// Else
			// End If
			// End With
			intNumPics = 0;
			intNumRows = 0;
			if (!boolSeqIsCorrect)
			{
				if (!clsLoad.EndOfFile())
				{
					intNumPics = clsLoad.RecordCount();
					intNumRows = FCConvert.ToInt32(Conversion.Int(intNumPics / 3.0));
					if ((intNumPics / 3.0) > intNumRows)
					{
						intNumRows += 1;
					}
				}
			}
			GridPics.Rows = intNumRows;
			fecherFoundation.Extensions.ArrayExtensions.ResizeArray(ref aryPicInfo, 3 + 1, intNumRows + 1);
			x = 0;
			y = 0;
			if (!boolSeqIsCorrect)
			{
				while (!clsLoad.EndOfFile())
				{
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, x, y, FCGrid.PictureAlignmentSettings.flexPicAlignStretch);
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpAlignment, x, y, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					strPicPath = modGlobalRoutines.GetFullPictureName(clsLoad.Get_Fields_String("picturelocation"));
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpForeColor, x, y, modGlobalConstants.Statics.TRIOCOLORRED);
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpFontSize, x, y, 24);
					if (strPicPath != "")
					{
						GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, x, y, FCUtils.LoadPicture(strPicPath));
					}
					else
					{
						GridPics.TextMatrix(x, y, "Could not load.");
					}
					aryPicInfo[y, x].boolNew = false;
					aryPicInfo[y, x].boolDeleted = false;
					aryPicInfo[y, x].boolIstheAddCell = false;
					aryPicInfo[y, x].boolIsTheDeleteCell = false;
					aryPicInfo[y, x].boolChanged = false;
					aryPicInfo[y, x].SequenceNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("picnum"));
					aryPicInfo[y, x].boolNotEmpty = true;
					aryPicInfo[y, x].lngID = 0;
					if (!clsTemp.EndOfFile())
					{
						// Call clsTemp.FindFirstRecord("path", clsLoad.Fields("picturelocation"))
						if (clsTemp.FindFirst("path = " + clsLoad.Get_Fields_String("Picturelocation")))
						{
							// If Not clsTemp.NoMatch Then
							aryPicInfo[y, x].lngID = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("id"));
						}
					}
					aryPicInfo[y, x].FileName = FCConvert.ToString(clsLoad.Get_Fields_String("picturelocation"));
					if (y == 2)
					{
						x += 1;
						y = 0;
					}
					else
					{
						y += 1;
					}
					clsLoad.MoveNext();
				}
			}
			// now load the new pics from the handheld
			// also load the changed pics
			// don't load any pics not marked as new or changed
			// they are original pics
			if (!clsLoadFromHH.EndOfFile())
			{
				// should not be in this code if it is end of file, but just in case
				intTotalPics = intNumPics + clsLoadFromHH.RecordCount();
				intNumRows = FCConvert.ToInt32(intTotalPics / 3.0);
				if ((intTotalPics / 3.0) > intNumRows)
					intNumRows += 1;
				GridPics.Rows = intNumRows;
				fecherFoundation.Extensions.ArrayExtensions.ResizeArray(ref aryPicInfo, 3 + 1, intNumRows + 1);
				// grid and array are set up, now fill them
				while (!clsLoadFromHH.EndOfFile())
				{
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, x, y, FCGrid.PictureAlignmentSettings.flexPicAlignStretch);
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpAlignment, x, y, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpForeColor, x, y, modGlobalConstants.Statics.TRIOCOLORRED);
					GridPics.Cell(FCGrid.CellPropertySettings.flexcpFontSize, x, y, 24);
					if (!boolSeqIsCorrect)
					{
						strPicPath = strPath + "files\\" + clsLoadFromHH.Get_Fields_Int32("id") + ".jpg";
					}
					else
					{
						// TODO Get_Fields: Field [psnew] not found!! (maybe it is an alias?)
						if (FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("psnew")))
						{
							strPicPath = strPath + "files\\" + clsLoadFromHH.Get_Fields_Int32("id") + ".jpg";
						}
						else
						{
							// TODO Get_Fields: Field [path] not found!! (maybe it is an alias?)
							strPicPath = FCConvert.ToString(clsLoadFromHH.Get_Fields("path"));
						}
					}
					if (File.Exists(strPicPath))
					{
						GridPics.TextMatrix(x, y, "New");
						// TODO Get_Fields: Field [psnew] not found!! (maybe it is an alias?)
						aryPicInfo[y, x].boolNew = FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("psnew"));
						// TODO Get_Fields: Field [psdeleted] not found!! (maybe it is an alias?)
						aryPicInfo[y, x].boolDeleted = FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("psdeleted"));
						aryPicInfo[y, x].boolIstheAddCell = false;
						aryPicInfo[y, x].boolIsTheDeleteCell = false;
						// TODO Get_Fields: Field [pschanged] not found!! (maybe it is an alias?)
						aryPicInfo[y, x].boolChanged = FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("pschanged"));
						aryPicInfo[y, x].lngID = FCConvert.ToInt32(clsLoadFromHH.Get_Fields_Int32("id"));
						if (!boolSeqIsCorrect)
						{
							aryPicInfo[y, x].SequenceNumber = 0;
							aryPicInfo[y, x].FileName = "";
						}
						else
						{
							// TODO Get_Fields: Field [seqnumber] not found!! (maybe it is an alias?)
							aryPicInfo[y, x].SequenceNumber = FCConvert.ToInt32(clsLoadFromHH.Get_Fields("seqnumber"));
							// TODO Get_Fields: Field [path] not found!! (maybe it is an alias?)
							aryPicInfo[y, x].FileName = FCConvert.ToString(clsLoadFromHH.Get_Fields("path"));
						}
						aryPicInfo[y, x].boolNotEmpty = true;
						GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, x, y, FCUtils.LoadPicture(strPicPath));
						if (y == 2)
						{
							x += 1;
							y = 0;
						}
						else
						{
							y += 1;
						}
					}
					clsLoadFromHH.MoveNext();
				}
			}
			// .Rows = 2
			// .Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, 0, 0, 1, 2) = flexPicAlignStretch
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 0, 0) = LoadPicture(App.Path & "\rehandheld\front.jpg")
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 0, 1) = LoadPicture(App.Path & "\rehandheld\backyard.jpg")
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 0, 2) = LoadPicture(App.Path & "\rehandheld\frontright.jpg")
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0) = LoadPicture(App.Path & "\rehandheld\driveway.jpg")
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 1) = LoadPicture(App.Path & "\rehandheld\left.jpg")
			// .TextMatrix(1, 2) = "New"
			// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2) = flexAlignCenterCenter
			GridNewPics.Rows = 1;
			GridNewPics.Cols = 2;
			// .Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, 0, 0, 0, 2) = flexPicAlignStretch
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 0, 0) = LoadPicture(App.Path & "\rehandheld\25-145.jpg")
			// .Cell(FCGrid.CellPropertySettings.flexcpPicture, 0, 1) = LoadPicture(App.Path & "\rehandheld\25-39.jpg")
			// .TextMatrix(0, 2) = ""
			GridNewPics.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 1, modGlobalConstants.Statics.TRIOCOLORRED);
			GridNewPics.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, 1, 24);
			GridNewPics.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GridNewPics.TextMatrix(0, 0, "Remove");
			GridNewPics.TextMatrix(0, 1, "Add/Restore");
			// 
			// now do the same for sketches
			// clsLoadFromHH.Path = strPath
			clsLoadFromHH.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and pstype = 'S' and (psnew = true or pschanged = true) order by id ", "RETempIm.vb1");
			if (clsLoadFromHH.EndOfFile())
			{
				// nothing new to deal with
				clsLoad = null;
				clsLoadFromHH = null;
				SSTab1.TabPages[4].Enabled = false;
			}
			intNumPics = 0;
			intNumRows = 0;
			strSketchList = GetSketchList(ref lngMasterID);
			if (!(Strings.Trim(strSketchList) == string.Empty))
			{
				strSketchListArray = Strings.Split(strSketchList, ",", -1, CompareConstants.vbTextCompare);
				intNumPics = Information.UBound(strSketchListArray, 1) + 1;
				intNumRows = FCConvert.ToInt32(Conversion.Int(intNumPics / 3.0));
				if ((intNumPics / 3.0) > intNumRows)
				{
					intNumRows += 1;
				}
			}
			GridSketches.Rows = intNumRows;
			fecherFoundation.Extensions.ArrayExtensions.ResizeArray(ref arySketchesInfo, 3 + 1, intNumRows + 1);
			x = 0;
			y = 0;
			for (intSketchCount = 0; intSketchCount <= intNumPics - 1; intSketchCount++)
			{
				GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, x, y, FCGrid.PictureAlignmentSettings.flexPicAlignStretch);
				GridSketches.Cell(FCGrid.CellPropertySettings.flexcpAlignment, x, y, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				strPicPath = strSketchListArray[intSketchCount];
				GridSketches.Cell(FCGrid.CellPropertySettings.flexcpForeColor, x, y, modGlobalConstants.Statics.TRIOCOLORRED);
				GridSketches.Cell(FCGrid.CellPropertySettings.flexcpFontSize, x, y, 24);
				if (strPicPath != "")
				{
					GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, x, y, FCUtils.LoadPicture(strPicPath));
				}
				else
				{
					GridSketches.TextMatrix(x, y, "Could not load.");
				}
				arySketchesInfo[y, x].boolNew = false;
				arySketchesInfo[y, x].boolDeleted = false;
				arySketchesInfo[y, x].boolIstheAddCell = false;
				arySketchesInfo[y, x].boolIsTheDeleteCell = false;
				arySketchesInfo[y, x].boolChanged = false;
				// thesequencenumber is in hex and is the last character before the .bmp
				// so take the right 5 chars and take the first char from that then convert to decimal
				intSeqNumber = 0;
				if (FCConvert.ToInt32(Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1))) >= 1 && FCConvert.ToInt32(Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1))) <= 9)
				{
					intSeqNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(Strings.Right(strPicPath, 5), 1))));
				}
				else if (Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1)) == "A")
				{
					intSeqNumber = 10;
				}
				else if (Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1)) == "B")
				{
					intSeqNumber = 11;
				}
				else if (Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1)) == "C")
				{
					intSeqNumber = 12;
				}
				else if (Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1)) == "D")
				{
					intSeqNumber = 13;
				}
				else if (Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1)) == "E")
				{
					intSeqNumber = 14;
				}
				else if (Strings.UCase(Strings.Left(Strings.Right(strPicPath, 5), 1)) == "F")
				{
					intSeqNumber = 15;
				}
				arySketchesInfo[y, x].SequenceNumber = intSeqNumber;
				arySketchesInfo[y, x].boolNotEmpty = true;
				if (y == 2)
				{
					x += 1;
					y = 0;
				}
				else
				{
					y += 1;
				}
			}
			// intSketchCount
			// now for the new sketches
			// also load the changed sketches
			// don't load any sketches not marked as new or changed
			// they are original sketches
			if (!clsLoadFromHH.EndOfFile())
			{
				// should not be in this code if it is end of file, but just in case
				intTotalPics = intNumPics + clsLoadFromHH.RecordCount();
				intNumRows = FCConvert.ToInt32(intTotalPics / 3.0);
				if ((intTotalPics / 3.0) > intNumRows)
					intNumRows += 1;
				GridSketches.Rows = intNumRows;
				fecherFoundation.Extensions.ArrayExtensions.ResizeArray(ref arySketchesInfo, 3 + 1, intNumRows + 1);
				// grid and array are set up, now fill them
				while (!clsLoadFromHH.EndOfFile())
				{
					GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, x, y, FCGrid.PictureAlignmentSettings.flexPicAlignStretch);
					GridSketches.Cell(FCGrid.CellPropertySettings.flexcpAlignment, x, y, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					GridSketches.Cell(FCGrid.CellPropertySettings.flexcpForeColor, x, y, modGlobalConstants.Statics.TRIOCOLORRED);
					GridSketches.Cell(FCGrid.CellPropertySettings.flexcpFontSize, x, y, 24);
					strPicPath = strPath + "files\\" + clsLoadFromHH.Get_Fields_Int32("id") + ".bmp";
					if (File.Exists(strPicPath))
					{
						GridSketches.TextMatrix(x, y, "New");
						// TODO Get_Fields: Field [psnew] not found!! (maybe it is an alias?)
						arySketchesInfo[y, x].boolNew = FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("psnew"));
						// TODO Get_Fields: Field [psdeleted] not found!! (maybe it is an alias?)
						arySketchesInfo[y, x].boolDeleted = FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("psdeleted"));
						arySketchesInfo[y, x].boolIstheAddCell = false;
						arySketchesInfo[y, x].boolIsTheDeleteCell = false;
						// TODO Get_Fields: Field [pschanged] not found!! (maybe it is an alias?)
						arySketchesInfo[y, x].boolChanged = FCConvert.ToBoolean(clsLoadFromHH.Get_Fields("pschanged"));
						arySketchesInfo[y, x].SequenceNumber = 0;
						arySketchesInfo[y, x].boolNotEmpty = true;
						GridSketches.Cell(FCGrid.CellPropertySettings.flexcpPicture, x, y, FCUtils.LoadPicture(strPicPath));
						if (y == 2)
						{
							x += 1;
							y = 0;
						}
						else
						{
							y += 1;
						}
					}
					clsLoadFromHH.MoveNext();
				}
			}
			GridNewSketches.Rows = 1;
			GridNewSketches.Cols = 2;
			GridNewSketches.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 1, modGlobalConstants.Statics.TRIOCOLORRED);
			GridNewSketches.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, 1, 24);
			GridNewSketches.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GridNewSketches.TextMatrix(0, 0, "Remove");
			GridNewSketches.TextMatrix(0, 1, "Add/Restore");
		}

		private void GridPics_BeforeMouseDown(object sender, MouseEventArgs e)
		{
			//if (e.Cancel) return;
			// if the click was on the active cell, start dragging
			// If .MouseRow = .Row And .MouseCol = .Col Then
			GridPics.Row = GridPics.MouseRow;
			GridPics.Col = GridPics.MouseCol;
			if (GridPics.Row >= 0 && GridPics.Col >= 0)
			{
				if (aryPicInfo[GridPics.Col, GridPics.Row].boolNotEmpty)
				{
					// use OLEDrag method to start manual OLE drag operation
					// this will fire the OLEStartDrag event, which we will use
					// to fill the DataObject with the data we want to drag.
					// .OLEDrag
					// Image1.Picture = .Cell(FCGrid.CellPropertySettings.flexcpPicture, .Row, .Col)
					// Image1.Visible = True
					// .Drag
					intSourceRow = GridPics.Row;
					intSourceCol = GridPics.Col;
					boolDragDrop = true;
					framImage.Visible = true;
					Image2.Image = GridPics.Cell(FCGrid.CellPropertySettings.flexcpPicture, GridPics.Row, GridPics.Col);
					// Cancel = True
				}
				// tell grid control to ignore mouse movements until the
				// mouse button goes up again
				// Cancel = True
			}
		}

		private void SetupGridOutbuilding()
		{
			shapeOutbuilding.BackColor = ColorTranslator.FromOle(FCConvert.ToInt32(modColorScheme.SetGridColor(1)));
			GridOutbuilding.Rows = 1;
			GridOutbuilding.TextMatrix(0, 1, "Code");
			GridOutbuilding.TextMatrix(0, 2, "Year");
			GridOutbuilding.TextMatrix(0, 3, "Units");
			GridOutbuilding.TextMatrix(0, 4, "Grade");
			GridOutbuilding.TextMatrix(0, 5, "Factor");
			GridOutbuilding.TextMatrix(0, 6, "Condition");
			GridOutbuilding.TextMatrix(0, 7, "Physical");
			GridOutbuilding.TextMatrix(0, 8, "Functional");
		}

		private void ResizeGridOutbuilding()
		{
			int GridWidth = 0;
			GridWidth = GridOutbuilding.WidthOriginal;
			GridOutbuilding.ColWidth(0, FCConvert.ToInt32(0.03 * GridWidth));
			GridOutbuilding.ColWidth(1, FCConvert.ToInt32(0.1 * GridWidth));
			GridOutbuilding.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			GridOutbuilding.ColWidth(3, FCConvert.ToInt32(0.1 * GridWidth));
			GridOutbuilding.ColWidth(4, FCConvert.ToInt32(0.1 * GridWidth));
			GridOutbuilding.ColWidth(5, FCConvert.ToInt32(0.12 * GridWidth));
			GridOutbuilding.ColWidth(6, FCConvert.ToInt32(0.14 * GridWidth));
			GridOutbuilding.ColWidth(7, FCConvert.ToInt32(0.14 * GridWidth));
			GridOutbuilding.ColWidth(8, FCConvert.ToInt32(0.14 * GridWidth));
		}

		private void SetupGridComms()
		{
			GridComm1.TextMatrix(0, 1, "New");
			GridComm1.TextMatrix(0, 2, "Old");
			GridComm1.TextMatrix(1, 0, "Occupancy Code");
			GridComm1.TextMatrix(2, 0, "# of Dwellings");
			GridComm1.TextMatrix(3, 0, "Class");
			GridComm1.TextMatrix(4, 0, "Quality");
			GridComm1.TextMatrix(5, 0, "Grade Factor");
			GridComm1.TextMatrix(6, 0, "Exterior Walls");
			GridComm1.TextMatrix(7, 0, "Stories");
			GridComm1.TextMatrix(8, 0, "Story Height");
			GridComm1.TextMatrix(9, 0, "Base Floor Area");
			GridComm1.TextMatrix(10, 0, "Perimeter/Units");
			GridComm1.TextMatrix(11, 0, "Heating/Cooling");
			GridComm1.TextMatrix(12, 0, "Year Built");
			GridComm1.TextMatrix(13, 0, "Year Remodeled");
			GridComm1.TextMatrix(14, 0, "Condition");
			GridComm1.TextMatrix(15, 0, "Physical % Good");
			GridComm1.TextMatrix(16, 0, "Functional % Good");
			GridComm1.TextMatrix(17, 0, "Economic % Good");
			GridComm2.TextMatrix(0, 1, "New");
			GridComm2.TextMatrix(0, 2, "Old");
			GridComm2.TextMatrix(1, 0, "Occupancy Code");
			GridComm2.TextMatrix(2, 0, "# of Dwellings");
			GridComm2.TextMatrix(3, 0, "Class");
			GridComm2.TextMatrix(4, 0, "Quality");
			GridComm2.TextMatrix(5, 0, "Grade Factor");
			GridComm2.TextMatrix(6, 0, "Exterior Walls");
			GridComm2.TextMatrix(7, 0, "Stories");
			GridComm2.TextMatrix(8, 0, "Story Height");
			GridComm2.TextMatrix(9, 0, "Base Floor Area");
			GridComm2.TextMatrix(10, 0, "Perimeter/Units");
			GridComm2.TextMatrix(11, 0, "Heating/Cooling");
			GridComm2.TextMatrix(12, 0, "Year Built");
			GridComm2.TextMatrix(13, 0, "Year Remodeled");
			GridComm2.TextMatrix(14, 0, "Condition");
			GridComm2.TextMatrix(15, 0, "Physical % Good");
			GridComm2.TextMatrix(16, 0, "Functional % Good");
		}

		private void ResizeGridComms()
		{
			int GridWidth = 0;
			GridWidth = GridComm1.WidthOriginal;
			GridComm1.ColWidth(0, FCConvert.ToInt32(0.6 * GridWidth));
			GridComm1.ColWidth(1, FCConvert.ToInt32(0.2 * GridWidth));
			//GridComm1.Height = GridComm1.RowHeight(0) * GridComm1.Rows + 80;
			GridWidth = GridComm2.WidthOriginal;
			GridComm2.ColWidth(0, FCConvert.ToInt32(0.6 * GridWidth));
			GridComm2.ColWidth(1, FCConvert.ToInt32(0.2 * GridWidth));
			//GridComm2.Height = GridComm2.RowHeight(0) * GridComm2.Rows + 80;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			// save the changes to the changes, then exit
			if (SSTab1.TabPages[0].Enabled)
			{
				// property
				SavePropertyChanges();
			}
			if (SSTab1.TabPages[1].Enabled)
			{
				// dwelling or commercial
				SaveDwellOrCommChanges();
			}
			if (SSTab1.TabPages[2].Enabled)
			{
				// outbuildings
				SaveOutbuildingChanges();
			}
			if (SSTab1.TabPages[3].Enabled)
			{
				// pictures
				SavePictureChanges();
			}
			if (SSTab1.TabPages[4].Enabled)
			{
				// sketches
			}
			if (SSTab1.TabPages[5].Enabled)
			{
				// audio
				SaveSoundChanges();
			}
			mnuExit_Click();
		}

		private void SavePictureChanges()
		{
			// mark deleted files as deleted
			// put original pictures as entries into the database
			// Number all pictures correctly
			// add a record with pstype ps for picture sequence
			// this will mean that the entries have the correct picture sequences
			// so go ahead change your entries accordingly
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strFileName = "";
			string strFullName = "";
			int intRow;
			int intCol;
			int intpic = 0;
			int lngMaxId = 0;
			string strTemp;
			strTemp = clsLoad.MakeODBCConnectionStringForAccess(strDataPath + "RETempIm.vb1");
			clsLoad.AddConnection(strTemp, "RETempIm.vb1", FCConvert.ToInt32(dbConnectionTypes.ODBC));
			// clsSave.Path = strDataPath
			// go through each picture and record the information
			for (intRow = 0; intRow <= GridPics.Rows - 1; intRow++)
			{
				for (intCol = 0; intCol <= 2; intCol++)
				{
					intpic = (3 * intRow) + intCol + 1;
					// now look at the picinfo to determine what to do with it
					// first see if there is an entry for it yet
					if (aryPicInfo[intCol, intRow].lngID > 0)
					{
						clsSave.OpenRecordset("select * from tblpicturessketches where id = " + FCConvert.ToString(aryPicInfo[intCol, intRow].lngID), "retempim.vb1");
						clsSave.Edit();
						clsSave.Set_Fields("seqnumber", intpic);
						clsSave.Set_Fields("psnew", aryPicInfo[intCol, intRow].boolNew);
						clsSave.Set_Fields("pschanged", aryPicInfo[intCol, intRow].boolChanged);
						clsSave.Set_Fields("psdeleted", aryPicInfo[intCol, intRow].boolDeleted);
						clsSave.Update();
					}
					else
					{
						// there is no entry.  This better be an original
						clsSave.OpenRecordset("select max(id) as maxid from tblpicturessketches", "retempim.vb1");
						if (!clsSave.EndOfFile())
						{
							// TODO Get_Fields: Field [maxid] not found!! (maybe it is an alias?)
							lngMaxId = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("maxid"))));
						}
						else
						{
							lngMaxId = 0;
						}
						clsSave.OpenRecordset("select * from tblpicturessketches where masterid = -1", "retempim.vb1");
						clsSave.AddNew();
						clsSave.Set_Fields("masterid", lngMasterID);
						clsSave.Set_Fields("seqnumber", intpic);
						lngMaxId += 1;
						clsSave.Set_Fields("id", lngMaxId);
						clsSave.Set_Fields("path", aryPicInfo[intCol, intRow].FileName);
						clsSave.Set_Fields("pstype", "P");
						aryPicInfo[intCol, intRow].lngID = lngMaxId;
						clsSave.Set_Fields("psnew", false);
						clsSave.Set_Fields("pschanged", aryPicInfo[intCol, intRow].boolChanged);
						clsSave.Set_Fields("psdeleted", aryPicInfo[intCol, intRow].boolDeleted);
						clsSave.Update();
					}
				}
				// intCol
			}
			// intRow
			clsSave.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and pstype = 'PS'", "retempim.vb1");
			if (clsSave.EndOfFile())
			{
				clsSave.AddNew();
				clsSave.Set_Fields("masterid", lngMasterID);
				clsSave.Set_Fields("pstype", "PS");
				clsSave.Update();
			}
		}

		private void SaveSoundChanges()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			if (boolSoundChanged)
			{
				// MMControl1.Command = "Save"
			}
			string strTemp;
			strTemp = clsSave.MakeODBCConnectionStringForAccess(strDataPath + "RETempIm.vb1");
			clsSave.AddConnection(strTemp, "RETempIm.vb1", FCConvert.ToInt32(dbConnectionTypes.ODBC));
			// clsSave.Path = strDataPath
			clsSave.OpenRecordset("select * from tblpicturessketches where masterid = " + FCConvert.ToString(lngMasterID) + " and pstype = 'W'", "retempim.vb1");
			if (!clsSave.EndOfFile())
			{
				clsSave.Edit();
			}
			else
			{
				clsSave.AddNew();
				clsSave.Set_Fields("masterid", lngMasterID);
				clsSave.Set_Fields("pstype", "W");
			}
			if (chkWavFile.CheckState == Wisej.Web.CheckState.Checked)
			{
				clsSave.Set_Fields("psdeleted", false);
			}
			else
			{
				clsSave.Set_Fields("psdeleted", true);
			}
			clsSave.Update();
		}

		private void SaveDwellOrCommChanges()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// clsLoad.Path = strDataPath
			clsLoad.OpenRecordset("select iscommercial,isdwelling from tblparcels where masterid = " + FCConvert.ToString(lngMasterID), "retempim.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [isdwelling] not found!! (maybe it is an alias?)
				if (FCConvert.ToBoolean(clsLoad.Get_Fields("isdwelling")))
				{
					SaveDwellingChanges();
				}
				// TODO Get_Fields: Field [iscommercial] not found!! (maybe it is an alias?)
				else if (clsLoad.Get_Fields("iscommercial"))
				{
					SaveCommercialChanges();
				}
			}
		}

		private void SaveCommercialChanges()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int intSeq;
			// clsSave.Path = strDataPath
			clsSave.OpenRecordset("select * from tblcommercial where masterid = " + FCConvert.ToString(lngMasterID) + " order by id", "retempim.vb1");
			intSeq = 1;
			while (!clsSave.EndOfFile())
			{
				if (intSeq < 3)
				{
					clsSave.Edit();
					if (intSeq == 1)
					{
						clsSave.Set_Fields("occupancycode", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(1, 1))));
						clsSave.Set_Fields("NumberDwellingUnits", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(2, 1))));
						clsSave.Set_Fields("Class", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(3, 1))));
						clsSave.Set_Fields("quality", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(4, 1))));
						clsSave.Set_Fields("Gradefactor", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(5, 1))));
						clsSave.Set_Fields("ExteriorWalls", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(6, 1))));
						clsSave.Set_Fields("NumberOfStories", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(7, 1))));
						clsSave.Set_Fields("StoriesHeight", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(8, 1))));
						clsSave.Set_Fields("BaseFloorArea", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(9, 1))));
						clsSave.Set_Fields("PerimeterUnits", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(10, 1))));
						clsSave.Set_Fields("HeatingCooling", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(11, 1))));
						clsSave.Set_Fields("Yearbuilt", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(12, 1))));
						clsSave.Set_Fields("YearRemodeled", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(13, 1))));
						clsSave.Set_Fields("Condition", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(14, 1))));
						clsSave.Set_Fields("PhysicalPercent", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(15, 1))));
						clsSave.Set_Fields("Functional", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(16, 1))));
						clsSave.Set_Fields("Economic", FCConvert.ToString(Conversion.Val(GridComm1.TextMatrix(17, 1))));
					}
					else
					{
						clsSave.Set_Fields("occupancycode", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(1, 1))));
						clsSave.Set_Fields("NumberDwellingUnits", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(2, 1))));
						clsSave.Set_Fields("Class", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(3, 1))));
						clsSave.Set_Fields("quality", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(4, 1))));
						clsSave.Set_Fields("Gradefactor", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(5, 1))));
						clsSave.Set_Fields("ExteriorWalls", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(6, 1))));
						clsSave.Set_Fields("NumberOfStories", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(7, 1))));
						clsSave.Set_Fields("StoriesHeight", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(8, 1))));
						clsSave.Set_Fields("BaseFloorArea", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(9, 1))));
						clsSave.Set_Fields("PerimeterUnits", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(10, 1))));
						clsSave.Set_Fields("HeatingCooling", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(11, 1))));
						clsSave.Set_Fields("Yearbuilt", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(12, 1))));
						clsSave.Set_Fields("YearRemodeled", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(13, 1))));
						clsSave.Set_Fields("Condition", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(14, 1))));
						clsSave.Set_Fields("PhysicalPercent", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(15, 1))));
						clsSave.Set_Fields("Functional", FCConvert.ToString(Conversion.Val(GridComm2.TextMatrix(16, 1))));
					}
					clsSave.Update();
				}
				clsSave.MoveNext();
				intSeq += 1;
			}
		}

		private void SaveDwellingChanges()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			// clsSave.Path = strDataPath
			clsSave.OpenRecordset("select * from tbldwelling where masterid = " + FCConvert.ToString(lngMasterID), "retempim.vb1");
			if (!clsSave.EndOfFile())
			{
				clsSave.Edit();
				clsSave.Set_Fields("buildingstyle", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(1, 1))));
				clsSave.Set_Fields("dwellingunits", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(2, 1))));
				clsSave.Set_Fields("otherunits", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(3, 1))));
				clsSave.Set_Fields("stories", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(4, 1))));
				clsSave.Set_Fields("extprimwallid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(5, 1))));
				clsSave.Set_Fields("extroofcovid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(6, 1))));
				clsSave.Set_Fields("sfmasonrytrim", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(7, 1))));
				clsSave.Set_Fields("open3", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(8, 1))));
				clsSave.Set_Fields("open4", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(9, 1))));
				clsSave.Set_Fields("yrbuilt", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(10, 1))));
				clsSave.Set_Fields("YearRemodeled", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(11, 1))));
				clsSave.Set_Fields("extfoundid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(12, 1))));
				clsSave.Set_Fields("Basement", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(13, 1))));
				clsSave.Set_Fields("BasementGarage", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(14, 1))));
				clsSave.Set_Fields("WetBasement", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(15, 1))));
				clsSave.Set_Fields("sfbasementliving", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(16, 1))));
				clsSave.Set_Fields("finishedbasementgrade", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(17, 1))));
				clsSave.Set_Fields("finishedbasementfactor", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(18, 1))));
				clsSave.Set_Fields("open5", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(19, 1))));
				clsSave.Set_Fields("heattypeid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(20, 1))));
				clsSave.Set_Fields("PercHeatedID", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(21, 1))));
				clsSave.Set_Fields("acid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(22, 1))));
				clsSave.Set_Fields("PercentCooled", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(23, 1))));
				clsSave.Set_Fields("kratingid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(24, 1))));
				clsSave.Set_Fields("bathstyle", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(25, 1))));
				clsSave.Set_Fields("numrooms", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(26, 1))));
				clsSave.Set_Fields("numbedrooms", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(27, 1))));
				clsSave.Set_Fields("fullbaths", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(28, 1))));
				clsSave.Set_Fields("12baths", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(29, 1))));
				clsSave.Set_Fields("otherfixtures", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(30, 1))));
				clsSave.Set_Fields("fireplaces", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(31, 1))));
				clsSave.Set_Fields("layout", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(32, 1))));
				clsSave.Set_Fields("attic", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(33, 1))));
				clsSave.Set_Fields("insulid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(34, 1))));
				clsSave.Set_Fields("PercentUnfinished", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(35, 1))));
				clsSave.Set_Fields("gradeid", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(36, 1))));
				clsSave.Set_Fields("Factor", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(37, 1))));
				clsSave.Set_Fields("SquareFootage", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(38, 1))));
				clsSave.Set_Fields("Condition", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(39, 1))));
				clsSave.Set_Fields("PhysicalPercent", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(40, 1))));
				clsSave.Set_Fields("FunctPerc", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(41, 1))));
				clsSave.Set_Fields("FunctionalID", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(42, 1))));
				clsSave.Set_Fields("Econperc", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(43, 1))));
				clsSave.Set_Fields("EconomicID", FCConvert.ToString(Conversion.Val(GridDwelling1.TextMatrix(44, 1))));
				clsSave.Update();
			}
		}

		private void SaveOutbuildingChanges()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int intRow;
			// clsSave.Path = strDataPath
			clsSave.OpenRecordset("select * from tbloutbuilding where masterid = " + FCConvert.ToString(lngMasterID) + " and not yideleted order by id", "retempim.vb1");
			intRow = 1;
			while (!clsSave.EndOfFile())
			{
				clsSave.Edit();
				clsSave.Set_Fields("buildingtype", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 1))));
				clsSave.Set_Fields("yrbuilt", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 2))));
				clsSave.Set_Fields("sqft", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 3))));
				clsSave.Set_Fields("qualityid", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 4))));
				clsSave.Set_Fields("gradepercent", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 5))));
				clsSave.Set_Fields("conditionid", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 6))));
				clsSave.Set_Fields("depperc", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 7))));
				clsSave.Set_Fields("functionalpercent", FCConvert.ToString(Conversion.Val(GridOutbuilding.TextMatrix(intRow, 8))));
				clsSave.Update();
				clsSave.MoveNext();
				intRow += 2;
			}
		}

		private void SavePropertyChanges()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			int x;
			string strPhone = "";
			// clsSave.Path = strDataPath
			clsSave.OpenRecordset("select * from tblparcels where masterid = " + FCConvert.ToString(lngMasterID), "retempim.vb1");
			if (!clsSave.EndOfFile())
			{
				clsSave.Edit();
				// name,location info first
				clsSave.Set_Fields("Owner", txtowner.Text);
				clsSave.Set_Fields("SecondOwner", txtSecondOwner.Text);
				clsSave.Set_Fields("Address1", txtAddress1.Text);
				clsSave.Set_Fields("Address2", txtAddress2.Text);
				clsSave.Set_Fields("City", txtCity.Text);
				clsSave.Set_Fields("Zip", txtZip.Text);
				clsSave.Set_Fields("zip4", txtZip4.Text);
				clsSave.Set_Fields("parcelid", txtMapLot.Text);
				clsSave.Set_Fields("legalrefno", txtRef1.Text);
				clsSave.Set_Fields("reference2", txtRef2.Text);
				clsSave.Set_Fields("location", Strings.Trim(txtStreetNumber.Text + " " + txtStreetName.Text));
				strTemp = Strings.Trim(txtTelephone.Text);
				strPhone = "";
				for (x = 1; x <= strTemp.Length; x++)
				{
					if (Information.IsNumeric(Strings.Mid(strTemp, x, 1)))
					{
						strPhone += Strings.Mid(strTemp, x, 1);
					}
				}
				// x
				strPhone = Strings.Format(strPhone, "0000000000");
				clsSave.Set_Fields("ownerphone", strPhone);
				clsSave.Set_Fields("neighborhood", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(1, 1))));
				clsSave.Set_Fields("treegrowth", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(2, 1))));
				clsSave.Set_Fields("XCoord", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(3, 1))));
				clsSave.Set_Fields("YCoord", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(4, 1))));
				clsSave.Set_Fields("Zone", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(5, 1))));
				clsSave.Set_Fields("SecondaryZone", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(6, 1))));
				strTemp = Strings.Format(GridMisc.TextMatrix(7, 1), "00");
				clsSave.Set_Fields("topography1", FCConvert.ToString(Conversion.Val(Strings.Left(strTemp, 1))));
				clsSave.Set_Fields("topography2", FCConvert.ToString(Conversion.Val(Strings.Right(strTemp, 1))));
				strTemp = Strings.Format(GridMisc.TextMatrix(8, 1), "00");
				clsSave.Set_Fields("utilities1", FCConvert.ToString(Conversion.Val(Strings.Left(strTemp, 1))));
				clsSave.Set_Fields("utilities2", FCConvert.ToString(Conversion.Val(Strings.Right(strTemp, 1))));
				clsSave.Set_Fields("streetcode", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(9, 1))));
				clsSave.Set_Fields("Open1", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(10, 1))));
				clsSave.Set_Fields("Open2", FCConvert.ToString(Conversion.Val(GridMisc.TextMatrix(11, 1))));
				clsSave.Update();
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string GetSketchList(ref int lngAutoID)
		{
			string GetSketchList = "";
			// this function will return a comma delimited list of all sketch files for the given id
			string strTemp;
			string strTemp2;
			string strFile;
			string strReturn;
			string strComma;
			strComma = "";
			strReturn = "";
			strTemp = Convert.ToString(FCConvert.ToInt32(lngAutoID), 16).ToUpper();
			if (strTemp.Length < 7)
			{
				strTemp = Strings.StrDup(7 - strTemp.Length, "0") + strTemp;
			}
			//FC:FINAL:DSE Path should be relative to deployment
			//strTemp2 = Environment.CurrentDirectory + "\\sketches\\" + strTemp;
			strTemp2 = Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "sketches", strTemp);
			strFile = FCFileSystem.Dir(strTemp2 + "*.bmp", 0);
			while (strFile != string.Empty)
			{
				// will now allow sketch pictures without sketch files
				//FC:FINAL:DSE Path should be relative to deployment
				//strReturn += strComma + Environment.CurrentDirectory + "\\sketches\\" + strFile;
				strReturn += strComma + Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "sketches", strFile);
				strComma = ",";
				strFile = FCFileSystem.Dir();
			}
			GetSketchList = strReturn;
			return GetSketchList;
		}
	}
}
