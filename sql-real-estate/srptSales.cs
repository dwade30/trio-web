﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptSales.
	/// </summary>
	public partial class srptSales : FCSectionReport
	{
		public static srptSales InstancePtr
		{
			get
			{
				return (srptSales)Sys.GetInstance(typeof(srptSales));
			}
		}

		protected srptSales _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptSales()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptSales	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
		int mean;
		string strOrderByClause = "";
		int lngPlaceCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// EOF = clsSalesRec.EndOfFile
			eArgs.EOF = lngPlaceCounter >= frmShowSalesAnalysis.InstancePtr.Grid1.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngPlaceCounter = 1;
			// Call clsSalesRec.OpenRecordset("select  srmaster.* from (srmaster inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) ) on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber) and (srmaster.saledate = extracttable.saledate)) where srmaster.rsaccount > 0 order by srmaster.rsaccount,srmaster.rscard,srmaster.saledate", strredatabase)
			// txtMean.Text = Val(Me.Tag & "")
			// mean = Round(CDbl(SalesAnalRec.MeantoUse), 0)
			// txtMean.Text = mean
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			float SPrice;
			float salesf;
			string strTemp;
			float Landval;
			float Bldgval;
			float TotVal;
			int SRatio;
			// txtAccount.Text = clsSalesRec.GetData("rsaccount")
			txtAccount.Text = frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 0);
			// txtMapLot.Text = clsSalesRec.GetData("rsmaplot")
			txtMapLot.Text = frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 1);
			// txtDate.Text = clsSalesRec.GetData("saledate")
			txtDate.Text = frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 2);
			strTemp = txtDate.Text;
			// SPrice = Val(clsSalesRec.GetData("pisaleprice"))
			txtPrice.Text = Strings.Format(frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 3), "###,###,###,##0");
			
			txtValuation.Text = Strings.Format(frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 4), "###,###,###,##0");
			// TotVal = Landval + Bldgval
			// SRatio = CInt(CSng(TotVal) / SPrice * 100)
			// txtRatio.Text = SRatio
			txtRatio.Text = frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 5);
			// txtDev.Text = Abs(mean - SRatio)
			txtMean.Text = frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 6);
			txtDev.Text = frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(lngPlaceCounter, 7);
			// Call clsSalesRec.MoveNext
			lngPlaceCounter += 1;
		}

	
	}
}
