//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmRECResSizeFactor.
	/// </summary>
	partial class frmRECResSizeFactor : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt1PGD;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt1SFLA;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt2PGD;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt2SFLA;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt3PGD;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt3SFLA;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt4SFLA;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt4PGD;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt5PGD;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txt5SFLA;
		public fecherFoundation.FCTextBox txt1PGD_0;
		public fecherFoundation.FCTextBox txt1SFLA_0;
		public fecherFoundation.FCTextBox txt1PGD_1;
		public fecherFoundation.FCTextBox txt1PGD_2;
		public fecherFoundation.FCTextBox txt1PGD_3;
		public fecherFoundation.FCTextBox txt1PGD_4;
		public fecherFoundation.FCTextBox txt2PGD_0;
		public fecherFoundation.FCTextBox txt2SFLA_0;
		public fecherFoundation.FCTextBox txt3PGD_0;
		public fecherFoundation.FCTextBox txt3SFLA_0;
		public fecherFoundation.FCTextBox txt4SFLA_0;
		public fecherFoundation.FCTextBox txt4PGD_0;
		public fecherFoundation.FCTextBox txt5PGD_0;
		public fecherFoundation.FCTextBox txt5SFLA_0;
		public fecherFoundation.FCTextBox txt2SFLA_1;
		public fecherFoundation.FCTextBox txt2SFLA_2;
		public fecherFoundation.FCTextBox txt2SFLA_3;
		public fecherFoundation.FCTextBox txt2SFLA_4;
		public fecherFoundation.FCTextBox txt2PGD_1;
		public fecherFoundation.FCTextBox txt2PGD_2;
		public fecherFoundation.FCTextBox txt2PGD_3;
		public fecherFoundation.FCTextBox txt2PGD_4;
		public fecherFoundation.FCTextBox txt3SFLA_1;
		public fecherFoundation.FCTextBox txt3SFLA_2;
		public fecherFoundation.FCTextBox txt3SFLA_3;
		public fecherFoundation.FCTextBox txt3SFLA_4;
		public fecherFoundation.FCTextBox txt3PGD_1;
		public fecherFoundation.FCTextBox txt3PGD_2;
		public fecherFoundation.FCTextBox txt3PGD_3;
		public fecherFoundation.FCTextBox txt3PGD_4;
		public fecherFoundation.FCTextBox txt4SFLA_1;
		public fecherFoundation.FCTextBox txt4SFLA_2;
		public fecherFoundation.FCTextBox txt4SFLA_3;
		public fecherFoundation.FCTextBox txt4SFLA_4;
		public fecherFoundation.FCTextBox txt4PGD_1;
		public fecherFoundation.FCTextBox txt4PGD_2;
		public fecherFoundation.FCTextBox txt4PGD_3;
		public fecherFoundation.FCTextBox txt4PGD_4;
		public fecherFoundation.FCTextBox txt5SFLA_1;
		public fecherFoundation.FCTextBox txt5SFLA_2;
		public fecherFoundation.FCTextBox txt5SFLA_3;
		public fecherFoundation.FCTextBox txt5SFLA_4;
		public fecherFoundation.FCTextBox txt5PGD_1;
		public fecherFoundation.FCTextBox txt5PGD_2;
		public fecherFoundation.FCTextBox txt5PGD_3;
		public fecherFoundation.FCTextBox txt5PGD_4;
		public fecherFoundation.FCTextBox txt1SFLA_1;
		public fecherFoundation.FCTextBox txt1SFLA_2;
		public fecherFoundation.FCTextBox txt1SFLA_3;
		public fecherFoundation.FCTextBox txt1SFLA_4;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRECResSizeFactor));
            this.txt1PGD_0 = new fecherFoundation.FCTextBox();
            this.txt1SFLA_0 = new fecherFoundation.FCTextBox();
            this.txt1PGD_1 = new fecherFoundation.FCTextBox();
            this.txt1PGD_2 = new fecherFoundation.FCTextBox();
            this.txt1PGD_3 = new fecherFoundation.FCTextBox();
            this.txt1PGD_4 = new fecherFoundation.FCTextBox();
            this.txt2PGD_0 = new fecherFoundation.FCTextBox();
            this.txt2SFLA_0 = new fecherFoundation.FCTextBox();
            this.txt3PGD_0 = new fecherFoundation.FCTextBox();
            this.txt3SFLA_0 = new fecherFoundation.FCTextBox();
            this.txt4SFLA_0 = new fecherFoundation.FCTextBox();
            this.txt4PGD_0 = new fecherFoundation.FCTextBox();
            this.txt5PGD_0 = new fecherFoundation.FCTextBox();
            this.txt5SFLA_0 = new fecherFoundation.FCTextBox();
            this.txt2SFLA_1 = new fecherFoundation.FCTextBox();
            this.txt2SFLA_2 = new fecherFoundation.FCTextBox();
            this.txt2SFLA_3 = new fecherFoundation.FCTextBox();
            this.txt2SFLA_4 = new fecherFoundation.FCTextBox();
            this.txt2PGD_1 = new fecherFoundation.FCTextBox();
            this.txt2PGD_2 = new fecherFoundation.FCTextBox();
            this.txt2PGD_3 = new fecherFoundation.FCTextBox();
            this.txt2PGD_4 = new fecherFoundation.FCTextBox();
            this.txt3SFLA_1 = new fecherFoundation.FCTextBox();
            this.txt3SFLA_2 = new fecherFoundation.FCTextBox();
            this.txt3SFLA_3 = new fecherFoundation.FCTextBox();
            this.txt3SFLA_4 = new fecherFoundation.FCTextBox();
            this.txt3PGD_1 = new fecherFoundation.FCTextBox();
            this.txt3PGD_2 = new fecherFoundation.FCTextBox();
            this.txt3PGD_3 = new fecherFoundation.FCTextBox();
            this.txt3PGD_4 = new fecherFoundation.FCTextBox();
            this.txt4SFLA_1 = new fecherFoundation.FCTextBox();
            this.txt4SFLA_2 = new fecherFoundation.FCTextBox();
            this.txt4SFLA_3 = new fecherFoundation.FCTextBox();
            this.txt4SFLA_4 = new fecherFoundation.FCTextBox();
            this.txt4PGD_1 = new fecherFoundation.FCTextBox();
            this.txt4PGD_2 = new fecherFoundation.FCTextBox();
            this.txt4PGD_3 = new fecherFoundation.FCTextBox();
            this.txt4PGD_4 = new fecherFoundation.FCTextBox();
            this.txt5SFLA_1 = new fecherFoundation.FCTextBox();
            this.txt5SFLA_2 = new fecherFoundation.FCTextBox();
            this.txt5SFLA_3 = new fecherFoundation.FCTextBox();
            this.txt5SFLA_4 = new fecherFoundation.FCTextBox();
            this.txt5PGD_1 = new fecherFoundation.FCTextBox();
            this.txt5PGD_2 = new fecherFoundation.FCTextBox();
            this.txt5PGD_3 = new fecherFoundation.FCTextBox();
            this.txt5PGD_4 = new fecherFoundation.FCTextBox();
            this.txt1SFLA_1 = new fecherFoundation.FCTextBox();
            this.txt1SFLA_2 = new fecherFoundation.FCTextBox();
            this.txt1SFLA_3 = new fecherFoundation.FCTextBox();
            this.txt1SFLA_4 = new fecherFoundation.FCTextBox();
            this.cmdSave = new fecherFoundation.FCButton();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.BackColor = System.Drawing.Color.FromName("@window");
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 460);
            this.BottomPanel.Size = new System.Drawing.Size(898, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientArea.Controls.Add(this.txt1PGD_0);
            this.ClientArea.Controls.Add(this.txt1SFLA_0);
            this.ClientArea.Controls.Add(this.txt1PGD_1);
            this.ClientArea.Controls.Add(this.txt1PGD_2);
            this.ClientArea.Controls.Add(this.txt1PGD_3);
            this.ClientArea.Controls.Add(this.txt1PGD_4);
            this.ClientArea.Controls.Add(this.txt2PGD_0);
            this.ClientArea.Controls.Add(this.txt2SFLA_0);
            this.ClientArea.Controls.Add(this.txt3PGD_0);
            this.ClientArea.Controls.Add(this.txt3SFLA_0);
            this.ClientArea.Controls.Add(this.txt4SFLA_0);
            this.ClientArea.Controls.Add(this.txt4PGD_0);
            this.ClientArea.Controls.Add(this.txt5PGD_0);
            this.ClientArea.Controls.Add(this.txt5SFLA_0);
            this.ClientArea.Controls.Add(this.txt2SFLA_1);
            this.ClientArea.Controls.Add(this.txt2SFLA_2);
            this.ClientArea.Controls.Add(this.txt2SFLA_3);
            this.ClientArea.Controls.Add(this.txt2SFLA_4);
            this.ClientArea.Controls.Add(this.txt2PGD_1);
            this.ClientArea.Controls.Add(this.txt2PGD_2);
            this.ClientArea.Controls.Add(this.txt2PGD_3);
            this.ClientArea.Controls.Add(this.txt2PGD_4);
            this.ClientArea.Controls.Add(this.txt3SFLA_1);
            this.ClientArea.Controls.Add(this.txt3SFLA_2);
            this.ClientArea.Controls.Add(this.txt3SFLA_3);
            this.ClientArea.Controls.Add(this.txt3SFLA_4);
            this.ClientArea.Controls.Add(this.txt3PGD_1);
            this.ClientArea.Controls.Add(this.txt3PGD_2);
            this.ClientArea.Controls.Add(this.txt3PGD_3);
            this.ClientArea.Controls.Add(this.txt3PGD_4);
            this.ClientArea.Controls.Add(this.txt4SFLA_1);
            this.ClientArea.Controls.Add(this.txt4SFLA_2);
            this.ClientArea.Controls.Add(this.txt4SFLA_3);
            this.ClientArea.Controls.Add(this.txt4SFLA_4);
            this.ClientArea.Controls.Add(this.txt4PGD_1);
            this.ClientArea.Controls.Add(this.txt4PGD_2);
            this.ClientArea.Controls.Add(this.txt4PGD_3);
            this.ClientArea.Controls.Add(this.txt4PGD_4);
            this.ClientArea.Controls.Add(this.txt5SFLA_1);
            this.ClientArea.Controls.Add(this.txt5SFLA_2);
            this.ClientArea.Controls.Add(this.txt5SFLA_3);
            this.ClientArea.Controls.Add(this.txt5SFLA_4);
            this.ClientArea.Controls.Add(this.txt5PGD_1);
            this.ClientArea.Controls.Add(this.txt5PGD_2);
            this.ClientArea.Controls.Add(this.txt5PGD_3);
            this.ClientArea.Controls.Add(this.txt5PGD_4);
            this.ClientArea.Controls.Add(this.txt1SFLA_1);
            this.ClientArea.Controls.Add(this.txt1SFLA_2);
            this.ClientArea.Controls.Add(this.txt1SFLA_3);
            this.ClientArea.Controls.Add(this.txt1SFLA_4);
            this.ClientArea.Controls.Add(this.Label15);
            this.ClientArea.Controls.Add(this.Label14);
            this.ClientArea.Controls.Add(this.Label13);
            this.ClientArea.Controls.Add(this.Label12);
            this.ClientArea.Controls.Add(this.Label11);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Size = new System.Drawing.Size(898, 400);
            // 
            // TopPanel
            // 
            this.TopPanel.BackColor = System.Drawing.Color.FromName("@window");
            this.TopPanel.Size = new System.Drawing.Size(898, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(266, 30);
            this.HeaderText.Text = "Residential Size Factor";
            // 
            // txt1PGD_0
            // 
            this.txt1PGD_0.AutoSize = false;
            this.txt1PGD_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt1PGD_0.LinkItem = null;
            this.txt1PGD_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1PGD_0.LinkTopic = null;
            this.txt1PGD_0.Location = new System.Drawing.Point(105, 120);
            this.txt1PGD_0.MaxLength = 3;
            this.txt1PGD_0.Name = "txt1PGD_0";
            this.txt1PGD_0.Size = new System.Drawing.Size(65, 40);
            this.txt1PGD_0.TabIndex = 1;
            this.txt1PGD_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txt1PGD_KeyUp);
            this.txt1PGD_0.Enter += new System.EventHandler(this.txt1PGD_Enter);
            this.txt1PGD_0.Leave += new System.EventHandler(this.txt1PGD_Leave);
            this.txt1PGD_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt1PGD_Validating);
            // 
            // txt1SFLA_0
            // 
            this.txt1SFLA_0.AutoSize = false;
            this.txt1SFLA_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt1SFLA_0.LinkItem = null;
            this.txt1SFLA_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1SFLA_0.LinkTopic = null;
            this.txt1SFLA_0.Location = new System.Drawing.Point(30, 120);
            this.txt1SFLA_0.MaxLength = 4;
            this.txt1SFLA_0.Name = "txt1SFLA_0";
            this.txt1SFLA_0.Size = new System.Drawing.Size(65, 40);
            this.txt1SFLA_0.TabIndex = 0;
            this.txt1SFLA_0.Enter += new System.EventHandler(this.txt1SFLA_Enter);
            this.txt1SFLA_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt1SFLA_Validating);
            // 
            // txt1PGD_1
            // 
            this.txt1PGD_1.AutoSize = false;
            this.txt1PGD_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt1PGD_1.LinkItem = null;
            this.txt1PGD_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1PGD_1.LinkTopic = null;
            this.txt1PGD_1.Location = new System.Drawing.Point(105, 170);
            this.txt1PGD_1.MaxLength = 3;
            this.txt1PGD_1.Name = "txt1PGD_1";
            this.txt1PGD_1.Size = new System.Drawing.Size(65, 40);
            this.txt1PGD_1.TabIndex = 3;
            this.txt1PGD_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txt1PGD_KeyUp);
            this.txt1PGD_1.Enter += new System.EventHandler(this.txt1PGD_Enter);
            this.txt1PGD_1.Leave += new System.EventHandler(this.txt1PGD_Leave);
            this.txt1PGD_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt1PGD_Validating);
            // 
            // txt1PGD_2
            // 
            this.txt1PGD_2.AutoSize = false;
            this.txt1PGD_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt1PGD_2.LinkItem = null;
            this.txt1PGD_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1PGD_2.LinkTopic = null;
            this.txt1PGD_2.Location = new System.Drawing.Point(105, 220);
            this.txt1PGD_2.MaxLength = 3;
            this.txt1PGD_2.Name = "txt1PGD_2";
            this.txt1PGD_2.Size = new System.Drawing.Size(65, 40);
            this.txt1PGD_2.TabIndex = 5;
            this.txt1PGD_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txt1PGD_KeyUp);
            this.txt1PGD_2.Enter += new System.EventHandler(this.txt1PGD_Enter);
            this.txt1PGD_2.Leave += new System.EventHandler(this.txt1PGD_Leave);
            this.txt1PGD_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt1PGD_Validating);
            // 
            // txt1PGD_3
            // 
            this.txt1PGD_3.AutoSize = false;
            this.txt1PGD_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt1PGD_3.LinkItem = null;
            this.txt1PGD_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1PGD_3.LinkTopic = null;
            this.txt1PGD_3.Location = new System.Drawing.Point(105, 270);
            this.txt1PGD_3.MaxLength = 3;
            this.txt1PGD_3.Name = "txt1PGD_3";
            this.txt1PGD_3.Size = new System.Drawing.Size(65, 40);
            this.txt1PGD_3.TabIndex = 7;
            this.txt1PGD_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txt1PGD_KeyUp);
            this.txt1PGD_3.Enter += new System.EventHandler(this.txt1PGD_Enter);
            this.txt1PGD_3.Leave += new System.EventHandler(this.txt1PGD_Leave);
            this.txt1PGD_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt1PGD_Validating);
            // 
            // txt1PGD_4
            // 
            this.txt1PGD_4.AutoSize = false;
            this.txt1PGD_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt1PGD_4.LinkItem = null;
            this.txt1PGD_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1PGD_4.LinkTopic = null;
            this.txt1PGD_4.Location = new System.Drawing.Point(105, 320);
            this.txt1PGD_4.MaxLength = 3;
            this.txt1PGD_4.Name = "txt1PGD_4";
            this.txt1PGD_4.Size = new System.Drawing.Size(65, 40);
            this.txt1PGD_4.TabIndex = 9;
            this.txt1PGD_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txt1PGD_KeyUp);
            this.txt1PGD_4.Enter += new System.EventHandler(this.txt1PGD_Enter);
            this.txt1PGD_4.Leave += new System.EventHandler(this.txt1PGD_Leave);
            this.txt1PGD_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt1PGD_Validating);
            // 
            // txt2PGD_0
            // 
            this.txt2PGD_0.AutoSize = false;
            this.txt2PGD_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt2PGD_0.LinkItem = null;
            this.txt2PGD_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2PGD_0.LinkTopic = null;
            this.txt2PGD_0.Location = new System.Drawing.Point(275, 120);
            this.txt2PGD_0.MaxLength = 3;
            this.txt2PGD_0.Name = "txt2PGD_0";
            this.txt2PGD_0.Size = new System.Drawing.Size(65, 40);
            this.txt2PGD_0.TabIndex = 11;
            this.txt2PGD_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txt2PGD_KeyUp);
            this.txt2PGD_0.Enter += new System.EventHandler(this.txt2PGD_Enter);
            this.txt2PGD_0.Leave += new System.EventHandler(this.txt2PGD_Leave);
            this.txt2PGD_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt2PGD_Validating);
            // 
            // txt2SFLA_0
            // 
            this.txt2SFLA_0.AutoSize = false;
            this.txt2SFLA_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt2SFLA_0.LinkItem = null;
            this.txt2SFLA_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2SFLA_0.LinkTopic = null;
            this.txt2SFLA_0.Location = new System.Drawing.Point(200, 120);
            this.txt2SFLA_0.MaxLength = 4;
            this.txt2SFLA_0.Name = "txt2SFLA_0";
            this.txt2SFLA_0.Size = new System.Drawing.Size(65, 40);
            this.txt2SFLA_0.TabIndex = 10;
            this.txt2SFLA_0.Enter += new System.EventHandler(this.txt2SFLA_Enter);
            this.txt2SFLA_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt2SFLA_Validating);
            // 
            // txt3PGD_0
            // 
            this.txt3PGD_0.AutoSize = false;
            this.txt3PGD_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt3PGD_0.LinkItem = null;
            this.txt3PGD_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3PGD_0.LinkTopic = null;
            this.txt3PGD_0.Location = new System.Drawing.Point(442, 120);
            this.txt3PGD_0.MaxLength = 3;
            this.txt3PGD_0.Name = "txt3PGD_0";
            this.txt3PGD_0.Size = new System.Drawing.Size(65, 40);
            this.txt3PGD_0.TabIndex = 21;
            this.txt3PGD_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txt3PGD_KeyUp);
            this.txt3PGD_0.Enter += new System.EventHandler(this.txt3PGD_Enter);
            this.txt3PGD_0.Leave += new System.EventHandler(this.txt3PGD_Leave);
            this.txt3PGD_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt3PGD_Validating);
            // 
            // txt3SFLA_0
            // 
            this.txt3SFLA_0.AutoSize = false;
            this.txt3SFLA_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt3SFLA_0.LinkItem = null;
            this.txt3SFLA_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3SFLA_0.LinkTopic = null;
            this.txt3SFLA_0.Location = new System.Drawing.Point(367, 120);
            this.txt3SFLA_0.MaxLength = 4;
            this.txt3SFLA_0.Name = "txt3SFLA_0";
            this.txt3SFLA_0.Size = new System.Drawing.Size(65, 40);
            this.txt3SFLA_0.TabIndex = 20;
            this.txt3SFLA_0.Enter += new System.EventHandler(this.txt3SFLA_Enter);
            this.txt3SFLA_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt3SFLA_Validating);
            // 
            // txt4SFLA_0
            // 
            this.txt4SFLA_0.AutoSize = false;
            this.txt4SFLA_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt4SFLA_0.LinkItem = null;
            this.txt4SFLA_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4SFLA_0.LinkTopic = null;
            this.txt4SFLA_0.Location = new System.Drawing.Point(537, 120);
            this.txt4SFLA_0.MaxLength = 4;
            this.txt4SFLA_0.Name = "txt4SFLA_0";
            this.txt4SFLA_0.Size = new System.Drawing.Size(65, 40);
            this.txt4SFLA_0.TabIndex = 30;
            this.txt4SFLA_0.Enter += new System.EventHandler(this.txt4SFLA_Enter);
            this.txt4SFLA_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt4SFLA_Validating);
            // 
            // txt4PGD_0
            // 
            this.txt4PGD_0.AutoSize = false;
            this.txt4PGD_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt4PGD_0.LinkItem = null;
            this.txt4PGD_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4PGD_0.LinkTopic = null;
            this.txt4PGD_0.Location = new System.Drawing.Point(612, 120);
            this.txt4PGD_0.MaxLength = 3;
            this.txt4PGD_0.Name = "txt4PGD_0";
            this.txt4PGD_0.Size = new System.Drawing.Size(65, 40);
            this.txt4PGD_0.TabIndex = 31;
            this.txt4PGD_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txt4PGD_KeyUp);
            this.txt4PGD_0.Enter += new System.EventHandler(this.txt4PGD_Enter);
            this.txt4PGD_0.Leave += new System.EventHandler(this.txt4PGD_Leave);
            this.txt4PGD_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt4PGD_Validating);
            // 
            // txt5PGD_0
            // 
            this.txt5PGD_0.AutoSize = false;
            this.txt5PGD_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt5PGD_0.LinkItem = null;
            this.txt5PGD_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5PGD_0.LinkTopic = null;
            this.txt5PGD_0.Location = new System.Drawing.Point(782, 120);
            this.txt5PGD_0.MaxLength = 3;
            this.txt5PGD_0.Name = "txt5PGD_0";
            this.txt5PGD_0.Size = new System.Drawing.Size(65, 40);
            this.txt5PGD_0.TabIndex = 41;
            this.txt5PGD_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txt5PGD_KeyUp);
            this.txt5PGD_0.Enter += new System.EventHandler(this.txt5PGD_Enter);
            this.txt5PGD_0.Leave += new System.EventHandler(this.txt5PGD_Leave);
            this.txt5PGD_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt5PGD_Validating);
            // 
            // txt5SFLA_0
            // 
            this.txt5SFLA_0.AutoSize = false;
            this.txt5SFLA_0.BackColor = System.Drawing.SystemColors.Window;
            this.txt5SFLA_0.LinkItem = null;
            this.txt5SFLA_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5SFLA_0.LinkTopic = null;
            this.txt5SFLA_0.Location = new System.Drawing.Point(707, 120);
            this.txt5SFLA_0.MaxLength = 4;
            this.txt5SFLA_0.Name = "txt5SFLA_0";
            this.txt5SFLA_0.Size = new System.Drawing.Size(65, 40);
            this.txt5SFLA_0.TabIndex = 40;
            this.txt5SFLA_0.Enter += new System.EventHandler(this.txt5SFLA_Enter);
            this.txt5SFLA_0.Validating += new System.ComponentModel.CancelEventHandler(this.txt5SFLA_Validating);
            // 
            // txt2SFLA_1
            // 
            this.txt2SFLA_1.AutoSize = false;
            this.txt2SFLA_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt2SFLA_1.LinkItem = null;
            this.txt2SFLA_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2SFLA_1.LinkTopic = null;
            this.txt2SFLA_1.Location = new System.Drawing.Point(200, 170);
            this.txt2SFLA_1.MaxLength = 4;
            this.txt2SFLA_1.Name = "txt2SFLA_1";
            this.txt2SFLA_1.Size = new System.Drawing.Size(65, 40);
            this.txt2SFLA_1.TabIndex = 12;
            this.txt2SFLA_1.Enter += new System.EventHandler(this.txt2SFLA_Enter);
            this.txt2SFLA_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt2SFLA_Validating);
            // 
            // txt2SFLA_2
            // 
            this.txt2SFLA_2.AutoSize = false;
            this.txt2SFLA_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt2SFLA_2.LinkItem = null;
            this.txt2SFLA_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2SFLA_2.LinkTopic = null;
            this.txt2SFLA_2.Location = new System.Drawing.Point(200, 220);
            this.txt2SFLA_2.MaxLength = 4;
            this.txt2SFLA_2.Name = "txt2SFLA_2";
            this.txt2SFLA_2.Size = new System.Drawing.Size(65, 40);
            this.txt2SFLA_2.TabIndex = 14;
            this.txt2SFLA_2.Enter += new System.EventHandler(this.txt2SFLA_Enter);
            this.txt2SFLA_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt2SFLA_Validating);
            // 
            // txt2SFLA_3
            // 
            this.txt2SFLA_3.AutoSize = false;
            this.txt2SFLA_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt2SFLA_3.LinkItem = null;
            this.txt2SFLA_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2SFLA_3.LinkTopic = null;
            this.txt2SFLA_3.Location = new System.Drawing.Point(200, 270);
            this.txt2SFLA_3.MaxLength = 4;
            this.txt2SFLA_3.Name = "txt2SFLA_3";
            this.txt2SFLA_3.Size = new System.Drawing.Size(65, 40);
            this.txt2SFLA_3.TabIndex = 16;
            this.txt2SFLA_3.Enter += new System.EventHandler(this.txt2SFLA_Enter);
            this.txt2SFLA_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt2SFLA_Validating);
            // 
            // txt2SFLA_4
            // 
            this.txt2SFLA_4.AutoSize = false;
            this.txt2SFLA_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt2SFLA_4.LinkItem = null;
            this.txt2SFLA_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2SFLA_4.LinkTopic = null;
            this.txt2SFLA_4.Location = new System.Drawing.Point(200, 320);
            this.txt2SFLA_4.MaxLength = 4;
            this.txt2SFLA_4.Name = "txt2SFLA_4";
            this.txt2SFLA_4.Size = new System.Drawing.Size(65, 40);
            this.txt2SFLA_4.TabIndex = 18;
            this.txt2SFLA_4.Enter += new System.EventHandler(this.txt2SFLA_Enter);
            this.txt2SFLA_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt2SFLA_Validating);
            // 
            // txt2PGD_1
            // 
            this.txt2PGD_1.AutoSize = false;
            this.txt2PGD_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt2PGD_1.LinkItem = null;
            this.txt2PGD_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2PGD_1.LinkTopic = null;
            this.txt2PGD_1.Location = new System.Drawing.Point(275, 170);
            this.txt2PGD_1.MaxLength = 3;
            this.txt2PGD_1.Name = "txt2PGD_1";
            this.txt2PGD_1.Size = new System.Drawing.Size(65, 40);
            this.txt2PGD_1.TabIndex = 13;
            this.txt2PGD_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txt2PGD_KeyUp);
            this.txt2PGD_1.Enter += new System.EventHandler(this.txt2PGD_Enter);
            this.txt2PGD_1.Leave += new System.EventHandler(this.txt2PGD_Leave);
            this.txt2PGD_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt2PGD_Validating);
            // 
            // txt2PGD_2
            // 
            this.txt2PGD_2.AutoSize = false;
            this.txt2PGD_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt2PGD_2.LinkItem = null;
            this.txt2PGD_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2PGD_2.LinkTopic = null;
            this.txt2PGD_2.Location = new System.Drawing.Point(275, 220);
            this.txt2PGD_2.MaxLength = 3;
            this.txt2PGD_2.Name = "txt2PGD_2";
            this.txt2PGD_2.Size = new System.Drawing.Size(65, 40);
            this.txt2PGD_2.TabIndex = 15;
            this.txt2PGD_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txt2PGD_KeyUp);
            this.txt2PGD_2.Enter += new System.EventHandler(this.txt2PGD_Enter);
            this.txt2PGD_2.Leave += new System.EventHandler(this.txt2PGD_Leave);
            this.txt2PGD_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt2PGD_Validating);
            // 
            // txt2PGD_3
            // 
            this.txt2PGD_3.AutoSize = false;
            this.txt2PGD_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt2PGD_3.LinkItem = null;
            this.txt2PGD_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2PGD_3.LinkTopic = null;
            this.txt2PGD_3.Location = new System.Drawing.Point(275, 270);
            this.txt2PGD_3.MaxLength = 3;
            this.txt2PGD_3.Name = "txt2PGD_3";
            this.txt2PGD_3.Size = new System.Drawing.Size(65, 40);
            this.txt2PGD_3.TabIndex = 17;
            this.txt2PGD_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txt2PGD_KeyUp);
            this.txt2PGD_3.Enter += new System.EventHandler(this.txt2PGD_Enter);
            this.txt2PGD_3.Leave += new System.EventHandler(this.txt2PGD_Leave);
            this.txt2PGD_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt2PGD_Validating);
            // 
            // txt2PGD_4
            // 
            this.txt2PGD_4.AutoSize = false;
            this.txt2PGD_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt2PGD_4.LinkItem = null;
            this.txt2PGD_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2PGD_4.LinkTopic = null;
            this.txt2PGD_4.Location = new System.Drawing.Point(275, 320);
            this.txt2PGD_4.MaxLength = 3;
            this.txt2PGD_4.Name = "txt2PGD_4";
            this.txt2PGD_4.Size = new System.Drawing.Size(65, 40);
            this.txt2PGD_4.TabIndex = 19;
            this.txt2PGD_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txt2PGD_KeyUp);
            this.txt2PGD_4.Enter += new System.EventHandler(this.txt2PGD_Enter);
            this.txt2PGD_4.Leave += new System.EventHandler(this.txt2PGD_Leave);
            this.txt2PGD_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt2PGD_Validating);
            // 
            // txt3SFLA_1
            // 
            this.txt3SFLA_1.AutoSize = false;
            this.txt3SFLA_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt3SFLA_1.LinkItem = null;
            this.txt3SFLA_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3SFLA_1.LinkTopic = null;
            this.txt3SFLA_1.Location = new System.Drawing.Point(367, 170);
            this.txt3SFLA_1.MaxLength = 4;
            this.txt3SFLA_1.Name = "txt3SFLA_1";
            this.txt3SFLA_1.Size = new System.Drawing.Size(65, 40);
            this.txt3SFLA_1.TabIndex = 22;
            this.txt3SFLA_1.Enter += new System.EventHandler(this.txt3SFLA_Enter);
            this.txt3SFLA_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt3SFLA_Validating);
            // 
            // txt3SFLA_2
            // 
            this.txt3SFLA_2.AutoSize = false;
            this.txt3SFLA_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt3SFLA_2.LinkItem = null;
            this.txt3SFLA_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3SFLA_2.LinkTopic = null;
            this.txt3SFLA_2.Location = new System.Drawing.Point(367, 220);
            this.txt3SFLA_2.MaxLength = 4;
            this.txt3SFLA_2.Name = "txt3SFLA_2";
            this.txt3SFLA_2.Size = new System.Drawing.Size(65, 40);
            this.txt3SFLA_2.TabIndex = 24;
            this.txt3SFLA_2.Enter += new System.EventHandler(this.txt3SFLA_Enter);
            this.txt3SFLA_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt3SFLA_Validating);
            // 
            // txt3SFLA_3
            // 
            this.txt3SFLA_3.AutoSize = false;
            this.txt3SFLA_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt3SFLA_3.LinkItem = null;
            this.txt3SFLA_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3SFLA_3.LinkTopic = null;
            this.txt3SFLA_3.Location = new System.Drawing.Point(367, 270);
            this.txt3SFLA_3.MaxLength = 4;
            this.txt3SFLA_3.Name = "txt3SFLA_3";
            this.txt3SFLA_3.Size = new System.Drawing.Size(65, 40);
            this.txt3SFLA_3.TabIndex = 26;
            this.txt3SFLA_3.Enter += new System.EventHandler(this.txt3SFLA_Enter);
            this.txt3SFLA_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt3SFLA_Validating);
            // 
            // txt3SFLA_4
            // 
            this.txt3SFLA_4.AutoSize = false;
            this.txt3SFLA_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt3SFLA_4.LinkItem = null;
            this.txt3SFLA_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3SFLA_4.LinkTopic = null;
            this.txt3SFLA_4.Location = new System.Drawing.Point(367, 320);
            this.txt3SFLA_4.MaxLength = 4;
            this.txt3SFLA_4.Name = "txt3SFLA_4";
            this.txt3SFLA_4.Size = new System.Drawing.Size(65, 40);
            this.txt3SFLA_4.TabIndex = 28;
            this.txt3SFLA_4.Enter += new System.EventHandler(this.txt3SFLA_Enter);
            this.txt3SFLA_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt3SFLA_Validating);
            // 
            // txt3PGD_1
            // 
            this.txt3PGD_1.AutoSize = false;
            this.txt3PGD_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt3PGD_1.LinkItem = null;
            this.txt3PGD_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3PGD_1.LinkTopic = null;
            this.txt3PGD_1.Location = new System.Drawing.Point(442, 170);
            this.txt3PGD_1.MaxLength = 3;
            this.txt3PGD_1.Name = "txt3PGD_1";
            this.txt3PGD_1.Size = new System.Drawing.Size(65, 40);
            this.txt3PGD_1.TabIndex = 23;
            this.txt3PGD_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txt3PGD_KeyUp);
            this.txt3PGD_1.Enter += new System.EventHandler(this.txt3PGD_Enter);
            this.txt3PGD_1.Leave += new System.EventHandler(this.txt3PGD_Leave);
            this.txt3PGD_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt3PGD_Validating);
            // 
            // txt3PGD_2
            // 
            this.txt3PGD_2.AutoSize = false;
            this.txt3PGD_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt3PGD_2.LinkItem = null;
            this.txt3PGD_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3PGD_2.LinkTopic = null;
            this.txt3PGD_2.Location = new System.Drawing.Point(442, 220);
            this.txt3PGD_2.MaxLength = 3;
            this.txt3PGD_2.Name = "txt3PGD_2";
            this.txt3PGD_2.Size = new System.Drawing.Size(65, 40);
            this.txt3PGD_2.TabIndex = 25;
            this.txt3PGD_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txt3PGD_KeyUp);
            this.txt3PGD_2.Enter += new System.EventHandler(this.txt3PGD_Enter);
            this.txt3PGD_2.Leave += new System.EventHandler(this.txt3PGD_Leave);
            this.txt3PGD_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt3PGD_Validating);
            // 
            // txt3PGD_3
            // 
            this.txt3PGD_3.AutoSize = false;
            this.txt3PGD_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt3PGD_3.LinkItem = null;
            this.txt3PGD_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3PGD_3.LinkTopic = null;
            this.txt3PGD_3.Location = new System.Drawing.Point(442, 270);
            this.txt3PGD_3.MaxLength = 3;
            this.txt3PGD_3.Name = "txt3PGD_3";
            this.txt3PGD_3.Size = new System.Drawing.Size(65, 40);
            this.txt3PGD_3.TabIndex = 27;
            this.txt3PGD_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txt3PGD_KeyUp);
            this.txt3PGD_3.Enter += new System.EventHandler(this.txt3PGD_Enter);
            this.txt3PGD_3.Leave += new System.EventHandler(this.txt3PGD_Leave);
            this.txt3PGD_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt3PGD_Validating);
            // 
            // txt3PGD_4
            // 
            this.txt3PGD_4.AutoSize = false;
            this.txt3PGD_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt3PGD_4.LinkItem = null;
            this.txt3PGD_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt3PGD_4.LinkTopic = null;
            this.txt3PGD_4.Location = new System.Drawing.Point(442, 320);
            this.txt3PGD_4.MaxLength = 3;
            this.txt3PGD_4.Name = "txt3PGD_4";
            this.txt3PGD_4.Size = new System.Drawing.Size(65, 40);
            this.txt3PGD_4.TabIndex = 29;
            this.txt3PGD_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txt3PGD_KeyUp);
            this.txt3PGD_4.Enter += new System.EventHandler(this.txt3PGD_Enter);
            this.txt3PGD_4.Leave += new System.EventHandler(this.txt3PGD_Leave);
            this.txt3PGD_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt3PGD_Validating);
            // 
            // txt4SFLA_1
            // 
            this.txt4SFLA_1.AutoSize = false;
            this.txt4SFLA_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt4SFLA_1.LinkItem = null;
            this.txt4SFLA_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4SFLA_1.LinkTopic = null;
            this.txt4SFLA_1.Location = new System.Drawing.Point(537, 170);
            this.txt4SFLA_1.MaxLength = 4;
            this.txt4SFLA_1.Name = "txt4SFLA_1";
            this.txt4SFLA_1.Size = new System.Drawing.Size(65, 40);
            this.txt4SFLA_1.TabIndex = 32;
            this.txt4SFLA_1.Enter += new System.EventHandler(this.txt4SFLA_Enter);
            this.txt4SFLA_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt4SFLA_Validating);
            // 
            // txt4SFLA_2
            // 
            this.txt4SFLA_2.AutoSize = false;
            this.txt4SFLA_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt4SFLA_2.LinkItem = null;
            this.txt4SFLA_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4SFLA_2.LinkTopic = null;
            this.txt4SFLA_2.Location = new System.Drawing.Point(537, 220);
            this.txt4SFLA_2.MaxLength = 4;
            this.txt4SFLA_2.Name = "txt4SFLA_2";
            this.txt4SFLA_2.Size = new System.Drawing.Size(65, 40);
            this.txt4SFLA_2.TabIndex = 34;
            this.txt4SFLA_2.Enter += new System.EventHandler(this.txt4SFLA_Enter);
            this.txt4SFLA_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt4SFLA_Validating);
            // 
            // txt4SFLA_3
            // 
            this.txt4SFLA_3.AutoSize = false;
            this.txt4SFLA_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt4SFLA_3.LinkItem = null;
            this.txt4SFLA_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4SFLA_3.LinkTopic = null;
            this.txt4SFLA_3.Location = new System.Drawing.Point(537, 270);
            this.txt4SFLA_3.MaxLength = 4;
            this.txt4SFLA_3.Name = "txt4SFLA_3";
            this.txt4SFLA_3.Size = new System.Drawing.Size(65, 40);
            this.txt4SFLA_3.TabIndex = 36;
            this.txt4SFLA_3.Enter += new System.EventHandler(this.txt4SFLA_Enter);
            this.txt4SFLA_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt4SFLA_Validating);
            // 
            // txt4SFLA_4
            // 
            this.txt4SFLA_4.AutoSize = false;
            this.txt4SFLA_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt4SFLA_4.LinkItem = null;
            this.txt4SFLA_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4SFLA_4.LinkTopic = null;
            this.txt4SFLA_4.Location = new System.Drawing.Point(537, 320);
            this.txt4SFLA_4.MaxLength = 4;
            this.txt4SFLA_4.Name = "txt4SFLA_4";
            this.txt4SFLA_4.Size = new System.Drawing.Size(65, 40);
            this.txt4SFLA_4.TabIndex = 38;
            this.txt4SFLA_4.Enter += new System.EventHandler(this.txt4SFLA_Enter);
            this.txt4SFLA_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt4SFLA_Validating);
            // 
            // txt4PGD_1
            // 
            this.txt4PGD_1.AutoSize = false;
            this.txt4PGD_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt4PGD_1.LinkItem = null;
            this.txt4PGD_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4PGD_1.LinkTopic = null;
            this.txt4PGD_1.Location = new System.Drawing.Point(612, 170);
            this.txt4PGD_1.MaxLength = 3;
            this.txt4PGD_1.Name = "txt4PGD_1";
            this.txt4PGD_1.Size = new System.Drawing.Size(65, 40);
            this.txt4PGD_1.TabIndex = 33;
            this.txt4PGD_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txt4PGD_KeyUp);
            this.txt4PGD_1.Enter += new System.EventHandler(this.txt4PGD_Enter);
            this.txt4PGD_1.Leave += new System.EventHandler(this.txt4PGD_Leave);
            this.txt4PGD_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt4PGD_Validating);
            // 
            // txt4PGD_2
            // 
            this.txt4PGD_2.AutoSize = false;
            this.txt4PGD_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt4PGD_2.LinkItem = null;
            this.txt4PGD_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4PGD_2.LinkTopic = null;
            this.txt4PGD_2.Location = new System.Drawing.Point(612, 220);
            this.txt4PGD_2.MaxLength = 3;
            this.txt4PGD_2.Name = "txt4PGD_2";
            this.txt4PGD_2.Size = new System.Drawing.Size(65, 40);
            this.txt4PGD_2.TabIndex = 35;
            this.txt4PGD_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txt4PGD_KeyUp);
            this.txt4PGD_2.Enter += new System.EventHandler(this.txt4PGD_Enter);
            this.txt4PGD_2.Leave += new System.EventHandler(this.txt4PGD_Leave);
            this.txt4PGD_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt4PGD_Validating);
            // 
            // txt4PGD_3
            // 
            this.txt4PGD_3.AutoSize = false;
            this.txt4PGD_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt4PGD_3.LinkItem = null;
            this.txt4PGD_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4PGD_3.LinkTopic = null;
            this.txt4PGD_3.Location = new System.Drawing.Point(612, 270);
            this.txt4PGD_3.MaxLength = 3;
            this.txt4PGD_3.Name = "txt4PGD_3";
            this.txt4PGD_3.Size = new System.Drawing.Size(65, 40);
            this.txt4PGD_3.TabIndex = 37;
            this.txt4PGD_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txt4PGD_KeyUp);
            this.txt4PGD_3.Enter += new System.EventHandler(this.txt4PGD_Enter);
            this.txt4PGD_3.Leave += new System.EventHandler(this.txt4PGD_Leave);
            this.txt4PGD_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt4PGD_Validating);
            // 
            // txt4PGD_4
            // 
            this.txt4PGD_4.AutoSize = false;
            this.txt4PGD_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt4PGD_4.LinkItem = null;
            this.txt4PGD_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt4PGD_4.LinkTopic = null;
            this.txt4PGD_4.Location = new System.Drawing.Point(612, 320);
            this.txt4PGD_4.MaxLength = 3;
            this.txt4PGD_4.Name = "txt4PGD_4";
            this.txt4PGD_4.Size = new System.Drawing.Size(65, 40);
            this.txt4PGD_4.TabIndex = 39;
            this.txt4PGD_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txt4PGD_KeyUp);
            this.txt4PGD_4.Enter += new System.EventHandler(this.txt4PGD_Enter);
            this.txt4PGD_4.Leave += new System.EventHandler(this.txt4PGD_Leave);
            this.txt4PGD_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt4PGD_Validating);
            // 
            // txt5SFLA_1
            // 
            this.txt5SFLA_1.AutoSize = false;
            this.txt5SFLA_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt5SFLA_1.LinkItem = null;
            this.txt5SFLA_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5SFLA_1.LinkTopic = null;
            this.txt5SFLA_1.Location = new System.Drawing.Point(707, 170);
            this.txt5SFLA_1.MaxLength = 4;
            this.txt5SFLA_1.Name = "txt5SFLA_1";
            this.txt5SFLA_1.Size = new System.Drawing.Size(65, 40);
            this.txt5SFLA_1.TabIndex = 42;
            this.txt5SFLA_1.Enter += new System.EventHandler(this.txt5SFLA_Enter);
            this.txt5SFLA_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt5SFLA_Validating);
            // 
            // txt5SFLA_2
            // 
            this.txt5SFLA_2.AutoSize = false;
            this.txt5SFLA_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt5SFLA_2.LinkItem = null;
            this.txt5SFLA_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5SFLA_2.LinkTopic = null;
            this.txt5SFLA_2.Location = new System.Drawing.Point(707, 220);
            this.txt5SFLA_2.MaxLength = 4;
            this.txt5SFLA_2.Name = "txt5SFLA_2";
            this.txt5SFLA_2.Size = new System.Drawing.Size(65, 40);
            this.txt5SFLA_2.TabIndex = 44;
            this.txt5SFLA_2.Enter += new System.EventHandler(this.txt5SFLA_Enter);
            this.txt5SFLA_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt5SFLA_Validating);
            // 
            // txt5SFLA_3
            // 
            this.txt5SFLA_3.AutoSize = false;
            this.txt5SFLA_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt5SFLA_3.LinkItem = null;
            this.txt5SFLA_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5SFLA_3.LinkTopic = null;
            this.txt5SFLA_3.Location = new System.Drawing.Point(707, 270);
            this.txt5SFLA_3.MaxLength = 4;
            this.txt5SFLA_3.Name = "txt5SFLA_3";
            this.txt5SFLA_3.Size = new System.Drawing.Size(65, 40);
            this.txt5SFLA_3.TabIndex = 46;
            this.txt5SFLA_3.Enter += new System.EventHandler(this.txt5SFLA_Enter);
            this.txt5SFLA_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt5SFLA_Validating);
            // 
            // txt5SFLA_4
            // 
            this.txt5SFLA_4.AutoSize = false;
            this.txt5SFLA_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt5SFLA_4.LinkItem = null;
            this.txt5SFLA_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5SFLA_4.LinkTopic = null;
            this.txt5SFLA_4.Location = new System.Drawing.Point(707, 320);
            this.txt5SFLA_4.MaxLength = 4;
            this.txt5SFLA_4.Name = "txt5SFLA_4";
            this.txt5SFLA_4.Size = new System.Drawing.Size(65, 40);
            this.txt5SFLA_4.TabIndex = 48;
            this.txt5SFLA_4.Enter += new System.EventHandler(this.txt5SFLA_Enter);
            this.txt5SFLA_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt5SFLA_Validating);
            // 
            // txt5PGD_1
            // 
            this.txt5PGD_1.AutoSize = false;
            this.txt5PGD_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt5PGD_1.LinkItem = null;
            this.txt5PGD_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5PGD_1.LinkTopic = null;
            this.txt5PGD_1.Location = new System.Drawing.Point(782, 170);
            this.txt5PGD_1.MaxLength = 3;
            this.txt5PGD_1.Name = "txt5PGD_1";
            this.txt5PGD_1.Size = new System.Drawing.Size(65, 40);
            this.txt5PGD_1.TabIndex = 43;
            this.txt5PGD_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txt5PGD_KeyUp);
            this.txt5PGD_1.Enter += new System.EventHandler(this.txt5PGD_Enter);
            this.txt5PGD_1.Leave += new System.EventHandler(this.txt5PGD_Leave);
            this.txt5PGD_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt5PGD_Validating);
            // 
            // txt5PGD_2
            // 
            this.txt5PGD_2.AutoSize = false;
            this.txt5PGD_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt5PGD_2.LinkItem = null;
            this.txt5PGD_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5PGD_2.LinkTopic = null;
            this.txt5PGD_2.Location = new System.Drawing.Point(782, 220);
            this.txt5PGD_2.MaxLength = 3;
            this.txt5PGD_2.Name = "txt5PGD_2";
            this.txt5PGD_2.Size = new System.Drawing.Size(65, 40);
            this.txt5PGD_2.TabIndex = 45;
            this.txt5PGD_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txt5PGD_KeyUp);
            this.txt5PGD_2.Enter += new System.EventHandler(this.txt5PGD_Enter);
            this.txt5PGD_2.Leave += new System.EventHandler(this.txt5PGD_Leave);
            this.txt5PGD_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt5PGD_Validating);
            // 
            // txt5PGD_3
            // 
            this.txt5PGD_3.AutoSize = false;
            this.txt5PGD_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt5PGD_3.LinkItem = null;
            this.txt5PGD_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5PGD_3.LinkTopic = null;
            this.txt5PGD_3.Location = new System.Drawing.Point(782, 270);
            this.txt5PGD_3.MaxLength = 3;
            this.txt5PGD_3.Name = "txt5PGD_3";
            this.txt5PGD_3.Size = new System.Drawing.Size(65, 40);
            this.txt5PGD_3.TabIndex = 47;
            this.txt5PGD_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txt5PGD_KeyUp);
            this.txt5PGD_3.Enter += new System.EventHandler(this.txt5PGD_Enter);
            this.txt5PGD_3.Leave += new System.EventHandler(this.txt5PGD_Leave);
            this.txt5PGD_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt5PGD_Validating);
            // 
            // txt5PGD_4
            // 
            this.txt5PGD_4.AutoSize = false;
            this.txt5PGD_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt5PGD_4.LinkItem = null;
            this.txt5PGD_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt5PGD_4.LinkTopic = null;
            this.txt5PGD_4.Location = new System.Drawing.Point(782, 320);
            this.txt5PGD_4.MaxLength = 3;
            this.txt5PGD_4.Name = "txt5PGD_4";
            this.txt5PGD_4.Size = new System.Drawing.Size(65, 40);
            this.txt5PGD_4.TabIndex = 49;
            this.txt5PGD_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txt5PGD_KeyUp);
            this.txt5PGD_4.Enter += new System.EventHandler(this.txt5PGD_Enter);
            this.txt5PGD_4.Leave += new System.EventHandler(this.txt5PGD_Leave);
            this.txt5PGD_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt5PGD_Validating);
            // 
            // txt1SFLA_1
            // 
            this.txt1SFLA_1.AutoSize = false;
            this.txt1SFLA_1.BackColor = System.Drawing.SystemColors.Window;
            this.txt1SFLA_1.LinkItem = null;
            this.txt1SFLA_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1SFLA_1.LinkTopic = null;
            this.txt1SFLA_1.Location = new System.Drawing.Point(30, 170);
            this.txt1SFLA_1.MaxLength = 4;
            this.txt1SFLA_1.Name = "txt1SFLA_1";
            this.txt1SFLA_1.Size = new System.Drawing.Size(65, 40);
            this.txt1SFLA_1.TabIndex = 2;
            this.txt1SFLA_1.Enter += new System.EventHandler(this.txt1SFLA_Enter);
            this.txt1SFLA_1.Validating += new System.ComponentModel.CancelEventHandler(this.txt1SFLA_Validating);
            // 
            // txt1SFLA_2
            // 
            this.txt1SFLA_2.AutoSize = false;
            this.txt1SFLA_2.BackColor = System.Drawing.SystemColors.Window;
            this.txt1SFLA_2.LinkItem = null;
            this.txt1SFLA_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1SFLA_2.LinkTopic = null;
            this.txt1SFLA_2.Location = new System.Drawing.Point(30, 220);
            this.txt1SFLA_2.MaxLength = 4;
            this.txt1SFLA_2.Name = "txt1SFLA_2";
            this.txt1SFLA_2.Size = new System.Drawing.Size(65, 40);
            this.txt1SFLA_2.TabIndex = 4;
            this.txt1SFLA_2.Enter += new System.EventHandler(this.txt1SFLA_Enter);
            this.txt1SFLA_2.Validating += new System.ComponentModel.CancelEventHandler(this.txt1SFLA_Validating);
            // 
            // txt1SFLA_3
            // 
            this.txt1SFLA_3.AutoSize = false;
            this.txt1SFLA_3.BackColor = System.Drawing.SystemColors.Window;
            this.txt1SFLA_3.LinkItem = null;
            this.txt1SFLA_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1SFLA_3.LinkTopic = null;
            this.txt1SFLA_3.Location = new System.Drawing.Point(30, 270);
            this.txt1SFLA_3.MaxLength = 4;
            this.txt1SFLA_3.Name = "txt1SFLA_3";
            this.txt1SFLA_3.Size = new System.Drawing.Size(65, 40);
            this.txt1SFLA_3.TabIndex = 6;
            this.txt1SFLA_3.Enter += new System.EventHandler(this.txt1SFLA_Enter);
            this.txt1SFLA_3.Validating += new System.ComponentModel.CancelEventHandler(this.txt1SFLA_Validating);
            // 
            // txt1SFLA_4
            // 
            this.txt1SFLA_4.AutoSize = false;
            this.txt1SFLA_4.BackColor = System.Drawing.SystemColors.Window;
            this.txt1SFLA_4.LinkItem = null;
            this.txt1SFLA_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt1SFLA_4.LinkTopic = null;
            this.txt1SFLA_4.Location = new System.Drawing.Point(30, 320);
            this.txt1SFLA_4.MaxLength = 4;
            this.txt1SFLA_4.Name = "txt1SFLA_4";
            this.txt1SFLA_4.Size = new System.Drawing.Size(65, 40);
            this.txt1SFLA_4.TabIndex = 8;
            this.txt1SFLA_4.Enter += new System.EventHandler(this.txt1SFLA_Enter);
            this.txt1SFLA_4.Validating += new System.ComponentModel.CancelEventHandler(this.txt1SFLA_Validating);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(402, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(88, 48);
            this.cmdSave.TabIndex = 51;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.BorderStyle = 1;
            this.Label15.Location = new System.Drawing.Point(105, 75);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(60, 15);
            this.Label15.TabIndex = 66;
            this.Label15.Text = "% GOOD";
            this.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.BorderStyle = 1;
            this.Label14.Location = new System.Drawing.Point(30, 75);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(59, 15);
            this.Label14.TabIndex = 65;
            this.Label14.Text = "SFLA TO";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label13
            // 
            this.Label13.BackColor = System.Drawing.Color.FromName("@window");
            this.Label13.Location = new System.Drawing.Point(63, 30);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(69, 15);
            this.Label13.TabIndex = 64;
            this.Label13.Text = "- 1 UNIT -";
            this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.BorderStyle = 1;
            this.Label12.Location = new System.Drawing.Point(275, 75);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 63;
            this.Label12.Text = "% GOOD";
            this.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.BorderStyle = 1;
            this.Label11.Location = new System.Drawing.Point(200, 75);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(59, 15);
            this.Label11.TabIndex = 62;
            this.Label11.Text = "SFLA TO";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(231, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(69, 15);
            this.Label10.TabIndex = 61;
            this.Label10.Text = "- 2 UNIT -";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.BorderStyle = 1;
            this.Label9.Location = new System.Drawing.Point(442, 75);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(60, 15);
            this.Label9.TabIndex = 60;
            this.Label9.Text = "% GOOD";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.BorderStyle = 1;
            this.Label8.Location = new System.Drawing.Point(367, 75);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(59, 15);
            this.Label8.TabIndex = 59;
            this.Label8.Text = "SFLA TO";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(396, 30);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(69, 15);
            this.Label7.TabIndex = 58;
            this.Label7.Text = "- 3 UNIT -";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(570, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(69, 15);
            this.Label1.TabIndex = 57;
            this.Label1.Text = "- 4 UNIT -";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BorderStyle = 1;
            this.Label2.Location = new System.Drawing.Point(537, 75);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(59, 15);
            this.Label2.TabIndex = 56;
            this.Label2.Text = "SFLA TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BorderStyle = 1;
            this.Label3.Location = new System.Drawing.Point(612, 75);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 15);
            this.Label3.TabIndex = 55;
            this.Label3.Text = "% GOOD";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.BorderStyle = 1;
            this.Label6.Location = new System.Drawing.Point(782, 75);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(60, 15);
            this.Label6.TabIndex = 54;
            this.Label6.Text = "% GOOD";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BorderStyle = 1;
            this.Label5.Location = new System.Drawing.Point(707, 75);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(59, 15);
            this.Label5.TabIndex = 53;
            this.Label5.Text = "SFLA TO";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(739, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(69, 15);
            this.Label4.TabIndex = 52;
            this.Label4.Text = "- 5 & OVER -";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuQuit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 2;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = 3;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            // 
            // frmRECResSizeFactor
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(898, 568);
            this.FillColor = 12632256;
            this.KeyPreview = true;
            this.Name = "frmRECResSizeFactor";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Residential Size Factor";
            this.Load += new System.EventHandler(this.frmRECResSizeFactor_Load);
            this.Activated += new System.EventHandler(this.frmRECResSizeFactor_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRECResSizeFactor_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRECResSizeFactor_KeyPress);
            this.Enter += new System.EventHandler(this.frmRECResSizeFactor_Enter);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}