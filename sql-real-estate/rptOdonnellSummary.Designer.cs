﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptOdonellSummary.
	/// </summary>
	partial class rptOdonellSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptOdonellSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotUpdatedLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotUpdatedBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotUpdatedExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSoftAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoftValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMixedAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotUpdatedLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotUpdatedBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotUpdatedExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotUpdatedLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotUpdatedBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotUpdatedExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdatedLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdatedBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdatedExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotUpdatedLand,
				this.txtTotUpdatedBuilding,
				this.txtTotUpdatedExemption,
				this.Label8,
				this.Label9,
				this.Label10,
				this.txtNumNotUpdated,
				this.Label11,
				this.txtSoftAcres,
				this.txtSoftValue,
				this.Label13,
				this.Label14,
				this.txtMixedAcres,
				this.txtMixedValue,
				this.txtHardAcres,
				this.txtHardValue,
				this.txtOtherAcres,
				this.txtOtherValue,
				this.txtTotalAcres,
				this.txtTotalLand,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.txtNumUpdated,
				this.txtNumNew,
				this.Label20,
				this.Label12,
				this.txtNewland,
				this.txtNewBldg,
				this.txtNewExempt,
				this.txtNotUpdatedLand,
				this.txtNotUpdatedBldg,
				this.txtNotUpdatedExempt,
				this.txtTotLand,
				this.txtTotBldg,
				this.txtTotExemption,
				this.Line1
			});
			this.Detail.Height = 2.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate
			});
			this.PageHeader.Height = 0.46875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Summary";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 2.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// txtTotUpdatedLand
			// 
			this.txtTotUpdatedLand.Height = 0.1770833F;
			this.txtTotUpdatedLand.Left = 2.6875F;
			this.txtTotUpdatedLand.Name = "txtTotUpdatedLand";
			this.txtTotUpdatedLand.Style = "text-align: right";
			this.txtTotUpdatedLand.Text = null;
			this.txtTotUpdatedLand.Top = 0.4375F;
			this.txtTotUpdatedLand.Width = 1.25F;
			// 
			// txtTotUpdatedBuilding
			// 
			this.txtTotUpdatedBuilding.Height = 0.1770833F;
			this.txtTotUpdatedBuilding.Left = 4F;
			this.txtTotUpdatedBuilding.Name = "txtTotUpdatedBuilding";
			this.txtTotUpdatedBuilding.Style = "text-align: right";
			this.txtTotUpdatedBuilding.Text = null;
			this.txtTotUpdatedBuilding.Top = 0.4375F;
			this.txtTotUpdatedBuilding.Width = 1.25F;
			// 
			// txtTotUpdatedExemption
			// 
			this.txtTotUpdatedExemption.Height = 0.1770833F;
			this.txtTotUpdatedExemption.Left = 5.3125F;
			this.txtTotUpdatedExemption.Name = "txtTotUpdatedExemption";
			this.txtTotUpdatedExemption.Style = "text-align: right";
			this.txtTotUpdatedExemption.Text = null;
			this.txtTotUpdatedExemption.Top = 0.4375F;
			this.txtTotUpdatedExemption.Width = 1.25F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Land";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 0.9375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.3125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Building";
			this.Label9.Top = 0.1875F;
			this.Label9.Width = 0.9375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: right";
			this.Label10.Text = "Exemption";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 0.9375F;
			// 
			// txtNumNotUpdated
			// 
			this.txtNumNotUpdated.Height = 0.1770833F;
			this.txtNumNotUpdated.Left = 1.6875F;
			this.txtNumNotUpdated.Name = "txtNumNotUpdated";
			this.txtNumNotUpdated.Style = "text-align: left";
			this.txtNumNotUpdated.Text = null;
			this.txtNumNotUpdated.Top = 0.8125F;
			this.txtNumNotUpdated.Width = 0.75F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: left";
			this.Label11.Text = "Not Updated";
			this.Label11.Top = 0.8125F;
			this.Label11.Width = 1.0625F;
			// 
			// txtSoftAcres
			// 
			this.txtSoftAcres.Height = 0.1770833F;
			this.txtSoftAcres.Left = 1.1875F;
			this.txtSoftAcres.Name = "txtSoftAcres";
			this.txtSoftAcres.Style = "text-align: right";
			this.txtSoftAcres.Text = null;
			this.txtSoftAcres.Top = 1.6875F;
			this.txtSoftAcres.Width = 1.125F;
			// 
			// txtSoftValue
			// 
			this.txtSoftValue.Height = 0.1770833F;
			this.txtSoftValue.Left = 1.1875F;
			this.txtSoftValue.Name = "txtSoftValue";
			this.txtSoftValue.Style = "text-align: right";
			this.txtSoftValue.Text = null;
			this.txtSoftValue.Top = 1.875F;
			this.txtSoftValue.Width = 1.125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Text = "Acres";
			this.Label13.Top = 1.6875F;
			this.Label13.Width = 0.9375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Text = "Value";
			this.Label14.Top = 1.875F;
			this.Label14.Width = 0.9375F;
			// 
			// txtMixedAcres
			// 
			this.txtMixedAcres.Height = 0.1770833F;
			this.txtMixedAcres.Left = 2.375F;
			this.txtMixedAcres.Name = "txtMixedAcres";
			this.txtMixedAcres.Style = "text-align: right";
			this.txtMixedAcres.Text = null;
			this.txtMixedAcres.Top = 1.6875F;
			this.txtMixedAcres.Width = 1.125F;
			// 
			// txtMixedValue
			// 
			this.txtMixedValue.Height = 0.1770833F;
			this.txtMixedValue.Left = 2.375F;
			this.txtMixedValue.Name = "txtMixedValue";
			this.txtMixedValue.Style = "text-align: right";
			this.txtMixedValue.Text = null;
			this.txtMixedValue.Top = 1.875F;
			this.txtMixedValue.Width = 1.125F;
			// 
			// txtHardAcres
			// 
			this.txtHardAcres.Height = 0.1770833F;
			this.txtHardAcres.Left = 3.5625F;
			this.txtHardAcres.Name = "txtHardAcres";
			this.txtHardAcres.Style = "text-align: right";
			this.txtHardAcres.Text = null;
			this.txtHardAcres.Top = 1.6875F;
			this.txtHardAcres.Width = 1.125F;
			// 
			// txtHardValue
			// 
			this.txtHardValue.Height = 0.1770833F;
			this.txtHardValue.Left = 3.5625F;
			this.txtHardValue.Name = "txtHardValue";
			this.txtHardValue.Style = "text-align: right";
			this.txtHardValue.Text = null;
			this.txtHardValue.Top = 1.875F;
			this.txtHardValue.Width = 1.125F;
			// 
			// txtOtherAcres
			// 
			this.txtOtherAcres.Height = 0.1770833F;
			this.txtOtherAcres.Left = 4.75F;
			this.txtOtherAcres.Name = "txtOtherAcres";
			this.txtOtherAcres.Style = "text-align: right";
			this.txtOtherAcres.Text = null;
			this.txtOtherAcres.Top = 1.6875F;
			this.txtOtherAcres.Width = 1.125F;
			// 
			// txtOtherValue
			// 
			this.txtOtherValue.Height = 0.1770833F;
			this.txtOtherValue.Left = 4.75F;
			this.txtOtherValue.Name = "txtOtherValue";
			this.txtOtherValue.Style = "text-align: right";
			this.txtOtherValue.Text = null;
			this.txtOtherValue.Top = 1.875F;
			this.txtOtherValue.Width = 1.125F;
			// 
			// txtTotalAcres
			// 
			this.txtTotalAcres.Height = 0.1770833F;
			this.txtTotalAcres.Left = 5.9375F;
			this.txtTotalAcres.Name = "txtTotalAcres";
			this.txtTotalAcres.Style = "text-align: right";
			this.txtTotalAcres.Text = null;
			this.txtTotalAcres.Top = 1.6875F;
			this.txtTotalAcres.Width = 1.125F;
			// 
			// txtTotalLand
			// 
			this.txtTotalLand.Height = 0.1770833F;
			this.txtTotalLand.Left = 5.9375F;
			this.txtTotalLand.Name = "txtTotalLand";
			this.txtTotalLand.Style = "text-align: right";
			this.txtTotalLand.Text = null;
			this.txtTotalLand.Top = 1.875F;
			this.txtTotalLand.Width = 1.125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 1.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Text = "Soft";
			this.Label15.Top = 1.4375F;
			this.Label15.Width = 0.9375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.5625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold; text-align: right";
			this.Label16.Text = "Mixed";
			this.Label16.Top = 1.4375F;
			this.Label16.Width = 0.9375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.75F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Text = "Hard";
			this.Label17.Top = 1.4375F;
			this.Label17.Width = 0.9375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 4.9375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: right";
			this.Label18.Text = "Other";
			this.Label18.Top = 1.4375F;
			this.Label18.Width = 0.9375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 6.125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold; text-align: right";
			this.Label19.Text = "Total";
			this.Label19.Top = 1.4375F;
			this.Label19.Width = 0.9375F;
			// 
			// txtNumUpdated
			// 
			this.txtNumUpdated.Height = 0.1770833F;
			this.txtNumUpdated.Left = 1.6875F;
			this.txtNumUpdated.Name = "txtNumUpdated";
			this.txtNumUpdated.Style = "text-align: left";
			this.txtNumUpdated.Text = null;
			this.txtNumUpdated.Top = 0.4375F;
			this.txtNumUpdated.Width = 0.75F;
			// 
			// txtNumNew
			// 
			this.txtNumNew.Height = 0.1770833F;
			this.txtNumNew.Left = 1.6875F;
			this.txtNumNew.Name = "txtNumNew";
			this.txtNumNew.Style = "text-align: left";
			this.txtNumNew.Text = null;
			this.txtNumNew.Top = 0.625F;
			this.txtNumNew.Width = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: left";
			this.Label20.Text = "Updated";
			this.Label20.Top = 0.4375F;
			this.Label20.Width = 0.75F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: left";
			this.Label12.Text = "New";
			this.Label12.Top = 0.625F;
			this.Label12.Width = 0.75F;
			// 
			// txtNewland
			// 
			this.txtNewland.Height = 0.1770833F;
			this.txtNewland.Left = 2.6875F;
			this.txtNewland.Name = "txtNewland";
			this.txtNewland.Style = "text-align: right";
			this.txtNewland.Text = null;
			this.txtNewland.Top = 0.625F;
			this.txtNewland.Width = 1.25F;
			// 
			// txtNewBldg
			// 
			this.txtNewBldg.Height = 0.1770833F;
			this.txtNewBldg.Left = 4F;
			this.txtNewBldg.Name = "txtNewBldg";
			this.txtNewBldg.Style = "text-align: right";
			this.txtNewBldg.Text = null;
			this.txtNewBldg.Top = 0.625F;
			this.txtNewBldg.Width = 1.25F;
			// 
			// txtNewExempt
			// 
			this.txtNewExempt.Height = 0.1770833F;
			this.txtNewExempt.Left = 5.3125F;
			this.txtNewExempt.Name = "txtNewExempt";
			this.txtNewExempt.Style = "text-align: right";
			this.txtNewExempt.Text = null;
			this.txtNewExempt.Top = 0.625F;
			this.txtNewExempt.Width = 1.25F;
			// 
			// txtNotUpdatedLand
			// 
			this.txtNotUpdatedLand.Height = 0.1770833F;
			this.txtNotUpdatedLand.Left = 2.6875F;
			this.txtNotUpdatedLand.Name = "txtNotUpdatedLand";
			this.txtNotUpdatedLand.Style = "text-align: right";
			this.txtNotUpdatedLand.Text = null;
			this.txtNotUpdatedLand.Top = 0.8125F;
			this.txtNotUpdatedLand.Width = 1.25F;
			// 
			// txtNotUpdatedBldg
			// 
			this.txtNotUpdatedBldg.Height = 0.1770833F;
			this.txtNotUpdatedBldg.Left = 4F;
			this.txtNotUpdatedBldg.Name = "txtNotUpdatedBldg";
			this.txtNotUpdatedBldg.Style = "text-align: right";
			this.txtNotUpdatedBldg.Text = null;
			this.txtNotUpdatedBldg.Top = 0.8125F;
			this.txtNotUpdatedBldg.Width = 1.25F;
			// 
			// txtNotUpdatedExempt
			// 
			this.txtNotUpdatedExempt.Height = 0.1770833F;
			this.txtNotUpdatedExempt.Left = 5.3125F;
			this.txtNotUpdatedExempt.Name = "txtNotUpdatedExempt";
			this.txtNotUpdatedExempt.Style = "text-align: right";
			this.txtNotUpdatedExempt.Text = null;
			this.txtNotUpdatedExempt.Top = 0.8125F;
			this.txtNotUpdatedExempt.Width = 1.25F;
			// 
			// txtTotLand
			// 
			this.txtTotLand.Height = 0.1770833F;
			this.txtTotLand.Left = 2.6875F;
			this.txtTotLand.Name = "txtTotLand";
			this.txtTotLand.Style = "text-align: right";
			this.txtTotLand.Text = null;
			this.txtTotLand.Top = 1.0625F;
			this.txtTotLand.Width = 1.25F;
			// 
			// txtTotBldg
			// 
			this.txtTotBldg.Height = 0.1770833F;
			this.txtTotBldg.Left = 4F;
			this.txtTotBldg.Name = "txtTotBldg";
			this.txtTotBldg.Style = "text-align: right";
			this.txtTotBldg.Text = null;
			this.txtTotBldg.Top = 1.0625F;
			this.txtTotBldg.Width = 1.25F;
			// 
			// txtTotExemption
			// 
			this.txtTotExemption.Height = 0.1770833F;
			this.txtTotExemption.Left = 5.3125F;
			this.txtTotExemption.Name = "txtTotExemption";
			this.txtTotExemption.Style = "text-align: right";
			this.txtTotExemption.Text = null;
			this.txtTotExemption.Top = 1.0625F;
			this.txtTotExemption.Width = 1.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.645833F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9895833F;
			this.Line1.Width = 3.96875F;
			this.Line1.X1 = 2.645833F;
			this.Line1.X2 = 6.614583F;
			this.Line1.Y1 = 0.9895833F;
			this.Line1.Y2 = 0.9895833F;
			// 
			// rptOdonellSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotUpdatedLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotUpdatedBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotUpdatedExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdatedLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdatedBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdatedExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotUpdatedLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotUpdatedBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotUpdatedExemption;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumNew;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotUpdatedLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotUpdatedBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotUpdatedExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotExemption;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
