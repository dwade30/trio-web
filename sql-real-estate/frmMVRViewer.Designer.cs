﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

//using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMVRViewer.
	/// </summary>
	partial class frmMVRViewer : BaseForm
	{
		public fecherFoundation.FCRichTextBox txtRichText;
		public fecherFoundation.FCComboBox cmbInput;
		public Global.T2KDateBox t2kInput;
		public FCGrid GridValuation;
		public FCGrid Grid;
		public fecherFoundation.FCTextBox txtInput;
		public ARViewer ARViewer21;
		public fecherFoundation.FCLine LineRatio;
		public fecherFoundation.FCButton cmdLoadLast;
		public fecherFoundation.FCButton cmdExcelDoc;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMVRViewer));
            this.txtRichText = new fecherFoundation.FCRichTextBox();
            this.cmbInput = new fecherFoundation.FCComboBox();
            this.t2kInput = new Global.T2KDateBox();
            this.GridValuation = new fecherFoundation.FCGrid();
            this.Grid = new fecherFoundation.FCGrid();
            this.txtInput = new fecherFoundation.FCTextBox();
            this.ARViewer21 = new Global.ARViewer();
            this.LineRatio = new fecherFoundation.FCLine();
            this.cmdLoadLast = new fecherFoundation.FCButton();
            this.cmdExcelDoc = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRichText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridValuation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExcelDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 452);
            this.BottomPanel.Size = new System.Drawing.Size(978, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtRichText);
            this.ClientArea.Controls.Add(this.cmbInput);
            this.ClientArea.Controls.Add(this.t2kInput);
            this.ClientArea.Controls.Add(this.GridValuation);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.txtInput);
            this.ClientArea.Controls.Add(this.ARViewer21);
            this.ClientArea.Controls.Add(this.LineRatio);
            this.ClientArea.Size = new System.Drawing.Size(998, 469);
            this.ClientArea.Controls.SetChildIndex(this.LineRatio, 0);
            this.ClientArea.Controls.SetChildIndex(this.ARViewer21, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtInput, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridValuation, 0);
            this.ClientArea.Controls.SetChildIndex(this.t2kInput, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbInput, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtRichText, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdExcelDoc);
            this.TopPanel.Controls.Add(this.cmdLoadLast);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLoadLast, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdExcelDoc, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // txtRichText
            // 
            this.txtRichText.Location = new System.Drawing.Point(97, 387);
            this.txtRichText.Name = "txtRichText";
            this.txtRichText.Size = new System.Drawing.Size(228, 40);
            this.txtRichText.TabIndex = 6;
            // 
            // cmbInput
            // 
            this.cmbInput.BackColor = System.Drawing.SystemColors.Window;
            this.cmbInput.Items.AddRange(new object[] {
            "False / No",
            "True / Yes"});
            this.cmbInput.Location = new System.Drawing.Point(81, 319);
            this.cmbInput.Name = "cmbInput";
            this.cmbInput.Size = new System.Drawing.Size(130, 40);
            this.cmbInput.TabIndex = 5;
            this.cmbInput.SelectedIndexChanged += new System.EventHandler(this.cmbInput_SelectedIndexChanged);
            // 
            // t2kInput
            // 
            this.t2kInput.Location = new System.Drawing.Point(324, 344);
            this.t2kInput.Mask = "##/##/####";
            this.t2kInput.Name = "t2kInput";
            this.t2kInput.Size = new System.Drawing.Size(122, 22);
            this.t2kInput.TabIndex = 4;
            this.t2kInput.Visible = false;
            this.t2kInput.Validating += new System.ComponentModel.CancelEventHandler(this.t2kInput_Validate);
            this.t2kInput.KeyDown += new Wisej.Web.KeyEventHandler(this.t2kInput_KeyDownEvent);
            // 
            // GridValuation
            // 
            this.GridValuation.Cols = 7;
            this.GridValuation.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridValuation.ExtendLastCol = true;
            this.GridValuation.Location = new System.Drawing.Point(8, 75);
            this.GridValuation.Name = "GridValuation";
            this.GridValuation.ReadOnly = false;
            this.GridValuation.Rows = 9;
            this.GridValuation.Size = new System.Drawing.Size(586, 144);
            this.GridValuation.StandardTab = false;
            this.GridValuation.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridValuation.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.GridValuation, "Hit ENTER to apply changes and exit edit mode");
            this.GridValuation.Visible = false;
            this.GridValuation.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridValuation_KeyDownEdit);
            this.GridValuation.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.GridValuation_KeyPressEdit);
            this.GridValuation.KeyDown += new Wisej.Web.KeyEventHandler(this.GridValuation_KeyDownEvent);
            // 
            // Grid
            // 
            this.Grid.Cols = 10;
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.FixedCols = 0;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(574, 41);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 0;
            this.Grid.Size = new System.Drawing.Size(25, 18);
            this.Grid.TabIndex = 2;
            this.Grid.Visible = false;
            // 
            // txtInput
            // 
            this.txtInput.BackColor = System.Drawing.SystemColors.Window;
            this.txtInput.Location = new System.Drawing.Point(73, 277);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(114, 40);
            this.txtInput.TabIndex = 1;
            this.txtInput.Text = "Text1";
            this.txtInput.Validating += new System.ComponentModel.CancelEventHandler(this.txtInput_Validating);
            this.txtInput.KeyDown += new Wisej.Web.KeyEventHandler(this.txtInput_KeyDown);
            // 
            // ARViewer21
            // 
            this.ARViewer21.Dock = Wisej.Web.DockStyle.Fill;
            this.ARViewer21.Name = "ARViewer21";
            this.ARViewer21.Size = new System.Drawing.Size(981, 452);
            this.ARViewer21.TabIndex = 1001;
            this.ARViewer21.MouseDown += new Wisej.Web.MouseEventHandler(this.ARViewer21_MouseDownEvent);
            this.ARViewer21.MouseHover += new System.EventHandler(this.ARViewer21_MouseOver);
            // 
            // LineRatio
            // 
            this.LineRatio.Name = "LineRatio";
            this.LineRatio.Size = new System.Drawing.Size(1440, 1);
            this.LineRatio.Visible = false;
            this.LineRatio.X2 = 1440F;
            // 
            // cmdLoadLast
            // 
            this.cmdLoadLast.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLoadLast.Location = new System.Drawing.Point(583, 29);
            this.cmdLoadLast.Name = "cmdLoadLast";
            this.cmdLoadLast.Size = new System.Drawing.Size(117, 24);
            this.cmdLoadLast.TabIndex = 1;
            this.cmdLoadLast.Text = "Load Last Saved";
            this.cmdLoadLast.Click += new System.EventHandler(this.mnuLoadLast_Click);
            // 
            // cmdExcelDoc
            // 
            this.cmdExcelDoc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdExcelDoc.Location = new System.Drawing.Point(704, 29);
            this.cmdExcelDoc.Name = "cmdExcelDoc";
            this.cmdExcelDoc.Size = new System.Drawing.Size(218, 24);
            this.cmdExcelDoc.TabIndex = 2;
            this.cmdExcelDoc.Text = "Create Excel Document for State";
            this.cmdExcelDoc.Click += new System.EventHandler(this.mnuExcelDoc_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(925, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(46, 24);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(421, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmMVRViewer
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(998, 529);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmMVRViewer";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Municipal Valuation Return";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmMVRViewer_Load);
            this.Resize += new System.EventHandler(this.frmMVRViewer_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMVRViewer_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRichText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridValuation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExcelDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
