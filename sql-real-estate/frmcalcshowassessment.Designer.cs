﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for Frmcalcshowassessment.
	/// </summary>
	partial class Frmcalcshowassessment : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCComboBox cmbtupdate;
		public fecherFoundation.FCLabel lbltupdate;
		public fecherFoundation.FCComboBox cmbshow;
		public fecherFoundation.FCLabel lblshow;
		public fecherFoundation.FCComboBox cmbpresentation;
		public fecherFoundation.FCLabel lblpresentation;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblTo;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmcalcshowassessment));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.lblReport = new fecherFoundation.FCLabel();
            this.cmbtupdate = new fecherFoundation.FCComboBox();
            this.lbltupdate = new fecherFoundation.FCLabel();
            this.cmbshow = new fecherFoundation.FCComboBox();
            this.lblshow = new fecherFoundation.FCLabel();
            this.cmbpresentation = new fecherFoundation.FCComboBox();
            this.lblpresentation = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.cmdContinue = new Wisej.Web.Button();
            this.chkExportPDFs = new Wisej.Web.CheckBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 358);
            this.BottomPanel.Size = new System.Drawing.Size(677, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkExportPDFs);
            this.ClientArea.Controls.Add(this.cmbReport);
            this.ClientArea.Controls.Add(this.lblReport);
            this.ClientArea.Controls.Add(this.cmbtupdate);
            this.ClientArea.Controls.Add(this.lbltupdate);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.cmbshow);
            this.ClientArea.Controls.Add(this.lblshow);
            this.ClientArea.Controls.Add(this.cmbpresentation);
            this.ClientArea.Controls.Add(this.lblpresentation);
            this.ClientArea.Size = new System.Drawing.Size(697, 447);
            this.ClientArea.Controls.SetChildIndex(this.lblpresentation, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbpresentation, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblshow, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbshow, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lbltupdate, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbtupdate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReport, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbReport, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkExportPDFs, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(697, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(191, 28);
            this.HeaderText.Text = "Batch Calculation";
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Summary",
            "Valuation",
            "One Page Card"});
            this.cmbReport.Location = new System.Drawing.Point(242, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(169, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.Text = "Summary";
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.cmbReport_SelectedIndexChanged);
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(30, 44);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(96, 15);
            this.lblReport.TabIndex = 10;
            this.lblReport.Text = "REPORT TYPE";
            // 
            // cmbtupdate
            // 
            this.cmbtupdate.Items.AddRange(new object[] {
            "Update",
            "Don\'t Update"});
            this.cmbtupdate.Location = new System.Drawing.Point(242, 150);
            this.cmbtupdate.Name = "cmbtupdate";
            this.cmbtupdate.Size = new System.Drawing.Size(169, 40);
            this.cmbtupdate.TabIndex = 5;
            this.cmbtupdate.Text = "Update";
            this.cmbtupdate.SelectedIndexChanged += new System.EventHandler(this.cmbtupdate_SelectedIndexChanged);
            // 
            // lbltupdate
            // 
            this.lbltupdate.Location = new System.Drawing.Point(30, 164);
            this.lbltupdate.Name = "lbltupdate";
            this.lbltupdate.Size = new System.Drawing.Size(204, 18);
            this.lbltupdate.TabIndex = 4;
            this.lbltupdate.Text = "SAVED CORRELATED VALUES";
            // 
            // cmbshow
            // 
            this.cmbshow.Items.AddRange(new object[] {
            "Regular",
            "Changes Only"});
            this.cmbshow.Location = new System.Drawing.Point(242, 90);
            this.cmbshow.Name = "cmbshow";
            this.cmbshow.Size = new System.Drawing.Size(169, 40);
            this.cmbshow.TabIndex = 3;
            this.cmbshow.Text = "Regular";
            this.cmbshow.SelectedIndexChanged += new System.EventHandler(this.cmbshow_SelectedIndexChanged);
            // 
            // lblshow
            // 
            this.lblshow.AutoSize = true;
            this.lblshow.Location = new System.Drawing.Point(30, 104);
            this.lblshow.Name = "lblshow";
            this.lblshow.Size = new System.Drawing.Size(44, 15);
            this.lblshow.TabIndex = 2;
            this.lblshow.Text = "SHOW";
            // 
            // cmbpresentation
            // 
            this.cmbpresentation.Items.AddRange(new object[] {
            "Account Number",
            "Name",
            "Map/Lot Number"});
            this.cmbpresentation.Location = new System.Drawing.Point(242, 210);
            this.cmbpresentation.Name = "cmbpresentation";
            this.cmbpresentation.Size = new System.Drawing.Size(169, 40);
            this.cmbpresentation.TabIndex = 7;
            this.cmbpresentation.Text = "Account Number";
            this.cmbpresentation.SelectedIndexChanged += new System.EventHandler(this.cmbpresentation_SelectedIndexChanged);
            // 
            // lblpresentation
            // 
            this.lblpresentation.AutoSize = true;
            this.lblpresentation.Location = new System.Drawing.Point(30, 224);
            this.lblpresentation.Name = "lblpresentation";
            this.lblpresentation.Size = new System.Drawing.Size(72, 15);
            this.lblpresentation.TabIndex = 6;
            this.lblpresentation.Text = "ORDER BY";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtEnd);
            this.Frame3.Controls.Add(this.txtStart);
            this.Frame3.Controls.Add(this.cmbRange);
            this.Frame3.Controls.Add(this.lblTo);
            this.Frame3.Location = new System.Drawing.Point(431, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(205, 220);
            this.Frame3.TabIndex = 8;
            this.Frame3.Text = "Include";
            // 
            // txtEnd
            // 
            this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnd.Location = new System.Drawing.Point(20, 163);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(164, 40);
            this.txtEnd.TabIndex = 3;
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(20, 90);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(164, 40);
            this.txtStart.TabIndex = 1;
            // 
            // cmbRange
            // 
            this.cmbRange.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRange.Location = new System.Drawing.Point(20, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(164, 40);
            this.cmbRange.TabIndex = 4;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(86, 140);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(35, 13);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "TO";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Location = new System.Drawing.Point(300, 30);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(100, 48);
            this.cmdContinue.TabIndex = 0;
            this.cmdContinue.Text = "Continue";
            this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // chkExportPDFs
            // 
            this.chkExportPDFs.Anchor = Wisej.Web.AnchorStyles.Left;
            this.chkExportPDFs.Location = new System.Drawing.Point(30, 336);
            this.chkExportPDFs.Name = "chkExportPDFs";
            this.chkExportPDFs.Size = new System.Drawing.Size(106, 22);
            this.chkExportPDFs.TabIndex = 9;
            this.chkExportPDFs.Text = "Export PDFs";
            this.chkExportPDFs.Visible = false;
            // 
            // Frmcalcshowassessment
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(697, 507);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "Frmcalcshowassessment";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Batch Calculation";
            this.Load += new System.EventHandler(this.Frmcalcshowassessment_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.Frmcalcshowassessment_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdContinue;
        private CheckBox chkExportPDFs;
    }
}
