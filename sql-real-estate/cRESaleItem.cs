﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cRESaleItem
	{
		//=========================================================
		private int intSaleVerification;
		private int intSaleFinancing;
		private int intSaleType;
		private double dblSalePrice;
		private int intSaleValidity;
		private string strSaleDate = "";

		public string SaleDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strSaleDate = value;
				}
				else
				{
					strSaleDate = "";
				}
			}
			get
			{
				string SaleDate = "";
				SaleDate = strSaleDate;
				return SaleDate;
			}
		}

		public short ValidityCode
		{
			set
			{
				intSaleValidity = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short ValidityCode = 0;
				ValidityCode = FCConvert.ToInt16(intSaleValidity);
				return ValidityCode;
			}
		}

		public double Price
		{
			set
			{
				dblSalePrice = value;
			}
			get
			{
				double Price = 0;
				Price = dblSalePrice;
				return Price;
			}
		}

		public short SaleTypeCode
		{
			set
			{
				intSaleType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short SaleTypeCode = 0;
				SaleTypeCode = FCConvert.ToInt16(intSaleType);
				return SaleTypeCode;
			}
		}

		public short FinancingCode
		{
			set
			{
				intSaleFinancing = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FinancingCode = 0;
				FinancingCode = FCConvert.ToInt16(intSaleFinancing);
				return FinancingCode;
			}
		}

		public short VerificationCode
		{
			set
			{
				intSaleVerification = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short VerificationCode = 0;
				VerificationCode = FCConvert.ToInt16(intSaleVerification);
				return VerificationCode;
			}
		}
	}
}
