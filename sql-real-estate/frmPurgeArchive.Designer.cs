﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPurgeArchive.
	/// </summary>
	partial class frmPurgeArchive : BaseForm
	{
		public fecherFoundation.FCButton cmdPurge;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblInstructions;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeArchive));
            this.cmdPurge = new fecherFoundation.FCButton();
            this.txtDate = new Global.T2KDateBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 344);
            this.BottomPanel.Size = new System.Drawing.Size(549, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPurge);
            this.ClientArea.Controls.Add(this.txtDate);
            this.ClientArea.Controls.Add(this.lblInstructions);
            this.ClientArea.Size = new System.Drawing.Size(549, 284);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(549, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(167, 30);
            this.HeaderText.Text = "Purge Archive";
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "acceptButton";
            this.cmdPurge.ForeColor = System.Drawing.Color.White;
            this.cmdPurge.Location = new System.Drawing.Point(30, 161);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPurge.Size = new System.Drawing.Size(115, 48);
            this.cmdPurge.TabIndex = 2;
            this.cmdPurge.Text = "Purge";
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(30, 90);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(115, 40);
            this.txtDate.TabIndex = 1;
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(505, 42);
            this.lblInstructions.Text = "PLEASE ENTER A DATE AT LEAST ONE YEAR IN THE PAST AND CLICK THE \'PURGE\' BUTTON.  " +
    "ALL ARCHIVE INFORMATION THAT THAT WAS SAVED ON OR BEFORE THAT DATE WILL BE DELET" +
    "ED";
            // 
            // frmPurgeArchive
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(549, 452);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPurgeArchive";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Archive";
            this.Load += new System.EventHandler(this.frmPurgeArchive_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeArchive_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
