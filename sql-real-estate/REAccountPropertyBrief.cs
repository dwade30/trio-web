﻿namespace TWRE0000
{
    public class REAccountPropertyBrief
    {
        public int Account { get; set; } = 0;
        public string MapLot { get; set; } = "";
        public string DeedName1 { get; set; } = "";
        public string DeedName2 { get; set; } = "";
        public RELocation Location { get;  }

        public REAccountPropertyBrief()
        {
            Location = new RELocation()
            {
                Apartment =  "",
                StreetName =  "",
                StreetNumber = ""
            };
        }
    }
}