﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmXXmsg.
	/// </summary>
	partial class frmXXmsg : BaseForm
	{
		public fecherFoundation.FCButton cmdProcessNext;
		public fecherFoundation.FCButton cmdProcessAll;
		public fecherFoundation.FCLabel lblNew;
		public fecherFoundation.FCLabel lblExisting;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmXXmsg));
			this.cmdProcessNext = new fecherFoundation.FCButton();
			this.cmdProcessAll = new fecherFoundation.FCButton();
			this.lblNew = new fecherFoundation.FCLabel();
			this.lblExisting = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessNext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessAll)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 362);
			this.BottomPanel.Size = new System.Drawing.Size(599, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcessNext);
			this.ClientArea.Controls.Add(this.cmdProcessAll);
			this.ClientArea.Controls.Add(this.lblNew);
			this.ClientArea.Controls.Add(this.lblExisting);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(599, 302);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(599, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(209, 30);
			this.HeaderText.Text = "Process Map Lots";
			// 
			// cmdProcessNext
			// 
			this.cmdProcessNext.AppearanceKey = "actionButton";
			this.cmdProcessNext.Location = new System.Drawing.Point(193, 239);
			this.cmdProcessNext.Name = "cmdProcessNext";
			this.cmdProcessNext.Size = new System.Drawing.Size(153, 40);
			this.cmdProcessNext.TabIndex = 1;
			this.cmdProcessNext.Text = "Process Next";
			this.cmdProcessNext.Click += new System.EventHandler(this.Command2_Click);
			// 
			// cmdProcessAll
			// 
			this.cmdProcessAll.AppearanceKey = "actionButton";
			this.cmdProcessAll.Location = new System.Drawing.Point(30, 239);
			this.cmdProcessAll.Name = "cmdProcessAll";
			this.cmdProcessAll.Size = new System.Drawing.Size(133, 40);
			this.cmdProcessAll.TabIndex = 0;
			this.cmdProcessAll.Text = "Process All";
			this.cmdProcessAll.Click += new System.EventHandler(this.Command1_Click);
			// 
			// lblNew
			// 
			this.lblNew.Location = new System.Drawing.Point(30, 155);
			this.lblNew.Name = "lblNew";
			this.lblNew.Size = new System.Drawing.Size(163, 19);
			this.lblNew.TabIndex = 7;
			// 
			// lblExisting
			// 
			this.lblExisting.Location = new System.Drawing.Point(30, 63);
			this.lblExisting.Name = "lblExisting";
			this.lblExisting.Size = new System.Drawing.Size(165, 19);
			this.lblExisting.TabIndex = 6;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 112);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(110, 13);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "NEW MAP-LOT FORMAT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(122, 13);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "EXISTING MAP-LOT FORMAT";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 194);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(242, 15);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "HOW WOULD YOU LIKE TO CONTINUE?";
			// 
			// frmXXmsg
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(599, 470);
			this.ControlBox = false;
			this.FillColor = 0;
			this.Name = "frmXXmsg";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Process Map Lots";
			this.Load += new System.EventHandler(this.frmXXmsg_Load);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessNext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessAll)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
