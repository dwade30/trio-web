﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetSaleInfo.
	/// </summary>
	partial class frmGetSaleInfo : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public fecherFoundation.FCComboBox cmbValidity;
		public fecherFoundation.FCComboBox cmbVerified;
		public fecherFoundation.FCComboBox cmbFinancing;
		public fecherFoundation.FCComboBox cmbSale;
		public fecherFoundation.FCTextBox Txtmrpisaleprice;
		public Global.T2KDateBox txtMRPISaleDate;
		public fecherFoundation.FCLabel Label8_27;
		public fecherFoundation.FCLabel Label8_26;
		public fecherFoundation.FCLabel Label8_25;
		public fecherFoundation.FCLabel Label8_24;
		public fecherFoundation.FCLabel Label8_22;
		public fecherFoundation.FCLabel Label8_23;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetSaleInfo));
            this.cmbValidity = new fecherFoundation.FCComboBox();
            this.cmbVerified = new fecherFoundation.FCComboBox();
            this.cmbFinancing = new fecherFoundation.FCComboBox();
            this.cmbSale = new fecherFoundation.FCComboBox();
            this.Txtmrpisaleprice = new fecherFoundation.FCTextBox();
            this.txtMRPISaleDate = new Global.T2KDateBox();
            this.Label8_27 = new fecherFoundation.FCLabel();
            this.Label8_26 = new fecherFoundation.FCLabel();
            this.Label8_25 = new fecherFoundation.FCLabel();
            this.Label8_24 = new fecherFoundation.FCLabel();
            this.Label8_22 = new fecherFoundation.FCLabel();
            this.Label8_23 = new fecherFoundation.FCLabel();
            this.cmdSave = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMRPISaleDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 435);
            this.BottomPanel.Size = new System.Drawing.Size(407, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbValidity);
            this.ClientArea.Controls.Add(this.cmbVerified);
            this.ClientArea.Controls.Add(this.cmbFinancing);
            this.ClientArea.Controls.Add(this.cmbSale);
            this.ClientArea.Controls.Add(this.Txtmrpisaleprice);
            this.ClientArea.Controls.Add(this.txtMRPISaleDate);
            this.ClientArea.Controls.Add(this.Label8_27);
            this.ClientArea.Controls.Add(this.Label8_26);
            this.ClientArea.Controls.Add(this.Label8_25);
            this.ClientArea.Controls.Add(this.Label8_24);
            this.ClientArea.Controls.Add(this.Label8_22);
            this.ClientArea.Controls.Add(this.Label8_23);
            this.ClientArea.Size = new System.Drawing.Size(407, 375);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(407, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(120, 30);
            this.HeaderText.Text = "Sale Data";
            // 
            // cmbValidity
            // 
            this.cmbValidity.AutoSize = false;
            this.cmbValidity.BackColor = System.Drawing.SystemColors.Window;
            this.cmbValidity.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbValidity.FormattingEnabled = true;
            this.cmbValidity.Location = new System.Drawing.Point(136, 330);
            this.cmbValidity.Name = "cmbValidity";
            this.cmbValidity.Size = new System.Drawing.Size(243, 40);
            this.cmbValidity.TabIndex = 11;
            // 
            // cmbVerified
            // 
            this.cmbVerified.AutoSize = false;
            this.cmbVerified.BackColor = System.Drawing.SystemColors.Window;
            this.cmbVerified.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbVerified.FormattingEnabled = true;
            this.cmbVerified.Location = new System.Drawing.Point(136, 270);
            this.cmbVerified.Name = "cmbVerified";
            this.cmbVerified.Size = new System.Drawing.Size(243, 40);
            this.cmbVerified.TabIndex = 10;
            // 
            // cmbFinancing
            // 
            this.cmbFinancing.AutoSize = false;
            this.cmbFinancing.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFinancing.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFinancing.FormattingEnabled = true;
            this.cmbFinancing.Location = new System.Drawing.Point(136, 210);
            this.cmbFinancing.Name = "cmbFinancing";
            this.cmbFinancing.Size = new System.Drawing.Size(243, 40);
            this.cmbFinancing.TabIndex = 9;
            // 
            // cmbSale
            // 
            this.cmbSale.AutoSize = false;
            this.cmbSale.BackColor = System.Drawing.SystemColors.Window;
            this.cmbSale.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSale.FormattingEnabled = true;
            this.cmbSale.Location = new System.Drawing.Point(136, 150);
            this.cmbSale.Name = "cmbSale";
            this.cmbSale.Size = new System.Drawing.Size(243, 40);
            this.cmbSale.TabIndex = 8;
            // 
            // Txtmrpisaleprice
            // 
            this.Txtmrpisaleprice.AutoSize = false;
            this.Txtmrpisaleprice.BackColor = System.Drawing.SystemColors.Window;
            this.Txtmrpisaleprice.LinkItem = null;
            this.Txtmrpisaleprice.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.Txtmrpisaleprice.LinkTopic = null;
            this.Txtmrpisaleprice.Location = new System.Drawing.Point(135, 30);
            this.Txtmrpisaleprice.Name = "Txtmrpisaleprice";
            this.Txtmrpisaleprice.Size = new System.Drawing.Size(120, 40);
            this.Txtmrpisaleprice.TabIndex = 0;
            this.Txtmrpisaleprice.Tag = "sale";
            this.Txtmrpisaleprice.Text = "0";
            this.Txtmrpisaleprice.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtMRPISaleDate
            // 
            this.txtMRPISaleDate.Location = new System.Drawing.Point(135, 90);
            this.txtMRPISaleDate.Mask = "##/##/####";
            this.txtMRPISaleDate.MaxLength = 10;
            this.txtMRPISaleDate.Name = "txtMRPISaleDate";
            this.txtMRPISaleDate.Size = new System.Drawing.Size(120, 40);
            this.txtMRPISaleDate.TabIndex = 1;
            this.txtMRPISaleDate.Tag = "sale";
            this.txtMRPISaleDate.Text = "  /  /";
            // 
            // Label8_27
            // 
            this.Label8_27.Location = new System.Drawing.Point(30, 344);
            this.Label8_27.Name = "Label8_27";
            this.Label8_27.Size = new System.Drawing.Size(54, 15);
            this.Label8_27.TabIndex = 7;
            this.Label8_27.Text = "VALIDITY";
            // 
            // Label8_26
            // 
            this.Label8_26.Location = new System.Drawing.Point(30, 284);
            this.Label8_26.Name = "Label8_26";
            this.Label8_26.Size = new System.Drawing.Size(54, 15);
            this.Label8_26.TabIndex = 6;
            this.Label8_26.Text = "VERIFIED";
            // 
            // Label8_25
            // 
            this.Label8_25.Location = new System.Drawing.Point(30, 224);
            this.Label8_25.Name = "Label8_25";
            this.Label8_25.Size = new System.Drawing.Size(70, 15);
            this.Label8_25.TabIndex = 5;
            this.Label8_25.Text = "FINANCING";
            // 
            // Label8_24
            // 
            this.Label8_24.Location = new System.Drawing.Point(30, 164);
            this.Label8_24.Name = "Label8_24";
            this.Label8_24.Size = new System.Drawing.Size(66, 15);
            this.Label8_24.TabIndex = 4;
            this.Label8_24.Text = "SALE TYPE";
            // 
            // Label8_22
            // 
            this.Label8_22.Location = new System.Drawing.Point(30, 104);
            this.Label8_22.Name = "Label8_22";
            this.Label8_22.Size = new System.Drawing.Size(70, 15);
            this.Label8_22.TabIndex = 3;
            this.Label8_22.Text = "SALE DATE";
            // 
            // Label8_23
            // 
            this.Label8_23.Location = new System.Drawing.Point(30, 44);
            this.Label8_23.Name = "Label8_23";
            this.Label8_23.Size = new System.Drawing.Size(70, 18);
            this.Label8_23.TabIndex = 2;
            this.Label8_23.Text = "SALE PRICE";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(116, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(174, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmGetSaleInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(407, 543);
            this.ControlBox = false;
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGetSaleInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Sale Data";
            this.Load += new System.EventHandler(this.frmGetSaleInfo_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetSaleInfo_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMRPISaleDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSave;
	}
}
