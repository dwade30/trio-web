﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTaxAcquired.
	/// </summary>
	public partial class rptTaxAcquired : BaseSectionReport
	{
		public static rptTaxAcquired InstancePtr
		{
			get
			{
				return (rptTaxAcquired)Sys.GetInstance(typeof(rptTaxAcquired));
			}
		}

		protected rptTaxAcquired _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptTaxAcquired()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Acquired";
		}
		// nObj = 1
		//   0	rptTaxAcquired	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		double dblTotalAssess;
		// vbPorter upgrade warning: intOrderBy As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngReportType, short intOrderBy = 0)
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsReport.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			string strSQL;
			string strFName = "";
			strSQL = "(select rsaccount as account,sum(lastlandval & '') as landsum,sum(lastbldgval & '') as bldgsum,sum(rlexemption & '') as exemptsum from master group by rsaccount)  tbl1 ";
			// strSQL = "select * from master inner join " & strSQL & " on (tbl1.account = master.rsaccount) where not rsdeleted = 1 and rscard = 1 "
			strSQL = "select * from " + strMasterJoinQuery + " inner join " + strSQL + " on (tbl1.account = mj.rsaccount) where not mj.rsdeleted = 1 and mj.rscard = 1 ";
			switch (lngReportType)
			{
				case modGlobalVariables.CNSTREPORTTYPETAXACQUIRED:
					{
						strSQL += " and taxacquired = 1 ";
						strFName = "TaxAcquired";
						lblTitle.Text = "Tax Acquired";
						break;
					}
				case modGlobalVariables.CNSTREPORTTYPEBANKRUPTCY:
					{
						strSQL += " and inbankruptcy = 1";
						strFName = "Bankruptcy";
						lblTitle.Text = "In Bankruptcy";
						break;
					}
			}
			//end switch
			switch (intOrderBy)
			{
				case 0:
					{
						// account
						strSQL += " order by rsaccount,rscard ";
						break;
					}
				case 1:
					{
						// name
						strSQL += " order by rsname,rsaccount,rscard ";
						break;
					}
				case 2:
					{
						// maplot
						strSQL += " order by rsmaplot,rsaccount,rscard ";
						break;
					}
				case 3:
					{
						// location
						strSQL += " order by rslocstreet,convert(int, rslocnumalph),rsaccount,rscard ";
						break;
					}
			}
			//end switch
			clsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			dblTotalAssess = 0;
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, strFName);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			double dblAssess = 0;
			if (!clsReport.EndOfFile())
			{
				txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("rsaccount"));
				txtName.Text = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rsname")));
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rsmaplot")));
				strTemp = "";
				if (Conversion.Val(clsReport.Get_Fields_String("rslocnumalph")) > 0)
				{
					strTemp = FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_String("rslocnumalph"))) + " ";
				}
				if (Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rslocapt"))) != string.Empty)
				{
					strTemp += Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rslocapt"))) + " ";
				}
				strTemp += Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rslocstreet")));
				strTemp = Strings.Trim(strTemp);
				txtLocation.Text = strTemp;
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				dblAssess = Conversion.Val(clsReport.Get_Fields("landsum")) + Conversion.Val(clsReport.Get_Fields("bldgsum")) - Conversion.Val(clsReport.Get_Fields("exemptsum"));
				dblTotalAssess += dblAssess;
				txtAssessment.Text = Strings.Format(dblAssess, "#,###,###,##0");
				clsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalAssessment.Text = Strings.Format(dblTotalAssess, "#,###,###,##0");
		}

		
	}
}
