﻿//Fecher vbPorter - Version 1.0.0.40
using Global;
using Wisej.Web;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for Frmcalcexemptions.
	/// </summary>
	public partial class Frmcalcexemptions : BaseForm
	{
		public Frmcalcexemptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static Frmcalcexemptions InstancePtr
		{
			get
			{
				return (Frmcalcexemptions)Sys.GetInstance(typeof(Frmcalcexemptions));
			}
		}

		protected Frmcalcexemptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool stillcalculate;
		// the program checks this periodically to see if it should abort
		public void Initialize()
		{
			// lblMuniname.Caption = MuniName
			// lblDate.Caption = Date$
			// lbltime.Caption = Time
			ProgressBar1.Value = 0;
			lblpercentdone.Text = "0%";
			stillcalculate = true;
			//FC:FINAL:MSH - i.issue #1070: bring form to front
			this.TopMost = true;
			//FC:FINAL:MSH - i.issue #1070: show form as dialog window
			//Frmcalcexemptions.InstancePtr.Show();
			this.Show(FormShowEnum.Modeless);
			this.Refresh();
		}

		private void cmdcancexempt_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: response As Variant --> As DialogResult
			DialogResult response;
			response = MessageBox.Show("This will stop calculating." + "\r\n" + "Some files will be left calculated and others won't", "Cancel Calculations", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
			if (response == DialogResult.OK)
			{
				stillcalculate = false;
			}
		}

		private void Frmcalcexemptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//Frmcalcexemptions properties;
			//Frmcalcexemptions.ScaleWidth	= 3885;
			//Frmcalcexemptions.ScaleHeight	= 2415;
			//Frmcalcexemptions.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}
	}
}
