﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage9.
	/// </summary>
	partial class srptMVRPage9
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage9));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txt2b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ShapeGrid = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label235 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label237 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label238 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label239 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label241 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label242 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label243 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label245 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label246 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label247 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label248 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label249 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label250 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label251 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label252 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label253 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label254 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label255 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label256 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label257 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label258 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label260 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label261 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label262 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOneFamilyNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOneFamilyDemolished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOneFamilyConverted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOneFamilyIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOneFamilyLoss = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTwoFamilyNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTwoFamilyDemolished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTwoFamilyConverted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTwoFamilyIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTwoFamilyLoss = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThreeFourFamilyNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThreeFourFamilyDemolished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThreeFourFamilyConverted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThreeFourFamilyIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThreeFourFamilyLoss = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverFourNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverFourDemolished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverFourConverted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverFourIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverFourLoss = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMHNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMHDemolished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMHConverted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMHIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMHLoss = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSHNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSHDemolished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSHConverted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSHIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSHLoss = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtOneFamilyNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTwoFamilyNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThreeFourFamilyNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverFourNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMHNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSHNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt2c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt2d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt2e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt2f = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt2g = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt2h = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3f = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3g = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt3h = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4f = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4g = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape70 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4h = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4i = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape72 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txt4j = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txt2b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label235)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label237)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label238)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label239)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label241)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label242)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label243)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label245)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label246)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label247)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label248)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label249)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label250)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label251)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label252)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label253)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label254)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label255)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label256)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label257)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label258)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label260)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label261)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label262)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyDemolished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyConverted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyLoss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyDemolished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyConverted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyLoss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyDemolished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyConverted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyLoss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourDemolished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourConverted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourLoss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHDemolished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHConverted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHLoss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHDemolished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHConverted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHLoss)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2f)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2g)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2h)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3f)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3g)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3h)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4f)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4g)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4h)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4i)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4j)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt2b,
            this.txt2a,
            this.ShapeGrid,
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label175,
            this.Label176,
            this.Label112,
            this.Label235,
            this.txtCounty,
            this.Label237,
            this.Label238,
            this.lblYear1,
            this.Label239,
            this.lblYear2,
            this.Label241,
            this.Label242,
            this.Label243,
            this.lblYear3,
            this.Label245,
            this.Label246,
            this.Label247,
            this.lblYear4,
            this.Label248,
            this.Label249,
            this.Label250,
            this.Label251,
            this.Label252,
            this.Label253,
            this.Label254,
            this.Label255,
            this.Label256,
            this.Label257,
            this.Label258,
            this.Label260,
            this.Label261,
            this.Label262,
            this.txtOneFamilyNew,
            this.txtOneFamilyDemolished,
            this.txtOneFamilyConverted,
            this.txtOneFamilyIncrease,
            this.txtOneFamilyLoss,
            this.txtTwoFamilyNew,
            this.txtTwoFamilyDemolished,
            this.txtTwoFamilyConverted,
            this.txtTwoFamilyIncrease,
            this.txtTwoFamilyLoss,
            this.txtThreeFourFamilyNew,
            this.txtThreeFourFamilyDemolished,
            this.txtThreeFourFamilyConverted,
            this.txtThreeFourFamilyIncrease,
            this.txtThreeFourFamilyLoss,
            this.txtOverFourNew,
            this.txtOverFourDemolished,
            this.txtOverFourConverted,
            this.txtOverFourIncrease,
            this.txtOverFourLoss,
            this.txtMHNew,
            this.txtMHDemolished,
            this.txtMHConverted,
            this.txtMHIncrease,
            this.txtMHLoss,
            this.txtSHNew,
            this.txtSHDemolished,
            this.txtSHConverted,
            this.txtSHIncrease,
            this.txtSHLoss,
            this.Line1,
            this.Line2,
            this.Line3,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line9,
            this.Line10,
            this.Line11,
            this.Line12,
            this.txtOneFamilyNet,
            this.txtTwoFamilyNet,
            this.txtThreeFourFamilyNet,
            this.txtOverFourNet,
            this.txtMHNet,
            this.txtSHNet,
            this.Line13,
            this.Line8,
            this.Line7,
            this.Line14,
            this.Shape1,
            this.Shape2,
            this.Shape47,
            this.Shape48,
            this.Shape49,
            this.txt2c,
            this.Shape50,
            this.txt2d,
            this.Shape51,
            this.txt2e,
            this.Shape52,
            this.txt2f,
            this.Shape53,
            this.txt2g,
            this.Shape54,
            this.txt2h,
            this.Shape55,
            this.txt3a,
            this.Shape56,
            this.txt3b,
            this.Shape57,
            this.txt3c,
            this.Shape58,
            this.txt3d,
            this.Shape59,
            this.txt3e,
            this.Shape60,
            this.txt3f,
            this.Shape61,
            this.txt3g,
            this.Shape62,
            this.txt3h,
            this.Shape63,
            this.txt4a,
            this.Shape64,
            this.txt4b,
            this.Shape65,
            this.txt4c,
            this.Shape66,
            this.txt4d,
            this.Shape67,
            this.txt4e,
            this.Shape68,
            this.txt4f,
            this.Shape69,
            this.txt4g,
            this.Shape70,
            this.txt4h,
            this.Shape71,
            this.txt4i,
            this.Shape72,
            this.txt4j});
			this.Detail.Height = 9.708333F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// txt2b
			// 
			this.txt2b.Height = 0.1875F;
			this.txt2b.Left = 0.5208333F;
			this.txt2b.Name = "txt2b";
			this.txt2b.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2b.Text = " ";
			this.txt2b.Top = 3.75F;
			this.txt2b.Width = 6.416667F;
			// 
			// txt2a
			// 
			this.txt2a.Height = 0.1875F;
			this.txt2a.Left = 0.5208333F;
			this.txt2a.Name = "txt2a";
			this.txt2a.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2a.Text = " ";
			this.txt2a.Top = 3.5625F;
			this.txt2a.Width = 6.416667F;
			// 
			// ShapeGrid
			// 
			this.ShapeGrid.Height = 1.791667F;
			this.ShapeGrid.Left = 0.15625F;
			this.ShapeGrid.Name = "ShapeGrid";
			this.ShapeGrid.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.ShapeGrid.Top = 1.28125F;
			this.ShapeGrid.Width = 6.833333F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.2708333F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.3854167F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 1.083333F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.3854167F;
			this.txtMunicipality.Width = 2.125F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.1145833F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-9-";
			this.Label175.Top = 9.489583F;
			this.Label175.Width = 1F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 1F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label176.Text = "VALUATION INFORMATION";
			this.Label176.Top = 0.6354167F;
			this.Label176.Width = 5.5F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0.375F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label112.Text = "1. Enter the number and type of new, demolished and converted residential buildin" +
    "gs in you municipality since";
			this.Label112.Top = 0.8958333F;
			this.Label112.Width = 6.083333F;
			// 
			// Label235
			// 
			this.Label235.Height = 0.1875F;
			this.Label235.HyperLink = null;
			this.Label235.Left = 3.270833F;
			this.Label235.Name = "Label235";
			this.Label235.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label235.Text = "County:";
			this.Label235.Top = 0.3854167F;
			this.Label235.Width = 0.7916667F;
			// 
			// txtCounty
			// 
			this.txtCounty.Height = 0.1875F;
			this.txtCounty.Left = 4.083333F;
			this.txtCounty.Name = "txtCounty";
			this.txtCounty.Style = "font-family: \'Times New Roman\'";
			this.txtCounty.Text = null;
			this.txtCounty.Top = 0.3854167F;
			this.txtCounty.Width = 2.125F;
			// 
			// Label237
			// 
			this.Label237.Height = 0.1875F;
			this.Label237.HyperLink = null;
			this.Label237.Left = 0.5104167F;
			this.Label237.Name = "Label237";
			this.Label237.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label237.Text = "April 1,";
			this.Label237.Top = 1.083333F;
			this.Label237.Width = 0.5F;
			// 
			// Label238
			// 
			this.Label238.Height = 0.1875F;
			this.Label238.HyperLink = null;
			this.Label238.Left = 1.260417F;
			this.Label238.Name = "Label238";
			this.Label238.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label238.Text = "giving the approximate increase or decrease in full market value.";
			this.Label238.Top = 1.083333F;
			this.Label238.Width = 5F;
			// 
			// lblYear1
			// 
			this.lblYear1.Height = 0.1875F;
			this.lblYear1.HyperLink = null;
			this.lblYear1.Left = 0.9270833F;
			this.lblYear1.Name = "lblYear1";
			this.lblYear1.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.lblYear1.Text = null;
			this.lblYear1.Top = 1.083333F;
			this.lblYear1.Width = 0.3333333F;
			// 
			// Label239
			// 
			this.Label239.Height = 0.1875F;
			this.Label239.HyperLink = null;
			this.Label239.Left = 0.375F;
			this.Label239.Name = "Label239";
			this.Label239.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label239.Text = "2. Enter any new industrial or commercial growth started or expanded since April " +
    "1,";
			this.Label239.Top = 3.166667F;
			this.Label239.Width = 4.25F;
			// 
			// lblYear2
			// 
			this.lblYear2.Height = 0.1875F;
			this.lblYear2.HyperLink = null;
			this.lblYear2.Left = 4.625F;
			this.lblYear2.Name = "lblYear2";
			this.lblYear2.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.lblYear2.Text = "2004";
			this.lblYear2.Top = 3.166667F;
			this.lblYear2.Width = 0.3333333F;
			// 
			// Label241
			// 
			this.Label241.Height = 0.1875F;
			this.Label241.HyperLink = null;
			this.Label241.Left = 4.958333F;
			this.Label241.Name = "Label241";
			this.Label241.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label241.Text = "giving the approximate";
			this.Label241.Top = 3.166667F;
			this.Label241.Width = 1.75F;
			// 
			// Label242
			// 
			this.Label242.Height = 0.1875F;
			this.Label242.HyperLink = null;
			this.Label242.Left = 0.5104167F;
			this.Label242.Name = "Label242";
			this.Label242.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label242.Text = "full market value and additional machinery, equipment, etc.";
			this.Label242.Top = 3.354167F;
			this.Label242.Width = 4.25F;
			// 
			// Label243
			// 
			this.Label243.Height = 0.1875F;
			this.Label243.HyperLink = null;
			this.Label243.Left = 0.375F;
			this.Label243.Name = "Label243";
			this.Label243.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label243.Text = "3. Enter any extreme losses in valuation since April 1,";
			this.Label243.Top = 5.125F;
			this.Label243.Width = 2.744F;
			// 
			// lblYear3
			// 
			this.lblYear3.Height = 0.1875F;
			this.lblYear3.HyperLink = null;
			this.lblYear3.Left = 3.119F;
			this.lblYear3.Name = "lblYear3";
			this.lblYear3.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.lblYear3.Text = "2004";
			this.lblYear3.Top = 5.125F;
			this.lblYear3.Width = 0.3333333F;
			// 
			// Label245
			// 
			this.Label245.Height = 0.1875F;
			this.Label245.HyperLink = null;
			this.Label245.Left = 3.452333F;
			this.Label245.Name = "Label245";
			this.Label245.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label245.Text = "giving a brief explanation such as";
			this.Label245.Top = 5.125F;
			this.Label245.Width = 3.166667F;
			// 
			// Label246
			// 
			this.Label246.Height = 0.1875F;
			this.Label246.HyperLink = null;
			this.Label246.Left = 0.5104167F;
			this.Label246.Name = "Label246";
			this.Label246.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label246.Text = "\"fire\" or \"mill closing\", etc. giving the loss at full market value.";
			this.Label246.Top = 5.3125F;
			this.Label246.Width = 4.25F;
			// 
			// Label247
			// 
			this.Label247.Height = 0.1875F;
			this.Label247.HyperLink = null;
			this.Label247.Left = 0.375F;
			this.Label247.Name = "Label247";
			this.Label247.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label247.Text = "4. Explain any general increase or decrease in valuation since April 1,";
			this.Label247.Top = 7.083333F;
			this.Label247.Width = 3.583333F;
			// 
			// lblYear4
			// 
			this.lblYear4.Height = 0.1875F;
			this.lblYear4.HyperLink = null;
			this.lblYear4.Left = 3.958333F;
			this.lblYear4.Name = "lblYear4";
			this.lblYear4.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.lblYear4.Text = "2004";
			this.lblYear4.Top = 7.083333F;
			this.lblYear4.Width = 0.3333333F;
			// 
			// Label248
			// 
			this.Label248.Height = 0.1875F;
			this.Label248.HyperLink = null;
			this.Label248.Left = 4.291667F;
			this.Label248.Name = "Label248";
			this.Label248.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label248.Text = "such as revaluations, change in ratio";
			this.Label248.Top = 7.083333F;
			this.Label248.Width = 2.5F;
			// 
			// Label249
			// 
			this.Label249.Height = 0.1875F;
			this.Label249.HyperLink = null;
			this.Label249.Left = 0.5104167F;
			this.Label249.Name = "Label249";
			this.Label249.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label249.Text = "used, adjustments, etc.";
			this.Label249.Top = 7.270833F;
			this.Label249.Width = 4.25F;
			// 
			// Label250
			// 
			this.Label250.Height = 0.21875F;
			this.Label250.HyperLink = null;
			this.Label250.Left = 1.604167F;
			this.Label250.Name = "Label250";
			this.Label250.Style = "text-align: center";
			this.Label250.Text = "One Family";
			this.Label250.Top = 1.302083F;
			this.Label250.Width = 0.78125F;
			// 
			// Label251
			// 
			this.Label251.Height = 0.21875F;
			this.Label251.HyperLink = null;
			this.Label251.Left = 2.4375F;
			this.Label251.Name = "Label251";
			this.Label251.Style = "text-align: center";
			this.Label251.Text = "Two Family";
			this.Label251.Top = 1.302083F;
			this.Label251.Width = 0.78125F;
			// 
			// Label252
			// 
			this.Label252.Height = 0.21875F;
			this.Label252.HyperLink = null;
			this.Label252.Left = 3.270833F;
			this.Label252.Name = "Label252";
			this.Label252.Style = "text-align: center";
			this.Label252.Text = "3-4 Family";
			this.Label252.Top = 1.302083F;
			this.Label252.Width = 0.78125F;
			// 
			// Label253
			// 
			this.Label253.Height = 0.21875F;
			this.Label253.HyperLink = null;
			this.Label253.Left = 4.020833F;
			this.Label253.Name = "Label253";
			this.Label253.Style = "text-align: center";
			this.Label253.Text = "5 Family Plus";
			this.Label253.Top = 1.302083F;
			this.Label253.Width = 1.03125F;
			// 
			// Label254
			// 
			this.Label254.Height = 0.21875F;
			this.Label254.HyperLink = null;
			this.Label254.Left = 5.020833F;
			this.Label254.Name = "Label254";
			this.Label254.Style = "text-align: center";
			this.Label254.Text = "Mobile Homes";
			this.Label254.Top = 1.302083F;
			this.Label254.Width = 0.9479167F;
			// 
			// Label255
			// 
			this.Label255.Height = 0.21875F;
			this.Label255.HyperLink = null;
			this.Label255.Left = 5.9375F;
			this.Label255.Name = "Label255";
			this.Label255.Style = "text-align: center";
			this.Label255.Text = "Seasonal Homes";
			this.Label255.Top = 1.302083F;
			this.Label255.Width = 1.03125F;
			// 
			// Label256
			// 
			this.Label256.Height = 0.21875F;
			this.Label256.HyperLink = null;
			this.Label256.Left = 0.1666667F;
			this.Label256.Name = "Label256";
			this.Label256.Style = "text-align: left";
			this.Label256.Text = "New";
			this.Label256.Top = 1.520833F;
			this.Label256.Width = 1.03125F;
			// 
			// Label257
			// 
			this.Label257.Height = 0.21875F;
			this.Label257.HyperLink = null;
			this.Label257.Left = 0.1666667F;
			this.Label257.Name = "Label257";
			this.Label257.Style = "text-align: left";
			this.Label257.Text = "Demolished";
			this.Label257.Top = 1.739583F;
			this.Label257.Width = 1.03125F;
			// 
			// Label258
			// 
			this.Label258.Height = 0.21875F;
			this.Label258.HyperLink = null;
			this.Label258.Left = 0.1666667F;
			this.Label258.Name = "Label258";
			this.Label258.Style = "text-align: left";
			this.Label258.Text = "Converted";
			this.Label258.Top = 1.958333F;
			this.Label258.Width = 1.03125F;
			// 
			// Label260
			// 
			this.Label260.Height = 0.21875F;
			this.Label260.HyperLink = null;
			this.Label260.Left = 0.1666667F;
			this.Label260.Name = "Label260";
			this.Label260.Style = "font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Label260.Text = "Valuation Increase (+)";
			this.Label260.Top = 2.270833F;
			this.Label260.Width = 1.40625F;
			// 
			// Label261
			// 
			this.Label261.Height = 0.21875F;
			this.Label261.HyperLink = null;
			this.Label261.Left = 0.1666667F;
			this.Label261.Name = "Label261";
			this.Label261.Style = "font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Label261.Text = "Valuation Loss (-)";
			this.Label261.Top = 2.489583F;
			this.Label261.Width = 1.28125F;
			// 
			// Label262
			// 
			this.Label262.Height = 0.21875F;
			this.Label262.HyperLink = null;
			this.Label262.Left = 0.1666667F;
			this.Label262.Name = "Label262";
			this.Label262.Style = "text-align: left";
			this.Label262.Text = "Net increase/loss";
			this.Label262.Top = 2.833333F;
			this.Label262.Width = 1.03125F;
			// 
			// txtOneFamilyNew
			// 
			this.txtOneFamilyNew.Height = 0.21875F;
			this.txtOneFamilyNew.Left = 1.583333F;
			this.txtOneFamilyNew.Name = "txtOneFamilyNew";
			this.txtOneFamilyNew.Style = "text-align: right";
			this.txtOneFamilyNew.Text = "Field1";
			this.txtOneFamilyNew.Top = 1.520833F;
			this.txtOneFamilyNew.Width = 0.78125F;
			// 
			// txtOneFamilyDemolished
			// 
			this.txtOneFamilyDemolished.Height = 0.21875F;
			this.txtOneFamilyDemolished.Left = 1.583333F;
			this.txtOneFamilyDemolished.Name = "txtOneFamilyDemolished";
			this.txtOneFamilyDemolished.Style = "text-align: right";
			this.txtOneFamilyDemolished.Text = "Field2";
			this.txtOneFamilyDemolished.Top = 1.739583F;
			this.txtOneFamilyDemolished.Width = 0.78125F;
			// 
			// txtOneFamilyConverted
			// 
			this.txtOneFamilyConverted.Height = 0.21875F;
			this.txtOneFamilyConverted.Left = 1.583333F;
			this.txtOneFamilyConverted.Name = "txtOneFamilyConverted";
			this.txtOneFamilyConverted.Style = "text-align: right";
			this.txtOneFamilyConverted.Text = "Field3";
			this.txtOneFamilyConverted.Top = 1.958333F;
			this.txtOneFamilyConverted.Width = 0.78125F;
			// 
			// txtOneFamilyIncrease
			// 
			this.txtOneFamilyIncrease.Height = 0.21875F;
			this.txtOneFamilyIncrease.Left = 1.583333F;
			this.txtOneFamilyIncrease.Name = "txtOneFamilyIncrease";
			this.txtOneFamilyIncrease.Style = "text-align: right";
			this.txtOneFamilyIncrease.Text = "Field4";
			this.txtOneFamilyIncrease.Top = 2.270833F;
			this.txtOneFamilyIncrease.Width = 0.78125F;
			// 
			// txtOneFamilyLoss
			// 
			this.txtOneFamilyLoss.Height = 0.21875F;
			this.txtOneFamilyLoss.Left = 1.583333F;
			this.txtOneFamilyLoss.Name = "txtOneFamilyLoss";
			this.txtOneFamilyLoss.Style = "text-align: right";
			this.txtOneFamilyLoss.Text = "Field5";
			this.txtOneFamilyLoss.Top = 2.489583F;
			this.txtOneFamilyLoss.Width = 0.78125F;
			// 
			// txtTwoFamilyNew
			// 
			this.txtTwoFamilyNew.Height = 0.21875F;
			this.txtTwoFamilyNew.Left = 2.416667F;
			this.txtTwoFamilyNew.Name = "txtTwoFamilyNew";
			this.txtTwoFamilyNew.Style = "text-align: right";
			this.txtTwoFamilyNew.Text = "Field6";
			this.txtTwoFamilyNew.Top = 1.520833F;
			this.txtTwoFamilyNew.Width = 0.78125F;
			// 
			// txtTwoFamilyDemolished
			// 
			this.txtTwoFamilyDemolished.Height = 0.21875F;
			this.txtTwoFamilyDemolished.Left = 2.416667F;
			this.txtTwoFamilyDemolished.Name = "txtTwoFamilyDemolished";
			this.txtTwoFamilyDemolished.Style = "text-align: right";
			this.txtTwoFamilyDemolished.Text = "Field7";
			this.txtTwoFamilyDemolished.Top = 1.739583F;
			this.txtTwoFamilyDemolished.Width = 0.78125F;
			// 
			// txtTwoFamilyConverted
			// 
			this.txtTwoFamilyConverted.Height = 0.21875F;
			this.txtTwoFamilyConverted.Left = 2.416667F;
			this.txtTwoFamilyConverted.Name = "txtTwoFamilyConverted";
			this.txtTwoFamilyConverted.Style = "text-align: right";
			this.txtTwoFamilyConverted.Text = "Field8";
			this.txtTwoFamilyConverted.Top = 1.958333F;
			this.txtTwoFamilyConverted.Width = 0.78125F;
			// 
			// txtTwoFamilyIncrease
			// 
			this.txtTwoFamilyIncrease.Height = 0.21875F;
			this.txtTwoFamilyIncrease.Left = 2.416667F;
			this.txtTwoFamilyIncrease.Name = "txtTwoFamilyIncrease";
			this.txtTwoFamilyIncrease.Style = "text-align: right";
			this.txtTwoFamilyIncrease.Text = "Field9";
			this.txtTwoFamilyIncrease.Top = 2.270833F;
			this.txtTwoFamilyIncrease.Width = 0.78125F;
			// 
			// txtTwoFamilyLoss
			// 
			this.txtTwoFamilyLoss.Height = 0.21875F;
			this.txtTwoFamilyLoss.Left = 2.416667F;
			this.txtTwoFamilyLoss.Name = "txtTwoFamilyLoss";
			this.txtTwoFamilyLoss.Style = "text-align: right";
			this.txtTwoFamilyLoss.Text = "Field10";
			this.txtTwoFamilyLoss.Top = 2.489583F;
			this.txtTwoFamilyLoss.Width = 0.78125F;
			// 
			// txtThreeFourFamilyNew
			// 
			this.txtThreeFourFamilyNew.Height = 0.21875F;
			this.txtThreeFourFamilyNew.Left = 3.25F;
			this.txtThreeFourFamilyNew.Name = "txtThreeFourFamilyNew";
			this.txtThreeFourFamilyNew.Style = "text-align: right";
			this.txtThreeFourFamilyNew.Text = "Field11";
			this.txtThreeFourFamilyNew.Top = 1.520833F;
			this.txtThreeFourFamilyNew.Width = 0.78125F;
			// 
			// txtThreeFourFamilyDemolished
			// 
			this.txtThreeFourFamilyDemolished.Height = 0.21875F;
			this.txtThreeFourFamilyDemolished.Left = 3.25F;
			this.txtThreeFourFamilyDemolished.Name = "txtThreeFourFamilyDemolished";
			this.txtThreeFourFamilyDemolished.Style = "text-align: right";
			this.txtThreeFourFamilyDemolished.Text = "Field12";
			this.txtThreeFourFamilyDemolished.Top = 1.739583F;
			this.txtThreeFourFamilyDemolished.Width = 0.78125F;
			// 
			// txtThreeFourFamilyConverted
			// 
			this.txtThreeFourFamilyConverted.Height = 0.21875F;
			this.txtThreeFourFamilyConverted.Left = 3.25F;
			this.txtThreeFourFamilyConverted.Name = "txtThreeFourFamilyConverted";
			this.txtThreeFourFamilyConverted.Style = "text-align: right";
			this.txtThreeFourFamilyConverted.Text = "Field13";
			this.txtThreeFourFamilyConverted.Top = 1.958333F;
			this.txtThreeFourFamilyConverted.Width = 0.78125F;
			// 
			// txtThreeFourFamilyIncrease
			// 
			this.txtThreeFourFamilyIncrease.Height = 0.21875F;
			this.txtThreeFourFamilyIncrease.Left = 3.25F;
			this.txtThreeFourFamilyIncrease.Name = "txtThreeFourFamilyIncrease";
			this.txtThreeFourFamilyIncrease.Style = "text-align: right";
			this.txtThreeFourFamilyIncrease.Text = "Field14";
			this.txtThreeFourFamilyIncrease.Top = 2.270833F;
			this.txtThreeFourFamilyIncrease.Width = 0.78125F;
			// 
			// txtThreeFourFamilyLoss
			// 
			this.txtThreeFourFamilyLoss.Height = 0.21875F;
			this.txtThreeFourFamilyLoss.Left = 3.25F;
			this.txtThreeFourFamilyLoss.Name = "txtThreeFourFamilyLoss";
			this.txtThreeFourFamilyLoss.Style = "text-align: right";
			this.txtThreeFourFamilyLoss.Text = "Field15";
			this.txtThreeFourFamilyLoss.Top = 2.489583F;
			this.txtThreeFourFamilyLoss.Width = 0.78125F;
			// 
			// txtOverFourNew
			// 
			this.txtOverFourNew.Height = 0.21875F;
			this.txtOverFourNew.Left = 4.25F;
			this.txtOverFourNew.Name = "txtOverFourNew";
			this.txtOverFourNew.Style = "text-align: right";
			this.txtOverFourNew.Text = "Field16";
			this.txtOverFourNew.Top = 1.520833F;
			this.txtOverFourNew.Width = 0.78125F;
			// 
			// txtOverFourDemolished
			// 
			this.txtOverFourDemolished.Height = 0.21875F;
			this.txtOverFourDemolished.Left = 4.25F;
			this.txtOverFourDemolished.Name = "txtOverFourDemolished";
			this.txtOverFourDemolished.Style = "text-align: right";
			this.txtOverFourDemolished.Text = "Field17";
			this.txtOverFourDemolished.Top = 1.739583F;
			this.txtOverFourDemolished.Width = 0.78125F;
			// 
			// txtOverFourConverted
			// 
			this.txtOverFourConverted.Height = 0.21875F;
			this.txtOverFourConverted.Left = 4.25F;
			this.txtOverFourConverted.Name = "txtOverFourConverted";
			this.txtOverFourConverted.Style = "text-align: right";
			this.txtOverFourConverted.Text = "Field18";
			this.txtOverFourConverted.Top = 1.958333F;
			this.txtOverFourConverted.Width = 0.78125F;
			// 
			// txtOverFourIncrease
			// 
			this.txtOverFourIncrease.Height = 0.21875F;
			this.txtOverFourIncrease.Left = 4.25F;
			this.txtOverFourIncrease.Name = "txtOverFourIncrease";
			this.txtOverFourIncrease.Style = "text-align: right";
			this.txtOverFourIncrease.Text = "Field19";
			this.txtOverFourIncrease.Top = 2.270833F;
			this.txtOverFourIncrease.Width = 0.78125F;
			// 
			// txtOverFourLoss
			// 
			this.txtOverFourLoss.Height = 0.21875F;
			this.txtOverFourLoss.Left = 4.25F;
			this.txtOverFourLoss.Name = "txtOverFourLoss";
			this.txtOverFourLoss.Style = "text-align: right";
			this.txtOverFourLoss.Text = "Field20";
			this.txtOverFourLoss.Top = 2.489583F;
			this.txtOverFourLoss.Width = 0.78125F;
			// 
			// txtMHNew
			// 
			this.txtMHNew.Height = 0.21875F;
			this.txtMHNew.Left = 5.166667F;
			this.txtMHNew.Name = "txtMHNew";
			this.txtMHNew.Style = "text-align: right";
			this.txtMHNew.Text = "Field21";
			this.txtMHNew.Top = 1.520833F;
			this.txtMHNew.Width = 0.78125F;
			// 
			// txtMHDemolished
			// 
			this.txtMHDemolished.Height = 0.21875F;
			this.txtMHDemolished.Left = 5.166667F;
			this.txtMHDemolished.Name = "txtMHDemolished";
			this.txtMHDemolished.Style = "text-align: right";
			this.txtMHDemolished.Text = "Field22";
			this.txtMHDemolished.Top = 1.739583F;
			this.txtMHDemolished.Width = 0.78125F;
			// 
			// txtMHConverted
			// 
			this.txtMHConverted.Height = 0.21875F;
			this.txtMHConverted.Left = 5.166667F;
			this.txtMHConverted.Name = "txtMHConverted";
			this.txtMHConverted.Style = "text-align: right";
			this.txtMHConverted.Text = "Field23";
			this.txtMHConverted.Top = 1.958333F;
			this.txtMHConverted.Width = 0.78125F;
			// 
			// txtMHIncrease
			// 
			this.txtMHIncrease.Height = 0.21875F;
			this.txtMHIncrease.Left = 5.166667F;
			this.txtMHIncrease.Name = "txtMHIncrease";
			this.txtMHIncrease.Style = "text-align: right";
			this.txtMHIncrease.Text = "Field24";
			this.txtMHIncrease.Top = 2.270833F;
			this.txtMHIncrease.Width = 0.78125F;
			// 
			// txtMHLoss
			// 
			this.txtMHLoss.Height = 0.21875F;
			this.txtMHLoss.Left = 5.166667F;
			this.txtMHLoss.Name = "txtMHLoss";
			this.txtMHLoss.Style = "text-align: right";
			this.txtMHLoss.Text = "Field25";
			this.txtMHLoss.Top = 2.489583F;
			this.txtMHLoss.Width = 0.78125F;
			// 
			// txtSHNew
			// 
			this.txtSHNew.Height = 0.21875F;
			this.txtSHNew.Left = 6.166667F;
			this.txtSHNew.Name = "txtSHNew";
			this.txtSHNew.Style = "text-align: right";
			this.txtSHNew.Text = "Field26";
			this.txtSHNew.Top = 1.520833F;
			this.txtSHNew.Width = 0.78125F;
			// 
			// txtSHDemolished
			// 
			this.txtSHDemolished.Height = 0.21875F;
			this.txtSHDemolished.Left = 6.166667F;
			this.txtSHDemolished.Name = "txtSHDemolished";
			this.txtSHDemolished.Style = "text-align: right";
			this.txtSHDemolished.Text = "Field27";
			this.txtSHDemolished.Top = 1.739583F;
			this.txtSHDemolished.Width = 0.78125F;
			// 
			// txtSHConverted
			// 
			this.txtSHConverted.Height = 0.21875F;
			this.txtSHConverted.Left = 6.166667F;
			this.txtSHConverted.Name = "txtSHConverted";
			this.txtSHConverted.Style = "text-align: right";
			this.txtSHConverted.Text = "Field28";
			this.txtSHConverted.Top = 1.958333F;
			this.txtSHConverted.Width = 0.78125F;
			// 
			// txtSHIncrease
			// 
			this.txtSHIncrease.Height = 0.21875F;
			this.txtSHIncrease.Left = 6.166667F;
			this.txtSHIncrease.Name = "txtSHIncrease";
			this.txtSHIncrease.Style = "text-align: right";
			this.txtSHIncrease.Text = "Field29";
			this.txtSHIncrease.Top = 2.270833F;
			this.txtSHIncrease.Width = 0.78125F;
			// 
			// txtSHLoss
			// 
			this.txtSHLoss.Height = 0.21875F;
			this.txtSHLoss.Left = 6.166667F;
			this.txtSHLoss.Name = "txtSHLoss";
			this.txtSHLoss.Style = "text-align: right";
			this.txtSHLoss.Text = "Field30";
			this.txtSHLoss.Top = 2.489583F;
			this.txtSHLoss.Width = 0.78125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.1458333F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.520833F;
			this.Line1.Width = 6.833333F;
			this.Line1.X1 = 0.1458333F;
			this.Line1.X2 = 6.979167F;
			this.Line1.Y1 = 1.520833F;
			this.Line1.Y2 = 1.520833F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.1458333F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.729167F;
			this.Line2.Width = 6.833333F;
			this.Line2.X1 = 0.1458333F;
			this.Line2.X2 = 6.979167F;
			this.Line2.Y1 = 1.729167F;
			this.Line2.Y2 = 1.729167F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.15625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.958333F;
			this.Line3.Width = 6.833333F;
			this.Line3.X1 = 0.15625F;
			this.Line3.X2 = 6.989583F;
			this.Line3.Y1 = 1.958333F;
			this.Line3.Y2 = 1.958333F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.15625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.166667F;
			this.Line4.Width = 6.833333F;
			this.Line4.X1 = 0.15625F;
			this.Line4.X2 = 6.989583F;
			this.Line4.Y1 = 2.166667F;
			this.Line4.Y2 = 2.166667F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.1666667F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.270833F;
			this.Line5.Width = 6.822917F;
			this.Line5.X1 = 0.1666667F;
			this.Line5.X2 = 6.989583F;
			this.Line5.Y1 = 2.270833F;
			this.Line5.Y2 = 2.270833F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0.1666667F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 2.479167F;
			this.Line6.Width = 6.822917F;
			this.Line6.X1 = 0.1666667F;
			this.Line6.X2 = 6.989583F;
			this.Line6.Y1 = 2.479167F;
			this.Line6.Y2 = 2.479167F;
			// 
			// Line9
			// 
			this.Line9.Height = 1.770833F;
			this.Line9.Left = 3.239583F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 1.291667F;
			this.Line9.Width = 0F;
			this.Line9.X1 = 3.239583F;
			this.Line9.X2 = 3.239583F;
			this.Line9.Y1 = 1.291667F;
			this.Line9.Y2 = 3.0625F;
			// 
			// Line10
			// 
			this.Line10.Height = 1.770833F;
			this.Line10.Left = 4.072917F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 1.291667F;
			this.Line10.Width = 0F;
			this.Line10.X1 = 4.072917F;
			this.Line10.X2 = 4.072917F;
			this.Line10.Y1 = 1.291667F;
			this.Line10.Y2 = 3.0625F;
			// 
			// Line11
			// 
			this.Line11.Height = 1.770833F;
			this.Line11.Left = 5.041667F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 1.291667F;
			this.Line11.Width = 0F;
			this.Line11.X1 = 5.041667F;
			this.Line11.X2 = 5.041667F;
			this.Line11.Y1 = 1.291667F;
			this.Line11.Y2 = 3.0625F;
			// 
			// Line12
			// 
			this.Line12.Height = 1.770833F;
			this.Line12.Left = 5.958333F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 1.291667F;
			this.Line12.Width = 0F;
			this.Line12.X1 = 5.958333F;
			this.Line12.X2 = 5.958333F;
			this.Line12.Y1 = 1.291667F;
			this.Line12.Y2 = 3.0625F;
			// 
			// txtOneFamilyNet
			// 
			this.txtOneFamilyNet.Height = 0.21875F;
			this.txtOneFamilyNet.Left = 1.583333F;
			this.txtOneFamilyNet.Name = "txtOneFamilyNet";
			this.txtOneFamilyNet.Style = "text-align: right";
			this.txtOneFamilyNet.Text = "Field5";
			this.txtOneFamilyNet.Top = 2.833333F;
			this.txtOneFamilyNet.Width = 0.78125F;
			// 
			// txtTwoFamilyNet
			// 
			this.txtTwoFamilyNet.Height = 0.21875F;
			this.txtTwoFamilyNet.Left = 2.416667F;
			this.txtTwoFamilyNet.Name = "txtTwoFamilyNet";
			this.txtTwoFamilyNet.Style = "text-align: right";
			this.txtTwoFamilyNet.Text = "Field10";
			this.txtTwoFamilyNet.Top = 2.833333F;
			this.txtTwoFamilyNet.Width = 0.78125F;
			// 
			// txtThreeFourFamilyNet
			// 
			this.txtThreeFourFamilyNet.Height = 0.21875F;
			this.txtThreeFourFamilyNet.Left = 3.25F;
			this.txtThreeFourFamilyNet.Name = "txtThreeFourFamilyNet";
			this.txtThreeFourFamilyNet.Style = "text-align: right";
			this.txtThreeFourFamilyNet.Text = "Field15";
			this.txtThreeFourFamilyNet.Top = 2.833333F;
			this.txtThreeFourFamilyNet.Width = 0.78125F;
			// 
			// txtOverFourNet
			// 
			this.txtOverFourNet.Height = 0.21875F;
			this.txtOverFourNet.Left = 4.25F;
			this.txtOverFourNet.Name = "txtOverFourNet";
			this.txtOverFourNet.Style = "text-align: right";
			this.txtOverFourNet.Text = "Field20";
			this.txtOverFourNet.Top = 2.833333F;
			this.txtOverFourNet.Width = 0.78125F;
			// 
			// txtMHNet
			// 
			this.txtMHNet.Height = 0.21875F;
			this.txtMHNet.Left = 5.166667F;
			this.txtMHNet.Name = "txtMHNet";
			this.txtMHNet.Style = "text-align: right";
			this.txtMHNet.Text = "Field25";
			this.txtMHNet.Top = 2.833333F;
			this.txtMHNet.Width = 0.78125F;
			// 
			// txtSHNet
			// 
			this.txtSHNet.Height = 0.21875F;
			this.txtSHNet.Left = 6.166667F;
			this.txtSHNet.Name = "txtSHNet";
			this.txtSHNet.Style = "text-align: right";
			this.txtSHNet.Text = "Field30";
			this.txtSHNet.Top = 2.833333F;
			this.txtSHNet.Width = 0.78125F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0.1666667F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 2.833333F;
			this.Line13.Width = 6.822917F;
			this.Line13.X1 = 0.1666667F;
			this.Line13.X2 = 6.989583F;
			this.Line13.Y1 = 2.833333F;
			this.Line13.Y2 = 2.833333F;
			// 
			// Line8
			// 
			this.Line8.Height = 1.770833F;
			this.Line8.Left = 2.416667F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.291667F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 2.416667F;
			this.Line8.X2 = 2.416667F;
			this.Line8.Y1 = 1.291667F;
			this.Line8.Y2 = 3.0625F;
			// 
			// Line7
			// 
			this.Line7.Height = 1.770833F;
			this.Line7.Left = 1.583333F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 1.291667F;
			this.Line7.Width = 0F;
			this.Line7.X1 = 1.583333F;
			this.Line7.X2 = 1.583333F;
			this.Line7.Y1 = 1.291667F;
			this.Line7.Y2 = 3.0625F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 0.1666667F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 2.708333F;
			this.Line14.Width = 6.822917F;
			this.Line14.X1 = 0.1666667F;
			this.Line14.X2 = 6.989583F;
			this.Line14.Y1 = 2.708333F;
			this.Line14.Y2 = 2.708333F;
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(128, 128, 128);
			this.Shape1.Height = 0.1111111F;
			this.Shape1.Left = 0.15625F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 2.166667F;
			this.Shape1.Width = 6.833333F;
			// 
			// Shape2
			// 
			this.Shape2.BackColor = System.Drawing.Color.FromArgb(128, 128, 128);
			this.Shape2.Height = 0.1319444F;
			this.Shape2.Left = 0.15625F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 2.708333F;
			this.Shape2.Width = 6.833333F;
			// 
			// Shape47
			// 
			this.Shape47.Height = 0.1875F;
			this.Shape47.Left = 0.4583333F;
			this.Shape47.Name = "Shape47";
			this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape47.Top = 3.5625F;
			this.Shape47.Width = 6.541667F;
			// 
			// Shape48
			// 
			this.Shape48.Height = 0.1875F;
			this.Shape48.Left = 0.4583333F;
			this.Shape48.Name = "Shape48";
			this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape48.Top = 3.75F;
			this.Shape48.Width = 6.541667F;
			// 
			// Shape49
			// 
			this.Shape49.Height = 0.1875F;
			this.Shape49.Left = 0.4583333F;
			this.Shape49.Name = "Shape49";
			this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape49.Top = 3.9375F;
			this.Shape49.Width = 6.541667F;
			// 
			// txt2c
			// 
			this.txt2c.Height = 0.1875F;
			this.txt2c.Left = 0.5208333F;
			this.txt2c.Name = "txt2c";
			this.txt2c.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2c.Text = " ";
			this.txt2c.Top = 3.9375F;
			this.txt2c.Width = 6.416667F;
			// 
			// Shape50
			// 
			this.Shape50.Height = 0.1875F;
			this.Shape50.Left = 0.4583333F;
			this.Shape50.Name = "Shape50";
			this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape50.Top = 4.125F;
			this.Shape50.Width = 6.541667F;
			// 
			// txt2d
			// 
			this.txt2d.Height = 0.1875F;
			this.txt2d.Left = 0.5208333F;
			this.txt2d.Name = "txt2d";
			this.txt2d.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2d.Text = " ";
			this.txt2d.Top = 4.125F;
			this.txt2d.Width = 6.416667F;
			// 
			// Shape51
			// 
			this.Shape51.Height = 0.1875F;
			this.Shape51.Left = 0.4583333F;
			this.Shape51.Name = "Shape51";
			this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape51.Top = 4.3125F;
			this.Shape51.Width = 6.541667F;
			// 
			// txt2e
			// 
			this.txt2e.Height = 0.1875F;
			this.txt2e.Left = 0.5208333F;
			this.txt2e.Name = "txt2e";
			this.txt2e.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2e.Text = " ";
			this.txt2e.Top = 4.3125F;
			this.txt2e.Width = 6.416667F;
			// 
			// Shape52
			// 
			this.Shape52.Height = 0.1875F;
			this.Shape52.Left = 0.4583333F;
			this.Shape52.Name = "Shape52";
			this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape52.Top = 4.5F;
			this.Shape52.Width = 6.541667F;
			// 
			// txt2f
			// 
			this.txt2f.Height = 0.1875F;
			this.txt2f.Left = 0.5208333F;
			this.txt2f.Name = "txt2f";
			this.txt2f.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2f.Text = " ";
			this.txt2f.Top = 4.5F;
			this.txt2f.Width = 6.416667F;
			// 
			// Shape53
			// 
			this.Shape53.Height = 0.1875F;
			this.Shape53.Left = 0.4583333F;
			this.Shape53.Name = "Shape53";
			this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape53.Top = 4.6875F;
			this.Shape53.Width = 6.541667F;
			// 
			// txt2g
			// 
			this.txt2g.Height = 0.1875F;
			this.txt2g.Left = 0.5208333F;
			this.txt2g.Name = "txt2g";
			this.txt2g.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2g.Text = " ";
			this.txt2g.Top = 4.6875F;
			this.txt2g.Width = 6.416667F;
			// 
			// Shape54
			// 
			this.Shape54.Height = 0.1875F;
			this.Shape54.Left = 0.4583333F;
			this.Shape54.Name = "Shape54";
			this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape54.Top = 4.875F;
			this.Shape54.Width = 6.541667F;
			// 
			// txt2h
			// 
			this.txt2h.Height = 0.1875F;
			this.txt2h.Left = 0.5208333F;
			this.txt2h.Name = "txt2h";
			this.txt2h.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt2h.Text = " ";
			this.txt2h.Top = 4.875F;
			this.txt2h.Width = 6.416667F;
			// 
			// Shape55
			// 
			this.Shape55.Height = 0.1875F;
			this.Shape55.Left = 0.4583333F;
			this.Shape55.Name = "Shape55";
			this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape55.Top = 5.5F;
			this.Shape55.Width = 6.541667F;
			// 
			// txt3a
			// 
			this.txt3a.Height = 0.1875F;
			this.txt3a.Left = 0.5208333F;
			this.txt3a.Name = "txt3a";
			this.txt3a.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3a.Text = " ";
			this.txt3a.Top = 5.5F;
			this.txt3a.Width = 6.416667F;
			// 
			// Shape56
			// 
			this.Shape56.Height = 0.1875F;
			this.Shape56.Left = 0.4583333F;
			this.Shape56.Name = "Shape56";
			this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape56.Top = 5.6875F;
			this.Shape56.Width = 6.541667F;
			// 
			// txt3b
			// 
			this.txt3b.Height = 0.1875F;
			this.txt3b.Left = 0.5208333F;
			this.txt3b.Name = "txt3b";
			this.txt3b.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3b.Text = " ";
			this.txt3b.Top = 5.6875F;
			this.txt3b.Width = 6.416667F;
			// 
			// Shape57
			// 
			this.Shape57.Height = 0.1875F;
			this.Shape57.Left = 0.4583333F;
			this.Shape57.Name = "Shape57";
			this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape57.Top = 5.875F;
			this.Shape57.Width = 6.541667F;
			// 
			// txt3c
			// 
			this.txt3c.Height = 0.1875F;
			this.txt3c.Left = 0.5208333F;
			this.txt3c.Name = "txt3c";
			this.txt3c.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3c.Text = " ";
			this.txt3c.Top = 5.875F;
			this.txt3c.Width = 6.416667F;
			// 
			// Shape58
			// 
			this.Shape58.Height = 0.1875F;
			this.Shape58.Left = 0.4583333F;
			this.Shape58.Name = "Shape58";
			this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape58.Top = 6.0625F;
			this.Shape58.Width = 6.541667F;
			// 
			// txt3d
			// 
			this.txt3d.Height = 0.1875F;
			this.txt3d.Left = 0.5208333F;
			this.txt3d.Name = "txt3d";
			this.txt3d.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3d.Text = " ";
			this.txt3d.Top = 6.0625F;
			this.txt3d.Width = 6.416667F;
			// 
			// Shape59
			// 
			this.Shape59.Height = 0.1875F;
			this.Shape59.Left = 0.4583333F;
			this.Shape59.Name = "Shape59";
			this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape59.Top = 6.25F;
			this.Shape59.Width = 6.541667F;
			// 
			// txt3e
			// 
			this.txt3e.Height = 0.1875F;
			this.txt3e.Left = 0.5208333F;
			this.txt3e.Name = "txt3e";
			this.txt3e.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3e.Text = " ";
			this.txt3e.Top = 6.25F;
			this.txt3e.Width = 6.416667F;
			// 
			// Shape60
			// 
			this.Shape60.Height = 0.1875F;
			this.Shape60.Left = 0.4583333F;
			this.Shape60.Name = "Shape60";
			this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape60.Top = 6.4375F;
			this.Shape60.Width = 6.541667F;
			// 
			// txt3f
			// 
			this.txt3f.Height = 0.1875F;
			this.txt3f.Left = 0.5208333F;
			this.txt3f.Name = "txt3f";
			this.txt3f.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3f.Text = " ";
			this.txt3f.Top = 6.4375F;
			this.txt3f.Width = 6.416667F;
			// 
			// Shape61
			// 
			this.Shape61.Height = 0.1875F;
			this.Shape61.Left = 0.4583333F;
			this.Shape61.Name = "Shape61";
			this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape61.Top = 6.625F;
			this.Shape61.Width = 6.541667F;
			// 
			// txt3g
			// 
			this.txt3g.Height = 0.1875F;
			this.txt3g.Left = 0.5208333F;
			this.txt3g.Name = "txt3g";
			this.txt3g.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3g.Text = " ";
			this.txt3g.Top = 6.625F;
			this.txt3g.Width = 6.416667F;
			// 
			// Shape62
			// 
			this.Shape62.Height = 0.1875F;
			this.Shape62.Left = 0.4583333F;
			this.Shape62.Name = "Shape62";
			this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape62.Top = 6.8125F;
			this.Shape62.Width = 6.541667F;
			// 
			// txt3h
			// 
			this.txt3h.Height = 0.1875F;
			this.txt3h.Left = 0.5208333F;
			this.txt3h.Name = "txt3h";
			this.txt3h.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt3h.Text = " ";
			this.txt3h.Top = 6.8125F;
			this.txt3h.Width = 6.416667F;
			// 
			// Shape63
			// 
			this.Shape63.Height = 0.1875F;
			this.Shape63.Left = 0.4583333F;
			this.Shape63.Name = "Shape63";
			this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape63.Top = 7.5F;
			this.Shape63.Width = 6.541667F;
			// 
			// txt4a
			// 
			this.txt4a.Height = 0.1875F;
			this.txt4a.Left = 0.5208333F;
			this.txt4a.Name = "txt4a";
			this.txt4a.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4a.Text = " ";
			this.txt4a.Top = 7.5F;
			this.txt4a.Width = 6.416667F;
			// 
			// Shape64
			// 
			this.Shape64.Height = 0.1875F;
			this.Shape64.Left = 0.4583333F;
			this.Shape64.Name = "Shape64";
			this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape64.Top = 7.6875F;
			this.Shape64.Width = 6.541667F;
			// 
			// txt4b
			// 
			this.txt4b.Height = 0.1875F;
			this.txt4b.Left = 0.5208333F;
			this.txt4b.Name = "txt4b";
			this.txt4b.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4b.Text = " ";
			this.txt4b.Top = 7.6875F;
			this.txt4b.Width = 6.416667F;
			// 
			// Shape65
			// 
			this.Shape65.Height = 0.1875F;
			this.Shape65.Left = 0.4583333F;
			this.Shape65.Name = "Shape65";
			this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape65.Top = 7.875F;
			this.Shape65.Width = 6.541667F;
			// 
			// txt4c
			// 
			this.txt4c.Height = 0.1875F;
			this.txt4c.Left = 0.5208333F;
			this.txt4c.Name = "txt4c";
			this.txt4c.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4c.Text = " ";
			this.txt4c.Top = 7.875F;
			this.txt4c.Width = 6.416667F;
			// 
			// Shape66
			// 
			this.Shape66.Height = 0.1875F;
			this.Shape66.Left = 0.4583333F;
			this.Shape66.Name = "Shape66";
			this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape66.Top = 8.0625F;
			this.Shape66.Width = 6.541667F;
			// 
			// txt4d
			// 
			this.txt4d.Height = 0.1875F;
			this.txt4d.Left = 0.5208333F;
			this.txt4d.Name = "txt4d";
			this.txt4d.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4d.Text = " ";
			this.txt4d.Top = 8.0625F;
			this.txt4d.Width = 6.416667F;
			// 
			// Shape67
			// 
			this.Shape67.Height = 0.1875F;
			this.Shape67.Left = 0.4583333F;
			this.Shape67.Name = "Shape67";
			this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape67.Top = 8.25F;
			this.Shape67.Width = 6.541667F;
			// 
			// txt4e
			// 
			this.txt4e.Height = 0.1875F;
			this.txt4e.Left = 0.5208333F;
			this.txt4e.Name = "txt4e";
			this.txt4e.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4e.Text = " ";
			this.txt4e.Top = 8.25F;
			this.txt4e.Width = 6.416667F;
			// 
			// Shape68
			// 
			this.Shape68.Height = 0.1875F;
			this.Shape68.Left = 0.4583333F;
			this.Shape68.Name = "Shape68";
			this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape68.Top = 8.4375F;
			this.Shape68.Width = 6.541667F;
			// 
			// txt4f
			// 
			this.txt4f.Height = 0.1875F;
			this.txt4f.Left = 0.5208333F;
			this.txt4f.Name = "txt4f";
			this.txt4f.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4f.Text = " ";
			this.txt4f.Top = 8.4375F;
			this.txt4f.Width = 6.416667F;
			// 
			// Shape69
			// 
			this.Shape69.Height = 0.1875F;
			this.Shape69.Left = 0.4583333F;
			this.Shape69.Name = "Shape69";
			this.Shape69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape69.Top = 8.625F;
			this.Shape69.Width = 6.541667F;
			// 
			// txt4g
			// 
			this.txt4g.Height = 0.1875F;
			this.txt4g.Left = 0.5208333F;
			this.txt4g.Name = "txt4g";
			this.txt4g.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4g.Text = " ";
			this.txt4g.Top = 8.625F;
			this.txt4g.Width = 6.416667F;
			// 
			// Shape70
			// 
			this.Shape70.Height = 0.1875F;
			this.Shape70.Left = 0.4583333F;
			this.Shape70.Name = "Shape70";
			this.Shape70.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape70.Top = 8.8125F;
			this.Shape70.Width = 6.541667F;
			// 
			// txt4h
			// 
			this.txt4h.Height = 0.1875F;
			this.txt4h.Left = 0.5208333F;
			this.txt4h.Name = "txt4h";
			this.txt4h.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4h.Text = " ";
			this.txt4h.Top = 8.8125F;
			this.txt4h.Width = 6.416667F;
			// 
			// Shape71
			// 
			this.Shape71.Height = 0.1875F;
			this.Shape71.Left = 0.4583333F;
			this.Shape71.Name = "Shape71";
			this.Shape71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape71.Top = 9F;
			this.Shape71.Width = 6.541667F;
			// 
			// txt4i
			// 
			this.txt4i.Height = 0.1875F;
			this.txt4i.Left = 0.5208333F;
			this.txt4i.Name = "txt4i";
			this.txt4i.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4i.Text = " ";
			this.txt4i.Top = 9F;
			this.txt4i.Width = 6.416667F;
			// 
			// Shape72
			// 
			this.Shape72.Height = 0.1875F;
			this.Shape72.Left = 0.4583333F;
			this.Shape72.Name = "Shape72";
			this.Shape72.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape72.Top = 9.1875F;
			this.Shape72.Width = 6.541667F;
			// 
			// txt4j
			// 
			this.txt4j.Height = 0.1875F;
			this.txt4j.Left = 0.5208333F;
			this.txt4j.Name = "txt4j";
			this.txt4j.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt4j.Text = " ";
			this.txt4j.Top = 9.1875F;
			this.txt4j.Width = 6.416667F;
			// 
			// srptMVRPage9
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txt2b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label235)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label237)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label238)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label239)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label241)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label242)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label243)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label245)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label246)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label247)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label248)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label249)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label250)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label251)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label252)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label253)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label254)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label255)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label256)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label257)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label258)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label260)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label261)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label262)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyDemolished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyConverted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyLoss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyDemolished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyConverted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyLoss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyDemolished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyConverted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyLoss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourDemolished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourConverted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourLoss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHDemolished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHConverted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHLoss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHDemolished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHConverted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHLoss)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOneFamilyNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTwoFamilyNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThreeFourFamilyNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverFourNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMHNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSHNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2f)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2g)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2h)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3f)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3g)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3h)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4f)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4g)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4h)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4i)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4j)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape ShapeGrid;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label235;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCounty;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label237;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label238;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label239;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label241;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label242;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label243;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label245;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label246;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label247;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label248;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label249;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label250;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label251;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label252;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label253;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label254;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label255;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label256;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label257;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label258;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label260;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label261;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label262;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOneFamilyNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOneFamilyDemolished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOneFamilyConverted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOneFamilyIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOneFamilyLoss;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTwoFamilyNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTwoFamilyDemolished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTwoFamilyConverted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTwoFamilyIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTwoFamilyLoss;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThreeFourFamilyNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThreeFourFamilyDemolished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThreeFourFamilyConverted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThreeFourFamilyIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThreeFourFamilyLoss;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverFourNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverFourDemolished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverFourConverted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverFourIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverFourLoss;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHDemolished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHConverted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHLoss;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHDemolished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHConverted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHLoss;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOneFamilyNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTwoFamilyNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThreeFourFamilyNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverFourNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMHNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSHNet;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2a;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2b;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2c;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2d;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2e;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2f;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2g;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2h;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3a;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3b;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3c;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3d;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3e;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3f;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3g;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3h;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4a;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4b;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4c;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4d;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4e;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4f;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4g;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape70;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4h;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape71;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4i;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape72;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4j;
	}
}
