﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCostSchedule.
	/// </summary>
	public partial class frmCostSchedule : BaseForm
	{
		public frmCostSchedule()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCostSchedule InstancePtr
		{
			get
			{
				return (frmCostSchedule)Sys.GetInstance(typeof(frmCostSchedule));
			}
		}

		protected frmCostSchedule _InstancePtr = null;
		//=========================================================
		bool boolUseNextPrev;
		int CurrSchedNumber;
		clsDRWrapper clsCRec = new clsDRWrapper();
		string strFactorAmountTitle = "";
		int FakeSchedNumber;
		bool boolLoaded;
		bool boolOutBuildingSQFTFactor;
		int lngLastTownCode;

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			Grid.RowData(Grid.GetFlexRowIndex(e.RowIndex), true);
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridTownCode.EndEdit();
			//Application.DoEvents();
			Grid.Focus();
			//Application.DoEvents();
		}

		private void gridTownCode_Leave(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (lngLastTownCode == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
				return;
			FillGrid();
			lngLastTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
		}

		private void ResizeGrid()
		{
			int lngWidth = 0;
			lngWidth = Grid.WidthOriginal;
			//FC:FINAL:MSH - i.issue #1091: changed size of columns for fully displaying last column
			//Grid.ColWidth(0, FCConvert.ToInt32(lngWidth * 0.162));
			//Grid.ColWidth(1, FCConvert.ToInt32(lngWidth * 0.163));
			//Grid.ColWidth(2, FCConvert.ToInt32(lngWidth * 0.5));
			Grid.ColWidth(0, FCConvert.ToInt32(lngWidth * 0.14));
			Grid.ColWidth(1, FCConvert.ToInt32(lngWidth * 0.16));
			Grid.ColWidth(2, FCConvert.ToInt32(lngWidth * 0.48));
            //FC:FINAL:BSE #3344 expand column 
            Grid.ColWidth(3, FCConvert.ToInt32(lngWidth * 0.16));
			// .ColWidth(3) = lngWidth * 0.162
		}

		private void SetupGrid()
		{
			clsDRWrapper clsDesc = new clsDRWrapper();
			int x;
			int lngWidth;
			lngWidth = Grid.WidthOriginal;
			Grid.Cols = 4;
            //FC:FINAL:BSE #3344 all cols should not be fixed cols 
            //Grid.FixedCols = 3;
            Grid.FixedCols = 2;
			Grid.TextMatrix(0, 0, "Code");
			Grid.TextMatrix(0, 1, "S Desc.");
			Grid.TextMatrix(0, 2, "Description");
			Grid.TextMatrix(0, 3, "Factor");
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//FC:FINAL:CHN - i.issue #1632: Incorrect column content alignment.
			//Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			Grid.Rows = 700;
			clsDesc.OpenRecordset("select * from costrecord where crecordnumber between 1 and 699 order by crecordnumber", modGlobalVariables.strREDatabase);
			for (x = 1; x <= 699; x++)
			{
				Grid.TextMatrix(x, 1, Strings.Trim(FCConvert.ToString(clsDesc.Get_Fields_String("csdesc" + ""))));
				Grid.TextMatrix(x, 2, Strings.Trim(FCConvert.ToString(clsDesc.Get_Fields_String("cldesc" + ""))));
				clsDesc.MoveNext();
			}
            //FC:FINAL:MSH - issue #1670: change alignment of columns headers and data
            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 699, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// x
		}

		public void Init(string strTitle)
		{
			modGlobalFunctions.SetFixedSize(this);
			// Grid.Rows = 700
			// Call clsCRec.OpenRecordset("select * from outbuildingsqftfactors order by code", strREDatabase)
			// Do While Not clsCRec.EndOfFile
			// Grid.TextMatrix(clsCRec.Fields("code"), 0) = clsCRec.Fields("code")
			// Grid.TextMatrix(clsCRec.Fields("code"), 3) = Format(Val(clsCRec.Fields("factor")), "0.00")
			// clsCRec.MoveNext
			// Loop
			this.Text = strTitle;
            //FC:FINAL:MSH - issue #1670: change the HeaderText with the form title
            HeaderText.Text = strTitle;
			this.Show(App.MainForm);
		}

		private void FillGrid()
		{
			Grid.Rows = 700;
			clsCRec.OpenRecordset("select * from outbuildingsqftfactors where isnull(townnumber,0) = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))) + " order by code", modGlobalVariables.strREDatabase);
			while (!clsCRec.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				Grid.TextMatrix(clsCRec.Get_Fields("code"), 0, FCConvert.ToString(clsCRec.Get_Fields("code")));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				Grid.TextMatrix(clsCRec.Get_Fields("code"), 3, Strings.Format(FCConvert.ToString(Conversion.Val(clsCRec.Get_Fields_Double("factor"))), "0.00"));
				clsCRec.MoveNext();
			}
		}

		private void frmCostSchedule_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				// Call SetupGrid
				boolLoaded = true;
			}
		}

		private void frmCostSchedule_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuQuit_Click();
			}
		}

		private void frmCostSchedule_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCostSchedule properties;
			//frmCostSchedule.ScaleWidth	= 9045;
			//frmCostSchedule.ScaleHeight	= 7380;
			//frmCostSchedule.LinkTopic	= "Form1";
			//frmCostSchedule.LockControls	= true;
			//End Unmaped Properties
			boolLoaded = false;
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			SetupGridTownCode();
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			FillGrid();
		}

		private void frmCostSchedule_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void Grid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rptSchedule.Show , MDIParent
			// boolPrintAllScheds = False
			frmReportViewer.InstancePtr.Init(rptSchedule.InstancePtr);
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuQuit_Click()
		{
			//mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//Application.DoEvents();
			SaveData();
		}

		private bool SaveData()
		{
			bool SaveData = false;
			SaveData = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngRow;
			modGlobalVariables.Statics.boolCostRecChanged = true;
			lngRow = Grid.FindRow(true);
			while (lngRow > 0)
			{
				clsSave.Execute("update outbuildingsqftfactors set factor = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, 3))) + " where townnumber  = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))) + " and  code = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, 0))), modGlobalVariables.strREDatabase);
				Grid.RowData(lngRow, false);
				if (lngRow < Grid.Rows - 1)
				{
					lngRow = Grid.FindRow(lngRow + 1);
				}
				else
				{
					lngRow = -1;
				}
			}
			MessageBox.Show("Save Complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
			SaveData = true;
			return SaveData;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//Application.DoEvents();
			if (SaveData())
			{
				mnuQuit_Click();
			}
		}
	}
}
