﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExportODonnell.
	/// </summary>
	partial class frmExportODonnell : BaseForm
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkNameAddress;
		public fecherFoundation.FCCheckBox chkProperty;
		public fecherFoundation.FCLabel lblProgress;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExportODonnell));
			this.cmbType = new fecherFoundation.FCComboBox();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkNameAddress = new fecherFoundation.FCCheckBox();
			this.chkProperty = new fecherFoundation.FCCheckBox();
			this.lblProgress = new fecherFoundation.FCLabel();
			this.cmdExport = new Wisej.Web.Button();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNameAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkProperty)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdExport);
			this.BottomPanel.Location = new System.Drawing.Point(0, 445);
			this.BottomPanel.Size = new System.Drawing.Size(711, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.lblProgress);
			this.ClientArea.Size = new System.Drawing.Size(711, 385);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(711, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(83, 30);
			this.HeaderText.Text = "Export";
			// 
			// cmbType
			// 
			this.cmbType.AutoSize = false;
			this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbType.FormattingEnabled = true;
			this.cmbType.Items.AddRange(new object[] {
				"Account",
				"Map Lot"
			});
			this.cmbType.Location = new System.Drawing.Point(10, 10);
			this.cmbType.Name = "cmbType";
			this.cmbType.Size = new System.Drawing.Size(145, 40);
			this.cmbType.TabIndex = 11;
			this.cmbType.Text = "Account";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(153, 164);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(120, 40);
			this.cmbRange.TabIndex = 7;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(30, 178);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 8;
			this.lblRange.Text = "RANGE";
			// 
			// Frame3
			// 
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.txtEnd);
			this.Frame3.Controls.Add(this.cmbType);
			this.Frame3.Controls.Add(this.txtStart);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Location = new System.Drawing.Point(410, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(166, 203);
			this.Frame3.TabIndex = 6;
			this.Frame3.Visible = false;
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(10, 148);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(145, 40);
			this.txtEnd.TabIndex = 10;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(10, 70);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(145, 40);
			this.txtStart.TabIndex = 9;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(72, 120);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(25, 18);
			this.Label1.TabIndex = 11;
			this.Label1.Text = "TO";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkNameAddress);
			this.Frame1.Controls.Add(this.chkProperty);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(369, 114);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Export";
			// 
			// chkNameAddress
			// 
			this.chkNameAddress.Checked = true;
			this.chkNameAddress.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkNameAddress.Location = new System.Drawing.Point(20, 30);
			this.chkNameAddress.Name = "chkNameAddress";
			this.chkNameAddress.Size = new System.Drawing.Size(167, 27);
			this.chkNameAddress.TabIndex = 2;
			this.chkNameAddress.Text = "Name and Address";
			// 
			// chkProperty
			// 
			this.chkProperty.Checked = true;
			this.chkProperty.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkProperty.Location = new System.Drawing.Point(20, 67);
			this.chkProperty.Name = "chkProperty";
			this.chkProperty.Size = new System.Drawing.Size(176, 27);
			this.chkProperty.TabIndex = 1;
			this.chkProperty.Text = "Property Information";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(22, 271);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(305, 21);
			this.lblProgress.TabIndex = 12;
			// 
			// cmdExport
			// 
			this.cmdExport.AppearanceKey = "acceptButton";
			this.cmdExport.Location = new System.Drawing.Point(310, 30);
			this.cmdExport.Name = "cmdExport";
			this.cmdExport.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdExport.Size = new System.Drawing.Size(100, 48);
			this.cmdExport.TabIndex = 1;
			this.cmdExport.Text = "Export";
			this.cmdExport.Click += new System.EventHandler(this.mnuExport_Click);
			// 
			// frmExportODonnell
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(711, 553);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmExportODonnell";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Export";
			this.Load += new System.EventHandler(this.frmExportODonnell_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExportODonnell_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNameAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkProperty)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdExport;
	}
}
