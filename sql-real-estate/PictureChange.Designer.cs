﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPictureChange.
	/// </summary>
	partial class frmPictureChange : BaseForm
	{
		public fecherFoundation.FCButton cmdNO;
		public fecherFoundation.FCButton cmdYes;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblAccountCard;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPictureBox imgPicture;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPictureChange));
            this.cmdNO = new fecherFoundation.FCButton();
            this.cmdYes = new fecherFoundation.FCButton();
            this.lblName = new fecherFoundation.FCLabel();
            this.lblAccountCard = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.imgPicture = new fecherFoundation.FCPictureBox();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 413);
            this.BottomPanel.Size = new System.Drawing.Size(765, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdNO);
            this.ClientArea.Controls.Add(this.cmdYes);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.lblAccountCard);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.imgPicture);
            this.ClientArea.Size = new System.Drawing.Size(785, 528);
            this.ClientArea.Controls.SetChildIndex(this.imgPicture, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAccountCard, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblName, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdYes, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdNO, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(785, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(233, 30);
            this.HeaderText.Text = "Change/Add Picture";
            // 
            // cmdNO
            // 
            this.cmdNO.AppearanceKey = "actionButton";
            this.cmdNO.Location = new System.Drawing.Point(134, 373);
            this.cmdNO.Name = "cmdNO";
            this.cmdNO.Size = new System.Drawing.Size(74, 40);
            this.cmdNO.TabIndex = 4;
            this.cmdNO.Text = "No";
            this.cmdNO.Click += new System.EventHandler(this.cmdNO_Click);
            // 
            // cmdYes
            // 
            this.cmdYes.AppearanceKey = "actionButton";
            this.cmdYes.Location = new System.Drawing.Point(30, 373);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(74, 40);
            this.cmdYes.TabIndex = 3;
            this.cmdYes.Text = "Yes";
            this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(30, 119);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(301, 14);
            this.lblName.TabIndex = 2;
            // 
            // lblAccountCard
            // 
            this.lblAccountCard.Location = new System.Drawing.Point(30, 75);
            this.lblAccountCard.Name = "lblAccountCard";
            this.lblAccountCard.Size = new System.Drawing.Size(301, 14);
            this.lblAccountCard.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(752, 15);
            this.Label1.Text = "IS THIS THE CORRECT PICTURE?  IF YES IS CHOSEN, THIS PICTURE WILL BE LINKED WITH " +
    "THE CURRENT ACCOUNT AND CARD";
            // 
            // imgPicture
            // 
            this.imgPicture.Image = ((System.Drawing.Image)(resources.GetObject("imgPicture.Image")));
            this.imgPicture.Location = new System.Drawing.Point(30, 163);
            this.imgPicture.Name = "imgPicture";
            this.imgPicture.Size = new System.Drawing.Size(339, 180);
            this.imgPicture.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // frmPictureChange
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(785, 588);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPictureChange";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Change/Add Picture";
            this.Load += new System.EventHandler(this.frmPictureChange_Load);
            this.Activated += new System.EventHandler(this.frmPictureChange_Activated);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPicture)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
