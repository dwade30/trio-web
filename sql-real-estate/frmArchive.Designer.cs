﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmArchive.
	/// </summary>
	partial class frmArchive : BaseForm
	{
		public fecherFoundation.FCButton cmdNew;
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuRunArchive;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuCreateArchive;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmArchive));
            this.cmdNew = new fecherFoundation.FCButton();
            this.Grid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRunArchive = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCreateArchive = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdfrmAudit = new fecherFoundation.FCButton();
            this.cmdCreateArchive = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdfrmAudit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdNew);
            this.BottomPanel.Location = new System.Drawing.Point(0, 360);
            this.BottomPanel.Size = new System.Drawing.Size(575, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(575, 300);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdfrmAudit);
            this.TopPanel.Controls.Add(this.cmdCreateArchive);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Size = new System.Drawing.Size(575, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCreateArchive, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdfrmAudit, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(106, 30);
            this.HeaderText.Text = "Archives";
            // 
            // cmdNew
            // 
            this.cmdNew.AppearanceKey = "acceptButton";
            this.cmdNew.Location = new System.Drawing.Point(222, 30);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdNew.Size = new System.Drawing.Size(142, 48);
            this.cmdNew.TabIndex = 2;
            this.cmdNew.Text = "New Archive";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 4;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(20, 58);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.ReadOnly = true;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(535, 222);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 1;
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(180, 18);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "CHOOSE AN ARCHIVE TO RUN";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRunArchive,
            this.mnuSepar2,
            this.mnuCreateArchive,
            this.mnuSepar3,
            this.mnuDelete,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuRunArchive
            // 
            this.mnuRunArchive.Index = 0;
            this.mnuRunArchive.Name = "mnuRunArchive";
            this.mnuRunArchive.Text = "Run Archive";
            this.mnuRunArchive.Click += new System.EventHandler(this.mnuRunArchive_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuCreateArchive
            // 
            this.mnuCreateArchive.Index = 2;
            this.mnuCreateArchive.Name = "mnuCreateArchive";
            this.mnuCreateArchive.Text = "Create Archive";
            this.mnuCreateArchive.Click += new System.EventHandler(this.mnuCreateArchive_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 3;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 4;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Archive";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 5;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdfrmAudit
            // 
            this.cmdfrmAudit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdfrmAudit.AppearanceKey = "toolbarButton";
            this.cmdfrmAudit.Location = new System.Drawing.Point(225, 29);
            this.cmdfrmAudit.Name = "cmdfrmAudit";
            this.cmdfrmAudit.Size = new System.Drawing.Size(92, 24);
            this.cmdfrmAudit.TabIndex = 1;
            this.cmdfrmAudit.Text = "Run Archive";
            this.cmdfrmAudit.Click += new System.EventHandler(this.mnuRunArchive_Click);
            // 
            // cmdCreateArchive
            // 
            this.cmdCreateArchive.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCreateArchive.AppearanceKey = "toolbarButton";
            this.cmdCreateArchive.Location = new System.Drawing.Point(323, 29);
            this.cmdCreateArchive.Name = "cmdCreateArchive";
            this.cmdCreateArchive.Size = new System.Drawing.Size(108, 24);
            this.cmdCreateArchive.TabIndex = 2;
            this.cmdCreateArchive.Text = "Create Archive";
            this.cmdCreateArchive.Click += new System.EventHandler(this.mnuCreateArchive_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(437, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(110, 24);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete Archive";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // frmArchive
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(575, 468);
            this.FillColor = 0;
            this.Name = "frmArchive";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Archives";
            this.Load += new System.EventHandler(this.frmArchive_Load);
            this.Resize += new System.EventHandler(this.frmArchive_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdfrmAudit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDelete;
		private FCButton cmdCreateArchive;
		private FCButton cmdfrmAudit;
	}
}
