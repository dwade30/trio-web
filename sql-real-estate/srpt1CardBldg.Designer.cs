﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srpt1CardBldg.
	/// </summary>
	partial class srpt1CardBldg
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srpt1CardBldg));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSoundValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEcon = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFloors = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoundValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArea)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFloors)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtSoundValue,
				this.txtDesc,
				this.txtArea,
				this.txtValue,
				this.txtGrade,
				this.txtCondition,
				this.txtPhys,
				this.txtFunc,
				this.txtEcon,
				this.txtFloors
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10
			});
			this.ReportHeader.Height = 0.2083333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalValue,
				this.Label6
			});
			this.ReportFooter.Height = 0.1875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.9375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Sqft";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.5729167F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.552083F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: left";
			this.Label2.Text = "Grade";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.4270833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.4375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Value";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.5729167F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: left";
			this.Label4.Text = "Description";
			this.Label4.Top = 0F;
			this.Label4.Width = 1.239583F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: left";
			this.Label5.Text = "Condition";
			this.Label5.Top = 0F;
			this.Label5.Width = 0.8541667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Phys";
			this.Label7.Top = 0F;
			this.Label7.Width = 0.3645833F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.458333F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Func";
			this.Label8.Top = 0F;
			this.Label8.Width = 0.3645833F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.84375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Econ";
			this.Label9.Top = 0F;
			this.Label9.Width = 0.3645833F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.5F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: right";
			this.Label10.Text = "Floors";
			this.Label10.Top = 0F;
			this.Label10.Width = 0.4270833F;
			// 
			// txtSoundValue
			// 
			this.txtSoundValue.Height = 0.1875F;
			this.txtSoundValue.Left = 1.916667F;
			this.txtSoundValue.Name = "txtSoundValue";
			this.txtSoundValue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center";
			this.txtSoundValue.Text = "****  Sound Value  ****";
			this.txtSoundValue.Top = 0F;
			this.txtSoundValue.Visible = false;
			this.txtSoundValue.Width = 2.96875F;
			// 
			// txtDesc
			// 
			this.txtDesc.Height = 0.1875F;
			this.txtDesc.Left = 0F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtDesc.Text = null;
			this.txtDesc.Top = 0F;
			this.txtDesc.Width = 1.527778F;
			// 
			// txtArea
			// 
			this.txtArea.Height = 0.1875F;
			this.txtArea.Left = 1.9375F;
			this.txtArea.Name = "txtArea";
			this.txtArea.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtArea.Text = null;
			this.txtArea.Top = 0F;
			this.txtArea.Width = 0.5729167F;
			// 
			// txtValue
			// 
			this.txtValue.Height = 0.1875F;
			this.txtValue.Left = 5.239583F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtValue.Text = "999,999,999";
			this.txtValue.Top = 0F;
			this.txtValue.Width = 0.7708333F;
			// 
			// txtGrade
			// 
			this.txtGrade.Height = 0.1875F;
			this.txtGrade.Left = 2.552083F;
			this.txtGrade.Name = "txtGrade";
			this.txtGrade.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtGrade.Text = null;
			this.txtGrade.Top = 0F;
			this.txtGrade.Width = 0.4583333F;
			// 
			// txtCondition
			// 
			this.txtCondition.Height = 0.1875F;
			this.txtCondition.Left = 3.0625F;
			this.txtCondition.Name = "txtCondition";
			this.txtCondition.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtCondition.Text = null;
			this.txtCondition.Top = 0F;
			this.txtCondition.Width = 1.010417F;
			// 
			// txtPhys
			// 
			this.txtPhys.Height = 0.1875F;
			this.txtPhys.Left = 4.0625F;
			this.txtPhys.Name = "txtPhys";
			this.txtPhys.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtPhys.Text = "100%";
			this.txtPhys.Top = 0F;
			this.txtPhys.Width = 0.3645833F;
			// 
			// txtFunc
			// 
			this.txtFunc.Height = 0.1875F;
			this.txtFunc.Left = 4.458333F;
			this.txtFunc.Name = "txtFunc";
			this.txtFunc.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtFunc.Text = null;
			this.txtFunc.Top = 0F;
			this.txtFunc.Width = 0.3645833F;
			// 
			// txtEcon
			// 
			this.txtEcon.Height = 0.1875F;
			this.txtEcon.Left = 4.84375F;
			this.txtEcon.Name = "txtEcon";
			this.txtEcon.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtEcon.Text = null;
			this.txtEcon.Top = 0F;
			this.txtEcon.Width = 0.3645833F;
			// 
			// txtFloors
			// 
			this.txtFloors.Height = 0.1875F;
			this.txtFloors.Left = 1.5625F;
			this.txtFloors.Name = "txtFloors";
			this.txtFloors.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtFloors.Text = null;
			this.txtFloors.Top = 0F;
			this.txtFloors.Width = 0.3645833F;
			// 
			// txtTotalValue
			// 
			this.txtTotalValue.Height = 0.1875F;
			this.txtTotalValue.Left = 4.8125F;
			this.txtTotalValue.Name = "txtTotalValue";
			this.txtTotalValue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtTotalValue.Text = null;
			this.txtTotalValue.Top = 0F;
			this.txtTotalValue.Width = 1.197917F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: left";
			this.Label6.Text = "Total Value of Buildings:";
			this.Label6.Top = 0F;
			this.Label6.Width = 1.854167F;
			// 
			// srpt1CardBldg
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.041667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoundValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArea)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFloors)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoundValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtArea;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEcon;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFloors;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
