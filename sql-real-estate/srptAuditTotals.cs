﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptAuditTotals.
	/// </summary>
	public partial class srptAuditTotals : FCSectionReport
	{
		public static srptAuditTotals InstancePtr
		{
			get
			{
				return (srptAuditTotals)Sys.GetInstance(typeof(srptAuditTotals));
			}
		}

		protected srptAuditTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public srptAuditTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptAuditTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string strPrefix = "";
			int lngCount;
			double lngLand;
			double lngbldg;
			double lngExempt;
			double lngAssess;
			int lngTotBillCount;
			double lngTotBillLand;
			double lngTotBillBldg;
			double lngTotBillExempt;
			double lngTotBillAssess;
			int lngTotTotCount;
			double lngTotTotLand;
			double lngTotTotBldg;
			double lngTotTotExempt;
			double lngTotTotAssess;
			string[] aryTag = null;
			string strExemption = "";
			SubReport1.Report = new srptLastTran();
			SubReport1.Report.UserData = this.UserData;
			if (Strings.Left(FCConvert.ToString(this.UserData), 1) == "C")
			{
				strPrefix = "RL";
				strExemption = "CorrExemption";
			}
			else if (Strings.Left(FCConvert.ToString(this.UserData), 1) == "L")
			{
				strPrefix = "Last";
				strExemption = "RLExemption";
			}
			if (FCConvert.ToString(this.UserData).Length > 1)
			{
				aryTag = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
				this.UserData = aryTag[1];
				// Me.Tag = Mid(Me.Tag, 3)
			}
			lngCount = 0;
			lngLand = 0;
			lngbldg = 0;
			lngExempt = 0;
			lngAssess = 0;
			if (modGlobalVariables.Statics.boolinctot)
			{
				// totally exempt without homesteads
				strSQL = "(select rsaccount, sum(" + strPrefix + "landval) as Landsum1,sum(" + strPrefix + "bldgval) as bldgsum1,sum(" + strExemption + ") as exemptsum1 from master where not rsdeleted = 1  " + this.UserData + " group by rsaccount having  (sum(" + strPrefix + "landval) + sum(" + strPrefix + "bldgval) - sum(" + strExemption + ")) = 0 and sum(" + strExemption + ") > 0 and sum(isnull(homesteadvalue ,0)) = 0) as tblSums";
				strSQL = "Select count(rsaccount) as thecount,sum(landsum1) as landsum,sum(bldgsum1) as bldgsum,sum(exemptsum1) as exemptsum from " + strSQL;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					lngLand = Conversion.Val(clsLoad.Get_Fields("landsum"));
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					lngbldg = Conversion.Val(clsLoad.Get_Fields("bldgsum"));
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngExempt = Conversion.Val(clsLoad.Get_Fields("exemptsum"));
					lngAssess = lngLand - lngExempt + lngbldg;
				}
			}
			txtCountTotExempt.Text = Strings.Format(lngCount, "#,##0");
			txtLandTotExempt.Text = Strings.Format(lngLand, "#,###,##0");
			txtBldgTotExempt.Text = Strings.Format(lngbldg, "#,###,##0");
			txtExemptTotExempt.Text = Strings.Format(lngExempt, "#,###,##0");
			txtAssessTotExempt.Text = Strings.Format(lngAssess, "#,###,##0");
			lngTotTotCount = lngCount;
			lngTotTotLand = lngLand;
			lngTotTotBldg = lngbldg;
			lngTotTotExempt = lngExempt;
			lngTotTotAssess = lngAssess;
			lngCount = 0;
			lngLand = 0;
			lngbldg = 0;
			lngExempt = 0;
			lngAssess = 0;
			if (modGlobalVariables.Statics.boolinctot)
			{
				// totally exempt with homestead exemptions
				strSQL = "(select rsaccount, sum(" + strPrefix + "landval) as Landsum1,sum(" + strPrefix + "bldgval) as bldgsum1,sum(" + strExemption + ") as exemptsum1 from master where not rsdeleted = 1   " + this.UserData + " group by rsaccount having (sum(" + strPrefix + "landval) + sum(" + strPrefix + "bldgval) - sum(" + strExemption + ")) = 0 and sum(" + strExemption + ") > 0 and sum(isnull(homesteadvalue,0)) > 0) as tblSums";
				strSQL = "Select count(rsaccount) as thecount,sum(landsum1) as landsum,sum(bldgsum1) as bldgsum,sum(exemptsum1) as exemptsum from " + strSQL;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					lngLand = Conversion.Val(clsLoad.Get_Fields("landsum"));
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					lngbldg = Conversion.Val(clsLoad.Get_Fields("bldgsum"));
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngExempt = Conversion.Val(clsLoad.Get_Fields("exemptsum"));
					lngAssess = lngLand - lngExempt + lngbldg;
				}
			}
			lngTotTotCount += lngCount;
			lngTotTotLand += lngLand;
			lngTotTotBldg += lngbldg;
			lngTotTotExempt += lngExempt;
			lngTotTotAssess += lngAssess;
			lngTotBillCount = lngCount;
			lngTotBillLand = lngLand;
			lngTotBillBldg = lngbldg;
			lngTotBillExempt = lngExempt;
			lngTotBillAssess = lngAssess;
			txtCountHom.Text = Strings.Format(lngCount, "#,##0");
			txtLandHom.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtBldgHom.Text = Strings.Format(lngbldg, "#,###,###,##0");
			txtExemptHom.Text = Strings.Format(lngExempt, "#,###,###,##0");
			txtAssessHom.Text = Strings.Format(lngAssess, "#,###,###,##0");
			txtCountHom2.Text = txtCountHom.Text;
			txtLandHom2.Text = txtLandHom.Text;
			txtBldgHom2.Text = txtBldgHom.Text;
			txtExemptHom2.Text = txtExemptHom.Text;
			txtAssessHom2.Text = txtAssessHom.Text;
			// total the exempt section
			txtCountExempt.Text = Strings.Format(lngTotTotCount, "#,##0");
			txtLandExempt.Text = Strings.Format(lngTotTotLand, "#,###,###,##0");
			txtBldgExempt.Text = Strings.Format(lngTotTotBldg, "#,###,###,##0");
			txtExemptExempt.Text = Strings.Format(lngTotTotExempt, "#,###,###,##0");
			txtAssessExempt.Text = Strings.Format(lngTotTotAssess, "#,###,###,##0");
			// now billing
			strSQL = "(select rsaccount, sum(" + strPrefix + "landval) as Landsum1,sum(" + strPrefix + "bldgval) as bldgsum1,sum(" + strExemption + ") as exemptsum1 from master where not rsdeleted = 1 " + this.UserData + " group by rsaccount having  (sum(" + strPrefix + "landval) + sum(" + strPrefix + "bldgval) - sum(" + strExemption + ")) > 0) as tblSums";
			strSQL = "Select count(rsaccount) as thecount,sum(landsum1) as landsum,sum(bldgsum1) as bldgsum,sum(exemptsum1) as exemptsum from " + strSQL;
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount"))));
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				lngLand = Conversion.Val(clsLoad.Get_Fields("landsum"));
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				lngbldg = Conversion.Val(clsLoad.Get_Fields("bldgsum"));
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				lngExempt = Conversion.Val(clsLoad.Get_Fields("exemptsum"));
				lngAssess = lngLand - lngExempt + lngbldg;
			}
			else
			{
				lngCount = 0;
				lngLand = 0;
				lngbldg = 0;
				lngExempt = 0;
				lngAssess = 0;
			}
			lngTotTotCount += lngCount;
			lngTotTotLand += lngLand;
			lngTotTotBldg += lngbldg;
			lngTotTotExempt += lngExempt;
			lngTotTotAssess += lngAssess;
			lngTotBillCount += lngCount;
			lngTotBillLand += lngLand;
			lngTotBillBldg += lngbldg;
			lngTotBillExempt += lngExempt;
			lngTotBillAssess += lngAssess;
			// now the not billable and not exempt accounts
			strSQL = "select count(rsaccount) as thecount from (select rsaccount from master where not rsdeleted = 1 " + this.UserData + " group by rsaccount having  (sum(" + strExemption + ") = 0 and sum(" + strPrefix + "landval) + sum(" + strPrefix + "bldgval) = 0)) a";
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			int lngCountNoVal = 0;
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				lngCountNoVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount"))));
			}
			else
			{
				lngCountNoVal = 0;
			}
			lngTotTotCount += lngCountNoVal;
			txtCountNoVal.Text = Strings.Format(lngCountNoVal, "#,##0");
			txtCountBillable.Text = Strings.Format(lngCount, "#,##0");
			txtLandBillable.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtBldgBillable.Text = Strings.Format(lngbldg, "#,###,###,##0");
			txtExemptBillable.Text = Strings.Format(lngExempt, "#,###,###,##0");
			txtAssessBillable.Text = Strings.Format(lngAssess, "#,###,###,##0");
			txtCountTotBillable.Text = Strings.Format(lngTotBillCount, "#,##0");
			txtLandTotBillable.Text = Strings.Format(lngTotBillLand, "#,###,###,##0");
			txtBldgTotBillable.Text = Strings.Format(lngTotBillBldg, "#,###,###,##0");
			txtExemptTotBillable.Text = Strings.Format(lngTotBillExempt, "#,###,###,##0");
			txtAssessTotBillable.Text = Strings.Format(lngTotBillAssess, "#,###,###,##0");
			txtCountTotal.Text = Strings.Format(lngTotTotCount, "#,##0");
			txtLandTotal.Text = Strings.Format(lngTotTotLand, "#,###,###,##0");
			txtBldgTotal.Text = Strings.Format(lngTotTotBldg, "#,###,###,##0");
			txtExemptTotal.Text = Strings.Format(lngTotTotExempt, "#,###,###,##0");
			txtAssessTotal.Text = Strings.Format(lngTotTotAssess, "#,###,###,##0");
		}

		
	}
}
