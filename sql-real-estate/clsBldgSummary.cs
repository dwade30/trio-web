﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class clsBldgSummary
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strArea = "";
		private string strStories = string.Empty;
		private string strFunc = string.Empty;
		private string strPhys = "";
		private string strEcon = "";
		private string strValue = "";
		private string strCondition = "";
		private string strGrade = "";

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Area
		{
			get
			{
				return strArea;
			}
			set
			{
				strArea = value;
			}
		}

		public string Stories
		{
			set
			{
				strStories = value;
			}
			get
			{
				string Stories = "";
				Stories = strStories;
				return Stories;
			}
		}

		public string Func
		{
			set
			{
				strFunc = value;
			}
			get
			{
				string Func = "";
				Func = strFunc;
				return Func;
			}
		}

		public string Phys
		{
			get
			{
				string Phys = "";
				Phys = strPhys;
				return Phys;
			}
			set
			{
				strPhys = value;
			}
		}

		public string Econ
		{
			get
			{
				string Econ = "";
				Econ = strEcon;
				return Econ;
			}
			set
			{
				strEcon = value;
			}
		}

		public string BldgValue
		{
			get
			{
				string BldgValue = "";
				BldgValue = strValue;
				return BldgValue;
			}
			set
			{
				strValue = value;
			}
		}

		public string Condition
		{
			get
			{
				string Condition = "";
				Condition = strCondition;
				return Condition;
			}
			set
			{
				strCondition = value;
			}
		}

		public string Grade
		{
			get
			{
				string Grade = "";
				Grade = strGrade;
				return Grade;
			}
			set
			{
				strGrade = value;
			}
		}
	}
}
