﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewAssessReports.
	/// </summary>
	partial class frmNewAssessReports : BaseForm
	{
		public fecherFoundation.FCGrid Grid;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCComboBox cmbUsers;
		public fecherFoundation.FCTextBox txtTitle;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label5;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewAssessReports));
            this.Grid = new fecherFoundation.FCGrid();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmbUsers = new fecherFoundation.FCComboBox();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSaveReport = new fecherFoundation.FCButton();
            this.cmdNewReport = new fecherFoundation.FCButton();
            this.cmdLoadReport = new fecherFoundation.FCButton();
            this.cmdPrintReport = new fecherFoundation.FCButton();
            this.cmdAddParameter = new fecherFoundation.FCButton();
            this.cmdPrintCodes = new fecherFoundation.FCButton();
            this.cmdSaveAs = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddParameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintCodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveReport);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(977, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.cmbUsers);
            this.ClientArea.Controls.Add(this.txtTitle);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Size = new System.Drawing.Size(977, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddParameter);
            this.TopPanel.Controls.Add(this.cmdSaveAs);
            this.TopPanel.Controls.Add(this.cmdNewReport);
            this.TopPanel.Controls.Add(this.cmdLoadReport);
            this.TopPanel.Controls.Add(this.cmdPrintReport);
            this.TopPanel.Controls.Add(this.cmdPrintCodes);
            this.TopPanel.Size = new System.Drawing.Size(977, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintCodes, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintReport, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLoadReport, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewReport, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveAs, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddParameter, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(239, 30);
            this.HeaderText.Text = "Assessment Reports";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 7;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 160);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(917, 366);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.Grid, "Use Insert and Delete keys to add and remove parameters");
            this.Grid.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_CellChanged);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(176, 90);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(333, 40);
            this.txtDescription.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // cmbUsers
            // 
            this.cmbUsers.AutoSize = false;
            this.cmbUsers.BackColor = System.Drawing.SystemColors.Window;
            this.cmbUsers.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbUsers.FormattingEnabled = true;
            this.cmbUsers.Location = new System.Drawing.Point(755, 30);
            this.cmbUsers.Name = "cmbUsers";
            this.cmbUsers.Size = new System.Drawing.Size(193, 40);
            this.cmbUsers.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cmbUsers, null);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.LinkItem = null;
            this.txtTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTitle.LinkTopic = null;
            this.txtTitle.Location = new System.Drawing.Point(176, 30);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(333, 40);
            this.txtTitle.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtTitle, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(579, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(127, 14);
            this.Label7.TabIndex = 4;
            this.Label7.Text = "CREATOR / OWNER";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(79, 14);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(47, 14);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "TITLE";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // cmdSaveReport
            // 
            this.cmdSaveReport.AppearanceKey = "acceptButton";
            this.cmdSaveReport.Location = new System.Drawing.Point(421, 30);
            this.cmdSaveReport.Name = "cmdSaveReport";
            this.cmdSaveReport.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveReport.Size = new System.Drawing.Size(137, 48);
            this.cmdSaveReport.TabIndex = 0;
            this.cmdSaveReport.Text = "Save Report";
            this.ToolTip1.SetToolTip(this.cmdSaveReport, null);
            this.cmdSaveReport.Click += new System.EventHandler(this.mnuSaveandExit_Click);
            // 
            // cmdNewReport
            // 
            this.cmdNewReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewReport.AppearanceKey = "toolbarButton";
            this.cmdNewReport.Location = new System.Drawing.Point(526, 29);
            this.cmdNewReport.Name = "cmdNewReport";
            this.cmdNewReport.Size = new System.Drawing.Size(90, 24);
            this.cmdNewReport.TabIndex = 1;
            this.cmdNewReport.Text = "New Report";
            this.ToolTip1.SetToolTip(this.cmdNewReport, null);
            this.cmdNewReport.Click += new System.EventHandler(this.mnuNewReport_Click);
            // 
            // cmdLoadReport
            // 
            this.cmdLoadReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLoadReport.AppearanceKey = "toolbarButton";
            this.cmdLoadReport.Location = new System.Drawing.Point(622, 29);
            this.cmdLoadReport.Name = "cmdLoadReport";
            this.cmdLoadReport.Size = new System.Drawing.Size(92, 24);
            this.cmdLoadReport.TabIndex = 2;
            this.cmdLoadReport.Text = "Load Report";
            this.ToolTip1.SetToolTip(this.cmdLoadReport, null);
            this.cmdLoadReport.Click += new System.EventHandler(this.mnuLoadReport_Click);
            // 
            // cmdPrintReport
            // 
            this.cmdPrintReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintReport.AppearanceKey = "toolbarButton";
            this.cmdPrintReport.Location = new System.Drawing.Point(720, 29);
            this.cmdPrintReport.Name = "cmdPrintReport";
            this.cmdPrintReport.Size = new System.Drawing.Size(135, 24);
            this.cmdPrintReport.TabIndex = 3;
            this.cmdPrintReport.Text = "Print Report Layout";
            this.ToolTip1.SetToolTip(this.cmdPrintReport, null);
            this.cmdPrintReport.Click += new System.EventHandler(this.mnuPrintReport_Click);
            // 
            // cmdAddParameter
            // 
            this.cmdAddParameter.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddParameter.AppearanceKey = "toolbarButton";
            this.cmdAddParameter.Location = new System.Drawing.Point(262, 29);
            this.cmdAddParameter.Name = "cmdAddParameter";
            this.cmdAddParameter.Size = new System.Drawing.Size(110, 24);
            this.cmdAddParameter.TabIndex = 4;
            this.cmdAddParameter.Text = "Add Parameter";
            this.ToolTip1.SetToolTip(this.cmdAddParameter, null);
            this.cmdAddParameter.Click += new System.EventHandler(this.mnuAddParameter_Click);
            // 
            // cmdPrintCodes
            // 
            this.cmdPrintCodes.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintCodes.AppearanceKey = "toolbarButton";
            this.cmdPrintCodes.Location = new System.Drawing.Point(861, 29);
            this.cmdPrintCodes.Name = "cmdPrintCodes";
            this.cmdPrintCodes.Size = new System.Drawing.Size(88, 24);
            this.cmdPrintCodes.TabIndex = 5;
            this.cmdPrintCodes.Text = "Print Codes";
            this.ToolTip1.SetToolTip(this.cmdPrintCodes, null);
            this.cmdPrintCodes.Click += new System.EventHandler(this.mnuPrintCodes_Click);
            // 
            // cmdSaveAs
            // 
            this.cmdSaveAs.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveAs.AppearanceKey = "toolbarButton";
            this.cmdSaveAs.Location = new System.Drawing.Point(378, 29);
            this.cmdSaveAs.Name = "cmdSaveAs";
            this.cmdSaveAs.Size = new System.Drawing.Size(142, 24);
            this.cmdSaveAs.TabIndex = 6;
            this.cmdSaveAs.Text = "Save as New Report";
            this.ToolTip1.SetToolTip(this.cmdSaveAs, null);
            this.cmdSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // frmNewAssessReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(977, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmNewAssessReports";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Assessment Reports";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmNewAssessReports_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewAssessReports_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmNewAssessReports_KeyPress);
            this.Resize += new System.EventHandler(this.frmNewAssessReports_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddParameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintCodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveReport;
		private FCButton cmdNewReport;
		private FCButton cmdLoadReport;
		private FCButton cmdPrintReport;
		private FCButton cmdAddParameter;
		private FCButton cmdPrintCodes;
		private FCButton cmdSaveAs;
	}
}
