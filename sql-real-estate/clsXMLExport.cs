﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Xml;
using System.IO;

namespace TWRE0000
{
	public class clsXMLExport
	{
		//=========================================================
		private string strHeader = string.Empty;
		private string strFileName = string.Empty;
		private string strFilePath = string.Empty;
		private string strRootElement = string.Empty;
		private string strRootAttributes = string.Empty;
		private XmlDocument testdoc = new XmlDocument();
		private XmlDocumentFragment DocFrag;
		private XmlElement eTemp;
		private StreamWriter ts;

		public string RootElement
		{
			set
			{
				strRootElement = value;
			}
			get
			{
				string RootElement = "";
				RootElement = strRootElement;
				return RootElement;
			}
		}

		public string RootElementAttributeXML
		{
			set
			{
				strRootAttributes = value;
			}
			get
			{
				string RootElementAttributeXML = "";
				RootElementAttributeXML = strRootAttributes;
				return RootElementAttributeXML;
			}
		}

		public clsXMLExport() : base()
		{
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				strHeader = "<?xml version =" + FCConvert.ToString(Convert.ToChar(34)) + "1.0" + FCConvert.ToString(Convert.ToChar(34)) + " encoding = " + FCConvert.ToString(Convert.ToChar(34)) + "ascii" + FCConvert.ToString(Convert.ToChar(34)) + " ?>";
				testdoc = new XmlDocument();
				DocFrag = testdoc.CreateDocumentFragment();
				// Set eTemp = testdoc.createElement("woogy")
				// eTemp.setAttributeNode (testdoc.createAttribute("a1"))
				// Call eTemp.setAttribute("a1", "5.5" & Chr(34) & " inches")
				// DocFrag.appendChild (eTemp)
				// strTemp = eTemp.xml
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Initialize", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool AddToDoc(ref string strData)
		{
			bool AddToDoc = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (strData != string.Empty)
				{
					ts.Write(strData);
				}
				return AddToDoc;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ts.Close();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AddToDoc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddToDoc;
		}
		// vbPorter upgrade warning: strValue As string	OnWrite(string, int, double, bool)
		public string CreateElement(string strElementType, string strValue)
		{
			string CreateElement = "";
			string strReturn = "";
			eTemp = testdoc.CreateElement(strElementType);
			eTemp.InnerText = strValue;
			// strReturn = "<" & strElementType
			// If Trim(strAttributes) <> vbNullString Then
			// strReturn = strReturn & " " & strAttributes
			// End If
			// strReturn = strReturn & ">"
			// strReturn = strReturn & strValue
			// strReturn = strReturn & "</" & strElementType & ">"
			// BuildElement = strReturn
			return CreateElement;
		}
		// Public Function BuildAttributes(ByRef strAttributesList As String, strAttributeName As String, strAttributeValue As String) As String
		// Dim strReturn As String
		// strReturn = strAttributesList & " " & strAttributeName & " = " & Chr(34) & strAttributeValue & Chr(34)
		// BuildAttributes = strReturn
		// End Function
		// vbPorter upgrade warning: strAttributeValue As string	OnWrite(short, double)
		public void AddAttribute(string strAttributeName, string strAttributeValue)
		{
			eTemp.SetAttribute(strAttributeName, strAttributeValue);
		}

		public string GetElementXML()
		{
			string GetElementXML = "";
			//FC:FINAL:MSH - i.issue #1557: return XML with tags(not only text value)
			//GetElementXML = eTemp.InnerXml;
			GetElementXML = eTemp.OuterXml;
			return GetElementXML;
		}

		public bool StartDoc(ref string strPath, ref string strFile)
		{
			bool StartDoc = false;
			try
			{
				// On Error GoTo ErrorHandler
				StartDoc = false;
				strFileName = Strings.Trim(strFile);
				strFilePath = Strings.Trim(strPath);
				if (strFilePath != string.Empty)
				{
					if (Strings.Right(strFilePath, 1) != "\\")
					{
						strFilePath += "\\";
					}
				}
				if (strFileName != string.Empty)
				{
					if (System.IO.File.Exists(strFilePath + strFileName))
					{
						System.IO.File.Delete(strFilePath + strFileName);
					}
					ts = System.IO.File.CreateText(strFilePath + strFileName);
					ts.Write(strHeader);
					if (Strings.Trim(strRootElement) == string.Empty)
					{
						strRootElement = "RootElement";
					}
					if (Strings.Trim(strRootAttributes) == string.Empty)
					{
						ts.Write("<" + strRootElement + ">");
					}
					else
					{
						ts.Write("<" + strRootElement + " " + strRootAttributes + ">");
					}
					StartDoc = true;
				}
				return StartDoc;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In StartDoc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return StartDoc;
		}

		public bool EndDoc()
		{
			bool EndDoc = false;
			try
			{
				// On Error GoTo ErrorHandler
				EndDoc = false;
				ts.Write("</" + strRootElement + ">");
				ts.Close();
				EndDoc = true;
				return EndDoc;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In EndDoc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return EndDoc;
		}
	}
}
