﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class modREConstants
	{
		//=========================================================
		// Categories for Land Types
		public const int CNSTLANDCATNONE = 0;
		public const int CNSTLANDCATSOFTWOOD = 1;
		public const int CNSTLANDCATMIXEDWOOD = 2;
		public const int CNSTLANDCATHARDWOOD = 3;
		public const int CNSTLANDCATSOFTWOODFARM = 4;
		public const int CNSTLANDCATMIXEDWOODFARM = 5;
		public const int CNSTLANDCATHARDWOODFARM = 6;
		public const int CNSTLANDCATCROP = 7;
		public const int CNSTLANDCATORCHARD = 8;
		public const int CNSTLANDCATPASTURE = 9;
		public const int CNSTLANDCATOPENSPACE = 10;
		public const int CNSTLANDCATWATERFRONT = 11;
		public const int CNSTLANDTYPEFRONTFOOT = 0;
		public const int CNSTLANDTYPESQUAREFEET = 1;
		public const int CNSTLANDTYPEFRACTIONALACREAGE = 2;
		public const int CNSTLANDTYPEACRES = 3;
		public const int CNSTLANDTYPESITE = 5;
		public const int CNSTLANDTYPELINEARFEET = 6;
		public const int CNSTLANDTYPEIMPROVEMENTS = 7;
		public const int CNSTLANDTYPEFRONTFOOTSCALED = 8;
		public const int CNSTLANDTYPELINEARFEETSCALED = 9;
		public const int CNSTLANDTYPEEXTRAINFLUENCE = 4;
		public const int CNSTLANDSCHEDULESTANDARDDEPTH = -1;
		public const int CNSTLANDSCHEDULESTANDARDLOT = -2;
		public const int CNSTLANDSCHEDULEDESCRIPTION = -3;
		public const int CNSTLANDSCHEDULESTANDARDWIDTH = -4;
		public const int CNSTGRIDPHONESCOLNUMBER = 0;
		public const int CNSTGRIDPHONESCOLDESC = 1;
		public const int CNSTGRIDPHONESCOLAUTOID = 2;
		public const int CNSTPHONETYPEMASTER = 0;
	}
}
