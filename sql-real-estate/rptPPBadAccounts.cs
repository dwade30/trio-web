﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPBadAccounts.
	/// </summary>
	public partial class rptPPBadAccounts : BaseSectionReport
	{
		public rptPPBadAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Bad Accounts";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPPBadAccounts InstancePtr
		{
			get
			{
				return (rptPPBadAccounts)Sys.GetInstance(typeof(rptPPBadAccounts));
			}
		}

		protected rptPPBadAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPBadAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;
		int lngBadUpdated;
		int lngBadDeleted;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCRow >= frmPPImportXF.InstancePtr.GridBadAccounts.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			txtTotal.Text = Strings.Format(frmPPImportXF.InstancePtr.GridBadAccounts.Rows - 1, "#,###,###,##0");
			lngBadUpdated = 0;
			lngBadDeleted = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			txtName.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPOWNER));
			txtTRIOAccount.Text = frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPACCOUNT);
			txtOtherAccount.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPOTHERACCOUNT));
			if (Conversion.Val(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER)) > 0)
			{
				txtLocation.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER) + " " + frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
			}
			else
			{
				txtLocation.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
			}
			strTemp = "";
			if (Conversion.Val(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPUPDATED)) > 1)
			{
				strTemp += "Updated " + FCConvert.ToString(Conversion.Val(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPUPDATED))) + " times,";
				lngBadUpdated += 1;
			}
			if (FCConvert.CBool(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPDELETED)))
			{
				strTemp += "Deleted Account,";
				lngBadDeleted += 1;
			}
			if (strTemp != string.Empty)
			{
				// trim off the comma
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			txtDescription.Text = strTemp;
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalDeleted.Text = Strings.Format(lngBadDeleted, "#,###,###,##0");
			txtTotalUpdates.Text = Strings.Format(lngBadUpdated, "#,###,###,##0");
		}

		
	}
}
