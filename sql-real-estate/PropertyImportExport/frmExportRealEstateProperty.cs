﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExportMasterInfo.
	/// </summary>
	public partial class frmExportRealEstateProperty : BaseForm
	{
		public frmExportRealEstateProperty()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExportRealEstateProperty InstancePtr
		{
			get
			{
				return (frmExportRealEstateProperty)Sys.GetInstance(typeof(frmExportRealEstateProperty));
			}
		}

		protected frmExportRealEstateProperty _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void ChkNameAddress_CheckedChanged(object sender, System.EventArgs e)
		{
			if (ChkNameAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				chkExportAllParties.Visible = false;
			}
			else
			{
				chkExportAllParties.Visible = true;
			}
		}

		private void frmExportRealEstateProperty_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExportRealEstateProperty_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void mnuCopy_Click(object sender, System.EventArgs e)
		{
			string strDestPath;
			//FileSystemObject fso = new FileSystemObject();
			try
			{
				// On Error GoTo ErrorHandler
				if (!File.Exists("REExpFil.xml"))
				{
					MessageBox.Show("Could not find export file to copy", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.FileName = "REExpFil.xml";
				MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Select a directory to copy export file to";
				MDIParent.InstancePtr.CommonDialog1.Show();
				strDestPath = MDIParent.InstancePtr.CommonDialog1.FileName;
				//FC:FINAL:DSE Path should be relative to deployment
				//if (Strings.UCase(strDestPath) == Strings.UCase(Environment.CurrentDirectory + "\\" + "REEXPFIL.xml"))
				if (Strings.UCase(strDestPath) == Strings.UCase(Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "REEXPFIL.xml")))
				{
					MessageBox.Show("Cannot copy over self", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				//FC:FINAL:DSE Path should be relative to deployment
				//File.Copy(Environment.CurrentDirectory + "\\" + "REExpFil.xml", strDestPath, true);
				File.Copy(Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "REExpFil.xml"), strDestPath, true);
				MessageBox.Show("File Copied", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 32755)
				{
					// cancel button on file save
					Information.Err(ex).Clear();
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuCopy_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveAndCreate();
		}

		private bool SaveAndCreate()
		{
			bool SaveAndCreate = false;
			int intFlag;
			int intType = 0;
			try
			{
				// On Error GoTo ErrorHandler
				SaveAndCreate = false;
				intFlag = 0;
				if (ChkNameAddress.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 2;
				}
				if (ChkRef1.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 4;
				}
				if (chkRef2.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 8;
				}
				if (chkBookPage.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 16;
				}
				if (ChkTranLandBldg.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 32;
				}
				if (chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 64;
				}
				if (chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 128;
				}
				if (chkLandInformation.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 256;
				}
				if (chkPropertyInfo.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 512;
				}
				if (chkSaleData.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1024;
				}
				if (chkExemptInfo.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 2048;
				}
				if (chkCommercial.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkDwelling.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkOutbuilding.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkPreviousOwners.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkInterestedParties.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkLocation.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkPictures.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (chkInspection.CheckState == Wisej.Web.CheckState.Checked)
				{
					intFlag += 1;
				}
				if (intFlag == 0)
				{
					MessageBox.Show("You have chosen nothing to export", "No data chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveAndCreate;
				}
				if (cmbAccounts.Text == "Range")
				{
					if (Strings.Trim(txtStart.Text) == string.Empty || Strings.Trim(txtEnd.Text) == string.Empty)
					{
						MessageBox.Show("You must specify the range", "No Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveAndCreate;
					}
				}
				string strFname;
				strFname = "";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNOverwritePrompt+vbPorterConverter.cdlOFNPathMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.Filter = "XML files|*.xml";
				MDIParent.InstancePtr.CommonDialog1.DefaultExt = "xml";
				MDIParent.InstancePtr.CommonDialog1.FileName = "REExpFil.xml";
				//FC:FINAL:RPU:#i1353 - save the file to client using Application.Download
				//MDIParent.InstancePtr.CommonDialog1.ShowSave();
				strFname = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (Strings.Trim(strFname) == string.Empty)
					return SaveAndCreate;
				if (cmbAccounts.Text == "Range")
				{
					if (cmbtRange.Text == "Account")
					{
						intType = 0;
					}
					else if (cmbtRange.Text == "Name")
					{
						intType = 1;
					}
					else if (cmbtRange.Text == "Map / Lot")
					{
						intType = 2;
					}
					else if (cmbtRange.Text == "Location")
					{
						intType = 3;
					}
					if (CreateXMLExport_74(1, intType, Strings.Trim(txtStart.Text), Strings.Trim(txtEnd.Text), strFname))
					{
						SaveAndCreate = true;
					}
					// If CreateExportFile(intFlag, 1, intType, Trim(txtStart.Text), Trim(txtEnd.Text)) Then
					// SaveAndCreate = True
					// End If
				}
				else
				{
					if (CreateXMLExport_41(0, strFname))
					{
						SaveAndCreate = true;
					}
					// If CreateExportFile(intFlag) Then
					// SaveAndCreate = True
					// End If
				}
				return SaveAndCreate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveAndCreate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return SaveAndCreate;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveAndCreate())
			{
				this.Unload();
			}
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		private bool CreateXMLExport_41(int intRecords, string strFname = "REExpFil.xml")
		{
			return CreateXMLExport(intRecords, 0, "", "", strFname);
		}

		private bool CreateXMLExport_74(int intRecords, int intType = 0, string strStart = "", string strEnd = "", string strFname = "REExpFil.xml")
		{
			return CreateXMLExport(intRecords, intType, strStart, strEnd, strFname);
		}

		private bool CreateXMLExport(int intRecords = 0, int intType = 0, string strStart = "", string strEnd = "", string strFname = "REExpFil.xml")
		{
			bool CreateXMLExport = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsXMLExport xDoc = new clsXMLExport();
				// Dim intSaveFlag As Integer
				string strSQL;
                bool boolUseDeedNames = false;
				bool boolUseNameAddress = false;
				bool boolUseMaplot = false;
				bool boolUseBookPage = false;
				bool boolUseTaxAcquired = false;
				bool boolUseBankruptcy = false;
				bool boolUseExemptInformation = false;
				bool boolUseSaleData = false;
				bool boolUseTranLandBldgProp = false;
				bool boolUseRef1 = false;
				bool boolUseRef2 = false;
				bool boolUseLandInformation = false;
				bool boolUsePropertyInfo = false;
				bool boolUseLocation = false;
				bool boolUseDwelling = false;
				bool boolUseCommercial = false;
				bool boolUseOutbuilding = false;
				bool boolUseInterestedParties = false;
				bool boolUsePreviousOwners = false;
				bool boolUsePictures = false;
				bool boolUseInspection = false;
				bool boolOnlyUsedParties;
				string strDir;
				string strFil;
				int x;
				cPartyController tPC = new cPartyController();
				// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				cPartyAddress tAddress;
				cPartyComment tComment;
				cPartyPhoneNumber tPhone;
				cContact tContact;
				// Dim partyCol As New Collection
				string strRecord;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp = "";
				string strElement = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int lngCurAccount;
				int intCurCard;
				//FileSystemObject fso = new FileSystemObject();
				CreateXMLExport = false;
				//FC:FINAL:RPU:#i1353 - Save the file to UserData directory
				//strDir = Directory.GetParent(strFname).FullName;
				strDir = fecherFoundation.VisualBasicLayer.FCFileSystem.CurDir();
				strFil = new FileInfo(strFname).Name;
				// intSaveFlag = intFlag
				strRecord = "<ExportType>";
				strSQL = "Select * from master where ";
				string strWhere = "";
				switch (intRecords)
				{
					case 0:
						{
							strSQL = "select * from master order by rsaccount,rscard";
							xDoc.CreateElement("Type", "");
							xDoc.AddAttribute("Code", FCConvert.ToString(0));
							strRecord += xDoc.GetElementXML();
							break;
						}
					case 1:
						{
							strRecord += "<Type Code = " + FCConvert.ToString(Convert.ToChar(34)) + "1" + FCConvert.ToString(Convert.ToChar(34)) + ">";
							xDoc.CreateElement("RangeType", FCConvert.ToString(intType));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("RangeStart", strStart);
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("RangeEnd", strEnd);
							strRecord += xDoc.GetElementXML();
							switch (intType)
							{
								case 0:
									{
										// strSQL = strSQL & "rsaccount between " & Val(strStart) & " and " & Val(strEnd) & " order by rsaccount,rscard"
										strWhere = "rsaccount between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd));
										strSQL += strWhere + " order by rsaccount,rscard";
										break;
									}
								case 1:
									{
										// strSQL = strSQL & "rsname between '" & strStart & "' and '" & strEnd & "zzz' order by rsname,rsaccount,rscard"
										strWhere = "deedname1 between '" + strStart + "' and '" + strEnd + "zzz'";
										strSQL += strWhere + " order by deedname1,rsaccount,rscard";
										break;
									}
								case 2:
									{
										// strSQL = strSQL & "rsmaplot between '" & strStart & "' and '" & strEnd & "' order by rsmaplot,rsaccount,rscard"
										strWhere = "rsmaplot between '" + strStart + "' and '" + strEnd + "'";
										strSQL += strWhere + " order by rsmaplot,rsaccount,rscard";
										break;
									}
								case 3:
									{
										strWhere = "rslocstreet between '" + strStart + "' and '" + strEnd + "zzz'";
										strSQL += strWhere + " order by val(rslocnumalph & ''),rslocstreet,rsaccount,rscard";
                                        strSQL = strSQL + strWhere +
                                                 @" order by CAST(LEFT(rslocnumalph, Patindex('%[^-.0-9]%', rslocnumalph + 'x') - 1) AS Float) ,rslocstreet,rsaccount,rscard";

                                        break;
									}
							}
							//end switch
							if (strWhere != "")
							{
								strWhere = " and " + strWhere;
							}
							strRecord += "</Type>";
							break;
						}
				}
				//end switch
				if (ChkNameAddress.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseNameAddress = true;
					xDoc.CreateElement("Include", "Owner");
					strRecord += xDoc.GetElementXML();
				}

                if (chkDeedNames.CheckState == Wisej.Web.CheckState.Checked)
                {
                    boolUseDeedNames = true;
                    xDoc.CreateElement("Include", "DeedNames");
                    strRecord += xDoc.GetElementXML();
                }
				if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseMaplot = true;
					xDoc.CreateElement("Include", "MapLot");
					strRecord += xDoc.GetElementXML();
				}
				if (chkLocation.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseLocation = true;
					xDoc.CreateElement("Include", "Location");
					strRecord += xDoc.GetElementXML();
				}
				if (chkBookPage.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseBookPage = true;
					xDoc.CreateElement("Include", "BookPage");
					strRecord += xDoc.GetElementXML();
				}
				if (chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseTaxAcquired = true;
					xDoc.CreateElement("Include", "TaxAcquired");
					strRecord += xDoc.GetElementXML();
				}
				if (chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseBankruptcy = true;
					xDoc.CreateElement("Include", "Bankruptcy");
					strRecord += xDoc.GetElementXML();
				}
				if (chkExemptInfo.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseExemptInformation = true;
					xDoc.CreateElement("Include", "Exempt");
					strRecord += xDoc.GetElementXML();
				}
				if (chkSaleData.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseSaleData = true;
					xDoc.CreateElement("Include", "Sale");
					strRecord += xDoc.GetElementXML();
				}
				if (ChkTranLandBldg.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseTranLandBldgProp = true;
					xDoc.CreateElement("Include", "TranLandBldgProp");
					strRecord += xDoc.GetElementXML();
				}
				if (ChkRef1.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseRef1 = true;
					xDoc.CreateElement("Include", "Reference1");
					strRecord += xDoc.GetElementXML();
				}
				if (chkRef2.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseRef2 = true;
					xDoc.CreateElement("Include", "Reference2");
					strRecord += xDoc.GetElementXML();
				}
				if (chkLandInformation.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseLandInformation = true;
					xDoc.CreateElement("Include", "Land");
					strRecord += xDoc.GetElementXML();
				}
				if (chkPropertyInfo.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUsePropertyInfo = true;
					xDoc.CreateElement("Include", "Property");
					strRecord += xDoc.GetElementXML();
				}
				if (chkDwelling.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseDwelling = true;
					xDoc.CreateElement("Include", "Dwelling");
					strRecord += xDoc.GetElementXML();
				}
				if (chkCommercial.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseCommercial = true;
					xDoc.CreateElement("Include", "Commercial");
					strRecord += xDoc.GetElementXML();
				}
				if (chkOutbuilding.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseOutbuilding = true;
					xDoc.CreateElement("Include", "Outbuilding");
					strRecord += xDoc.GetElementXML();
				}
				if (chkInterestedParties.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseInterestedParties = true;
					xDoc.CreateElement("Include", "InterestedParties");
					strRecord += xDoc.GetElementXML();
				}
				if (chkPreviousOwners.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUsePreviousOwners = true;
					xDoc.CreateElement("Include", "PreviousOwners");
					strRecord += xDoc.GetElementXML();
				}
				if (chkPictures.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUsePictures = true;
					xDoc.CreateElement("Include", "Pictures");
					strRecord += xDoc.GetElementXML();
				}
				if (chkInspection.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseInspection = true;
					xDoc.CreateElement("Include", "Inspection");
					strRecord += xDoc.GetElementXML();
				}
				strRecord += "</ExportType>";
				xDoc.RootElement = "REExport";
				xDoc.RootElementAttributeXML = " User = " + FCConvert.ToString(Convert.ToChar(34)) + modGlobalConstants.Statics.MuniName + FCConvert.ToString(Convert.ToChar(34)) + " Created = " + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(DateTime.Now) + FCConvert.ToString(Convert.ToChar(34)) + " ";
				if (!xDoc.StartDoc(ref strDir, ref strFil))
				{
					return CreateXMLExport;
				}
				lngCurAccount = 0;
				intCurCard = 0;
				int intProgress;
				intProgress = 0;
				xDoc.AddToDoc(ref strRecord);
				// header record
				frmWait.InstancePtr.Init("Export Parties", false);
				boolOnlyUsedParties = chkExportAllParties.CheckState == Wisej.Web.CheckState.Unchecked;
				if (boolUseNameAddress)
				{
					// Dim tColl As Collection
					// Set tColl = tPC.GetParties("select * from parties", True)
					if (!boolOnlyUsedParties)
					{
						rsTemp.OpenRecordset("select id as theid from parties order by id", "CentralParties");
					}
					else
					{
						rsTemp.OpenRecordset("select distinct ownerpartyid as theid from master where ownerpartyid > 0 " + strWhere + " union select distinct secownerpartyid as theid from master where secownerpartyid > 0" + strWhere + " order by theid", modGlobalVariables.strREDatabase);
					}
					tParty = new cParty();
					while (!rsTemp.EndOfFile())
					{
						//Application.DoEvents();
						intProgress += 1;
						if (intProgress > 100)
							intProgress = 1;
						frmWait.InstancePtr.prgProgress.Value = intProgress;
						// TODO Get_Fields: Field [theID] not found!! (maybe it is an alias?)
						frmWait.InstancePtr.lblMessage.Text = "Exporting Party " + rsTemp.Get_Fields("theID");
						frmWait.InstancePtr.lblMessage.Refresh();
						//FC:FINAL:RPU:#i1353 - Update the form to show progress
						FCUtils.ApplicationUpdate(frmWait.InstancePtr);
						//Application.DoEvents();
						// TODO Get_Fields: Field [theid] not found!! (maybe it is an alias?)
						tParty = tPC.GetParty(rsTemp.Get_Fields("theid"));
						if (!(tParty == null))
						{
							if (tParty.ID > 0)
							{
								// Call tPC.FillParty(tParty)
								strRecord = "";
								strElement = PartyAsXML(ref tParty);
								strRecord += strElement;
								xDoc.AddToDoc(ref strRecord);
							}
						}
						rsTemp.MoveNext();
					}
				}
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (rsLoad.EndOfFile())
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return CreateXMLExport;
				}
				intProgress = 0;
				frmWait.InstancePtr.lblMessage.Text = "Export Accounts";
                frmWait.InstancePtr.Refresh();
                while (!rsLoad.EndOfFile())
				{
					//Application.DoEvents();
					strRecord = "";
					intProgress += 1;
					if (intProgress > 100)
						intProgress = 1;
					frmWait.InstancePtr.prgProgress.Value = intProgress;
					frmWait.InstancePtr.lblMessage.Text = "Exporting Account " + rsLoad.Get_Fields_Int32("rsaccount");
					frmWait.InstancePtr.lblMessage.Refresh();
					if (boolUseNameAddress && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						strRecord += "<Owner>";
						// Call xDoc.CreateElement("Name", Trim(rsLoad.Fields("rsname")))
						if (Conversion.Val(rsLoad.Get_Fields_Int32("ownerpartyid")) > 0)
						{
							tParty = tPC.GetParty(rsLoad.Get_Fields_Int32("ownerpartyid"));
							if (!(tParty == null))
							{
								if (tParty.ID > 0)
								{
									xDoc.CreateElement("OwnerPartyGUID", tParty.PartyGUID);
								}
								else
								{
									xDoc.CreateElement("OwnerPartyGUID", "");
								}
							}
							else
							{
								xDoc.CreateElement("OwnerPartyGUID", "");
							}
						}
						else
						{
							xDoc.CreateElement("OwnerPartyGUID", "");
						}
						strRecord += xDoc.GetElementXML();
						if (Conversion.Val(rsLoad.Get_Fields_Int32("secownerpartyid")) > 0)
						{
							tParty = tPC.GetParty(rsLoad.Get_Fields_Int32("secownerpartyid"));
							if (!(tParty == null))
							{
								if (tParty.ID > 0)
								{
									xDoc.CreateElement("SecOwnerPartyGUID", tParty.PartyGUID);
								}
								else
								{
									xDoc.CreateElement("SecOwnerPartyGUID", "");
								}
							}
							else
							{
								xDoc.CreateElement("SecOwnerPartyGUID", "");
							}
						}
						else
						{
							xDoc.CreateElement("SecOwnerPartyGUID", "");
						}
						// Call xDoc.CreateElement("SecOwnerPartyGUID", Trim(rsLoad.Fields("SecOwnerPartyID")))
						strRecord += xDoc.GetElementXML();
						strElement = "";
						xDoc.CreateElement("PreviousOwner", Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rspreviousmaster"))));
						strRecord += xDoc.GetElementXML();
						strRecord += "</Owner>";
					}

                    if (boolUseDeedNames && rsLoad.Get_Fields_Int32("rscard") == 1)
                    {
                        strRecord = strRecord + @"<DeedNames>";
                        xDoc.CreateElement("DeedName1", rsLoad.Get_Fields_String("DeedName1").Trim());
                        strRecord = strRecord + xDoc.GetElementXML();
                        xDoc.CreateElement("DeedName2", rsLoad.Get_Fields_String("DeedName2").Trim());
                        strRecord = strRecord + xDoc.GetElementXML();
                        strRecord = strRecord + @"</DeedNames>";

                    }
					if (boolUseMaplot && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						xDoc.CreateElement("MapLot", Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rsmaplot"))));
						strRecord += xDoc.GetElementXML();
					}
					if (boolUseLocation)
					{
						strRecord += "<Location>";
						xDoc.CreateElement("StreetNumber", Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rslocnumalph"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Apartment", Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rslocapt"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Street", Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rslocstreet"))));
						strRecord += xDoc.GetElementXML();
						strRecord += "</Location>";
					}
					if (boolUsePreviousOwners && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						rsTemp.OpenRecordset("select * from previousowner where account = " + rsLoad.Get_Fields_Int32("rsaccount") + " ", modGlobalVariables.strREDatabase);
						while (!rsTemp.EndOfFile())
						{
							//Application.DoEvents();
							strRecord += "<PreviousOwner>";
							xDoc.CreateElement("Name", rsTemp.Get_Fields_String("name"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("SecondOwner", rsTemp.Get_Fields_String("secowner"));
							strRecord += xDoc.GetElementXML();
							strRecord += "<Address>";
							xDoc.CreateElement("Addr", rsTemp.Get_Fields_String("address1"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Addr", rsTemp.Get_Fields_String("address2"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("City", rsTemp.Get_Fields_String("city"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("State", rsTemp.Get_Fields_String("state"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Zip", rsTemp.Get_Fields_String("zip"));
							strRecord += xDoc.GetElementXML();
							if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("zip4"))) != string.Empty)
							{
								xDoc.CreateElement("Zip4", rsTemp.Get_Fields_String("zip4"));
								strRecord += xDoc.GetElementXML();
							}
							strRecord += "</Address>";
							xDoc.CreateElement("SaleDate", FCConvert.ToString(rsTemp.Get_Fields_DateTime("saledate")));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("DateCreated", FCConvert.ToString(rsTemp.Get_Fields_DateTime("DateCreated")));
							strRecord += xDoc.GetElementXML();
							strRecord += "</PreviousOwner>";
							rsTemp.MoveNext();
						}
					}
					if (boolUseInterestedParties && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						if (boolUsePreviousOwners)
						{
							rsTemp.OpenRecordset("select * from owners where account = " + rsLoad.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsTemp.OpenRecordset("select * from owners where account = " + rsLoad.Get_Fields_Int32("rsaccount") + " and associd = 0", modGlobalVariables.strREDatabase);
						}
						while (!rsTemp.EndOfFile())
						{
							//Application.DoEvents();
							strRecord += "<InterestedParty>";
							xDoc.CreateElement("Name", rsTemp.Get_Fields_String("name"));
							strRecord += xDoc.GetElementXML();
							strRecord += "<Address>";
							xDoc.CreateElement("Addr", rsTemp.Get_Fields_String("address1"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Addr", rsTemp.Get_Fields_String("address2"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("City", rsTemp.Get_Fields_String("city"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("State", rsTemp.Get_Fields_String("State"));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Zip", rsTemp.Get_Fields_String("Zip"));
							strRecord += xDoc.GetElementXML();
							if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("zip4"))) != string.Empty)
							{
								xDoc.CreateElement("Zip4", rsTemp.Get_Fields_String("zip4"));
								strRecord += xDoc.GetElementXML();
							}
							strRecord += "</Address>";
							xDoc.CreateElement("AssociateID", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("associd"))));
							strRecord += xDoc.GetElementXML();
							strRecord += "</InterestedParty>";
							rsTemp.MoveNext();
						}
					}
					if (boolUseBookPage && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						rsTemp.OpenRecordset("select * from bookpage where account = " + rsLoad.Get_Fields_Int32("rsaccount") + " order by line", modGlobalVariables.strREDatabase);
						while (!rsTemp.EndOfFile())
						{
							//Application.DoEvents();
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							xDoc.CreateElement("Book", rsTemp.Get_Fields("book"));
							strElement = xDoc.GetElementXML();
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							xDoc.CreateElement("Page", rsTemp.Get_Fields("Page"));
							strElement += xDoc.GetElementXML();
							xDoc.CreateElement("BPDate", rsTemp.Get_Fields_String("BPDate"));
							strElement += xDoc.GetElementXML();
							xDoc.CreateElement("SaleDate", FCConvert.ToString(rsTemp.Get_Fields_DateTime("saledate")));
							strElement += xDoc.GetElementXML();
							strElement = "<BookPage Current = " + FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Boolean("current") + FCConvert.ToString(Convert.ToChar(34)) + " >" + strElement;
							strElement += "</BookPage>";
							strRecord += strElement;
							rsTemp.MoveNext();
						}
					}
					if (boolUseTaxAcquired && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						xDoc.CreateElement("TaxAcquired", FCConvert.ToString(rsLoad.Get_Fields_Boolean("TaxAcquired")));
						strRecord += xDoc.GetElementXML();
					}
					if (boolUseBankruptcy && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						xDoc.CreateElement("Bankruptcy", FCConvert.ToString(rsLoad.Get_Fields_Boolean("InBankruptcy")));
						strRecord += xDoc.GetElementXML();
					}
					//Application.DoEvents();
					if (boolUseExemptInformation && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						strRecord += "<Exemptions>";
						if (Conversion.Val(rsLoad.Get_Fields_Int32("riexemptcd1")) > 0)
						{
							xDoc.CreateElement("Exemption", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("riexemptcd1"))));
							// TODO Get_Fields: Check the table for the column [exemptpct1] and replace with corresponding Get_Field method
							xDoc.AddAttribute("Percent", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("exemptpct1"))));
							strRecord += xDoc.GetElementXML();
						}
						if (Conversion.Val(rsLoad.Get_Fields_Int32("riexemptcd2")) > 0)
						{
							xDoc.CreateElement("Exemption", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("riexemptcd2"))));
							// TODO Get_Fields: Check the table for the column [exemptpct2] and replace with corresponding Get_Field method
							xDoc.AddAttribute("Percent", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("exemptpct2"))));
							strRecord += xDoc.GetElementXML();
						}
						if (Conversion.Val(rsLoad.Get_Fields_Int32("riexemptcd3")) > 0)
						{
							xDoc.CreateElement("Exemption", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("riexemptcd3"))));
							// TODO Get_Fields: Check the table for the column [Exemptpct3] and replace with corresponding Get_Field method
							xDoc.AddAttribute("Percent", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("Exemptpct3"))));
							strRecord += xDoc.GetElementXML();
						}
						strRecord += "</Exemptions>";
					}
					if (boolUseSaleData && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
					{
						strRecord += "<Sale>";
						if ((Information.IsDate(rsLoad.Get_Fields("saledate")) && rsLoad.Get_Fields_DateTime("saledate").ToOADate() != 0) || Conversion.Val(rsLoad.Get_Fields_Int32("pisaleprice")) > 0)
						{
							xDoc.CreateElement("SaleDate", FCConvert.ToString(rsLoad.Get_Fields_DateTime("saledate")));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Price", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pisaleprice"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Validity", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pisalevalidity"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Verified", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pisaleverified"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Type", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pisaletype"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Financing", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pisalefinancing"))));
							strRecord += xDoc.GetElementXML();
						}
						strRecord += "</Sale>";
					}
					if (boolUseTranLandBldgProp)
					{
						if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rscard")) == 1)
						{
							// tran code is by account
							xDoc.CreateElement("TranCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ritrancode"))));
							strRecord += xDoc.GetElementXML();
						}
						xDoc.CreateElement("LandCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rilandcode"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("BuildingCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ribldgcode"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("PropertyCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("PropertyCode"))));
						strRecord += xDoc.GetElementXML();
					}
					//Application.DoEvents();
					if (boolUseRef1)
					{
						xDoc.CreateElement("Reference1", rsLoad.Get_Fields_String("rsref1"));
						strRecord += xDoc.GetElementXML();
					}
					if (boolUseRef2)
					{
						xDoc.CreateElement("Reference2", rsLoad.Get_Fields_String("rsref2"));
						strRecord += xDoc.GetElementXML();
					}
					if (boolUseLandInformation)
					{
						strRecord += "<LandData>";
						for (x = 1; x <= 7; x++)
						{
							//Application.DoEvents();
							// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
							if (Conversion.Val(rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "type")) > 0)
							{
								strRecord += "<Land>";
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								xDoc.CreateElement("LandType", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "type"))));
								strRecord += xDoc.GetElementXML();
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								xDoc.CreateElement("UnitsA", rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "UnitsA"));
								strRecord += xDoc.GetElementXML();
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								xDoc.CreateElement("UnitsB", rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "UnitsB"));
								strRecord += xDoc.GetElementXML();
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								xDoc.CreateElement("Influence", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "Inf"))));
								strRecord += xDoc.GetElementXML();
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								xDoc.CreateElement("InfluenceCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "Infcode"))));
								strRecord += xDoc.GetElementXML();
								strRecord += "</Land>";
							}
						}
						// x
						xDoc.CreateElement("Acres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("piacres"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("SoftAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rssoft"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("MixedAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rsmixed"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("HardAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rshard"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("OtherAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rsother"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("SoftValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rssoftValue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("MixedValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rsmixedvalue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("HardValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rshardvalue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("OtherValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rsothervalue"))));
						strRecord += xDoc.GetElementXML();
						strRecord += "</LandData>";
					}
					if (boolUsePropertyInfo)
					{
						//Application.DoEvents();
						strRecord += "<Property>";
						xDoc.CreateElement("Neighborhood", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pineighborhood"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("TreeGrowthYear", FCConvert.ToString(rsLoad.Get_Fields_Int32("pistreetcode")));
						strRecord += xDoc.GetElementXML();
						// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
						xDoc.CreateElement("XCoord", rsLoad.Get_Fields("pixcoord"));
						strRecord += xDoc.GetElementXML();
						// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
						xDoc.CreateElement("YCoord", rsLoad.Get_Fields("piycoord"));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Utilities", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("piutilities1"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Utilities", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("piutilities2"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Topography", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pitopography1"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Topography", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pitopography2"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Zone", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pizone"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("SecondaryZone", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("piseczone"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("OverrideZone", FCConvert.ToString(rsLoad.Get_Fields_Boolean("ZoneOverride")));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Street", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("pistreet"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Open1", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("piopen1"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("Open2", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("piopen2"))));
						strRecord += xDoc.GetElementXML();
						strRecord += "</Property>";
					}
					if (boolUseInspection)
					{
						//Application.DoEvents();
						strRecord += "<Inspection>";
						// TODO Get_Fields: Check the table for the column [EntranceCode] and replace with corresponding Get_Field method
						xDoc.CreateElement("EntranceCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("EntranceCode"))));
						strRecord += xDoc.GetElementXML();
						// TODO Get_Fields: Check the table for the column [InformationCode] and replace with corresponding Get_Field method
						xDoc.CreateElement("InformationCode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("InformationCode"))));
						strRecord += xDoc.GetElementXML();
						if (Information.IsDate(rsLoad.Get_Fields("dateinspected")))
						{
							xDoc.CreateElement("InspectionDate", FCConvert.ToString(rsLoad.Get_Fields_DateTime("DateInspected")));
							strRecord += xDoc.GetElementXML();
						}
						strRecord += "</Inspection>";
					}
					if (boolUseDwelling)
					{
						rsTemp.OpenRecordset("select * from dwelling where rsaccount = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						if (!rsTemp.EndOfFile())
						{
							strRecord += "<Dwelling>";
							xDoc.CreateElement("Style", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("distyle"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("DwellingUnits", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diunitsdwelling"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("OtherUnits", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diunitsother"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Stories", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("distories"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Exterior", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diextwalls"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Roof", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diroof"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Masonry", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("DIsfmasonry"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Open3", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diopen3"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Open4", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diopen4"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("YearBuilt", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diyearbuilt"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("YearRemodeled", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diyearremodel"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Foundation", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("difoundation"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Basement", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dibsmt"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("BasementGarage", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dibsmtgar"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("WetBasement", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diwetbsmt"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("SQFTBasementLiving", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("disfbsmtliving"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("BasementFinishedGrade", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dibsmtfingrade1"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("BasementFinishedFactor", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dibsmtfingrade2"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Open5", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diopen5"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Heat", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diheat"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("PercentHeated", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dipctheat"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Cool", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dicool"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("PercentCooled", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dipctcool"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("KitchenStyle", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dikitchens"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("BathStyle", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dibaths"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Rooms", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dirooms"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Bedrooms", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dibedrooms"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("FullBaths", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("difullbaths"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("HalfBaths", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dihalfbaths"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("AdditionalFixtures", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diaddnfixtures"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Fireplaces", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("difireplaces"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Layout", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dilayout"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Attic", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diattic"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Insulation", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("diinsulation"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("PercentUnfinished", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dipctunfinished"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Grade", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("digrade1"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Factor", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("digrade2"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("SQFTFootprint", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("disqft"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("Condition", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dicondition"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("PhysicalPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dipctphys"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("FunctionalPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dipctfunct"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("FunctionalCode", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("difunctcode"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("EconomicPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dipctecon"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("EconomicCode", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dieconcode"))));
							strRecord += xDoc.GetElementXML();
							xDoc.CreateElement("SQFTLivingArea", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("disfla"))));
							strRecord += xDoc.GetElementXML();
							strRecord += "</Dwelling>";
						}
					}
					//Application.DoEvents();
					if (boolUseCommercial)
					{
						rsTemp.OpenRecordset("select * from commercial where rsaccount = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						if (!rsTemp.EndOfFile())
						{
							for (x = 1; x <= 2; x++)
							{
								// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
								if (Conversion.Val(rsTemp.Get_Fields("occ" + FCConvert.ToString(x))) > 0)
								{
									strRecord += "<Commercial>";
									// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
									xDoc.CreateElement("OccupancyCode", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("occ" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [dwel] not found!! (maybe it is an alias?)
									xDoc.CreateElement("DwellingUnits", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("dwel" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Class", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "class"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Quality", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("C" + FCConvert.ToString(x) + "quality"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
									xDoc.CreateElement("GradeFactor", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("C" + FCConvert.ToString(x) + "grade"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Exterior", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("C" + FCConvert.ToString(x) + "extwalls"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Stories", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "stories"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Height", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "height"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("BaseFloorArea", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "floor"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Perimeter", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "perimeter"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Heat", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "Heat"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("YearBuilt", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "Built"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
									xDoc.CreateElement("YearRemodeled", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("C" + FCConvert.ToString(x) + "Remodel"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Condition", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("C" + FCConvert.ToString(x) + "Condition"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("PhysicalPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "Phys"))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
									xDoc.CreateElement("FunctionalPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("c" + FCConvert.ToString(x) + "funct"))));
									strRecord += xDoc.GetElementXML();
									if (x == 1)
									{
										xDoc.CreateElement("EconomicPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("cmecon"))));
										strRecord += xDoc.GetElementXML();
									}
									strRecord += "</Commercial>";
								}
							}
							// x
						}
					}
					if (boolUseOutbuilding)
					{
						rsTemp.OpenRecordset("select * from outbuilding where rsaccount = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						if (!rsTemp.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								//Application.DoEvents();
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (Conversion.Val(rsTemp.Get_Fields("oitype" + FCConvert.ToString(x))) > 0)
								{
									strRecord += "<Outbuilding>";
									// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Type", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oitype" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
									xDoc.CreateElement("YearBuilt", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oiyear" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Units", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oiunits" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Grade", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oigradecd" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Factor", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oigradepct" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
									xDoc.CreateElement("Condition", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oicond" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
									xDoc.CreateElement("PhysicalPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oipctphys" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
									xDoc.CreateElement("FunctionalPercent", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oipctfunct" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
									xDoc.CreateElement("UseSoundValue", FCConvert.ToString(rsTemp.Get_Fields("oiusesound" + FCConvert.ToString(x))));
									strRecord += xDoc.GetElementXML();
									// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
									xDoc.CreateElement("SoundValue", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))));
									strRecord += xDoc.GetElementXML();
									strRecord += "</Outbuilding>";
								}
							}
							// x
						}
					}
					if (boolUsePictures)
					{
						rsTemp.OpenRecordset("select * from PICTURERECORD where mraccountnumber = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard") + " order by picnum", modGlobalVariables.strREDatabase);
						while (!rsTemp.EndOfFile())
						{
							//Application.DoEvents();
							if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("picturelocation"))) != string.Empty)
							{
								strRecord += "<Picture>";
								xDoc.CreateElement("Path", rsTemp.Get_Fields_String("picturelocation"));
								strRecord += xDoc.GetElementXML();
								xDoc.CreateElement("Description", rsTemp.Get_Fields_String("description"));
								strRecord += xDoc.GetElementXML();
								strRecord += "</Picture>";
							}
							rsTemp.MoveNext();
						}
					}
					strRecord = "<Account Number = " + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_Int32("rsaccount") + FCConvert.ToString(Convert.ToChar(34)) + " Card = " + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_Int32("rscard") + FCConvert.ToString(Convert.ToChar(34)) + ">" + strRecord;
					strRecord += "</Account>";
					// Call xDoc.CreateElement("Account", strRecord)
					// Call xDoc.AddAttribute("Number", rsLoad.Fields("rsaccount"))
					// Call xDoc.AddAttribute("Card", rsLoad.Fields("rscard"))
					// strRecord = xDoc.GetElementXML
					xDoc.AddToDoc(ref strRecord);
					rsLoad.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				if (!xDoc.EndDoc())
				{
					return CreateXMLExport;
				}
				//FC:FINAL:RPU:#i1353 - Download the file to client 
				FCUtils.Download(Path.Combine(strDir, strFil), strFname);
				MessageBox.Show("Export Complete" + "\r\n" + "Created " + strFname, "Export Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				CreateXMLExport = true;
				return CreateXMLExport;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CreateXMLExport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateXMLExport;
		}

		private void optAccounts_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						framRange.Visible = false;
						break;
					}
				case 1:
					{
						framRange.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbAccounts.SelectedIndex;
			optAccounts_CheckedChanged(index, sender, e);
		}

		private string PartyAsXML(ref cParty tParty)
		{
			string PartyAsXML = "";
			clsXMLExport xDoc = new clsXMLExport();
			string strReturn;
			string strTemp = "";
			string strElement = "";
			strReturn = "<Party>";
			if (!(tParty == null))
			{
				xDoc.CreateElement("PartyGUID", tParty.PartyGUID);
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("PartyType", FCConvert.ToString(tParty.PartyType));
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("FirstName", tParty.FirstName);
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("MiddleName", tParty.MiddleName);
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("LastName", tParty.LastName);
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("Designation", tParty.Designation);
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("Email", tParty.Email);
				strReturn += xDoc.GetElementXML();
				xDoc.CreateElement("WebAddress", tParty.WebAddress);
				strReturn += xDoc.GetElementXML();
				foreach (cPartyAddress tAddress in tParty.Addresses)
				{
					strTemp = "<Address>";
					xDoc.CreateElement("Address1", tAddress.Address1);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Address2", tAddress.Address2);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Address3", tAddress.Address3);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("City", tAddress.City);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("State", tAddress.State);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Zip", tAddress.Zip);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Country", tAddress.Country);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("OverrideName", tAddress.OverrideName);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("AddressType", tAddress.AddressType);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Seasonal", FCConvert.ToString(tAddress.Seasonal));
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("ProgModule", tAddress.ProgModule);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Comment", tAddress.Comment);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("StartMonth", FCConvert.ToString(tAddress.StartMonth));
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("StartDay", FCConvert.ToString(tAddress.StartDay));
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("EndMonth", FCConvert.ToString(tAddress.EndMonth));
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("EndDay", FCConvert.ToString(tAddress.EndDay));
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("ModAccountID", FCConvert.ToString(tAddress.ModAccountID));
					strTemp += xDoc.GetElementXML();
					strTemp += "</Address>";
					strReturn += strTemp;
				}
				// tAddress
				foreach (cPartyPhoneNumber tPhone in tParty.PhoneNumbers)
				{
					strTemp = "<CentralPhoneNumber>";
					xDoc.CreateElement("PhoneNumber", tPhone.PhoneNumber);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Description", tPhone.Description);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("PhoneOrder", FCConvert.ToString(tPhone.PhoneOrder));
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Extension", tPhone.Extension);
					strTemp += xDoc.GetElementXML();
					strTemp += "</CentralPhoneNumber>";
					strReturn += strTemp;
				}
				// tPhone
				foreach (cPartyComment tComment in tParty.Comments)
				{
					strTemp = "<CentralComment>";
					xDoc.CreateElement("Comment", tComment.Comment);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("ProgModule", tComment.ProgModule);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("EnteredBy", tComment.EnteredBy);
					strTemp += xDoc.GetElementXML();
					strTemp += "</CentralComment>";
					strReturn += strTemp;
				}
				// tComment
				foreach (cContact tContact in tParty.Contacts)
				{
					strTemp = "<Contact>";
					xDoc.CreateElement("Name", tContact.Name);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Description", tContact.Description);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Email", tContact.Email);
					strTemp += xDoc.GetElementXML();
					xDoc.CreateElement("Comment", tContact.Comment);
					strTemp += xDoc.GetElementXML();
					foreach (cPartyPhoneNumber tPhone in tContact.PhoneNumbers)
					{
						strElement = "<ContactPhoneNumber>";
						xDoc.CreateElement("PhoneNumber", tPhone.PhoneNumber);
						strElement += xDoc.GetElementXML();
						xDoc.CreateElement("Description", tPhone.Description);
						strElement += xDoc.GetElementXML();
						xDoc.CreateElement("PhoneOrder", FCConvert.ToString(tPhone.PhoneOrder));
						strElement += xDoc.GetElementXML();
						xDoc.CreateElement("Extension", tPhone.Extension);
						strElement += xDoc.GetElementXML();
						strElement += "</ContactPhoneNumber>";
						strTemp += strElement;
					}
					// tPhone
					strTemp += "</Contact>";
					strReturn += strTemp;
				}
				// tContact
			}
			strReturn += "</Party>";
			PartyAsXML = strReturn;
			return PartyAsXML;
		}
	}
}
