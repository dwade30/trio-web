﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLaserLabels.
	/// </summary>
	public partial class rptLaserLabels : BaseSectionReport
	{
		public FCGrid Grid;

		public static rptLaserLabels InstancePtr
		{
			get
			{
				return (rptLaserLabels)Sys.GetInstance(typeof(rptLaserLabels));
			}
		}

		protected rptLaserLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				clsBookPage?.Dispose();
                clsBookPage = null;
            }
			base.Dispose(disposing);
		}

		public rptLaserLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
			//FC:FINAL:AM:#i1216 - use FCGrid for the grid
			Grid = new FCGrid();
			//FC:FINAL:DDU:#1836 added columns based on their type for sort to work
			Grid.Columns.Add("", typeof(int));
			Grid.Columns.Add("", typeof(int));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(int));
			Grid.Columns.Add("", typeof(int));
			Grid.Columns.Add("", typeof(int));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(string));
			Grid.Columns.Add("", typeof(DateAndTime));
			Grid.Columns.Add("", typeof(int));
			Grid.Columns.Add("", typeof(double));
			Grid.Columns.Add("", typeof(string));
			//Grid.Cols = 23;
			Grid.FixedRows = 0;
			Grid.FixedCols = 0;
			Grid.Rows = 0;
		}
		// nObj = 1
		//   0	rptLaserLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND frmCustomLabels.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		bool boolUseEightPoint;
		int intBlankLabelsLeftToPrint;
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(double, short)
		float intLabelWidth;
		bool boolDifferentPageSize;
		int intTypeOfLabel;
		string strFont = "";
		string strLine1;
		string strLine2;
		string strLine3;
		string strLine4;
		string strLine5;
		string strLine6;
		string strOrder = "";
		bool boolFirstLabel;
		bool boolShowSaleData;
		clsDRWrapper clsBookPage = new clsDRWrapper();
		bool boolUseBookPage;
		const int COLACCOUNT = 0;
		const int COLCARD = 1;
		const int COLNAME = 2;
		const int COLSECOWNER = 3;
		const int COLMAPLOT = 4;
		const int COLADDR1 = 5;
		const int COLADDR2 = 6;
		const int COLCITY = 7;
		const int COLSTATE = 8;
		const int COLZIP = 9;
		const int COLZIP4 = 10;
		const int COLLOCNUM = 11;
		const int COLLOCSTREET = 12;
		const int COLTELEPHONE = 13;
		const int COLLANDVAL = 14;
		const int COLBLDGVAL = 15;
		const int COLEXEMPTION = 16;
		const int COLREF1 = 17;
		const int COLREF2 = 18;
		const int COLSALEDATE = 19;
		const int COLSALEPRICE = 20;
		const int COLACREAGE = 21;
		const int COLCOUNTRY = 22;
		int lngCurrRow;
		bool boolIncSecOwner;
		bool boolIncLoc;
		bool boolIncRef;
		bool boolIncRef2;
		bool boolIncAcct;
		bool boolIncMap;
		bool boolIncAddr;
		bool boolIncETax;
		bool boolIncAcreage;
		bool boolIncOwner;
		bool boolByAccount;
		string strTaxYear = "";
		float snglTax;
		float sngAdjustment;
		int intNumberofDuplicates;
		int intDuplicateCount;
		int intLabelToPrint;
		int intMaxLines;
		// vbPorter upgrade warning: intCharactersWide As short --> As int	OnWrite(double, int)
		int intCharactersWide;
		string strCMFNumber;
		bool boolCMF;
		int lngCMFSessionID;
		string strCompName = "";
		DateTime dtCMFStart;
		bool boolDymo;
		int intCharsPerInch;
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(double, string)
		float lngPrintWidth;
		bool boolShowInViewer;
		string strIMPBNumber;
		// Private Const CNSTLBLTYPECERTMAILLASER = 11
		// Private Const CNSTLBLTYPEDYMO30256 = 12
		const int const_printtoolid = 9950;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int const_printtoolid;
			int cnt;
			// If boolDotMatrix Then
			// const_printtoolid = 9950
			// For cnt = 0 To Me.ToolBar.Tools.Count - 1
			// If "Print..." = Me.ToolBar.Tools(cnt).Caption Then
			// Me.ToolBar.Tools(cnt).ID = const_printtoolid
			// Me.ToolBar.Tools(cnt).Enabled = True
			// End If
			// Next cnt
			// End If
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = lngCurrRow >= Grid.Rows;
		}
		// vbPorter upgrade warning: intHowToPrint As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intTypePrint As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intOrdBy As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: strMinAccount As string	OnWrite(int, string)
		// vbPorter upgrade warning: strMaxAccount As string	OnWrite(int, string)
		// vbPorter upgrade warning: intNumDuplicates As short --> As int	OnWriteFCConvert.ToDouble(
		public void Init(bool boolElim, short intHowToPrint, short intTypePrint, short intOrdBy, int intLabelType, int bytWhatInclude = 0, string strMinAccount = "", string strMaxAccount = "", string strYear = "", float sngTax = 0, string strPrinterName = "", float sngAdjust = 0, int intNumDuplicates = 0, int intLabelStartPosition = 1, string strCMFBarCode = "", bool boolUseEightPointFont = false, bool boolPrintDontShow = false, bool boolUseViewer = false, string strIMPBCode = "")
		{
			string strDBName;
			int intReturn;
			string strSQL = "";
			int x = 0;
			string strTempOrder = "";
			bool boolUseFont;
			string strCard = "";
			string strName = "";
			string strAddr1 = "";
			string strRefBookPage = "";
			int cnt;
			string strPrinterToUse;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = rsData.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			try
			{
				
				boolShowInViewer = true;
				strPrinterToUse = strPrinterName;
				intCharsPerInch = 12;
				
				strCMFNumber = "";
				boolCMF = false;
				strIMPBNumber = "";
				boolUseEightPoint = boolUseEightPointFont;
				if (boolUseEightPointFont)
				{
					intCharsPerInch = 15;
				}
				intBlankLabelsLeftToPrint = intLabelStartPosition - 1;
				boolUseBookPage = modGlobalVariables.Statics.CustomizedInfo.Ref1BookPage == 2;
				boolShowSaleData = false;
				if (intTypePrint == 7)
				{
					if (MessageBox.Show("Do you want book/page and sale information to print?", "Print sale info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						boolShowSaleData = true;
					}
				}
				if (boolShowSaleData || boolUseBookPage)
				{
					clsBookPage.OpenRecordset("select * from bookpage where [current] = 1 order by account,line desc", modGlobalVariables.strREDatabase);
				}
				intNumberofDuplicates = intNumDuplicates;
				intDuplicateCount = intNumberofDuplicates;
				sngAdjustment = sngAdjust;
				if (sngAdjustment < -0.5)
					sngAdjustment = FCConvert.ToSingle(-0.5);
				if (sngAdjustment > 0.5)
					sngAdjustment = 0.5f;
				snglTax = sngTax;
				lngCurrRow = 0;
				if (strPrinterName != string.Empty)
					this.Document.Printer.PrinterName = strPrinterName;
				// 9    intTypeOfLabel = intTypePrint
				intTypeOfLabel = intLabelType;
				intLabelToPrint = intTypePrint;
				strDBName = modGlobalVariables.strREDatabase;
				switch (intOrdBy)
				{
					case 1:
						{
							// account
							strOrder = "RSACCOUNT";
							break;
						}
					case 2:
						{
							// map lot
							strOrder = "RSMAPLOT";
							break;
						}
					case 3:
						{
							// zip
							strOrder = "RSZIP";
							break;
						}
					case 4:
						{
							// name
							strOrder = "RSNAME";
							break;
						}
					case 5:
						{
							// location
							strOrder = "RSLOCSTREET";
							break;
						}
				}
				//end switch
				if ((bytWhatInclude & 512) > 0)
				{
					boolIncOwner = true;
				}
				else
				{
					boolIncOwner = false;
				}
				if ((bytWhatInclude & 256) > 0)
				{
					boolIncAcreage = true;
				}
				else
				{
					boolIncAcreage = false;
				}
				if ((bytWhatInclude & 128) > 0)
				{
					boolIncSecOwner = true;
				}
				else
				{
					boolIncSecOwner = false;
				}
				if ((bytWhatInclude & 64) > 0)
				{
					boolIncLoc = true;
				}
				else
				{
					boolIncLoc = false;
				}
				if ((bytWhatInclude & 32) > 0)
				{
					boolIncRef2 = true;
				}
				else
				{
					boolIncRef2 = false;
				}
				if ((bytWhatInclude & 16) > 0)
				{
					boolIncAcct = true;
				}
				else
				{
					boolIncAcct = false;
				}
				if ((bytWhatInclude & 8) > 0)
				{
					boolIncRef = true;
				}
				else
				{
					boolIncRef = false;
				}
				if ((bytWhatInclude & 4) > 0)
				{
					boolIncAddr = true;
				}
				else
				{
					boolIncAddr = false;
				}
				if ((bytWhatInclude & 2) > 0)
				{
					boolIncMap = true;
				}
				else
				{
					boolIncMap = false;
				}
				if ((bytWhatInclude & 1) > 0)
				{
					boolIncETax = true;
				}
				else
				{
					boolIncETax = false;
				}
				if (boolByAccount)
				{
					strCard = "rscard = 1 and ";
				}
				else
				{
					strCard = "";
				}
				if (boolElim)
				{
					strTempOrder = "rsname,rsaddr1,rsaccount";
				}
				else
				{
					strTempOrder = strOrder;
					if (intOrdBy == 5)
					{
						strTempOrder = " rslocstreet,rslocnumalph ";
					}
				}
				int lngUID;
				lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				switch (intHowToPrint)
				{
					case 1:
						{
							// all
							strSQL = strMasterJoin + " where " + strCard + " not rsdeleted = 1  order by " + strTempOrder;
							break;
						}
					case 2:
						{
							/*? 53 */// range
							if (intOrdBy == 1)
							{
								strSQL = strMasterJoin + " where " + strCard + " not isnull(rsdeleted,0) = 1 and rsaccount between " + strMinAccount + " and " + strMaxAccount + " order by " + strTempOrder;
							}
							else
							{
								strMaxAccount += "z";
								strSQL = strMasterJoin + " where " + strCard + " not isnull(rsdeleted,0) = 1 and " + strOrder + " between '" + strMinAccount + "' and '" + strMaxAccount + "' order by " + strTempOrder;
							}
							break;
						}
					case 3:
						{
							// individual
							strSQL = strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + " rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") order by " + strTempOrder;
							break;
						}
					case 4:
						{
							// from extract
							strSQL = strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ")) order by " + strTempOrder;
							break;
						}
				}
				//end switch
				rsData.OpenRecordset(strSQL, strDBName);
				while (!rsData.EndOfFile())
				{
					strRefBookPage = "";
					if (boolUseBookPage)
					{
						if (!(clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile()))
						{
							// If clsBookPage.FindFirstRecord("account", .Fields("rsaccount")) Then
							if (clsBookPage.FindFirst("account = " + rsData.Get_Fields_Int32("rsaccount")))
							{
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
								while (clsBookPage.FindNextRecord("account", rsData.Get_Fields_Int32("rsaccount")))
								{
									// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
									strRefBookPage += " B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
								}
							}
						}
					}
					else
					{
						strRefBookPage = rsData.Get_Fields_String("RSref1") + "";
					}
					this.Grid.AddItem(rsData.Get_Fields_Int32("rsaccount") + "\t" + rsData.Get_Fields_Int32("rscard") + "\t" + rsData.Get_Fields_String("rsname") + "\t" + rsData.Get_Fields_String("rssecowner") + "\t" + rsData.Get_Fields_String("rsmaplot") + "\t" + rsData.Get_Fields_String("rsaddr1") + "\t" + rsData.Get_Fields_String("rsaddr2") + "\t" + rsData.Get_Fields_String("rsaddr3") + "\t" + rsData.Get_Fields_String("rsstate") + "\t" + rsData.Get_Fields_String("rszip") + "\t" + rsData.Get_Fields_String("rszip4") + "\t" + rsData.Get_Fields_String("rslocnumalph") + "\t" + rsData.Get_Fields_String("rslocstreet") + "\t" + rsData.Get_Fields_String("rstelephone") + "\t" + rsData.Get_Fields_Int32("lastlandval") + "\t" + rsData.Get_Fields_Int32("lastbldgval") + "\t" + rsData.Get_Fields_Int32("rlexemption") + "\t" + strRefBookPage + "\t" + rsData.Get_Fields_String("rsref2") + "\t" + rsData.Get_Fields_DateTime("SaleDate") + "\t" + FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Int32("PISalePRice"))) + "\t" + FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Double("piacres"))) + "\t" + rsData.Get_Fields_String("Country"));
					rsData.MoveNext();
				}
				if (boolElim)
				{
					// must eliminate duplicates then reorder the grid
					strName = "";
					strAddr1 = "";
					x = 0;
					while (x < Grid.Rows)
					{
						if ((Strings.UCase(Strings.Trim(Grid.TextMatrix(x, COLNAME))) == Strings.UCase(Strings.Trim(strName))) && (Strings.UCase(Strings.Trim(Grid.TextMatrix(x, COLADDR1))) == Strings.UCase(Strings.Trim(strAddr1))))
						{
							Grid.RemoveItem(x);
						}
						else
						{
							strName = Grid.TextMatrix(x, COLNAME);
							strAddr1 = Grid.TextMatrix(x, COLADDR1);
							x += 1;
						}
					}
					switch (intOrdBy)
					{
						case 1:
							{
								// account
								Grid.Col = COLACCOUNT;
								break;
							}
						case 2:
							{
								// maplot
								Grid.Col = COLMAPLOT;
								break;
							}
						case 3:
							{
								// zip
								Grid.Col = COLZIP;
								break;
							}
						case 4:
							{
								// name
								Grid.Col = COLNAME;
								break;
							}
						case 5:
							{
								// location
								Grid.Col = COLLOCNUM;
								Grid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
								Grid.Col = COLLOCSTREET;
								break;
							}
					}
					//end switch
					Grid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
				}
				// make them choose the printer first if you have to use a custom form
				// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
				// If rsData.RecordCount <> 0 Then rsData.MoveFirst
				boolDifferentPageSize = false;
				switch (intLabelType)
				{
					case 0:
						{
							// 4013
							boolDifferentPageSize = true;
							this.PageSettings.Margins.Top = 0;
							this.PageSettings.Margins.Left = 0;
							this.PageSettings.Margins.Right = 0;
							//PageSettings.PaperSize = 255;
							PageSettings.PaperHeight = 1;
							PageSettings.PaperWidth = 3.5F;
							intCharactersWide = FCConvert.ToInt32(3.5F * intCharsPerInch - 2);
							PrintWidth = 3.5F - 1F;
							break;
						}
					case 1:
						{
							// 4014
							boolDifferentPageSize = true;
							this.PageSettings.Margins.Top = 0;
							this.PageSettings.Margins.Left = 0;
							this.PageSettings.Margins.Right = 0;
							//PageSettings.PaperSize = 255;
							PageSettings.PaperHeight = 1.5F;
							PageSettings.PaperWidth = 4;
							intCharactersWide = 4 * intCharsPerInch - 2;
							PrintWidth = 4F - 1F;
							break;
						}
					case 2:
						{
							// 4029   the 4013 but 2 across
							boolDifferentPageSize = true;
							this.PageSettings.Margins.Top = 0;
							this.PageSettings.Margins.Left = 0;
							this.PageSettings.Margins.Right = 0;
							//PageSettings.PaperSize = 255;
							PageSettings.PaperHeight = 1;
							PageSettings.PaperWidth = FCConvert.ToSingle(3.5 * 2 + 360 / 1440f);
							intCharactersWide = FCConvert.ToInt32(3.5F * intCharsPerInch - 2);
							Detail.ColumnCount = 2;
							Detail.ColumnSpacing = 0.125F;
							// 16th of an inch
							break;
						}
					case 3:
						{
							// Avery 5260 (3 X 10)  Height = 1" Width = 2.63"
							intMaxLines = 6;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
							this.PageSettings.Margins.Top += (sngAdjustment);
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
							this.PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
							// this.PageSettings.Margins.Bottom = 0.25
							PrintWidth = (8.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 2.625F;
							intCharactersWide = FCConvert.ToInt32(2.625F * intCharsPerInch - 2);
							Detail.Height = 1;
							Detail.ColumnCount = 3;
							Detail.ColumnSpacing = 0.125F;
							break;
						}
					case 4:
						{
							// Avery 5261 (2 X 10)  Height = 1" Width = 4"
							intMaxLines = 6;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
							this.PageSettings.Margins.Top += (sngAdjustment);
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
							this.PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
							// this.PageSettings.Margins.Bottom = 0.25 * 1440
							PrintWidth = (8.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = (4 * 1440);
							intCharactersWide = 4 * intCharsPerInch - 2;
							Detail.Height = 1;
							Detail.ColumnCount = 2;
							Detail.ColumnSpacing = 0.1875F;
							break;
						}
					case 5:
						{
							// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
							intMaxLines = 8;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.8125);
							this.PageSettings.Margins.Top += (sngAdjustment);
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
							this.PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
							PrintWidth = (8.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 4F;
							// this.PageSettings.Margins.Bottom = (0.5 * 1440) - (sngAdjustment * 1440)
							// If this.PageSettings.Margins.Bottom < 0.25 Then
							// this.PageSettings.Margins.Bottom = 0.25
							// End If
							intCharactersWide = FCConvert.ToInt32(4F * intCharsPerInch - 2);
							Detail.Height = 1.33F;
							Detail.ColumnCount = 2;
							Detail.ColumnSpacing = 0.1875F;
							break;
						}
					case 6:
						{
							// Avery 5163 (2 X 5)  Height = 2" Width = 4"
							intMaxLines = 12;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
							this.PageSettings.Margins.Top += (sngAdjustment);
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
							this.PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
							// this.PageSettings.Margins.Bottom = 0.25 * 1440
							PrintWidth = 8.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 4F;
							intCharactersWide = 4 * intCharsPerInch - 2;
							Detail.Height = 2;
							Detail.ColumnCount = 2;
							Detail.ColumnSpacing = 0.15625F;
							break;
						}
					case 7:
						{
							// assessment label
							intMaxLines = 2;
							boolDifferentPageSize = true;
							this.PageSettings.Margins.Top = 0;
							this.PageSettings.Margins.Left = 0;
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							//PageSettings.PaperSize = 255;
							PageSettings.PaperHeight = 0.5F;
							PageSettings.PaperWidth = 5;
							PrintWidth = 5F - 1F;
							intCharactersWide = 5 * intCharsPerInch - 2;
							break;
						}
					case 8:
						{
							// Avery 5026 (2 X 9)  Height = 1" Width = 3.5"
							intMaxLines = 6;
							// this.PageSettings.Margins.Top = (0.5625 * 1440) '9/16 in.
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.625);
							// 9/16 + extra 1/16
							this.PageSettings.Margins.Top += (sngAdjustment);
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.5);
							this.PageSettings.Margins.Right = FCConvert.ToSingle(0.5);
							this.PageSettings.Margins.Bottom = FCConvert.ToSingle(0.25);
							PrintWidth = (8.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 3.5F;
							intCharactersWide = FCConvert.ToInt32(3.5F * intCharsPerInch - 2);
							Detail.Height = 1.125F;
							// 1 and 1/8 in. since there is a 1/8 in gap between labels
							Detail.ColumnCount = 2;
							// Detail.ColumnSpacing = (1.4375 * 1440)
							Detail.ColumnSpacing = 720 / 1440F;
							break;
						}
					case 9:
						{
							// avery 5066,5266 (2 X 15) height = .66" Width = 3.5"
							intMaxLines = 4;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
							// .75 in.
							this.PageSettings.Margins.Top += (sngAdjustment);
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.8125);
							// 3/4 + 1/16
							this.PageSettings.Margins.Right = FCConvert.ToSingle(0.5);
							this.PageSettings.Margins.Bottom = FCConvert.ToSingle(0.25);
							PrintWidth = (8.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 3.5F;
							intCharactersWide = FCConvert.ToInt32(3.5F * intCharsPerInch - 2);
							Detail.Height = 0.66625F;
							// 2/3"
							Detail.ColumnCount = 2;
							Detail.ColumnSpacing = 1F;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30256:
						{
							if (!boolPrintDontShow)
							{
								//if (!this.Document.Printer.PrintDialog)
								//{
								//	this.Unload();
								//	return;
								//}
							}
							strPrinterToUse = this.Document.Printer.PrinterName;
							boolShowInViewer = false;
							intMaxLines = 12;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
                            //FC:FINAL:AM:#3310 - incorrect PrintWidth (ignored in VB6)
							//if (Strings.UCase(modGlobalConstants.Statics.MuniName) != "WEST BATH")
							//{
							//	PrintWidth = 2.31F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							//}
							//else
							{
								PrintWidth = 4 - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							}
							intLabelWidth = 4F;
							lngPrintWidth = 4F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intCharactersWide = 4 * intCharsPerInch - 2;
							Detail.Height = 2.3125F - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
							// 2 5/16"
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = 4;
							PageSettings.PaperWidth = 2.31F;
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
							//{
							//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
							//	{
							//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
							//		this.Toolbar.Tools(cnt).Enabled = true;
							//	}
							//} // cnt
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30252:
						{
							if (!boolPrintDontShow)
							{
								//if (!this.Document.Printer.PrintDialog)
								//{
								//	this.Unload();
								//	return;
								//}
							}
							strPrinterToUse = this.Document.Printer.PrinterName;
							boolShowInViewer = false;
							intMaxLines = 6;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
							if (Strings.UCase(modGlobalConstants.Statics.MuniName) != "WEST BATH")
							{
                                //FC:FINAL:MSH - issue #1653: in VB6 in landscape format sizes will be inverted
								//PrintWidth = (1.1F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
								PrintWidth = (3.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							}
							else
							{
                                PrintWidth = (3.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							}
							intLabelWidth = 3.5F;
							lngPrintWidth = 3.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intCharactersWide = FCConvert.ToInt32(3.5F * intCharsPerInch - 2);
							Detail.Height = 1.1F - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
							Detail.ColumnCount = 1;
                            //FC:FINAL:MSH - issue #1653: in VB6 in landscape format sizes will be inverted
                            //PageSettings.PaperHeight = 3.5F;
                            //PageSettings.PaperWidth = 1.1F;
                            PageSettings.PaperHeight = 1.1F;
                            PageSettings.PaperWidth = 3.5F;
                            Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
							//{
							//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
							//	{
							//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
							//		this.Toolbar.Tools(cnt).Enabled = true;
							//	}
							//} // cnt
							break;
						}
					case 10:
						{
							intMaxLines = 6;
							this.Document.Printer.DefaultPageSettings.Landscape = false;
							this.PageSettings.Margins.Top = 720 / 1440f;
							this.PageSettings.Margins.Left = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Bottom = 720 / 1440f;
							Detail.Height = 11 - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
							PrintWidth = (8.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 3F;
							intCharactersWide = 3 * 12 - 2;
							Detail.ColumnCount = 1;
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							// this is for dot matrix
							break;
						}
					case 11:
						{
							// hygrade certified mail form - laser
							strCMFNumber = strCMFBarCode;
							boolCMF = true;
							strIMPBNumber = strIMPBCode;
							intMaxLines = 6;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							if (sngAdjustment >= 0)
							{
								this.PageSettings.Margins.Top = sngAdjustment;
							}
							this.PageSettings.Margins.Left = 0;
							this.PageSettings.Margins.Right = 0;
							// PageSettings.PaperSize = 255
							// PageSettings.PaperHeight = 15840
							// PageSettings.PaperWidth = 12240
							PrintWidth = 11 - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 3F;
							intCharactersWide = 3 * 12 - 2;
							Detail.ColumnCount = 1;
							Detail.Height = 8.5F - 1 / 1440f;
							if (this.PageSettings.Margins.Top > 0)
							{
								Detail.Height -= this.PageSettings.Margins.Top;
							}
							lngCMFSessionID = GetCMFSessionID();
							strCompName = modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer();
							strCompName = Strings.Replace(strCompName, "'", "", 1, -1, CompareConstants.vbTextCompare);
							dtCMFStart = DateTime.Now;
							break;
						}
				}
				//end switch
				if (boolDifferentPageSize)
				{
					Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				if (intTypeOfLabel != 11 && intTypeOfLabel != modLabels.CNSTLBLTYPEDYMO30256 && intTypeOfLabel != modLabels.CNSTLBLTYPEDYMO30252)
				{
					PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
					this.Document.Printer.DefaultPageSettings.Landscape = false;
					lngPrintWidth = PrintWidth;
				}
				else
				{
					PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
					this.Document.Printer.DefaultPageSettings.Landscape = true;
				}
				// End If
				Detail.Controls.Clear();
				CreateDataFields();
				if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
				{
					intCharactersWide -= 1;
				}
				if (!boolPrintDontShow)
				{
					if (!boolShowInViewer)
					{
						//this.Show(App.MainForm);
						frmReportViewer.InstancePtr.Init(this);
					}
					else
					{
						frmReportViewer.InstancePtr.Init(this, strPrinterToUse);
					}
				}
				else
				{
					this.PrintReport(false);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in Init", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int GetCMFSessionID()
		{
			int GetCMFSessionID = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: lngID As int	OnWrite(short, double)
			int lngID;
            try
            {
                // On Error GoTo ErrorHandler
                lngID = 1;
                clsLoad.OpenRecordset("select top 1 sessionid from cmfnumbers order by sessionid desc",
                    modGlobalVariables.strREDatabase);
                if (!clsLoad.EndOfFile())
                {
                    if (Conversion.Val(clsLoad.Get_Fields_Int32("sessionid")) > 0)
                    {
                        lngID = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("sessionid")) + 1);
                    }
                }

                GetCMFSessionID = lngID;
                return GetCMFSessionID;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In GetCMFSessionID", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
            return GetCMFSessionID;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			int const_printtoolid;
			int cnt;
			try
			{
				// On Error GoTo ErrorHandler
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				// override the print button so the print dialog doesn't come up again
				// const_printtoolid = 9950
				// 3       For cnt = 0 To Me.Toolbar.Tools.Count - 1
				// If "Print..." = Me.Toolbar.Tools(cnt).Caption Then
				// Me.Toolbar.Tools(cnt).ID = const_printtoolid
				// Me.Toolbar.Tools(cnt).Enabled = True
				// End If
				// Next cnt
				if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
				{
					for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
					{
						switch (intTypeOfLabel)
						{
							case modLabels.CNSTLBLTYPEDYMO30252:
								{
									if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
									{
										this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
										break;
									}
									break;
								}
							case modLabels.CNSTLBLTYPEDYMO30256:
								{
									if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
									{
										this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
										break;
									}
									break;
								}
						}
						//end switch
					}
					// cnt
				}
				else
				{
                    //FC:FINAL:DSE:#1654 Printing will be done on the client side
					//if (modSysInfo.IsNTBased())
					//{
					//	//modCustomPageSize.CheckDefaultPrinter(this.Document.Printer.PrinterName);
					//	//intReturn = modCustomPageSize.SelectForm_74("CustomLabel", this.Handle.ToInt32(), FCConvert.ToInt32(PageSettings.PaperWidth * 25400 / 1440), FCConvert.ToInt32(PageSettings.PaperHeight * 25400 / 1440));
					//	if (intReturn > 0)
					//	{
					//		FCGlobal.Printer.PaperSize = FCConvert.ToInt16(intReturn;
					//	}
					//	else
					//	{
					//		MessageBox.Show("Error. Could not access form on printer.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					//		this.Close();
					//		return;
					//	}
					//}
					//else
					{
						FCGlobal.Printer.PaperSize = 255;
						FCGlobal.Printer.PaperHeight = PageSettings.PaperHeight;
						FCGlobal.Printer.PaperWidth = PageSettings.PaperWidth;
					}
				}
				boolFirstLabel = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in ReportStart", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField = null;
			int intRow;
			int intCol;
			int intNumber = 0;
			try
			{
				// On Error GoTo ErrorHandler
				// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
				// this will be module specific
				// cases 0,1,2,5 could be used generically
				// they have 6 lines of data each
				// other cases uses textboxes placed where the data appears
				// Select Case intTypeOfLabel
				switch (intLabelToPrint)
				{
					case 0:
					case 1:
					case 2:
					case 5:
					case 7:
						{
							// these cases are 6 lines of data
							if (!(intTypeOfLabel == 11) && !(intTypeOfLabel == 10))
							{
								if (intLabelToPrint == 5 || intMaxLines < 6)
								{
									intNumber = intMaxLines;
								}
								else
								{
									intNumber = 6;
								}
								for (intRow = 1; intRow <= intNumber; intRow++)
								{
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtData" + FCConvert.ToString(intRow);
									NewField.Left = 120 / 1440f;
									// one space
									NewField.Width = ((lngPrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 121 / 1440f;
									intCharactersWide = FCConvert.ToInt32(Conversion.Int(FCConvert.ToDouble(NewField.Width) * intCharsPerInch));
									if (!boolUseEightPoint)
									{
										NewField.Top = (intRow - 1) * 225 / 1440f;
										NewField.Height = 225 / 1440f;
										NewField.Font = new Font(NewField.Font.Name, 10);
									}
									else
									{
										NewField.Top = (intRow) * 200 / 1440f - 100 / 1440f;
										// dont start at 0
										NewField.Height = 200 / 1440f;
										NewField.Font = new Font(NewField.Font.Name, 8);
									}
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									Detail.Controls.Add(NewField);
								}
							}
							else
							{
								// certified mail - laser
								// vbPorter upgrade warning: lngAdj As int	OnWrite(short, float)
								int lngAdj = 0;
								lngAdj = 0;
								if (sngAdjustment < 0)
								{
									lngAdj = FCConvert.ToInt32(sngAdjustment * 1440);
								}
								for (intRow = 1; intRow <= 6; intRow++)
								{
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (intTypeOfLabel == 11)
									{
										NewField.Top = (((intRow - 1) * 225 / 1440f) + 2132 / 1440f) + lngAdj;
										// + (gdblCMFAdjustment * lngLineHeight)
										NewField.Left = 4200 / 1440f;
										NewField.Font = new Font(NewField.Font.Name, 10);
									}
									else
									{
										NewField.Top = (((intRow - 1) * 225 / 1440f) + 3100 / 1440f);
										// + (gdblCMFAdjustment * lngLineHeight)
										NewField.Left = 3600 / 1440f;
										NewField.Font = new Font(NewField.Font.Name, 10);
									}
									NewField.Width = 3;
									NewField.Name = "txtData" + FCConvert.ToString(intRow);
									Detail.Controls.Add(NewField);
								}
								// intRow
								for (intRow = 7; intRow <= 12; intRow++)
								{
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (intTypeOfLabel == 11)
									{
										NewField.Left = 5000 / 1440f;
										NewField.Top = (((intRow - 1) * 225 / 1440f) + 6300 / 1440f) + lngAdj;
										// + (gdblCMFAdjustment * lngLineHeight)
										NewField.Font = new Font(NewField.Font.Name, 10);
									}
									else
									{
										NewField.Font = new Font(NewField.Font.Name, 10);
										NewField.Top = (((intRow - 1) * 225 / 1440f) + 9200 / 1440f);
										// + (gdblCMFAdjustment * lngLineHeight)
										NewField.Left = 5000 / 1440f;
									}
									NewField.Width = 4;
									NewField.Name = "txtData" + FCConvert.ToString(intRow);
									Detail.Controls.Add(NewField);
								}
								// intRow
							}
							break;
						}
					case 3:
						{
							// change of assessment
							// this case is for two labels per account
							// also there are textboxes for individual data, not just
							// 1 textbox for each line
							for (intRow = 1; intRow <= 15; intRow++)
							{
								NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
								NewField.CanGrow = false;
								if (!boolUseEightPoint)
								{
									NewField.Height = 225 / 1440f;
								}
								else
								{
									NewField.Height = 200 / 1440f;
									NewField.Font = new Font(NewField.Font.Name, 8);
								}
								NewField.Text = string.Empty;
								NewField.MultiLine = false;
								NewField.WordWrap = true;
								if (intRow >= 1 && intRow <= 5)
								{
									// 5 label lines
									NewField.Name = "txtData" + FCConvert.ToString(intRow);
									if (!boolUseEightPoint)
									{
										NewField.Top = (intRow - 1) * 225 / 1440f;
									}
									else
									{
										NewField.Top = (intRow) * 200 / 1440f - 100 / 1440f;
									}
									NewField.Left = 144 / 1440f;
									// one space
									NewField.Width = ((lngPrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
									/*? 24
																   25 */
								}
								else if (intRow >= 6 && intRow <= 10)
								{
									// 5 labels for textboxes
									if (!boolUseEightPoint)
									{
										NewField.Top = (intRow - 6) * 225 / 1440f;
									}
									else
									{
										NewField.Top = (intRow - 5) * 200 / 1440f - 100 / 1440f;
									}
									NewField.Width = 810 / 1440f;
									NewField.Left = 3300 / 1440f;
									NewField.Visible = false;
									NewField.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
									switch (intRow)
									{
										case 6:
											{
												NewField.Text = "Land";
												NewField.Name = "lblLand";
												break;
											}
										case 7:
											{
												NewField.Text = "Bldg";
												NewField.Name = "lblBldg";
												break;
											}
										case 8:
											{
												NewField.Text = "Exempt";
												NewField.Name = "lblExempt";
												break;
											}
										case 9:
											{
												NewField.Text = "Total";
												NewField.Name = "lblTotal";
												break;
											}
										case 10:
											{
												NewField.Text = "Estimated Tax:";
												NewField.Width = 1800 / 1440f;
												NewField.Left = 2300 / 1440f;
												NewField.Name = "lblTax";
												break;
											}
									}
									//end switch
								}
								else if (intRow >= 11 && intRow <= 15)
								{
									// 5 textboxes
									NewField.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
									if (!boolUseEightPoint)
									{
										NewField.Top = (intRow - 11) * 225 / 1440f;
									}
									else
									{
										NewField.Top = (intRow - 10) * 200 / 1440f - 100 / 1440f;
									}
									NewField.Left = 4140 / 1440f;
									NewField.Width = 1530 / 1440f;
									NewField.OutputFormat = "#,###,##0";
									NewField.Visible = false;
									switch (intRow)
									{
										case 11:
											{
												NewField.Name = "txtLand";
												break;
											}
										case 12:
											{
												NewField.Name = "txtBldg";
												break;
											}
										case 13:
											{
												NewField.Name = "txtExempt";
												break;
											}
										case 14:
											{
												NewField.Name = "txtTotal";
												break;
											}
										case 15:
											{
												NewField.Name = "txtTax";
												NewField.OutputFormat = "#,##0.00";
												break;
											}
									}
									//end switch
								}
                                Detail.Controls.Add(NewField);
                            }
							// intRow
							break;
						}
					case 4:
						{
							// assessment
							// this label also has textboxes in specific places
							for (intRow = 1; intRow <= 6; intRow++)
							{
								NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
								NewField.CanGrow = false;
								NewField.Height = 225 / 1440f;
								if (boolUseEightPoint)
								{
									NewField.Font = new Font(NewField.Font.Name, 8);
								}
								NewField.Text = string.Empty;
								NewField.MultiLine = false;
								NewField.WordWrap = true;
								switch (intRow)
								{
									case 1:
										{
											NewField.Name = "txtYear";
											NewField.Top = 0;
											NewField.Left = 90 / 1440f;
											NewField.Width = 810 / 1440f;
											break;
										}
									case 2:
										{
											NewField.Name = "txtLand";
											NewField.Top = 0;
											NewField.Left = 900 / 1440f;
											NewField.Width = 1800 / 1440f;
											break;
										}
									case 3:
										{
											NewField.Name = "txtBldg";
											NewField.Top = 0;
											NewField.Left = 2700 / 1440f;
											NewField.Width = 1620 / 1440f;
											break;
										}
									case 4:
										{
											NewField.Name = "txtExempt";
											NewField.Top = 0;
											NewField.Left = 4320 / 1440f;
											NewField.Width = 1260 / 1440f;
											break;
										}
									case 5:
										{
											NewField.Name = "txtTotal";
											NewField.Top = 0;
											NewField.Left = 5580 / 1440f;
											NewField.Width = 1620 / 1440f;
											break;
										}
									case 6:
										{
											// line 2
											NewField.Name = "txtData2";
											NewField.Top = 225 / 1440f;
											NewField.Left = 90 / 1440f;
											NewField.Width = ((lngPrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
											break;
										}
								}
                                //end switch
                                Detail.Controls.Add(NewField);
                            }
							// intRow
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in CreateDataFields.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
            {
                if (boolCMF)
                {
                    fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                    rptCMFList.InstancePtr.Init(FCConvert.ToString(lngCMFSessionID) + ";" + strCompName);
                }

            }
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number" + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Terminate", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
		private void Detail_Format(object sender, EventArgs e)
		{
			int intControl;
			int intRow;
			try
			{
				// On Error GoTo ErrorHandler
				if (intBlankLabelsLeftToPrint <= 0)
				{
					if (lngCurrRow < Grid.Rows)
					{
						Grid.Row = lngCurrRow;
						// 3        Select Case intTypeOfLabel
						switch (intLabelToPrint)
						{
						// the functions called are all module specific functions
							case 1:
								{
									// mailing
									MakeMailing();
									break;
								}
							case 2:
								{
									// address card
									MakeAddressCard();
									break;
								}
							case 3:
								{
									// change assessment
									MakeChangeAssessment();
									break;
								}
							case 4:
								{
									// assessment
									makeassessmentcard();
									break;
								}
							case 5:
								{
									// custom
									MakeCustom();
									break;
								}
							case 7:
								{
									MakeCardLabel();
									break;
								}
						}
						//end switch
						if (intDuplicateCount == 0)
						{
							if ((boolFirstLabel) || intLabelToPrint != 3)
							{
								lngCurrRow += 1;
								intDuplicateCount = intNumberofDuplicates;
							}
						}
						else
						{
							intDuplicateCount -= 1;
							if (intDuplicateCount < 0)
								intDuplicateCount = 0;
						}
					}
				}
				else
				{
					// print blanks
					intBlankLabelsLeftToPrint -= 1;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in Detail Format");
			}
		}

		private void MakeAddressCard()
		{
			// vbPorter upgrade warning: strTemp As string	OnWriteFCConvert.ToInt32(
			string strTemp = "";
			string strMLot1 = "";
			string strMLot2 = "";
			string strMLot3 = "";
			string strMLot4 = "";
			string strML = "";
			string[] strL = new string[6 + 1];
			int intCurLine = 0;
			// vbPorter upgrade warning: lngAct As int	OnRead(string)
			int lngAct = 0;
			string strCountry = "";
			try
			{
				// On Error GoTo ErrorHandler
				// Module specific
				strLine1 = Strings.StrDup(intCharactersWide, " ");
				strLine2 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				lngAct = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, COLACCOUNT))));
				strCountry = Strings.Trim(Grid.TextMatrix(Grid.Row, COLCOUNTRY));
				if (Strings.LCase(strCountry) == "united states")
				{
					strCountry = "";
				}
				// 7            Mid(strLine1, 1) = Left(" " & .TextMatrix(.Row, COLNAME), intCharactersWide - Len(.TextMatrix(.Row, COLACCOUNT)) - 2)
				strLine1 = Strings.Left(" " + Grid.TextMatrix(Grid.Row, COLNAME) + Strings.StrDup(intCharactersWide, " "), intCharactersWide);
				// 8            Mid(strLine1, intCharactersWide - 6, 6) = Format(.TextMatrix(.Row, COLACCOUNT), "@@@@@@")
				if (!modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					fecherFoundation.Strings.MidSet(ref strLine1, intCharactersWide - Strings.Len(Grid.TextMatrix(Grid.Row, COLACCOUNT)) + 1, Strings.Len(Grid.TextMatrix(Grid.Row, COLACCOUNT)), Grid.TextMatrix(Grid.Row, COLACCOUNT));
				}
				if (Strings.Trim(Grid.TextMatrix(Grid.Row, COLSECOWNER)) != string.Empty)
				{
					strLine2 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLSECOWNER)), 1, intCharactersWide);
					if (Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR1)) != string.Empty)
					{
						strLine3 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR1)), 1, intCharactersWide);
						strLine4 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLCITY)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLSTATE)) + "  " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP4)), 1, intCharactersWide);
						if (Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR2)) != string.Empty)
						{
							// If Not CustomizedInfo.boolPrintMapLotOnSide Then
							// 15                        strLine2 = strLine3
							// 16                        strLine3 = Mid(" " & Trim(.TextMatrix(.Row, COLADDR2)), 1, intCharactersWide)
							// Else
							strLine5 = strLine4;
							strLine4 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR2)), 1, intCharactersWide);
							// End If
						}
					}
					else
					{
						strLine3 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR2)), 1, intCharactersWide);
						strLine4 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLCITY)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLSTATE)) + "  " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP4)), 1, intCharactersWide);
					}
					if (strCountry != "")
					{
						if (Strings.Trim(strLine5) == "")
						{
							strLine5 = " " + strCountry;
						}
						else
						{
							strLine6 = " " + strCountry;
						}
					}
				}
				else
				{
					strLine2 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR1)), 1, intCharactersWide);
					if (Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR2)) != string.Empty)
					{
						strLine3 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLADDR2)), 1, intCharactersWide);
						strLine4 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLCITY)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLSTATE)) + "  " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP4)), 1, intCharactersWide);
						if (strCountry != "")
						{
							strLine5 = " " + strCountry;
						}
					}
					else
					{
						strLine3 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLCITY)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLSTATE)) + "  " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP)) + " " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLZIP4)), 1, intCharactersWide);
						if (strCountry != "")
						{
							strLine4 = " " + strCountry;
						}
					}
				}
				if (strLine4 == string.Empty)
				{
					strLine4 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLREF1)), 1, intCharactersWide);
					if (!modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						strLine5 = " MapLot: " + Strings.Mid(Strings.Trim(Grid.TextMatrix(Grid.Row, COLMAPLOT)), 1, 30);
					}
				}
				else if (strLine5 == string.Empty)
				{
					strLine5 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLREF1)), 1, intCharactersWide);
					if (!modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						strLine6 = " MapLot: " + Strings.Mid(Strings.Trim(Grid.TextMatrix(Grid.Row, COLMAPLOT)), 1, 30);
					}
				}
				else
				{
					if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						strLine6 = Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(Grid.Row, COLREF1)), 1, intCharactersWide);
					}
					else
					{
						strLine6 = " MapLot: " + Strings.Mid(Strings.Trim(Grid.TextMatrix(Grid.Row, COLMAPLOT)), 1, 30);
					}
				}
				if (Strings.Trim(strLine5) == string.Empty)
				{
					strLine5 = strLine6;
					strLine6 = "";
				}
				if (Strings.Trim(strLine4) == string.Empty)
				{
					strLine4 = strLine5;
					strLine5 = strLine6;
					strLine6 = "";
				}
				if (Strings.Trim(strLine3) == string.Empty)
				{
					strLine3 = strLine4;
					strLine4 = strLine5;
					strLine5 = strLine6;
					strLine6 = "";
				}
				if (Strings.Trim(strLine2) == string.Empty)
				{
					strLine2 = strLine3;
					strLine3 = strLine4;
					strLine4 = strLine5;
					strLine5 = strLine6;
					strLine6 = "";
				}
				if (Strings.Trim(strLine1) == string.Empty)
				{
					strLine1 = strLine2;
					strLine2 = strLine3;
					strLine3 = strLine4;
					strLine4 = strLine5;
					strLine5 = strLine6;
					strLine6 = "";
				}
				strL[1] = strLine1;
				strL[2] = strLine2;
				strL[3] = strLine3;
				strL[4] = strLine4;
				strL[5] = strLine5;
				strL[6] = strLine6;
				if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
				{
					if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
					{
						intCurLine = 1;
					}
					else if (Strings.Trim(strMLot4) != string.Empty)
					{
						intCurLine = 2;
					}
					else
					{
						intCurLine = 3;
					}
					strML = Strings.Trim(Grid.TextMatrix(Grid.Row, COLMAPLOT));
					strMLot1 = "";
					strMLot2 = "";
					strMLot3 = "";
					strMLot4 = "";
					ParseMapLot(strML + "", ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
					if (strL[1].Length < intCharactersWide)
					{
						strL[1] += Strings.StrDup(intCharactersWide - strL[1].Length, " ");
					}
					if (strL[2].Length < intCharactersWide)
					{
						strL[2] += Strings.StrDup(intCharactersWide - strL[2].Length, " ");
					}
					if (strL[3].Length < intCharactersWide)
					{
						strL[3] += Strings.StrDup(intCharactersWide - strL[3].Length, " ");
					}
					if (strL[4].Length < intCharactersWide)
					{
						strL[4] += Strings.StrDup(intCharactersWide - strL[4].Length, " ");
					}
					if (strL[5].Length < intCharactersWide)
					{
						strL[5] += Strings.StrDup(intCharactersWide - strL[5].Length, " ");
					}
					if (strL[6].Length < intCharactersWide)
					{
						strL[6] += Strings.StrDup(intCharactersWide - strL[6].Length, " ");
					}
					if (strMLot1 != string.Empty)
					{
						fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot1), Strings.Len(strMLot1) + 1, " " + strMLot1);
						intCurLine += 1;
					}
					if (strMLot2 != string.Empty)
					{
						fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot2), Strings.Len(strMLot2) + 1, " " + strMLot2);
						intCurLine += 1;
					}
					if (strMLot3 != string.Empty)
					{
						if ((strL[intCurLine].Length - strMLot3.Length - 1) > 0)
						{
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot3), Strings.Len(strMLot3) + 1, " " + strMLot3);
						}
						else
						{
							strL[intCurLine] += Strings.StrDup(intCharactersWide - strL[intCurLine].Length, " ");
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], intCharactersWide - Strings.Len(strMLot3), Strings.Len(strMLot3), strMLot3);
						}
						intCurLine += 1;
					}
					if (strMLot4 != string.Empty)
					{
						if ((strL[intCurLine].Length - strMLot4.Length - 1) > 0)
						{
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot4), Strings.Len(strMLot4) + 1, " " + strMLot4);
						}
						else
						{
							strL[intCurLine] += Strings.StrDup(intCharactersWide - strL[intCurLine].Length, " ");
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], intCharactersWide - Strings.Len(strMLot4), Strings.Len(strMLot4), strMLot4);
						}
					}
				}
				if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					strTemp = FCConvert.ToString(lngAct);
					modProperty.Statics.intTemp = strTemp.Length;
					strL[5] = Strings.Left(strL[5] + Strings.StrDup(intCharactersWide, " "), intCharactersWide);
					fecherFoundation.Strings.MidSet(ref strL[5], intCharactersWide - modProperty.Statics.intTemp - 1, " " + strTemp);
				}
				strLine1 = Strings.RTrim(strL[1]);
				strLine2 = Strings.RTrim(strL[2]);
				strLine3 = Strings.RTrim(strL[3]);
				strLine4 = Strings.RTrim(strL[4]);
				strLine5 = Strings.RTrim(strL[5]);
				strLine6 = Strings.RTrim(strL[6]);
				(this.Detail.Controls["txtData1"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine1;
				(this.Detail.Controls["txtData2"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
				if (intMaxLines > 2)
				{
					(this.Detail.Controls["txtData3"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine3;
					if (intMaxLines > 3)
					{
						(this.Detail.Controls["txtData4"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine4;
						if (intMaxLines > 4)
						{
							(this.Detail.Controls["txtData5"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine5;
							if (intMaxLines > 5)
							{
								(this.Detail.Controls["txtData6"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine6;
							}
						}
					}
				}
				if (intTypeOfLabel == 11)
				{
					// certified mail - laser
					(this.Detail.Controls["txtData7"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine1;
					(this.Detail.Controls["txtData8"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
					(this.Detail.Controls["txtData9"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine3;
					(this.Detail.Controls["txtData10"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine4;
					(this.Detail.Controls["txtData11"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine5;
					(this.Detail.Controls["txtData12"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine6;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeAddressCard");
			}
		}

		private void MakeMailing()
		{
			var clsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                // module specific function
                // vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
                string strTemp = "";

                string strNam;
                string strA1;
                string strA2 = "";
                string strA3 = "";
                string strSecOwn = "";
                string strZip = "";
                string strZip4 = "";
                string strST = "";
                string strML = "";
                // vbPorter upgrade warning: lngAct As int	OnRead(string)
                int lngAct = 0;
                string strMLot1 = "";
                string strMLot2 = "";
                string strMLot3 = "";
                string strMLot4 = "";
                string strSQL = "";
                string[] strL = new string[6 + 1];
                int intCurLine = 0;
                string strCountry = "";
                strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
                strA1 = Grid.TextMatrix(lngCurrRow, COLADDR1);
                // 3    Call escapequote(strNam)
                // 4    Call escapequote(strA1)
                strA2 = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLADDR2));
                strA3 = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLCITY));
                if (boolIncSecOwner)
                {
                    strSecOwn = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLSECOWNER));
                }
                else
                {
                    strSecOwn = "";
                }

                strZip = Grid.TextMatrix(lngCurrRow, COLZIP);
                strZip4 = Grid.TextMatrix(lngCurrRow, COLZIP4);
                strST = Grid.TextMatrix(lngCurrRow, COLSTATE);
                strML = Grid.TextMatrix(lngCurrRow, COLMAPLOT);
                lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
                strCountry = Grid.TextMatrix(lngCurrRow, COLCOUNTRY);
                if (Strings.LCase(strCountry) == "united states")
                {
                    strCountry = "";
                }

                if (boolCMF)
                {
                    if (strCMFNumber == "-1")
                    {
                        MessageBox.Show(
                            "There was an error generating the next certified mail number for account " +
                            FCConvert.ToString(lngAct) + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK,
                            MessageBoxIcon.Hand);
                        lngCurrRow = Grid.Rows - 1;
                        (this.Detail.Controls["txtData1"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData2"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData3"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData4"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData5"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData6"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData7"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData8"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData9"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData10"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData11"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        (this.Detail.Controls["txtData12"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            "";
                        return;
                    }

                    strSQL =
                        "Insert into cmfnumbers (SessionID,ComputerName,barcode,name,account,CMFDateTime,IMPBTrackingNumber) values (";
                    strSQL += FCConvert.ToString(lngCMFSessionID);
                    strSQL += ",'" + strCompName + "'";
                    strSQL += ",'" + strCMFNumber + "'";
                    strSQL += ",'" + modGlobalFunctions.EscapeQuotes(strNam) + "'";
                    strSQL += "," + FCConvert.ToString(lngAct);
                    strSQL += ",'" + FCConvert.ToString(dtCMFStart) + "'";
                    strSQL += ",'" + strIMPBNumber + "'";
                    strSQL += ")";
                    clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
                    strTemp = modGlobalFunctions.GetNextMailFormCode(ref strCMFNumber);
                    strCMFNumber = strTemp;
                    strTemp = modGlobalFunctions.GetNextMailFormCode_6(strIMPBNumber, true);
                    if (strTemp == "-1")
                    {
                        strIMPBNumber = "";
                    }
                    else
                    {
                        strIMPBNumber = strTemp;
                    }
                }

                strTemp = Strings.StrDup(38, " ");
                strLine2 = Strings.StrDup(intCharactersWide, " ");
                strLine1 = "";
                strLine3 = "";
                strLine4 = "";
                strLine5 = "";
                strLine6 = "";
                // 20        Mid(strLine2, 1) = Left(" " & strNam & "", intCharactersWide - 8)
                strLine2 = Strings.Left(" " + strNam + Strings.StrDup(intCharactersWide, " "), intCharactersWide);
                if (!boolCMF && !modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom &&
                    !modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount)
                {
                    fecherFoundation.Strings.MidSet(ref strLine2, intCharactersWide - 5, 6,
                        Strings.Format(FCConvert.ToString(lngAct) + "", "@@@@@@"));
                }

                if (Strings.Trim(strSecOwn + "") != string.Empty && (strCountry == "" || strA2 == ""))
                {
                    strLine3 = Strings.Mid(" " + Strings.Trim(strSecOwn), 1, intCharactersWide);
                    strLine4 = Strings.Mid(" " + Strings.Trim(strA1 + ""), 1, intCharactersWide);
                    if (Strings.Trim(strA2 + "") != string.Empty)
                    {
                        strLine5 = Strings.Mid(" " + Strings.Trim(strA2), 1, intCharactersWide);
                        strLine6 = Strings.Mid(
                            " " + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " +
                            Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharactersWide);
                    }
                    else
                    {
                        strLine5 = Strings.Mid(
                            " " + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " +
                            Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharactersWide);
                        if (strCountry != "")
                        {
                            strLine6 = " " + strCountry;
                        }
                    }
                }
                else
                {
                    strLine3 = Strings.Mid(" " + Strings.Trim(strA1 + ""), 1, intCharactersWide);
                    if (Strings.Trim(strA2 + "") != string.Empty)
                    {
                        strLine4 = Strings.Mid(" " + Strings.Trim(strA2), 1, intCharactersWide);
                        strLine5 = Strings.Mid(
                            " " + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " +
                            Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharactersWide);
                        if (strCountry != "")
                        {
                            strLine6 = " " + strCountry;
                        }
                    }
                    else
                    {
                        strLine4 = Strings.Mid(
                            " " + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " +
                            Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharactersWide);
                        if (strCountry != "")
                        {
                            strLine5 = " " + strCountry;
                        }
                    }
                }

                if (boolIncMap)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
                    {
                        strLine1 = " MapLot: " + Strings.Mid(Strings.Trim(strML + ""), 1, 30);
                    }
                    else
                    {
                        // If boolDotMatrix Then
                        // If strline6 <> vbNullString Then
                        // strLine1 = strLine2
                        // strLine2 = strLine3
                        // strLine3 = strLine4
                        // strLine4 = strLine5
                        // strLine5 = strLine6
                        // strLine6 = vbNullString
                        // 
                        // 
                        // End If
                        // Mid(strTemp, 1, 32) = Left(strLine2 & "", 32)
                        // Mid(strTemp, 34, 4) = strMLot1 & ""
                        // strLine2 = strTemp
                        // strTemp = String(38, " ")
                        // Mid(strTemp, 1, 32) = Left(strLine3 & "", 32)
                        // Mid(strTemp, 34, 4) = strMLot2 & ""
                        // strLine3 = strTemp
                        // strTemp = String(38, " ")
                        // Mid(strTemp, 1, 32) = Left(strLine4 & "", 32)
                        // Mid(strTemp, 34, 4) = strMLot3 & ""
                        // strLine4 = strTemp
                        // strTemp = String(38, " ")
                        // Mid(strTemp, 1, 32) = Left(strLine5 & "", 32)
                        // Mid(strTemp, 34, 4) = strMLot4 & ""
                        // strLine5 = strTemp
                    }
                }
                else
                {
                    strLine1 = strLine2;
                    strLine2 = strLine3;
                    strLine3 = strLine4;
                    strLine4 = strLine5;
                    strLine5 = strLine6;
                    strLine6 = "";
                }

                if (Strings.Trim(strLine5) == string.Empty)
                {
                    strLine5 = strLine6;
                    strLine6 = "";
                }

                if (Strings.Trim(strLine4) == string.Empty)
                {
                    strLine4 = strLine5;
                    strLine5 = strLine6;
                    strLine6 = "";
                }

                if (Strings.Trim(strLine3) == string.Empty)
                {
                    strLine3 = strLine4;
                    strLine4 = strLine5;
                    strLine5 = strLine6;
                    strLine6 = "";
                }

                if (Strings.Trim(strLine2) == string.Empty)
                {
                    strLine2 = strLine3;
                    strLine3 = strLine4;
                    strLine4 = strLine5;
                    strLine5 = strLine6;
                    strLine6 = "";
                }

                if (Strings.Trim(strLine1) == string.Empty)
                {
                    strLine1 = strLine2;
                    strLine2 = strLine3;
                    strLine3 = strLine4;
                    strLine4 = strLine5;
                    strLine5 = strLine6;
                    strLine6 = "";
                }

                strL[1] = strLine1;
                strL[2] = strLine2;
                if (Information.UBound(strL, 1) >= 3)
                {
                    strL[3] = strLine3;
                    if (Information.UBound(strL, 1) >= 4)
                    {
                        strL[4] = strLine4;
                        if (Information.UBound(strL, 1) >= 5)
                        {
                            strL[5] = strLine5;
                            if (Information.UBound(strL, 1) >= 6)
                            {
                                strL[6] = strLine6;
                            }
                        }
                    }
                }

                if (boolIncMap && modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
                {
                    if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
                    {
                        intCurLine = 1;
                    }
                    else if (Strings.Trim(strMLot4) != string.Empty)
                    {
                        intCurLine = 2;
                    }
                    else
                    {
                        intCurLine = 3;
                    }

                    strMLot1 = "";
                    strMLot2 = "";
                    strMLot3 = "";
                    strMLot4 = "";
                    ParseMapLot(strML + "", ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
                    if (strL[2].Length < intCharactersWide)
                    {
                        strL[2] += Strings.StrDup(intCharactersWide - strL[2].Length, " ");
                    }

                    if (Information.UBound(strL, 1) >= 3)
                    {
                        if (strL[3].Length < intCharactersWide)
                        {
                            strL[3] += Strings.StrDup(intCharactersWide - strL[3].Length, " ");
                        }

                        if (Information.UBound(strL, 1) >= 4)
                        {
                            if (strL[4].Length < intCharactersWide)
                            {
                                strL[4] += Strings.StrDup(intCharactersWide - strL[4].Length, " ");
                            }

                            if (Information.UBound(strL, 1) >= 5)
                            {
                                if (strL[5].Length < intCharactersWide)
                                {
                                    strL[5] += Strings.StrDup(intCharactersWide - strL[5].Length, " ");
                                }

                                if (Information.UBound(strL, 1) >= 6)
                                {
                                    if (strL[6].Length < intCharactersWide)
                                    {
                                        strL[6] += Strings.StrDup(intCharactersWide - strL[6].Length, " ");
                                    }
                                }
                            }
                        }
                    }

                    if (strMLot1 != string.Empty)
                    {
                        fecherFoundation.Strings.MidSet(ref strL[intCurLine],
                            Strings.Len(strL[intCurLine]) - Strings.Len(strMLot1) - 1, Strings.Len(strMLot1) + 1,
                            " " + strMLot1);
                        intCurLine += 1;
                    }

                    if (intCurLine <= Information.UBound(strL, 1))
                    {
                        if (strMLot2 != string.Empty)
                        {
                            fecherFoundation.Strings.MidSet(ref strL[intCurLine],
                                Strings.Len(strL[intCurLine]) - Strings.Len(strMLot2) - 1, Strings.Len(strMLot2) + 1,
                                " " + strMLot2);
                            intCurLine += 1;
                        }
                    }

                    if (intCurLine <= Information.UBound(strL, 1))
                    {
                        if (strMLot3 != string.Empty)
                        {
                            fecherFoundation.Strings.MidSet(ref strL[intCurLine],
                                Strings.Len(strL[intCurLine]) - Strings.Len(strMLot3) - 1, Strings.Len(strMLot3) + 1,
                                " " + strMLot3);
                            intCurLine += 1;
                        }
                    }

                    if (intCurLine <= Information.UBound(strL, 1))
                    {
                        if (strMLot4 != string.Empty)
                        {
                            fecherFoundation.Strings.MidSet(ref strL[intCurLine],
                                Strings.Len(strL[intCurLine]) - Strings.Len(strMLot4) - 1, Strings.Len(strMLot4) + 1,
                                " " + strMLot4);
                        }
                    }
                }

                if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom &&
                    !modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount && Information.UBound(strL, 1) >= 5)
                {
                    strTemp = FCConvert.ToString(lngAct);
                    modProperty.Statics.intTemp = strTemp.Length;
                    strL[5] = Strings.Left(strL[5] + Strings.StrDup(intCharactersWide, " "), intCharactersWide);
                    fecherFoundation.Strings.MidSet(ref strL[5], intCharactersWide - modProperty.Statics.intTemp,
                        " " + strTemp);
                }

                strLine1 = Strings.RTrim(strL[1]);
                strLine2 = Strings.RTrim(strL[2]);
                if (Information.UBound(strL, 1) >= 3)
                {
                    strLine3 = Strings.RTrim(strL[3]);
                    if (Information.UBound(strL, 1) >= 4)
                    {
                        strLine4 = Strings.RTrim(strL[4]);
                        if (Information.UBound(strL, 1) >= 5)
                        {
                            strLine5 = Strings.RTrim(strL[5]);
                            if (Information.UBound(strL, 1) >= 6)
                            {
                                strLine6 = Strings.RTrim(strL[6]);
                            }
                        }
                    }
                }

                (this.Detail.Controls["txtData1"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                    strLine1;
                (this.Detail.Controls["txtData2"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                    strLine2;
                if (intMaxLines > 2)
                {
                    (this.Detail.Controls["txtData3"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine3;
                    if (intMaxLines > 3)
                    {
                        (this.Detail.Controls["txtData4"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            strLine4;
                        if (intMaxLines > 4)
                        {
                            (this.Detail.Controls["txtData5"] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                .Text = strLine5;
                            if (intMaxLines > 5)
                            {
                                (this.Detail.Controls["txtData6"] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                    .Text = strLine6;
                            }
                        }
                    }
                }

                if (intTypeOfLabel == 11 || intTypeOfLabel == 10)
                {
                    // certified mail - laser
                    (this.Detail.Controls["txtData7"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine1;
                    (this.Detail.Controls["txtData8"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine2;
                    (this.Detail.Controls["txtData9"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine3;
                    (this.Detail.Controls["txtData10"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine4;
                    (this.Detail.Controls["txtData11"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine5;
                    (this.Detail.Controls["txtData12"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strLine6;
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeMailing",
                    "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				clsTemp.Dispose();
            }
		}

		private void makeassessmentcard()
		{
			// vbPorter upgrade warning: tValue As int	OnWriteFCConvert.ToDouble(
			int tValue = 0;
			// module specific function
			try
			{
				// On Error GoTo ErrorHandler
				strLine1 = "";
				strLine2 = "";
				if (boolByAccount)
				{
					tValue = FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLANDVAL)) + Conversion.Val(Grid.TextMatrix(lngCurrRow, COLBLDGVAL)) - Conversion.Val(Grid.TextMatrix(lngCurrRow, COLEXEMPTION)));
					(this.Detail.Controls["txtYear"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTaxYear + "";
					(this.Detail.Controls["txtLand"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLANDVAL));
					(this.Detail.Controls["txtBldg"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLBLDGVAL));
					(this.Detail.Controls["txtExempt"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLEXEMPTION));
					(this.Detail.Controls["txtTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = tValue;
					strLine2 = " " + "ACCT:" + Strings.Format(Grid.TextMatrix(lngCurrRow, COLACCOUNT), "@@@@@@") + "  " + "Map/Lot:" + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLMAPLOT));
				}
				else
				{
					Conversion.Val(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLANDVAL)) + Conversion.Val(Grid.TextMatrix(lngCurrRow, COLBLDGVAL)) - Conversion.Val(Grid.TextMatrix(lngCurrRow, COLEXEMPTION)));
					// Mid(strLine1, 1, 6) = Format(strTaxYear & "", "@@@@@") & " "
					(this.Detail.Controls["txtYear"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTaxYear + "";
					(this.Detail.Controls["txtLand"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLANDVAL));
					(this.Detail.Controls["txtBldg"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLBLDGVAL));
					(this.Detail.Controls["txtExempt"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLEXEMPTION));
					(this.Detail.Controls["txtTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = tValue;
					strLine2 = " " + "ACCT:" + Strings.Format(Grid.TextMatrix(lngCurrRow, COLACCOUNT), "@@@@@@") + "-" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLCARD))) + "  " + "Map/Lot:" + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLMAPLOT));
					(this.Detail.Controls["txtData2"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeAssessmentCard", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeChangeAssessment()
		{
			// vbPorter upgrade warning: lngTotal As int	OnWriteFCConvert.ToDouble(
			int lngTotal = 0;
			// vbPorter upgrade warning: strTemp As string	OnWriteFCConvert.ToSingle(
			string strTemp = "";
			string strTemp2 = "";
			try
			{
				// On Error GoTo ErrorHandler
				// module specific code
				strLine1 = "";
				strLine2 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				if (boolFirstLabel)
				{
					this.Detail.Controls["lblLand"].Visible = false;
					this.Detail.Controls["txtLand"].Visible = false;
					this.Detail.Controls["lblBldg"].Visible = false;
					this.Detail.Controls["txtBldg"].Visible = false;
					this.Detail.Controls["lblExempt"].Visible = false;
					this.Detail.Controls["txtExempt"].Visible = false;
					this.Detail.Controls["lblTotal"].Visible = false;
					this.Detail.Controls["txtTotal"].Visible = false;
					this.Detail.Controls["lblTax"].Visible = false;
					this.Detail.Controls["txtTax"].Visible = false;
					strLine1 = Strings.Mid(Strings.Trim(Grid.TextMatrix(lngCurrRow, COLNAME)), 1, 35) + " " + (FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLACCOUNT))) + "-" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLCARD))));
					strLine2 = Strings.Mid(Strings.Trim(Grid.TextMatrix(lngCurrRow, COLSECOWNER)), 1, 38);
					strLine3 = Strings.Mid(Strings.Trim(Grid.TextMatrix(lngCurrRow, COLADDR1)), 1, 38);
					strLine4 = Strings.Mid(Strings.Trim(Grid.TextMatrix(lngCurrRow, COLADDR2)), 1, 38);
					strLine5 = Strings.Mid(Strings.Trim(Grid.TextMatrix(lngCurrRow, COLCITY)) + " " + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLSTATE)) + "  " + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLZIP)) + " " + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLZIP4)), 1, 38);
					if (Strings.Trim(strLine4) == string.Empty)
					{
						strLine4 = strLine5;
						strLine5 = "";
					}
					if (Strings.Trim(strLine3) == string.Empty)
					{
						strLine3 = strLine4;
						strLine4 = strLine5;
						strLine5 = "";
					}
					if (Strings.Trim(strLine2) == string.Empty)
					{
						strLine2 = strLine3;
						strLine3 = strLine4;
						strLine4 = strLine5;
						strLine5 = "";
						/*? 34 */
					}
				}
				else
				{
					this.Detail.Controls["lblLand"].Visible = true;
					this.Detail.Controls["txtLand"].Visible = true;
					this.Detail.Controls["lblBldg"].Visible = true;
					this.Detail.Controls["txtBldg"].Visible = true;
					this.Detail.Controls["lblExempt"].Visible = true;
					this.Detail.Controls["txtExempt"].Visible = true;
					this.Detail.Controls["lblTotal"].Visible = true;
					this.Detail.Controls["txtTotal"].Visible = true;
					strLine1 = Strings.StrDup(38, " ");
					lngTotal = FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLANDVAL)) + Conversion.Val(Grid.TextMatrix(lngCurrRow, COLBLDGVAL)) - Conversion.Val(Grid.TextMatrix(lngCurrRow, COLEXEMPTION)));
					strLine2 = Strings.StrDup(38, " ");
					fecherFoundation.Strings.MidSet(ref strLine2, 1, 28, Strings.Mid(" MAP/LOT:" + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLMAPLOT)), 1, 28));
					strLine3 = Strings.StrDup(38, " ");
					strLine4 = Strings.StrDup(38, " ");
					if (boolIncETax)
					{
						this.Detail.Controls["lblTax"].Visible = true;
						this.Detail.Controls["txtTax"].Visible = true;
						// strline6 = " Estimated Tax:"
						// If boolDotMatrix Then
						// strLine5 = String(38, " ")
						// Else
						// strLine5 = String(16, " ")
						// End If
						// Mid(strLine5, 1, 15) = " Estimated Tax:"
						(this.Detail.Controls["txtTax"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = (snglTax / 1000) * lngTotal;
						strTemp = FCConvert.ToString((snglTax / 1000) * lngTotal);
						// If boolDotMatrix Then
						// strTemp = Format(strTemp, "###,###,##0.00")
						// Mid(strLine5, Len(strLine5) - Len(strTemp), Len(strTemp)) = strTemp
						// End If
						// If Not boolDotMatrix Then txtTax.Visible = True
					}
					fecherFoundation.Strings.MidSet(ref strLine1, 1, 28, Strings.Mid(" ACCOUNT:" + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLACCOUNT)) + "-" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLCARD))), 1, 28));
					fecherFoundation.Strings.MidSet(ref strLine3, 1, 28, Strings.Mid(" " + Strings.Trim(Grid.TextMatrix(lngCurrRow, COLLOCSTREET)), 1, 28));
					(this.Detail.Controls["txtLand"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLANDVAL));
					(this.Detail.Controls["txtBldg"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLBLDGVAL));
					(this.Detail.Controls["txtExempt"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = Conversion.Val(Grid.TextMatrix(lngCurrRow, COLEXEMPTION));
					(this.Detail.Controls["txtTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Value = lngTotal;
				}
				(this.Detail.Controls["txtData1"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine1;
				(this.Detail.Controls["txtData2"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
				(this.Detail.Controls["txtData3"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine3;
				(this.Detail.Controls["txtData4"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine4;
				(this.Detail.Controls["txtData5"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine5;
				boolFirstLabel = !boolFirstLabel;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in line " + Information.Erl() + " in MakeChangeAssessment", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeCardLabel()
		{
			string strNam;
			string strSecOwn = "";
			string strML;
			string strLC;
			string strLocNum;
			string strSale = "";
			string strBookPage = "";
			string[] strTempLine = new string[6 + 1];
			int intIndex;
			try
			{
				// On Error GoTo ErrorHandler
				// strNam = RSMster.Fields("rsname")
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				if (boolIncSecOwner)
				{
					// strSecOwn = Trim(RSMster.Fields("rssecowner") & "")
					strSecOwn = Grid.TextMatrix(lngCurrRow, COLSECOWNER);
				}
				else
				{
					strSecOwn = "";
				}
				// strML = Trim(RSMster.Fields("rsmaplot") & "")
				strML = Grid.TextMatrix(lngCurrRow, COLMAPLOT);
				// strSt = Trim(RSMster.Fields("rslocnumalph") & " " & RSMster.Fields("rslocstreet"))
				strLC = Grid.TextMatrix(lngCurrRow, COLLOCSTREET);
				strLocNum = "";
				if (Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLOCNUM)) > 0)
				{
					strLocNum = FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLOCNUM))) + " ";
				}
				strLC = Strings.Trim(strLocNum + " " + strLC);
				if (boolShowSaleData)
				{
					if (Grid.TextMatrix(lngCurrRow, COLSALEDATE) != string.Empty)
					{
						if (Information.IsDate(Grid.TextMatrix(lngCurrRow, COLSALEDATE)))
						{
							//if (Grid.TextMatrix(lngCurrRow, COLSALEDATE) != "12:00:00 AM")
							if (Grid.TextMatrix(lngCurrRow, COLSALEDATE) != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
							{
								strSale = Strings.Trim(Strings.Format(Grid.TextMatrix(lngCurrRow, COLSALEDATE), "MM/dd/yyyy") + " $" + Strings.Format(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLSALEPRICE))), "#,###,##0"));
							}
							else
							{
								strSale = "";
							}
						}
						else
						{
							strSale = "";
						}
					}
					else
					{
						strSale = "";
					}
					if (!clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile())
					{
						// If clsBookPage.FindFirstRecord("account", Val(Grid.TextMatrix(lngCurrRow, COLACCOUNT))) Then
						if (clsBookPage.FindFirst("account = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLACCOUNT)))))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strBookPage = Strings.Trim("B " + clsBookPage.Get_Fields("book") + " P " + clsBookPage.Get_Fields("Page"));
						}
						else
						{
							strBookPage = "";
						}
					}
					else
					{
						strBookPage = "";
					}
				}
				else
				{
					strSale = "";
					strBookPage = "";
				}
				FCUtils.EraseSafe(strTempLine);
				strTempLine[1] = strML;
				strSale = Strings.Trim(strBookPage + " " + strSale);
				intIndex = 3;
				if ((strSale != string.Empty) && (strSecOwn != string.Empty) && (strLC != string.Empty))
				{
					intIndex = 2;
				}
				strTempLine[intIndex] = strNam;
				intIndex += 1;
				if (strSecOwn == string.Empty && (strSale == string.Empty))
				{
					strTempLine[5] = strLC;
				}
				else
				{
					if (strSecOwn != string.Empty)
					{
						strTempLine[intIndex] = strSecOwn;
						intIndex += 1;
					}
					if (strLC != string.Empty)
					{
						strTempLine[intIndex] = strLC;
						intIndex += 1;
					}
					if (strSale != string.Empty)
					{
						strTempLine[intIndex] = strSale;
						intIndex += 1;
					}
				}
				strLine1 = strTempLine[1];
				strLine2 = strTempLine[2];
				strLine3 = strTempLine[3];
				strLine4 = strTempLine[4];
				strLine5 = strTempLine[5];
				strLine6 = "";
				(this.Detail.Controls["txtData1"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine1;
				(this.Detail.Controls["txtData2"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
				(this.Detail.Controls["txtData3"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine3;
				(this.Detail.Controls["txtData4"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine4;
				(this.Detail.Controls["txtData5"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine5;
				(this.Detail.Controls["txtData6"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine6;
				if (intTypeOfLabel == 11 || intTypeOfLabel == 10)
				{
					// certified mail - laser
					(this.Detail.Controls["txtData7"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine1;
					(this.Detail.Controls["txtData8"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
					(this.Detail.Controls["txtData9"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine3;
					(this.Detail.Controls["txtData10"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine4;
					(this.Detail.Controls["txtData11"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine5;
					(this.Detail.Controls["txtData12"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine6;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeCardLabel", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeCustom()
		{
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
			string strTemp = "";
			
			string strNam;
			string strA1;
			string strA2 = "";
			string strA3 = "";
			string strSecOwn = "";
			string strZip = "";
			string strZip4 = "";
			string strST = "";
			string strML = "";
			// vbPorter upgrade warning: lngAct As int	OnRead(string)
			int lngAct = 0;
			string strR1 = "";
			string strR2 = "";
			string strLC = "";
			string strLocNum = "";
			string strAcres = "";
			// vbPorter upgrade warning: intCharLen As short --> As int	OnWrite(double, int)
			int intCharLen;
			string[] strL = null;
			int intCurLine = 0;
			int x;
			int y;
			string strMLot1 = "";
			string strMLot2 = "";
			string strMLot3 = "";
			string strMLot4 = "";
			string strCountry = "";
			try
			{
				// On Error GoTo ErrorHandler
				strL = new string[intMaxLines + 1];
				for (x = 1; x <= intMaxLines; x++)
				{
					strL[x] = "";
				}
				// x
				// module specific function
				// If Not boolElimDups Then
				// strNam = RSMster.Fields("rsname")
				// strA1 = RSMster.Fields("rsaddr1")
				// 
				// Call escapequote(strNam)
				// Call escapequote(strA1)
				// Else
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				strA1 = Grid.TextMatrix(lngCurrRow, COLADDR1);
				// 3        Call escapequote(strNam)
				// 4        Call escapequote(strA1)
				// End If
                //FC:FINAL:MSH - issue #1659: remove division to avoid wrong calculations
				//intCharLen = FCConvert.ToInt32((this.Detail.Controls["txtData1"].Width / 1440) * intCharsPerInch);
				intCharLen = FCConvert.ToInt32(this.Detail.Controls["txtData1"].Width * intCharsPerInch);
				intCharLen -= 1;
				strCountry = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLCOUNTRY));
				if (Strings.LCase(strCountry) == "united states")
				{
					strCountry = "";
				}
				strA2 = Grid.TextMatrix(lngCurrRow, COLADDR2);
				strA3 = Grid.TextMatrix(lngCurrRow, COLCITY);
				strSecOwn = Grid.TextMatrix(lngCurrRow, COLSECOWNER);
				strZip = Grid.TextMatrix(lngCurrRow, COLZIP);
				strZip4 = Grid.TextMatrix(lngCurrRow, COLZIP4);
				strST = Grid.TextMatrix(lngCurrRow, COLSTATE);
				strML = Grid.TextMatrix(lngCurrRow, COLMAPLOT);
				lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
				strR1 = Grid.TextMatrix(lngCurrRow, COLREF1);
				strR2 = Grid.TextMatrix(lngCurrRow, COLREF2);
				strLC = Grid.TextMatrix(lngCurrRow, COLLOCSTREET);
				strLocNum = "";
				if (Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLOCNUM)) > 0)
				{
					strLocNum = FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLLOCNUM))) + " ";
				}
				strAcres = "Acres " + Strings.Format(Conversion.Val(Grid.TextMatrix(lngCurrRow, COLACREAGE)), "0.00");
				strL[1] = Strings.StrDup(intCharLen + 1, " ");
				strLine2 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				if (boolIncOwner)
				{
					intCurLine = 1;
					// Mid(strLine1, 1) = Left(strNam & "", 37)
					// 25        Mid(strLine1, 1) = Left(strNam & "", intCharLen)
					strL[1] = Strings.Left(strNam + Strings.StrDup(intCharLen, " "), intCharLen);
				}
				else
				{
					intCurLine = 0;
				}
				/*? 26 */// strL(intCurLine) = strLine1
				intCurLine += 1;
				if (boolIncSecOwner && Strings.Trim(strSecOwn) != string.Empty && intCurLine <= intMaxLines)
				{
					strL[intCurLine] = Strings.Mid(Strings.Trim(strSecOwn), 1, intCharLen);
					intCurLine += 1;
				}
				if (boolIncAddr)
				{
					if (Strings.Trim(strA1) != string.Empty && intCurLine <= intMaxLines)
					{
						strL[intCurLine] = Strings.Mid(Strings.Trim(strA1), 1, intCharLen);
						intCurLine += 1;
					}
					if (Strings.Trim(strA2) != string.Empty && intCurLine <= intMaxLines)
					{
						strL[intCurLine] = Strings.Mid(Strings.Trim(strA2), 1, intCharLen);
						intCurLine += 1;
					}
					if (Strings.Trim(strA3 + strST + strZip + strZip4) != string.Empty && intCurLine <= intMaxLines)
					{
						strL[intCurLine] = Strings.Mid(Strings.Trim(Strings.Trim(strA3 + "") + " " + strST + " " + strZip + " " + strZip4), 1, intCharLen);
						intCurLine += 1;
					}
					if (strCountry != "" && intCurLine <= intMaxLines)
					{
						strL[intCurLine] = strCountry;
						intCurLine += 1;
					}
				}
				if (boolIncRef && Strings.Trim(strR1) != string.Empty && intCurLine <= intMaxLines)
				{
					strL[intCurLine] = Strings.Mid(Strings.Trim(strR1 + ""), 1, intCharLen);
					intCurLine += 1;
				}
				if (boolIncMap && intCurLine <= intMaxLines && !modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
				{
					strL[intCurLine] = Strings.Mid("Maplot: " + Strings.Trim(strML + ""), 1, intCharLen);
					intCurLine += 1;
				}
				if (boolIncLoc && Strings.Trim(strLocNum + strLC) != string.Empty && intCurLine <= intMaxLines)
				{
					strL[intCurLine] = Strings.Mid(Strings.Trim(strLocNum + strLC + ""), 1, intCharLen);
					intCurLine += 1;
				}
				if (boolIncRef2 && Strings.Trim(strR2) != string.Empty && intCurLine <= intMaxLines)
				{
					strL[intCurLine] = Strings.Mid(Strings.Trim(strR2), 1, intCharLen);
					intCurLine += 1;
				}
				if (boolIncAcreage && intCurLine <= intMaxLines)
				{
					strL[intCurLine] = Strings.Mid(Strings.Trim(strAcres + ""), 1, intCharLen);
					intCurLine += 1;
				}
				if (boolIncAcct && !modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					strTemp = " " + FCConvert.ToString(lngAct);
					if (strL[1].Length < intCharLen)
					{
						strL[1] = Strings.Left(strL[1] + Strings.StrDup(intCharLen, " "), intCharLen);
					}
					fecherFoundation.Strings.MidSet(ref strL[1], intCharLen - Strings.Len(strTemp) + 1, Strings.Len(strTemp), strTemp);
				}
				// For x = intCurLine - 2 To 1 Step -1
				// If Trim(strL(x)) = vbNullString Then
				// For y = x To intCurLine - 2
				// strL(y) = strL(y + 1)
				// strL(y + 1) = ""
				// Next y
				// End If
				// Next x
				// 28        If boolIncSecOwner Then
				// 29            strLine2 = Mid(Trim(strSecOwn), 1, 38)
				// 30            If boolIncAddr Then
				// 31                strLine3 = Mid(Trim(strA1), 1, 38)
				// 32                If Trim(strA2 & "") <> vbNullString Then
				// 33                    strLine4 = Mid(Trim(strA2), 1, 38)
				// 34                    strLine5 = Mid(Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, 38)
				// 35                    If boolIncRef Then
				// 36                        strLine6 = Mid(Trim(strR1 & ""), 1, 38)
				// If boolDotMatrix Then
				// Call stripmiddlespaces(strLine6)
				// End If
				// Else
				// 37                        If boolIncMap Then
				// If Not boolDotMatrix Then
				// 38                                strLine6 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// End If
				// 
				// Else
				// 39                    strLine4 = Mid(Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, 38)
				// 40
				// 41                    If boolIncRef Then
				// 42                        strLine5 = Mid(Trim(strR1 & ""), 1, 38)
				// If Not boolDotMatrix Then
				// 43                            If boolIncMap Then
				// 44                                strLine6 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// Else
				// Call stripmiddlespaces(strLine5)
				// End If
				// Else
				// If Not boolDotMatrix Then
				// 45                            If boolIncMap Then
				// 46                                strLine5 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// End If
				// End If
				// Else
				// 
				// 47                If boolIncRef Then
				// 48                    strLine3 = Mid(Trim(strR1 & ""), 1, 38)
				// If boolDotMatrix Then
				// Call stripmiddlespaces(strLine3)
				// End If
				// 49                    If boolIncRef2 Then
				// 50                    strLine4 = Mid(Trim(strR2 & ""), 1, 38)
				// End If
				// 51                    If boolIncMap Then
				// If Not boolDotMatrix Then
				// 52                            strLine5 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// Else
				// 53                    If boolIncRef2 Then
				// 54                        strLine3 = Mid(Trim(strR2 & ""), 1, 38)
				// End If
				// If boolIncMap Then
				// If Not boolDotMatrix Then
				// 55                            strLine4 = Mid(Trim(strML & ""), 1, 38)
				// End If
				// End If
				// End If
				// 
				// If boolIncLoc Then
				// 56                    strLine6 = Mid(strLocNum & Trim(strLC & ""), 1, 38)
				// End If
				// End If
				// 57                If strLine2 = vbNullString Then
				// 58                    strLine2 = strLine3
				// 59                    strLine3 = strLine4
				// 60                    strLine4 = strLine5
				// 61                    strLine5 = strLine6
				// 62                    strLine6 = ""
				// End If
				// 
				// 
				// 
				// 
				// 
				// 
				// ElseIf boolIncAddr Then
				// 63            strLine2 = Mid(Trim(strA1), 1, 38)
				// 64            If Trim(strA2 & "") <> vbNullString Then
				// 65                strLine3 = Mid(Trim(strA2), 1, 38)
				// 66                strLine4 = Mid(Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, 38)
				// If boolIncRef Then
				// 67             strLine5 = Mid(Trim(strR1 & ""), 1, 38)
				// If boolDotMatrix Then
				// Call stripmiddlespaces(strLine5)
				// End If
				// If boolIncMap Then
				// If Not boolDotMatrix Then
				// 68                    strLine6 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// Else
				// If boolIncMap Then
				// If Not boolDotMatrix Then
				// 69                    strLine5 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// End If
				// 
				// Else
				// 70                strLine3 = Mid(Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, 38)
				// 
				// If boolIncRef Then
				// 71                    strLine4 = Mid(Trim(strR1 & ""), 1, 38)
				// If Not boolDotMatrix Then
				// 72                        strLine5 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// Else
				// Call stripmiddlespaces(strLine4)
				// End If
				// Else
				// If Not boolDotMatrix Then
				// 73                        strLine4 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// End If
				// If strLine2 = vbNullString Then
				// 74                strLine2 = strLine3
				// 75                strLine3 = strLine4
				// 76                strLine4 = strLine5
				// 77                strLine5 = strLine6
				// 78                strLine6 = ""
				// End If
				// 
				// Else
				// If boolIncRef Then
				// 79                strLine2 = Mid(Trim(strR1 & ""), 1, 38)
				// If boolDotMatrix Then
				// Call stripmiddlespaces(strLine2)
				// End If
				// If boolIncRef2 Then
				// 80                strLine3 = Mid(Trim(strR2 & ""), 1, 38)
				// End If
				// If boolIncMap Then
				// If Not boolDotMatrix Then
				// 81                        strLine4 = "MapLot: " & Mid(Trim(strML & ""), 1, 30)
				// End If
				// End If
				// Else
				// If boolIncRef2 Then
				// 82                    strLine2 = Mid(Trim(strR2 & ""), 1, 38)
				// End If
				// If boolIncMap Then
				// If Not boolDotMatrix Then
				// 83                        strLine3 = Mid(Trim(strML & ""), 1, 38)
				// End If
				// End If
				// End If
				// 
				// If boolIncLoc Then
				// 84                strLine5 = Mid(Trim(strLocNum & strLC & ""), 1, 38)
				// End If
				// End If
				// 
				// If strLine5 = vbNullString Then
				// 85            strLine5 = strLine6
				// 86            strLine6 = ""
				// End If
				// 87        If strLine4 = vbNullString Then
				// 88            strLine4 = strLine5
				// 89            strLine5 = strLine6
				// 90            strLine6 = ""
				// End If
				// 91        If strLine3 = vbNullString Then
				// 92            strLine3 = strLine4
				// 93            strLine4 = strLine5
				// 94            strLine5 = strLine6
				// 95            strLine6 = ""
				// End If
				// 96        If strLine2 = vbNullString Then
				// 97            strLine2 = strLine3
				// 98            strLine3 = strLine4
				// 99            strLine4 = strLine5
				// 100            strLine5 = strLine6
				// 101            strLine6 = ""
				// End If
				if (boolIncMap && modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
				{
					if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
					{
						intCurLine = 1;
						/*? 96 */
					}
					else if (Strings.Trim(strMLot4) != string.Empty)
					{
						intCurLine = 2;
					}
					else
					{
						intCurLine = 3;
					}
					strMLot1 = "";
					strMLot2 = "";
					strMLot3 = "";
					strMLot4 = "";
					ParseMapLot(strML + "", ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
					if (strL[1].Length < intCharactersWide)
					{
						strL[1] += Strings.StrDup(intCharactersWide - strL[1].Length, " ");
					}
					if (strL[2].Length < intCharactersWide)
					{
						strL[2] += Strings.StrDup(intCharactersWide - strL[2].Length, " ");
					}
					if (intMaxLines >= 3)
					{
						if (strL[3].Length < intCharactersWide)
						{
							strL[3] += Strings.StrDup(intCharactersWide - strL[3].Length, " ");
						}
						if (intMaxLines >= 4)
						{
							if (strL[4].Length < intCharactersWide)
							{
								strL[4] += Strings.StrDup(intCharactersWide - strL[4].Length, " ");
							}
							if (intMaxLines >= 5)
							{
								if (strL[5].Length < intCharactersWide)
								{
									strL[5] += Strings.StrDup(intCharactersWide - strL[5].Length, " ");
								}
								if (intMaxLines >= 6)
								{
									if (strL[6].Length < intCharactersWide)
									{
										strL[6] += Strings.StrDup(intCharactersWide - strL[6].Length, " ");
									}
								}
							}
						}
					}
					strMLot1 = Strings.Trim(strMLot1);
					strMLot2 = Strings.Trim(strMLot2);
					strMLot3 = Strings.Trim(strMLot3);
					strMLot4 = Strings.Trim(strMLot4);
					if (strMLot1 != string.Empty)
					{
						fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot1), Strings.Len(strMLot1) + 1, " " + strMLot1);
						intCurLine += 1;
					}
					if (intCurLine <= intMaxLines)
					{
						if (strMLot2 != string.Empty)
						{
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot2), Strings.Len(strMLot2) + 1, " " + strMLot2);
							intCurLine += 1;
						}
					}
					if (intCurLine <= intMaxLines)
					{
						if (strMLot3 != string.Empty)
						{
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot3), Strings.Len(strMLot3) + 1, " " + strMLot3);
							intCurLine += 1;
						}
					}
					if (intCurLine <= intMaxLines)
					{
						if (strMLot4 != string.Empty)
						{
							fecherFoundation.Strings.MidSet(ref strL[intCurLine], Strings.Len(strL[intCurLine]) - Strings.Len(strMLot4), Strings.Len(strMLot4) + 1, " " + strMLot4);
						}
					}
				}
				if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom && intMaxLines >= 5)
				{
					strTemp = FCConvert.ToString(lngAct);
					modProperty.Statics.intTemp = strTemp.Length;
					strL[5] = Strings.Left(strL[5] + Strings.StrDup(intCharactersWide, " "), intCharLen);
					fecherFoundation.Strings.MidSet(ref strL[5], intCharLen - modProperty.Statics.intTemp, " " + strTemp);
				}
				strL[1] = Strings.RTrim(strL[1]);
				strL[2] = Strings.RTrim(strL[2]);
				if (Information.UBound(strL, 1) >= 3)
				{
					strL[3] = Strings.RTrim(strL[3]);
					if (Information.UBound(strL, 1) >= 4)
					{
						strL[4] = Strings.RTrim(strL[4]);
						if (Information.UBound(strL, 1) >= 5)
						{
							strL[5] = Strings.RTrim(strL[5]);
						}
					}
				}
				// With Me.Detail
				// 102        .Controls("txtData1") = strLine1
				// 103        .Controls("txtData2") = strLine2
				// 104        .Controls("txtData3") = strLine3
				// 105        .Controls("txtData4") = strLine4
				// 106        .Controls("txtData5") = strLine5
				// 107        .Controls("txtData6") = strLine6
				// End With
				for (x = 1; x <= intMaxLines; x++)
				{
					(this.Detail.Controls["txtData" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strL[x];
				}
				// x
				if (intTypeOfLabel == 11 || intTypeOfLabel == 10)
				{
					// certified mail - laser
					(this.Detail.Controls["txtData7"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine1;
					(this.Detail.Controls["txtData8"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine2;
					(this.Detail.Controls["txtData9"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine3;
					(this.Detail.Controls["txtData10"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine4;
					(this.Detail.Controls["txtData11"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine5;
					(this.Detail.Controls["txtData12"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine6;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeCustom", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short ParseMapLot(string strMapLot, ref string strOut1, ref string strOut2, ref string strOut3, ref string strOut4)
		{
			short ParseMapLot = 0;
			int intPosition;
			int x;
			string[] strAry = null;
			// returns max length of a maplot division but a minimum of 4 chars
			intPosition = Strings.InStr(1, strMapLot, "-", CompareConstants.vbTextCompare);
			x = 1;
			if (intPosition > 0 && intPosition <= 5)
			{
				strAry = Strings.Split(strMapLot, "-", -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) > 3)
				{
					strAry[3] += "-" + strAry[4];
					strAry[4] = "";
				}
				if (Information.UBound(strAry, 1) > 4)
				{
					strAry[3] += "-" + strAry[5];
					strAry[5] = "";
				}
				strOut1 = strAry[0];
				x = Strings.Trim(strOut1).Length;
				if (Information.UBound(strAry, 1) > 0)
				{
					strOut2 = strAry[1];
					if (Strings.Trim(strOut2).Length > x)
						x = Strings.Trim(strOut2).Length;
				}
				if (Information.UBound(strAry, 1) > 1)
				{
					strOut3 = strAry[2];
					if (Strings.Trim(strOut3).Length > x)
						x = Strings.Trim(strOut3).Length;
				}
				if (Information.UBound(strAry, 1) > 2)
				{
					strOut4 = strAry[3];
					if (Strings.Trim(strOut4).Length > x)
						x = Strings.Trim(strOut4).Length;
				}
				if (x < 4)
					x = 4;
				ParseMapLot = FCConvert.ToInt16(x);
				// Do While (Len(strMapLot) > 0)
				// If intPosition = 0 Then
				// intPosition = Len(strMapLot) + 1
				// End If
				// Select Case x
				// Case 1
				// strOut1 = Mid(strMapLot, 1, intPosition - 1)
				// Case 2
				// strOut2 = Mid(strMapLot, 1, intPosition - 1)
				// Case 3
				// strOut3 = Mid(strMapLot, 1, intPosition - 1)
				// Case 4
				// strOut4 = Mid(strMapLot, 1, intPosition - 1)
				// End Select
				// If intPosition > Len(strMapLot) Then Exit Do
				// strMapLot = Mid(strMapLot, intPosition + 1)
				// intPosition = InStr(1, strMapLot, "-")
				// 
				// x = x + 1
				// Loop
			}
			else
			{
				strOut1 = Strings.Mid(strMapLot, 1, 4);
				strOut2 = Strings.Mid(strMapLot, 5, 4);
				strOut3 = Strings.Mid(strMapLot, 9, 4);
				strOut4 = Strings.Mid(strMapLot, 13, 4);
				ParseMapLot = 4;
			}
			strOut1 = Strings.Trim(strOut1);
			strOut2 = Strings.Trim(strOut2);
			strOut3 = Strings.Trim(strOut3);
			strOut4 = Strings.Trim(strOut4);
			return ParseMapLot;
		}

		
	}
}
