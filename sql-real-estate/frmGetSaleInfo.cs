﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetSaleInfo.
	/// </summary>
	public partial class frmGetSaleInfo : BaseForm
	{
		public frmGetSaleInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label8.AddControlArrayElement(Label8_27, 27);
			this.Label8.AddControlArrayElement(Label8_26, 26);
			this.Label8.AddControlArrayElement(Label8_25, 25);
			this.Label8.AddControlArrayElement(Label8_24, 24);
			this.Label8.AddControlArrayElement(Label8_22, 22);
			this.Label8.AddControlArrayElement(Label8_23, 23);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetSaleInfo InstancePtr
		{
			get
			{
				return (frmGetSaleInfo)Sys.GetInstance(typeof(frmGetSaleInfo));
			}
		}

		protected frmGetSaleInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		bool boolMayCancel;
		int lngPrice;
		// vbPorter upgrade warning: dtDate As DateTime	OnWrite(DateTime, string)
		DateTime dtDate;
		int intType;
		int intFinancing;
		int intVerified;
		int intValidity;
		bool boolDidCancel;
		int lngTownCode;
		// vbPorter upgrade warning: intSaleType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intSaleFinancing As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intSaleVerified As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intSaleValidity As short	OnWriteFCConvert.ToInt32(
		public bool Init(bool boolCanCancel, ref int lngSalePrice, ref DateTime dtSaleDate, ref int intSaleType, ref int intSaleFinancing, ref int intSaleVerified, ref int intSaleValidity, string strTitle = "Sale Data", int lngTownNumber = 1)
		{
			bool Init = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					lngTownCode = lngTownNumber;
				}
				else
				{
					lngTownCode = 0;
				}
				Init = false;
				boolDidCancel = true;
				boolMayCancel = boolCanCancel;
				lngPrice = lngSalePrice;
				dtDate = dtSaleDate;
				intType = intSaleType;
				intFinancing = intSaleFinancing;
				intVerified = intSaleVerified;
				intValidity = intSaleValidity;
				this.Text = strTitle;
				this.Show(FCForm.FormShowEnum.Modal);
				Init = !boolDidCancel;
				if (!boolDidCancel || !boolMayCancel)
				{
					lngSalePrice = lngPrice;
					dtSaleDate = dtDate;
					intSaleType = FCConvert.ToInt16(intType);
					intSaleVerified = FCConvert.ToInt16(intVerified);
					intSaleValidity = FCConvert.ToInt16(intValidity);
					intSaleFinancing = FCConvert.ToInt16(intFinancing);
				}
				return Init;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Init;
		}

		private void frmGetSaleInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						if (boolMayCancel)
						{
							KeyCode = (Keys)0;
							mnuExit_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void frmGetSaleInfo_Load(object sender, System.EventArgs e)
		{
			int x;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
           
			FillCombos();
			Txtmrpisaleprice.Text = Strings.Format(lngPrice, "#,###,###,##0");
			if (dtDate.ToOADate() != 0)
			{
				txtMRPISaleDate.Text = Strings.Format(dtDate, "MM/dd/yyyy");
			}

            if (cmbSale.Items.Count > 0)
            {
                cmbSale.SelectedIndex = 0;
                for (x = 0; x <= cmbSale.Items.Count - 1; x++)
                {
                    if (cmbSale.ItemData(x) == intType)
                    {
                        cmbSale.SelectedIndex = x;
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show(
                    "You do not have sales related information set up in the cost files, so you can't update sale information",
                    "Missing Cost Files", MessageBoxButtons.OK, MessageBoxIcon.Error);                
                Unload();
                return;
            }

            if (cmbFinancing.Items.Count > 0)
            {
                cmbFinancing.SelectedIndex = 0;
                for (x = 0; x <= cmbFinancing.Items.Count - 1; x++)
                {
                    if (cmbFinancing.ItemData(x) == intFinancing)
                    {
                        cmbFinancing.SelectedIndex = x;
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show(
                    "You do not have sales related information set up in the cost files, so you can't update sale information",
                    "Missing Cost Files", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Unload();
                return;
            }

            if (cmbVerified.Items.Count > 0)
            {
                cmbVerified.SelectedIndex = 0;
                for (x = 0; x <= cmbVerified.Items.Count - 1; x++)
                {
                    if (cmbVerified.ItemData(x) == intVerified)
                    {
                        cmbVerified.SelectedIndex = x;
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show(
                    "You do not have sales related information set up in the cost files, so you can't update sale information",
                    "Missing Cost Files", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Unload();
                return;
            }

            if (cmbValidity.Items.Count > 0)
            {
                cmbValidity.SelectedIndex = 0;
                for (x = 0; x <= cmbValidity.Items.Count - 1; x++)
                {
                    if (cmbValidity.ItemData(x) == intValidity)
                    {
                        cmbValidity.SelectedIndex = x;
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show(
                    "You do not have sales related information set up in the cost files, so you can't update sale information",
                    "Missing Cost Files", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Unload();
                return;
            }
        }

		private void FillCombos()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from costrecord where isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode) + " and crecordnumber between 1351 and 1359 order by crecordnumber", modGlobalVariables.strREDatabase);
				cmbSale.Clear();
				while (!clsLoad.EndOfFile())
				{
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"));
					strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
					if (strTemp != string.Empty)
					{
						cmbSale.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350) + "  " + strTemp);
						cmbSale.ItemData(cmbSale.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350));
					}
					clsLoad.MoveNext();
				}
				clsLoad.OpenRecordset("select * from costrecord where isnull(townnumber,0)  = " + FCConvert.ToString(lngTownCode) + " and crecordnumber between 1361 and 1369 order by crecordnumber", modGlobalVariables.strREDatabase);
				cmbFinancing.Clear();
				while (!clsLoad.EndOfFile())
				{
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"));
					strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
					if (strTemp != string.Empty)
					{
						cmbFinancing.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360) + "  " + strTemp);
						cmbFinancing.ItemData(cmbFinancing.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360));
					}
					clsLoad.MoveNext();
				}
				clsLoad.OpenRecordset("select * from costrecord where isnull(townnumber,0)  = " + FCConvert.ToString(lngTownCode) + " and crecordnumber between 1371 and 1379 order by crecordnumber", modGlobalVariables.strREDatabase);
				cmbVerified.Clear();
				while (!clsLoad.EndOfFile())
				{
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"));
					strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
					if (strTemp != string.Empty)
					{
						cmbVerified.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370) + "  " + strTemp);
						cmbVerified.ItemData(cmbVerified.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370));
					}
					clsLoad.MoveNext();
				}
				clsLoad.OpenRecordset("select * from costrecord where isnull(townnumber,0)  = " + FCConvert.ToString(lngTownCode) + " and crecordnumber between 1381 and 1389 order by crecordnumber", modGlobalVariables.strREDatabase);
				cmbValidity.Clear();
				while (!clsLoad.EndOfFile())
				{
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"));
					strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
					if (strTemp != string.Empty)
					{
						cmbValidity.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380) + "  " + strTemp);
						cmbValidity.ItemData(cmbValidity.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380));
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCombos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			
			if (Conversion.Val(Txtmrpisaleprice.Text) > 0)
			{
				lngPrice = FCConvert.ToInt32(FCConvert.ToDouble(Txtmrpisaleprice.Text));
			}
			else
			{
				lngPrice = 0;
			}
			if (Information.IsDate(txtMRPISaleDate.Text))
			{
				dtDate = FCConvert.ToDateTime(Strings.Format(txtMRPISaleDate.Text, "MM/dd/yyyy"));
			}
			else
			{
				MessageBox.Show("You must specify a valid sale date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbSale.SelectedIndex >= 0)
			{
				intType = cmbSale.ItemData(cmbSale.SelectedIndex);
			}
			else
			{
				MessageBox.Show("You must specify a valid sale type", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbFinancing.SelectedIndex >= 0)
			{
				intFinancing = cmbFinancing.ItemData(cmbFinancing.SelectedIndex);
			}
			else
			{
				MessageBox.Show("You must specify a valid financing type", "Invalid Financing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbVerified.SelectedIndex >= 0)
			{
				intVerified = cmbVerified.ItemData(cmbVerified.SelectedIndex);
			}
			else
			{
				MessageBox.Show("You must specify a valid verification type", "Invalid Verification", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbValidity.SelectedIndex >= 0)
			{
				intValidity = cmbValidity.ItemData(cmbValidity.SelectedIndex);
			}
			else
			{
				MessageBox.Show("You must specify a validity type", "No Validity Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			boolDidCancel = false;
			this.Unload();
		}
	}
}
