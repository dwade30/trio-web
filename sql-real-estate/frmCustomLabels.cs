﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWRE0000
{
    /// <summary>
    /// Summary description for frmCustomLabels.
    /// </summary>
    public partial class frmCustomLabels : BaseForm
    {
        public frmCustomLabels()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCustomLabels InstancePtr
        {
            get
            {
                return (frmCustomLabels)Sys.GetInstance(typeof(frmCustomLabels));
            }
        }

        protected frmCustomLabels _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        //
        // THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
        // TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
        // THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
        // BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
        //
        // THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomLabels.rpt
        //
        // THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
        // SetFormFieldCaptions IN modCustomReport.mod
        // NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
        //
        // A CALL TO THIS FORM WOULD BE:
        // frmCustomLabels.Show , MDIParent
        // Call SetFormFieldCaptions(frmCustomLabels, "Births")
        //
        //
        int intCounter;
        int intStart;
        int intEnd;
        int intID;
        string strTemp = "";
        bool boolSaveReport;
        bool boolFromMortgageHolder;
        string strPassSortOrder = "";
        clsPrintLabel labLabelTypes = new clsPrintLabel();
        // vbPorter upgrade warning: lngID As int	OnWrite(string)
        public void Init(int lngID, short intDummy)
        {
            // currently lngID is only used as a mortgage holder id
            intID = lngID;
            boolFromMortgageHolder = true;
            if (boolFromMortgageHolder)
            {
                modCustomReport.Statics.strReportType = "MORTGAGEHOLDER";
            }
            Show(App.MainForm);
        }

        private void FillLabelTypeCombo()
        {
            cmbLabelType.AddItem("Avery 4013");
            cmbLabelType.AddItem("Avery 4014");
            cmbLabelType.AddItem("Avery 5160,5260,5970");
            cmbLabelType.AddItem("Avery 5161,5261,5661");
            cmbLabelType.AddItem("Avery 5162,5262,5662");
            cmbLabelType.AddItem("Avery 5163,5263,5663");
        }

        private void NewFillLabelTypeCombo()
        {
            int counter;
            // hide labels that are not supported by BD because of the 5 lines I need to print for an address
            for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
            {
                if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
                {
                    labLabelTypes.Set_Visible(counter, false);
                    break;
                }
            }
            // fill combo box with all available types of labels
            for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
            {
                if (labLabelTypes.Get_Visible(counter))
                {
                    cmbLabelType.AddItem(labLabelTypes.Get_Caption(counter));
                    cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabelTypes.Get_ID(counter));
                }
                else
                {
                    if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO4150)
                    {
                        cmbLabelType.AddItem(labLabelTypes.Get_Caption(counter));
                        cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabelTypes.Get_ID(counter));
                    }
                }
            }
        }

        private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int intIndex;
            intIndex = labLabelTypes.Get_IndexFromID(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
            lblDescription.Text = labLabelTypes.Get_Description(intIndex);
            // Dim strDesc As String
            // 
            // With cmbLabelType
            // If .ListIndex < 0 Then Exit Sub
            // Select Case .ListIndex
            // Case 0
            // strDesc = "Style 4013.  1 in. x 3.5 in. This type is a continuous sheet of labels used with a dot matrix."
            // vsLayout.ColWidth(0) = 3.5 * 1440
            // chkChooseLabelStart.Visible = False
            // Case 1
            // strDesc = "Style 4014.  1 7/16 in. x 4 in. This type is a continuous sheet of labels used with a dot matrix."
            // vsLayout.ColWidth(0) = 4 * 1440
            // chkChooseLabelStart.Visible = False
            // Case 2
            // strDesc = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in."
            // vsLayout.ColWidth(0) = 2.625 * 1440
            // chkChooseLabelStart.Visible = True
            // Case 3
            // strDesc = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in."
            // vsLayout.ColWidth(0) = 4 * 1440
            // chkChooseLabelStart.Visible = True
            // Case 4
            // strDesc = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in."
            // vsLayout.ColWidth(0) = 4 * 1440
            // chkChooseLabelStart.Visible = True
            // Case 5
            // strDesc = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in."
            // vsLayout.ColWidth(0) = 4 * 1440
            // chkChooseLabelStart.Visible = True
            // End Select
            // End With
            // lblDescription.Caption = strDesc
        }

        private void cmdAdd_Click()
        {
            // THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
            // THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
            // WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
            // THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
            // IMPORTANT
            int intRow;
            int intCol;
            string strReturn;
            clsDRWrapper RSLayout = new clsDRWrapper();
            clsDRWrapper rsSave = new clsDRWrapper();
            int intLabelType = 0;
            strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report", null/*, -1, -1*/);
            if (strReturn == string.Empty)
            {
                // DO NOT SAVE REPORT
            }
            else
            {
                // THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
                // NOT SHOW IT
                boolSaveReport = true;
                cmdPrint_Click();
                boolSaveReport = false;
                // SAVE THE REPORT
                rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + strReturn + "' and type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", "twbl0000.vb1");
                if (!rsSave.EndOfFile())
                {
                    MessageBox.Show("A report by that name already exists. A different name must be selected.", "Select different name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    // If optType(0) Then
                    // intLabelType = 0
                    // ElseIf optType(1) Then
                    // intLabelType = 1
                    // ElseIf optType(2) Then
                    // intLabelType = 2
                    // ElseIf optType(3) Then
                    // intLabelType = 3
                    // End If
                    intLabelType = cmbLabelType.SelectedIndex;
                    rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type,LabelType) VALUES ('" + strReturn + "','" + modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL) + "','" + Strings.UCase(modCustomReport.Statics.strReportType) + "'," + FCConvert.ToString(intLabelType) + ")", "twbl0000.vb1");
                    rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", "twbl0000.vb1");
                    if (!rsSave.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
                        rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), "twbl0000.vb1");
                        for (intRow = 1; intRow <= frmCustomLabels.InstancePtr.vsLayout.Rows - 1; intRow++)
                        {
                            for (intCol = 0; intCol <= frmCustomLabels.InstancePtr.vsLayout.Cols - 1; intCol++)
                            {
                                // TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
                                RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), "twbl0000.vb1");
                                RSLayout.AddNew();
                                // TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
                                RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
                                RSLayout.Set_Fields("RowID", intRow);
                                RSLayout.Set_Fields("ColumnID", intCol);
                                if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
                                {
                                    RSLayout.Set_Fields("FieldID", -1);
                                }
                                else
                                {
                                    RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
                                }
                                RSLayout.Set_Fields("Width", frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol));
                                RSLayout.Set_Fields("DisplayText", frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
                                RSLayout.Update();
                            }
                        }
                    }
                    MessageBox.Show("Custom Report saved as " + strReturn, "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void cmdClear_Click(object sender, System.EventArgs e)
        {
            // CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
            int intCounter;
            for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
            {
                vsWhere.TextMatrix(intCounter, 1, string.Empty);
                vsWhere.TextMatrix(intCounter, 2, string.Empty);
                vsWhere.TextMatrix(intCounter, 3, string.Empty);
            }
        }

        public void cmdClear_Click()
        {
            cmdClear_Click(cmdClear, new System.EventArgs());
        }

        private void cmdExit_Click(object sender, System.EventArgs e)
        {
            Unload();
        }

        public void cmdExit_Click()
        {
            //cmdExit_Click(cmdExit, new System.EventArgs());
        }

        private void cmdPrint_Click(object sender, System.EventArgs e)
        {
            // THIS ROUTINE WORKS TO PRINT OUT THE REPORT
            string strSQL;
            string strPrinterName = "";
            int NumFonts = 0;
            bool boolUseFont;
            string strFont = "";
            string strDefPrinterName = "";
            string strSort = "";
            // 
            // CLEAR THE FIELDS WIDTH ARRAY
            for (intCounter = 0; intCounter <= 49; intCounter++)
            {
                modCustomReport.Statics.strFieldWidth[intCounter] = 0;
            }
            // 
            // PREPARE THE SQL TO SHOW THE REPORT
            // BUILD THE SQL FOR THE NEW CUSTOM REPORT
            strSQL = BuildSQL();
            // 
            // GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
            // ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
            // Call GetNumberOfFields(strCustomSQL)
            // 
            // 
            // GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
            // WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
            // FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
            // Call SetColumnCaptions(Me)
            // 
            // make them choose the printer so we can set the correct printer font
            //FC:FINAL:DDU:#i1563 - initialize CommonDialog1
            //FC:FINAL:DSE:#1855 Printing is done on client
            //MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
            //MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
            //strPrinterName = FCGlobal.Printer.DeviceName;
            boolUseFont = false;
            int intLabelStart;
            intLabelStart = 1;
            int intIndex;
            intIndex = labLabelTypes.Get_IndexFromID(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
            strFont = "";
            if (labLabelTypes.Get_IsLaserLabel(intIndex))
            {


                if (chkChooseLabelStart.CheckState == Wisej.Web.CheckState.Checked)
                {
                    int NRows = 0;
                    int NCols = 0;
                    NRows = FCConvert.ToInt32(labLabelTypes.Get_LabelsHigh(intIndex));
                    NCols = labLabelTypes.Get_LabelsWide(intIndex);

                    // Select Case cmbLabelType.ListIndex
                    // Case 2
                    // 5260
                    // NRows = 10
                    // NCols = 3
                    // Case 3
                    // 5261
                    // NRows = 10
                    // NCols = 2
                    // Case 4
                    // 5262
                    // NRows = 7
                    // NCols = 2
                    // Case 5
                    // NRows = 5
                    // NCols = 2
                    // End Select
                    intLabelStart = frmChooseLabelPosition.InstancePtr.Init(NRows, NCols);

                    if (intLabelStart < 1) return;
                }
            }

            rptCustomLabels.InstancePtr.Init(ref strSQL, ref modCustomReport.Statics.strReportType, cmbLabelType.ItemData(cmbLabelType.SelectedIndex), ref strPrinterName, ref strFont, intLabelStart);
            Unload();
        }

        public void cmdPrint_Click()
        {
            cmdPrint_Click(cmdPrint, new System.EventArgs());
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        public string BuildSQL()
        {
            string BuildSQL = "";
            // BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
            string strSQL = "";
            string strWhere;
            string strOrderBy;
            BuildSQL = "";
            if (Strings.UCase(modCustomReport.Statics.strReportType) == "MORTGAGEHOLDER")
            {
                strSQL = "Select * from mortgageholders ";
            }
            else if (Strings.UCase(modCustomReport.Statics.strReportType) == "REALESTATE")
            {
                strSQL = "Select *,rsname as name,rsaddr1 as address1,rsaddr2 as address2,rsaddr3 as city,rsstate as state,rszip as zip,rszip4 as zip4 from master ";
            }
            else if (Strings.UCase(modCustomReport.Statics.strReportType) == "PERSONALPROPERTY")
            {
                strSQL = "Select * from ppmaster ";
            }
            strWhere = BuildWhereParameter();
            strSQL += strWhere;
            // BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
            strOrderBy = BuildSortParameter();
            if (Strings.Trim(strOrderBy) != string.Empty)
            {
                strSQL += " order by " + strOrderBy;
            }
            BuildSQL = strSQL;
            return BuildSQL;
        }

        public string BuildSortParameter()
        {
            string BuildSortParameter = "";
            // BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
            string strSort;
            int intCounter;
            // CLEAR OUT THE VARIABLES
            strSort = " ";
            // GET THE FIELDS TO SORT BY
            for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
            {
                if (lstSort.Selected(intCounter))
                {
                    if (strSort != " ")
                        strSort += ", ";
                    strSort += vsWhere.TextMatrix(lstSort.ItemData(intCounter), 3);
                }
            }
            BuildSortParameter = strSort;
            return BuildSortParameter;
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        private string BuildWhereParameter()
        {
            string BuildWhereParameter = "";
            // BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
            string strWhere;
            int intCounter;
            string strTemp = "";
            string strWord;
            // CLEAR THE VARIABLES
            strWhere = " ";
            strWord = " where ";
            if (modCustomReport.Statics.strReportType == "REALESTATE")
            {
                strWord = " and ";
                strWhere = " where rscard = 1 ";
            }
            for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
            {
                switch (intCounter)
                {
                    case 0:
                        {
                            // name
                            if (modCustomReport.Statics.strReportType == "REALESTATE")
                            {
                                strTemp = "rsname";
                            }
                            else
                            {
                                strTemp = "name";
                            }
                            if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != string.Empty)
                            {
                                if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != string.Empty)
                                {
                                    strWhere += strWord + strTemp + " between '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(0, 2)) + "' ";
                                }
                                else
                                {
                                    strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' ";
                                }
                                strWord = " and ";
                            }
                            break;
                        }
                    case 1:
                        {
                            // account
                            if (modCustomReport.Statics.strReportType == "MORTGAGEHOLDER")
                            {
                                // mh
                                strTemp = "mortgageholderid";
                            }
                            else if (modCustomReport.Statics.strReportType == "REALESTATE")
                            {
                                // re
                                strTemp = "rsaccount";
                            }
                            else
                            {
                                strTemp = "account";
                                // pp
                            }
                            if (Conversion.Val(vsWhere.TextMatrix(1, 1)) > 0)
                            {
                                if (Conversion.Val(vsWhere.TextMatrix(1, 2)) > 0)
                                {
                                    strWhere += strWord + strTemp + " between " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " and " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2))) + " ";
                                }
                                else
                                {
                                    strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " ";
                                }
                                strWord = " and ";
                            }
                            break;
                        }
                    case 2:
                        {
                            // location
                            if (modCustomReport.Statics.strReportType == "MORTGAGEHOLDER")
                            {
                                // mh
                                strTemp = "zip";
                                if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
                                {
                                    if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
                                    {
                                        strWhere += strWord + strTemp + " between '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(3, 2)) + "' ";
                                    }
                                    else
                                    {
                                        strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' ";
                                    }
                                    strWord = " and ";
                                }
                            }
                            else if (modCustomReport.Statics.strReportType == "REALESTATE")
                            {
                                // re
                                strTemp = "rslocstreet";
                            }
                            else
                            {
                                // pp
                                strTemp = "street";
                            }
                            if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
                            {
                                if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
                                {
                                    strWhere += strWord + strTemp + " between '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(2, 2)) + "' ";
                                }
                                else
                                {
                                    strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' ";
                                }
                                strWord = " and ";
                            }
                            break;
                        }
                    case 3:
                        {
                            // zip
                            strTemp = "zip";
                            if (modCustomReport.Statics.strReportType == "PERSONALPROPERTY")
                            {
                                if (Strings.Trim(vsWhere.TextMatrix(3, 1)) != string.Empty)
                                {
                                    if (Strings.Trim(vsWhere.TextMatrix(3, 2)) != string.Empty)
                                    {
                                        strWhere += strWord + strTemp + " between " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 1))) + " and " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 2))) + " ";
                                    }
                                    else
                                    {
                                        strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 1))) + " ";
                                    }
                                    strWord = " and ";
                                }
                            }
                            else
                            {
                                if (modCustomReport.Statics.strReportType == "REALESTATE")
                                    strTemp = "rszip";
                                if (Strings.Trim(vsWhere.TextMatrix(3, 1)) != string.Empty)
                                {
                                    if (Strings.Trim(vsWhere.TextMatrix(3, 2)) != string.Empty)
                                    {
                                        strWhere += strWord + strTemp + " between '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(3, 2)) + "' ";
                                    }
                                    else
                                    {
                                        strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' ";
                                    }
                                    strWord = " and ";
                                }
                            }
                            break;
                        }
                    case 4:
                        {
                            // maplot or businesscode
                            if (modCustomReport.Statics.strReportType == "PERSONALPROPERTY")
                            {
                                if (Conversion.Val(vsWhere.TextMatrix(4, 1)) > 0)
                                {
                                    if (Conversion.Val(vsWhere.TextMatrix(4, 2)) > 0)
                                    {
                                        strWhere += strWord + " businesscode between " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 1))) + " and " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 2))) + " ";
                                    }
                                    else
                                    {
                                        strWhere += strWord + " businesscode = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 1))) + " ";
                                    }
                                    strWord = " and ";
                                }
                            }
                            else
                            {
                                if (Strings.Trim(vsWhere.TextMatrix(4, 1)) != string.Empty)
                                {
                                    if (Strings.Trim(vsWhere.TextMatrix(4, 2)) != string.Empty)
                                    {
                                        strWhere += strWord + " rsmaplot between '" + Strings.Trim(vsWhere.TextMatrix(4, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(4, 2)) + "' ";
                                    }
                                    else
                                    {
                                        strWhere += strWord + " rsmaplot = '" + Strings.Trim(vsWhere.TextMatrix(4, 1)) + "' ";
                                    }
                                    strWord = " and ";
                                }
                            }
                            break;
                        }
                }
                //end switch
            }
            // intCounter
            BuildWhereParameter = strWhere;
            return BuildWhereParameter;
        }

        private void frmCustomLabels_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                // Case vbKeyF2
                // KeyCode = 0
                // Call mnuAddRow_Click
                // 
                // Case vbKeyF3
                // KeyCode = 0
                // Call mnuAddColumn_Click
                // 
                // Case vbKeyF4
                // KeyCode = 0
                // Call mnuDeleteRow_Click
                // 
                // Case vbKeyF5
                // KeyCode = 0
                // Call mnuDeleteColumn_Click
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        private void frmCustomLabels_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmCustomLabels properties;
            //frmCustomLabels.ScaleWidth	= 8955;
            //frmCustomLabels.ScaleHeight	= 7110;
            //frmCustomLabels.LinkTopic	= "Form1";
            //frmCustomLabels.LockControls	= true;
            //End Unmaped Properties
            // LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
            // Dim rsCreateTable As New clsDRWrapper
            // 
            // With rsCreateTable
            // CREATE A NEW TABLE IF IT DOESN'T EXIST
            // If .CreateNewDatabaseTable("tblCustomReports", "twbl0000.vb1") Then
            // CREATE THE NEW FIELDS
            // .CreateTableField "ID", dbLong
            // .CreateTableField "ReportName", dbText
            // .CreateTableField "Type", dbText
            // .CreateTableField "LabelType", dbInteger
            // .CreateTableField "SQL", dbMemo
            // .CreateTableField "LastUpdated", dbDate
            // 
            // SET THE PROPERTIES OF THE NEW FIELDS
            // .SetFieldAttribute "ID", dbAutoIncrField
            // .SetFieldDefaultValue "LastUpdated", "NOW()"
            // .SetFieldAllowZeroLength "ReportName", True
            // .SetFieldAllowZeroLength "Type", True
            // .SetFieldAllowZeroLength "SQL", True
            // 
            // DO THE ACTUAL CREATION OF THE TABLE
            // .UpdateTableCreation
            // Else
            // append a field to a table
            // .UpdateDatabaseTable "tblCustomReports", "twbl0000.vb1"
            // .AddTableField "LabelType", dbInteger
            // End If
            // End With
            // 
            // With rsCreateTable
            // CREATE A NEW TABLE IF IT DOESN'T EXIST
            // If .CreateNewDatabaseTable("tblReportLayout", "twbl0000.vb1") Then
            // CREATE THE NEW FIELDS
            // .CreateTableField "id", dbLong
            // .CreateTableField "ReportID", dbInteger
            // .CreateTableField "RowID", dbInteger
            // .CreateTableField "ColumnID", dbInteger
            // .CreateTableField "FieldID", dbInteger
            // .CreateTableField "Width", dbInteger
            // .CreateTableField "DisplayText", dbText
            // .CreateTableField "LastUpdated", dbDate
            // 
            // SET THE PROPERTIES OF THE NEW FIELDS
            // .SetFieldAttribute "id", dbAutoIncrField
            // .SetFieldDefaultValue "LastUpdated", "NOW()"
            // .SetFieldAllowZeroLength "DisplayText", True
            // 
            // DO THE ACTUAL CREATION OF THE TABLE
            // .UpdateTableCreation
            // End If
            // End With
            modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "MortgageHolder");
            FillLayoutGrid();
            // GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
            // OF THE MDI PARENT FORM
            modGlobalFunctions.SetFixedSize(this);
            modGlobalFunctions.SetTRIOColors(this);
            // lblDescription.ForeColor = TRIOCOLORBLUE
            // Text1.ForeColor = TRIOCOLORBLUE
            // Call FillLabelTypeCombo
            NewFillLabelTypeCombo();
            cmbLabelType.SelectedIndex = 0;
            if (modCustomReport.Statics.strPreSetReport == string.Empty)
            {
                fraSort.Enabled = true;
                fraFields.Enabled = true;
                fraMessage.Visible = false;
                vsLayout.Visible = true;
            }
            else
            {
                fraSort.Enabled = false;
                fraFields.Enabled = false;
                fraMessage.Visible = true;
                vsLayout.Visible = false;
            }
            if (boolFromMortgageHolder)
            {
                lstFields.Enabled = false;
                lstSort.Enabled = false;
                vsWhere.Enabled = false;
                vsWhere.TextMatrix(1, 1, FCConvert.ToString(intID));
                fraFields.Enabled = false;
                fraSort.Enabled = false;
                fraWhere.Enabled = false;
            }
            // 
            // fraNotes.Enabled = strPreSetReport = vbNullString
            // fraReports.Enabled = strPreSetReport = vbNullString
            // fraMessage.Visible = Not strPreSetReport = vbNullString
        }

        private void frmCustomLabels_Resize(object sender, System.EventArgs e)
        {
            ResizeVSWhere();
        }

        private void ResizeVSWhere()
        {
            int GridWidth = 0;
            GridWidth = vsWhere.WidthOriginal;
            vsWhere.ColWidth(0, FCConvert.ToInt32(0.3 * GridWidth));
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            modCustomReport.Statics.strPreSetReport = string.Empty;
            boolFromMortgageHolder = false;
        }

        private void fraFields_DoubleClick(object sender, System.EventArgs e)
        {
            int intCounter;
            for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
            {
                lstFields.SetSelected(intCounter, true);
            }
        }

        private void fraSort_DoubleClick(object sender, System.EventArgs e)
        {
            int intCounter;
            for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
            {
                lstSort.SetSelected(intCounter, true);
            }
        }

        private void fraWhere_DoubleClick(object sender, System.EventArgs e)
        {
            cmdClear_Click();
        }

        private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (lstFields.SelectedIndex < 0)
                return;
            switch (lstFields.SelectedIndex)
            {
                case 0:
                    {
                        // mortgage holders
                        modCustomReport.Statics.strReportType = "MORTGAGEHOLDER";
                        modCustomReport.LoadWhereGrid(this);
                        modCustomReport.LoadSortList(this);
                        break;
                    }
                case 1:
                    {
                        // Real Estate
                        modCustomReport.Statics.strReportType = "REALESTATE";
                        modCustomReport.LoadWhereGrid(this);
                        modCustomReport.LoadSortList(this);
                        break;
                    }
                case 2:
                    {
                        // Personal Property
                        modCustomReport.Statics.strReportType = "PERSONALPROPERTY";
                        modCustomReport.LoadWhereGrid(this);
                        modCustomReport.LoadSortList(this);
                        break;
                    }
            }
            //end switch
        }

        private void FillLayoutGrid()
        {
            int GridWidth;
            vsLayout.Cols = 1;
            vsLayout.Rows = 4;
            vsLayout.ColWidth(0, 4 * 1440);
            // four inches
            vsLayout.TextMatrix(0, 0, "Name");
            vsLayout.TextMatrix(1, 0, "Address 1");
            vsLayout.TextMatrix(2, 0, "Address 2");
            vsLayout.TextMatrix(3, 0, "City, State 00000");
        }

        //private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
        //	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
        //	// WHERE TO SWAP THE TWO ITEMS.
        //	// 
        //	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //	// WILL BE DISPLAYED ON THE REPORT ITSELF
        //	// intStart = lstFields.ListIndex
        //}

        //private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
        //	// ITEMS THAT ARE TO BE SWAPED
        //	// 
        //	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //	// WILL BE DISPLAYED ON THE REPORT ITSELF
        //	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
        //	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
        //	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
        //	// strtemp = lstFields.List(lstFields.ListIndex)
        //	// intID = lstFields.ItemData(lstFields.ListIndex)
        //	// 
        //	// CHANGE THE NEW ITEM
        //	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
        //	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
        //	// 
        //	// SAVE THE OLD ITEM
        //	// lstFields.List(intStart) = strtemp
        //	// lstFields.ItemData(intStart) = intID
        //	// 
        //	// SET BOTH ITEMS TO BE SELECTED
        //	// lstFields.Selected(lstFields.ListIndex) = True
        //	// lstFields.Selected(intStart) = True
        //	// End If
        //}

        //private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //    MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //    int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //    float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //    float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //    // THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
        //    // ITEMS THAT ARE TO BE SWAPED
        //    //
        //    // THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //    // WILL BE DISPLAYED ON THE REPORT ITSELF
        //    // IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
        //    if (intStart != lstSort.SelectedIndex)
        //    {
        //        // SAVE THE CAPTION AND ID FOR THE NEW ITEM
        //        strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
        //        intID = lstSort.ItemData(lstSort.SelectedIndex);
        //        // CHANGE THE NEW ITEM
        //        lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
        //        lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
        //        // SAVE THE OLD ITEM
        //        lstSort.Items[intStart].Text = strTemp;
        //        lstSort.ItemData(intStart, intID);
        //        // SET BOTH ITEMS TO BE SELECTED
        //        lstSort.SetSelected(lstSort.ListIndex, true);
        //        lstSort.SetSelected(intStart, true);
        //    }
        //}
        private void mnuAddColumn_Click()
        {
            if (vsLayout.Row < 0)
                vsLayout.Row = 0;
            vsLayout.Cols += 1;
            vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
            vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
        }

        private void mnuAddRow_Click()
        {
            vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
            vsLayout.Select(vsLayout.Rows - 1, 0);
        }

        private void mnuClear_Click(object sender, System.EventArgs e)
        {
            cmdClear_Click();
        }

        private void mnuDeleteColumn_Click()
        {
            if (vsLayout.Cols > 1)
            {
                vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
                vsLayout.Cols -= 1;
            }
            else
            {
                MessageBox.Show("You must have at least one column in a report.", "Add Column", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuDeleteRow_Click()
        {
            if (vsLayout.Rows > 2)
            {
                vsLayout.RemoveItem(vsLayout.Row);
            }
            else
            {
                MessageBox.Show("You must have at least one row in a report.", "Add Row", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            cmdExit_Click();
        }

        public void mnuExit_Click()
        {
            //mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuPrint_Click(object sender, System.EventArgs e)
        {
            cmdPrint_Click();
        }

        private void optType_Click(ref short Index)
        {
            // NUMBER OF ROWS IS HIGHT / 240 (HEIGHT OF ONE ROW) PLUS ONE FOR
            // THE HEADER ROW   1440 = 1 INCH
            int intCounter;
            // THESE DIMENTIONS ARE A BIT DIFFERENT THEN THE NUMBERS OUTLINED
            // IN MS WORD BECAUSE WE ARE USING A DIFFERENT FONT HERE THEN
            // THE MS WORD DEFAULT FONT.
            // 
            vsLayout.Clear();
            switch (Index)
            {
                case 0:
                    {
                        // Avery 5160 (3 X 10)  Height = 1" Width = 2.63"
                        // 1440/240(row height)
                        vsLayout.Rows = 7;
                        vsLayout.Cols = 1;
                        Line1.X1 = 2.8F;
                        Line1.X2 = 2.8F;
                        Line1.Y1 = 0;
                        Line1.Y2 = vsLayout.Height;
                        break;
                    }
                case 1:
                    {
                        // Avery 5161 (2 X 10)  Height = 1" Width = 4"
                        // 1440/240(row height)
                        vsLayout.Rows = 7;
                        vsLayout.Cols = 1;
                        Line1.X1 = 4F;
                        Line1.X2 = 4F;
                        Line1.Y1 = 0;
                        Line1.Y2 = vsLayout.Height;
                        break;
                    }
                case 2:
                    {
                        // Avery 5163 (2 X 5)  Height = 2" Width = 4"
                        // 1440/240(row height)
                        vsLayout.Rows = 13;
                        vsLayout.Cols = 1;
                        Line1.X1 = 4F;
                        Line1.X2 = 4F;
                        Line1.Y1 = 0;
                        Line1.Y2 = vsLayout.Height;
                        break;
                    }
                case 3:
                    {
                        // Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
                        // 1440/240(row height)
                        vsLayout.Rows = 9;
                        vsLayout.Cols = 1;
                        Line1.X1 = 4F;
                        Line1.X2 = 4F;
                        Line1.Y1 = 0;
                        Line1.Y2 = vsLayout.Height;
                        break;
                    }
            }
            //end switch
            for (intCounter = 0; intCounter <= vsLayout.Rows - 1; intCounter++)
            {
                vsLayout.MergeRow(intCounter, true);
            }
        }

        private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
            {
                vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
            }
        }

        private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
        {
            // If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
        }

        private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            // AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
            // GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
            // 
            // THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
            // AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
            // 
            // THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
            vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
            if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
            {
                vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
            }
            if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
            {
                vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
            }
        }

        private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            // SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
            // IF TEXT...ALLOW ANYTHING
            // IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
            // IF COMBO...ADD THE LIST OF OPTIONS
            vsWhere.EditMask = string.Empty;
            vsWhere.ComboList = string.Empty;
            //FC:FINAL:RPU: #i1167 - Check first if the string is null
            if (modCustomReport.Statics.strWhereType[vsWhere.Row] != null)
            {
                switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
                {
                    case modCustomReport.GRIDTEXT:
                        {
                            break;
                        }
                    case modCustomReport.GRIDDATE:
                        {
                            vsWhere.EditMask = "##/##/####";
                            break;
                        }
                    case modCustomReport.GRIDCOMBOIDTEXT:
                    case modCustomReport.GRIDCOMBOIDNUM:
                    case modCustomReport.GRIDCOMBOTEXT:
                        {
                            vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
                            break;
                        }
                }
                //end switch
            }
        }

        private void vsWhere_RowColChange(object sender, System.EventArgs e)
        {
            // SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
            // IF COMBO...ADD THE LIST OF OPTIONS
            if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
            {
                vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
            }
            if (vsWhere.Col == 2)
            {
                //FC:FINAL:RPU: #i1167 - Use the right comparision as in original
                //if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == this.BackColor)
                if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
                {
                    vsWhere.Col = 1;
                }
            }
        }

        private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            // THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
            // GIRD THAT WILL FILTER OUT RECORDS
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = vsWhere.GetFlexRowIndex(e.RowIndex);
            int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
            //FC:FINAL:RPU: #i1167 - Check first if the string is null
            if (modCustomReport.Statics.strWhereType[row] != null)
            {
                switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[row]))
                {
                    case modCustomReport.GRIDTEXT:
                        {
                            // ANYTHING GOES IF IT IS A TEXT FIELD
                            break;
                        }
                    case modCustomReport.GRIDDATE:
                        {
                            // MAKE SURE THAT IT IS A VALID DATE
                            if (Strings.Trim(vsWhere.EditText) == "/  /")
                            {
                                vsWhere.EditMask = string.Empty;
                                vsWhere.EditText = string.Empty;
                                vsWhere.TextMatrix(row, col, string.Empty);
                                vsWhere.Refresh();
                                return;
                            }
                            if (Strings.Trim(vsWhere.EditText).Length == 0)
                            {
                            }
                            else if (Strings.Trim(vsWhere.EditText).Length != 10)
                            {
                                MessageBox.Show("Invalid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                e.Cancel = true;
                                return;
                            }
                            if (!modCustomReport.IsValidDate(vsWhere.EditText))
                            {
                                e.Cancel = true;
                                return;
                            }
                            break;
                        }
                    case modCustomReport.GRIDCOMBOIDTEXT:
                        {
                            // ASSIGN THE LIST TO THE COMBO IN THE GRID
                            vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
                            break;
                        }
                }
                //end switch
            }
        }
    }
}
