﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptChangedAccounts.
	/// </summary>
	public partial class rptChangedAccounts : BaseSectionReport
	{
		public rptChangedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptChangedAccounts InstancePtr
		{
			get
			{
				return (rptChangedAccounts)Sys.GetInstance(typeof(rptChangedAccounts));
			}
		}

		protected rptChangedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptChangedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTCMBUPDATEAMOUNTS = 0;
		const int CNSTCMBUPDATEINFOONLY = 1;
		int lngCRow;
		int intCurCol;
		bool boolInfoOnly;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: boolAChange As bool	OnWrite(bool, string)
			bool boolAChange = false;
			eArgs.EOF = lngCRow >= frmImportCompetitorXF.InstancePtr.GridGood.Rows;
			boolAChange = false;
			if (!eArgs.EOF)
			{
				boolAChange = FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTHASACHANGE));
				if (frmImportCompetitorXF.InstancePtr.cmbUpdateType.SelectedIndex == CNSTCMBUPDATEAMOUNTS)
				{
					boolAChange = boolAChange || FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTHASVALCHANGE));
				}
				while (!eArgs.EOF && !boolAChange)
				{
					lngCRow += 1;
					eArgs.EOF = lngCRow >= frmImportCompetitorXF.InstancePtr.GridGood.Rows;
					if (!eArgs.EOF)
					{
						boolAChange = FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTHASACHANGE));
						if (frmImportCompetitorXF.InstancePtr.cmbUpdateType.SelectedIndex == CNSTCMBUPDATEAMOUNTS)
						{
							boolAChange = boolAChange || FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTHASVALCHANGE));
						}
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			intCurCol = -1;
			lngCRow = 1;
			if (frmImportCompetitorXF.InstancePtr.cmbUpdateType.SelectedIndex == CNSTCMBUPDATEINFOONLY)
			{
				boolInfoOnly = true;
			}
			else
			{
				boolInfoOnly = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			bool boolAChange;
			if (lngCRow < frmImportCompetitorXF.InstancePtr.GridGood.Rows)
			{
				if (intCurCol == -1)
				{
					Line1.Visible = true;
					txtFieldName.Text = "Account";
					txtOldValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTTRIOACCOUNT);
					txtNewValue.Text = "";
					intCurCol = GetFirstCol();
					return;
				}
				else
				{
					Line1.Visible = false;
					while (!IsAChange(ref intCurCol) && intCurCol >= 0)
					{
						intCurCol = GetNextCol(ref intCurCol);
					}
				}
				if (intCurCol >= 0)
				{
					switch (intCurCol)
					{
						case modGridConstantsXF.CNSTOLDOWNER:
							{
								txtFieldName.Text = "Owner";
								txtOldValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDOWNER);
								txtNewValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOWNER);
								break;
							}
						case modGridConstantsXF.CNSTOLDSECOWNER:
							{
								txtFieldName.Text = "Second Owner";
								txtOldValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSECOWNER);
								txtNewValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSECOWNER);
								break;
							}
						case modGridConstantsXF.CNSTOLDADDRESS1:
							{
								txtFieldName.Text = "Address 1";
								txtOldValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDADDRESS1);
								txtNewValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTADDRESS1);
								break;
							}
						case modGridConstantsXF.CNSTOLDADDRESS2:
							{
								txtFieldName.Text = "Address 2";
								txtOldValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDADDRESS2);
								txtNewValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTADDRESS2);
								break;
							}
						case modGridConstantsXF.CNSTOLDCITY:
							{
								txtFieldName.Text = "City/State/Zip";
								txtOldValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDCITY) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSTATE) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDZIP) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDZIP4);
								txtNewValue.Text = frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTCITY) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTATE) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTZIP) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTZIP4);
								break;
							}
						case modGridConstantsXF.CNSTOLDSTREET:
							{
								txtFieldName.Text = "Location";
								txtOldValue.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSTREETNUM) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSTREET));
								txtNewValue.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREETNUM) + " " + frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREET));
								break;
							}
						case modGridConstantsXF.CNSTOLDACREAGE:
							{
								txtFieldName.Text = "Acreage";
								txtOldValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDACREAGE)), "#,###,##0.00");
								txtNewValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDACREAGE)), "#,###,##0.00");
								break;
							}
						case modGridConstantsXF.CNSTORIGINALLAND:
							{
								txtFieldName.Text = "Land Value";
								txtOldValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALLAND)), "#,###,###,##0");
								txtNewValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDVALUE)), "#,###,###,##0");
								break;
							}
						case modGridConstantsXF.CNSTORIGINALBLDG:
							{
								txtFieldName.Text = "Building Value";
								txtOldValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALBLDG)), "#,###,###,##0");
								txtNewValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTBLDGVALUE)), "#,###,###,##0");
								break;
							}
						case modGridConstantsXF.CNSTORIGINALEXEMPTION:
							{
								txtFieldName.Text = "Exempt Value";
								txtOldValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALEXEMPTION)), "#,###,###,##0");
								txtNewValue.Text = Strings.Format(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3)), "#,###,###,##0");
								break;
							}
					}
					//end switch
					intCurCol = GetNextCol(ref intCurCol);
					while (!IsAChange(ref intCurCol) && intCurCol >= 0)
					{
						intCurCol = GetNextCol(ref intCurCol);
					}
					if (intCurCol == -1)
					{
						lngCRow += 1;
					}
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private int GetFirstCol()
		{
			int GetFirstCol = 0;
			GetFirstCol = modGridConstantsXF.CNSTOLDOWNER;
			return GetFirstCol;
		}

		private int GetNextCol(ref int intColNum)
		{
			int GetNextCol = 0;
			switch (intColNum)
			{
				case modGridConstantsXF.CNSTOLDOWNER:
					{
						GetNextCol = modGridConstantsXF.CNSTOLDSECOWNER;
						break;
					}
				case modGridConstantsXF.CNSTOLDSECOWNER:
					{
						GetNextCol = modGridConstantsXF.CNSTOLDADDRESS1;
						break;
					}
				case modGridConstantsXF.CNSTOLDADDRESS1:
					{
						GetNextCol = modGridConstantsXF.CNSTOLDADDRESS2;
						break;
					}
				case modGridConstantsXF.CNSTOLDADDRESS2:
					{
						GetNextCol = modGridConstantsXF.CNSTOLDCITY;
						break;
					}
				case modGridConstantsXF.CNSTOLDCITY:
					{
						GetNextCol = modGridConstantsXF.CNSTOLDSTREET;
						break;
					}
				case modGridConstantsXF.CNSTOLDSTREET:
					{
						// If Not boolInfoOnly Then
						GetNextCol = modGridConstantsXF.CNSTOLDACREAGE;
						// Else
						// GetNextCol = -1
						// End If
						break;
					}
				case modGridConstantsXF.CNSTOLDACREAGE:
					{
						if (!boolInfoOnly)
						{
							GetNextCol = modGridConstantsXF.CNSTORIGINALLAND;
						}
						else
						{
							GetNextCol = -1;
						}
						break;
					}
				case modGridConstantsXF.CNSTORIGINALLAND:
					{
						if (!boolInfoOnly)
						{
							GetNextCol = modGridConstantsXF.CNSTORIGINALBLDG;
						}
						else
						{
							GetNextCol = -1;
						}
						break;
					}
				case modGridConstantsXF.CNSTORIGINALBLDG:
					{
						if (!boolInfoOnly)
						{
							GetNextCol = modGridConstantsXF.CNSTORIGINALEXEMPTION;
						}
						else
						{
							GetNextCol = -1;
						}
						break;
					}
				case modGridConstantsXF.CNSTORIGINALEXEMPTION:
					{
						GetNextCol = -1;
						break;
					}
			}
			//end switch
			return GetNextCol;
		}

		private bool IsAChange(ref int intColNum)
		{
			bool IsAChange = false;
			IsAChange = false;
			switch (intColNum)
			{
				case modGridConstantsXF.CNSTOLDOWNER:
					{
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDOWNER)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOWNER)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTOLDSECOWNER:
					{
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSECOWNER)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSECOWNER)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTOLDADDRESS1:
					{
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDADDRESS1)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTADDRESS1)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTOLDADDRESS2:
					{
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDADDRESS2)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTADDRESS2)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTOLDCITY:
					{
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDCITY)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTCITY)))
							IsAChange = true;
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSTATE)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTATE)))
							IsAChange = true;
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDZIP)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTZIP)))
							IsAChange = true;
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDZIP4)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTZIP4)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTOLDSTREET:
					{
						if (fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSTREET)) != fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREET)))
							IsAChange = true;
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDSTREETNUM)) != Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREETNUM)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTOLDACREAGE:
					{
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOLDACREAGE)) != Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDACREAGE)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTORIGINALLAND:
					{
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALLAND)) != Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDVALUE)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTORIGINALBLDG:
					{
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALBLDG)) != Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTBLDGVALUE)))
							IsAChange = true;
						break;
					}
				case modGridConstantsXF.CNSTORIGINALEXEMPTION:
					{
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALEXEMPTION)) != (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3))))
							IsAChange = true;
						break;
					}
			}
			//end switch
			return IsAChange;
		}

		
	}
}
