﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for dlgTaxMessage.
	/// </summary>
	partial class dlgTaxMessage : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtNotice;
		public fecherFoundation.FCRichTextBox RichTextBox2;
		public fecherFoundation.FCRichTextBox RichTextBox1;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCTextBox Text21;
		public fecherFoundation.FCTextBox Text20;
		public fecherFoundation.FCTextBox Text19;
		public fecherFoundation.FCTextBox Text18;
		public fecherFoundation.FCTextBox txtHeading;
		public fecherFoundation.FCTextBox Text16;
		public fecherFoundation.FCTextBox txtNotice_14;
		public fecherFoundation.FCTextBox txtNotice_13;
		public fecherFoundation.FCTextBox txtNotice_12;
		public fecherFoundation.FCTextBox txtNotice_11;
		public fecherFoundation.FCTextBox txtNotice_10;
		public fecherFoundation.FCTextBox txtNotice_9;
		public fecherFoundation.FCTextBox txtNotice_8;
		public fecherFoundation.FCTextBox txtNotice_7;
		public fecherFoundation.FCLine LineTest;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuDone;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgTaxMessage));
			//FC:FINAL:CHN - issue #1468: Incorrect showing report title and not setted max lines count.
			this.components = new System.ComponentModel.Container();
			this.clientEventKeyPressCheckLinesCount = new JavaScript.ClientEvent();
			this.javaScriptCheckLinesCount = new Wisej.Web.JavaScript(this.components);
			clientEventKeyPressCheckLinesCount.Event = "keypress";
			clientEventKeyPressCheckLinesCount.JavaScript = resources.GetString("JavaScript.CheckLinesCount");
			this.RichTextBox2 = new fecherFoundation.FCRichTextBox();
			this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
			this.Text1 = new fecherFoundation.FCTextBox();
			this.Text21 = new fecherFoundation.FCTextBox();
			this.Text20 = new fecherFoundation.FCTextBox();
			this.Text19 = new fecherFoundation.FCTextBox();
			this.Text18 = new fecherFoundation.FCTextBox();
			this.txtHeading = new fecherFoundation.FCTextBox();
			this.Text16 = new fecherFoundation.FCTextBox();
			this.txtNotice_14 = new fecherFoundation.FCTextBox();
			this.txtNotice_13 = new fecherFoundation.FCTextBox();
			this.txtNotice_12 = new fecherFoundation.FCTextBox();
			this.txtNotice_11 = new fecherFoundation.FCTextBox();
			this.txtNotice_10 = new fecherFoundation.FCTextBox();
			this.txtNotice_9 = new fecherFoundation.FCTextBox();
			this.txtNotice_8 = new fecherFoundation.FCTextBox();
			this.txtNotice_7 = new fecherFoundation.FCTextBox();
			this.LineTest = new fecherFoundation.FCLine();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDone = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.cmdDone = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdDone);
			this.BottomPanel.Location = new System.Drawing.Point(0, 708);
			this.BottomPanel.Size = new System.Drawing.Size(994, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.RichTextBox2);
			this.ClientArea.Controls.Add(this.RichTextBox1);
			this.ClientArea.Controls.Add(this.Text1);
			this.ClientArea.Controls.Add(this.Text21);
			this.ClientArea.Controls.Add(this.Text20);
			this.ClientArea.Controls.Add(this.Text19);
			this.ClientArea.Controls.Add(this.Text18);
			this.ClientArea.Controls.Add(this.txtHeading);
			this.ClientArea.Controls.Add(this.Text16);
			this.ClientArea.Controls.Add(this.txtNotice_14);
			this.ClientArea.Controls.Add(this.txtNotice_13);
			this.ClientArea.Controls.Add(this.txtNotice_12);
			this.ClientArea.Controls.Add(this.txtNotice_11);
			this.ClientArea.Controls.Add(this.txtNotice_10);
			this.ClientArea.Controls.Add(this.txtNotice_9);
			this.ClientArea.Controls.Add(this.txtNotice_8);
			this.ClientArea.Controls.Add(this.txtNotice_7);
			this.ClientArea.Controls.Add(this.LineTest);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(994, 648);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(994, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(129, 30);
			this.HeaderText.Text = "Tax Notice";
			// 
			// RichTextBox2
			// 
			this.RichTextBox2.Location = new System.Drawing.Point(351, 414);
			this.RichTextBox2.Multiline = true;
			this.RichTextBox2.Name = "RichTextBox2";
			this.RichTextBox2.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.RichTextBox2.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.RichTextBox2.SelTabCount = null;
			this.RichTextBox2.Size = new System.Drawing.Size(469, 155);
			this.RichTextBox2.TabIndex = 17;
			this.RichTextBox2.KeyDown += new Wisej.Web.KeyEventHandler(this.RichTextBox2_KeyDown);
			// 
			// RichTextBox1
			// 
			this.RichTextBox1.Location = new System.Drawing.Point(20, 128);
			this.RichTextBox1.Multiline = true;
			this.RichTextBox1.Name = "RichTextBox1";
			this.RichTextBox1.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.RichTextBox1.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.RichTextBox1.SelTabCount = null;
			this.RichTextBox1.Size = new System.Drawing.Size(800, 276);
			this.RichTextBox1.TabIndex = 16;
			this.RichTextBox1.KeyDown += new Wisej.Web.KeyEventHandler(this.RichTextBox1_KeyDown);
			this.javaScriptCheckLinesCount.GetJavaScriptEvents(this.RichTextBox1).Add(clientEventKeyPressCheckLinesCount);
			// 
			// Text1
			// 
			this.Text1.AutoSize = false;
			this.Text1.BackColor = System.Drawing.SystemColors.Window;
			this.Text1.Enabled = false;
			this.Text1.LinkItem = null;
			this.Text1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text1.LinkTopic = null;
			this.Text1.Location = new System.Drawing.Point(20, 414);
			this.Text1.Name = "Text1";
			this.Text1.Size = new System.Drawing.Size(313, 40);
			this.Text1.TabIndex = 15;
			this.Text1.Text = "Acct 000000 Map/Lot 000-000";
			// 
			// Text21
			// 
			this.Text21.AutoSize = false;
			this.Text21.BackColor = System.Drawing.SystemColors.Window;
			this.Text21.Enabled = false;
			this.Text21.LinkItem = null;
			this.Text21.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text21.LinkTopic = null;
			this.Text21.Location = new System.Drawing.Point(20, 664);
			this.Text21.Name = "Text21";
			this.Text21.Size = new System.Drawing.Size(313, 40);
			this.Text21.TabIndex = 14;
			this.Text21.Text = "City, State  Zip";
			// 
			// Text20
			// 
			this.Text20.AutoSize = false;
			this.Text20.BackColor = System.Drawing.SystemColors.Window;
			this.Text20.Enabled = false;
			this.Text20.LinkItem = null;
			this.Text20.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text20.LinkTopic = null;
			this.Text20.Location = new System.Drawing.Point(20, 614);
			this.Text20.Name = "Text20";
			this.Text20.Size = new System.Drawing.Size(313, 40);
			this.Text20.TabIndex = 13;
			this.Text20.Text = "Address 2";
			// 
			// Text19
			// 
			this.Text19.AutoSize = false;
			this.Text19.BackColor = System.Drawing.SystemColors.Window;
			this.Text19.Enabled = false;
			this.Text19.LinkItem = null;
			this.Text19.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text19.LinkTopic = null;
			this.Text19.Location = new System.Drawing.Point(20, 564);
			this.Text19.Name = "Text19";
			this.Text19.Size = new System.Drawing.Size(313, 40);
			this.Text19.TabIndex = 12;
			this.Text19.Text = "Address 1";
			// 
			// Text18
			// 
			this.Text18.AutoSize = false;
			this.Text18.BackColor = System.Drawing.SystemColors.Window;
			this.Text18.Enabled = false;
			this.Text18.LinkItem = null;
			this.Text18.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text18.LinkTopic = null;
			this.Text18.Location = new System.Drawing.Point(20, 514);
			this.Text18.Name = "Text18";
			this.Text18.Size = new System.Drawing.Size(313, 40);
			this.Text18.TabIndex = 11;
			this.Text18.Text = "Name";
			// 
			// txtHeading
			// 
			this.txtHeading.AutoSize = false;
			this.txtHeading.BackColor = System.Drawing.SystemColors.Window;
			this.txtHeading.LinkItem = null;
			this.txtHeading.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHeading.LinkTopic = null;
			this.txtHeading.Location = new System.Drawing.Point(20, 68);
			this.txtHeading.MaxLength = 75;
			this.txtHeading.Name = "txtHeading";
			this.txtHeading.Size = new System.Drawing.Size(800, 40);
			this.txtHeading.TabIndex = 0;
			this.txtHeading.KeyDown += new Wisej.Web.KeyEventHandler(this.txtHeading_KeyDown);
			this.txtHeading.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHeading_KeyPress);
			this.txtHeading.Enter += new System.EventHandler(this.txtHeading_Enter);
			// 
			// Text16
			// 
			this.Text16.AutoSize = false;
			this.Text16.BackColor = System.Drawing.SystemColors.Window;
			this.Text16.Enabled = false;
			this.Text16.LinkItem = null;
			this.Text16.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text16.LinkTopic = null;
			this.Text16.Location = new System.Drawing.Point(20, 464);
			this.Text16.Name = "Text16";
			this.Text16.Size = new System.Drawing.Size(313, 40);
			this.Text16.TabIndex = 9;
			this.Text16.Text = "Location";
			// 
			// txtNotice_14
			// 
			this.txtNotice_14.Appearance = 0;
			this.txtNotice_14.AutoSize = false;
			this.txtNotice_14.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_14.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_14.Enabled = false;
			this.txtNotice_14.LinkItem = null;
			this.txtNotice_14.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_14.LinkTopic = null;
			this.txtNotice_14.Location = new System.Drawing.Point(352, 764);
			this.txtNotice_14.Name = "txtNotice_14";
			this.txtNotice_14.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_14.TabIndex = 8;
			// 
			// txtNotice_13
			// 
			this.txtNotice_13.Appearance = 0;
			this.txtNotice_13.AutoSize = false;
			this.txtNotice_13.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_13.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_13.Enabled = false;
			this.txtNotice_13.LinkItem = null;
			this.txtNotice_13.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_13.LinkTopic = null;
			this.txtNotice_13.Location = new System.Drawing.Point(351, 714);
			this.txtNotice_13.Name = "txtNotice_13";
			this.txtNotice_13.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_13.TabIndex = 7;
			// 
			// txtNotice_12
			// 
			this.txtNotice_12.Appearance = 0;
			this.txtNotice_12.AutoSize = false;
			this.txtNotice_12.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_12.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_12.Enabled = false;
			this.txtNotice_12.LinkItem = null;
			this.txtNotice_12.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_12.LinkTopic = null;
			this.txtNotice_12.Location = new System.Drawing.Point(351, 664);
			this.txtNotice_12.Name = "txtNotice_12";
			this.txtNotice_12.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_12.TabIndex = 6;
			// 
			// txtNotice_11
			// 
			this.txtNotice_11.Appearance = 0;
			this.txtNotice_11.AutoSize = false;
			this.txtNotice_11.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_11.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_11.Enabled = false;
			this.txtNotice_11.LinkItem = null;
			this.txtNotice_11.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_11.LinkTopic = null;
			this.txtNotice_11.Location = new System.Drawing.Point(351, 614);
			this.txtNotice_11.Name = "txtNotice_11";
			this.txtNotice_11.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_11.TabIndex = 5;
			// 
			// txtNotice_10
			// 
			this.txtNotice_10.Appearance = 0;
			this.txtNotice_10.AutoSize = false;
			this.txtNotice_10.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_10.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_10.Enabled = false;
			this.txtNotice_10.LinkItem = null;
			this.txtNotice_10.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_10.LinkTopic = null;
			this.txtNotice_10.Location = new System.Drawing.Point(351, 564);
			this.txtNotice_10.Name = "txtNotice_10";
			this.txtNotice_10.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_10.TabIndex = 4;
			// 
			// txtNotice_9
			// 
			this.txtNotice_9.Appearance = 0;
			this.txtNotice_9.AutoSize = false;
			this.txtNotice_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_9.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_9.Enabled = false;
			this.txtNotice_9.LinkItem = null;
			this.txtNotice_9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_9.LinkTopic = null;
			this.txtNotice_9.Location = new System.Drawing.Point(351, 514);
			this.txtNotice_9.Name = "txtNotice_9";
			this.txtNotice_9.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_9.TabIndex = 3;
			// 
			// txtNotice_8
			// 
			this.txtNotice_8.Appearance = 0;
			this.txtNotice_8.AutoSize = false;
			this.txtNotice_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_8.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_8.Enabled = false;
			this.txtNotice_8.LinkItem = null;
			this.txtNotice_8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_8.LinkTopic = null;
			this.txtNotice_8.Location = new System.Drawing.Point(351, 464);
			this.txtNotice_8.Name = "txtNotice_8";
			this.txtNotice_8.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_8.TabIndex = 2;
			// 
			// txtNotice_7
			// 
			this.txtNotice_7.Appearance = 0;
			this.txtNotice_7.AutoSize = false;
			this.txtNotice_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_7.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtNotice_7.Enabled = false;
			this.txtNotice_7.LinkItem = null;
			this.txtNotice_7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_7.LinkTopic = null;
			this.txtNotice_7.Location = new System.Drawing.Point(352, 414);
			this.txtNotice_7.MaxLength = 48;
			this.txtNotice_7.Name = "txtNotice_7";
			this.txtNotice_7.Size = new System.Drawing.Size(469, 40);
			this.txtNotice_7.TabIndex = 1;
			// 
			// LineTest
			// 
			this.LineTest.BorderColor = System.Drawing.SystemColors.WindowText;
			this.LineTest.BorderWidth = ((short)(1));
			this.LineTest.LineWidth = 0;
			this.LineTest.Location = new System.Drawing.Point(0, 0);
			this.LineTest.Name = "LineTest";
			this.LineTest.Size = new System.Drawing.Size(1440, 1);
			this.LineTest.Visible = false;
			this.LineTest.X1 = 0F;
			this.LineTest.X2 = 1440F;
			this.LineTest.Y1 = 0F;
			this.LineTest.Y2 = 0F;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(120, 18);
			this.Label1.TabIndex = 10;
			this.Label1.Text = "HEADING OR TITLE";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuSepar2,
				this.mnuDone,
				this.mnuSep,
				this.mnuCancel
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "New";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuDone
			// 
			this.mnuDone.Index = 2;
			this.mnuDone.Name = "mnuDone";
			this.mnuDone.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuDone.Text = "Save && Continue";
			this.mnuDone.Click += new System.EventHandler(this.mnuDone_Click);
			// 
			// mnuSep
			// 
			this.mnuSep.Index = 3;
			this.mnuSep.Name = "mnuSep";
			this.mnuSep.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 4;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Exit";
			this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "acceptButton";
			this.cmdDone.Location = new System.Drawing.Point(318, 30);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdDone.Size = new System.Drawing.Size(153, 48);
			this.cmdDone.TabIndex = 0;
			this.cmdDone.Text = "Save & Continue";
			this.cmdDone.Click += new System.EventHandler(this.mnuDone_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(918, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(48, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "New";
			this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// dlgTaxMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(994, 816);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "dlgTaxMessage";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Tax Notice";
			this.Load += new System.EventHandler(this.dlgTaxMessage_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.dlgTaxMessage_KeyDown);
			this.Resize += new System.EventHandler(this.dlgTaxMessage_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDone;
		private FCButton cmdClear;
		//FC:FINAL:CHN - issue #1468: Incorrect showing report title and not setted max lines count.
		private JavaScript javaScriptCheckLinesCount;
		private Wisej.Web.JavaScript.ClientEvent clientEventKeyPressCheckLinesCount;
	}
}
