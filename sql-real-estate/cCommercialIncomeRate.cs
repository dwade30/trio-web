﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialIncomeRate
	{
		//=========================================================
		private int lngRecordID;
		private int lngOccupancyCodeNumber;
		private double dblRate;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
		// Public Property Let CommercialOccupancyCodeID(ByVal lngID As Long)
		// lngOccupancyCodeID = lngID
		// IsUpdated = True
		// End Property
		//
		// Public Property Get CommercialOccupancyCodeID() As Long
		// CommercialOccupancyCodeID = lngOccupancyCodeID
		// End Property
		public double Rate
		{
			set
			{
				dblRate = value;
				IsUpdated = true;
			}
			get
			{
				double Rate = 0;
				Rate = dblRate;
				return Rate;
			}
		}

		public int CommercialOccupancyCodeNumber
		{
			set
			{
				lngOccupancyCodeNumber = value;
				IsUpdated = true;
			}
			get
			{
				int CommercialOccupancyCodeNumber = 0;
				CommercialOccupancyCodeNumber = lngOccupancyCodeNumber;
				return CommercialOccupancyCodeNumber;
			}
		}
	}
}
