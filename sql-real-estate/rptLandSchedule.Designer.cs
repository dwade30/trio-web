﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLandSchedule.
	/// </summary>
	partial class rptLandSchedule
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLandSchedule));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExponent1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExponent2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWidthExponent1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWidthExponent2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblScheduleNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStandardDepth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStandardLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStandardWidth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExponent1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExponent2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWidthExponent1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWidthExponent2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScheduleNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStandardDepth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStandardLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStandardWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCode,
				this.txtAmount,
				this.txtExponent1,
				this.txtExponent2,
				this.txtType,
				this.txtDescription,
				this.txtWidthExponent1,
				this.txtWidthExponent2
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.19F;
			this.txtCode.Left = 0F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "text-align: right";
			this.txtCode.Text = null;
			this.txtCode.Top = 0F;
			this.txtCode.Width = 0.5F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.19F;
			this.txtAmount.Left = 4.680555F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.9166667F;
			// 
			// txtExponent1
			// 
			this.txtExponent1.Height = 0.19F;
			this.txtExponent1.Left = 5.645833F;
			this.txtExponent1.Name = "txtExponent1";
			this.txtExponent1.Style = "text-align: right";
			this.txtExponent1.Text = null;
			this.txtExponent1.Top = 0F;
			this.txtExponent1.Width = 0.59375F;
			// 
			// txtExponent2
			// 
			this.txtExponent2.Height = 0.19F;
			this.txtExponent2.Left = 6.340278F;
			this.txtExponent2.Name = "txtExponent2";
			this.txtExponent2.Style = "text-align: right";
			this.txtExponent2.Text = null;
			this.txtExponent2.Top = 0F;
			this.txtExponent2.Width = 0.4583333F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.19F;
			this.txtType.Left = 0.5555556F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "text-align: left";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 1.416667F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.19F;
			this.txtDescription.Left = 2.041667F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "text-align: left";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 2.583333F;
			// 
			// txtWidthExponent1
			// 
			this.txtWidthExponent1.Height = 0.19F;
			this.txtWidthExponent1.Left = 6.895833F;
			this.txtWidthExponent1.Name = "txtWidthExponent1";
			this.txtWidthExponent1.Style = "text-align: right";
			this.txtWidthExponent1.Text = null;
			this.txtWidthExponent1.Top = 0F;
			this.txtWidthExponent1.Width = 0.5104167F;
			// 
			// txtWidthExponent2
			// 
			this.txtWidthExponent2.Height = 0.19F;
			this.txtWidthExponent2.Left = 7.458333F;
			this.txtWidthExponent2.Name = "txtWidthExponent2";
			this.txtWidthExponent2.Style = "text-align: right";
			this.txtWidthExponent2.Text = null;
			this.txtWidthExponent2.Top = 0F;
			this.txtWidthExponent2.Width = 0.4583333F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0.3645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblScheduleNumber,
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label11,
				this.Label12
			});
			this.GroupHeader1.Height = 0.8645833F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// lblScheduleNumber
			// 
			this.lblScheduleNumber.Height = 0.1875F;
			this.lblScheduleNumber.HyperLink = null;
			this.lblScheduleNumber.Left = 2F;
			this.lblScheduleNumber.Name = "lblScheduleNumber";
			this.lblScheduleNumber.Style = "text-align: center";
			this.lblScheduleNumber.Text = null;
			this.lblScheduleNumber.Top = 0.25F;
			this.lblScheduleNumber.Width = 3.625F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.583333F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1666667F;
			this.txtPage.Width = 0.9166667F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.916667F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Land Schedule";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 1.75F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0F;
			this.txtMuni.Width = 2.916667F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1666667F;
			this.txtTime.Width = 1.416667F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.166667F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.333333F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Code";
			this.Label1.Top = 0.6666667F;
			this.Label1.Width = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.041667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: left";
			this.Label2.Text = "Description";
			this.Label2.Top = 0.6666667F;
			this.Label2.Width = 2.583333F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.5520833F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "Type";
			this.Label3.Top = 0.6666667F;
			this.Label3.Width = 1.416667F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.677083F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Amount";
			this.Label4.Top = 0.6666667F;
			this.Label4.Width = 0.9166667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.645833F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "<= Std";
			this.Label5.Top = 0.6666667F;
			this.Label5.Width = 0.59375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.34375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "> Std";
			this.Label6.Top = 0.6666667F;
			this.Label6.Width = 0.4583333F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.166667F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: center";
			this.Label7.Text = "Exponents";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 0.8333333F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.833333F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Text = "<= Wid";
			this.Label11.Top = 0.6666667F;
			this.Label11.Width = 0.5729167F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 7.458333F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Text = "> Wid";
			this.Label12.Top = 0.6666667F;
			this.Label12.Width = 0.4583333F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanGrow = false;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8,
				this.Label9,
				this.txtStandardDepth,
				this.txtStandardLot,
				this.Label10,
				this.txtStandardWidth
			});
			this.GroupFooter1.Height = 0.625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.916667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: left";
			this.Label8.Text = "Standard Depth";
			this.Label8.Top = 0.08333334F;
			this.Label8.Width = 1.25F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.916667F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: left";
			this.Label9.Text = "Standard Lot";
			this.Label9.Top = 0.25F;
			this.Label9.Width = 1.25F;
			// 
			// txtStandardDepth
			// 
			this.txtStandardDepth.Height = 0.19F;
			this.txtStandardDepth.Left = 3.166667F;
			this.txtStandardDepth.Name = "txtStandardDepth";
			this.txtStandardDepth.Style = "text-align: right";
			this.txtStandardDepth.Text = null;
			this.txtStandardDepth.Top = 0.08333334F;
			this.txtStandardDepth.Width = 0.5833333F;
			// 
			// txtStandardLot
			// 
			this.txtStandardLot.Height = 0.19F;
			this.txtStandardLot.Left = 3.166667F;
			this.txtStandardLot.Name = "txtStandardLot";
			this.txtStandardLot.Style = "text-align: right";
			this.txtStandardLot.Text = null;
			this.txtStandardLot.Top = 0.25F;
			this.txtStandardLot.Width = 0.5833333F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.916667F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: left";
			this.Label10.Text = "Standard Width";
			this.Label10.Top = 0.4173611F;
			this.Label10.Width = 1.25F;
			// 
			// txtStandardWidth
			// 
			this.txtStandardWidth.Height = 0.19F;
			this.txtStandardWidth.Left = 3.166667F;
			this.txtStandardWidth.Name = "txtStandardWidth";
			this.txtStandardWidth.Style = "text-align: right";
			this.txtStandardWidth.Text = null;
			this.txtStandardWidth.Top = 0.4173611F;
			this.txtStandardWidth.Width = 0.5833333F;
			// 
			// rptLandSchedule
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExponent1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExponent2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWidthExponent1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWidthExponent2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScheduleNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStandardDepth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStandardLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStandardWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExponent1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExponent2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWidthExponent1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWidthExponent2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScheduleNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStandardDepth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStandardLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStandardWidth;
	}
}
