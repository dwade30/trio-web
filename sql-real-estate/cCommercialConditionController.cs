﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cCommercialConditionController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCommercialCondition GetCommercialCondition(int lngID)
		{
			cCommercialCondition GetCommercialCondition = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialCondition commCond = null;
				rsLoad.OpenRecordset("select * from CommercialConditions where id = " + FCConvert.ToString(lngID), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					commCond = new cCommercialCondition();
					commCond.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					commCond.Exponent = rsLoad.Get_Fields_Double("Exponent");
					commCond.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					commCond.High = rsLoad.Get_Fields("High");
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					commCond.CodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields("CodeNumber"));
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					commCond.Low = rsLoad.Get_Fields("Low");
					commCond.IsUpdated = false;
				}
				GetCommercialCondition = commCond;
				return GetCommercialCondition;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialCondition;
		}

		public cCommercialCondition GetCommercialConditionByCode(int lngCode)
		{
			cCommercialCondition GetCommercialConditionByCode = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialCondition commCond = null;
				rsLoad.OpenRecordset("select * from CommercialConditions where codenumber = " + FCConvert.ToString(lngCode), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					commCond = new cCommercialCondition();
					commCond.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					commCond.Exponent = rsLoad.Get_Fields_Double("Exponent");
					commCond.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					commCond.High = rsLoad.Get_Fields("High");
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					commCond.CodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields("CodeNumber"));
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					commCond.Low = rsLoad.Get_Fields("Low");
					commCond.IsUpdated = false;
				}
				GetCommercialConditionByCode = commCond;
				return GetCommercialConditionByCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialConditionByCode;
		}

		public cGenericCollection GetCommercialConditions()
		{
			cGenericCollection GetCommercialConditions = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialCondition commCond;
				rs.OpenRecordset("select * from CommercialConditions order by Description", "RealEstate");
				while (!rs.EndOfFile())
				{
					commCond = new cCommercialCondition();
					commCond.Description = FCConvert.ToString(rs.Get_Fields_String("Description"));
					commCond.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					commCond.Exponent = rs.Get_Fields_Double("Exponent");
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					commCond.High = rs.Get_Fields("High");
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					commCond.Low = rs.Get_Fields("Low");
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					commCond.CodeNumber = FCConvert.ToInt32(rs.Get_Fields("CodeNumber"));
					commCond.IsUpdated = false;
					gColl.AddItem(commCond);
					rs.MoveNext();
				}
				GetCommercialConditions = gColl;
				return GetCommercialConditions;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialConditions;
		}

		public void DeleteCommercialCondition(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialConditions where id = " + FCConvert.ToString(lngID), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialCondition(ref cCommercialCondition commCond)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (commCond.IsDeleted)
				{
					DeleteCommercialCondition(commCond.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialconditions where id = " + commCond.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (commCond.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Commercial condition record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Description", commCond.Description);
				rsSave.Set_Fields("Exponent", commCond.Exponent);
				rsSave.Set_Fields("High", commCond.High);
				rsSave.Set_Fields("Low", commCond.Low);
				rsSave.Set_Fields("CodeNumber", commCond.CodeNumber);
				rsSave.Update();
				commCond.ID = rsSave.Get_Fields_Int32("ID");
				commCond.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialConditions(ref cGenericCollection commConditions)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(commConditions == null))
				{
					cCommercialCondition commCond = null;
					int lngIndex;
					commConditions.MoveFirst();
					while (commConditions.IsCurrent())
					{
						if (commCond.IsUpdated || commCond.IsDeleted)
						{
							SaveCommercialCondition(ref commCond);
						}
						if (HadError)
						{
							return;
						}
						if (commCond.IsDeleted)
						{
							commConditions.RemoveCurrent();
						}
						commConditions.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
