﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExportMasterInfo.
	/// </summary>
	partial class frmExportMasterInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtRange;
		public fecherFoundation.FCComboBox cmbAccounts;
		public fecherFoundation.FCLabel lblAccounts;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame framRange;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkExportAllParties;
		public fecherFoundation.FCCheckBox chkInspection;
		public fecherFoundation.FCCheckBox chkPictures;
		public fecherFoundation.FCCheckBox chkInterestedParties;
		public fecherFoundation.FCCheckBox chkPreviousOwners;
		public fecherFoundation.FCCheckBox chkCommercial;
		public fecherFoundation.FCCheckBox chkDwelling;
		public fecherFoundation.FCCheckBox chkLocation;
		public fecherFoundation.FCCheckBox chkExemptInfo;
		public fecherFoundation.FCCheckBox chkSaleData;
		public fecherFoundation.FCCheckBox chkPropertyInfo;
		public fecherFoundation.FCCheckBox chkLandInformation;
		public fecherFoundation.FCCheckBox chkBookPage;
		public fecherFoundation.FCCheckBox chkBankruptcy;
		public fecherFoundation.FCCheckBox chkTaxAcquired;
		public fecherFoundation.FCCheckBox ChkTranLandBldg;
		public fecherFoundation.FCCheckBox chkRef2;
		public fecherFoundation.FCCheckBox ChkRef1;
		public fecherFoundation.FCCheckBox chkMapLot;
		public fecherFoundation.FCCheckBox ChkNameAddress;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExportMasterInfo));
            this.cmbtRange = new fecherFoundation.FCComboBox();
            this.cmbAccounts = new fecherFoundation.FCComboBox();
            this.lblAccounts = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.framRange = new fecherFoundation.FCFrame();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkDeedNames = new fecherFoundation.FCCheckBox();
            this.chkOutbuilding = new fecherFoundation.FCCheckBox();
            this.chkExportAllParties = new fecherFoundation.FCCheckBox();
            this.chkInspection = new fecherFoundation.FCCheckBox();
            this.chkPictures = new fecherFoundation.FCCheckBox();
            this.chkInterestedParties = new fecherFoundation.FCCheckBox();
            this.chkPreviousOwners = new fecherFoundation.FCCheckBox();
            this.chkCommercial = new fecherFoundation.FCCheckBox();
            this.chkDwelling = new fecherFoundation.FCCheckBox();
            this.chkLocation = new fecherFoundation.FCCheckBox();
            this.chkExemptInfo = new fecherFoundation.FCCheckBox();
            this.chkSaleData = new fecherFoundation.FCCheckBox();
            this.chkPropertyInfo = new fecherFoundation.FCCheckBox();
            this.chkLandInformation = new fecherFoundation.FCCheckBox();
            this.chkBookPage = new fecherFoundation.FCCheckBox();
            this.chkBankruptcy = new fecherFoundation.FCCheckBox();
            this.chkTaxAcquired = new fecherFoundation.FCCheckBox();
            this.ChkTranLandBldg = new fecherFoundation.FCCheckBox();
            this.chkRef2 = new fecherFoundation.FCCheckBox();
            this.ChkRef1 = new fecherFoundation.FCCheckBox();
            this.chkMapLot = new fecherFoundation.FCCheckBox();
            this.ChkNameAddress = new fecherFoundation.FCCheckBox();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new Wisej.Web.Button();
            this.cmdCopy = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
            this.framRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeedNames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutbuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportAllParties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPictures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterestedParties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousOwners)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCommercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDwelling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExemptInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaleData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTranLandBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRef2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNameAddress)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 736);
            this.BottomPanel.Size = new System.Drawing.Size(888, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(908, 628);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCopy);
            this.TopPanel.Size = new System.Drawing.Size(908, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCopy, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(191, 28);
            this.HeaderText.Text = "Export Name Etc.";
            // 
            // cmbtRange
            // 
            this.cmbtRange.Items.AddRange(new object[] {
            "Account",
            "Name",
            "Map / Lot",
            "Location"});
            this.cmbtRange.Location = new System.Drawing.Point(10, 166);
            this.cmbtRange.Name = "cmbtRange";
            this.cmbtRange.Size = new System.Drawing.Size(268, 40);
            this.cmbtRange.TabIndex = 23;
            this.cmbtRange.Text = "Account";
            // 
            // cmbAccounts
            // 
            this.cmbAccounts.Items.AddRange(new object[] {
            "All",
            "Range"});
            this.cmbAccounts.Location = new System.Drawing.Point(168, 30);
            this.cmbAccounts.Name = "cmbAccounts";
            this.cmbAccounts.Size = new System.Drawing.Size(120, 40);
            this.cmbAccounts.TabIndex = 31;
            this.cmbAccounts.Text = "All";
            this.cmbAccounts.SelectedIndexChanged += new System.EventHandler(this.optAccounts_CheckedChanged);
            // 
            // lblAccounts
            // 
            this.lblAccounts.AutoSize = true;
            this.lblAccounts.Location = new System.Drawing.Point(20, 44);
            this.lblAccounts.Name = "lblAccounts";
            this.lblAccounts.Size = new System.Drawing.Size(115, 16);
            this.lblAccounts.TabIndex = 32;
            this.lblAccounts.Text = "ACCOUNTS TYPE";
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxLeftBorder";
            this.Frame2.Controls.Add(this.framRange);
            this.Frame2.Controls.Add(this.cmbAccounts);
            this.Frame2.Controls.Add(this.lblAccounts);
            this.Frame2.Location = new System.Drawing.Point(576, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(289, 306);
            this.Frame2.TabIndex = 29;
            this.Frame2.Text = "Accounts";
            // 
            // framRange
            // 
            this.framRange.AppearanceKey = "groupBoxNoBorders";
            this.framRange.Controls.Add(this.txtStart);
            this.framRange.Controls.Add(this.cmbtRange);
            this.framRange.Controls.Add(this.txtEnd);
            this.framRange.Controls.Add(this.Label2);
            this.framRange.Controls.Add(this.Label1);
            this.framRange.Location = new System.Drawing.Point(10, 80);
            this.framRange.Name = "framRange";
            this.framRange.Size = new System.Drawing.Size(278, 226);
            this.framRange.TabIndex = 30;
            this.framRange.Visible = false;
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(10, 10);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(268, 40);
            this.txtStart.TabIndex = 22;
            // 
            // txtEnd
            // 
            this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnd.Location = new System.Drawing.Point(10, 88);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(268, 40);
            this.txtEnd.TabIndex = 23;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(135, 138);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(20, 18);
            this.Label2.TabIndex = 32;
            this.Label2.Text = "OF";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(135, 64);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(20, 18);
            this.Label1.TabIndex = 31;
            this.Label1.Text = "TO";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkDeedNames);
            this.Frame1.Controls.Add(this.chkOutbuilding);
            this.Frame1.Controls.Add(this.chkExportAllParties);
            this.Frame1.Controls.Add(this.chkInspection);
            this.Frame1.Controls.Add(this.chkPictures);
            this.Frame1.Controls.Add(this.chkInterestedParties);
            this.Frame1.Controls.Add(this.chkPreviousOwners);
            this.Frame1.Controls.Add(this.chkCommercial);
            this.Frame1.Controls.Add(this.chkDwelling);
            this.Frame1.Controls.Add(this.chkLocation);
            this.Frame1.Controls.Add(this.chkExemptInfo);
            this.Frame1.Controls.Add(this.chkSaleData);
            this.Frame1.Controls.Add(this.chkPropertyInfo);
            this.Frame1.Controls.Add(this.chkLandInformation);
            this.Frame1.Controls.Add(this.chkBookPage);
            this.Frame1.Controls.Add(this.chkBankruptcy);
            this.Frame1.Controls.Add(this.chkTaxAcquired);
            this.Frame1.Controls.Add(this.ChkTranLandBldg);
            this.Frame1.Controls.Add(this.chkRef2);
            this.Frame1.Controls.Add(this.ChkRef1);
            this.Frame1.Controls.Add(this.chkMapLot);
            this.Frame1.Controls.Add(this.ChkNameAddress);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(516, 706);
            this.Frame1.TabIndex = 28;
            this.Frame1.Text = "Data To Export";
            // 
            // chkDeedNames
            // 
            this.chkDeedNames.Checked = true;
            this.chkDeedNames.CheckState = Wisej.Web.CheckState.Checked;
            this.chkDeedNames.Location = new System.Drawing.Point(20, 67);
            this.chkDeedNames.Name = "chkDeedNames";
            this.chkDeedNames.Size = new System.Drawing.Size(110, 23);
            this.chkDeedNames.TabIndex = 1;
            this.chkDeedNames.Text = "Deed Names";
            // 
            // chkOutbuilding
            // 
            this.chkOutbuilding.Location = new System.Drawing.Point(306, 30);
            this.chkOutbuilding.Name = "chkOutbuilding";
            this.chkOutbuilding.Size = new System.Drawing.Size(104, 23);
            this.chkOutbuilding.TabIndex = 18;
            this.chkOutbuilding.Text = "Outbuilding";
            // 
            // chkExportAllParties
            // 
            this.chkExportAllParties.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.chkExportAllParties.Location = new System.Drawing.Point(306, 140);
            this.chkExportAllParties.Name = "chkExportAllParties";
            this.chkExportAllParties.Size = new System.Drawing.Size(140, 23);
            this.chkExportAllParties.TabIndex = 21;
            this.chkExportAllParties.Text = "Export All Parties";
            this.ToolTip1.SetToolTip(this.chkExportAllParties, "If unchecked, only parties associated with accounts in this export will be export" +
        "ed");
            // 
            // chkInspection
            // 
            this.chkInspection.Location = new System.Drawing.Point(306, 104);
            this.chkInspection.Name = "chkInspection";
            this.chkInspection.Size = new System.Drawing.Size(172, 23);
            this.chkInspection.TabIndex = 20;
            this.chkInspection.Text = "Inspection Information";
            // 
            // chkPictures
            // 
            this.chkPictures.Location = new System.Drawing.Point(306, 67);
            this.chkPictures.Name = "chkPictures";
            this.chkPictures.Size = new System.Drawing.Size(83, 23);
            this.chkPictures.TabIndex = 19;
            this.chkPictures.Text = "Pictures";
            // 
            // chkInterestedParties
            // 
            this.chkInterestedParties.Location = new System.Drawing.Point(20, 584);
            this.chkInterestedParties.Name = "chkInterestedParties";
            this.chkInterestedParties.Size = new System.Drawing.Size(141, 23);
            this.chkInterestedParties.TabIndex = 15;
            this.chkInterestedParties.Text = "Interested Parties";
            // 
            // chkPreviousOwners
            // 
            this.chkPreviousOwners.Location = new System.Drawing.Point(20, 547);
            this.chkPreviousOwners.Name = "chkPreviousOwners";
            this.chkPreviousOwners.Size = new System.Drawing.Size(186, 23);
            this.chkPreviousOwners.TabIndex = 14;
            this.chkPreviousOwners.Text = "Previous Owner Records";
            // 
            // chkCommercial
            // 
            this.chkCommercial.Location = new System.Drawing.Point(20, 658);
            this.chkCommercial.Name = "chkCommercial";
            this.chkCommercial.Size = new System.Drawing.Size(106, 23);
            this.chkCommercial.TabIndex = 17;
            this.chkCommercial.Text = "Commercial";
            // 
            // chkDwelling
            // 
            this.chkDwelling.Location = new System.Drawing.Point(20, 621);
            this.chkDwelling.Name = "chkDwelling";
            this.chkDwelling.Size = new System.Drawing.Size(85, 23);
            this.chkDwelling.TabIndex = 16;
            this.chkDwelling.Text = "Dwelling";
            // 
            // chkLocation
            // 
            this.chkLocation.Checked = true;
            this.chkLocation.CheckState = Wisej.Web.CheckState.Checked;
            this.chkLocation.Location = new System.Drawing.Point(20, 140);
            this.chkLocation.Name = "chkLocation";
            this.chkLocation.Size = new System.Drawing.Size(86, 23);
            this.chkLocation.TabIndex = 3;
            this.chkLocation.Text = "Location";
            // 
            // chkExemptInfo
            // 
            this.chkExemptInfo.Location = new System.Drawing.Point(20, 510);
            this.chkExemptInfo.Name = "chkExemptInfo";
            this.chkExemptInfo.Size = new System.Drawing.Size(154, 23);
            this.chkExemptInfo.TabIndex = 13;
            this.chkExemptInfo.Text = "Exempt Information";
            // 
            // chkSaleData
            // 
            this.chkSaleData.Location = new System.Drawing.Point(20, 473);
            this.chkSaleData.Name = "chkSaleData";
            this.chkSaleData.Size = new System.Drawing.Size(90, 23);
            this.chkSaleData.TabIndex = 12;
            this.chkSaleData.Text = "Sale Data";
            // 
            // chkPropertyInfo
            // 
            this.chkPropertyInfo.Location = new System.Drawing.Point(20, 436);
            this.chkPropertyInfo.Name = "chkPropertyInfo";
            this.chkPropertyInfo.Size = new System.Drawing.Size(160, 23);
            this.chkPropertyInfo.TabIndex = 11;
            this.chkPropertyInfo.Text = "Property Information";
            // 
            // chkLandInformation
            // 
            this.chkLandInformation.Location = new System.Drawing.Point(20, 399);
            this.chkLandInformation.Name = "chkLandInformation";
            this.chkLandInformation.Size = new System.Drawing.Size(138, 23);
            this.chkLandInformation.TabIndex = 10;
            this.chkLandInformation.Text = "Land Information";
            // 
            // chkBookPage
            // 
            this.chkBookPage.Checked = true;
            this.chkBookPage.CheckState = Wisej.Web.CheckState.Checked;
            this.chkBookPage.Location = new System.Drawing.Point(20, 251);
            this.chkBookPage.Name = "chkBookPage";
            this.chkBookPage.Size = new System.Drawing.Size(112, 23);
            this.chkBookPage.TabIndex = 6;
            this.chkBookPage.Text = "Book & Page";
            // 
            // chkBankruptcy
            // 
            this.chkBankruptcy.Location = new System.Drawing.Point(20, 362);
            this.chkBankruptcy.Name = "chkBankruptcy";
            this.chkBankruptcy.Size = new System.Drawing.Size(133, 23);
            this.chkBankruptcy.TabIndex = 9;
            this.chkBankruptcy.Text = "Bankruptcy Flag";
            // 
            // chkTaxAcquired
            // 
            this.chkTaxAcquired.Location = new System.Drawing.Point(20, 325);
            this.chkTaxAcquired.Name = "chkTaxAcquired";
            this.chkTaxAcquired.Size = new System.Drawing.Size(144, 23);
            this.chkTaxAcquired.TabIndex = 8;
            this.chkTaxAcquired.Text = "Tax Acquired Flag";
            // 
            // ChkTranLandBldg
            // 
            this.ChkTranLandBldg.Location = new System.Drawing.Point(20, 288);
            this.ChkTranLandBldg.Name = "ChkTranLandBldg";
            this.ChkTranLandBldg.Size = new System.Drawing.Size(225, 23);
            this.ChkTranLandBldg.TabIndex = 7;
            this.ChkTranLandBldg.Text = "Tran / Land / Bldg / Prop Codes";
            // 
            // chkRef2
            // 
            this.chkRef2.Checked = true;
            this.chkRef2.CheckState = Wisej.Web.CheckState.Checked;
            this.chkRef2.Location = new System.Drawing.Point(20, 214);
            this.chkRef2.Name = "chkRef2";
            this.chkRef2.Size = new System.Drawing.Size(105, 23);
            this.chkRef2.TabIndex = 5;
            this.chkRef2.Text = "Reference 2";
            // 
            // ChkRef1
            // 
            this.ChkRef1.Checked = true;
            this.ChkRef1.CheckState = Wisej.Web.CheckState.Checked;
            this.ChkRef1.Location = new System.Drawing.Point(20, 177);
            this.ChkRef1.Name = "ChkRef1";
            this.ChkRef1.Size = new System.Drawing.Size(105, 23);
            this.ChkRef1.TabIndex = 4;
            this.ChkRef1.Text = "Reference 1";
            // 
            // chkMapLot
            // 
            this.chkMapLot.Checked = true;
            this.chkMapLot.CheckState = Wisej.Web.CheckState.Checked;
            this.chkMapLot.Location = new System.Drawing.Point(20, 103);
            this.chkMapLot.Name = "chkMapLot";
            this.chkMapLot.Size = new System.Drawing.Size(89, 23);
            this.chkMapLot.TabIndex = 2;
            this.chkMapLot.Text = "Map / Lot";
            // 
            // ChkNameAddress
            // 
            this.ChkNameAddress.Checked = true;
            this.ChkNameAddress.CheckState = Wisej.Web.CheckState.Checked;
            this.ChkNameAddress.Location = new System.Drawing.Point(20, 30);
            this.ChkNameAddress.Name = "ChkNameAddress";
            this.ChkNameAddress.Size = new System.Drawing.Size(147, 23);
            this.ChkNameAddress.TabIndex = 22;
            this.ChkNameAddress.Text = "Owner Information";
            this.ChkNameAddress.CheckedChanged += new System.EventHandler(this.ChkNameAddress_CheckedChanged);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(385, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdCopy
            // 
            this.cmdCopy.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCopy.AppearanceKey = "toolbarButton";
            this.cmdCopy.Location = new System.Drawing.Point(821, 29);
            this.cmdCopy.Name = "cmdCopy";
            this.cmdCopy.Size = new System.Drawing.Size(50, 24);
            this.cmdCopy.TabIndex = 1;
            this.cmdCopy.Text = "Copy";
            this.cmdCopy.Click += new System.EventHandler(this.mnuCopy_Click);
            // 
            // frmExportMasterInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(908, 688);
            this.FillColor = 0;
            this.Name = "frmExportMasterInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Export Name Etc.";
            this.Load += new System.EventHandler(this.frmExportMasterInfo_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExportMasterInfo_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
            this.framRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeedNames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutbuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportAllParties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPictures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterestedParties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousOwners)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCommercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDwelling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExemptInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaleData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTranLandBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRef2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNameAddress)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSave;
		private Button cmdCopy;
        public FCCheckBox chkDeedNames;
        public FCCheckBox chkOutbuilding;
    }
}
