﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPrintDuplex.
	/// </summary>
	partial class frmPrintDuplex : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDuplex;
		public fecherFoundation.FCLabel lblDuplex;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame Frame2;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintDuplex));
			this.cmbDuplex = new fecherFoundation.FCComboBox();
			this.lblDuplex = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 331);
			this.BottomPanel.Size = new System.Drawing.Size(617, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(617, 271);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(617, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(62, 30);
			this.HeaderText.Text = "Print";
			// 
			// cmbDuplex
			// 
			this.cmbDuplex.AutoSize = false;
			this.cmbDuplex.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDuplex.FormattingEnabled = true;
			this.cmbDuplex.Items.AddRange(new object[] {
				"None (Print pages on separate pieces of paper)",
				"Duplex (Printer can print on both sides of paper)",
				"After flipping, top page is the last page",
				"After flipping, top page is the first page"
			});
			this.cmbDuplex.Location = new System.Drawing.Point(137, 30);
			this.cmbDuplex.Name = "cmbDuplex";
			this.cmbDuplex.Size = new System.Drawing.Size(208, 40);
			this.cmbDuplex.TabIndex = 1;
			this.cmbDuplex.Text = "None (Print pages on separate pieces of paper)";
			// 
			// lblDuplex
			// 
			this.lblDuplex.AutoSize = true;
			this.lblDuplex.Location = new System.Drawing.Point(20, 44);
			this.lblDuplex.Name = "lblDuplex";
			this.lblDuplex.Size = new System.Drawing.Size(57, 15);
			this.lblDuplex.TabIndex = 0;
			this.lblDuplex.Text = "DUPLEX";
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxLeftBorder";
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Controls.Add(this.cmbDuplex);
			this.Frame1.Controls.Add(this.lblDuplex);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(553, 225);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Duplex Printing";
			// 
			// Frame2
			// 
			this.Frame2.Location = new System.Drawing.Point(20, 100);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(502, 107);
			this.Frame2.TabIndex = 2;
			this.Frame2.Text = "Manual (Print All First Side Pages Then All Second Sides)";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(305, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmPrintDuplex
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(617, 439);
			this.FillColor = 0;
			this.Name = "frmPrintDuplex";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Print";
			this.Load += new System.EventHandler(this.frmPrintDuplex_Load);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
