﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptLandDescription.
	/// </summary>
	partial class srptLandDescription
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLandDescription));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMethod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFctr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpersfla = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFctr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpersfla)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtUnits,
				this.txtMethod,
				this.txtPrice,
				this.txtTotal,
				this.txtFctr,
				this.txtInfluence,
				this.txtValue
			});
			this.Detail.Height = 0.19625F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Line1,
				this.Line2,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7
			});
			this.ReportHeader.Height = 0.3125F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAcres,
				this.Label9,
				this.txtTotValue,
				this.txtpersfla
			});
			this.ReportFooter.Height = 0.19625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.19625F;
			this.Field1.Left = 2.875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field1.Tag = "bold";
			this.Field1.Text = "Land Description";
			this.Field1.Top = 0F;
			this.Field1.Width = 1.625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.09375F;
			this.Line1.Width = 2.71875F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 2.78125F;
			this.Line1.Y1 = 0.09375F;
			this.Line1.Y2 = 0.09375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.59375F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 2.71875F;
			this.Line2.X1 = 4.59375F;
			this.Line2.X2 = 7.3125F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'";
			this.Label1.Tag = "text";
			this.Label1.Text = "Units";
			this.Label1.Top = 0.15625F;
			this.Label1.Width = 0.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.9375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'";
			this.Label2.Tag = "text";
			this.Label2.Text = "Method - Description";
			this.Label2.Top = 0.15625F;
			this.Label2.Width = 1.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label3.Tag = "text";
			this.Label3.Text = "Price/Unit";
			this.Label3.Top = 0.15625F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.3125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label4.Tag = "text";
			this.Label4.Text = "Total";
			this.Label4.Top = 0.15625F;
			this.Label4.Width = 0.5F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'";
			this.Label5.Tag = "text";
			this.Label5.Text = "Fctr";
			this.Label5.Top = 0.15625F;
			this.Label5.Width = 0.5625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.5F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Tag = "text";
			this.Label6.Text = "Influence";
			this.Label6.Top = 0.15625F;
			this.Label6.Width = 0.8125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.75F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label7.Tag = "text";
			this.Label7.Text = "Value";
			this.Label7.Top = 0.15625F;
			this.Label7.Width = 0.5625F;
			// 
			// txtUnits
			// 
			this.txtUnits.CanGrow = false;
			this.txtUnits.Height = 0.19625F;
			this.txtUnits.Left = 0.0625F;
			this.txtUnits.MultiLine = false;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Tahoma\'";
			this.txtUnits.Tag = "text";
			this.txtUnits.Text = "Field2";
			this.txtUnits.Top = 0F;
			this.txtUnits.Width = 0.8125F;
			// 
			// txtMethod
			// 
			this.txtMethod.CanGrow = false;
			this.txtMethod.Height = 0.19625F;
			this.txtMethod.Left = 0.9375F;
			this.txtMethod.MultiLine = false;
			this.txtMethod.Name = "txtMethod";
			this.txtMethod.Style = "font-family: \'Tahoma\'";
			this.txtMethod.Tag = "text";
			this.txtMethod.Text = "Field2";
			this.txtMethod.Top = 0F;
			this.txtMethod.Width = 1.875F;
			// 
			// txtPrice
			// 
			this.txtPrice.CanGrow = false;
			this.txtPrice.Height = 0.19625F;
			this.txtPrice.Left = 2.875F;
			this.txtPrice.MultiLine = false;
			this.txtPrice.Name = "txtPrice";
			this.txtPrice.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPrice.Tag = "text";
			this.txtPrice.Text = "Field2";
			this.txtPrice.Top = 0F;
			this.txtPrice.Width = 1F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.19625F;
			this.txtTotal.Left = 3.9375F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal.Tag = "text";
			this.txtTotal.Text = "Field2";
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 0.875F;
			// 
			// txtFctr
			// 
			this.txtFctr.CanGrow = false;
			this.txtFctr.Height = 0.19625F;
			this.txtFctr.Left = 4.875F;
			this.txtFctr.MultiLine = false;
			this.txtFctr.Name = "txtFctr";
			this.txtFctr.Style = "font-family: \'Tahoma\'";
			this.txtFctr.Tag = "text";
			this.txtFctr.Text = "Field2";
			this.txtFctr.Top = 0F;
			this.txtFctr.Width = 0.5625F;
			// 
			// txtInfluence
			// 
			this.txtInfluence.CanGrow = false;
			this.txtInfluence.Height = 0.19625F;
			this.txtInfluence.Left = 5.5F;
			this.txtInfluence.MultiLine = false;
			this.txtInfluence.Name = "txtInfluence";
			this.txtInfluence.Style = "font-family: \'Tahoma\'";
			this.txtInfluence.Tag = "text";
			this.txtInfluence.Text = "Field2";
			this.txtInfluence.Top = 0F;
			this.txtInfluence.Width = 0.8125F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19625F;
			this.txtValue.Left = 6.375F;
			this.txtValue.MultiLine = false;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtValue.Tag = "text";
			this.txtValue.Text = "Field2";
			this.txtValue.Top = 0F;
			this.txtValue.Width = 0.9375F;
			// 
			// txtAcres
			// 
			this.txtAcres.CanGrow = false;
			this.txtAcres.Height = 0.19625F;
			this.txtAcres.Left = 0.0625F;
			this.txtAcres.MultiLine = false;
			this.txtAcres.Name = "txtAcres";
			this.txtAcres.Style = "font-family: \'Tahoma\'";
			this.txtAcres.Tag = "text";
			this.txtAcres.Text = null;
			this.txtAcres.Top = 0F;
			this.txtAcres.Width = 2.25F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'";
			this.Label9.Tag = "text";
			this.Label9.Text = "Land Total";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.125F;
			// 
			// txtTotValue
			// 
			this.txtTotValue.CanGrow = false;
			this.txtTotValue.Height = 0.19625F;
			this.txtTotValue.Left = 6.3125F;
			this.txtTotValue.MultiLine = false;
			this.txtTotValue.Name = "txtTotValue";
			this.txtTotValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotValue.Tag = "text";
			this.txtTotValue.Text = null;
			this.txtTotValue.Top = 0F;
			this.txtTotValue.Width = 1F;
			// 
			// txtpersfla
			// 
			this.txtpersfla.CanGrow = false;
			this.txtpersfla.Height = 0.19625F;
			this.txtpersfla.Left = 2.4375F;
			this.txtpersfla.MultiLine = false;
			this.txtpersfla.Name = "txtpersfla";
			this.txtpersfla.Style = "font-family: \'Tahoma\'";
			this.txtpersfla.Tag = "text";
			this.txtpersfla.Text = null;
			this.txtpersfla.Top = 0F;
			this.txtpersfla.Width = 2.1875F;
			// 
			// srptLandDescription
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.385417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFctr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpersfla)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMethod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFctr;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcres;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpersfla;
	}
}
