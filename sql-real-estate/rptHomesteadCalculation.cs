﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptHomesteadCalculation.
	/// </summary>
	public partial class rptHomesteadCalculation : BaseSectionReport
	{
		public static rptHomesteadCalculation InstancePtr
		{
			get
			{
				return (rptHomesteadCalculation)Sys.GetInstance(typeof(rptHomesteadCalculation));
			}
		}

		protected rptHomesteadCalculation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptHomesteadCalculation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptHomesteadCalculation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this);
		}

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
            

            double dblRatio;
            string strTotExemptCodes;
            string strSQL;
            string strQuery1;
            string strQuery2;
            string strQuery3;
            string strWhere;
            string strWhere2;
            string strHomesteads;
            string strQuery4;
            int lngNumRegHomesteads = 0;
            int lngNumSecTierHomesteads = 0;
            int lngNumThirdTierHomesteads = 0;
            int lngTotExemptions;
            //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            txtTitle.Text = FCConvert.ToString(DateTime.Today.Year) + " HOMESTEAD EXEMPTION CALCULATION FORM";
            lbl13.Text = "TOTAL AMOUNT OF " + FCConvert.ToString(DateTime.Today.Year) +
                         " HOMESTEAD VALUE EXEMPTED (SUM OF LINES 4,8,11)";
            // get ratio first
            dblRatio = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
            txtCertifiedRatio.Text = FCConvert.ToString(dblRatio * 100) + "%";
            txtLine1Ratio.Text = FCConvert.ToString(dblRatio * 100) + "%";
            txtLine2Ratio.Text = FCConvert.ToString(dblRatio * 100) + "%";
            txtLine5Ratio.Text = FCConvert.ToString(dblRatio * 100) + "%";
            txtLine6Ratio.Text = FCConvert.ToString(dblRatio * 100) + "%";
            txtLine9Ratio.Text = FCConvert.ToString(dblRatio * 100) + "%";
            // get a list of totally exempt codes.  If the homestead code follows it it will not be a homestead code, it will be a percentage
            strTotExemptCodes = "";
            // Call clsLoad.OpenRecordset("select crecordnumber - 1900 as code from costrecord where crecordnumber between 1901 and 1999 and val(cunit & '') = 0", strREDatabase)
            // Do While Not clsLoad.EndOfFile
            // strTotExemptCodes = strTotExemptCodes & clsLoad.Fields("code") & ","
            // clsLoad.MoveNext
            // Loop
            // 
            // strip off the last comma
            // If strTotExemptCodes <> vbNullString Then
            // strTotExemptCodes = Mid(strTotExemptCodes, 1, Len(strTotExemptCodes) - 1)
            // strWhere = " and Riexemptcd1 not in (" & strTotExemptCodes & ")"
            // strWhere2 = " and riexemptcd2 not in (" & strTotExemptCodes & ")"
            // Else
            strWhere = "";
            strWhere2 = "";
            // End If
            // get a list of homestead codes
            strHomesteads = "";
            // Call clsLoad.OpenRecordset("select * from customize", strREDatabase)
            // If Not clsLoad.EndOfFile Then
            // If Val(clsLoad.Fields("homesteadcode1")) > 0 Then
            // strHomesteads = Val(clsLoad.Fields("homesteadcode1"))
            // If Val(clsLoad.Fields("homesteadcode2")) > 0 Then
            // strHomesteads = strHomesteads & "," & Val(clsLoad.Fields("homesteadcode2"))
            // If Val(clsLoad.Fields("homesteadcode3")) > 0 Then
            // strHomesteads = strHomesteads & "," & Val(clsLoad.Fields("homesteadcode3"))
            // If Val(clsLoad.Fields("homesteadcode4")) > 0 Then
            // strHomesteads = strHomesteads & "," & Val(clsLoad.Fields("homesteadcode4"))
            // If Val(clsLoad.Fields("homesteadcode5")) > 0 Then
            // strHomesteads = strHomesteads & "," & Val(clsLoad.Fields("homesteadcode5"))
            // End If
            // End If
            // End If
            // End If
            // End If
            // End If
            clsLoad.OpenRecordset(
                "select * from exemptcode where category = " +
                FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHOMESTEAD), modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                strHomesteads += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + ",";
                clsLoad.MoveNext();
            }

            if (strHomesteads != string.Empty)
            {
                strHomesteads = Strings.Mid(strHomesteads, 1, strHomesteads.Length - 1);
                strHomesteads = "(" + strHomesteads + ")";
            }
            else
            {
                MessageBox.Show("Could not find homestead codes. Cannot continue.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
                this.Close();
                return;
            }

            strQuery1 = "select rsaccount from master where rscard = 1 and not rsdeleted = 1 and riexemptcd1 in " +
                        strHomesteads;
            // Call clsLoad.CreateStoredProcedure("HomesteadQuery1", strQuery1, strREDatabase)
            strQuery2 = "select rsaccount from master where rscard = 1 and not rsdeleted = 1 and riexemptcd2 in " +
                        strHomesteads + " " + strWhere;
            // Call clsLoad.CreateStoredProcedure("HomesteadQuery2", strQuery2, strREDatabase)
            strQuery3 = "select rsaccount from master where rscard = 1 and not rsdeleted = 1 and riexemptcd3 in " +
                        strHomesteads + " " + strWhere;
            // Call clsLoad.CreateStoredProcedure("HomesteadQuery3", strQuery3, strREDatabase)
            // strQuery4 = "select * from (select * from homesteadquery1) union (select * from homesteadquery2) union (select * from homesteadquery3) "
            strQuery4 = "select * from ((" + strQuery1 + ") union all (" + strQuery2 + ") union all (" + strQuery3 +
                        ")) tbl1 ";
            // Call clsLoad.CreateStoredProcedure("HomesteadQuery4", strQuery4, strREDatabase)
            // strSQL = "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment from master inner join homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having sum(lastlandval + lastbldgval) < " & Int(125000 * dblRatio)
            strSQL =
                "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment from master inner join (" +
                strQuery4 +
                " ) as homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having sum(lastlandval + lastbldgval) < " +
                FCConvert.ToString(Conversion.Int(125000 * dblRatio));
            clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
            if (!clsLoad.EndOfFile())
            {
                lngNumRegHomesteads = clsLoad.RecordCount();
            }
            else
            {
                lngNumRegHomesteads = 0;
            }

            strSQL =
                "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment from master inner join homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having sum(lastlandval + lastbldgval) between " +
                FCConvert.ToString(Conversion.Int(125000 * dblRatio)) + " and " +
                FCConvert.ToString(Conversion.Int(250000 * dblRatio) - 1);
            clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
            if (!clsLoad.EndOfFile())
            {
                lngNumSecTierHomesteads = clsLoad.RecordCount();
            }
            else
            {
                lngNumSecTierHomesteads = 0;
            }

            strSQL =
                "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment from master inner join homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having sum(lastlandval + lastbldgval) >= " +
                FCConvert.ToString(Conversion.Int(250000 * dblRatio));
            clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
            if (!clsLoad.EndOfFile())
            {
                lngNumThirdTierHomesteads = clsLoad.RecordCount();
            }
            else
            {
                lngNumThirdTierHomesteads = 0;
            }

            lngTotExemptions = 0;
            TXTLine1.Text = "$" + Strings.Format(FCConvert.ToInt32(125000 * dblRatio), "#,###,##0");
            txtLine2.Text = "$" + Strings.Format(FCConvert.ToInt32(7000 * dblRatio), "#,###,##0");
            txtLine3Amount.Text = TXTLine1.Text;
            txtLine3NumHomesteads.Text = Strings.Format(lngNumRegHomesteads, "#,###,##0");
            txtLine4HomesteadVal.Text = txtLine2.Text;
            txtLine4NumHomesteads.Text = txtLine3NumHomesteads.Text;
            lngTotExemptions = FCConvert.ToInt32(lngNumRegHomesteads * 7000 * dblRatio);
            txtLine4Total.Text = "$" + Strings.Format(lngTotExemptions, "#,###,##0");
            txtLine5.Text = "$" + Strings.Format(FCConvert.ToInt32(250000 * dblRatio), "#,###,##0");
            txtLine6.Text = "$" + Strings.Format(FCConvert.ToInt32(5000 * dblRatio), "#,###,##0");
            txtLine7GreaterThan.Text = TXTLine1.Text;
            txtLine7LessThan.Text = txtLine5.Text;
            txtLine7NumHomesteads.Text = Strings.Format(lngNumSecTierHomesteads, "#,###,##0");
            txtLine8HomesteadVal.Text = txtLine6.Text;
            txtLine8NumHomesteads.Text = txtLine7NumHomesteads.Text;
            txtLine8Total.Text =
                "$" + Strings.Format(FCConvert.ToInt32(lngNumSecTierHomesteads * 5000 * dblRatio), "#,###,##0");
            lngTotExemptions += FCConvert.ToInt32(lngNumSecTierHomesteads * 5000 * dblRatio);
            txtLine9.Text = "$" + Strings.Format(FCConvert.ToInt32(dblRatio * 2500), "#,###,##0");
            txtLine10LessThan.Text = txtLine5.Text;
            txtLine10NumHomesteads.Text = Strings.Format(lngNumThirdTierHomesteads, "#,###,##0");
            txtLine11ExemptVal.Text = txtLine9.Text;
            txtLine11NumHomesteads.Text = txtLine10NumHomesteads.Text;
            txtLine11Total.Text = "$" + Strings.Format(FCConvert.ToInt32(lngNumThirdTierHomesteads * 2500 * dblRatio),
                "#,###,##0");
            lngTotExemptions += FCConvert.ToInt32(lngNumThirdTierHomesteads * 2500 * dblRatio);
            txtLine12.Text = Strings.Format(lngNumRegHomesteads + lngNumSecTierHomesteads + lngNumThirdTierHomesteads,
                "#,###,##0");
            txtLine13.Text = "$" + Strings.Format(lngTotExemptions, "#,###,##0");
            }
        }

		
	}
}
