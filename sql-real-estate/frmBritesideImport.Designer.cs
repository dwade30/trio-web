﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmBritesideImport.
	/// </summary>
	partial class frmBritesideImport : BaseForm
	{
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBritesideImport));
            this.Grid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 313);
            this.BottomPanel.Size = new System.Drawing.Size(501, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(501, 253);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(501, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(82, 30);
            this.HeaderText.Text = "Import";
            // 
            // Grid
            // 
            this.Grid.Cols = 10;
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.FixedCols = 0;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(30, 78);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 0;
            this.Grid.Size = new System.Drawing.Size(300, 150);
            this.Grid.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(135, 18);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "IMPORTING ACCOUNT";
            // 
            // frmBritesideImport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(501, 421);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBritesideImport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Import";
            this.Load += new System.EventHandler(this.frmBritesideImport_Load);
            this.Activated += new System.EventHandler(this.frmBritesideImport_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBritesideImport_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
