﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmOwnershipChangeReason.
	/// </summary>
	public partial class frmOwnershipChangeReason : BaseForm
	{
		public frmOwnershipChangeReason()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmOwnershipChangeReason InstancePtr
		{
			get
			{
				return (frmOwnershipChangeReason)Sys.GetInstance(typeof(frmOwnershipChangeReason));
			}
		}

		protected frmOwnershipChangeReason _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int intReturn;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			ChoseReason();
		}

		private void frmOwnershipChangeReason_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmOwnershipChangeReason_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOwnershipChangeReason properties;
			//frmOwnershipChangeReason.FillStyle	= 0;
			//frmOwnershipChangeReason.ScaleWidth	= 5880;
			//frmOwnershipChangeReason.ScaleHeight	= 2295;
			//frmOwnershipChangeReason.LinkTopic	= "Form2";
			//frmOwnershipChangeReason.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUMSHORT);
			modGlobalFunctions.SetTRIOColors(this);
			SetupCmbReason();
			cmbReason.SelectedIndex = -1;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupCmbReason()
		{
			cmbReason.Clear();
			cmbReason.AddItem("Ownership has changed");
			cmbReason.AddItem("Owner party was incorrect");
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Init()
		{
			short Init = 0;
			intReturn = -1;
			Init = FCConvert.ToInt16(intReturn);
			this.Show(FCForm.FormShowEnum.Modal);
			Init = FCConvert.ToInt16(intReturn);
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			ChoseReason();
		}

		private bool ChoseReason()
		{
			bool ChoseReason = false;
			if (cmbReason.SelectedIndex >= 0)
			{
				intReturn = cmbReason.SelectedIndex;
				ChoseReason = true;
				this.Unload();
			}
			else
			{
				MessageBox.Show("You must choose a reason or cancel", "No Reason Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				ChoseReason = false;
			}
			return ChoseReason;
		}
	}
}
