﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPictureChange.
	/// </summary>
	public partial class frmPictureChange : BaseForm
    {
        private byte[] imageBytes;
        public void SetImage(byte[] imageData)
        {
            imageBytes = imageData;
        }
		public frmPictureChange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            this.Closed += FrmPictureChange_Closed;
		}

        private void FrmPictureChange_Closed(object sender, EventArgs e)
        {
            if (imgPicture.Image != null)
            {
                imgPicture.Image.Dispose();
            }

            imageBytes = null;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmPictureChange InstancePtr
		{
			get
			{
				return (frmPictureChange)Sys.GetInstance(typeof(frmPictureChange));
			}
		}

		protected frmPictureChange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdNO_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.response = "NO";
			frmPictureChange.InstancePtr.Hide();
		}

		private void cmdYes_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.response = "YES";
			frmPictureChange.InstancePtr.Hide();
			modGNBas.Statics.LinkPicture = false;
		}

		private void frmPictureChange_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorRoutine
				lblAccountCard.Text = "Account: " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + "")) + "   Card: " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + ""));
				imgPicture.Image = FCUtils.PictureFromBytes(imageBytes);
				modGNBas.Statics.LinkPicture = true;
			}
			catch
			{
				// ErrorRoutine:
			}
		}

		private void frmPictureChange_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}
	}
}
