﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptZoneSchedule.
	/// </summary>
	partial class rptZoneSchedule
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptZoneSchedule));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtneighborhood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeighborhoodDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSchedule = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtZone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtneighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhoodDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchedule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtneighborhood,
				this.txtNeighborhoodDesc,
				this.txtLandFactor,
				this.txtBldgFactor,
				this.txtSchedule
			});
			this.Detail.Height = 0.28125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtneighborhood
			// 
			this.txtneighborhood.Height = 0.2083333F;
			this.txtneighborhood.Left = 3.375F;
			this.txtneighborhood.Name = "txtneighborhood";
			this.txtneighborhood.Style = "text-align: right";
			this.txtneighborhood.Text = null;
			this.txtneighborhood.Top = 0.04166667F;
			this.txtneighborhood.Width = 0.40625F;
			// 
			// txtNeighborhoodDesc
			// 
			this.txtNeighborhoodDesc.Height = 0.2083333F;
			this.txtNeighborhoodDesc.Left = 3.8125F;
			this.txtNeighborhoodDesc.Name = "txtNeighborhoodDesc";
			this.txtNeighborhoodDesc.Text = null;
			this.txtNeighborhoodDesc.Top = 0.04166667F;
			this.txtNeighborhoodDesc.Width = 1.989583F;
			// 
			// txtLandFactor
			// 
			this.txtLandFactor.Height = 0.2083333F;
			this.txtLandFactor.Left = 5.8125F;
			this.txtLandFactor.Name = "txtLandFactor";
			this.txtLandFactor.Style = "text-align: right";
			this.txtLandFactor.Text = null;
			this.txtLandFactor.Top = 0.04166667F;
			this.txtLandFactor.Width = 0.5520833F;
			// 
			// txtBldgFactor
			// 
			this.txtBldgFactor.Height = 0.2083333F;
			this.txtBldgFactor.Left = 6.375F;
			this.txtBldgFactor.Name = "txtBldgFactor";
			this.txtBldgFactor.Style = "text-align: right";
			this.txtBldgFactor.Text = null;
			this.txtBldgFactor.Top = 0.04166667F;
			this.txtBldgFactor.Width = 0.5520833F;
			// 
			// txtSchedule
			// 
			this.txtSchedule.Height = 0.2083333F;
			this.txtSchedule.Left = 6.9375F;
			this.txtSchedule.Name = "txtSchedule";
			this.txtSchedule.Style = "text-align: right";
			this.txtSchedule.Text = null;
			this.txtSchedule.Top = 0.04166667F;
			this.txtSchedule.Width = 0.5520833F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8
			});
			this.PageHeader.Height = 0.9583333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Zone Schedule";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 2.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Zone";
			this.Label3.Top = 0.7291667F;
			this.Label3.Width = 0.40625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2083333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: left";
			this.Label4.Text = "Description";
			this.Label4.Top = 0.7291667F;
			this.Label4.Visible = false;
			this.Label4.Width = 2.927083F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2083333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: left";
			this.Label5.Text = "Neighborhood";
			this.Label5.Top = 0.7291667F;
			this.Label5.Width = 1.989583F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.3958333F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.8125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Land Factor";
			this.Label6.Top = 0.5625F;
			this.Label6.Width = 0.5520833F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.3958333F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Bldg Factor";
			this.Label7.Top = 0.5625F;
			this.Label7.Width = 0.5520833F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.2083333F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.9375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Sched.";
			this.Label8.Top = 0.7291667F;
			this.Label8.Width = 0.5520833F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtZone,
				this.txtDescription
			});
			this.GroupHeader1.Height = 0.28125F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// txtZone
			// 
			this.txtZone.Height = 0.2083333F;
			this.txtZone.Left = 0F;
			this.txtZone.Name = "txtZone";
			this.txtZone.Style = "text-align: right";
			this.txtZone.Text = null;
			this.txtZone.Top = 0.04166667F;
			this.txtZone.Width = 0.40625F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.2083333F;
			this.txtDescription.Left = 0.4375F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.04166667F;
			this.txtDescription.Width = 3.427083F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptZoneSchedule
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtneighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhoodDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchedule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtneighborhood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeighborhoodDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSchedule;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
