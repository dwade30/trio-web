﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwellAnalysis.
	/// </summary>
	public partial class srptDwellAnalysis : FCSectionReport
	{
		public static srptDwellAnalysis InstancePtr
		{
			get
			{
				return (srptDwellAnalysis)Sys.GetInstance(typeof(srptDwellAnalysis));
			}
		}

		protected srptDwellAnalysis _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsBCode?.Dispose();
				clsDwell?.Dispose();
                clsDwell = null;
                clsBCode = null;
            }
			base.Dispose(disposing);
		}

		public srptDwellAnalysis()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptDwellAnalysis	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
		clsDRWrapper clsBCode = new clsDRWrapper();
		clsDRWrapper clsDwell = new clsDRWrapper();
		int lngBCode;
		int lngAssess;
		int lngCount;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			string strSQL = "";
			// Call clsBCode.OpenRecordset("select * from costrecord where crecordnumber between 1801 and 1899 order by crecordnumber", strredatabase)
			clsBCode.OpenRecordset("SELECT * from tblBldgCode order by code", modGlobalVariables.strREDatabase);
			// Call clsdwell.OpenRecordset("select ribldgcode, count(ribldgcode) as thecount,sum(val(rlbldgval)) as thesum from master where rsdwellingcode = 'Y' and ribldgcode >= 0 group by ribldgcode order by ribldgcode", strredatabase)
			// Call clsdwell.OpenRecordset("select val(master.ribldgcode & '') as ribldgcode,count(*) as thecount,sum(val(summrecord.sdwellrcnld & '')) as thesum from (dwelling inner join (master inner join  summrecord on (master.rsaccount = SUMMRecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard)) where master.rsdwellingcode = 'Y' and val(master.ribldgcode & '') >= 0 and not master.rsdeleted group by val(master.ribldgcode & '') order by val(master.ribldgcode & '')", strredatabase)
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "select isnull(master.ribldgcode,0) as ribldgcode,count(*) as thecount,sum(isnull(summrecord.sdwellrcnld,0)) as thesum from (dwelling inner join (master inner join  summrecord on (master.rsaccount = SUMMRecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard)) where  master.ribldgcode >= 0 and not master.rsdeleted = 1 group by master.ribldgcode  order by master.ribldgcode ";
			}
			else
			{
				strSQL = "select isnull(master.ribldgcode,0) as ribldgcode,count(*) as thecount,sum(isnull(summrecord.sdwellrcnld,0)) as thesum from (dwelling inner join (master inner join  summrecord on (master.rsaccount = SUMMRecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard)) where  master.ribldgcode >= 0 and not master.rsdeleted = 1 and ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " group by master.ribldgcode order by master.ribldgcode";
			}
			clsDwell.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			// Call clsBldg.OpenRecordset("select val(dwelling.distyle & '')as distyle,count(distyle) as thecount,sum(val(summrecord.sdwellrcnld & '')) as thetotal from (dwelling inner join (master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard)) where not master.rsdeleted group by val(dwelling.distyle & '') order by val(dwelling.distyle & '')", strredatabase)
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (clsDwell.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsDwell.EndOfFile())
			{
				lngBCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsDwell.Get_Fields_Int32("ribldgcode") + "")));
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsDwell.Get_Fields("thecount"))));
				// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
				lngAssess = FCConvert.ToInt32(Math.Round(Conversion.Val(clsDwell.Get_Fields("thesum"))));
				txtAve.Text = Strings.Format(FCConvert.ToDouble(lngAssess) / lngCount, "#,###,###,##0");
			}
			else
			{
				lngBCode = 0;
				lngCount = 0;
				lngAssess = 0;
				txtAve.Text = "0";
			}
			txtBldgCode.Text = lngBCode.ToString();
			if (lngBCode == 0)
			{
				txtBldgCode.Text = "Uncoded";
			}
			else
			{
				// If clsBCode.FindFirstRecord("crecordnumber", lngBCode + 1800) Then
				// If clsBCode.FindFirstRecord("code", lngBCode) Then
				if (clsBCode.FindFirst("code = " + FCConvert.ToString(lngBCode)))
				{
					// txtBldgCode.Text = txtBldgCode.Text & " " & clsBCode.Fields("cldesc")
					txtBldgCode.Text = txtBldgCode.Text + " " + clsBCode.Get_Fields_String("description");
				}
				else
				{
					txtBldgCode.Text = txtBldgCode.Text + " No description Found";
				}
			}
			txtCount.Text = lngCount.ToString();
			txtAssess.Text = Strings.Format(lngAssess, "##,###,###,##0");
			clsDwell.MoveNext();
		}

		
	}
}
