﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptLandDescription.
	/// </summary>
	public partial class srptLandDescription : FCSectionReport
	{
		public static srptLandDescription InstancePtr
		{
			get
			{
				return (srptLandDescription)Sys.GetInstance(typeof(srptLandDescription));
			}
		}

		protected srptLandDescription _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptLandDescription()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptLandDescription	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCurLine;
		int intCurC;
		// vbPorter upgrade warning: lngTotVal As int	OnWrite(short, float)
		float lngTotVal;
		
		int intNumLandEntries;
		double dblTotAcres;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modDataTypes.Statics.MR.EndOfFile())
			{
				intCurLine += 1;
				if (intCurC < 0)
				{
					// not from valuation screen
					eArgs.EOF = intCurLine > 7;
					if (eArgs.EOF)
						return;
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					if (modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "type") > 0)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
				else
				{
					// EOF = intCurLine > (frmNewValuationReport.PropertyGrid(intCurC).Rows - 2)
					eArgs.EOF = intCurLine > intNumLandEntries;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			dblTotAcres = 0;
			intCurC = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (intCurC == -10)
			{
				// Unload Me
				this.Close();
				return;
			}
			// If intCurC < 0 Then
			// intCurLine = 1
			// Else
			intCurLine = 0;
			// End If
			lngTotVal = 0;
			// If UCase(MuniName) = "GARDINER" Then
			// Line1.Visible = False
			// Line2.Visible = False
			// End If
			if (intCurC >= 0)
			{
				for (x = 1; x <= 7; x++)
				{
					// If Trim(CalcPropertyList(intCurC + 1).LandDesc(x)) <> vbNullString Then
					if (FCConvert.ToInt16(modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandType[x]) > 0)
					{
						intNumLandEntries = x;
					}
				}
				// x
			}

		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!modDataTypes.Statics.MR.EndOfFile())
			{
				if (intCurC < 0)
				{
					// not from valuation screen
					// vbPorter upgrade warning: intHoldType As short --> As int	OnWriteFCConvert.ToDouble(
					int intHoldType = 0;
					// vbPorter upgrade warning: strUnits1 As string	OnWrite(string, double)
					string strUnits1 = "";
					string strUnits2 = "";
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					strUnits1 = modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "unitsa") + "";
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					strUnits2 = modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "unitsb") + "";
					dblTotAcres = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres"));
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					intHoldType = FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "type") + "") - 10);
					if (modGlobalVariables.Statics.LandTypes.FindCode(intHoldType + 10))
					{
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									txtUnits.Text = strUnits1 + " X " + strUnits2;
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									strUnits1 = FCConvert.ToString(Conversion.Val(strUnits1) * 1000 + Conversion.Val(strUnits2));
									txtUnits.Text = Strings.Format(strUnits1, "###,###");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									strUnits1 = FCConvert.ToString(Conversion.Val(strUnits1 + strUnits2) / 100);
									txtUnits.Text = Strings.Format(strUnits1, "###0.00");
									break;
								}
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									goto REAS03_5101_TAG;
									break;
								}
						}
						//end switch
						// vbPorter upgrade warning: strAcreType As Variant --> As string
						string strAcreType = "";
						strAcreType = Strings.Left(Strings.Trim(modGlobalVariables.Statics.LandTypes.ShortDescription) + "     ", 5) + "-" + modGlobalVariables.Statics.LandTypes.Description;
						txtMethod.Text = Strings.Trim(strAcreType);
					}
					if (modREASValuations.Statics.UnitPrice[intCurLine] < 100000.0)
					{
						txtPrice.Text = Strings.Format(modREASValuations.Statics.UnitPrice[intCurLine], "##,###.00");
					}
					else if (modREASValuations.Statics.UnitPrice[intCurLine] < 10000000.0)
					{
						txtPrice.Text = Strings.Format(modREASValuations.Statics.UnitPrice[intCurLine], "#,###,###");
					}
					else
					{
						txtPrice.Text = Strings.Format(modREASValuations.Statics.UnitPrice[intCurLine], "#########");
					}
					txtTotal.Text = Strings.Format(modREASValuations.Statics.Price[intCurLine], "###,###,###");
					REAS03_5101_TAG:
					;
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "infcode") + "") == 0)
					{
						modREASValuations.Statics.WORKA6 = "";
					}
					else
					{
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1440 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "infcode") + "")));
						modREASValuations.Statics.WORKA6 = Strings.Mid(modGlobalVariables.Statics.CostRec.ClDesc, 1, 10) + " ";
					}
					// intHoldType = Val(WORKA2) / 100
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					txtFctr.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(intCurLine) + "inf") + "")) + "%";
					txtInfluence.Text = modREASValuations.Statics.WORKA6;
					txtValue.Text = Strings.Format(modREASValuations.Statics.Value[intCurLine], "#,##0");
					lngTotVal += modREASValuations.Statics.Value[intCurLine];
				}
				else
				{
					// With frmNewValuationReport.PropertyGrid(intCurC)
					// txtUnits.Text = .TextMatrix(intCurLine, 0)
					// txtMethod.Text = Trim(.TextMatrix(intCurLine, 1) & "")
					// txtPrice.Text = .TextMatrix(intCurLine, 2)
					// txtTotal.Text = .TextMatrix(intCurLine, 3)
					// txtFctr.Text = .TextMatrix(intCurLine, 4)
					// txtInfluence.Text = .TextMatrix(intCurLine, 5)
					// txtValue.Text = .TextMatrix(intCurLine, 6)
					if (modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandType[intCurLine] != 99)
					{
						txtMethod.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandDesc[intCurLine];
						txtUnits.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandUnits[intCurLine];
						txtPrice.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandPricePerUnit[intCurLine];
						txtTotal.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandTotal[intCurLine];
					}
					else
					{
						txtMethod.Text = "";
						txtUnits.Text = "";
						txtPrice.Text = "";
						txtTotal.Text = "";
					}
					txtFctr.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandFctr[intCurLine];
					txtInfluence.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandInfluence[intCurLine];
					txtValue.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].LandValue[intCurLine];
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (intCurC < 0)
			{
				txtAcres.Text = "Total Acres " + Strings.Format(dblTotAcres, "#,##0.00");
				txtTotValue.Text = Strings.Format(lngTotVal, "#,###,###,##0");
				if (dblTotAcres > 0)
				{
					if (modGlobalVariables.Statics.CustomizedInfo.AcreOption != 2)
					{
						txtpersfla.Text = Strings.Format(lngTotVal / dblTotAcres, "#,###,##0.00") + " Per Acre";
					}
					else
					{
						txtpersfla.Text = "";
					}
				}
				else
				{
					txtpersfla.Text = "";
				}
			}
			else
			{
				// With frmNewValuationReport.PropertyGrid(intCurC)
				// txtAcres.Text = .TextMatrix(.Rows - 1, 0)
				// txtTotValue.Text = .TextMatrix(.Rows - 1, 6)
				// txtpersfla.Text = .TextMatrix(.Rows - 1, 2)
				txtAcres.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].Acres;
				txtTotValue.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].TotalLandValue;
				txtpersfla.Text = modSpeedCalc.Statics.CalcPropertyList[intCurC + 1].PerAcreVal;
			}
		}

		
	}
}
