﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modSpeedCalc
	{
		public static void CalcTreeGrowth()
		{
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			intResp = MessageBox.Show("This will replace all Tree Growth values with calculated ones." + "\r\n" + "Do you wish to continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (intResp == DialogResult.No)
				return;
			frmTreeGrowth.InstancePtr.Show(App.MainForm);
		}

		public static void CalcCard()
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: x As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
				{
					// calculates current record in MR
					bool TotalsDone;
					int intCounter;
					modProperty.Statics.AccountAcreage = 0;
					modREASValuations.Statics.LandAcreValue = 0;
					FCUtils.EraseSafe(modProperty.Statics.LandTotal);
					FCUtils.EraseSafe(modProperty.Statics.BuildingTotal);
					FCUtils.EraseSafe(modProperty.Statics.lngCurrentLand);
					FCUtils.EraseSafe(modProperty.Statics.lngCurrentBldg);
					FCUtils.EraseSafe(modProperty.Statics.lngPreviousLand);
					FCUtils.EraseSafe(modProperty.Statics.lngPreviousBldg);
					FCUtils.EraseSafe(modProperty.Statics.lngMarketLand);
					FCUtils.EraseSafe(modProperty.Statics.lngMarketBldg);
					FCUtils.EraseSafe(modProperty.Statics.lngIncomeLand);
					FCUtils.EraseSafe(modProperty.Statics.lngIncomeBldg);
					FCUtils.EraseSafe(modProperty.Statics.lngAcceptedLand);
					FCUtils.EraseSafe(modProperty.Statics.lngAcceptedBldg);
					FCUtils.EraseSafe(modProperty.Statics.lngCorrelatedLand);
					FCUtils.EraseSafe(modProperty.Statics.lngCorrelatedBldg);
					FCUtils.EraseSafe(modProperty.Statics.lngOverRideLand);
					FCUtils.EraseSafe(modProperty.Statics.lngOverRideBldg);
					FCUtils.EraseSafe(modProperty.Statics.intOverRideCodeLand);
					FCUtils.EraseSafe(modProperty.Statics.intOverRideCodeBldg);
					// Erase OutbuildingTotal
					FCUtils.EraseSafe(modGlobalVariables.Statics.SoftAcresArray);
					FCUtils.EraseSafe(modGlobalVariables.Statics.SoftValueArray);
					FCUtils.EraseSafe(modGlobalVariables.Statics.HardAcresArray);
					FCUtils.EraseSafe(modGlobalVariables.Statics.HardValueArray);
					FCUtils.EraseSafe(modGlobalVariables.Statics.MixedAcresArray);
					FCUtils.EraseSafe(modGlobalVariables.Statics.MixedValueArray);
					modDwelling.Statics.intSFLA = 0;
					if (!modGlobalVariables.Statics.boolfromcalcandassessment)
					{
						modREASValuations.Statics.BBPhysicalPercent = 0;
						modREASValuations.Statics.HoldDwellingEconomic = 0;
					}
					modDwelling.Statics.HoldGrade2 = 0;
				}
				CalcAnotherCard:
				;
				Statics.SummaryListIndex += 1;
				// ReDim Preserve SummaryList(SummaryListIndex)
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode");
					if (modGlobalVariables.Statics.boolUseArrays)
					{
						modGlobalVariables.Statics.LandTRec.SetupArrays(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
					}
				}
				modGlobalVariables.Statics.intCurrentCard = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
				modGNBas.Statics.gintCardNumber = modGlobalVariables.Statics.intCurrentCard;
				if (modGlobalVariables.Statics.intCurrentCard == 1)
				{
					modDwelling.Statics.intHoldYear = 0;
					modDwelling.Statics.HoldCondition = 0;
					modDwelling.Statics.HoldGrade2 = 0;
					modREASValuations.Statics.BBPhysicalPercent = 0;
					modDwelling.Statics.dblPhysicalPercent = 0;
				}
				if (modProperty.Statics.RUNTYPE == "D")
				{
					// frmNewValuationReport.AddGrids (intCurrentCard - 1)
					Array.Resize(ref modGlobalVariables.Statics.SoftAcresArray, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref modGlobalVariables.Statics.HardAcresArray, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref modGlobalVariables.Statics.MixedAcresArray, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref modGlobalVariables.Statics.SoftValueArray, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref modGlobalVariables.Statics.HardValueArray, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref modGlobalVariables.Statics.MixedValueArray, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref Statics.CalcPropertyList, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref Statics.CalcDwellList, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref Statics.CalcCommList, modGlobalVariables.Statics.intCurrentCard + 1);
					Array.Resize(ref Statics.CalcOutList, modGlobalVariables.Statics.intCurrentCard + 1);
					//FC:FINAL:MSH - i.issue #1192: initialize class through constructor 
					for (int i = 0; i < Statics.CalcPropertyList.Length; i++)
					{
                        //FC:FINAL:SBE - #3369 - do not reinitialize the same record because current values will be lost
                        if (Statics.CalcPropertyList[i].LandType == null)
                        {
                            Statics.CalcPropertyList[i] = new modGlobalVariables.CalcPropertyType(1);
                        }
					}
					//FC:FINAL:MSH - i.issue #1192: initialize class through constructor 
					for (int i = 0; i < Statics.CalcDwellList.Length; i++)
					{
                        //FC:FINAL:SBE - #3369 - do not reinitialize the same record because current values will be lost
                        if (Statics.CalcDwellList[i].AddnFixtures == null)
                        {
                            Statics.CalcDwellList[i] = new modGlobalVariables.CalcDwellingType(1);
                        }
					}
					//FC:FINAL:MSH - i.issue #1192: initialize class through constructor 
					for (int i = 0; i < Statics.CalcCommList.Length; i++)
					{
                        //FC:FINAL:SBE - #3369 - do not reinitialize the same record because current values will be lost
                        if (Statics.CalcCommList[i].OccupancyType == null)
                        {
                            Statics.CalcCommList[i] = new modGlobalVariables.CalcCommercialType(1);
                        }
					}
					//FC:FINAL:MSH - i.issue #1192: initialize class through constructor 
					for (int i = 0; i < Statics.CalcOutList.Length; i++)
					{
                        //FC:FINAL:SBE - #3369 - do not reinitialize the same record because current values will be lost
                        if (Statics.CalcOutList[i].OutbuildingType == null)
                        {
                            Statics.CalcOutList[i] = new modGlobalVariables.CalcOutBuildingType(1);
                        }
					}
					FCUtils.EraseSafe(Statics.CalcPropertyList[modGlobalVariables.Statics.intCurrentCard].LandType);
					FCUtils.EraseSafe(Statics.CalcPropertyList[modGlobalVariables.Statics.intCurrentCard].LandDesc);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].OutbuildingType);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Year);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Units);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Grade);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Functional);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Physcial);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Economic);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].OutbuildingValue);
					FCUtils.EraseSafe(Statics.CalcOutList[modGlobalVariables.Statics.intCurrentCard].Description);
					FCUtils.EraseSafe(Statics.CalcCommList[modGlobalVariables.Statics.intCurrentCard].OccupancyType);
					FCUtils.EraseSafe(Statics.CalcCommList[modGlobalVariables.Statics.intCurrentCard].AdjustedCost);
					FCUtils.EraseSafe(Statics.CalcCommList[modGlobalVariables.Statics.intCurrentCard].subtotal);
					Statics.CalcDwellIndex = modGlobalVariables.Statics.intCurrentCard;
					Statics.CalcOutIndex = modGlobalVariables.Statics.intCurrentCard;
					Statics.CalcPropertyIndex = modGlobalVariables.Statics.intCurrentCard;
					Statics.CalcCommIndex = modGlobalVariables.Statics.intCurrentCard;
				}
				for (x = 1; x <= 10; x++)
				{
					modOutbuilding.Statics.intOIType[x] = 0;
				}
				// x
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					// Call DWL.FindFirstRecord2("rsaccount,rscard", gintLastAccountNumber & "," & intCurrentCard, ",")
					modDataTypes.Statics.DWL.FindFirst("rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard));
					// Call OUT.FindFirstRecord2("rsaccount,rscard", gintLastAccountNumber & "," & intCurrentCard, ",")
					modDataTypes.Statics.OUT.FindFirst("rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard));
					// Call CMR.FindFirstRecord2("rsaccount,rscard", gintLastAccountNumber & "," & intCurrentCard, ",")
					if (!modDataTypes.Statics.CMR.FindFirst("rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard)))
					{
						if (!modDataTypes.Statics.CMR.EndOfFile())
						{
							modDataTypes.Statics.CMR.MoveLast();
							modDataTypes.Statics.CMR.MoveNext();
						}
					}
					Statics.SummaryList[Statics.SummaryListIndex].srecordnumber = modGlobalVariables.Statics.gintLastAccountNumber;
					Statics.SummaryList[Statics.SummaryListIndex].CardNumber = modGlobalVariables.Statics.intCurrentCard;
					Statics.SummaryList[Statics.SummaryListIndex].dwellcode = modDataTypes.Statics.MR.Get_Fields_Int32("ribldgcode");
					Statics.SummaryList[Statics.SummaryListIndex].LandCode = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("rilandcode"));
					Statics.SummaryList[Statics.SummaryListIndex].ssfla = FCConvert.ToString(0);
				}
				else
				{
					// Call DWL.FindFirstRecord2("rsaccount,rscard,saledate", gintLastAccountNumber & "," & intCurrentCard & "," & MR.Fields("saledate"), ",")
					modDataTypes.Statics.DWL.FindFirst("rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'");
					// Call OUT.FindFirstRecord2("rsaccount,rscard,saledate", gintLastAccountNumber & "," & intCurrentCard & "," & MR.Fields("saledate"), ",")
					modDataTypes.Statics.OUT.FindFirst("rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'");
					// Call CMR.FindFirstRecord2("rsaccount,rscard,saledate", gintLastAccountNumber & "," & intCurrentCard & "," & MR.Fields("saledate"), ",")
					modDataTypes.Statics.CMR.FindFirst("rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'");
				}
				// 
				// For intCounter = 1 To gintMaxCards
				// Call fillolsflarray
				// If intCounter > 0 Then LandSummaryTotal(intCounter - 1) = LandTotal(intCounter - 1)
				modProperty.Statics.LandSummaryTotal[modGlobalVariables.Statics.intCurrentCard - 1] = modProperty.Statics.LandTotal[modGlobalVariables.Statics.intCurrentCard - 1];
				FCUtils.EraseSafe(modProperty.Statics.LandTotal);
				FCUtils.EraseSafe(modOutbuilding.Statics.lngOutbuildingTotal);
				// Call REAS03_3500_COMPUTE_LAND
				Speed_COMPUTE_LAND();
				if (modProperty.Statics.RUNTYPE == "D")
				{
					modProperty.Speed_PROPERTY_DATA();
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].DwellingOrCommercial = modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "";
				}
				Statics.gstrDwellCode = modDataTypes.Statics.MR.Get_Fields_String("rsdwellingCode") + "";
				if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" == "Y")
				{
					// Call REAS03_3020_DWELLING
					Speed_DWELLING();
				}
				else if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" == "C" && !modDataTypes.Statics.CMR.EndOfFile())
				{
					// corey
					if (modGNBas.Statics.gboolCommercial)
					{
						new_reas03_4300_economic_percent();
						Speed_COMMERCIAL();
					}
					else
					{
						modDwelling.Statics.HoldCondition = modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition");
						modDwelling.Statics.HoldGrade1 = modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality") + 1;
						modDwelling.Statics.HoldGrade2 = modDataTypes.Statics.CMR.Get_Fields_Int32("c1grade");
					}
				}
				if (((Strings.UCase(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") != "Y") && ((Strings.UCase(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") != "C") || !modGNBas.Statics.gboolCommercial)) || (fecherFoundation.FCUtils.IsNull(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"))))
				{
				}
				for (x = 1; x <= 10; x++)
				{
					modOutbuilding.Statics.contributestosfla[x] = false;
					modOutbuilding.Statics.lngRepCostNewLD[x] = 0;
				}
				// x
				if ((modDataTypes.Statics.MR.Get_Fields_String("rsoutbuildingcode") + "" != "Y") && (Strings.UCase(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "Y"))
				{
					modDwelling.Statics.dblFunctionalPercent = modGlobalVariables.Statics.dbldwellfunctional;
					modDwelling.SpeedResidentialSizeFactor();
					if (modProperty.Statics.RUNTYPE == "D")
					{
						modDwelling.Speed_DWELLING_DATA();
					}
				}
				if (modDataTypes.Statics.MR.Get_Fields_String("rsoutbuildingcode") + "" == "Y")
				{
					new_reas03_4300_economic_percent();
					modOutbuilding.Speed_COMPUTE_OUTBUILDING();
					if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" == "Y")
					{
						// printdwellingdata
						if (modProperty.Statics.RUNTYPE == "D")
						{
							modDwelling.Speed_DWELLING_DATA();
						}
						// If RUNTYPE$ = "D" Then Print #40, "XXXXXDWEL"
					}
					if (modProperty.Statics.RUNTYPE == "D")
					{
						// Call REAS03_6000_OUTBUILDING_DATA
						modOutbuilding.Speed_OUTBUILDING_DATA();
					}
					// If RUNTYPE$ = "D" Then Print #40, "XXXXXOUTB"
				}
				// save outbuilding values to summrecord
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					Statics.SummaryList[Statics.SummaryListIndex].sobv1 = modOutbuilding.Statics.lngRepCostNewLD[1];
					Statics.SummaryList[Statics.SummaryListIndex].sobc1 = modOutbuilding.Statics.intOIType[1];
					Statics.SummaryList[Statics.SummaryListIndex].sobv2 = modOutbuilding.Statics.lngRepCostNewLD[2];
					Statics.SummaryList[Statics.SummaryListIndex].sobc2 = modOutbuilding.Statics.intOIType[2];
					Statics.SummaryList[Statics.SummaryListIndex].sobv3 = modOutbuilding.Statics.lngRepCostNewLD[3];
					Statics.SummaryList[Statics.SummaryListIndex].sobc3 = modOutbuilding.Statics.intOIType[3];
					Statics.SummaryList[Statics.SummaryListIndex].sobv4 = modOutbuilding.Statics.lngRepCostNewLD[4];
					Statics.SummaryList[Statics.SummaryListIndex].sobc4 = modOutbuilding.Statics.intOIType[4];
					Statics.SummaryList[Statics.SummaryListIndex].sobv5 = modOutbuilding.Statics.lngRepCostNewLD[5];
					Statics.SummaryList[Statics.SummaryListIndex].sobc5 = modOutbuilding.Statics.intOIType[5];
					Statics.SummaryList[Statics.SummaryListIndex].sobv6 = modOutbuilding.Statics.lngRepCostNewLD[6];
					Statics.SummaryList[Statics.SummaryListIndex].sobc6 = modOutbuilding.Statics.intOIType[6];
					Statics.SummaryList[Statics.SummaryListIndex].sobv7 = modOutbuilding.Statics.lngRepCostNewLD[7];
					Statics.SummaryList[Statics.SummaryListIndex].sobc7 = modOutbuilding.Statics.intOIType[7];
					Statics.SummaryList[Statics.SummaryListIndex].sobv8 = modOutbuilding.Statics.lngRepCostNewLD[8];
					Statics.SummaryList[Statics.SummaryListIndex].sobc8 = modOutbuilding.Statics.intOIType[8];
					Statics.SummaryList[Statics.SummaryListIndex].sobv9 = modOutbuilding.Statics.lngRepCostNewLD[9];
					Statics.SummaryList[Statics.SummaryListIndex].sobc9 = modOutbuilding.Statics.intOIType[9];
					Statics.SummaryList[Statics.SummaryListIndex].sobv10 = modOutbuilding.Statics.lngRepCostNewLD[10];
					Statics.SummaryList[Statics.SummaryListIndex].sobc10 = modOutbuilding.Statics.intOIType[10];
				}
				// If RUNTYPE$ = "D" Then Print #40, "XXXXXOUTB"
				modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(modProperty.Statics.LandTotal[modGNBas.Statics.gintCardNumber]);
				modProperty.Statics.LandSummaryTotal[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard];
				modProperty.Statics.lngCurrentBldg[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard]);
				modProperty.Statics.lngPreviousLand[modGlobalVariables.Statics.intCurrentCard] = modDataTypes.Statics.MR.Get_Fields_Int32("hlcompvalland");
				modProperty.Statics.lngPreviousBldg[modGlobalVariables.Statics.intCurrentCard] = modDataTypes.Statics.MR.Get_Fields_Int32("hlcompvalbldg");
				modProperty.Statics.lngMarketBldg[modGlobalVariables.Statics.intCurrentCard] = modDataTypes.Statics.MR.Get_Fields_Int32("HLALT1BLDG");
				modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] = modDataTypes.Statics.MR.Get_Fields_Int32("HLALT2BLDG") - modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard];
				// Perform Rounding
				// lngCurrentLand(intCurrentCard) = ((lngCurrentLand(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
				modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound(ref modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard]);
				// lngCurrentBldg(intCurrentCard) = ((lngCurrentBldg(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
				modProperty.Statics.lngCurrentBldg[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound(ref modProperty.Statics.lngCurrentBldg[modGlobalVariables.Statics.intCurrentCard]);
				// lngPreviousLand(intCurrentCard) = ((lngPreviousLand(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
				modProperty.Statics.lngPreviousLand[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound(ref modProperty.Statics.lngPreviousLand[modGlobalVariables.Statics.intCurrentCard]);
				// lngPreviousBldg(intCurrentCard) = ((lngPreviousBldg(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
				modProperty.Statics.lngPreviousBldg[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound(ref modProperty.Statics.lngPreviousBldg[modGlobalVariables.Statics.intCurrentCard]);
				// lngMarketBldg(intCurrentCard) = ((lngMarketBldg(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
				modProperty.Statics.lngMarketBldg[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound(ref modProperty.Statics.lngMarketBldg[modGlobalVariables.Statics.intCurrentCard]);
				// lngIncomeBldg(intCurrentCard) = ((lngIncomeBldg(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
				modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound(ref modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard]);
				modProperty.Statics.intTemp = modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode");
				if (modProperty.Statics.intTemp == 0)
					modProperty.Statics.intTemp = 1;
				modProperty.Statics.intOverRideCodeLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.intTemp;
				modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngPreviousLand[modGlobalVariables.Statics.intCurrentCard];
				// If boolUpdateWhenCalculate Then
				switch (modProperty.Statics.intTemp)
				{
					case 1:
						{
							// If boolUpdateWhenCalculate Then
							modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard];
							// End If
							break;
						}
					case 2:
						{
							modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngPreviousLand[modGlobalVariables.Statics.intCurrentCard];
							break;
						}
					case 3:
						{
							modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngMarketLand[modGlobalVariables.Statics.intCurrentCard];
							break;
						}
					case 4:
						{
							modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngIncomeLand[modGlobalVariables.Statics.intCurrentCard];
							break;
						}
					case 6:
						{
							modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modDataTypes.Statics.MR.Get_Fields_Int32("HLVALLAND");
							break;
						}
					default:
						{
							if (modGlobalVariables.Statics.boolUpdateWhenCalculate)
							{
								modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngCurrentLand[modGlobalVariables.Statics.intCurrentCard];
							}
							break;
						}
				}
				//end switch
				modProperty.Statics.lngOverRideLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard];
				modProperty.Statics.lngIncomeLand[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard];
				modProperty.Statics.intTemp = modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode");
				if (modProperty.Statics.intTemp == 0)
					modProperty.Statics.intTemp = 1;
				modProperty.Statics.intOverRideCodeBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.intTemp;
				modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngPreviousBldg[modGlobalVariables.Statics.intCurrentCard];
				// If boolUpdateWhenCalculate Then
				switch (modProperty.Statics.intTemp)
				{
					case 1:
						{
							// If boolUpdateWhenCalculate Then
							modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngCurrentBldg[modGlobalVariables.Statics.intCurrentCard];
							// End If
							break;
						}
					case 2:
						{
							modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngPreviousBldg[modGlobalVariables.Statics.intCurrentCard];
							break;
						}
					case 3:
						{
							modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngMarketBldg[modGlobalVariables.Statics.intCurrentCard];
							break;
						}
					case 4:
						{
							Statics.clsLoadIncomeData.OpenRecordset("select indicatedvalue from incomevalue where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
							if (!Statics.clsLoadIncomeData.EndOfFile())
							{
								modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Conversion.Val(Statics.clsLoadIncomeData.Get_Fields_Int32("indicatedvalue")) - modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard]);
							}
							else
							{
								modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] = 0;
							}
							modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard];
							break;
						}
					case 6:
						{
							modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modDataTypes.Statics.MR.Get_Fields_Int32("HLVALBLDG");
							break;
						}
					default:
						{
							if (modGlobalVariables.Statics.boolUpdateWhenCalculate)
							{
								modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngCurrentBldg[modGlobalVariables.Statics.intCurrentCard];
							}
							break;
						}
				}
				//end switch
				modProperty.Statics.lngOverRideBldg[modGlobalVariables.Statics.intCurrentCard] = modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard];
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1452, "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				modProperty.Statics.lngCorrelatedLand[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound_2(FCConvert.ToInt32(modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] * (FCConvert.ToInt32(Math.Round(Conversion.Val(modGlobalVariables.Statics.CostRec.CBase))) / 100.0)));
				modProperty.Statics.lngCorrelatedBldg[modGlobalVariables.Statics.intCurrentCard] = modGlobalRoutines.RERound_2(FCConvert.ToInt32(modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] * (FCConvert.ToInt32(Math.Round(Conversion.Val(modGlobalVariables.Statics.CostRec.CBase))) / 100.0)));
				modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngCorrelatedLand[modGlobalVariables.Statics.intCurrentCard]);
				modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngCorrelatedBldg[modGlobalVariables.Statics.intCurrentCard]);
				if (modGlobalVariables.Statics.boolUpdateWhenCalculate)
				{
					// Call SaveMasterTable(MR, gintLastAccountNumber, gintCardNumber)
					modDataTypes.Statics.MR.Set_Fields("hlcompvalland", modProperty.Statics.lngCorrelatedLand[modGlobalVariables.Statics.intCurrentCard]);
					modDataTypes.Statics.MR.Set_Fields("hlcompvalbldg", modProperty.Statics.lngCorrelatedBldg[modGlobalVariables.Statics.intCurrentCard]);
					modDataTypes.Statics.MR.Update();
					// MR.Edit
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						clsTemp.Execute("update dwelling set disFLa = " + FCConvert.ToString(modDwelling.Statics.intSFLA) + ", dwellvalue = " + FCConvert.ToString(Conversion.Val(Statics.SummaryList[Statics.SummaryListIndex].sdwellrcnld)) + " where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard), modGlobalVariables.strREDatabase);
					}
					else
					{
						clsTemp.Execute("update dwelling set disFLa = " + FCConvert.ToString(modDwelling.Statics.intSFLA) + " where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard), modGlobalVariables.strREDatabase);
					}
					clsTemp.Execute("update commercial set sqft = " + FCConvert.ToString(modDwelling.Statics.intSFLA) + " where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard), modGlobalVariables.strREDatabase);
				}
				if (!modGlobalVariables.Statics.boolfromcalcandassessment)
				{
					modGlobalVariables.Statics.SoftAcresArray[modGlobalVariables.Statics.intCurrentCard - 1] = modGlobalVariables.Statics.SoftAcres;
					modGlobalVariables.Statics.HardAcresArray[modGlobalVariables.Statics.intCurrentCard - 1] = modGlobalVariables.Statics.HardAcres;
					modGlobalVariables.Statics.MixedAcresArray[modGlobalVariables.Statics.intCurrentCard - 1] = modGlobalVariables.Statics.MixedAcres;
					modGlobalVariables.Statics.SoftValueArray[modGlobalVariables.Statics.intCurrentCard - 1] = modGlobalVariables.Statics.SoftValue;
					modGlobalVariables.Statics.HardValueArray[modGlobalVariables.Statics.intCurrentCard - 1] = modGlobalVariables.Statics.HardValue;
					modGlobalVariables.Statics.MixedValueArray[modGlobalVariables.Statics.intCurrentCard - 1] = modGlobalVariables.Statics.MixedValue;
					modDataTypes.Statics.MR.MoveNext();
					if (!modDataTypes.Statics.MR.EndOfFile())
					{
						modDataTypes.Statics.MR.Edit();
						if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount")) == modGlobalVariables.Statics.gintLastAccountNumber)
						{
							goto CalcAnotherCard;
						}
						else
						{
							modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
						}
					}
				}
				if (modGlobalVariables.Statics.boolfromcalcandassessment)
				{
				}
				else if (modProperty.Statics.RUNTYPE == "D" && !modProperty.Statics.boolFromProperty)
				{
					//if (modPrintRoutines.Statics.boolPrintCalculation)
					//{
					//	// COREY
					//	// rptSingleCalculation.Show 1
					//	// rptSingleCalculation.Run (True)
					//	// rptSingleCalculation.PrintReport (False)
					//	// Unload rptSingleCalculation
					//	// ElseIf gboolPrintFormOpen Then
					//	// 
					//	// ElseIf boolRangePrintCalculation Then
					//	// rptRangeCalculation.Show 1
					//	// rptRangeCalculation.Run (True)
					//	// rptRangeCalculation.PrintReport (False)
					//	// Unload rptRangeCalculation
					//}
					//else
					//{
						// Unload frmValuationReport
						// Load frmValuationReport
						// frmValuationReport.Show 1, MDIParent
						if (Statics.boolCalcErrors)
						{
							//Application.DoEvents();
							MessageBox.Show("There were errors during calculation." + "\r\n" + "Following is a list of errors.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							//! Load frmShowResults;
							frmShowResults.InstancePtr.Init(ref Statics.CalcLog);
							frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							//Application.DoEvents();
							frmShowResults.InstancePtr.Show(FCForm.FormShowEnum.Modal);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
						}
						frmNewValuationReport.InstancePtr.Init(ref modGlobalVariables.Statics.intCurrentCard);
						frmNewValuationReport.InstancePtr.Show(App.MainForm);
					//}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (modGlobalVariables.Statics.boolfromcalcandassessment)
				{
					Statics.boolCalcErrors = true;
					Statics.CalcLog += "Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + " Occurred in function CalcCard." + " With Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard) + "\r\n";
				}
				else
				{
					string strEMessage = "";
					strEMessage = "Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "Occurred in function CalcCard" + "\r\n" + "With Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGlobalVariables.Statics.intCurrentCard);
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						frmNewREProperty.InstancePtr.cmdCalculate.Enabled = true;
					}
					else
					{
						frmREProperty.InstancePtr.mnuCalculate.Enabled = true;
					}
					//Application.DoEvents();
					MessageBox.Show(strEMessage, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		public static void Speed_COMPUTE_LAND()
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolNoLand;
			try
			{
				// On Error GoTo ErrorHandler
				boolNoLand = true;
				modGlobalVariables.Statics.SoftAcres = 0;
				modGlobalVariables.Statics.HardAcres = 0;
				modGlobalVariables.Statics.MixedAcres = 0;
				modGlobalVariables.Statics.SoftValue = 0;
				modGlobalVariables.Statics.HardValue = 0;
				modGlobalVariables.Statics.MixedValue = 0;
				for (x = 1; x <= 7; x++)
				{
					modREASValuations.Statics.Value[x] = 0;
					modGlobalVariables.Statics.Acres[x] = 0;
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(x) + "type") + "") > 0)
					{
						boolNoLand = false;
					}
				}
				// x
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood")) == 0)
					modDataTypes.Statics.MR.Set_Fields("pineighborhood", 1);
				modREASValuations.Statics.WorkNeighborhood = modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood");
				modREASValuations.Statics.WorkZone = modDataTypes.Statics.MR.Get_Fields_Int32("pizone");
				if (modDataTypes.Statics.MR.Get_Fields_Int32("piseczone") > 0 && modDataTypes.Statics.MR.Get_Fields_Boolean("zoneoverride"))
				{
					modREASValuations.Statics.WorkZone = modDataTypes.Statics.MR.Get_Fields_Int32("piseczone");
				}
				if (modREASValuations.Statics.WorkZone < 1)
				{
					Statics.boolCalcErrors = true;
					Statics.CalcLog += "Warning: Default zone used because no zone is assigned to Acct " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " Card " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "\r\n";
				}
				REAS03_3501_TAGA:
				;
				modProperty.Statics.LandFactor = modGlobalVariables.Statics.LandTRec.LandFactor(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone);
				modProperty.Statics.WKey = FCConvert.ToInt32(modGlobalVariables.Statics.LandTRec.Schedule(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone));
				modGlobalRoutines.get_Land_Schedule_Rec(ref modProperty.Statics.WKey);
				modREASValuations.Statics.Schedule = modProperty.Statics.WKey;
				if (modREASValuations.Statics.Schedule == 0)
				{
					modREASValuations.Statics.Schedule = modGlobalVariables.Statics.CustomizedInfo.DefaultSchedule;
					if (modREASValuations.Statics.Schedule > 0 && !boolNoLand)
					{
						// Dave 08/13/07 Call# 116962 Added following line to set values needed to calc actual land value.  Before this was added value was set to 0 when calculated
						modGlobalRoutines.get_Land_Schedule_Rec(ref modREASValuations.Statics.Schedule);
						Statics.boolCalcErrors = true;
						Statics.CalcLog += "Warning: Default schedule " + FCConvert.ToString(modREASValuations.Statics.Schedule) + " used because no schedule is defined for Neighborhood " + FCConvert.ToString(modREASValuations.Statics.WorkNeighborhood) + " Zone " + FCConvert.ToString(modREASValuations.Statics.WorkZone) + " Acct " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " Card " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "\r\n";
					}
				}
				if (modREASValuations.Statics.Schedule == 0)
				{
					if (!boolNoLand)
					{
						Statics.boolCalcErrors = true;
						Statics.CalcLog += "No Schedule defined for Neighborhood " + FCConvert.ToString(modREASValuations.Statics.WorkNeighborhood) + " Zone " + FCConvert.ToString(modREASValuations.Statics.WorkZone) + " Acct " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " Card " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "\r\n";
						return;
					}
				}
				modREASValuations.Statics.StandardDepth = FCConvert.ToInt32(modGlobalVariables.Statics.LandSRec.StandardDepth);
				modREASValuations.Statics.StandardLot = modGlobalVariables.Statics.LandSRec.StandardLot;
				modREASValuations.Statics.StandardWidth = modGlobalVariables.Statics.LandSRec.StandardWidth;
				modProperty.Statics.WKey = 1451;
				// Call REAS03_1600_GET_COST_RECORD
				// Call OpenCRTable(CR, WKEY, dbOpenForwardOnly)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				// LandTrend = cr.fields("cunit") / 100
				modProperty.Statics.LandTrend = Conversion.Val(modGlobalVariables.Statics.CostRec.CUnit) / 100;
				modREASValuations.Statics.Acreage = 0;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland1type") + "") < 11)
					goto REAS03_3501_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland1type") + "") - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland1unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland1unitsb");
				modREASValuations.Statics.WORKA2 = modDataTypes.Statics.MR.Get_Fields_Double("piland1inf") + "";
				modProperty.Statics.WORKA3 = modDataTypes.Statics.MR.Get_Fields_Int32("piland1infcode") + "";
				modREASValuations.Statics.Row = 1;
				modGlobalVariables.Statics.AcresIndex = 1;
				New_Select_Method();
				REAS03_3501_TAG:
				;
				if (modREASValuations.Statics.FirstLandFactor == "Y")
				{
					modProperty.Statics.WORK1 = 0;
					New_Select_Method();
				}
				// If Val(mr.fields("piland2type") & "") < 11 Then GoTo REAS03_3507_TAG
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland2type") + "") < 11)
					goto REAS03_3502_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland2type") + "") - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland2unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland2unitsb");
				modREASValuations.Statics.WORKA2 = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland2inf") + ""));
				modProperty.Statics.WORKA3 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland2infcode") + "");
				modREASValuations.Statics.Row = 2;
				modGlobalVariables.Statics.AcresIndex = 2;
				New_Select_Method();
				REAS03_3502_TAG:
				;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland3type") + "") < 11)
					goto REAS03_3503_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland3type")) - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland3unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland3unitsb");
				modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Double("piland3inf"));
				modProperty.Statics.WORKA3 = modDataTypes.Statics.MR.Get_Fields_Int32("piland3infcode");
				modREASValuations.Statics.Row = 3;
				modGlobalVariables.Statics.AcresIndex = 3;
				New_Select_Method();
				REAS03_3503_TAG:
				;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland4type") + "") < 11)
					goto REAS03_3504_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland4type") + "") - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland4unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland4unitsb");
				modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Double("piland4inf"));
				modProperty.Statics.WORKA3 = modDataTypes.Statics.MR.Get_Fields_Int32("piland4infcode");
				modREASValuations.Statics.Row = 4;
				modGlobalVariables.Statics.AcresIndex = 4;
				New_Select_Method();
				REAS03_3504_TAG:
				;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("PILAND5TYPE") + "") < 11)
					goto REAS03_3505_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("PILAND5TYPE")) - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland5unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland5unitsb");
				modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Double("piland5inf"));
				modProperty.Statics.WORKA3 = modDataTypes.Statics.MR.Get_Fields_Int32("piland5infcode");
				modREASValuations.Statics.Row = 5;
				modGlobalVariables.Statics.AcresIndex = 5;
				New_Select_Method();
				REAS03_3505_TAG:
				;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland6type") + "") < 11)
					goto REAS03_3506_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland6type")) - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland6unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland6unitsb");
				modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Double("piland6inf"));
				modProperty.Statics.WORKA3 = modDataTypes.Statics.MR.Get_Fields_Int32("piland6infcode");
				modREASValuations.Statics.Row = 6;
				modGlobalVariables.Statics.AcresIndex = 6;
				New_Select_Method();
				REAS03_3506_TAG:
				;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland7type") + "") < 11)
					goto REAS03_3507_TAG;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland7type")) - 10;
				modProperty.Statics.WORKA1 = modDataTypes.Statics.MR.Get_Fields_String("piland7unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland7unitsb");
				modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Double("piland7inf"));
				modProperty.Statics.WORKA3 = modDataTypes.Statics.MR.Get_Fields_Int32("piland7infcode");
				modREASValuations.Statics.Row = 7;
				modGlobalVariables.Statics.AcresIndex = 7;
				New_Select_Method();
				REAS03_3507_TAG:
				;
				// now store values in summ tablw
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					// Call OpenSRTable(SR, gintLastAccountNumber, gintCardNumber)
					// For x = 1 To 7
					// Select Case Val(MR.Fields("piland" & x & "type") & "")
					// Case SoftType
					// SoftAcres = SoftAcres + Acres(x)
					// SoftValue = SoftValue + Value(x)
					// Case HardType
					// HardAcres = HardAcres + Acres(x)
					// HardValue = HardValue + Value(x)
					// Case MixedType
					// MixedAcres = MixedAcres + Acres(x)
					// MixedValue = MixedValue + Value(x)
					// End Select
					// 
					// Next x
					for (x = 1; x <= 7; x++)
					{
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("piland" + FCConvert.ToString(x) + "type") + ""))))
						{
							if (!modGlobalVariables.Statics.LandTypes.IsFarmTreeGrowth())
							{
								if (modGlobalVariables.Statics.LandTypes.IsSoftWood())
								{
									modGlobalVariables.Statics.SoftAcres += modGlobalVariables.Statics.Acres[x];
									modGlobalVariables.Statics.SoftValue += modREASValuations.Statics.Value[x];
								}
								else if (modGlobalVariables.Statics.LandTypes.IsHardWood())
								{
									modGlobalVariables.Statics.HardAcres += modGlobalVariables.Statics.Acres[x];
									modGlobalVariables.Statics.HardValue += modREASValuations.Statics.Value[x];
								}
								else if (modGlobalVariables.Statics.LandTypes.IsMixedWood())
								{
									modGlobalVariables.Statics.MixedAcres += modGlobalVariables.Statics.Acres[x];
									modGlobalVariables.Statics.MixedValue += modREASValuations.Statics.Value[x];
								}
							}
						}
					}
					// x
					modGlobalVariables.Statics.SoftAcres = modGlobalRoutines.Round(modGlobalVariables.Statics.SoftAcres, 3);
					modGlobalVariables.Statics.HardAcres = modGlobalRoutines.Round(modGlobalVariables.Statics.HardAcres, 3);
					modGlobalVariables.Statics.MixedAcres = modGlobalRoutines.Round(modGlobalVariables.Statics.MixedAcres, 3);
					Statics.SummaryList[Statics.SummaryListIndex].slandv1 = FCConvert.ToInt32(modREASValuations.Statics.Value[1]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc1 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland1type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda1 = modGlobalVariables.Statics.Acres[1];
					Statics.SummaryList[Statics.SummaryListIndex].slandv2 = FCConvert.ToInt32(modREASValuations.Statics.Value[2]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc2 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland2type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda2 = modGlobalVariables.Statics.Acres[2];
					Statics.SummaryList[Statics.SummaryListIndex].slandv3 = FCConvert.ToInt32(modREASValuations.Statics.Value[3]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc3 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland3type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda3 = modGlobalVariables.Statics.Acres[3];
					Statics.SummaryList[Statics.SummaryListIndex].slandv4 = FCConvert.ToInt32(modREASValuations.Statics.Value[4]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc4 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland4type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda4 = modGlobalVariables.Statics.Acres[4];
					Statics.SummaryList[Statics.SummaryListIndex].slandv5 = FCConvert.ToInt32(modREASValuations.Statics.Value[5]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc5 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland5type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda5 = modGlobalVariables.Statics.Acres[5];
					Statics.SummaryList[Statics.SummaryListIndex].slandv6 = FCConvert.ToInt32(modREASValuations.Statics.Value[6]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc6 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland6type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda6 = modGlobalVariables.Statics.Acres[6];
					Statics.SummaryList[Statics.SummaryListIndex].slandv7 = FCConvert.ToInt32(modREASValuations.Statics.Value[7]);
					Statics.SummaryList[Statics.SummaryListIndex].slandc7 = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("piland7type"));
					Statics.SummaryList[Statics.SummaryListIndex].slanda7 = modGlobalVariables.Statics.Acres[7];
					modDataTypes.Statics.MR.Set_Fields("piacres", modGlobalRoutines.Round(FCConvert.ToDouble(modREASValuations.Statics.Acreage), 2));
					modDataTypes.Statics.MR.Set_Fields("rssoft", Strings.Format(modGlobalVariables.Statics.SoftAcres, "0.00"));
					modDataTypes.Statics.MR.Set_Fields("rshard", Strings.Format(modGlobalVariables.Statics.HardAcres, "0.00"));
					modDataTypes.Statics.MR.Set_Fields("rsmixed", Strings.Format(modGlobalVariables.Statics.MixedAcres, "0.00"));
					modDataTypes.Statics.MR.Set_Fields("rssoftvalue", modGlobalVariables.Statics.SoftValue);
					modDataTypes.Statics.MR.Set_Fields("rshardvalue", modGlobalVariables.Statics.HardValue);
					modDataTypes.Statics.MR.Set_Fields("rsMixedValue", modGlobalVariables.Statics.MixedValue);
					modDataTypes.Statics.MR.Set_Fields("rsother", Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres")) - FCConvert.ToDouble(Strings.Format(modGlobalVariables.Statics.SoftAcres, "0.00")) - FCConvert.ToDouble(Strings.Format(modGlobalVariables.Statics.HardAcres, "0.00")) - FCConvert.ToDouble(Strings.Format(modGlobalVariables.Statics.MixedAcres, "0.00")));
					modDataTypes.Statics.MR.Set_Fields("rsothervalue", modProperty.Statics.LandTotal[modGNBas.Statics.gintCardNumber] - (modGlobalVariables.Statics.SoftValue + modGlobalVariables.Statics.HardValue + modGlobalVariables.Statics.MixedValue));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				Statics.boolCalcErrors = true;
				Statics.CalcLog += "Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + " In Speed_compute_land. Acct " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " Card " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "\r\n";
			}
		}

		private static void Speed_Dwell_economic_percent()
		{
			double dblHoldDwellingEconomic = 0;
			if (modProperty.Statics.WKey < 1)
			{
				Statics.boolCalcErrors = true;
				Statics.CalcLog += "Land Table Record " + FCConvert.ToString(modProperty.Statics.WKey) + " doesn't exist.  Can't Finish calculating this account" + "\r\n";
				return;
			}
			if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "Y")
			{
				modDwelling.Statics.dblEconomicPercent = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctecon") + "") * modGlobalVariables.Statics.LandTRec.BldgFactor(ref modProperty.Statics.intWorkNeighborhood, ref modProperty.Statics.intWorkZone) / 100;
				modProperty.Statics.WKey = modDataTypes.Statics.DWL.Get_Fields_Int32("dieconcode");
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "ECONOMIC");
				if (Conversion.Val(modGlobalVariables.Statics.CostRec.CUnit) > 9999)
				{
					Statics.boolCalcErrors = true;
					Statics.CalcLog += "Economic Code is " + modDataTypes.Statics.DWL.Get_Fields_Int32("diECONCODE") + " and the cost files do not have a Unit " + "\r\n" + "set up for that Economic Code.  Economic Percent Good will be " + FCConvert.ToString(modDwelling.Statics.dblEconomicPercent * 100) + ". Current Account is" + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "\r\n";
					modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(1);
				}
				modDwelling.Statics.dblEconomicPercent *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit);
				dblHoldDwellingEconomic = modDwelling.Statics.dblEconomicPercent;
			}
			else if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C" && modGNBas.Statics.gboolCommercial)
			{
				modDwelling.Statics.dblEconomicPercent = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("cmecon")) * modGlobalVariables.Statics.LandTRec.BldgFactor(ref modProperty.Statics.intWorkNeighborhood, ref modProperty.Statics.intWorkZone) / 100;
				dblHoldDwellingEconomic = modDwelling.Statics.dblEconomicPercent;
			}
			else
			{
				if (modGlobalVariables.Statics.intCurrentCard == 1 || FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rspropertycode")) == "Y")
				{
					if (dblHoldDwellingEconomic != 0 && modGlobalVariables.Statics.intCurrentCard != 1)
					{
						modDwelling.Statics.dblEconomicPercent = dblHoldDwellingEconomic;
					}
					else
					{
						modDwelling.Statics.dblEconomicPercent = modGlobalVariables.Statics.LandTRec.BldgFactor(ref modProperty.Statics.intWorkNeighborhood, ref modProperty.Statics.intWorkZone);
					}
				}
				else
				{
					modDwelling.Statics.dblEconomicPercent = modDwelling.Statics.dblEconomicPercent;
				}
			}
			if (modGlobalVariables.Statics.intCurrentCard > 1 && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rspropertycode")) != "Y")
			{
				if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C" || !modGNBas.Statics.gboolCommercial))
					goto REAS03_4302_TAG;
			}
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") < 1 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") > 9)
				goto REAS03_4301_TAG;
			modProperty.Statics.WKey = FCConvert.ToInt32(1330 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + ""));
			// Call OpenCRTable(CR, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
				goto REAS03_4301_TAG;
			modDwelling.Statics.dblEconomicPercent *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 100;
			REAS03_4301_TAG:
			;
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + " ") < 1 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2")) > 9)
				goto REAS03_4302_TAG;
			modProperty.Statics.WKey = FCConvert.ToInt32(1340 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + " "));
			// Call OpenCRTable(CR, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
				goto REAS03_4302_TAG;
			modDwelling.Statics.dblEconomicPercent *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 100;
			REAS03_4302_TAG:
			;
		}

		public static void Speed_DWELLING()
		{
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pizone")) < 1)
			{
				Statics.boolCalcErrors = true;
				Statics.CalcLog += "Unable to correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " because the zone is invalid." + "\r\n";
				return;
			}
			modProperty.Statics.intWorkZone = modDataTypes.Statics.MR.Get_Fields_Int32("pizone");
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone")) > 0 && modDataTypes.Statics.MR.Get_Fields_Boolean("zoneoverride"))
			{
				modProperty.Statics.intWorkZone = modDataTypes.Statics.MR.Get_Fields_Int32("piseczone");
			}
			modProperty.Statics.intWorkNeighborhood = modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood");
			if (modProperty.Statics.intWorkNeighborhood < 1)
			{
				MessageBox.Show("Unable to correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n" + " because the neighborhood is invalid.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			// Call REAS03_4300_ECONOMIC_PERCENT
			Speed_Dwell_economic_percent();
			// Call new_compute_dwelling
			modDwelling.Speed_compute_dwelling();
		}

		public static void Speed_COMMERCIAL()
		{
			// Call REAS03_4400_COMPUTE_COMMERCIAL
			modCommercial.Speed_compute_commercial();
			if (modProperty.Statics.RUNTYPE == "D")
			{
				// Call REAS03_5600_COMMERCIAL_DATA
				modCommercial.Speed_COMMERCIAL_DATA();
			}
		}

		public static void SpeedPrintProperty()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// With frmNewValuationReport.SaleGrid(intCurrentCard - 1)
				if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate")))
				{
					//FC:FINAL:MSH - i.issue #1026: incorrect convert from datetime to int
					//if (Information.IsDate(modDataTypes.MR.Get_Fields("saledate") + "") && (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0))
					if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate") + "") && (modDataTypes.Statics.MR.Get_Fields_DateTime("saledate").ToOADate() != 0))
					{
						// .TextMatrix(0, 1) = MR.Fields("saledate")
						//FC:FINAL:MSH - i.issue #1193: save date in short format
						//CalcPropertyList[CalcPropertyIndex].SaleDate = FCConvert.ToString(modDataTypes.MR.Get_Fields("saledate"));
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].SaleDate = Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
					}
					else
					{
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].SaleDate = "";
					}
				}
				else
				{
					// .TextMatrix(0, 1) = ""
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].SaleDate = "";
				}
				// .TextMatrix(1, 1) = Format(MR.Fields("pisaleprice") & "", "#,###,###,###")
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].SalePrice = Strings.Format(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "", "#,###,###,###");
				modProperty.Statics.WKey = FCConvert.ToInt32(1350 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("PISALETYPE") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].SaleType = modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = FCConvert.ToInt32(1360 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalefinancing") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Financing = modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = FCConvert.ToInt32(1370 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleverified") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Verified = modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = FCConvert.ToInt32(1380 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Validity = modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood");
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "Property", "Neighborhood", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].NeighborhoodCode = FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood"));
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].NeighborhoodDesc = modGlobalVariables.Statics.CostRec.ClDesc;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pistreetcode") + "") != 0)
				{
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].TreeGrowth = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("pistreetcode"));
				}
				else
				{
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].TreeGrowth = "";
				}
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pizone"))), "Property", "Zone", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone") + "")), "Property", "SecZone", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Zone = modProperty.Statics.WORKA1 + " " + modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = FCConvert.ToInt32(1300 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pitopography1") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
				modProperty.Statics.WKey = FCConvert.ToInt32(1300 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pitopography2") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Topography = modProperty.Statics.WORKA1 + "/" + modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = FCConvert.ToInt32(1310 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piutilities1") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
				modProperty.Statics.WKey = FCConvert.ToInt32(1310 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piutilities2") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Utilities = modProperty.Statics.WORKA1 + "/" + modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WKey = FCConvert.ToInt32(1320 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pistreet") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Street = modProperty.Statics.WORKA1;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") == 0)
				{
					modProperty.Statics.WORKA1 = " ";
					modREASValuations.Statics.WORKA2 = " ";
				}
				else
				{
					modProperty.Statics.WKey = 1330;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc + "  ";
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") > 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") < 9)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1330 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + ""));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
					}
					else
					{
						modREASValuations.Statics.WORKA2 = modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "";
					}
				}
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Open1Title = modProperty.Statics.WORKA1;
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Open1 = modREASValuations.Statics.WORKA2;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + " ") == 0)
				{
					modProperty.Statics.WORKA1 = " ";
					modREASValuations.Statics.WORKA2 = " ";
				}
				else
				{
					modProperty.Statics.WKey = 1340;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc + "  ";
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1340 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + " "));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
					}
					else
					{
						modREASValuations.Statics.WORKA2 = modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "";
					}
				}
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Open2Title = modProperty.Statics.WORKA1;
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Open2 = modREASValuations.Statics.WORKA2;
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Ref1 = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + "";
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].Ref2 = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsref2") + "");
				if (modGlobalVariables.Statics.ValuationReportOption == 1 || modGlobalVariables.Statics.ValuationReportOption == 3)
				{
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].TranLandBldgDesc = "Tran/Land/Bldg Codes";
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].TranLandBldgCode = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("RITRANCODE") + "")) + " " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("RILANDCODE") + "")) + " " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("RIBLDGCODE") + ""));
				}
				else if (modGlobalVariables.Statics.ValuationReportOption == 2)
				{
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].TranLandBldgDesc = "BLDG CODES";
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].TranLandBldgCode = modDataTypes.Statics.MR.Get_Fields_Int32("RIBLDGCODE") + "";
				}
				// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
				if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields("pixcoord") + "") != string.Empty || Strings.Trim(modDataTypes.Statics.MR.Get_Fields("piycoord") + "") != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
					if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields("pixcoord") + "") != string.Empty)
					{
						modProperty.Statics.WKey = 1091;
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modProperty.Statics.WORKA1 = "X Coordinate";
						if (Strings.Mid(modGlobalVariables.Statics.CostRec.ClDesc, 1, 2) != "..")
							modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].XCoordTitle = modProperty.Statics.WORKA1;
						// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].XCoord = modDataTypes.Statics.MR.Get_Fields("pixcoord") + "";
					}
					else
					{
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].XCoord = "";
					}
					// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
					if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields("piycoord") + "") != string.Empty)
					{
						modProperty.Statics.WKey = 1092;
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modProperty.Statics.WORKA1 = "Y Coordinate";
						if (Strings.Mid(modGlobalVariables.Statics.CostRec.ClDesc, 1, 2) != "..")
							modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].YCoordTitle = modProperty.Statics.WORKA1;
						// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].YCoord = modDataTypes.Statics.MR.Get_Fields("piycoord") + "";
					}
					else
					{
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].YCoord = "";
					}
				}
				else
				{
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].XCoord = "";
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].YCoord = "";
				}
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd1") + "") != 0 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd2") + "") != 0 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd3") + "") != 0)
				{
					if (modGlobalVariables.Statics.ValuationReportOption == 2 || modGlobalVariables.Statics.ValuationReportOption == 3)
					{
						Statics.CalcPropertyList[Statics.CalcPropertyIndex].ExemptCodes = modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd1") + "  " + modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd2") + "  " + modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd3");
					}
				}
				else
				{
					Statics.CalcPropertyList[Statics.CalcPropertyIndex].ExemptCodes = "";
				}
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].LandSchedule = 0;
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood") + "") == 0)
					return;
				// dont need to process anymore
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood")) == 0)
					modDataTypes.Statics.MR.Set_Fields("pineighborhood", 1);
				modREASValuations.Statics.WorkNeighborhood = modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood");
				modREASValuations.Statics.WorkZone = modDataTypes.Statics.MR.Get_Fields_Int32("pizone");
				if (modDataTypes.Statics.MR.Get_Fields_Boolean("zoneoverride") && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone")) > 0)
				{
					modREASValuations.Statics.WorkZone = modDataTypes.Statics.MR.Get_Fields_Int32("piseczone");
				}
				REAS03_3501_TAGA:
				;
				Statics.CalcPropertyList[Statics.CalcPropertyIndex].LandSchedule = FCConvert.ToInt32(modGlobalVariables.Statics.LandTRec.Schedule(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				Statics.boolCalcErrors = true;
				Statics.CalcLog += "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + " In Print Property" + "\r\n";
			}
		}

		public static void SaveSummary(int lngCommitmentYear = 0)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngAcct = 0;
			int intCardN = 0;
			string strCols;
			string strSQL = "";
			bool boolShowWait;
			try
			{
				// On Error GoTo ErrorHandler
				boolShowWait = false;
				strCols = "(sdwellrcnld,slandc1,slandv1,slandc2,slandv2,slandc3,slandv3,slandc4,slandv4,slandc5,slandv5,slandc6,slandv6,slandc7,slandv7";
				strCols += ",sobc1,sobv1,sobc2,sobv2,sobc3,sobv3,sobc4,sobv4,sobc5,sobv5,sobc6,sobv6,sobc7,sobv7,sobc8,sobv8,sobc9,sobv9,sobc10,sobv10";
				strCols += ",ssfla,sovlandc,sovlanda,sovbldgc,sovbldga,scmocc1,scmval1,scmocc2,scmval2,srecordnumber,slanda1";
				strCols += ",slanda2,slanda3,slanda4,slanda5,slanda6,slanda7,dwellcode,landcode,cardnumber,sage,comval1,comval2";
				if (lngCommitmentYear > 0)
				{
					strCols += ",commitmentyear";
				}
				strCols += ")";
				if (!modGlobalRoutines.Formisloaded_2("frmnewvaluationreport"))
				{
					// Load frmREWait
					// frmREWait.lblMessage = "Saving Summary Data. Please Wait"
					// frmREWait.Show
					frmWait.InstancePtr.Init("Saving Summary Data. Please Wait", false, 100, true);
					boolShowWait = true;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//Application.DoEvents();
				for (x = 1; x <= Statics.SummaryListIndex; x++)
				{
					lngAcct = Statics.SummaryList[x].srecordnumber;
					intCardN = Statics.SummaryList[x].CardNumber;
					if (boolShowWait)
					{
						frmWait.InstancePtr.lblMessage.Text = "Saving Summary Data.      Account " + FCConvert.ToString(lngAcct);
                        frmWait.InstancePtr.Refresh();
                    }
					//Application.DoEvents();
					if (lngCommitmentYear <= 0)
					{
						clsTemp.Execute("Delete from summrecord where srecordnumber = " + FCConvert.ToString(lngAcct) + " and cardnumber = " + FCConvert.ToString(intCardN), modGlobalVariables.strREDatabase, false);
					}
					else
					{
						clsTemp.Execute("Delete from summrecord where srecordnumber = " + FCConvert.ToString(lngAcct) + " and cardnumber = " + FCConvert.ToString(intCardN) + " and commitmentyear = " + FCConvert.ToString(lngCommitmentYear), modGlobalVariables.Statics.strCommitDB, false);
					}
					strSQL = "(' ";
					strSQL += Statics.SummaryList[x].sdwellrcnld + "', " + FCConvert.ToString(Statics.SummaryList[x].slandc1) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv1) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandc2) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv2) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandc3) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv3);
					strSQL += ", " + FCConvert.ToString(Statics.SummaryList[x].slandc4) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv4) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandc5) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv5) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandc6) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv6) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandc7) + ", " + FCConvert.ToString(Statics.SummaryList[x].slandv7);
					strSQL += ", " + FCConvert.ToString(Statics.SummaryList[x].sobc1) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv1) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc2) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv2) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc3) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv3) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc4) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv4);
					strSQL += ", " + FCConvert.ToString(Statics.SummaryList[x].sobc5) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv5) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc6) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv6) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc7) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv7) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc8) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv8);
					strSQL += ", " + FCConvert.ToString(Statics.SummaryList[x].sobc9) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv9) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobc10) + ", " + FCConvert.ToString(Statics.SummaryList[x].sobv10) + ",' " + Statics.SummaryList[x].ssfla + "',' " + Statics.SummaryList[x].sovlandc + "',' " + Statics.SummaryList[x].sovlanda;
					strSQL += "',' " + Statics.SummaryList[x].sovbldgc + "',' " + Statics.SummaryList[x].sovbldga + "',' " + Statics.SummaryList[x].scmocc1 + "',' " + Statics.SummaryList[x].scmval1 + "',' " + Statics.SummaryList[x].scmocc2 + "',' " + Statics.SummaryList[x].scmval2 + "'," + FCConvert.ToString(lngAcct);
					strSQL += "," + FCConvert.ToString(Statics.SummaryList[x].slanda1) + "," + FCConvert.ToString(Statics.SummaryList[x].slanda2) + "," + FCConvert.ToString(Statics.SummaryList[x].slanda3) + "," + FCConvert.ToString(Statics.SummaryList[x].slanda4) + "," + FCConvert.ToString(Statics.SummaryList[x].slanda5) + "," + FCConvert.ToString(Statics.SummaryList[x].slanda6) + "," + FCConvert.ToString(Statics.SummaryList[x].slanda7) + "," + FCConvert.ToString(Statics.SummaryList[x].dwellcode);
					strSQL += "," + FCConvert.ToString(Statics.SummaryList[x].LandCode) + "," + FCConvert.ToString(intCardN) + "," + FCConvert.ToString(Statics.SummaryList[x].sage) + "," + FCConvert.ToString(Statics.SummaryList[x].comval1) + "," + FCConvert.ToString(Statics.SummaryList[x].comval2);
					if (lngCommitmentYear > 0)
					{
						strSQL += "," + FCConvert.ToString(lngCommitmentYear);
					}
					strSQL += ")";
					if (lngCommitmentYear <= 0)
					{
						clsTemp.Execute("insert into summrecord " + strCols + " values " + strSQL, modGlobalVariables.strREDatabase);
						clsTemp.Execute("update dwelling set dwellvalue = " + FCConvert.ToString(Conversion.Val(Statics.SummaryList[x].sdwellrcnld)) + " where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCardN), modGlobalVariables.strREDatabase);
					}
					else
					{
						clsTemp.Execute("insert into summrecord " + strCols + " values " + strSQL, modGlobalVariables.Statics.strCommitDB);
						clsTemp.Execute("update dwelling set dwellvalue = " + FCConvert.ToString(Conversion.Val(Statics.SummaryList[x].sdwellrcnld)) + " where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCardN), modGlobalVariables.Statics.strCommitDB);
					}
				}
				// x
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (!modGlobalRoutines.Formisloaded_2("frmnewvaluationreport"))
				{
					frmWait.InstancePtr.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Save Summary", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void new_reas03_4300_economic_percent()
		{
			if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "Y")
			{
				modREASValuations.Statics.EconomicPercent = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctecon") + "") * modGlobalVariables.Statics.LandTRec.BldgFactor(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone) / 100;
				modProperty.Statics.WKey = modDataTypes.Statics.DWL.Get_Fields_Int32("dieconcode");
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "ECONOMIC");
				if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) < 99999)
					modREASValuations.Statics.EconomicPercent *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit);
				modREASValuations.Statics.HoldDwellingEconomic = modREASValuations.Statics.EconomicPercent;
				modDwelling.Statics.dblEconomicPercent = modREASValuations.Statics.EconomicPercent;
			}
			else if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C" && modGNBas.Statics.gboolCommercial)
			{
				modREASValuations.Statics.EconomicPercent = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("cmecon") + "") * modGlobalVariables.Statics.LandTRec.BldgFactor(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone) / 100;
				modREASValuations.Statics.HoldDwellingEconomic = modREASValuations.Statics.EconomicPercent;
				modDwelling.Statics.dblEconomicPercent = modREASValuations.Statics.EconomicPercent;
			}
			else
			{
				if (modGlobalVariables.Statics.intCurrentCard == 1 || FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rspropertycode")) == "Y")
				{
					if (FCConvert.ToInt32(modREASValuations.Statics.HoldDwellingEconomic) != 0 && modGlobalVariables.Statics.intCurrentCard != 1)
					{
						modREASValuations.Statics.EconomicPercent = FCConvert.ToDouble(modREASValuations.Statics.HoldDwellingEconomic);
					}
					else
					{
						modREASValuations.Statics.EconomicPercent = modGlobalVariables.Statics.LandTRec.BldgFactor(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone);
						modDwelling.Statics.dblEconomicPercent = modREASValuations.Statics.EconomicPercent;
					}
				}
			}
			if (modGlobalVariables.Statics.intCurrentCard > 1 && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rspropertycode")) != "Y")
			{
				if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C" || !modGNBas.Statics.gboolCommercial))
					goto REAS03_4302_TAG;
			}
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") < 1 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") > 9)
				goto REAS03_4301_TAG;
			modProperty.Statics.WKey = FCConvert.ToInt32(1330 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1")));
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
				goto REAS03_4301_TAG;
			modREASValuations.Statics.EconomicPercent *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 100;
			REAS03_4301_TAG:
			;
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "") < 1 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "") > 9)
				goto REAS03_4302_TAG;
			modProperty.Statics.WKey = FCConvert.ToInt32(1340 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2")));
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
				goto REAS03_4302_TAG;
			modREASValuations.Statics.EconomicPercent *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 100;
			REAS03_4302_TAG:
			;
			modDwelling.Statics.dblEconomicPercent = modREASValuations.Statics.EconomicPercent;
		}

		public static void REAS03_1075_GET_PARAM_RECORD()
		{
			modGlobalConstants.Statics.MuniName = modGlobalConstants.Statics.MuniName;
			modGlobalVariables.Statics.SFLAOption = modGlobalVariables.Statics.CustomizedInfo.SquareFootLivingArea;
			modGlobalVariables.Statics.intSFLAOption = modGlobalVariables.Statics.SFLAOption;
			modGlobalVariables.Statics.AcreOption = modGlobalVariables.Statics.CustomizedInfo.AcreOption;
			modREASValuations.Statics.NET = modReplaceWorkFiles.Statics.gstrNetworkFlag;
			modGlobalVariables.Statics.DepreciationYear = modGlobalVariables.Statics.CustomizedInfo.DepreciationYear;
			modGlobalVariables.Statics.DepreciationYearMoHo = modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear;
			modGlobalVariables.Statics.ValuationReportOption = modGlobalVariables.Statics.CustomizedInfo.ValReportOption;
			if (modGlobalVariables.Statics.CustomizedInfo.Round == 0)
				modGlobalVariables.Statics.CustomizedInfo.Round = 2;
			if (modGlobalVariables.Statics.CustomizedInfo.Round == 1)
				modGlobalVariables.Statics.RoundingVar = 1000;
			if (modGlobalVariables.Statics.CustomizedInfo.Round == 2)
				modGlobalVariables.Statics.RoundingVar = 100;
			if (modGlobalVariables.Statics.CustomizedInfo.Round == 3)
				modGlobalVariables.Statics.RoundingVar = 10;
			if (modGlobalVariables.Statics.CustomizedInfo.Round == 4)
				modGlobalVariables.Statics.RoundingVar = 1;
			modGlobalVariables.Statics.HoldRounding = modGlobalVariables.Statics.RoundingVar;
			modGlobalVariables.Statics.Rounding2 = modGlobalVariables.Statics.RoundingVar;
			modREASValuations.Statics.Account = modGlobalVariables.Statics.gintLastAccountNumber;
			if (modREASValuations.Statics.Account == 0)
				modREASValuations.Statics.Account = 1;
			modREASValuations.Statics.LPP = 60;
			Statics.DefAccount = modREASValuations.Statics.Account;
			modREASValuations.Statics.HighAccount = modREASValuations.Statics.Account;
		}

		public static void New_Select_Method()
		{
			try
			{
				// On Error GoTo ErrorHandler
				modREASValuations.Statics.AcreType = "N";
				if (FCConvert.ToInt32(modProperty.Statics.WORK1) > 0 && FCConvert.ToInt32(modProperty.Statics.WORK1) != 89)
				{
					modREASValuations.Statics.HoldAcreage = modREASValuations.Statics.Acreage;
					New_compute_acreage();
					if (modREASValuations.Statics.HoldAcreage != modREASValuations.Statics.Acreage || modProperty.Statics.LFRA[FCConvert.ToInt32(modProperty.Statics.WORK1)] == 1)
						modREASValuations.Statics.AcreType = "Y";
				}
				else if (modREASValuations.Statics.FirstLandFactor == "Y")
				{
					New_compute_acreage();
					if (modProperty.Statics.WORK1 != 89)
						goto REAS03_3609_RETURN;
				}
				if (modProperty.Statics.WORK1 != 89)
					modREASValuations.Statics.StreetPrice = modGlobalVariables.Statics.LandSRec.GetRate(FCConvert.ToInt16(Conversion.Val(modProperty.Statics.WORK1)));
				if (Conversion.Val(modREASValuations.Statics.WORKA2) == 0)
				{
					if (Conversion.Val(modProperty.Statics.WORKA3) != 0)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1440 + Conversion.Val(modProperty.Statics.WORKA3));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.InfluenceFactor = FCConvert.ToSingle(FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100);
					}
					else
					{
						modREASValuations.Statics.InfluenceFactor = 0;
					}
				}
				else
				{
					modREASValuations.Statics.InfluenceFactor = FCConvert.ToSingle(Conversion.Val(modREASValuations.Statics.WORKA2) / 100);
				}
				modREASValuations.Statics.WorkA = modProperty.Statics.WORKA1;
				modREASValuations.Statics.LandUnitType = modGlobalVariables.Statics.CustomizedInfo.LandUnitType;
				if (modREASValuations.Statics.HHAcreType == "Y" && FCConvert.ToInt32(modProperty.Statics.WORK1) == 89 && modREASValuations.Statics.Row > 1)
				{
					modREASValuations.Statics.LandAcreValue -= modREASValuations.Statics.Value[modREASValuations.Statics.Row - 1];
				}
				modREASValuations.LandCalculation();
				if (modREASValuations.Statics.AcreType == "Y")
				{
					modREASValuations.Statics.LandAcreValue += modREASValuations.Statics.Value[modREASValuations.Statics.Row];
				}
				else if (modREASValuations.Statics.HHAcreType == "Y" && FCConvert.ToInt32(modProperty.Statics.WORK1) == 89 && modREASValuations.Statics.Row > 1)
				{
					modREASValuations.Statics.LandAcreValue += modREASValuations.Statics.Value[modREASValuations.Statics.Row];
					modREASValuations.Statics.AcreType = "Y";
				}
				modREASValuations.Statics.HHAcreType = modREASValuations.Statics.AcreType;
				REAS03_3609_RETURN:
				;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In New_select_method", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void New_compute_acreage()
		{
			// vbPorter upgrade warning: LandType As int	OnWriteFCConvert.ToDouble(
			int LandType;
			if (modREASValuations.Statics.FirstLandFactor != "N")
			{
				Fill_Some_Arrays_2(31);
			}
			modREASValuations.Statics.Worked1 = Strings.Mid(modProperty.Statics.WORKA1, 1, FCConvert.ToString(modProperty.Statics.WORKA1).Length - 3);
			modREASValuations.Statics.Worked2 = Strings.Right(modProperty.Statics.WORKA1, 3);
			if (modProperty.Statics.WORK1 > 0)
			{
				modREASValuations.Statics.RateFactor = modProperty.Statics.LFRA[FCConvert.ToInt32(modProperty.Statics.WORK1)] / 100;
				if (modProperty.Statics.LFRA[FCConvert.ToInt32(modProperty.Statics.WORK1)] == 1)
					modREASValuations.Statics.RateFactor = 0;
			}
			else
			{
				return;
			}
			LandType = FCConvert.ToInt32(modProperty.Statics.WORK1 + 10);
			if (Conversion.Val(Conversion.Val(modREASValuations.Statics.Worked1) + Conversion.Val(modREASValuations.Statics.Worked2)) != 0 && LandType != 0 && LandType != 99)
			{
				if (modGlobalVariables.Statics.LandTypes.FindCode(LandType))
				{
					switch (modGlobalVariables.Statics.LandTypes.UnitsType)
					{
						case modREConstants.CNSTLANDTYPEFRONTFOOT:
						case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
							{
								modREASValuations.Statics.Acreage += FCConvert.ToSingle(((Conversion.Val(modREASValuations.Statics.Worked1) * Conversion.Val(modREASValuations.Statics.Worked2)) / 43560) * modREASValuations.Statics.RateFactor);
								modGlobalVariables.Statics.Acres[modGlobalVariables.Statics.AcresIndex] = (((Conversion.Val(modREASValuations.Statics.Worked1) * Conversion.Val(modREASValuations.Statics.Worked2)) / 43560) * modREASValuations.Statics.RateFactor);
								break;
							}
						case modREConstants.CNSTLANDTYPESQUAREFEET:
							{
								modREASValuations.Statics.Acreage += FCConvert.ToSingle(((Conversion.Val(modREASValuations.Statics.Worked1) * 1000 + Conversion.Val(modREASValuations.Statics.Worked2)) / 43560) * modREASValuations.Statics.RateFactor);
								modGlobalVariables.Statics.Acres[modGlobalVariables.Statics.AcresIndex] = (((Conversion.Val(modREASValuations.Statics.Worked1) * 1000 + Conversion.Val(modREASValuations.Statics.Worked2)) / 43560) * modREASValuations.Statics.RateFactor);
								// Case 21 To 46
								break;
							}
						case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
						case modREConstants.CNSTLANDTYPEACRES:
						case modREConstants.CNSTLANDTYPESITE:
						case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
						case modREConstants.CNSTLANDTYPELINEARFEET:
						case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
							{
								modREASValuations.Statics.Acreage += FCConvert.ToSingle(((Conversion.Val(modREASValuations.Statics.Worked1) * 10) + (Conversion.Val(modREASValuations.Statics.Worked2) / 100)) * modREASValuations.Statics.RateFactor);
								modGlobalVariables.Statics.Acres[modGlobalVariables.Statics.AcresIndex] = (((Conversion.Val(modREASValuations.Statics.Worked1) * 10) + (Conversion.Val(modREASValuations.Statics.Worked2) / 100)) * modREASValuations.Statics.RateFactor);
								break;
							}
					}
					//end switch
				}
				else
				{
					return;
				}
			}
			modREASValuations.Statics.Acreage = FCConvert.ToSingle((fecherFoundation.FCUtils.iDiv((modREASValuations.Statics.Acreage * 100), 1)) / 100.0);
			REAS25_END_SUB6:
			;
		}

		public static void Fill_Some_Arrays_2(short WKey, bool boolFillem = false)
		{
			Fill_Some_Arrays(ref WKey, boolFillem);
		}

		public static void Fill_Some_Arrays_8(short WKey, bool boolFillem = false)
		{
			Fill_Some_Arrays(ref WKey, boolFillem);
		}

		public static void Fill_Some_Arrays(ref short WKey, bool boolFillem = false)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			if ((modREASValuations.Statics.FirstLandFactor == "Y") || (boolFillem))
			{
				clsLoad.OpenRecordset("select * from landtype ORDER BY code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					//FC:FINAL:AAKV:  removed an exception
					//modProperty.LFRA[(Conversion.Val(clsLoad.Get_Fields("code")) - 10] = Conversion.Val(clsLoad.Get_Fields("factor")) * 100;
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					modProperty.Statics.LFRA[FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("code")) - 10)] = Conversion.Val(clsLoad.Get_Fields_Double("factor")) * 100;
					clsLoad.MoveNext();
				}
				modREASValuations.Statics.FirstLandFactor = "N";
			}
		}

		public static void REAS04_4000_LANDS()
		{
			// Call OpenLSRTable(LSR, 31)
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from landtype ORDER BY code", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				Statics.LandsValue[FCConvert.ToInt32(clsLoad.Get_Fields("code")) - 10] = FCConvert.ToSingle(Conversion.Val(clsLoad.Get_Fields_Double("factor")));
				clsLoad.MoveNext();
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public int DefAccount;
			// Dim ONE                 As String
			public modGlobalVariables.SummaryRecType[] SummaryList = null;
			public int SummaryListIndex;
			public modGlobalVariables.CalcPropertyType[] CalcPropertyList = null;
			public modGlobalVariables.CalcDwellingType[] CalcDwellList = null;
			public modGlobalVariables.CalcCommercialType[] CalcCommList = null;
			public modGlobalVariables.CalcOutBuildingType[] CalcOutList = null;
			public int CalcPropertyIndex;
			public int CalcDwellIndex;
			public int CalcCommIndex;
			public int CalcOutIndex;
			public string CalcLog = "";
			public bool boolCalcErrors;
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper clsLoadIncomeData = new clsDRWrapper();
			public clsDRWrapper clsLoadIncomeData_AutoInitialized = null;
			public clsDRWrapper clsLoadIncomeData
			{
				get
				{
					if ( clsLoadIncomeData_AutoInitialized == null)
					{
						 clsLoadIncomeData_AutoInitialized = new clsDRWrapper();
					}
					return clsLoadIncomeData_AutoInitialized;
				}
				set
				{
					 clsLoadIncomeData_AutoInitialized = value;
				}
			}
			public float[] LandsValue = new float[199 + 1];

			public string gstrDwellCode = string.Empty;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
