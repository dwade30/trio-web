﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage3.
	/// </summary>
	public partial class srptMVRPage3 : FCSectionReport
	{

		public srptMVRPage3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage3(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }

		private void InitializeComponentEx()
		{

		}
		// nObj = 1
		//   0	srptMVRPage3	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp = "";
			txtMunicipality.Text = valuationReturn.Municipality;
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
            txtLine23.Text = valuationReturn.ForestTreeGrowth.NumberOfAcresFirstClassifiedForTaxYear
                .FormatAsCurrencyNoSymbol();            
			
			lblYear1.Text = valuationReturn.ReportYear.ToString();
			lblYear5.Text = lblYear1.Text;
			lblYear6.Text = lblYear1.Text;
			lblYear7.Text = lblYear1.Text;
			lblYear8.Text = lblYear1.Text;
			lblyear9.Text = (valuationReturn.ReportYear - 1).ToString();
            strTemp = valuationReturn.ReportYear.ToString();
			lblDateRange3.Text = "4/2/" + Strings.Right(FCConvert.ToString(Conversion.Val(strTemp) - 1), 2) + " through 4/1/" + Strings.Right(strTemp, 2);
			lblDateRange4.Text = lblDateRange3.Text;
			lblDateRange5.Text = lblDateRange3.Text;
			lblYear2.Text = lblDateRange3.Text;
			lblYear3.Text = lblYear2.Text;
			lblYear4.Text = lblYear2.Text;
			txtLine24a.Text = valuationReturn.ForestTreeGrowth.NumberOfParcelsWithdrawn.FormatAsNumber();			
			txtLine24b.Text = valuationReturn.ForestTreeGrowth.NumberOfAcresWithdrawn.FormatAsCurrencyNoSymbol();
            txtLine24c.Text =
                valuationReturn.ForestTreeGrowth.TotalAmountOfPenaltiesAssessed.FormatAsCurrencyNoSymbol();			
			txtLine24d.Text = valuationReturn.ForestTreeGrowth.TotalNumberOfNonCompliancePenalties.FormatAsNumber();
			
			txtLine25.Text = valuationReturn.FarmAndOpenSpace.NumberOfParcelsClassifiedAsFarmland.FormatAsNumber();
            txtLine26.Text = valuationReturn.FarmAndOpenSpace.NumberOfAcresClassifiedAsFarmlandForTaxYear
                .FormatAsCurrencyNoSymbol();
            txtLine27a.Text =
                valuationReturn.FarmAndOpenSpace.TotalNumberOfAcresOfAllFarmland.FormatAsCurrencyNoSymbol();
            txtLine28b.Text = valuationReturn.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres.FormatAsCurrencyNoSymbol();
            txtLine27b.Text = valuationReturn.FarmAndOpenSpace.TotalValuationOfAllFarmland.ToInteger().FormatAsNumber();
            txtLine28c.Text = valuationReturn.FarmAndOpenSpace.TotalValuationOfFarmWoodland.ToInteger()
                .FormatAsNumber();
            txtLine29a.Text = valuationReturn.FarmAndOpenSpace.TotalNumberOfFarmlandParcelsWithdrawn.FormatAsNumber();
            txtLine29b.Text = valuationReturn.FarmAndOpenSpace.TotalNumberOfFarmlandAcresWithdrawn
                .FormatAsCurrencyNoSymbol();
            txtLine29c.Text = valuationReturn.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnFarmland
                .FormatAsCurrencyNoSymbol();
            txtLine30.Text = valuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceParcelsFirstClassified.FormatAsNumber();
            txtLine31.Text = valuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceAcresFirstClassified
                .FormatAsCurrencyNoSymbol();
            txtLine32.Text = valuationReturn.FarmAndOpenSpace.TotalAcresOfOpenSpaceFirstClassified
                .FormatAsCurrencyNoSymbol();
            txtLine33.Text = valuationReturn.FarmAndOpenSpace.TotalValuationOfAllOpenSpace.ToInteger().FormatAsNumber();
            txtHardwood.Text = valuationReturn.FarmAndOpenSpace.FarmHardwoodRate.FormatAsCurrencyNoSymbol();
            txtSoftwood.Text = valuationReturn.FarmAndOpenSpace.FarmSoftwoodRate.FormatAsCurrencyNoSymbol();
            txtMixedWood.Text = valuationReturn.FarmAndOpenSpace.FarmMixedWoodRate.FormatAsCurrencyNoSymbol();
            txtSoftWoodAcres.Text =
                valuationReturn.FarmAndOpenSpace.NumberOfFarmSoftwoodAcres.FormatAsCurrencyNoSymbol();
            txtMixedWoodAcres.Text =
                valuationReturn.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres.FormatAsCurrencyNoSymbol();
            txtHardWoodAcres.Text =
                valuationReturn.FarmAndOpenSpace.NumberOfFarmHardwoodAcres.FormatAsCurrencyNoSymbol();

            txtLine241.Text = valuationReturn.ForestTreeGrowth.TreeGrowthHasBeenTransferredToFarmlandThisYear
                ? "YES"
                : "NO";			
		}

		
	}
}
