﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptDeletedAccounts.
	/// </summary>
	public partial class rptDeletedAccounts : BaseSectionReport
	{
		public static rptDeletedAccounts InstancePtr
		{
			get
			{
				return (rptDeletedAccounts)Sys.GetInstance(typeof(rptDeletedAccounts));
			}
		}

		protected rptDeletedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
                rsTemp = null;
            }
			base.Dispose(disposing);
		}

		public rptDeletedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Deleted Accounts Report";
		}
		// nObj = 1
		//   0	rptDeletedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		object varAccount;
		int intPage;
		string strWhere;
		int lngCount;

		public void Init(string strStartDate = "", string strEndDate = "")
		{
			strWhere = "";
			if (Information.IsDate(strStartDate))
			{
				if (Information.IsDate(strEndDate))
				{
					strWhere = " and hlupdate between '" + strStartDate + "' and '" + strEndDate + "' ";
				}
				else
				{
					strWhere = " and hlupdate  >= '" + strStartDate + "' ";
				}
			}
			else if (Information.IsDate(strEndDate))
			{
				strWhere = " and hlupdate  <= '" + strEndDate + "' ";
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "DeletedAccounts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			nextaccount:
			;
			if (rsTemp.EndOfFile())
				return;
			txtAccount.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSAccount"), "00000");
			if (Information.IsDate(rsTemp.Get_Fields("hlupdate")))
			{
				if (rsTemp.Get_Fields_DateTime("hlupdate") != DateTime.FromOADate(0))
				{
					txtDateDeleted.Text = Strings.Format(rsTemp.Get_Fields_DateTime("hlupdate"), "MM/dd/yyyy");
				}
				else
				{
					txtDateDeleted.Text = "";
				}
			}
			else
			{
				txtDateDeleted.Text = "";
			}
			txtName.Text = Strings.Trim(rsTemp.Get_Fields_String("RSName") + " ");
			lngCount += 1;
			if (Strings.Trim(rsTemp.Get_Fields_String("rslocnumalph") + " ").Length <= 5)
			{
				txtLocation.Text = Strings.Trim(rsTemp.Get_Fields_String("RSLOCNUMALPH") + " " + Strings.StrDup(5 - Strings.Trim(rsTemp.Get_Fields_String("RSLOCNUMALPH") + " ").Length, " ") + rsTemp.Get_Fields_String("RSLOCStreet") + " ");
			}
			else
			{
				txtLocation.Text = Strings.Trim(rsTemp.Get_Fields_String("RSLOCNUMALPH") + " " + rsTemp.Get_Fields_String("RSLOCStreet") + " ");
			}
			txtMap.Text = Strings.Trim(rsTemp.Get_Fields_String("RSMAPLOT") + " ");
			varAccount = FCConvert.ToString(rsTemp.Get_Fields_Int32("RSAccount"));
			if (!rsTemp.EndOfFile())
				rsTemp.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_Initialize()
		{
			// If rptDeletedAccounts.Printer <> "" Then
			// rptDeletedAccounts.Printer.PrintQuality = ddPQMedium
			// End If
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp = "";
			string strREFullDBName;
			string strMasterJoin;
			txtCaption.Text = "Real Estate";
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngCount = 0;
			strREFullDBName = FCConvert.ToString(rsTemp.Get_GetFullDBName("RealEstate"));
			strMasterJoin = modREMain.GetMasterJoinForJoin();
			// Call rsTemp.OpenRecordset("SELECT  rsaccount,hlupdate,rsname,rsmaplot,rslocnumalph,rslocstreet FROM Master where rscard = 1 and rsdeleted = 1 " & strWhere & " order by " & gstrFieldName & ",hlupdate", strREDatabase)
			rsTemp.OpenRecordset("SELECT  rsaccount,hlupdate,rsname,rsmaplot,rslocnumalph,rslocstreet FROM " + strMasterJoin + " where rscard = 1 and rsdeleted = 1 " + strWhere + " order by " + modPrintRoutines.Statics.gstrFieldName + ",hlupdate", modGlobalVariables.strREDatabase);
			if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
			{
				strTemp = "ACCOUNT";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSNAME")
			{
				strTemp = "NAME";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
			{
				strTemp = "LOCATION";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSMAPLOT")
			{
				strTemp = "MAPLOT";
			}
			txtCaption2.Text = "ACCOUNT LIST BY " + strTemp + " (DELETED ACCOUNTS)";
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
