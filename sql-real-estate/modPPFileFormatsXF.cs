﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWRE0000
{
	public class modPPFileFormatsXF
	{
		//=========================================================
		public struct PPConvertedData
		{
			public int Account;
			public string BusinessName;
			public string Name;
			public string Address1;
			public string Address2;
			public string City;
			public string State;
			public string Zip;
			public string Zip4;
			public int StreetNumber;
			public string Street;
			public int Value;
			public int BusinessCode;
			public string OtherAccount;
			public string Open1;
			public string Open2;
			public int Category1;
			public int Category2;
			public int Category3;
			public int Category4;
			public int Category5;
			public int Category6;
			public int Category7;
			public int Category8;
			public int Category9;
			public int ExemptAmount;
			public bool UpdatedTooMany;
			public int Category1Bete;
			public int Category2Bete;
			public int Category3Bete;
			public int Category4Bete;
			public int Category5Bete;
			public int Category6Bete;
			public int Category7Bete;
			public int Category8Bete;
			public int Category9Bete;

			public PPConvertedData(int unusedParam)
			{
				this.Account = 0;
				this.BusinessName = "";
				this.Name = "";
				this.Address1 = "";
				this.Address2 = "";
				this.City = "";
				this.State = "";
				this.Zip = "";
				this.Zip4 = "";
				this.StreetNumber = 0;
				this.Street = "";
				this.Value = 0;
				this.BusinessCode = 0;
				this.OtherAccount = "";
				this.Open1 = "";
				this.Open2 = "";
				this.Category1 = 0;
				this.Category2 = 0;
				this.Category3 = 0;
				this.Category4 = 0;
				this.Category5 = 0;
				this.Category6 = 0;
				this.Category7 = 0;
				this.Category8 = 0;
				this.Category9 = 0;
				this.ExemptAmount = 0;
				this.UpdatedTooMany = false;
				this.Category1Bete = 0;
				this.Category2Bete = 0;
				this.Category3Bete = 0;
				this.Category4Bete = 0;
				this.Category5Bete = 0;
				this.Category6Bete = 0;
				this.Category7Bete = 0;
				this.Category8Bete = 0;
				this.Category9Bete = 0;
			}
		};

		public struct PPVisionAccountInfo
		{
			// occurs once
			public FCFixedString Account;
			public FCFixedString StreetNumber;
			public FCFixedString StreetName;
			public FCFixedString BusinessName;
			public FCFixedString OwnerName;
			public FCFixedString Address1;
			public FCFixedString Address2;
			public FCFixedString City;
			public FCFixedString State;
			public FCFixedString Zip;
			public FCFixedString District;
			public FCFixedString BusinessCode;

			public PPVisionAccountInfo(int unusedParam)
			{
				this.Account = new FCFixedString(15);
				this.StreetNumber = new FCFixedString(5);
				this.StreetName = new FCFixedString(25);
				this.BusinessName = new FCFixedString(30);
				this.OwnerName = new FCFixedString(30);
				this.Address1 = new FCFixedString(30);
				this.Address2 = new FCFixedString(30);
				this.City = new FCFixedString(18);
				this.State = new FCFixedString(2);
				this.Zip = new FCFixedString(10);
				this.District = new FCFixedString(3);
				this.BusinessCode = new FCFixedString(3);
			}
		};

		public struct PPVisionPropertyItems
		{
			// occurs 20 times
			public FCFixedString PropertyCode;
			public FCFixedString Description;
			public FCFixedString AppraisedValue;
			public FCFixedString AssessedValue;

			public PPVisionPropertyItems(int unusedParam)
			{
				this.PropertyCode = new FCFixedString(5);
				this.Description = new FCFixedString(17);
				this.AppraisedValue = new FCFixedString(9);
				this.AssessedValue = new FCFixedString(9);
			}
		};
	}
}
