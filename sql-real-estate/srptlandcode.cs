﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptlandcode.
	/// </summary>
	public partial class srptlandcode : FCSectionReport
	{
		public static srptlandcode InstancePtr
		{
			get
			{
				return (srptlandcode)Sys.GetInstance(typeof(srptlandcode));
			}
		}

		protected srptlandcode _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsAssess?.Dispose();
				clsCode?.Dispose();
                clsAssess = null;
                clsCode = null;
            }
			base.Dispose(disposing);
		}

		public srptlandcode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptlandcode	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsCode = new clsDRWrapper();
		clsDRWrapper clsAssess = new clsDRWrapper();
		int lngCode;
		int lngLand;
		int lngbldg;
		int lngExempt;
		int lngTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsAssess.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string vbPorterVar = this.UserData.ToString();
			if (vbPorterVar == "C")
			{
				clsAssess.OpenRecordset("select rilandcode,count(rsaccount) as thecount,sum(rllandval) as landtot, sum(rlbldgval) as bldgtot, sum(correxemption) as exempttot from master where not rsdeleted = 1 group by rilandcode order by rilandcode", modGlobalVariables.strREDatabase);
			}
			else
			{
				clsAssess.OpenRecordset("select rilandcode,count(rsaccount) as thecount,sum(lastlandval) as landtot, sum(lastbldgval) as bldgtot, sum(rlexemption) as exempttot from master where not rsdeleted = 1 group by rilandcode order by rilandcode", modGlobalVariables.strREDatabase);
			}
			// Call clsCode.OpenRecordset("select * from costrecord where crecordnumber between 1001 and 1009", strredatabase)
			clsCode.OpenRecordset("select * from tblLandCode order by code", modGlobalVariables.strREDatabase);
			// Call clsAssess.OpenRecordset("select rilandcode,count(rsaccount) as thecount,sum(lastlandval) as landtot, sum(lastbldgval) as bldgtot, sum(rlexemption) as exempttot from master where not rsdeleted = 1 group by rilandcode order by rilandcode", strREDatabase)
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("rilandcode"))));
			txtCode.Text = lngCode.ToString();
			if (lngCode == 0)
			{
				txtCode.Text = txtCode.Text + " Uncoded";
			}
			else
			{
				// Call clsCode.FindFirstRecord("crecordnumber", 1000 + lngCode)
				// Call clsCode.FindFirstRecord("Code", lngCode)
				clsCode.FindFirst("code = " + FCConvert.ToString(lngCode));
				if (!clsCode.NoMatch)
				{
					// txtCode.Text = txtCode.Text & " " & clsCode.GetData("csdesc")
					txtCode.Text = txtCode.Text + " " + clsCode.Get_Fields_String("ShortDescription");
				}
				else
				{
					txtCode.Text = txtCode.Text + " Unknown";
				}
			}
			txtCount.Text = FCConvert.ToString(Conversion.Val(clsAssess.GetData("thecount")));
			lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("landtot"))));
			lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("bldgtot"))));
			lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("exempttot"))));
			lngTotal = lngLand - lngExempt + lngbldg;
			txtland.Text = Strings.Format(lngLand, "###,###,##0");
			txtbldg.Text = Strings.Format(lngbldg, "##,###,###,##0");
			txtexempt.Text = Strings.Format(lngExempt, "###,###,##0");
			txttotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			clsAssess.MoveNext();
		}

		
	}
}
