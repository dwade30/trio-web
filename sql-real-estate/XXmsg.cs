﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmXXmsg.
	/// </summary>
	public partial class frmXXmsg : BaseForm
	{
		public frmXXmsg()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void Command1_Click(object sender, System.EventArgs e)
		{
			// ALL
			modGNBas.Statics.Resp1 = "2";
			this.Unload();
		}

		private void Command2_Click(object sender, System.EventArgs e)
		{
			// NEXT
			modGNBas.Statics.Resp1 = "1";
			this.Unload();
		}

		private void Command3_Click(object sender, System.EventArgs e)
		{
			// QUIT
			modGNBas.Statics.Resp1 = "3";
			this.Unload();
		}

		private void frmXXmsg_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmXXmsg properties;
			//frmXXmsg.ScaleWidth	= 5880;
			//frmXXmsg.ScaleHeight	= 4230;
			//frmXXmsg.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			// frmXXmsg.Top = 2000
			// frmXXmsg.Left = 5000
			lblExisting.Text = FCConvert.ToString(modGNBas.Statics.Resp2);
			lblNew.Text = FCConvert.ToString(modGNBas.Statics.Resp3);
		}
	}
}
