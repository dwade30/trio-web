﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptRevocableLivingTrust.
	/// </summary>
	public partial class rptRevocableLivingTrust : BaseSectionReport
	{
		public static rptRevocableLivingTrust InstancePtr
		{
			get
			{
				return (rptRevocableLivingTrust)Sys.GetInstance(typeof(rptRevocableLivingTrust));
			}
		}

		protected rptRevocableLivingTrust _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptRevocableLivingTrust()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReports1";
		}
		// nObj = 1
		//   0	rptRevocableLivingTrust	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		private int lngCount;
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref bool boolRange, ref short intType, string strmin = "", string strmax = "", bool boolFromExtract = false)
		{
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = rsReport.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			switch (intType)
			{
				case 1:
					{
						// account
						strOrderBy = " order by rsaccount ";
						strWhere = " and rsaccount between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
						break;
					}
				case 2:
					{
						// name
						strOrderBy = " order by rsname,rsaccount";
						strWhere = " and rsname between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 3:
					{
						// map lot
						strOrderBy = " order by rsmaplot,rsaccount ";
						strWhere = " and rsmaplot between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 4:
					{
						// location
						strOrderBy = " order by rslocstreet,convert(int,rslocnumalph ),rsaccount";
						strWhere = " and rslocstreet between '" + strmin + "' and '" + strmax + "'";
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolFromExtract)
			{
				// strSQL = "select master.rsaccount,master.rscard,master.rsmaplot,master.rsname,rslocnumalph,rslocapt,rslocstreet from (master inner join  (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and not rsdeleted = 1 and rscard = 1 AND revocabletrust = 1" & strOrderBy
				strSQL = "select mj.rsaccount,mj.rscard,mj.rsmaplot,mj.rsname,rslocnumalph,rslocapt,rslocstreet from (" + strMasterJoinQuery + " inner join  (extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and not rsdeleted = 1 and rscard = 1 AND revocabletrust = 1" + strOrderBy;
			}
			else
			{
				if (!boolRange)
				{
					strWhere = "";
				}
				// strSQL = "select rsaccount,rsmaplot,rsname,rslocnumalph,rslocapt,rslocstreet from master  where not rsdeleted = 1 and rscard = 1 and revocabletrust = 1 " & strWhere & " " & strOrderBy
				strSQL = "select rsaccount,rsmaplot,rsname,rslocnumalph,rslocapt,rslocstreet from " + strMasterJoinQuery + "  where not rsdeleted = 1 and rscard = 1 and revocabletrust = 1 " + strWhere + " " + strOrderBy;
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Revocable Living Trusts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtPage.Text = "Page 1";
			lngCount = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				lngCount += 1;
				txtAcct.Text = FCConvert.ToString(rsReport.Get_Fields_Int32("rsaccount"));
				txtName.Text = FCConvert.ToString(rsReport.Get_Fields_String("rsname"));
				txtMaplot.Text = FCConvert.ToString(rsReport.Get_Fields_String("rsmaplot"));
				if (Conversion.Val(rsReport.Get_Fields_String("rslocnumalph")) > 0)
				{
					txtLocation.Text = Strings.Trim(rsReport.Get_Fields_String("rslocnumalph") + " " + rsReport.Get_Fields_String("rslocapt")) + " " + rsReport.Get_Fields_String("rslocstreet");
				}
				else
				{
					txtLocation.Text = FCConvert.ToString(rsReport.Get_Fields_String("rslocstreet"));
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
		}

		
	}
}
