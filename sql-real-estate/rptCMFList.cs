﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptCMFList.
	/// </summary>
	public partial class rptCMFList : BaseSectionReport
	{
		public static rptCMFList InstancePtr
		{
			get
			{
				return (rptCMFList)Sys.GetInstance(typeof(rptCMFList));
			}
		}

		protected rptCMFList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptCMFList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Certified Mail List";
		}
		// nObj = 1
		//   0	rptCMFList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		string strTitle = "";
		int lngCount;
		int lngPage;

		public void Init(string strList, bool boolViewer = false)
		{
			// accepts a comma delimited string
			string[] strGroups = null;
			string[] strTemp = null;
			string strSQL;
			int x;
			lngCount = 0;
			lngPage = 0;
			strSQL = "Select * from cmfnumbers where ";
			strGroups = Strings.Split(strList, ",", -1, CompareConstants.vbTextCompare);
			for (x = 0; x <= Information.UBound(strGroups, 1); x++)
			{
				strTemp = Strings.Split(strGroups[x], ";", -1, CompareConstants.vbTextCompare);
				strSQL += "(sessionid = " + strTemp[0];
				strSQL += " and computername = '" + strTemp[1] + "'";
				strSQL += ") or ";
			}
			// x
			strSQL = Strings.Mid(strSQL, 1, strSQL.Length - 3);
			strSQL += " order by sessionid ,computername,barcode";
			clsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			// 
			//FC:FINAL:DSE:#1841 Report should be displayed in all conditions
			//if (!boolViewer)
			//{
			//	//this.Show(App.MainForm);
			//}
			//else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
			if (!eArgs.EOF)
			{
                using (clsDRWrapper clsLoad = new clsDRWrapper())
                {
                    clsLoad.OpenRecordset(
                        "select min(barcode) as mincode,max(barcode) as maxcode from cmfnumbers where sessionid = " +
                        clsReport.Get_Fields_Int32("sessionid") + " and computername = '" +
                        clsReport.Get_Fields_String("computername") + "'", modGlobalVariables.strREDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Field [mincode] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
                        strTitle = clsLoad.Get_Fields("mincode") + " to " + clsLoad.Get_Fields("maxcode");
                    }

                    this.Fields["grpHeader"].Value = clsReport.Get_Fields_Int32("sessionid") +
                                                     clsReport.Get_Fields_String("computername");
                }
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsReport.EndOfFile())
			{
				lngCount += 1;
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsReport.Get_Fields_String("account");
				txtName.Text = clsReport.Get_Fields_String("name");
				txtCMFNumber.Text = clsReport.Get_Fields_String("barcode");
				txtIMPBNumber.Text = clsReport.Get_Fields_String("IMPBTrackingNumber");
				clsReport.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,###,###");
			lngCount = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			lngPage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lblTitle2.Text = strTitle;
		}

	

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
