//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTransferAccount.
	/// </summary>
	partial class frmTransferAccount : BaseForm
	{
		public fecherFoundation.FCComboBox cmbTransferType;
		public fecherFoundation.FCLabel lblTransferType;
		public fecherFoundation.FCLabel lblTransfer;
		public fecherFoundation.FCComboBox cmbTransfer;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtPage;
		public fecherFoundation.FCTextBox txtBook;
		public fecherFoundation.FCTextBox txtRef2;
		public fecherFoundation.FCTextBox txtRef1;
		public fecherFoundation.FCTextBox txtStreet;
		public fecherFoundation.FCTextBox txtApt;
		public fecherFoundation.FCTextBox txtStreetNumber;
		public fecherFoundation.FCTextBox txtMapLot;
		public Global.T2KDateBox t2kBPDate;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8_0;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdRemoveOwner;
		public fecherFoundation.FCButton cmdRemoveSecOwner;
		public fecherFoundation.FCButton cmdSearchOwner;
		public fecherFoundation.FCTextBox txtOwnerID;
		public fecherFoundation.FCButton cmdSearchSecOwner;
		public fecherFoundation.FCTextBox txt2ndOwnerID;
		public fecherFoundation.FCLabel lbl2ndOwner;
		public fecherFoundation.FCLabel Label2_9;
		public fecherFoundation.FCLabel Label43;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblOwner1;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid Grid;
		public Global.T2KDateBox T2KTransferDate;
		public FCGrid SaleGrid;
		public fecherFoundation.FCLabel Label22;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferAccount));
            this.cmbTransferType = new fecherFoundation.FCComboBox();
            this.lblTransferType = new fecherFoundation.FCLabel();
            this.cmbTransfer = new fecherFoundation.FCComboBox();
            this.lblTransfer = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtAccount = new fecherFoundation.FCTextBox();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtPage = new fecherFoundation.FCTextBox();
            this.txtBook = new fecherFoundation.FCTextBox();
            this.txtRef2 = new fecherFoundation.FCTextBox();
            this.txtRef1 = new fecherFoundation.FCTextBox();
            this.txtStreet = new fecherFoundation.FCTextBox();
            this.txtApt = new fecherFoundation.FCTextBox();
            this.txtStreetNumber = new fecherFoundation.FCTextBox();
            this.txtMapLot = new fecherFoundation.FCTextBox();
            this.t2kBPDate = new Global.T2KDateBox();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8_0 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtDeedName2 = new fecherFoundation.FCTextBox();
            this.txtDeedName1 = new fecherFoundation.FCTextBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.cmdRemoveOwner = new fecherFoundation.FCButton();
            this.cmdRemoveSecOwner = new fecherFoundation.FCButton();
            this.cmdSearchOwner = new fecherFoundation.FCButton();
            this.txtOwnerID = new fecherFoundation.FCTextBox();
            this.cmdSearchSecOwner = new fecherFoundation.FCButton();
            this.txt2ndOwnerID = new fecherFoundation.FCTextBox();
            this.lbl2ndOwner = new fecherFoundation.FCLabel();
            this.Label2_9 = new fecherFoundation.FCLabel();
            this.Label43 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblOwner1 = new fecherFoundation.FCLabel();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.T2KTransferDate = new Global.T2KDateBox();
            this.SaleGrid = new fecherFoundation.FCGrid();
            this.Label22 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kBPDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdAdd);
            this.BottomPanel.Location = new System.Drawing.Point(0, 893);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.T2KTransferDate);
            this.ClientArea.Controls.Add(this.SaleGrid);
            this.ClientArea.Controls.Add(this.Label22);
            this.ClientArea.Size = new System.Drawing.Size(1078, 741);
            this.ClientArea.Controls.SetChildIndex(this.Label22, 0);
            this.ClientArea.Controls.SetChildIndex(this.SaleGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.T2KTransferDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame4, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(188, 28);
            this.HeaderText.Text = "Pending Transfer";
            // 
            // cmbTransferType
            // 
            this.cmbTransferType.Items.AddRange(new object[] {
            "Same Account",
            "New Account",
            "Existing Account"});
            this.cmbTransferType.Location = new System.Drawing.Point(146, 30);
            this.cmbTransferType.Name = "cmbTransferType";
            this.cmbTransferType.Size = new System.Drawing.Size(190, 40);
            this.cmbTransferType.TabIndex = 1;
            this.cmbTransferType.Text = "Same Account";
            this.cmbTransferType.SelectedIndexChanged += new System.EventHandler(this.optTransferType_CheckedChanged);
            // 
            // lblTransferType
            // 
            this.lblTransferType.AutoSize = true;
            this.lblTransferType.Location = new System.Drawing.Point(20, 44);
            this.lblTransferType.Name = "lblTransferType";
            this.lblTransferType.Size = new System.Drawing.Size(112, 15);
            this.lblTransferType.TabIndex = 4;
            this.lblTransferType.Text = "TRANSFER TYPE";
            // 
            // cmbTransfer
            // 
            this.cmbTransfer.Items.AddRange(new object[] {
            "Entire Account",
            "Specific Cards"});
            this.cmbTransfer.Location = new System.Drawing.Point(114, 30);
            this.cmbTransfer.Name = "cmbTransfer";
            this.cmbTransfer.Size = new System.Drawing.Size(222, 40);
            this.cmbTransfer.TabIndex = 1;
            this.cmbTransfer.Text = "Entire Account";
            this.cmbTransfer.SelectedIndexChanged += new System.EventHandler(this.optTransfer_CheckedChanged);
            // 
            // lblTransfer
            // 
            this.lblTransfer.AutoSize = true;
            this.lblTransfer.Location = new System.Drawing.Point(20, 44);
            this.lblTransfer.Name = "lblTransfer";
            this.lblTransfer.Size = new System.Drawing.Size(74, 15);
            this.lblTransfer.TabIndex = 3;
            this.lblTransfer.Text = "TRANSFER";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtAccount);
            this.Frame5.Controls.Add(this.cmbTransferType);
            this.Frame5.Controls.Add(this.lblTransferType);
            this.Frame5.Controls.Add(this.Label14);
            this.Frame5.Location = new System.Drawing.Point(30, 307);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(356, 150);
            this.Frame5.TabIndex = 1;
            this.Frame5.Text = "Transfer To";
            // 
            // txtAccount
            // 
            this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccount.Location = new System.Drawing.Point(146, 90);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(190, 40);
            this.txtAccount.TabIndex = 3;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 104);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(81, 18);
            this.Label14.TabIndex = 2;
            this.Label14.Text = "ACCOUNT";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtPage);
            this.Frame4.Controls.Add(this.txtBook);
            this.Frame4.Controls.Add(this.txtRef2);
            this.Frame4.Controls.Add(this.txtRef1);
            this.Frame4.Controls.Add(this.txtStreet);
            this.Frame4.Controls.Add(this.txtApt);
            this.Frame4.Controls.Add(this.txtStreetNumber);
            this.Frame4.Controls.Add(this.txtMapLot);
            this.Frame4.Controls.Add(this.t2kBPDate);
            this.Frame4.Controls.Add(this.Label23);
            this.Frame4.Controls.Add(this.Label13);
            this.Frame4.Controls.Add(this.Label12);
            this.Frame4.Controls.Add(this.Label11);
            this.Frame4.Controls.Add(this.Label10);
            this.Frame4.Controls.Add(this.Label9);
            this.Frame4.Controls.Add(this.Label8_0);
            this.Frame4.Location = new System.Drawing.Point(423, 464);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(585, 290);
            this.Frame4.TabIndex = 6;
            this.Frame4.Text = "New Property Information";
            // 
            // txtPage
            // 
            this.txtPage.BackColor = System.Drawing.SystemColors.Window;
            this.txtPage.Location = new System.Drawing.Point(298, 230);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(64, 40);
            this.txtPage.TabIndex = 12;
            // 
            // txtBook
            // 
            this.txtBook.BackColor = System.Drawing.SystemColors.Window;
            this.txtBook.Location = new System.Drawing.Point(136, 230);
            this.txtBook.Name = "txtBook";
            this.txtBook.Size = new System.Drawing.Size(66, 40);
            this.txtBook.TabIndex = 10;
            // 
            // txtRef2
            // 
            this.txtRef2.BackColor = System.Drawing.SystemColors.Window;
            this.txtRef2.Location = new System.Drawing.Point(135, 180);
            this.txtRef2.Name = "txtRef2";
            this.txtRef2.Size = new System.Drawing.Size(351, 40);
            this.txtRef2.TabIndex = 8;
            // 
            // txtRef1
            // 
            this.txtRef1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRef1.Location = new System.Drawing.Point(136, 130);
            this.txtRef1.Name = "txtRef1";
            this.txtRef1.Size = new System.Drawing.Size(350, 40);
            this.txtRef1.TabIndex = 6;
            // 
            // txtStreet
            // 
            this.txtStreet.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreet.Location = new System.Drawing.Point(290, 80);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(196, 40);
            this.txtStreet.TabIndex = 4;
            // 
            // txtApt
            // 
            this.txtApt.BackColor = System.Drawing.SystemColors.Window;
            this.txtApt.Location = new System.Drawing.Point(230, 80);
            this.txtApt.MaxLength = 1;
            this.txtApt.Name = "txtApt";
            this.txtApt.Size = new System.Drawing.Size(50, 40);
            this.txtApt.TabIndex = 3;
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNumber.Location = new System.Drawing.Point(136, 80);
            this.txtStreetNumber.Name = "txtStreetNumber";
            this.txtStreetNumber.Size = new System.Drawing.Size(82, 40);
            this.txtStreetNumber.TabIndex = 2;
            // 
            // txtMapLot
            // 
            this.txtMapLot.BackColor = System.Drawing.SystemColors.Window;
            this.txtMapLot.Location = new System.Drawing.Point(135, 30);
            this.txtMapLot.MaxLength = 17;
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Size = new System.Drawing.Size(351, 40);
            this.txtMapLot.TabIndex = 1;
            // 
            // t2kBPDate
            // 
            this.t2kBPDate.AutoSize = false;
            this.t2kBPDate.Location = new System.Drawing.Point(455, 230);
            this.t2kBPDate.Mask = "##/##/####";
            this.t2kBPDate.MaxLength = 10;
            this.t2kBPDate.Name = "t2kBPDate";
            this.t2kBPDate.Size = new System.Drawing.Size(115, 40);
            this.t2kBPDate.TabIndex = 14;
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(393, 244);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(46, 18);
            this.Label23.TabIndex = 13;
            this.Label23.Text = "DATE";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(234, 244);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(46, 18);
            this.Label13.TabIndex = 11;
            this.Label13.Text = "PAGE";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 244);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(62, 18);
            this.Label12.TabIndex = 9;
            this.Label12.Text = "BOOK";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 194);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(98, 18);
            this.Label11.TabIndex = 7;
            this.Label11.Text = "REFERENCE 2";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 144);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(110, 18);
            this.Label10.TabIndex = 5;
            this.Label10.Text = "REFERENCE 1";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 94);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(62, 18);
            this.Label9.TabIndex = 1;
            this.Label9.Text = "LOCATION";
            // 
            // Label8_0
            // 
            this.Label8_0.Location = new System.Drawing.Point(20, 44);
            this.Label8_0.Name = "Label8_0";
            this.Label8_0.Size = new System.Drawing.Size(62, 18);
            this.Label8_0.TabIndex = 15;
            this.Label8_0.Text = "MAP/LOT";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtDeedName2);
            this.Frame3.Controls.Add(this.txtDeedName1);
            this.Frame3.Controls.Add(this.fcLabel1);
            this.Frame3.Controls.Add(this.fcLabel2);
            this.Frame3.Controls.Add(this.cmdRemoveOwner);
            this.Frame3.Controls.Add(this.cmdRemoveSecOwner);
            this.Frame3.Controls.Add(this.cmdSearchOwner);
            this.Frame3.Controls.Add(this.txtOwnerID);
            this.Frame3.Controls.Add(this.cmdSearchSecOwner);
            this.Frame3.Controls.Add(this.txt2ndOwnerID);
            this.Frame3.Controls.Add(this.lbl2ndOwner);
            this.Frame3.Controls.Add(this.Label2_9);
            this.Frame3.Controls.Add(this.Label43);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.lblOwner1);
            this.Frame3.Controls.Add(this.lblAddress);
            this.Frame3.Location = new System.Drawing.Point(423, 69);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(585, 372);
            this.Frame3.TabIndex = 5;
            this.Frame3.Text = " New Owner Information";
            // 
            // txtDeedName2
            // 
            this.txtDeedName2.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeedName2.Location = new System.Drawing.Point(135, 243);
            this.txtDeedName2.Name = "txtDeedName2";
            this.txtDeedName2.Size = new System.Drawing.Size(300, 40);
            this.txtDeedName2.TabIndex = 9;
            // 
            // txtDeedName1
            // 
            this.txtDeedName1.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeedName1.Location = new System.Drawing.Point(135, 197);
            this.txtDeedName1.Name = "txtDeedName1";
            this.txtDeedName1.Size = new System.Drawing.Size(300, 40);
            this.txtDeedName1.TabIndex = 8;
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(20, 259);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(102, 18);
            this.fcLabel1.TabIndex = 16;
            this.fcLabel1.Text = "DEED NAME 2";
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(20, 209);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(103, 18);
            this.fcLabel2.TabIndex = 14;
            this.fcLabel2.Text = "DEED NAME 1";
            // 
            // cmdRemoveOwner
            // 
            this.cmdRemoveOwner.AppearanceKey = "actionButton";
            this.cmdRemoveOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveOwner.Location = new System.Drawing.Point(298, 30);
            this.cmdRemoveOwner.Name = "cmdRemoveOwner";
            this.cmdRemoveOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveOwner.TabIndex = 4;
            this.cmdRemoveOwner.Click += new System.EventHandler(this.cmdRemoveOwner_Click);
            // 
            // cmdRemoveSecOwner
            // 
            this.cmdRemoveSecOwner.AppearanceKey = "actionButton";
            this.cmdRemoveSecOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveSecOwner.Location = new System.Drawing.Point(298, 112);
            this.cmdRemoveSecOwner.Name = "cmdRemoveSecOwner";
            this.cmdRemoveSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveSecOwner.TabIndex = 10;
            this.cmdRemoveSecOwner.Click += new System.EventHandler(this.cmdRemoveSecOwner_Click);
            // 
            // cmdSearchOwner
            // 
            this.cmdSearchOwner.AppearanceKey = "actionButton";
            this.cmdSearchOwner.ImageSource = "button-search";
            this.cmdSearchOwner.Location = new System.Drawing.Point(248, 30);
            this.cmdSearchOwner.Name = "cmdSearchOwner";
            this.cmdSearchOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchOwner.TabIndex = 2;
            this.cmdSearchOwner.Click += new System.EventHandler(this.cmdSearchOwner_Click);
            // 
            // txtOwnerID
            // 
            this.txtOwnerID.Location = new System.Drawing.Point(136, 30);
            this.txtOwnerID.Name = "txtOwnerID";
            this.txtOwnerID.Size = new System.Drawing.Size(109, 40);
            this.txtOwnerID.TabIndex = 1;
            this.txtOwnerID.Tag = "address";
            this.txtOwnerID.Text = "0";
            // 
            // cmdSearchSecOwner
            // 
            this.cmdSearchSecOwner.AppearanceKey = "actionButton";
            this.cmdSearchSecOwner.ImageSource = "button-search";
            this.cmdSearchSecOwner.Location = new System.Drawing.Point(248, 112);
            this.cmdSearchSecOwner.Name = "cmdSearchSecOwner";
            this.cmdSearchSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchSecOwner.TabIndex = 8;
            this.cmdSearchSecOwner.Click += new System.EventHandler(this.cmdSearchSecOwner_Click);
            // 
            // txt2ndOwnerID
            // 
            this.txt2ndOwnerID.Location = new System.Drawing.Point(135, 112);
            this.txt2ndOwnerID.Name = "txt2ndOwnerID";
            this.txt2ndOwnerID.Size = new System.Drawing.Size(109, 40);
            this.txt2ndOwnerID.TabIndex = 7;
            this.txt2ndOwnerID.Tag = "address";
            this.txt2ndOwnerID.Text = "0";
            // 
            // lbl2ndOwner
            // 
            this.lbl2ndOwner.Location = new System.Drawing.Point(20, 163);
            this.lbl2ndOwner.Name = "lbl2ndOwner";
            this.lbl2ndOwner.Size = new System.Drawing.Size(368, 17);
            this.lbl2ndOwner.TabIndex = 11;
            // 
            // Label2_9
            // 
            this.Label2_9.Location = new System.Drawing.Point(20, 302);
            this.Label2_9.Name = "Label2_9";
            this.Label2_9.Size = new System.Drawing.Size(80, 17);
            this.Label2_9.TabIndex = 12;
            this.Label2_9.Text = "ADDRESS";
            // 
            // Label43
            // 
            this.Label43.Location = new System.Drawing.Point(20, 126);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(80, 17);
            this.Label43.TabIndex = 6;
            this.Label43.Text = "2ND OWNER";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(80, 17);
            this.Label1.TabIndex = 13;
            this.Label1.Text = "OWNER";
            // 
            // lblOwner1
            // 
            this.lblOwner1.Location = new System.Drawing.Point(20, 82);
            this.lblOwner1.Name = "lblOwner1";
            this.lblOwner1.Size = new System.Drawing.Size(368, 17);
            this.lblOwner1.TabIndex = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(136, 302);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(300, 63);
            this.lblAddress.TabIndex = 13;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.Grid);
            this.Frame1.Controls.Add(this.cmbTransfer);
            this.Frame1.Controls.Add(this.lblTransfer);
            this.Frame1.Location = new System.Drawing.Point(30, 69);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(356, 218);
            this.Frame1.TabIndex = 7;
            this.Frame1.Text = "Transfer";
            // 
            // Grid
            // 
            this.Grid.Cols = 1;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(20, 90);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.RowHeadersVisible = false;
            this.Grid.Size = new System.Drawing.Size(316, 109);
            this.Grid.TabIndex = 2;
            this.Grid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEdit);
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // T2KTransferDate
            // 
            this.T2KTransferDate.AutoSize = false;
            this.T2KTransferDate.Location = new System.Drawing.Point(221, 14);
            this.T2KTransferDate.Mask = "##/##/####";
            this.T2KTransferDate.MaxLength = 10;
            this.T2KTransferDate.Name = "T2KTransferDate";
            this.T2KTransferDate.Size = new System.Drawing.Size(115, 40);
            this.T2KTransferDate.TabIndex = 1;
            // 
            // SaleGrid
            // 
            this.SaleGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.SaleGrid.Cols = 6;
            this.SaleGrid.ColumnHeadersHeight = 60;
            this.SaleGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.SaleGrid.ExtendLastCol = true;
            this.SaleGrid.FixedCols = 0;
            this.SaleGrid.FixedRows = 2;
            this.SaleGrid.Location = new System.Drawing.Point(30, 785);
            this.SaleGrid.Name = "SaleGrid";
            this.SaleGrid.ReadOnly = false;
            this.SaleGrid.RowHeadersVisible = false;
            this.SaleGrid.Rows = 3;
            this.SaleGrid.Size = new System.Drawing.Size(1006, 108);
            this.SaleGrid.StandardTab = false;
            this.SaleGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.SaleGrid.TabIndex = 7;
            this.SaleGrid.Tag = "sale";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(30, 31);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(195, 18);
            this.Label22.TabIndex = 3;
            this.Label22.Text = "MINIMUM TRANSFER DATE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdd,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 0;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuAdd.Text = "Save & Exit";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "acceptButton";
            this.cmdAdd.Location = new System.Drawing.Point(474, 25);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdAdd.Size = new System.Drawing.Size(83, 48);
            this.cmdAdd.TabIndex = 0;
            this.cmdAdd.Text = "Save";
            this.cmdAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // frmTransferAccount
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 801);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmTransferAccount";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Pending Transfer";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmTransferAccount_Load);
            this.Resize += new System.EventHandler(this.frmTransferAccount_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTransferAccount_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.t2kBPDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdAdd;
        public FCTextBox txtDeedName2;
        public FCTextBox txtDeedName1;
        public FCLabel fcLabel1;
        public FCLabel fcLabel2;
    }
}