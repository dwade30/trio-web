﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptREPPAudit.
	/// </summary>
	partial class rptREPPAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptREPPAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTELand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTERECount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEREExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEPPCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEPPExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBRECount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBREExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBPPCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBPPExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotPPBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotREBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTETotCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTEHomeCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBHomeCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHalfHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBRENoValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBPPNoValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTELand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTERECount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEREExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEPPCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEPPExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBRECount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBREExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBPPCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBPPExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPPBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotREBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTETotCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEHomeCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBHomeCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBRENoValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBPPNoValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Field11,
            this.Field12,
            this.Field13,
            this.Field14,
            this.Field15,
            this.Field16,
            this.Field17,
            this.Field18,
            this.Field19,
            this.Field20,
            this.Field21,
            this.Field22,
            this.Field23,
            this.Field24,
            this.Field25,
            this.Field26,
            this.Field27,
            this.Field28,
            this.Field29,
            this.txtTEBuilding,
            this.txtTELand,
            this.txtTERECount,
            this.txtTEREExemption,
            this.txtTEHomestead,
            this.txtTEValue,
            this.txtTEPPCount,
            this.txtTEPPExemption,
            this.txtBBuilding,
            this.txtBLand,
            this.txtBRECount,
            this.txtBREExemption,
            this.txtBValuation,
            this.txtBPPCount,
            this.txtBPPExemption,
            this.txtBHomestead,
            this.txtTotBillable,
            this.txtTotPPBillable,
            this.txtTotREBillable,
            this.txtTotCount,
            this.txtTotHomestead,
            this.txtTotTot,
            this.Field30,
            this.txtTETotCount,
            this.txtTEHomeCount,
            this.txtBHomeCount,
            this.Field31,
            this.Field32,
            this.Field33,
            this.Field34,
            this.Field35,
            this.txtHalfHomestead,
            this.Field36,
            this.txtBRENoValuation,
            this.Field37,
            this.txtBPPNoValuation});
			this.Detail.Height = 5.53125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Field1
			// 
			this.Field1.Height = 0.19F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'";
			this.Field1.Text = "Totally Exempt:";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 1.5F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.19F;
			this.Field2.Left = 1.625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'";
			this.Field2.Text = "Real Estate";
			this.Field2.Top = 0.21875F;
			this.Field2.Width = 1.125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 2.125F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'";
			this.Field3.Text = "Count";
			this.Field3.Top = 0.375F;
			this.Field3.Width = 0.6875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 2.125F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'";
			this.Field4.Text = "Land";
			this.Field4.Top = 0.53125F;
			this.Field4.Width = 0.6875F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 2.125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'";
			this.Field5.Text = "Building";
			this.Field5.Top = 0.6875F;
			this.Field5.Width = 0.9375F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 1.625F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'";
			this.Field6.Text = "Personal Property";
			this.Field6.Top = 1F;
			this.Field6.Width = 1.5F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.19F;
			this.Field7.Left = 2.125F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'";
			this.Field7.Text = "Count";
			this.Field7.Top = 1.15625F;
			this.Field7.Width = 0.6875F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 2.125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'";
			this.Field8.Text = "Exemption";
			this.Field8.Top = 0.84375F;
			this.Field8.Width = 1.0625F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.19F;
			this.Field9.Left = 2.125F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'";
			this.Field9.Text = "Valuation";
			this.Field9.Top = 1.3125F;
			this.Field9.Width = 1F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 2.125F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'";
			this.Field10.Text = "Exemption";
			this.Field10.Top = 1.46875F;
			this.Field10.Width = 0.875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.19F;
			this.Field11.Left = 4.5625F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field11.Text = "Homestead";
			this.Field11.Top = 0.84375F;
			this.Field11.Width = 1.0625F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.19F;
			this.Field12.Left = 0F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'";
			this.Field12.Text = "Billable:";
			this.Field12.Top = 1.9375F;
			this.Field12.Width = 1.5F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.19F;
			this.Field13.Left = 1.625F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'";
			this.Field13.Text = "Real Estate";
			this.Field13.Top = 2.15625F;
			this.Field13.Width = 1.125F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.19F;
			this.Field14.Left = 2.125F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'";
			this.Field14.Text = "Count";
			this.Field14.Top = 2.46875F;
			this.Field14.Width = 0.6875F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.19F;
			this.Field15.Left = 2.125F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'";
			this.Field15.Text = "Land";
			this.Field15.Top = 2.625F;
			this.Field15.Width = 0.6875F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.19F;
			this.Field16.Left = 2.125F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'";
			this.Field16.Text = "Building";
			this.Field16.Top = 2.78125F;
			this.Field16.Width = 0.9375F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.19F;
			this.Field17.Left = 1.625F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'";
			this.Field17.Text = "Personal Property";
			this.Field17.Top = 3.09375F;
			this.Field17.Width = 1.5F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.19F;
			this.Field18.Left = 2.125F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'";
			this.Field18.Text = "Count";
			this.Field18.Top = 3.395833F;
			this.Field18.Width = 0.6875F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.19F;
			this.Field19.Left = 2.125F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'";
			this.Field19.Text = "Exemption";
			this.Field19.Top = 2.9375F;
			this.Field19.Width = 1.0625F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.19F;
			this.Field20.Left = 2.125F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'";
			this.Field20.Text = "Valuation";
			this.Field20.Top = 3.552083F;
			this.Field20.Width = 1F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.19F;
			this.Field21.Left = 2.125F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'";
			this.Field21.Text = "Exemption";
			this.Field21.Top = 3.708333F;
			this.Field21.Width = 0.875F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.19F;
			this.Field22.Left = 4.5625F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field22.Text = "Homestead";
			this.Field22.Top = 2.9375F;
			this.Field22.Width = 1.0625F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.19F;
			this.Field23.Left = 1.625F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'";
			this.Field23.Text = "Total Billable";
			this.Field23.Top = 3.864583F;
			this.Field23.Width = 1.5F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.19F;
			this.Field24.Left = 0F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'";
			this.Field24.Text = "Tax Rate Valuation:";
			this.Field24.Top = 4.208333F;
			this.Field24.Width = 1.8125F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.19F;
			this.Field25.Left = 1.625F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'";
			this.Field25.Text = "Count";
			this.Field25.Top = 4.364583F;
			this.Field25.Width = 0.6875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.19F;
			this.Field26.Left = 1.625F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'";
			this.Field26.Text = "Real Estate Billable";
			this.Field26.Top = 4.520833F;
			this.Field26.Width = 2F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.19F;
			this.Field27.Left = 1.625F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Tahoma\'";
			this.Field27.Text = "Personal Property Billable";
			this.Field27.Top = 4.677083F;
			this.Field27.Width = 2.375F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.19F;
			this.Field28.Left = 1.625F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'";
			this.Field28.Text = "Homestead";
			this.Field28.Top = 4.833333F;
			this.Field28.Width = 1.125F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.19F;
			this.Field29.Left = 1.625F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'";
			this.Field29.Text = "Total Valuation";
			this.Field29.Top = 5.15625F;
			this.Field29.Width = 1.4375F;
			// 
			// txtTEBuilding
			// 
			this.txtTEBuilding.Height = 0.19F;
			this.txtTEBuilding.Left = 3.25F;
			this.txtTEBuilding.Name = "txtTEBuilding";
			this.txtTEBuilding.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEBuilding.Text = "0";
			this.txtTEBuilding.Top = 0.6875F;
			this.txtTEBuilding.Width = 1.1875F;
			// 
			// txtTELand
			// 
			this.txtTELand.Height = 0.19F;
			this.txtTELand.Left = 3.25F;
			this.txtTELand.Name = "txtTELand";
			this.txtTELand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTELand.Text = "0";
			this.txtTELand.Top = 0.53125F;
			this.txtTELand.Width = 1.1875F;
			// 
			// txtTERECount
			// 
			this.txtTERECount.Height = 0.19F;
			this.txtTERECount.Left = 3.25F;
			this.txtTERECount.Name = "txtTERECount";
			this.txtTERECount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTERECount.Text = "0";
			this.txtTERECount.Top = 0.375F;
			this.txtTERECount.Width = 1.1875F;
			// 
			// txtTEREExemption
			// 
			this.txtTEREExemption.Height = 0.19F;
			this.txtTEREExemption.Left = 3.25F;
			this.txtTEREExemption.Name = "txtTEREExemption";
			this.txtTEREExemption.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEREExemption.Text = "0";
			this.txtTEREExemption.Top = 0.84375F;
			this.txtTEREExemption.Width = 1.1875F;
			// 
			// txtTEHomestead
			// 
			this.txtTEHomestead.Height = 0.19F;
			this.txtTEHomestead.Left = 5.625F;
			this.txtTEHomestead.Name = "txtTEHomestead";
			this.txtTEHomestead.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEHomestead.Text = "0";
			this.txtTEHomestead.Top = 0.84375F;
			this.txtTEHomestead.Width = 1.0625F;
			// 
			// txtTEValue
			// 
			this.txtTEValue.Height = 0.19F;
			this.txtTEValue.Left = 3.25F;
			this.txtTEValue.Name = "txtTEValue";
			this.txtTEValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEValue.Text = "0";
			this.txtTEValue.Top = 1.3125F;
			this.txtTEValue.Width = 1.1875F;
			// 
			// txtTEPPCount
			// 
			this.txtTEPPCount.Height = 0.19F;
			this.txtTEPPCount.Left = 3.25F;
			this.txtTEPPCount.Name = "txtTEPPCount";
			this.txtTEPPCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEPPCount.Text = "0";
			this.txtTEPPCount.Top = 1.15625F;
			this.txtTEPPCount.Width = 1.1875F;
			// 
			// txtTEPPExemption
			// 
			this.txtTEPPExemption.Height = 0.19F;
			this.txtTEPPExemption.Left = 3.25F;
			this.txtTEPPExemption.Name = "txtTEPPExemption";
			this.txtTEPPExemption.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEPPExemption.Text = "0";
			this.txtTEPPExemption.Top = 1.46875F;
			this.txtTEPPExemption.Width = 1.1875F;
			// 
			// txtBBuilding
			// 
			this.txtBBuilding.Height = 0.19F;
			this.txtBBuilding.Left = 3.25F;
			this.txtBBuilding.Name = "txtBBuilding";
			this.txtBBuilding.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBBuilding.Text = "0";
			this.txtBBuilding.Top = 2.78125F;
			this.txtBBuilding.Width = 1.1875F;
			// 
			// txtBLand
			// 
			this.txtBLand.Height = 0.19F;
			this.txtBLand.Left = 3.25F;
			this.txtBLand.Name = "txtBLand";
			this.txtBLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBLand.Text = "0";
			this.txtBLand.Top = 2.625F;
			this.txtBLand.Width = 1.1875F;
			// 
			// txtBRECount
			// 
			this.txtBRECount.Height = 0.19F;
			this.txtBRECount.Left = 3.25F;
			this.txtBRECount.Name = "txtBRECount";
			this.txtBRECount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBRECount.Text = "0";
			this.txtBRECount.Top = 2.46875F;
			this.txtBRECount.Width = 1.1875F;
			// 
			// txtBREExemption
			// 
			this.txtBREExemption.Height = 0.19F;
			this.txtBREExemption.Left = 3.25F;
			this.txtBREExemption.Name = "txtBREExemption";
			this.txtBREExemption.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBREExemption.Text = "0";
			this.txtBREExemption.Top = 2.9375F;
			this.txtBREExemption.Width = 1.1875F;
			// 
			// txtBValuation
			// 
			this.txtBValuation.Height = 0.19F;
			this.txtBValuation.Left = 3.25F;
			this.txtBValuation.Name = "txtBValuation";
			this.txtBValuation.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBValuation.Text = "0";
			this.txtBValuation.Top = 3.552083F;
			this.txtBValuation.Width = 1.1875F;
			// 
			// txtBPPCount
			// 
			this.txtBPPCount.Height = 0.19F;
			this.txtBPPCount.Left = 3.25F;
			this.txtBPPCount.Name = "txtBPPCount";
			this.txtBPPCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBPPCount.Text = "0";
			this.txtBPPCount.Top = 3.395833F;
			this.txtBPPCount.Width = 1.1875F;
			// 
			// txtBPPExemption
			// 
			this.txtBPPExemption.Height = 0.19F;
			this.txtBPPExemption.Left = 3.25F;
			this.txtBPPExemption.Name = "txtBPPExemption";
			this.txtBPPExemption.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBPPExemption.Text = "0";
			this.txtBPPExemption.Top = 3.708333F;
			this.txtBPPExemption.Width = 1.1875F;
			// 
			// txtBHomestead
			// 
			this.txtBHomestead.Height = 0.19F;
			this.txtBHomestead.Left = 5.625F;
			this.txtBHomestead.Name = "txtBHomestead";
			this.txtBHomestead.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBHomestead.Text = "0";
			this.txtBHomestead.Top = 2.9375F;
			this.txtBHomestead.Width = 1.0625F;
			// 
			// txtTotBillable
			// 
			this.txtTotBillable.Height = 0.19F;
			this.txtTotBillable.Left = 3.25F;
			this.txtTotBillable.Name = "txtTotBillable";
			this.txtTotBillable.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotBillable.Text = "0";
			this.txtTotBillable.Top = 3.864583F;
			this.txtTotBillable.Width = 1.1875F;
			// 
			// txtTotPPBillable
			// 
			this.txtTotPPBillable.Height = 0.19F;
			this.txtTotPPBillable.Left = 4.4375F;
			this.txtTotPPBillable.Name = "txtTotPPBillable";
			this.txtTotPPBillable.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotPPBillable.Text = "0";
			this.txtTotPPBillable.Top = 4.677083F;
			this.txtTotPPBillable.Width = 1.1875F;
			// 
			// txtTotREBillable
			// 
			this.txtTotREBillable.Height = 0.19F;
			this.txtTotREBillable.Left = 4.4375F;
			this.txtTotREBillable.Name = "txtTotREBillable";
			this.txtTotREBillable.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotREBillable.Text = "0";
			this.txtTotREBillable.Top = 4.520833F;
			this.txtTotREBillable.Width = 1.1875F;
			// 
			// txtTotCount
			// 
			this.txtTotCount.Height = 0.19F;
			this.txtTotCount.Left = 4.4375F;
			this.txtTotCount.Name = "txtTotCount";
			this.txtTotCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotCount.Text = "0";
			this.txtTotCount.Top = 4.364583F;
			this.txtTotCount.Width = 1.1875F;
			// 
			// txtTotHomestead
			// 
			this.txtTotHomestead.Height = 0.19F;
			this.txtTotHomestead.Left = 4.4375F;
			this.txtTotHomestead.Name = "txtTotHomestead";
			this.txtTotHomestead.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotHomestead.Text = "0";
			this.txtTotHomestead.Top = 4.833333F;
			this.txtTotHomestead.Width = 1.1875F;
			// 
			// txtTotTot
			// 
			this.txtTotTot.Height = 0.19F;
			this.txtTotTot.Left = 4.4375F;
			this.txtTotTot.Name = "txtTotTot";
			this.txtTotTot.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotTot.Text = "0";
			this.txtTotTot.Top = 5.15625F;
			this.txtTotTot.Width = 1.1875F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.19F;
			this.Field30.Left = 1.625F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'";
			this.Field30.Text = "Total Count";
			this.Field30.Top = 1.625F;
			this.Field30.Width = 1.5F;
			// 
			// txtTETotCount
			// 
			this.txtTETotCount.Height = 0.19F;
			this.txtTETotCount.Left = 3.25F;
			this.txtTETotCount.Name = "txtTETotCount";
			this.txtTETotCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTETotCount.Text = "0";
			this.txtTETotCount.Top = 1.625F;
			this.txtTETotCount.Width = 1.1875F;
			// 
			// txtTEHomeCount
			// 
			this.txtTEHomeCount.Height = 0.19F;
			this.txtTEHomeCount.Left = 6.75F;
			this.txtTEHomeCount.Name = "txtTEHomeCount";
			this.txtTEHomeCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTEHomeCount.Text = "0";
			this.txtTEHomeCount.Top = 0.84375F;
			this.txtTEHomeCount.Width = 0.6875F;
			// 
			// txtBHomeCount
			// 
			this.txtBHomeCount.Height = 0.19F;
			this.txtBHomeCount.Left = 6.75F;
			this.txtBHomeCount.Name = "txtBHomeCount";
			this.txtBHomeCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBHomeCount.Text = "0";
			this.txtBHomeCount.Top = 2.9375F;
			this.txtBHomeCount.Width = 0.6875F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.19F;
			this.Field31.Left = 6.75F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field31.Text = "Count";
			this.Field31.Top = 0.6875F;
			this.Field31.Width = 0.6875F;
			// 
			// Field32
			// 
			this.Field32.Height = 0.19F;
			this.Field32.Left = 6F;
			this.Field32.Name = "Field32";
			this.Field32.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field32.Text = "Value";
			this.Field32.Top = 0.6875F;
			this.Field32.Width = 0.6875F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.19F;
			this.Field33.Left = 6.75F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field33.Text = "Count";
			this.Field33.Top = 2.78125F;
			this.Field33.Width = 0.6875F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.19F;
			this.Field34.Left = 6F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field34.Text = "Value";
			this.Field34.Top = 2.78125F;
			this.Field34.Width = 0.6875F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.19F;
			this.Field35.Left = 1.625F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'";
			this.Field35.Text = "Homestead x .7";
			this.Field35.Top = 4.989583F;
			this.Field35.Width = 1.4375F;
			// 
			// txtHalfHomestead
			// 
			this.txtHalfHomestead.Height = 0.19F;
			this.txtHalfHomestead.Left = 4.4375F;
			this.txtHalfHomestead.Name = "txtHalfHomestead";
			this.txtHalfHomestead.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHalfHomestead.Text = "0";
			this.txtHalfHomestead.Top = 4.989583F;
			this.txtHalfHomestead.Width = 1.1875F;
			// 
			// Field36
			// 
			this.Field36.Height = 0.19F;
			this.Field36.Left = 2.125F;
			this.Field36.Name = "Field36";
			this.Field36.Style = "font-family: \'Tahoma\'";
			this.Field36.Text = "No Valuation";
			this.Field36.Top = 2.3125F;
			this.Field36.Width = 1F;
			// 
			// txtBRENoValuation
			// 
			this.txtBRENoValuation.Height = 0.19F;
			this.txtBRENoValuation.Left = 3.25F;
			this.txtBRENoValuation.Name = "txtBRENoValuation";
			this.txtBRENoValuation.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBRENoValuation.Text = "0";
			this.txtBRENoValuation.Top = 2.3125F;
			this.txtBRENoValuation.Width = 1.1875F;
			// 
			// Field37
			// 
			this.Field37.Height = 0.19F;
			this.Field37.Left = 2.125F;
			this.Field37.Name = "Field37";
			this.Field37.Style = "font-family: \'Tahoma\'";
			this.Field37.Text = "No Valuation";
			this.Field37.Top = 3.25F;
			this.Field37.Width = 1F;
			// 
			// txtBPPNoValuation
			// 
			this.txtBPPNoValuation.Height = 0.19F;
			this.txtBPPNoValuation.Left = 3.25F;
			this.txtBPPNoValuation.Name = "txtBPPNoValuation";
			this.txtBPPNoValuation.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBPPNoValuation.Text = "0";
			this.txtBPPNoValuation.Top = 3.25F;
			this.txtBPPNoValuation.Width = 1.1875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuniname,
            this.txtDate,
            this.txtTime,
            this.txtTitle,
            this.txtPage,
            this.txtTownName});
			this.PageHeader.Height = 0.4583333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtMuniname
			// 
			this.txtMuniname.CanGrow = false;
			this.txtMuniname.Height = 0.19F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = "Field1";
			this.txtMuniname.Top = 0.03125F;
			this.txtMuniname.Width = 1.8125F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.5F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.2395833F;
			this.txtTitle.Left = 1.833333F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Real Estate & Personal Property Audit Summary";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 4.208333F;
			// 
			// txtPage
			// 
			this.txtPage.CanGrow = false;
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTownName
			// 
			this.txtTownName.Height = 0.19F;
			this.txtTownName.HyperLink = null;
			this.txtTownName.Left = 1.822917F;
			this.txtTownName.Name = "txtTownName";
			this.txtTownName.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtTownName.Text = null;
			this.txtTownName.Top = 0.21875F;
			this.txtTownName.Width = 4.1875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptREPPAudit
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTELand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTERECount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEREExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEPPCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEPPExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBRECount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBREExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBPPCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBPPExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPPBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotREBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTETotCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTEHomeCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBHomeCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBRENoValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBPPNoValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTELand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTERECount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEREExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEPPCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEPPExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBRECount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBREExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBPPCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBPPExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotPPBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotREBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTETotCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTEHomeCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBHomeCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHalfHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBRENoValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBPPNoValuation;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTownName;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
