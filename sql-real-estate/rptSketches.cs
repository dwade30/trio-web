﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using System.IO;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptSketches.
	/// </summary>
	public partial class rptSketches : BaseSectionReport
	{
		public static rptSketches InstancePtr
		{
			get
			{
				return (rptSketches)Sys.GetInstance(typeof(rptSketches));
			}
		}

		protected rptSketches _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptSketches()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Sketches";
		}
		// nObj = 1
		//   0	rptSketches	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		bool boolSketchless;
		bool boolBldgValueOnly;
		string strOrderBy = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
			string strReturn = "";
			if (!eArgs.EOF)
			{
				strReturn = GetSketchNames_2(clsReport.Get_Fields_Int32("id"));
				if (!boolSketchless)
				{
					while (strReturn == string.Empty && !clsReport.EndOfFile())
					{
						clsReport.MoveNext();
						if (!clsReport.EndOfFile())
						{
							strReturn = GetSketchNames_2(clsReport.Get_Fields_Int32("id"));
						}
						else
						{
							strReturn = "";
						}
					}
					txtFile.Text = strReturn;
				}
				else
				{
					while (strReturn != string.Empty && !clsReport.EndOfFile())
					{
						clsReport.MoveNext();
						if (!clsReport.EndOfFile())
						{
							strReturn = GetSketchNames_2(clsReport.Get_Fields_Int32("id"));
						}
						else
						{
							strReturn = "";
						}
					}
				}
				if (!clsReport.EndOfFile())
				{
					txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("rsaccount"));
					txtCard.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("rscard"));
					txtMapLot.Text = FCConvert.ToString(clsReport.Get_Fields_String("rsmaplot"));
				}
			}
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolNoSketches, bool boolBldgValOnly = false, short intType = 1)
		{
			boolSketchless = boolNoSketches;
			boolBldgValueOnly = boolBldgValOnly;
			if (boolSketchless)
			{
				lblTitle.Text = "Accounts Without Sketches";
				lblFileorName.Visible = false;
				txtFile.Visible = false;
				Detail.ColumnCount = 2;
				Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.DownAcross;
				lblMapLot2.Visible = true;
				lblAcct2.Visible = true;
				lblCard2.Visible = true;
			}
			if (intType == 1)
			{
				// order by account
				strOrderBy = " rsaccount,rscard";
			}
			else
			{
				// order by maplot
				strOrderBy = " rsmaplot,rsaccount,rscard";
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Sketches");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			if (!boolBldgValueOnly)
			{
				clsReport.OpenRecordset("select id,rsaccount,rscard,rsmaplot from master where not rsdeleted = 1 order by " + strOrderBy, modGlobalVariables.strREDatabase);
			}
			else
			{
				clsReport.OpenRecordset("select id,rsaccount,rscard,rsmaplot from master where not rsdeleted = 1 and lastbldgval > 0 order by " + strOrderBy, modGlobalVariables.strREDatabase);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsReport.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private string GetSketchNames_2(int lngAutoID)
		{
			return GetSketchNames(ref lngAutoID);
		}

		private string GetSketchNames(ref int lngAutoID)
		{
			//FileSystemObject fso = new FileSystemObject();
			string GetSketchNames = "";
			string strReturn;
			string strSketchName;
			int x;
			GetSketchNames = "";
			strSketchName = Convert.ToString(FCConvert.ToInt32(lngAutoID), 16).ToUpper();
			strSketchName = Strings.Right("0000000" + strSketchName, 7);
			x = 1;
			strReturn = "";
			while (File.Exists("Sketches\\" + strSketchName + Convert.ToString(FCConvert.ToInt16(x), 16).ToUpper() + ".skt"))
			{
				strReturn += strSketchName + Convert.ToString(FCConvert.ToInt16(x), 16).ToUpper() + ".skt" + ", ";
				x += 1;
			}
			strReturn = Strings.Trim(strReturn);
			if (strReturn != string.Empty)
			{
				strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
			}
			GetSketchNames = strReturn;
			return GetSketchNames;
		}

		
	}
}
