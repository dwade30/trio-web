﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportChanges.
	/// </summary>
	partial class frmImportChanges : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8_0;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel shapeLand;
		public fecherFoundation.FCLabel Label13;
		public FCGrid GridLand;
		public fecherFoundation.FCTextBox txtowner;
		public fecherFoundation.FCTextBox txtSecondOwner;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtTelephone;
		public fecherFoundation.FCTextBox txtStreetNumber;
		public fecherFoundation.FCTextBox txtRef1;
		public fecherFoundation.FCTextBox txtRef2;
		public fecherFoundation.FCTextBox txtMapLot;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public FCGrid GridMisc;
		public fecherFoundation.FCTextBox txtStreetName;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame framCommercial;
		public FCGrid GridComm2;
		public FCGrid GridComm1;
		public fecherFoundation.FCFrame framDwelling;
		public fecherFoundation.FCGrid GridDwelling1;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel shapeOutbuilding;
		public FCGrid GridOutbuilding;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid GridPics;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid GridNewPics;
		public fecherFoundation.FCPanel framImage;
		public fecherFoundation.FCPictureBox Image2;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public fecherFoundation.FCPanel framSketchDragIcon;
		public fecherFoundation.FCPictureBox ImDragImage;
		public fecherFoundation.FCFrame framSketchMenu;
		public FCGrid GridNewSketches;
		public fecherFoundation.FCFrame framSketch;
		public FCGrid VSFlexGrid1;
		public FCGrid VSFlexGrid2;
		public FCGrid GridSketches;
		public fecherFoundation.FCPictureBox SketchCopyImage;
		public fecherFoundation.FCTabPage SSTab1_Page6;
		public fecherFoundation.FCCheckBox chkWavFile;
		//public AxMCI.AxMMControl MMControl1;
		public dynamic MMControl1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmImportChanges));
			this.components = new System.ComponentModel.Container();
			//this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8_0 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.shapeLand = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.GridLand = new FCGrid();
			this.txtowner = new fecherFoundation.FCTextBox();
			this.txtSecondOwner = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtTelephone = new fecherFoundation.FCTextBox();
			this.txtStreetNumber = new fecherFoundation.FCTextBox();
			this.txtRef1 = new fecherFoundation.FCTextBox();
			this.txtRef2 = new fecherFoundation.FCTextBox();
			this.txtMapLot = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.GridMisc = new FCGrid();
			this.txtStreetName = new fecherFoundation.FCTextBox();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.framCommercial = new fecherFoundation.FCFrame();
			this.GridComm2 = new FCGrid();
			this.GridComm1 = new FCGrid();
			this.framDwelling = new fecherFoundation.FCFrame();
			this.GridDwelling1 = new fecherFoundation.FCGrid();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.Label14 = new fecherFoundation.FCLabel();
			this.shapeOutbuilding = new fecherFoundation.FCLabel();
			this.GridOutbuilding = new FCGrid();
			this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.GridPics = new FCGrid();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.GridNewPics = new FCGrid();
			this.framImage = new fecherFoundation.FCPanel();
			this.Image2 = new fecherFoundation.FCPictureBox();
			this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
			this.framSketchDragIcon = new fecherFoundation.FCPanel();
			this.ImDragImage = new fecherFoundation.FCPictureBox();
			this.framSketchMenu = new fecherFoundation.FCFrame();
			this.GridNewSketches = new FCGrid();
			this.framSketch = new fecherFoundation.FCFrame();
			this.VSFlexGrid1 = new FCGrid();
			this.VSFlexGrid2 = new FCGrid();
			this.GridSketches = new FCGrid();
			this.SketchCopyImage = new fecherFoundation.FCPictureBox();
			this.SSTab1_Page6 = new fecherFoundation.FCTabPage();
			this.chkWavFile = new fecherFoundation.FCCheckBox();
			//this.MMControl1 = new AxMCI.AxMMControl();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			//
			// SSTab1
			//
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Controls.Add(this.SSTab1_Page4);
			this.SSTab1.Controls.Add(this.SSTab1_Page5);
			this.SSTab1.Controls.Add(this.SSTab1_Page6);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.TabIndex = 0;
			this.SSTab1.Location = new System.Drawing.Point(2, 28);
			this.SSTab1.Size = new System.Drawing.Size(618, 499);
			//
			// SSTab1_Page1
			//
			this.SSTab1_Page1.Controls.Add(this.Label1);
			this.SSTab1_Page1.Controls.Add(this.Label2);
			this.SSTab1_Page1.Controls.Add(this.Label3);
			this.SSTab1_Page1.Controls.Add(this.Label5);
			this.SSTab1_Page1.Controls.Add(this.Label6);
			this.SSTab1_Page1.Controls.Add(this.Label7);
			this.SSTab1_Page1.Controls.Add(this.Label8_0);
			this.SSTab1_Page1.Controls.Add(this.Label9);
			this.SSTab1_Page1.Controls.Add(this.Label10);
			this.SSTab1_Page1.Controls.Add(this.Label11);
			this.SSTab1_Page1.Controls.Add(this.Label12);
			this.SSTab1_Page1.Controls.Add(this.shapeLand);
			this.SSTab1_Page1.Controls.Add(this.Label13);
			this.SSTab1_Page1.Controls.Add(this.GridLand);
			this.SSTab1_Page1.Controls.Add(this.txtowner);
			this.SSTab1_Page1.Controls.Add(this.txtSecondOwner);
			this.SSTab1_Page1.Controls.Add(this.txtAddress1);
			this.SSTab1_Page1.Controls.Add(this.txtAddress2);
			this.SSTab1_Page1.Controls.Add(this.txtCity);
			this.SSTab1_Page1.Controls.Add(this.txtTelephone);
			this.SSTab1_Page1.Controls.Add(this.txtStreetNumber);
			this.SSTab1_Page1.Controls.Add(this.txtRef1);
			this.SSTab1_Page1.Controls.Add(this.txtRef2);
			this.SSTab1_Page1.Controls.Add(this.txtMapLot);
			this.SSTab1_Page1.Controls.Add(this.txtState);
			this.SSTab1_Page1.Controls.Add(this.txtZip);
			this.SSTab1_Page1.Controls.Add(this.txtZip4);
			this.SSTab1_Page1.Controls.Add(this.GridMisc);
			this.SSTab1_Page1.Controls.Add(this.txtStreetName);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Size = new System.Drawing.Size(618, 479);
			this.SSTab1_Page1.Text = "Property";
			//
			// Label1
			//
			this.Label1.Name = "Label1";
			this.Label1.Tag = "Owner";
			this.Label1.TabIndex = 9;
			this.Label1.Location = new System.Drawing.Point(10, 59);
			this.Label1.Size = new System.Drawing.Size(48, 13);
			this.Label1.Text = "Owner";
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.Tag = "Second Owner";
			this.Label2.TabIndex = 10;
			this.Label2.Location = new System.Drawing.Point(10, 81);
			this.Label2.Size = new System.Drawing.Size(86, 13);
			this.Label2.Text = "Second Owner";
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.Tag = "Address1";
			this.Label3.TabIndex = 11;
			this.Label3.Location = new System.Drawing.Point(10, 104);
			this.Label3.Size = new System.Drawing.Size(88, 15);
			this.Label3.Text = "Mailing Address";
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label5
			//
			this.Label5.Name = "Label5";
			this.Label5.Tag = "City";
			this.Label5.TabIndex = 12;
			this.Label5.Location = new System.Drawing.Point(10, 148);
			this.Label5.Size = new System.Drawing.Size(39, 13);
			this.Label5.Text = "City";
			//this.Label5.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label6
			//
			this.Label6.Name = "Label6";
			this.Label6.Tag = "telephone";
			this.Label6.TabIndex = 13;
			this.Label6.Location = new System.Drawing.Point(10, 170);
			this.Label6.Size = new System.Drawing.Size(62, 15);
			this.Label6.Text = "Telephone";
			//this.Label6.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label7
			//
			this.Label7.Name = "Label7";
			this.Label7.Tag = "Street Number";
			this.Label7.TabIndex = 14;
			this.Label7.Location = new System.Drawing.Point(307, 81);
			this.Label7.Size = new System.Drawing.Size(66, 13);
			this.Label7.Text = "Location";
			//this.Label7.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label8_0
			//
			this.Label8_0.Name = "Label8_0";
			this.Label8_0.Tag = "ref1";
			this.Label8_0.TabIndex = 15;
			this.Label8_0.Location = new System.Drawing.Point(307, 104);
			this.Label8_0.Size = new System.Drawing.Size(78, 13);
			this.Label8_0.Text = "Reference 1";
			//this.Label8_0.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label9
			//
			this.Label9.Name = "Label9";
			this.Label9.Tag = "ref2";
			this.Label9.TabIndex = 17;
			this.Label9.Location = new System.Drawing.Point(307, 126);
			this.Label9.Size = new System.Drawing.Size(78, 13);
			this.Label9.Text = "Reference 2";
			//this.Label9.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label10
			//
			this.Label10.Name = "Label10";
			this.Label10.Tag = "maplot";
			this.Label10.TabIndex = 19;
			this.Label10.Location = new System.Drawing.Point(307, 59);
			this.Label10.Size = new System.Drawing.Size(60, 17);
			this.Label10.Text = "Map/Lot";
			//this.Label10.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label11
			//
			this.Label11.Name = "Label11";
			this.Label11.Tag = "City";
			this.Label11.TabIndex = 21;
			this.Label11.Location = new System.Drawing.Point(245, 148);
			this.Label11.Size = new System.Drawing.Size(23, 13);
			this.Label11.Text = "ST";
			//this.Label11.BackColor = System.Drawing.SystemColors.Control;
			//
			// Label12
			//
			this.Label12.Name = "Label12";
			this.Label12.Tag = "Zip";
			this.Label12.TabIndex = 23;
			this.Label12.Location = new System.Drawing.Point(311, 148);
			this.Label12.Size = new System.Drawing.Size(21, 15);
			this.Label12.Text = "Zip";
			//this.Label12.BackColor = System.Drawing.SystemColors.Control;
			//
			// shapeLand
			//
			this.shapeLand.Name = "shapeLand";
			this.shapeLand.Location = new System.Drawing.Point(330, 393);
			this.shapeLand.Size = new System.Drawing.Size(17, 17);
			this.shapeLand.Text = "";
			this.shapeLand.BackColor = System.Drawing.Color.White;
			//
			// Label13
			//
			this.Label13.Name = "Label13";
			this.Label13.TabIndex = 33;
			this.Label13.Location = new System.Drawing.Point(358, 395);
			this.Label13.Size = new System.Drawing.Size(191, 21);
			this.Label13.Text = "Original values";
			//this.Label13.BackColor = System.Drawing.SystemColors.Control;
			//
			// GridLand
			//
			this.GridLand.Name = "GridLand";
			this.GridLand.Enabled = true;
			this.GridLand.TabIndex = 26;
			this.GridLand.Location = new System.Drawing.Point(309, 231);
			this.GridLand.Size = new System.Drawing.Size(282, 141);
			this.GridLand.Rows = 8;
			this.GridLand.Cols = 6;
			this.GridLand.FixedRows = 1;
			this.GridLand.FixedCols = 1;
			this.GridLand.ExtendLastCol = true;
			this.GridLand.ExplorerBar = 0;
			this.GridLand.TabBehavior = 0;
			this.GridLand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridLand.FrozenRows = 0;
			this.GridLand.FrozenCols = 0;
			//
			// txtowner
			//
			this.txtowner.Name = "txtowner";
			this.txtowner.Tag = "Owner";
			this.txtowner.TabIndex = 1;
			this.txtowner.Location = new System.Drawing.Point(111, 57);
			this.txtowner.Size = new System.Drawing.Size(175, 40);
			this.txtowner.Text = "";
			this.txtowner.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtSecondOwner
			//
			this.txtSecondOwner.Name = "txtSecondOwner";
			this.txtSecondOwner.Tag = "Second Owner";
			this.txtSecondOwner.TabIndex = 2;
			this.txtSecondOwner.Location = new System.Drawing.Point(111, 79);
			this.txtSecondOwner.Size = new System.Drawing.Size(175, 40);
			this.txtSecondOwner.Text = "";
			this.txtSecondOwner.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtAddress1
			//
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "Address1";
			this.txtAddress1.TabIndex = 3;
			this.txtAddress1.Location = new System.Drawing.Point(111, 102);
			this.txtAddress1.Size = new System.Drawing.Size(175, 40);
			this.txtAddress1.Text = "";
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtAddress2
			//
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "Address2";
			this.txtAddress2.TabIndex = 4;
			this.txtAddress2.Location = new System.Drawing.Point(111, 124);
			this.txtAddress2.Size = new System.Drawing.Size(175, 40);
			this.txtAddress2.Text = "";
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtCity
			//
			this.txtCity.Name = "txtCity";
			this.txtCity.Tag = "City";
			this.txtCity.TabIndex = 5;
			this.txtCity.Location = new System.Drawing.Point(111, 146);
			this.txtCity.Size = new System.Drawing.Size(124, 40);
			this.txtCity.Text = "";
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtTelephone
			//
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Tag = "telephone";
			this.txtTelephone.TabIndex = 6;
			this.txtTelephone.Location = new System.Drawing.Point(111, 168);
			this.txtTelephone.Size = new System.Drawing.Size(175, 40);
			this.txtTelephone.Text = "";
			this.txtTelephone.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtStreetNumber
			//
			this.txtStreetNumber.Name = "txtStreetNumber";
			this.txtStreetNumber.Tag = "Street Number";
			this.txtStreetNumber.TabIndex = 7;
			this.txtStreetNumber.Location = new System.Drawing.Point(390, 79);
			this.txtStreetNumber.Size = new System.Drawing.Size(52, 40);
			this.txtStreetNumber.Text = "";
			this.txtStreetNumber.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtRef1
			//
			this.txtRef1.Name = "txtRef1";
			this.txtRef1.Tag = "ref1";
			this.txtRef1.TabIndex = 8;
			this.txtRef1.Location = new System.Drawing.Point(390, 102);
			this.txtRef1.Size = new System.Drawing.Size(203, 40);
			this.txtRef1.Text = "";
			this.txtRef1.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtRef2
			//
			this.txtRef2.Name = "txtRef2";
			this.txtRef2.Tag = "ref2";
			this.txtRef2.TabIndex = 16;
			this.txtRef2.Location = new System.Drawing.Point(390, 124);
			this.txtRef2.Size = new System.Drawing.Size(203, 40);
			this.txtRef2.Text = "";
			this.txtRef2.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtMapLot
			//
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "maplot";
			this.txtMapLot.TabIndex = 18;
			this.txtMapLot.Location = new System.Drawing.Point(390, 57);
			this.txtMapLot.Size = new System.Drawing.Size(203, 40);
			this.txtMapLot.Text = "";
			this.txtMapLot.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtState
			//
			this.txtState.Name = "txtState";
			this.txtState.Tag = "City";
			this.txtState.TabIndex = 20;
			this.txtState.Location = new System.Drawing.Point(277, 146);
			this.txtState.Size = new System.Drawing.Size(25, 40);
			this.txtState.Text = "";
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtZip
			//
			this.txtZip.Name = "txtZip";
			this.txtZip.Tag = "Zip";
			this.txtZip.TabIndex = 22;
			this.txtZip.Location = new System.Drawing.Point(342, 146);
			this.txtZip.Size = new System.Drawing.Size(48, 40);
			this.txtZip.Text = "";
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtZip4
			//
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Tag = "Zip4";
			this.txtZip4.TabIndex = 24;
			this.txtZip4.Location = new System.Drawing.Point(390, 146);
			this.txtZip4.Size = new System.Drawing.Size(39, 40);
			this.txtZip4.Text = "";
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			//
			// GridMisc
			//
			this.GridMisc.Name = "GridMisc";
			this.GridMisc.Enabled = true;
			this.GridMisc.TabIndex = 25;
			this.GridMisc.Location = new System.Drawing.Point(14, 229);
			this.GridMisc.Size = new System.Drawing.Size(246, 210);
			this.GridMisc.Rows = 12;
			this.GridMisc.Cols = 3;
			this.GridMisc.FixedRows = 1;
			this.GridMisc.FixedCols = 1;
			this.GridMisc.ExtendLastCol = true;
			this.GridMisc.ExplorerBar = 0;
			this.GridMisc.TabBehavior = 0;
			this.GridMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridMisc.FrozenRows = 0;
			this.GridMisc.FrozenCols = 0;
			//
			// txtStreetName
			//
			this.txtStreetName.Name = "txtStreetName";
			this.txtStreetName.Tag = "Street";
			this.txtStreetName.TabIndex = 27;
			this.txtStreetName.Location = new System.Drawing.Point(443, 79);
			this.txtStreetName.Size = new System.Drawing.Size(151, 40);
			this.txtStreetName.Text = "";
			this.txtStreetName.BackColor = System.Drawing.SystemColors.Window;
			//
			// SSTab1_Page2
			//
			this.SSTab1_Page2.Controls.Add(this.framCommercial);
			this.SSTab1_Page2.Controls.Add(this.framDwelling);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Size = new System.Drawing.Size(618, 479);
			this.SSTab1_Page2.Text = "Dwelling";
			//
			// framCommercial
			//
			this.framCommercial.Controls.Add(this.GridComm2);
			this.framCommercial.Controls.Add(this.GridComm1);
			this.framCommercial.Name = "framCommercial";
			this.framCommercial.Visible = false;
			this.framCommercial.TabIndex = 30;
			this.framCommercial.Location = new System.Drawing.Point(6, 37);
			this.framCommercial.Size = new System.Drawing.Size(608, 450);
			this.framCommercial.Text = "";
			//this.framCommercial.BackColor = System.Drawing.SystemColors.Control;
			//
			// GridComm2
			//
			this.GridComm2.Name = "GridComm2";
			this.GridComm2.Enabled = true;
			this.GridComm2.TabIndex = 35;
			this.GridComm2.Location = new System.Drawing.Point(309, 51);
			this.GridComm2.Size = new System.Drawing.Size(290, 296);
			this.GridComm2.Rows = 17;
			this.GridComm2.Cols = 3;
			this.GridComm2.FixedRows = 1;
			this.GridComm2.FixedCols = 1;
			this.GridComm2.ExtendLastCol = true;
			this.GridComm2.ExplorerBar = 0;
			this.GridComm2.TabBehavior = 0;
			this.GridComm2.Editable = 0;
			this.GridComm2.FrozenRows = 0;
			this.GridComm2.FrozenCols = 0;
			//
			// GridComm1
			//
			this.GridComm1.Name = "GridComm1";
			this.GridComm1.Enabled = true;
			this.GridComm1.TabIndex = 36;
			this.GridComm1.Location = new System.Drawing.Point(10, 51);
			this.GridComm1.Size = new System.Drawing.Size(290, 314);
			this.GridComm1.Rows = 18;
			this.GridComm1.Cols = 3;
			this.GridComm1.FixedRows = 1;
			this.GridComm1.FixedCols = 1;
			this.GridComm1.ExtendLastCol = true;
			this.GridComm1.ExplorerBar = 0;
			this.GridComm1.TabBehavior = 0;
			this.GridComm1.Editable = 0;
			this.GridComm1.FrozenRows = 0;
			this.GridComm1.FrozenCols = 0;
			//
			// framDwelling
			//
			this.framDwelling.Controls.Add(this.GridDwelling1);
			this.framDwelling.Name = "framDwelling";
			this.framDwelling.TabIndex = 28;
			this.framDwelling.Location = new System.Drawing.Point(6, 37);
			this.framDwelling.Size = new System.Drawing.Size(608, 450);
			this.framDwelling.Text = "";
			//this.framDwelling.BackColor = System.Drawing.SystemColors.Control;
			//
			// GridDwelling1
			//
			this.GridDwelling1.Name = "GridDwelling1";
			this.GridDwelling1.Enabled = true;
			this.GridDwelling1.TabIndex = 29;
			this.GridDwelling1.Location = new System.Drawing.Point(127, 24);
			this.GridDwelling1.Size = new System.Drawing.Size(325, 408);
			this.GridDwelling1.Rows = 45;
			this.GridDwelling1.Cols = 3;
			this.GridDwelling1.FixedRows = 1;
			this.GridDwelling1.FixedCols = 1;
			this.GridDwelling1.ExtendLastCol = true;
			this.GridDwelling1.ExplorerBar = 0;
			this.GridDwelling1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDwelling1.Editable = 0;
			this.GridDwelling1.FrozenRows = 0;
			this.GridDwelling1.FrozenCols = 0;
			this.GridDwelling1.KeyDown += new KeyEventHandler(this.GridDwelling1_KeyDownEvent);
			this.GridDwelling1.KeyDownEdit += new KeyEventHandler(this.GridDwelling1_KeyDownEdit);
			this.GridDwelling1.CurrentCellChanged += new System.EventHandler(this.GridDwelling1_RowColChange);
			//
			// SSTab1_Page3
			//
			this.SSTab1_Page3.Controls.Add(this.Label14);
			this.SSTab1_Page3.Controls.Add(this.shapeOutbuilding);
			this.SSTab1_Page3.Controls.Add(this.GridOutbuilding);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Size = new System.Drawing.Size(618, 479);
			this.SSTab1_Page3.Text = "Outbuildings";
			//
			// Label14
			//
			this.Label14.Name = "Label14";
			this.Label14.TabIndex = 34;
			this.Label14.Location = new System.Drawing.Point(279, 61);
			this.Label14.Size = new System.Drawing.Size(191, 21);
			this.Label14.Text = "Original values";
			//this.Label14.BackColor = System.Drawing.SystemColors.Control;
			//
			// shapeOutbuilding
			//
			this.shapeOutbuilding.Name = "shapeOutbuilding";
			this.shapeOutbuilding.Location = new System.Drawing.Point(251, 59);
			this.shapeOutbuilding.Size = new System.Drawing.Size(17, 17);
			this.shapeOutbuilding.Text = "";
			this.shapeOutbuilding.BackColor = System.Drawing.Color.White;
			//
			// GridOutbuilding
			//
			this.GridOutbuilding.Name = "GridOutbuilding";
			this.GridOutbuilding.Enabled = true;
			this.GridOutbuilding.TabIndex = 32;
			this.GridOutbuilding.Location = new System.Drawing.Point(10, 116);
			this.GridOutbuilding.Size = new System.Drawing.Size(594, 221);
			this.GridOutbuilding.Rows = 10;
			this.GridOutbuilding.Cols = 9;
			this.GridOutbuilding.FixedRows = 1;
			this.GridOutbuilding.FixedCols = 1;
			this.GridOutbuilding.ExtendLastCol = true;
			this.GridOutbuilding.ExplorerBar = 0;
			this.GridOutbuilding.TabBehavior = 0;
			this.GridOutbuilding.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridOutbuilding.FrozenRows = 0;
			this.GridOutbuilding.FrozenCols = 0;
			//
			// SSTab1_Page4
			//
			this.SSTab1_Page4.Controls.Add(this.Image1);
			this.SSTab1_Page4.Controls.Add(this.Frame1);
			this.SSTab1_Page4.Controls.Add(this.Frame2);
			this.SSTab1_Page4.Controls.Add(this.framImage);
			this.SSTab1_Page4.Name = "SSTab1_Page4";
			this.SSTab1_Page4.Size = new System.Drawing.Size(618, 479);
			this.SSTab1_Page4.Text = "Pictures";
			//
			// Image1
			//
			this.Image1.Name = "Image1";
			this.Image1.Visible = false;
			this.Image1.Location = new System.Drawing.Point(26, 403);
			this.Image1.Size = new System.Drawing.Size(33, 37);
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.Image1.BackColor = System.Drawing.SystemColors.Control;
			//
			// Frame1
			//
			this.Frame1.Controls.Add(this.GridPics);
			this.Frame1.Name = "Frame1";
			this.Frame1.TabIndex = 31;
			this.Frame1.Location = new System.Drawing.Point(8, 8);
			this.Frame1.Size = new System.Drawing.Size(602, 335);
			this.Frame1.Text = "";
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			//
			// GridPics
			//
			this.GridPics.Name = "GridPics";
			this.GridPics.Enabled = true;
			this.GridPics.TabIndex = 37;
			this.ToolTip1.SetToolTip(this.GridPics, "Drag and drop entries onto the Add/Remove areas or onto other pictures to edit.");
			this.GridPics.Location = new System.Drawing.Point(6, 14);
			this.GridPics.Size = new System.Drawing.Size(590, 314);
			this.GridPics.Rows = 0;
			this.GridPics.Cols = 3;
			this.GridPics.FixedRows = 0;
			this.GridPics.FixedCols = 0;
			this.GridPics.ExtendLastCol = true;
			this.GridPics.ExplorerBar = 0;
			this.GridPics.TabBehavior = 0;
			this.GridPics.Editable = 0;
			this.GridPics.FrozenRows = 0;
			this.GridPics.FrozenCols = 0;
			this.GridPics.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GridPics_MouseMoveEvent);
			this.GridPics.MouseUp += new MouseEventHandler(this.GridPics_MouseUpEvent);
			this.GridPics.MouseDown += new MouseEventHandler(this.GridPics_BeforeMouseDown);
			//
			// Frame2
			//
			this.Frame2.Controls.Add(this.GridNewPics);
			this.Frame2.Name = "Frame2";
			this.Frame2.TabIndex = 38;
			this.Frame2.Location = new System.Drawing.Point(107, 344);
			this.Frame2.Size = new System.Drawing.Size(403, 128);
			this.Frame2.Text = "";
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.MouseUp += new Wisej.Web.MouseEventHandler(this.Frame2_MouseUp);
			//
			// GridNewPics
			//
			this.GridNewPics.Name = "GridNewPics";
			this.GridNewPics.Enabled = true;
			this.GridNewPics.TabIndex = 39;
			this.ToolTip1.SetToolTip(this.GridNewPics, "Drag and drop entries onto the Add/Remove areas or onto other pictures to edit.");
			this.GridNewPics.Location = new System.Drawing.Point(4, 14);
			this.GridNewPics.Size = new System.Drawing.Size(393, 106);
			this.GridNewPics.Rows = 1;
			this.GridNewPics.Cols = 2;
			this.GridNewPics.FixedRows = 0;
			this.GridNewPics.FixedCols = 0;
			this.GridNewPics.ExtendLastCol = true;
			this.GridNewPics.ExplorerBar = 0;
			this.GridNewPics.TabBehavior = 0;
			this.GridNewPics.Editable = 0;
			this.GridNewPics.FrozenRows = 0;
			this.GridNewPics.FrozenCols = 0;
			this.GridNewPics.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GridNewPics_MouseMoveEvent);
			this.GridNewPics.MouseUp += new MouseEventHandler(this.GridNewPics_MouseUpEvent);
			//
			// framImage
			//
			this.framImage.Controls.Add(this.Image2);
			this.framImage.Name = "framImage";
			this.framImage.TabIndex = 40;
			this.framImage.Location = new System.Drawing.Point(522, 405);
			this.framImage.Size = new System.Drawing.Size(66, 54);
			this.framImage.Text = "";
			//this.framImage.BackColor = System.Drawing.SystemColors.Control;
			this.framImage.MouseMove += new Wisej.Web.MouseEventHandler(this.framImage_MouseMove);
			//
			// Image2
			//
			this.Image2.Name = "Image2";
			this.Image2.Location = new System.Drawing.Point(0, 0);
			this.Image2.Size = new System.Drawing.Size(60, 48);
			this.Image2.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.Image2.BackColor = System.Drawing.SystemColors.Control;
			this.Image2.MouseMove += new Wisej.Web.MouseEventHandler(this.Image2_MouseMove);
			this.Image2.MouseUp += new Wisej.Web.MouseEventHandler(this.Image2_MouseUp);
			//
			// SSTab1_Page5
			//
			this.SSTab1_Page5.Controls.Add(this.framSketchDragIcon);
			this.SSTab1_Page5.Controls.Add(this.framSketchMenu);
			this.SSTab1_Page5.Controls.Add(this.framSketch);
			this.SSTab1_Page5.Controls.Add(this.SketchCopyImage);
			this.SSTab1_Page5.Name = "SSTab1_Page5";
			this.SSTab1_Page5.Size = new System.Drawing.Size(618, 479);
			this.SSTab1_Page5.Text = "Sketches";
			//
			// framSketchDragIcon
			//
			this.framSketchDragIcon.Controls.Add(this.ImDragImage);
			this.framSketchDragIcon.Name = "framSketchDragIcon";
			this.framSketchDragIcon.TabIndex = 48;
			this.framSketchDragIcon.Location = new System.Drawing.Point(520, 377);
			this.framSketchDragIcon.Size = new System.Drawing.Size(66, 54);
			this.framSketchDragIcon.Text = "";
			//this.framSketchDragIcon.BackColor = System.Drawing.SystemColors.Control;
			//
			// ImDragImage
			//
			this.ImDragImage.Name = "ImDragImage";
			this.ImDragImage.Location = new System.Drawing.Point(0, 0);
			this.ImDragImage.Size = new System.Drawing.Size(60, 48);
			this.ImDragImage.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.ImDragImage.BackColor = System.Drawing.SystemColors.Control;
			this.ImDragImage.MouseMove += new Wisej.Web.MouseEventHandler(this.ImDragImage_MouseMove);
			this.ImDragImage.MouseUp += new Wisej.Web.MouseEventHandler(this.ImDragImage_MouseUp);
			//
			// framSketchMenu
			//
			this.framSketchMenu.Controls.Add(this.GridNewSketches);
			this.framSketchMenu.Name = "framSketchMenu";
			this.framSketchMenu.TabIndex = 46;
			this.framSketchMenu.Location = new System.Drawing.Point(93, 342);
			this.framSketchMenu.Size = new System.Drawing.Size(403, 128);
			this.framSketchMenu.Text = "";
			//this.framSketchMenu.BackColor = System.Drawing.SystemColors.Control;
			//
			// GridNewSketches
			//
			this.GridNewSketches.Name = "GridNewSketches";
			this.GridNewSketches.Enabled = true;
			this.GridNewSketches.TabIndex = 47;
			this.ToolTip1.SetToolTip(this.GridNewSketches, "Drag and drop entries onto the Add/Remove areas or onto other sketches to edit.");
			this.GridNewSketches.Location = new System.Drawing.Point(4, 14);
			this.GridNewSketches.Size = new System.Drawing.Size(393, 106);
			this.GridNewSketches.Rows = 1;
			this.GridNewSketches.Cols = 2;
			this.GridNewSketches.FixedRows = 0;
			this.GridNewSketches.FixedCols = 0;
			this.GridNewSketches.ExtendLastCol = true;
			this.GridNewSketches.ExplorerBar = 0;
			this.GridNewSketches.TabBehavior = 0;
			this.GridNewSketches.Editable = 0;
			this.GridNewSketches.FrozenRows = 0;
			this.GridNewSketches.FrozenCols = 0;
			this.GridNewSketches.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GridNewSketches_MouseMoveEvent);
			this.GridNewSketches.MouseUp += new MouseEventHandler(this.GridNewSketches_MouseUpEvent);
			//
			// framSketch
			//
			this.framSketch.Controls.Add(this.VSFlexGrid1);
			this.framSketch.Controls.Add(this.VSFlexGrid2);
			this.framSketch.Controls.Add(this.GridSketches);
			this.framSketch.Name = "framSketch";
			this.framSketch.TabIndex = 42;
			this.framSketch.Location = new System.Drawing.Point(8, 6);
			this.framSketch.Size = new System.Drawing.Size(602, 335);
			this.framSketch.Text = "";
			//this.framSketch.BackColor = System.Drawing.SystemColors.Control;
			//
			// VSFlexGrid1
			//
			this.VSFlexGrid1.Name = "VSFlexGrid1";
			this.VSFlexGrid1.Enabled = true;
			this.VSFlexGrid1.Visible = false;
			this.VSFlexGrid1.TabIndex = 43;
			this.VSFlexGrid1.Location = new System.Drawing.Point(475, 336);
			this.VSFlexGrid1.Size = new System.Drawing.Size(120, 118);
			this.VSFlexGrid1.Rows = 2;
			this.VSFlexGrid1.Cols = 4;
			this.VSFlexGrid1.FixedRows = 0;
			this.VSFlexGrid1.FixedCols = 0;
			this.VSFlexGrid1.ExtendLastCol = true;
			this.VSFlexGrid1.ExplorerBar = 0;
			this.VSFlexGrid1.TabBehavior = 0;
			this.VSFlexGrid1.Editable = 0;
			this.VSFlexGrid1.FrozenRows = 0;
			this.VSFlexGrid1.FrozenCols = 0;
			//
			// VSFlexGrid2
			//
			this.VSFlexGrid2.Name = "VSFlexGrid2";
			this.VSFlexGrid2.Enabled = true;
			this.VSFlexGrid2.Visible = false;
			this.VSFlexGrid2.TabIndex = 44;
			this.VSFlexGrid2.Location = new System.Drawing.Point(453, 338);
			this.VSFlexGrid2.Size = new System.Drawing.Size(149, 94);
			this.VSFlexGrid2.Rows = 2;
			this.VSFlexGrid2.Cols = 4;
			this.VSFlexGrid2.FixedRows = 0;
			this.VSFlexGrid2.FixedCols = 0;
			this.VSFlexGrid2.ExtendLastCol = true;
			this.VSFlexGrid2.ExplorerBar = 0;
			this.VSFlexGrid2.TabBehavior = 0;
			this.VSFlexGrid2.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			this.VSFlexGrid2.FrozenRows = 0;
			this.VSFlexGrid2.FrozenCols = 0;
			//
			// GridSketches
			//
			this.GridSketches.Name = "GridSketches";
			this.GridSketches.Enabled = true;
			this.GridSketches.TabIndex = 45;
			this.ToolTip1.SetToolTip(this.GridSketches, "Drag and drop entries onto the Add/Remove areas or onto other sketches to edit.");
			this.GridSketches.Location = new System.Drawing.Point(6, 12);
			this.GridSketches.Size = new System.Drawing.Size(590, 314);
			this.GridSketches.Rows = 0;
			this.GridSketches.Cols = 3;
			this.GridSketches.FixedRows = 0;
			this.GridSketches.FixedCols = 0;
			this.GridSketches.ExtendLastCol = true;
			this.GridSketches.ExplorerBar = 0;
			this.GridSketches.TabBehavior = 0;
			this.GridSketches.Editable = 0;
			this.GridSketches.FrozenRows = 0;
			this.GridSketches.FrozenCols = 0;
			this.GridSketches.MouseDown += new MouseEventHandler(this.GridSketches_BeforeMouseDown);
			this.GridSketches.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GridSketches_MouseMoveEvent);
			this.GridSketches.MouseUp += new MouseEventHandler(this.GridSketches_MouseUpEvent);
			//
			// SketchCopyImage
			//
			this.SketchCopyImage.Name = "SketchCopyImage";
			this.SketchCopyImage.Visible = false;
			this.SketchCopyImage.Location = new System.Drawing.Point(22, 403);
			this.SketchCopyImage.Size = new System.Drawing.Size(33, 37);
			this.SketchCopyImage.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.SketchCopyImage.BackColor = System.Drawing.SystemColors.Control;
			//
			// SSTab1_Page6
			//
			this.SSTab1_Page6.Controls.Add(this.chkWavFile);
			this.SSTab1_Page6.Controls.Add(this.MMControl1);
			this.SSTab1_Page6.Name = "SSTab1_Page6";
			this.SSTab1_Page6.Size = new System.Drawing.Size(618, 479);
			this.SSTab1_Page6.Text = "Audio Note";
			//
			// chkWavFile
			//
			this.chkWavFile.Name = "chkWavFile";
			this.chkWavFile.TabIndex = 41;
			this.chkWavFile.Location = new System.Drawing.Point(220, 75);
			this.chkWavFile.Size = new System.Drawing.Size(207, 21);
			this.chkWavFile.Text = "Import Audio Note";
			//this.chkWavFile.BackColor = System.Drawing.SystemColors.Control;
			this.chkWavFile.Checked = true;
			this.chkWavFile.CheckState = Wisej.Web.CheckState.Checked;
			//
			// MMControl1
			//
			this.MMControl1.Name = "MMControl1";
			this.MMControl1.TabIndex = 49;
			this.MMControl1.Location = new System.Drawing.Point(204, 186);
			this.MMControl1.Size = new System.Drawing.Size(190, 49);
			this.MMControl1.PlayEnabled = true;
			this.MMControl1.PauseEnabled = true;
			this.MMControl1.PrevVisible = false;
			this.MMControl1.NextVisible = false;
			this.MMControl1.BackVisible = false;
			this.MMControl1.StepVisible = false;
			this.MMControl1.EjectVisible = false;
			this.MMControl1.DeviceType = "";
			//this.MMControl1.PlayCompleted += new AxMCI.DmciEvents_PlayCompletedEventHandler(this.MMControl1_PlayCompleted);
			//this.MMControl1.RecordClick += new AxMCI.DmciEvents_RecordClickEventHandler(this.MMControl1_RecordClick);
			//this.MMControl1.RecordCompleted += new AxMCI.DmciEvents_RecordCompletedEventHandler(this.MMControl1_RecordCompleted);
			//this.MMControl1.StopCompleted += new AxMCI.DmciEvents_StopCompletedEventHandler(this.MMControl1_StopCompleted);
			//
			// vsElasticLight1
			//
			//this.vsElasticLight1.Location = new System.Drawing.Point(437, 27);
			//this.vsElasticLight1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsElasticLight1.OcxState")));
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveExit,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuSaveExit
			//
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			//
			// mnuSepar1
			//
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile
			});
			//
			// frmImportChanges
			//
			this.ClientSize = new System.Drawing.Size(622, 526);
			this.ClientArea.Controls.Add(this.SSTab1);
			//this.ClientArea.Controls.Add(this.vsElasticLight1);
			this.Menu = this.MainMenu1;
			this.Name = "frmImportChanges";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmImportChanges.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Load += new System.EventHandler(this.frmImportChanges_Load);
			this.MouseUp += new Wisej.Web.MouseEventHandler(this.frmImportChanges_MouseUp);
			this.Resize += new System.EventHandler(this.frmImportChanges_Resize);
			this.Text = "Changes";
			//this.Label8.SetIndex( Label8_0, FCConvert.ToInt16( 0 ) );
			//((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			this.GridLand.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridLand)).EndInit();
			this.GridMisc.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridMisc)).EndInit();
			this.SSTab1_Page1.ResumeLayout(false);
			this.GridComm2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridComm2)).EndInit();
			this.GridComm1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridComm1)).EndInit();
			this.framCommercial.ResumeLayout(false);
			this.GridDwelling1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling1)).EndInit();
			this.framDwelling.ResumeLayout(false);
			this.SSTab1_Page2.ResumeLayout(false);
			this.GridOutbuilding.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridOutbuilding)).EndInit();
			this.SSTab1_Page3.ResumeLayout(false);
			this.GridPics.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridPics)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.GridNewPics.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridNewPics)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.framImage.ResumeLayout(false);
			this.SSTab1_Page4.ResumeLayout(false);
			this.framSketchDragIcon.ResumeLayout(false);
			this.GridNewSketches.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridNewSketches)).EndInit();
			this.framSketchMenu.ResumeLayout(false);
			this.VSFlexGrid1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).EndInit();
			this.VSFlexGrid2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid2)).EndInit();
			this.GridSketches.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridSketches)).EndInit();
			this.framSketch.ResumeLayout(false);
			this.SSTab1_Page5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.MMControl1)).EndInit();
			this.SSTab1_Page6.ResumeLayout(false);
			this.SSTab1.ResumeLayout(false);
			//((System.ComponentModel.ISupportInitialize)(this.vsElasticLight1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
