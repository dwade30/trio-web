﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using Wisej.Web;
using System.IO;
using System.Linq;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.RealEstate;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rpt1PagePropertyCard.
	/// </summary>
	public partial class rpt1PagePropertyCard : BaseSectionReport
	{
		public static rpt1PagePropertyCard InstancePtr
		{
			get
			{
				return (rpt1PagePropertyCard)Sys.GetInstance(typeof(rpt1PagePropertyCard));
			}
		}

		protected rpt1PagePropertyCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsExemption?.Dispose();
                rsExemption = null;
                partyCont = null;
				
            }
			base.Dispose(disposing);
		}

		public rpt1PagePropertyCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Property Card";
		}
		// nObj = 1
		//   0	rpt1PagePropertyCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrintTest;
		private int lngCurrentAccount;
		private int intCurrentCard;
		private FCCollection LandList = new FCCollection();
		private FCCollection BldgList = new FCCollection();
		private bool boolReset;
		private int NumRecords;
		private bool boolHidePics;
		private clsDRWrapper rsExemption = new clsDRWrapper();
		private bool boolNotByBatch;
        private cPartyController partyCont = new cPartyController();
		public FCCollection BldgSummary
		{
			get
			{
				FCCollection BldgSummary = null;
				BldgSummary = BldgList;
				return BldgSummary;
			}
		}

		public FCCollection LandSummary
		{
			get
			{
				FCCollection LandSummary = null;
				LandSummary = LandList;
				return LandSummary;
			}
		}

		public bool PrintTest
		{
			get
			{
				bool PrintTest = false;
				PrintTest = boolPrintTest;
				return PrintTest;
			}
		}

		public int CurrentAccount
		{
			get
			{
				int CurrentAccount = 0;
				CurrentAccount = lngCurrentAccount;
				return CurrentAccount;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short CurrentCard
		{
			get
			{
				short CurrentCard = 0;
				CurrentCard = FCConvert.ToInt16(intCurrentCard);
				return CurrentCard;
			}
		}

		public void Init(bool boolModal, bool boolTestPrint, bool modalDialog)
		{
			boolPrintTest = boolTestPrint;
			boolNotByBatch = true;
			if (!boolModal)
			{
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", true, showModal: modalDialog);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", true, showModal: modalDialog);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// VB6 Bad Scope Dim:
			int x = 0;
			eArgs.EOF = modDataTypes.Statics.MR.EndOfFile();
			if (!eArgs.EOF)
			{
				// If MR.Fields("rscard") > 1 Then GoTo SkipCalc
				if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
				{
					modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
					txtAccount.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
					txtAccountName.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("DeedName1"));
					FillPicAndSketch(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), Guid.Parse(modDataTypes.Statics.MR.Get_Fields_String("cardId")));
					string[] straddr = new string[4 + 1];
					double dblLand = 0;
					double dblBldg = 0;
					double dblExemption = 0;
					double dblAcres = 0;
					// vbPorter upgrade warning: dblTax As double	OnWrite(short, string)
					double dblTax = 0;
					string strMapLot = "";
					string strMapList = "";
					strMapLot = "";
					dblTax = 0;
					dblLand = 0;
					dblBldg = 0;
					dblAcres = 0;
					dblExemption = 0;
					double[] dblExemptVal = new double[3 + 1];
					dblExemptVal[1] = 0;
					dblExemptVal[2] = 0;
					dblExemptVal[3] = 0;
					x = 0;
                    var tAddr = partyCont.GetPrimaryAddressByPartyID(
                        modDataTypes.Statics.MR.Get_Fields_Int32("OwnerPartyID"));
                    if (tAddr != null)
                    {


                        if (Strings.Trim(tAddr.Address1) !=
                            "")
                        {
                            x += 1;
                            straddr[x] = tAddr.Address1;
                        }

                        if (Strings.Trim(tAddr.Address2) !=
                            "")
                        {
                            x += 1;
                            straddr[x] = tAddr.Address2;
                        }

                        if (tAddr.Address3.Trim() != "")
                        {
                            x += 1;
                            straddr[x] = tAddr.Address3;
                        }
                        x += 1;
                        straddr[x] = tAddr.Address3 + ", " +
                                     tAddr.State + " " +
                                     tAddr.Zip;
                        if (Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rszip4"))) != "")
                        {
                            straddr[x] += " " + modDataTypes.Statics.MR.Get_Fields_String("rszip4");
                        }
                    }

                    txtAddr1.Text = straddr[1];
					txtAddr2.Text = straddr[2];
					txtAddr3.Text = straddr[3];
                    txtAddr4.Text = straddr[4];
					txtLocation.Text = Strings.Trim(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocapt")) + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocstreet"));
					dblExemptVal[1] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("exemptval1"));
					dblExemptVal[2] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("exemptval2"));
					dblExemptVal[3] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("exemptval3"));
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd1")) > 0)
					{
						txtExempt1Value.Text = Strings.Format(dblExemptVal[1], "#,###,##0");
						// If rsExemption.FindFirstRecord("Code", Val(MR.Fields("riexemptcd1"))) Then
						if (rsExemption.FindFirst("code = " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd1")))))
						{
							lblExempt1.Text = rsExemption.Get_Fields_String("description") + ":";
						}
						else
						{
							lblExempt1.Text = "Exemption 1:";
						}
					}
					else
					{
						txtExempt1Value.Text = "";
						lblExempt1.Text = "";
					}
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd2")) > 0)
					{
						txtExempt2Value.Text = Strings.Format(dblExemptVal[2], "#,###,##0");
						// If rsExemption.FindFirstRecord("Code", Val(MR.Fields("riexemptcd2"))) Then
						if (rsExemption.FindFirst("code = " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd2")))))
						{
							lblExempt2.Text = rsExemption.Get_Fields_String("description") + ":";
						}
						else
						{
							lblExempt2.Text = "Exemption 2:";
						}
					}
					else
					{
						txtExempt2Value.Text = "";
						lblExempt2.Text = "";
					}
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd3")) > 0)
					{
						txtExempt3Value.Text = Strings.Format(dblExemptVal[3], "#,###,##0");
						// If rsExemption.FindFirstRecord("Code", Val(MR.Fields("riexemptcd3"))) Then
						if (rsExemption.FindFirst("code = " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd3")))))
						{
							lblExempt3.Text = rsExemption.Get_Fields_String("Description") + ":";
						}
						else
						{
							lblExempt3.Text = "Exemption 3:";
						}
					}
					else
					{
						txtExempt3Value.Text = "";
						lblExempt3.Text = "";
					}
					txtBook.Text = modGlobalRoutines.GetCurrentBookString(modGlobalVariables.Statics.gintLastAccountNumber);
					txtPage.Text = modGlobalRoutines.GetCurrentPageString(modGlobalVariables.Statics.gintLastAccountNumber);
					txtMapLot.Text = modDataTypes.Statics.MR.Get_Fields_String("rsmaplot");
					modREASValuations.Statics.BBPhysicalPercent = 0;
					modREASValuations.Statics.HoldDwellingEconomic = 0;
					ClearLists();
					while (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount")) == modGlobalVariables.Statics.gintLastAccountNumber)
					{
						modGlobalVariables.Statics.boolfromcalcandassessment = true;
						modGlobalVariables.Statics.boolSeparateLandMethod = true;
						modDataTypes.Statics.MR.Edit();
						modSpeedCalc.CalcCard();
						modGlobalVariables.Statics.boolSeparateLandMethod = false;
						modGlobalVariables.Statics.boolfromcalcandassessment = false;
						FillLandData(modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
						FillBldgData(modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
						dblLand += modProperty.Statics.lngCorrelatedLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")];
						dblBldg += modProperty.Statics.lngCorrelatedBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")];
						dblAcres += modDataTypes.Statics.MR.Get_Fields_Double("piacres");
						modDataTypes.Statics.MR.MoveNext();
						if (modDataTypes.Statics.MR.EndOfFile())
							break;
					}
					txtLandValue.Text = Strings.Format(dblLand, "#,###,###,##0");
					txtBldgValue.Text = Strings.Format(dblBldg, "#,###,###,##0");
					// vbPorter upgrade warning: dblAssess As double	OnWrite(string)
					double dblAssess = 0;
					dblExemption = dblExemptVal[1] + dblExemptVal[2] + dblExemptVal[3];
					if (dblExemption > dblLand + dblBldg)
					{
						dblExemption = dblAssess;
					}
					dblAssess = FCConvert.ToDouble(Strings.Format(dblLand + dblBldg - dblExemption, "0"));
					txtAssessment.Text = Strings.Format(dblAssess, "#,###,###,##0");
					txtAcreage.Text = Strings.Format(dblAcres, "#,###,##0.00");
					dblTax = FCConvert.ToDouble(Strings.Format(dblAssess * (modGlobalVariables.Statics.CustomizedInfo.BillRate / 1000), "0.00"));
					txtTax.Text = Strings.Format(dblTax, "#,###,###,##0.00");
				}
			}
		}

		private void FillBldgData(int intcnum)
		{
			int x;
			clsBldgSummary tBldg;
			int intTemp = 0;
			string vbPorterVar = modSpeedCalc.Statics.CalcPropertyList[intcnum].DwellingOrCommercial;
			//FC:FINAL:RPU:#i1029 - initialize structure fields through constructor
			for (int i = 0; i < modSpeedCalc.Statics.CalcOutIndex + 1; i++)
			{
				modSpeedCalc.Statics.CalcOutList[i] = new modGlobalVariables.CalcOutBuildingType(1);
			}
			//FC:FINAL:RPU:#i1029 - initialize structure fields through constructor
			for (int i = 0; i < modSpeedCalc.Statics.CalcDwellIndex + 1; i++)
			{
				modSpeedCalc.Statics.CalcDwellList[i] = new modGlobalVariables.CalcDwellingType(1);
			}
			modDwelling.Speed_DWELLING_DATA();
			modOutbuilding.Speed_OUTBUILDING_DATA();
			if (vbPorterVar == "Y")
			{
				tBldg = new clsBldgSummary();
				tBldg.Description = modSpeedCalc.Statics.CalcDwellList[intcnum].BuildingType;
				// tBldg.Stories = .Stories
				tBldg.Stories = Strings.Trim(modSpeedCalc.Statics.CalcDwellList[intcnum].StoriesShort);
				tBldg.Area = modSpeedCalc.Statics.CalcDwellList[intcnum].Sqft;
				tBldg.Condition = modSpeedCalc.Statics.CalcDwellList[intcnum].Condition;
				tBldg.Grade = Strings.Trim(modSpeedCalc.Statics.CalcDwellList[intcnum].Grade.Replace("Grade", ""));
				// tBldg.BldgValue = .Total
				tBldg.BldgValue = modSpeedCalc.Statics.CalcDwellList[intcnum].DwellValue;
				tBldg.Phys = modSpeedCalc.Statics.CalcDwellList[intcnum].PhysPerc;
				tBldg.Func = modSpeedCalc.Statics.CalcDwellList[intcnum].FuncPerc;
				tBldg.Econ = modSpeedCalc.Statics.CalcDwellList[intcnum].EconPerc;
				BldgList.Add(tBldg);
			}
			else if (vbPorterVar == "C")
			{
				for (x = 1; x <= 2; x++)
				{
					if (Strings.Trim(modSpeedCalc.Statics.CalcCommList[intcnum].OccupancyType[x]) != "")
					{
						tBldg = new clsBldgSummary();
						tBldg.Description = modSpeedCalc.Statics.CalcCommList[intcnum].OccupancyType[x].Replace(".", "");
						tBldg.Stories = modSpeedCalc.Statics.CalcCommList[intcnum].Stories[x];
						tBldg.BldgValue = modSpeedCalc.Statics.CalcCommList[intcnum].subtotal[x];
						tBldg.Condition = modSpeedCalc.Statics.CalcCommList[intcnum].Condition[x];
						tBldg.Econ = FCConvert.ToString(FCConvert.ToDouble(modSpeedCalc.Statics.CalcCommList[intcnum].EconFactor) * 100) + "%";
						tBldg.Func = FCConvert.ToString(FCConvert.ToDouble(modSpeedCalc.Statics.CalcCommList[intcnum].Functional[x]) * 100) + "%";
						if (Strings.Trim(modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x]) != "")
						{
							modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x] = modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x].Replace("..", ".");
							modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x] = modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x].Replace("..", ".");
							modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x] = modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x].Replace("..", ".");
							modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x] = modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x].Replace("..", ".");
							modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x] = modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x].Replace("..", ".");
							intTemp = Strings.InStr(1, modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x], ".", CompareConstants.vbBinaryCompare);
							if (intTemp < modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x].Length)
							{
								tBldg.Grade = Strings.Mid(modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[x], intTemp + 1);
							}
						}
						// tBldg.Grade = .ClassQuality(x)
						tBldg.Phys = FCConvert.ToString(FCConvert.ToDouble(modSpeedCalc.Statics.CalcCommList[intcnum].Physical[x]) * 100) + "%";
						BldgList.Add(tBldg);
					}
				}
				// x
			}
			for (x = 1; x <= 10; x++)
			{
				if (Strings.Trim(modSpeedCalc.Statics.CalcOutList[intcnum].Description[x]) != "")
				{
					tBldg = new clsBldgSummary();
					tBldg.Area = modSpeedCalc.Statics.CalcOutList[intcnum].Units[x];
					tBldg.BldgValue = modSpeedCalc.Statics.CalcOutList[intcnum].OutbuildingValue[x];
					tBldg.Condition = modSpeedCalc.Statics.CalcOutList[intcnum].Condition[x];
					tBldg.Description = modSpeedCalc.Statics.CalcOutList[intcnum].Description[x];
					tBldg.Econ = modSpeedCalc.Statics.CalcOutList[intcnum].Economic[x];
					tBldg.Func = modSpeedCalc.Statics.CalcOutList[intcnum].Functional[x];
					tBldg.Grade = modSpeedCalc.Statics.CalcOutList[intcnum].Grade[x];
					tBldg.Phys = modSpeedCalc.Statics.CalcOutList[intcnum].Physcial[x];
					tBldg.Stories = "";
					BldgList.Add(tBldg);
				}
			}
			// x
		}

		private void FillLandData(int intcnum)
		{
			int x;
			clsLandSummary tLand;
			for (x = 1; x <= 7; x++)
			{
				if (modSpeedCalc.Statics.CalcPropertyList[intcnum].LandType[x] > 0)
				{
					tLand = new clsLandSummary();
					// If Trim(.LandDesc(x)) <> vbNullString Then
					if (modSpeedCalc.Statics.CalcPropertyList[intcnum].LandType[x] != 99)
					{
						tLand.Factor = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandFctr[x];
						tLand.Description = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandDesc[x];
						tLand.Units = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandUnits[x];
						tLand.LandValue = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandValue[x];
						tLand.Total = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandTotal[x];
						tLand.Method = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandMethod[x];
					}
					else
					{
						tLand.Factor = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandFctr[x];
						tLand.LandValue = modSpeedCalc.Statics.CalcPropertyList[intcnum].LandValue[x];
					}
					LandList.Add(tLand);
				}
			}
			// x
		}

		private void ClearLists()
		{
			int x;
			for (x = BldgList.Count; x >= 1; x--)
			{
				BldgList.Remove(x);
			}
			// x
			for (x = LandList.Count; x >= 1; x--)
			{
				LandList.Remove(x);
			}
			// x
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (boolNotByBatch)
			{
				if (modSpeedCalc.Statics.boolCalcErrors)
				{
					MessageBox.Show("There were errors calculating.  Following is a list.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					//! Load frmShowResults;
					frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
					frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
					frmShowResults.InstancePtr.Show();
				}
				modSpeedCalc.Statics.boolCalcErrors = false;
				modSpeedCalc.Statics.CalcLog = "";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strMasterJoin;
			txtDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			//strMasterJoin = modREMain.GetMasterJoin();
			rsExemption.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
			if (!boolPrintTest)
			{
				if (boolNotByBatch)
				{
					modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
					modProperty.Statics.RUNTYPE = "D";
					modDataTypes.Statics.MR.OpenRecordset(
                         "select * from master where rsaccount = " +
                         FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by rscard",
                         modGlobalVariables.strREDatabase);
					//modDataTypes.Statics.MR.OpenRecordset(strMasterJoin + " where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by rscard", modGlobalVariables.strREDatabase);
					if (modDataTypes.Statics.MR.EndOfFile())
					{
						MessageBox.Show("No Matches found", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						this.Close();
						return;
					}
					modDataTypes.Statics.MR.MoveLast();
					modDataTypes.Statics.MR.MoveFirst();
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding order by rsaccount,rscard", modGlobalVariables.strREDatabase);
						modDataTypes.Statics.CMR.OpenRecordset("select * from commercial order by rsaccount,rscard", modGlobalVariables.strREDatabase);
						modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling order by rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						modDataTypes.Statics.OUT.OpenRecordset("select * from sroutbuilding where saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
						modDataTypes.Statics.CMR.OpenRecordset("select * from srcommercial where saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
						modDataTypes.Statics.DWL.OpenRecordset("select * from srdwelling where saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
				}
				modDataTypes.Statics.MR.Edit();
				NumRecords = modDataTypes.Statics.MR.RecordCount();
				Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
				modSpeedCalc.Statics.SummaryListIndex = 0;
				Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = NumRecords + 1;
				modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			}
			else
			{
				clsLandSummary tLand = new clsLandSummary();
				tLand.Acreage = "23.50";
				tLand.Units = "23.50";
				tLand.Description = "Soft Wood";
				tLand.Total = "2,600";
				tLand.Factor = "100%";
				tLand.LandValue = "2,600";
				LandList.Add(tLand);
				tLand = new clsLandSummary();
				tLand.Acreage = "1.00";
				tLand.Units = "1.00";
				tLand.Description = "Lot Improvements";
				tLand.Factor = "100%";
				tLand.Total = "12,000";
				tLand.LandValue = "12,000";
				LandList.Add(tLand);
				clsBldgSummary tBldg = new clsBldgSummary();
				tBldg.BldgValue = "115,000";
				tBldg.Description = "Raised Ranch";
				tBldg.Func = "100%";
				tBldg.Econ = "100%";
				tBldg.Condition = "Average";
				tBldg.Phys = "100%";
				tBldg.Area = "1,235";
				tBldg.Stories = "2";
				tBldg.Grade = "C 100";
				BldgList.Add(tBldg);
				tBldg = new clsBldgSummary();
				tBldg.BldgValue = "20,000";
				tBldg.Description = "Garage";
				tBldg.Func = "100%";
				tBldg.Econ = "100%";
				tBldg.Phys = "100%";
				tBldg.Area = "500";
				tBldg.Condition = "Average";
				tBldg.Stories = "1";
				tBldg.Grade = "C 100";
				BldgList.Add(tBldg);
			}
			SubReport1.Report = srpt1CardLand.InstancePtr;
			SubReport2.Report = srpt1CardBldg.InstancePtr;
		}
		//
		// Private Sub ActiveReport_Terminate()
		// If Not boolReset Then
		// ResetRecords
		// End If
		// End Sub
		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolPrintTest)
			{
			}
			else
			{
				txtAccount.Text = "115";
				txtAccountName.Text = "Smith, John & Jane";
				txtMapLot.Text = "010-129";
				txtLandValue.Text = "54,000";
				txtBldgValue.Text = "215,000";
				lblExempt1.Text = "Veteran:";
				lblExempt2.Text = "Homestead:";
				lblExempt3.Text = "Blind:";
				txtExempt1Value.Text = "6,000";
				txtExempt2Value.Text = "10,000";
				txtExempt3Value.Text = "4,000";
				txtLocation.Text = "125 Main Road";
				txtAddr1.Text = "P.O. Box 119";
				txtAddr2.Text = "Testville, ME 04000";
				txtBook.Text = "125" + "\r\n" + "130" + "\r\n" + "131";
				txtPage.Text = "55" + "\r\n" + "394" + "\r\n" + "598";
				txtAssessment.Text = "249,000";
				txtTax.Text = "4,357.50";
			}
		}

		private void FillPicAndSketch( int lngAccount, Guid cardIdentifier)
		{
			int intNumPics;
			// max number of pictures to draw
			intNumPics = 3;
			int intMaxPics = 0;
			string strTemp = "";
			int x;
			FCCollection colPics = new FCCollection();
			string[] strAry = null;
			string strSketchPath = "";
			if (!boolHidePics)
            {
                int numSketches = 0;
                var sketchInfos = StaticSettings.GlobalCommandDispatcher.Send(new GetSketchInfos(cardIdentifier)).Result.ToList();
				int intCurPic = 0;
				intCurPic = 0;
				if (sketchInfos.Any())
				{
					intMaxPics = intNumPics - 1;
				}
				else
				{
					intMaxPics = intNumPics;
				}

                using (clsDRWrapper rsTemp = new clsDRWrapper())
                {
                    rsTemp.OpenRecordset(
                        "Select * from picturerecord where mraccountnumber = " + FCConvert.ToString(lngAccount) +
                        " order by picnum,RScard", modGlobalVariables.strREDatabase);
                    while (!rsTemp.EndOfFile() && intCurPic < intMaxPics)
                    {
                        //strTemp = modGlobalRoutines.GetFullPictureName_2(rsTemp.Get_Fields_String("picturelocation"));
                        strTemp = rsTemp.Get_Fields_Guid("DocumentIdentifier").ToString();
                        if (strTemp != string.Empty)
                        {
                            intCurPic += 1;
                            colPics.Add(strTemp);
                        }

                        rsTemp.MoveNext();
                    }
                }

                if (sketchInfos.Any())
                {
                     numSketches = intNumPics - intCurPic;
				}
				if (colPics.Count >= 1)
                {
                    var docGuid = Guid.Parse(colPics[1]);
                    var centralDocument = StaticSettings.GlobalCommandDispatcher.Send(new GetCentralDocumentByIdentifier(docGuid))
                        .Result;
                    if (centralDocument != null)
                    {
                        Image1.Image = FCUtils.PictureFromBytes(centralDocument.ItemData);
                    }
                    else
                    {
                        Image1.Image = null;
                    }
                    
					if (colPics.Count >= 2)
					{
                        docGuid = Guid.Parse(colPics[2]);
                        centralDocument = StaticSettings.GlobalCommandDispatcher.Send(new GetCentralDocumentByIdentifier(docGuid))
                            .Result;
                        if (centralDocument != null)
                        {
                            Image2.Image = FCUtils.PictureFromBytes(centralDocument.ItemData);
                        }
                        else
                        {
                            Image2.Image = null;
                        }
                        
						if (colPics.Count >= 3)
						{
                            docGuid = Guid.Parse(colPics[3]);
                            centralDocument = StaticSettings.GlobalCommandDispatcher.Send(new GetCentralDocumentByIdentifier(docGuid))
                                .Result;
                            if (centralDocument != null)
                            {
                                Image3.Image = FCUtils.PictureFromBytes(centralDocument.ItemData);
                            }
                            else
                            {
                                Image3.Image = null;
                            }
                        }
						else
						{
							Image3.Image = null;
						}
					}
					else
					{
						Image2.Image = null;
						Image3.Image = null;
					}
				}
				else
				{
					Image1.Image = null;
					Image2.Image = null;
					Image3.Image = null;
				}

                var imageIndex = 1;
                var width = Image1.Width;
                var height = Image1.Height;
                while (numSketches > 0)
                {
                    if (sketchInfos.Count >= imageIndex)
                    {
                        var sketchId = sketchInfos[imageIndex - 1].Id;
                        var image = FCUtils.PictureFromBytes(StaticSettings.GlobalCommandDispatcher.Send(new GetSketchImage(sketchId)).Result,width,height);
                        switch (imageIndex)
                        {
                            case 1:
                                Image1.Image = image;
                                break;
                            case 2:
                                Image2.Image = image;
                                break;
                            case 3:
                                Image3.Image = image;
                                break;
                        }

                        imageIndex++;
                    }
                    else
                    {
                        break;
                    }
                    numSketches--;
                }
            }
			else
			{
				Image1.Image = null;
				Image2.Image = null;
				Image3.Image = null;
			}

            
		}

		
	}
}
