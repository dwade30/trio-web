﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cRELandItem
	{
		//=========================================================
		private int intLandNumber;
		private int intLandCode;
		private string strUnitsA = string.Empty;
		private string strUnitsB = string.Empty;
		private double dblInfluence;
		private int intInfluenceCode;
		private bool boolUpdated;
		private bool boolDeleted;

		public short LandNumber
		{
			set
			{
				intLandNumber = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short LandNumber = 0;
				LandNumber = FCConvert.ToInt16(intLandNumber);
				return LandNumber;
			}
		}

		public short LandCode
		{
			set
			{
				intLandCode = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short LandCode = 0;
				LandCode = FCConvert.ToInt16(intLandCode);
				return LandCode;
			}
		}

		public string UnitsA
		{
			set
			{
				strUnitsA = value;
				IsUpdated = true;
			}
			get
			{
				string UnitsA = "";
				UnitsA = strUnitsA;
				return UnitsA;
			}
		}

		public string UnitsB
		{
			set
			{
				strUnitsB = value;
				IsUpdated = true;
			}
			get
			{
				string UnitsB = "";
				UnitsB = strUnitsB;
				return UnitsB;
			}
		}

		public double Influence
		{
			set
			{
				dblInfluence = value;
				IsUpdated = true;
			}
			get
			{
				double Influence = 0;
				Influence = dblInfluence;
				return Influence;
			}
		}

		public short InfluenceCode
		{
			set
			{
				intInfluenceCode = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short InfluenceCode = 0;
				InfluenceCode = FCConvert.ToInt16(intInfluenceCode);
				return InfluenceCode;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
		}
	}
}
