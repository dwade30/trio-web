﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPictures.
	/// </summary>
	partial class rptPictures
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPictures));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFileorName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFile = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFileorName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtMapLot,
				this.txtFile,
				this.txtCard,
				this.txtLocation
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.lblTitle,
				this.Label2,
				this.Label3,
				this.lblFileorName,
				this.Label5,
				this.lblLocation
			});
			this.PageHeader.Height = 0.9375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.25F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Account Photo Report";
			this.lblTitle.Top = 0.03125F;
			this.lblTitle.Width = 4.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "";
			this.Label2.Text = "Account";
			this.Label2.Top = 0.6875F;
			this.Label2.Width = 0.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "";
			this.Label3.Text = "Map Lot";
			this.Label3.Top = 0.6875F;
			this.Label3.Width = 1.25F;
			// 
			// lblFileorName
			// 
			this.lblFileorName.Height = 0.1875F;
			this.lblFileorName.HyperLink = null;
			this.lblFileorName.Left = 2.3125F;
			this.lblFileorName.Name = "lblFileorName";
			this.lblFileorName.Style = "";
			this.lblFileorName.Text = "File";
			this.lblFileorName.Top = 0.6875F;
			this.lblFileorName.Width = 0.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "";
			this.Label5.Text = "Card";
			this.Label5.Top = 0.6875F;
			this.Label5.Width = 0.375F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 5.4375F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "";
			this.lblLocation.Text = "Location";
			this.lblLocation.Top = 0.6875F;
			this.lblLocation.Visible = false;
			this.lblLocation.Width = 1.9375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.625F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 1F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.25F;
			// 
			// txtFile
			// 
			this.txtFile.Height = 0.1875F;
			this.txtFile.Left = 2.3125F;
			this.txtFile.Name = "txtFile";
			this.txtFile.Text = null;
			this.txtFile.Top = 0F;
			this.txtFile.Width = 5.0625F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.1875F;
			this.txtCard.Left = 0.625F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Text = null;
			this.txtCard.Top = 0F;
			this.txtCard.Width = 0.375F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 5.4375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0F;
			this.txtLocation.Visible = false;
			this.txtLocation.Width = 2F;
			// 
			// rptPictures
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFileorName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFile;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFileorName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
