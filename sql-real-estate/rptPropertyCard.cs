﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using Wisej.Web;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPropertyCard.
	/// </summary>
	public partial class rptPropertyCard : BaseSectionReport
	{
		public static rptPropertyCard InstancePtr
		{
			get
			{
				return (rptPropertyCard)Sys.GetInstance(typeof(rptPropertyCard));
			}
		}

		protected rptPropertyCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCost_AutoInitialized?.Dispose();
				clsProp_AutoInitialized?.Dispose();
                clsCost_AutoInitialized = null;
                clsProp_AutoInitialized = null;
            }
			base.Dispose(disposing);
		}

		public rptPropertyCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Property Card";
		}
		// nObj = 1
		//   0	rptPropertyCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper clsCost = new clsDRWrapper();
			public clsDRWrapper clsCost_AutoInitialized = null;
			public clsDRWrapper clsCost
			{
				get
				{
					if ( clsCost_AutoInitialized == null)
					{
						 clsCost_AutoInitialized = new clsDRWrapper();
					}
					return clsCost_AutoInitialized;
				}
				set
				{
					 clsCost_AutoInitialized = value;
				}
			}
			public clsDRWrapper clsProp_AutoInitialized = null;
			public clsDRWrapper clsProp
			{
				get
				{
					if ( clsProp_AutoInitialized == null)
					{
						 clsProp_AutoInitialized = new clsDRWrapper();
					}
					return clsProp_AutoInitialized;
				}
				set
				{
					 clsProp_AutoInitialized = value;
				}
			}
		private bool boolHidePics;
		string strPropSQL = "";
		bool boolFirstTime;
		

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolFirstTime)
			{
				clsProp.MoveNext();
			}
			else
			{
				boolFirstTime = false;
			}
			eArgs.EOF = clsProp.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{

		}

		public bool ShowPictures
		{
			get
			{
				bool ShowPictures = false;
				ShowPictures = !boolHidePics;
				return ShowPictures;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int const_printtoolid;
			int cnt;
			boolFirstTime = true;
			clsProp.OpenRecordset(strPropSQL, modGlobalVariables.strREDatabase);
			if (clsProp.EndOfFile())
			{
				//Application.DoEvents();
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			modGlobalVariables.Statics.LandTypes.Init();
			// override the print button so we can control it
			const_printtoolid = 9950;

			clsCost.OpenRecordset("Select * from costrecord order by crecordnumber", modGlobalVariables.strREDatabase);
			SubReport1.Report = new srptPropertyCard1();
			SubReport2.Report = new srptPropertyCard2();
			(SubReport2.Report as srptPropertyCard2).ShowPics = !boolHidePics;
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolRange, short intType, string strStart = "", string strEnd = "", bool boolFromExtract = false, bool boolSingleCard = false)
		{
			// intType tells what to order by and what the range is of
			string strOrderBy = "";
			string strWhere;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsProp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			boolHidePics = false;
			strWhere = "where not rsdeleted = 1 ";
			if (boolFromExtract)
			{
				if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
				{
					boolHidePics = true;
				}
			}
			switch (intType)
			{
				case 1:
					{
						// account
						strOrderBy = " Order by rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rsaccount between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd));
							if (Conversion.Val(strStart) != Conversion.Val(strEnd))
							{
								if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
								{
									boolHidePics = true;
								}
							}
						}
						else if (boolSingleCard)
						{
							strWhere += " and rsaccount = " + FCConvert.ToString(Conversion.Val(strStart)) + " and rscard = " + FCConvert.ToString(Conversion.Val(strEnd));
						}
						else if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
				case 2:
					{
						// name
						strOrderBy = " order by rsname,rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rsname between '" + strStart + "' and '" + strEnd + "'";
						}
						if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
				case 3:
					{
						// maplot
						strOrderBy = " order by rsmaplot,rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rsmaplot between '" + strStart + "' and '" + strEnd + "'";
						}
						if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
				case 4:
					{
						// location
						strOrderBy = " order by rslocstreet,rslocnumalph ,rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rslocstreet between '" + strStart + "' and '" + strEnd;
						}
						if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolFromExtract)
			{
				strPropSQL = "select master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and master.rsdeleted = 0 " + strOrderBy;
			}
			else
			{
				// strPropSQL = "Select * from master " & strWhere & strOrderBy
				strPropSQL = strMasterJoin + strWhere + strOrderBy;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, true, true, "Cards", false, "", "TRIO Software", false, true, "PropertyCard");
			// Me.Show , MDIParent
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}

		private void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			SubReport1.Report.UserData = FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rsaccount"))) + "," + FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rscard")));
			SubReport2.Report.UserData = FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rsaccount"))) + "," + FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rscard")));
		}

		
	}
}
