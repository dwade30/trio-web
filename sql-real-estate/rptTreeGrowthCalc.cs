﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTreeGrowthCalc.
	/// </summary>
	public partial class rptTreeGrowthCalc : BaseSectionReport
	{
		public static rptTreeGrowthCalc InstancePtr
		{
			get
			{
				return (rptTreeGrowthCalc)Sys.GetInstance(typeof(rptTreeGrowthCalc));
			}
		}

		protected rptTreeGrowthCalc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptTreeGrowthCalc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tree Growth Summary";
		}
		// nObj = 1
		//   0	rptTreeGrowthCalc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngRow == frmTreeGrowth.InstancePtr.Grid.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			if (frmTreeGrowth.InstancePtr.cmbOption.Text == "Summary report only")
			{
				lngRow = frmTreeGrowth.InstancePtr.Grid.Rows;
			}
			else
			{
				lngRow = 2;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmTreeGrowth.InstancePtr.Grid.Rows)
			{
				txtAccount.Text = FCConvert.ToString(Conversion.Val(frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 0)));
				txtName.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 1);
				txtPSoft.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 2);
				txtPMixed.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 3);
				txtPHard.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 4);
				txtSoft.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 5);
				txtMixed.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 6);
				txtHard.Text = frmTreeGrowth.InstancePtr.Grid.TextMatrix(lngRow, 7);
				lngRow += 1;
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotPSoft.Text = frmTreeGrowth.InstancePtr.TotalGrid.TextMatrix(1, 1);
			txtTotPMixed.Text = frmTreeGrowth.InstancePtr.TotalGrid.TextMatrix(1, 2);
			txtTotPHard.Text = frmTreeGrowth.InstancePtr.TotalGrid.TextMatrix(1, 3);
			txtTotSoft.Text = frmTreeGrowth.InstancePtr.TotalGrid.TextMatrix(1, 4);
			txtTotMixed.Text = frmTreeGrowth.InstancePtr.TotalGrid.TextMatrix(1, 5);
			txtTotHard.Text = frmTreeGrowth.InstancePtr.TotalGrid.TextMatrix(1, 6);
		}

		
	}
}
