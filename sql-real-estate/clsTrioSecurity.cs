//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace Global
{
	public class clsTrioSecurity
	{

		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// Date           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               01/14/2003              *
		// ********************************************************

		private string strSecurityDB;
		private string strAppName;
		private clsDRWrapper dcSecurity = new clsDRWrapper();
		int intUserID;
		private bool boolDefPermission;
		private bool boolUseSec;
		private bool boolGlobalUseSec;
		private string strComputerName = "";
		private string strUserName = "";
		private bool boolDontLogActivity;

		public clsTrioSecurity() : base()
		{
			// just sets up a default database name
			strSecurityDB = "SystemSettings"; // default. can be changed
		}

		public string GetNetworkUserName()
		{
			string GetNetworkUserName = "";
			// returns the user name logged onto this computer
			GetNetworkUserName = strUserName;
			return GetNetworkUserName;
		}

		public string GetNameOfComputer()
		{
			string GetNameOfComputer = "";
			// returns the name of the computer as seen by the network
			GetNameOfComputer = strComputerName;
			return GetNameOfComputer;
		}

		public void Init(string strModuleName, string strDBName = "SystemSettings", object strID = null, object intUser = null, bool boolDontLog = false)
		{
			// strAName is the application name.  If strdbname isnt sent it defaults to SystemSettings
			// This loads the security permissions for the current user into rssecurity
			// In some cases you might not want the permissions for the person who logged in
			// In that situation you can provide the operatorid or the username or id number and then set intuser  to show which
			// of the three you sent

			clsDRWrapper dcTemp = new clsDRWrapper();
			string strReturn = "";
			string strField = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strName;
			int lngNameLen;

			boolDontLogActivity = boolDontLog;

			lngNameLen = 1024;
			strName = Strings.StrDup(lngNameLen, " ");
			modAPIsConst.GetComputerNameWrp(ref strName, ref lngNameLen);
			if (Strings.Trim(strName).Length>0) {
				strComputerName = Strings.Replace(Strings.Trim(strName), Convert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
			}
			lngNameLen = 1024;
			strName = Strings.StrDup(lngNameLen, " ");
			modAPIsConst.GetUserNameWrp(ref strName, ref lngNameLen);
			if (Strings.Trim(strName).Length>0) {
				strUserName = Strings.Replace(Strings.Trim(strName), Convert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
			}

			if (!Information.IsNothing(strDBName)) {
				strSecurityDB = strDBName;
			} else { strSecurityDB = "SystemSettings";
			}

			strAppName = strModuleName;

			clsTemp.OpenRecordset("select * from globalvariables", strSecurityDB);
			// TODO Get_Fields: Check the table for the column [usesecurity] and replace with corresponding Get_Field method
			boolUseSec = Convert.ToString(clsTemp.Get_Fields("usesecurity"))=="Y";
			boolGlobalUseSec = boolUseSec;

			if (dcTemp.Execute("update globalvariables set defaultpermission = defaultpermission", strSecurityDB, false)) {
				boolDefPermission = Convert.ToBoolean(clsTemp.Get_Fields_Boolean("defaultpermission"));
			}


			if (Information.IsNothing(strID)) {
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "securityid", ref strReturn);
				intUserID = (int)Math.Round(Conversion.Val(strReturn+""));

				if (intUserID==-1) {
					DontUseSecurity();
					SaveActivity(strModuleName);
					return;
				}
			} else {
				if (Convert.ToString(strID)==string.Empty) {
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "securityid", ref strReturn);
					intUserID = (int)Math.Round(Conversion.Val(strReturn+""));

					if (intUserID==-1) {
						DontUseSecurity();
						SaveActivity(strModuleName);
						return;
					}
				} else {
					switch (intUser) {
						
						case 1:
						{
							intUserID = (int)Math.Round(Conversion.Val(Convert.ToString(strID)));
							break;
						}
						case 2:
						{
							strField = "username";
							dcTemp.OpenRecordset("select * from security where "+strField+" = '"+strID+"'", strSecurityDB);
							intUserID = Convert.ToInt32(dcTemp.Get_Fields_Int32("id"));
							break;
						}
						case 3:
						{
							strField = "opid";
							dcTemp.OpenRecordset("select * from security where "+strField+" = '"+strID+"'", strSecurityDB);
							intUserID = Convert.ToInt32(dcTemp.Get_Fields_Int32("id"));
							break;
						}
					} //end switch
				}
			}

			SaveActivity(strModuleName);
			if (intUserID==-1) {
				DontUseSecurity();
				return;
			}

			if (boolUseSec) {
				dcSecurity.OpenRecordset("select * from permissionstable where userid = "+Convert.ToString(intUserID)+" and modulename = '"+strAppName+"'", strSecurityDB);
				dcTemp.OpenRecordset("select * from security where id = "+Convert.ToString(intUserID), strSecurityDB);
				// TODO Get_Fields: Check the table for the column [usesecurity] and replace with corresponding Get_Field method
				boolUseSec = boolUseSec && dcTemp.Get_Fields("usesecurity");
				if (dcTemp.Execute("update globalvariables set defaultpermission = defaultpermission", strSecurityDB, false)) {
					boolDefPermission = Convert.ToBoolean(clsTemp.Get_Fields_Boolean("defaultpermission"));
				} else {
					// Call dcTemp.Execute("alter table globalvariables add column defaultpermission number", strsecuritydb, False)
				}
			}
			return;
		ErrorHandler: ;
			if (Information.Err(ex).Number==0) {
				boolDefPermission = false;
			} else {
				MessageBox.Show(" Error number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+" occured in security init function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveActivity(string strMod)
		{
			// saves the module,user and time that they logged in
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL = "";
			int intTries;

			try
			{	// On Error GoTo ErrorHandler
				if (boolDontLogActivity) return;
				AddActivityEntry_2("Start Up");
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In SaveActivitiy", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void AddActivityEntry_2(string strDescription, string strText1 = "", string strMod = "", string strDBName = "SystemSettings") { AddActivityEntry(ref strDescription, strText1, strMod, strDBName); }
		public void AddActivityEntry(ref string strDescription, string strText1 = "", string strMod = "", string strDBName = "SystemSettings")
		{
			// saves module,user, time etc
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL;
			int intTries;
			string strMd = "";

			try
			{	// On Error GoTo ErrorHandler
				if (strMod==string.Empty) {
					strMd = strAppName;
				} else {
					strMd = strMod;
				}
				intTries = 0;
			ReTry: ;
				strSQL = "Insert into activitylog (module,user,networkusername,computername,activitydate,activitytime,description,text1) values (";
				strSQL += "'"+strMd+"'";
				strSQL += ","+Convert.ToString(intUserID);
				strSQL += ",'"+GetNetworkUserName()+"'";
				strSQL += ",'"+GetNameOfComputer()+"'";
				strSQL += ",'"+Strings.Format(DateTime.Today, "MM/dd/yyyy")+"'";
				strSQL += ",'"+Convert.ToString(DateTime.Now)+"'";
				strSQL += ",'"+strDescription+"'";
				strSQL += ",'"+strText1+"'";
				strSQL += ")";
				if (!clsSave.Execute(strSQL, strDBName, false)) {

				}
				return;
			}
			catch (Exception ex)
            {	// ErrorHandler:

				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\n"+"In SaveActivity", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}



		public string CheckOtherPermissions(int lngCode, string strModuleName = "", short intUserIDNumber = 0)
		{
			string CheckOtherPermissions = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intUserIDToUse = 0;
			string strModToUse = "";


			if (!boolGlobalUseSec) {
				// noone uses security
				CheckOtherPermissions = "F";
				return CheckOtherPermissions;
			}

			if (intUserIDNumber==0) {
				intUserIDToUse = intUserID;
				if (!boolUseSec) {
					// no security for this person
					CheckOtherPermissions = "F";
					return CheckOtherPermissions;
				}
			} else {
				intUserIDToUse = intUserIDNumber;
			}

			if (intUserIDToUse==-1) {
				// super user
				CheckOtherPermissions = "F";
				return CheckOtherPermissions;
			}

			if (strModuleName==string.Empty) {
				strModToUse = strAppName;
			} else {
				strModToUse = strModuleName;
			}

			if (intUserIDToUse==intUserID && strModToUse==strAppName) {
				CheckOtherPermissions = Convert.ToString(Check_Permissions(lngCode));
				return CheckOtherPermissions;
			}

			clsLoad.OpenRecordset("select * from permissionstable where userid = "+Convert.ToString(intUserIDToUse)+" and modulename = '"+strModToUse+"' and functionid = "+Convert.ToString(lngCode), strSecurityDB);
			if (!clsLoad.EndOfFile()) {
				CheckOtherPermissions = Convert.ToString(clsLoad.Get_Fields_String("permission"));
			} else {
				if (boolDefPermission) {
					CheckOtherPermissions = "F";
				} else {
					CheckOtherPermissions = "N";
				}
			}

			return CheckOtherPermissions;
		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public object Check_Permissions(int lngCode)
		{
			object Check_Permissions = null;
			// inputs the code representing a function
			bool boolReturn;
			try
			{	// On Error GoTo ErrorHandler

				if (!boolUseSec) {
					Check_Permissions = "F";
					return Check_Permissions;
				}
				if (dcSecurity.EndOfFile() && dcSecurity.BeginningOfFile()) {
					MessageBox.Show("No permissions are defined for user "+Convert.ToString(Get_UsersUserID()), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return Check_Permissions;
				}

				// boolReturn = dcSecurity.FindFirstRecord("FunctionID", lngCode)
				boolReturn = dcSecurity.FindFirst("FunctionID = "+Convert.ToString(lngCode));
				if (boolReturn) {
					Check_Permissions = dcSecurity.GetData("Permission");
				} else {
					// The function permission is missing from the database for this user
					// send a 0 back as an error signal
					// MsgBox "Error.  Security Permissions for code " & lngCode & vbNewLine & "for user " & intUserID & " is missing." & vbNewLine & "Permission for this function cannot be given."
					// Check_Permissions = "0"
					dcSecurity.AddNew();
					dcSecurity.SetData("functionid", lngCode);
					dcSecurity.SetData("permission", boolDefPermission);
					dcSecurity.SetData("modulename", strAppName);
					dcSecurity.SetData("userid", intUserID);

					dcSecurity.Update();
					if (boolDefPermission) {
						Check_Permissions = "F";
					} else {
						Check_Permissions = "N";
					}
				}
				return Check_Permissions;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error. Security Permissions for code "+Convert.ToString(lngCode)+"\n"+"for user "+Convert.ToString(intUserID)+" is missing."+"\n"+"Permission for this function cannot be given.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Check_Permissions = "0";
			}
			return Check_Permissions;
		}

		public void Get_Children(ref clsDRWrapper Children, ref int lngParent)
		{
			// takes a dataconnection and a parent code as input
			// Then it gets all records from the database with intparent as a parent

			Children.OpenRecordset("Select * from IDTable where modulename = '"+strAppName+"' and functionid = "+Convert.ToString(lngParent), strSecurityDB);
		}

		// vbPorter upgrade warning: 'Return' As Variant --> As short	OnWrite(int)
		public short Get_UserID()
		{
			short Get_UserID = 0;
			Get_UserID = (short)intUserID;
			return Get_UserID;
		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public object Get_UserName()
		{
			object Get_UserName = null;
			clsDRWrapper dcTemp = new clsDRWrapper();

			if (intUserID==-1) {
				Get_UserName = "SuperUser";
				return Get_UserName;
			}

			dcTemp.OpenRecordset("select * from security where id = "+Convert.ToString(intUserID), strSecurityDB);
			if (!dcTemp.EndOfFile()) {
				Get_UserName = dcTemp.Get_Fields_String("username");
			} else {
				Get_UserName = "";
			}
			return Get_UserName;
		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public object Get_OpID()
		{
			object Get_OpID = null;
			clsDRWrapper dcTemp = new clsDRWrapper();

			if (intUserID==-1) {
				Get_OpID = "SuperUser";
				return Get_OpID;
			}

			dcTemp.OpenRecordset("select * from security where id = "+Convert.ToString(intUserID), strSecurityDB);
			if (!dcTemp.EndOfFile()) {
				Get_OpID = dcTemp.Get_Fields_String("opid");
			} else {
				Get_OpID = "";
			}
			return Get_OpID;
		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public object Get_UsersUserID()
		{
			object Get_UsersUserID = null;
			clsDRWrapper dcTemp = new clsDRWrapper();

			if (intUserID==-1) {
				Get_UsersUserID = "SuperUser";
				return Get_UsersUserID;
			}

			dcTemp.OpenRecordset("select * from security where id = "+Convert.ToString(intUserID), strSecurityDB);
			if (!dcTemp.EndOfFile()) {
				// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
				Get_UsersUserID = dcTemp.Get_Fields("userid");
			} else {
				Get_UsersUserID = "";
			}
			return Get_UsersUserID;
		}

		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool Using_Security()
		{
			bool Using_Security = false;
			Using_Security = boolUseSec;
			return Using_Security;
		}

		public void DontUseSecurity()
		{
			boolUseSec = false;
		}

	}
}
