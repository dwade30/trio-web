﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cExemptAssessInfo
	{
		//=========================================================
		private int lngExemptCode;
		private string strExemptDescription = string.Empty;
		private double dblExemptAmount;
		private int lngAccount;

		public int Exemptcode
		{
			set
			{
				lngExemptCode = value;
			}
			get
			{
				int Exemptcode = 0;
				Exemptcode = lngExemptCode;
				return Exemptcode;
			}
		}

		public string Description
		{
			set
			{
				strExemptDescription = value;
			}
			get
			{
				string Description = "";
				Description = strExemptDescription;
				return Description;
			}
		}

		public double Amount
		{
			set
			{
				dblExemptAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblExemptAmount;
				return Amount;
			}
		}

		public int Account
		{
			set
			{
				lngAccount = value;
			}
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}
	}
}
