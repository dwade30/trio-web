﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for sprtIncomeExpenses.
	/// </summary>
	partial class srptIncomeExpenses
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptIncomeExpenses));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPerSF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPctGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerSF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPctGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmount,
				this.txtPerSF,
				this.txtPctGross,
				this.txtCode,
				this.txtDescription
			});
			this.Detail.Height = 0.25F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5
			});
			this.ReportHeader.Height = 0.6666667F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label6,
				this.txtTotal
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.5833333F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Field1.Text = "EXPENSES";
			this.Field1.Top = 0F;
			this.Field1.Width = 2.864583F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.01041667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Code";
			this.Label1.Top = 0.46875F;
			this.Label1.Width = 0.46875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.4166667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Description";
			this.Label2.Top = 0.46875F;
			this.Label2.Width = 0.8958333F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.3333333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.052083F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: center";
			this.Label3.Text = "% Gross";
			this.Label3.Top = 0.3020833F;
			this.Label3.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.645833F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "$/SF";
			this.Label4.Top = 0.46875F;
			this.Label4.Width = 0.46875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.166667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Amount";
			this.Label5.Top = 0.46875F;
			this.Label5.Width = 0.8125F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 3.135417F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0.04166667F;
			this.txtAmount.Width = 0.8020833F;
			// 
			// txtPerSF
			// 
			this.txtPerSF.Height = 0.1875F;
			this.txtPerSF.Left = 2.645833F;
			this.txtPerSF.Name = "txtPerSF";
			this.txtPerSF.Style = "text-align: right";
			this.txtPerSF.Text = null;
			this.txtPerSF.Top = 0.04166667F;
			this.txtPerSF.Width = 0.46875F;
			// 
			// txtPctGross
			// 
			this.txtPctGross.Height = 0.1875F;
			this.txtPctGross.Left = 2.052083F;
			this.txtPctGross.Name = "txtPctGross";
			this.txtPctGross.Style = "text-align: right";
			this.txtPctGross.Text = null;
			this.txtPctGross.Top = 0.04166667F;
			this.txtPctGross.Width = 0.5520833F;
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.1875F;
			this.txtCode.Left = 0F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "text-align: left";
			this.txtCode.Text = null;
			this.txtCode.Top = 0.04166667F;
			this.txtCode.Width = 0.4270833F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 0.4166667F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "text-align: left";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.04166667F;
			this.txtDescription.Width = 1.59375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 1.666667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Total Expense";
			this.Label6.Top = 0.08333334F;
			this.Label6.Width = 1.21875F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 2.96875F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "text-align: right";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.08333334F;
			this.txtTotal.Width = 0.96875F;
			// 
			// sprtIncomeExpenses
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 3.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerSF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPctGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPerSF;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPctGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
	}
}
