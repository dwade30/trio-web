﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewValuationAnalysis.
	/// </summary>
	partial class frmNewValuationAnalysis : BaseForm
	{
		public fecherFoundation.FCTextBox txtPreviousVal;
		public fecherFoundation.FCTextBox txtCurrentVal;
		public fecherFoundation.FCTextBox txtTotalNewValuation;
		public fecherFoundation.FCFrame framDetail;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdBack;
		public FCGrid GridSavedDetail;
		public FCGrid GridDetail;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblAccount;
		public FCGrid Grid;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewValuationAnalysis));
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			this.txtPreviousVal = new fecherFoundation.FCTextBox();
			this.txtCurrentVal = new fecherFoundation.FCTextBox();
			this.txtTotalNewValuation = new fecherFoundation.FCTextBox();
			this.framDetail = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdBack = new fecherFoundation.FCButton();
			this.GridSavedDetail = new fecherFoundation.FCGrid();
			this.GridDetail = new fecherFoundation.FCGrid();
			this.lblMapLot = new fecherFoundation.FCLabel();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.Grid = new fecherFoundation.FCGrid();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framDetail)).BeginInit();
			this.framDetail.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSavedDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(748, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtPreviousVal);
			this.ClientArea.Controls.Add(this.txtCurrentVal);
			this.ClientArea.Controls.Add(this.txtTotalNewValuation);
			this.ClientArea.Controls.Add(this.framDetail);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(748, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(748, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// txtPreviousVal
			// 
			this.txtPreviousVal.AutoSize = false;
			this.txtPreviousVal.BackColor = System.Drawing.SystemColors.Window;
			this.txtPreviousVal.Enabled = false;
			this.txtPreviousVal.LinkItem = null;
			this.txtPreviousVal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPreviousVal.LinkTopic = null;
			this.txtPreviousVal.Location = new System.Drawing.Point(353, 422);
			this.txtPreviousVal.Name = "txtPreviousVal";
			this.txtPreviousVal.Size = new System.Drawing.Size(98, 40);
			this.txtPreviousVal.TabIndex = 4;
			this.txtPreviousVal.Text = "0";
			this.txtPreviousVal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtPreviousVal, null);
			// 
			// txtCurrentVal
			// 
			this.txtCurrentVal.AutoSize = false;
			this.txtCurrentVal.BackColor = System.Drawing.SystemColors.Window;
			this.txtCurrentVal.Enabled = false;
			this.txtCurrentVal.LinkItem = null;
			this.txtCurrentVal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCurrentVal.LinkTopic = null;
			this.txtCurrentVal.Location = new System.Drawing.Point(479, 423);
			this.txtCurrentVal.Name = "txtCurrentVal";
			this.txtCurrentVal.Size = new System.Drawing.Size(98, 40);
			this.txtCurrentVal.TabIndex = 6;
			this.txtCurrentVal.Text = "0";
			this.txtCurrentVal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtCurrentVal, null);
			// 
			// txtTotalNewValuation
			// 
			this.txtTotalNewValuation.AutoSize = false;
			this.txtTotalNewValuation.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalNewValuation.Enabled = false;
			this.txtTotalNewValuation.LinkItem = null;
			this.txtTotalNewValuation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotalNewValuation.LinkTopic = null;
			this.txtTotalNewValuation.Location = new System.Drawing.Point(608, 423);
			this.txtTotalNewValuation.Name = "txtTotalNewValuation";
			this.txtTotalNewValuation.Size = new System.Drawing.Size(98, 40);
			this.txtTotalNewValuation.TabIndex = 8;
			this.txtTotalNewValuation.Text = "0";
			this.txtTotalNewValuation.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtTotalNewValuation, resources.GetString("txtTotalNewValuation.ToolTip"));
			// 
			// framDetail
			// 
			this.framDetail.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.framDetail.AppearanceKey = "groupBoxNoBorders";
			this.framDetail.Controls.Add(this.cmdCancel);
			this.framDetail.Controls.Add(this.cmdBack);
			this.framDetail.Controls.Add(this.GridSavedDetail);
			this.framDetail.Controls.Add(this.GridDetail);
			this.framDetail.Controls.Add(this.lblMapLot);
			this.framDetail.Controls.Add(this.lblName);
			this.framDetail.Controls.Add(this.lblAccount);
			this.framDetail.Location = new System.Drawing.Point(0, 0);
			this.framDetail.Name = "framDetail";
			this.framDetail.Size = new System.Drawing.Size(718, 366);
			this.framDetail.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.framDetail, null);
			this.framDetail.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(126, 319);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(82, 40);
			this.cmdCancel.TabIndex = 5;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdBack
			// 
			this.cmdBack.AppearanceKey = "actionButton";
			this.cmdBack.ForeColor = System.Drawing.Color.White;
			this.cmdBack.Location = new System.Drawing.Point(30, 319);
			this.cmdBack.Name = "cmdBack";
			this.cmdBack.Size = new System.Drawing.Size(82, 40);
			this.cmdBack.TabIndex = 4;
			this.cmdBack.Text = "Update";
			this.ToolTip1.SetToolTip(this.cmdBack, null);
			this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
			// 
			// GridSavedDetail
			// 
			this.GridSavedDetail.AllowSelection = false;
			this.GridSavedDetail.AllowUserToResizeColumns = false;
			this.GridSavedDetail.AllowUserToResizeRows = false;
			this.GridSavedDetail.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridSavedDetail.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridSavedDetail.BackColorBkg = System.Drawing.Color.Empty;
			this.GridSavedDetail.BackColorFixed = System.Drawing.Color.Empty;
			this.GridSavedDetail.BackColorSel = System.Drawing.Color.Empty;
			this.GridSavedDetail.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridSavedDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridSavedDetail.ColumnHeadersHeight = 30;
			this.GridSavedDetail.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridSavedDetail.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridSavedDetail.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridSavedDetail.DragIcon = null;
			this.GridSavedDetail.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridSavedDetail.FixedCols = 0;
			this.GridSavedDetail.FixedRows = 0;
			this.GridSavedDetail.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridSavedDetail.FrozenCols = 0;
			this.GridSavedDetail.GridColor = System.Drawing.Color.Empty;
			this.GridSavedDetail.GridColorFixed = System.Drawing.Color.Empty;
			this.GridSavedDetail.Location = new System.Drawing.Point(558, 320);
			this.GridSavedDetail.Name = "GridSavedDetail";
			this.GridSavedDetail.ReadOnly = true;
			this.GridSavedDetail.RowHeadersVisible = false;
			this.GridSavedDetail.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridSavedDetail.RowHeightMin = 0;
			this.GridSavedDetail.Rows = 50;
			this.GridSavedDetail.ScrollTipText = null;
			this.GridSavedDetail.ShowColumnVisibilityMenu = false;
			this.GridSavedDetail.Size = new System.Drawing.Size(50, 10);
			this.GridSavedDetail.StandardTab = true;
			this.GridSavedDetail.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridSavedDetail.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridSavedDetail.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.GridSavedDetail, null);
			this.GridSavedDetail.Visible = false;
			// 
			// GridDetail
			// 
			this.GridDetail.AllowSelection = false;
			this.GridDetail.AllowUserToResizeColumns = false;
			this.GridDetail.AllowUserToResizeRows = false;
			this.GridDetail.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridDetail.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDetail.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDetail.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDetail.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDetail.BackColorSel = System.Drawing.Color.Empty;
			this.GridDetail.Cols = 8;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridDetail.ColumnHeadersHeight = 30;
			this.GridDetail.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDetail.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridDetail.DragIcon = null;
			this.GridDetail.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDetail.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDetail.ExtendLastCol = true;
			this.GridDetail.FixedCols = 5;
			this.GridDetail.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDetail.FrozenCols = 0;
			this.GridDetail.GridColor = System.Drawing.Color.Empty;
			this.GridDetail.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDetail.Location = new System.Drawing.Point(30, 60);
			this.GridDetail.Name = "GridDetail";
			this.GridDetail.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDetail.RowHeightMin = 0;
			this.GridDetail.Rows = 1;
			this.GridDetail.ScrollTipText = null;
			this.GridDetail.ShowColumnVisibilityMenu = false;
			this.GridDetail.Size = new System.Drawing.Size(688, 246);
			this.GridDetail.StandardTab = true;
			this.GridDetail.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDetail.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDetail.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.GridDetail, null);
			// 
			// lblMapLot
			// 
			this.lblMapLot.Location = new System.Drawing.Point(110, 30);
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Size = new System.Drawing.Size(106, 18);
			this.lblMapLot.TabIndex = 1;
			this.lblMapLot.Text = "MAPLOT";
			this.ToolTip1.SetToolTip(this.lblMapLot, null);
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(251, 30);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(252, 18);
			this.lblName.TabIndex = 2;
			this.lblName.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblName, null);
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(30, 30);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(58, 18);
			this.lblAccount.TabIndex = 0;
			this.lblAccount.Text = "0";
			this.ToolTip1.SetToolTip(this.lblAccount, null);
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 12;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle6;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 30);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(688, 330);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Grid, null);
			this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Shape1.Location = new System.Drawing.Point(30, 378);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(15, 20);
			this.Shape1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.Shape1, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(52, 380);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(273, 18);
			this.Label4.TabIndex = 2;
			this.Label4.Text = " = CHANGES DO NOT INDICATE NEW VALUATION";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(346, 380);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(115, 39);
			this.Label3.TabIndex = 3;
			this.Label3.Text = "TOTAL PREVIOUS VALUATION";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(479, 380);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(98, 39);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "TOTAL CURRENT VALUATION";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(625, 380);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(68, 39);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "TOTAL NEW VALUATION";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(352, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(100, 48);
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(636, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(80, 24);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete Line";
			this.ToolTip1.SetToolTip(this.cmdDelete, null);
			this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteEntry_Click);
			// 
			// frmNewValuationAnalysis
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(748, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmNewValuationAnalysis";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmNewValuationAnalysis_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewValuationAnalysis_KeyDown);
			this.Resize += new System.EventHandler(this.frmNewValuationAnalysis_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framDetail)).EndInit();
			this.framDetail.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSavedDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
		private FCButton cmdDelete;
	}
}
