﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srpt1CardLand.
	/// </summary>
	partial class srpt1CardLand
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srpt1CardLand));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMethod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLandType,
				this.txtUnits,
				this.txtFactor,
				this.txtValue,
				this.txtMethod
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4
			});
			this.ReportHeader.Height = 0.2083333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalValue,
				this.Label6
			});
			this.ReportFooter.Height = 0.1875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label1.Text = "Description";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.020833F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label2.Text = "Units";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.5625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label3.Text = "Value";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.604167F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label4.Text = "Factor";
			this.Label4.Top = 0F;
			this.Label4.Width = 0.4375F;
			// 
			// txtLandType
			// 
			this.txtLandType.Height = 0.1875F;
			this.txtLandType.Left = 0F;
			this.txtLandType.Name = "txtLandType";
			this.txtLandType.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtLandType.Text = null;
			this.txtLandType.Top = 0F;
			this.txtLandType.Width = 1.552083F;
			// 
			// txtUnits
			// 
			this.txtUnits.Height = 0.1875F;
			this.txtUnits.Left = 1.947917F;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtUnits.Text = null;
			this.txtUnits.Top = 0F;
			this.txtUnits.Width = 0.6354167F;
			// 
			// txtFactor
			// 
			this.txtFactor.Height = 0.1875F;
			this.txtFactor.Left = 2.604167F;
			this.txtFactor.Name = "txtFactor";
			this.txtFactor.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtFactor.Text = null;
			this.txtFactor.Top = 0F;
			this.txtFactor.Width = 0.375F;
			// 
			// txtValue
			// 
			this.txtValue.Height = 0.1875F;
			this.txtValue.Left = 3F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0F;
			this.txtValue.Width = 0.6875F;
			// 
			// txtMethod
			// 
			this.txtMethod.Height = 0.1875F;
			this.txtMethod.Left = 0.8194444F;
			this.txtMethod.Name = "txtMethod";
			this.txtMethod.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtMethod.Text = null;
			this.txtMethod.Top = 0F;
			this.txtMethod.Width = 1.083333F;
			// 
			// txtTotalValue
			// 
			this.txtTotalValue.Height = 0.1875F;
			this.txtTotalValue.Left = 2.489583F;
			this.txtTotalValue.Name = "txtTotalValue";
			this.txtTotalValue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtTotalValue.Text = null;
			this.txtTotalValue.Top = 0F;
			this.txtTotalValue.Width = 1.197917F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.9583333F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: left";
			this.Label6.Text = "Total Value of Land:";
			this.Label6.Top = 0F;
			this.Label6.Width = 1.479167F;
			// 
			// srpt1CardLand
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 3.6875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMethod;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
