﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class clsLandType
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strShortDescription = string.Empty;
		private int lngCategory;
		private int lngCode;
		private double dblFactor;
		private int lngType;
		private clsDRWrapper clsLTypes_AutoInitialized;

		private clsDRWrapper clsLTypes
		{
			get
			{
				if (clsLTypes_AutoInitialized == null)
				{
					clsLTypes_AutoInitialized = new clsDRWrapper();
				}
				return clsLTypes_AutoInitialized;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public int Category
		{
			set
			{
				lngCategory = value;
			}
			get
			{
				int Category = 0;
				Category = lngCategory;
				return Category;
			}
		}

		public int Code
		{
			set
			{
				lngCode = value;
			}
			get
			{
				int Code = 0;
				Code = lngCode;
				return Code;
			}
		}

		public double Factor
		{
			set
			{
				dblFactor = value;
			}
			get
			{
				double Factor = 0;
				Factor = dblFactor;
				return Factor;
			}
		}

		public int UnitsType
		{
			set
			{
				lngType = value;
			}
			get
			{
				int UnitsType = 0;
				UnitsType = lngType;
				return UnitsType;
			}
		}

		public void Init()
		{
			clsLTypes.OpenRecordset("select * from LandType order by code", modGlobalVariables.strREDatabase);
			if (!clsLTypes.EndOfFile())
			{
				// initialize some values
				strDescription = FCConvert.ToString(clsLTypes.Get_Fields_String("Description"));
				strShortDescription = FCConvert.ToString(clsLTypes.Get_Fields_String("ShortDescription"));
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				lngCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("category"))));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("code"))));
				dblFactor = Conversion.Val(clsLTypes.Get_Fields_Double("factor"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("Type"))));
			}
		}
		// vbPorter upgrade warning: lngCodeToFind As int	OnWrite(int, double, string)
		public bool FindCode(int lngCodeToFind)
		{
			bool FindCode = false;
			FindCode = false;
			// If clsLTypes.FindFirstRecord("Code", lngCodeToFind) Then
			if (clsLTypes.FindFirst("code = " + FCConvert.ToString(lngCodeToFind)))
			{
				strDescription = FCConvert.ToString(clsLTypes.Get_Fields_String("Description"));
				strShortDescription = FCConvert.ToString(clsLTypes.Get_Fields_String("ShortDescription"));
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				lngCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("category"))));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("code"))));
				dblFactor = Conversion.Val(clsLTypes.Get_Fields_Double("factor"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("Type"))));
				FindCode = true;
			}
			return FindCode;
		}

		public void UnloadLandTypes()
		{
			clsLTypes.Reset();
		}

		public bool IsSoftWood()
		{
			bool IsSoftWood = false;
			IsSoftWood = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATSOFTWOOD:
				case modREConstants.CNSTLANDCATSOFTWOODFARM:
					{
						IsSoftWood = true;
						break;
					}
			}
			//end switch
			return IsSoftWood;
		}

		public bool IsHardWood()
		{
			bool IsHardWood = false;
			IsHardWood = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATHARDWOOD:
				case modREConstants.CNSTLANDCATHARDWOODFARM:
					{
						IsHardWood = true;
						break;
					}
			}
			//end switch
			return IsHardWood;
		}

		public bool IsMixedWood()
		{
			bool IsMixedWood = false;
			IsMixedWood = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATMIXEDWOOD:
				case modREConstants.CNSTLANDCATMIXEDWOODFARM:
					{
						IsMixedWood = true;
						break;
					}
			}
			//end switch
			return IsMixedWood;
		}

		public bool IsTreeGrowth()
		{
			bool IsTreeGrowth = false;
			IsTreeGrowth = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATHARDWOOD:
				case modREConstants.CNSTLANDCATHARDWOODFARM:
					{
						IsTreeGrowth = true;
						break;
					}
				case modREConstants.CNSTLANDCATMIXEDWOOD:
				case modREConstants.CNSTLANDCATMIXEDWOODFARM:
					{
						IsTreeGrowth = true;
						break;
					}
				case modREConstants.CNSTLANDCATSOFTWOOD:
				case modREConstants.CNSTLANDCATSOFTWOODFARM:
					{
						IsTreeGrowth = true;
						break;
					}
			}
			//end switch
			return IsTreeGrowth;
		}

		public bool IsFarmTreeGrowth()
		{
			bool IsFarmTreeGrowth = false;
			IsFarmTreeGrowth = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATHARDWOODFARM:
				case modREConstants.CNSTLANDCATMIXEDWOODFARM:
				case modREConstants.CNSTLANDCATSOFTWOODFARM:
					{
						IsFarmTreeGrowth = true;
						break;
					}
			}
			//end switch
			return IsFarmTreeGrowth;
		}

		public bool IsPlainTreeGrowth()
		{
			bool IsPlainTreeGrowth = false;
			IsPlainTreeGrowth = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATHARDWOOD:
				case modREConstants.CNSTLANDCATMIXEDWOOD:
				case modREConstants.CNSTLANDCATSOFTWOOD:
					{
						IsPlainTreeGrowth = true;
						break;
					}
			}
			//end switch
			return IsPlainTreeGrowth;
		}

		public bool IsTillable()
		{
			bool IsTillable = false;
			IsTillable = false;
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATCROP:
				case modREConstants.CNSTLANDCATORCHARD:
				case modREConstants.CNSTLANDCATPASTURE:
					{
						IsTillable = true;
						break;
					}
			}
			//end switch
			return IsTillable;
		}

		public bool IsWaterfront()
		{
			bool IsWaterfront = false;
			IsWaterfront = false;
			if (lngCategory == modREConstants.CNSTLANDCATWATERFRONT)
				IsWaterfront = true;
			return IsWaterfront;
		}

		public void MoveFirst()
		{
			clsLTypes.MoveFirst();
		}

		public void MoveNext()
		{
			clsLTypes.MoveNext();
			if (!clsLTypes.EndOfFile())
			{
				strDescription = FCConvert.ToString(clsLTypes.Get_Fields_String("Description"));
				strShortDescription = FCConvert.ToString(clsLTypes.Get_Fields_String("ShortDescription"));
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				lngCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("category"))));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("code"))));
				dblFactor = Conversion.Val(clsLTypes.Get_Fields_Double("factor"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLTypes.Get_Fields("Type"))));
			}
		}

		public bool EndOfLandTypes()
		{
			bool EndOfLandTypes = false;
			EndOfLandTypes = clsLTypes.EndOfFile();
			return EndOfLandTypes;
		}

		public string TypeDescription()
		{
			string TypeDescription = "";
			string strReturn;
			strReturn = "";
			TypeDescription = "";
			switch (lngType)
			{
				case modREConstants.CNSTLANDTYPEFRONTFOOT:
					{
						strReturn = "Front Foot";
						break;
					}
				case modREConstants.CNSTLANDTYPESQUAREFEET:
					{
						strReturn = "Square Feet";
						break;
					}
				case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
					{
						strReturn = "Fractional Acreage";
						break;
					}
				case modREConstants.CNSTLANDTYPEACRES:
					{
						strReturn = "Acres";
						break;
					}
				case modREConstants.CNSTLANDTYPESITE:
					{
						strReturn = "Site";
						break;
					}
				case modREConstants.CNSTLANDTYPELINEARFEET:
					{
						strReturn = "Linear Feet";
						break;
					}
				case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
					{
						strReturn = "Improvements";
						break;
					}
				case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
					{
						strReturn = "Front Foot - Width";
						break;
					}
				case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
					{
						strReturn = "Linear Feet - Width";
						break;
					}
			}
			//end switch
			TypeDescription = strReturn;
			return TypeDescription;
		}

		public string CategoryDescription()
		{
			string CategoryDescription = "";
			string strReturn;
			strReturn = "";
			switch (lngCategory)
			{
				case modREConstants.CNSTLANDCATNONE:
					{
						break;
					}
				case modREConstants.CNSTLANDCATSOFTWOOD:
					{
						strReturn = "Softwood";
						break;
					}
				case modREConstants.CNSTLANDCATMIXEDWOOD:
					{
						strReturn = "Mixedwood";
						break;
					}
				case modREConstants.CNSTLANDCATHARDWOOD:
					{
						strReturn = "Hardwood";
						break;
					}
				case modREConstants.CNSTLANDCATSOFTWOODFARM:
					{
						strReturn = "Softwood (Farm)";
						break;
					}
				case modREConstants.CNSTLANDCATMIXEDWOODFARM:
					{
						strReturn = "Mixedwood (Farm)";
						break;
					}
				case modREConstants.CNSTLANDCATHARDWOODFARM:
					{
						strReturn = "Hardwood (Farm)";
						break;
					}
				case modREConstants.CNSTLANDCATCROP:
					{
						strReturn = "Crop";
						break;
					}
				case modREConstants.CNSTLANDCATORCHARD:
					{
						strReturn = "Orchard";
						break;
					}
				case modREConstants.CNSTLANDCATPASTURE:
					{
						strReturn = "Pasture";
						break;
					}
				case modREConstants.CNSTLANDCATOPENSPACE:
					{
						strReturn = "Open Space";
						break;
					}
				case modREConstants.CNSTLANDCATWATERFRONT:
					{
						strReturn = "Waterfront";
						break;
					}
			}
			//end switch
			CategoryDescription = strReturn;
			return CategoryDescription;
		}
	}
}
