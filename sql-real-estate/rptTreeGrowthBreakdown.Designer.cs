﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTreeGrowthBreakdown.
	/// </summary>
	partial class rptTreeGrowthBreakdown
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTreeGrowthBreakdown));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtASoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotASoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtASoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotASoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtASoft,
				this.txtAMixed,
				this.txtAHard,
				this.txtSoft,
				this.txtMixed,
				this.txtHard,
				this.txtAOther,
				this.txtOther,
				this.txtMapLot
			});
			this.Detail.Height = 0.4583333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field11,
				this.txtTotASoft,
				this.txtTotAMixed,
				this.txtTotAHard,
				this.txtTotSoft,
				this.txtTotMixed,
				this.txtTotHard,
				this.txtTotAOther,
				this.txtTotOther
			});
			this.ReportFooter.Height = 0.3125F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.txtDate,
				this.txtPage,
				this.Field2,
				this.Field19,
				this.Field20,
				this.Field21,
				this.Field22,
				this.Field23,
				this.Field24,
				this.Field26,
				this.Field27,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.txtMuni,
				this.txtTime,
				this.Field18,
				this.Field28,
				this.Field29,
				this.Field25,
				this.lblTitle2
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Tree Growth Breakdown";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 3.5625F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 7F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 0.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 6.375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field2.Text = "Page";
			this.Field2.Top = 0.1875F;
			this.Field2.Width = 0.625F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 1.4375F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field19.Text = "Soft";
			this.Field19.Top = 0.625F;
			this.Field19.Width = 0.6875F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 2.1875F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field20.Text = "Mixed";
			this.Field20.Top = 0.625F;
			this.Field20.Width = 0.6875F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 2.9375F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field21.Text = "Hard";
			this.Field21.Top = 0.625F;
			this.Field21.Width = 0.6875F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 4.4375F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field22.Text = "Soft";
			this.Field22.Top = 0.625F;
			this.Field22.Width = 0.6875F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 5.1875F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field23.Text = "Mixed";
			this.Field23.Top = 0.625F;
			this.Field23.Width = 0.6875F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 5.9375F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field24.Text = "Hard";
			this.Field24.Top = 0.625F;
			this.Field24.Width = 0.6875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 2.5625F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field26.Text = "Acreage";
			this.Field26.Top = 0.4375F;
			this.Field26.Width = 0.75F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.1875F;
			this.Field27.Left = 5.6875F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field27.Text = "Value";
			this.Field27.Top = 0.4375F;
			this.Field27.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.4375F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5625F;
			this.Line1.Width = 1.0625F;
			this.Line1.X1 = 2.5F;
			this.Line1.X2 = 1.4375F;
			this.Line1.Y1 = 0.5625F;
			this.Line1.Y2 = 0.5625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.375F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5625F;
			this.Line2.Width = 0.9375F;
			this.Line2.X1 = 4.3125F;
			this.Line2.X2 = 3.375F;
			this.Line2.Y1 = 0.5625F;
			this.Line2.Y2 = 0.5625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.5F;
			this.Line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.5625F;
			this.Line3.Width = 1.125F;
			this.Line3.X1 = 5.625F;
			this.Line3.X2 = 4.5F;
			this.Line3.Y1 = 0.5625F;
			this.Line3.Y2 = 0.5625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 6.3125F;
			this.Line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.5625F;
			this.Line4.Width = 1.0625F;
			this.Line4.X1 = 7.375F;
			this.Line4.X2 = 6.3125F;
			this.Line4.Y1 = 0.5625F;
			this.Line4.Y2 = 0.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.8125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.19F;
			this.Field18.Left = 0F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field18.Text = "Account";
			this.Field18.Top = 0.625F;
			this.Field18.Width = 0.625F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 6.6875F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field28.Text = "Other";
			this.Field28.Top = 0.625F;
			this.Field28.Width = 0.75F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 3.6875F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field29.Text = "Other";
			this.Field29.Top = 0.625F;
			this.Field29.Width = 0.6875F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 0.6875F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field25.Text = "Name";
			this.Field25.Top = 0.625F;
			this.Field25.Width = 0.75F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1666667F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 2F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblTitle2.Text = null;
			this.lblTitle2.Top = 0.25F;
			this.lblTitle2.Width = 3.5F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.6875F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtName.Text = null;
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 2.3125F;
			// 
			// txtASoft
			// 
			this.txtASoft.Height = 0.1875F;
			this.txtASoft.Left = 1.4375F;
			this.txtASoft.Name = "txtASoft";
			this.txtASoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtASoft.Text = null;
			this.txtASoft.Top = 0.1875F;
			this.txtASoft.Width = 0.6875F;
			// 
			// txtAMixed
			// 
			this.txtAMixed.Height = 0.1875F;
			this.txtAMixed.Left = 2.1875F;
			this.txtAMixed.Name = "txtAMixed";
			this.txtAMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAMixed.Text = null;
			this.txtAMixed.Top = 0.1875F;
			this.txtAMixed.Width = 0.6875F;
			// 
			// txtAHard
			// 
			this.txtAHard.Height = 0.1875F;
			this.txtAHard.Left = 2.9375F;
			this.txtAHard.Name = "txtAHard";
			this.txtAHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAHard.Text = null;
			this.txtAHard.Top = 0.1875F;
			this.txtAHard.Width = 0.6875F;
			// 
			// txtSoft
			// 
			this.txtSoft.Height = 0.1875F;
			this.txtSoft.Left = 4.4375F;
			this.txtSoft.Name = "txtSoft";
			this.txtSoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSoft.Text = null;
			this.txtSoft.Top = 0.1875F;
			this.txtSoft.Width = 0.6875F;
			// 
			// txtMixed
			// 
			this.txtMixed.Height = 0.1875F;
			this.txtMixed.Left = 5.1875F;
			this.txtMixed.Name = "txtMixed";
			this.txtMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMixed.Text = null;
			this.txtMixed.Top = 0.1875F;
			this.txtMixed.Width = 0.6875F;
			// 
			// txtHard
			// 
			this.txtHard.Height = 0.1875F;
			this.txtHard.Left = 5.9375F;
			this.txtHard.Name = "txtHard";
			this.txtHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtHard.Text = null;
			this.txtHard.Top = 0.1875F;
			this.txtHard.Width = 0.6875F;
			// 
			// txtAOther
			// 
			this.txtAOther.Height = 0.1875F;
			this.txtAOther.Left = 3.6875F;
			this.txtAOther.Name = "txtAOther";
			this.txtAOther.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAOther.Text = null;
			this.txtAOther.Top = 0.1875F;
			this.txtAOther.Width = 0.6875F;
			// 
			// txtOther
			// 
			this.txtOther.Height = 0.1875F;
			this.txtOther.Left = 6.6875F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtOther.Text = null;
			this.txtOther.Top = 0.1875F;
			this.txtOther.Width = 0.75F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 3.0625F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0.03125F;
			this.txtMapLot.Width = 1.25F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Field11.Text = "Total";
			this.Field11.Top = 0.125F;
			this.Field11.Width = 0.9375F;
			// 
			// txtTotASoft
			// 
			this.txtTotASoft.Height = 0.1875F;
			this.txtTotASoft.Left = 1.4375F;
			this.txtTotASoft.Name = "txtTotASoft";
			this.txtTotASoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotASoft.Text = null;
			this.txtTotASoft.Top = 0.125F;
			this.txtTotASoft.Width = 0.6875F;
			// 
			// txtTotAMixed
			// 
			this.txtTotAMixed.Height = 0.1875F;
			this.txtTotAMixed.Left = 2.1875F;
			this.txtTotAMixed.Name = "txtTotAMixed";
			this.txtTotAMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotAMixed.Text = "Field13";
			this.txtTotAMixed.Top = 0.125F;
			this.txtTotAMixed.Width = 0.6875F;
			// 
			// txtTotAHard
			// 
			this.txtTotAHard.Height = 0.1875F;
			this.txtTotAHard.Left = 2.9375F;
			this.txtTotAHard.Name = "txtTotAHard";
			this.txtTotAHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotAHard.Text = "Field14";
			this.txtTotAHard.Top = 0.125F;
			this.txtTotAHard.Width = 0.6875F;
			// 
			// txtTotSoft
			// 
			this.txtTotSoft.Height = 0.1875F;
			this.txtTotSoft.Left = 4.4375F;
			this.txtTotSoft.Name = "txtTotSoft";
			this.txtTotSoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotSoft.Text = "Field15";
			this.txtTotSoft.Top = 0.125F;
			this.txtTotSoft.Width = 0.6875F;
			// 
			// txtTotMixed
			// 
			this.txtTotMixed.Height = 0.1875F;
			this.txtTotMixed.Left = 5.1875F;
			this.txtTotMixed.Name = "txtTotMixed";
			this.txtTotMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotMixed.Text = "Field16";
			this.txtTotMixed.Top = 0.125F;
			this.txtTotMixed.Width = 0.6875F;
			// 
			// txtTotHard
			// 
			this.txtTotHard.Height = 0.1875F;
			this.txtTotHard.Left = 5.9375F;
			this.txtTotHard.Name = "txtTotHard";
			this.txtTotHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotHard.Text = "Field17";
			this.txtTotHard.Top = 0.125F;
			this.txtTotHard.Width = 0.6875F;
			// 
			// txtTotAOther
			// 
			this.txtTotAOther.Height = 0.1875F;
			this.txtTotAOther.Left = 3.6875F;
			this.txtTotAOther.Name = "txtTotAOther";
			this.txtTotAOther.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotAOther.Text = "Field14";
			this.txtTotAOther.Top = 0.125F;
			this.txtTotAOther.Width = 0.6875F;
			// 
			// txtTotOther
			// 
			this.txtTotOther.Height = 0.1875F;
			this.txtTotOther.Left = 6.6875F;
			this.txtTotOther.Name = "txtTotOther";
			this.txtTotOther.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotOther.Text = "Field17";
			this.txtTotOther.Top = 0.125F;
			this.txtTotOther.Width = 0.75F;
			// 
			// rptTreeGrowthBreakdown
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtASoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotASoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtASoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotASoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotOther;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
