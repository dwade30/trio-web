﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptHighestAssessment.
	/// </summary>
	partial class rptHighestAssessment
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptHighestAssessment));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Title1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Title2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNumOf = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Title1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Title2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNumOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtLand,
				this.txtBldg,
				this.txtExempt,
				this.txtTotal
			});
			this.Detail.Height = 0.25F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLandTot,
				this.txtBldgTot,
				this.txtExemptTot,
				this.txtTotTot,
				this.Line1
			});
			this.ReportFooter.Height = 0.28125F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Title1,
				this.Title2,
				this.lblAccount,
				this.lblNumOf,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Field5,
				this.Field6
			});
			this.PageHeader.Height = 1.0625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 3.25F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Field1.Text = "REAL ESTATE";
			this.Field1.Top = 0F;
			this.Field1.Width = 1.3125F;
			// 
			// Title1
			// 
			this.Title1.Height = 0.1875F;
			this.Title1.Left = 2.875F;
			this.Title1.Name = "Title1";
			this.Title1.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
			this.Title1.Text = "Field4";
			this.Title1.Top = 0.21875F;
			this.Title1.Width = 1.9375F;
			// 
			// Title2
			// 
			this.Title2.CanGrow = false;
			this.Title2.Height = 0.1875F;
			this.Title2.Left = 2.75F;
			this.Title2.MultiLine = false;
			this.Title2.Name = "Title2";
			this.Title2.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
			this.Title2.Text = "Field5";
			this.Title2.Top = 0.40625F;
			this.Title2.Visible = false;
			this.Title2.Width = 2.125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.0625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.8125F;
			this.lblAccount.Width = 0.5F;
			// 
			// lblNumOf
			// 
			this.lblNumOf.Height = 0.21875F;
			this.lblNumOf.HyperLink = null;
			this.lblNumOf.Left = 0.0625F;
			this.lblNumOf.Name = "lblNumOf";
			this.lblNumOf.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblNumOf.Text = "No. OF";
			this.lblNumOf.Top = 0.59375F;
			this.lblNumOf.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.75F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label4.Text = "Name / Location";
			this.Label4.Top = 0.8125F;
			this.Label4.Width = 1.5F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.Label5.Text = "Land";
			this.Label5.Top = 0.8125F;
			this.Label5.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.Label6.Text = "Buildings";
			this.Label6.Top = 0.8125F;
			this.Label6.Width = 1.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.3125F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.Label7.Text = "Exemption";
			this.Label7.Top = 0.8125F;
			this.Label7.Width = 0.875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.5F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.Label8.Text = "Total";
			this.Label8.Top = 0.8125F;
			this.Label8.Width = 0.9375F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 7.1875F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.Field5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.Field5.Text = null;
			this.Field5.Top = 0.1875F;
			this.Field5.Width = 0.25F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 6.5625F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field6.Text = "Page";
			this.Field6.Top = 0.1875F;
			this.Field6.Width = 0.625F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtAccount.Text = "Field7";
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.75F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtName.Text = "Field8";
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 2.1875F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.1875F;
			this.txtLand.Left = 3F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtLand.Text = "Field9";
			this.txtLand.Top = 0.03125F;
			this.txtLand.Width = 0.875F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.1875F;
			this.txtBldg.Left = 3.9375F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtBldg.Text = "Field10";
			this.txtBldg.Top = 0.03125F;
			this.txtBldg.Width = 1.25F;
			// 
			// txtExempt
			// 
			this.txtExempt.CanGrow = false;
			this.txtExempt.Height = 0.1875F;
			this.txtExempt.Left = 5.25F;
			this.txtExempt.MultiLine = false;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtExempt.Text = "Field11";
			this.txtExempt.Top = 0.03125F;
			this.txtExempt.Width = 0.9375F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 6.25F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtTotal.Text = "Field12";
			this.txtTotal.Top = 0.03125F;
			this.txtTotal.Width = 1.1875F;
			// 
			// txtLandTot
			// 
			this.txtLandTot.Height = 0.1875F;
			this.txtLandTot.Left = 3F;
			this.txtLandTot.MultiLine = false;
			this.txtLandTot.Name = "txtLandTot";
			this.txtLandTot.Style = "font-family: \'Courier\'; text-align: right; white-space: nowrap; ddo-char-set: 1";
			this.txtLandTot.Text = null;
			this.txtLandTot.Top = 0.125F;
			this.txtLandTot.Width = 0.875F;
			// 
			// txtBldgTot
			// 
			this.txtBldgTot.CanGrow = false;
			this.txtBldgTot.Height = 0.1875F;
			this.txtBldgTot.Left = 3.9375F;
			this.txtBldgTot.MultiLine = false;
			this.txtBldgTot.Name = "txtBldgTot";
			this.txtBldgTot.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtBldgTot.Text = null;
			this.txtBldgTot.Top = 0.125F;
			this.txtBldgTot.Width = 1.25F;
			// 
			// txtExemptTot
			// 
			this.txtExemptTot.CanGrow = false;
			this.txtExemptTot.Height = 0.1875F;
			this.txtExemptTot.Left = 5.25F;
			this.txtExemptTot.MultiLine = false;
			this.txtExemptTot.Name = "txtExemptTot";
			this.txtExemptTot.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtExemptTot.Text = null;
			this.txtExemptTot.Top = 0.125F;
			this.txtExemptTot.Width = 0.9375F;
			// 
			// txtTotTot
			// 
			this.txtTotTot.CanGrow = false;
			this.txtTotTot.Height = 0.1875F;
			this.txtTotTot.Left = 6.25F;
			this.txtTotTot.MultiLine = false;
			this.txtTotTot.Name = "txtTotTot";
			this.txtTotTot.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtTotTot.Text = null;
			this.txtTotTot.Top = 0.125F;
			this.txtTotTot.Width = 1.1875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 4.375F;
			this.Line1.X1 = 3.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// rptHighestAssessment
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Title1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Title2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNumOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTot;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Title1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Title2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNumOf;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
