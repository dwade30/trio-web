﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTranLandBldgFiles.
	/// </summary>
	public partial class frmTranLandBldgFiles : BaseForm
	{
		public frmTranLandBldgFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTranLandBldgFiles InstancePtr
		{
			get
			{
				return (frmTranLandBldgFiles)Sys.GetInstance(typeof(frmTranLandBldgFiles));
			}
		}

		protected frmTranLandBldgFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intCodeType;
		bool boolChanges;
		const int CNSTLANDCODES = 2;
		const int CNSTTRANCODES = 1;
		const int CNSTBLDGCODES = 3;

		private void frmTranLandBldgFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTranLandBldgFiles properties;
			//frmTranLandBldgFiles.ScaleWidth	= 9225;
			//frmTranLandBldgFiles.ScaleHeight	= 7710;
			//frmTranLandBldgFiles.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			boolChanges = false;
			intCodeType = -1;
			// load code won't load if intcodetype is equal to the trancode it is called with
			if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				LoadCodes_2(1);
				// trancodes
			}
			else
			{
				LoadCodes_2(2);
				//FC:FINAl:MSH - i.issue #1101: replace menu items to toolbar buttons
				//mnuTran.Visible = false; // can't edit their town codes from here
				cmdTran.Visible = false;
				// can't edit their town codes from here
			}
		}

		private void frmTranLandBldgFiles_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
			// mark as edited
			boolChanges = true;
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			Grid.Rows += 1;
			Grid.RowData(Grid.Rows - 1, false);
			Grid.TextMatrix(Grid.Rows - 1, 3, "0.00");
		}

		private void mnuBldg_Click(object sender, System.EventArgs e)
		{
			LoadCodes_2(3);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			GridDeleted.AddItem(Grid.TextMatrix(Grid.Row, 4));
			// add the id of this item
			Grid.RemoveItem(Grid.Row);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.Cols = 5;
			Grid.ColHidden(4, true);
			Grid.TextMatrix(0, 0, "Code");
			Grid.TextMatrix(0, 1, "Description");
			Grid.TextMatrix(0, 2, "Short Description");
			Grid.TextMatrix(0, 3, "Amount");

            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.5 * GridWidth));
			Grid.ColWidth(2, FCConvert.ToInt32(0.24 * GridWidth));
			Grid.ColWidth(3, FCConvert.ToInt32(0.12 * GridWidth));
		}

		private void mnuLand_Click(object sender, System.EventArgs e)
		{
			LoadCodes_2(2);
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			string strType = "";
			switch (intCodeType)
			{
				case CNSTLANDCODES:
					{
						strType = "LAND";
						break;
					}
				case CNSTTRANCODES:
					{
						strType = "TRAN";
						break;
					}
				case CNSTBLDGCODES:
					{
						strType = "BUILDING";
						break;
					}
			}
			//end switch
			rptNewCostFiles.InstancePtr.Init("PROPERTY", ref strType);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//Application.DoEvents();
			SaveCodes();
            //FC:FINAL:BSE #3345 add save successful message
            FCMessageBox.Show("Save Successful.", MsgBoxStyle.Information, "Save Complete");
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//Application.DoEvents();
			if (SaveCodes())
			{
				mnuExit_Click();
			}
		}

		private void mnuTran_Click(object sender, System.EventArgs e)
		{
			LoadCodes_2(1);
		}

		private bool SaveCodes()
		{
			bool SaveCodes = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strTableName = "";
				string strSQL = "";
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				SaveCodes = false;
				switch (intCodeType)
				{
					case 1:
						{
							strTableName = "tblTranCode";
							break;
						}
					case 2:
						{
							strTableName = "tblLandCode";
							break;
						}
					case 3:
						{
							strTableName = "tblBldgCode";
							break;
						}
				}
				//end switch
				// delete deleted items first
				for (x = 0; x <= GridDeleted.Rows - 1; x++)
				{
					strSQL = "delete from " + strTableName + " where id = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(x, 0)));
					clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
				}
				// x
				GridDeleted.Rows = 0;
				// now save the changes
				x = Grid.FindRow(true);
				while (x >= 0)
				{
					// if this is new then id = 0 and will never update anything so insert
					if (Conversion.Val(Grid.TextMatrix(x, 0)) == 0)
					{
						// no blank codes allowed
						Grid.RemoveItem(x);
					}
					else
					{
						if (Conversion.Val(Grid.TextMatrix(x, 4)) == 0)
						{
							strSQL = "insert into " + strTableName + " (Code,Description,ShortDescription,amount) values (";
							strSQL += FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 0))) + ",'" + Strings.Trim(Grid.TextMatrix(x, 1)) + "','" + Strings.Trim(Grid.TextMatrix(x, 2)) + "'," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 3)));
							strSQL += ")";
						}
						else
						{
							strSQL = "update " + strTableName + " set code = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 0))) + ",description = '" + Strings.Trim(Grid.TextMatrix(x, 1)) + "'";
							strSQL += ",ShortDescription = '" + Strings.Trim(Grid.TextMatrix(x, 2)) + "',amount = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 3)));
							strSQL += " where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 4)));
						}
						clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
						Grid.RowData(x, false);
					}
					x = Grid.FindRow(true);
				}
				boolChanges = false;
				SaveCodes = true;
				return SaveCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveCodes");
			}
			return SaveCodes;
		}

		private void LoadCodes_2(short intCode)
		{
			LoadCodes(ref intCode);
		}

		private void LoadCodes(ref short intCode)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				string strTableName = "";
				// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
				DialogResult intResponse;
				if (intCodeType == intCode)
				{
					return;
				}
				else
				{
					// check to see if there are changes to save
					if (GridDeleted.Rows > 0 || boolChanges)
					{
						intResponse = MessageBox.Show("Changes have been made.  Do you want to save first?", "Save First?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						if (intResponse == DialogResult.Cancel)
							return;
						if (intResponse == DialogResult.Yes)
						{
							if (!SaveCodes())
								return;
						}
					}
				}
				intCodeType = intCode;
				switch (intCodeType)
				{
					case 1:
						{
							this.Text = modGlobalVariables.Statics.strTranCodeTownCode + " Codes";
							//FC:FINAL:MSH - i.issue #1101: change text in header when page type changed
							HeaderText.Text = modGlobalVariables.Statics.strTranCodeTownCode + " Codes";
							strTableName = "tblTranCode";
							break;
						}
					case 2:
						{
							this.Text = "Land Codes";
							//FC:FINAL:MSH - i.issue #1101: change text in header when page type changed
							HeaderText.Text = "Land Codes";
							strTableName = "tblLandCode";
							break;
						}
					case 3:
						{
							this.Text = "Building Codes";
							//FC:FINAL:MSH - i.issue #1101: change text in header when page type changed
							HeaderText.Text = "Building Codes";
							strTableName = "tblBldgCode";
							break;
						}
				}
				//end switch
				Grid.Rows = 1;
				clsLoad.OpenRecordset("select * from " + strTableName + " order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					Grid.AddItem(clsLoad.Get_Fields("Code") + "\t" + clsLoad.Get_Fields_String("Description") + "\t" + clsLoad.Get_Fields_String("ShortDescription") + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "0.00") + "\t" + clsLoad.Get_Fields_Int32("id"));
					Grid.RowData(Grid.Rows - 1, false);
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadCodes");
			}
		}
	}
}
