﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srpttemp.
	/// </summary>
	public partial class srpttemp : FCSectionReport
	{
		public static srpttemp InstancePtr
		{
			get
			{
				return (srpttemp)Sys.GetInstance(typeof(srpttemp));
			}
		}

		protected srpttemp _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srpttemp()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srpttemp	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
	}
}
