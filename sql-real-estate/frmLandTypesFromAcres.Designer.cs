﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandTypesFromAcres.
	/// </summary>
	partial class frmLandTypesFromAcres : BaseForm
	{
		public fecherFoundation.FCCheckBox chkReplace;
		public fecherFoundation.FCComboBox cmbOther;
		public fecherFoundation.FCComboBox cmbHardwood;
		public fecherFoundation.FCComboBox cmbMixedWood;
		public fecherFoundation.FCComboBox cmbSoftWood;
		public fecherFoundation.FCLabel lblUpdate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLandTypesFromAcres));
			this.chkReplace = new fecherFoundation.FCCheckBox();
			this.cmbOther = new fecherFoundation.FCComboBox();
			this.cmbHardwood = new fecherFoundation.FCComboBox();
			this.cmbMixedWood = new fecherFoundation.FCComboBox();
			this.cmbSoftWood = new fecherFoundation.FCComboBox();
			this.lblUpdate = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkReplace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 518);
			this.BottomPanel.Size = new System.Drawing.Size(689, 25);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.chkReplace);
			this.ClientArea.Controls.Add(this.cmbOther);
			this.ClientArea.Controls.Add(this.cmbHardwood);
			this.ClientArea.Controls.Add(this.cmbMixedWood);
			this.ClientArea.Controls.Add(this.cmbSoftWood);
			this.ClientArea.Controls.Add(this.lblUpdate);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(689, 458);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(689, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(257, 30);
			this.HeaderText.Text = "Land Types To Create";
			// 
			// chkReplace
			// 
			this.chkReplace.Location = new System.Drawing.Point(30, 278);
			this.chkReplace.Name = "chkReplace";
			this.chkReplace.Size = new System.Drawing.Size(227, 27);
			this.chkReplace.TabIndex = 10;
			this.chkReplace.Text = "Replace existing land types";
			// 
			// cmbOther
			// 
			this.cmbOther.AutoSize = false;
			this.cmbOther.BackColor = System.Drawing.SystemColors.Window;
			this.cmbOther.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOther.FormattingEnabled = true;
			this.cmbOther.Location = new System.Drawing.Point(181, 218);
			this.cmbOther.Name = "cmbOther";
			this.cmbOther.Size = new System.Drawing.Size(288, 40);
			this.cmbOther.TabIndex = 3;
			// 
			// cmbHardwood
			// 
			this.cmbHardwood.AutoSize = false;
			this.cmbHardwood.BackColor = System.Drawing.SystemColors.Window;
			this.cmbHardwood.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbHardwood.FormattingEnabled = true;
			this.cmbHardwood.Location = new System.Drawing.Point(181, 158);
			this.cmbHardwood.Name = "cmbHardwood";
			this.cmbHardwood.Size = new System.Drawing.Size(288, 40);
			this.cmbHardwood.TabIndex = 2;
			// 
			// cmbMixedWood
			// 
			this.cmbMixedWood.AutoSize = false;
			this.cmbMixedWood.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMixedWood.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbMixedWood.FormattingEnabled = true;
			this.cmbMixedWood.Location = new System.Drawing.Point(181, 98);
			this.cmbMixedWood.Name = "cmbMixedWood";
			this.cmbMixedWood.Size = new System.Drawing.Size(288, 40);
			this.cmbMixedWood.TabIndex = 1;
			// 
			// cmbSoftWood
			// 
			this.cmbSoftWood.AutoSize = false;
			this.cmbSoftWood.BackColor = System.Drawing.SystemColors.Window;
			this.cmbSoftWood.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSoftWood.FormattingEnabled = true;
			this.cmbSoftWood.Location = new System.Drawing.Point(181, 38);
			this.cmbSoftWood.Name = "cmbSoftWood";
			this.cmbSoftWood.Size = new System.Drawing.Size(288, 40);
			this.cmbSoftWood.TabIndex = 0;
			// 
			// lblUpdate
			// 
			this.lblUpdate.Location = new System.Drawing.Point(30, 335);
			this.lblUpdate.Name = "lblUpdate";
			this.lblUpdate.Size = new System.Drawing.Size(257, 22);
			this.lblUpdate.TabIndex = 9;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 232);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(104, 20);
			this.Label4.TabIndex = 7;
			this.Label4.Text = "OTHER";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 172);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(104, 20);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "HARDWOOD";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 112);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(104, 20);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "MIXED WOOD";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 52);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(80, 18);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "SOFTWOOD";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 390);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmLandTypesFromAcres
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(689, 543);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLandTypesFromAcres";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Create Land Entries";
			this.Load += new System.EventHandler(this.frmLandTypesFromAcres_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLandTypesFromAcres_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkReplace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
