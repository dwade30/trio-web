﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTranLandBldgFiles.
	/// </summary>
	partial class frmTranLandBldgFiles : BaseForm
	{
		public FCGrid GridDeleted;
		public FCGrid Grid;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuTran;
		public fecherFoundation.FCToolStripMenuItem mnuLand;
		public fecherFoundation.FCToolStripMenuItem mnuBldg;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTranLandBldgFiles));
			this.GridDeleted = new fecherFoundation.FCGrid();
			this.Grid = new fecherFoundation.FCGrid();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuTran = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLand = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBldg = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			//FC:FINAL:MSH - i.issue #1101: replacing menu by buttons
			this.cmdTran = new fecherFoundation.FCButton();
			this.cmdLand = new fecherFoundation.FCButton();
			this.cmdBldg = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTran)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 582);
			this.BottomPanel.Size = new System.Drawing.Size(582, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridDeleted);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Size = new System.Drawing.Size(582, 522);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdTran);
			this.TopPanel.Controls.Add(this.cmdLand);
			this.TopPanel.Controls.Add(this.cmdBldg);
			this.TopPanel.Controls.Add(this.cmdAdd);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdPrintPreview);
			this.TopPanel.Size = new System.Drawing.Size(582, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdBldg, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLand, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdTran, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(140, 30);
			this.HeaderText.Text = "Tran Codes";
			// 
			// GridDeleted
			// 
			this.GridDeleted.AllowSelection = false;
			this.GridDeleted.AllowUserToResizeColumns = false;
			this.GridDeleted.AllowUserToResizeRows = false;
			this.GridDeleted.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDeleted.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDeleted.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDeleted.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDeleted.BackColorSel = System.Drawing.Color.Empty;
			this.GridDeleted.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			//FC:FINAL:MSH - i.issue #1146: set FixedCols to 0 for correct initializing columns of Grid
			this.GridDeleted.FixedCols = 0;
			this.GridDeleted.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDeleted.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDeleted.ColumnHeadersHeight = 30;
			this.GridDeleted.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDeleted.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDeleted.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDeleted.DragIcon = null;
			this.GridDeleted.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDeleted.FixedRows = 0;
			this.GridDeleted.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDeleted.FrozenCols = 0;
			this.GridDeleted.GridColor = System.Drawing.Color.Empty;
			this.GridDeleted.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDeleted.Location = new System.Drawing.Point(18, 202);
			this.GridDeleted.Name = "GridDeleted";
			this.GridDeleted.ReadOnly = true;
			this.GridDeleted.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDeleted.RowHeightMin = 0;
			this.GridDeleted.Rows = 0;
			this.GridDeleted.ScrollTipText = null;
			this.GridDeleted.ShowColumnVisibilityMenu = false;
			this.GridDeleted.Size = new System.Drawing.Size(13, 31);
			this.GridDeleted.StandardTab = true;
			this.GridDeleted.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDeleted.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDeleted.TabIndex = 1;
			this.GridDeleted.Visible = false;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 5;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
			this.Grid.DragIcon = null;
			this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 30);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(522, 467);
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.Grid.TabIndex = 0;
			this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuTran,
			//this.mnuLand,
			//this.mnuBldg});
			//this.MainMenu1.Name = "MainMenu1";
			//// 
			//// mnuTran
			//// 
			//this.mnuTran.Index = 0;
			//this.mnuTran.Name = "mnuTran";
			//this.mnuTran.Text = "Tran Codes";
			//this.mnuTran.Click += new System.EventHandler(this.mnuTran_Click);
			//// 
			//// mnuLand
			//// 
			//this.mnuLand.Index = 1;
			//this.mnuLand.Name = "mnuLand";
			//this.mnuLand.Text = "Land Codes";
			//this.mnuLand.Click += new System.EventHandler(this.mnuLand_Click);
			//// 
			//// mnuBldg
			//// 
			//this.mnuBldg.Index = 2;
			//this.mnuBldg.Name = "mnuBldg";
			//this.mnuBldg.Text = "Building Codes";
			//this.mnuBldg.Click += new System.EventHandler(this.mnuBldg_Click);
			//// 
			//// mnuFile
			//// 
			//this.mnuFile.Index = -1;
			//this.mnuFile.Name = "mnuFile";
			//this.mnuFile.Text = "File";
			//// 
			//// mnuAdd
			//// 
			//this.mnuAdd.Index = -1;
			//this.mnuAdd.Name = "mnuAdd";
			//this.mnuAdd.Text = "Add Code";
			//this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
			//// 
			//// mnuDelete
			//// 
			//this.mnuDelete.Index = -1;
			//this.mnuDelete.Name = "mnuDelete";
			//this.mnuDelete.Text = "Delete Code";
			//this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			//// 
			//// mnuSepar3
			//// 
			//this.mnuSepar3.Index = -1;
			//this.mnuSepar3.Name = "mnuSepar3";
			//this.mnuSepar3.Text = "-";
			//// 
			//// mnuSepar2
			//// 
			//this.mnuSepar2.Index = -1;
			//this.mnuSepar2.Name = "mnuSepar2";
			//this.mnuSepar2.Text = "-";
			//// 
			//// mnuPrintPreview
			//// 
			//this.mnuPrintPreview.Index = -1;
			//this.mnuPrintPreview.Name = "mnuPrintPreview";
			//this.mnuPrintPreview.Text = "Print Preview";
			//this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			//// 
			//// mnuSepar4
			//// 
			//this.mnuSepar4.Index = -1;
			//this.mnuSepar4.Name = "mnuSepar4";
			//this.mnuSepar4.Text = "-";
			//// 
			//// mnuSave
			//// 
			//this.mnuSave.Index = -1;
			//this.mnuSave.Name = "mnuSave";
			//this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			//this.mnuSave.Text = "Save";
			//this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			//// 
			//// mnuSaveExit
			//// 
			//this.mnuSaveExit.Index = -1;
			//this.mnuSaveExit.Name = "mnuSaveExit";
			//this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			//this.mnuSaveExit.Text = "Save & Exit";
			//this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			//// 
			//// mnuSepar1
			//// 
			//this.mnuSepar1.Index = -1;
			//this.mnuSepar1.Name = "mnuSepar1";
			//this.mnuSepar1.Text = "-";
			//// 
			//// mnuExit
			//// 
			//this.mnuExit.Index = -1;
			//this.mnuExit.Name = "mnuExit";
			//this.mnuExit.Text = "Exit";
			//this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// cmdTran
			//
			this.cmdTran.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdTran.AppearanceKey = "toolbarButton";
			this.cmdTran.Location = new System.Drawing.Point(0, 29);
			this.cmdTran.Name = "cmdTran";
			this.cmdTran.Size = new System.Drawing.Size(88, 24);
			this.cmdTran.Text = "Tran Codes";
			this.cmdTran.Click += new EventHandler(this.mnuTran_Click);
			//
			// cmdLand
			//
			this.cmdLand.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLand.AppearanceKey = "toolbarButton";
			this.cmdLand.Location = new System.Drawing.Point(94, 29);
			this.cmdLand.Name = "cmdLand";
			this.cmdLand.Size = new System.Drawing.Size(88, 24);
			this.cmdLand.Text = "Land Codes";
			this.cmdLand.Click += new EventHandler(this.mnuLand_Click);
			//
			// cmdBldg
			//
			this.cmdBldg.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdBldg.AppearanceKey = "toolbarButton";
			this.cmdBldg.Location = new System.Drawing.Point(188, 29);
			this.cmdBldg.Name = "cmdBldg";
			this.cmdBldg.Size = new System.Drawing.Size(108, 24);
			this.cmdBldg.Text = "Building Codes";
			this.cmdBldg.Click += new EventHandler(this.mnuBldg_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(250, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(93, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdAdd
			// 
			this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAdd.AppearanceKey = "toolbarButton";
			this.cmdAdd.Location = new System.Drawing.Point(302, 29);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(80, 24);
			this.cmdAdd.TabIndex = 1;
			this.cmdAdd.Text = "Add Code";
			this.cmdAdd.Click += new System.EventHandler(this.mnuAdd_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(388, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(94, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete Code";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintPreview.AppearanceKey = "toolbarButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(488, 29);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Size = new System.Drawing.Size(98, 24);
			this.cmdPrintPreview.TabIndex = 3;
			this.cmdPrintPreview.Text = "Print Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// frmTranLandBldgFiles
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(582, 690);
			this.FillColor = 0;
			//this.Menu = this.MainMenu1;
			this.Name = "frmTranLandBldgFiles";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Tran Codes";
			this.Load += new System.EventHandler(this.frmTranLandBldgFiles_Load);
			this.Resize += new System.EventHandler(this.frmTranLandBldgFiles_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTran)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrintPreview;
		private FCButton cmdDelete;
		private FCButton cmdAdd;
		//FC:FINAL:MSH - i.issue #1101: replacing menu by buttons
		private FCButton cmdTran;
		private FCButton cmdLand;
		private FCButton cmdBldg;
	}
}
