﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptLastTran.
	/// </summary>
	public partial class srptLastTran : FCSectionReport
	{
		public static srptLastTran InstancePtr
		{
			get
			{
				return (srptLastTran)Sys.GetInstance(typeof(srptLastTran));
			}
		}

		protected srptLastTran _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCode?.Dispose();
				clsAssess?.Dispose();
                clsAssess = null;
                clsAssess = null;
            }
			base.Dispose(disposing);
		}

		public srptLastTran()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptLastTran	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsCode = new clsDRWrapper();
		clsDRWrapper clsAssess = new clsDRWrapper();
		int lngCode;
		double lngLand;
		double lngbldg;
		double lngExempt;
		double lngTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsAssess.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string[] arySQL = null;
			string strTemp = "";
			string strExemption = "";
			// Call clsCode.OpenRecordset("select * from costrecord where crecordnumber between 1431 and 1439", strredatabase)
			clsCode.OpenRecordset("select * from tblTrancode order by code", modGlobalVariables.strREDatabase);
			if (Strings.Left(this.UserData + " ", 1) == "C")
			{
				strExemption = "COrrExemption";
				if (FCConvert.ToString(this.UserData).Length == 1)
				{
					clsAssess.OpenRecordset("select isnull(ritrancode,0) AS TRANCODE,count(rsaccount) as thecount,sum(cast(rllandval as bigint)) as landtot,sum(cast(rlbldgval as bigint)) as bldgtot,sum(cast(" + strExemption + " as bigint)) as exempttot from master where not rsdeleted = 1 group by convert(int,ritrancode) order by convert(int,ritrancode )", modGlobalVariables.strREDatabase);
				}
				else
				{
					arySQL = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(arySQL, 1) == 2)
					{
						strTemp = arySQL[2];
						strTemp = Strings.Replace(strTemp, "lastlandval", "rllandval", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, "lastbldgval", "rlbldgval", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, "rlexemption", "Correxemption", 1, -1, CompareConstants.vbTextCompare);
					}
					else
					{
						strTemp = "";
					}
					// Me.Tag = Mid(Me.Tag, 3)
					GroupHeader1.Visible = true;
					// strSQL = "select * from "
					// strSQL = strSQL & "(select count(rsaccount) as thecount,ritrancode from master where not rsdeleted = 1 and rscard = 1 " & arySQL(1) & " group by ritrancode ) as tblCount"
					// strSQL = strSQL & " inner join "
					// strSQL = strSQL & " (select ritrancode,sum(rllandval) as landtot,sum(rlbldgval) as bldgtot,sum(rlexemption) as exempttot from master where not rsdeleted = 1 " & arySQL(1) & " group by ritrancode " & strTemp & ") as tblmast "
					// strSQL = strSQL & " on (tblmast.ritrancode = tblcount.ritrancode) order by tblmast.ritrancode "
					strSQL = "select count(tbl1.rsaccount) as thecount,sum(cast(landtotal as bigint)) as landtot,sum(cast(bldgtotal as bigint)) as bldgtot,sum(cast(exempttotal as bigint)) as exempttot,tbl1.ritrancode AS TRANCODE from ((";
					strSQL += "select rsaccount,ritrancode from master where rscard = 1 " + arySQL[1] + ") as tbl1 inner join";
					strSQL += " (select rsaccount,sum(cast(rllandval as bigint)) as landtotal,sum(cast(rlbldgval as bigint)) as bldgtotal,sum(cast(correxemption as bigint)) as exempttotal from master where not rsdeleted = 1 group by rsaccount ";
					strSQL += strTemp;
					strSQL += " ) as tbl2 on (tbl1.rsaccount = tbl2.rsaccount)) group by tbl1.ritrancode ";
					clsAssess.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				}
			}
			else if (Strings.Left(this.UserData + " ", 1) == "L")
			{
				if (FCConvert.ToString(this.UserData).Length == 1)
				{
					clsAssess.OpenRecordset("select ritrancode as trancode,count(rsaccount) as thecount,sum(lastlandval) as landtot,sum(lastbldgval) as bldgtot,sum(rlexemption) as exempttot from master where not rsdeleted = 1 group by ritrancode order by ritrancode", modGlobalVariables.strREDatabase);
				}
				else
				{
					// Me.Tag = Mid(Me.Tag, 3)
					arySQL = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(arySQL, 1) == 2)
					{
						strTemp = arySQL[2];
					}
					else
					{
						strTemp = "";
					}
					GroupHeader1.Visible = true;
					// strSQL = "select *,tblmast.ritrancode as trancode from "
					// strSQL = strSQL & "(select count(rsaccount) as thecount,ritrancode from master where not rsdeleted = 1 and rscard = 1 " & arySQL(1) & " group by ritrancode " & strTemp & ") as tblCount"
					// strSQL = strSQL & " inner join "
					// strSQL = strSQL & " (select ritrancode,sum(lastlandval) as landtot,sum(lastbldgval) as bldgtot,sum(rlexemption) as exempttot from master where not rsdeleted = 1 " & arySQL(1) & " group by ritrancode " & strTemp & " ) as tblmast "
					// strSQL = strSQL & " on (tblmast.ritrancode = tblcount.ritrancode) order by tblmast.ritrancode "
					strSQL = "select count(theaccount) as thecount,sum(landtotal) as landtot,sum(bldgtotal) as bldgtot,sum(exempttotal) as exempttot,tbl1.ritrancode as trancode from ((";
					strSQL += "select rsaccount as theaccount,ritrancode,rsaccount from master where rscard = 1 " + arySQL[1] + ") as tbl1 inner join";
					strSQL += " (select rsaccount,sum(lastlandval) as landtotal,sum(lastbldgval) as bldgtotal,sum(rlexemption) as exempttotal from master where not rsdeleted = 1 group by rsaccount ";
					strSQL += strTemp;
					strSQL += " ) as tbl2 on (tbl1.rsaccount = tbl2.rsaccount)) group by tbl1.ritrancode";
					clsAssess.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("trancode"))));
			lngLand = Conversion.Val(clsAssess.GetData("landtot"));
			lngbldg = Conversion.Val(clsAssess.GetData("bldgtot"));
			lngExempt = Conversion.Val(clsAssess.GetData("exempttot"));
			lngTotal = lngLand - lngExempt + lngbldg;
			txtCode.Text = lngCode.ToString();
			if (lngCode == 0)
			{
				txtCode.Text = txtCode.Text + " Uncoded";
			}
			else
			{
				// Call clsCode.FindFirstRecord("crecordnumber", 1430 + lngCode)
				// Call clsCode.FindFirstRecord("code", lngCode)
				clsCode.FindFirst("code = " + FCConvert.ToString(lngCode));
				if (!clsCode.NoMatch)
				{
					// txtCode.Text = txtCode.Text & " " & clsCode.GetData("csdesc")
					txtCode.Text = txtCode.Text + " " + clsCode.Get_Fields_String("ShortDescription");
				}
				else
				{
					txtCode.Text = txtCode.Text + " Unknown";
				}
			}
			txtCount.Text = FCConvert.ToString(Conversion.Val(clsAssess.GetData("thecount")));
			txtLand.Text = Strings.Format(lngLand, "###,###,##0");
			txtBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			txtExempt.Text = Strings.Format(lngExempt, "##,###,###,##0");
			txtTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			clsAssess.MoveNext();
		}

		
	}
}
