﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmAssessExtract.
	/// </summary>
	partial class frmAssessExtract : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReports;
		public fecherFoundation.FCLabel lblReports;
		public fecherFoundation.FCComboBox cmbRorS;
		public fecherFoundation.FCLabel lblRorS;
		public FCGrid Grid;
		public fecherFoundation.FCTextBox txtTitle;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNewReport;
		public fecherFoundation.FCToolStripMenuItem mnuEditReport;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuExtract;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAssessExtract));
            this.cmbReports = new fecherFoundation.FCComboBox();
            this.lblReports = new fecherFoundation.FCLabel();
            this.cmbRorS = new fecherFoundation.FCComboBox();
            this.lblRorS = new fecherFoundation.FCLabel();
            this.Grid = new fecherFoundation.FCGrid();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNewReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExtract = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdExtract = new fecherFoundation.FCButton();
            this.cmdNewReport = new fecherFoundation.FCButton();
            this.cmdEditReport = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExtract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdExtract);
            this.BottomPanel.Location = new System.Drawing.Point(0, 617);
            this.BottomPanel.Size = new System.Drawing.Size(821, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbReports);
            this.ClientArea.Controls.Add(this.lblReports);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.txtTitle);
            this.ClientArea.Controls.Add(this.cmbRorS);
            this.ClientArea.Controls.Add(this.lblRorS);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(821, 557);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNewReport);
            this.TopPanel.Controls.Add(this.cmdEditReport);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Size = new System.Drawing.Size(821, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEditReport, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewReport, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(416, 30);
            this.HeaderText.Text = "Extract for Reports , Labels, Analysis";
            // 
            // cmbReports
            // 
            this.cmbReports.AutoSize = false;
            this.cmbReports.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReports.FormattingEnabled = true;
            this.cmbReports.Items.AddRange(new object[] {
            "All reports",
            "Only this user\'s reports"});
            this.cmbReports.Location = new System.Drawing.Point(568, 90);
            this.cmbReports.Name = "cmbReports";
            this.cmbReports.Size = new System.Drawing.Size(225, 40);
            this.cmbReports.TabIndex = 0;
            this.cmbReports.Text = "All reports";
            this.cmbReports.SelectedIndexChanged += new System.EventHandler(this.cmbReports_SelectedIndexChanged);
            // 
            // lblReports
            // 
            this.lblReports.AutoSize = true;
            this.lblReports.Location = new System.Drawing.Point(464, 104);
            this.lblReports.Name = "lblReports";
            this.lblReports.Size = new System.Drawing.Size(46, 15);
            this.lblReports.TabIndex = 1;
            this.lblReports.Text = "SHOW";
            this.lblReports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbRorS
            // 
            this.cmbRorS.AutoSize = false;
            this.cmbRorS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRorS.FormattingEnabled = true;
            this.cmbRorS.Items.AddRange(new object[] {
            "Regular",
            "From Sales Records"});
            this.cmbRorS.Location = new System.Drawing.Point(201, 90);
            this.cmbRorS.Name = "cmbRorS";
            this.cmbRorS.Size = new System.Drawing.Size(221, 40);
            this.cmbRorS.TabIndex = 6;
            this.cmbRorS.Text = "Regular";
            // 
            // lblRorS
            // 
            this.lblRorS.Location = new System.Drawing.Point(30, 104);
            this.lblRorS.Name = "lblRorS";
            this.lblRorS.Size = new System.Drawing.Size(115, 18);
            this.lblRorS.TabIndex = 7;
            this.lblRorS.Text = "TYPE OF EXTRACT";
            this.lblRorS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 150);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.ReadOnly = true;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(763, 379);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 5;
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.LinkItem = null;
            this.txtTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTitle.LinkTopic = null;
            this.txtTitle.Location = new System.Drawing.Point(201, 30);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(592, 40);
            this.txtTitle.TabIndex = 0;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(95, 18);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "EXTRACT TITLE";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNewReport,
            this.mnuEditReport,
            this.mnuSepar2,
            this.mnuDelete,
            this.mnuSepar3,
            this.mnuExtract,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNewReport
            // 
            this.mnuNewReport.Index = 0;
            this.mnuNewReport.Name = "mnuNewReport";
            this.mnuNewReport.Text = "New Report";
            this.mnuNewReport.Click += new System.EventHandler(this.mnuNewReport_Click);
            // 
            // mnuEditReport
            // 
            this.mnuEditReport.Index = 1;
            this.mnuEditReport.Name = "mnuEditReport";
            this.mnuEditReport.Text = "Edit Report";
            this.mnuEditReport.Click += new System.EventHandler(this.mnuEditReport_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 2;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 3;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Report";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 4;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuExtract
            // 
            this.mnuExtract.Index = 5;
            this.mnuExtract.Name = "mnuExtract";
            this.mnuExtract.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuExtract.Text = "Save & Continue";
            this.mnuExtract.Click += new System.EventHandler(this.mnuExtract_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 6;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 7;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdExtract
            // 
            this.cmdExtract.AppearanceKey = "acceptButton";
            this.cmdExtract.Location = new System.Drawing.Point(315, 30);
            this.cmdExtract.Name = "cmdExtract";
            this.cmdExtract.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdExtract.Size = new System.Drawing.Size(172, 48);
            this.cmdExtract.TabIndex = 0;
            this.cmdExtract.Text = "Save & Continue";
            this.cmdExtract.Click += new System.EventHandler(this.mnuExtract_Click);
            // 
            // cmdNewReport
            // 
            this.cmdNewReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewReport.AppearanceKey = "toolbarButton";
            this.cmdNewReport.Location = new System.Drawing.Point(505, 29);
            this.cmdNewReport.Name = "cmdNewReport";
            this.cmdNewReport.Size = new System.Drawing.Size(90, 24);
            this.cmdNewReport.TabIndex = 1;
            this.cmdNewReport.Text = "New Report";
            this.cmdNewReport.Click += new System.EventHandler(this.mnuNewReport_Click);
            // 
            // cmdEditReport
            // 
            this.cmdEditReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEditReport.AppearanceKey = "toolbarButton";
            this.cmdEditReport.Location = new System.Drawing.Point(601, 29);
            this.cmdEditReport.Name = "cmdEditReport";
            this.cmdEditReport.Size = new System.Drawing.Size(84, 24);
            this.cmdEditReport.TabIndex = 2;
            this.cmdEditReport.Text = "Edit Report";
            this.cmdEditReport.Click += new System.EventHandler(this.mnuEditReport_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(691, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(102, 24);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete Report";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // frmAssessExtract
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(821, 725);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAssessExtract";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Extract for Reports , Labels, Analysis";
            this.Load += new System.EventHandler(this.frmAssessExtract_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAssessExtract_KeyDown);
            this.Resize += new System.EventHandler(this.frmAssessExtract_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExtract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdExtract;
		private FCButton cmdDelete;
		private FCButton cmdEditReport;
		private FCButton cmdNewReport;
	}
}
