﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptSalesAnalysis.
	/// </summary>
	public partial class rptSalesAnalysis : BaseSectionReport
	{
		public static rptSalesAnalysis InstancePtr
		{
			get
			{
				return (rptSalesAnalysis)Sys.GetInstance(typeof(rptSalesAnalysis));
			}
		}

		protected rptSalesAnalysis _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptSalesAnalysis()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Sales Analysis Report";
		}
		// nObj = 1
		//   0	rptSalesAnalysis	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		//FC:FINAL:RPU - Use custom constructor to initialize fields
		modGlobalVariables.SalesAnalysisRec SaleRec = new modGlobalVariables.SalesAnalysisRec(0);
		int intColSortedBy;
		bool boolAscend;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                int lngUID;
                string strTemp = "";
                lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                clsTemp.OpenRecordset(
                    "select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0",
                    modGlobalVariables.strREDatabase);
                txtExtractTitle.Text = FCConvert.ToString(clsTemp.Get_Fields_String("title"));
                if (Strings.Trim(txtExtractTitle.Text) == string.Empty)
                {
                    clsTemp.OpenRecordset(
                        "select * from extract where userid = " + FCConvert.ToString(lngUID) +
                        " and reportnumber > 0 order by reportnumber", modGlobalVariables.strREDatabase);
                    strTemp = "";
                    while (!clsTemp.EndOfFile())
                    {
                        strTemp += clsTemp.Get_Fields_String("title") + "\r\n";
                        clsTemp.MoveNext();
                    }

                    if (strTemp != string.Empty)
                    {
                        strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                        txtExtractTitle.Text = strTemp;
                    }
                }

                intPage = 1;
                txtMuniName.Text = modGlobalConstants.Statics.MuniName;
                txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
                txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                SubReport1.Report = new srptSales();
                SubReport1.Report.UserData = SaleRec.MeantoUse;
                if (modGlobalVariables.Statics.SalesAnalRec.SaleFactor != 0)
                    lblTrended.Visible = true;
                if ((modGlobalVariables.Statics.SalesAnalRec.LandFactor != 1) ||
                    (modGlobalVariables.Statics.SalesAnalRec.BldgFactor != 1))
                    lblAdjusted.Visible = true;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			txtBldgFactor.Text = (SaleRec.BldgFactor * 100).ToString();
			txtDeviation.Text = Strings.Format(SaleRec.TotDeviation, "###,###,##0");
			txtLandFactor.Text = (SaleRec.LandFactor * 100).ToString();
			txtSaleFactor.Text = (SaleRec.SaleFactor * 100).ToString();
			txtSalePrice.Text = Strings.Format(SaleRec.TotSale, "###,###,###,##0");
			txtValuation.Text = Strings.Format(SaleRec.TotValuation, "###,###,###,##0");
			txtMedian.Text = Strings.Format(SaleRec.Median, "###.0000");
			txtAveDev.Text = Strings.Format(SaleRec.AveDev, "###.0000");
			txtCoOfDisp.Text = Strings.Format(SaleRec.CoeffOfDisp, "###.0000");
			txtCoOfVar.Text = Strings.Format(SaleRec.CoeffofVar, "###.0000");
			txtMean.Text = Strings.Format(SaleRec.mean, "###.0000");
			txtMidQuart.Text = Strings.Format(SaleRec.MidQuart, "##0.0000");
			txtPriceDiff.Text = Strings.Format(SaleRec.PriceDifferential, "##0.0000");
			txtStandDev.Text = Strings.Format(SaleRec.StandDev, "##0.0000");
			txtWeighted.Text = Strings.Format(SaleRec.Weighted, "##0.0000");
			switch (frmShowSalesAnalysis.InstancePtr.Grid2.Row)
			{
				case 0:
					{
						lblMedian.Text = "*" + lblMedian.Text + "*";
						break;
					}
				case 1:
					{
						lblMean.Text = "*" + lblMean.Text + "*";
						break;
					}
				case 2:
					{
						lblMidQuart.Text = "*" + lblMidQuart.Text + "*";
						break;
					}
				case 3:
					{
						lblWeighted.Text = "*" + lblWeighted.Text + "*";
						break;
					}
			}
			//end switch
		}

		public void Init(int lngSales)
		{
			SaleRec = modGlobalVariables.Statics.SalesAnalRec;
			txtNumSales.Text = lngSales.ToString();
			// Me.Show
			frmReportViewer.InstancePtr.Init(this);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		
	}
}
