﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandExtract.
	/// </summary>
	partial class frmLandExtract : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCComboBox cmbLandUnitOneColumn;
		public fecherFoundation.FCLabel lblLandUnitOneColumn;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtFilename;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCCheckBox chkColumnHeaders;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkAcres;
		public fecherFoundation.FCCheckBox chkOwner;
		public fecherFoundation.FCCheckBox chkCard;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLandExtract));
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.cmbLandUnitOneColumn = new fecherFoundation.FCComboBox();
			this.lblLandUnitOneColumn = new fecherFoundation.FCLabel();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtFilename = new fecherFoundation.FCTextBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.chkColumnHeaders = new fecherFoundation.FCCheckBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkAcres = new fecherFoundation.FCCheckBox();
			this.chkOwner = new fecherFoundation.FCCheckBox();
			this.chkCard = new fecherFoundation.FCCheckBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdCreate = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkColumnHeaders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdCreate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 477);
			this.BottomPanel.Size = new System.Drawing.Size(705, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdBrowse);
			this.ClientArea.Controls.Add(this.txtFilename);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.chkColumnHeaders);
			this.ClientArea.Controls.Add(this.cmbLandUnitOneColumn);
			this.ClientArea.Controls.Add(this.lblLandUnitOneColumn);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(705, 417);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(705, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(150, 30);
			this.HeaderText.Text = "Land Extract";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
				"All",
				"Account",
                "Map Lot",
                "Name",
				"From Extract"
			});
			this.cmbAll.Location = new System.Drawing.Point(20, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(150, 40);
			this.cmbAll.TabIndex = 12;
			this.cmbAll.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbAll, null);
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
			// 
			// cmbLandUnitOneColumn
			// 
			this.cmbLandUnitOneColumn.AutoSize = false;
			this.cmbLandUnitOneColumn.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLandUnitOneColumn.FormattingEnabled = true;
			this.cmbLandUnitOneColumn.Items.AddRange(new object[] {
                "Let land type determine format",
                "Always show in one column"
			});
			this.cmbLandUnitOneColumn.Location = new System.Drawing.Point(266, 30);
			this.cmbLandUnitOneColumn.Name = "cmbLandUnitOneColumn";
			this.cmbLandUnitOneColumn.Size = new System.Drawing.Size(278, 40);
			this.cmbLandUnitOneColumn.TabIndex = 19;
			this.cmbLandUnitOneColumn.Text = "Let land type determine format";
			this.ToolTip1.SetToolTip(this.cmbLandUnitOneColumn, null);
			// 
			// lblLandUnitOneColumn
			// 
			this.lblLandUnitOneColumn.AutoSize = true;
			this.lblLandUnitOneColumn.Location = new System.Drawing.Point(165, 44);
			this.lblLandUnitOneColumn.Name = "lblLandUnitOneColumn";
			this.lblLandUnitOneColumn.Size = new System.Drawing.Size(81, 15);
			this.lblLandUnitOneColumn.TabIndex = 20;
			this.lblLandUnitOneColumn.Text = "LAND UNITS";
			this.ToolTip1.SetToolTip(this.lblLandUnitOneColumn, null);
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(392, 418);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(75, 40);
			this.cmdBrowse.TabIndex = 18;
			this.cmdBrowse.Text = "Browse";
			this.ToolTip1.SetToolTip(this.cmdBrowse, null);
			this.cmdBrowse.Visible = false;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// txtFilename
			// 
			this.txtFilename.AutoSize = false;
			this.txtFilename.BackColor = System.Drawing.SystemColors.Window;
			this.txtFilename.LinkItem = null;
			this.txtFilename.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFilename.LinkTopic = null;
			this.txtFilename.Location = new System.Drawing.Point(130, 418);
			this.txtFilename.Name = "txtFilename";
			this.txtFilename.Size = new System.Drawing.Size(242, 40);
			this.txtFilename.TabIndex = 17;
			this.ToolTip1.SetToolTip(this.txtFilename, null);
			this.txtFilename.Visible = false;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtStart);
			this.Frame3.Controls.Add(this.cmbAll);
			this.Frame3.Controls.Add(this.txtEnd);
			this.Frame3.Controls.Add(this.lblTo);
			this.Frame3.Location = new System.Drawing.Point(30, 248);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(404, 150);
			this.Frame3.TabIndex = 15;
			this.Frame3.Text = "All / Range Of";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(20, 90);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(150, 40);
			this.txtStart.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtStart, null);
			this.txtStart.Visible = false;
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(234, 90);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(150, 40);
			this.txtEnd.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.txtEnd, null);
			this.txtEnd.Visible = false;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(194, 104);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(25, 18);
			this.lblTo.TabIndex = 16;
			this.lblTo.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo, null);
			this.lblTo.Visible = false;
			// 
			// chkColumnHeaders
			// 
			this.chkColumnHeaders.Checked = true;
			this.chkColumnHeaders.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkColumnHeaders.Location = new System.Drawing.Point(30, 201);
			this.chkColumnHeaders.Name = "chkColumnHeaders";
			this.chkColumnHeaders.Size = new System.Drawing.Size(208, 26);
			this.chkColumnHeaders.TabIndex = 5;
			this.chkColumnHeaders.Text = "Make first row column headers";
			this.ToolTip1.SetToolTip(this.chkColumnHeaders, "First row in file will be column header descriptions");
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkAcres);
			this.Frame1.Controls.Add(this.chkOwner);
			this.Frame1.Controls.Add(this.chkCard);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(115, 151);
			this.Frame1.TabIndex = 13;
			this.Frame1.Text = "Include";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkAcres
			// 
			this.chkAcres.Location = new System.Drawing.Point(20, 104);
			this.chkAcres.Name = "chkAcres";
			this.chkAcres.Size = new System.Drawing.Size(60, 26);
			this.chkAcres.TabIndex = 2;
			this.chkAcres.Text = "Acres";
			this.ToolTip1.SetToolTip(this.chkAcres, null);
			// 
			// chkOwner
			// 
			this.chkOwner.Location = new System.Drawing.Point(20, 67);
			this.chkOwner.Name = "chkOwner";
			this.chkOwner.Size = new System.Drawing.Size(65, 26);
			this.chkOwner.TabIndex = 1;
			this.chkOwner.Text = "Owner";
			this.ToolTip1.SetToolTip(this.chkOwner, null);
			// 
			// chkCard
			// 
			this.chkCard.Location = new System.Drawing.Point(20, 30);
			this.chkCard.Name = "chkCard";
			this.chkCard.Size = new System.Drawing.Size(55, 26);
			this.chkCard.TabIndex = 0;
			this.chkCard.Text = "Card";
			this.ToolTip1.SetToolTip(this.chkCard, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 432);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(62, 22);
			this.Label1.TabIndex = 19;
			this.Label1.Text = "FILENAME";
			this.ToolTip1.SetToolTip(this.Label1, null);
			this.Label1.Visible = false;
			// 
			// cmdCreate
			// 
			this.cmdCreate.AppearanceKey = "acceptButton";
			this.cmdCreate.Location = new System.Drawing.Point(301, 30);
			this.cmdCreate.Name = "cmdCreate";
			this.cmdCreate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCreate.Size = new System.Drawing.Size(120, 48);
			this.cmdCreate.TabIndex = 19;
			this.cmdCreate.Text = "Create File";
			this.ToolTip1.SetToolTip(this.cmdCreate, null);
			this.cmdCreate.Click += new System.EventHandler(this.mnuCreateFile_Click);
			// 
			// frmLandExtract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(705, 585);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLandExtract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Land Extract";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmLandExtract_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLandExtract_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkColumnHeaders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdCreate;
	}
}
