﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportJFRyan.
	/// </summary>
	public partial class frmImportJFRyan : BaseForm
	{
		public frmImportJFRyan()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportJFRyan InstancePtr
		{
			get
			{
				return (frmImportJFRyan)Sys.GetInstance(typeof(frmImportJFRyan));
			}
		}

		protected frmImportJFRyan _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTCOLPROPID = 0;
		const int CNSTCOLMAP = 1;
		const int CNSTCOLPARCEL = 2;
		const int CNSTCOLSUFFIX = 3;
		const int CNSTCOLUNIT = 4;
		const int CNSTCOLTYPE = 5;
		const int CNSTCOLNAME1 = 6;
		const int CNSTCOLNAME2 = 7;
		const int CNSTCOLHOUSE = 8;
		const int CNSTCOLSTREET = 9;
		const int CNSTCOLADDRESS1 = 10;
		const int CNSTCOLCITY = 11;
		const int CNSTCOLSTATE = 12;
		const int CNSTCOLZIP = 13;
		const int CNSTCOLLAND = 14;
		const int CNSTCOLBLDG = 15;
		const int CNSTCOLOUTBLDG = 16;
		const int CNSTCOLTOTAL = 17;
		const int CNSTCOLHOMESTEAD = 18;
		const int CNSTCOLVETERAN = 19;
		const int CNSTCOLBLIND = 20;
		const int CNSTCOLPARAPLEGIC = 21;
		const int CNSTCOLPARSONAGE = 22;
		private int[] aryExempts = new int[5 + 1];

		private struct ExemptConversion
		{
			public short Homestead;
			public short Veteran;
			public short Blind;
			public short Paraplegic;
			public short Parsonage;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public ExemptConversion(int unusedParam)
			{
				this.Homestead = 0;
				this.Veteran = 0;
				this.Blind = 0;
				this.Paraplegic = 0;
				this.Parsonage = 0;
			}
		};
		//FC:FINAL:RPU - Use custom constructor to initialize fields
		private ExemptConversion tExempts = new ExemptConversion(0);
		PartyUtil tPu = new PartyUtil();
		private object[] arParties = null;

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			BrowseForFile();
		}

		private void BrowseForFile()
		{
			string strFile = "";
			try
			{
				// On Error GoTo ErrorHandler
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNPathMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNFileMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.Filter = "All Files|*.*";
				MDIParent.InstancePtr.CommonDialog1.FilterIndex = 0;
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFile = Strings.Trim(MDIParent.InstancePtr.CommonDialog1.FileName);
				if (strFile != string.Empty)
				{
					txtFilename.Text = strFile;
					btnImport.Enabled = true;
				}
				else
				{
					btnImport.Enabled = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In BrowseForFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void btnExit_Click(object sender, System.EventArgs e)
		{
			LeaveForm();
		}

		private void LeaveForm()
		{
			this.Unload();
		}

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			ImportFile();
		}

		private void SetupExempts()
		{
			tExempts.Homestead = 1;
			tExempts.Veteran = 2;
			tExempts.Blind = 3;
			tExempts.Paraplegic = 4;
			tExempts.Parsonage = 5;
		}

		private void ImportFile()
		{
			if (Strings.Trim(txtFilename.Text) == "")
			{
				MessageBox.Show("You must specify an import file to continue", "Invalid Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!System.IO.File.Exists(txtFilename.Text))
			{
				MessageBox.Show("The specified file could not be found", "Invalid Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			SetupExempts();
			ImportJFRyan(Strings.Trim(txtFilename.Text));
		}

		private void SetupForImport()
		{
			btnImport.Enabled = false;
			framFile.Enabled = false;
			framProgress.Visible = true;
			lblAccount.Text = "";
		}

		private void ResetForm()
		{
			btnImport.Enabled = true;
			framFile.Enabled = true;
			framProgress.Visible = false;
		}

		private void UpdateProgress(int lngAcct)
		{
			lblAccount.Text = FCConvert.ToString(lngAcct);
			lblAccount.Refresh();
			//Application.DoEvents();
		}

		private void ImportJFRyan(string strFileName)
		{
			StreamReader ts = null;
			string strRecord = "";
			string[] strAry = null;
			int lngAcct = 0;
			cCreateGUID tGuid = new cCreateGUID();
			string strZip = "";
			string strZip4 = "";
			string strZipCode = "";
			string strMapLot = "";
			double dblLand = 0;
			double dblBldg = 0;
			double dblOut;
			double dblExemption = 0;
			int intEx = 0;
			cPartyController tPC = new cPartyController();
			// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
			cParty tParty = new cParty();
			bool boolFileOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				cParty oParty = new cParty();
				cParty soParty = new cParty();
				UtilParty uParty = new UtilParty();
				// vbPorter upgrade warning: tVar As object	OnRead(UtilParty)
				UtilParty[] tVar = null;
				string strOwner1 = "";
				string strOwner2 = "";
				string strAddress1 = "";
				string strAddress2 = "";
				string strCity = "";
				string strState = "";
				cPartyAddress tAddr = null;
				if (System.IO.File.Exists(strFileName))
				{
					ts = System.IO.File.OpenText(strFileName/*, IOMode.ForReading, false, Tristate.TristateUseDefault*/);
					if (!(ts == null))
					{
						SetupForImport();
						boolFileOpen = true;
						clsDRWrapper rsSave = new clsDRWrapper();
						// mark everything as deleted?
						rsSave.Execute("update master set rsdeleted = 1", modGlobalVariables.strREDatabase);
						rsSave.OpenRecordset("select * from master where rscard = 1 order by rsaccount", modGlobalVariables.strREDatabase);
						strRecord = ts.ReadLine();
						// column headers
						while (!ts.EndOfStream)
						{
							strRecord = ts.ReadLine();
							strAry = Strings.Split(strRecord, "\t", -1, CompareConstants.vbBinaryCompare);
							if (Information.UBound(strAry, 1) > 0)
							{
								if (Information.UBound(strAry, 1) < 21)
								{
									MessageBox.Show("Error in record", "Bad Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									boolFileOpen = false;
									ts.Close();
									ResetForm();
									return;
								}
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[CNSTCOLPROPID])));
								if (lngAcct > 0)
								{
									UpdateProgress(lngAcct);
									if (rsSave.FindFirstRecord("rsaccount", lngAcct))
									{
										rsSave.Edit();
									}
									else
									{
										rsSave.AddNew();
										rsSave.Set_Fields("rsaccount", lngAcct);
										rsSave.Set_Fields("rscard", 1);
										rsSave.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
										rsSave.Set_Fields("AccountID", tGuid.CreateGUID());
										rsSave.Set_Fields("CardID", rsSave.Get_Fields_String("AccountID"));
									}
									rsSave.Set_Fields("rsdeleted", false);
									rsSave.Set_Fields("hlupdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
									tParty = tPC.GetParty(rsSave.Get_Fields_Int32("ownerpartyid"));
									strOwner1 = Strings.Trim(strAry[CNSTCOLNAME1]);
									strOwner2 = Strings.Trim(strAry[CNSTCOLNAME2]);
									strAddress1 = Strings.Trim(strAry[CNSTCOLADDRESS1]);
									strAddress2 = "";
									strCity = Strings.Trim(strAry[CNSTCOLCITY]);
									strState = Strings.Trim(strAry[CNSTCOLSTATE]);
									strZip = Strings.Trim(strAry[CNSTCOLZIP]);
									if (!(tParty == null))
									{
										if (tParty.ID > 0)
										{
											if (Strings.LCase(tParty.FullNameLastFirst) != Strings.LCase(strOwner1))
											{
												rsSave.Set_Fields("Ownerpartyid", 0);
												rsSave.Set_Fields("secownerpartyid", 0);
												oParty.Clear();
												oParty.DateCreated = Convert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
												oParty.PartyGUID = tGuid.CreateGUID();
												oParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
												soParty.Clear();
												soParty.DateCreated = oParty.DateCreated;
												soParty.PartyGUID = tGuid.CreateGUID();
												soParty.CreatedBy = oParty.CreatedBy;
												if (Strings.Trim(strOwner2) == "")
												{
													tVar = tPu.SplitName(strOwner1, true);
													if (Information.UBound(tVar, 1) >= 0)
													{
														if (Information.UBound(tVar, 1) > 0)
														{
															uParty = tVar[1];
															soParty.Designation = uParty.Designation;
															soParty.FirstName = uParty.FirstName;
															soParty.MiddleName = uParty.MiddleName;
															soParty.LastName = uParty.LastName;
															soParty.PartyType = uParty.PartyType;
														}
														uParty = tVar[0];
														oParty.Designation = uParty.Designation;
														oParty.FirstName = uParty.FirstName;
														oParty.MiddleName = uParty.MiddleName;
														oParty.LastName = uParty.LastName;
														oParty.PartyType = uParty.PartyType;
													}
												}
												else
												{
													tVar = tPu.SplitName(strOwner1, false);
													if (Information.UBound(tVar, 1) >= 0)
													{
														uParty = tVar[0];
														oParty.Designation = uParty.Designation;
														oParty.FirstName = uParty.FirstName;
														oParty.MiddleName = uParty.MiddleName;
														oParty.LastName = uParty.LastName;
														oParty.PartyType = uParty.PartyType;
													}
													tVar = tPu.SplitName(strOwner2, false);
													if (Information.UBound(tVar, 1) >= 0)
													{
														uParty = tVar[0];
														soParty.Designation = uParty.Designation;
														soParty.FirstName = uParty.FirstName;
														soParty.MiddleName = uParty.MiddleName;
														soParty.LastName = uParty.LastName;
														soParty.PartyType = uParty.PartyType;
													}
												}
												if (oParty.FirstName != "" || oParty.LastName != "")
												{
													tAddr.Address1 = strAddress1;
													tAddr.Address2 = strAddress2;
													tAddr.Address3 = "";
													tAddr.City = strCity;
													tAddr.State = strState;
													tAddr.Zip = strZip;
													tAddr.AddressType = "Primary";
													oParty.Addresses.Add(tAddr);
													tPC.SaveParty(ref oParty, false);
													rsSave.Set_Fields("ownerpartyid", oParty.ID);
													Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
													arParties[Information.UBound(arParties)] = oParty.ID;
												}
												if (soParty.FirstName != "" || soParty.LastName != "")
												{
													tPC.SaveParty(ref soParty, true);
													rsSave.Set_Fields("secownerpartyid", soParty.ID);
													Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
													arParties[Information.UBound(arParties)] = soParty.ID;
												}
											}
										}
									}
									// rsSave.Fields("rsname") = Trim(strAry(CNSTCOLNAME1))
									// rsSave.Fields("rssecowner") = Trim(strAry(CNSTCOLNAME2))
									// rsSave.Fields("rsaddr1") = Trim(strAry(CNSTCOLADDRESS1))
									// rsSave.Fields("rsaddr2") = ""
									// rsSave.Fields("rsaddr3") = Trim(strAry(CNSTCOLCITY))
									// rsSave.Fields("rsstate") = Trim(strAry(CNSTCOLSTATE))
									// strZipCode = Trim(strAry(CNSTCOLZIP))
									// Call ParseZip(strZipCode, strZip, strZip4)
									// rsSave.Fields("rszip") = strZip
									// rsSave.Fields("rszip4") = strZip4
									rsSave.Set_Fields("rslocnumalph", Strings.Trim(strAry[CNSTCOLHOUSE]));
									rsSave.Set_Fields("rslocstreet", Strings.Trim(strAry[CNSTCOLSTREET]));
									strMapLot = "";
									strMapLot = strAry[CNSTCOLMAP] + "-" + strAry[CNSTCOLSUFFIX];
									if (Strings.Trim(strAry[CNSTCOLUNIT]) != "" && strAry[CNSTCOLUNIT] != "000")
									{
										strMapLot += "-" + strAry[CNSTCOLUNIT];
									}
									rsSave.Set_Fields("rsmaplot", strMapLot);
									dblLand = Conversion.Val(strAry[CNSTCOLLAND]);
									dblBldg = Conversion.Val(strAry[CNSTCOLBLDG]) + Conversion.Val(strAry[CNSTCOLOUTBLDG]);
									rsSave.Set_Fields("lastlandval", dblLand);
									rsSave.Set_Fields("rllandval", dblLand);
									rsSave.Set_Fields("rsothervalue", dblLand);
									rsSave.Set_Fields("lastbldgval", dblBldg);
									rsSave.Set_Fields("rlbldgval", dblBldg);
									rsSave.Set_Fields("rssoftvalue", 0);
									rsSave.Set_Fields("rsmixedvalue", 0);
									rsSave.Set_Fields("rshardvalue", 0);
									dblExemption = 0;
									dblExemption = Conversion.Val(strAry[CNSTCOLHOMESTEAD]) + Conversion.Val(strAry[CNSTCOLVETERAN]) + Conversion.Val(strAry[CNSTCOLBLIND]) + Conversion.Val(strAry[CNSTCOLPARAPLEGIC]) + Conversion.Val(strAry[CNSTCOLPARSONAGE]);
									rsSave.Set_Fields("rlexemption", dblExemption);
									rsSave.Set_Fields("exemptpct1", 100);
									rsSave.Set_Fields("exemptpct2", 100);
									rsSave.Set_Fields("exemptpct3", 100);
									intEx = 1;
									rsSave.Set_Fields("hashomestead", false);
									rsSave.Set_Fields("homesteadvalue", 0);
									rsSave.Set_Fields("riexemptcd1", 0);
									rsSave.Set_Fields("exemptval1", 0);
									rsSave.Set_Fields("riexemptcd2", 0);
									rsSave.Set_Fields("exemptval2", 0);
									rsSave.Set_Fields("riexemptcd3", 0);
									rsSave.Set_Fields("exemptval3", 0);
									if (Conversion.Val(strAry[CNSTCOLHOMESTEAD]) > 0)
									{
										rsSave.Set_Fields("riexemptcd" + intEx, tExempts.Homestead);
										rsSave.Set_Fields("exemptval" + intEx, FCConvert.ToString(Conversion.Val(strAry[CNSTCOLHOMESTEAD])));
										rsSave.Set_Fields("hashomestead", true);
										rsSave.Set_Fields("homesteadvalue", FCConvert.ToString(Conversion.Val(strAry[CNSTCOLHOMESTEAD])));
										intEx += 1;
									}
									if (Conversion.Val(strAry[CNSTCOLVETERAN]) > 0)
									{
										rsSave.Set_Fields("riexemptcd" + intEx, tExempts.Veteran);
										rsSave.Set_Fields("exemptval" + intEx, FCConvert.ToString(Conversion.Val(strAry[CNSTCOLVETERAN])));
										intEx += 1;
									}
									if (Conversion.Val(strAry[CNSTCOLBLIND]) > 0)
									{
										rsSave.Set_Fields("riexemptcd" + intEx, tExempts.Blind);
										rsSave.Set_Fields("exemptval" + intEx, FCConvert.ToString(Conversion.Val(strAry[CNSTCOLBLIND])));
										intEx += 1;
									}
									if (Conversion.Val(strAry[CNSTCOLPARAPLEGIC]) > 0)
									{
										if (intEx < 4)
										{
											rsSave.Set_Fields("riexemptcd" + intEx, tExempts.Paraplegic);
											rsSave.Set_Fields("exemptval" + intEx, FCConvert.ToString(Conversion.Val(strAry[CNSTCOLPARAPLEGIC])));
											intEx += 1;
										}
										else
										{
											MessageBox.Show("Too many exemptions for account " + FCConvert.ToString(lngAcct) + "\r\n" + "You will need to manually correct the exempt amounts.", "Bad Exemption", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										}
									}
									if (Conversion.Val(strAry[CNSTCOLPARSONAGE]) > 0)
									{
										if (intEx < 4)
										{
											rsSave.Set_Fields("riexemptcd" + intEx, tExempts.Parsonage);
											rsSave.Set_Fields("exemptval" + intEx, FCConvert.ToString(Conversion.Val(strAry[CNSTCOLPARSONAGE])));
											intEx += 1;
										}
										else
										{
											MessageBox.Show("Too many exemptions for account " + FCConvert.ToString(lngAcct) + "\r\n" + "You will need to manually correct the exempt amounts.", "Bad Exemption", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										}
									}
									rsSave.Update();
								}
								else
								{
									MessageBox.Show("0 property id error", "Record Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									boolFileOpen = false;
									ts.Close();
									ResetForm();
									return;
								}
							}
						}
						boolFileOpen = false;
						ts.Close();
					}
					else
					{
						MessageBox.Show("Could not open file " + strFileName, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						ResetForm();
						return;
					}
				}
				else
				{
					ResetForm();
					return;
				}
				ResetForm();
				MessageBox.Show("Finished importing", "Import Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// If UBound(arParties) > 0 Then
				// If MsgBox(UBound(arParties) & " New parties added" & vbNewLine & "Would you like to check for duplicates now?" & vbNewLine & "This can also be done at any time later.", vbYesNo + vbQuestion, "Check for Duplicates") = vbYes Then
				// lblProgress.Caption = "Loading Party information"
				// lblAccount.Caption = ""
				// lblProgress.Refresh
				// DoEvents
				// Call tPu.GetCondensedPartiesHintsFromListAsynchronously(arParties())
				// End If
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ResetForm();
				if (boolFileOpen)
				{
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Import", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddBookPage(ref int strBook, ref int strPage, ref int lngAcct)
		{
			clsDRWrapper rsMisc = new clsDRWrapper();
			bool boolFound = false;
			int intLine;
			if (strBook == FCConvert.ToDouble("") || strPage == FCConvert.ToDouble(""))
			{
				return;
			}
			intLine = 1;
			rsMisc.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct) + " order by line", modGlobalVariables.strREDatabase);
			while (!rsMisc.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsMisc.Get_Fields("book")) == strBook && FCConvert.ToInt32(rsMisc.Get_Fields("page")) == strPage)
				{
					boolFound = true;
				}
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				intLine = FCConvert.ToInt32(rsMisc.Get_Fields("line"));
				rsMisc.MoveNext();
			}
			if (!boolFound && strBook != FCConvert.ToDouble("") && strPage != FCConvert.ToDouble(""))
			{
				rsMisc.AddNew();
				rsMisc.Set_Fields("account", lngAcct);
				rsMisc.Set_Fields("card", 1);
				rsMisc.Set_Fields("line", intLine);
				rsMisc.Set_Fields("book", strBook);
				rsMisc.Set_Fields("page", strPage);
				rsMisc.Set_Fields("current", true);
				rsMisc.Set_Fields("saledate", 0);
				rsMisc.Update();
			}
		}

		private void ParseZip(string strZipCode, ref string strZip, ref string strZip4)
		{
			strZip = "";
			strZip4 = "";
			if (Strings.Trim(strZipCode) != "")
			{
				string[] strAry = null;
				strAry = Strings.Split(Strings.Trim(strZipCode), "-", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strAry, 1) > 0)
				{
					strZip = strAry[0];
					strZip4 = strAry[1];
				}
				else
				{
					strAry = Strings.Split(Strings.Trim(strZipCode), " ", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(strAry, 1) > 0)
					{
						strZip = strAry[0];
						strZip4 = strAry[1];
					}
					else
					{
						strZip = Strings.Trim(strZipCode);
					}
				}
			}
		}

		private void frmImportJFRyan_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportJFRyan_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportJFRyan properties;
			//frmImportJFRyan.FillStyle	= 0;
			//frmImportJFRyan.ScaleWidth	= 5880;
			//frmImportJFRyan.ScaleHeight	= 4050;
			//frmImportJFRyan.LinkTopic	= "Form2";
			//frmImportJFRyan.LockControls	= true;
			//frmImportJFRyan.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			tPu = new PartyUtil();
		}

		private void tPu_UpdateMessage(double dblProgress, string strMessage)
		{
			lblProgress.Text = strMessage;
			lblAccount.Text = "";
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			LeaveForm();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
