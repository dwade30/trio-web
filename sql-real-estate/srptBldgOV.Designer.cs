﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptBldgOV.
	/// </summary>
	partial class srptBldgOV
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBldgOV));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtBldgCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtcount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtamount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtave = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtamount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtBldgCode,
				this.txtcount,
				this.txtamount,
				this.txtave
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtBldgCode
			// 
			this.txtBldgCode.Height = 0.19F;
			this.txtBldgCode.Left = 0.0625F;
			this.txtBldgCode.Name = "txtBldgCode";
			this.txtBldgCode.Style = "font-family: \'Tahoma\'";
			this.txtBldgCode.Text = "Field1";
			this.txtBldgCode.Top = 0.03125F;
			this.txtBldgCode.Width = 1.9375F;
			// 
			// txtcount
			// 
			this.txtcount.Height = 0.19F;
			this.txtcount.Left = 3.625F;
			this.txtcount.Name = "txtcount";
			this.txtcount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtcount.Text = "Field2";
			this.txtcount.Top = 0.03125F;
			this.txtcount.Width = 0.6875F;
			// 
			// txtamount
			// 
			this.txtamount.Height = 0.19F;
			this.txtamount.Left = 4.5F;
			this.txtamount.Name = "txtamount";
			this.txtamount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtamount.Text = "Field3";
			this.txtamount.Top = 0.03125F;
			this.txtamount.Width = 1.6875F;
			// 
			// txtave
			// 
			this.txtave.Height = 0.19F;
			this.txtave.Left = 6.25F;
			this.txtave.Name = "txtave";
			this.txtave.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtave.Text = "Field4";
			this.txtave.Top = 0.03125F;
			this.txtave.Width = 1.125F;
			// 
			// srptBldgOV
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtamount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtcount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtamount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtave;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
