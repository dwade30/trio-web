﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptREService.
	/// </summary>
	public partial class rptREService : BaseSectionReport
	{
		public static rptREService InstancePtr
		{
			get
			{
				return (rptREService)Sys.GetInstance(typeof(rptREService));
			}
		}

		protected rptREService _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptREService()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptREService	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string[] strAryDesc = null;
		string[] strAryType = null;
		string[] strarySize = null;
		int intCounter;

		public void Init(ref string strOutput, bool modalDialog)
		{
			string[] strAry = null;
			string[] strAryDet = null;
			int x;
			int y;
			strAryDesc = new string[1 + 1];
			strAryType = new string[1 + 1];
			strarySize = new string[1 + 1];
			try
			{
				// On Error GoTo ErrorHandler
				strAry = Strings.Split(strOutput, "\r\n", -1, CompareConstants.vbBinaryCompare);
				for (x = 0; x <= Information.UBound(strAry, 1); x++)
				{
					if (Information.UBound(strAryDesc, 1) < x)
					{
						Array.Resize(ref strAryDesc, x + 1);
						Array.Resize(ref strAryType, x + 1);
						Array.Resize(ref strarySize, x + 1);
					}
					strAryDet = Strings.Split(strAry[x], "\t", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(strAryDet, 1) == 2)
					{
						strAryDesc[x] = strAryDet[0];
						strAryType[x] = strAryDet[1];
						strarySize[x] = strAryDet[2];
					}
				}
				// x
				intCounter = 0;
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (intCounter > Information.UBound(strAryDesc, 1))
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intCounter <= Information.UBound(strAryDesc, 1))
			{
				txtDescription.Text = strAryDesc[intCounter];
				txtType.Text = strAryType[intCounter];
				txtSize.Text = strarySize[intCounter];
				intCounter += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
