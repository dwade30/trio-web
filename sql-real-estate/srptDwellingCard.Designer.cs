﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwellingCard.
	/// </summary>
	partial class srptDwellingCard
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDwellingCard));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStyleCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLayoutCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayoutCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStoriesCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStoriesCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRoofCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOpen3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOpen4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFoundationCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundationCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBasementCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasementCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOpen5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHeatCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtHeatCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCoolCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtKitchenCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchenCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWetCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBathCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAtticCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtInsulationCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGradeCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtConditionCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFunctionalCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctionalCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEconomicCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomicCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEntranceCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtInformationCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtHeatCost11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyleCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtDateInspected = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStories = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExterior = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoof = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMasonry = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemodel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBsmtGar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayout = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAttic = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnfinished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysPct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFuncPct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFuncCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconPct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntrance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSFBsmtLiv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinBsmtGrade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCool = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRooms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBedrooms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBaths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHalfBaths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFixtures = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFirePlaces = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatPct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolPct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateInspected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExterior)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoof)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMasonry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBsmtGar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttic)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysPct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFuncPct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFuncCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconPct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntrance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSFBsmtLiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinBsmtGrade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCool)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRooms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBedrooms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfBaths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFixtures)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirePlaces)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatPct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolPct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtStyleCost1,
				this.txtStyleCost2,
				this.txtStyleCost3,
				this.txtStyleCost10,
				this.txtStyleCost11,
				this.txtStyleCost9,
				this.txtStyleCost5,
				this.txtStyleCost6,
				this.txtStyleCost7,
				this.Label2,
				this.Label3,
				this.txtLayoutCost1,
				this.txtLayoutCost2,
				this.txtLayoutCost3,
				this.txtLayoutCost8,
				this.txtLayoutCost9,
				this.txtLayoutCost7,
				this.txtLayoutCost4,
				this.txtLayoutCost5,
				this.txtLayoutCost6,
				this.Label4,
				this.Label5,
				this.Label6,
				this.txtStoriesCost1,
				this.txtStoriesCost2,
				this.txtStoriesCost3,
				this.txtStoriesCost8,
				this.txtStoriesCost9,
				this.txtStoriesCost7,
				this.txtStoriesCost4,
				this.txtStoriesCost5,
				this.txtStoriesCost6,
				this.Label7,
				this.Label8,
				this.txtRoofCost1,
				this.txtRoofCost2,
				this.txtRoofCost3,
				this.txtRoofCost8,
				this.txtRoofCost9,
				this.txtRoofCost7,
				this.txtRoofCost4,
				this.txtRoofCost5,
				this.txtRoofCost6,
				this.Label9,
				this.lblOpen3,
				this.lblOpen4,
				this.Label12,
				this.Label13,
				this.Label14,
				this.txtFoundationCost1,
				this.txtFoundationCost2,
				this.txtFoundationCost3,
				this.txtFoundationCost8,
				this.txtFoundationCost9,
				this.txtFoundationCost7,
				this.txtFoundationCost4,
				this.txtFoundationCost5,
				this.txtFoundationCost6,
				this.Label15,
				this.txtBasementCost1,
				this.txtBasementCost2,
				this.txtBasementCost3,
				this.txtBasementCost8,
				this.txtBasementCost9,
				this.txtBasementCost7,
				this.txtBasementCost4,
				this.txtBasementCost5,
				this.txtBasementCost6,
				this.Label16,
				this.Label17,
				this.lblOpen5,
				this.Label19,
				this.txtHeatCost1,
				this.txtHeatCost2,
				this.txtHeatCost3,
				this.txtHeatCost7,
				this.txtHeatCost8,
				this.txtHeatCost9,
				this.Line15,
				this.txtHeatCost4,
				this.txtHeatCost10,
				this.txtHeatCost5,
				this.Label20,
				this.txtCoolCost1,
				this.txtCoolCost2,
				this.txtCoolCost3,
				this.txtCoolCost6,
				this.txtCoolCost7,
				this.txtCoolCost8,
				this.txtCoolCost4,
				this.txtCoolCost9,
				this.txtCoolCost5,
				this.Label21,
				this.txtKitchenCost1,
				this.txtKitchenCost2,
				this.txtKitchenCost3,
				this.txtKitchenCost6,
				this.txtKitchenCost7,
				this.txtKitchenCost8,
				this.txtKitchenCost4,
				this.txtKitchenCost9,
				this.txtKitchenCost5,
				this.Label22,
				this.txtWetCost1,
				this.txtWetCost2,
				this.txtWetCost3,
				this.txtWetCost8,
				this.txtWetCost9,
				this.txtWetCost7,
				this.txtWetCost4,
				this.txtWetCost5,
				this.txtWetCost6,
				this.Label23,
				this.txtBathCost1,
				this.txtBathCost2,
				this.txtBathCost3,
				this.txtBathCost6,
				this.txtBathCost7,
				this.txtBathCost8,
				this.txtBathCost4,
				this.txtBathCost9,
				this.txtBathCost5,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.txtAtticCost1,
				this.txtAtticCost2,
				this.txtAtticCost3,
				this.txtAtticCost8,
				this.txtAtticCost9,
				this.txtAtticCost7,
				this.txtAtticCost4,
				this.txtAtticCost5,
				this.txtAtticCost6,
				this.Label31,
				this.txtInsulationCost1,
				this.txtInsulationCost2,
				this.txtInsulationCost3,
				this.txtInsulationCost8,
				this.txtInsulationCost9,
				this.txtInsulationCost7,
				this.txtInsulationCost4,
				this.txtInsulationCost5,
				this.txtInsulationCost6,
				this.Label32,
				this.Label33,
				this.txtGradeCost1,
				this.txtGradeCost2,
				this.txtGradeCost3,
				this.txtGradeCost8,
				this.txtGradeCost9,
				this.txtGradeCost7,
				this.txtGradeCost4,
				this.txtGradeCost5,
				this.txtGradeCost6,
				this.Label34,
				this.Label35,
				this.txtConditionCost1,
				this.txtConditionCost2,
				this.txtConditionCost3,
				this.txtConditionCost8,
				this.txtConditionCost9,
				this.txtConditionCost7,
				this.txtConditionCost4,
				this.txtConditionCost5,
				this.txtConditionCost6,
				this.Label36,
				this.Label37,
				this.Label38,
				this.txtFunctionalCost1,
				this.txtFunctionalCost2,
				this.txtFunctionalCost3,
				this.txtFunctionalCost8,
				this.txtFunctionalCost9,
				this.txtFunctionalCost7,
				this.txtFunctionalCost4,
				this.txtFunctionalCost5,
				this.txtFunctionalCost6,
				this.Label39,
				this.Label40,
				this.txtEconomicCost1,
				this.txtEconomicCost2,
				this.txtEconomicCost3,
				this.txtEconomicCost8,
				this.txtEconomicCost9,
				this.txtEconomicCost7,
				this.txtEconomicCost4,
				this.txtEconomicCost5,
				this.txtEconomicCost6,
				this.Label41,
				this.txtEntranceCost1,
				this.txtEntranceCost2,
				this.txtEntranceCost3,
				this.txtEntranceCost8,
				this.txtEntranceCost9,
				this.txtEntranceCost7,
				this.txtEntranceCost4,
				this.txtEntranceCost5,
				this.txtEntranceCost6,
				this.Label42,
				this.txtInformationCost1,
				this.txtInformationCost2,
				this.txtInformationCost3,
				this.txtInformationCost8,
				this.txtInformationCost9,
				this.txtInformationCost7,
				this.txtInformationCost4,
				this.txtInformationCost5,
				this.txtInformationCost6,
				this.Label43,
				this.Line2,
				this.txtHeatCost11,
				this.txtStyleCost4,
				this.txtStyleCost12,
				this.txtStyleCost8,
				this.txtExteriorCost1,
				this.txtExteriorCost2,
				this.txtExteriorCost3,
				this.txtExteriorCost10,
				this.txtExteriorCost11,
				this.txtExteriorCost9,
				this.txtExteriorCost5,
				this.txtExteriorCost6,
				this.txtExteriorCost7,
				this.txtExteriorCost4,
				this.txtExteriorCost12,
				this.txtExteriorCost8,
				this.txtHeatCost6,
				this.txtHeatCost12,
				this.Line1,
				this.txtDateInspected,
				this.txtStyle,
				this.txtUnits,
				this.txtOtherUnits,
				this.txtStories,
				this.txtExterior,
				this.txtRoof,
				this.txtMasonry,
				this.txtOpen3,
				this.txtOpen4,
				this.txtYear,
				this.txtRemodel,
				this.txtFoundation,
				this.txtBasement,
				this.txtBsmtGar,
				this.txtWet,
				this.txtLayout,
				this.txtAttic,
				this.txtInsulation,
				this.txtUnfinished,
				this.txtGradeFactor,
				this.txtSqft,
				this.txtCondition,
				this.txtPhysPct,
				this.txtFuncPct,
				this.txtFuncCode,
				this.txtEconPct,
				this.txtEconCode,
				this.txtEntrance,
				this.txtInformation,
				this.txtSFBsmtLiv,
				this.txtFinBsmtGrade,
				this.txtHeat,
				this.txtCool,
				this.txtKitchen,
				this.txtBath,
				this.txtRooms,
				this.txtBedrooms,
				this.txtBaths,
				this.txtHalfBaths,
				this.txtFixtures,
				this.txtFirePlaces,
				this.txtOpen5,
				this.txtHeatPct,
				this.txtCoolPct,
				this.Line16,
				this.Line14,
				this.Line13,
				this.Line12,
				this.Line11,
				this.Line10,
				this.Line9,
				this.Line8,
				this.Line7,
				this.Line6,
				this.Line5,
				this.Line4,
				this.Line3,
				this.Line17,
				this.Line18,
				this.Line19,
				this.Line25,
				this.Line26,
				this.Line27,
				this.Line28,
				this.Line29,
				this.Line30,
				this.Line24,
				this.Line32,
				this.Line33,
				this.Line34,
				this.Line35,
				this.Line36,
				this.Line37,
				this.Line38,
				this.Line39,
				this.Line40,
				this.Line41,
				this.Line42,
				this.Line43,
				this.Line44,
				this.Line45,
				this.Line46,
				this.Image1,
				this.Line31,
				this.Line23
			});
			this.Detail.Height = 5.125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.125F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 6pt";
			this.Label1.Text = "Building Style";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.5625F;
			// 
			// txtStyleCost1
			// 
			this.txtStyleCost1.CanGrow = false;
			this.txtStyleCost1.Height = 0.125F;
			this.txtStyleCost1.Left = 0F;
			this.txtStyleCost1.Name = "txtStyleCost1";
			this.txtStyleCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost1.Text = "1.";
			this.txtStyleCost1.Top = 0.125F;
			this.txtStyleCost1.Width = 0.5F;
			// 
			// txtStyleCost2
			// 
			this.txtStyleCost2.CanGrow = false;
			this.txtStyleCost2.Height = 0.125F;
			this.txtStyleCost2.Left = 0F;
			this.txtStyleCost2.Name = "txtStyleCost2";
			this.txtStyleCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost2.Text = "2.";
			this.txtStyleCost2.Top = 0.25F;
			this.txtStyleCost2.Width = 0.5F;
			// 
			// txtStyleCost3
			// 
			this.txtStyleCost3.CanGrow = false;
			this.txtStyleCost3.Height = 0.125F;
			this.txtStyleCost3.Left = 0F;
			this.txtStyleCost3.Name = "txtStyleCost3";
			this.txtStyleCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost3.Text = "3.";
			this.txtStyleCost3.Top = 0.375F;
			this.txtStyleCost3.Width = 0.5F;
			// 
			// txtStyleCost10
			// 
			this.txtStyleCost10.CanGrow = false;
			this.txtStyleCost10.Height = 0.125F;
			this.txtStyleCost10.Left = 1.125F;
			this.txtStyleCost10.Name = "txtStyleCost10";
			this.txtStyleCost10.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost10.Text = "10";
			this.txtStyleCost10.Top = 0.25F;
			this.txtStyleCost10.Width = 0.5F;
			// 
			// txtStyleCost11
			// 
			this.txtStyleCost11.CanGrow = false;
			this.txtStyleCost11.Height = 0.125F;
			this.txtStyleCost11.Left = 1.125F;
			this.txtStyleCost11.Name = "txtStyleCost11";
			this.txtStyleCost11.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost11.Text = "11";
			this.txtStyleCost11.Top = 0.375F;
			this.txtStyleCost11.Width = 0.5F;
			// 
			// txtStyleCost9
			// 
			this.txtStyleCost9.CanGrow = false;
			this.txtStyleCost9.Height = 0.125F;
			this.txtStyleCost9.Left = 1.125F;
			this.txtStyleCost9.Name = "txtStyleCost9";
			this.txtStyleCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost9.Text = "9";
			this.txtStyleCost9.Top = 0.125F;
			this.txtStyleCost9.Width = 0.5F;
			// 
			// txtStyleCost5
			// 
			this.txtStyleCost5.CanGrow = false;
			this.txtStyleCost5.Height = 0.125F;
			this.txtStyleCost5.Left = 0.5625F;
			this.txtStyleCost5.Name = "txtStyleCost5";
			this.txtStyleCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost5.Text = "5.";
			this.txtStyleCost5.Top = 0.125F;
			this.txtStyleCost5.Width = 0.5F;
			// 
			// txtStyleCost6
			// 
			this.txtStyleCost6.CanGrow = false;
			this.txtStyleCost6.Height = 0.125F;
			this.txtStyleCost6.Left = 0.5625F;
			this.txtStyleCost6.Name = "txtStyleCost6";
			this.txtStyleCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost6.Text = "6.";
			this.txtStyleCost6.Top = 0.25F;
			this.txtStyleCost6.Width = 0.5F;
			// 
			// txtStyleCost7
			// 
			this.txtStyleCost7.CanGrow = false;
			this.txtStyleCost7.Height = 0.125F;
			this.txtStyleCost7.Left = 0.5625F;
			this.txtStyleCost7.Name = "txtStyleCost7";
			this.txtStyleCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost7.Text = "7.";
			this.txtStyleCost7.Top = 0.375F;
			this.txtStyleCost7.Width = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.125F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 6pt";
			this.Label2.Text = "SF Bsmt Living";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.125F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 6pt";
			this.Label3.Text = "Layout";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.375F;
			// 
			// txtLayoutCost1
			// 
			this.txtLayoutCost1.CanGrow = false;
			this.txtLayoutCost1.Height = 0.125F;
			this.txtLayoutCost1.Left = 3.625F;
			this.txtLayoutCost1.Name = "txtLayoutCost1";
			this.txtLayoutCost1.Style = "font-family: \'Tahoma\'; font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost1.Text = "1.";
			this.txtLayoutCost1.Top = 0.125F;
			this.txtLayoutCost1.Width = 0.5F;
			// 
			// txtLayoutCost2
			// 
			this.txtLayoutCost2.CanGrow = false;
			this.txtLayoutCost2.Height = 0.125F;
			this.txtLayoutCost2.Left = 3.625F;
			this.txtLayoutCost2.Name = "txtLayoutCost2";
			this.txtLayoutCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost2.Text = "2.";
			this.txtLayoutCost2.Top = 0.25F;
			this.txtLayoutCost2.Width = 0.5F;
			// 
			// txtLayoutCost3
			// 
			this.txtLayoutCost3.CanGrow = false;
			this.txtLayoutCost3.Height = 0.125F;
			this.txtLayoutCost3.Left = 3.625F;
			this.txtLayoutCost3.Name = "txtLayoutCost3";
			this.txtLayoutCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost3.Text = "3.";
			this.txtLayoutCost3.Top = 0.375F;
			this.txtLayoutCost3.Width = 0.5F;
			// 
			// txtLayoutCost8
			// 
			this.txtLayoutCost8.CanGrow = false;
			this.txtLayoutCost8.Height = 0.125F;
			this.txtLayoutCost8.Left = 4.75F;
			this.txtLayoutCost8.Name = "txtLayoutCost8";
			this.txtLayoutCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost8.Text = "8.";
			this.txtLayoutCost8.Top = 0.25F;
			this.txtLayoutCost8.Width = 0.5F;
			// 
			// txtLayoutCost9
			// 
			this.txtLayoutCost9.CanGrow = false;
			this.txtLayoutCost9.Height = 0.125F;
			this.txtLayoutCost9.Left = 4.75F;
			this.txtLayoutCost9.Name = "txtLayoutCost9";
			this.txtLayoutCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost9.Text = "9.";
			this.txtLayoutCost9.Top = 0.375F;
			this.txtLayoutCost9.Width = 0.5F;
			// 
			// txtLayoutCost7
			// 
			this.txtLayoutCost7.CanGrow = false;
			this.txtLayoutCost7.Height = 0.125F;
			this.txtLayoutCost7.Left = 4.75F;
			this.txtLayoutCost7.Name = "txtLayoutCost7";
			this.txtLayoutCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost7.Text = "7.";
			this.txtLayoutCost7.Top = 0.125F;
			this.txtLayoutCost7.Width = 0.5F;
			// 
			// txtLayoutCost4
			// 
			this.txtLayoutCost4.CanGrow = false;
			this.txtLayoutCost4.Height = 0.125F;
			this.txtLayoutCost4.Left = 4.1875F;
			this.txtLayoutCost4.Name = "txtLayoutCost4";
			this.txtLayoutCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost4.Text = "4.";
			this.txtLayoutCost4.Top = 0.125F;
			this.txtLayoutCost4.Width = 0.5F;
			// 
			// txtLayoutCost5
			// 
			this.txtLayoutCost5.CanGrow = false;
			this.txtLayoutCost5.Height = 0.125F;
			this.txtLayoutCost5.Left = 4.1875F;
			this.txtLayoutCost5.Name = "txtLayoutCost5";
			this.txtLayoutCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost5.Text = "5.";
			this.txtLayoutCost5.Top = 0.25F;
			this.txtLayoutCost5.Width = 0.5F;
			// 
			// txtLayoutCost6
			// 
			this.txtLayoutCost6.CanGrow = false;
			this.txtLayoutCost6.Height = 0.125F;
			this.txtLayoutCost6.Left = 4.1875F;
			this.txtLayoutCost6.Name = "txtLayoutCost6";
			this.txtLayoutCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtLayoutCost6.Text = "6.";
			this.txtLayoutCost6.Top = 0.375F;
			this.txtLayoutCost6.Width = 0.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.125F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 6pt";
			this.Label4.Text = "Dwelling Units";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 0.5625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.125F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 6pt";
			this.Label5.Text = "Other Units";
			this.Label5.Top = 0.75F;
			this.Label5.Width = 0.5F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.125F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 6pt";
			this.Label6.Text = "Stories";
			this.Label6.Top = 0.875F;
			this.Label6.Width = 0.4375F;
			// 
			// txtStoriesCost1
			// 
			this.txtStoriesCost1.CanGrow = false;
			this.txtStoriesCost1.Height = 0.125F;
			this.txtStoriesCost1.Left = 0F;
			this.txtStoriesCost1.Name = "txtStoriesCost1";
			this.txtStoriesCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost1.Text = "1.";
			this.txtStoriesCost1.Top = 1F;
			this.txtStoriesCost1.Width = 0.5F;
			// 
			// txtStoriesCost2
			// 
			this.txtStoriesCost2.CanGrow = false;
			this.txtStoriesCost2.Height = 0.125F;
			this.txtStoriesCost2.Left = 0F;
			this.txtStoriesCost2.Name = "txtStoriesCost2";
			this.txtStoriesCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost2.Text = "2.";
			this.txtStoriesCost2.Top = 1.125F;
			this.txtStoriesCost2.Width = 0.5F;
			// 
			// txtStoriesCost3
			// 
			this.txtStoriesCost3.CanGrow = false;
			this.txtStoriesCost3.Height = 0.125F;
			this.txtStoriesCost3.Left = 0F;
			this.txtStoriesCost3.Name = "txtStoriesCost3";
			this.txtStoriesCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost3.Text = "3.";
			this.txtStoriesCost3.Top = 1.25F;
			this.txtStoriesCost3.Width = 0.5F;
			// 
			// txtStoriesCost8
			// 
			this.txtStoriesCost8.CanGrow = false;
			this.txtStoriesCost8.Height = 0.125F;
			this.txtStoriesCost8.Left = 1.125F;
			this.txtStoriesCost8.Name = "txtStoriesCost8";
			this.txtStoriesCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost8.Text = "8.";
			this.txtStoriesCost8.Top = 1.125F;
			this.txtStoriesCost8.Width = 0.5F;
			// 
			// txtStoriesCost9
			// 
			this.txtStoriesCost9.CanGrow = false;
			this.txtStoriesCost9.Height = 0.125F;
			this.txtStoriesCost9.Left = 1.125F;
			this.txtStoriesCost9.Name = "txtStoriesCost9";
			this.txtStoriesCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost9.Text = "9.";
			this.txtStoriesCost9.Top = 1.25F;
			this.txtStoriesCost9.Width = 0.5F;
			// 
			// txtStoriesCost7
			// 
			this.txtStoriesCost7.CanGrow = false;
			this.txtStoriesCost7.Height = 0.125F;
			this.txtStoriesCost7.Left = 1.125F;
			this.txtStoriesCost7.Name = "txtStoriesCost7";
			this.txtStoriesCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost7.Text = "7.";
			this.txtStoriesCost7.Top = 1F;
			this.txtStoriesCost7.Width = 0.5F;
			// 
			// txtStoriesCost4
			// 
			this.txtStoriesCost4.CanGrow = false;
			this.txtStoriesCost4.Height = 0.125F;
			this.txtStoriesCost4.Left = 0.5625F;
			this.txtStoriesCost4.Name = "txtStoriesCost4";
			this.txtStoriesCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost4.Text = "4.";
			this.txtStoriesCost4.Top = 1F;
			this.txtStoriesCost4.Width = 0.5F;
			// 
			// txtStoriesCost5
			// 
			this.txtStoriesCost5.CanGrow = false;
			this.txtStoriesCost5.Height = 0.125F;
			this.txtStoriesCost5.Left = 0.5625F;
			this.txtStoriesCost5.Name = "txtStoriesCost5";
			this.txtStoriesCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost5.Text = "5.";
			this.txtStoriesCost5.Top = 1.125F;
			this.txtStoriesCost5.Width = 0.5F;
			// 
			// txtStoriesCost6
			// 
			this.txtStoriesCost6.CanGrow = false;
			this.txtStoriesCost6.Height = 0.125F;
			this.txtStoriesCost6.Left = 0.5625F;
			this.txtStoriesCost6.Name = "txtStoriesCost6";
			this.txtStoriesCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStoriesCost6.Text = "6.";
			this.txtStoriesCost6.Top = 1.25F;
			this.txtStoriesCost6.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.125F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 6pt";
			this.Label7.Text = "Exterior Walls";
			this.Label7.Top = 1.375F;
			this.Label7.Width = 0.5625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.125F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 6pt";
			this.Label8.Text = "Roof Surface";
			this.Label8.Top = 2F;
			this.Label8.Width = 0.5625F;
			// 
			// txtRoofCost1
			// 
			this.txtRoofCost1.CanGrow = false;
			this.txtRoofCost1.Height = 0.125F;
			this.txtRoofCost1.Left = 0F;
			this.txtRoofCost1.Name = "txtRoofCost1";
			this.txtRoofCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost1.Text = "1.";
			this.txtRoofCost1.Top = 2.125F;
			this.txtRoofCost1.Width = 0.5F;
			// 
			// txtRoofCost2
			// 
			this.txtRoofCost2.CanGrow = false;
			this.txtRoofCost2.Height = 0.125F;
			this.txtRoofCost2.Left = 0F;
			this.txtRoofCost2.Name = "txtRoofCost2";
			this.txtRoofCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost2.Text = "2.";
			this.txtRoofCost2.Top = 2.25F;
			this.txtRoofCost2.Width = 0.5F;
			// 
			// txtRoofCost3
			// 
			this.txtRoofCost3.CanGrow = false;
			this.txtRoofCost3.Height = 0.125F;
			this.txtRoofCost3.Left = 0F;
			this.txtRoofCost3.Name = "txtRoofCost3";
			this.txtRoofCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost3.Text = "3.";
			this.txtRoofCost3.Top = 2.375F;
			this.txtRoofCost3.Width = 0.5F;
			// 
			// txtRoofCost8
			// 
			this.txtRoofCost8.CanGrow = false;
			this.txtRoofCost8.Height = 0.125F;
			this.txtRoofCost8.Left = 1.125F;
			this.txtRoofCost8.Name = "txtRoofCost8";
			this.txtRoofCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost8.Text = "8.";
			this.txtRoofCost8.Top = 2.25F;
			this.txtRoofCost8.Width = 0.5F;
			// 
			// txtRoofCost9
			// 
			this.txtRoofCost9.CanGrow = false;
			this.txtRoofCost9.Height = 0.125F;
			this.txtRoofCost9.Left = 1.125F;
			this.txtRoofCost9.Name = "txtRoofCost9";
			this.txtRoofCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost9.Text = "9.";
			this.txtRoofCost9.Top = 2.375F;
			this.txtRoofCost9.Width = 0.5F;
			// 
			// txtRoofCost7
			// 
			this.txtRoofCost7.CanGrow = false;
			this.txtRoofCost7.Height = 0.125F;
			this.txtRoofCost7.Left = 1.125F;
			this.txtRoofCost7.Name = "txtRoofCost7";
			this.txtRoofCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost7.Text = "7.";
			this.txtRoofCost7.Top = 2.125F;
			this.txtRoofCost7.Width = 0.5F;
			// 
			// txtRoofCost4
			// 
			this.txtRoofCost4.CanGrow = false;
			this.txtRoofCost4.Height = 0.125F;
			this.txtRoofCost4.Left = 0.5625F;
			this.txtRoofCost4.Name = "txtRoofCost4";
			this.txtRoofCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost4.Text = "4.";
			this.txtRoofCost4.Top = 2.125F;
			this.txtRoofCost4.Width = 0.5F;
			// 
			// txtRoofCost5
			// 
			this.txtRoofCost5.CanGrow = false;
			this.txtRoofCost5.Height = 0.125F;
			this.txtRoofCost5.Left = 0.5625F;
			this.txtRoofCost5.Name = "txtRoofCost5";
			this.txtRoofCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost5.Text = "5.";
			this.txtRoofCost5.Top = 2.25F;
			this.txtRoofCost5.Width = 0.5F;
			// 
			// txtRoofCost6
			// 
			this.txtRoofCost6.CanGrow = false;
			this.txtRoofCost6.Height = 0.125F;
			this.txtRoofCost6.Left = 0.5625F;
			this.txtRoofCost6.Name = "txtRoofCost6";
			this.txtRoofCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtRoofCost6.Text = "6.";
			this.txtRoofCost6.Top = 2.375F;
			this.txtRoofCost6.Width = 0.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.125F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 6pt";
			this.Label9.Text = "SF Masonry Trim";
			this.Label9.Top = 2.5F;
			this.Label9.Width = 0.6875F;
			// 
			// lblOpen3
			// 
			this.lblOpen3.Height = 0.125F;
			this.lblOpen3.HyperLink = null;
			this.lblOpen3.Left = 0F;
			this.lblOpen3.Name = "lblOpen3";
			this.lblOpen3.Style = "font-size: 6pt";
			this.lblOpen3.Text = null;
			this.lblOpen3.Top = 2.625F;
			this.lblOpen3.Width = 0.6875F;
			// 
			// lblOpen4
			// 
			this.lblOpen4.Height = 0.125F;
			this.lblOpen4.HyperLink = null;
			this.lblOpen4.Left = 0F;
			this.lblOpen4.Name = "lblOpen4";
			this.lblOpen4.Style = "font-size: 6pt";
			this.lblOpen4.Text = "SF Masonry Trim";
			this.lblOpen4.Top = 2.75F;
			this.lblOpen4.Width = 0.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.125F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 6pt";
			this.Label12.Text = "Year Built";
			this.Label12.Top = 2.875F;
			this.Label12.Width = 0.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.125F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 6pt";
			this.Label13.Text = "Year Remodeled";
			this.Label13.Top = 3F;
			this.Label13.Width = 0.6875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.125F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 6pt";
			this.Label14.Text = "Foundation";
			this.Label14.Top = 3.125F;
			this.Label14.Width = 0.5F;
			// 
			// txtFoundationCost1
			// 
			this.txtFoundationCost1.CanGrow = false;
			this.txtFoundationCost1.Height = 0.125F;
			this.txtFoundationCost1.Left = 0F;
			this.txtFoundationCost1.Name = "txtFoundationCost1";
			this.txtFoundationCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost1.Text = "1.";
			this.txtFoundationCost1.Top = 3.25F;
			this.txtFoundationCost1.Width = 0.5F;
			// 
			// txtFoundationCost2
			// 
			this.txtFoundationCost2.CanGrow = false;
			this.txtFoundationCost2.Height = 0.125F;
			this.txtFoundationCost2.Left = 0F;
			this.txtFoundationCost2.Name = "txtFoundationCost2";
			this.txtFoundationCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost2.Text = "2.";
			this.txtFoundationCost2.Top = 3.375F;
			this.txtFoundationCost2.Width = 0.5F;
			// 
			// txtFoundationCost3
			// 
			this.txtFoundationCost3.CanGrow = false;
			this.txtFoundationCost3.Height = 0.125F;
			this.txtFoundationCost3.Left = 0F;
			this.txtFoundationCost3.Name = "txtFoundationCost3";
			this.txtFoundationCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost3.Text = "3.";
			this.txtFoundationCost3.Top = 3.5F;
			this.txtFoundationCost3.Width = 0.5F;
			// 
			// txtFoundationCost8
			// 
			this.txtFoundationCost8.CanGrow = false;
			this.txtFoundationCost8.Height = 0.125F;
			this.txtFoundationCost8.Left = 1.125F;
			this.txtFoundationCost8.Name = "txtFoundationCost8";
			this.txtFoundationCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost8.Text = "8.";
			this.txtFoundationCost8.Top = 3.375F;
			this.txtFoundationCost8.Width = 0.5F;
			// 
			// txtFoundationCost9
			// 
			this.txtFoundationCost9.CanGrow = false;
			this.txtFoundationCost9.Height = 0.125F;
			this.txtFoundationCost9.Left = 1.125F;
			this.txtFoundationCost9.Name = "txtFoundationCost9";
			this.txtFoundationCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost9.Text = "9.";
			this.txtFoundationCost9.Top = 3.5F;
			this.txtFoundationCost9.Width = 0.5F;
			// 
			// txtFoundationCost7
			// 
			this.txtFoundationCost7.CanGrow = false;
			this.txtFoundationCost7.Height = 0.125F;
			this.txtFoundationCost7.Left = 1.125F;
			this.txtFoundationCost7.Name = "txtFoundationCost7";
			this.txtFoundationCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost7.Text = "7.";
			this.txtFoundationCost7.Top = 3.25F;
			this.txtFoundationCost7.Width = 0.5F;
			// 
			// txtFoundationCost4
			// 
			this.txtFoundationCost4.CanGrow = false;
			this.txtFoundationCost4.Height = 0.125F;
			this.txtFoundationCost4.Left = 0.5625F;
			this.txtFoundationCost4.Name = "txtFoundationCost4";
			this.txtFoundationCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost4.Text = "4.";
			this.txtFoundationCost4.Top = 3.25F;
			this.txtFoundationCost4.Width = 0.5F;
			// 
			// txtFoundationCost5
			// 
			this.txtFoundationCost5.CanGrow = false;
			this.txtFoundationCost5.Height = 0.125F;
			this.txtFoundationCost5.Left = 0.5625F;
			this.txtFoundationCost5.Name = "txtFoundationCost5";
			this.txtFoundationCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost5.Text = "5.";
			this.txtFoundationCost5.Top = 3.375F;
			this.txtFoundationCost5.Width = 0.5F;
			// 
			// txtFoundationCost6
			// 
			this.txtFoundationCost6.CanGrow = false;
			this.txtFoundationCost6.Height = 0.125F;
			this.txtFoundationCost6.Left = 0.5625F;
			this.txtFoundationCost6.Name = "txtFoundationCost6";
			this.txtFoundationCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFoundationCost6.Text = "6.";
			this.txtFoundationCost6.Top = 3.5F;
			this.txtFoundationCost6.Width = 0.5F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.125F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 6pt";
			this.Label15.Text = "Basement";
			this.Label15.Top = 3.625F;
			this.Label15.Width = 0.4375F;
			// 
			// txtBasementCost1
			// 
			this.txtBasementCost1.CanGrow = false;
			this.txtBasementCost1.Height = 0.125F;
			this.txtBasementCost1.Left = 0F;
			this.txtBasementCost1.Name = "txtBasementCost1";
			this.txtBasementCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost1.Text = "1.";
			this.txtBasementCost1.Top = 3.75F;
			this.txtBasementCost1.Width = 0.5F;
			// 
			// txtBasementCost2
			// 
			this.txtBasementCost2.CanGrow = false;
			this.txtBasementCost2.Height = 0.125F;
			this.txtBasementCost2.Left = 0F;
			this.txtBasementCost2.Name = "txtBasementCost2";
			this.txtBasementCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost2.Text = "2.";
			this.txtBasementCost2.Top = 3.875F;
			this.txtBasementCost2.Width = 0.5F;
			// 
			// txtBasementCost3
			// 
			this.txtBasementCost3.CanGrow = false;
			this.txtBasementCost3.Height = 0.125F;
			this.txtBasementCost3.Left = 0F;
			this.txtBasementCost3.Name = "txtBasementCost3";
			this.txtBasementCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost3.Text = "3.";
			this.txtBasementCost3.Top = 4F;
			this.txtBasementCost3.Width = 0.5F;
			// 
			// txtBasementCost8
			// 
			this.txtBasementCost8.CanGrow = false;
			this.txtBasementCost8.Height = 0.125F;
			this.txtBasementCost8.Left = 1.125F;
			this.txtBasementCost8.Name = "txtBasementCost8";
			this.txtBasementCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost8.Text = "8.";
			this.txtBasementCost8.Top = 3.875F;
			this.txtBasementCost8.Width = 0.5F;
			// 
			// txtBasementCost9
			// 
			this.txtBasementCost9.CanGrow = false;
			this.txtBasementCost9.Height = 0.125F;
			this.txtBasementCost9.Left = 1.125F;
			this.txtBasementCost9.Name = "txtBasementCost9";
			this.txtBasementCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost9.Text = "9.";
			this.txtBasementCost9.Top = 4F;
			this.txtBasementCost9.Width = 0.5F;
			// 
			// txtBasementCost7
			// 
			this.txtBasementCost7.CanGrow = false;
			this.txtBasementCost7.Height = 0.125F;
			this.txtBasementCost7.Left = 1.125F;
			this.txtBasementCost7.Name = "txtBasementCost7";
			this.txtBasementCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost7.Text = "7.";
			this.txtBasementCost7.Top = 3.75F;
			this.txtBasementCost7.Width = 0.5F;
			// 
			// txtBasementCost4
			// 
			this.txtBasementCost4.CanGrow = false;
			this.txtBasementCost4.Height = 0.125F;
			this.txtBasementCost4.Left = 0.5625F;
			this.txtBasementCost4.Name = "txtBasementCost4";
			this.txtBasementCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost4.Text = "4.";
			this.txtBasementCost4.Top = 3.75F;
			this.txtBasementCost4.Width = 0.5F;
			// 
			// txtBasementCost5
			// 
			this.txtBasementCost5.CanGrow = false;
			this.txtBasementCost5.Height = 0.125F;
			this.txtBasementCost5.Left = 0.5625F;
			this.txtBasementCost5.Name = "txtBasementCost5";
			this.txtBasementCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost5.Text = "5.";
			this.txtBasementCost5.Top = 3.875F;
			this.txtBasementCost5.Width = 0.5F;
			// 
			// txtBasementCost6
			// 
			this.txtBasementCost6.CanGrow = false;
			this.txtBasementCost6.Height = 0.125F;
			this.txtBasementCost6.Left = 0.5625F;
			this.txtBasementCost6.Name = "txtBasementCost6";
			this.txtBasementCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBasementCost6.Text = "6.";
			this.txtBasementCost6.Top = 4F;
			this.txtBasementCost6.Width = 0.5F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.125F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 6pt";
			this.Label16.Text = "Bsmt Gar # Cars";
			this.Label16.Top = 4.125F;
			this.Label16.Width = 0.6875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.125F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 6pt";
			this.Label17.Text = "Fin Bsmt Grade";
			this.Label17.Top = 0.125F;
			this.Label17.Width = 0.6875F;
			// 
			// lblOpen5
			// 
			this.lblOpen5.Height = 0.125F;
			this.lblOpen5.HyperLink = null;
			this.lblOpen5.Left = 1.625F;
			this.lblOpen5.Name = "lblOpen5";
			this.lblOpen5.Style = "font-size: 6pt";
			this.lblOpen5.Text = null;
			this.lblOpen5.Top = 0.25F;
			this.lblOpen5.Width = 0.8125F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.125F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1.625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 6pt";
			this.Label19.Text = "Heat Type";
			this.Label19.Top = 0.375F;
			this.Label19.Width = 0.5F;
			// 
			// txtHeatCost1
			// 
			this.txtHeatCost1.CanGrow = false;
			this.txtHeatCost1.Height = 0.125F;
			this.txtHeatCost1.Left = 1.625F;
			this.txtHeatCost1.Name = "txtHeatCost1";
			this.txtHeatCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost1.Text = "1.";
			this.txtHeatCost1.Top = 0.5F;
			this.txtHeatCost1.Width = 0.625F;
			// 
			// txtHeatCost2
			// 
			this.txtHeatCost2.CanGrow = false;
			this.txtHeatCost2.Height = 0.125F;
			this.txtHeatCost2.Left = 1.625F;
			this.txtHeatCost2.Name = "txtHeatCost2";
			this.txtHeatCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost2.Text = "2.";
			this.txtHeatCost2.Top = 0.625F;
			this.txtHeatCost2.Width = 0.625F;
			// 
			// txtHeatCost3
			// 
			this.txtHeatCost3.CanGrow = false;
			this.txtHeatCost3.Height = 0.125F;
			this.txtHeatCost3.Left = 1.625F;
			this.txtHeatCost3.Name = "txtHeatCost3";
			this.txtHeatCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost3.Text = "3.";
			this.txtHeatCost3.Top = 0.75F;
			this.txtHeatCost3.Width = 0.625F;
			// 
			// txtHeatCost7
			// 
			this.txtHeatCost7.CanGrow = false;
			this.txtHeatCost7.Height = 0.125F;
			this.txtHeatCost7.Left = 2.3125F;
			this.txtHeatCost7.Name = "txtHeatCost7";
			this.txtHeatCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost7.Text = "7";
			this.txtHeatCost7.Top = 0.75F;
			this.txtHeatCost7.Width = 0.625F;
			// 
			// txtHeatCost8
			// 
			this.txtHeatCost8.CanGrow = false;
			this.txtHeatCost8.Height = 0.125F;
			this.txtHeatCost8.Left = 2.3125F;
			this.txtHeatCost8.Name = "txtHeatCost8";
			this.txtHeatCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost8.Text = "8";
			this.txtHeatCost8.Top = 0.875F;
			this.txtHeatCost8.Width = 0.625F;
			// 
			// txtHeatCost9
			// 
			this.txtHeatCost9.CanGrow = false;
			this.txtHeatCost9.Height = 0.125F;
			this.txtHeatCost9.Left = 3F;
			this.txtHeatCost9.Name = "txtHeatCost9";
			this.txtHeatCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost9.Text = "9";
			this.txtHeatCost9.Top = 0.5F;
			this.txtHeatCost9.Width = 0.625F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 0F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 4.125F;
			this.Line15.Width = 1.625F;
			this.Line15.X1 = 0F;
			this.Line15.X2 = 1.625F;
			this.Line15.Y1 = 4.125F;
			this.Line15.Y2 = 4.125F;
			// 
			// txtHeatCost4
			// 
			this.txtHeatCost4.CanGrow = false;
			this.txtHeatCost4.Height = 0.125F;
			this.txtHeatCost4.Left = 1.625F;
			this.txtHeatCost4.Name = "txtHeatCost4";
			this.txtHeatCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost4.Text = "4";
			this.txtHeatCost4.Top = 0.875F;
			this.txtHeatCost4.Width = 0.625F;
			// 
			// txtHeatCost10
			// 
			this.txtHeatCost10.CanGrow = false;
			this.txtHeatCost10.Height = 0.125F;
			this.txtHeatCost10.Left = 3F;
			this.txtHeatCost10.Name = "txtHeatCost10";
			this.txtHeatCost10.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost10.Text = "10";
			this.txtHeatCost10.Top = 0.625F;
			this.txtHeatCost10.Width = 0.625F;
			// 
			// txtHeatCost5
			// 
			this.txtHeatCost5.CanGrow = false;
			this.txtHeatCost5.Height = 0.125F;
			this.txtHeatCost5.Left = 2.3125F;
			this.txtHeatCost5.Name = "txtHeatCost5";
			this.txtHeatCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost5.Text = "5";
			this.txtHeatCost5.Top = 0.5F;
			this.txtHeatCost5.Width = 0.625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.125F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 1.625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 6pt";
			this.Label20.Text = "Cool Type";
			this.Label20.Top = 1F;
			this.Label20.Width = 0.5F;
			// 
			// txtCoolCost1
			// 
			this.txtCoolCost1.CanGrow = false;
			this.txtCoolCost1.Height = 0.125F;
			this.txtCoolCost1.Left = 1.625F;
			this.txtCoolCost1.Name = "txtCoolCost1";
			this.txtCoolCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost1.Text = "1.";
			this.txtCoolCost1.Top = 1.125F;
			this.txtCoolCost1.Width = 0.625F;
			// 
			// txtCoolCost2
			// 
			this.txtCoolCost2.CanGrow = false;
			this.txtCoolCost2.Height = 0.125F;
			this.txtCoolCost2.Left = 1.625F;
			this.txtCoolCost2.Name = "txtCoolCost2";
			this.txtCoolCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost2.Text = "2.";
			this.txtCoolCost2.Top = 1.25F;
			this.txtCoolCost2.Width = 0.625F;
			// 
			// txtCoolCost3
			// 
			this.txtCoolCost3.CanGrow = false;
			this.txtCoolCost3.Height = 0.125F;
			this.txtCoolCost3.Left = 1.625F;
			this.txtCoolCost3.Name = "txtCoolCost3";
			this.txtCoolCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost3.Text = "3.";
			this.txtCoolCost3.Top = 1.375F;
			this.txtCoolCost3.Width = 0.625F;
			// 
			// txtCoolCost6
			// 
			this.txtCoolCost6.CanGrow = false;
			this.txtCoolCost6.Height = 0.125F;
			this.txtCoolCost6.Left = 2.3125F;
			this.txtCoolCost6.Name = "txtCoolCost6";
			this.txtCoolCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost6.Text = "4.";
			this.txtCoolCost6.Top = 1.375F;
			this.txtCoolCost6.Width = 0.625F;
			// 
			// txtCoolCost7
			// 
			this.txtCoolCost7.CanGrow = false;
			this.txtCoolCost7.Height = 0.125F;
			this.txtCoolCost7.Left = 3F;
			this.txtCoolCost7.Name = "txtCoolCost7";
			this.txtCoolCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost7.Text = "5.";
			this.txtCoolCost7.Top = 1.125F;
			this.txtCoolCost7.Width = 0.625F;
			// 
			// txtCoolCost8
			// 
			this.txtCoolCost8.CanGrow = false;
			this.txtCoolCost8.Height = 0.125F;
			this.txtCoolCost8.Left = 3F;
			this.txtCoolCost8.Name = "txtCoolCost8";
			this.txtCoolCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost8.Text = "6.";
			this.txtCoolCost8.Top = 1.25F;
			this.txtCoolCost8.Width = 0.625F;
			// 
			// txtCoolCost4
			// 
			this.txtCoolCost4.CanGrow = false;
			this.txtCoolCost4.Height = 0.125F;
			this.txtCoolCost4.Left = 2.3125F;
			this.txtCoolCost4.Name = "txtCoolCost4";
			this.txtCoolCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost4.Text = "3.";
			this.txtCoolCost4.Top = 1.125F;
			this.txtCoolCost4.Width = 0.625F;
			// 
			// txtCoolCost9
			// 
			this.txtCoolCost9.CanGrow = false;
			this.txtCoolCost9.Height = 0.125F;
			this.txtCoolCost9.Left = 3F;
			this.txtCoolCost9.Name = "txtCoolCost9";
			this.txtCoolCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost9.Text = "6.";
			this.txtCoolCost9.Top = 1.375F;
			this.txtCoolCost9.Width = 0.625F;
			// 
			// txtCoolCost5
			// 
			this.txtCoolCost5.CanGrow = false;
			this.txtCoolCost5.Height = 0.125F;
			this.txtCoolCost5.Left = 2.3125F;
			this.txtCoolCost5.Name = "txtCoolCost5";
			this.txtCoolCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtCoolCost5.Text = "3.";
			this.txtCoolCost5.Top = 1.25F;
			this.txtCoolCost5.Width = 0.625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.125F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 6pt";
			this.Label21.Text = "Kitchen Style";
			this.Label21.Top = 1.5F;
			this.Label21.Width = 0.5625F;
			// 
			// txtKitchenCost1
			// 
			this.txtKitchenCost1.CanGrow = false;
			this.txtKitchenCost1.Height = 0.125F;
			this.txtKitchenCost1.Left = 1.625F;
			this.txtKitchenCost1.Name = "txtKitchenCost1";
			this.txtKitchenCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost1.Text = "1.";
			this.txtKitchenCost1.Top = 1.625F;
			this.txtKitchenCost1.Width = 0.625F;
			// 
			// txtKitchenCost2
			// 
			this.txtKitchenCost2.CanGrow = false;
			this.txtKitchenCost2.Height = 0.125F;
			this.txtKitchenCost2.Left = 1.625F;
			this.txtKitchenCost2.Name = "txtKitchenCost2";
			this.txtKitchenCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost2.Text = "2.";
			this.txtKitchenCost2.Top = 1.75F;
			this.txtKitchenCost2.Width = 0.625F;
			// 
			// txtKitchenCost3
			// 
			this.txtKitchenCost3.CanGrow = false;
			this.txtKitchenCost3.Height = 0.125F;
			this.txtKitchenCost3.Left = 1.625F;
			this.txtKitchenCost3.Name = "txtKitchenCost3";
			this.txtKitchenCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost3.Text = "3.";
			this.txtKitchenCost3.Top = 1.875F;
			this.txtKitchenCost3.Width = 0.625F;
			// 
			// txtKitchenCost6
			// 
			this.txtKitchenCost6.CanGrow = false;
			this.txtKitchenCost6.Height = 0.125F;
			this.txtKitchenCost6.Left = 2.3125F;
			this.txtKitchenCost6.Name = "txtKitchenCost6";
			this.txtKitchenCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost6.Text = "6";
			this.txtKitchenCost6.Top = 1.875F;
			this.txtKitchenCost6.Width = 0.625F;
			// 
			// txtKitchenCost7
			// 
			this.txtKitchenCost7.CanGrow = false;
			this.txtKitchenCost7.Height = 0.125F;
			this.txtKitchenCost7.Left = 3F;
			this.txtKitchenCost7.Name = "txtKitchenCost7";
			this.txtKitchenCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost7.Text = "7";
			this.txtKitchenCost7.Top = 1.625F;
			this.txtKitchenCost7.Width = 0.625F;
			// 
			// txtKitchenCost8
			// 
			this.txtKitchenCost8.CanGrow = false;
			this.txtKitchenCost8.Height = 0.125F;
			this.txtKitchenCost8.Left = 3F;
			this.txtKitchenCost8.Name = "txtKitchenCost8";
			this.txtKitchenCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost8.Text = "8";
			this.txtKitchenCost8.Top = 1.75F;
			this.txtKitchenCost8.Width = 0.625F;
			// 
			// txtKitchenCost4
			// 
			this.txtKitchenCost4.CanGrow = false;
			this.txtKitchenCost4.Height = 0.125F;
			this.txtKitchenCost4.Left = 2.3125F;
			this.txtKitchenCost4.Name = "txtKitchenCost4";
			this.txtKitchenCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost4.Text = "4";
			this.txtKitchenCost4.Top = 1.625F;
			this.txtKitchenCost4.Width = 0.625F;
			// 
			// txtKitchenCost9
			// 
			this.txtKitchenCost9.CanGrow = false;
			this.txtKitchenCost9.Height = 0.125F;
			this.txtKitchenCost9.Left = 3F;
			this.txtKitchenCost9.Name = "txtKitchenCost9";
			this.txtKitchenCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost9.Text = "9";
			this.txtKitchenCost9.Top = 1.875F;
			this.txtKitchenCost9.Width = 0.625F;
			// 
			// txtKitchenCost5
			// 
			this.txtKitchenCost5.CanGrow = false;
			this.txtKitchenCost5.Height = 0.125F;
			this.txtKitchenCost5.Left = 2.3125F;
			this.txtKitchenCost5.Name = "txtKitchenCost5";
			this.txtKitchenCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtKitchenCost5.Text = "5";
			this.txtKitchenCost5.Top = 1.75F;
			this.txtKitchenCost5.Width = 0.625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.125F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 6pt";
			this.Label22.Text = "Wet Basement";
			this.Label22.Top = 4.25F;
			this.Label22.Width = 0.625F;
			// 
			// txtWetCost1
			// 
			this.txtWetCost1.CanGrow = false;
			this.txtWetCost1.Height = 0.125F;
			this.txtWetCost1.Left = 0F;
			this.txtWetCost1.Name = "txtWetCost1";
			this.txtWetCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost1.Text = "1.";
			this.txtWetCost1.Top = 4.375F;
			this.txtWetCost1.Width = 0.5F;
			// 
			// txtWetCost2
			// 
			this.txtWetCost2.CanGrow = false;
			this.txtWetCost2.Height = 0.125F;
			this.txtWetCost2.Left = 0F;
			this.txtWetCost2.Name = "txtWetCost2";
			this.txtWetCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost2.Text = "2.";
			this.txtWetCost2.Top = 4.5F;
			this.txtWetCost2.Width = 0.5F;
			// 
			// txtWetCost3
			// 
			this.txtWetCost3.CanGrow = false;
			this.txtWetCost3.Height = 0.125F;
			this.txtWetCost3.Left = 0F;
			this.txtWetCost3.Name = "txtWetCost3";
			this.txtWetCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost3.Text = "3.";
			this.txtWetCost3.Top = 4.625F;
			this.txtWetCost3.Width = 0.5F;
			// 
			// txtWetCost8
			// 
			this.txtWetCost8.CanGrow = false;
			this.txtWetCost8.Height = 0.125F;
			this.txtWetCost8.Left = 1.125F;
			this.txtWetCost8.Name = "txtWetCost8";
			this.txtWetCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost8.Text = "8.";
			this.txtWetCost8.Top = 4.5F;
			this.txtWetCost8.Width = 0.5F;
			// 
			// txtWetCost9
			// 
			this.txtWetCost9.CanGrow = false;
			this.txtWetCost9.Height = 0.125F;
			this.txtWetCost9.Left = 1.125F;
			this.txtWetCost9.Name = "txtWetCost9";
			this.txtWetCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost9.Text = "9.";
			this.txtWetCost9.Top = 4.625F;
			this.txtWetCost9.Width = 0.5F;
			// 
			// txtWetCost7
			// 
			this.txtWetCost7.CanGrow = false;
			this.txtWetCost7.Height = 0.125F;
			this.txtWetCost7.Left = 1.125F;
			this.txtWetCost7.Name = "txtWetCost7";
			this.txtWetCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost7.Text = "7.";
			this.txtWetCost7.Top = 4.375F;
			this.txtWetCost7.Width = 0.5F;
			// 
			// txtWetCost4
			// 
			this.txtWetCost4.CanGrow = false;
			this.txtWetCost4.Height = 0.125F;
			this.txtWetCost4.Left = 0.5625F;
			this.txtWetCost4.Name = "txtWetCost4";
			this.txtWetCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost4.Text = "4.";
			this.txtWetCost4.Top = 4.375F;
			this.txtWetCost4.Width = 0.5F;
			// 
			// txtWetCost5
			// 
			this.txtWetCost5.CanGrow = false;
			this.txtWetCost5.Height = 0.125F;
			this.txtWetCost5.Left = 0.5625F;
			this.txtWetCost5.Name = "txtWetCost5";
			this.txtWetCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost5.Text = "5.";
			this.txtWetCost5.Top = 4.5F;
			this.txtWetCost5.Width = 0.5F;
			// 
			// txtWetCost6
			// 
			this.txtWetCost6.CanGrow = false;
			this.txtWetCost6.Height = 0.125F;
			this.txtWetCost6.Left = 0.5625F;
			this.txtWetCost6.Name = "txtWetCost6";
			this.txtWetCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtWetCost6.Text = "6.";
			this.txtWetCost6.Top = 4.625F;
			this.txtWetCost6.Width = 0.5F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.125F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 1.625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 6pt";
			this.Label23.Text = "Bath(s) Style";
			this.Label23.Top = 2F;
			this.Label23.Width = 0.5625F;
			// 
			// txtBathCost1
			// 
			this.txtBathCost1.CanGrow = false;
			this.txtBathCost1.Height = 0.125F;
			this.txtBathCost1.Left = 1.625F;
			this.txtBathCost1.Name = "txtBathCost1";
			this.txtBathCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost1.Text = "1.";
			this.txtBathCost1.Top = 2.125F;
			this.txtBathCost1.Width = 0.625F;
			// 
			// txtBathCost2
			// 
			this.txtBathCost2.CanGrow = false;
			this.txtBathCost2.Height = 0.125F;
			this.txtBathCost2.Left = 1.625F;
			this.txtBathCost2.Name = "txtBathCost2";
			this.txtBathCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost2.Text = "2.";
			this.txtBathCost2.Top = 2.25F;
			this.txtBathCost2.Width = 0.625F;
			// 
			// txtBathCost3
			// 
			this.txtBathCost3.CanGrow = false;
			this.txtBathCost3.Height = 0.125F;
			this.txtBathCost3.Left = 1.625F;
			this.txtBathCost3.Name = "txtBathCost3";
			this.txtBathCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost3.Text = "3.";
			this.txtBathCost3.Top = 2.375F;
			this.txtBathCost3.Width = 0.625F;
			// 
			// txtBathCost6
			// 
			this.txtBathCost6.CanGrow = false;
			this.txtBathCost6.Height = 0.125F;
			this.txtBathCost6.Left = 2.3125F;
			this.txtBathCost6.Name = "txtBathCost6";
			this.txtBathCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost6.Text = "6";
			this.txtBathCost6.Top = 2.375F;
			this.txtBathCost6.Width = 0.625F;
			// 
			// txtBathCost7
			// 
			this.txtBathCost7.CanGrow = false;
			this.txtBathCost7.Height = 0.125F;
			this.txtBathCost7.Left = 3F;
			this.txtBathCost7.Name = "txtBathCost7";
			this.txtBathCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost7.Text = "7";
			this.txtBathCost7.Top = 2.125F;
			this.txtBathCost7.Width = 0.625F;
			// 
			// txtBathCost8
			// 
			this.txtBathCost8.CanGrow = false;
			this.txtBathCost8.Height = 0.125F;
			this.txtBathCost8.Left = 3F;
			this.txtBathCost8.Name = "txtBathCost8";
			this.txtBathCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost8.Text = "8";
			this.txtBathCost8.Top = 2.25F;
			this.txtBathCost8.Width = 0.625F;
			// 
			// txtBathCost4
			// 
			this.txtBathCost4.CanGrow = false;
			this.txtBathCost4.Height = 0.125F;
			this.txtBathCost4.Left = 2.3125F;
			this.txtBathCost4.Name = "txtBathCost4";
			this.txtBathCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost4.Text = "4";
			this.txtBathCost4.Top = 2.125F;
			this.txtBathCost4.Width = 0.625F;
			// 
			// txtBathCost9
			// 
			this.txtBathCost9.CanGrow = false;
			this.txtBathCost9.Height = 0.125F;
			this.txtBathCost9.Left = 3F;
			this.txtBathCost9.Name = "txtBathCost9";
			this.txtBathCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost9.Text = "9";
			this.txtBathCost9.Top = 2.375F;
			this.txtBathCost9.Width = 0.625F;
			// 
			// txtBathCost5
			// 
			this.txtBathCost5.CanGrow = false;
			this.txtBathCost5.Height = 0.125F;
			this.txtBathCost5.Left = 2.3125F;
			this.txtBathCost5.Name = "txtBathCost5";
			this.txtBathCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtBathCost5.Text = "5";
			this.txtBathCost5.Top = 2.25F;
			this.txtBathCost5.Width = 0.625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.125F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 6pt";
			this.Label24.Text = "# Rooms";
			this.Label24.Top = 2.5F;
			this.Label24.Width = 0.5F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.125F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 1.625F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 6pt";
			this.Label25.Text = "# Bedrooms";
			this.Label25.Top = 2.625F;
			this.Label25.Width = 0.5625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.125F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1.625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 6pt";
			this.Label26.Text = "# Full Baths";
			this.Label26.Top = 2.75F;
			this.Label26.Width = 0.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.125F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 1.625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 6pt";
			this.Label27.Text = "# Half Baths";
			this.Label27.Top = 2.875F;
			this.Label27.Width = 0.5625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.125F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 1.625F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 6pt";
			this.Label28.Text = "# Addn Fixtures";
			this.Label28.Top = 3F;
			this.Label28.Width = 0.6875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.125F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 1.625F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 6pt";
			this.Label29.Text = "# Fireplaces";
			this.Label29.Top = 3.125F;
			this.Label29.Width = 0.5625F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.125F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 3.625F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 6pt";
			this.Label30.Text = "Attic";
			this.Label30.Top = 0.5F;
			this.Label30.Width = 0.25F;
			// 
			// txtAtticCost1
			// 
			this.txtAtticCost1.CanGrow = false;
			this.txtAtticCost1.Height = 0.125F;
			this.txtAtticCost1.Left = 3.625F;
			this.txtAtticCost1.Name = "txtAtticCost1";
			this.txtAtticCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost1.Text = "1.";
			this.txtAtticCost1.Top = 0.625F;
			this.txtAtticCost1.Width = 0.5F;
			// 
			// txtAtticCost2
			// 
			this.txtAtticCost2.CanGrow = false;
			this.txtAtticCost2.Height = 0.125F;
			this.txtAtticCost2.Left = 3.625F;
			this.txtAtticCost2.Name = "txtAtticCost2";
			this.txtAtticCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost2.Text = "2.";
			this.txtAtticCost2.Top = 0.75F;
			this.txtAtticCost2.Width = 0.5F;
			// 
			// txtAtticCost3
			// 
			this.txtAtticCost3.CanGrow = false;
			this.txtAtticCost3.Height = 0.125F;
			this.txtAtticCost3.Left = 3.625F;
			this.txtAtticCost3.Name = "txtAtticCost3";
			this.txtAtticCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost3.Text = "3.";
			this.txtAtticCost3.Top = 0.875F;
			this.txtAtticCost3.Width = 0.5F;
			// 
			// txtAtticCost8
			// 
			this.txtAtticCost8.CanGrow = false;
			this.txtAtticCost8.Height = 0.125F;
			this.txtAtticCost8.Left = 4.75F;
			this.txtAtticCost8.Name = "txtAtticCost8";
			this.txtAtticCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost8.Text = "8.";
			this.txtAtticCost8.Top = 0.75F;
			this.txtAtticCost8.Width = 0.5F;
			// 
			// txtAtticCost9
			// 
			this.txtAtticCost9.CanGrow = false;
			this.txtAtticCost9.Height = 0.125F;
			this.txtAtticCost9.Left = 4.75F;
			this.txtAtticCost9.Name = "txtAtticCost9";
			this.txtAtticCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost9.Text = "9.";
			this.txtAtticCost9.Top = 0.875F;
			this.txtAtticCost9.Width = 0.5F;
			// 
			// txtAtticCost7
			// 
			this.txtAtticCost7.CanGrow = false;
			this.txtAtticCost7.Height = 0.125F;
			this.txtAtticCost7.Left = 4.75F;
			this.txtAtticCost7.Name = "txtAtticCost7";
			this.txtAtticCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost7.Text = "7.";
			this.txtAtticCost7.Top = 0.625F;
			this.txtAtticCost7.Width = 0.5F;
			// 
			// txtAtticCost4
			// 
			this.txtAtticCost4.CanGrow = false;
			this.txtAtticCost4.Height = 0.125F;
			this.txtAtticCost4.Left = 4.1875F;
			this.txtAtticCost4.Name = "txtAtticCost4";
			this.txtAtticCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost4.Text = "4.";
			this.txtAtticCost4.Top = 0.625F;
			this.txtAtticCost4.Width = 0.5F;
			// 
			// txtAtticCost5
			// 
			this.txtAtticCost5.CanGrow = false;
			this.txtAtticCost5.Height = 0.125F;
			this.txtAtticCost5.Left = 4.1875F;
			this.txtAtticCost5.Name = "txtAtticCost5";
			this.txtAtticCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost5.Text = "5.";
			this.txtAtticCost5.Top = 0.75F;
			this.txtAtticCost5.Width = 0.5F;
			// 
			// txtAtticCost6
			// 
			this.txtAtticCost6.CanGrow = false;
			this.txtAtticCost6.Height = 0.125F;
			this.txtAtticCost6.Left = 4.1875F;
			this.txtAtticCost6.Name = "txtAtticCost6";
			this.txtAtticCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtAtticCost6.Text = "6.";
			this.txtAtticCost6.Top = 0.875F;
			this.txtAtticCost6.Width = 0.5F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.125F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 3.625F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 6pt";
			this.Label31.Text = "Insulation";
			this.Label31.Top = 1F;
			this.Label31.Width = 0.4375F;
			// 
			// txtInsulationCost1
			// 
			this.txtInsulationCost1.CanGrow = false;
			this.txtInsulationCost1.Height = 0.125F;
			this.txtInsulationCost1.Left = 3.625F;
			this.txtInsulationCost1.Name = "txtInsulationCost1";
			this.txtInsulationCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost1.Text = "1.";
			this.txtInsulationCost1.Top = 1.125F;
			this.txtInsulationCost1.Width = 0.5F;
			// 
			// txtInsulationCost2
			// 
			this.txtInsulationCost2.CanGrow = false;
			this.txtInsulationCost2.Height = 0.125F;
			this.txtInsulationCost2.Left = 3.625F;
			this.txtInsulationCost2.Name = "txtInsulationCost2";
			this.txtInsulationCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost2.Text = "2.";
			this.txtInsulationCost2.Top = 1.25F;
			this.txtInsulationCost2.Width = 0.5F;
			// 
			// txtInsulationCost3
			// 
			this.txtInsulationCost3.CanGrow = false;
			this.txtInsulationCost3.Height = 0.125F;
			this.txtInsulationCost3.Left = 3.625F;
			this.txtInsulationCost3.Name = "txtInsulationCost3";
			this.txtInsulationCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost3.Text = "3.";
			this.txtInsulationCost3.Top = 1.375F;
			this.txtInsulationCost3.Width = 0.5F;
			// 
			// txtInsulationCost8
			// 
			this.txtInsulationCost8.CanGrow = false;
			this.txtInsulationCost8.Height = 0.125F;
			this.txtInsulationCost8.Left = 4.75F;
			this.txtInsulationCost8.Name = "txtInsulationCost8";
			this.txtInsulationCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost8.Text = "8.";
			this.txtInsulationCost8.Top = 1.25F;
			this.txtInsulationCost8.Width = 0.5F;
			// 
			// txtInsulationCost9
			// 
			this.txtInsulationCost9.CanGrow = false;
			this.txtInsulationCost9.Height = 0.125F;
			this.txtInsulationCost9.Left = 4.75F;
			this.txtInsulationCost9.Name = "txtInsulationCost9";
			this.txtInsulationCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost9.Text = "9.";
			this.txtInsulationCost9.Top = 1.375F;
			this.txtInsulationCost9.Width = 0.5F;
			// 
			// txtInsulationCost7
			// 
			this.txtInsulationCost7.CanGrow = false;
			this.txtInsulationCost7.Height = 0.125F;
			this.txtInsulationCost7.Left = 4.75F;
			this.txtInsulationCost7.Name = "txtInsulationCost7";
			this.txtInsulationCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost7.Text = "7.";
			this.txtInsulationCost7.Top = 1.125F;
			this.txtInsulationCost7.Width = 0.5F;
			// 
			// txtInsulationCost4
			// 
			this.txtInsulationCost4.CanGrow = false;
			this.txtInsulationCost4.Height = 0.125F;
			this.txtInsulationCost4.Left = 4.1875F;
			this.txtInsulationCost4.Name = "txtInsulationCost4";
			this.txtInsulationCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost4.Text = "4.";
			this.txtInsulationCost4.Top = 1.125F;
			this.txtInsulationCost4.Width = 0.5F;
			// 
			// txtInsulationCost5
			// 
			this.txtInsulationCost5.CanGrow = false;
			this.txtInsulationCost5.Height = 0.125F;
			this.txtInsulationCost5.Left = 4.1875F;
			this.txtInsulationCost5.Name = "txtInsulationCost5";
			this.txtInsulationCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost5.Text = "5.";
			this.txtInsulationCost5.Top = 1.25F;
			this.txtInsulationCost5.Width = 0.5F;
			// 
			// txtInsulationCost6
			// 
			this.txtInsulationCost6.CanGrow = false;
			this.txtInsulationCost6.Height = 0.125F;
			this.txtInsulationCost6.Left = 4.1875F;
			this.txtInsulationCost6.Name = "txtInsulationCost6";
			this.txtInsulationCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInsulationCost6.Text = "6.";
			this.txtInsulationCost6.Top = 1.375F;
			this.txtInsulationCost6.Width = 0.5F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.125F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 3.625F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 6pt";
			this.Label32.Text = "Unfinished %";
			this.Label32.Top = 1.5F;
			this.Label32.Width = 0.5625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.125F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 3.625F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 6pt";
			this.Label33.Text = "Grade & Factor";
			this.Label33.Top = 1.625F;
			this.Label33.Width = 0.625F;
			// 
			// txtGradeCost1
			// 
			this.txtGradeCost1.CanGrow = false;
			this.txtGradeCost1.Height = 0.125F;
			this.txtGradeCost1.Left = 3.625F;
			this.txtGradeCost1.Name = "txtGradeCost1";
			this.txtGradeCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost1.Text = "1.";
			this.txtGradeCost1.Top = 1.75F;
			this.txtGradeCost1.Width = 0.5F;
			// 
			// txtGradeCost2
			// 
			this.txtGradeCost2.CanGrow = false;
			this.txtGradeCost2.Height = 0.125F;
			this.txtGradeCost2.Left = 3.625F;
			this.txtGradeCost2.Name = "txtGradeCost2";
			this.txtGradeCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost2.Text = "2.";
			this.txtGradeCost2.Top = 1.875F;
			this.txtGradeCost2.Width = 0.5F;
			// 
			// txtGradeCost3
			// 
			this.txtGradeCost3.CanGrow = false;
			this.txtGradeCost3.Height = 0.125F;
			this.txtGradeCost3.Left = 3.625F;
			this.txtGradeCost3.Name = "txtGradeCost3";
			this.txtGradeCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost3.Text = "3.";
			this.txtGradeCost3.Top = 2F;
			this.txtGradeCost3.Width = 0.5F;
			// 
			// txtGradeCost8
			// 
			this.txtGradeCost8.CanGrow = false;
			this.txtGradeCost8.Height = 0.125F;
			this.txtGradeCost8.Left = 4.75F;
			this.txtGradeCost8.Name = "txtGradeCost8";
			this.txtGradeCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost8.Text = "8.";
			this.txtGradeCost8.Top = 1.875F;
			this.txtGradeCost8.Width = 0.5F;
			// 
			// txtGradeCost9
			// 
			this.txtGradeCost9.CanGrow = false;
			this.txtGradeCost9.Height = 0.125F;
			this.txtGradeCost9.Left = 4.75F;
			this.txtGradeCost9.Name = "txtGradeCost9";
			this.txtGradeCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost9.Text = "9.";
			this.txtGradeCost9.Top = 2F;
			this.txtGradeCost9.Width = 0.5F;
			// 
			// txtGradeCost7
			// 
			this.txtGradeCost7.CanGrow = false;
			this.txtGradeCost7.Height = 0.125F;
			this.txtGradeCost7.Left = 4.75F;
			this.txtGradeCost7.Name = "txtGradeCost7";
			this.txtGradeCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost7.Text = "7.";
			this.txtGradeCost7.Top = 1.75F;
			this.txtGradeCost7.Width = 0.5F;
			// 
			// txtGradeCost4
			// 
			this.txtGradeCost4.CanGrow = false;
			this.txtGradeCost4.Height = 0.125F;
			this.txtGradeCost4.Left = 4.1875F;
			this.txtGradeCost4.Name = "txtGradeCost4";
			this.txtGradeCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost4.Text = "4.";
			this.txtGradeCost4.Top = 1.75F;
			this.txtGradeCost4.Width = 0.5F;
			// 
			// txtGradeCost5
			// 
			this.txtGradeCost5.CanGrow = false;
			this.txtGradeCost5.Height = 0.125F;
			this.txtGradeCost5.Left = 4.1875F;
			this.txtGradeCost5.Name = "txtGradeCost5";
			this.txtGradeCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost5.Text = "5.";
			this.txtGradeCost5.Top = 1.875F;
			this.txtGradeCost5.Width = 0.5F;
			// 
			// txtGradeCost6
			// 
			this.txtGradeCost6.CanGrow = false;
			this.txtGradeCost6.Height = 0.125F;
			this.txtGradeCost6.Left = 4.1875F;
			this.txtGradeCost6.Name = "txtGradeCost6";
			this.txtGradeCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtGradeCost6.Text = "6.";
			this.txtGradeCost6.Top = 2F;
			this.txtGradeCost6.Width = 0.5F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.125F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 3.625F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 6pt";
			this.Label34.Text = "SQFT (Footprint)";
			this.Label34.Top = 2.125F;
			this.Label34.Width = 0.6875F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.125F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 3.625F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 6pt";
			this.Label35.Text = "Condition";
			this.Label35.Top = 2.25F;
			this.Label35.Width = 0.4375F;
			// 
			// txtConditionCost1
			// 
			this.txtConditionCost1.CanGrow = false;
			this.txtConditionCost1.Height = 0.125F;
			this.txtConditionCost1.Left = 3.625F;
			this.txtConditionCost1.Name = "txtConditionCost1";
			this.txtConditionCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost1.Text = "1.";
			this.txtConditionCost1.Top = 2.375F;
			this.txtConditionCost1.Width = 0.5F;
			// 
			// txtConditionCost2
			// 
			this.txtConditionCost2.CanGrow = false;
			this.txtConditionCost2.Height = 0.125F;
			this.txtConditionCost2.Left = 3.625F;
			this.txtConditionCost2.Name = "txtConditionCost2";
			this.txtConditionCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost2.Text = "2.";
			this.txtConditionCost2.Top = 2.5F;
			this.txtConditionCost2.Width = 0.5F;
			// 
			// txtConditionCost3
			// 
			this.txtConditionCost3.CanGrow = false;
			this.txtConditionCost3.Height = 0.125F;
			this.txtConditionCost3.Left = 3.625F;
			this.txtConditionCost3.Name = "txtConditionCost3";
			this.txtConditionCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost3.Text = "3.";
			this.txtConditionCost3.Top = 2.625F;
			this.txtConditionCost3.Width = 0.5F;
			// 
			// txtConditionCost8
			// 
			this.txtConditionCost8.CanGrow = false;
			this.txtConditionCost8.Height = 0.125F;
			this.txtConditionCost8.Left = 4.75F;
			this.txtConditionCost8.Name = "txtConditionCost8";
			this.txtConditionCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost8.Text = "8.";
			this.txtConditionCost8.Top = 2.5F;
			this.txtConditionCost8.Width = 0.5F;
			// 
			// txtConditionCost9
			// 
			this.txtConditionCost9.CanGrow = false;
			this.txtConditionCost9.Height = 0.125F;
			this.txtConditionCost9.Left = 4.75F;
			this.txtConditionCost9.Name = "txtConditionCost9";
			this.txtConditionCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost9.Text = "9.";
			this.txtConditionCost9.Top = 2.625F;
			this.txtConditionCost9.Width = 0.5F;
			// 
			// txtConditionCost7
			// 
			this.txtConditionCost7.CanGrow = false;
			this.txtConditionCost7.Height = 0.125F;
			this.txtConditionCost7.Left = 4.75F;
			this.txtConditionCost7.Name = "txtConditionCost7";
			this.txtConditionCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost7.Text = "7.";
			this.txtConditionCost7.Top = 2.375F;
			this.txtConditionCost7.Width = 0.5F;
			// 
			// txtConditionCost4
			// 
			this.txtConditionCost4.CanGrow = false;
			this.txtConditionCost4.Height = 0.125F;
			this.txtConditionCost4.Left = 4.1875F;
			this.txtConditionCost4.Name = "txtConditionCost4";
			this.txtConditionCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost4.Text = "4.";
			this.txtConditionCost4.Top = 2.375F;
			this.txtConditionCost4.Width = 0.5F;
			// 
			// txtConditionCost5
			// 
			this.txtConditionCost5.CanGrow = false;
			this.txtConditionCost5.Height = 0.125F;
			this.txtConditionCost5.Left = 4.1875F;
			this.txtConditionCost5.Name = "txtConditionCost5";
			this.txtConditionCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost5.Text = "5.";
			this.txtConditionCost5.Top = 2.5F;
			this.txtConditionCost5.Width = 0.5F;
			// 
			// txtConditionCost6
			// 
			this.txtConditionCost6.CanGrow = false;
			this.txtConditionCost6.Height = 0.125F;
			this.txtConditionCost6.Left = 4.1875F;
			this.txtConditionCost6.Name = "txtConditionCost6";
			this.txtConditionCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtConditionCost6.Text = "6.";
			this.txtConditionCost6.Top = 2.625F;
			this.txtConditionCost6.Width = 0.5F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.125F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3.625F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 6pt";
			this.Label36.Text = "Phys. % Good";
			this.Label36.Top = 2.75F;
			this.Label36.Width = 0.625F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.125F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 3.625F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 6pt";
			this.Label37.Text = "Funct. % Good";
			this.Label37.Top = 2.875F;
			this.Label37.Width = 0.6875F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.125F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 3.625F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-size: 6pt";
			this.Label38.Text = "Functional Code";
			this.Label38.Top = 3F;
			this.Label38.Width = 0.6875F;
			// 
			// txtFunctionalCost1
			// 
			this.txtFunctionalCost1.CanGrow = false;
			this.txtFunctionalCost1.Height = 0.125F;
			this.txtFunctionalCost1.Left = 3.625F;
			this.txtFunctionalCost1.Name = "txtFunctionalCost1";
			this.txtFunctionalCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost1.Text = "1.";
			this.txtFunctionalCost1.Top = 3.125F;
			this.txtFunctionalCost1.Width = 0.5F;
			// 
			// txtFunctionalCost2
			// 
			this.txtFunctionalCost2.CanGrow = false;
			this.txtFunctionalCost2.Height = 0.125F;
			this.txtFunctionalCost2.Left = 3.625F;
			this.txtFunctionalCost2.Name = "txtFunctionalCost2";
			this.txtFunctionalCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost2.Text = "2.";
			this.txtFunctionalCost2.Top = 3.25F;
			this.txtFunctionalCost2.Width = 0.5F;
			// 
			// txtFunctionalCost3
			// 
			this.txtFunctionalCost3.CanGrow = false;
			this.txtFunctionalCost3.Height = 0.125F;
			this.txtFunctionalCost3.Left = 3.625F;
			this.txtFunctionalCost3.Name = "txtFunctionalCost3";
			this.txtFunctionalCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost3.Text = "3.";
			this.txtFunctionalCost3.Top = 3.375F;
			this.txtFunctionalCost3.Width = 0.5F;
			// 
			// txtFunctionalCost8
			// 
			this.txtFunctionalCost8.CanGrow = false;
			this.txtFunctionalCost8.Height = 0.125F;
			this.txtFunctionalCost8.Left = 4.75F;
			this.txtFunctionalCost8.Name = "txtFunctionalCost8";
			this.txtFunctionalCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost8.Text = "8.";
			this.txtFunctionalCost8.Top = 3.25F;
			this.txtFunctionalCost8.Width = 0.5F;
			// 
			// txtFunctionalCost9
			// 
			this.txtFunctionalCost9.CanGrow = false;
			this.txtFunctionalCost9.Height = 0.125F;
			this.txtFunctionalCost9.Left = 4.75F;
			this.txtFunctionalCost9.Name = "txtFunctionalCost9";
			this.txtFunctionalCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost9.Text = "9.";
			this.txtFunctionalCost9.Top = 3.375F;
			this.txtFunctionalCost9.Width = 0.5F;
			// 
			// txtFunctionalCost7
			// 
			this.txtFunctionalCost7.CanGrow = false;
			this.txtFunctionalCost7.Height = 0.125F;
			this.txtFunctionalCost7.Left = 4.75F;
			this.txtFunctionalCost7.Name = "txtFunctionalCost7";
			this.txtFunctionalCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost7.Text = "7.";
			this.txtFunctionalCost7.Top = 3.125F;
			this.txtFunctionalCost7.Width = 0.5F;
			// 
			// txtFunctionalCost4
			// 
			this.txtFunctionalCost4.CanGrow = false;
			this.txtFunctionalCost4.Height = 0.125F;
			this.txtFunctionalCost4.Left = 4.1875F;
			this.txtFunctionalCost4.Name = "txtFunctionalCost4";
			this.txtFunctionalCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost4.Text = "4.";
			this.txtFunctionalCost4.Top = 3.125F;
			this.txtFunctionalCost4.Width = 0.5F;
			// 
			// txtFunctionalCost5
			// 
			this.txtFunctionalCost5.CanGrow = false;
			this.txtFunctionalCost5.Height = 0.125F;
			this.txtFunctionalCost5.Left = 4.1875F;
			this.txtFunctionalCost5.Name = "txtFunctionalCost5";
			this.txtFunctionalCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost5.Text = "5.";
			this.txtFunctionalCost5.Top = 3.25F;
			this.txtFunctionalCost5.Width = 0.5F;
			// 
			// txtFunctionalCost6
			// 
			this.txtFunctionalCost6.CanGrow = false;
			this.txtFunctionalCost6.Height = 0.125F;
			this.txtFunctionalCost6.Left = 4.1875F;
			this.txtFunctionalCost6.Name = "txtFunctionalCost6";
			this.txtFunctionalCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtFunctionalCost6.Text = "6.";
			this.txtFunctionalCost6.Top = 3.375F;
			this.txtFunctionalCost6.Width = 0.5F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.125F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 3.625F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-size: 6pt";
			this.Label39.Text = "Econ. % Good";
			this.Label39.Top = 3.5F;
			this.Label39.Width = 0.625F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.125F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 3.625F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-size: 6pt";
			this.Label40.Text = "Economic Code";
			this.Label40.Top = 3.625F;
			this.Label40.Width = 0.625F;
			// 
			// txtEconomicCost1
			// 
			this.txtEconomicCost1.CanGrow = false;
			this.txtEconomicCost1.Height = 0.125F;
			this.txtEconomicCost1.Left = 3.625F;
			this.txtEconomicCost1.Name = "txtEconomicCost1";
			this.txtEconomicCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost1.Text = "1.";
			this.txtEconomicCost1.Top = 3.75F;
			this.txtEconomicCost1.Width = 0.5F;
			// 
			// txtEconomicCost2
			// 
			this.txtEconomicCost2.CanGrow = false;
			this.txtEconomicCost2.Height = 0.125F;
			this.txtEconomicCost2.Left = 3.625F;
			this.txtEconomicCost2.Name = "txtEconomicCost2";
			this.txtEconomicCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost2.Text = "2.";
			this.txtEconomicCost2.Top = 3.875F;
			this.txtEconomicCost2.Width = 0.5F;
			// 
			// txtEconomicCost3
			// 
			this.txtEconomicCost3.CanGrow = false;
			this.txtEconomicCost3.Height = 0.125F;
			this.txtEconomicCost3.Left = 3.625F;
			this.txtEconomicCost3.Name = "txtEconomicCost3";
			this.txtEconomicCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost3.Text = "3.";
			this.txtEconomicCost3.Top = 4F;
			this.txtEconomicCost3.Width = 0.5F;
			// 
			// txtEconomicCost8
			// 
			this.txtEconomicCost8.CanGrow = false;
			this.txtEconomicCost8.Height = 0.125F;
			this.txtEconomicCost8.Left = 4.75F;
			this.txtEconomicCost8.Name = "txtEconomicCost8";
			this.txtEconomicCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost8.Text = "8.";
			this.txtEconomicCost8.Top = 3.875F;
			this.txtEconomicCost8.Width = 0.5F;
			// 
			// txtEconomicCost9
			// 
			this.txtEconomicCost9.CanGrow = false;
			this.txtEconomicCost9.Height = 0.125F;
			this.txtEconomicCost9.Left = 4.75F;
			this.txtEconomicCost9.Name = "txtEconomicCost9";
			this.txtEconomicCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost9.Text = "9.";
			this.txtEconomicCost9.Top = 4F;
			this.txtEconomicCost9.Width = 0.5F;
			// 
			// txtEconomicCost7
			// 
			this.txtEconomicCost7.CanGrow = false;
			this.txtEconomicCost7.Height = 0.125F;
			this.txtEconomicCost7.Left = 4.75F;
			this.txtEconomicCost7.Name = "txtEconomicCost7";
			this.txtEconomicCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost7.Text = "7.";
			this.txtEconomicCost7.Top = 3.75F;
			this.txtEconomicCost7.Width = 0.5F;
			// 
			// txtEconomicCost4
			// 
			this.txtEconomicCost4.CanGrow = false;
			this.txtEconomicCost4.Height = 0.125F;
			this.txtEconomicCost4.Left = 4.1875F;
			this.txtEconomicCost4.Name = "txtEconomicCost4";
			this.txtEconomicCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost4.Text = "4.";
			this.txtEconomicCost4.Top = 3.75F;
			this.txtEconomicCost4.Width = 0.5F;
			// 
			// txtEconomicCost5
			// 
			this.txtEconomicCost5.CanGrow = false;
			this.txtEconomicCost5.Height = 0.125F;
			this.txtEconomicCost5.Left = 4.1875F;
			this.txtEconomicCost5.Name = "txtEconomicCost5";
			this.txtEconomicCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost5.Text = "5.";
			this.txtEconomicCost5.Top = 3.875F;
			this.txtEconomicCost5.Width = 0.5F;
			// 
			// txtEconomicCost6
			// 
			this.txtEconomicCost6.CanGrow = false;
			this.txtEconomicCost6.Height = 0.125F;
			this.txtEconomicCost6.Left = 4.1875F;
			this.txtEconomicCost6.Name = "txtEconomicCost6";
			this.txtEconomicCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEconomicCost6.Text = "6.";
			this.txtEconomicCost6.Top = 4F;
			this.txtEconomicCost6.Width = 0.5F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.125F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 3.625F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-size: 6pt";
			this.Label41.Text = "Entrance Code";
			this.Label41.Top = 4.125F;
			this.Label41.Width = 0.625F;
			// 
			// txtEntranceCost1
			// 
			this.txtEntranceCost1.CanGrow = false;
			this.txtEntranceCost1.Height = 0.125F;
			this.txtEntranceCost1.Left = 3.625F;
			this.txtEntranceCost1.Name = "txtEntranceCost1";
			this.txtEntranceCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost1.Text = "1.";
			this.txtEntranceCost1.Top = 4.25F;
			this.txtEntranceCost1.Width = 0.5F;
			// 
			// txtEntranceCost2
			// 
			this.txtEntranceCost2.CanGrow = false;
			this.txtEntranceCost2.Height = 0.125F;
			this.txtEntranceCost2.Left = 3.625F;
			this.txtEntranceCost2.Name = "txtEntranceCost2";
			this.txtEntranceCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost2.Text = "2.";
			this.txtEntranceCost2.Top = 4.375F;
			this.txtEntranceCost2.Width = 0.5F;
			// 
			// txtEntranceCost3
			// 
			this.txtEntranceCost3.CanGrow = false;
			this.txtEntranceCost3.Height = 0.125F;
			this.txtEntranceCost3.Left = 3.625F;
			this.txtEntranceCost3.Name = "txtEntranceCost3";
			this.txtEntranceCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost3.Text = "3.";
			this.txtEntranceCost3.Top = 4.5F;
			this.txtEntranceCost3.Width = 0.5F;
			// 
			// txtEntranceCost8
			// 
			this.txtEntranceCost8.CanGrow = false;
			this.txtEntranceCost8.Height = 0.125F;
			this.txtEntranceCost8.Left = 4.75F;
			this.txtEntranceCost8.Name = "txtEntranceCost8";
			this.txtEntranceCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost8.Text = "8.";
			this.txtEntranceCost8.Top = 4.375F;
			this.txtEntranceCost8.Width = 0.5F;
			// 
			// txtEntranceCost9
			// 
			this.txtEntranceCost9.CanGrow = false;
			this.txtEntranceCost9.Height = 0.125F;
			this.txtEntranceCost9.Left = 4.75F;
			this.txtEntranceCost9.Name = "txtEntranceCost9";
			this.txtEntranceCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost9.Text = "9.";
			this.txtEntranceCost9.Top = 4.5F;
			this.txtEntranceCost9.Width = 0.5F;
			// 
			// txtEntranceCost7
			// 
			this.txtEntranceCost7.CanGrow = false;
			this.txtEntranceCost7.Height = 0.125F;
			this.txtEntranceCost7.Left = 4.75F;
			this.txtEntranceCost7.Name = "txtEntranceCost7";
			this.txtEntranceCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost7.Text = "7.";
			this.txtEntranceCost7.Top = 4.25F;
			this.txtEntranceCost7.Width = 0.5F;
			// 
			// txtEntranceCost4
			// 
			this.txtEntranceCost4.CanGrow = false;
			this.txtEntranceCost4.Height = 0.125F;
			this.txtEntranceCost4.Left = 4.1875F;
			this.txtEntranceCost4.Name = "txtEntranceCost4";
			this.txtEntranceCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost4.Text = "4.";
			this.txtEntranceCost4.Top = 4.25F;
			this.txtEntranceCost4.Width = 0.5F;
			// 
			// txtEntranceCost5
			// 
			this.txtEntranceCost5.CanGrow = false;
			this.txtEntranceCost5.Height = 0.125F;
			this.txtEntranceCost5.Left = 4.1875F;
			this.txtEntranceCost5.Name = "txtEntranceCost5";
			this.txtEntranceCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost5.Text = "5.";
			this.txtEntranceCost5.Top = 4.375F;
			this.txtEntranceCost5.Width = 0.5F;
			// 
			// txtEntranceCost6
			// 
			this.txtEntranceCost6.CanGrow = false;
			this.txtEntranceCost6.Height = 0.125F;
			this.txtEntranceCost6.Left = 4.1875F;
			this.txtEntranceCost6.Name = "txtEntranceCost6";
			this.txtEntranceCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtEntranceCost6.Text = "6.";
			this.txtEntranceCost6.Top = 4.5F;
			this.txtEntranceCost6.Width = 0.5F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.125F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 3.625F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-size: 6pt";
			this.Label42.Text = "Information Code";
			this.Label42.Top = 4.625F;
			this.Label42.Width = 0.6875F;
			// 
			// txtInformationCost1
			// 
			this.txtInformationCost1.CanGrow = false;
			this.txtInformationCost1.Height = 0.125F;
			this.txtInformationCost1.Left = 3.625F;
			this.txtInformationCost1.Name = "txtInformationCost1";
			this.txtInformationCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost1.Text = "1.";
			this.txtInformationCost1.Top = 4.75F;
			this.txtInformationCost1.Width = 0.5F;
			// 
			// txtInformationCost2
			// 
			this.txtInformationCost2.CanGrow = false;
			this.txtInformationCost2.Height = 0.125F;
			this.txtInformationCost2.Left = 3.625F;
			this.txtInformationCost2.Name = "txtInformationCost2";
			this.txtInformationCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost2.Text = "2.";
			this.txtInformationCost2.Top = 4.875F;
			this.txtInformationCost2.Width = 0.5F;
			// 
			// txtInformationCost3
			// 
			this.txtInformationCost3.CanGrow = false;
			this.txtInformationCost3.Height = 0.125F;
			this.txtInformationCost3.Left = 3.625F;
			this.txtInformationCost3.Name = "txtInformationCost3";
			this.txtInformationCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost3.Text = "3.";
			this.txtInformationCost3.Top = 5F;
			this.txtInformationCost3.Width = 0.5F;
			// 
			// txtInformationCost8
			// 
			this.txtInformationCost8.CanGrow = false;
			this.txtInformationCost8.Height = 0.125F;
			this.txtInformationCost8.Left = 4.75F;
			this.txtInformationCost8.Name = "txtInformationCost8";
			this.txtInformationCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost8.Text = "8.";
			this.txtInformationCost8.Top = 4.875F;
			this.txtInformationCost8.Width = 0.5F;
			// 
			// txtInformationCost9
			// 
			this.txtInformationCost9.CanGrow = false;
			this.txtInformationCost9.Height = 0.125F;
			this.txtInformationCost9.Left = 4.75F;
			this.txtInformationCost9.Name = "txtInformationCost9";
			this.txtInformationCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost9.Text = "9.";
			this.txtInformationCost9.Top = 5F;
			this.txtInformationCost9.Width = 0.5F;
			// 
			// txtInformationCost7
			// 
			this.txtInformationCost7.CanGrow = false;
			this.txtInformationCost7.Height = 0.125F;
			this.txtInformationCost7.Left = 4.75F;
			this.txtInformationCost7.Name = "txtInformationCost7";
			this.txtInformationCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost7.Text = "7.";
			this.txtInformationCost7.Top = 4.75F;
			this.txtInformationCost7.Width = 0.5F;
			// 
			// txtInformationCost4
			// 
			this.txtInformationCost4.CanGrow = false;
			this.txtInformationCost4.Height = 0.125F;
			this.txtInformationCost4.Left = 4.1875F;
			this.txtInformationCost4.Name = "txtInformationCost4";
			this.txtInformationCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost4.Text = "4.";
			this.txtInformationCost4.Top = 4.75F;
			this.txtInformationCost4.Width = 0.5F;
			// 
			// txtInformationCost5
			// 
			this.txtInformationCost5.CanGrow = false;
			this.txtInformationCost5.Height = 0.125F;
			this.txtInformationCost5.Left = 4.1875F;
			this.txtInformationCost5.Name = "txtInformationCost5";
			this.txtInformationCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost5.Text = "5.";
			this.txtInformationCost5.Top = 4.875F;
			this.txtInformationCost5.Width = 0.5F;
			// 
			// txtInformationCost6
			// 
			this.txtInformationCost6.CanGrow = false;
			this.txtInformationCost6.Height = 0.125F;
			this.txtInformationCost6.Left = 4.1875F;
			this.txtInformationCost6.Name = "txtInformationCost6";
			this.txtInformationCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtInformationCost6.Text = "6.";
			this.txtInformationCost6.Top = 5F;
			this.txtInformationCost6.Width = 0.5F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.19F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 1.1875F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-size: 8.5pt";
			this.Label43.Text = "Date Inspected";
			this.Label43.Top = 4.96875F;
			this.Label43.Width = 0.9375F;
			// 
			// Line2
			// 
			this.Line2.Height = 5.125F;
			this.Line2.Left = 3.625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 0F;
			this.Line2.X1 = 3.625F;
			this.Line2.X2 = 3.625F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 5.125F;
			// 
			// txtHeatCost11
			// 
			this.txtHeatCost11.CanGrow = false;
			this.txtHeatCost11.Height = 0.125F;
			this.txtHeatCost11.Left = 3F;
			this.txtHeatCost11.Name = "txtHeatCost11";
			this.txtHeatCost11.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost11.Text = "11";
			this.txtHeatCost11.Top = 0.75F;
			this.txtHeatCost11.Width = 0.625F;
			// 
			// txtStyleCost4
			// 
			this.txtStyleCost4.CanGrow = false;
			this.txtStyleCost4.Height = 0.125F;
			this.txtStyleCost4.Left = 0F;
			this.txtStyleCost4.Name = "txtStyleCost4";
			this.txtStyleCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost4.Text = "4.";
			this.txtStyleCost4.Top = 0.5F;
			this.txtStyleCost4.Width = 0.5F;
			// 
			// txtStyleCost12
			// 
			this.txtStyleCost12.CanGrow = false;
			this.txtStyleCost12.Height = 0.125F;
			this.txtStyleCost12.Left = 1.125F;
			this.txtStyleCost12.Name = "txtStyleCost12";
			this.txtStyleCost12.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost12.Text = "12";
			this.txtStyleCost12.Top = 0.5F;
			this.txtStyleCost12.Width = 0.5F;
			// 
			// txtStyleCost8
			// 
			this.txtStyleCost8.CanGrow = false;
			this.txtStyleCost8.Height = 0.125F;
			this.txtStyleCost8.Left = 0.5625F;
			this.txtStyleCost8.Name = "txtStyleCost8";
			this.txtStyleCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtStyleCost8.Text = "8.";
			this.txtStyleCost8.Top = 0.5F;
			this.txtStyleCost8.Width = 0.5F;
			// 
			// txtExteriorCost1
			// 
			this.txtExteriorCost1.CanGrow = false;
			this.txtExteriorCost1.Height = 0.125F;
			this.txtExteriorCost1.Left = 0F;
			this.txtExteriorCost1.Name = "txtExteriorCost1";
			this.txtExteriorCost1.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost1.Text = "1.";
			this.txtExteriorCost1.Top = 1.5F;
			this.txtExteriorCost1.Width = 0.5F;
			// 
			// txtExteriorCost2
			// 
			this.txtExteriorCost2.CanGrow = false;
			this.txtExteriorCost2.Height = 0.125F;
			this.txtExteriorCost2.Left = 0F;
			this.txtExteriorCost2.Name = "txtExteriorCost2";
			this.txtExteriorCost2.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost2.Text = "2.";
			this.txtExteriorCost2.Top = 1.625F;
			this.txtExteriorCost2.Width = 0.5F;
			// 
			// txtExteriorCost3
			// 
			this.txtExteriorCost3.CanGrow = false;
			this.txtExteriorCost3.Height = 0.125F;
			this.txtExteriorCost3.Left = 0F;
			this.txtExteriorCost3.Name = "txtExteriorCost3";
			this.txtExteriorCost3.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost3.Text = "3.";
			this.txtExteriorCost3.Top = 1.75F;
			this.txtExteriorCost3.Width = 0.5F;
			// 
			// txtExteriorCost10
			// 
			this.txtExteriorCost10.CanGrow = false;
			this.txtExteriorCost10.Height = 0.125F;
			this.txtExteriorCost10.Left = 1.125F;
			this.txtExteriorCost10.Name = "txtExteriorCost10";
			this.txtExteriorCost10.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost10.Text = "10";
			this.txtExteriorCost10.Top = 1.625F;
			this.txtExteriorCost10.Width = 0.5F;
			// 
			// txtExteriorCost11
			// 
			this.txtExteriorCost11.CanGrow = false;
			this.txtExteriorCost11.Height = 0.125F;
			this.txtExteriorCost11.Left = 1.125F;
			this.txtExteriorCost11.Name = "txtExteriorCost11";
			this.txtExteriorCost11.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost11.Text = "11";
			this.txtExteriorCost11.Top = 1.75F;
			this.txtExteriorCost11.Width = 0.5F;
			// 
			// txtExteriorCost9
			// 
			this.txtExteriorCost9.CanGrow = false;
			this.txtExteriorCost9.Height = 0.125F;
			this.txtExteriorCost9.Left = 1.125F;
			this.txtExteriorCost9.Name = "txtExteriorCost9";
			this.txtExteriorCost9.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost9.Text = "9";
			this.txtExteriorCost9.Top = 1.5F;
			this.txtExteriorCost9.Width = 0.5F;
			// 
			// txtExteriorCost5
			// 
			this.txtExteriorCost5.CanGrow = false;
			this.txtExteriorCost5.Height = 0.125F;
			this.txtExteriorCost5.Left = 0.5625F;
			this.txtExteriorCost5.Name = "txtExteriorCost5";
			this.txtExteriorCost5.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost5.Text = "5.";
			this.txtExteriorCost5.Top = 1.5F;
			this.txtExteriorCost5.Width = 0.5F;
			// 
			// txtExteriorCost6
			// 
			this.txtExteriorCost6.CanGrow = false;
			this.txtExteriorCost6.Height = 0.125F;
			this.txtExteriorCost6.Left = 0.5625F;
			this.txtExteriorCost6.Name = "txtExteriorCost6";
			this.txtExteriorCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost6.Text = "6.";
			this.txtExteriorCost6.Top = 1.625F;
			this.txtExteriorCost6.Width = 0.5F;
			// 
			// txtExteriorCost7
			// 
			this.txtExteriorCost7.CanGrow = false;
			this.txtExteriorCost7.Height = 0.125F;
			this.txtExteriorCost7.Left = 0.5625F;
			this.txtExteriorCost7.Name = "txtExteriorCost7";
			this.txtExteriorCost7.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost7.Text = "7.";
			this.txtExteriorCost7.Top = 1.75F;
			this.txtExteriorCost7.Width = 0.5F;
			// 
			// txtExteriorCost4
			// 
			this.txtExteriorCost4.CanGrow = false;
			this.txtExteriorCost4.Height = 0.125F;
			this.txtExteriorCost4.Left = 0F;
			this.txtExteriorCost4.Name = "txtExteriorCost4";
			this.txtExteriorCost4.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost4.Text = "4.";
			this.txtExteriorCost4.Top = 1.875F;
			this.txtExteriorCost4.Width = 0.5F;
			// 
			// txtExteriorCost12
			// 
			this.txtExteriorCost12.CanGrow = false;
			this.txtExteriorCost12.Height = 0.125F;
			this.txtExteriorCost12.Left = 1.125F;
			this.txtExteriorCost12.Name = "txtExteriorCost12";
			this.txtExteriorCost12.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost12.Text = "12";
			this.txtExteriorCost12.Top = 1.875F;
			this.txtExteriorCost12.Width = 0.5F;
			// 
			// txtExteriorCost8
			// 
			this.txtExteriorCost8.CanGrow = false;
			this.txtExteriorCost8.Height = 0.125F;
			this.txtExteriorCost8.Left = 0.5625F;
			this.txtExteriorCost8.Name = "txtExteriorCost8";
			this.txtExteriorCost8.Style = "font-size: 6pt; white-space: nowrap";
			this.txtExteriorCost8.Text = "8.";
			this.txtExteriorCost8.Top = 1.875F;
			this.txtExteriorCost8.Width = 0.5F;
			// 
			// txtHeatCost6
			// 
			this.txtHeatCost6.CanGrow = false;
			this.txtHeatCost6.Height = 0.125F;
			this.txtHeatCost6.Left = 2.3125F;
			this.txtHeatCost6.Name = "txtHeatCost6";
			this.txtHeatCost6.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost6.Text = "6";
			this.txtHeatCost6.Top = 0.625F;
			this.txtHeatCost6.Width = 0.625F;
			// 
			// txtHeatCost12
			// 
			this.txtHeatCost12.CanGrow = false;
			this.txtHeatCost12.Height = 0.125F;
			this.txtHeatCost12.Left = 3F;
			this.txtHeatCost12.Name = "txtHeatCost12";
			this.txtHeatCost12.Style = "font-size: 6pt; white-space: nowrap";
			this.txtHeatCost12.Text = "12";
			this.txtHeatCost12.Top = 0.875F;
			this.txtHeatCost12.Width = 0.625F;
			// 
			// Line1
			// 
			this.Line1.Height = 4.75F;
			this.Line1.Left = 1.625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 0F;
			this.Line1.X1 = 1.625F;
			this.Line1.X2 = 1.625F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 4.75F;
			// 
			// txtDateInspected
			// 
			this.txtDateInspected.Height = 0.19F;
			this.txtDateInspected.Left = 2.125F;
			this.txtDateInspected.Name = "txtDateInspected";
			this.txtDateInspected.Text = null;
			this.txtDateInspected.Top = 4.96875F;
			this.txtDateInspected.Width = 1.0625F;
			// 
			// txtStyle
			// 
			this.txtStyle.CanGrow = false;
			this.txtStyle.Height = 0.125F;
			this.txtStyle.Left = 0.5625F;
			this.txtStyle.MultiLine = false;
			this.txtStyle.Name = "txtStyle";
			this.txtStyle.Style = "font-size: 6pt; font-weight: bold";
			this.txtStyle.Text = null;
			this.txtStyle.Top = 0F;
			this.txtStyle.Width = 1.0625F;
			// 
			// txtUnits
			// 
			this.txtUnits.CanGrow = false;
			this.txtUnits.Height = 0.125F;
			this.txtUnits.Left = 0.5625F;
			this.txtUnits.MultiLine = false;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-size: 6pt; font-weight: bold";
			this.txtUnits.Text = null;
			this.txtUnits.Top = 0.625F;
			this.txtUnits.Width = 1.0625F;
			// 
			// txtOtherUnits
			// 
			this.txtOtherUnits.CanGrow = false;
			this.txtOtherUnits.Height = 0.125F;
			this.txtOtherUnits.Left = 0.5625F;
			this.txtOtherUnits.MultiLine = false;
			this.txtOtherUnits.Name = "txtOtherUnits";
			this.txtOtherUnits.Style = "font-size: 6pt; font-weight: bold";
			this.txtOtherUnits.Text = null;
			this.txtOtherUnits.Top = 0.75F;
			this.txtOtherUnits.Width = 1.0625F;
			// 
			// txtStories
			// 
			this.txtStories.CanGrow = false;
			this.txtStories.Height = 0.125F;
			this.txtStories.Left = 0.5625F;
			this.txtStories.MultiLine = false;
			this.txtStories.Name = "txtStories";
			this.txtStories.Style = "font-size: 6pt; font-weight: bold";
			this.txtStories.Text = null;
			this.txtStories.Top = 0.875F;
			this.txtStories.Width = 1.0625F;
			// 
			// txtExterior
			// 
			this.txtExterior.CanGrow = false;
			this.txtExterior.Height = 0.125F;
			this.txtExterior.Left = 0.5625F;
			this.txtExterior.MultiLine = false;
			this.txtExterior.Name = "txtExterior";
			this.txtExterior.Style = "font-size: 6pt; font-weight: bold";
			this.txtExterior.Text = null;
			this.txtExterior.Top = 1.375F;
			this.txtExterior.Width = 1.0625F;
			// 
			// txtRoof
			// 
			this.txtRoof.CanGrow = false;
			this.txtRoof.Height = 0.125F;
			this.txtRoof.Left = 0.5625F;
			this.txtRoof.MultiLine = false;
			this.txtRoof.Name = "txtRoof";
			this.txtRoof.Style = "font-size: 6pt; font-weight: bold";
			this.txtRoof.Text = null;
			this.txtRoof.Top = 2F;
			this.txtRoof.Width = 1.0625F;
			// 
			// txtMasonry
			// 
			this.txtMasonry.CanGrow = false;
			this.txtMasonry.Height = 0.125F;
			this.txtMasonry.Left = 0.6875F;
			this.txtMasonry.MultiLine = false;
			this.txtMasonry.Name = "txtMasonry";
			this.txtMasonry.Style = "font-size: 6pt; font-weight: bold";
			this.txtMasonry.Text = null;
			this.txtMasonry.Top = 2.5F;
			this.txtMasonry.Width = 0.9375F;
			// 
			// txtOpen3
			// 
			this.txtOpen3.CanGrow = false;
			this.txtOpen3.Height = 0.125F;
			this.txtOpen3.Left = 0.6875F;
			this.txtOpen3.MultiLine = false;
			this.txtOpen3.Name = "txtOpen3";
			this.txtOpen3.Style = "font-size: 6pt; font-weight: bold";
			this.txtOpen3.Text = null;
			this.txtOpen3.Top = 2.625F;
			this.txtOpen3.Width = 0.9375F;
			// 
			// txtOpen4
			// 
			this.txtOpen4.CanGrow = false;
			this.txtOpen4.Height = 0.125F;
			this.txtOpen4.Left = 0.6875F;
			this.txtOpen4.MultiLine = false;
			this.txtOpen4.Name = "txtOpen4";
			this.txtOpen4.Style = "font-size: 6pt; font-weight: bold";
			this.txtOpen4.Text = null;
			this.txtOpen4.Top = 2.75F;
			this.txtOpen4.Width = 0.9375F;
			// 
			// txtYear
			// 
			this.txtYear.CanGrow = false;
			this.txtYear.Height = 0.125F;
			this.txtYear.Left = 0.6875F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-size: 6pt; font-weight: bold";
			this.txtYear.Text = null;
			this.txtYear.Top = 2.875F;
			this.txtYear.Width = 0.9375F;
			// 
			// txtRemodel
			// 
			this.txtRemodel.CanGrow = false;
			this.txtRemodel.Height = 0.125F;
			this.txtRemodel.Left = 0.6875F;
			this.txtRemodel.MultiLine = false;
			this.txtRemodel.Name = "txtRemodel";
			this.txtRemodel.Style = "font-size: 6pt; font-weight: bold";
			this.txtRemodel.Text = null;
			this.txtRemodel.Top = 3F;
			this.txtRemodel.Width = 0.9375F;
			// 
			// txtFoundation
			// 
			this.txtFoundation.CanGrow = false;
			this.txtFoundation.Height = 0.125F;
			this.txtFoundation.Left = 0.5625F;
			this.txtFoundation.MultiLine = false;
			this.txtFoundation.Name = "txtFoundation";
			this.txtFoundation.Style = "font-size: 6pt; font-weight: bold";
			this.txtFoundation.Text = null;
			this.txtFoundation.Top = 3.125F;
			this.txtFoundation.Width = 1.0625F;
			// 
			// txtBasement
			// 
			this.txtBasement.CanGrow = false;
			this.txtBasement.Height = 0.125F;
			this.txtBasement.Left = 0.5F;
			this.txtBasement.MultiLine = false;
			this.txtBasement.Name = "txtBasement";
			this.txtBasement.Style = "font-size: 6pt; font-weight: bold";
			this.txtBasement.Text = null;
			this.txtBasement.Top = 3.625F;
			this.txtBasement.Width = 1.125F;
			// 
			// txtBsmtGar
			// 
			this.txtBsmtGar.CanGrow = false;
			this.txtBsmtGar.Height = 0.125F;
			this.txtBsmtGar.Left = 0.6875F;
			this.txtBsmtGar.MultiLine = false;
			this.txtBsmtGar.Name = "txtBsmtGar";
			this.txtBsmtGar.Style = "font-size: 6pt; font-weight: bold";
			this.txtBsmtGar.Text = null;
			this.txtBsmtGar.Top = 4.125F;
			this.txtBsmtGar.Width = 0.9375F;
			// 
			// txtWet
			// 
			this.txtWet.CanGrow = false;
			this.txtWet.Height = 0.125F;
			this.txtWet.Left = 0.625F;
			this.txtWet.MultiLine = false;
			this.txtWet.Name = "txtWet";
			this.txtWet.Style = "font-size: 6pt; font-weight: bold";
			this.txtWet.Text = null;
			this.txtWet.Top = 4.25F;
			this.txtWet.Width = 1F;
			// 
			// txtLayout
			// 
			this.txtLayout.CanGrow = false;
			this.txtLayout.Height = 0.125F;
			this.txtLayout.Left = 4F;
			this.txtLayout.MultiLine = false;
			this.txtLayout.Name = "txtLayout";
			this.txtLayout.Style = "font-size: 6pt; font-weight: bold";
			this.txtLayout.Text = null;
			this.txtLayout.Top = 0F;
			this.txtLayout.Width = 1.25F;
			// 
			// txtAttic
			// 
			this.txtAttic.CanGrow = false;
			this.txtAttic.Height = 0.125F;
			this.txtAttic.Left = 3.9375F;
			this.txtAttic.MultiLine = false;
			this.txtAttic.Name = "txtAttic";
			this.txtAttic.Style = "font-size: 6pt; font-weight: bold";
			this.txtAttic.Text = null;
			this.txtAttic.Top = 0.5F;
			this.txtAttic.Width = 1.3125F;
			// 
			// txtInsulation
			// 
			this.txtInsulation.CanGrow = false;
			this.txtInsulation.Height = 0.125F;
			this.txtInsulation.Left = 4.0625F;
			this.txtInsulation.MultiLine = false;
			this.txtInsulation.Name = "txtInsulation";
			this.txtInsulation.Style = "font-size: 6pt; font-weight: bold";
			this.txtInsulation.Text = null;
			this.txtInsulation.Top = 1F;
			this.txtInsulation.Width = 1.1875F;
			// 
			// txtUnfinished
			// 
			this.txtUnfinished.CanGrow = false;
			this.txtUnfinished.Height = 0.125F;
			this.txtUnfinished.Left = 4.25F;
			this.txtUnfinished.MultiLine = false;
			this.txtUnfinished.Name = "txtUnfinished";
			this.txtUnfinished.Style = "font-size: 6pt; font-weight: bold";
			this.txtUnfinished.Text = null;
			this.txtUnfinished.Top = 1.5F;
			this.txtUnfinished.Width = 1F;
			// 
			// txtGradeFactor
			// 
			this.txtGradeFactor.CanGrow = false;
			this.txtGradeFactor.Height = 0.125F;
			this.txtGradeFactor.Left = 4.25F;
			this.txtGradeFactor.MultiLine = false;
			this.txtGradeFactor.Name = "txtGradeFactor";
			this.txtGradeFactor.Style = "font-size: 6pt; font-weight: bold";
			this.txtGradeFactor.Text = null;
			this.txtGradeFactor.Top = 1.625F;
			this.txtGradeFactor.Width = 1F;
			// 
			// txtSqft
			// 
			this.txtSqft.CanGrow = false;
			this.txtSqft.Height = 0.125F;
			this.txtSqft.Left = 4.3125F;
			this.txtSqft.MultiLine = false;
			this.txtSqft.Name = "txtSqft";
			this.txtSqft.Style = "font-size: 6pt; font-weight: bold";
			this.txtSqft.Text = null;
			this.txtSqft.Top = 2.125F;
			this.txtSqft.Width = 0.9375F;
			// 
			// txtCondition
			// 
			this.txtCondition.CanGrow = false;
			this.txtCondition.Height = 0.125F;
			this.txtCondition.Left = 4.125F;
			this.txtCondition.MultiLine = false;
			this.txtCondition.Name = "txtCondition";
			this.txtCondition.Style = "font-size: 6pt; font-weight: bold";
			this.txtCondition.Text = null;
			this.txtCondition.Top = 2.25F;
			this.txtCondition.Width = 1.125F;
			// 
			// txtPhysPct
			// 
			this.txtPhysPct.CanGrow = false;
			this.txtPhysPct.Height = 0.125F;
			this.txtPhysPct.Left = 4.3125F;
			this.txtPhysPct.MultiLine = false;
			this.txtPhysPct.Name = "txtPhysPct";
			this.txtPhysPct.Style = "font-size: 6pt; font-weight: bold";
			this.txtPhysPct.Text = null;
			this.txtPhysPct.Top = 2.75F;
			this.txtPhysPct.Width = 0.9375F;
			// 
			// txtFuncPct
			// 
			this.txtFuncPct.CanGrow = false;
			this.txtFuncPct.Height = 0.125F;
			this.txtFuncPct.Left = 4.3125F;
			this.txtFuncPct.MultiLine = false;
			this.txtFuncPct.Name = "txtFuncPct";
			this.txtFuncPct.Style = "font-size: 6pt; font-weight: bold";
			this.txtFuncPct.Text = null;
			this.txtFuncPct.Top = 2.875F;
			this.txtFuncPct.Width = 0.9375F;
			// 
			// txtFuncCode
			// 
			this.txtFuncCode.CanGrow = false;
			this.txtFuncCode.Height = 0.125F;
			this.txtFuncCode.Left = 4.3125F;
			this.txtFuncCode.MultiLine = false;
			this.txtFuncCode.Name = "txtFuncCode";
			this.txtFuncCode.Style = "font-size: 6pt; font-weight: bold";
			this.txtFuncCode.Text = null;
			this.txtFuncCode.Top = 3F;
			this.txtFuncCode.Width = 0.9375F;
			// 
			// txtEconPct
			// 
			this.txtEconPct.CanGrow = false;
			this.txtEconPct.Height = 0.125F;
			this.txtEconPct.Left = 4.25F;
			this.txtEconPct.MultiLine = false;
			this.txtEconPct.Name = "txtEconPct";
			this.txtEconPct.Style = "font-size: 6pt; font-weight: bold";
			this.txtEconPct.Text = null;
			this.txtEconPct.Top = 3.5F;
			this.txtEconPct.Width = 1F;
			// 
			// txtEconCode
			// 
			this.txtEconCode.CanGrow = false;
			this.txtEconCode.Height = 0.125F;
			this.txtEconCode.Left = 4.25F;
			this.txtEconCode.MultiLine = false;
			this.txtEconCode.Name = "txtEconCode";
			this.txtEconCode.Style = "font-size: 6pt; font-weight: bold";
			this.txtEconCode.Text = null;
			this.txtEconCode.Top = 3.625F;
			this.txtEconCode.Width = 1F;
			// 
			// txtEntrance
			// 
			this.txtEntrance.CanGrow = false;
			this.txtEntrance.Height = 0.125F;
			this.txtEntrance.Left = 4.25F;
			this.txtEntrance.MultiLine = false;
			this.txtEntrance.Name = "txtEntrance";
			this.txtEntrance.Style = "font-size: 6pt; font-weight: bold";
			this.txtEntrance.Text = null;
			this.txtEntrance.Top = 4.125F;
			this.txtEntrance.Width = 1F;
			// 
			// txtInformation
			// 
			this.txtInformation.CanGrow = false;
			this.txtInformation.Height = 0.125F;
			this.txtInformation.Left = 4.375F;
			this.txtInformation.MultiLine = false;
			this.txtInformation.Name = "txtInformation";
			this.txtInformation.Style = "font-size: 6pt; font-weight: bold";
			this.txtInformation.Text = null;
			this.txtInformation.Top = 4.625F;
			this.txtInformation.Width = 0.875F;
			// 
			// txtSFBsmtLiv
			// 
			this.txtSFBsmtLiv.Height = 0.125F;
			this.txtSFBsmtLiv.Left = 2.375F;
			this.txtSFBsmtLiv.Name = "txtSFBsmtLiv";
			this.txtSFBsmtLiv.Style = "font-size: 6pt; font-weight: bold";
			this.txtSFBsmtLiv.Text = null;
			this.txtSFBsmtLiv.Top = 0F;
			this.txtSFBsmtLiv.Width = 1.0625F;
			// 
			// txtFinBsmtGrade
			// 
			this.txtFinBsmtGrade.Height = 0.125F;
			this.txtFinBsmtGrade.Left = 2.375F;
			this.txtFinBsmtGrade.Name = "txtFinBsmtGrade";
			this.txtFinBsmtGrade.Style = "font-size: 6pt; font-weight: bold";
			this.txtFinBsmtGrade.Text = null;
			this.txtFinBsmtGrade.Top = 0.125F;
			this.txtFinBsmtGrade.Width = 1.0625F;
			// 
			// txtHeat
			// 
			this.txtHeat.Height = 0.125F;
			this.txtHeat.Left = 2.5625F;
			this.txtHeat.Name = "txtHeat";
			this.txtHeat.Style = "font-size: 6pt; font-weight: bold";
			this.txtHeat.Text = null;
			this.txtHeat.Top = 0.375F;
			this.txtHeat.Width = 1.0625F;
			// 
			// txtCool
			// 
			this.txtCool.Height = 0.125F;
			this.txtCool.Left = 2.5625F;
			this.txtCool.Name = "txtCool";
			this.txtCool.Style = "font-size: 6pt; font-weight: bold";
			this.txtCool.Text = null;
			this.txtCool.Top = 1F;
			this.txtCool.Width = 1.0625F;
			// 
			// txtKitchen
			// 
			this.txtKitchen.CanGrow = false;
			this.txtKitchen.Height = 0.125F;
			this.txtKitchen.Left = 2.375F;
			this.txtKitchen.Name = "txtKitchen";
			this.txtKitchen.Style = "font-size: 6pt; font-weight: bold";
			this.txtKitchen.Text = null;
			this.txtKitchen.Top = 1.5F;
			this.txtKitchen.Width = 1.25F;
			// 
			// txtBath
			// 
			this.txtBath.CanGrow = false;
			this.txtBath.Height = 0.125F;
			this.txtBath.Left = 2.375F;
			this.txtBath.Name = "txtBath";
			this.txtBath.Style = "font-size: 6pt; font-weight: bold";
			this.txtBath.Text = null;
			this.txtBath.Top = 2F;
			this.txtBath.Width = 1.25F;
			// 
			// txtRooms
			// 
			this.txtRooms.CanGrow = false;
			this.txtRooms.Height = 0.125F;
			this.txtRooms.Left = 2.375F;
			this.txtRooms.MultiLine = false;
			this.txtRooms.Name = "txtRooms";
			this.txtRooms.Style = "font-size: 6pt; font-weight: bold";
			this.txtRooms.Text = null;
			this.txtRooms.Top = 2.5F;
			this.txtRooms.Width = 1.1875F;
			// 
			// txtBedrooms
			// 
			this.txtBedrooms.CanGrow = false;
			this.txtBedrooms.Height = 0.125F;
			this.txtBedrooms.Left = 2.375F;
			this.txtBedrooms.MultiLine = false;
			this.txtBedrooms.Name = "txtBedrooms";
			this.txtBedrooms.Style = "font-size: 6pt; font-weight: bold";
			this.txtBedrooms.Text = null;
			this.txtBedrooms.Top = 2.625F;
			this.txtBedrooms.Width = 1.1875F;
			// 
			// txtBaths
			// 
			this.txtBaths.CanGrow = false;
			this.txtBaths.Height = 0.125F;
			this.txtBaths.Left = 2.375F;
			this.txtBaths.MultiLine = false;
			this.txtBaths.Name = "txtBaths";
			this.txtBaths.Style = "font-size: 6pt; font-weight: bold";
			this.txtBaths.Text = null;
			this.txtBaths.Top = 2.75F;
			this.txtBaths.Width = 1.1875F;
			// 
			// txtHalfBaths
			// 
			this.txtHalfBaths.CanGrow = false;
			this.txtHalfBaths.Height = 0.125F;
			this.txtHalfBaths.Left = 2.375F;
			this.txtHalfBaths.MultiLine = false;
			this.txtHalfBaths.Name = "txtHalfBaths";
			this.txtHalfBaths.Style = "font-size: 6pt; font-weight: bold";
			this.txtHalfBaths.Text = null;
			this.txtHalfBaths.Top = 2.875F;
			this.txtHalfBaths.Width = 1.1875F;
			// 
			// txtFixtures
			// 
			this.txtFixtures.CanGrow = false;
			this.txtFixtures.Height = 0.125F;
			this.txtFixtures.Left = 2.375F;
			this.txtFixtures.MultiLine = false;
			this.txtFixtures.Name = "txtFixtures";
			this.txtFixtures.Style = "font-size: 6pt; font-weight: bold";
			this.txtFixtures.Text = null;
			this.txtFixtures.Top = 3F;
			this.txtFixtures.Width = 1.1875F;
			// 
			// txtFirePlaces
			// 
			this.txtFirePlaces.CanGrow = false;
			this.txtFirePlaces.Height = 0.125F;
			this.txtFirePlaces.Left = 2.375F;
			this.txtFirePlaces.MultiLine = false;
			this.txtFirePlaces.Name = "txtFirePlaces";
			this.txtFirePlaces.Style = "font-size: 6pt; font-weight: bold";
			this.txtFirePlaces.Text = null;
			this.txtFirePlaces.Top = 3.125F;
			this.txtFirePlaces.Width = 1.1875F;
			// 
			// txtOpen5
			// 
			this.txtOpen5.Height = 0.125F;
			this.txtOpen5.Left = 2.5F;
			this.txtOpen5.Name = "txtOpen5";
			this.txtOpen5.Style = "font-size: 6pt; font-weight: bold";
			this.txtOpen5.Text = null;
			this.txtOpen5.Top = 0.25F;
			this.txtOpen5.Width = 0.9375F;
			// 
			// txtHeatPct
			// 
			this.txtHeatPct.Height = 0.125F;
			this.txtHeatPct.Left = 2.125F;
			this.txtHeatPct.Name = "txtHeatPct";
			this.txtHeatPct.Style = "font-size: 6pt; font-weight: bold";
			this.txtHeatPct.Text = null;
			this.txtHeatPct.Top = 0.375F;
			this.txtHeatPct.Width = 0.375F;
			// 
			// txtCoolPct
			// 
			this.txtCoolPct.Height = 0.125F;
			this.txtCoolPct.Left = 2.125F;
			this.txtCoolPct.Name = "txtCoolPct";
			this.txtCoolPct.Style = "font-size: 6pt; font-weight: bold";
			this.txtCoolPct.Text = null;
			this.txtCoolPct.Top = 1F;
			this.txtCoolPct.Width = 0.375F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 4.25F;
			this.Line16.Width = 1.625F;
			this.Line16.X1 = 0F;
			this.Line16.X2 = 1.625F;
			this.Line16.Y1 = 4.25F;
			this.Line16.Y2 = 4.25F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 0F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 3.625F;
			this.Line14.Width = 1.625F;
			this.Line14.X1 = 0F;
			this.Line14.X2 = 1.625F;
			this.Line14.Y1 = 3.625F;
			this.Line14.Y2 = 3.625F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 3.125F;
			this.Line13.Width = 1.625F;
			this.Line13.X1 = 0F;
			this.Line13.X2 = 1.625F;
			this.Line13.Y1 = 3.125F;
			this.Line13.Y2 = 3.125F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 3F;
			this.Line12.Width = 1.625F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 1.625F;
			this.Line12.Y1 = 3F;
			this.Line12.Y2 = 3F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 0F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 2.875F;
			this.Line11.Width = 1.625F;
			this.Line11.X1 = 0F;
			this.Line11.X2 = 1.625F;
			this.Line11.Y1 = 2.875F;
			this.Line11.Y2 = 2.875F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 0F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 2.75F;
			this.Line10.Width = 1.625F;
			this.Line10.X1 = 0F;
			this.Line10.X2 = 1.625F;
			this.Line10.Y1 = 2.75F;
			this.Line10.Y2 = 2.75F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 0F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 2.625F;
			this.Line9.Width = 1.625F;
			this.Line9.X1 = 0F;
			this.Line9.X2 = 1.625F;
			this.Line9.Y1 = 2.625F;
			this.Line9.Y2 = 2.625F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 2.5F;
			this.Line8.Width = 1.625F;
			this.Line8.X1 = 0F;
			this.Line8.X2 = 1.625F;
			this.Line8.Y1 = 2.5F;
			this.Line8.Y2 = 2.5F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 2F;
			this.Line7.Width = 1.625F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 1.625F;
			this.Line7.Y1 = 2F;
			this.Line7.Y2 = 2F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.375F;
			this.Line6.Width = 1.625F;
			this.Line6.X1 = 0F;
			this.Line6.X2 = 1.625F;
			this.Line6.Y1 = 1.375F;
			this.Line6.Y2 = 1.375F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.875F;
			this.Line5.Width = 1.625F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 1.625F;
			this.Line5.Y1 = 0.875F;
			this.Line5.Y2 = 0.875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.75F;
			this.Line4.Width = 1.625F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 1.625F;
			this.Line4.Y1 = 0.75F;
			this.Line4.Y2 = 0.75F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.625F;
			this.Line3.Width = 1.625F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 1.625F;
			this.Line3.Y1 = 0.625F;
			this.Line3.Y2 = 0.625F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 1.625F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 0.125F;
			this.Line17.Width = 2F;
			this.Line17.X1 = 1.625F;
			this.Line17.X2 = 3.625F;
			this.Line17.Y1 = 0.125F;
			this.Line17.Y2 = 0.125F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 1.625F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 0.25F;
			this.Line18.Width = 2F;
			this.Line18.X1 = 1.625F;
			this.Line18.X2 = 3.625F;
			this.Line18.Y1 = 0.25F;
			this.Line18.Y2 = 0.25F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 1.625F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 0.375F;
			this.Line19.Width = 2F;
			this.Line19.X1 = 1.625F;
			this.Line19.X2 = 3.625F;
			this.Line19.Y1 = 0.375F;
			this.Line19.Y2 = 0.375F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 1.625F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 2.5F;
			this.Line25.Width = 2F;
			this.Line25.X1 = 1.625F;
			this.Line25.X2 = 3.625F;
			this.Line25.Y1 = 2.5F;
			this.Line25.Y2 = 2.5F;
			// 
			// Line26
			// 
			this.Line26.Height = 0F;
			this.Line26.Left = 1.625F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 2.625F;
			this.Line26.Width = 2F;
			this.Line26.X1 = 1.625F;
			this.Line26.X2 = 3.625F;
			this.Line26.Y1 = 2.625F;
			this.Line26.Y2 = 2.625F;
			// 
			// Line27
			// 
			this.Line27.Height = 0F;
			this.Line27.Left = 1.625F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 2.75F;
			this.Line27.Width = 2F;
			this.Line27.X1 = 1.625F;
			this.Line27.X2 = 3.625F;
			this.Line27.Y1 = 2.75F;
			this.Line27.Y2 = 2.75F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 1.625F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 2.875F;
			this.Line28.Width = 2F;
			this.Line28.X1 = 1.625F;
			this.Line28.X2 = 3.625F;
			this.Line28.Y1 = 2.875F;
			this.Line28.Y2 = 2.875F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 1.625F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 3F;
			this.Line29.Width = 2F;
			this.Line29.X1 = 1.625F;
			this.Line29.X2 = 3.625F;
			this.Line29.Y1 = 3F;
			this.Line29.Y2 = 3F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 1.625F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 3.125F;
			this.Line30.Width = 2F;
			this.Line30.X1 = 1.625F;
			this.Line30.X2 = 3.625F;
			this.Line30.Y1 = 3.125F;
			this.Line30.Y2 = 3.125F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 3.625F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 0.5F;
			this.Line24.Width = 1.625F;
			this.Line24.X1 = 3.625F;
			this.Line24.X2 = 5.25F;
			this.Line24.Y1 = 0.5F;
			this.Line24.Y2 = 0.5F;
			// 
			// Line32
			// 
			this.Line32.Height = 0F;
			this.Line32.Left = 3.625F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 1F;
			this.Line32.Width = 1.625F;
			this.Line32.X1 = 3.625F;
			this.Line32.X2 = 5.25F;
			this.Line32.Y1 = 1F;
			this.Line32.Y2 = 1F;
			// 
			// Line33
			// 
			this.Line33.Height = 0F;
			this.Line33.Left = 3.625F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 1.5F;
			this.Line33.Width = 1.625F;
			this.Line33.X1 = 3.625F;
			this.Line33.X2 = 5.25F;
			this.Line33.Y1 = 1.5F;
			this.Line33.Y2 = 1.5F;
			// 
			// Line34
			// 
			this.Line34.Height = 0F;
			this.Line34.Left = 3.625F;
			this.Line34.LineWeight = 1F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 1.625F;
			this.Line34.Width = 1.625F;
			this.Line34.X1 = 3.625F;
			this.Line34.X2 = 5.25F;
			this.Line34.Y1 = 1.625F;
			this.Line34.Y2 = 1.625F;
			// 
			// Line35
			// 
			this.Line35.Height = 0F;
			this.Line35.Left = 3.625F;
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 2.125F;
			this.Line35.Width = 1.625F;
			this.Line35.X1 = 3.625F;
			this.Line35.X2 = 5.25F;
			this.Line35.Y1 = 2.125F;
			this.Line35.Y2 = 2.125F;
			// 
			// Line36
			// 
			this.Line36.Height = 0F;
			this.Line36.Left = 1.625F;
			this.Line36.LineWeight = 1F;
			this.Line36.Name = "Line36";
			this.Line36.Top = 1.5F;
			this.Line36.Width = 2F;
			this.Line36.X1 = 1.625F;
			this.Line36.X2 = 3.625F;
			this.Line36.Y1 = 1.5F;
			this.Line36.Y2 = 1.5F;
			// 
			// Line37
			// 
			this.Line37.Height = 0F;
			this.Line37.Left = 3.625F;
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 2.75F;
			this.Line37.Width = 1.625F;
			this.Line37.X1 = 3.625F;
			this.Line37.X2 = 5.25F;
			this.Line37.Y1 = 2.75F;
			this.Line37.Y2 = 2.75F;
			// 
			// Line38
			// 
			this.Line38.Height = 0F;
			this.Line38.Left = 3.625F;
			this.Line38.LineWeight = 1F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 2.875F;
			this.Line38.Width = 1.625F;
			this.Line38.X1 = 3.625F;
			this.Line38.X2 = 5.25F;
			this.Line38.Y1 = 2.875F;
			this.Line38.Y2 = 2.875F;
			// 
			// Line39
			// 
			this.Line39.Height = 0F;
			this.Line39.Left = 3.625F;
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 3F;
			this.Line39.Width = 1.625F;
			this.Line39.X1 = 3.625F;
			this.Line39.X2 = 5.25F;
			this.Line39.Y1 = 3F;
			this.Line39.Y2 = 3F;
			// 
			// Line40
			// 
			this.Line40.Height = 0F;
			this.Line40.Left = 3.625F;
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 3.5F;
			this.Line40.Width = 1.625F;
			this.Line40.X1 = 3.625F;
			this.Line40.X2 = 5.25F;
			this.Line40.Y1 = 3.5F;
			this.Line40.Y2 = 3.5F;
			// 
			// Line41
			// 
			this.Line41.Height = 0F;
			this.Line41.Left = 3.625F;
			this.Line41.LineWeight = 1F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 3.625F;
			this.Line41.Width = 1.625F;
			this.Line41.X1 = 3.625F;
			this.Line41.X2 = 5.25F;
			this.Line41.Y1 = 3.625F;
			this.Line41.Y2 = 3.625F;
			// 
			// Line42
			// 
			this.Line42.Height = 0F;
			this.Line42.Left = 3.625F;
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 4.125F;
			this.Line42.Width = 1.625F;
			this.Line42.X1 = 3.625F;
			this.Line42.X2 = 5.25F;
			this.Line42.Y1 = 4.125F;
			this.Line42.Y2 = 4.125F;
			// 
			// Line43
			// 
			this.Line43.Height = 0F;
			this.Line43.Left = 3.625F;
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 4.625F;
			this.Line43.Width = 1.625F;
			this.Line43.X1 = 3.625F;
			this.Line43.X2 = 5.25F;
			this.Line43.Y1 = 4.625F;
			this.Line43.Y2 = 4.625F;
			// 
			// Line44
			// 
			this.Line44.Height = 0F;
			this.Line44.Left = 1.625F;
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 1F;
			this.Line44.Width = 2F;
			this.Line44.X1 = 1.625F;
			this.Line44.X2 = 3.625F;
			this.Line44.Y1 = 1F;
			this.Line44.Y2 = 1F;
			// 
			// Line45
			// 
			this.Line45.Height = 0F;
			this.Line45.Left = 3.625F;
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 2.25F;
			this.Line45.Width = 1.625F;
			this.Line45.X1 = 3.625F;
			this.Line45.X2 = 5.25F;
			this.Line45.Y1 = 2.25F;
			this.Line45.Y2 = 2.25F;
			// 
			// Line46
			// 
			this.Line46.Height = 0F;
			this.Line46.Left = 1.625F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 2F;
			this.Line46.Width = 2F;
			this.Line46.X1 = 1.625F;
			this.Line46.X2 = 3.625F;
			this.Line46.Y1 = 2F;
			this.Line46.Y2 = 2F;
			// 
			// Image1
			// 
			this.Image1.Height = 1.5F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 1.875F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image1.Top = 3.25F;
			this.Image1.Width = 1.5F;
			// 
			// Line31
			// 
			this.Line31.Height = 0F;
			this.Line31.Left = 1.625F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 3.25F;
			this.Line31.Width = 2F;
			this.Line31.X1 = 1.625F;
			this.Line31.X2 = 3.625F;
			this.Line31.Y1 = 3.25F;
			this.Line31.Y2 = 3.25F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 0F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 4.75F;
			this.Line23.Width = 3.625F;
			this.Line23.X1 = 0F;
			this.Line23.X2 = 3.625F;
			this.Line23.Y1 = 4.75F;
			this.Line23.Y2 = 4.75F;
			// 
			// srptDwellingCard
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 5.25F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayoutCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStoriesCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundationCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasementCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctionalCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomicCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyleCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateInspected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExterior)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoof)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMasonry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBsmtGar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttic)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysPct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFuncPct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFuncCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconPct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntrance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSFBsmtLiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinBsmtGrade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCool)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRooms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBedrooms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfBaths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFixtures)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirePlaces)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatPct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolPct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayoutCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStoriesCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundationCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasementCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctionalCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomicCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyleCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateInspected;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStories;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExterior;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoof;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMasonry;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemodel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBsmtGar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayout;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttic;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnfinished;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysPct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFuncPct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFuncCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconPct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntrance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSFBsmtLiv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinBsmtGrade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCool;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRooms;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBedrooms;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBaths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHalfBaths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFixtures;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirePlaces;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatPct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolPct;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
	}
}
