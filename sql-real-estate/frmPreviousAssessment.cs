﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPreviousAssessment.
	/// </summary>
	public partial class frmPreviousAssessment : BaseForm
	{
		public frmPreviousAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPreviousAssessment InstancePtr
		{
			get
			{
				return (frmPreviousAssessment)Sys.GetInstance(typeof(frmPreviousAssessment));
			}
		}

		protected frmPreviousAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private int lngCurrAcct;
		const int COLYEAR = 0;
		const int COLCARD = 1;
		const int COLLAND = 2;
		const int COLBLDG = 3;
		const int COLEXEMPT = 4;
		const int COLEXEMPT1 = 5;
		const int COLEXEMPT2 = 6;
		const int COLEXEMPT3 = 7;
		const int COLAUTOID = 8;

		private void SetupGrid()
		{
			Grid.Cols = 9;
			Grid.ColHidden(COLEXEMPT1, true);
			Grid.ColHidden(COLEXEMPT2, true);
			Grid.ColHidden(COLEXEMPT3, true);
			Grid.ColHidden(COLAUTOID, true);
			Grid.ExtendLastCol = true;
			Grid.TextMatrix(0, COLYEAR, "Year");
			Grid.TextMatrix(0, COLLAND, "Land");
			Grid.TextMatrix(0, COLBLDG, "Building");
			Grid.TextMatrix(0, COLEXEMPT, "Exempt");
			Grid.TextMatrix(0, COLCARD, "Card");

            Grid.ColAlignment(COLYEAR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(COLCARD, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(COLLAND, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(COLBLDG, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(COLEXEMPT, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(COLYEAR, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(COLCARD, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(COLLAND, FCConvert.ToInt32(0.22 * GridWidth));
			Grid.ColWidth(COLBLDG, FCConvert.ToInt32(0.23 * GridWidth));
		}

		private void frmPreviousAssessment_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						this.Unload();
						break;
					}
			}
			//end switch
		}

		private void frmPreviousAssessment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPreviousAssessment properties;
			//frmPreviousAssessment.ScaleWidth	= 5880;
			//frmPreviousAssessment.ScaleHeight	= 4305;
			//frmPreviousAssessment.LinkTopic	= "Form1";
			//End Unmaped Properties
			lngCurrAcct = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			if (modGlobalConstants.Statics.gboolCL)
			{
				cmdImport.Enabled = true;
			}
			else
			{
				cmdImport.Enabled = false;
			}
			SetupGrid();
		}

		private void frmPreviousAssessment_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						mnuDelete_Click();
						break;
					}
			}
			//end switch
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			txtAccount.Text = FCConvert.ToString(lngCurrAcct);
			Grid.Rows += 1;
			Grid.RowData(Grid.Rows - 1, true);
			Grid.TopRow = Grid.Rows - 1;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			txtAccount.Text = FCConvert.ToString(lngCurrAcct);
			if (Grid.Rows > 1)
			{
				if (Grid.Row > 0)
				{
					GridDelete.AddItem(Grid.TextMatrix(Grid.Row, COLAUTOID));
					Grid.RemoveItem(Grid.Row);
				}
			}
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuImport_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			clsDRWrapper clsCheck = new clsDRWrapper();
			bool boolCheck = false;
			bool boolFound = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngLand = 0;
			int lngbldg = 0;
			int lngExempt = 0;
			int lngAssess = 0;
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobalConstants.Statics.gboolCL)
				{
					lngCurrAcct = 0;
					lblCurrentAcct.Text = "";
					lblName.Text = "";
					txtAccount.Text = "";
					Grid.Rows = 1;
					frmWait.InstancePtr.Init("Checking Account", false);
					clsCheck.OpenRecordset("select rsaccount from master group by rsaccount having count(rsaccount) > 1 order by rsaccount", modGlobalVariables.strREDatabase);
					if (clsCheck.EndOfFile())
					{
						boolCheck = false;
					}
					else
					{
						boolCheck = true;
					}
					// Dim strFullCLName As String
					// Dim strFullREName As String
					// strFullCLName = clsLoad.GetFullDBName("Collections")
					// strFullREName = clsLoad.GetFullDBName("RealEstate")
					// strSQL = "select billingmaster.* from billingmaster inner join master on (billingmaster.account = master.rsaccount) where rscard = 1 and billINGtype = 'RE' and right(billingyear,1) = '1' order by account,billingyear"
					// strSQL = "select * from billingmaster  where  billINGtype = 'RE' and right(billingyear,1) = '1' order by account,billingyear"
					strSQL = "select account ,billingyear , landvalue, buildingvalue, exemptvalue from billingmaster  where  billINGtype = 'RE' and right(cast(billingyear as varchar(5)),1) = '1' order by account,billingyear";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
					while (!clsLoad.EndOfFile())
					{
						//Application.DoEvents();
						// check to see that the account has only one card
						// if there is more than one, the billing info won't match the cards since it is by account
						boolFound = false;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						frmWait.InstancePtr.lblMessage.Text = "Checking Account " + clsLoad.Get_Fields("account") + "  Year " + FCConvert.ToString(FCConvert.ToInt32(clsLoad.Get_Fields_Int32("billingyear") / 10));
						frmWait.InstancePtr.lblMessage.Refresh();
						//FC:FINAL:RPU: #i1188 - Update frmWait to show progress
						FCUtils.ApplicationUpdate(frmWait.InstancePtr);
						if (boolCheck)
						{
							// If clsCheck.FindFirstRecord("rsaccount", clsLoad.Fields("account")) Then
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							if (clsCheck.FindFirst("rsaccount = " + clsLoad.Get_Fields("account")))
							{
								boolFound = true;
							}
						}
						if (!boolFound)
						{
							// don't overwrite, just add entries
							lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("landvalue"))));
							lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("buildingvalue"))));
							lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("exemptvalue"))));
							lngAssess = lngLand + lngbldg - lngExempt;
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							clsSave.OpenRecordset("select * from previousassessment where account = " + clsLoad.Get_Fields("account") + " and taxyear = " + FCConvert.ToString(FCConvert.ToInt32(clsLoad.Get_Fields_Int32("billingyear") / 10)), modGlobalVariables.strREDatabase);
							if (clsSave.EndOfFile())
							{
								clsSave.AddNew();
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsSave.Set_Fields("account", clsLoad.Get_Fields("account"));
								clsSave.Set_Fields("land", lngLand);
								clsSave.Set_Fields("building", lngbldg);
								clsSave.Set_Fields("exempt", lngExempt);
								clsSave.Set_Fields("assessment", lngAssess);
								clsSave.Set_Fields("exemptcode1", 0);
								clsSave.Set_Fields("exemptcode2", 0);
								clsSave.Set_Fields("exemptcode3", 0);
								clsSave.Set_Fields("card", 1);
								clsSave.Set_Fields("taxyear", Conversion.Val(clsLoad.Get_Fields_Int32("billingyear")) / 10);
								clsSave.Update();
							}
						}
						clsLoad.MoveNext();
					}
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("Import complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuImport_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			bool boolEmpty = false;
			int x;
			int lngLand = 0;
			int lngbldg = 0;
			int lngExempt = 0;
			string strSQL = "";
			int lngYear = 0;
			try
			{
				// On Error GoTo ErrorHandler
				SaveInfo = false;
				if (lngCurrAcct <= 0)
				{
					MessageBox.Show("Cannot save to " + FCConvert.ToString(lngCurrAcct), "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return SaveInfo;
				}
				for (x = 1; x <= Grid.Rows - 1; x++)
				{
					lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLYEAR))));
					if (lngYear < 1980 || (lngYear > DateTime.Today.Year + 3))
					{
						MessageBox.Show("Cannot save. Make sure all tax years are valid", "Invalid Tax Year(s)", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						Grid.TopRow = x;
						return SaveInfo;
					}
					if (Conversion.Val(Grid.TextMatrix(x, COLCARD)) < 1)
					{
						MessageBox.Show("Cannot save. Make sure all cards are valid", "Invalid Card", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						Grid.TopRow = x;
						return SaveInfo;
					}
				}
				// x
				clsSave.OpenRecordset("select * from previousassessment where account = " + FCConvert.ToString(lngCurrAcct) + " order by taxyear,card", modGlobalVariables.strREDatabase);
				if (clsSave.EndOfFile())
				{
					boolEmpty = true;
				}
				else
				{
					boolEmpty = false;
				}
				if (boolEmpty && Grid.Rows < 2)
				{
					GridDelete.Rows = 0;
					SaveInfo = true;
					return SaveInfo;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// process griddelete first
				for (x = 0; x <= GridDelete.Rows - 1; x++)
				{
					clsSave.Execute("delete from previousassessment where id = " + GridDelete.TextMatrix(x, 0), modGlobalVariables.strREDatabase);
				}
				// x
				GridDelete.Rows = 0;
				for (x = 1; x <= Grid.Rows - 1; x++)
				{
					if (FCConvert.ToBoolean(Grid.RowData(x)))
					{
						if (Conversion.Val(Grid.TextMatrix(x, COLAUTOID)) > 0 && !(boolEmpty))
						{
							// changed
							strSQL = "Update previousassessment ";
							strSQL += " set taxyear = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLYEAR)));
							strSQL += ",card = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCARD)));
							strSQL += ",land = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLLAND)));
							strSQL += ",building = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLBLDG)));
							strSQL += ",exempt = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT)));
							lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLLAND))));
							lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLBLDG))));
							lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT))));
							strSQL += ",assessment = " + FCConvert.ToString(lngLand + lngbldg - lngExempt);
							strSQL += ",exemptcode1 = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT1)));
							strSQL += ",exemptcode2 = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT2)));
							strSQL += ",exemptcode3 = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT3)));
							strSQL += " where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLAUTOID)));
							clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
						}
						else
						{
							// new
							clsSave.AddNew();
							clsSave.Set_Fields("account", lngCurrAcct);
							if (Conversion.Val(Grid.TextMatrix(x, COLCARD)) < 1)
							{
								Grid.TextMatrix(x, COLCARD, FCConvert.ToString(1));
							}
							clsSave.Set_Fields("card", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCARD))));
							clsSave.Set_Fields("taxyear", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLYEAR))));
							lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLLAND))));
							lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLBLDG))));
							lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT))));
							clsSave.Set_Fields("land", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLLAND))));
							clsSave.Set_Fields("Building", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLBLDG))));
							clsSave.Set_Fields("Exempt", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLEXEMPT))));
							clsSave.Set_Fields("exemptcode1", 0);
							clsSave.Set_Fields("exemptcode2", 0);
							clsSave.Set_Fields("exemptcode3", 0);
							clsSave.Set_Fields("Assessment", lngLand + lngbldg - lngExempt);
							Grid.TextMatrix(x, COLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("id")));
							clsSave.Update();
						}
					}
					Grid.RowData(x, false);
				}
				// x
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + "  " + Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
                        //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
						//txtAccount_Validate(false);
                        Support.SendKeys("{TAB}");
						break;
					}
			}
			//end switch
		}

		private void txtAccount_Validating_2(bool Cancel)
		{
			txtAccount_Validating(new object(), new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngAcct;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			// strREFullDBName = rsTemp.GetFullDBName("RealEstate")
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			string strSQL = "";
			if (!Information.IsNumeric(txtAccount.Text))
			{
				MessageBox.Show("You must enter a valid account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtAccount.Text = FCConvert.ToString(lngCurrAcct);
				return;
			}
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
			if (lngAcct < 1)
			{
				MessageBox.Show("You must enter a valid account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtAccount.Text = FCConvert.ToString(lngCurrAcct);
				return;
			}
			clsLoad.OpenRecordset("select rsaccount,rsname from " + strMasterJoinQuery + " where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
			// Call clsLoad.OpenRecordset("select rsaccount,rsname from master where rsaccount = " & lngAcct, strREDatabase)
			if (!clsLoad.EndOfFile())
			{
				if (lngCurrAcct == lngAcct)
					return;
				lngCurrAcct = lngAcct;
				lblCurrentAcct.Text = FCConvert.ToString(lngCurrAcct);
				lblName.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("rsname")));
				LoadGrid();
			}
			else
			{
				MessageBox.Show("This account was not found", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtAccount.Text = FCConvert.ToString(lngCurrAcct);
				return;
			}
		}

		public void txtAccount_Validate(bool Cancel)
		{
			txtAccount_Validating(txtAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void LoadGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			Grid.Rows = 1;
			GridDelete.Rows = 0;
			clsLoad.OpenRecordset("select * from previousassessment where account = " + FCConvert.ToString(lngCurrAcct) + " order by TAXYEAR,card", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [card] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
				Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("taxyear"))) + "\t" + clsLoad.Get_Fields("card") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("land"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("building"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("exempt"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptcode1"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptcode2"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("exemptcode3"))) + "\t" + clsLoad.Get_Fields_Int32("id"));
				clsLoad.MoveNext();
			}
		}
	}
}
