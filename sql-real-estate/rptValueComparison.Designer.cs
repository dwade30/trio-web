﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptValueComparison.
	/// </summary>
	partial class rptValueComparison
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptValueComparison));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtIncDec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalIncDec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPrevBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPrevLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncDec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalIncDec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtIncDec,
				this.txtCurrTotal,
				this.txtCurrBldg,
				this.txtCurrLand,
				this.txtPrevBldg,
				this.txtPrevLand,
				this.txtAccount
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label10,
				this.txtTotalIncDec,
				this.txtTotalTotal,
				this.txtTotalCurrBldg,
				this.txtTotalCurrLand,
				this.txtTotalPrevBldg,
				this.txtTotalPrevLand
			});
			this.ReportFooter.Height = 0.3229167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label1,
				this.lblAccount,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label11,
				this.Line1,
				this.Line2,
				this.Label12,
				this.Line3,
				this.Line4
			});
			this.PageHeader.Height = 0.84375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Assessed Values Comparison";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.9375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.19F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-weight: bold; text-align: right";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.65625F;
			this.lblAccount.Width = 1.1875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.25F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Land";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.291667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Building";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 1.0625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3.416667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Land";
			this.Label6.Top = 0.65625F;
			this.Label6.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.479167F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Building";
			this.Label7.Top = 0.65625F;
			this.Label7.Width = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.5625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Total";
			this.Label8.Top = 0.65625F;
			this.Label8.Width = 1.0625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.6875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Inc/Dec";
			this.Label9.Top = 0.65625F;
			this.Label9.Width = 0.75F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: center";
			this.Label11.Text = "Billing";
			this.Label11.Top = 0.46875F;
			this.Label11.Width = 0.8020833F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.25F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5625F;
			this.Line1.Width = 0.625F;
			this.Line1.X1 = 1.25F;
			this.Line1.X2 = 1.875F;
			this.Line1.Y1 = 0.5625F;
			this.Line1.Y2 = 0.5625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.6875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5625F;
			this.Line2.Width = 0.625F;
			this.Line2.X1 = 2.6875F;
			this.Line2.X2 = 3.3125F;
			this.Line2.Y1 = 0.5625F;
			this.Line2.Y2 = 0.5625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.614583F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: center";
			this.Label12.Text = "Current";
			this.Label12.Top = 0.46875F;
			this.Label12.Width = 0.78125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 3.4375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.5625F;
			this.Line3.Width = 1.125F;
			this.Line3.X1 = 3.4375F;
			this.Line3.X2 = 4.5625F;
			this.Line3.Y1 = 0.5625F;
			this.Line3.Y2 = 0.5625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 5.4375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.5625F;
			this.Line4.Width = 1.1875F;
			this.Line4.X1 = 5.4375F;
			this.Line4.X2 = 6.625F;
			this.Line4.Y1 = 0.5625F;
			this.Line4.Y2 = 0.5625F;
			// 
			// txtIncDec
			// 
			this.txtIncDec.CanGrow = false;
			this.txtIncDec.Height = 0.19F;
			this.txtIncDec.Left = 6.6875F;
			this.txtIncDec.Name = "txtIncDec";
			this.txtIncDec.Style = "text-align: right";
			this.txtIncDec.Text = null;
			this.txtIncDec.Top = 0.03125F;
			this.txtIncDec.Width = 0.75F;
			// 
			// txtCurrTotal
			// 
			this.txtCurrTotal.Height = 0.19F;
			this.txtCurrTotal.Left = 5.510417F;
			this.txtCurrTotal.Name = "txtCurrTotal";
			this.txtCurrTotal.Style = "text-align: right";
			this.txtCurrTotal.Text = null;
			this.txtCurrTotal.Top = 0.03125F;
			this.txtCurrTotal.Width = 1.114583F;
			// 
			// txtCurrBldg
			// 
			this.txtCurrBldg.Height = 0.19F;
			this.txtCurrBldg.Left = 4.489583F;
			this.txtCurrBldg.Name = "txtCurrBldg";
			this.txtCurrBldg.Style = "text-align: right";
			this.txtCurrBldg.Text = "9,999,888,777";
			this.txtCurrBldg.Top = 0.03125F;
			this.txtCurrBldg.Width = 0.9895833F;
			// 
			// txtCurrLand
			// 
			this.txtCurrLand.Height = 0.19F;
			this.txtCurrLand.Left = 3.416667F;
			this.txtCurrLand.Name = "txtCurrLand";
			this.txtCurrLand.Style = "text-align: right";
			this.txtCurrLand.Text = null;
			this.txtCurrLand.Top = 0.03125F;
			this.txtCurrLand.Width = 1F;
			// 
			// txtPrevBldg
			// 
			this.txtPrevBldg.Height = 0.19F;
			this.txtPrevBldg.Left = 2.322917F;
			this.txtPrevBldg.Name = "txtPrevBldg";
			this.txtPrevBldg.Style = "text-align: right";
			this.txtPrevBldg.Text = null;
			this.txtPrevBldg.Top = 0.03125F;
			this.txtPrevBldg.Width = 1.03125F;
			// 
			// txtPrevLand
			// 
			this.txtPrevLand.Height = 0.19F;
			this.txtPrevLand.Left = 1.25F;
			this.txtPrevLand.Name = "txtPrevLand";
			this.txtPrevLand.Style = "text-align: right";
			this.txtPrevLand.Text = null;
			this.txtPrevLand.Top = 0.03125F;
			this.txtPrevLand.Width = 1F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "text-align: left";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 1.197917F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Total";
			this.Label10.Top = 0.15625F;
			this.Label10.Width = 0.5625F;
			// 
			// txtTotalIncDec
			// 
			this.txtTotalIncDec.CanGrow = false;
			this.txtTotalIncDec.Height = 0.19F;
			this.txtTotalIncDec.Left = 6.6875F;
			this.txtTotalIncDec.Name = "txtTotalIncDec";
			this.txtTotalIncDec.Style = "text-align: right";
			this.txtTotalIncDec.Text = null;
			this.txtTotalIncDec.Top = 0.15625F;
			this.txtTotalIncDec.Width = 0.75F;
			// 
			// txtTotalTotal
			// 
			this.txtTotalTotal.Height = 0.19F;
			this.txtTotalTotal.Left = 5.5F;
			this.txtTotalTotal.Name = "txtTotalTotal";
			this.txtTotalTotal.Style = "text-align: right";
			this.txtTotalTotal.Text = null;
			this.txtTotalTotal.Top = 0.15625F;
			this.txtTotalTotal.Width = 1.125F;
			// 
			// txtTotalCurrBldg
			// 
			this.txtTotalCurrBldg.Height = 0.19F;
			this.txtTotalCurrBldg.Left = 4.489583F;
			this.txtTotalCurrBldg.Name = "txtTotalCurrBldg";
			this.txtTotalCurrBldg.Style = "text-align: right";
			this.txtTotalCurrBldg.Text = null;
			this.txtTotalCurrBldg.Top = 0.15625F;
			this.txtTotalCurrBldg.Width = 0.9895833F;
			// 
			// txtTotalCurrLand
			// 
			this.txtTotalCurrLand.Height = 0.19F;
			this.txtTotalCurrLand.Left = 3.416667F;
			this.txtTotalCurrLand.Name = "txtTotalCurrLand";
			this.txtTotalCurrLand.Style = "text-align: right";
			this.txtTotalCurrLand.Text = null;
			this.txtTotalCurrLand.Top = 0.15625F;
			this.txtTotalCurrLand.Width = 1F;
			// 
			// txtTotalPrevBldg
			// 
			this.txtTotalPrevBldg.Height = 0.19F;
			this.txtTotalPrevBldg.Left = 2.3125F;
			this.txtTotalPrevBldg.Name = "txtTotalPrevBldg";
			this.txtTotalPrevBldg.Style = "text-align: right";
			this.txtTotalPrevBldg.Text = null;
			this.txtTotalPrevBldg.Top = 0.15625F;
			this.txtTotalPrevBldg.Width = 1.041667F;
			// 
			// txtTotalPrevLand
			// 
			this.txtTotalPrevLand.Height = 0.19F;
			this.txtTotalPrevLand.Left = 1.1875F;
			this.txtTotalPrevLand.Name = "txtTotalPrevLand";
			this.txtTotalPrevLand.Style = "text-align: right";
			this.txtTotalPrevLand.Text = null;
			this.txtTotalPrevLand.Top = 0.15625F;
			this.txtTotalPrevLand.Width = 1.0625F;
			// 
			// rptValueComparison
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncDec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalIncDec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIncDec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalIncDec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPrevBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPrevLand;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
