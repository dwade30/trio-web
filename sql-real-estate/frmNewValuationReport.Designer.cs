//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewValuationReport.
	/// </summary>
	partial class frmNewValuationReport : BaseForm
	{
		public fecherFoundation.FCTextBox txtCardNumber;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCGrid PropertyGrid;
		public fecherFoundation.FCGrid SaleGrid;
		public fecherFoundation.FCGrid MiscGrid;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCGrid DwellingGrid;
		public fecherFoundation.FCGrid DwellCondGrid;
		public fecherFoundation.FCGrid DwellDescGrid;
		public fecherFoundation.FCGrid DwellMiscGrid;
		public fecherFoundation.FCGrid DwellPercGrid;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCGrid CommercialGrid;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCGrid OutBuildingGrid;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCGrid CorrelationGrid;
		public fecherFoundation.FCTextBox txtLandFrom;
		public fecherFoundation.FCTextBox txtBldgFrom;
		public fecherFoundation.FCTabPage SSTab1_Page6;
		public fecherFoundation.FCGrid TotalGrid;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel lblNumCards;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel lblCard;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblSalesRecord;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		//FC:FINAL:MSH - i.issue #1192: remove items from menu
		//public fecherFoundation.FCToolStripMenuItem mnuPrintCard;
		//public fecherFoundation.FCToolStripMenuItem mnuPrintCardCalc;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAccount;
		public fecherFoundation.FCToolStripMenuItem mnuCurrentAccountCalc;
		public fecherFoundation.FCToolStripMenuItem mnuPrintForm;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewValuationReport));
			this.txtCardNumber = new fecherFoundation.FCTextBox();
			this.txtName = new fecherFoundation.FCTextBox();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.PropertyGrid = new fecherFoundation.FCGrid();
			this.SaleGrid = new fecherFoundation.FCGrid();
			this.MiscGrid = new fecherFoundation.FCGrid();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.DwellingGrid = new fecherFoundation.FCGrid();
			this.DwellCondGrid = new fecherFoundation.FCGrid();
			this.DwellDescGrid = new fecherFoundation.FCGrid();
			this.DwellMiscGrid = new fecherFoundation.FCGrid();
			this.DwellPercGrid = new fecherFoundation.FCGrid();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.CommercialGrid = new fecherFoundation.FCGrid();
			this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
			this.OutBuildingGrid = new fecherFoundation.FCGrid();
			this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Label19 = new fecherFoundation.FCLabel();
			this.CorrelationGrid = new fecherFoundation.FCGrid();
			this.txtLandFrom = new fecherFoundation.FCTextBox();
			this.txtBldgFrom = new fecherFoundation.FCTextBox();
			this.SSTab1_Page6 = new fecherFoundation.FCTabPage();
			this.TotalGrid = new fecherFoundation.FCGrid();
			this.Label20 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.lblNumCards = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.lblMapLot = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblLocation = new fecherFoundation.FCLabel();
			this.lblCard = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblSalesRecord = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuPrintAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCurrentAccountCalc = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintForm = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeparator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdPrintCard = new fecherFoundation.FCButton();
			this.cmdPrintCardCalc = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PropertyGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MiscGrid)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.DwellingGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellCondGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellDescGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellMiscGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellPercGrid)).BeginInit();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CommercialGrid)).BeginInit();
			this.SSTab1_Page4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.OutBuildingGrid)).BeginInit();
			this.SSTab1_Page5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CorrelationGrid)).BeginInit();
			this.SSTab1_Page6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCardCalc)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtCardNumber);
			this.ClientArea.Controls.Add(this.txtName);
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.Label20);
			this.ClientArea.Controls.Add(this.Label11);
			this.ClientArea.Controls.Add(this.lblNumCards);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.lblMapLot);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblLocation);
			this.ClientArea.Controls.Add(this.lblCard);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblSalesRecord);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintCard);
			this.TopPanel.Controls.Add(this.cmdPrintCardCalc);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintCardCalc, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintCard, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(196, 30);
			this.HeaderText.Text = "Valuation Report";
			// 
			// txtCardNumber
			// 
			this.txtCardNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtCardNumber.Location = new System.Drawing.Point(292, 30);
			this.txtCardNumber.Name = "txtCardNumber";
			this.txtCardNumber.Size = new System.Drawing.Size(48, 40);
			this.txtCardNumber.TabIndex = 39;
			this.txtCardNumber.Text = "1";
			this.txtCardNumber.TextChanged += new System.EventHandler(this.txtCardNumber_TextChanged);
			// 
			// txtName
			// 
			this.txtName.Appearance = 0;
			this.txtName.BackColor = System.Drawing.SystemColors.Menu;
			this.txtName.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtName.Location = new System.Drawing.Point(102, 90);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(256, 40);
			this.txtName.TabIndex = 34;
			// 
			// SSTab1
			// 
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Controls.Add(this.SSTab1_Page4);
			this.SSTab1.Controls.Add(this.SSTab1_Page5);
			this.SSTab1.Controls.Add(this.SSTab1_Page6);
			this.SSTab1.Location = new System.Drawing.Point(30, 188);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(993, 650);
			this.SSTab1.TabIndex = 40;
			this.SSTab1.Text = "Correlation";
			this.SSTab1.SelectedIndexChanged += new System.EventHandler(this.SSTab1_SelectedIndexChanged);
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.Label6);
			this.SSTab1_Page1.Controls.Add(this.Label7);
			this.SSTab1_Page1.Controls.Add(this.PropertyGrid);
			this.SSTab1_Page1.Controls.Add(this.SaleGrid);
			this.SSTab1_Page1.Controls.Add(this.MiscGrid);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Size = new System.Drawing.Size(991, 614);
			this.SSTab1_Page1.Text = "Property";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 300);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(120, 18);
			this.Label6.TabIndex = 13;
			this.Label6.Text = "LAND DESCRIPTION";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(469, 30);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(75, 18);
			this.Label7.TabIndex = 14;
			this.Label7.Text = "SALE DATA";
			// 
			// PropertyGrid
			// 
			this.PropertyGrid.Cols = 10;
			this.PropertyGrid.Location = new System.Drawing.Point(20, 338);
			this.PropertyGrid.Name = "PropertyGrid";
			this.PropertyGrid.Rows = 50;
			this.PropertyGrid.ShowFocusCell = false;
			this.PropertyGrid.Size = new System.Drawing.Size(950, 242);
			this.PropertyGrid.TabIndex = 12;
			// 
			// SaleGrid
			// 
			this.SaleGrid.Cols = 10;
			this.SaleGrid.Location = new System.Drawing.Point(465, 68);
			this.SaleGrid.Name = "SaleGrid";
			this.SaleGrid.Rows = 50;
			this.SaleGrid.ShowFocusCell = false;
			this.SaleGrid.Size = new System.Drawing.Size(505, 212);
			this.SaleGrid.TabIndex = 6;
			// 
			// MiscGrid
			// 
			this.MiscGrid.Cols = 10;
			this.MiscGrid.ExtendLastCol = true;
			this.MiscGrid.Location = new System.Drawing.Point(20, 30);
			this.MiscGrid.Name = "MiscGrid";
			this.MiscGrid.Rows = 50;
			this.MiscGrid.ShowFocusCell = false;
			this.MiscGrid.Size = new System.Drawing.Size(424, 250);
			this.MiscGrid.TabIndex = 5;
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.Label8);
			this.SSTab1_Page2.Controls.Add(this.Label9);
			this.SSTab1_Page2.Controls.Add(this.Label10);
			this.SSTab1_Page2.Controls.Add(this.DwellingGrid);
			this.SSTab1_Page2.Controls.Add(this.DwellCondGrid);
			this.SSTab1_Page2.Controls.Add(this.DwellDescGrid);
			this.SSTab1_Page2.Controls.Add(this.DwellMiscGrid);
			this.SSTab1_Page2.Controls.Add(this.DwellPercGrid);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Size = new System.Drawing.Size(991, 614);
			this.SSTab1_Page2.Text = "Dwelling";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(174, 20);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(150, 18);
			this.Label8.TabIndex = 15;
			this.Label8.Text = "DWELLING DESCRIPTION";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 379);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(140, 18);
			this.Label9.TabIndex = 16;
			this.Label9.Text = "DWELLING CONDITION";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(667, 20);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(130, 18);
			this.Label10.TabIndex = 17;
			this.Label10.Text = "REPLACEMENT COST";
			// 
			// DwellingGrid
			// 
			this.DwellingGrid.Cols = 10;
			this.DwellingGrid.Location = new System.Drawing.Point(667, 48);
			this.DwellingGrid.Name = "DwellingGrid";
			this.DwellingGrid.Rows = 50;
			this.DwellingGrid.ShowFocusCell = false;
			this.DwellingGrid.Size = new System.Drawing.Size(304, 311);
			this.DwellingGrid.TabIndex = 11;
			// 
			// DwellCondGrid
			// 
			this.DwellCondGrid.Cols = 10;
			this.DwellCondGrid.Location = new System.Drawing.Point(20, 417);
			this.DwellCondGrid.Name = "DwellCondGrid";
			this.DwellCondGrid.Rows = 50;
			this.DwellCondGrid.ShowFocusCell = false;
			this.DwellCondGrid.Size = new System.Drawing.Size(951, 72);
			this.DwellCondGrid.TabIndex = 4;
			// 
			// DwellDescGrid
			// 
			this.DwellDescGrid.Cols = 10;
			this.DwellDescGrid.Location = new System.Drawing.Point(20, 48);
			this.DwellDescGrid.Name = "DwellDescGrid";
			this.DwellDescGrid.Rows = 50;
			this.DwellDescGrid.ShowFocusCell = false;
			this.DwellDescGrid.Size = new System.Drawing.Size(304, 311);
			this.DwellDescGrid.TabIndex = 3;
			// 
			// DwellMiscGrid
			// 
			this.DwellMiscGrid.Cols = 10;
			this.DwellMiscGrid.Location = new System.Drawing.Point(344, 48);
			this.DwellMiscGrid.Name = "DwellMiscGrid";
			this.DwellMiscGrid.Rows = 50;
			this.DwellMiscGrid.ShowFocusCell = false;
			this.DwellMiscGrid.Size = new System.Drawing.Size(304, 311);
			this.DwellMiscGrid.TabIndex = 2;
			// 
			// DwellPercGrid
			// 
			this.DwellPercGrid.Cols = 10;
			this.DwellPercGrid.Location = new System.Drawing.Point(20, 513);
			this.DwellPercGrid.Name = "DwellPercGrid";
			this.DwellPercGrid.Rows = 50;
			this.DwellPercGrid.ShowFocusCell = false;
			this.DwellPercGrid.Size = new System.Drawing.Size(951, 72);
			this.DwellPercGrid.TabIndex = 1;
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.CommercialGrid);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Size = new System.Drawing.Size(991, 614);
			this.SSTab1_Page3.Text = "Commercial";
			// 
			// CommercialGrid
			// 
			this.CommercialGrid.Cols = 10;
			this.CommercialGrid.Dock = Wisej.Web.DockStyle.Fill;
			this.CommercialGrid.Name = "CommercialGrid";
			this.CommercialGrid.Rows = 50;
			this.CommercialGrid.ShowFocusCell = false;
			this.CommercialGrid.Size = new System.Drawing.Size(991, 614);
			this.CommercialGrid.TabIndex = 10;
			// 
			// SSTab1_Page4
			// 
			this.SSTab1_Page4.Controls.Add(this.OutBuildingGrid);
			this.SSTab1_Page4.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page4.Name = "SSTab1_Page4";
			this.SSTab1_Page4.Size = new System.Drawing.Size(991, 614);
			this.SSTab1_Page4.Text = "Outbuilding";
			// 
			// OutBuildingGrid
			// 
			this.OutBuildingGrid.AppearanceKey = "groupBoxNoBorders";
			this.OutBuildingGrid.Cols = 10;
			this.OutBuildingGrid.Dock = Wisej.Web.DockStyle.Fill;
			this.OutBuildingGrid.Name = "OutBuildingGrid";
			this.OutBuildingGrid.Rows = 50;
			this.OutBuildingGrid.ShowFocusCell = false;
			this.OutBuildingGrid.Size = new System.Drawing.Size(991, 614);
			this.OutBuildingGrid.TabIndex = 9;
			// 
			// SSTab1_Page5
			// 
			this.SSTab1_Page5.Controls.Add(this.Label12);
			this.SSTab1_Page5.Controls.Add(this.Label13);
			this.SSTab1_Page5.Controls.Add(this.Label14);
			this.SSTab1_Page5.Controls.Add(this.Label15);
			this.SSTab1_Page5.Controls.Add(this.Label16);
			this.SSTab1_Page5.Controls.Add(this.Label17);
			this.SSTab1_Page5.Controls.Add(this.Label18);
			this.SSTab1_Page5.Controls.Add(this.Label19);
			this.SSTab1_Page5.Controls.Add(this.CorrelationGrid);
			this.SSTab1_Page5.Controls.Add(this.txtLandFrom);
			this.SSTab1_Page5.Controls.Add(this.txtBldgFrom);
			this.SSTab1_Page5.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page5.Name = "SSTab1_Page5";
			this.SSTab1_Page5.Size = new System.Drawing.Size(991, 614);
			this.SSTab1_Page5.Text = "Correlation";
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(176, 35);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(20, 18);
			this.Label12.TabIndex = 28;
			this.Label12.Text = "(1)";
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(285, 35);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(20, 18);
			this.Label13.TabIndex = 29;
			this.Label13.Text = "(2)";
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(380, 35);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(20, 18);
			this.Label14.TabIndex = 30;
			this.Label14.Text = "(3)";
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(490, 35);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(20, 18);
			this.Label15.TabIndex = 31;
			this.Label15.Text = "(4)";
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(600, 35);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(20, 18);
			this.Label16.TabIndex = 32;
			this.Label16.Text = "(5)";
			// 
			// Label17
			// 
			this.Label17.Location = new System.Drawing.Point(690, 35);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(20, 18);
			this.Label17.TabIndex = 33;
			this.Label17.Text = "(6)";
			// 
			// Label18
			// 
			this.Label18.Location = new System.Drawing.Point(20, 429);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(80, 18);
			this.Label18.TabIndex = 35;
			this.Label18.Text = "LAND FROM";
			// 
			// Label19
			// 
			this.Label19.Location = new System.Drawing.Point(20, 489);
			this.Label19.Name = "Label19";
			this.Label19.Size = new System.Drawing.Size(98, 18);
			this.Label19.TabIndex = 36;
			this.Label19.Text = "BUILDING FROM";
			// 
			// CorrelationGrid
			// 
			this.CorrelationGrid.Cols = 10;
			this.CorrelationGrid.Location = new System.Drawing.Point(10, 58);
			this.CorrelationGrid.Name = "CorrelationGrid";
			this.CorrelationGrid.Rows = 50;
			this.CorrelationGrid.ShowFocusCell = false;
			this.CorrelationGrid.Size = new System.Drawing.Size(960, 327);
			this.CorrelationGrid.TabIndex = 8;
			this.CorrelationGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.CorrelationGrid_ValidateEdit);
			this.CorrelationGrid.CurrentCellChanged += new System.EventHandler(this.CorrelationGrid_RowColChange);
			this.CorrelationGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.CorrelationGrid_KeyDownEvent);
			// 
			// txtLandFrom
			// 
			this.txtLandFrom.BackColor = System.Drawing.SystemColors.Window;
			this.txtLandFrom.Location = new System.Drawing.Point(158, 415);
			this.txtLandFrom.MaxLength = 1;
			this.txtLandFrom.Name = "txtLandFrom";
			this.txtLandFrom.Size = new System.Drawing.Size(85, 40);
			this.txtLandFrom.TabIndex = 37;
			this.txtLandFrom.KeyDown += new Wisej.Web.KeyEventHandler(this.txtLandFrom_KeyDown);
			this.txtLandFrom.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLandFrom_KeyPress);
			this.txtLandFrom.KeyUp += new Wisej.Web.KeyEventHandler(this.txtLandFrom_KeyUp);
			// 
			// txtBldgFrom
			// 
			this.txtBldgFrom.BackColor = System.Drawing.SystemColors.Window;
			this.txtBldgFrom.Location = new System.Drawing.Point(158, 475);
			this.txtBldgFrom.MaxLength = 1;
			this.txtBldgFrom.Name = "txtBldgFrom";
			this.txtBldgFrom.Size = new System.Drawing.Size(85, 40);
			this.txtBldgFrom.TabIndex = 38;
			this.txtBldgFrom.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBldgFrom_KeyDown);
			this.txtBldgFrom.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBldgFrom_KeyPress);
			this.txtBldgFrom.KeyUp += new Wisej.Web.KeyEventHandler(this.txtBldgFrom_KeyUp);
			// 
			// SSTab1_Page6
			// 
			this.SSTab1_Page6.Controls.Add(this.TotalGrid);
			this.SSTab1_Page6.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page6.Name = "SSTab1_Page6";
			this.SSTab1_Page6.Size = new System.Drawing.Size(991, 614);
			this.SSTab1_Page6.Text = "Total";
			// 
			// TotalGrid
			// 
			this.TotalGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.TotalGrid.Cols = 10;
			this.TotalGrid.Location = new System.Drawing.Point(20, 20);
			this.TotalGrid.Name = "TotalGrid";
			this.TotalGrid.Rows = 50;
			this.TotalGrid.ShowFocusCell = false;
			this.TotalGrid.Size = new System.Drawing.Size(950, 500);
			this.TotalGrid.TabIndex = 7;
			// 
			// Label20
			// 
			this.Label20.Location = new System.Drawing.Point(220, 44);
			this.Label20.Name = "Label20";
			this.Label20.Size = new System.Drawing.Size(37, 18);
			this.Label20.TabIndex = 42;
			this.Label20.Text = "CARD";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(375, 44);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(20, 18);
			this.Label11.TabIndex = 41;
			this.Label11.Text = "OF";
			// 
			// lblNumCards
			// 
			this.lblNumCards.Location = new System.Drawing.Point(420, 44);
			this.lblNumCards.Name = "lblNumCards";
			this.lblNumCards.Size = new System.Drawing.Size(37, 18);
			this.lblNumCards.TabIndex = 40;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(383, 104);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(62, 18);
			this.Label5.TabIndex = 27;
			this.Label5.Text = "MAP/LOT";
			// 
			// lblMapLot
			// 
			this.lblMapLot.Location = new System.Drawing.Point(465, 104);
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Size = new System.Drawing.Size(220, 20);
			this.lblMapLot.TabIndex = 26;
			this.lblMapLot.Text = "MAP/LOT";
			this.lblMapLot.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(383, 140);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(62, 18);
			this.Label4.TabIndex = 25;
			this.Label4.Text = "LOCATION";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(37, 18);
			this.Label2.TabIndex = 24;
			this.Label2.Text = "NAME";
			// 
			// lblLocation
			// 
			this.lblLocation.Location = new System.Drawing.Point(465, 140);
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Size = new System.Drawing.Size(220, 20);
			this.lblLocation.TabIndex = 23;
			this.lblLocation.Text = "LOCATION";
			this.lblLocation.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblCard
			// 
			this.lblCard.Location = new System.Drawing.Point(246, 140);
			this.lblCard.Name = "lblCard";
			this.lblCard.Size = new System.Drawing.Size(28, 19);
			this.lblCard.TabIndex = 22;
			this.lblCard.Text = "000";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(171, 140);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(40, 18);
			this.Label3.TabIndex = 21;
			this.Label3.Text = "CARD";
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(126, 140);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(35, 18);
			this.lblAccount.TabIndex = 20;
			this.lblAccount.Text = "0000";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 140);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(61, 18);
			this.Label1.TabIndex = 19;
			this.Label1.Text = "ACCOUNT";
			// 
			// lblSalesRecord
			// 
			this.lblSalesRecord.Font = new System.Drawing.Font("semibold", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblSalesRecord.Location = new System.Drawing.Point(30, 40);
			this.lblSalesRecord.Name = "lblSalesRecord";
			this.lblSalesRecord.Size = new System.Drawing.Size(170, 25);
			this.lblSalesRecord.TabIndex = 18;
			this.lblSalesRecord.Text = "SALES RECORD";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintAccount,
            this.mnuCurrentAccountCalc,
            this.mnuPrintForm});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuPrintAccount
			// 
			this.mnuPrintAccount.Index = 0;
			this.mnuPrintAccount.Name = "mnuPrintAccount";
			this.mnuPrintAccount.Text = "Print Current Account  (Use accepted values)";
			this.mnuPrintAccount.Click += new System.EventHandler(this.mnuPrintAccount_Click);
			// 
			// mnuCurrentAccountCalc
			// 
			this.mnuCurrentAccountCalc.Index = 1;
			this.mnuCurrentAccountCalc.Name = "mnuCurrentAccountCalc";
			this.mnuCurrentAccountCalc.Text = "Print Current Account  (Use calculated values)";
			this.mnuCurrentAccountCalc.Click += new System.EventHandler(this.mnuCurrentAccountCalc_Click);
			// 
			// mnuPrintForm
			// 
			this.mnuPrintForm.Index = 2;
			this.mnuPrintForm.Name = "mnuPrintForm";
			this.mnuPrintForm.Text = "Print Valuation Screen";
			this.mnuPrintForm.Click += new System.EventHandler(this.mnuPrintForm_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSeparator,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar2,
            this.mnuQuit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSeparator
			// 
			this.mnuSeparator.Index = 0;
			this.mnuSeparator.Name = "mnuSeparator";
			this.mnuSeparator.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 1;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 2;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 3;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 4;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Quit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(357, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// cmdPrintCard
			// 
			this.cmdPrintCard.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintCard.Location = new System.Drawing.Point(859, 29);
			this.cmdPrintCard.Name = "cmdPrintCard";
			this.cmdPrintCard.Size = new System.Drawing.Size(260, 24);
			this.cmdPrintCard.TabIndex = 1;
			this.cmdPrintCard.Text = "Print Current Card (Use accepted values)";
			this.cmdPrintCard.Click += new System.EventHandler(this.mnuPrintCard_Click);
			// 
			// cmdPrintCardCalc
			// 
			this.cmdPrintCardCalc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintCardCalc.Location = new System.Drawing.Point(588, 29);
			this.cmdPrintCardCalc.Name = "cmdPrintCardCalc";
			this.cmdPrintCardCalc.Size = new System.Drawing.Size(265, 24);
			this.cmdPrintCardCalc.TabIndex = 2;
			this.cmdPrintCardCalc.Text = "Print Current Card (Use calculated values)";
			this.cmdPrintCardCalc.Click += new System.EventHandler(this.mnuPrintCardCalc_Click);
			// 
			// frmNewValuationReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmNewValuationReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Valuation Report";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmNewValuationReport_Load);
			this.Resize += new System.EventHandler(this.frmNewValuationReport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewValuationReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PropertyGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MiscGrid)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.DwellingGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellCondGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellDescGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellMiscGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DwellPercGrid)).EndInit();
			this.SSTab1_Page3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.CommercialGrid)).EndInit();
			this.SSTab1_Page4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.OutBuildingGrid)).EndInit();
			this.SSTab1_Page5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.CorrelationGrid)).EndInit();
			this.SSTab1_Page6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCardCalc)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrintCardCalc;
		private FCButton cmdPrintCard;
	}
}