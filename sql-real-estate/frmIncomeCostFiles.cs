﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmIncomeCostFiles.
	/// </summary>
	public partial class frmIncomeCostFiles : BaseForm
	{
		public frmIncomeCostFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmIncomeCostFiles InstancePtr
		{
			get
			{
				return (frmIncomeCostFiles)Sys.GetInstance(typeof(frmIncomeCostFiles));
			}
		}

		protected frmIncomeCostFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intWhichFunction;
		// used to tell what this form is showing
		private void Command1_Click(object sender, System.EventArgs e)
		{
			int intTemp;
			int x;
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Text1.Text)));
			x = Grid.FindRow(intTemp, 1, 0);
			if (x > 0)
			{
				Grid.Row = x;
				Grid.TopRow = x;
			}
			else
			{
				MessageBox.Show("That number does not exist in this collection of Cost Files", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			Text1.Text = string.Empty;
		}

		private void frmIncomeCostFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmIncomeCostFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmIncomeCostFiles properties;
			//frmIncomeCostFiles.ScaleWidth	= 9045;
			//frmIncomeCostFiles.ScaleHeight	= 7245;
			//frmIncomeCostFiles.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
		}

		private void frmIncomeCostFiles_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			Grid.TextMatrix(Grid.GetFlexRowIndex(e.RowIndex), 3, "True");
			if (Grid.GetFlexColIndex(e.ColumnIndex) == 2)
			{
				Grid.EditText = Strings.Format(Grid.EditText, "##0.00");
			}
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngCode As int	OnWrite(double, short)
			int lngCode = 0;
			if (Grid.Rows > 0)
			{
				lngCode = FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(Grid.Rows - 1, 0)) + 1);
			}
			else
			{
				lngCode = 1;
			}
			Grid.AddItem(FCConvert.ToString(lngCode));
			Grid.Row = Grid.Rows - 1;
			Grid.TopRow = Grid.Row;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
        {
            //FC:FINAL:CHN: Uncomment closing form.
            mnuExit_Click(null, new System.EventArgs());
		}

		private void SetupGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			Grid.ExtendLastCol = true;
			Grid.Cols = 7;
			Grid.ColHidden(3, true);
			Grid.ColHidden(4, true);
			Grid.ColHidden(5, true);
			Grid.ColHidden(6, true);
			Grid.TextMatrix(0, 0, "Code");
			Grid.TextMatrix(0, 1, "Description");
			Grid.TextMatrix(0, 2, "Rate");
			switch (intWhichFunction)
			{
				case 1:
					{
						// occupancy codes
						clsLoad.OpenRecordset("select * from OccupancyCodes order by code", modGlobalVariables.strREDatabase);
						while (!clsLoad.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
							Grid.AddItem(clsLoad.Get_Fields("code") + "\t" + clsLoad.Get_Fields_String("description") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("rate")), "##0.00") + "\t" + "False" + "\t" + clsLoad.Get_Fields_String("shortdescription") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("unit"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("base"))));
							clsLoad.MoveNext();
						}
						break;
					}
				case 2:
					{
						// expense codes
						clsLoad.OpenRecordset("select * from ExpenseTable order by code", modGlobalVariables.strREDatabase);
						while (!clsLoad.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
							Grid.AddItem(clsLoad.Get_Fields("code") + "\t" + clsLoad.Get_Fields_String("description") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("rate")), "##0.00") + "\t" + "False");
							clsLoad.MoveNext();
						}
						break;
					}
				case 3:
					{
						// standard overall rate
						clsLoad.OpenRecordset("select OverallRate from Defaults ", modGlobalVariables.strREDatabase);
						if (!clsLoad.EndOfFile())
						{
							Grid.AddItem("\t" + "Standard Overall Rate" + "\t" + Strings.Format(clsLoad.Get_Fields_Double("overallrate"), "##0.00"));
						}
						else
						{
							Grid.AddItem("\t" + "Standard Overall Rate" + "\t" + "0.00");
						}
						break;
					}
			}
			//end switch
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.76 * GridWidth));
			Grid.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			//FC:FINAL:AAKV -anchored in design
			//if (intWhichFunction == 3)
			//{
			//    Grid.Height = Grid.RowHeight(0) * 2 + 60;
			//}
		}

		public void Init(short intFunction)
		{
			intWhichFunction = intFunction;
			switch (intWhichFunction)
			{
				case 1:
					{
						this.Text = "Occupancy Codes";
						break;
					}
				case 2:
					{
						this.Text = "Expense Codes";
						break;
					}
				case 3:
					{
						this.Text = "Standard Overall Rate";
						cmdNew.Visible = false;
						Text1.Visible = false;
						Command1.Visible = false;
						break;
					}
			}
			//end switch
			SetupGrid();
			this.Show(App.MainForm);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int intTemp = 0;
			string strTemp1 = "";
			string strTemp2 = "";
			string strSQL = "";
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			switch (intWhichFunction)
			{
				case 1:
					{
						// occupancy codes
						intTemp = Grid.FindRow("True", 1, 3);
						while (intTemp > 0)
						{
							strSQL = "";
							strTemp1 = Grid.TextMatrix(intTemp, 1);
							modGlobalRoutines.escapequote(ref strTemp1);
							strTemp2 = Grid.TextMatrix(intTemp, 4);
							modGlobalRoutines.escapequote(ref strTemp2);
							strSQL = "(" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intTemp, 0))) + ",'" + strTemp1 + "'," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intTemp, 2))) + ",'" + strTemp2 + "'," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intTemp, 5))) + "," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intTemp, 6))) + ")";
							clsSave.Execute("delete from occupancycodes where code = " + Grid.TextMatrix(intTemp, 0), modGlobalVariables.strREDatabase);
							clsSave.Execute("insert into occupancycodes (code,description,rate,shortdescription,unit,base) values " + strSQL, modGlobalVariables.strREDatabase);
							intTemp = Grid.FindRow("True", intTemp + 1, 3);
						}
						Grid.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 5, Grid.Rows - 1, 5, "False");
						break;
					}
				case 2:
					{
						intTemp = Grid.FindRow("True", 1, 3);
						while (intTemp > 0)
						{
							strSQL = "";
							strTemp1 = Grid.TextMatrix(intTemp, 1);
							strSQL = "(" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intTemp, 0))) + ",'" + strTemp1 + "'," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intTemp, 2))) + ")";
							clsSave.Execute("delete from expensetable where code = " + Grid.TextMatrix(intTemp, 0), modGlobalVariables.strREDatabase);
							clsSave.Execute("insert into expensetable (code,description,rate) values " + strSQL, modGlobalVariables.strREDatabase);
							intTemp = Grid.FindRow("True", intTemp + 1, 3);
						}
						Grid.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 5, Grid.Rows - 1, 5, "False");
						break;
					}
				case 3:
					{
						// standard rate
						clsSave.Execute("update defaults set OverallRate = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(1, 2))), modGlobalVariables.strREDatabase);
						break;
					}
			}
			//end switch
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            //FC:FINAL:BSE #3347 add save successful message
            FCMessageBox.Show("Save Successful.", MsgBoxStyle.Information , "Save Complete");
            //mnuExit_Click();
		}
	}
}
