﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCostFiles.
	/// </summary>
	partial class frmCostFiles : BaseForm
	{
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCGrid Grid1;
		public FCGrid gridTownCode;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCostFiles));
            this.Text1 = new fecherFoundation.FCTextBox();
            this.Command1 = new fecherFoundation.FCButton();
            this.Grid1 = new fecherFoundation.FCGrid();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(619, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Text1);
            this.ClientArea.Controls.Add(this.Command1);
            this.ClientArea.Controls.Add(this.Grid1);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Size = new System.Drawing.Size(619, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(619, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(121, 30);
            this.HeaderText.Text = "Cost Files";
            // 
            // Text1
            // 
            this.Text1.BackColor = System.Drawing.SystemColors.Window;
            this.Text1.Location = new System.Drawing.Point(200, 30);
            this.Text1.Name = "Text1";
            this.Text1.Size = new System.Drawing.Size(150, 40);
            this.Text1.TabIndex = 3;
            this.Text1.KeyDown += new Wisej.Web.KeyEventHandler(this.Text1_KeyDown);
            // 
            // Command1
            // 
            this.Command1.AppearanceKey = "actionButton";
            this.Command1.Location = new System.Drawing.Point(370, 30);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(90, 40);
            this.Command1.TabIndex = 2;
            this.Command1.Text = "Select";
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // Grid1
            // 
            this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid1.Cols = 5;
            this.Grid1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid1.ExtendLastCol = true;
            this.Grid1.Location = new System.Drawing.Point(30, 90);
            this.Grid1.Name = "Grid1";
            this.Grid1.ReadOnly = false;
            this.Grid1.Rows = 50;
            this.Grid1.Size = new System.Drawing.Size(561, 378);
            this.Grid1.StandardTab = false;
            this.Grid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid1.TabIndex = 4;
            this.Grid1.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid1_AfterEdit);
            this.Grid1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid1_ValidateEdit);
            // 
            // gridTownCode
            // 
            this.gridTownCode.Cols = 1;
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(30, 30);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(150, 40);
            this.gridTownCode.TabIndex = 5;
            this.gridTownCode.Visible = false;
            this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
            this.gridTownCode.Leave += new System.EventHandler(this.gridTownCode_Leave);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(493, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(98, 24);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print Preview";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(234, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmCostFiles
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(619, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCostFiles";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Cost Files";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCostFiles_Load);
            this.Activated += new System.EventHandler(this.frmCostFiles_Activated);
            this.Resize += new System.EventHandler(this.frmCostFiles_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCostFiles_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
