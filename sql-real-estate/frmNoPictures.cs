﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNoPictures.
	/// </summary>
	public partial class frmNoPictures : BaseForm
	{
		public frmNoPictures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNoPictures InstancePtr
		{
			get
			{
				return (frmNoPictures)Sys.GetInstance(typeof(frmNoPictures));
			}
		}

		protected frmNoPictures _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdNo_Click(object sender, System.EventArgs e)
		{
			// frmnewreproperty.SetFocus
			this.Unload();
		}

		private void cmdYes_Click(object sender, System.EventArgs e)
		{
			string strprefix;
			clsDRWrapper clsTemp = new clsDRWrapper();
			//FileSystemObject fso = new FileSystemObject();
			string strFileName;
			string[] strAry = null;
			string[] strTempARY = null;
			string strSrcDir = "";
			int intStart = 0;
			int x;
			int y = 0;
			try
			{
				// On Error GoTo ErrorHandler
				strprefix = Strings.Trim(txtPrefix.Text);
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNAllowMultiselect+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNExplorer	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				clsTemp.OpenRecordset("select * from defaultpicturelocations", modGlobalVariables.strREDatabase);
				if (clsTemp.ErrorNumber != 0)
				{
					MessageBox.Show("Error Opening defaultpicturelocation table", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				while (!clsTemp.EndOfFile())
				{
					if (Directory.Exists(FCConvert.ToString(clsTemp.Get_Fields_String("location"))))
					{
						MDIParent.InstancePtr.CommonDialog1.InitDir = FCConvert.ToString(clsTemp.Get_Fields_String("location"));
						break;
					}
					clsTemp.MoveNext();
				}
				if (strprefix != string.Empty)
				{
					MDIParent.InstancePtr.CommonDialog1.FileName = strprefix + "*";
				}
				else
				{
					MDIParent.InstancePtr.CommonDialog1.FileName = "";
				}
				MDIParent.InstancePtr.CommonDialog1.Filter = "Pictures|*.jpg;*.bmp;*.gif";
				//Application.DoEvents();
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFileName = Strings.Trim(MDIParent.InstancePtr.CommonDialog1.FileName);
				if (strFileName != string.Empty)
				{
					// make sure this isn't a prefix
					if (FCConvert.ToBoolean(~Strings.InStr(1, strFileName, "*", CompareConstants.vbTextCompare)))
					{
						// so far so good
						// check for multiple choices
						strTempARY = Strings.Split(strFileName, FCConvert.ToString(Convert.ToChar(0)), -1, CompareConstants.vbBinaryCompare);
						if (Information.UBound(strTempARY, 1) > 0)
						{
							// more than one. The first in the list is the path
							intStart = 1;
							strSrcDir = (strTempARY[0]);
							if (Strings.Right(strSrcDir, 1) != "\\")
								strSrcDir += "\\";
						}
						else
						{
							// only one. The first is the path and file
							intStart = 0;
							strSrcDir = "";
						}
						// this next bit makes strary the revers of strtempary since
						// windows hands you the list backwards.
						strAry = new string[Information.UBound(strTempARY, 1) - intStart + 1];
						y = 0;
						for (x = Information.UBound(strTempARY, 1); x >= intStart; x--)
						{
							strAry[y] = strTempARY[x];
							y += 1;
						}
						// x
						intStart = 0;
						y = 1;
						for (x = intStart; x <= Information.UBound(strAry, 1); x++)
						{
							strFileName = strSrcDir + strAry[x];
							if (File.Exists(strFileName))
							{
								// now add the file
								modGNBas.Statics.DocumentLocationName = strFileName;
								frmPictureChange.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								if (FCConvert.ToString(modGNBas.Statics.response) == "YES")
								{
									modGlobalRoutines.NewRecordPicture_2(modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.DocumentLocationName, y);
									frmNewREProperty.InstancePtr.imgPicture.Image = FCUtils.LoadPicture(modGNBas.Statics.DocumentLocationName);
									frmNewREProperty.InstancePtr.cmdNextPicture.Enabled = false;
									if ((y) > 1)
									{
										frmNewREProperty.InstancePtr.cmdPrevPicture.Enabled = true;
									}
									frmNewREProperty.InstancePtr.Auto_Size_Photo();
									frmNewREProperty.InstancePtr.imgPicture.Refresh();
									frmNewREProperty.InstancePtr.imgPicture.Tag = y;
									// 43                            frmnewreproperty.cmdPrint.Enabled = True
									frmNewREProperty.InstancePtr.lblPicName.Text = strFileName;
									// DoEvents
									y += 1;
								}
								else
								{
								}
							}
						}
						// x
					}
				}
				// 46      frmnewreproperty.SetFocus
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in cmdYes_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmNoPictures_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNoPictures properties;
			//frmNoPictures.ScaleWidth	= 3885;
			//frmNoPictures.ScaleHeight	= 2235;
			//frmNoPictures.LinkTopic	= "Form1";
			//frmNoPictures.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
	}
}
