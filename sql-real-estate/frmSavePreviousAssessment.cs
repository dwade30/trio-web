﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmSavePreviousAssessment.
	/// </summary>
	public partial class frmSavePreviousAssessment : BaseForm
	{
		public frmSavePreviousAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSavePreviousAssessment InstancePtr
		{
			get
			{
				return (frmSavePreviousAssessment)Sys.GetInstance(typeof(frmSavePreviousAssessment));
			}
		}

		protected frmSavePreviousAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void frmSavePreviousAssessment_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSavePreviousAssessment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSavePreviousAssessment properties;
			//frmSavePreviousAssessment.ScaleWidth	= 3885;
			//frmSavePreviousAssessment.ScaleHeight	= 2175;
			//frmSavePreviousAssessment.LinkTopic	= "Form1";
			//End Unmaped Properties
			int lngYear = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			clsLoad.OpenRecordset("select BILLYEAR from customize", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				lngYear = DateTime.Today.Year;
				if (lngYear == 0)
				{
					txtTaxYear.Text = FCConvert.ToString(DateTime.Today.Year);
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [billyear] and replace with corresponding Get_Field method
					if (lngYear - Conversion.Val(clsLoad.Get_Fields("billyear")) > 1)
					{
						// doubtful the database is current
						txtTaxYear.Text = FCConvert.ToString(DateTime.Today.Year);
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [billyear] and replace with corresponding Get_Field method
						txtTaxYear.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("billyear")));
					}
				}
			}
			else
			{
				txtTaxYear.Text = FCConvert.ToString(DateTime.Today.Year);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("This will not save current billing values in the previous assessment table." + "\r\n" + "Do you want to quit?", "Quit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			try
			{
				int lngYear;
				int intTemp;
				string strCols;
				string strSQL;
				bool boolLastYear;
				bool boolYearBeforeLast;
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTaxYear.Text)));
				if (lngYear < DateTime.Today.Year - 1 || lngYear > (DateTime.Today.Year + 1))
				{
					MessageBox.Show("Please enter a valid year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				intTemp = lngYear;
				clsLoad.OpenRecordset("select top 1 * from previousassessment where taxyear > " + FCConvert.ToString(lngYear), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (MessageBox.Show("There are previous assessment records for years after " + FCConvert.ToString(lngYear) + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
					{
						return;
					}
				}
				else
				{
					clsLoad.OpenRecordset("select top 1 * from previousassessment where taxyear = " + FCConvert.ToString(lngYear), modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						if (MessageBox.Show("There are already previous assessment records for " + FCConvert.ToString(lngYear) + "\r\n" + "Do you want to overwrite these values?", "Overwrite?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
						{
							return;
						}
					}
					else
					{
						clsLoad.OpenRecordset("select top 1 * from previousassessment where taxyear = " + FCConvert.ToString(lngYear - 1), modGlobalVariables.strREDatabase);
						if (clsLoad.EndOfFile())
						{
							clsLoad.OpenRecordset("select top 1 * from previousassessment where taxyear = " + FCConvert.ToString(lngYear - 2), modGlobalVariables.strREDatabase);
							if (!clsLoad.EndOfFile())
							{
								if (MessageBox.Show("There are previous assessment records for " + FCConvert.ToString(lngYear - 2) + " but none for " + FCConvert.ToString(lngYear - 1) + "\r\n" + "Are you sure " + FCConvert.ToString(lngYear) + " is the year you want to save?", "Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
								{
									return;
								}
							}
						}
					}
				}
				// only want up to 14 years data. That's how much goes on a card
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				clsLoad.Execute("delete from previousassessment where taxyear <= " + FCConvert.ToString(lngYear - 14), modGlobalVariables.strREDatabase);
				// Call clsLoad.Execute("update customize set billyear = " & lngYear, strREDatabase)
				modGlobalVariables.Statics.CustomizedInfo.BillYear = lngYear;
				modGlobalVariables.Statics.CustomizedInfo.SaveCustomInfo();
				clsLoad.Execute("delete from previousassessment where taxyear = " + FCConvert.ToString(lngYear), modGlobalVariables.strREDatabase);
				strCols = "(Account,Card,Land,Building,Exempt,Assessment,ExemptCode1,ExemptCode2,ExemptCode3,taxyear)";
				strSQL = "Select rsaccount,rscard,cast(lastlandval as int),cast(lastbldgval as int),cast(rlexemption as int),cast(lastlandval as int) + cast(lastbldgval as int) - cast(rlexemption as int),riexemptcd1,riexemptcd2,riexemptcd3, " + FCConvert.ToString(lngYear) + " as taxyear from master where not rsdeleted = 1";
				// Call clsLoad.Execute("insert into PreviousAssessment " & strCols & " values " & strSQL,strredatabase)
				clsLoad.Execute("insert into PreviousAssessment " + strCols + " " + strSQL, modGlobalVariables.strREDatabase);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Billing values saved in Previous Assessment table", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuSaveContinue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
