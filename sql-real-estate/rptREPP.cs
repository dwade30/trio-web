﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptREPPAssociation.
	/// </summary>
	public partial class rptREPPAssociation : BaseSectionReport
	{
		public rptREPPAssociation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate and Personal Property Association List";
		}

		public static rptREPPAssociation InstancePtr
		{
			get
			{
				return (rptREPPAssociation)Sys.GetInstance(typeof(rptREPPAssociation));
			}
		}

		protected rptREPPAssociation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre?.Dispose();
				clsPP?.Dispose();
                clsre = null;
                clsPP = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptREPPAssociation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsPP = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL = "";
			string strOrder = "";
			string strMasterJoin;
			string strMasterJoinJoin = "";
			// strREFullDBName = rsTemp.GetFullDBName("RealEstate")
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			if (boolNameOrder)
			{
				strOrder = " order by mparty.rsname ";
			}
			else
			{
				strOrder = " order by mparty.rsaccount ";
			}
			string strfulltable;
			strfulltable = clsre.Get_GetFullDBName("CentralData");
			// Call clsre.OpenRecordset("select * from master inner join moduleassociation on (moduleassociation.remasteracct = master.rsaccount)  where moduleassociation.module = 'PP' and not master.rsdeleted = 1 and master.rscard = 1" & strOrder, "twre0000.vb1")
			clsre.OpenRecordset(strMasterJoin + " inner join " + strfulltable + ".dbo.moduleassociation on (" + strfulltable + ".dbo.moduleassociation.remasteracct = mparty.rsaccount)  where " + strfulltable + ".dbo.moduleassociation.module = 'PP' and not mparty.rsdeleted = 1 and mparty.rscard = 1" + strOrder, "twre0000.vb1");
			clsPP.OpenRecordset("select * from ppmaster order by account", "twpp0000.vb1");
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "REPPAssociation");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = DateTime.Today.ToString();
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsre.EndOfFile())
			{
				txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
				txtName.Text = FCConvert.ToString(clsre.Get_Fields_String("rsname"));
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtPPAccount.Text = FCConvert.ToString(clsre.Get_Fields("account"));
				// Call clsPP.FindFirstRecord("account", clsre.Fields("account"))
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				clsPP.FindFirst("account = " + clsre.Get_Fields("account"));
				if (clsPP.NoMatch)
				{
					txtPPName.Text = "Error PP record not found.";
				}
				else
				{
					txtPPName.Text = FCConvert.ToString(clsPP.Get_Fields_String("name"));
				}
				clsre.MoveNext();
			}
		}

		
	}
}
