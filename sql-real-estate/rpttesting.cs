﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rpttesting.
	/// </summary>
	public partial class rpttesting : BaseSectionReport
	{
		public static rpttesting InstancePtr
		{
			get
			{
				return (rpttesting)Sys.GetInstance(typeof(rpttesting));
			}
		}

		protected rpttesting _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rpttesting()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rpttesting	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			RichEdit1.LoadFile("TestTax.txt");
			RichEdit1.SelectionStart = 0;
			RichEdit1.SelectionLength = RichEdit1.Text.Length;
			RichEdit1.SelectionFont = new Font("Tahoma", 10);
			RichEdit1.Font = new Font("Tahoma", RichEdit1.Font.Size);
			RichEdit1.Font = new Font(RichEdit1.Font.Name, 10);
		}

		
	}
}
