﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class clsLandScheduleRecord
	{
		//=========================================================
		// MATTHEW 10/7/05 CALL ID 78691
		// Private lsrate(38) As String
		// BEN AND I HAVE ADDED A FEW MORE LAND TYPES FOR WHITEFIELD AND THIS ARRAY
		// DIDN'T SEEM TO CHECK FOR THE MAX AMOUNT OF LAND TYPES. THIS MAY NEED TO
		// BE DYNAMIC SO WHEN MORE TYPES ARE ADDED THEN WE WILL NOT NEED TO REMEMBER
		// TO CHANGE THIS. COMMENTS?
		private string[] lsrate = null;
		private int lngLandTypes;
		private double dblStandardDepth;
		private double dblStandardLot;
		private double dblStandardWidth;

		public int MaxTypes
		{
			get
			{
				int MaxTypes = 0;
				MaxTypes = Information.UBound(lsrate, 1);
				return MaxTypes;
			}
		}

		public double StandardWidth
		{
			set
			{
				dblStandardWidth = value;
			}
			get
			{
				double StandardWidth = 0;
				StandardWidth = dblStandardWidth;
				return StandardWidth;
			}
		}
		// vbPorter upgrade warning: recnum As short	OnWrite(int, double)
		// vbPorter upgrade warning: 'Return' As Variant --> As double	OnWrite(double, short)
		public double GetRate(int recnum)
		{
			double GetRate = 0;
			if (recnum <= Information.UBound(lsrate, 1))
			{
				GetRate = Conversion.Val(lsrate[recnum]);
			}
			else
			{
				GetRate = 0;
			}
			return GetRate;
		}
		// vbPorter upgrade warning: recnum As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: str As string	OnWriteFCConvert.ToDouble(
		public void putrate(int recnum, string str)
		{
			if (recnum <= Information.UBound(lsrate, 1))
			{
				lsrate[recnum] = str + "";
			}
		}

		public double StandardDepth
		{
			set
			{
				dblStandardDepth = value;
			}
			get
			{
				double StandardDepth = 0;
				StandardDepth = dblStandardDepth;
				return StandardDepth;
			}
		}

		public double StandardLot
		{
			set
			{
				dblStandardLot = value;
			}
			get
			{
				double StandardLot = 0;
				StandardLot = dblStandardLot;
				return StandardLot;
			}
		}

		public clsLandScheduleRecord() : base()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int intTypes;
			// Call rsLoad.OpenRecordset("select count(code) as numtypes from landtype", strREDatabase)
			rsLoad.OpenRecordset("select max(code) as numtypes from landtype", modGlobalVariables.strREDatabase);
			if (!rsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [numtypes] not found!! (maybe it is an alias?)
				intTypes = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("numtypes"))));
			}
			else
			{
				intTypes = 1;
			}
			lsrate = new string[intTypes + 1];
		}
	}
}
