﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class cRECommandHandler
	{
		//=========================================================
		public delegate void MenuChangedEventHandler(string strMenuName);

		public event MenuChangedEventHandler MenuChanged;

		private bool boolDontShellToGE;

		public void ExecuteCommand(string strCommand)
		{
			if (Strings.LCase(strCommand) == "re_exit")
			{
				ExitApplication();
			}
			else if (Strings.LCase(strCommand) == "re_menu_costfileupdate")
			{
				if (this.MenuChanged != null)
					this.MenuChanged("CostFileMain");
			}
			else if (Strings.LCase(strCommand) == "re_menu_printing")
			{
				if (this.MenuChanged != null)
					this.MenuChanged("PrintMenu1");
			}
			else if (Strings.LCase(strCommand) == "re_menu_salerecords")
			{
				if (this.MenuChanged != null)
					this.MenuChanged("SalesMenu");
			}
			else if (Strings.LCase(strCommand) == "re_menu_compute")
			{
				if (this.MenuChanged != null)
					this.MenuChanged("System");
			}
			else if (Strings.LCase(strCommand) == "re_menu_importexport")
			{
				if (this.MenuChanged != null)
					this.MenuChanged("ImportExport");
			}
			else if (Strings.LCase(strCommand) == "re_menu_file")
			{
				if (this.MenuChanged != null)
					this.MenuChanged("File");
			}
			else if (Strings.LCase(strCommand) == "re_form_getaccountfull")
			{
				modGlobalVariables.Statics.wmainresponse = 1;
				frmREProperty.InstancePtr.Unload();
				frmNewREProperty.InstancePtr.Unload();
				frmSHREBLUpdate.InstancePtr.Unload();
				frmGetAccount.InstancePtr.Show(App.MainForm);
			}
			else if (Strings.LCase(strCommand) == "re_form_getaccountshort")
			{
				modGlobalVariables.Statics.wmainresponse = 1;
				frmREProperty.InstancePtr.Unload();
				frmNewREProperty.InstancePtr.Unload();
				frmSHREBLUpdate.InstancePtr.Unload();
				frmGetAccount.InstancePtr.Show(App.MainForm);
			}
			else if (Strings.LCase(strCommand) == "re_calculateexemptions")
			{
				//FC:FINAL:MSH - i.issue #1070: call form as tab
				//modcalcexemptions.NewCalcExemptions();
				frmExemptList.InstancePtr.Init();
			}
		}

		private void ExitApplication()
		{
			modGNBas.Statics.gboolExiting = true;
			modReplaceWorkFiles.EntryFlagFile(true, "RE", true);
			if (!(modGlobalVariables.Statics.boolFromBilling || boolDontShellToGE))
			{
				if (modGlobalVariables.Statics.boolInArchiveMode && modGlobalVariables.Statics.gstrArchiveParentDir != string.Empty)
				{
					//Environment.CurrentDirectory = modGlobalVariables.Statics.gstrArchiveParentDir;
				}
                //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                //Interaction.Shell(FCFileSystem.Statics.UserDataFolder + "\\Twgnenty.exe", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
                App.MainForm.OpenModule("TWGNENTY");
			}
			Application.Exit();
		}
	}
}
