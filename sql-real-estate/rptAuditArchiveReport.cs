﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAuditArchiveReport.
	/// </summary>
	public partial class rptAuditArchiveReport : BaseSectionReport
	{
		public static rptAuditArchiveReport InstancePtr
		{
			get
			{
				return (rptAuditArchiveReport)Sys.GetInstance(typeof(rptAuditArchiveReport));
			}
		}

		protected rptAuditArchiveReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}

		public rptAuditArchiveReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Archive Report";
		}
		// nObj = 1
		//   0	rptAuditArchiveReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intPage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool boolMoveToArchive;

		public void Init(string strSQL, bool modalDialog, bool boolMoveEntries = false, string strTitle = "Changes Audit")
		{
			boolMoveToArchive = boolMoveEntries;
			Label1.Text = strTitle;
			this.Name = strTitle;
			rsData.OpenRecordset(strSQL);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Changes Audit", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
            using (clsDRWrapper rsSave = new clsDRWrapper())
            {
                if (boolMoveToArchive)
                {
                    rsSave.Execute(
                        "insert into auditchangesarchive (location,changedescription,userfield1,userfield2,userfield3,userfield4,userid,dateupdated,timeupdated) select location,changedescription,userfield1,userfield2,userfield3,userfield4,userid,dateupdated,timeupdated from AUDITchanges",
                        modGlobalVariables.strREDatabase);
                    rsSave.Execute("delete  from auditchanges", modGlobalVariables.strREDatabase);
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtDate As Variant --> As string
			// vbPorter upgrade warning: txtTime As Variant --> As string
			// vbPorter upgrade warning: blnFirstRecord As Variant --> As bool
			//string txtMuniname; string txtDate, txtTime;
			bool blnFirstRecord;
			// - "AutoDim"
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			//txtMuniname = gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtAccount.Text = FCConvert.ToString(rsData.Get_Fields_String("userfield1"));
			fldCard.Text = FCConvert.ToString(rsData.Get_Fields_String("userfield2"));
			// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
			txtUser.Text = FCConvert.ToString(rsData.Get_Fields("UserID"));
			DateTime temp;
			if (Information.IsDate(rsData.Get_Fields("DateUpdated")))
			{
				temp = rsData.Get_Fields_DateTime("DateUpdated");
				txtDateTimeField.Text = temp.ToShortDateString();
			}
			if (Information.IsDate(rsData.Get_Fields("TimeUpdated")))
			{
				temp = rsData.Get_Fields_DateTime("TimeUpdated");
				fldTime.Text = temp.ToString("hh:mm:ss tt");
			}
			fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
			fldDescription.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
			rsData.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As Variant --> As string
			//string txtPage; // - "AutoDim"
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
