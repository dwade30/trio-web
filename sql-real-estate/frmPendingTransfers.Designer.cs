﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPendingTransfers.
	/// </summary>
	partial class frmPendingTransfers : BaseForm
	{
		public FCGrid GridChanges;
		public FCGrid GridReport;
		public FCGrid GridXfer;
		public fecherFoundation.FCLabel lblGridTitle;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuToggleView;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuEditCurrentPending;
		public fecherFoundation.FCToolStripMenuItem mnuDel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPendingChanges;
		public fecherFoundation.FCToolStripMenuItem mnuPendingTransfersReport;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuSaveChanges;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.GridChanges = new fecherFoundation.FCGrid();
            this.GridReport = new fecherFoundation.FCGrid();
            this.GridXfer = new fecherFoundation.FCGrid();
            this.lblGridTitle = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuToggleView = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditCurrentPending = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPendingChanges = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPendingTransfersReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveChanges = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridXfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 609);
            this.BottomPanel.Size = new System.Drawing.Size(631, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridReport);
            this.ClientArea.Controls.Add(this.lblGridTitle);
            this.ClientArea.Controls.Add(this.GridChanges);
            this.ClientArea.Controls.Add(this.GridXfer);
            this.ClientArea.Size = new System.Drawing.Size(651, 612);
            this.ClientArea.Controls.SetChildIndex(this.GridXfer, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridChanges, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblGridTitle, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridReport, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(651, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(350, 28);
            this.HeaderText.Text = "Pending Changes and Transfers";
            // 
            // GridChanges
            // 
            this.GridChanges.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridChanges.Cols = 5;
            this.GridChanges.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridChanges.ExtendLastCol = true;
            this.GridChanges.FixedCols = 0;
            this.GridChanges.Location = new System.Drawing.Point(30, 60);
            this.GridChanges.Name = "GridChanges";
            this.GridChanges.RowHeadersVisible = false;
            this.GridChanges.Rows = 1;
            this.GridChanges.Size = new System.Drawing.Size(591, 549);
            this.GridChanges.TabIndex = 2;
            this.GridChanges.CurrentCellChanged += new System.EventHandler(this.GridChanges_RowColChange);
            this.GridChanges.DoubleClick += new System.EventHandler(this.GridChanges_DblClick);
            // 
            // GridReport
            // 
            this.GridReport.Cols = 7;
            this.GridReport.ColumnHeadersVisible = false;
            this.GridReport.FixedCols = 0;
            this.GridReport.FixedRows = 0;
            this.GridReport.Location = new System.Drawing.Point(10, 5);
            this.GridReport.Name = "GridReport";
            this.GridReport.RowHeadersVisible = false;
            this.GridReport.Rows = 0;
            this.GridReport.Size = new System.Drawing.Size(517, 21);
            this.GridReport.TabIndex = 3;
            this.GridReport.Visible = false;
            // 
            // GridXfer
            // 
            this.GridXfer.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridXfer.Cols = 6;
            this.GridXfer.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridXfer.ExtendLastCol = true;
            this.GridXfer.FixedCols = 0;
            this.GridXfer.Location = new System.Drawing.Point(30, 60);
            this.GridXfer.Name = "GridXfer";
            this.GridXfer.RowHeadersVisible = false;
            this.GridXfer.Rows = 1;
            this.GridXfer.Size = new System.Drawing.Size(591, 549);
            this.GridXfer.TabIndex = 4;
            this.GridXfer.Visible = false;
            this.GridXfer.CurrentCellChanged += new System.EventHandler(this.GridXfer_RowColChange);
            this.GridXfer.DoubleClick += new System.EventHandler(this.GridXfer_DblClick);
            // 
            // lblGridTitle
            // 
            this.lblGridTitle.Location = new System.Drawing.Point(30, 30);
            this.lblGridTitle.Name = "lblGridTitle";
            this.lblGridTitle.Size = new System.Drawing.Size(312, 16);
            this.lblGridTitle.TabIndex = 1;
            this.lblGridTitle.Text = "PENDING CHANGES";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuOptions,
            this.mnuSepar3,
            this.mnuSaveChanges});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuOptions
            // 
            this.mnuOptions.Index = 0;
            this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuToggleView,
            this.mnuSepar1,
            this.mnuEditCurrentPending,
            this.mnuDel,
            this.mnuSepar4,
            this.mnuPrintPendingChanges,
            this.mnuPendingTransfersReport});
            this.mnuOptions.Name = "mnuOptions";
            this.mnuOptions.Text = "Options";
            // 
            // mnuToggleView
            // 
            this.mnuToggleView.Index = 0;
            this.mnuToggleView.Name = "mnuToggleView";
            this.mnuToggleView.Text = "View pending transfers";
            this.mnuToggleView.Click += new System.EventHandler(this.mnuToggleView_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuEditCurrentPending
            // 
            this.mnuEditCurrentPending.Index = 2;
            this.mnuEditCurrentPending.Name = "mnuEditCurrentPending";
            this.mnuEditCurrentPending.Text = "Edit current pending entry";
            this.mnuEditCurrentPending.Click += new System.EventHandler(this.mnuEditCurrentPending_Click);
            // 
            // mnuDel
            // 
            this.mnuDel.Index = 3;
            this.mnuDel.Name = "mnuDel";
            this.mnuDel.Text = "Delete current pending entry";
            this.mnuDel.Click += new System.EventHandler(this.mnuDel_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = 4;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuPrintPendingChanges
            // 
            this.mnuPrintPendingChanges.Index = 5;
            this.mnuPrintPendingChanges.Name = "mnuPrintPendingChanges";
            this.mnuPrintPendingChanges.Text = "Print Pending Changes Report";
            this.mnuPrintPendingChanges.Click += new System.EventHandler(this.mnuPrintPendingChanges_Click);
            // 
            // mnuPendingTransfersReport
            // 
            this.mnuPendingTransfersReport.Index = 6;
            this.mnuPendingTransfersReport.Name = "mnuPendingTransfersReport";
            this.mnuPendingTransfersReport.Text = "Print Pending Transfers Report";
            this.mnuPendingTransfersReport.Click += new System.EventHandler(this.mnuPendingTransfersReport_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSaveChanges
            // 
            this.mnuSaveChanges.Index = 2;
            this.mnuSaveChanges.Name = "mnuSaveChanges";
            this.mnuSaveChanges.Text = "Save Changes";
            this.mnuSaveChanges.Click += new System.EventHandler(this.mnuSaveChanges_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Cursor = Wisej.Web.Cursors.Default;
            this.cmdProcess.Location = new System.Drawing.Point(237, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(229, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process Pending Activity";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frmPendingTransfers
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(651, 672);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.Menu = this.MainMenu1;
            this.Name = "frmPendingTransfers";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Pending Changes and Transfers";
            this.Load += new System.EventHandler(this.frmPendingTransfers_Load);
            this.Resize += new System.EventHandler(this.frmPendingTransfers_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridXfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
	}
}
