﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using System;
using System.IO;
using Wisej.Web;

namespace TWRE0000
{
    public class modGlobalRoutines
    {
        const int CNSTSALECOLPRICE = 0;
        const int CNSTSALECOLDATE = 1;
        const int CNSTSALECOLSALE = 2;
        const int CNSTSALECOLFINANCING = 3;
        const int CNSTSALECOLVERIFIED = 4;
        const int CNSTSALECOLVALIDITY = 5;
        const int CNSTNOEXEMPT = -1;
        const int CNSTEXEMPTCOLCODE = 0;
        const int CNSTEXEMPTCOLPERCENT = 1;

        private struct BookPageRec
        {
            public string strBookNum;
            public string strPageNum;
            public DateTime dtBPDate;
            //FC:FINAL:RPU - Use custom constructor to initialize fields
            public BookPageRec(int unusedParam)
            {
                strBookNum = string.Empty;
                strPageNum = string.Empty;
                dtBPDate = default(DateTime);
            }
        };
        // Public Declare Function GetComputerName& Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long)
        //[DllImport("winspool.drv")]
        //public static extern object WritePrinter(int hPrinter, ref string pbuf, int cdbuf, ref int pcwritten);
        //public static int WritePrinter(int hPrinter, string pBuf, int cdBuf, ref int pcWritten)
        //{
        //    ParameterObjects p = new ParameterObjects();
        //    p.FunctionToExecute = FunctionToExecute.WritePrinter;
        //    p.Parameter[0] = new ParameterObject(hPrinter, false);
        //    p.Parameter[1] = new ParameterObject(pBuf, false);
        //    p.Parameter[2] = new ParameterObject(cdBuf, false);
        //    p.Parameter[3] = new ParameterObject(pcWritten, true);
        //    PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
        //    pcWritten = FCConvert.ToInt32(p.Parameter[3].Value);
        //    return p.ReturnValue;
        //}

        public static string GetCurrentBookString(int lngAccount, short intEntriesLimit = 0)
        {
            string GetCurrentBookString = "";
            clsDRWrapper clsBookPage = new clsDRWrapper();
            string strBookPage;
            string strBookPageDate = "";
            int x;
            bool boolContinue;
            string strEOL;
            try
            {
                // On Error GoTo ErrorHandler
                GetCurrentBookString = "";
                clsBookPage.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAccount) + " and [current] = 1 order by line desc", "twre0000.vb1");
                strBookPage = "";
                strEOL = "";
                boolContinue = true;
                x = 0;
                while (!clsBookPage.EndOfFile() && boolContinue)
                {
                    x += 1;
                    if (x >= intEntriesLimit && intEntriesLimit > 0)
                    {
                        boolContinue = false;
                    }
                    // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                    strBookPage += strEOL + clsBookPage.Get_Fields("Book");
                    strEOL = "\r\n";
                    clsBookPage.MoveNext();
                }
                GetCurrentBookString = strBookPage;
                return GetCurrentBookString;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCurrentBookString", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetCurrentBookString;
        }

        public static string GetCurrentPageString(int lngAccount, short intEntriesLimit = 0)
        {
            string GetCurrentPageString = "";
            clsDRWrapper clsBookPage = new clsDRWrapper();
            string strBookPage;
            string strBookPageDate = "";
            int x;
            bool boolContinue;
            string strEOL;
            try
            {
                // On Error GoTo ErrorHandler
                GetCurrentPageString = "";
                clsBookPage.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAccount) + " and [current] = 1 order by line desc", "twre0000.vb1");
                strBookPage = "";
                strEOL = "";
                boolContinue = true;
                x = 0;
                while (!clsBookPage.EndOfFile() && boolContinue)
                {
                    x += 1;
                    if (x >= intEntriesLimit && intEntriesLimit > 0)
                    {
                        boolContinue = false;
                    }
                    // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                    strBookPage += strEOL + clsBookPage.Get_Fields("Page");
                    strEOL = "\r\n";
                    clsBookPage.MoveNext();
                }
                GetCurrentPageString = strBookPage;
                return GetCurrentPageString;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCurrentPageString", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetCurrentPageString;
        }

        public static string GetCurrentMapLotsString(ref int lngAccount, short intEntriesLimit = 0)
        {
            string GetCurrentMapLotsString = "";
            string strmaplot;
            clsDRWrapper rsMapLot = new clsDRWrapper();
            bool boolContinue;
            int x;
            string strEOL;
            try
            {
                // On Error GoTo ErrorHandler
                GetCurrentMapLotsString = "";
                rsMapLot.OpenRecordset("Select * from maplot where account = " + FCConvert.ToString(lngAccount) + " order by line", modGlobalVariables.strREDatabase);
                strmaplot = "";
                strEOL = "";
                boolContinue = true;
                x = 0;
                while (!rsMapLot.EndOfFile() && boolContinue)
                {
                    x += 1;
                    if (x >= intEntriesLimit && intEntriesLimit > 0)
                    {
                        boolContinue = false;
                    }
                    strmaplot += strEOL + rsMapLot.Get_Fields_String("maplot");
                    strEOL = "\r\n";
                    rsMapLot.MoveNext();
                }
                GetCurrentMapLotsString = strmaplot;
                return GetCurrentMapLotsString;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCurrentMapLotsString", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetCurrentMapLotsString;
        }
        //FC:FINAL:AM: not needed
        // vbPorter upgrade warning: frmName As Form	OnWrite(frmREOptions)
        // vbPorter upgrade warning: lngBackColor As int	OnWrite(Color)
        //public static void SetTRIOColorsSpecific(ref Form frmName, ref int lngBackColor, bool boolUseFontResizer = true)
        //{
        //    // this sub will set all of the control colors on the form passed in to TRIO colors
        //    int lngCT;
        //    int lngCount = 0;
        //    Control ctrl = new Control();
        //    if (modGlobalConstants.Statics.gboolUse256Colors)
        //    {
        //        // do nothing because the user is set at 256 colors
        //    }
        //    else
        //    {
        //        // if the variables are not set, then use the background of the form that is passed in until they get set
        //        // this will prevent the first forms that are loaded before the LoadTRIOColors routine is used from being black
        //        if (modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND == 0)
        //        {
        //            modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND = ColorTranslator.ToOle(frmName.BackColor);
        //        }
        //        // set the background of the form
        //        frmName.BackColor = ColorTranslator.FromOle(lngBackColor);
        //        lngCount = frmName.Controls.Count;
        //        for (lngCT = 0; lngCT <= lngCount - 1; lngCT++)
        //        {
        //            ctrl = frmName.Controls[lngCT];
        //            if (ctrl is FCLabel)
        //            {
        //                ctrl.BackColor = ColorTranslator.FromOle(lngBackColor);
        //                ctrl.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORFORECOLOR);
        //                if (boolUseFontResizer) ctrl.Font = Support.FontChangeSize(ctrl.Font, 9);
        //            }
        //            else if (ctrl is FCButton)
        //            {
        //                ctrl.BackColor = ColorTranslator.FromOle(lngBackColor);
        //                if (boolUseFontResizer) ctrl.Font = Support.FontChangeSize(ctrl.Font, 9);
        //            }
        //            else if (ctrl is FCFrame)
        //            {
        //                ctrl.BackColor = ColorTranslator.FromOle(lngBackColor);
        //                ctrl.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
        //                if (boolUseFontResizer) ctrl.Font = Support.FontChangeSize(ctrl.Font, 9);
        //            }
        //            else if (ctrl is FCCheckBox)
        //            {
        //                ctrl.BackColor = ColorTranslator.FromOle(lngBackColor);
        //                ctrl.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORFORECOLOR);
        //                if (boolUseFontResizer) ctrl.Font = Support.FontChangeSize(ctrl.Font, 9);
        //            }
        //            else if (ctrl is FCRadioButton)
        //            {
        //                ctrl.BackColor = ColorTranslator.FromOle(lngBackColor);
        //                ctrl.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORFORECOLOR);
        //                if (boolUseFontResizer) ctrl.Font = Support.FontChangeSize(ctrl.Font, 9);
        //            }
        //            else if (ctrl is FCPictureBox)
        //            {
        //                ctrl.BackColor = ColorTranslator.FromOle(lngBackColor);
        //            }
        //            else if (ctrl is VScrollBar || ctrl is HScrollBar)
        //            {
        //                // ElseIf TypeOf ctrl Is SSTab Then
        //                // ctrl.BackColor = TRIOCOLORGRAYBACKGROUND
        //            }
        //            else if (ctrl is FCComboBox || ctrl is FCTextBox)
        //            {
        //                ctrl.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORFORECOLOR);
        //                if (boolUseFontResizer) ctrl.Font = Support.FontChangeSize(ctrl.Font, 9);
        //            }
        //            else if (ctrl.GetName() == "GRID" || Strings.UCase(Strings.Left(ctrl.GetName(), 2)) == "VS" && !(Strings.UCase(ctrl.GetName()) == Strings.UCase("vsElasticLight1")) && !(Strings.UCase(ctrl.GetName()) == Strings.UCase("vsElasticLight2")))
        //            {
        //                ctrl.BackColorFixed = lngBackColor;
        //                ctrl.BackColorBkg = lngBackColor;
        //            }
        //        }
        //    }
        //}
        public static void SetupSigInformation()
        {
            // dummy sub so modCLCalculations will compile
        }
        // vbPorter upgrade warning: lngNumToRound As int	OnWrite(string, int, double)
        public static int RERound_2(int lngNumToRound)
        {
            return RERound(ref lngNumToRound);
        }

        public static int RERound(ref int lngNumToRound)
        {
            int RERound = 0;
            // rounds based on rounding option
            int lngRoundVar;
            int lngReturn;
            lngReturn = 0;
            lngRoundVar = modGlobalVariables.Statics.CustomizedInfo.Round;
            // Dave 8/13/07 Call# 116963 Added because it was rounding 0 up to half of what they wanted to round to insead of leaving it as 0
            if (lngNumToRound == 0)
            {
                RERound = 0;
                return RERound;
            }
            switch (lngRoundVar)
            {
                case 4:
                    {
                        // 1
                        lngRoundVar = 1;
                        break;
                    }
                case 3:
                    {
                        // 10
                        lngRoundVar = 10;
                        if (lngNumToRound < 10 && modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDNEXTOPTION)
                        {
                            lngRoundVar = 1;
                        }
                        else if (lngNumToRound < 5 && modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDHALFOPTION)
                        {
                            RERound = 5;
                            return RERound;
                        }
                        break;
                    }
                case 2:
                    {
                        // 100
                        lngRoundVar = 100;
                        if (lngNumToRound < lngRoundVar && modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDNEXTOPTION)
                        {
                            lngRoundVar = 10;
                        }
                        else if (lngNumToRound < 50 && modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDHALFOPTION)
                        {
                            RERound = 50;
                            return RERound;
                        }
                        break;
                    }
                case 1:
                    {
                        // 1000
                        lngRoundVar = 1000;
                        if (lngNumToRound < lngRoundVar && modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDNEXTOPTION)
                        {
                            lngRoundVar = 100;
                        }
                        else if (lngNumToRound < 500 && modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDHALFOPTION)
                        {
                            RERound = 500;
                            return RERound;
                        }
                        break;
                    }
                default:
                    {
                        lngRoundVar = 1;
                        break;
                    }
            }
            //end switch
            // If lngNumToRound < 1000 And lngRoundVar = 1000 And CustomizedInfo.RoundContingency = CNSTROUNDNEXTOPTION Then
            // round to nearest 100
            // lngRoundVar = 100
            // End If
            lngReturn = (fecherFoundation.FCUtils.iDiv(((lngNumToRound + 0.01) / lngRoundVar), 1)) * lngRoundVar;
            // lngReturn = lngReturn \ 1
            // lngReturn = lngReturn * lngRoundVar
            RERound = lngReturn;
            return RERound;
        }

        private static void CheckOutbuildingRecords()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            clsDRWrapper rsSave = new clsDRWrapper();
            string strRpt;
            //StreamWriter ts;
            StreamWriter ts;
            ////FileSystemObject fso = new FileSystemObject();
            try
            {
                // On Error GoTo ErrorHandler
                strRpt = "";
                rsLoad.OpenRecordset("select rsaccount,rscard from outbuilding group by rsaccount,rscard having count(rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    strRpt += "Account " + Strings.Left(rsLoad.Get_Fields_Int32("rsaccount") + Strings.StrDup(7, " "), 7) + " Card " + rsLoad.Get_Fields_Int32("rscard") + "\r\n";
                    modGlobalFunctions.AddCYAEntry_242("RE", "Duplicate Record(s) Corrected", "Outbuilding", FCConvert.ToString(rsLoad.Get_Fields_Int32("rsaccount")), FCConvert.ToString(rsLoad.Get_Fields_Int32("rscard")));
                    rsSave.OpenRecordset("select * from outbuilding where rsaccount = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard") + " order by id", modGlobalVariables.strREDatabase);
                    rsSave.MoveNext();
                    while (!rsSave.EndOfFile())
                    {
                        rsSave.Delete();
                        rsSave.MoveNext();
                        rsSave.MoveNext();
                    }
                    rsLoad.MoveNext();
                }
                if (strRpt != string.Empty)
                {
                    strRpt = "Duplicate Outbuilding records were found and deleted for the following. You must verify that the current data for these accounts is correct." + "\r\n" + "\r\n" + strRpt;
                    //if (Directory.Exists("rpt"))
                    if (Directory.Exists("rpt"))
                    {
                        //ts = fso.CreateTextFile("rpt\\REOFix" + Strings.Format(DateTime.Today, "m-d-yyyy") + ".txt", true, false);
                        ts = File.CreateText("rpt\\REOFix" + Strings.Format(DateTime.Today, "m-d-yyyy") + ".txt");
                        ts.WriteLine(strRpt);
                        ts.Close();
                    }
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    frmShowResults.InstancePtr.Init(ref strRpt, true);
                    frmShowResults.InstancePtr.Text = "Verify Records";
                    frmShowResults.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmStartupWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CheckOutbuildingRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private static void CheckDwellingRecords()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            clsDRWrapper rsSave = new clsDRWrapper();
            string strRpt;
            ////FileSystemObject fso = new FileSystemObject();
            //StreamWriter ts;
            StreamWriter ts;
            try
            {
                // On Error GoTo ErrorHandler
                strRpt = "";
                rsLoad.OpenRecordset("select rsaccount,rscard from dwelling group by rsaccount,rscard having count(rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    strRpt += "Account " + Strings.Left(rsLoad.Get_Fields_Int32("rsaccount") + Strings.StrDup(7, " "), 7) + " Card " + rsLoad.Get_Fields_Int32("rscard") + "\r\n";
                    modGlobalFunctions.AddCYAEntry_242("RE", "Duplicate Record(s) Corrected", "Dwelling", FCConvert.ToString(rsLoad.Get_Fields_Int32("rsaccount")), FCConvert.ToString(rsLoad.Get_Fields_Int32("rscard")));
                    rsSave.OpenRecordset("select * from dwelling where rsaccount = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard") + " order by id", modGlobalVariables.strREDatabase);
                    rsSave.MoveNext();
                    while (!rsSave.EndOfFile())
                    {
                        rsSave.Delete();
                        rsSave.Update();
                        rsSave.MoveNext();
                    }
                    rsLoad.MoveNext();
                }
                if (strRpt != string.Empty)
                {
                    strRpt = "Duplicate Dwelling records were found and deleted for the following. You must verify that the current data for these accounts is correct." + "\r\n" + "\r\n" + strRpt;
                    //if (Directory.Exists("rpt"))
                    if (Directory.Exists("rpt"))
                    {
                        //ts = fso.CreateTextFile("rpt\\REDFix" + Strings.Format(DateTime.Today, "m-d-yyyy") + ".txt", true, false);
                        ts = File.CreateText("rpt\\REDFix" + Strings.Format(DateTime.Today, "m-d-yyyy") + ".txt");
                        ts.WriteLine(strRpt);
                        ts.Close();
                    }
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    frmShowResults.InstancePtr.Init(ref strRpt, true);
                    frmShowResults.InstancePtr.Text = "Verify Records";
                    frmShowResults.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CheckDwellingRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private static void CheckCommercialRecords()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            clsDRWrapper rsSave = new clsDRWrapper();
            string strRpt;
            StreamWriter ts;
            try
            {
                // On Error GoTo ErrorHandler
                strRpt = "";
                rsLoad.OpenRecordset("select rsaccount,rscard from commercial group by rsaccount,rscard having count(rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    strRpt += "Account " + Strings.Left(rsLoad.Get_Fields_Int32("rsaccount") + Strings.StrDup(7, " "), 7) + " Card " + rsLoad.Get_Fields_Int32("rscard") + "\r\n";
                    modGlobalFunctions.AddCYAEntry_242("RE", "Duplicate Record(s) Corrected", "Commercial", FCConvert.ToString(rsLoad.Get_Fields_Int32("rsaccount")), FCConvert.ToString(rsLoad.Get_Fields_Int32("rscard")));
                    rsSave.OpenRecordset("select * from commercial where rsaccount = " + rsLoad.Get_Fields_Int32("rsaccount") + " and rscard = " + rsLoad.Get_Fields_Int32("rscard") + " order by id", modGlobalVariables.strREDatabase);
                    rsSave.MoveNext();
                    while (!rsSave.EndOfFile())
                    {
                        rsSave.Delete();
                        rsSave.Update();
                        rsSave.MoveNext();
                    }
                    rsLoad.MoveNext();
                }
                if (strRpt != string.Empty)
                {
                    strRpt = "Duplicate Commercial records were found and deleted for the following. You must verify that the current data is correct." + "\r\n" + "\r\n" + strRpt;
                    if (Directory.Exists("rpt"))
                    {
                        ts = System.IO.File.CreateText("rpt\\RECFix" + Strings.Format(DateTime.Today, "m-d-yyyy") + ".txt");
                        ts.WriteLine(strRpt);
                        ts.Close();
                    }
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    frmShowResults.InstancePtr.Init(ref strRpt, true);
                    frmShowResults.InstancePtr.Text = "Verify Records";
                    frmShowResults.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                }
                rsLoad.OpenRecordset("select commercial.rsaccount,commercial.rscard from commercial inner join master on (commercial.rsaccount = master.rsaccount) and (commercial.rscard = master.rscard) where master.RSDWELLINGcode = 'Y' or (master.rsdwellingcode <> 'C' and occ1 < 1)", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    rsSave.Execute("delete from commercial where rsaccount = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rsaccount"))) + " and rscard = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rscard"))), modGlobalVariables.strREDatabase);
                    rsLoad.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CheckCommercialRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void CheckPrevAssessForDupes()
        {
            // delete duplicate previous assessments
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strSQL;
            int lngPrevAccount;
            int intPrevCard;
            int lngPrevYear;
            lngPrevAccount = 0;
            intPrevCard = 0;
            lngPrevYear = 0;
            strSQL = "select * from previousassessment order by account,card,taxyear desc,id desc";
            clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                //Application.DoEvents();
                // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [card] and replace with corresponding Get_Field method
                if (lngPrevAccount == Conversion.Val(clsLoad.Get_Fields("account")) && intPrevCard == Conversion.Val(clsLoad.Get_Fields("card")) && lngPrevYear == Conversion.Val(clsLoad.Get_Fields_Int32("taxyear")))
                {
                    clsLoad.Delete();
                    clsLoad.Update();
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    lngPrevAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("account"))));
                    // TODO Get_Fields: Check the table for the column [card] and replace with corresponding Get_Field method
                    intPrevCard = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("card"))));
                    lngPrevYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("taxyear"))));
                }
                clsLoad.MoveNext();
            }
        }

        public static void CheckBookPageForDupes()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strSQL;
            int lngPrevAccount;
            int intPrevLine;
            strSQL = "select * from bookpage where account in (SELECT account from bookpage group by account,line having count(line) > 1) order by account,line,[current]";
            clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
            lngPrevAccount = 0;
            intPrevLine = 0;
            while (!clsLoad.EndOfFile())
            {
                //Application.DoEvents();
                // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                if (lngPrevAccount == Conversion.Val(clsLoad.Get_Fields("account")) && intPrevLine == Conversion.Val(clsLoad.Get_Fields("line")))
                {
                    clsLoad.Delete();
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    lngPrevAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("account"))));
                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                    intPrevLine = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("line"))));
                }
                clsLoad.MoveNext();
            }
        }

        public static void LoadCustomizedInfo()
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                modGlobalVariables.Statics.CustomizedInfo.LoadCustomInfo();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadCustomizedInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void ModSpecificReset()
        {
            // CheckDatabaseStructure
            CheckVersion_2(false);
            // CheckVersion (True)
            LoadCustomizedInfo();
            // force program to reload any cost info
            modGlobalVariables.Statics.boolCostRecChanged = true;
            modGlobalVariables.Statics.boolComCostChanged = true;
            modGlobalVariables.Statics.boolLandTChanged = true;
            modGlobalVariables.Statics.boolLandSChanged = true;
            modGlobalVariables.Statics.boolRSZChanged = true;
            // MDIParent.FileMaintCaptions
            //MDIParent.InstancePtr.SetMenuOptions("FILE");
            //FC:FINAL:MSH - reload menu for removing "Archives" item from list (same with issue #1518)
            App.MainForm.NavigationMenu.Clear();
            MDIParent.InstancePtr.SetMenuOptions("MAIN");
            ////FC:FINAL:MSH - hide menu if enabled archive mode
            //if (modGlobalVariables.Statics.boolInArchiveMode)
            //{
            //	MDIParent.InstancePtr.mnuMenucolor.Visible = true;
            //}
            //else
            //{
            //	MDIParent.InstancePtr.mnuMenucolor.Visible = false;
            //}
        }

        public static void SplitExemption(ref int lngAccountNumber)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strList = "";
            int lngExemptamount;
            int lngExemptLeft;
            int[] lngCodes = new int[3 + 1];
            int[] lngWorth = new int[3 + 1];
            // vbPorter upgrade warning: lngAmounts As int	OnWrite(int, double)
            int[] lngAmounts = new int[3 + 1];
            int[] intApplyTo = new int[3 + 1];
            int[] intPercent = new int[3 + 1];
            // Dim lngHomesteadCodes(5) As Long
            int lngHomesteadAmount;
            // vbPorter upgrade warning: lngAssessment As int	OnWriteFCConvert.ToDouble(
            int lngAssessment;
            int[] intProcessOrder = new int[3 + 1];
            bool[] boolIsHomestead = new bool[3 + 1];
            int x;
            int y;
            int intTemp = 0;
            bool boolHasHomestead;
            int lngLandExempt;
            int lngBldgExempt;
            int lngLand;
            int lngbldg;
            int lngTemp = 0;
            boolHasHomestead = false;
            // lngHomesteadCodes(1) = CustomizedInfo.HomesteadCode1
            // lngHomesteadCodes(2) = CustomizedInfo.HomesteadCode2
            // lngHomesteadCodes(3) = CustomizedInfo.HomesteadCode3
            // lngHomesteadCodes(4) = CustomizedInfo.HomesteadCode4
            // lngHomesteadCodes(5) = CustomizedInfo.HomesteadCode5
            lngLandExempt = 0;
            lngBldgExempt = 0;
            lngLand = 0;
            lngbldg = 0;
            lngHomesteadAmount = 0;
            intPercent[1] = 0;
            intPercent[2] = 0;
            intPercent[3] = 0;
            intApplyTo[1] = modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST;
            intApplyTo[2] = modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST;
            intApplyTo[3] = modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST;
            intProcessOrder[1] = 1;
            intProcessOrder[2] = 2;
            intProcessOrder[3] = 3;
            boolIsHomestead[1] = false;
            boolIsHomestead[2] = false;
            boolIsHomestead[3] = false;
            clsLoad.OpenRecordset("select sum(rlexemption) as ExemptSum,sum(lastlandval) as LandVal,sum(lastbldgval) as bldgval from master where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " group by rsaccount", modGlobalVariables.strREDatabase);
            // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
            lngExemptamount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptsum"))));
            // TODO Get_Fields: Field [landval] not found!! (maybe it is an alias?)
            // TODO Get_Fields: Field [bldgval] not found!! (maybe it is an alias?)
            lngAssessment = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("landval")) + Conversion.Val(clsLoad.Get_Fields("bldgval")));
            // TODO Get_Fields: Field [landval] not found!! (maybe it is an alias?)
            lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("landval"))));
            // TODO Get_Fields: Field [bldgval] not found!! (maybe it is an alias?)
            lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("bldgval"))));
            lngExemptLeft = lngExemptamount;
            clsLoad.OpenRecordset("select riexemptcd1,riexemptcd2,riexemptcd3,exemptpct1,exemptpct2,exemptpct3 from master where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " and rscard = 1", modGlobalVariables.strREDatabase);
            lngCodes[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("riexemptcd1"))));
            lngCodes[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("riexemptcd2"))));
            lngCodes[3] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("riexemptcd3"))));
            // TODO Get_Fields: Check the table for the column [exemptpct1] and replace with corresponding Get_Field method
            intPercent[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptpct1"))));
            // TODO Get_Fields: Check the table for the column [exemptpct2] and replace with corresponding Get_Field method
            intPercent[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptpct2"))));
            // TODO Get_Fields: Check the table for the column [exemptpct3] and replace with corresponding Get_Field method
            intPercent[3] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptpct3"))));
            if (lngExemptamount == 0 || lngCodes[1] == 0)
            {
                lngAmounts[1] = lngExemptamount;
                lngAmounts[2] = 0;
                lngAmounts[3] = 0;
                clsLoad.Execute("update master set LANDEXEMPT = 0,bldgexempt = 0,exemptval1 = 0,exemptval2 = 0,exemptval3 = 0, homesteadvalue = 0, hashomestead = false where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " and rscard = 1", modGlobalVariables.strREDatabase);
            }
            else
            {
                // strList = "(" & lngCodes(1) + 1900
                // If lngCodes(2) > 0 Then
                // strList = strList & "," & lngCodes(2) + 1900
                // End If
                // If lngCodes(3) > 0 Then
                // strList = strList & "," & lngCodes(3) + 1900
                // End If
                strList = "(" + FCConvert.ToString(lngCodes[1]);
                if (lngCodes[2] > 0)
                {
                    strList += "," + FCConvert.ToString(lngCodes[2]);
                }
                if (lngCodes[3] > 0)
                {
                    strList += "," + FCConvert.ToString(lngCodes[3]);
                }
                strList += ")";
                // Call clsLoad.OpenRecordset("select * from costrecord where crecordnumber in " & strList, strREDatabase)
                clsLoad.OpenRecordset("select * from exemptcode WHERE code in " + strList, modGlobalVariables.strREDatabase);
                if (clsLoad.EndOfFile())
                {
                    lngAmounts[1] = lngExemptamount;
                    lngAmounts[2] = 0;
                    lngAmounts[3] = 0;
                    return;
                }
                for (x = 1; x <= 3; x++)
                {
                    if (lngCodes[x] > 0)
                    {
                        // Call clsLoad.FindFirstRecord("crecordnumber", lngCodes(x) + 1900)
                        // Call clsLoad.FindFirstRecord("code", lngCodes(x))
                        clsLoad.FindFirst("code = " + FCConvert.ToString(lngCodes[x]));
                        // If Val(clsLoad.Fields("cunit")) = 99999999 Then
                        // TODO Get_Fields: Check the table for the column [AMOUNT] and replace with corresponding Get_Field method
                        if (Conversion.Val(clsLoad.Get_Fields("AMOUNT")) == 999999)
                        {
                            lngCodes[x] = 0;
                            lngAmounts[x] = 0;
                        }
                        // TODO Get_Fields: Check the table for the column [AMOUNT] and replace with corresponding Get_Field method
                        else if (Conversion.Val(clsLoad.Get_Fields("AMOUNT")) == 0)
                        {
                            if (x < 3)
                            {
                                // intPercent(x) = lngCodes(x + 1)
                                // lngCodes(x + 1) = 0
                                lngAmounts[x] = lngExemptamount;
                                if (intPercent[x] > 0)
                                {
                                    lngAmounts[x] = FCConvert.ToInt32(lngExemptamount * (intPercent[x] / 100.0));
                                }
                            }
                        }
                        else
                        {
                            // lngAmounts(x) = clsLoad.Fields("cunit") / 100
                            // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                            lngAmounts[x] = FCConvert.ToInt32(clsLoad.Get_Fields("amount"));
                            // For Y = 1 To 5
                            // If lngHomesteadCodes(Y) = lngCodes(x) Then
                            // TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(clsLoad.Get_Fields("category")) == modcalcexemptions.CNSTEXEMPTCATHOMESTEAD)
                            {
                                boolIsHomestead[x] = true;
                                boolHasHomestead = true;
                                // If (lngAssessment / CustomizedInfo.CertifiedRatio) < 125000 Then
                                // 
                                // ElseIf lngAssessment < 250000 Then
                                // lngAmounts(x) = (5 / 7) * lngAmounts(x)
                                // 
                                // Else
                                // lngAmounts(x) = (2.5 / 7) * lngAmounts(x)
                                // 
                                // End If
                                if (x < 3 && lngCodes[x + 1] > 0)
                                {
                                    if (x == 1)
                                    {
                                        intProcessOrder[1] = 2;
                                        intProcessOrder[2] = 3;
                                        intProcessOrder[3] = 1;
                                    }
                                    else
                                    {
                                        intTemp = intProcessOrder[2];
                                        intProcessOrder[2] = intProcessOrder[3];
                                        intProcessOrder[3] = intTemp;
                                    }
                                }
                                // Exit For
                            }
                            // Next Y
                            // TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
                            if (!boolIsHomestead[x] && FCConvert.ToInt32(clsLoad.Get_Fields("category")) != modcalcexemptions.CNSTEXEMPTCATPARSONAGE)
                            {
                                lngAmounts[x] = FCConvert.ToInt32(lngAmounts[x] * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio);
                            }
                        }
                        intApplyTo[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("applyto"))));
                    }
                }
                // x
                // now the process order is established and any codes that were actually percents are taken care of
                for (x = 1; x <= 3; x++)
                {
                    // go through the process order and try to split the exemption correctly
                    if (lngCodes[intProcessOrder[x]] > 0)
                    {
                        if (lngAmounts[intProcessOrder[x]] == lngExemptLeft)
                        {
                            lngWorth[intProcessOrder[x]] = lngAmounts[intProcessOrder[x]];
                            lngExemptLeft = 0;
                        }
                        else if (lngAmounts[intProcessOrder[x]] > lngExemptLeft)
                        {
                            // not enough to cover amount
                            lngWorth[intProcessOrder[x]] = lngExemptLeft;
                            lngExemptLeft = 0;
                        }
                        else
                        {
                            // exemption left over
                            lngWorth[intProcessOrder[x]] = lngAmounts[intProcessOrder[x]];
                            lngExemptLeft -= lngWorth[intProcessOrder[x]];
                        }
                        if (boolIsHomestead[intProcessOrder[x]])
                        {
                            lngHomesteadAmount += lngWorth[intProcessOrder[x]];
                        }
                        if (intApplyTo[intProcessOrder[x]] == modcalcexemptions.CNSTEXEMPTAPPLYTOLANDFIRST)
                        {
                            if (lngLand >= lngWorth[intProcessOrder[x]])
                            {
                                lngLandExempt += lngWorth[intProcessOrder[x]];
                                lngLand -= lngWorth[intProcessOrder[x]];
                            }
                            else
                            {
                                lngTemp = lngWorth[intProcessOrder[x]] - lngLand;
                                lngLandExempt += lngLand;
                                lngBldgExempt += lngTemp;
                                lngLand = 0;
                                lngbldg -= lngTemp;
                            }
                        }
                        else
                        {
                            if (lngbldg >= lngWorth[intProcessOrder[x]])
                            {
                                lngBldgExempt += lngWorth[intProcessOrder[x]];
                                lngbldg -= lngWorth[intProcessOrder[x]];
                            }
                            else
                            {
                                lngTemp = lngWorth[intProcessOrder[x]] - lngbldg;
                                lngBldgExempt += lngbldg;
                                lngLandExempt += lngTemp;
                                lngbldg = 0;
                                lngLand -= lngTemp;
                            }
                        }
                    }
                }
                // x
                // now save in database
                clsLoad.Execute("update master set exemptval1 = " + FCConvert.ToString(lngWorth[1]) + ",exemptval2 = " + FCConvert.ToString(lngWorth[2]) + " ,exemptval3 = " + FCConvert.ToString(lngWorth[3]) + ", homesteadvalue = " + FCConvert.ToString(lngHomesteadAmount) + ", hashomestead = " + FCConvert.ToString(boolHasHomestead) + ",landexempt = " + FCConvert.ToString(lngLandExempt) + ",bldgexempt = " + FCConvert.ToString(lngBldgExempt) + " where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " and rscard = 1", modGlobalVariables.strREDatabase);
            }
        }

        private static bool CheckTranTownTable()
        {
            bool CheckTranTownTable = false;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper clsSave = new clsDRWrapper();
                clsDRWrapper clsLoad = new clsDRWrapper();
                CheckTranTownTable = false;
                if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    // must force tran code table to be the same as town codes
                    clsSave.Execute("delete * from tbltrancode", modGlobalVariables.strREDatabase);
                    clsLoad.OpenRecordset("select * from tblregions", modGlobalVariables.Statics.strGNDatabase);
                    clsSave.OpenRecordset("select * from tbltrancode", modGlobalVariables.strREDatabase);
                    while (!clsLoad.EndOfFile())
                    {
                        clsSave.AddNew();
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        clsSave.Set_Fields("CODE", clsLoad.Get_Fields("townnumber"));
                        clsSave.Set_Fields("Description", clsLoad.Get_Fields_String("townname"));
                        clsSave.Set_Fields("shortdescription", clsLoad.Get_Fields_String("townabbreviation"));
                        clsSave.Set_Fields("amount", 0);
                        clsSave.Update();
                        clsLoad.MoveNext();
                    }
                }
                CheckTranTownTable = true;
                return CheckTranTownTable;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CheckTranTownTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CheckTranTownTable;
        }
        //FC:FINAL:AM: not supported
        //public static void NewExtractToDisk()
        //{
        //    string strRec = "";
        //
        //    Scripting.Folder flder;
        //    Scripting.Drive tempDrive;
        //    int intRecsPerDisk;
        //    int intCounter;
        //    // vbPorter upgrade warning: intRes As short, int --> As DialogResult
        //    DialogResult intRes;
        //    // vbPorter upgrade warning: ans9 As Variant --> As DialogResult
        //    DialogResult ans9;
        //    int intRecsOnThisDisk;
        //    Scripting.Files FilesList;
        //    int x;
        //    Scripting.File fl;
        //    int lngSpaceLeft;
        //    int lngSpaceUsed;
        //    int lngSpacePerDisk;
        //    string strPath;
        //    try
        //    {   // On Error GoTo ErrorHandler
        //        if (!File.Exists("tsdbase.asc"))
        //        {
        //            MessageBox.Show("No extract file found to copy", "No File Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //            return;
        //        }
        //        // strPath = frmGetDirectory.Init("Select Drive and Directory to copy to", "", False, "Copy Extract To")
        //        // MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
        //        MDIParent.InstancePtr.CommonDialog1.FileName = "tsdbase.asc";
        //        //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
        //        MDIParent.InstancePtr.CommonDialog1.ShowDialog();
        //        strPath = MDIParent.InstancePtr.CommonDialog1.FileName;
        //        if (Strings.Trim(strPath) == string.Empty)
        //        {
        //            return;
        //        }
        //        if (Strings.Left(Strings.UCase(fso.GetDriveName(strPath)), 1) != "A")
        //        {
        //            tempDrive = fso.GetDrive(fso.GetDriveName(strPath));
        //            if (tempDrive.DriveType == DriveTypeConst.CDRom)
        //            {
        //                MessageBox.Show("Cannot write to this type of medium", "Invalid Medium", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //                return;
        //            }
        //            if (!tempDrive.IsReady)
        //            {
        //                MessageBox.Show("Drive isn't ready", "Drive Not Ready", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //                return;
        //            }
        //            File.Copy("tsdbase.asc", strPath, true);
        //            MessageBox.Show("File Copied", "Copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            return;
        //        }
        //        here:;
        //        ans9 = MessageBox.Show("Put the first diskette into Drive A.", "Copy DB Extract to Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        //        if (ans9 == DialogResult.Cancel)
        //        {
        //            MDIParent.InstancePtr.GRID.Focus();
        //            return;
        //        }
        //        tempDrive = fso.GetDrive("a:");
        //        if (!tempDrive.IsReady)
        //        {
        //            intRes = MessageBox.Show("The drive is not ready.", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        //            if (intRes == DialogResult.Cancel)
        //            {
        //                MDIParent.InstancePtr.GRID.Focus();
        //                return;
        //            }
        //            goto here;
        //        }
        //        FCGlobal.Screen.MousePointer = 11;
        //        Close(1, 2);
        //        FCFileSystem.FileOpen(2, "TSDBASE.ASC", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        FCFileSystem.FileClose(2);
        //        FCFileSystem.FileOpen(2, "TSDBASE.ASC", OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        flder = fso.GetFolder("A:");
        //        FilesList = flder.Files;
        //        lngSpaceUsed = 0;
        //        foreach (int fl in FilesList)
        //        {
        //            lngSpaceUsed += fl.Size;
        //        } // fl
        //        FCFileSystem.FileOpen(1, "A:\\TSDBASE.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        lngSpacePerDisk = 1440000;
        //        strRec = FCFileSystem.LineInput(2);
        //        NextDiskTag:;
        //        if (lngSpaceUsed + strRec.Length >= lngSpacePerDisk)
        //        {
        //            lngSpaceUsed = 0;
        //            FCFileSystem.FileClose(1);
        //            NextPrompt:;
        //            intRes = MessageBox.Show("Be sure the light on Drive A is OFF, then insert a disk and press Enter.", " ", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        //            if (intRes == DialogResult.Cancel) goto alldone;
        //            tempDrive = fso.GetDrive("a:");
        //            if (!tempDrive.IsReady)
        //            {
        //                intRes = MessageBox.Show("The drive is not ready.", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        //                if (intRes == DialogResult.Cancel)
        //                {
        //                    MDIParent.InstancePtr.GRID.Focus();
        //                    return;
        //                }
        //                goto NextPrompt;
        //            }
        //            flder = fso.GetFolder("A:");
        //            FilesList = flder.Files;
        //            lngSpaceUsed = 0;
        //            foreach (int fl in FilesList)
        //            {
        //                lngSpaceUsed += fl.Size;
        //            } // fl
        //            FCFileSystem.FileOpen(1, "A:\\TSDBASE.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        }
        //        // Do Until EOF(2) Or intcounter = intRecsPerDisk
        //        while (!(FCFileSystem.EOF(2) || (lngSpaceUsed + strRec.Length >= lngSpacePerDisk)))
        //        {
        //            FCFileSystem.PrintLine(1, strRec);
        //            lngSpaceUsed += strRec.Length;
        //            if (!FCFileSystem.EOF(2))
        //            {
        //                strRec = FCFileSystem.LineInput(2);
        //            }
        //            else
        //            {
        //                strRec = "";
        //            }
        //        }
        //        // If intcounter >= intRecsPerDisk And Not EOF(2) Then GoTo NextDiskTag
        //        if (!FCFileSystem.EOF(2)) goto NextDiskTag;
        //        alldone:;
        //        FCFileSystem.FileClose(1);
        //        FCFileSystem.FileClose(2);
        //        MessageBox.Show("When the drive light goes off you may remove disk.  Copy is finished.", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        FCGlobal.Screen.MousePointer = 0;
        //        return;
        //    }
        //    catch(Exception ex)
        //    {   // ErrorHandler:
        //        FCFileSystem.FileClose(1);
        //        FCFileSystem.FileClose(2);
        //        if (Information.Err(ex).Number != 32755)
        //        {
        //            MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "The copy was not successful.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //        }
        //        else
        //        {
        //            // cancelled copy
        //        }
        //    }
        //}
        //public static void ExtractToDisk()
        //{
        //    string strRec = "";
        //
        //    Scripting.Folder flder;
        //    Scripting.Drive tempDrive;
        //    // vbPorter upgrade warning: intRecsPerDisk As int	OnWrite(double, int)
        //    int intRecsPerDisk;
        //    int intCounter;
        //    // vbPorter upgrade warning: intRes As short, int --> As DialogResult
        //    DialogResult intRes;
        //    // vbPorter upgrade warning: ans9 As Variant --> As DialogResult
        //    DialogResult ans9;
        //    // vbPorter upgrade warning: intRecsOnThisDisk As int	OnWrite(int, double)
        //    int intRecsOnThisDisk;
        //    Scripting.Files FilesList;
        //    int x;
        //    Scripting.File fl;
        //    int lngSpaceLeft;
        //    int lngSpaceUsed;
        //    try
        //    {   // On Error GoTo ErrorHandler
        //        here:;
        //        ans9 = MessageBox.Show("Put the first diskette into Drive A.", "Copy DB Extract to Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        //        if (ans9 == DialogResult.Cancel)
        //        {
        //            MDIParent.InstancePtr.GRID.Focus();
        //            return;
        //        }
        //        tempDrive = fso.GetDrive("a:");
        //        if (!tempDrive.IsReady)
        //        {
        //            intRes = MessageBox.Show("The drive is not ready.", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        //            if (intRes == DialogResult.Cancel)
        //            {
        //                MDIParent.InstancePtr.GRID.Focus();
        //                return;
        //            }
        //            goto here;
        //        }
        //        FCGlobal.Screen.MousePointer = 11;
        //        Close(1, 2);
        //        FCFileSystem.FileOpen(2, "TSDBASE.ASC", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        FCFileSystem.FileClose(2);
        //        FCFileSystem.FileOpen(2, "TSDBASE.ASC", OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        flder = fso.GetFolder("A:");
        //        // intRecsOnThisDisk = flder.Size
        //        FilesList = flder.Files;
        //        intRecsOnThisDisk = 0;
        //        foreach (int fl in FilesList)
        //        {
        //            intRecsOnThisDisk += fl.Size;
        //        } // fl
        //        FCFileSystem.FileOpen(1, "A:\\TSDBASE.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        strRec = FCFileSystem.LineInput(2);
        //        FCFileSystem.PrintLine(1, strRec);
        //        intCounter = 1;
        //        intRecsPerDisk = FCConvert.ToInt32(1440000.0 / strRec.Length);
        //        intRecsOnThisDisk /= strRec.Length;
        //        intRecsOnThisDisk = intRecsPerDisk - intRecsOnThisDisk;
        //        intRecsPerDisk -= 1;
        //        NextDiskTag:;
        //        // If intcounter >= intRecsPerDisk Then
        //        if (intCounter >= intRecsOnThisDisk)
        //        {
        //            intCounter = 0;
        //            FCFileSystem.FileClose(1);
        //            MessageBox.Show("Be sure the light on Drive A is OFF, then insert disk and press Enter.", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            FCFileSystem.FileOpen(1, "A:\\TSDBASE.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
        //        }
        //        // Do Until EOF(2) Or intcounter = intRecsPerDisk
        //        while (!(FCFileSystem.EOF(2) || intCounter == intRecsOnThisDisk))
        //        {
        //            intCounter += 1;
        //            strRec = FCFileSystem.LineInput(2);
        //            FCFileSystem.PrintLine(1, strRec);
        //            flder = fso.GetFolder("A:");
        //            FilesList = flder.Files;
        //            intRecsOnThisDisk = 0;
        //            foreach (int fl in FilesList)
        //            {
        //                intRecsOnThisDisk += fl.Size;
        //            } // fl
        //            intRecsOnThisDisk = intRecsPerDisk - intRecsOnThisDisk;
        //        }
        //        // If intcounter >= intRecsPerDisk And Not EOF(2) Then GoTo NextDiskTag
        //        if (intCounter >= intRecsOnThisDisk && !FCFileSystem.EOF(2)) goto NextDiskTag;
        //        FCFileSystem.FileClose(1);
        //        FCFileSystem.FileClose(2);
        //        MessageBox.Show("When the drive light goes off you may remove disk.  Copy is finished.", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        FCGlobal.Screen.MousePointer = 0;
        //        return;
        //    }
        //    catch
        //    {   // ErrorHandler:
        //        FCFileSystem.FileClose(1);
        //        FCFileSystem.FileClose(2);
        //        MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "The copy was not successful.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //    }
        //}
        public static void GetAccount()
        {
            string strDwelling;
            string strOutbuilding;
            // TODO Get_Fields: Field [dwellingflag] not found!! (maybe it is an alias?)
            strDwelling = FCConvert.ToString(modGlobalVariables.Statics.rs.Get_Fields("dwellingflag"));
            // TODO Get_Fields: Field [outbuildingflag] not found!! (maybe it is an alias?)
            strOutbuilding = FCConvert.ToString(modGlobalVariables.Statics.rs.Get_Fields("outbuildingflag"));
            string strLine1;
            string strLine2;
            string strLine3;
            string strLine4;
            strLine1 = "";
            strLine2 = "";
            strLine3 = "";
            strLine4 = "";
            strLine1 = "SELECT *, *, *";
            // Dwelling/Commercial
            if (strDwelling == "Y")
            {
                strLine2 = " FROM ((SRMaster INNER JOIN SRDwelling ON " + "SRMaster.Key = SRDwelling.dKey)";
            }
            else if (strDwelling == "C")
            {
                strLine2 = " FROM ((SRMaster INNER JOIN SRCommercial ON " + "SRMaster.Key = SRCommercial.cKey)";
            }
            else
            {
                strLine2 = " FROM (SRMaster ";
            }
            // Outbuildings
            if (strOutbuilding == "Y")
            {
                strLine3 = " INNER JOIN SROutbuilding ON SRMaster.Key = SROutbuilding.oKey)";
            }
            else
            {
                if (strOutbuilding != "Y" && (strDwelling != "C" || strDwelling != "Y"))
                {
                    strLine1 = "SELECT * FROM SRMaster";
                    strLine2 = "";
                    strLine3 = "";
                }
                else
                {
                    strLine3 = ")";
                }
            }
            strLine4 = " WHERE SRMaster.Key = " + modGlobalVariables.Statics.rs.Get_FieldsIndexValue(0);
            modGlobalVariables.Statics.strSQL = strLine1 + strLine2 + strLine3 + strLine4;
            modGlobalVariables.Statics.rs.OpenRecordset(modGlobalVariables.Statics.strSQL, modGlobalVariables.strREDatabase);
            if (modGlobalVariables.Statics.rs.EndOfFile() != true && modGlobalVariables.Statics.rs.BeginningOfFile() != true)
            {
                modGlobalVariables.Statics.rs.MoveLast();
                modGlobalVariables.Statics.rs.MoveFirst();
            }
            else
            {
                MessageBox.Show("Invalid Sale Database.  Call TRIO.", "Sale Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void UnloadAllForms()
        {
            modGlobalVariables.Statics.UnloadingForms = true;
            frmGetAccount.InstancePtr.Show();
            //FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
            //foreach (FCForm frm1 in Application.OpenForms)
            //{
            //    if (frm1.Name != "frmGetAccount")
            //    {
            //        frm1.Unload();
            //    }
            //}
            FCUtils.CloseFormsOnProject(formName: "frmGetAccount");
            modGlobalVariables.Statics.UnloadingForms = false;
        }

        public static void WaitForTerm(ref int pID)
        {
            int pHND;
            int pCheck = 0;
            pHND = modGlobalVariables.OpenProcess(modGlobalVariables.SYNCHRONIZE, 0, pID);
            if (pHND != 0)
            {
                do
                {
                    pCheck = modGlobalVariables.WaitForSingleObject(pHND, 0);
                    if (pCheck != 0x102)
                        break;
                    //Application.DoEvents();
                }
                while (true);
                modGlobalVariables.CloseHandle(pHND);
            }
        }

        public static bool UpdateViewAnother()
        {
            bool UpdateViewAnother = false;
            // vbPorter upgrade warning: ans As Variant --> As DialogResult
            DialogResult ans;
            ans = MessageBox.Show("Would you like to save this account first?", "Save First", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ans == DialogResult.Yes)
            {
                UpdateViewAnother = true;
            }
            else
            {
                UpdateViewAnother = false;
            }
            return UpdateViewAnother;
        }

        public static void SaveData2()
        {
            try
            {
                // On Error GoTo ErrorHandler
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                modGlobalVariables.Statics.CurrentAccountKey = modGlobalVariables.Statics.gintLastAccountNumber;
                frmREProperty.InstancePtr.FrmToMRArray();
                modGlobalVariables.Statics.DataChanged = false;
                SaveAddressInfo(modGNBas.Statics.gintCardNumber);
                // Screen.MousePointer = vbDefault
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: intNewCard As short	OnWriteFCConvert.ToInt32(
        public static void SaveAddressInfo(int intNewCard = 0)
        {
            string strMRRSName;
            int intCounter;
            // Dim clsTemp As New clsDRWrapper
            string strSQL = "";
            string strMRRSSecOwner;
            string strMRRSAddr1;
            string strMRRSAddr2;
            string strMRRSCity;
            string strMRRSState;
            string strMRRSZip;
            string strMRRSZip4;
            string strMRRSRef1;
            string strMRRSRef2;
            string strMRRSMAPLOT;
            string strMastName = "";
            string strPrevMast = "";
            string strPhone = "";
            // vbPorter upgrade warning: dtSDate As DateTime	OnWriteFCConvert.ToInt16(
            DateTime dtSDate;
            int lngSPrice;
            int intSValidity;
            int intSType;
            int intSVerified;
            int intSFinanced;
            string strTemp = "";
            int intTemp;
            int lngTranCode;
            // vbPorter upgrade warning: boolRevocableTrust As bool	OnWrite(string)
            bool boolRevocableTrust = false;
            string strPhone2 = "";
            string strEmail;
            string strPhoneDesc = "";
            string str2ndPhoneDesc = "";
            int x;
            int[] ExemptCd = new int[3 + 1];
            double[] ExemptPct = new double[3 + 1];
            try
            {
                // On Error GoTo ErrorHandler
                // MKEY = CurrentAccount
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    strMastName = "Master";
                }
                else
                {
                    strMastName = "SRMaster";
                }
                // Call GetMaster(gintLastAccountNumber, 1)
                modDataTypes.Statics.MR.Set_Fields("rsaccount", modGlobalVariables.Statics.CurrentAccount);
                modDataTypes.Statics.MR.Set_Fields("rscard", intNewCard);
                clsDRWrapper temp = modDataTypes.Statics.MR;
                modREMain.SaveMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount, intNewCard);
                modDataTypes.Statics.MR = temp;
                for (x = 1; x <= 3; x++)
                {
                    if (Conversion.Val(frmREProperty.InstancePtr.GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE)) > -1)
                    {
                        ExemptCd[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(frmREProperty.InstancePtr.GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE))));
                        ExemptPct[x] = Conversion.Val(frmREProperty.InstancePtr.GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT));
                    }
                    else
                    {
                        ExemptCd[x] = 0;
                        ExemptPct[x] = 100;
                    }
                }
                // x
                // If boolRegRecords Then
                // mr.Fields("ownerpartyid") =
                // mr.Fields("secownerpartyid") =
                // Else
                // MR.Fields("rsname") = Trim(frmREProperty.txtMRRSName.Text & "")
                // MR.Fields("rssecowner") = Trim(frmREProperty.txtMRRSSecOwner.Text & "")
                // MR.Fields("rsaddr1") = Trim(frmREProperty.txtMRRSAddr1.Text & "")
                // MR.Fields("rsaddr2") = Trim(frmREProperty.txtMRRSAddr2.Text & "")
                // MR.Fields("rsaddr3") = Trim(frmREProperty.txtMRRSAddr3.Text & "")
                // MR.Fields("rsstate") = Trim(frmREProperty.txtMRRSState.Text & "")
                // MR.Fields("rszip") = Trim(frmREProperty.txtMRRSZip.Text & "")
                // MR.Fields("rszip4") = Trim(frmREProperty.txtMRRSZip4.Text & "")
                // MR.Fields("Email") = Trim(frmREProperty.txtEmail.Text)
                // End If
                modDataTypes.Statics.MR.Set_Fields("rspreviousmaster", Strings.Trim(frmREProperty.InstancePtr.txtMRRSPrevMaster.Text + ""));
                modDataTypes.Statics.MR.Set_Fields("rsmaplot", Strings.Trim(frmREProperty.InstancePtr.txtMRRSMAPLOT.Text + ""));
                modDataTypes.Statics.MR.Set_Fields("rsref1", Strings.RTrim(frmREProperty.InstancePtr.txtMRRSRef1.Text));
                modDataTypes.Statics.MR.Set_Fields("rsref2", Strings.Trim(frmREProperty.InstancePtr.txtMRRSRef2.Text + ""));
                // MR.Fields("RSBook") = Val(frmREProperty.txtBook.Text & "")
                // MR.Fields("RSPage") = Val(frmREProperty.txtpage.Text & "")
                modDataTypes.Statics.MR.Set_Fields("rslocstreet", Strings.Trim(frmREProperty.InstancePtr.txtMRRSLocStreet.Text + ""));
                modDataTypes.Statics.MR.Set_Fields("rslocnumalph", Strings.Trim(frmREProperty.InstancePtr.txtMRRSLocnumalph.Text + ""));
                modDataTypes.Statics.MR.Set_Fields("rslocapt", Strings.Trim(frmREProperty.InstancePtr.txtMRRSLocapt.Text + ""));
                // MR.Fields("ritrancode") = Val(frmREProperty.txtMRRITranCode.Text)
                modDataTypes.Statics.MR.Set_Fields("ritrancode", FCConvert.ToString(Conversion.Val(frmREProperty.InstancePtr.gridTranCode.TextMatrix(0, 0))));
                // If frmREProperty.GridPhones.Rows > 1 Then
                // strTemp = frmREProperty.GridPhones.TextMatrix(1, CNSTGRIDPHONESCOLNUMBER)
                // strPhoneDesc = frmREProperty.GridPhones.TextMatrix(1, CNSTGRIDPHONESCOLDESC)
                // Else
                // strTemp = ""
                // strPhoneDesc = ""
                // End If
                // strPhone = ""
                // For intTemp = 1 To Len(strTemp)
                // If IsNumeric(Mid(strTemp, intTemp, 1)) Then
                // strPhone = strPhone & Mid(strTemp, intTemp, 1)
                // End If
                // Next intTemp
                // strPhone = PadToString(strPhone, 10)
                // MR.Fields("rstelephone") = strPhone
                // strTemp = frmREProperty.txtPhone2.Text
                // strPhone2 = ""
                // For intTemp = 1 To Len(strTemp)
                // If IsNumeric(Mid(strTemp, intTemp, 1)) Then
                // strPhone2 = strPhone2 & Mid(strTemp, intTemp, 1)
                // End If
                // Next intTemp
                // strPhone2 = PadToString(strPhone2, 10)
                // MR.Fields("SecondPhone") = strPhone2
                // strPhoneDesc = Trim(frmREProperty.txtPhone1Desc.Text)
                // MR.Fields("PhoneDescription") = strPhoneDesc
                // str2ndPhoneDesc = Trim(frmREProperty.txtPhone2Desc.Text)
                // MR.Fields("SecondPhoneDescription") = str2ndPhoneDesc
                temp = modDataTypes.Statics.MR;
                modREMain.SaveMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount, intNewCard);
                modDataTypes.Statics.MR = temp;
                strMRRSName = modDataTypes.Statics.MR.Get_Fields_String("rsname") + "";
                lngTranCode = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode"))));
                strMRRSSecOwner = modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + "";
                strMRRSAddr1 = modDataTypes.Statics.MR.Get_Fields_String("rsaddr1") + "";
                strMRRSAddr2 = modDataTypes.Statics.MR.Get_Fields_String("rsaddr2") + "";
                strMRRSCity = modDataTypes.Statics.MR.Get_Fields_String("rsaddr3") + "";
                strMRRSState = modDataTypes.Statics.MR.Get_Fields_String("rsstate") + "";
                strMRRSZip = modDataTypes.Statics.MR.Get_Fields_String("rszip") + "";
                strMRRSZip4 = modDataTypes.Statics.MR.Get_Fields_String("rszip4") + "";
                strMRRSRef1 = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + "";
                strMRRSRef2 = modDataTypes.Statics.MR.Get_Fields_String("rsref2") + "";
                strMRRSMAPLOT = modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "";
                strEmail = modDataTypes.Statics.MR.Get_Fields_String("email") + "";
                lngSPrice = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "")));
                if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate") + ""))
                {
                    dtSDate = (DateTime)modDataTypes.Statics.MR.Get_Fields_DateTime("saledate");
                }
                else
                {
                    dtSDate = DateTime.FromOADate(0);
                }
                intSValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity") + "")));
                intSType = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaletype") + "")));
                intSVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleverified") + "")));
                intSFinanced = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalefinancing") + "")));
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    boolRevocableTrust = FCConvert.CBool(modDataTypes.Statics.MR.Get_Fields_Boolean("revocabletrust") + "");
                }
                // If intNewCard = 0 Then
                // For intCounter = 2 To MAXCARDS
                // If frmREProperty.optCardNumber(intCounter - 1).Enabled Then
                // Call GetMaster(CurrentAccount, intCounter)
                // Call clsTemp.OpenRecordset("select * from " & strMastName & " where rsaccount = " & gintLastAccountNumber, strredatabase)
                temp = modDataTypes.Statics.MR;
                modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount);
                modDataTypes.Statics.MR = temp;
                while (!modDataTypes.Statics.MR.EndOfFile())
                {
                    modDataTypes.Statics.MR.Set_Fields("RSNAME", strMRRSName);
                    modDataTypes.Statics.MR.Set_Fields("rssecowner", strMRRSSecOwner);
                    modDataTypes.Statics.MR.Set_Fields("rsaddr1", strMRRSAddr1);
                    modDataTypes.Statics.MR.Set_Fields("rsaddr2", strMRRSAddr2);
                    modDataTypes.Statics.MR.Set_Fields("rsaddr3", strMRRSCity);
                    modDataTypes.Statics.MR.Set_Fields("rsstate", strMRRSState);
                    modDataTypes.Statics.MR.Set_Fields("rszip", strMRRSZip);
                    modDataTypes.Statics.MR.Set_Fields("rszip4", strMRRSZip4);
                    modDataTypes.Statics.MR.Set_Fields("ritrancode", lngTranCode);
                    modDataTypes.Statics.MR.Set_Fields("rsmaplot", strMRRSMAPLOT);
                    modDataTypes.Statics.MR.Set_Fields("email", strEmail);
                    modDataTypes.Statics.MR.Set_Fields("rstelephone", strPhone);
                    modDataTypes.Statics.MR.Set_Fields("secondphone", strPhone2);
                    modDataTypes.Statics.MR.Set_Fields("PhoneDescription", strPhoneDesc);
                    modDataTypes.Statics.MR.Set_Fields("SecondPhoneDescription", str2ndPhoneDesc);
                    modDataTypes.Statics.MR.Set_Fields("saledate", dtSDate);
                    modDataTypes.Statics.MR.Set_Fields("pisalefinancing", intSFinanced);
                    modDataTypes.Statics.MR.Set_Fields("pisaleprice", lngSPrice);
                    modDataTypes.Statics.MR.Set_Fields("pisalevalidity", intSValidity);
                    modDataTypes.Statics.MR.Set_Fields("pisaleverified", intSVerified);
                    modDataTypes.Statics.MR.Set_Fields("pisaletype", intSType);
                    for (x = 1; x <= 3; x++)
                    {
                        modDataTypes.Statics.MR.Set_Fields("exemptpct" + x, ExemptPct[x]);
                        modDataTypes.Statics.MR.Set_Fields("riexemptcd" + x, ExemptCd[x]);
                    }
                    // x
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        modDataTypes.Statics.MR.Set_Fields("revocabletrust", boolRevocableTrust);
                    }
                    if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
                    {
                    }
                    modDataTypes.Statics.MR.Update();
                    modDataTypes.Statics.MR.MoveNext();
                    if (!modDataTypes.Statics.MR.EndOfFile())
                        modDataTypes.Statics.MR.Edit();
                }
                // Call SaveMasterTable(MR, CurrentAccount, intCounter)
                // Else
                // Exit For
                // End If
                // Next
                // Else
                // Call GetMaster(CurrentAccount, intNewCard)
                // 
                // MR.fields("rsname") = Trim(strMRRSName)
                // MR.fields("rssecowner") = Trim(strMRRSSecOwner)
                // MR.fields("rsaddr1") = Trim(strMRRSAddr1)
                // MR.fields("rsaddr2") = Trim(strMRRSAddr2)
                // MR.fields("rsaddr3") = Trim(strMRRSCity)
                // MR.fields("rsstate") = Trim(strMRRSState)
                // MR.fields("rszip") = Trim(strMRRSZip)
                // MR.fields("rszip4") = Trim(strMRRSZip4)
                // MR.fields("rsref1") = Trim(strMRRSRef1)
                // MR.fields("rsref2") = Trim(strMRRSRef2)
                // MR.fields("rsmaplot") = Trim(strMRRSMAPLOT)
                // End If
                temp = modDataTypes.Statics.MR;
                modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.MR = temp;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveAddressInfo", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void GetMaster(int intAccount = 0, int intCard = 0)
        {
            // 
            // this on error checks to see if record is locked
            object ans;
        TryOver:
            ;
            /*? On Error Resume Next  */
            try
            {
                clsDRWrapper temp = modDataTypes.Statics.MR;
                modREMain.OpenMasterTable(ref temp, intAccount, intCard);
                modDataTypes.Statics.MR = temp;
                // Call OpenCOMMERCIALTable(CMR, intAccount, intCard)
                // Call OpenIncomeTable(INCVB, intAccount, intCard)
            }
            catch
            {
            }
        }

        public static void UnloadForms(string strFormName)
        {
            //FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
            //foreach (FCForm frm in Application.OpenForms)
            //{
            //    if (frm.Name != strFormName)
            //    {
            //        frm.Unload();
            //    }
            //}
            FCUtils.CloseFormsOnProject(formName: strFormName);
        }

        public static void FixOpts(dynamic frm)
        {
            if (FCConvert.ToInt32(modGNBas.Statics.Resp1) != 0)
            {
                frm.optCardNumber(modGNBas.Statics.Resp2).Enabled = true;
            }
            else
            {
                frm.optCardNumber(modGNBas.Statics.Resp2).Enabled = false;
            }
        }

        public static int GetNextAccountToCreate()
        {
            int GetNextAccountToCreate = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            int x = 0;
            int lngLastAccount;
            GetNextAccountToCreate = 0;
            clsLoad.OpenRecordset("select rsaccount from master where rsaccount > 0 group by rsaccount order by rsaccount", modGlobalVariables.strREDatabase);
            lngLastAccount = 0;
            while (!clsLoad.EndOfFile())
            {
                x = lngLastAccount + 1;
                if (Strings.UCase(modGlobalConstants.Statics.MuniName) != "BANGOR" || (x < 10000 || x > 10999))
                {
                    if (lngLastAccount + 1 != FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rsaccount")))
                    {
                        GetNextAccountToCreate = lngLastAccount + 1;
                        return GetNextAccountToCreate;
                    }
                }
                lngLastAccount = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rsaccount"));
                clsLoad.MoveNext();
            }
            if (lngLastAccount == 0)
            {
                GetNextAccountToCreate = 1;
            }
            else
            {
                GetNextAccountToCreate = lngLastAccount + 1;
            }
            return GetNextAccountToCreate;
        }

        public static int GetMaxCardsAllowed()
        {
            int GetMaxCardsAllowed = 0;
            // Dim clsTemp As New clsDRWrapper
            // 
            // Call clsTemp.OpenRecordset("select * from modules", strGNDatabase)
            // 
            // GetMaxCardsAllowed = Val(clsTemp.Fields("RE_MaxAccounts"))
            // If GetMaxCardsAllowed = 0 Then GetMaxCardsAllowed = EleventyBillion
            // get rid of the next line to make max accounts work
            GetMaxCardsAllowed = modGlobalConstants.EleventyBillion;
            return GetMaxCardsAllowed;
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(short, object)
        public static object GetMaxAccounts()
        {
            object GetMaxAccounts = null;
            clsDRWrapper rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset("select rsaccount from master", modGlobalVariables.strREDatabase);
            if (rsTemp.EndOfFile())
            {
                GetMaxAccounts = 0;
                return GetMaxAccounts;
            }
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                rsTemp.OpenRecordset("SELECT max(RSAccount) as MaxAccount FROM Master", modGlobalVariables.strREDatabase);
            }
            else
            {
                rsTemp.OpenRecordset("select max(rsaccount) as maxaccount from srmaster", modGlobalVariables.strREDatabase);
            }
            // TODO Get_Fields: Field [maxaccount] not found!! (maybe it is an alias?)
            if (fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields("maxaccount")))
            {
                GetMaxAccounts = 0;
            }
            else
            {
                // TODO Get_Fields: Field [MaxAccount] not found!! (maybe it is an alias?)
                GetMaxAccounts = rsTemp.Get_Fields("MaxAccount");
            }
            rsTemp = null;
            return GetMaxAccounts;
        }
        // vbPorter upgrade warning: intpic As short	OnWriteFCConvert.ToInt32(
        public static void NewRecordPicture_2(int lngMrAccount, string strdoclocation, int intpic)
        {
            NewRecordPicture(ref lngMrAccount, ref strdoclocation, ref intpic);
        }

        public static void NewRecordPicture(ref int lngMrAccount, ref string strdoclocation, ref int intpic)
        {
            int lnglength;
            try
            {
                // On Error GoTo ErrorHandler
                // If TrioDATA <> CurDir Then
                // ChDrive Mid(TrioDATA, 1, 1)
                // ChDir TrioDATA
                // End If
                modDataTypes.Statics.PIC.AddNew();
                modDataTypes.Statics.PIC.Set_Fields("mraccountnumber", lngMrAccount);
                modDataTypes.Statics.PIC.Set_Fields("picturelocation", strdoclocation);
                modDataTypes.Statics.PIC.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.PIC.Set_Fields("picnum", intpic);
                modDataTypes.Statics.PIC.Update();
                // 7    Call PIC.FindFirstRecord("picnum", intpic)
                modDataTypes.Statics.PIC.FindFirst("picnum = " + FCConvert.ToString(intpic));
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " of NewRecordPicture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        //public static void RecordPicture(ref int lngMrAccount, ref string strdoclocation, ref short intpic)
        //{
        //    int lnglength;
        //    // strDocLocation = Trim(strDocLocation)
        //    // strDocLocation = RIGHT(strDocLocation, Len(strDocLocation) - (InStrRev(strDocLocation, "\", -1)))
        //    if (modGNBas.Statics.TrioDATA != FCFileSystem.Statics.UserDataFolder)
        //    {
        //        //ChDrive(Strings.Mid(modGNBas.TrioDATA, 1, 1));
        //        //Environment.CurrentDirectory = modGNBas.TrioDATA;
        //    }
        //    // If frmREProperty2.ActiveControl.Name = frmREProperty2.cmdPic1.Name Then
        //    // intpic = 1
        //    // ElseIf frmREProperty2.ActiveControl.Name = frmREProperty2.cmdPic2.Name Then
        //    // intpic = 2
        //    // ElseIf frmREProperty2.ActiveControl.Name = frmREProperty2.cmdPic3.Name Then
        //    // intpic = 3
        //    // End If
        //    clsDRWrapper temp = modDataTypes.Statics.PIC;
        //    modREMain.OpenPICTable(ref temp, lngMrAccount, modGNBas.Statics.gintCardNumber, intpic);
        //    object lngmraccount = lngMrAccount;
        //    short intLength = 6;
        //    temp.Set_Fields("mraccountnumber", PadToString_8(ref lngmraccount, ref intLength));
        //    lngMrAccount = FCConvert.ToInt32(lngmraccount);
        //    temp.Set_Fields("PictureLocation", strdoclocation);
        //    temp.Set_Fields("endofrecord", "~~");
        //    temp.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
        //    temp.Set_Fields("picnum", intpic);
        //    // Response = lngLength
        //    // Put #66, lngLength, PIC
        //    modREMain.SavePICTable(ref temp, lngMrAccount, modGNBas.Statics.gintCardNumber, intpic);
        //    modDataTypes.Statics.PIC = temp;
        //    // Close #66
        //}

        public static void FillMortgageHolderInfo()
        {
        }

        public static void GetOpens()
        {
        }
        // vbPorter upgrade warning: intAccounttoDelete As int	OnRead(string)
        public static void UndeleteMasterRecord(int intAccounttoDelete = 0, int intAID = 0)
        {
            try
            {
                // On Error GoTo ErrorHandler
                // vbPorter upgrade warning: ans As string	OnWrite(string, int)
                string ans = "";
                clsDRWrapper clsTemp = new clsDRWrapper();
                clsDRWrapper dbTemp = new clsDRWrapper();
                if (intAccounttoDelete == 0)
                {
                    ans = Interaction.InputBox("Please enter the account number to undelete", "Undelete Master Record", null/*, -1, -1*/);
                }
                else
                {
                    ans = FCConvert.ToString(intAccounttoDelete);
                }
                if (ans != "")
                {
                    if (Conversion.Val(ans) != 0)
                    {
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            clsTemp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(ans)) + " and rscard = 1", modGlobalVariables.strREDatabase);
                            if (!clsTemp.EndOfFile())
                            {
                                if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("AccountLocked")))
                                {
                                    MessageBox.Show("The account you have selected is a deleted account and cannot be undeleted because it is locked" + "\r\n" + "\r\n" + "Reason locked: " + clsTemp.Get_Fields_String("ReasonAccountLocked"), "Deleted and Locked", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                            dbTemp.Execute("Update Master Set rsdeleted = 0 where RSAccount = " + ans, "RealEstate");
                            MessageBox.Show("Account #" + ans + " is active.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // dbTemp.Execute ("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Undelete','Undeleted under file maintenance menu account " & Ans & "')")
                            modGlobalFunctions.AddCYAEntry_26("RE", "Undelete", "Undeleted under file maintenance menu account " + ans);
                        }
                        else
                        {
                            if (intAID == 0)
                            {
                                modGlobalVariables.Statics.gRstemp.OpenRecordset("select * from srmaster where rsaccount = " + ans, "RealEstate");
                                if (!modGlobalVariables.Statics.gRstemp.EndOfFile())
                                {
                                    modGlobalVariables.Statics.gRstemp.MoveLast();
                                    modGlobalVariables.Statics.gRstemp.MoveFirst();
                                    if (modGlobalVariables.Statics.gRstemp.RecordCount() > 0)
                                    {
                                        modGlobalVariables.Statics.gRstemp.Edit();
                                        modGlobalVariables.Statics.CancelledIt = false;
                                        if (modGlobalVariables.Statics.gRstemp.RecordCount() > 1)
                                        {
                                            FrmChooseSR.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                                        }
                                        if (!modGlobalVariables.Statics.CancelledIt)
                                        {
                                            if (FCConvert.ToInt32(modGlobalVariables.Statics.gRstemp.Get_Fields_Boolean("rsdeleted")) == 0)
                                            {
                                                MessageBox.Show("This account wasn't deleted.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return;
                                            }
                                            else if (modGlobalVariables.Statics.gRstemp.Get_Fields_Boolean("AccountLocked"))
                                            {
                                                MessageBox.Show("The account you have selected is a deleted account and cannot be undeleted because it is locked" + "\r\n" + "\r\n" + "Reason locked: " + modGlobalVariables.Statics.gRstemp.Get_Fields_String("ReasonAccountLocked"), "Deleted and Locked", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            }
                                            modGlobalVariables.Statics.gRstemp.Set_Fields("rsdeleted", false);
                                            modGlobalVariables.Statics.gRstemp.Update();
                                            // dbTemp.Execute ("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Undelete','Undeleted under file maintenance menu sale account " & Ans & "')")
                                            modGlobalFunctions.AddCYAEntry_26("RE", "Undelete", "Undeleted under file maintenance menu sale account " + ans);
                                            MessageBox.Show("Account #" + ans + " is active.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                modGlobalVariables.Statics.gRstemp.OpenRecordset("select * from srmaster where id = " + FCConvert.ToString(intAID), "RealEstate");
                                if (FCConvert.ToBoolean(modGlobalVariables.Statics.gRstemp.Get_Fields_Boolean("AccountLocked")))
                                {
                                    MessageBox.Show("The account you have selected is a deleted account and cannot be undeleted because it is locked" + "\r\n" + "\r\n" + "Reason locked: " + modDataTypes.Statics.MR3.Get_Fields_String("ReasonAccountLocked"), "Deleted and Locked", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                                modGlobalVariables.Statics.gRstemp.Edit();
                                modGlobalVariables.Statics.gRstemp.Set_Fields("rsdeleted", false);
                                modGlobalVariables.Statics.gRstemp.Update();
                                // dbTemp.Execute ("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Undelete','Undeleted under file maintenance menu sale account " & Ans & "')")
                                modGlobalFunctions.AddCYAEntry_26("RE", "Undelete", "Undeleted under file maintenance menu sale account " + ans);
                                MessageBox.Show("account #" + ans + " has been reactivated.", "Account Un-Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
                else
                {
                    // MDIParent.Grid.SetFocus
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In UndeleteMasterRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void transfervaluestobillingamounts()
        {
            // copies correlated figures to billing figures
            int NumRecords;
            clsDRWrapper clsSave = new clsDRWrapper();
            // Load frmREWait
            // frmREWait.lblMessage = "Transferring to Billing Values Please Wait."
            frmWait.InstancePtr.Init("Committing to Billing Values Please Wait.", false, 100, true);
            // Frmtransfervals.initialize
            // Frmtransfervals.Caption = "Transfer Billing Values"
            // Frmtransfervals.Label2.Caption = "Transferring billing values, Please Wait."
            //Application.DoEvents();
            // Call clsSave.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_OpID & "',#" & Now & "#,'Transferred to Billing Amounts','Transferred to Billing Amounts')", strredatabase)
            modGlobalFunctions.AddCYAEntry_26("RE", "Commit to Billing Amounts", "Commit to Billing Amounts");
            clsSave.Execute("update master set lastlandval = rllandval", modGlobalVariables.strREDatabase);
            //Application.DoEvents();
            clsSave.Execute("update master set lastbldgval = rlbldgval", modGlobalVariables.strREDatabase);
            //Application.DoEvents();
            clsSave.Execute("update status set xferbillingvalues = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
            // 
            frmWait.InstancePtr.Unload();
            MessageBox.Show("Done Committing Billing Values", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public static bool Formisloaded_2(string FormName)
        {
            return Formisloaded(ref FormName);
        }

        public static bool Formisloaded(ref string FormName)
        {
            bool Formisloaded = false;
            //Form frm = null;
            foreach (Form frm in FCGlobal.Statics.Forms)
            {
                if (Strings.UCase(frm.Name) == Strings.UCase(FormName))
                {
                    Formisloaded = true;
                    return Formisloaded;
                }
            }
            return Formisloaded;
        }

        public static void WhichSR(ref clsDRWrapper rsTemp, ref short intAccount, object vSaleDate = null)
        {
            if (!Information.IsNothing(vSaleDate))
            {
                rsTemp.OpenRecordset("Select * from Srmaster where rsaccount = " + FCConvert.ToString(intAccount) + " and saledate = '" + vSaleDate + "'", "RealEstate");
            }
            else
            {
                rsTemp.OpenRecordset("select * from srmaster where rsaccount = " + FCConvert.ToString(intAccount), "RealEstate");
            }
        }

        public static void CopyDataConnection(ref clsDRWrapper clsSource, ref clsDRWrapper clsDest)
        {
            try
            {
                string fld = "";
                int x;
                clsDest.RunSilent = true;
                clsSource.RunSilent = true;
                for (x = 0; x <= clsSource.FieldsCount - 1; x++)
                {
                    fld = clsSource.Get_FieldsIndexName(x);
                    if (Strings.UCase(fld) != "ID")
                    {
                        /*? On Error Resume Next  */
                        if (clsDest.ContainsField(fld))
                        {
                            clsDest.Set_Fields(fld, clsSource.Get_Fields(fld));
                            if (Information.Err().Number != 0)
                            {
                                if (Information.Err().Number == 3421)
                                {
                                    if (clsSource.Get_Fields(fld) == string.Empty)
                                    {
                                        clsDest.Set_Fields(fld, 0);
                                    }
                                    else
                                    {
                                        //goto ErrorHandler;
                                    }
                                }
                                else
                                {
                                    if (Information.Err().Number != 0)
                                    {
                                        //goto ErrorHandler;
                                    }
                                }
                            }
                        }
                    }
                }
                // x
                return;
            }
            catch (Exception ex)
            {
            ErrorHandler:
                ;
                if (Information.Err(ex).Number == 0)
                    return;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Copy Data Connection", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void CopyRecordSet(ref clsDRWrapper RSSource, ref clsDRWrapper rsTemp)
        {
            /*? On Error Resume Next  */
            try
            {
                // ignore if one recordset is missing some of the fields
                int x;
                bool boolTemp;
                boolTemp = rsTemp.RunSilent;
                rsTemp.RunSilent = true;
                for (x = 0; x <= RSSource.FieldsCount - 1; x++)
                {
                    if (Strings.LCase(RSSource.Get_FieldsIndexName(x)) != "id" && rsTemp.ContainsField(RSSource.Get_FieldsIndexName(x)))
                    {
                        rsTemp.Set_Fields(RSSource.Get_FieldsIndexName(x), RSSource.Get_Fields(RSSource.Get_FieldsIndexName(x)));
                    }
                }
                // x
                rsTemp.RunSilent = false;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CopyRecordSet", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void CopyOwnerInfo(ref clsDRWrapper RSSource, ref clsDRWrapper RSDest)
        {
            RSDest.Set_Fields("RSNAME", RSSource.Get_Fields_String("RSNAME"));
            RSDest.Set_Fields("rsaddr1", RSSource.Get_Fields_String("rsaddr1"));
            RSDest.Set_Fields("rsaddr2", RSSource.Get_Fields_String("rsaddr2"));
            RSDest.Set_Fields("rsaddr3", RSSource.Get_Fields_String("rsaddr3"));
            RSDest.Set_Fields("rsstate", RSSource.Get_Fields_String("rsstate"));
            RSDest.Set_Fields("rszip", RSSource.Get_Fields_String("rszip"));
            RSDest.Set_Fields("rszip4", RSSource.Get_Fields_String("rszip4"));
        }

        public static void CopyMasterInfo(ref clsDRWrapper RSSource, ref clsDRWrapper RSDest)
        {
            RSDest.Set_Fields("OwnerPartyID", RSSource.Get_Fields_Int32("OwnerPartyID"));
            RSDest.Set_Fields("SecOwnerPartyID", RSSource.Get_Fields_Int32("SecOwnerPartyID"));
            // RSDest.Fields("rsaddr1") = RSSource.Fields("rsaddr1")
            // RSDest.Fields("rsaddr2") = RSSource.Fields("rsaddr2")
            // RSDest.Fields("rsaddr3") = RSSource.Fields("rsaddr3")
            // RSDest.Fields("rsstate") = RSSource.Fields("rsstate")
            // RSDest.Fields("rszip") = RSSource.Fields("rszip")
            // RSDest.Fields("rszip4") = RSSource.Fields("rszip4")
            // RSDest.Fields("rstelephone") = RSSource.Fields("rstelephone")
            RSDest.Set_Fields("rslocnumalph", RSSource.Get_Fields_String("rslocnumalph"));
            RSDest.Set_Fields("rslocapt", RSSource.Get_Fields_String("rslocapt"));
            RSDest.Set_Fields("rslocstreet", RSSource.Get_Fields_String("rslocstreet"));
            RSDest.Set_Fields("rsref1", RSSource.Get_Fields_String("rsref1"));
            RSDest.Set_Fields("rsref2", RSSource.Get_Fields_String("rsref2"));
            RSDest.Set_Fields("rsMapLot", RSSource.Get_Fields_String("rsMapLot"));
            RSDest.Set_Fields("pineighborhood", RSSource.Get_Fields_Int32("pineighborhood"));
            RSDest.Set_Fields("pizone", RSSource.Get_Fields_Int32("pizone"));
            RSDest.Set_Fields("piseczone", RSSource.Get_Fields_Int32("piseczone"));
            RSDest.Set_Fields("piopen1", RSSource.Get_Fields_Int32("piopen1"));
            RSDest.Set_Fields("piopen2", RSSource.Get_Fields_Int32("piopen2"));
            RSDest.Set_Fields("pistreetcode", Conversion.Val(RSSource.Get_Fields_Int32("pistreetcode")));
            RSDest.Set_Fields("pistreet", RSSource.Get_Fields_Int32("pistreet"));
            RSDest.Set_Fields("piutilities1", RSSource.Get_Fields_Int32("piutilities1"));
            RSDest.Set_Fields("piutilities2", RSSource.Get_Fields_Int32("piutilities2"));
            // TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
            RSDest.Set_Fields("pixcoord", RSSource.Get_Fields("pixcoord"));
            // TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
            RSDest.Set_Fields("piycoord", RSSource.Get_Fields("piycoord"));
            RSDest.Set_Fields("pitopography1", RSSource.Get_Fields_Int32("pitopography1"));
            RSDest.Set_Fields("pitopography2", RSSource.Get_Fields_Int32("pitopography2"));
            RSDest.Set_Fields("ritrancode", Conversion.Val(RSSource.Get_Fields_Int32("ritrancode") + ""));
            if (!modGlobalVariables.Statics.boolRegRecords)
            {
                RSDest.Set_Fields("saledate", RSSource.Get_Fields_DateTime("saledate"));
                RSDest.Set_Fields("saleid", Conversion.Val(RSSource.Get_Fields_Int32("saleid") + ""));
                RSDest.Set_Fields("pisaleprice", Conversion.Val(RSSource.Get_Fields_Int32("pisaleprice") + ""));
                RSDest.Set_Fields("pisaleverified", Conversion.Val(RSSource.Get_Fields_Int32("pisaleverified") + ""));
                RSDest.Set_Fields("pisalevalidity", Conversion.Val(RSSource.Get_Fields_Int32("pisalevalidity") + ""));
                RSDest.Set_Fields("PISALEfinancing", Conversion.Val(RSSource.Get_Fields_Int32("pisalefinancing") + ""));
                RSDest.Set_Fields("pisaletype", Conversion.Val(RSSource.Get_Fields_Int32("pisaletype") + ""));
            }
        }

        public static void FixTranCodes()
        {
            clsDRWrapper clsExecute = new clsDRWrapper();
            clsDRWrapper clsLoad = new clsDRWrapper();
            // makes all cards for an account have the same trancode
            clsLoad.OpenRecordset("select rsaccount,ritrancode from master where rscard = 1 and rsaccount in (select rsaccount from master where rscard = 2)", modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                //Application.DoEvents();
                clsExecute.Execute("update master set ritrancode = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ritrancode"))) + " where rsaccount = " + clsLoad.Get_Fields_Int32("rsaccount") + " and rscard > 1 ", modGlobalVariables.strREDatabase);
                clsLoad.MoveNext();
            }
        }

        public static bool Get_Cost_Record(ref short recnum, string strCat = "", string strType = "")
        {
            bool Get_Cost_Record = false;
            clsDRWrapper clsLoad = new clsDRWrapper();
            // returns false if the code doesn't exist
            Get_Cost_Record = true;
            if (strType == "")
            {
                if (modGlobalVariables.Statics.boolUseArrays)
                {
                    modGlobalVariables.Statics.CostRec.CBase = modGlobalVariables.Statics.CostArray[recnum].CBase + "";
                    modGlobalVariables.Statics.CostRec.ClDesc = modGlobalVariables.Statics.CostArray[recnum].ClDesc + "";
                    modGlobalVariables.Statics.CostRec.CSDesc = modGlobalVariables.Statics.CostArray[recnum].CSDesc + "";
                    modGlobalVariables.Statics.CostRec.CUnit = modGlobalVariables.Statics.CostArray[recnum].CUnit + "";
                }
                else
                {
                    clsDRWrapper temp = modDataTypes.Statics.CR;
                    modREMain.OpenCRTable(ref temp, recnum);
                    modDataTypes.Statics.CR = temp;
                    modGlobalVariables.Statics.CostRec.CBase = modDataTypes.Statics.CR.Get_Fields_String("cbase") + "";
                    modGlobalVariables.Statics.CostRec.ClDesc = modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "";
                    modGlobalVariables.Statics.CostRec.CSDesc = modDataTypes.Statics.CR.Get_Fields_String("csdesc") + "";
                    modGlobalVariables.Statics.CostRec.CUnit = modDataTypes.Statics.CR.Get_Fields_String("cunit") + "";
                }
            }
            else
            {
                if (modGlobalVariables.Statics.boolUseArrays)
                {
                    if (Strings.UCase(strCat) == "DWELLING")
                    {
                        // check dwelling cost recordset for type,recnum
                        // If rsCostRec.FindFirstRecord(, , "code = " & recnum & " and category = '" & strCat & "' and type = '" & strType & "'") Then
                        // If rsCostRec.FindFirstRecord2("code,category,type", recnum & "," & strCat & "," & strType, ",") Then
                        if (modGlobalVariables.Statics.rsCostRec.FindFirst("code = " + FCConvert.ToString(recnum) + " and category = '" + strCat + "' and type = '" + strType + "'"))
                        {
                            // TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
                            modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.rsCostRec.Get_Fields("base")));
                            modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(modGlobalVariables.Statics.rsCostRec.Get_Fields_String("description"));
                            modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(modGlobalVariables.Statics.rsCostRec.Get_Fields_String("shortdescription"));
                            // TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
                            modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.rsCostRec.Get_Fields("unit")));
                        }
                        else
                        {
                            modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(99999999);
                            modGlobalVariables.Statics.CostRec.ClDesc = "";
                            modGlobalVariables.Statics.CostRec.CSDesc = "";
                            modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(99999999);
                        }
                    }
                    else if (Strings.UCase(strCat) == "PROPERTY")
                    {
                        // If rsCostRec.FindFirstRecord(, , "code = " & recnum & " and category = '" & strCat & "' and type = '" & strType & "'") Then
                        // If rsCostRec.FindFirstRecord2("code,category,type", recnum & "," & strCat & "," & strType, ",") Then
                        if (modGlobalVariables.Statics.rsCostRec.FindFirst("code = " + FCConvert.ToString(recnum) + " and category = '" + strCat + "' and type = '" + strType + "'"))
                        {
                            // TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
                            modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.rsCostRec.Get_Fields("base")));
                            modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(modGlobalVariables.Statics.rsCostRec.Get_Fields_String("description"));
                            modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(modGlobalVariables.Statics.rsCostRec.Get_Fields_String("shortdescription"));
                            // TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
                            modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.rsCostRec.Get_Fields("unit")));
                        }
                        else
                        {
                            modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(99999999);
                            modGlobalVariables.Statics.CostRec.ClDesc = "";
                            modGlobalVariables.Statics.CostRec.CSDesc = "";
                            modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(99999999);
                        }
                    }
                }
                else
                {
                    if (Strings.UCase(strCat) == "DWELLING")
                    {
                        if (Strings.UCase(strType) == "STYLE")
                        {
                            clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription, 0 as base from dwellingstyle where code = " + FCConvert.ToString(recnum), modGlobalVariables.strREDatabase);
                        }
                        else if (Strings.UCase(strType) == "HEAT")
                        {
                            clsLoad.OpenRecordset("select code,unit,base,description,shortdescription from dwellingheat where code = " + FCConvert.ToString(recnum), modGlobalVariables.strREDatabase);
                        }
                        else if (Strings.UCase(strType) == "EXTERIOR")
                        {
                            clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription,0 as base from dwellingexterior where code = " + FCConvert.ToString(recnum), modGlobalVariables.strREDatabase);
                        }
                    }
                    else if (Strings.UCase(strCat) == "PROPERTY")
                    {
                        if (Strings.UCase(strType) == "LANDTYPE")
                        {
                            clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription,0 as base from landtype where code = " + FCConvert.ToString(recnum), modGlobalVariables.strREDatabase);
                        }
                    }
                    if (!clsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
                        modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("base")));
                        modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                        modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(clsLoad.Get_Fields_String("shortdescription"));
                        // TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
                        modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("unit")));
                    }
                    else
                    {
                        Get_Cost_Record = false;
                        modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(0);
                        modGlobalVariables.Statics.CostRec.ClDesc = "";
                        modGlobalVariables.Statics.CostRec.CSDesc = "";
                        modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(0);
                    }
                }
            }
            return Get_Cost_Record;
        }
        // vbPorter upgrade warning: recnum As short	OnWriteFCConvert.ToInt32(
        public static void get_Land_Schedule_Rec(ref int recnum)
        {
            int x;
            int RecNumToUse;
            int intLimit = 0;
            RecNumToUse = recnum;
            if (recnum > 0)
            {
                if (modGlobalVariables.Statics.boolUseArrays)
                {
                    // If recnum >= 31 Then RecNumToUse = recnum + 5
                    intLimit = modGlobalVariables.Statics.LandSArray[RecNumToUse].MaxTypes;
                    for (x = 1; x <= intLimit; x++)
                    {
                        modGlobalVariables.Statics.LandSRec.putrate(x, FCConvert.ToString(modGlobalVariables.Statics.LandSArray[RecNumToUse].GetRate(x)));
                        // End If
                    }
                    // x
                    modGlobalVariables.Statics.LandSRec.StandardDepth = modGlobalVariables.Statics.LandSArray[RecNumToUse].StandardDepth;
                    modGlobalVariables.Statics.LandSRec.StandardLot = modGlobalVariables.Statics.LandSArray[RecNumToUse].StandardLot;
                    modGlobalVariables.Statics.LandSRec.StandardWidth = modGlobalVariables.Statics.LandSArray[RecNumToUse].StandardWidth;
                }
                else
                {
                    clsDRWrapper clsLoad = new clsDRWrapper();
                    clsLoad.OpenRecordset("SELECT * FROM LANDSCHEDULE where schedule = " + FCConvert.ToString(recnum) + "  order by code", modGlobalVariables.strREDatabase);
                    while (!clsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        if (clsLoad.Get_Fields("code") > 0)
                        {
                            // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                            if (clsLoad.Get_Fields("code") > 10)
                            {
                                //FC:FINAL:MSH - i.issue #1059: incorrect input params for function call
                                //modGlobalVariables.Statics.LandSRec.putrate(clsLoad.Get_Fields("code") - 10, clsLoad.Get_Fields("AMOUNT"));
                                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [AMOUNT] and replace with corresponding Get_Field method
                                modGlobalVariables.Statics.LandSRec.putrate(FCConvert.ToInt32(clsLoad.Get_Fields("code") - 10), FCConvert.ToString(clsLoad.Get_Fields("AMOUNT")));
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                            object vbPorterVar = clsLoad.Get_Fields("code");
                            if (FCConvert.ToInt32(vbPorterVar) == modREConstants.CNSTLANDSCHEDULESTANDARDDEPTH)
                            {
                                // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                                modGlobalVariables.Statics.LandSRec.StandardDepth = clsLoad.Get_Fields("amount");
                            }
                            else if (FCConvert.ToInt32(vbPorterVar) == modREConstants.CNSTLANDSCHEDULESTANDARDLOT)
                            {
                                // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                                modGlobalVariables.Statics.LandSRec.StandardLot = clsLoad.Get_Fields("amount");
                            }
                            else if (FCConvert.ToInt32(vbPorterVar) == modREConstants.CNSTLANDSCHEDULESTANDARDWIDTH)
                            {
                                // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                                modGlobalVariables.Statics.LandSRec.StandardWidth = clsLoad.Get_Fields("amount");
                            }
                        }
                        clsLoad.MoveNext();
                    }
                }
            }
        }

        public static void FillLandSArray()
        {
            int x = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            // Dim rsTemp As clsDRWrapper
            // Dim dbTemp As DAO.Database
            // vbPorter upgrade warning: reccount As short --> As int	OnWriteFCConvert.ToDouble(
            int reccount;
            // vbPorter upgrade warning: dblPerc As double	OnReadFCConvert.ToInt32(
            double dblPerc = 0;
            // vbPorter upgrade warning: intPerc As short --> As int	OnWriteFCConvert.ToDouble(
            int intPerc = 0;
            int intTypes;
            int intCount;
            int lngLastSched;

            try
            {
                // On Error GoTo ErrorHandler
                modGlobalVariables.Statics.boolLandSChanged = false;
                modProperty.Statics.gintCurrentLandSchedule = 0;

                // since it changed, make the exponents reload also
                clsLoad.OpenRecordset("select count(CODE) as numtypes from landtype", modGlobalVariables.strREDatabase);
                intTypes = 0;

                if (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Field [numtypes] not found!! (maybe it is an alias?)
                    intTypes = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("numtypes"))));
                }

                intCount = 0;
                clsLoad.OpenRecordset("select * from landschedule order by schedule,code", modGlobalVariables.strREDatabase);
                reccount = FCConvert.ToInt32(FCConvert.ToDouble(clsLoad.RecordCount()) / (intTypes + 2));

                // Set dbTemp = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
                // Set rsTemp = dbTemp.OpenRecordset("select * from landsrecord order by lrecordnumber",strredatabase)
                // rsTemp.MoveLast
                // reccount = rsTemp.RecordCount
                // rsTemp.MoveFirst
                //FC:FINAL:RPU:#i1191 - Show the form modeless
                //Frmassessmentprogress.InstancePtr.Show();
                Frmassessmentprogress.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
                Frmassessmentprogress.InstancePtr.ProgressBar1.Value = 1;
                Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = reccount + 1;
                Frmassessmentprogress.InstancePtr.Label2.Text = "Pre-Loading Land Schedule for Calculation";
                Frmassessmentprogress.InstancePtr.Text = "Load Land Schedule Record";
                Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = false;

                // For x = 0 To 4600
                lngLastSched = 0;
                int rem;

                while (!clsLoad.EndOfFile())
                {
                    x = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("schedule"));

                    if (lngLastSched != x)
                    {
                        Array.Resize(ref modGlobalVariables.Statics.LandSArray, x + 1);
                        modGlobalVariables.Statics.LandSArray[x] = new clsLandScheduleRecord();
                        lngLastSched = x;
                    }

                    // update progress bar every 100 records
                    Math.DivRem(clsLoad.AbsolutePosition(), 100, out rem);

                    if (rem == 0)
                    {
                        dblPerc = FCConvert.ToDouble(clsLoad.AbsolutePosition()) / (intTypes + 2);
                        if (dblPerc > 100) dblPerc = 100;
                        intPerc = FCConvert.ToInt32(dblPerc);
                        Frmassessmentprogress.InstancePtr.ProgressBar1.Value = intPerc;
                        Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(dblPerc / reccount * 100, "##0.0") + "%";
                        Frmassessmentprogress.InstancePtr.BringToFront();

                        FCUtils.ApplicationUpdate(Frmassessmentprogress.InstancePtr);
                    }

                    double amt = FCConvert.ToDouble(clsLoad.Get_Fields("amount"));
                    int code = FCConvert.ToInt32(clsLoad.Get_Fields("code"));

                    if (code > 0)
                    {
                        if (code > 10)
                        {
                            modGlobalVariables.Statics.LandSArray[x]
                                              .putrate(code - 10, FCConvert.ToString(amt));
                        }
                    }
                    else
                    {
                        switch (code)
                        {
                            case modREConstants.CNSTLANDSCHEDULESTANDARDDEPTH:
                                modGlobalVariables.Statics.LandSArray[x]
                                                  .StandardDepth = amt;

                                break;
                            case modREConstants.CNSTLANDSCHEDULESTANDARDLOT:
                                modGlobalVariables.Statics.LandSArray[x]
                                                  .StandardLot = amt;

                                break;
                            case modREConstants.CNSTLANDSCHEDULESTANDARDWIDTH:
                                modGlobalVariables.Statics.LandSArray[x]
                                                  .StandardWidth = amt;

                                break;
                        }
                    }

                    clsLoad.MoveNext();
                }

                // 
                Frmassessmentprogress.InstancePtr.Unload();

                //Application.DoEvents();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number "
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + "  "
                                + Information.Err(ex)
                                             .Description
                                + "\r\n"
                                + "In FillLandSArray", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsLoad.DisposeOf();
            }
        }

        public static void FillRSZArray()
        {
            // Dim dbTemp As DAO.Database
            clsDRWrapper rsTemp = new clsDRWrapper();
            modGlobalVariables.Statics.boolRSZChanged = false;
            rsTemp.OpenRecordset("select * from rszrecord ", modGlobalVariables.strREDatabase);
            if (!rsTemp.EndOfFile())
            {
                int x;
                for (x = 1; x <= 25; x++)
                {
                    // TODO Get_Fields: Field [rszrge] not found!! (maybe it is an alias?)
                    modGlobalVariables.Statics.RSZRange[x] = FCConvert.ToString(rsTemp.Get_Fields("rszrge" + Strings.Format(x, "00")));
                    // TODO Get_Fields: Field [rszfct] not found!! (maybe it is an alias?)
                    modGlobalVariables.Statics.RSZFact[x] = FCConvert.ToString(rsTemp.Get_Fields("rszfct" + Strings.Format(x, "00")));
                }
                // x
            }
        }

        public static void Check_Arrays()
        {
            modGlobalVariables.Statics.LandTypes.Init();
            if (modGlobalVariables.Statics.boolUseArrays)
            {
                if (modGlobalVariables.Statics.boolCostRecChanged || modGlobalVariables.Statics.boolComCostChanged)
                {
                    // Call FillCostArray
                    modGlobalVariables.Statics.clsCostRecords.InitCostFiles();
                    modGlobalVariables.Statics.boolCostRecChanged = false;
                    modGlobalVariables.Statics.boolComCostChanged = false;
                }
                // If boolComCostChanged Then
                // Call Fill_Com_Cost_Array
                // End If
                if (modGlobalVariables.Statics.boolLandTChanged)
                {
                    modGlobalVariables.Statics.LandTRec.SetupArrays(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber, true);
                }
                if (modGlobalVariables.Statics.boolLandSChanged)
                {
                    FillLandSArray();
                    // Call fillolsflarray
                }
                if (modGlobalVariables.Statics.boolRSZChanged)
                {
                    FillRSZArray();
                }
                modSpeedCalc.Fill_Some_Arrays_8(31, true);
            }
            else
            {
                // Call Fill_Some_Arrays(31, True)
                // Call OpenLSRTable(LSR, 31)
                modProperty.FillArrays();
            }
        }

        public static void Check_Some_Arrays()
        {
            if (modGlobalVariables.Statics.boolLandSChanged)
            {
                FillLandSArray();
                // Call fillolsflarray
            }
            if (modGlobalVariables.Statics.boolRSZChanged)
            {
                FillRSZArray();
            }
        }
        //
        // Public Sub ChangeTheFont(strprinter As String, strport As String, strpname As String, stroutput As String)
        // Printer.Port = strport
        // Dim x As Printer
        //
        // For Each x In Printers
        // If x.Port = strport Then
        // If x.DeviceName = strpname Then
        // Set Printer = x
        // End If
        // End If
        // Next
        // Printer.FontName = "courier new"
        // Printer.FontSize = 10
        // Printer.Height = 1440  'one inch
        // Printer.Width = 1440 * 4  '4 inches
        //
        //
        // Printer.Print stroutput
        // Printer.Print "howdy" & vbNewLine
        // Printer.CurrentY = Printer.CurrentY - 50
        // Printer.Print "hello there" & vbNewLine
        // Printer.EndDoc
        // End Sub
        public static string escapequote_2(string a)
        {
            return escapequote(ref a);
        }

        public static string escapequote(ref string a)
        {
            string escapequote = "";
            // searches for a singlequote and puts another next to it
            string tstring = "";
            string t2string = "";
            int stringindex;
            int intpos = 0;
            escapequote = a;
            if (a == string.Empty)
                return escapequote;
            stringindex = 1;
            while (stringindex <= a.Length)
            {
                intpos = Strings.InStr(stringindex, a, "'", CompareConstants.vbBinaryCompare);
                if (intpos == 0)
                    break;
                tstring = Strings.Mid(a, 1, intpos);
                t2string = Strings.Mid(a, intpos + 1);
                if (intpos == a.Length)
                {
                    // a = tstring & "' "
                    a = Strings.Mid(a, 1, intpos - 1);
                    break;
                }
                else
                {
                    a = tstring + "'" + t2string;
                }
                stringindex = intpos + 2;
            }
            // stringindex = 1
            // Do While stringindex <= Len(a)
            // intpos = InStr(stringindex, a, Chr(34))
            // If intpos = 0 Then Exit Do
            // tstring = Mid(a, 1, intpos)
            // t2string = Mid(a, intpos + 1)
            // If intpos = Len(a) Then
            // a = Mid(a, 1, intpos - 1)
            // Exit Do
            // End If
            // a = tstring & Chr(34) & t2string
            // stringindex = intpos + 2
            // Loop
            escapequote = a;
            return escapequote;
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public static bool CompareTheDates_24(string strDB, string strTBL, string strFld)
        {
            return CompareTheDates(ref strDB, ref strTBL, ref strFld);
        }

        public static bool CompareTheDates(ref string strDB, ref string strTBL, ref string strFld)
        {
            bool CompareTheDates = false;
            DateTime tDate;
            clsDRWrapper clsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                clsTemp.OpenRecordset("Select " + strFld + " from " + strTBL, strDB);
                if (Information.IsDate(clsTemp.Get_Fields(strFld)))
                {
                    tDate = (DateTime)clsTemp.Get_Fields(strFld);
                    if (tDate.ToOADate() <= DateTime.Today.ToOADate())
                    {
                        CompareTheDates = false;
                    }
                    else
                    {
                        CompareTheDates = true;
                    }
                }
                else
                {
                    CompareTheDates = false;
                }
                return CompareTheDates;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error no. " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CompareTheDates;
        }

        public static void VerifySummTable()
        {
            // gets rid of any summary records that don't have a matching card in master
            clsDRWrapper clsLoad = new clsDRWrapper();
            clsDRWrapper clsM = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                //Application.DoEvents();
                clsM.OpenRecordset("select rsaccount,rscard from master where not rsdeleted = 1 order by rsaccount,rscard", modGlobalVariables.strREDatabase);
                if (clsM.EndOfFile())
                {
                    clsLoad.Execute("delete from summrecord", modGlobalVariables.strREDatabase);
                    return;
                }
                clsLoad.OpenRecordset("select * from summrecord order by srecordnumber,cardnumber", modGlobalVariables.strREDatabase);
                // vbPorter upgrade warning: aryFields As Variant --> As string
                string[] aryFields = new string[1 + 1];
                aryFields[0] = "rsaccount";
                aryFields[1] = "rscard";
                object[] aryValues = new object[1 + 1];
                while (!clsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    if (clsM.EndOfFile())
                    {
                        clsLoad.Delete();
                        clsLoad.Update();
                    }
                    else
                    {
                        if (!((clsM.Get_Fields_Int32("rsaccount") == clsLoad.Get_Fields_Int32("srecordnumber")) && (clsM.Get_Fields_Int32("rscard") == clsLoad.Get_Fields_Int32("cardnumber"))))
                        {
                            // If Not clsM.FindNextRecord(, , "rsaccount = " & clsLoad.Fields("srecordnumber") & " and rscard = " & clsLoad.Fields("cardnumber")) Then
                            // If Not clsM.FindNextRecord2("rsaccount,rscard", clsLoad.Fields("srecordnumber") & "," & clsLoad.Fields("cardnumber")) Then
                            aryValues[0] = clsLoad.Get_Fields_Int32("srecordnumber");
                            aryValues[1] = clsLoad.Get_Fields_Int32("cardnumber");
                            if (!clsM.FindNextRecord2(aryFields, aryValues))
                            {
                                clsLoad.Delete();
                                clsLoad.Update();
                            }
                        }
                    }
                    clsLoad.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In VerifySummTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: SomeNum As Variant --> As int
        public static bool Even(int SomeNum)
        {
            bool Even = false;
            float sngTemp;
            int lngTemp;
            if ((FCConvert.ToSingle(SomeNum) / 2) - FCConvert.ToInt32(SomeNum / 2.0) != 0)
            {
                Even = false;
            }
            else
            {
                Even = true;
            }
            return Even;
        }
        // vbPorter upgrade warning: intValid As short	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: intVerified As short	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: intSaleType As short	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: intFinancing As short	OnWriteFCConvert.ToInt32(
        public static void SaveSaleRecord_2(bool boolOverWrite, int lngAcctNum, int lngTempSaleID, string strDate = "", int lngSalePrice = 0, int intValid = 0, int intVerified = 0, int intSaleType = 0, int intFinancing = 0)
        {
            SaveSaleRecord(ref boolOverWrite, ref lngAcctNum, ref lngTempSaleID, strDate, lngSalePrice, intValid, intVerified, intSaleType, intFinancing);
        }

        public static void SaveSaleRecord(ref bool boolOverWrite, ref int lngAcctNum, ref int lngTempSaleID, string strDate = "", int lngSalePrice = 0, int intValid = 0, int intVerified = 0, int intSaleType = 0, int intFinancing = 0)
        {
            clsDRWrapper clsSrc = new clsDRWrapper();
            clsDRWrapper clsDest = new clsDRWrapper();
            int oldcardnumber;
            string strSaleDate = "";
            clsDRWrapper srdwl = null;
            clsDRWrapper srout = null;
            clsDRWrapper srcom = null;
            clsDRWrapper tDwl = null;
            clsDRWrapper tOut = null;
            clsDRWrapper tCMR = null;
            clsDRWrapper TMR = null;
            int lngSaleID;
            bool boolLoadInfo = false;
            try
            {
                // On Error GoTo ErrorHandler
                if (Information.IsDate(strDate))
                {
                    boolLoadInfo = false;
                }
                else
                {
                    boolLoadInfo = true;
                }
                oldcardnumber = modGNBas.Statics.gintCardNumber;
                lngSaleID = lngTempSaleID;
                // Call clsSrc.OpenRecordset("select * from master where rsaccount = " & lngAcctNum & " order by rscard", strredatabase)
                modREMain.OpenMasterTable(ref TMR, lngAcctNum);
                if (!TMR.EndOfFile())
                {
                    if (boolLoadInfo)
                    {
                        strSaleDate = frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLDATE);
                        if (!Information.IsDate(strSaleDate))
                        {
                            return;
                        }
                    }
                    else
                    {
                        strSaleDate = strDate;
                    }
                    // End If
                }
                cREAccount tAccount = new cREAccount();
                cREAccountController tCont = new cREAccountController();
                cPartyAddress tAddr;
                tCont.LoadAccount(ref tAccount, lngAcctNum);
                while (!TMR.EndOfFile())
                {
                    modGNBas.Statics.gintCardNumber = FCConvert.ToInt32(TMR.Get_Fields_Int32("rscard"));
                    clsDRWrapper temp = modDataTypes.Statics.SRMR;
                    if (boolOverWrite)
                    {
                        modREMain.OpenSRMasterTable(ref temp, lngAcctNum, TMR.Get_Fields_Int32("rscard"), strSaleDate, lngSaleID);
                    }
                    else
                    {
                        modREMain.OpenSRMasterTable(ref temp, lngAcctNum, 1000, strSaleDate);
                        // use 1000 so you cant get a match, just addnew
                    }
                    modDataTypes.Statics.SRMR = temp;
                    if (!boolOverWrite && FCConvert.ToInt32(TMR.Get_Fields_Int32("rscard")) == 1)
                    {
                        lngSaleID = FCConvert.ToInt32(modDataTypes.Statics.SRMR.Get_Fields_Int32("id"));
                        if (lngSaleID == 0)
                        {
                            modDataTypes.Statics.SRMR.Update();
                            lngSaleID = FCConvert.ToInt32(modDataTypes.Statics.SRMR.Get_Fields_Int32("id"));
                            modDataTypes.Statics.SRMR.Edit();
                        }
                    }
                    // Call FrmToSRMRArray               ' Copy data from ALL forms to Master Record
                    temp = modDataTypes.Statics.SRMR;
                    CopyRecordSet(ref TMR, ref temp);
                    modDataTypes.Statics.SRMR = temp;
                    if (!(tAccount == null))
                    {
                        if (!(tAccount.OwnerParty == null))
                        {
                            modDataTypes.Statics.SRMR.Set_Fields("rsname", tAccount.DeedName1);
                            modDataTypes.Statics.SRMR.Set_Fields("RSSecOwner", tAccount.DeedName2);
                            tAddr = tAccount.OwnerParty.GetPrimaryAddress();
                            if (!(tAddr == null))
                            {
                                modDataTypes.Statics.SRMR.Set_Fields("rsaddr1", tAddr.Address1);
                                modDataTypes.Statics.SRMR.Set_Fields("rsaddr2", tAddr.Address2);
                                modDataTypes.Statics.SRMR.Set_Fields("rsaddr3", tAddr.City);
                                modDataTypes.Statics.SRMR.Set_Fields("rsstate", tAddr.State);
                                modDataTypes.Statics.SRMR.Set_Fields("rszip", tAddr.Zip);
                            }
                        }
                    }
                    modDataTypes.Statics.SRMR.Set_Fields("saleid", lngSaleID);
                    modDataTypes.Statics.SRMR.Set_Fields("saledate", strSaleDate);
                    if (boolLoadInfo)
                    {
                        modDataTypes.Statics.SRMR.Set_Fields("pisalevalidity", FCConvert.ToString(Conversion.Val(frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
                        if (Conversion.Val(frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)) > 0)
                        {
                            modDataTypes.Statics.SRMR.Set_Fields("pisaleprice", FCConvert.ToInt32(FCConvert.ToDouble(frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLPRICE))));
                        }
                        else
                        {
                            modDataTypes.Statics.SRMR.Set_Fields("pisaleprice", 0);
                        }
                        modDataTypes.Statics.SRMR.Set_Fields("pisalefinancing", FCConvert.ToString(Conversion.Val(frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
                        modDataTypes.Statics.SRMR.Set_Fields("pisaletype", FCConvert.ToString(Conversion.Val(frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
                        modDataTypes.Statics.SRMR.Set_Fields("pisaleverified", FCConvert.ToString(Conversion.Val(frmNewREProperty.InstancePtr.SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
                    }
                    else
                    {
                        modDataTypes.Statics.SRMR.Set_Fields("pisalevalidity", intValid);
                        modDataTypes.Statics.SRMR.Set_Fields("pisaleprice", lngSalePrice);
                        modDataTypes.Statics.SRMR.Set_Fields("pisalefinancing", intFinancing);
                        modDataTypes.Statics.SRMR.Set_Fields("pisaletype", intSaleType);
                        modDataTypes.Statics.SRMR.Set_Fields("pisaleverified", intVerified);
                    }
                    temp = modDataTypes.Statics.SRMR;
                    modREMain.SaveSRMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strSaleDate, lngSaleID);
                    modDataTypes.Statics.SRMR = temp;
                    if (Strings.UCase(FCConvert.ToString(TMR.Get_Fields_String("rsdwellingcode"))) == "Y")
                    {
                        modREMain.OpenSRDwellingTable(ref srdwl, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strSaleDate);
                        modREMain.OpenDwellingTable(ref tDwl, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                        CopyRecordSet(ref tDwl, ref srdwl);
                        srdwl.Set_Fields("saledate", strSaleDate);
                        // Call frmtosrdwlarray(srdwl)
                        modREMain.savesrdwellingtable(ref srdwl, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strSaleDate);
                    }
                    if (Strings.UCase(FCConvert.ToString(TMR.Get_Fields_String("rsdwellingcode"))) == "C")
                    {
                        modREMain.opensrcommercialtable(ref srcom, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strSaleDate);
                        modREMain.OpenCOMMERCIALTable(ref tCMR, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                        // Call frmtosrcomarray(srcom)
                        CopyRecordSet(ref tCMR, ref srcom);
                        srcom.Set_Fields("saledate", strSaleDate);
                        modREMain.savesrcommercialtable(ref srcom, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strSaleDate);
                    }
                    modREMain.opensroutbuildingtable(ref srout, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                    modREMain.OpenOutBuildingTable(ref tOut, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                    // Call frmtosroutarray(srout)
                    CopyRecordSet(ref tOut, ref srout);
                    srout.Set_Fields("saledate", strSaleDate);
                    modREMain.savesroutbuildingtable(ref srout, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                    TMR.MoveNext();
                }
                modGNBas.Statics.gintCardNumber = oldcardnumber;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveSaleRecord", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // Public Function CheckDBSForArchives() As Boolean
        //
        // CheckDatabaseStructure = False
        // boolMessageShown = False
        // Call AddLinks
        // End Function
        public static bool CheckDatabaseStructure()
        {
            bool CheckDatabaseStructure = false;
            clsDRWrapper clsTemp = new clsDRWrapper();
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Database", false);
            CheckDatabaseStructure = false;
            Statics.boolMessageShown = false;
            // frmREWait.Show , MDIParent
            // frmREWait.lblMessage = "Checking Database Structure."
            //Application.DoEvents();
            // This is done internally now
            // If File.Exists("rehelpcp.exe") Then
            // If File.Exists("rehelpdb.vb1") Then
            // Shell "rehelpcp.exe", vbNormalFocus
            // End If
            // End If
            // Call AddLinks
            // Call clsTemp.Execute("update master set rslocnumalph = trim(rslocnumalph)", strREDatabase)
            // frmWait.prgProgress.Value = 5
            clsTemp.Execute("delete from master where rsaccount = 0 or rscard  = 0", modGlobalVariables.strREDatabase);
            clsTemp.Execute("delete from outbuilding where rsaccount = 0", modGlobalVariables.strREDatabase);
            clsTemp.Execute("delete from dwelling where rsaccount = 0", modGlobalVariables.strREDatabase);
            clsTemp.Execute("delete from commercial where rsaccount = 0", modGlobalVariables.strREDatabase);
            clsTemp.Execute("delete from srmaster where saleid = 0", modGlobalVariables.strREDatabase);
            FixDeletedAccounts();
            // CheckSaleRecords
            VerifySummTable();
            CheckBookPageForDupes();
            FixTranCodes();
            CheckPrevAssessForDupes();
            frmWait.InstancePtr.Unload();
            CheckDatabaseStructure = true;
            CheckDatabaseStructure = CheckDatabaseStructure && CheckVersion_2(true);
            if (Statics.boolMessageShown == true)
            {
                MessageBox.Show("Update Complete. It is safe to allow others to enter Real Estate now.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return CheckDatabaseStructure;
        }

        public static bool CheckVersion_2(bool boolCheckDBStructure)
        {
            return CheckVersion(boolCheckDBStructure);
        }

        public static bool CheckVersion(bool boolCheckDBStructure = false)
        {
            bool CheckVersion = false;
            // checks if this is a different version than what has been run before.
            clsDRWrapper clsTemp = new clsDRWrapper();
            bool boolReturn;
            Statics.boolMessageShown = false;
            CheckVersion = true;
        // Call clsTemp.OpenRecordset("select * from programversions", strGNDatabase)
        // If IsNull(clsTemp.Fields("reversion")) Then GoTo CheckFields
        // If (clsTemp.Fields("rEVersion") = App.Major & "." & App.Minor & "." & App.Revision) And Not boolCheckDBStructure Then Exit Function
        // clsTemp.DisconnectDatabase
        CheckFields:
            ;
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            boolReturn = true;
            if (boolCheckDBStructure)
            {
            }
            // 06/16/2006
            CheckOutbuildingRecords();
            // 09/20/2006
            CheckDwellingRecords();
            CheckCommercialRecords();
           
            cRealEstateDB rDB = new cRealEstateDB();
            boolReturn = rDB.CheckVersion();
            CheckVersion = boolReturn;
            cCentralParties cpDB = new cCentralParties();
            cpDB.CheckVersion();
            var cddb = new cCentralDataDB();
            cddb.CheckVersion();
            var docDB = new cCentralDocumentDB();
            docDB.CheckVersion();

            var util = new cArchiveUtility();
            var archives = util.GetArchiveGroupNames("CommitmentArchive", 0);
            var wrapper = new clsDRWrapper();
            var liveGroup = wrapper.DefaultGroup;
            try
            {
                foreach (string archive in archives)
                {
                    wrapper.DefaultGroup = archive;
                    var archiveDB = new cRealEstateDB();
                    archiveDB.CheckVersion();
                    var archiveCentralData = new cCentralDataDB();
                    archiveCentralData.CheckVersion();
                    var archiveCDocDB = new cCentralDocumentDB();
                    archiveCDocDB.CheckVersion();
                    var archiveCentralParties = new cCentralParties();
                    archiveCentralParties.CheckVersion();
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                frmStartupWait.InstancePtr.Unload();
                wrapper.DefaultGroup = liveGroup;
            }

            if (Statics.boolMessageShown == true)
            {
                Statics.boolMessageShown = false;
                MessageBox.Show("Update Complete. It is safe to allow others to enter Real Estate now.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return true;
        }

        private static bool ShowStartMessage(ref string strMessage)
        {
            bool ShowStartMessage = false;
            ShowStartMessage = false;
            if (!Statics.boolMessageShown)
            {
                Statics.boolMessageShown = true;
                if (MessageBox.Show(strMessage, null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    MessageBox.Show("Operation Cancelled.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return ShowStartMessage;
                }
                frmStartupWait.InstancePtr.Show();
                //Application.DoEvents();
            }
            ShowStartMessage = true;
            return ShowStartMessage;
        }
        // vbPorter upgrade warning: ReportName As ActiveReport	OnWrite(rptLabels)
        //public static void VerifyPrintToFile(ref GrapeCity.ActiveReports.SectionReport ReportName/*, ref DDActiveReports2.DDTool Tool*/)
        //{
        //    const int curOnErrorGoToLabel_Default = 0;
        //    const int curOnErrorGoToLabel_ErrorHandler = 1;
        //    const int curOnErrorGoToLabel_ErrorHandler = 2;
        //    int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
        //    try
        //    {
        //        string oldPath = "";
        //        // oldPath = CurDir
        //        if (Tool.Caption == "PRINT TO FILE")
        //        {
        //            vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
        //            Information.Err(ex).Clear();
        //            // .dlg1.FromPage = 1
        //            // .dlg1.ToPage = .Pages.Count
        //            // .dlg1.FLAGS = &H0
        //            // .dlg1.CancelError = True
        //            // On Error Resume Next
        //            // MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
        //            //- MDIParent.InstancePtr.CommonDialog1.CancelError = false;
        //            MDIParent.InstancePtr.CommonDialog1.Filter = "*.rtf";
        //            MDIParent.InstancePtr.CommonDialog1.InitialDirectory = Environment.CurrentDirectory;
        //            /*? On Error Resume Next  */
        //            MDIParent.InstancePtr.CommonDialog1.ShowDialog();
        //            if (Information.Err(ex).Number == 0)
        //            {
        //                Information.Err(ex).Clear();
        //                vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
        //                ActiveReportsRTFExport.ARExportRTF a = new ActiveReportsRTFExport.ARExportRTF();
        //                a = new ActiveReportsRTFExport.ARExportRTF;
        //                a.FileName = MDIParent.InstancePtr.CommonDialog1.FileName;
        //                if (a.FileName == string.Empty)
        //                {
        //                    MessageBox.Show("You must choose a filename", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //                    return;
        //                }
        //                if (Conversion.Val(Strings.InStr(1, a.FileName, ".", CompareConstants.vbTextCompare)) + "") > 0)
        //                {
        //                }
        //                else
        //                {
        //                    a.FileName = a.FileName + ".rtf";
        //                }
        //                ReportName.Export(a);
        //                MessageBox.Show("Export to file  " + a.FileName + "  was completed successfully.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            }
        //            // End If
        //            // End If
        //        }
        //        // ChDir oldPath
        //        return;
        //        ErrorHandler:;
        //        Environment.CurrentDirectory = oldPath;
        //        MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "TRIO Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //    }
        //    catch
        //    {
        //        switch (vOnErrorGoToLabel)
        //        {
        //            default:
        //            case curOnErrorGoToLabel_Default:
        //                // ...
        //                break;
        //            case curOnErrorGoToLabel_ErrorHandler:
        //                //? goto ErrorHandler;
        //                break;
        //            case curOnErrorGoToLabel_ErrorHandler:
        //                //? goto ErrorHandler;
        //                break;
        //        }
        //    }
        //}
        // vbPorter upgrade warning: ReportName As ActiveReport	OnWrite(rptLabels)
        public static void SetPrintProperties(GrapeCity.ActiveReports.SectionReport ReportName)
        {
            int x;
            //ReportName.Toolbar.Tools.Add("");
            //x = ReportName.Toolbar.Tools.Count;
            //ReportName.Toolbar.Tools(x - 1).Type = 2;
            //ReportName.Toolbar.Tools(x - 1).ID = "9998";
            //ReportName.Toolbar.Tools.Add("PRINT TO FILE");
            //x = ReportName.Toolbar.Tools.Count;
            //ReportName.Toolbar.Tools(x - 1).ID = "9999";
            //ReportName.Toolbar.Tools.Add("");
            //x = ReportName.Toolbar.Tools.Count;
            //ReportName.Toolbar.Tools(x - 1).Type = 2;
            //ReportName.Toolbar.Tools(x - 1).ID = "9997";
        }

        private static void CheckSaleRecords()
        {
            clsDRWrapper clsExecute = new clsDRWrapper();
            clsDRWrapper clsDelete = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                clsExecute.Execute("delete from srmaster where saledate is null or saledate = 0", modGlobalVariables.strREDatabase);
                clsExecute.Execute("delete from srdwelling where saledate is null or saledate = 0", modGlobalVariables.strREDatabase);
                clsExecute.Execute("delete from srcommercial where saledate is null or saledate = 0", modGlobalVariables.strREDatabase);
                clsExecute.Execute("delete from sroutbuilding where saledate is null or saledate = 0", modGlobalVariables.strREDatabase);
                clsExecute.OpenRecordset("select rsaccount,rscard,saledate,rsname from srmaster group by rsaccount,rscard,saledate,rsname having count (rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!clsExecute.EndOfFile())
                {
                    clsDelete.OpenRecordset("select * from srmaster where rsaccount = " + clsExecute.Get_Fields_Int32("rsaccount") + " and rscard = " + clsExecute.Get_Fields_Int32("rscard") + " and rsname = '" + clsExecute.Get_Fields_String("rsname") + "' and saledate = '" + clsExecute.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                    clsDelete.MoveNext();
                    while (!clsDelete.EndOfFile())
                    {
                        clsDelete.Delete();
                        clsDelete.MoveNext();
                    }
                    clsExecute.MoveNext();
                }
                clsExecute.OpenRecordset("select rsaccount,rscard,saledate from srdwelling group by rsaccount,rscard,saledate having count(rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!clsExecute.EndOfFile())
                {
                    clsDelete.OpenRecordset("select * from srdwelling where rsaccount = " + clsExecute.Get_Fields_Int32("rsaccount") + " and rscard = " + clsExecute.Get_Fields_Int32("rscard") + " and saledate = '" + clsExecute.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                    clsDelete.MoveNext();
                    while (!clsDelete.EndOfFile())
                    {
                        clsDelete.Delete();
                        clsDelete.MoveNext();
                    }
                    clsExecute.MoveNext();
                }
                clsExecute.OpenRecordset("select rsaccount,rscard,saledate from srcommercial group by rsaccount,rscard,saledate having count(rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!clsExecute.EndOfFile())
                {
                    clsDelete.OpenRecordset("select * from srcommercial where rsaccount = " + clsExecute.Get_Fields_Int32("rsaccount") + " and rscard = " + clsExecute.Get_Fields_Int32("rscard") + " and saledate = #" + clsExecute.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
                    clsDelete.MoveNext();
                    while (!clsDelete.EndOfFile())
                    {
                        clsDelete.Delete();
                        clsDelete.MoveNext();
                    }
                    clsExecute.MoveNext();
                }
                clsExecute.OpenRecordset("select rsaccount,rscard,saledate from sroutbuilding group by rsaccount,rscard,saledate having count(rsaccount) > 1", modGlobalVariables.strREDatabase);
                while (!clsExecute.EndOfFile())
                {
                    clsDelete.OpenRecordset("select * from sroutbuilding where rsaccount = " + clsExecute.Get_Fields_Int32("rsaccount") + " and rscard = " + clsExecute.Get_Fields_Int32("rscard") + " and saledate = #" + clsExecute.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
                    clsDelete.MoveNext();
                    while (!clsDelete.EndOfFile())
                    {
                        clsDelete.Delete();
                        clsDelete.MoveNext();
                    }
                    clsExecute.MoveNext();
                }
            }
            catch
            {
                // ErrorHandler:
            }
        }

        public static void LeavetoBilling()
        {
            //FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
            //foreach (FCForm fm in Application.OpenForms)
            //{
            //    fm.Unload();
            //    Application.Exit();
            //} // fm
            FCUtils.CloseFormsOnProject();
        }

        private static void CheckPropertyCode()
        {
            clsDRWrapper rsSave = new clsDRWrapper();
            rsSave.Execute("update master set propertycode = val(propertycode & '')", modGlobalVariables.strREDatabase);
        }

        private static void FixDeletedAccounts()
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            clsTemp.Execute("update master set rsdeleted = 1 where rsaccount in (select rsaccount from master where rsdeleted = 1 and rscard = 1)", modGlobalVariables.strREDatabase);
            clsTemp.Execute("update srmaster set rsdeleted = 1 where rsaccount in (select rsaccount from srmaster where rsdeleted = 1 and rscard = 1)", modGlobalVariables.strREDatabase);
        }
        // Private Sub AddTWArchiveDirectory()
        // Dim fso As New FCFileSystemObject
        //
        // If Not Directory.Exists("TWArchive") Then
        // fso.CreateFolder ("TWArchive")
        // End If
        // End Sub
        private static void CheckExemptCodes()
        {
            clsDRWrapper rsSave = new clsDRWrapper();
            rsSave.Execute("update master set riexemptcd1 = 0,riexemptcd2 = 0,riexemptcd3 = 0 where rscard > 1", modGlobalVariables.strREDatabase);
        }

        public static bool CheckOutstandingTaxes(ref int lngAccount)
        {
            bool CheckOutstandingTaxes = false;
            // when an account is deleted, this checks to see if there are outstanding taxes on the property
            clsDRWrapper clsTemp = new clsDRWrapper();
            // vbPorter upgrade warning: intResponse As short, int --> As DialogResult
            DialogResult intResponse;
            double dblOwed = 0;
            double dblPaid;
            try
            {
                // On Error GoTo ErrorHandler
                CheckOutstandingTaxes = true;
                // assume true unless we find a collections database and there are taxes
                if (modGlobalConstants.Statics.gboolCL)
                {
                    if (System.IO.File.Exists(modGlobalVariables.strCLDatabase))
                    {
                        // now add all owed taxes
                        dblOwed = 0;
                        dblOwed = modCLCalculations.CalculateAccountTotal(lngAccount, true);
                        // Call clsTemp.OpenRecordset("select sum(taxdue1 + taxdue2 + taxdue3 + taxdue4 + interestcharged + demandfees) as TotTaxes,sum(PrincipalPaid + InterestPaid + demandFeespaid) as TotPaid from billingmaster where ACCOUNT = " & lngAccount & " and billingtype = 'RE' AND lienrecordnumber = 0", strCLDatabase)
                        // If Not clsTemp.EndOfFile Then
                        // dblOwed = Val(clsTemp.Fields("tottaxes"))
                        // dblPaid = Val(clsTemp.Fields("totpaid"))
                        // 
                        // dblOwed = dblOwed - dblPaid
                        // 
                        // End If
                        // dblOwed = dblOwed + CheckLienTaxes(lngAccount)
                        if (dblOwed > 0)
                        {
                            // trying to avoid message box being stuck behind windows
                            //Application.DoEvents();
                            intResponse = MessageBox.Show("There is an outstanding amount owed for this account. Do you still want to delete it?", "Outstanding Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (intResponse == DialogResult.Yes)
                            {
                                // Call clsTemp.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Delete Account','Deleted account " & gintLastAccountNumber & " with an outstanding amount present')", strredatabase)
                                modGlobalFunctions.AddCYAEntry_26("RE", "Delete Account", "Deleted account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " with an outstanding amount present");
                            }
                            else
                            {
                                CheckOutstandingTaxes = false;
                            }
                        }
                        else if (dblOwed < 0)
                        {
                            //Application.DoEvents();
                            intResponse = MessageBox.Show("There is an overpayment found for this account.  Do you still want to delete it?", "Overpayment", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (intResponse == DialogResult.Yes)
                            {
                                // Call clsTemp.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Delete Account','Deleted account " & gintLastAccountNumber & " with an overpayment present')", strredatabase)
                                modGlobalFunctions.AddCYAEntry_26("RE", "Delete Account", "Deleted account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " with an overpayment present");
                            }
                            else
                            {
                                CheckOutstandingTaxes = false;
                            }
                        }
                    }
                }
                return CheckOutstandingTaxes;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                //Application.DoEvents();
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CheckOutstandingTaxes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CheckOutstandingTaxes;
        }

        public static void NewExtractToDisk()
        {
            string strRec = "";
            FCFileSystem fso = new FCFileSystem();
            DirectoryInfo flder;
            DriveInfo tempDrive;
            int intRecsPerDisk;
            short intCounter;
            // vbPorter upgrade warning: intRes As short	OnWrite(DialogResult)
            short intRes = 0;
            // vbPorter upgrade warning: ans9 As object	OnWrite(DialogResult)
            object ans9;
            int intRecsOnThisDisk;
            FileInfo[] FilesList;
            short x;
            int lngSpaceLeft;
            long lngSpaceUsed;
            int lngSpacePerDisk;
            string strPath;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                if (!FCFileSystem.FileExists("tsdbase.asc"))
                {
                    MessageBox.Show("No extract file found to copy", "No File Found", MessageBoxButtons.OK);
                    return;
                }
                // strPath = frmGetDirectory.Init("Select Drive and Directory to copy to", "", False, "Copy Extract To")
                MDIParent.InstancePtr.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNExplorer) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNLongNames) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir);
                MDIParent.InstancePtr.CommonDialog1.FileName = "tsdbase.asc";
                MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                //FC:FINAL:RPU:#i339 - Use Application.Download to download the file to clent
                //MDIParent.InstancePtr.CommonDialog1.ShowSave();
                FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "tsdbase.asc"), "tsdbase.asc");
                //FC:FINAL:RPU:#i339 - Actualize the path
                //strPath = MDIParent.InstancePtr.CommonDialog1.FileName;
                strPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "tsdbase.asc");
                if (fecherFoundation.Strings.Trim(strPath) == string.Empty)
                {
                    return;
                }
                if (Strings.Left(fecherFoundation.Strings.UCase(fso.GetDriveName(strPath)), 1) != "A")
                {
                    tempDrive = fso.GetDrive(fso.GetDriveName(strPath));
                    if (tempDrive.DriveType == DriveType.CDRom)
                    {
                        MessageBox.Show("Cannot write to this type of medium", "Invalid Medium", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (!tempDrive.IsReady)
                    {
                        MessageBox.Show("Drive isn't ready", "Drive Not Ready", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    //FC:FINAL:RPU:#i339 - Don't need to do that because the file was downloaded
                    //FCFileSystem.FileCopy("tsdbase.asc", strPath);
                    MessageBox.Show("File Copied", "Copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            here:
                ;
                ans9 = FCConvert.ToInt32(MessageBox.Show("Put the first diskette into Drive A.", "Copy DB Extract to Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information));
                if (FCConvert.ToInt32(ans9) == FCConvert.ToInt32(DialogResult.Cancel))
                {
                    //MDIParent.Grid.SetFocus();
                    return;
                }
                tempDrive = fso.GetDrive("a:");
                if (!tempDrive.IsReady)
                {
                    intRes = FCConvert.ToInt16(MessageBox.Show("The drive is not ready.", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning));
                    if (intRes == FCConvert.ToInt16(DialogResult.Cancel))
                    {
                        //MDIParent.Grid.SetFocus();
                        return;
                    }
                    goto here;
                }
                FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                FCFileSystem.FileClose(1, 2);
                FCFileSystem.FileOpen(2, "TSDBASE.ASC", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), -1);
                FCFileSystem.FileClose(2);
                FCFileSystem.FileOpen(2, "TSDBASE.ASC", OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
                //flder = fso.GetFolder("A:");
                //FilesList = flder.Files;
                flder = new DirectoryInfo("A:");
                FilesList = flder.GetFiles();
                lngSpaceUsed = 0;
                foreach (FileInfo fl in FilesList)
                {
                    lngSpaceUsed += fl.Length;
                }
                // fl
                FCFileSystem.FileOpen(1, "A:\\TSDBASE.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                lngSpacePerDisk = 1440000;
                strRec = FCFileSystem.LineInput(2);
            NextDiskTag:
                ;
                if (lngSpaceUsed + strRec.Length >= lngSpacePerDisk)
                {
                    lngSpaceUsed = 0;
                    FCFileSystem.FileClose(1);
                NextPrompt:
                    ;
                    intRes = FCConvert.ToInt16(MessageBox.Show("Be sure the light on Drive A is OFF, then insert a disk and press Enter.", " ", MessageBoxButtons.OKCancel, MessageBoxIcon.Information));
                    if (intRes == FCConvert.ToInt16(DialogResult.Cancel))
                        goto alldone;
                    tempDrive = fso.GetDrive("a:");
                    if (!tempDrive.IsReady)
                    {
                        intRes = FCConvert.ToInt16(MessageBox.Show("The drive is not ready.", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning));
                        if (intRes == FCConvert.ToInt16(DialogResult.Cancel))
                        {
                            //MDIParent.InstancePtr.Grid.SetFocus();
                            return;
                        }
                        goto NextPrompt;
                    }
                    flder = new DirectoryInfo("A:");
                    FilesList = flder.GetFiles();
                    lngSpaceUsed = 0;
                    foreach (FileInfo fl in FilesList)
                    {
                        lngSpaceUsed += fl.Length;
                    }
                    // fl
                    FCFileSystem.FileOpen(1, "A:\\TSDBASE.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                }
                // Do Until EOF(2) Or intcounter = intRecsPerDisk
                while (!(FCFileSystem.EOF(2) || (lngSpaceUsed + strRec.Length >= lngSpacePerDisk)))
                {
                    FCFileSystem.PrintLine(1, strRec);
                    lngSpaceUsed += strRec.Length;
                    if (!FCFileSystem.EOF(2))
                    {
                        strRec = FCFileSystem.LineInput(2);
                    }
                    else
                    {
                        strRec = "";
                    }
                }
                // If intcounter >= intRecsPerDisk And Not EOF(2) Then GoTo NextDiskTag
                if (!FCFileSystem.EOF(2))
                    goto NextDiskTag;
                alldone:
                ;
                FCFileSystem.FileClose(1);
                FCFileSystem.FileClose(2);
                MessageBox.Show("When the drive light goes off you may remove disk.  Copy is finished.", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FCGlobal.Screen.MousePointer = 0;
                return;
            }
            catch
            {
                // ErrorHandler:
                FCFileSystem.FileClose(1);
                FCFileSystem.FileClose(2);
                if (fecherFoundation.Information.Err().Number != 32755)
                {
                    MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + "  " + fecherFoundation.Information.Err().Description + "\r\n" + "The copy was not successful.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    // cancelled copy
                }
            }
        }

        public static double CheckLienTaxes(ref int lngAccount)
        {
            double CheckLienTaxes = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strSQL;
            double dblOwed;
            clsDRWrapper clsTemp = new clsDRWrapper();
            CheckLienTaxes = 0;
            strSQL = "select * from billingmaster inner join lienrec on (billingmaster.lienrecordnumber = lienrec.lienrecordnumber) where account = " + FCConvert.ToString(lngAccount) + " and billingtype = 'RE'";
            clsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
            dblOwed = 0;
            while (!clsLoad.EndOfFile())
            {
                // TODO Get_Fields: Check the table for the column [principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                // TODO Get_Fields: Field [lienrec.interestcharged] not found!! (maybe it is an alias?)
                // TODO Get_Fields: Check the table for the column [maturityfee] and replace with corresponding Get_Field method
                // TODO Get_Fields: Field [lienrec.principalpaid] not found!! (maybe it is an alias?)
                // TODO Get_Fields: Field [lienrec.interestpaid] not found!! (maybe it is an alias?)
                dblOwed += ((clsLoad.Get_Fields("principal") + clsLoad.Get_Fields("Interest") + clsLoad.Get_Fields("Costs") - clsLoad.Get_Fields("lienrec.interestcharged") - clsLoad.Get_Fields("maturityfee")) - clsLoad.Get_Fields("lienrec.principalpaid") - clsLoad.Get_Fields("lienrec.interestpaid") - clsLoad.Get_Fields_Decimal("costspaid"));
                clsLoad.MoveNext();
            }
            CheckLienTaxes = dblOwed;
            return CheckLienTaxes;
        }

        public static bool CheckMortgage(ref int lngAccount)
        {
            bool CheckMortgage = false;
            // when an account is deleted, you may need to warn them if there is a mortgage held on the property
            clsDRWrapper clsTemp = new clsDRWrapper();
            // vbPorter upgrade warning: intResponse As short, int --> As DialogResult
            DialogResult intResponse;
            int lngMortID = 0;
            try
            {
                // On Error GoTo ErrorHandler
                CheckMortgage = true;
                clsTemp.OpenRecordset("select * from mortgageassociation where account = " + FCConvert.ToString(lngAccount) + " and module = 'RE'", "CentralData");
                if (!clsTemp.EndOfFile())
                {
                    //Application.DoEvents();
                    intResponse = MessageBox.Show("Mortgage holder number " + clsTemp.Get_Fields_Int32("mortgageholderid") + " is currently listed as holding a mortgage on this account." + "\r\n" + "Do you still want to delete the account?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (intResponse == DialogResult.No)
                    {
                        CheckMortgage = false;
                    }
                    else
                    {
                        lngMortID = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("mortgageholderid"));
                        // Call clsTemp.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Delete Account','Chose to delete account " & gintLastAccountNumber & " with a mortgage held by " & lngMortID & "')")
                        modGlobalFunctions.AddCYAEntry_26("RE", "Delete Account", "Chose to delete account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " with a mortgage held by " + FCConvert.ToString(lngMortID));
                    }
                }
                return CheckMortgage;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                //Application.DoEvents();
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CheckMortgage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CheckMortgage;
        }

        public static void checkGroup_6(int lngAccount, bool boolSelling)
        {
            checkGroup(ref lngAccount, ref boolSelling);
        }

        public static void checkGroup_8(int lngAccount, bool boolSelling)
        {
            checkGroup(ref lngAccount, ref boolSelling);
        }

        public static void checkGroup(ref int lngAccount, ref bool boolSelling)
        {
            // when an account is deleted or sold in RE and the account is in a group you must change the group
            // check to see if it's in a group
            // if it is then see what needs to be done
            clsDRWrapper clsTemp = new clsDRWrapper();
            clsDRWrapper clsGroup = new clsDRWrapper();
            int lngGroupNum;
            //Application.DoEvents();
            // first see if its in a group
            clsTemp.OpenRecordset("select * from MODULEassociation where account = " + FCConvert.ToString(lngAccount) + " and module = 'RE'", "CentralData", 0, false, 0, false, "", false);
            if (clsTemp.EndOfFile())
            {
                // not in a group so leave
                return;
            }
            int LngNumAccts;
            // it is in a group so lets see what we need to do about it
            lngGroupNum = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("groupnumber"));
            clsGroup.OpenRecordset("select * from moduleassociation where groupnumber = " + clsTemp.Get_Fields_Int32("groupnumber"), "CentralData");
            clsGroup.MoveLast();
            clsGroup.MoveFirst();
            LngNumAccts = clsGroup.RecordCount();
            if (!boolSelling)
            {
                // just deleting the account
                if (LngNumAccts < 2)
                {
                    // its the only one in the group so just get rid of the group
                    clsGroup.Execute("delete  from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupNum), "CentralData");
                    clsGroup.Execute("delete  from groupmaster where id = " + FCConvert.ToString(lngGroupNum), "CentralData");
                    return;
                }
                // it's not the only one in the group.
                // take it out, give a warning and send them to the group screen to fix things
                clsGroup.Execute("delete from moduleassociation where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and module = 'RE'", "CentralData");
                // now take out links to it
                clsGroup.Execute("update moduleassociation  set primaryassociate = 0,REMASTERACCT = 0" + " where remasteracct = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "CentralData");
                // not give a message
                //Application.DoEvents();
                MessageBox.Show("This account was in a group and has been removed." + "\r\n" + "If it was the group primary a new one has been randomly assigned." + "\r\n" + "Please check that the group information is still correct.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //Application.DoEvents();
                frmGroup.InstancePtr.Init(lngGroupNum);
                return;
            }
        }

        public static void ErrorRoutine()
        {
            try
            {
                object answer;
                FCGlobal.Screen.MousePointer = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("  Error Number " + Information.Err(ex).Number + "  " + Information.Err(ex).Description + "\r\n" + "  Has been encountered", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static void CheckNumeric()
        {
            int x;
            for (x = 1; x <= FCConvert.ToString(modGNBas.Statics.Resp1).Length; x++)
            {
                modGNBas.Statics.Resp2 = Convert.ToByte(Strings.Mid(FCConvert.ToString(modGNBas.Statics.Resp1), x, 1)[0]);
                if (FCConvert.ToDouble(modGNBas.Statics.Resp2) < 48 || FCConvert.ToDouble(modGNBas.Statics.Resp2) > 57)
                {
                    string Resp1Temp = FCConvert.ToString(modGNBas.Statics.Resp1);
                    fecherFoundation.Strings.MidSet(ref Resp1Temp, x, Strings.Mid(FCConvert.ToString(modGNBas.Statics.Resp1), x + 1, Strings.Len(modGNBas.Statics.Resp1) - x/*, FCConvert.ToString(modGNBas.Resp1).Length - 1*/));
                    fecherFoundation.Strings.MidSet(ref Resp1Temp, Strings.Len(modGNBas.Statics.Resp1), 1, " ");
                    break;
                }
            }
            // x
        }
        // vbPorter upgrade warning: intValue As object	OnWrite(int, string, double)
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        public static string PadToString(object intValue, short intLength)
        {
            return PadToString_8(ref intValue, ref intLength);
        }

        public static string PadToString_8(ref object intValue, ref short intLength)
        {
            string PadToString = "";
            // converts an integer to string
            // this routine is faster than ConvertToString
            // to use this function pass it a value and then length of string
            // Example:  XYZStr = PadToString(XYZInt, 6)
            // if XYZInt is equal to 123 then XYZStr is equal to "000123"
            // 
            // vbPorter upgrade warning: strTemp As string	OnWrite(string, short)
            string strTemp = "";
            if (fecherFoundation.FCUtils.IsNull(intValue) == true)
                intValue = 0;
            if (Information.IsNumeric(intValue))
            {
                strTemp = Conversion.Str(FCConvert.ToString(Conversion.Val(intValue)));
            }
            else
            {
                strTemp = FCConvert.ToString(0);
            }
            strTemp = Strings.Trim(strTemp);
            if (strTemp.Length > intLength)
            {
                PadToString = Strings.Right(strTemp, intLength);
            }
            else
            {
                PadToString = Strings.StrDup(intLength - strTemp.Length, "0") + strTemp;
            }
            return PadToString;
        }

        public static double Round(double dValue, short iDigits)
        {
            //FC:FINAL:MSH - i.issue #1075: incorrect rounding. Part of number after iDigits won't be taken into account
            //double Round = 0;
            //Round = Conversion.Int(Conversion.Val(dValue * (Math.Pow(10, iDigits))) + 0.5) / (Math.Pow(10, iDigits));
            //return Round;
            return Math.Round(dValue, iDigits, MidpointRounding.AwayFromZero);
        }

        public static double RoundDown_8(double dValue, short iDigits)
        {
            return RoundDown(ref dValue, ref iDigits);
        }

        public static double RoundDown(ref double dValue, ref short iDigits)
        {
            double RoundDown = 0;
            RoundDown = Conversion.Int(dValue * (Math.Pow(10, iDigits))) / (Math.Pow(10, iDigits));
            return RoundDown;
        }

        public static void WriteYY()
        {
            modReplaceWorkFiles.EntryFlagFile(true, "YY", true);
        }

        public static void SaveWindowSize(ref Form frm)
        {
            Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", FCConvert.ToString(frm.Left));
            Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", FCConvert.ToString(frm.Top));
            Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", FCConvert.ToString(frm.Width));
            Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", FCConvert.ToString(frm.Height));
        }

        public static void GetWindowSize(ref Form frm)
        {
            // vbPorter upgrade warning: lngHeight As int	OnWrite(string)
            int lngHeight;
            // vbPorter upgrade warning: lngWidth As int	OnWrite(string)
            int lngWidth;
            // vbPorter upgrade warning: lngTop As int	OnWrite(string)
            int lngTop;
            // vbPorter upgrade warning: lngLeft As int	OnWrite(string)
            int lngLeft;
            // 
            lngHeight = FCConvert.ToInt32(Interaction.GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", FCConvert.ToString(6500)));
            lngLeft = FCConvert.ToInt32(Interaction.GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", FCConvert.ToString(1000)));
            lngTop = FCConvert.ToInt32(Interaction.GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", FCConvert.ToString(1000)));
            lngWidth = FCConvert.ToInt32(Interaction.GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", FCConvert.ToString(6500)));
            if (lngHeight != frm.Height)
            {
                if (lngWidth != frm.Width)
                {
                    if (frm.WindowState != FormWindowState.Normal)
                    {
                        frm.WindowState = FormWindowState.Normal;
                    }
                    frm.Height = lngHeight;
                    frm.Left = lngLeft;
                    frm.Top = lngTop;
                    frm.Width = lngWidth;
                    if (FCGlobal.Screen.Height - frm.Height <= 100)
                    {
                        if (FCGlobal.Screen.Width - frm.Width <= 100)
                        {
                            frm.WindowState = FormWindowState.Maximized;
                        }
                    }
                }
            }
        }
        // vbPorter upgrade warning: lngAccount As int	OnWrite(double, int)
        public static bool RenumberCards_2(int lngAccount)
        {
            return RenumberCards(ref lngAccount);
        }

        public static bool RenumberCards(ref int lngAccount)
        {
            bool RenumberCards = false;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper clsLoad = new clsDRWrapper();
                clsDRWrapper clsChange = new clsDRWrapper();
                int x;
                int intCard = 0;
                string strSQL;
                int lastCard = 0;
                int lastBuildingStyle = 0;
                RenumberCards = false;
                x = 1;
                modGlobalFunctions.AddCYAEntry_8("RE", "Fixed Account " + FCConvert.ToString(lngAccount));
                clsLoad.OpenRecordset("Select rsaccount,rscard from master where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    if (clsLoad.Get_Fields_Int32("rscard") > x)
                    {
                        intCard = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rscard"));
                        clsChange.Execute("UPdate Master SET RSCard = " + FCConvert.ToString(x) + " where RSAccount = " + FCConvert.ToString(lngAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate Commercial SET RSCard = " + FCConvert.ToString(x) + " where RSAccount = " + FCConvert.ToString(lngAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate Dwelling SET RSCard = " + FCConvert.ToString(x) + " where RSAccount = " + FCConvert.ToString(lngAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate OutBuilding SET RSCard = " + FCConvert.ToString(x) + " where RSAccount = " + FCConvert.ToString(lngAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate IncomeApproach SET Card = " + FCConvert.ToString(x) + " where Account = " + FCConvert.ToString(lngAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate Incomeexpense SET Card = " + FCConvert.ToString(x) + " where Account = " + FCConvert.ToString(lngAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate Incomevalue SET Card = " + FCConvert.ToString(x) + " where Account = " + FCConvert.ToString(lngAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                        clsChange.Execute("UPdate PictureRecord SET RSCard = " + FCConvert.ToString(x) + " where MRAccountNumber = " + FCConvert.ToString(lngAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);

                    }
                    else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rscard")) != x)
                    {
                        clsLoad.Edit();
                        clsLoad.Set_Fields("rscard", x);
                        clsLoad.Update();
                    }
                    x += 1;
                    clsLoad.MoveNext();
                }
                strSQL = "Select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard ";
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                if (!clsLoad.EndOfFile())
                {
                    lastCard = -2;
                }
                while (!clsLoad.EndOfFile())
                {
                    if (Conversion.Val(clsLoad.Get_Fields_Int32("rscard")) == lastCard)
                    {
                        clsLoad.Delete();
                    }
                    else
                    {
                        lastCard = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rscard"));
                    }
                    clsLoad.MoveNext();
                }
                strSQL = "select * from dwelling where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard";
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                if (!clsLoad.EndOfFile())
                {
                    lastCard = -2;
                }
                while (!clsLoad.EndOfFile())
                {
                    if (Conversion.Val(clsLoad.Get_Fields_Int32("rscard")) == lastCard)
                    {
                        // try to see which one has good data first
                        if (Conversion.Val(clsLoad.Get_Fields_Int32("distyle")) < 1 || lastBuildingStyle > 0)
                        {
                            clsLoad.Delete();
                        }
                        else if (lastBuildingStyle < 1)
                        {
                            lastBuildingStyle = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("distyle"))));
                            clsLoad.MovePrevious();
                            clsLoad.Delete();
                        }
                    }
                    else
                    {
                        lastCard = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rscard"));
                        lastBuildingStyle = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("DISTYLE"))));
                    }
                    clsLoad.MoveNext();
                }
                strSQL = "select max(rscard) as maxcard from master where rsaccount = " + FCConvert.ToString(lngAccount);
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                // TODO Get_Fields: Field [maxcard] not found!! (maybe it is an alias?)
                lastCard = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("maxcard"))));
                int intCount = 0;
                for (x = 1; x <= lastCard; x++)
                {
                    strSQL = "select * from commercial where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = " + FCConvert.ToString(x);
                    intCount = x;
                    clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                    while (!clsLoad.EndOfFile())
                    {
                        if (intCount != Conversion.Val(clsLoad.Get_Fields_Int32("rscard")))
                        {
                            clsLoad.Edit();
                            clsLoad.Set_Fields("rscard", intCount);
                            clsLoad.Update();
                        }
                        intCount += 1;
                        clsLoad.MoveNext();
                    }
                }
                // x
                RenumberCards = true;
                return RenumberCards;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Renumber Cards", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return RenumberCards;
        }
        // vbPorter upgrade warning: lngAccount As object	OnWriteFCConvert.ToInt32(
        public static bool OverwriteSalesRecord_6(object lngAccount, int lngSaleID, bool boolOverwriteMaster = true)
        {
            return OverwriteSalesRecord(ref lngAccount, ref lngSaleID, boolOverwriteMaster);
        }

        public static bool OverwriteSalesRecord(ref object lngAccount, ref int lngSaleID, bool boolOverwriteMaster = true)
        {
            bool OverwriteSalesRecord = false;
            // deletes the old data and create new data
            clsDRWrapper clsSource = new clsDRWrapper();
            clsDRWrapper clsSale = new clsDRWrapper();
            int lngID = 0;
            DateTime dtSaleDate = default(DateTime);
            try
            {
                // On Error GoTo ErrorHandler
                OverwriteSalesRecord = false;
                clsSource.OpenRecordset("Select * from master where rsaccount = " + lngAccount + " order by rscard", modGlobalVariables.strREDatabase);
                if (boolOverwriteMaster)
                {
                    clsSale.Execute("delete from srmaster where saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
                }
                clsSale.OpenRecordset("select * from srmaster where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    if (FCConvert.ToInt32(clsSource.Get_Fields_Int32("rscard")) == 1)
                    {
                        lngID = FCConvert.ToInt32(clsSale.Get_Fields_Int32("id"));
                        dtSaleDate = (DateTime)clsSource.Get_Fields_DateTime("saledate");
                    }
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    clsSale.Set_Fields("saleid", lngID);
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // copy dwelling stuff
                clsSource.OpenRecordset("select * from dwelling where rsaccount = " + lngAccount + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from srdwelling where rsaccount = " + lngAccount + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srdwelling where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    // clsSale.Update
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    // clsSale.Fields("saleid") = lngID
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // copy commercial stuff
                clsSource.OpenRecordset("select * from commercial where rsaccount = " + lngAccount + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from srcommercial where rsaccount = " + lngAccount + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srcommercial where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    // clsSale.Update
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    // clsSale.Fields("saleid") = lngID
                    clsSale.Set_Fields("cmexp1", " ");
                    clsSale.Set_Fields("cmexp15", " ");
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // copy outbuilding stuff
                clsSource.OpenRecordset("select * from outbuilding where rsaccount = " + lngAccount + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from sroutbuilding where rsaccount = " + lngAccount + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from sroutbuilding where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    // clsSale.Update
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    // clsSale.Fields("saleid") = lngID
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // book and page stuff
                clsSource.OpenRecordset("select * from bookpage where account = " + lngAccount + " order by card,line", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from bookpage where account = " + lngAccount + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srbookpage where account = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    // clsSale.Update
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                OverwriteSalesRecord = true;
                return OverwriteSalesRecord;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In OverwriteSalesRecord", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return OverwriteSalesRecord;
        }

        public static bool CreateSalesRecord(ref int lngAccount)
        {
            bool CreateSalesRecord = false;
            // this will open the account passed in and create a sale record
            // the master,dwelling,commercial,outbuilding and bookpage tables must all be copied
            clsDRWrapper clsSource = new clsDRWrapper();
            string strREFullDBName;
            string strMasterJoin;
            string strMasterJoinJoin = "";
            strREFullDBName = clsSource.Get_GetFullDBName("RealEstate");
            strMasterJoin = modREMain.GetMasterJoin();
            string strMasterJoinQuery;
            strMasterJoinQuery = "(" + strMasterJoin + ") mj";
            clsDRWrapper clsSale = new clsDRWrapper();
            int lngID = 0;
            // sale id
            DateTime dtSaleDate = default(DateTime);
            try
            {
                // On Error GoTo ErrorHandler
                CreateSalesRecord = false;
                clsSource.OpenRecordset(strMasterJoin + " where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srmaster where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    lngID = 0;
                    clsSale.AddNew();
                    if (FCConvert.ToInt32(clsSource.Get_Fields_Int32("rscard")) == 1)
                    {
                        dtSaleDate = (DateTime)clsSource.Get_Fields_DateTime("saledate");
                    }
                    CopyDataConnection(ref clsSource, ref clsSale);
                    clsSale.Set_Fields("rsname", clsSource.Get_Fields_String("DeedName1"));
                    clsSale.Set_Fields("rssecowner", clsSource.Get_Fields_String("DeedName2"));
                    clsSale.Set_Fields("saleid", lngID);
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    if (clsSource.Get_Fields_Int32("rscard") == 1)
                    {
                        lngID = clsSale.Get_Fields_Int32("id");
                        clsSale.Edit();
                        clsSale.Set_Fields("saleid", lngID);
                        clsSale.Update();
                    }
                    clsSource.MoveNext();
                }
                // copy dwelling stuff
                clsSource.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from srdwelling where rsaccount = " + FCConvert.ToString(lngAccount) + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srdwelling where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    CopyDataConnection(ref clsSource, ref clsSale);
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // copy commercial stuff
                clsSource.OpenRecordset("select * from commercial where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from srcommercial where rsaccount = " + FCConvert.ToString(lngAccount) + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srcommercial where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    CopyDataConnection(ref clsSource, ref clsSale);
                    clsSale.Set_Fields("cmexp1", " ");
                    clsSale.Set_Fields("cmexp15", " ");
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // copy outbuilding stuff
                clsSource.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
                clsSale.Execute("delete from sroutbuilding where rsaccount = " + FCConvert.ToString(lngAccount) + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from sroutbuilding where rsaccount = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    // clsSale.Update
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    // clsSale.Fields("saleid") = lngID
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                // book and page stuff
                clsSource.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAccount) + " order by card,line", modGlobalVariables.strREDatabase);
                clsSale.OpenRecordset("select * from srbookpage where account = -10", modGlobalVariables.strREDatabase);
                while (!clsSource.EndOfFile())
                {
                    clsSale.AddNew();
                    // clsSale.Update
                    CopyDataConnection(ref clsSource, ref clsSale);
                    // clsSale.Edit
                    clsSale.Set_Fields("saledate", dtSaleDate);
                    clsSale.Update();
                    clsSource.MoveNext();
                }
                CreateSalesRecord = true;
                return CreateSalesRecord;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Create Sales Record", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CreateSalesRecord;
        }

        public static bool AccountExists(ref int lngAccount)
        {
            bool AccountExists = false;
            clsDRWrapper clsAccount = new clsDRWrapper();
            AccountExists = false;
            clsAccount.OpenRecordset("select rsaccount from master where rsaccount = " + FCConvert.ToString(lngAccount), modGlobalVariables.strREDatabase);
            if (!clsAccount.EndOfFile())
            {
                AccountExists = true;
            }
            return AccountExists;
        }
        // vbPorter upgrade warning: boolfirstChange As Variant --> As bool
        public static void CompareFieldValues(ref clsDRWrapper clsNew, ref clsDRWrapper clsOrig, string strFieldName, string strTableName, DateTime dtPendingDate, int lngTransactionID, bool boolfirstChange)
        {
            // compare field values and insert into pending if necessary
            clsDRWrapper clsSavePending = new clsDRWrapper();
            bool boolChanged;
            string strValue = "";
            string strCols;
            string strSQL = "";
            int fldType;
            try
            {
                // On Error GoTo ErrorHandler
                fldType = clsNew.ConvertTypeToDBType(clsNew.Get_FieldsDataType(strFieldName));
                strCols = "Insert into pendingchanges (TransactionNumber,Account,Card,CardID,TableName,FieldName,FieldValue,FieldType,MinDate) values (";
                boolChanged = false;
                if (fldType == FCConvert.ToInt32(DataTypeEnum.dbLong) || fldType == FCConvert.ToInt32(DataTypeEnum.dbInteger) || fldType == FCConvert.ToInt32(DataTypeEnum.dbDouble) || fldType == FCConvert.ToInt32(DataTypeEnum.dbFloat) || fldType == FCConvert.ToInt32(DataTypeEnum.dbNumeric))
                {
                    // a number field
                    if (Conversion.Val(clsNew.Get_Fields(strFieldName)) != Conversion.Val(clsOrig.Get_Fields(strFieldName)))
                    {
                        boolChanged = true;
                        strValue = FCConvert.ToString(Conversion.Val(clsNew.Get_Fields(strFieldName)));
                    }
                }
                else if (fldType == FCConvert.ToInt32(DataTypeEnum.dbBoolean))
                {
                    if (clsNew.Get_Fields(strFieldName) && !clsOrig.Get_Fields(strFieldName))
                    {
                        boolChanged = true;
                        strValue = "True";
                    }
                    else if (!clsNew.Get_Fields(strFieldName) && clsOrig.Get_Fields(strFieldName))
                    {
                        boolChanged = true;
                        strValue = "False";
                    }
                }
                else if (fldType == FCConvert.ToInt32(DataTypeEnum.dbDate))
                {
                    if (Information.IsDate(clsNew.Get_Fields(strFieldName) + ""))
                    {
                        //FC:FINAL:MSH - issue #1763: can't compare DateTime and string. Replace comparing to avoid exceptions
                        //if (clsNew.Get_Fields(strFieldName) != clsOrig.Get_Fields(strFieldName))
                        if (clsNew.Get_Fields_DateTime(strFieldName).ToOADate() != clsOrig.Get_Fields_DateTime(strFieldName).ToOADate())
                        {
                            boolChanged = true;
                            if (clsNew.Get_Fields_DateTime(strFieldName).ToOADate() == 0)
                            {
                                strValue = "'0'";
                            }
                            else
                            {
                                strValue = "'" + Strings.Format(clsNew.Get_Fields(strFieldName), "MM/dd/yyyy") + "'";
                            }
                        }
                    }
                }
                else
                {
                    // a text or memo field
                    if ((clsNew.Get_Fields(strFieldName) + "") != (clsOrig.Get_Fields(strFieldName)))
                    {
                        boolChanged = true;
                        if (fecherFoundation.FCUtils.IsNull(clsNew.Get_Fields(strFieldName)))
                        {
                            strValue = "''";
                        }
                        else
                        {
                            strValue = "'" + modGlobalFunctions.EscapeQuotes(clsNew.Get_Fields(strFieldName)) + "'";
                        }
                    }
                }
                if (boolChanged)
                {
                    if (boolfirstChange)
                    {
                        // do an addnew to get the id number
                        clsSavePending.OpenRecordset("select * from pendingchanges where id = -1", modGlobalVariables.strREDatabase);
                        clsSavePending.AddNew();
                        clsSavePending.Update();
                        clsSavePending.Set_Fields("TransactionNumber", clsSavePending.Get_Fields_Int32("id"));
                        lngTransactionID = FCConvert.ToInt32(clsSavePending.Get_Fields_Int32("id"));
                        clsSavePending.Update();
                        boolfirstChange = false;
                        strSQL = "Update pendingchanges set account = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + ",card = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard");
                        strSQL += ",cardid = " + modDataTypes.Statics.MR.Get_Fields_Int32("id") + ",tablename = '" + strTableName + "'";
                        strSQL += ",fieldname = '" + strFieldName + "',fieldvalue = " + strValue;
                        strSQL += ",mindate = '" + Strings.Format(dtPendingDate, "MM/dd/yyyy") + "'";
                        strSQL += ",fieldtype = " + FCConvert.ToString(fldType);
                        strSQL += " where id = " + FCConvert.ToString(lngTransactionID);
                    }
                    else
                    {
                        strSQL = strCols;
                        strSQL += FCConvert.ToString(lngTransactionID);
                        strSQL += "," + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount");
                        strSQL += "," + modDataTypes.Statics.MR.Get_Fields_Int32("rscard");
                        strSQL += "," + modDataTypes.Statics.MR.Get_Fields_Int32("id");
                        strSQL += ",'" + strTableName + "'";
                        strSQL += ",'" + strFieldName + "'";
                        strSQL += "," + strValue;
                        strSQL += "," + FCConvert.ToString(fldType);
                        strSQL += ",'" + Strings.Format(dtPendingDate, "MM/dd/yyyy") + "'";
                        strSQL += ")";
                    }
                    clsSavePending.Execute(strSQL, modGlobalVariables.strREDatabase);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In compare field values", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        //public static string ReturnFileNameNoPath(ref string strFullName)
        //{
        //    string ReturnFileNameNoPath = "";
        //    string strFilename;
        //    // takes a full pathname and returns just the filename
        //    ReturnFileNameNoPath = "";
        //    strFilename = strFullName;
        //    while (Strings.InStr(1, strFilename, "\\", CompareConstants.vbTextCompare) > 0)
        //    {
        //        strFilename = Strings.Mid(strFilename, Strings.InStr(1, strFilename, "\\", CompareConstants.vbTextCompare) + 1);
        //    }
        //    ReturnFileNameNoPath = strFilename;
        //    return ReturnFileNameNoPath;
        //}


        public static string GetFullPictureName(string strPicture)
        {
            string GetFullPictureName = "";
            string strPicName;
            string strPathName = "";
            //FileSystemObject fso = new FileSystemObject();
            clsDRWrapper clsLoad = new clsDRWrapper();
            // takes a filename which may or may not have path information
            // and returns the full path that it was actually found in
            // the only paths it searches for are the specified path and the default picture directories
            strPicName = strPicture;
            GetFullPictureName = "";
            if (Strings.InStr(1, strPicName, "\\", CompareConstants.vbTextCompare) == 0)
            {
                strPathName = "";
            }
            else
            {
                strPathName = Strings.Mid(strPicName, 1, Strings.InStrRev(strPicName, "\\", -1, CompareConstants.vbTextCompare/*?*/));
            }
            if (strPathName != string.Empty)
            {
                strPicName = Strings.Right(strPicName, strPicName.Length - (Strings.InStrRev(strPicName, "\\", -1, CompareConstants.vbTextCompare/*?*/)));
            }
            strPathName += strPicName;
            if (FCFileSystem.FileExists(strPathName))
            {
                GetFullPictureName = strPathName;
                return GetFullPictureName;
            }
            else
            {
                clsLoad.OpenRecordset("select * from defaultpicturelocations", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    strPathName = FCConvert.ToString(clsLoad.Get_Fields_String("location"));
                    if (Strings.Trim(strPathName) != string.Empty)
                    {
                        if (Strings.Right(strPathName, 1) != "\\")
                            strPathName += "\\";
                        if (FCFileSystem.FileExists(strPathName + strPicName))
                        {
                            GetFullPictureName = Strings.Trim(strPathName + strPicName);
                            return GetFullPictureName;
                        }
                    }
                    clsLoad.MoveNext();
                }
            }
            return GetFullPictureName;
        }

        //public static string GetListOfSketchesByAccount_6(int lngAccount, bool boolCardOrder)
        //{
        //    return GetListOfSketchesByAccount(ref lngAccount, ref boolCardOrder);
        //}

        //public static string GetListOfSketchesByAccount(ref int lngAccount, ref bool boolCardOrder)
        //{
        //    string GetListOfSketchesByAccount = "";
        //    string strList = "";
        //    string strComma = "";
        //    string strPathName = "";
        //    string strFilename = "";
        //    string strFileFound = "";
        //    string strTemp = "";
        //    clsDRWrapper rsLoad = new clsDRWrapper();
        //    int lngAutoID = 0;
        //    int intIndex = 0;
        //    string[] strAryList = null;
        //    FCCollection[] colList = null;
        //    int intCounter;
        //    int intMaxBound = 0;
        //    GetListOfSketchesByAccount = "";
        //    if (lngAccount > 0)
        //    {
        //        rsLoad.OpenRecordset("select id from master where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
        //        strPathName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches");
        //        strComma = "";
        //        intIndex = 0;
        //        colList = new FCCollection[1 + 1];
        //        while (!rsLoad.EndOfFile())
        //        {
        //            strTemp = "0000000";
        //            if (intIndex > 1)
        //            {
        //                Array.Resize(ref colList, intIndex + 1);
        //            }
        //            lngAutoID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
        //            fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(Conversion.Hex(lngAutoID)) + 1, Strings.Len(Conversion.Hex(lngAutoID)), Convert.ToString(FCConvert.ToInt32(lngAutoID), 16).ToUpper());
        //            strFilename = strTemp + "*.bmp";
        //            strFileFound = FCFileSystem.Dir(Path.Combine(strPathName, strFilename), 0);
        //            while (strFileFound != string.Empty)
        //            {
        //                colList[intIndex].Add(strFileFound);
        //                strFileFound = FCFileSystem.Dir();
        //            }
        //            rsLoad.MoveNext();
        //            intIndex += 1;
        //        }
        //        strComma = "";
        //        if (boolCardOrder)
        //        {
        //            for (intIndex = 0; intIndex <= Information.UBound(colList, 1); intIndex++)
        //            {
        //                if (!(colList[intIndex] == null))
        //                {
        //                    if (colList[intIndex].Count > 0)
        //                    {
        //                        for (intCounter = 1; intCounter <= colList[intIndex].Count; intCounter++)
        //                        {
        //                            strList += strComma + colList[intIndex][intCounter];
        //                            strComma = ",";
        //                        }
        //                        // intCounter
        //                    }
        //                }
        //            }
        //            // intIndex
        //        }
        //        else
        //        {
        //            for (intIndex = 0; intIndex <= Information.UBound(colList, 1); intIndex++)
        //            {
        //                //FC:FINAL:RPU:#i1029 - Check if is not null first
        //                //if (colList[intIndex].Count > intMaxBound)
        //                if (colList[intIndex] != null && colList[intIndex].Count > intMaxBound)
        //                {
        //                    intMaxBound = colList[intIndex].Count;
        //                }
        //            }
        //            // intIndex
        //            for (intCounter = 1; intCounter <= intMaxBound; intCounter++)
        //            {
        //                for (intIndex = 0; intIndex <= Information.UBound(colList, 1); intIndex++)
        //                {
        //                    if (colList[intIndex].Count >= intCounter)
        //                    {
        //                        strList += strComma + colList[intIndex][intCounter];
        //                        strComma = ",";
        //                    }
        //                }
        //                // intIndex
        //            }
        //            // intCounter
        //        }
        //    }
        //    GetListOfSketchesByAccount = strList;
        //    return GetListOfSketchesByAccount;
        //}
        // vbPorter upgrade warning: lngAutoID As int	OnWrite(string, int)
        //public static string GetListOfSketches_2(int lngAutoID)
        //{
        //    return GetListOfSketches(ref lngAutoID);
        //}

        //public static string GetListOfSketches(ref int lngAutoID)
        //{
        //    string GetListOfSketches = "";
        //    // takes an auto id for a card and returns all sketch files (jpg's) for it in a comma delimited string
        //    string strList = "";
        //    string strComma;
        //    string strPathName;
        //    string strFilename;
        //    string strFileFound;
        //    string strTemp;
        //    GetListOfSketches = "";
        //    strComma = "";
        //    strPathName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches");
        //    strTemp = "0000000";
        //    fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(Conversion.Hex(lngAutoID)) + 1, Strings.Len(Conversion.Hex(lngAutoID)), Convert.ToString(FCConvert.ToInt32(lngAutoID), 16).ToUpper());
        //    strFilename = strTemp + "*.bmp";
        //    strFileFound = FCFileSystem.Dir(Path.Combine(strPathName, strFilename), 0);
        //    while (strFileFound != string.Empty)
        //    {
        //        strList += strComma + strFileFound;
        //        strComma = ",";
        //        strFileFound = FCFileSystem.Dir();
        //    }
        //    GetListOfSketches = strList;
        //    return GetListOfSketches;
        //}

        //public static string GetListOfSketchesWithPath(ref int lngAutoID)
        //{
        //    string GetListOfSketchesWithPath = "";
        //    // takes an auto id for a card and returns all sketch files (jpg's) for it in a comma delimited string
        //    string strList = "";
        //    string strComma;
        //    string strPathName;
        //    string strFilename;
        //    string strFileFound;
        //    string strTemp;
        //    GetListOfSketchesWithPath = "";
        //    strComma = "";
        //    strPathName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches");
        //    strTemp = "0000000";
        //    fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(Conversion.Hex(lngAutoID)) + 1, Strings.Len(Conversion.Hex(lngAutoID)), Convert.ToString(FCConvert.ToInt32(lngAutoID), 16).ToUpper());
        //    strFilename = strTemp + "*.bmp";
        //    strFileFound = FCFileSystem.Dir(Path.Combine(strPathName, strFilename), 0);
        //    while (strFileFound != string.Empty)
        //    {
        //        strList += strComma + strPathName + strFileFound;
        //        strComma = ",";
        //        strFileFound = FCFileSystem.Dir();
        //    }
        //    GetListOfSketchesWithPath = strList;
        //    return GetListOfSketchesWithPath;
        //}

        //public static string GetListOfPictures(ref int lngAcct, ref short intCard, int intLimit = 0)
        //{
        //    string GetListOfPictures = "";
        //    // returns a ^ delimited list of all pictures linked to the card
        //    clsDRWrapper clsLoad = new clsDRWrapper();
        //    string strTemp;
        //    string strList;
        //    int x = 0;
        //    GetListOfPictures = "";
        //    strTemp = "";
        //    strList = "";
        //    clsLoad.OpenRecordset("select * from picturerecord where mraccountnumber = " + FCConvert.ToString(lngAcct) + " and RScard = " + FCConvert.ToString(intCard) + " order by picnum", modGlobalVariables.strREDatabase);
        //    if (intLimit <= 0)
        //    {
        //        x = 0;
        //    }
        //    else
        //    {
        //        x = 1;
        //    }
        //    while (!clsLoad.EndOfFile() && x <= intLimit)
        //    {
        //        if (intLimit > 0)
        //        {
        //            x += 1;
        //        }
        //        strTemp = GetFullPictureName_2(clsLoad.Get_Fields_String("picturelocation"));
        //        if (Strings.Trim(strTemp) != string.Empty)
        //        {
        //            strList += strTemp + "^";
        //        }
        //        clsLoad.MoveNext();
        //    }
        //    if (strList != string.Empty)
        //    {
        //        // get rid of last comma
        //        strList = Strings.Mid(strList, 1, strList.Length - 1);
        //    }
        //    if (Strings.Trim(strList) != string.Empty)
        //    {
        //        GetListOfPictures = strList;
        //    }
        //    return GetListOfPictures;
        //}

        //private static bool ParseBookPage(ref string strRef, ref string strBook, ref string strPage, ref DateTime dtBPDate)
        //{
        //    bool ParseBookPage = false;
        //    int intpos;
        //    string strLeftOver = "";
        //    string strTemp = "";
        //    string strTemp2 = "";
        //    int x;
        //    int intMonth;
        //    int lngYear;
        //    ParseBookPage = false;
        //    intpos = Strings.InStr(1, strRef, "B", CompareConstants.vbTextCompare);
        //    if (intpos > 0)
        //    {
        //        strRef = Strings.Mid(strRef, intpos);
        //    }
        //    else
        //    {
        //        return ParseBookPage;
        //    }
        //    intpos = Strings.InStr(1, strRef, "P", CompareConstants.vbTextCompare);
        //    if (intpos <= 0)
        //        return ParseBookPage;
        //    // there is a b and a p.
        //    if (intpos > 2)
        //    {
        //        strTemp = Strings.Trim(Strings.Mid(strRef, 2, intpos - 2));
        //        // if this isn't numeric then exit
        //        if (!Information.IsNumeric(strTemp))
        //        {
        //            // check if it is bk, this could still be legit
        //            if (intpos > 3)
        //            {
        //                if (Strings.UCase(Strings.Mid(strTemp, 1, 1)) == "K")
        //                {
        //                    strTemp = Strings.Trim(Strings.Mid(strRef, 3, intpos - 3));
        //                    if (!Information.IsNumeric(strTemp))
        //                    {
        //                        return ParseBookPage;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                return ParseBookPage;
        //            }
        //        }
        //        if (strTemp.Length > 7)
        //            return ParseBookPage;
        //        strBook = strTemp;
        //        if (strBook == "")
        //            return ParseBookPage;
        //        // we have the book, now parse the page
        //        if (strRef.Length <= intpos)
        //            return ParseBookPage;
        //        // there is at least one char after the p
        //        strTemp = Strings.Trim(Strings.Mid(strRef, intpos + 1));
        //        if (strTemp == string.Empty)
        //            return ParseBookPage;
        //        if (Strings.UCase(Strings.Mid(strTemp, 1, 1)) == "G")
        //        {
        //            if (strTemp.Length > 1)
        //            {
        //                strTemp = Strings.Trim(Strings.Mid(strTemp, 2));
        //                if (strTemp == string.Empty)
        //                    return ParseBookPage;
        //            }
        //            else
        //            {
        //                return ParseBookPage;
        //            }
        //        }
        //        if (strTemp.Length > 7)
        //            return ParseBookPage;
        //        strPage = strTemp;
        //        if (strPage != "")
        //        {
        //            ParseBookPage = true;
        //        }
        //        else
        //        {
        //            return ParseBookPage;
        //        }
        //        // for now don't try to convert a date
        //        // nothing left of the ref to parse
        //        strRef = "";
        //        return ParseBookPage;
        //    }
        //    else
        //    {
        //        return ParseBookPage;
        //    }
        //    return ParseBookPage;
        //}

        //private static string Convert6CharsToDate(ref string strDate)
        //{
        //    string Convert6CharsToDate = "";
        //    string strTemp;
        //    int intMonth;
        //    int intDay;
        //    int lngYear;
        //    Convert6CharsToDate = "";
        //    strTemp = Strings.Trim(strDate);
        //    if (strTemp.Length < 6)
        //    {
        //        return Convert6CharsToDate;
        //    }
        //    else if (strTemp.Length > 6)
        //    {
        //        if (Information.IsNumeric(Strings.Mid(strTemp, 7, 1)))
        //        {
        //            return Convert6CharsToDate;
        //        }
        //        else
        //        {
        //            strTemp = Strings.Mid(strTemp, 1, 6);
        //            if (!Information.IsNumeric(strTemp))
        //                return Convert6CharsToDate;
        //        }
        //    }
        //    else
        //    {
        //        if (!Information.IsNumeric(strTemp))
        //            return Convert6CharsToDate;
        //    }
        //    // strtemp is now 6 numeric chars
        //    intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, 1, 2))));
        //    intDay = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, 3, 2))));
        //    lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, 5, 2))));
        //    if ((2000 + lngYear) > DateTime.Today.Year)
        //    {
        //        lngYear = 1900 + lngYear;
        //    }
        //    else
        //    {
        //        lngYear = 2000 + lngYear;
        //    }
        //    if (Information.IsDate(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(intDay) + "/" + FCConvert.ToString(lngYear)))
        //    {
        //        Convert6CharsToDate = FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(intDay) + "/" + FCConvert.ToString(lngYear);
        //    }
        //    return Convert6CharsToDate;
        //}

        //public static string GetFieldsDescription(ref string strTable, ref string strField)
        //{
        //    var strTemp = Strings.UCase(strField);

        //    int lngTemp = 0;
        //    var strDesc = "";

        //    var table = Strings.UCase(strTable);
        //    switch (table)
        //    {
        //        case "MASTER":
        //            {
        //                if (Strings.Left(strTemp + "      ", 6) == "PILAND")
        //                {
        //                    lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, 7, 1))));
        //                    strTemp = Strings.Mid(strTemp, 8);
        //                }

        //                var land = "Land " + FCConvert.ToString(lngTemp);

        //                switch (strTemp)
        //                {
        //                    case "LASTBLDGVAL":
        //                        strDesc = "Building Billing Value";

        //                        break;
        //                    case "LASTLANDVAL":
        //                        strDesc = "Land Billing Value";

        //                        break;
        //                    case "PINEIGHBORHOOD":
        //                        strDesc = "Neighborhood";

        //                        break;
        //                    case "PISTREETCODE":
        //                        strDesc = "Tree Growth Year";

        //                        break;
        //                    case "PIXCOORD":
        //                        break;
        //                    case "PIYCOORD":
        //                        break;
        //                    case "PIZONE":
        //                        strDesc = "Zone";

        //                        break;
        //                    case "PISECZONE":
        //                        strDesc = "Secondary Zone";

        //                        break;
        //                    case "PITOPOGRAPHY1":
        //                        strDesc = "Topography 1";

        //                        break;
        //                    case "PITOPOGRAPHY2":
        //                        strDesc = "Topography 2";

        //                        break;
        //                    case "PIUTILITIES1":
        //                        strDesc = "Utilities 1";

        //                        break;
        //                    case "PIUTILITIES2":
        //                        strDesc = "Utilities 2";

        //                        break;
        //                    case "PISTREET":
        //                        strDesc = "Street Code";

        //                        break;
        //                    case "PIOPEN1":
        //                        strDesc = "Open 1 Field";

        //                        break;
        //                    case "PIOPEN2":
        //                        strDesc = "Open 2 Field";

        //                        break;
        //                    case "PISALEPRICE":
        //                        strDesc = "Sale Price";

        //                        break;
        //                    case "PISALETYPE":
        //                        strDesc = "Sale Type";

        //                        break;
        //                    case "PISALEFINANCING":
        //                        strDesc = "Sale Financing";

        //                        break;
        //                    case "PISALEVERIFIED":
        //                        strDesc = "Sale Verified";

        //                        break;
        //                    case "PISALEVALIDITY":
        //                        strDesc = "Sale Validity";

        //                        break;
        //                    case "TYPE":
        //                        strDesc = $"{land} Type";

        //                        break;
        //                    case "INF":
        //                        strDesc = $"{land} Influence";

        //                        break;
        //                    case "INFCODE":
        //                        strDesc = $"{land} Influence Code";

        //                        break;
        //                    case "RSNAME":
        //                        strDesc = "Name";

        //                        break;
        //                }

        //                break;
        //            }
        //        case "DWELLING":
        //        {
        //            switch (strTemp)
        //            {
        //                case "DISTYLE":
        //                    strDesc = "Dwelling Style";

        //                    break;
        //                case "DIUNITSDWELLING":
        //                    strDesc = "Dwelling Units";

        //                    break;
        //                case "DIUNITSOTHER":
        //                    strDesc = "Dwelling Other Units";

        //                    break;
        //                case "DISTORIES":
        //                    strDesc = "Dwelling Stories";

        //                    break;
        //                case "DIEXTWALLS":
        //                    strDesc = "Dwelling Exterior Walls";

        //                    break;
        //                case "DIROOF":
        //                    strDesc = "Dwelling Roof";

        //                    break;
        //                case "DIMASONRY":
        //                    strDesc = "Dwelling Masonry";

        //                    break;
        //                case "DIOPEN3":
        //                    strDesc = "Dwelling Open 3";

        //                    break;
        //                case "DIOPEN4":
        //                    strDesc = "Dwelling Open 4";

        //                    break;
        //                case "DIYEARBUILT":
        //                    strDesc = "Dwelling Year Built";

        //                    break;
        //                case "DIYEARREMODEL":
        //                    strDesc = "Dwelling Year Remodel";

        //                    break;
        //                case "DIFOUNDATION":
        //                    strDesc = "Dwelling Foundation";

        //                    break;
        //                case "DIBSMT":
        //                    strDesc = "Dwelling Basement";

        //                    break;
        //                case "DIBSMTGAR":
        //                    strDesc = "Dwelling Basement Garage";

        //                    break;
        //                case "DIWETBSMT":
        //                    strDesc = "Dwelling Wet Basement";

        //                    break;
        //                case "DISFBSMTLIVING":
        //                    strDesc = "Dwelling SQFT Basement Liv.";

        //                    break;
        //                case "DIBSMTFINGRADE1":
        //                    strDesc = "Dwelling Bsmt Fin Grade";

        //                    break;
        //                case "DIBSMTFINGRADE2":
        //                    strDesc = "Dwelling Bsmt Fin Grade Pct";

        //                    break;
        //                case "DIOPEN5":
        //                    strDesc = "Dwelling Open 5";

        //                    break;
        //                case "DIHEAT":
        //                    strDesc = "Dwelling Heat";

        //                    break;
        //                case "DIPCTHEAT":
        //                    strDesc = "Dwelling Percent Heated";

        //                    break;
        //                case "DICOOL":
        //                    strDesc = "Dwelling Cool";

        //                    break;
        //                case "DIPCTCOOL":
        //                    strDesc = "Dwelling Percent Cooled";

        //                    break;
        //                case "DIKITCHENS":
        //                    strDesc = "Dwelling Kitchen Style";

        //                    break;
        //                case "DIBATHS":
        //                    strDesc = "Dwelling Bath Style";

        //                    break;
        //                case "DIROOMS":
        //                    strDesc = "Dwelling Rooms";

        //                    break;
        //                case "DIBEDROOMS":
        //                    strDesc = "Dwelling Bedrooms";

        //                    break;
        //                case "DIFULLBATHS":
        //                    strDesc = "Dwelling Full Baths";

        //                    break;
        //                case "DIHALFBATHS":
        //                    strDesc = "Dwelling Half Baths";

        //                    break;
        //                case "DIADDNFIXTURES":
        //                    strDesc = "Dwelling Additional Fixtures";

        //                    break;
        //                case "DIFIREPLACES":
        //                    strDesc = "Dwelling Fireplaces";

        //                    break;
        //                case "DILAYOUT":
        //                    strDesc = "Dwelling Layout";

        //                    break;
        //                case "DIATTIC":
        //                    strDesc = "Dwelling Attic";

        //                    break;
        //                case "DIINSULATION":
        //                    strDesc = "Dwelling Insulation";

        //                    break;
        //                case "DIPCTUNFINISHED":
        //                    strDesc = "Dwelling Percent Unfinished";

        //                    break;
        //                case "DIGRADE1":
        //                    strDesc = "Dwelling Grade";

        //                    break;
        //                case "DIGRADE2":
        //                    strDesc = "Dwelling Grade Percent";

        //                    break;
        //                case "DISQFT":
        //                    strDesc = "Dwelling Square Feet";

        //                    break;
        //                case "DICONDITION":
        //                    strDesc = "Dwelling Condition";

        //                    break;
        //                case "DIPCTPHYS":
        //                    strDesc = "Dwelling Physical Percent";

        //                    break;
        //                case "DIPCTFUNCT":
        //                    strDesc = "Dwelling Functional Percent";

        //                    break;
        //                case "DIFUNCTCODE":
        //                    strDesc = "Dwelling Functional Code";

        //                    break;
        //                case "DIPCTECON":
        //                    strDesc = "Dwelling Economic Percent";

        //                    break;
        //                case "DIECONCODE":
        //                    strDesc = "Dwelling Economic Code";

        //                    break;
        //            }

        //            break;
        //        }
        //        case "COMMERCIAL":
        //            {
        //                string strWhich = "";
        //                if (Strings.Left(strTemp, 2) == "C1" || Strings.Left(strTemp, 2) == "C2" || Strings.Left(strTemp, 2) == "CM")
        //                {
        //                    // strip of the first two chars
        //                    strWhich = Strings.Mid(strTemp, 2, 1);
        //                    strTemp = Strings.Mid(strTemp, 3);
        //                }
        //                else
        //                {
        //                    // this is occ1,occ2,dwel1 or dwel2 so strip off last char
        //                    strWhich = Strings.Right(strTemp, 1);
        //                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
        //                }

        //                var commercial = "Commercial " + strWhich;

        //                switch (strTemp)
        //                {
        //                    case "OCC":
        //                        strDesc = $"{commercial} Occupancy";

        //                        break;
        //                    case "DWEL":
        //                        strDesc = $"{commercial} Dwelling Units";

        //                        break;
        //                    case "CLASS":
        //                        strDesc = $"{commercial} Class";

        //                        break;
        //                    case "QUALITY":
        //                        strDesc = $"{commercial} Class Quality";

        //                        break;
        //                    case "GRADE":
        //                        strDesc = $"{commercial} Grade";

        //                        break;
        //                    case "EXTWALLS":
        //                        strDesc = $"{commercial} Exterior Walls";

        //                        break;
        //                    case "STORIES":
        //                        strDesc = $"{commercial} Stories";

        //                        break;
        //                    case "HEIGHT":
        //                        strDesc = $"{commercial} Stories Height";

        //                        break;
        //                    case "FLOOR":
        //                        strDesc = $"{commercial} Floor Area";

        //                        break;
        //                    case "PERIMETER":
        //                        strDesc = $"{commercial} Perimeter";

        //                        break;
        //                    case "HEAT":
        //                        strDesc = $"{commercial} Heat";

        //                        break;
        //                    case "BUILT":
        //                        strDesc = $"{commercial} Year Built";

        //                        break;
        //                    case "REMODEL":
        //                        strDesc = $"{commercial} Year Remodel";

        //                        break;
        //                    case "CONDITION":
        //                        strDesc = $"{commercial} Condition";

        //                        break;
        //                    case "PHYS":
        //                        strDesc = $"{commercial} Physical Percent";

        //                        break;
        //                    case "FUNCT":
        //                        strDesc = $"{commercial} Functional Percent";

        //                        break;
        //                    case "ECON":
        //                        strDesc = "Commercial Economic Percent";

        //                        break;
        //                }

        //                break;
        //            }
        //        case "OUTBUILDING":
        //            {
        //                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
        //                lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(strField, 1))));
        //                if (Strings.Right(strTemp, 1) == "1")
        //                {
        //                    strTemp = Strings.Mid(strField, 1, strTemp.Length - 1);
        //                    lngTemp = 10 + lngTemp;
        //                }

        //                var outbuilding = "Outbuilding " + FCConvert.ToString(lngTemp);

        //                switch (strTemp)
        //                {
        //                    case "OITYPE":
        //                        strDesc = $"{outbuilding} Type";

        //                        break;
        //                    case "OIYEAR":
        //                        strDesc = $"{outbuilding} Year";

        //                        break;
        //                    case "OIUNITS":
        //                        strDesc = $"{outbuilding} Units";

        //                        break;
        //                    case "OIGRADECD":
        //                        strDesc = $"{outbuilding} Grade";

        //                        break;
        //                    case "OIGRADEPCT":
        //                        strDesc = $"{outbuilding} Grade Pct";

        //                        break;
        //                    case "OICOND":
        //                        strDesc = $"{outbuilding} Condition";

        //                        break;
        //                    case "OIPCTPHYS":
        //                        strDesc = $"{outbuilding} Physical Pct";

        //                        break;
        //                    case "OIPCTFUNCT":
        //                        strDesc = $"{outbuilding} Functional Pct";

        //                        break;
        //                    case "OISOUNDVALUE":
        //                        strDesc = $"{outbuilding} Sound Value";

        //                        break;
        //                    case "OIUSESOUND":
        //                        strDesc = $"{outbuilding} Use Sound Value";

        //                        break;
        //                }

        //                break;
        //            }
        //        case "BOOKPAGE":
        //            {
        //                switch (strTemp)
        //                {
        //                    case "BOOK":
        //                        strDesc = "Book Number";

        //                        break;
        //                    case "PAGE":
        //                        strDesc = "Page Number";

        //                        break;
        //                    case "BPDATE":
        //                        strDesc = "Book Page Date";

        //                        break;
        //                    case "CURRENT":
        //                        strDesc = "Current Book & Page";

        //                        break;
        //                }

        //                break;
        //            }
        //    }

        //    return strDesc;
        //}
        // vbPorter upgrade warning: intLength As short	OnWriteFCConvert.ToInt32(
        public static string NumtoString_6(int lngNumber, short intLength)
        {
            return NumtoString(ref lngNumber, ref intLength);
        }

        public static string NumtoString(ref int lngNumber, ref short intLength)
        {
            string NumtoString = "";
            // makes a string (with commas) from lngnumber that is intlength characters
            string strNum = "";
            string strTemp = "";
            bool boolUseCommas;
            if (intLength < 1)
            {
                NumtoString = "";
                return NumtoString;
            }
            boolUseCommas = true;
            switch (intLength)
            {
                case 4:
                    {
                        if (lngNumber > 999)
                            boolUseCommas = false;
                        break;
                    }
                case 5:
                    {
                        if (lngNumber > 9999)
                            boolUseCommas = false;
                        break;
                    }
                case 6:
                    {
                        if (lngNumber > 99999)
                            boolUseCommas = false;
                        break;
                    }
                case 7:
                case 8:
                    {
                        if (lngNumber > 999999)
                            boolUseCommas = false;
                        break;
                    }
                case 9:
                    {
                        if (lngNumber > 9999999)
                            boolUseCommas = false;
                        break;
                    }
                case 10:
                    {
                        if (lngNumber > 99999999)
                            boolUseCommas = false;
                        break;
                    }
                case 11:
                case 12:
                    {
                        if (lngNumber > 999999999)
                            boolUseCommas = false;
                        break;
                    }
            }
            //end switch
            // THIS WILL PREVENT THE COMMAS FROM SHOWING IN THE OUTPRINT FILE
            if (boolUseCommas)
            {
                strNum = Strings.Format(lngNumber, "###,###,###,###,##0");
            }
            else
            {
                strNum = Strings.Format(lngNumber, "0");
            }
            if (strNum.Length >= intLength)
            {
                NumtoString = Strings.Right(strNum, intLength);
            }
            else
            {
                NumtoString = Strings.StrDup(intLength - strNum.Length, " ") + strNum;
            }
            return NumtoString;
        }

        public static string GetCategoryByCode_2(int lngCode)
        {
            return GetCategoryByCode(ref lngCode);
        }

        public static string GetCategoryByCode(ref int lngCode)
        {
            string GetCategoryByCode = "";
            // takes a category code and returns the description
            string strTemp;
            strTemp = "NONE";
            switch (lngCode)
            {
                case modcalcexemptions.CNSTEXEMPTCATNONE:
                    {
                        strTemp = "NONE";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATDISABLEDVET:
                    {
                        strTemp = "Disabled Vet";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATHOMESTEAD:
                    {
                        strTemp = "Homestead";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATKOREAN:
                    {
                        strTemp = "Korean";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET:
                    {
                        strTemp = "Paraplegic Vet";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATVETERAN:
                    {
                        strTemp = "Veteran (General)";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATVIETNAM:
                    {
                        strTemp = "Vietnam";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATWORLDWARI:
                    {
                        strTemp = "World War I";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATWORLDWARII:
                    {
                        strTemp = "World War II";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATBENEVCHARITABLE:
                    {
                        strTemp = "Benevolent/Charitable";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATBLIND:
                    {
                        strTemp = "Blind";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATCHAMBERCOMMERCE:
                    {
                        strTemp = "Chamber of Comm/Board of Trade";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATFRATERNAL:
                    {
                        strTemp = "Fraternal Organization";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATHOSPITALLEASED:
                    {
                        strTemp = "Hospital Leased";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATMUNIAIRPORT:
                    {
                        strTemp = "Municipal Airport";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATMUNICOUNTY:
                    {
                        strTemp = "Municipal/County";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATPARSONAGE:
                    {
                        strTemp = "Parsonage";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATRELIGIOUS:
                    {
                        strTemp = "Religious";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATSCIANDLIT:
                    {
                        strTemp = "Literary/Scientific";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATSTATE:
                    {
                        strTemp = "State of Maine";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATUNITEDSTATES:
                    {
                        strTemp = "United States";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATVETERANSORG:
                    {
                        strTemp = "Veterans Organization";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATANIMALWASTE:
                    {
                        strTemp = "Animal Waste Facility";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATPOLLUTIONCONTROL:
                    {
                        strTemp = "Pollution Control";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATQUASIMUNICIPAL:
                    {
                        strTemp = "Quasi Municipal";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATSEWAGEDISPOSAL:
                    {
                        strTemp = "Sewage Disposal";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATWATERPIPES:
                    {
                        strTemp = "Water Supply";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATNHWATER:
                    {
                        strTemp = "NH Water Board";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATPRIVATEAIRPORT:
                    {
                        strTemp = "Private Airport";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATHYDROOUTSIDEMUNI:
                    {
                        strTemp = "Hydro Outside Muni Corporation";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATWWIWIDOWER:
                    {
                        strTemp = "World War I Widower";
                        break;
                    }
                case modcalcexemptions.CNSTEXEMPTCATWWIIWIDOWER:
                    {
                        strTemp = "World War II Widower";
                        break;
                    }
            }
            //end switch
            GetCategoryByCode = strTemp;
            return GetCategoryByCode;
        }

        public static bool IsOdd_2(int lngVal)
        {
            return IsOdd(ref lngVal);
        }

        public static bool IsOdd(ref int lngVal)
        {
            bool IsOdd = false;
            IsOdd = false;
            if (lngVal % 2 == 1)
                IsOdd = true;
            return IsOdd;
        }

        public class StaticVariables
        {
            //=========================================================
            public bool boolMessageShown;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
