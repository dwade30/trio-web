﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptShortCard.
	/// </summary>
	public partial class rptShortCard : BaseSectionReport
	{
		public static rptShortCard InstancePtr
		{
			get
			{
				return (rptShortCard)Sys.GetInstance(typeof(rptShortCard));
			}
		}

		protected rptShortCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptShortCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Short Screen Print";
		}
		// nObj = 1
		//   0	rptShortCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtAccount.Text = "Account: " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "  Card: " + FCConvert.ToString(modGNBas.Statics.gintCardNumber);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			string strBookPage = "";
			txtAddr1.Text = frmSHREBLUpdate.InstancePtr.lblAddress;
			txtLocation.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRRSLOCNUMALPH.Text + frmSHREBLUpdate.InstancePtr.txtMRRSLOCAPT.Text) + " " + Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRRSLOCSTREET.Text);
			txtMapLot.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRRSMAPLOT.Text);
            txtDeedName1.Text = frmSHREBLUpdate.InstancePtr.txtDeedName1.Text;
            txtDeedName2.Text = frmSHREBLUpdate.InstancePtr.txtDeedName2.Text;
			txtRef1.Text = frmSHREBLUpdate.InstancePtr.txtMRRSREF1.Text;
			txtRef2.Text = frmSHREBLUpdate.InstancePtr.txtMRRSREF2.Text;
			txtLandCode.Text = FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridLandCode.TextMatrix(0, 0)));
			txtBldgCode.Text = FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridBldgCode.TextMatrix(0, 0)));
			txtTranCode.Text = FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridTranCode.TextMatrix(0, 0)));
			txtLandDesc.Text = frmSHREBLUpdate.InstancePtr.gridLandCode.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay);
			txtBldgDesc.Text = frmSHREBLUpdate.InstancePtr.gridBldgCode.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay);
			// txtTranDesc.Text = Trim(.lblMRPITRAN.Caption)
			txtTranDesc.Text = frmSHREBLUpdate.InstancePtr.gridTranCode.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay);

			for (x = 1; x <= 3; x++)
			{
				if (Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 0)) > -1)
				{
					(Detail.Controls["txtExempt" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 0);
					(Detail.Controls["txtExempt" + x + "Desc"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmSHREBLUpdate.InstancePtr.GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, x, 0);
				}
				else
				{
					(Detail.Controls["txtExempt" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "0";
					(Detail.Controls["txtExempt" + x + "Desc"] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
			}
			// x
			txtOpen1Desc.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.Open1Label.Text);
			txtOpen2Desc.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.Open2Label.Text);
			txtOpen1.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRPIOPEN1.Text);
			txtOpen2.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRPIOPEN2.Text);
			if (Conversion.Val(txtOpen1.Text) == 0 && txtOpen1Desc.Text == "")
			{
				txtOpen1.Text = "";
			}
			if (Conversion.Val(txtOpen2.Text) == 0 && txtOpen2Desc.Text == "")
			{
				txtOpen2.Text = "";
			}
			txtEntranceCode.Text = FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIENTRANCECODE.Text));
			txtInformationCode.Text = FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIINFORMATION.Text));
			txtInspectionDate.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text);
			txtEntranceDescription.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblMRDIENTRANCE.Text);
			txtInformationDescription.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblMRDIINFORMATION.Text);
			txtSoftWoodAcres.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 1));
			txtHardWoodAcres.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 3));
			txtMixedAcres.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 2));
			txtOtherAcres.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 4));
			txtTotalAcres.Text = frmSHREBLUpdate.InstancePtr.txtMRPIACRES.Text;
			txtSoftValue.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 1));
			txtHardValue.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 3));
			txtMixedValue.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 2));
			txtOtherValue.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 4));
			txtLand.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text);
			txtBuilding.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text);
			txtExemption.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text);
			txtLandAcct.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblMRRLLANDVAL.Text);
			txtBldgAcct.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblMRRLBLDGVAL.Text);
			txtExemptAcct.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblMRRLEXEMPTION.Text);
			txtTaxable.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblNetAssessment.Text);
			txtTaxRate.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblTaxRate.Text);
			txtTax.Text = Strings.Trim(frmSHREBLUpdate.InstancePtr.lblTaxAmount.Text);
			strBookPage = "";
			for (x = frmSHREBLUpdate.InstancePtr.BPGrid.Rows - 1; x >= 1; x--)
			{
				if (frmSHREBLUpdate.InstancePtr.BPGrid.TextMatrix(x, 1) != "" && frmSHREBLUpdate.InstancePtr.BPGrid.TextMatrix(x, 2) != "")
				{
					strBookPage += "B" + frmSHREBLUpdate.InstancePtr.BPGrid.TextMatrix(x, 1) + "P" + frmSHREBLUpdate.InstancePtr.BPGrid.TextMatrix(x, 2) + " ";
				}
			}
			// x
			txtBookPage.Text = Strings.Trim(strBookPage);
		}

		
	}
}
