﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTaxNotice.
	/// </summary>
	partial class rptTaxNotice
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxNotice));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.rtbMsg2 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMail1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMail2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMail4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMail3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTaxLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLandLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBldgLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExemptLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.rtbMsg1 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
            this.txtMail5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLandLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.rtbMsg2,
            this.txtTitle,
            this.Field1,
            this.Line1,
            this.txtLocation,
            this.txtMail1,
            this.txtMail2,
            this.txtMail4,
            this.txtMail3,
            this.txtMapLot,
            this.txtAccount,
            this.txtLand,
            this.txtBldg,
            this.txtExempt,
            this.txtTotal,
            this.txtTotalLabel,
            this.txtTax,
            this.txtTaxLabel,
            this.txtLandLabel,
            this.txtBldgLabel,
            this.txtExemptLabel,
            this.rtbMsg1,
            this.txtMail5});
            this.Detail.Height = 3.472222F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // rtbMsg2
            // 
            this.rtbMsg2.AutoReplaceFields = true;
            this.rtbMsg2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtbMsg2.Height = 1.552083F;
            this.rtbMsg2.Left = 3.895833F;
            this.rtbMsg2.Name = "rtbMsg2";
            this.rtbMsg2.RTF = resources.GetString("rtbMsg2.RTF");
            this.rtbMsg2.Top = 1.5F;
            this.rtbMsg2.Visible = false;
            this.rtbMsg2.Width = 3.572917F;
            // 
            // txtTitle
            // 
            this.txtTitle.CanGrow = false;
            this.txtTitle.Height = 0.19F;
            this.txtTitle.Left = 0.0625F;
            this.txtTitle.MultiLine = false;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: \'Tahoma\'; text-align: center";
            this.txtTitle.Text = null;
            this.txtTitle.Top = 0.0625F;
            this.txtTitle.Width = 7.25F;
            // 
            // Field1
            // 
            this.Field1.CanGrow = false;
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 0.0625F;
            this.Field1.MultiLine = false;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.Field1.Text = "* * * * * * * * * * *  T H I S   I S   N O T   A   T A X   B I L L * * * * * * * " +
    "* * * *";
            this.Field1.Top = 3.125F;
            this.Field1.Width = 7.25F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 3.375F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 3.375F;
            this.Line1.Y2 = 3.375F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.19F;
            this.txtLocation.Left = 0.3125F;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Style = "font-family: \'Tahoma\'";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 1.677083F;
            this.txtLocation.Width = 3.5F;
            // 
            // txtMail1
            // 
            this.txtMail1.Height = 0.19F;
            this.txtMail1.Left = 0.3125F;
            this.txtMail1.Name = "txtMail1";
            this.txtMail1.Style = "font-family: \'Tahoma\'";
            this.txtMail1.Text = "Field4";
            this.txtMail1.Top = 2.135417F;
            this.txtMail1.Width = 3.5F;
            // 
            // txtMail2
            // 
            this.txtMail2.Height = 0.19F;
            this.txtMail2.Left = 0.3125F;
            this.txtMail2.Name = "txtMail2";
            this.txtMail2.Style = "font-family: \'Tahoma\'";
            this.txtMail2.Text = "Field5";
            this.txtMail2.Top = 2.302083F;
            this.txtMail2.Width = 3.5F;
            // 
            // txtMail4
            // 
            this.txtMail4.Height = 0.19F;
            this.txtMail4.Left = 0.3125F;
            this.txtMail4.Name = "txtMail4";
            this.txtMail4.Style = "font-family: \'Tahoma\'";
            this.txtMail4.Text = "Field6";
            this.txtMail4.Top = 2.635417F;
            this.txtMail4.Width = 3.5F;
            // 
            // txtMail3
            // 
            this.txtMail3.Height = 0.19F;
            this.txtMail3.Left = 0.3125F;
            this.txtMail3.Name = "txtMail3";
            this.txtMail3.Style = "font-family: \'Tahoma\'";
            this.txtMail3.Text = "Field7";
            this.txtMail3.Top = 2.46875F;
            this.txtMail3.Width = 3.5F;
            // 
            // txtMapLot
            // 
            this.txtMapLot.CanGrow = false;
            this.txtMapLot.Height = 0.19F;
            this.txtMapLot.Left = 1.5625F;
            this.txtMapLot.MultiLine = false;
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtMapLot.Text = null;
            this.txtMapLot.Top = 1.510417F;
            this.txtMapLot.Width = 2.25F;
            // 
            // txtAccount
            // 
            this.txtAccount.CanGrow = false;
            this.txtAccount.Height = 0.19F;
            this.txtAccount.Left = 0.3125F;
            this.txtAccount.MultiLine = false;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 1.510417F;
            this.txtAccount.Width = 1.1875F;
            // 
            // txtLand
            // 
            this.txtLand.CanGrow = false;
            this.txtLand.Height = 0.19F;
            this.txtLand.Left = 3.9375F;
            this.txtLand.MultiLine = false;
            this.txtLand.Name = "txtLand";
            this.txtLand.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtLand.Text = null;
            this.txtLand.Top = 2.197917F;
            this.txtLand.Visible = false;
            this.txtLand.Width = 0.875F;
            // 
            // txtBldg
            // 
            this.txtBldg.CanGrow = false;
            this.txtBldg.Height = 0.19F;
            this.txtBldg.Left = 4.875F;
            this.txtBldg.MultiLine = false;
            this.txtBldg.Name = "txtBldg";
            this.txtBldg.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtBldg.Text = null;
            this.txtBldg.Top = 2.197917F;
            this.txtBldg.Visible = false;
            this.txtBldg.Width = 1.0625F;
            // 
            // txtExempt
            // 
            this.txtExempt.CanGrow = false;
            this.txtExempt.Height = 0.19F;
            this.txtExempt.Left = 6.25F;
            this.txtExempt.MultiLine = false;
            this.txtExempt.Name = "txtExempt";
            this.txtExempt.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtExempt.Text = null;
            this.txtExempt.Top = 2.197917F;
            this.txtExempt.Visible = false;
            this.txtExempt.Width = 0.9375F;
            // 
            // txtTotal
            // 
            this.txtTotal.Height = 0.19F;
            this.txtTotal.Left = 5.5625F;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtTotal.Text = "Field2";
            this.txtTotal.Top = 2.53125F;
            this.txtTotal.Visible = false;
            this.txtTotal.Width = 1.375F;
            // 
            // txtTotalLabel
            // 
            this.txtTotalLabel.Height = 0.19F;
            this.txtTotalLabel.Left = 4.125F;
            this.txtTotalLabel.Name = "txtTotalLabel";
            this.txtTotalLabel.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtTotalLabel.Text = "TOTAL:";
            this.txtTotalLabel.Top = 2.53125F;
            this.txtTotalLabel.Visible = false;
            this.txtTotalLabel.Width = 1F;
            // 
            // txtTax
            // 
            this.txtTax.CanGrow = false;
            this.txtTax.Height = 0.19F;
            this.txtTax.Left = 5.9375F;
            this.txtTax.MultiLine = false;
            this.txtTax.Name = "txtTax";
            this.txtTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtTax.Text = "Field2";
            this.txtTax.Top = 2.697917F;
            this.txtTax.Visible = false;
            this.txtTax.Width = 1F;
            // 
            // txtTaxLabel
            // 
            this.txtTaxLabel.CanGrow = false;
            this.txtTaxLabel.Height = 0.19F;
            this.txtTaxLabel.Left = 4.125F;
            this.txtTaxLabel.MultiLine = false;
            this.txtTaxLabel.Name = "txtTaxLabel";
            this.txtTaxLabel.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtTaxLabel.Text = "Estimated Tax";
            this.txtTaxLabel.Top = 2.697917F;
            this.txtTaxLabel.Visible = false;
            this.txtTaxLabel.Width = 1.8125F;
            // 
            // txtLandLabel
            // 
            this.txtLandLabel.CanGrow = false;
            this.txtLandLabel.Height = 0.19F;
            this.txtLandLabel.Left = 3.9375F;
            this.txtLandLabel.MultiLine = false;
            this.txtLandLabel.Name = "txtLandLabel";
            this.txtLandLabel.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtLandLabel.Text = "Land";
            this.txtLandLabel.Top = 2.03125F;
            this.txtLandLabel.Visible = false;
            this.txtLandLabel.Width = 0.875F;
            // 
            // txtBldgLabel
            // 
            this.txtBldgLabel.CanGrow = false;
            this.txtBldgLabel.Height = 0.19F;
            this.txtBldgLabel.Left = 4.875F;
            this.txtBldgLabel.MultiLine = false;
            this.txtBldgLabel.Name = "txtBldgLabel";
            this.txtBldgLabel.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtBldgLabel.Text = "Buildings";
            this.txtBldgLabel.Top = 2.03125F;
            this.txtBldgLabel.Visible = false;
            this.txtBldgLabel.Width = 1.0625F;
            // 
            // txtExemptLabel
            // 
            this.txtExemptLabel.CanGrow = false;
            this.txtExemptLabel.Height = 0.19F;
            this.txtExemptLabel.Left = 6.25F;
            this.txtExemptLabel.MultiLine = false;
            this.txtExemptLabel.Name = "txtExemptLabel";
            this.txtExemptLabel.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtExemptLabel.Text = "Exemption";
            this.txtExemptLabel.Top = 2.03125F;
            this.txtExemptLabel.Visible = false;
            this.txtExemptLabel.Width = 0.9375F;
            // 
            // rtbMsg1
            // 
            this.rtbMsg1.AutoReplaceFields = true;
            this.rtbMsg1.CanGrow = false;
            this.rtbMsg1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtbMsg1.Height = 1.1875F;
            this.rtbMsg1.Left = 0F;
            this.rtbMsg1.Name = "rtbMsg1";
            this.rtbMsg1.RTF = resources.GetString("rtbMsg1.RTF");
            this.rtbMsg1.Top = 0.2F;
            this.rtbMsg1.Width = 7.499306F;
            // 
            // txtMail5
            // 
            this.txtMail5.Height = 0.19F;
            this.txtMail5.Left = 0.3125F;
            this.txtMail5.Name = "txtMail5";
            this.txtMail5.Style = "font-family: \'Tahoma\'";
            this.txtMail5.Text = "Field5";
            this.txtMail5.Top = 2.791667F;
            this.txtMail5.Width = 3.5F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptTaxNotice
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLandLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtbMsg2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptLabel;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtbMsg1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
