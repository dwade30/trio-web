﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cREMaster
	{
		//=========================================================
		private int lngOwnerPartyID;
		private int lngSecOwnerPartyID;
		private int lngAccount;
		private int intCard;
		private string strAccountID = string.Empty;
		private string strCardId = string.Empty;
		private string strLocNum = string.Empty;
		private string strLocation = string.Empty;
		private string strDwellingCode = string.Empty;
		private string strMapLot = string.Empty;
		private double dblLastBldgVal;
		private double dblLastLandVal;
		private DateTime dtHlUpdate;
		private string strhiUpdCode = "";
		private double dblHLCompValBldg;
		private double dblHLCompValLand;
		private double dblHLValBldg;
		private int intHIValBldgCOde;
		private double dblHLValLand;
		private int intHIValLandCode;
		private int intHlAlt1Bldg;
		private int intHiAlt1BldgCode;
		private int intHlAlt2Bldg;
		private int intHlAlt2BldgCode;
		private int intNeighborhood;
		private int intStreetCode;
		private string strXCoord = string.Empty;
		private string strYCoord = string.Empty;
		private int intZone;
		private int intSecZone;
		private int[] intTopography = new int[2 + 1];
		private int[] intUtilities = new int[2 + 1];
		private int intStreet;
		private int intOpen1;
		private int intOpen2;
		private double dblSalePrice;
		private int intSaleType;
		private int intSaleFinancing;
		private int intSaleVerified;
		private int intSaleValidity;
		private double dblHlOlExemption;
		private int[] intLandType = new int[7 + 1];
		private string[] strLandUnitsA = new string[7 + 1];
		private string[] strLandUnitsB = new string[7 + 1];
		private double[] dblLandInf = new double[7 + 1];
		private int[] intLandInfCode = new int[7 + 1];
		private int[] intExemptCode = new int[3 + 1];
		private double dblAcres;
		private string strRSPropertyCode = "";
		private string strOutbuildingCode = "";
		private string strLocNumAlph = "";
		private string strLocApt = "";
		private string strLocStreet = "";
		private string strREF1 = string.Empty;
		private string strRef2 = string.Empty;
		private int intLandCode;
		private int intBldgCode;
		private double dblRLLandVal;
		private double dblRLBldgVal;
		private double dblCorrExemption;
		private double dblRLExemption;
		private int intTranCode;
		private string strPreviousMaster = "";
		private bool boolDeleted;
		private DateTime dtSaleDate;
		private bool boolTaxAcquired;
		private double dblRsSoft;
		private double dblrsHard;
		private double dblrsMixed;
		private double dblSoftValue;
		private double dblHardValue;
		private double dblMixedValue;
		private double dblHomesteadValue;
		private bool boolInBankruptcy;
		private double[] dblExemptValue = new double[3 + 1];
		private string strComment = "";

		public double HomesteadValue
		{
			get
			{
				double HomesteadValue = 0;
				HomesteadValue = dblHomesteadValue;
				return HomesteadValue;
			}
			set
			{
				dblHomesteadValue = value;
			}
		}

		public bool InBankruptcy
		{
			get
			{
				bool InBankruptcy = false;
				InBankruptcy = boolInBankruptcy;
				return InBankruptcy;
			}
			set
			{
				boolInBankruptcy = value;
			}
		}

		public string Comment
		{
			get
			{
				string Comment = "";
				Comment = strComment;
				return Comment;
			}
			set
			{
				strComment = value;
			}
		}

		public void Set_ExemptCode(int intIndex, int intCode)
		{
			intExemptCode[intIndex] = intCode;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Get_ExemptCode(int intIndex)
		{
			short ExemptCode = 0;
			ExemptCode = FCConvert.ToInt16(intExemptCode[intIndex]);
			return ExemptCode;
		}

		public void Set_ExemptValue(int intIndex, double dblValue)
		{
			dblExemptValue[intIndex] = dblValue;
		}

		public double Get_ExemptValue(int intIndex)
		{
			double ExemptValue = 0;
			ExemptValue = dblExemptValue[intIndex];
			return ExemptValue;
		}

		public void Clear()
		{
			cCreateGUID tGUID = new cCreateGUID();
			lngAccount = 0;
			intCard = 1;
			intLandCode = 0;
			intTranCode = 0;
			intBldgCode = 0;
			dblSalePrice = 0;
			intOpen1 = 0;
			intOpen2 = 0;
			intNeighborhood = 0;
			intZone = 0;
			strDwellingCode = "N";
			strAccountID = tGUID.CreateGUID();
			strCardId = strAccountID;
			strComment = "";
			strLocNum = "";
			strLocation = "";
			strMapLot = "";
			strREF1 = "";
			strRef2 = "";
			strYCoord = "";
			strXCoord = "";
			dblAcres = 0;
			dblExemptValue[1] = 0;
			dblExemptValue[2] = 0;
			dblExemptValue[3] = 0;
			intExemptCode[1] = 0;
			intExemptCode[2] = 0;
			intExemptCode[3] = 0;
			dblHomesteadValue = 0;
			// boolHasHomestead = False
		}

		private void Copy(ref cREMaster tMast)
		{
			// tMast.boolHasHomestead = boolHasHomestead
			// tMast.dblAcres = dblAcres
			// tMast.dblBldg = dblBldg
			// tMast.dblExemption = dblExemption
			// tMast.dblHardAcres = Me.dblHardAcres
			// tMast.dblHardValue = Me.dblHardValue
			// tMast.dblHomesteadValue = Me.dblHomesteadValue
			// tMast.dblLand = Me.dblLand
			// tMast.dblMixedAcres = Me.dblMixedAcres
			// tMast.dblMixedValue = Me.dblMixedValue
			// tMast.dblOtherAcres = Me.dblOtherAcres
			// tMast.dblOtherValue = Me.dblOtherValue
			// tMast.dblSalePrice = Me.dblSalePrice
			// tMast.dblSoftAcres = Me.dblSoftAcres
			// tMast.dblSoftValue = Me.dblSoftValue
			// tMast.DwellingCode = Me.DwellingCode
			// tMast.ExemptCode(0) = Me.ExemptCode(0)
			// tMast.ExemptCode(1) = Me.ExemptCode(1)
			// tMast.ExemptCode(2) = Me.ExemptCode(2)
			// tMast.ExemptValue(0) = Me.ExemptValue(0)
			// tMast.ExemptValue(1) = Me.ExemptValue(1)
			// tMast.ExemptValue(2) = Me.ExemptValue(2)
			// tMast.intBldgCode = Me.intBldgCode
			// tMast.intCard = Me.intCard
			// tMast.intLandCode = Me.intLandCode
			// tMast.intNeighborhood = Me.intNeighborhood
			// tMast.intOpen1 = Me.intOpen1
			// tMast.intOpen2 = Me.intOpen2
			// tMast.intTranCode = Me.intTranCode
			// tMast.intZone = Me.intZone
			// tMast.lngAccount = Me.lngAccount
			// tMast.lngBook = Me.lngBook
			// tMast.lngPage = Me.lngPage
			// tMast.strAccountID = Me.strAccountID
			// tMast.strAddr1 = Me.strAddr1
			// tMast.strAddr2 = Me.strAddr2
			// tMast.strBPDate = Me.strBPDate
			// tMast.strCardId = Me.strCardId
			// tMast.strCardId = Me.strCardId
			// tMast.strComment = Me.strComment
			// tMast.strDwellingCode = Me.strDwellingCode
			// tMast.strLocation = Me.strLocation
			// tMast.strLocNum = Me.strLocNum
			// tMast.strMapLot = Me.strMapLot
			// tMast.strName = Me.strName
			// tMast.strREF1 = Me.strREF1
			// tMast.strRef2 = Me.strRef2
			// tMast.strSecOwner = Me.strSecOwner
			// tMast.strState = Me.strState
			// tMast.strYCoord = Me.strYCoord
			// tMast.strXCoord = Me.strXCoord
			// tMast.strZip = Me.strZip
			// tMast.strZip4 = Me.strZip4
		}
		// private Function SaveAccount(ByRef rsSave As clsDataConnection) As Long
		// Dim lngReturn As Long
		// lngReturn = 0
		// If lngAccount <= 0 Then
		// Exit Function
		// End If
		// rsSave.AddNew
		// lngReturn = rsSave.Fields("id")
		// rsSave.Fields("rsname") = strName
		// rsSave.Fields("rssecowner") = strSecOwner
		// rsSave.Fields("rsaccount") = lngAccount
		// rsSave.Fields("rscard") = intCard
		// rsSave.Fields("AccountID") = strAccountID
		// rsSave.Fields("CardID") = strCardId
		// rsSave.Fields("rsaddr1") = strAddr1
		// rsSave.Fields("rsaddr2") = strAddr2
		// rsSave.Fields("rsaddr3") = strCity
		// rsSave.Fields("rsstate") = strState
		// rsSave.Fields("Rszip") = strZip
		// rsSave.Fields("rszip4") = strZip4
		// rsSave.Fields("rslocnumalph") = strLocNum
		// rsSave.Fields("rslocstreet") = strLocation
		// rsSave.Fields("rsmaplot") = strmaplot
		// rsSave.Fields("rsref1") = strRef1
		// rsSave.Fields("rsref2") = strRef2
		// rsSave.Fields("pisaleprice") = dblSalePrice
		// rsSave.Fields("lastlandval") = dblLand
		// rsSave.Fields("rllandval") = dblLand
		// rsSave.Fields("lastbldgval") = dblBldg
		// rsSave.Fields("rlbldgval") = dblBldg
		// rsSave.Fields("piacres") = dblAcres
		// rsSave.Fields("rsmixed") = dblMixedAcres
		// rsSave.Fields("rsmixedvalue") = dblMixedValue
		// rsSave.Fields("rssoft") = dblSoftAcres
		// rsSave.Fields("rssoftvalue") = dblSoftValue
		// rsSave.Fields("rshard") = dblHardAcres
		// rsSave.Fields("rshardvalue") = dblHardValue
		// rsSave.Fields("rsother") = dblOtherAcres
		// rsSave.Fields("rsothervalue") = dblOtherValue
		// rsSave.Fields("rlexemption") = dblExemption
		// rsSave.Fields("ExemptVal1") = dblExemptValue(1)
		// rsSave.Fields("ExemptVal2") = dblExemptValue(2)
		// rsSave.Fields("ExemptVal3") = dblExemptValue(3)
		// rsSave.Fields("RIExemptCD1") = intExemptCode(1)
		// rsSave.Fields("riexemptcd2") = intExemptCode(2)
		// rsSave.Fields("riexemptcd3") = intExemptCode(3)
		// rsSave.Fields("exemptpct1") = 100
		// rsSave.Fields("exemptpct2") = 100
		// rsSave.Fields("exemptpct3") = 100
		// rsSave.Fields("HomesteadValue") = dblHomesteadValue
		// rsSave.Fields("hashomestead") = boolHasHomestead
		// rsSave.Fields("rilandcode") = intLandCode
		// rsSave.Fields("ritrancode") = intTranCode
		// rsSave.Fields("ribldgcode") = intBldgCode
		// rsSave.Fields("PIOPEN1") = intOpen1
		// rsSave.Fields("PIOPEN2") = intOpen2
		// rsSave.Fields("piycoord") = strYCoord
		// rsSave.Fields("pixcoord") = strXCoord
		// rsSave.Fields("rsdwellingcode") = strDwellingCode
		// rsSave.Fields("pineighborhood") = intNeighborhood
		// rsSave.Fields("pizone") = intZone
		// rsSave.Update
		// If lngBook > 0 And lngPage > 0 Then
		// Call AddBookPage(lngBook, lngPage, lngAccount, strBPDate, True, "twre0000.vb1")
		// End If
		// If Trim(strComment) <> "" Then
		// Call AddComment(lngAccount, strComment, intCard, "twre0000.vb1")
		// End If
		// SaveAccount = lngReturn
		// End Function
	}
}
