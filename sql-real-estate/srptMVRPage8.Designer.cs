﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage8.
	/// </summary>
	partial class srptMVRPage8
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage8));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtSeniorTaxDeferralGranted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumberQualifiedForSeniorTaxDeferral = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtSeniorDeferralYesNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine51a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine51b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine51c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine43a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine44a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine44c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine44b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt43b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine42Due3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine42Due1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine42Due4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine42Due2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine40To = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt39a1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt39a2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.LineProvision1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.LineProvision2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.LineProvision3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.LineName1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.LineName2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label204 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label211 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine40From = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label214 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label215 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label216 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label222 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label224 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label225 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label226 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label228 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label229 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label230 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label231 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label232 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label233 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label234 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label235 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label236 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label237 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label238 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label239 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label240 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label241 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label242 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label243 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label245 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label246 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label247 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label248 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label249 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label250 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label251 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label252 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label253 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label254 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label255 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label256 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtSeniorTaxDeferralGranted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberQualifiedForSeniorTaxDeferral)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeniorDeferralYesNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine51a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine51b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine51c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine43a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine44a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine44c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine44b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt43b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine40To)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39a1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39a2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine40From)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label222)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label224)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label225)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label226)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label228)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label229)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label230)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label231)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label232)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label233)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label234)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label235)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label236)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label237)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label238)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label239)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label240)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label241)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label242)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label243)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label245)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label246)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label247)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label248)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label249)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label250)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label251)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label252)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label253)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label254)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label255)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label256)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSeniorTaxDeferralGranted,
            this.txtNumberQualifiedForSeniorTaxDeferral,
            this.shape2,
            this.shape3,
            this.txtSeniorDeferralYesNo,
            this.txtLine51a,
            this.Shape65,
            this.shape1,
            this.txtLine51b,
            this.txtLine51c,
            this.txtLine43a,
            this.txtLine44a,
            this.txtLine44c,
            this.txtLine44b,
            this.txt43b,
            this.txtLine42Due3,
            this.txtLine42Due1,
            this.txtLine42Due4,
            this.txtLine42Due2,
            this.txtLine41,
            this.txtLine40To,
            this.txt39a1,
            this.txt39a2,
            this.Shape57,
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label175,
            this.Label176,
            this.Line3,
            this.Label112,
            this.Label117,
            this.Label118,
            this.Label119,
            this.LineProvision1,
            this.LineProvision2,
            this.LineProvision3,
            this.LineName1,
            this.LineName2,
            this.Label203,
            this.Label204,
            this.Shape47,
            this.Label211,
            this.Label212,
            this.Shape49,
            this.txtLine40From,
            this.Label214,
            this.Shape50,
            this.Label215,
            this.Shape51,
            this.Shape52,
            this.Shape53,
            this.Label216,
            this.Label217,
            this.Label222,
            this.Shape54,
            this.txtMuniname,
            this.Label224,
            this.Label225,
            this.Label226,
            this.Label228,
            this.Label229,
            this.Shape56,
            this.txtDate,
            this.Label230,
            this.Label231,
            this.Label232,
            this.Label233,
            this.Label234,
            this.lblYear,
            this.lblYear2,
            this.Label235,
            this.Shape59,
            this.Label236,
            this.Shape60,
            this.Shape61,
            this.Label237,
            this.Label238,
            this.Label239,
            this.Label240,
            this.Label241,
            this.Label242,
            this.Label243,
            this.Shape62,
            this.Label245,
            this.Label246,
            this.Shape63,
            this.Label247,
            this.Shape64,
            this.Label248,
            this.Label249,
            this.Label250,
            this.Label251,
            this.Shape66,
            this.Label252,
            this.Shape67,
            this.Label253,
            this.Label254,
            this.Label255,
            this.txtEmail,
            this.Label256,
            this.Shape68,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.Label124,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.label11});
			this.Detail.Height = 9.6875F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// txtSeniorTaxDeferralGranted
			// 
			this.txtSeniorTaxDeferralGranted.Height = 0.1875F;
			this.txtSeniorTaxDeferralGranted.Left = 4.896F;
			this.txtSeniorTaxDeferralGranted.Name = "txtSeniorTaxDeferralGranted";
			this.txtSeniorTaxDeferralGranted.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtSeniorTaxDeferralGranted.Text = " ";
			this.txtSeniorTaxDeferralGranted.Top = 5.673501F;
			this.txtSeniorTaxDeferralGranted.Width = 1.5625F;
			// 
			// txtNumberQualifiedForSeniorTaxDeferral
			// 
			this.txtNumberQualifiedForSeniorTaxDeferral.Height = 0.1875F;
			this.txtNumberQualifiedForSeniorTaxDeferral.Left = 4.896F;
			this.txtNumberQualifiedForSeniorTaxDeferral.Name = "txtNumberQualifiedForSeniorTaxDeferral";
			this.txtNumberQualifiedForSeniorTaxDeferral.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtNumberQualifiedForSeniorTaxDeferral.Text = " ";
			this.txtNumberQualifiedForSeniorTaxDeferral.Top = 5.486001F;
			this.txtNumberQualifiedForSeniorTaxDeferral.Width = 1.5625F;
			// 
			// shape2
			// 
			this.shape2.Height = 0.1875F;
			this.shape2.Left = 4.8335F;
			this.shape2.Name = "shape2";
			this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shape2.Top = 5.486001F;
			this.shape2.Width = 1.6875F;
			// 
			// shape3
			// 
			this.shape3.Height = 0.1875F;
			this.shape3.Left = 4.8335F;
			this.shape3.Name = "shape3";
			this.shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shape3.Top = 5.673501F;
			this.shape3.Width = 1.6875F;
			// 
			// txtSeniorDeferralYesNo
			// 
			this.txtSeniorDeferralYesNo.Height = 0.1875F;
			this.txtSeniorDeferralYesNo.Left = 1.750168F;
			this.txtSeniorDeferralYesNo.Name = "txtSeniorDeferralYesNo";
			this.txtSeniorDeferralYesNo.Style = "text-align: right";
			this.txtSeniorDeferralYesNo.Text = " ";
			this.txtSeniorDeferralYesNo.Top = 5.486001F;
			this.txtSeniorDeferralYesNo.Width = 0.625F;
			// 
			// txtLine51a
			// 
			this.txtLine51a.Height = 0.1875F;
			this.txtLine51a.Left = 1.750168F;
			this.txtLine51a.Name = "txtLine51a";
			this.txtLine51a.Style = "text-align: right";
			this.txtLine51a.Text = " ";
			this.txtLine51a.Top = 4.792F;
			this.txtLine51a.Width = 0.625F;
			// 
			// Shape65
			// 
			this.Shape65.Height = 0.1875F;
			this.Shape65.Left = 1.666835F;
			this.Shape65.Name = "Shape65";
			this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape65.Top = 4.792F;
			this.Shape65.Width = 0.7708333F;
			// 
			// shape1
			// 
			this.shape1.Height = 0.1875F;
			this.shape1.Left = 1.666835F;
			this.shape1.Name = "shape1";
			this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shape1.Top = 5.486001F;
			this.shape1.Width = 0.7708333F;
			// 
			// txtLine51b
			// 
			this.txtLine51b.Height = 0.1875F;
			this.txtLine51b.Left = 4.896F;
			this.txtLine51b.Name = "txtLine51b";
			this.txtLine51b.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtLine51b.Text = " ";
			this.txtLine51b.Top = 4.792F;
			this.txtLine51b.Width = 1.5625F;
			// 
			// txtLine51c
			// 
			this.txtLine51c.Height = 0.1875F;
			this.txtLine51c.Left = 4.896F;
			this.txtLine51c.Name = "txtLine51c";
			this.txtLine51c.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtLine51c.Text = " ";
			this.txtLine51c.Top = 4.9795F;
			this.txtLine51c.Width = 1.5625F;
			// 
			// txtLine43a
			// 
			this.txtLine43a.Height = 0.1875F;
			this.txtLine43a.Left = 1.750168F;
			this.txtLine43a.Name = "txtLine43a";
			this.txtLine43a.Style = "text-align: right";
			this.txtLine43a.Text = " ";
			this.txtLine43a.Top = 3.529502F;
			this.txtLine43a.Width = 0.625F;
			// 
			// txtLine44a
			// 
			this.txtLine44a.Height = 0.1875F;
			this.txtLine44a.Left = 1.750168F;
			this.txtLine44a.Name = "txtLine44a";
			this.txtLine44a.Style = "text-align: right";
			this.txtLine44a.Text = " ";
			this.txtLine44a.Top = 4.092F;
			this.txtLine44a.Width = 0.625F;
			// 
			// txtLine44c
			// 
			this.txtLine44c.Height = 0.1875F;
			this.txtLine44c.Left = 4.896F;
			this.txtLine44c.Name = "txtLine44c";
			this.txtLine44c.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtLine44c.Text = " ";
			this.txtLine44c.Top = 4.2795F;
			this.txtLine44c.Width = 1.5625F;
			// 
			// txtLine44b
			// 
			this.txtLine44b.Height = 0.1875F;
			this.txtLine44b.Left = 4.896F;
			this.txtLine44b.Name = "txtLine44b";
			this.txtLine44b.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtLine44b.Text = " ";
			this.txtLine44b.Top = 4.092F;
			this.txtLine44b.Width = 1.5625F;
			// 
			// txt43b
			// 
			this.txt43b.Height = 0.1875F;
			this.txt43b.Left = 4.8335F;
			this.txt43b.Name = "txt43b";
			this.txt43b.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt43b.Text = " ";
			this.txt43b.Top = 3.529502F;
			this.txt43b.Width = 1.625F;
			// 
			// txtLine42Due3
			// 
			this.txtLine42Due3.Height = 0.1875F;
			this.txtLine42Due3.Left = 3.833666F;
			this.txtLine42Due3.Name = "txtLine42Due3";
			this.txtLine42Due3.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine42Due3.Text = " ";
			this.txtLine42Due3.Top = 3.046667F;
			this.txtLine42Due3.Width = 1F;
			// 
			// txtLine42Due1
			// 
			this.txtLine42Due1.Height = 0.1875F;
			this.txtLine42Due1.Left = 3.833666F;
			this.txtLine42Due1.Name = "txtLine42Due1";
			this.txtLine42Due1.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine42Due1.Text = " ";
			this.txtLine42Due1.Top = 2.859167F;
			this.txtLine42Due1.Width = 1F;
			// 
			// txtLine42Due4
			// 
			this.txtLine42Due4.Height = 0.1875F;
			this.txtLine42Due4.Left = 5.458666F;
			this.txtLine42Due4.Name = "txtLine42Due4";
			this.txtLine42Due4.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine42Due4.Text = " ";
			this.txtLine42Due4.Top = 3.046667F;
			this.txtLine42Due4.Width = 1F;
			// 
			// txtLine42Due2
			// 
			this.txtLine42Due2.Height = 0.1875F;
			this.txtLine42Due2.Left = 5.458666F;
			this.txtLine42Due2.Name = "txtLine42Due2";
			this.txtLine42Due2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine42Due2.Text = " ";
			this.txtLine42Due2.Top = 2.859167F;
			this.txtLine42Due2.Width = 1F;
			// 
			// txtLine41
			// 
			this.txtLine41.Height = 0.1875F;
			this.txtLine41.Left = 5.458666F;
			this.txtLine41.Name = "txtLine41";
			this.txtLine41.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine41.Text = " ";
			this.txtLine41.Top = 2.4425F;
			this.txtLine41.Width = 1F;
			// 
			// txtLine40To
			// 
			this.txtLine40To.Height = 0.1875F;
			this.txtLine40To.Left = 5.458666F;
			this.txtLine40To.Name = "txtLine40To";
			this.txtLine40To.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine40To.Text = " ";
			this.txtLine40To.Top = 2.13F;
			this.txtLine40To.Width = 1F;
			// 
			// txt39a1
			// 
			this.txt39a1.Height = 0.1875F;
			this.txt39a1.Left = 2.708333F;
			this.txt39a1.Name = "txt39a1";
			this.txt39a1.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt39a1.Text = " ";
			this.txt39a1.Top = 1.25F;
			this.txt39a1.Width = 2.104167F;
			// 
			// txt39a2
			// 
			this.txt39a2.Height = 0.1875F;
			this.txt39a2.Left = 2.708333F;
			this.txt39a2.Name = "txt39a2";
			this.txt39a2.Style = "font-family: \'Courier New\'; text-align: left";
			this.txt39a2.Text = " ";
			this.txt39a2.Top = 1.4375F;
			this.txt39a2.Width = 2.104167F;
			// 
			// Shape57
			// 
			this.Shape57.Height = 0.1875F;
			this.Shape57.Left = 1.666835F;
			this.Shape57.Name = "Shape57";
			this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape57.Top = 3.529502F;
			this.Shape57.Width = 0.7708333F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.9375F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.3125F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 1.75F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.3125F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.0625F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-8-";
			this.Label175.Top = 9.4375F;
			this.Label175.Width = 1F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 1F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label176.Text = "MUNICIPAL RECORDS CONTINUED";
			this.Label176.Top = 0.5625F;
			this.Label176.Width = 5.5F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.5625F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 0.5625F;
			this.Line3.Y2 = 0.5625F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0.375F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label112.Text = "Enter the best choice that describes how the municipality administers its assessm" +
    "ent function.  Choose";
			this.Label112.Top = 0.8125F;
			this.Label112.Width = 6.25F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 0.3753331F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label117.Text = "Interest rate charged on overdue 2006 property taxes (36 M.R.S. § 505)";
			this.Label117.Top = 2.4425F;
			this.Label117.Width = 4.166667F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 5.417F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label118.Text = "(not to exceed 9.00%)";
			this.Label118.Top = 2.63F;
			this.Label118.Width = 1.166667F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1875F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 0.3753331F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label119.Text = "Date(s) that 2007 property taxes are due.";
			this.Label119.Top = 2.88F;
			this.Label119.Width = 2.416667F;
			// 
			// LineProvision1
			// 
			this.LineProvision1.Height = 0F;
			this.LineProvision1.Left = 2.479167F;
			this.LineProvision1.LineWeight = 1F;
			this.LineProvision1.Name = "LineProvision1";
			this.LineProvision1.Top = 6.8125F;
			this.LineProvision1.Width = 4.291666F;
			this.LineProvision1.X1 = 2.479167F;
			this.LineProvision1.X2 = 6.770833F;
			this.LineProvision1.Y1 = 6.8125F;
			this.LineProvision1.Y2 = 6.8125F;
			// 
			// LineProvision2
			// 
			this.LineProvision2.Height = 0F;
			this.LineProvision2.Left = 2.479167F;
			this.LineProvision2.LineWeight = 1F;
			this.LineProvision2.Name = "LineProvision2";
			this.LineProvision2.Top = 7.0625F;
			this.LineProvision2.Width = 4.291666F;
			this.LineProvision2.X1 = 2.479167F;
			this.LineProvision2.X2 = 6.770833F;
			this.LineProvision2.Y1 = 7.0625F;
			this.LineProvision2.Y2 = 7.0625F;
			// 
			// LineProvision3
			// 
			this.LineProvision3.Height = 0F;
			this.LineProvision3.Left = 2.479167F;
			this.LineProvision3.LineWeight = 1F;
			this.LineProvision3.Name = "LineProvision3";
			this.LineProvision3.Top = 7.3125F;
			this.LineProvision3.Width = 4.291666F;
			this.LineProvision3.X1 = 2.479167F;
			this.LineProvision3.X2 = 6.770833F;
			this.LineProvision3.Y1 = 7.3125F;
			this.LineProvision3.Y2 = 7.3125F;
			// 
			// LineName1
			// 
			this.LineName1.Height = 0F;
			this.LineName1.Left = 2.479167F;
			this.LineName1.LineWeight = 1F;
			this.LineName1.Name = "LineName1";
			this.LineName1.Top = 7.5625F;
			this.LineName1.Width = 4.291666F;
			this.LineName1.X1 = 2.479167F;
			this.LineName1.X2 = 6.770833F;
			this.LineName1.Y1 = 7.5625F;
			this.LineName1.Y2 = 7.5625F;
			// 
			// LineName2
			// 
			this.LineName2.Height = 0F;
			this.LineName2.Left = 2.479167F;
			this.LineName2.LineWeight = 1F;
			this.LineName2.Name = "LineName2";
			this.LineName2.Top = 7.8125F;
			this.LineName2.Width = 4.291666F;
			this.LineName2.X1 = 2.479167F;
			this.LineName2.X2 = 6.770833F;
			this.LineName2.Y1 = 7.8125F;
			this.LineName2.Y2 = 7.8125F;
			// 
			// Label203
			// 
			this.Label203.Height = 0.1875F;
			this.Label203.HyperLink = null;
			this.Label203.Left = 1.0625F;
			this.Label203.Name = "Label203";
			this.Label203.Style = "font-size: 9pt";
			this.Label203.Text = "a) Function";
			this.Label203.Top = 1.25F;
			this.Label203.Width = 0.625F;
			// 
			// Label204
			// 
			this.Label204.Height = 0.1875F;
			this.Label204.HyperLink = null;
			this.Label204.Left = 1.0625F;
			this.Label204.Name = "Label204";
			this.Label204.Style = "font-size: 9pt";
			this.Label204.Text = "b) Name";
			this.Label204.Top = 1.4375F;
			this.Label204.Width = 0.5625F;
			// 
			// Shape47
			// 
			this.Shape47.Height = 0.1875F;
			this.Shape47.Left = 2.645833F;
			this.Shape47.Name = "Shape47";
			this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape47.Top = 1.4375F;
			this.Shape47.Width = 2.229167F;
			// 
			// Label211
			// 
			this.Label211.Height = 0.1875F;
			this.Label211.HyperLink = null;
			this.Label211.Left = 0.3753331F;
			this.Label211.Name = "Label211";
			this.Label211.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label211.Text = "List the beginning and ending dates of the fiscal year in your municipality.";
			this.Label211.Top = 1.900833F;
			this.Label211.Width = 5.333333F;
			// 
			// Label212
			// 
			this.Label212.Height = 0.1875F;
			this.Label212.HyperLink = null;
			this.Label212.Left = 2.166667F;
			this.Label212.Name = "Label212";
			this.Label212.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label212.Text = "45b";
			this.Label212.Top = 1.4375F;
			this.Label212.Width = 0.4583333F;
			// 
			// Shape49
			// 
			this.Shape49.Height = 0.1875F;
			this.Shape49.Left = 2.646166F;
			this.Shape49.Name = "Shape49";
			this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape49.Top = 2.13F;
			this.Shape49.Width = 1.125F;
			// 
			// txtLine40From
			// 
			this.txtLine40From.Height = 0.1875F;
			this.txtLine40From.Left = 2.708666F;
			this.txtLine40From.Name = "txtLine40From";
			this.txtLine40From.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine40From.Text = " ";
			this.txtLine40From.Top = 2.13F;
			this.txtLine40From.Width = 1F;
			// 
			// Label214
			// 
			this.Label214.Height = 0.1875F;
			this.Label214.HyperLink = null;
			this.Label214.Left = 1.804333F;
			this.Label214.Name = "Label214";
			this.Label214.Style = "font-family: \'Times New Roman\'; font-size: 9pt; ddo-char-set: 1";
			this.Label214.Text = "FROM";
			this.Label214.Top = 2.13F;
			this.Label214.Width = 0.4583333F;
			// 
			// Shape50
			// 
			this.Shape50.Height = 0.1875F;
			this.Shape50.Left = 5.396166F;
			this.Shape50.Name = "Shape50";
			this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape50.Top = 2.13F;
			this.Shape50.Width = 1.125F;
			// 
			// Label215
			// 
			this.Label215.Height = 0.1875F;
			this.Label215.HyperLink = null;
			this.Label215.Left = 4.601334F;
			this.Label215.Name = "Label215";
			this.Label215.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center; ddo-char-set:" +
    " 1";
			this.Label215.Text = "TO";
			this.Label215.Top = 2.13F;
			this.Label215.Width = 0.2916667F;
			// 
			// Shape51
			// 
			this.Shape51.Height = 0.1875F;
			this.Shape51.Left = 5.396166F;
			this.Shape51.Name = "Shape51";
			this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape51.Top = 2.4425F;
			this.Shape51.Width = 1.125F;
			// 
			// Shape52
			// 
			this.Shape52.Height = 0.1875F;
			this.Shape52.Left = 3.771166F;
			this.Shape52.Name = "Shape52";
			this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape52.Top = 2.859167F;
			this.Shape52.Width = 1.125F;
			// 
			// Shape53
			// 
			this.Shape53.Height = 0.1875F;
			this.Shape53.Left = 5.396166F;
			this.Shape53.Name = "Shape53";
			this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape53.Top = 2.859167F;
			this.Shape53.Width = 1.125F;
			// 
			// Label216
			// 
			this.Label216.Height = 0.1875F;
			this.Label216.HyperLink = null;
			this.Label216.Left = 0.3751676F;
			this.Label216.Name = "Label216";
			this.Label216.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label216.Text = "Are your assessment records computerized?";
			this.Label216.Top = 3.300335F;
			this.Label216.Width = 3.083333F;
			// 
			// Label217
			// 
			this.Label217.Height = 0.1875F;
			this.Label217.HyperLink = null;
			this.Label217.Left = 1.437668F;
			this.Label217.Name = "Label217";
			this.Label217.Style = "font-size: 9pt";
			this.Label217.Text = "49a";
			this.Label217.Top = 3.529502F;
			this.Label217.Width = 0.25F;
			// 
			// Label222
			// 
			this.Label222.Height = 0.1875F;
			this.Label222.HyperLink = null;
			this.Label222.Left = 3.062668F;
			this.Label222.Name = "Label222";
			this.Label222.Style = "font-size: 9pt; text-align: right";
			this.Label222.Text = "Name of software used";
			this.Label222.Top = 3.529502F;
			this.Label222.Width = 1.375F;
			// 
			// Shape54
			// 
			this.Shape54.Height = 0.1875F;
			this.Shape54.Left = 4.771F;
			this.Shape54.Name = "Shape54";
			this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape54.Top = 3.529502F;
			this.Shape54.Width = 1.75F;
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.1875F;
			this.txtMuniname.Left = 2.917F;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.txtMuniname.Text = null;
			this.txtMuniname.Top = 5.939F;
			this.txtMuniname.Width = 2.677083F;
			// 
			// Label224
			// 
			this.Label224.Height = 0.1875F;
			this.Label224.HyperLink = null;
			this.Label224.Left = 0.3751679F;
			this.Label224.Name = "Label224";
			this.Label224.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label224.Text = "I/We, the Assessor(s) of the Municipality of";
			this.Label224.Top = 5.939F;
			this.Label224.Width = 2.333333F;
			// 
			// Label225
			// 
			this.Label225.Height = 0.1875F;
			this.Label225.HyperLink = null;
			this.Label225.Left = 0.3751679F;
			this.Label225.Name = "Label225";
			this.Label225.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label225.Text = "foregoing information contained herein is, to the best knowledge and belief of th" +
    "is office, reported correctly";
			this.Label225.Top = 6.1265F;
			this.Label225.Width = 6.166667F;
			// 
			// Label226
			// 
			this.Label226.Height = 0.1875F;
			this.Label226.HyperLink = null;
			this.Label226.Left = 0.3751679F;
			this.Label226.Name = "Label226";
			this.Label226.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label226.Text = "and that all of the requirements of the law have been followed in valuing, listin" +
    "g, and submitting the information.";
			this.Label226.Top = 6.314F;
			this.Label226.Width = 6.166667F;
			// 
			// Label228
			// 
			this.Label228.Height = 0.1875F;
			this.Label228.HyperLink = null;
			this.Label228.Left = 0.3751679F;
			this.Label228.Name = "Label228";
			this.Label228.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label228.Text = "ASSESSOR(S)";
			this.Label228.Top = 6.854167F;
			this.Label228.Width = 1.208333F;
			// 
			// Label229
			// 
			this.Label229.Height = 0.1875F;
			this.Label229.HyperLink = null;
			this.Label229.Left = 0.3751679F;
			this.Label229.Name = "Label229";
			this.Label229.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label229.Text = "SIGNATURES";
			this.Label229.Top = 7.041667F;
			this.Label229.Width = 1.208333F;
			// 
			// Shape56
			// 
			this.Shape56.Height = 0.1875F;
			this.Shape56.Left = 0.6041667F;
			this.Shape56.Name = "Shape56";
			this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape56.Top = 8.041667F;
			this.Shape56.Width = 1.125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 0.6666667F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtDate.Text = " ";
			this.txtDate.Top = 8.041667F;
			this.txtDate.Width = 1F;
			// 
			// Label230
			// 
			this.Label230.Height = 0.1875F;
			this.Label230.HyperLink = null;
			this.Label230.Left = 0F;
			this.Label230.Name = "Label230";
			this.Label230.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label230.Text = "DATE";
			this.Label230.Top = 8.042F;
			this.Label230.Width = 0.4583333F;
			// 
			// Label231
			// 
			this.Label231.Height = 0.1875F;
			this.Label231.HyperLink = null;
			this.Label231.Left = 0F;
			this.Label231.Name = "Label231";
			this.Label231.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold";
			this.Label231.Text = "Notice: This return must be completed and sent to the Property Tax Division by No" +
    "vember 1,";
			this.Label231.Top = 8.5F;
			this.Label231.Width = 5.291667F;
			// 
			// Label232
			// 
			this.Label232.Height = 0.1875F;
			this.Label232.HyperLink = null;
			this.Label232.Left = 0F;
			this.Label232.Name = "Label232";
			this.Label232.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold";
			this.Label232.Text = "or within 30 days after the commitment date, whichever is later, in order to avoi" +
    "d reduction or loss of";
			this.Label232.Top = 8.688F;
			this.Label232.Width = 6.166667F;
			// 
			// Label233
			// 
			this.Label233.Height = 0.1875F;
			this.Label233.HyperLink = null;
			this.Label233.Left = 0F;
			this.Label233.Name = "Label233";
			this.Label233.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold";
			this.Label233.Text = "any entitlement under the Tree Growth Tax Law municipal reimbursement program for" +
    " the";
			this.Label233.Top = 8.875F;
			this.Label233.Width = 5.166667F;
			// 
			// Label234
			// 
			this.Label234.Height = 0.1875F;
			this.Label234.HyperLink = null;
			this.Label234.Left = 5.516F;
			this.Label234.Name = "Label234";
			this.Label234.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label234.Text = "tax year.";
			this.Label234.Top = 8.875F;
			this.Label234.Width = 0.6041667F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 5.354167F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold";
			this.lblYear.Text = null;
			this.lblYear.Top = 8.5F;
			this.lblYear.Width = 1.291667F;
			// 
			// lblYear2
			// 
			this.lblYear2.Height = 0.1875F;
			this.lblYear2.HyperLink = null;
			this.lblYear2.Left = 5.166667F;
			this.lblYear2.Name = "lblYear2";
			this.lblYear2.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold";
			this.lblYear2.Text = "2009";
			this.lblYear2.Top = 8.875F;
			this.lblYear2.Width = 0.4166667F;
			// 
			// Label235
			// 
			this.Label235.Height = 0.1875F;
			this.Label235.HyperLink = null;
			this.Label235.Left = 0.375F;
			this.Label235.Name = "Label235";
			this.Label235.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label235.Text = "SINGLE ASSESSOR, ASSESSORS\' AGENT or BOARD OF ASSESSORS. Include the name of any " +
    "single assessor or agent.";
			this.Label235.Top = 1F;
			this.Label235.Width = 6.5625F;
			// 
			// Shape59
			// 
			this.Shape59.Height = 0.1875F;
			this.Shape59.Left = 2.645833F;
			this.Shape59.Name = "Shape59";
			this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape59.Top = 1.25F;
			this.Shape59.Width = 2.229167F;
			// 
			// Label236
			// 
			this.Label236.Height = 0.1875F;
			this.Label236.HyperLink = null;
			this.Label236.Left = 2.166667F;
			this.Label236.Name = "Label236";
			this.Label236.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label236.Text = "45a";
			this.Label236.Top = 1.25F;
			this.Label236.Width = 0.4583333F;
			// 
			// Shape60
			// 
			this.Shape60.Height = 0.1875F;
			this.Shape60.Left = 3.771166F;
			this.Shape60.Name = "Shape60";
			this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape60.Top = 3.046667F;
			this.Shape60.Width = 1.125F;
			// 
			// Shape61
			// 
			this.Shape61.Height = 0.1875F;
			this.Shape61.Left = 5.396166F;
			this.Shape61.Name = "Shape61";
			this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape61.Top = 3.046667F;
			this.Shape61.Width = 1.125F;
			// 
			// Label237
			// 
			this.Label237.Height = 0.1875F;
			this.Label237.HyperLink = null;
			this.Label237.Left = 5.01075F;
			this.Label237.Name = "Label237";
			this.Label237.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label237.Text = "46b";
			this.Label237.Top = 2.13F;
			this.Label237.Width = 0.3333333F;
			// 
			// Label238
			// 
			this.Label238.Height = 0.1875F;
			this.Label238.HyperLink = null;
			this.Label238.Left = 2.281583F;
			this.Label238.Name = "Label238";
			this.Label238.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label238.Text = "46a";
			this.Label238.Top = 2.13F;
			this.Label238.Width = 0.3333333F;
			// 
			// Label239
			// 
			this.Label239.Height = 0.1875F;
			this.Label239.HyperLink = null;
			this.Label239.Left = 3.396166F;
			this.Label239.Name = "Label239";
			this.Label239.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label239.Text = "48a";
			this.Label239.Top = 2.859167F;
			this.Label239.Width = 0.3333333F;
			// 
			// Label240
			// 
			this.Label240.Height = 0.1875F;
			this.Label240.HyperLink = null;
			this.Label240.Left = 3.396166F;
			this.Label240.Name = "Label240";
			this.Label240.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label240.Text = "48c";
			this.Label240.Top = 3.046667F;
			this.Label240.Width = 0.3333333F;
			// 
			// Label241
			// 
			this.Label241.Height = 0.1875F;
			this.Label241.HyperLink = null;
			this.Label241.Left = 5.021166F;
			this.Label241.Name = "Label241";
			this.Label241.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label241.Text = "48b";
			this.Label241.Top = 2.859167F;
			this.Label241.Width = 0.3333333F;
			// 
			// Label242
			// 
			this.Label242.Height = 0.1875F;
			this.Label242.HyperLink = null;
			this.Label242.Left = 5.021166F;
			this.Label242.Name = "Label242";
			this.Label242.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label242.Text = "48d";
			this.Label242.Top = 3.046667F;
			this.Label242.Width = 0.3333333F;
			// 
			// Label243
			// 
			this.Label243.Height = 0.1875F;
			this.Label243.HyperLink = null;
			this.Label243.Left = 0.3751676F;
			this.Label243.Name = "Label243";
			this.Label243.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label243.Text = "Has your municipality implemented a local property tax relief program under 36 M." +
    "R.S. § 6232(1)?";
			this.Label243.Top = 3.862834F;
			this.Label243.Width = 6.333333F;
			// 
			// Shape62
			// 
			this.Shape62.Height = 0.1875F;
			this.Shape62.Left = 1.666835F;
			this.Shape62.Name = "Shape62";
			this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape62.Top = 4.092F;
			this.Shape62.Width = 0.7708333F;
			// 
			// Label245
			// 
			this.Label245.Height = 0.1875F;
			this.Label245.HyperLink = null;
			this.Label245.Left = 1.437668F;
			this.Label245.Name = "Label245";
			this.Label245.Style = "font-size: 9pt";
			this.Label245.Text = "50a";
			this.Label245.Top = 4.092F;
			this.Label245.Width = 0.25F;
			// 
			// Label246
			// 
			this.Label246.Height = 0.1875F;
			this.Label246.HyperLink = null;
			this.Label246.Left = 2.875168F;
			this.Label246.Name = "Label246";
			this.Label246.Style = "font-size: 9pt; text-align: right";
			this.Label246.Text = "How many people qualified?  50b";
			this.Label246.Top = 4.092F;
			this.Label246.Width = 1.9375F;
			// 
			// Shape63
			// 
			this.Shape63.Height = 0.1875F;
			this.Shape63.Left = 4.8335F;
			this.Shape63.Name = "Shape63";
			this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape63.Top = 4.092F;
			this.Shape63.Width = 1.6875F;
			// 
			// Label247
			// 
			this.Label247.Height = 0.1875F;
			this.Label247.HyperLink = null;
			this.Label247.Left = 2.875168F;
			this.Label247.Name = "Label247";
			this.Label247.Style = "font-size: 9pt; text-align: right";
			this.Label247.Text = "How much relief was granted?  50c";
			this.Label247.Top = 4.2795F;
			this.Label247.Width = 1.9375F;
			// 
			// Shape64
			// 
			this.Shape64.Height = 0.1875F;
			this.Shape64.Left = 4.8335F;
			this.Shape64.Name = "Shape64";
			this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape64.Top = 4.2795F;
			this.Shape64.Width = 1.6875F;
			// 
			// Label248
			// 
			this.Label248.Height = 0.1875F;
			this.Label248.HyperLink = null;
			this.Label248.Left = 5.01075F;
			this.Label248.Name = "Label248";
			this.Label248.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label248.Text = "47";
			this.Label248.Top = 2.4425F;
			this.Label248.Width = 0.3333333F;
			// 
			// Label249
			// 
			this.Label249.Height = 0.1875F;
			this.Label249.HyperLink = null;
			this.Label249.Left = 0.3751676F;
			this.Label249.Name = "Label249";
			this.Label249.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label249.Text = "Has your municipality implemented a local senior volunteer tax credit program und" +
    "er 36 M.R.S. § 6232(1-A)?";
			this.Label249.Top = 4.562833F;
			this.Label249.Width = 6.208333F;
			// 
			// Label250
			// 
			this.Label250.Height = 0.1875F;
			this.Label250.HyperLink = null;
			this.Label250.Left = 1.437668F;
			this.Label250.Name = "Label250";
			this.Label250.Style = "font-size: 9pt";
			this.Label250.Text = "51a";
			this.Label250.Top = 4.792F;
			this.Label250.Width = 0.25F;
			// 
			// Label251
			// 
			this.Label251.Height = 0.1875F;
			this.Label251.HyperLink = null;
			this.Label251.Left = 2.875168F;
			this.Label251.Name = "Label251";
			this.Label251.Style = "font-size: 9pt; text-align: right";
			this.Label251.Text = "How many people qualified?  51b";
			this.Label251.Top = 4.792F;
			this.Label251.Width = 1.9375F;
			// 
			// Shape66
			// 
			this.Shape66.Height = 0.1875F;
			this.Shape66.Left = 4.8335F;
			this.Shape66.Name = "Shape66";
			this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape66.Top = 4.792F;
			this.Shape66.Width = 1.6875F;
			// 
			// Label252
			// 
			this.Label252.Height = 0.1875F;
			this.Label252.HyperLink = null;
			this.Label252.Left = 2.875168F;
			this.Label252.Name = "Label252";
			this.Label252.Style = "font-size: 9pt; text-align: right";
			this.Label252.Text = "How much relief was granted?  51c";
			this.Label252.Top = 4.9795F;
			this.Label252.Width = 1.9375F;
			// 
			// Shape67
			// 
			this.Shape67.Height = 0.1875F;
			this.Shape67.Left = 4.8335F;
			this.Shape67.Name = "Shape67";
			this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape67.Top = 4.9795F;
			this.Shape67.Width = 1.6875F;
			// 
			// Label253
			// 
			this.Label253.Height = 0.1875F;
			this.Label253.HyperLink = null;
			this.Label253.Left = 5.687833F;
			this.Label253.Name = "Label253";
			this.Label253.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label253.Text = "do state that the";
			this.Label253.Top = 5.939F;
			this.Label253.Width = 1.208333F;
			// 
			// Label254
			// 
			this.Label254.Height = 0.1875F;
			this.Label254.HyperLink = null;
			this.Label254.Left = 4.527946F;
			this.Label254.Name = "Label254";
			this.Label254.Style = "font-size: 9pt";
			this.Label254.Text = "49b";
			this.Label254.Top = 3.529502F;
			this.Label254.Width = 0.25F;
			// 
			// Label255
			// 
			this.Label255.Height = 0.1875F;
			this.Label255.HyperLink = null;
			this.Label255.Left = 1.0625F;
			this.Label255.Name = "Label255";
			this.Label255.Style = "font-size: 9pt";
			this.Label255.Text = "c) Email address";
			this.Label255.Top = 1.625F;
			this.Label255.Width = 1.0625F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.1875F;
			this.txtEmail.Left = 2.708333F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtEmail.Text = " ";
			this.txtEmail.Top = 1.625F;
			this.txtEmail.Width = 2.104167F;
			// 
			// Label256
			// 
			this.Label256.Height = 0.1875F;
			this.Label256.HyperLink = null;
			this.Label256.Left = 2.166667F;
			this.Label256.Name = "Label256";
			this.Label256.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: right; ddo-char-set: " +
    "1";
			this.Label256.Text = "45c";
			this.Label256.Top = 1.625F;
			this.Label256.Width = 0.4583333F;
			// 
			// Shape68
			// 
			this.Shape68.Height = 0.1875F;
			this.Shape68.Left = 2.645833F;
			this.Shape68.Name = "Shape68";
			this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape68.Top = 1.625F;
			this.Shape68.Width = 2.229167F;
			// 
			// label1
			// 
			this.label1.Height = 0.1875F;
			this.label1.HyperLink = null;
			this.label1.Left = 0.3751679F;
			this.label1.Name = "label1";
			this.label1.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.label1.Text = "Has your municipality implemented a local property tax deferral for senior citize" +
    "ns under 36 M.R.S. § 6271?";
			this.label1.Top = 5.256835F;
			this.label1.Width = 6.208333F;
			// 
			// label2
			// 
			this.label2.Height = 0.1875F;
			this.label2.HyperLink = null;
			this.label2.Left = 1.437668F;
			this.label2.Name = "label2";
			this.label2.Style = "font-size: 9pt";
			this.label2.Text = "51a";
			this.label2.Top = 5.486001F;
			this.label2.Width = 0.25F;
			// 
			// label3
			// 
			this.label3.Height = 0.1875F;
			this.label3.HyperLink = null;
			this.label3.Left = 2.875167F;
			this.label3.Name = "label3";
			this.label3.Style = "font-size: 9pt; text-align: right";
			this.label3.Text = "How many people qualified?  51b";
			this.label3.Top = 5.486001F;
			this.label3.Width = 1.9375F;
			// 
			// label4
			// 
			this.label4.Height = 0.1875F;
			this.label4.HyperLink = null;
			this.label4.Left = 2.875167F;
			this.label4.Name = "label4";
			this.label4.Style = "font-size: 9pt; text-align: right";
			this.label4.Text = "How much relief was granted?  51c";
			this.label4.Top = 5.673501F;
			this.label4.Width = 1.9375F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.188F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-size: 9pt; text-align: left";
			this.Label124.Text = "45.";
			this.Label124.Top = 0.8125F;
			this.Label124.Width = 0.313F;
			// 
			// label5
			// 
			this.label5.Height = 0.188F;
			this.label5.HyperLink = null;
			this.label5.Left = 0F;
			this.label5.Name = "label5";
			this.label5.Style = "font-size: 9pt; text-align: left";
			this.label5.Text = "46.";
			this.label5.Top = 1.900833F;
			this.label5.Width = 0.313F;
			// 
			// label6
			// 
			this.label6.Height = 0.188F;
			this.label6.HyperLink = null;
			this.label6.Left = 0F;
			this.label6.Name = "label6";
			this.label6.Style = "font-size: 9pt; text-align: left";
			this.label6.Text = "47.";
			this.label6.Top = 2.4425F;
			this.label6.Width = 0.313F;
			// 
			// label7
			// 
			this.label7.Height = 0.188F;
			this.label7.HyperLink = null;
			this.label7.Left = 0F;
			this.label7.Name = "label7";
			this.label7.Style = "font-size: 9pt; text-align: left";
			this.label7.Text = "48.";
			this.label7.Top = 2.88F;
			this.label7.Width = 0.313F;
			// 
			// label8
			// 
			this.label8.Height = 0.188F;
			this.label8.HyperLink = null;
			this.label8.Left = 0F;
			this.label8.Name = "label8";
			this.label8.Style = "font-size: 9pt; text-align: left";
			this.label8.Text = "49.";
			this.label8.Top = 3.300335F;
			this.label8.Width = 0.313F;
			// 
			// label9
			// 
			this.label9.Height = 0.188F;
			this.label9.HyperLink = null;
			this.label9.Left = 0F;
			this.label9.Name = "label9";
			this.label9.Style = "font-size: 9pt; text-align: left";
			this.label9.Text = "50.";
			this.label9.Top = 3.862834F;
			this.label9.Width = 0.313F;
			// 
			// label10
			// 
			this.label10.Height = 0.188F;
			this.label10.HyperLink = null;
			this.label10.Left = 0F;
			this.label10.Name = "label10";
			this.label10.Style = "font-size: 9pt; text-align: left";
			this.label10.Text = "51.";
			this.label10.Top = 4.562833F;
			this.label10.Width = 0.313F;
			// 
			// label11
			// 
			this.label11.Height = 0.188F;
			this.label11.HyperLink = null;
			this.label11.Left = 0F;
			this.label11.Name = "label11";
			this.label11.Style = "font-size: 9pt; text-align: left";
			this.label11.Text = "52.";
			this.label11.Top = 5.256835F;
			this.label11.Width = 0.313F;
			// 
			// srptMVRPage8
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtSeniorTaxDeferralGranted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberQualifiedForSeniorTaxDeferral)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeniorDeferralYesNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine51a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine51b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine51c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine43a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine44a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine44c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine44b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt43b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine42Due2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine40To)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39a1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt39a2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine40From)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label222)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label224)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label225)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label226)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label228)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label229)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label230)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label231)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label232)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label233)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label234)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label235)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label236)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label237)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label238)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label239)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label240)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label241)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label242)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label243)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label245)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label246)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label247)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label248)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label249)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label250)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label251)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label252)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label253)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label254)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label255)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label256)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineProvision1;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineProvision2;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineProvision3;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineName1;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineName2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label203;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label204;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt39a2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label211;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine40From;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label214;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine40To;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label215;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine41;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine42Due1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine42Due2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label216;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine43a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label222;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt43b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label224;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label225;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label226;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label228;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label229;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label230;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label231;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label232;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label233;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label234;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label235;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt39a1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label236;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine42Due3;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine42Due4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label237;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label238;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label239;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label240;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label241;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label242;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label243;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine44a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label245;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label246;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine44b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label247;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine44c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label248;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label249;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine51a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label250;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label251;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine51b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label252;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine51c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label253;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label254;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label255;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeniorTaxDeferralGranted;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumberQualifiedForSeniorTaxDeferral;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeniorDeferralYesNo;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label label11;
	}
}
