﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptListingNameAddrLocMap.
	/// </summary>
	public partial class rptListingNameAddrLocMap : BaseSectionReport
	{
		public static rptListingNameAddrLocMap InstancePtr
		{
			get
			{
				return (rptListingNameAddrLocMap)Sys.GetInstance(typeof(rptListingNameAddrLocMap));
			}
		}

		protected rptListingNameAddrLocMap _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
                rsTemp = null;
            }
			base.Dispose(disposing);
		}

		public rptListingNameAddrLocMap()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Address Location Map/Lot";
		}
		// nObj = 1
		//   0	rptListingNameAddrLocMap	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		int varAccount;
		int intPage;
		bool boolStartedPrinting;
		int hdlPrinter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTemp = "";
			nextaccount:
			;
			if (rsTemp.EndOfFile())
				return;
			string strCountry;
			strCountry = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("country")));
			if (Strings.LCase(strCountry) == "united states")
			{
				strCountry = "";
			}
			txtAccount.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSAccount"), "00000");
			txtCard.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSCard"), "000");
			txtName.Text = Strings.Trim(rsTemp.Get_Fields_String("RSName") + " ");
			if (Information.IsNumeric(rsTemp.Get_Fields_String("rslocnumalph") + ""))
			{
				strTemp = Conversion.Str(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "")));
				if (Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "") == 0)
					strTemp = " ";
			}
			else
			{
				strTemp = rsTemp.Get_Fields_String("rslocnumalph") + "";
			}
			strTemp += rsTemp.Get_Fields_String("rslocapt") + "";
			txtLocation.Text = Strings.Trim(strTemp + " " + rsTemp.Get_Fields_String("RSLOCStreet") + " ");
			txtMap.Text = Strings.Trim(rsTemp.Get_Fields_String("RSMAPLOT") + " ");
			txtSecondOwner.Text = Strings.Trim(rsTemp.Get_Fields_String("rssecowner") + "");
			txtAddress.Text = Strings.Trim(rsTemp.Get_Fields_String("RSAddr1") + " ");
			txtAddress2.Text = Strings.Trim(rsTemp.Get_Fields_String("rsaddr2") + "");
			txtaddress3.Text = Strings.Trim(Strings.Trim(rsTemp.Get_Fields_String("rsaddr3") + "") + " " + rsTemp.Get_Fields_String("rsstate") + " " + rsTemp.Get_Fields_String("rszip") + " " + rsTemp.Get_Fields_String("rszip4"));
			if (Strings.Trim(txtAddress2.Text) == string.Empty)
			{
				txtAddress2.Text = txtaddress3.Text;
				txtaddress3.Text = "";
			}
			if (Strings.Trim(txtAddress.Text) == string.Empty)
			{
				txtAddress.Text = txtAddress2.Text;
				txtAddress2.Text = txtaddress3.Text;
				txtaddress3.Text = "";
			}
			if (Strings.Trim(txtSecondOwner.Text) == string.Empty)
			{
				txtSecondOwner.Text = txtAddress.Text;
				txtAddress.Text = txtAddress2.Text;
				txtAddress2.Text = txtaddress3.Text;
				txtaddress3.Text = "";
			}
			if (txtAddress2.Text == "")
			{
				txtAddress2.Text = strCountry;
			}
			else if (txtaddress3.Text == "")
			{
				txtaddress3.Text = strCountry;
			}
			// varAccount = rsTemp.fields("rsaccount")
			if (!rsTemp.EndOfFile())
				rsTemp.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_Initialize()
		{
			// If rptListingNameAddrLocMap.Printer <> "" Then
			// rptListingNameAddrLocMap.Printer.PrintQuality = ddPQMedium
			// End If
			// 
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			string prtrname = "";
			object res;
			try
			{
				// On Error GoTo ErrorHandler
				if (!boolStartedPrinting)
				{
					boolStartedPrinting = true;
                }
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In printprogress function", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Document.Printer.RenderMode = 1;
			//modREASValuations.Statics.REC = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtCaption.Text = "Real Estate";
			if (Strings.Trim(this.Document.Printer.PrinterName) != string.Empty)
			{
				//this.Document.Printer.RenderMode = 1;
			}
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			
		}

		private void PageFooter_Format(object sender, EventArgs e)
		{
			//modREASValuations.REC += vbFormFeed;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPage);

		}

		public void Start(bool boolByExtract, bool boolByAccount)
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			string strSQL = "";
			int lngUID;
			if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
			{
				txtCaption2.Text = "Account List by Account";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
			{
				txtCaption2.Text = "Account List by Location";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSMAPLOT")
			{
				txtCaption2.Text = "Account List by Map/Lot";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSNAME")
			{
				txtCaption2.Text = "Account List by Name";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					txtCaption2.Text = txtCaption2.Text + " (" + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " - " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + ")";
				}
				else
				{
					txtCaption2.Text = txtCaption2.Text + " (" + modPrintRoutines.Statics.gstrMinAccountRange + " - " + modPrintRoutines.Statics.gstrMaxAccountRange + ")";
				}
			}

			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolByExtract)
			{
				if (boolByAccount)
				{
					strSQL = strMasterJoin + " where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + ") AND master.rsdeleted = 0 ";
				}
				else
				{
					strSQL = "select  mj.* from (" + strMasterJoinQuery + " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and mj.rsdeleted = 0";
				}
				rsTemp.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
				if (!rsTemp.EndOfFile())
				{
					if (FCConvert.ToString(rsTemp.Get_Fields_String("title")) != string.Empty)
					{
						txtCaption2.Text = rsTemp.Get_Fields_String("title");
					}
				}
			}
			else
			{
				strSQL = strMasterJoin + " where rsdeleted = 0";
			}
			if (boolByAccount)
			{
				strSQL += " and rscard = 1";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
				}
				else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ", CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)   ", modGlobalVariables.strREDatabase);
				}
				else
				{
					if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSMAPLOT" && modPrintRoutines.Statics.gstrMinAccountRange == modPrintRoutines.Statics.gstrMaxAccountRange)
					{
						rsTemp.OpenRecordset(strSQL + " and rsmaplot like '" + modPrintRoutines.Statics.gstrMinAccountRange + "' order by rsmaplot,rsaccount,rscard", modGlobalVariables.strREDatabase);
						// ElseIf UCase(gstrFieldName) = "RSMAPLOT" And UCase(gstrMaxAccountRange) = UCase(gstrMinAccountRange & "ZZZZ") Then
						// call rsTemp.OpenRecordset(strSQL & " and rsmaplot like '" & gstrMinAccountRange & "*' order by rsmaplot,rsaccount,rscard",strredatabase)
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
					}
				}
			}
			else
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " order by rslocstreet, CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					if (Strings.LCase(modPrintRoutines.Statics.gstrFieldName) != "rsaccount")
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " order by rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
				}
			}
			// Me.Show
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "NameAddrLocMap");
		}

	
	
	}
}
