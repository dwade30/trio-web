﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewValEstimate.
	/// </summary>
	partial class rptNewValEstimate
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewValEstimate));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtmuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtassessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalCurr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPrev = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtmuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtassessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtMapLot,
				this.txtNewValuation,
				this.txtCurrValuation,
				this.txtPrevValuation
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalNew,
				this.Line1,
				this.txtTotalCurr,
				this.txtTotalPrev,
				this.Label7,
				this.Label8,
				this.txtCount
			});
			this.ReportFooter.Height = 0.3541667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtmuniname,
				this.txtassessment,
				this.txtdate,
				this.txtpage,
				this.txtTime,
				this.txtRange,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6
			});
			this.PageHeader.Height = 0.90625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtmuniname
			// 
			this.txtmuniname.Height = 0.19F;
			this.txtmuniname.Left = 0F;
			this.txtmuniname.Name = "txtmuniname";
			this.txtmuniname.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmuniname.Text = "Muniname";
			this.txtmuniname.Top = 0F;
			this.txtmuniname.Width = 2.666667F;
			// 
			// txtassessment
			// 
			this.txtassessment.Height = 0.25F;
			this.txtassessment.Left = 2.0625F;
			this.txtassessment.Name = "txtassessment";
			this.txtassessment.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtassessment.Text = "New Valuation Analysis";
			this.txtassessment.Top = 0F;
			this.txtassessment.Width = 3.4375F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.19F;
			this.txtdate.Left = 6F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtdate.Text = "DATE";
			this.txtdate.Top = 0F;
			this.txtdate.Width = 1.5F;
			// 
			// txtpage
			// 
			this.txtpage.Height = 0.2083333F;
			this.txtpage.Left = 6.333333F;
			this.txtpage.Name = "txtpage";
			this.txtpage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtpage.Text = "Page";
			this.txtpage.Top = 0.1666667F;
			this.txtpage.Width = 1.166667F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = "Muniname";
			this.txtTime.Top = 0.1666667F;
			this.txtTime.Width = 1.6875F;
			// 
			// txtRange
			// 
			this.txtRange.Height = 0.19F;
			this.txtRange.Left = 2.0625F;
			this.txtRange.Name = "txtRange";
			this.txtRange.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtRange.Text = null;
			this.txtRange.Top = 0.25F;
			this.txtRange.Width = 3.416667F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: left";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.6979167F;
			this.Label1.Width = 0.625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.7083333F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.6979167F;
			this.Label2.Width = 2.260417F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.145833F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Map/Lot";
			this.Label3.Top = 0.6979167F;
			this.Label3.Width = 0.7708333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.3541667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.729167F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: center";
			this.Label4.Text = "New Valuation";
			this.Label4.Top = 0.53125F;
			this.Label4.Width = 0.7708333F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.3541667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.666667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: center";
			this.Label5.Text = "Prev Valuation";
			this.Label5.Top = 0.53125F;
			this.Label5.Width = 0.7708333F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.3541667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.666667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: center";
			this.Label6.Text = "Curr Valuation";
			this.Label6.Top = 0.53125F;
			this.Label6.Width = 0.7708333F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.7083333F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.385417F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 3.145833F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.25F;
			// 
			// txtNewValuation
			// 
			this.txtNewValuation.Height = 0.19F;
			this.txtNewValuation.Left = 6.479167F;
			this.txtNewValuation.Name = "txtNewValuation";
			this.txtNewValuation.Style = "text-align: right";
			this.txtNewValuation.Text = null;
			this.txtNewValuation.Top = 0F;
			this.txtNewValuation.Width = 1.020833F;
			// 
			// txtCurrValuation
			// 
			this.txtCurrValuation.Height = 0.19F;
			this.txtCurrValuation.Left = 5.5F;
			this.txtCurrValuation.Name = "txtCurrValuation";
			this.txtCurrValuation.Style = "text-align: right";
			this.txtCurrValuation.Text = null;
			this.txtCurrValuation.Top = 0F;
			this.txtCurrValuation.Width = 0.9375F;
			// 
			// txtPrevValuation
			// 
			this.txtPrevValuation.Height = 0.19F;
			this.txtPrevValuation.Left = 4.5F;
			this.txtPrevValuation.Name = "txtPrevValuation";
			this.txtPrevValuation.Style = "text-align: right";
			this.txtPrevValuation.Text = null;
			this.txtPrevValuation.Top = 0F;
			this.txtPrevValuation.Width = 0.9375F;
			// 
			// txtTotalNew
			// 
			this.txtTotalNew.Height = 0.19F;
			this.txtTotalNew.Left = 6.479167F;
			this.txtTotalNew.Name = "txtTotalNew";
			this.txtTotalNew.Style = "text-align: right";
			this.txtTotalNew.Text = null;
			this.txtTotalNew.Top = 0.1666667F;
			this.txtTotalNew.Width = 1.020833F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.572917F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.07291666F;
			this.Line1.Width = 2.885417F;
			this.Line1.X1 = 4.572917F;
			this.Line1.X2 = 7.458333F;
			this.Line1.Y1 = 0.07291666F;
			this.Line1.Y2 = 0.07291666F;
			// 
			// txtTotalCurr
			// 
			this.txtTotalCurr.Height = 0.19F;
			this.txtTotalCurr.Left = 5.5F;
			this.txtTotalCurr.Name = "txtTotalCurr";
			this.txtTotalCurr.Style = "text-align: right";
			this.txtTotalCurr.Text = null;
			this.txtTotalCurr.Top = 0.1666667F;
			this.txtTotalCurr.Width = 0.9375F;
			// 
			// txtTotalPrev
			// 
			this.txtTotalPrev.Height = 0.19F;
			this.txtTotalPrev.Left = 4.5F;
			this.txtTotalPrev.Name = "txtTotalPrev";
			this.txtTotalPrev.Style = "text-align: right";
			this.txtTotalPrev.Text = null;
			this.txtTotalPrev.Top = 0.1666667F;
			this.txtTotalPrev.Width = 0.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.666667F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Total";
			this.Label7.Top = 0.1666667F;
			this.Label7.Width = 0.7708333F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.4166667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Count";
			this.Label8.Top = 0.1666667F;
			this.Label8.Width = 0.5208333F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 0.9166667F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "text-align: left";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.1666667F;
			this.txtCount.Width = 0.625F;
			// 
			// rptNewValEstimate
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtmuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtassessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevValuation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalNew;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurr;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPrev;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtassessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
