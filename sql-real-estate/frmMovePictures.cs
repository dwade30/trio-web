﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMovePictures.
	/// </summary>
	public partial class frmMovePictures : BaseForm
	{
		public frmMovePictures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMovePictures InstancePtr
		{
			get
			{
				return (frmMovePictures)Sys.GetInstance(typeof(frmMovePictures));
			}
		}

		protected frmMovePictures _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By Corey Gray
		// Date 5/10/2004
		// This form goes through the picture table
		// any pictures that aren't in default picture directories are copied
		// into a default picture directory that is specified, and
		// renamed to account-card-picnum format and relinked.
		// The original file can be optionally deleted.
		// ********************************************************
		bool boolUnloadMe;
		string strDirToCopyTo;

		private void frmMovePictures_Activated(object sender, System.EventArgs e)
		{
			if (boolUnloadMe)
			{
				this.Unload();
				return;
			}
		}

		private void frmMovePictures_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMovePictures_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMovePictures properties;
			//frmMovePictures.FillStyle	= 0;
			//frmMovePictures.ScaleWidth	= 5880;
			//frmMovePictures.ScaleHeight	= 4080;
			//frmMovePictures.LinkTopic	= "Form2";
			//frmMovePictures.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			boolUnloadMe = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupCombo();
			if (!SetupCombo())
			{
				MessageBox.Show("There are no default picture directories setup.  There must be at least one to continue.", "No Default Picture Directories", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				boolUnloadMe = true;
				// frmREOptions.Show , MDIParent
				return;
			}
		}

		private bool SetupCombo()
		{
			bool SetupCombo = false;
			// fill grid directories with a list of default directories
			// also fill the combo box with this list
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				GridDirectories.Rows = 0;
				cmbDirectory.Clear();
				clsLoad.OpenRecordset("select * from defaultpicturelocations order by location", modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					SetupCombo = false;
					return SetupCombo;
				}
				SetupCombo = true;
				while (!clsLoad.EndOfFile())
				{
					strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("location")));
					if (strTemp != string.Empty)
					{
						if (Strings.Right(strTemp, 1) != "\\")
							strTemp += "\\";
						cmbDirectory.AddItem(strTemp);
						GridDirectories.AddItem(strTemp);
					}
					clsLoad.MoveNext();
				}
				cmbDirectory.SelectedIndex = 0;
				return SetupCombo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SetupCombo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SetupCombo;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// get a list of default picture directories
			// look at all pictures.  Any that aren't in the default directories get copied to
			// the specified directory and get renamed to account-card-picnum format.
			// Depending on the option chosen, the original files may be deleted.
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			string strPath = "";
			string strFileName = "";
			string strNewFileName = "";
			int lngAcct = 0;
			int intCard = 0;
			int intPicNum = 0;
			//FileSystemObject fso = new FileSystemObject();
			int x = 0;
			string strExtension = "";
			bool boolDeleteOriginal = false;
			string strOriginalFile = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (cmbCopy.Text == "Copy and re-name, then delete original file")
				{
					boolDeleteOriginal = true;
				}
				else
				{
					boolDeleteOriginal = false;
				}
				strDirToCopyTo = Strings.UCase(cmbDirectory.Text);
				if (Strings.Right(strDirToCopyTo, 1) != "\\")
				{
					strDirToCopyTo += "\\";
				}
				if (boolDeleteOriginal)
				{
					modGlobalFunctions.AddCYAEntry_80("RE", "Copy/Move Pictures", "Chose to delete originals", "Copying to " + strDirToCopyTo);
				}
				else
				{
					modGlobalFunctions.AddCYAEntry_80("RE", "Copy/Move Pictures", "Chose not to delete originals", "Copying to " + strDirToCopyTo);
				}
				frmWait.InstancePtr.Init("Copying Picture Files", false, 100, true);
				clsLoad.OpenRecordset("select * from PICTURERECORD", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("mraccountnumber"))));
					intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rscard"))));
					intPicNum = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("picnum"))));
					strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("picturelocation")));
					strPath = GetPathName(ref strTemp);
					strFileName = GetPicFileName(ref strTemp);
					strOriginalFile = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("picturelocation")));
					x = Strings.InStrRev(strFileName, ".", -1, CompareConstants.vbTextCompare);
					if (x > 0)
					{
						strExtension = Strings.Mid(strFileName, x);
					}
					else
					{
						strExtension = "";
					}
					frmWait.InstancePtr.lblMessage.Text = "Copying " + strOriginalFile;
					frmWait.InstancePtr.lblMessage.Refresh();
					if (strPath != string.Empty)
					{
						if (!PathInList(ref strPath))
						{
							if (File.Exists(FCConvert.ToString(clsLoad.Get_Fields_String("picturelocation"))))
							{
								// must copy,rename etc
								// start the pic number at the picnum in database
								x = intPicNum;
								strNewFileName = "";
								// try to make file name, if it exists, then add one to picnum and try again
								while (strNewFileName == string.Empty)
								{
									strNewFileName = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(intCard) + "-" + FCConvert.ToString(x) + strExtension;
									if (File.Exists(strDirToCopyTo + strNewFileName))
									{
										x += 1;
										strNewFileName = "";
									}
								}
								// we have a new filename, now copy it
								File.Copy(strOriginalFile, strDirToCopyTo + strNewFileName, true);
								// now relink it
								clsLoad.Edit();
								clsLoad.Set_Fields("picturelocation", strDirToCopyTo + strNewFileName);
								clsLoad.Update();
								if (boolDeleteOriginal)
								{
									// user has opted to delete original photo
									File.Delete(strOriginalFile);
								}
							}
						}
					}
					clsLoad.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				if (boolDeleteOriginal)
				{
					MessageBox.Show("Move Complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Copy Complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuSaveContinue_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool PathInList(ref string strPath)
		{
			bool PathInList = false;
			string strTemp;
			string strListPath = "";
			int x;
			PathInList = false;
			strTemp = Strings.Trim(Strings.UCase(strPath));
			if (Strings.Right(strTemp, 1) != "\\")
			{
				strTemp += "\\";
			}
			for (x = 0; x <= GridDirectories.Rows - 1; x++)
			{
				strListPath = Strings.Trim(Strings.UCase(GridDirectories.TextMatrix(x, 0)));
				if (Strings.Right(strListPath, 1) != "\\")
				{
					strListPath += "\\";
				}
				if (strListPath == strTemp)
				{
					// match
					PathInList = true;
					return PathInList;
				}
			}
			// x
			return PathInList;
		}

		private string GetPathName(ref string strFileName)
		{
			string GetPathName = "";
			string[] strAry = null;
			int x;
			string strReturn;
			GetPathName = "";
			strReturn = "";
			strAry = Strings.Split(strFileName, "\\", -1, CompareConstants.vbTextCompare);
			for (x = 0; x <= Information.UBound(strAry, 1) - 1; x++)
			{
				strReturn += strAry[x] + "\\";
			}
			// x
			GetPathName = strReturn;
			return GetPathName;
		}

		private string GetPicFileName(ref string strFileName)
		{
			string GetPicFileName = "";
			string[] strAry = null;
			GetPicFileName = strFileName;
			strAry = Strings.Split(strFileName, "\\", -1, CompareConstants.vbTextCompare);
			GetPicFileName = strAry[Information.UBound(strAry, 1)];
			return GetPicFileName;
		}
	}
}
