﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCLNote.
	/// </summary>
	partial class frmCLNote : BaseForm
	{
		public fecherFoundation.FCCheckBox chkInRE;
		public fecherFoundation.FCCheckBox chkPopUp;
		public fecherFoundation.FCTextBox txtNote;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCLNote));
            this.chkInRE = new fecherFoundation.FCCheckBox();
            this.chkPopUp = new fecherFoundation.FCCheckBox();
            this.txtNote = new fecherFoundation.FCTextBox();
            this.cmdSaveContinue = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPopUp)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 287);
            this.BottomPanel.Size = new System.Drawing.Size(414, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkInRE);
            this.ClientArea.Controls.Add(this.chkPopUp);
            this.ClientArea.Controls.Add(this.txtNote);
            this.ClientArea.Size = new System.Drawing.Size(414, 227);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(414, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(192, 30);
            this.HeaderText.Text = "Collections Note";
            // 
            // chkInRE
            // 
            this.chkInRE.Location = new System.Drawing.Point(30, 177);
            this.chkInRE.Name = "chkInRE";
            this.chkInRE.Size = new System.Drawing.Size(242, 27);
            this.chkInRE.TabIndex = 2;
            this.chkInRE.Text = "Display pop up in Real Estate";
            // 
            // chkPopUp
            // 
            this.chkPopUp.Location = new System.Drawing.Point(30, 130);
            this.chkPopUp.Name = "chkPopUp";
            this.chkPopUp.Size = new System.Drawing.Size(238, 27);
            this.chkPopUp.TabIndex = 1;
            this.chkPopUp.Text = "Display pop up in Collections";
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtNote.BackColor = System.Drawing.SystemColors.Window;
            this.txtNote.Location = new System.Drawing.Point(30, 30);
            this.txtNote.MaxLength = 255;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(352, 80);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(124, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(156, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmCLNote
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(414, 395);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCLNote";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Collections Note";
            this.Load += new System.EventHandler(this.frmCLNote_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCLNote_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPopUp)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSaveContinue;
	}
}
