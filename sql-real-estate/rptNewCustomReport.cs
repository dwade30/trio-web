﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewCustomReport.
	/// </summary>
	public partial class rptNewCustomReport : BaseSectionReport
	{
		public static rptNewCustomReport InstancePtr
		{
			get
			{
				return (rptNewCustomReport)Sys.GetInstance(typeof(rptNewCustomReport));
			}
		}

		protected rptNewCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCustom?.Dispose();
				clsMast?.Dispose();
				clsDwel?.Dispose();
				clsOut?.Dispose();
				clsComm?.Dispose();
                clsComm = null;
                clsOut = null;
                clsDwel = null;
                clsMast = null;
                clsCustom = null;
            }
			base.Dispose(disposing);
		}

		public rptNewCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Report";
		}
		// nObj = 1
		//   0	rptNewCustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		float lngTotWidth;
		int woogy;
		int numCols;
		int numRows;
		clsDRWrapper clsCustom = new clsDRWrapper();
		clsDRWrapper clsMast = new clsDRWrapper();
		clsDRWrapper clsDwel = new clsDRWrapper();
		clsDRWrapper clsOut = new clsDRWrapper();
		clsDRWrapper clsComm = new clsDRWrapper();
		int intPage;
		// Dim aryCodes(500) As CustomCodeRec
		bool boolFromSales;
		string[] aryTots = null;
		string[] aryCounts = null;
		clsReportCodes clCodeList = new clsReportCodes();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int intReturn = 0;
			
			if (lngTotWidth > 10800 / 1440f)
			{
				//intReturn = modCustomPageSize.SelectForm("CustomReport", 0, 355600, 279400);
				//if (intReturn > 0)
				//{
				//	FCGlobal.Printer.PaperSize = FCConvert.ToInt16(intReturn);
				//	if (this.Document.Printer.PaperWidth < 13000 / 1440f)
				//	{
				//		// can't do that paper size
				//		this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				//		;
				//		this.Document.Printer.DefaultPageSettings.Landscape = true;
				//	}
				//	else
				//	{
				//		this.PageSettings.PaperWidth = this.Document.Printer.PaperWidth;
				//	}
				//	this.PrintWidth = this.PageSettings.PaperWidth - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
				//}
				//else
				//{
					this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
					this.Document.Printer.DefaultPageSettings.Landscape = true;
					this.PrintWidth = this.PageSettings.PaperWidth - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
				//}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsCustom.EndOfFile();
			if (eArgs.EOF)
				return;
			FillDetail();
		}

		public void Init(ref int lngRepNumber, ref bool boolByExtract, ref string strPrinter, ref string strFont, ref bool boolUseFont, string strOrderBy = "")
		{
			// Takes a report number and print the custom report based on that format
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsCustom.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL = "";
			int x;
			// vbPorter upgrade warning: CRow As short --> As int	OnWriteFCConvert.ToDouble(
			int CRow = 0;
			int CCol = 0;
			GrapeCity.ActiveReports.SectionReportModel.TextBox ctl;
			float intLeftPos = 0;
			int intLastWidth;
			string strTemp = "";
			string strOrder;
			int lngUID;
			float lngWidestCountHeading;
			int intCurCountCtl;
			float lngLowestPoint;
            try
            {
                // On Error GoTo ErrorHandler
                aryTots = new string[1 + 1];
                aryCounts = new string[1 + 1];
                lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                // MDIParent.CommonDialog1.ShowPrinter
                this.Document.Printer.PrinterName = strPrinter;
                //this.Document.Printer.RenderMode = 1;
                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                strOrder = strOrderBy;
                clsTemp.OpenRecordset(
                    "select * from customreports where reportnumber = " + FCConvert.ToString(lngRepNumber) +
                    " and isnull(rownumber,-10) = -10", modGlobalVariables.strREDatabase);
                if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("reporttitle"))) != string.Empty)
                {
                    lblTitle.Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("reporttitle")));
                }

                // get the column information and set up the grids
                clsTemp.OpenRecordset(
                    "select count(*) as numcols from customreports where reportnumber = " +
                    FCConvert.ToString(lngRepNumber) + " and rownumber = 1", modGlobalVariables.strREDatabase);
                // TODO Get_Fields: Field [numcols] not found!! (maybe it is an alias?)
                numCols = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("numcols"))));
                lngLowestPoint = 0;
                clsTemp.OpenRecordset(
                    "select count(rownumber) as numrows from customreports where reportnumber = " +
                    FCConvert.ToString(lngRepNumber) + " and rownumber > 0 group by rownumber",
                    modGlobalVariables.strREDatabase);
                numRows = clsTemp.RecordCount();
                // numRows = Val(clsTemp.Fields("numrows"))
                lblCount.Top = (225 / 1440f * (numRows + 2 / 1440f)) + Line1.Y1 + 120 / 1440f;
                clsTemp.OpenRecordset(
                    "select * from customreports where reportnumber = " + FCConvert.ToString(lngRepNumber) +
                    " and isnull(reporttitle,'notatitle') = 'notatitle' order by rownumber,colnumber",
                    modGlobalVariables.strREDatabase);
                intCurCountCtl = 0;
                lngWidestCountHeading = 0;
                while (!clsTemp.EndOfFile())
                {
                    CRow = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int16("rownumber")) - 1);
                    CCol = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("colnumber"))));
                    if (CCol == 0)
                        intLeftPos = 0;
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                    ctl.Top = 450 / 1440f + (CRow * 225 / 1440f);
                    ctl.Width = FCConvert.ToSingle(clsTemp.Get_Fields_Int16("colwidth") / 1440F);
                    ctl.Left = intLeftPos;
                    if (Strings.UCase(FCConvert.ToString(clsTemp.Get_Fields_String("justification"))) == "R")
                    {
                        ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                    }
                    else
                    {
                        ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                    }

                    if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("header"))) != string.Empty)
                    {
                        ctl.Height = 225 / 1440f;
                    }
                    else
                    {
                        ctl.Height = 0;
                    }

                    // ctl.Height = 225
                    strTemp = "HeadRow" + FCConvert.ToString(CRow) + "Col" + FCConvert.ToString(CCol);
                    // ctl.Name = "HeaderRow" & CRow & "Col" & CCol
                    ctl.Name = strTemp;
                    ctl.Font = new Font(ctl.Font, FontStyle.Bold);
                    Fields.Add(ctl.Name);
                    ctl.DataField = ctl.Name;
                    // If boolUseFont Then
                    // ctl.Font.Name = strFont
                    // Else
                    ctl.Font = new Font("Courier New", ctl.Font.Size);
                    // End If
                    if (ctl.Height + ctl.Top > lngLowestPoint && ctl.Height > 0)
                    {
                        lngLowestPoint = ctl.Height + ctl.Top;
                    }

                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    if (Conversion.Val(clsTemp.Get_Fields("code")) != 0)
                    {
                        Fields[strTemp].Value = clsTemp.Get_Fields_String("Header");
                    }

                    PageHeader.Controls.Add(ctl);
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                    // ctl.Top = 50 + (CRow * 225)
                    ctl.Top = (CRow * 225 / 1440f);
                    ctl.Height = 225 / 1440f;
                    ctl.Width = FCConvert.ToSingle(clsTemp.Get_Fields_Int16("colwidth") / 1440F);
                    ctl.Left = intLeftPos;
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    ctl.Tag = clsTemp.Get_Fields("code");
                    ctl.Height = 225 / 1440f;
                    if (Strings.UCase(FCConvert.ToString(clsTemp.Get_Fields_String("justification"))) == "R")
                    {
                        ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                    }
                    else
                    {
                        ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                    }

                    strTemp = "DetRow" + FCConvert.ToString(CRow) + "Col" + FCConvert.ToString(CCol);
                    ctl.Name = strTemp;
                    Fields.Add(ctl.Name);
                    ctl.DataField = ctl.Name;
                    // If boolUseFont Then
                    // ctl.Font.Name = strFont
                    // Else
                    ctl.Font = new Font("Courier New", ctl.Font.Size);
                    // End If
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    Fields[strTemp].Tag = clsTemp.Get_Fields("code");
                    // TODO Get_Fields: Check the table for the column [CODE] and replace with corresponding Get_Field method
                    if (FCConvert.ToInt32(clsTemp.Get_Fields("CODE")) == 0)
                    {
                        Fields[strTemp].Value = clsTemp.Get_Fields_String("Header");
                    }

                    intLeftPos += ctl.Width;
                    lngTotWidth = intLeftPos;
                    Detail.Controls.Add(ctl);
                    if (Conversion.Val(clsTemp.Get_Fields_Int16("totaloption")) > 0)
                    {
                        ReportFooter.Visible = true;
                        ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                        ctl.Height = 225 / 1440f;
                        if (Conversion.Val(clsTemp.Get_Fields_Int16("totaloption")) == 1)
                        {
                            // total
                            Array.Resize(ref aryTots, Information.UBound(aryTots, 1) + 1 + 1);
                            aryTots[Information.UBound(aryTots)] = strTemp;
                            ctl.Top = (CRow * 225 / 1440f) + Line1.Y1 + 120 / 1440f;
                            ctl.Left = Detail.Controls[strTemp].Left;
                            ctl.Width = FCConvert.ToSingle(clsTemp.Get_Fields_Int16("colwidth") / 1440F);
                            ctl.Alignment =
                                (Detail.Controls[strTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                .Alignment;
                            ctl.Name = "TotalFor" + strTemp;
                            //FC:FINAL:MSH - i.issue #1631: add default value for textbox
                            ctl.Text = "Field1";
                            if (ReportFooter.Height < ctl.Top + ctl.Height)
                            {
                                ReportFooter.Height = ctl.Top + ctl.Height + 20 / 1440f;
                            }

                            ReportFooter.Controls.Add(ctl);
                        }
                        else if (Conversion.Val(clsTemp.Get_Fields_Int16("totaloption")) == 2)
                        {
                            // count
                            Array.Resize(ref aryCounts, Information.UBound(aryCounts, 1) + 1 + 1);
                            aryCounts[Information.UBound(aryCounts)] = strTemp;
                            lblCount.Visible = true;
                            ctl.Width = FCConvert.ToSingle(clsTemp.Get_Fields_Int16("colwidth") / 1440F);
                            if (lngWidestCountHeading < ctl.Width)
                                lngWidestCountHeading = ctl.Width;
                            // ctl.Left = (lblCount.Left + lblCount.Width) - ctl.Width
                            ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                            intCurCountCtl += 1;
                            ctl.Top = (intCurCountCtl * 225 / 1440f) + lblCount.Top;
                            ctl.Name = "TotalFor" + strTemp;
                            //FC:FINAL:MSH - i.issue #1631: add default value for textbox
                            ctl.Text = "Field1";
                            if (ReportFooter.Height < ctl.Top + ctl.Height)
                            {
                                ReportFooter.Height = ctl.Top + ctl.Height + 20 / 1440f;
                            }

                            ReportFooter.Controls.Add(ctl);
                            ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                            ctl.Height = 225 / 1440f;
                            ctl.Left = lblCount.Left;
                            ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                            ctl.Width = FCConvert.ToSingle(clsTemp.Get_Fields_Int16("colwidth") / 1440F);
                            ctl.Top = (intCurCountCtl * 225 / 1440f) + lblCount.Top;
                            ctl.Name = "lbl" + strTemp;
                            ctl.Text = clsTemp.Get_Fields_String("Header");
                            ctl.Font = new Font(ctl.Font, FontStyle.Bold);
                            ctl.Font = new Font("Tahoma", ctl.Font.Size);
                            ctl.Font = new Font(ctl.Font.Name, 10);
                            ReportFooter.Controls.Add(ctl);
                        }

                        // now add the total/count field
                    }

                    clsTemp.MoveNext();
                }

                if (PageHeader.Height < lngLowestPoint + 30 / 1440f)
                {
                    PageHeader.Height = lngLowestPoint + 30 / 1440f;
                }

                for (x = 0; x <= Information.UBound(aryCounts, 1); x++)
                {
                    if (Strings.Trim(aryCounts[x]) != string.Empty)
                    {
                        ReportFooter.Controls["TotalFor" + aryCounts[x]].Left =
                            lblCount.Left + (lngWidestCountHeading * 2) -
                            ReportFooter.Controls["TotalFor" + aryCounts[x]].Width;
                    }
                }

                // x
                boolFromSales = false;
                if (boolByExtract)
                {
                    clsTemp.OpenRecordset(
                        "select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0",
                        modGlobalVariables.strREDatabase);
                    if (Strings.UCase(FCConvert.ToString(clsTemp.Get_Fields_String("EXTTYPE"))) == "SALE")
                    {
                        boolFromSales = true;
                    }

                    if (boolFromSales)
                    {
                        clsTemp.OpenRecordset("select * from extracttable where userid = " + FCConvert.ToString(lngUID),
                            modGlobalVariables.strREDatabase);
                        if (Conversion.Val(clsTemp.Get_Fields_Int32("saleid")) > 0)
                        {
                            strSQL =
                                "select  srmaster.* from (srmaster inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber) and (srmaster.saleid = extracttable.saleid)) where extracttable.userid = " +
                                FCConvert.ToString(lngUID) + " and srmaster.rsdeleted = 0";
                        }
                        else
                        {
                            strSQL =
                                "select  srmaster.* from (srmaster inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber)) where extracttable.userid = " +
                                FCConvert.ToString(lngUID) + " and srmaster.rsdeleted = 0";
                        }
                    }
                    else
                    {
                        strSQL =
                            "select  master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " +
                            FCConvert.ToString(lngUID) + " and master.rsdeleted = 0";
                    }
                }
                else
                {
                    // strSQL = "select * from master where rsdeleted = 0"
                    strSQL = strMasterJoin + " where rsdeleted = 0";
                }

                if (numRows == 1)
                {
                    Detail.Height = (numRows * 225 / 1440f);
                }
                else
                {
                    Detail.Height = (numRows * 225 / 1440F) + 225 / 1440f;
                }

                Detail.CanShrink = false;
                // If boolByAccount Then
                // strSQL = strSQL & " and master.rscard = 1"
                // End If
                if (boolFromSales)
                {
                    strOrder = Strings.Replace(strOrder, "master", "srmaster", 1, -1, CompareConstants.vbTextCompare);
                }

                if (modGlobalVariables.Statics.boolRange)
                {
                    if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
                    {
                        // Call clsCustom.OpenRecordset(strSQL & " and " & gstrFieldName & " >= " & gintMinAccountRange & " and " & gstrFieldName & " <= " & gintMaxAccountRange & " order by " & gstrFieldName & ",RSCard", strREDatabase)
                        clsCustom.OpenRecordset(
                            strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= " +
                            FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " +
                            modPrintRoutines.Statics.gstrFieldName + " <= " +
                            FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + strOrder,
                            modGlobalVariables.strREDatabase);
                        // Case "RSLOCSTREET"
                        // Call clsCustom.OpenRecordset(strSQL & " and " & gstrFieldName & " >= '" & gstrMinAccountRange & "' and " & gstrFieldName & " <= '" & gstrMaxAccountRange & "' order by " & gstrFieldName & ", val(rslocnumalph & '') ", strREDatabase)
                        // Call clsCustom.OpenRecordset(strSQL & " and " & gstrFieldName & " >= '" & gstrMinAccountRange & "' and " & gstrFieldName & " <= '" & gstrMaxAccountRange & "'" & strOrder, strREDatabase)
                    }
                    else
                    {
                        // Call clsCustom.OpenRecordset(strSQL & " and " & gstrFieldName & " >= '" & gstrMinAccountRange & "' and " & gstrFieldName & " <= '" & gstrMaxAccountRange & "' order by " & gstrFieldName & ",rsaccount,RSCard", strREDatabase)
                        clsCustom.OpenRecordset(
                            strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" +
                            modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                            modPrintRoutines.Statics.gstrFieldName + " <= '" +
                            modPrintRoutines.Statics.gstrMaxAccountRange + "'" + strOrder,
                            modGlobalVariables.strREDatabase);
                    }
                }
                else
                {
                    clsCustom.OpenRecordset(strSQL + strOrder, modGlobalVariables.strREDatabase);
                    // If UCase(gstrFieldName) = "RSLOCSTREET" Then
                    // If boolFromSales Then
                    // Call clsCustom.OpenRecordset(strSQL & " order by srmaster.rslocstreet, val(srmaster.rslocnumalph & '') ", strREDatabase)
                    // Else
                    // Call clsCustom.OpenRecordset(strSQL & " order by master.rslocstreet, val(master.rslocnumalph & '') ", strREDatabase)
                    // End If
                    // Else
                    // If boolFromSales Then
                    // Call clsCustom.OpenRecordset(strSQL & " order by srmaster." & gstrFieldName & ",srmaster.rscard", strREDatabase)
                    // Else
                    // Call clsCustom.OpenRecordset(strSQL & " order by master." & gstrFieldName & ",master.rscard", strREDatabase)
                    // End If
                    // End If
                }

                clCodeList.LoadCodes();
                // Call clsTemp.OpenRecordset("select * from reportcodes", strREDatabase)
                // Do While Not clsTemp.EndOfFile
                // aryCodes(clsTemp.Fields("code")).FieldName = clsTemp.Fields("fieldname")
                // aryCodes(clsTemp.Fields("code")).TableName = clsTemp.Fields("tablename")
                // aryCodes(clsTemp.Fields("code")).SpecialCase = clsTemp.Fields("specialcase")
                // aryCodes(clsTemp.Fields("code")).Code = clsTemp.Fields("code")
                // clsTemp.MoveNext
                // Loop
                frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName);
                // frmReportViewer.ARViewer21.ReportSource = Me
                // Me.Show
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "in Custom Report Init function", null,
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				clsTemp.Dispose();
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			int intIndex;
			// intIndex = Me.ToolBar.Tools.Count
			// Me.ToolBar.Tools.Add ""
			// Me.ToolBar.Tools(intIndex).Caption = ""
			// Me.ToolBar.Tools(intIndex).Type = 2
			// Me.ToolBar.Tools(intIndex).ID = "9990"
			// Me.ToolBar.Tools.Add "PRINT TO FILE"
			// Me.ToolBar.Tools(intIndex + 1).ID = "9950"
			// Me.ToolBar.Tools.Add ""
			// Me.ToolBar.Tools(intIndex + 2).Type = 2
			// Me.ToolBar.Tools(intIndex + 2).ID = "9990"
			intPage = 1;
			// Call SetFixedSizeReport(Me, MDIParent.Grid)
			// const_printtoolid = 9951
			// For cnt = 0 To Me.ToolBar.Tools.Count - 1
			// If "Print..." = Me.ToolBar.Tools(cnt).Caption Then
			// Me.ToolBar.Tools(cnt).ID = const_printtoolid
			// Me.ToolBar.Tools(cnt).Enabled = True
			// End If
			// Next cnt
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			this.Document.Printer.DefaultPageSettings.Landscape = false;
			frmReportViewer.InstancePtr.Unload();
		}
		
		private object getValueForCode(int lngCode)
		{
			object getValueForCode = null;
			try
			{
				// On Error GoTo ErrorHandler
				if (lngCode < 1)
				{
					getValueForCode = "";
					return getValueForCode;
				}
				clsReportParameter rp;
				rp = clCodeList.GetCodeByCode(lngCode);
				if (rp.SpecialCase)
				{
					getValueForCode = HandleSpecialCodes(lngCode);
				}
				else
				{
					getValueForCode = HandleRegularCodes(lngCode, ref rp);
				}
				return getValueForCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in Get Value for Code", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return getValueForCode;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, double, object)
		private object HandleSpecialCodes(int lngCode)
		{
			object HandleSpecialCodes = null;
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, double, int)
			string strTemp = "";
			// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)	OnRead(string)
			int lngTemp = 0;
			int lngTemp2 = 0;
			string strTemp2 = "";
			HandleSpecialCodes = "";
			try
			{
				// On Error GoTo ErrorHandler
				switch (lngCode)
				{
					case 7:
						{
							HandleSpecialCodes = clsMast.Get_Fields_Int32("pitopography1") + clsMast.Get_Fields_Int32("pitopography2") + "";
							break;
						}
					case 8:
						{
							HandleSpecialCodes = clsMast.Get_Fields_Int32("piutilities1") + clsMast.Get_Fields_Int32("piutilities2");
							break;
						}
					case 15:
						{
							if ((FCConvert.ToDateTime(clsMast.Get_Fields_DateTime("saledate") as object).ToOADate() != 0) && (FCConvert.ToString(clsMast.Get_Fields_DateTime("saledate")) != string.Empty))
							{
								HandleSpecialCodes = clsMast.Get_Fields_DateTime("saledate") + "";
							}
							else
							{
								HandleSpecialCodes = "";
							}
							break;
						}
					case 18:
						{
							HandleSpecialCodes = clsMast.Get_Fields_Int32("riexemptcd1") + " " + clsMast.Get_Fields_Int32("riexemptcd2") + " " + clsMast.Get_Fields_Int32("riexemptcd3") + "";
							break;
						}
					case 24:
						{
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMast.Get_Fields_Int32("PINeighborhood"))));
							if (lngTemp == 0)
								lngTemp = 1;
							lngTemp2 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMast.Get_Fields_Int32("PIZone"))));
							// If lngTemp2 > 50 Then
							// lngTemp2 = lngTemp2 - 50
							// Else
							// lngTemp2 = lngTemp2 - 10
							// End If
							if (FCConvert.ToBoolean(clsMast.Get_Fields_Boolean("zoneoverride")))
							{
								lngTemp2 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMast.Get_Fields_Int32("piseczone"))));
							}
							// If Val(clsMast.Fields("piseczone")) > 50 Then
							// lngTemp2 = Val(clsMast.Fields("piseczone")) - 50
							// End If
							if (lngTemp2 == 0)
								lngTemp2 = 1;
							// If lngTemp2 > 40 Then
							// lngTemp = 1100 + lngTemp
							// Else
							// lngTemp = lngTemp2 * 100 + lngTemp + 1000
							// End If
							// lngtemp is now the landtable KEY
							// Call clsTemp.OpenRecordset("select * from landTRECORD where lrecordnumber = " & lngTemp, strREDatabase)
							// If Not clsTemp.EndOfFile Then
							// HandleSpecialCodes = LandTRec.LandFactor(lngTemp, lngTemp2)
							HandleSpecialCodes = modGlobalVariables.Statics.LandTRec.Schedule(ref lngTemp, ref lngTemp2);
							// End If
							break;
						}
					case 27:
						{
							HandleSpecialCodes = Conversion.Val(clsMast.Get_Fields_Int32("hlcompvalland")) + Conversion.Val(clsMast.Get_Fields_Int32("hlcompvalbldg"));
							break;
						}
					case 30:
						{
							HandleSpecialCodes = Conversion.Val(clsMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsMast.Get_Fields_Int32("lastbldgval"));
							break;
						}
					case 34:
						{
							HandleSpecialCodes = Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rsmaplot")));
							break;
						}
					case 39:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rSADDR3"))) + " " + Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rsstate"))) + " " + Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rszip4"))));
							}
							break;
						}
					case 40:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rslocnumalph"))) + Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("RSLOCAPT")))) + " " + Strings.Trim(FCConvert.ToString(clsMast.Get_Fields_String("rslocstreet"))));
							}
							break;
						}
					case 43:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(Strings.Format(clsMast.Get_Fields_String("rstelephone"), "(000)###-####"));
							}
							break;
						}
					case 44:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Conversion.Val(clsMast.Get_Fields_Int32("hlcompvalland")) + Conversion.Val(clsMast.Get_Fields_Int32("hlcompvalbldg")) - Conversion.Val(clsMast.Get_Fields_Int32("rlexemption"));
							}
							break;
						}
					case 45:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Conversion.Val(clsMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsMast.Get_Fields_Int32("lastbldgval")) - Conversion.Val(clsMast.Get_Fields_Int32("rlexemption"));
							}
							break;
						}
					case 46:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Conversion.Val(clsMast.Get_Fields_Int32("rllandval")) + Conversion.Val(clsMast.Get_Fields_Int32("rlbldgval")) - Conversion.Val(clsMast.Get_Fields_Int32("rlexemption"));
							}
							break;
						}
					case 50:
						{
							if (!clsMast.EndOfFile())
							{
								if (fecherFoundation.FCUtils.IsEmptyDateTime(clsMast.Get_Fields_DateTime("DateInspected")))
								{
									HandleSpecialCodes = "";
								}
								else
								{
									if (FCConvert.ToDateTime(clsMast.Get_Fields_DateTime("Dateinspected") as object).ToOADate() == 0)
									{
										HandleSpecialCodes = "";
									}
									else
									{
										HandleSpecialCodes = Strings.Format(clsMast.Get_Fields_DateTime("DateInspected"), "MM/dd/yyyy");
									}
								}
							}
							break;
						}
					case 52:
						{
							if (!clsMast.EndOfFile())
							{
								HandleSpecialCodes = Conversion.Val(clsMast.Get_Fields_String("rslocnumalph") + "");
							}
							break;
						}
					case 61:
						{
							// book & Page
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select * from srbookpage where account = " + clsCustom.Get_Fields_Int32("rsaccount") + " and current and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "# order by line desc ", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select * from bookpage where account = " + clsCustom.Get_Fields_Int32("rsaccount") + " and current order by line desc", modGlobalVariables.strREDatabase);
							}
							strTemp = "";
							while (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
								strTemp += " B" + clsTemp.Get_Fields("Book") + "P" + clsTemp.Get_Fields("page");
								if (Information.IsDate(clsTemp.Get_Fields("bpdate")))
								{
									if (clsTemp.Get_Fields_DateTime("bpdate").ToOADate() != 0)
									{
										strTemp += " " + Strings.Format(clsTemp.Get_Fields_DateTime("bpdate"), "MM/dd/yy");
									}
								}
								clsTemp.MoveNext();
							}
							strTemp = Strings.Trim(strTemp);
							HandleSpecialCodes = strTemp;
							break;
						}
					case 62:
						{
							// ref1 without gemini
							strTemp = clsMast.Get_Fields_String("rsref1") + Strings.StrDup(22, " ");
							strTemp = Strings.Mid(strTemp, 1, 22);
							strTemp = Strings.Trim(strTemp);
							HandleSpecialCodes = strTemp;
							break;
						}
					case 63:
						{
							// ref1 gemini account
							strTemp = clsMast.Get_Fields_String("rsref1") + Strings.StrDup(36, " ");
							strTemp = Strings.Mid(strTemp, 24, 12);
							strTemp = Strings.Trim(strTemp);
							HandleSpecialCodes = strTemp;
							break;
						}
					case 64:
						{
							// change assessment
							strTemp = FCConvert.ToString((Conversion.Val(clsMast.Get_Fields_Int32("rllandval")) + Conversion.Val(clsMast.Get_Fields_Int32("rlbldgval"))) - (Conversion.Val(clsMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsMast.Get_Fields_Int32("lastbldgval"))));
							HandleSpecialCodes = strTemp;
							break;
						}
					case 65:
						{
							// tax amount
							strTemp = Strings.Format(FCConvert.ToDouble((Conversion.Val(clsMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsMast.Get_Fields_Int32("lastbldgval")) - Conversion.Val(clsMast.Get_Fields_Int32("rlexemption")))) * (Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.BillRate) / 1000), "#,###,##0.00");
							HandleSpecialCodes = strTemp;
							break;
						}
					case 66:
						{
							lngTemp = modGlobalVariables.Statics.CustomizedInfo.BillYear;
							if (lngTemp == 0)
							{
								lngTemp = DateTime.Today.Year;
							}
							clsTemp.OpenRecordset("select assessment from previousassessment where taxyear = " + FCConvert.ToString(lngTemp - 1) + " and account = " + clsMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							if (!clsTemp.EndOfFile())
							{
								lngTemp = FCConvert.ToInt32(Conversion.Val(clsMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsMast.Get_Fields_Int32("lastbldgval")) - Conversion.Val(clsMast.Get_Fields_Int32("rlexemption")) - Conversion.Val(clsTemp.Get_Fields_Int32("assessment")));
							}
							else
							{
								lngTemp = 0;
							}
							strTemp = FCConvert.ToString(lngTemp);
							HandleSpecialCodes = strTemp;
							break;
						}
					case 67:
						{
							// date updated
							if (FCConvert.ToDateTime(clsMast.Get_Fields_DateTime("hlupdate") as object).ToOADate() != 0)
							{
								strTemp = FCConvert.ToString(clsMast.Get_Fields_DateTime("hlupdate"));
								HandleSpecialCodes = strTemp;
							}
							else
							{
								strTemp = "";
								HandleSpecialCodes = strTemp;
							}
							break;
						}
					case 70:
						{
							// number of pictures
							if (boolFromSales)
							{
								HandleSpecialCodes = "0";
							}
							else
							{
								clsTemp.OpenRecordset("select count(mraccountnumber) as thecount from picturerecord where mraccountnumber = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thecount"));
							}
							else
							{
								HandleSpecialCodes = "0";
							}
							break;
						}
					case 71:
						{
							// number of sketches
							if (boolFromSales)
							{
								HandleSpecialCodes = "0";
							}
							else
							{
								// will have to find any sketch files that exist for this account
								lngTemp = 0;
								clsTemp.OpenRecordset("select id from master where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
								while (!clsTemp.EndOfFile())
								{
									// for each card find all files
									strTemp = Convert.ToString(FCConvert.ToInt16(clsTemp.Get_Fields_Int32("id")), 16).ToUpper();
									if (strTemp.Length < 7)
									{
										strTemp = Strings.StrDup(7 - strTemp.Length, "0") + strTemp;
									}
									strTemp2 = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", strTemp);
									strTemp = FCFileSystem.Dir(strTemp2 + "*.skt", 0);
									while (!(strTemp == string.Empty))
									{
										strTemp = FCFileSystem.Dir();
										lngTemp += 1;
									}
									clsTemp.MoveNext();
								}
								HandleSpecialCodes = Strings.Format(lngTemp, "0");
							}
							break;
						}
					case 74:
						{
							// date created
							if (boolFromSales)
							{
								HandleSpecialCodes = "";
							}
							else
							{
								if (!clsMast.EndOfFile())
								{
									if (!Information.IsDate(clsMast.Get_Fields("datecreated")))
									{
										HandleSpecialCodes = "";
									}
									else
									{
										if (clsMast.Get_Fields_DateTime("DateCreated").ToOADate() == 0)
										{
											HandleSpecialCodes = "";
										}
										else
										{
											HandleSpecialCodes = Strings.Format(clsMast.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
										}
									}
								}
							}
							break;
						}
					case 201:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(diunitsdwelling) as thesum from srdwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(diunitsdwelling) as thesum from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 203:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(diunitsother) as thesum from srdwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(diunitsother) as thesum from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 208:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(disfmasonry) as thesum from srdwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(disfmasonry) as thesum from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 218:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(disfbsmtliving) as thesum from srdwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(disfbsmtliving) as thesum from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 220:
						{
							if (!clsDwel.EndOfFile())
							{
								HandleSpecialCodes = clsDwel.Get_Fields_Int32("dibsmtfingrade1") + " " + clsDwel.Get_Fields_Int32("dibsmtfingrade2");
							}
							break;
						}
					case 238:
						{
							if (!clsDwel.EndOfFile())
							{
								HandleSpecialCodes = clsDwel.Get_Fields_Int32("digrade1") + clsDwel.Get_Fields_Int32("digrade2");
							}
							break;
						}
					case 239:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(disqft) as thesum from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(disqft) as thesum from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 301:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("occ1") + " " + clsComm.Get_Fields_Int32("occ2"));
							}
							break;
						}
					case 302:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(dwel1 + dwel2) from srcommercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(dwel1 + dwel2) from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 303:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(dwel1 + dwel2) from srcommercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(dwel1 + dwel2) from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 304:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1class") + " " + clsComm.Get_Fields_Int32("c2class"));
							}
							break;
						}
					case 305:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(FCConvert.ToString(clsComm.Get_Fields_Int32("c1quality")) + FCConvert.ToString(clsComm.Get_Fields_Int32("c2quality")));
							}
							break;
						}
					case 306:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1grade") + " " + clsComm.Get_Fields_Int32("c2grade"));
							}
							break;
						}
					case 307:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1extwalls") + " " + clsComm.Get_Fields_Int32("c2extwalls"));
							}
							break;
						}
					case 308:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1stories") + " " + clsComm.Get_Fields_Int32("c1stories"));
							}
							break;
						}
					case 309:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1height") + " " + clsComm.Get_Fields_Int32("c2height"));
							}
							break;
						}
					case 310:
						{
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(c1floor + c2floor) as thesum from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(c1floor + c2floor) as thesum from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 311:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1floor") + " " + clsComm.Get_Fields_Int32("c2floor"));
							}
							break;
						}
					case 312:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1perimeter") + " " + clsComm.Get_Fields_Int32("c2perimeter"));
							}
							break;
						}
					case 313:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1heat") + " " + clsComm.Get_Fields_Int32("c2heat"));
							}
							break;
						}
					case 314:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1built") + " " + clsComm.Get_Fields_Int32("c2built"));
							}
							break;
						}
					case 315:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1remodel") + " " + clsComm.Get_Fields_Int32("c2remodel"));
							}
							break;
						}
					case 316:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1condition") + " " + clsComm.Get_Fields_Int32("c2condition"));
							}
							break;
						}
					case 317:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1phys") + " " + clsComm.Get_Fields_Int32("c2phys"));
							}
							break;
						}
					case 318:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(clsComm.Get_Fields_Int32("c1funct") + " " + clsComm.Get_Fields_Int32("c2funct"));
							}
							break;
						}
					case 319:
						{
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = clsComm.Get_Fields_Int32("cmecon");
							}
							break;
						}
					case 320:
						{
							// sqft by account
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(val(c1floor & '') * val(c1stories & '') + (val(c2floor & '') * val(c2stories & ''))) as thesum from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(val(c1floor & '') * val(c1stories & '') + (val(c2floor & '') * val(c2stories & ''))) as thesum from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 321:
						{
							// sqft by card
							if (!clsComm.EndOfFile())
							{
								HandleSpecialCodes = Strings.Trim(FCConvert.ToString(Conversion.Val(clsComm.Get_Fields_Int32("c1floor")) * Conversion.Val(clsComm.Get_Fields_Int32("c1stories"))) + " " + FCConvert.ToString(Conversion.Val(clsComm.Get_Fields_Int32("c2floor")) * Conversion.Val(clsComm.Get_Fields_Int32("c2stories"))));
							}
							break;
						}
					case 322:
						{
							// total sqft by account
							if (boolFromSales)
							{
								clsTemp.OpenRecordset("select sum(val(sqft & '')) as thesum from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								clsTemp.OpenRecordset("select sum(val(sqft & '')) as the sum from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
							}
							if (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								HandleSpecialCodes = Conversion.Val(clsTemp.Get_Fields("thesum"));
							}
							break;
						}
					case 401:
						{
							break;
						}
				}
				//end switch
				return HandleSpecialCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in Handle Special Codes", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleSpecialCodes;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private object HandleRegularCodes(int lngCode, ref clsReportParameter rp)
		{
			object HandleRegularCodes = null;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strFld;
			string strTBL;
            try
            {
                // On Error GoTo ErrorHandler
                // strFld = aryCodes(lngCode).FieldName
                strFld = rp.FieldName;
                // strTBL = aryCodes(lngCode).TableName
                strTBL = rp.TableName;
                if (boolFromSales)
                {
                    if (Strings.UCase(strTBL) == "MASTER")
                    {
                        strTBL = "srmaster";
                    }
                    else if (Strings.UCase(strTBL) == "DWELLING")
                    {
                        strTBL = "srdwelling";
                    }
                    else if (Strings.UCase(strTBL) == "COMMERCIAL")
                    {
                        strTBL = "srcommercial";
                    }
                    else if (Strings.UCase(strTBL) == "OUTBUILDING")
                    {
                        strTBL = "sroutbuilding";
                    }
                }

                if (boolFromSales)
                {
                    if (Strings.UCase(strTBL) == "SRMASTER")
                    {
                        clsTemp.OpenRecordset(
                            "select " + strFld + " from " + strTBL + " where rsaccount = " +
                            clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " +
                            clsCustom.Get_Fields_Int32("rscard") + " and saleid = " +
                            clsCustom.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        clsTemp.OpenRecordset(
                            "select " + strFld + " from " + strTBL + " where rsaccount = " +
                            clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " +
                            clsCustom.Get_Fields_Int32("rscard") + " and saledate = '" +
                            clsCustom.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                    }
                }
                else
                {
                    if (Strings.LCase(strTBL) == "master")
                    {
                        if ((Strings.LCase(strFld) == "rsname") || (Strings.LCase(strFld) == "rssecowner") ||
                            (Strings.LCase(strFld) == "rsaddr1") || (Strings.LCase(strFld) == "rsaddr2") ||
                            (Strings.LCase(strFld) == "rsaddr3") || (Strings.LCase(strFld) == "rsstate") ||
                            (Strings.LCase(strFld) == "rszip") || (Strings.LCase(strFld) == "rszip4") ||
                            (Strings.LCase(strFld) == "email"))
                        {
                            string strREFullDBName = "";
                            string strMasterJoin = "";
                            string strMasterJoinJoin = "";
                            // strREFullDBName = rsTemp.GetFullDBName("RealEstate")
                            strMasterJoin = modREMain.GetMasterJoin();
                            string strMasterJoinQuery = "";
                            strMasterJoinQuery = "(" + strMasterJoin + ") mj";
                            clsTemp.OpenRecordset(
                                "select " + strFld + " from " + strMasterJoinQuery + " where rsaccount = " +
                                clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " +
                                clsCustom.Get_Fields_Int32("rscard") + " ", modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            clsTemp.OpenRecordset(
                                "select " + strFld + " from " + strTBL + " where rsaccount = " +
                                clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " +
                                clsCustom.Get_Fields_Int32("rscard") + " ", modGlobalVariables.strREDatabase);
                        }
                    }
                    else
                    {
                        clsTemp.OpenRecordset(
                            "select " + strFld + " from " + strTBL + " where rsaccount = " +
                            clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " +
                            clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                    }
                }

                if (clsTemp.EndOfFile())
                {
                    HandleRegularCodes = "";
                }
                else
                {
                    if (lngCode != 69)
                    {
                        HandleRegularCodes = clsTemp.Get_Fields(strFld);
                    }
                    else
                    {
                        if (FCConvert.ToBoolean(clsTemp.Get_Fields(strFld)))
                        {
                            HandleRegularCodes = "True";
                        }
                        else
                        {
                            HandleRegularCodes = "False";
                        }
                    }
                }

                return HandleRegularCodes;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In handle regular codes", null, MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsTemp.Dispose();
            }
			return HandleRegularCodes;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void FillDetail()
		{
			int i;
			int j;
			// vbPorter upgrade warning: varResult As object	OnWrite(object, string)	OnRead(string)
			object varResult = null;
			string strResult = "";
			int x;
			int y;
			try
			{
				// On Error GoTo ErrorHandler
				if (boolFromSales)
				{
					if (Conversion.Val(clsCustom.Get_Fields_Int32("saleid")) > 0)
					{
						clsMast.OpenRecordset("select * from srmaster where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard") + " and saleid = " + clsCustom.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					else
					{
						clsMast.OpenRecordset("select * from srmaster where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
					}
					clsDwel.OpenRecordset("select * from srdwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
					clsComm.OpenRecordset("select * from srcommercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
					clsOut.OpenRecordset("select * from sroutbuilding where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard") + " and saledate = #" + clsCustom.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
				}
				else
				{
					string strREFullDBName = "";
					string strMasterJoin = "";
					string strMasterJoinJoin = "";
					// strREFullDBName = rsTemp.GetFullDBName("RealEstate")
					strMasterJoin = modREMain.GetMasterJoin();
					string strMasterJoinQuery = "";
					strMasterJoinQuery = "(" + strMasterJoin + ") mj";
					clsMast.OpenRecordset(strMasterJoin + " where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
					clsDwel.OpenRecordset("select * from dwelling where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
					clsComm.OpenRecordset("select * from commercial where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
					clsOut.OpenRecordset("select * from outbuilding where rsaccount = " + clsCustom.Get_Fields_Int32("rsaccount") + " and rscard = " + clsCustom.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
				}
				for (i = 0; i <= numRows - 1; i++)
				{
					for (j = 0; j <= numCols - 1; j++)
					{
						if (Conversion.Val(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) != 0)
						{
							varResult = getValueForCode(FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag));
							if (Information.IsNumeric(varResult))
							{
								if (FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 2 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 32 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 34 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 41 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 42 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 43 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 62 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 63 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 65 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 68 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 212 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 213 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 314 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 315 || FCConvert.ToInt32(Fields["DetRow" + FCConvert.ToString(i) + "Col" + FCConvert.ToString(j)].Tag) == 402)
								{
									break;
								}
								else
								{
									strResult = FCConvert.ToString(varResult);
									if (strResult != string.Empty)
									{
										if (Strings.InStr(1, strResult, ".", CompareConstants.vbTextCompare) > 0)
										{
											varResult = Strings.Format(varResult, "###,###,###,##0.00");
										}
										else
										{
											varResult = Strings.Format(varResult, "###,###,###,###");
										}
									}
									else
									{
										varResult = Strings.Format(varResult, "###,###,###,###");
									}
								}
							}
							// Fields("DetRow" & i & "Col" & j).Value = Format(getValueForCode(Fields("DetRow" & i & "Col" & j).Tag), "")
							Fields["DetRow" + i + "Col" + j].Value = varResult;
							for (x = 1; x <= Information.UBound(aryTots, 1); x++)
							{
								if (Strings.UCase(aryTots[x]) == Strings.UCase("DETROW" + FCConvert.ToString(i) + "COL" + FCConvert.ToString(j)))
								{
									(ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text) + Conversion.Val(strResult));
									break;
								}
							}
							// x
							for (x = 1; x <= Information.UBound(aryCounts, 1); x++)
							{
								if (Strings.UCase(aryCounts[x]) == Strings.UCase("DETROW" + FCConvert.ToString(i) + "COL" + FCConvert.ToString(j)))
								{
									(ReportFooter.Controls["TotalFor" + aryCounts[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((ReportFooter.Controls["TotalFor" + aryCounts[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text) + 1);
									break;
								}
							}
						}
						// x
						else
						{
						}
					}
				}
				//FC:FINAL:MSH - i.issue #1633: move this part to here to avoid missing data(premature exit from the cycle)
				clsCustom.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in detail format", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			int x;
			bool boolMakeVisible;
			boolMakeVisible = false;
			for (x = 1; x <= Information.UBound(aryTots, 1); x++)
			{
				if (Strings.Trim(aryTots[x]) != string.Empty)
				{
					boolMakeVisible = true;
					if (Strings.InStr(1, (ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, ".", CompareConstants.vbTextCompare) > 0)
					{
						(ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0.00");
					}
					else
					{
						(ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls["TotalFor" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0");
					}
				}
			}
			// x
			for (x = 1; x <= Information.UBound(aryCounts, 1); x++)
			{
				if (Strings.Trim(aryCounts[x]) != string.Empty)
				{
					boolMakeVisible = true;
					(ReportFooter.Controls["TotalFor" + aryCounts[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls["TotalFor" + aryCounts[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0");
				}
			}
			// x
			if (boolMakeVisible)
				ReportFooter.Visible = true;
		}

		
	}
}
