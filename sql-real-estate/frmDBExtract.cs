﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Linq;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmDBExtract.
	/// </summary>
	public partial class frmDBExtract : BaseForm
	{
		public frmDBExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkInclude = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkInclude.AddControlArrayElement(chkInclude_18, 18);
			this.chkInclude.AddControlArrayElement(chkInclude_0, 0);
			this.chkInclude.AddControlArrayElement(chkInclude_1, 1);
			this.chkInclude.AddControlArrayElement(chkInclude_2, 2);
			this.chkInclude.AddControlArrayElement(chkInclude_3, 3);
			this.chkInclude.AddControlArrayElement(chkInclude_4, 4);
			this.chkInclude.AddControlArrayElement(chkInclude_5, 5);
			this.chkInclude.AddControlArrayElement(chkInclude_6, 6);
			this.chkInclude.AddControlArrayElement(chkInclude_9, 9);
			this.chkInclude.AddControlArrayElement(chkInclude_10, 10);
			this.chkInclude.AddControlArrayElement(chkInclude_11, 11);
			this.chkInclude.AddControlArrayElement(chkInclude_12, 12);
			this.chkInclude.AddControlArrayElement(chkInclude_13, 13);
			this.chkInclude.AddControlArrayElement(chkInclude_14, 14);
			this.chkInclude.AddControlArrayElement(chkInclude_15, 15);
			this.chkInclude.AddControlArrayElement(chkInclude_16, 16);
			this.chkInclude.AddControlArrayElement(chkInclude_8, 8);
			this.chkInclude.AddControlArrayElement(chkInclude_17, 17);
			this.chkInclude.AddControlArrayElement(chkInclude_7, 7);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDBExtract InstancePtr
		{
			get
			{
				return (frmDBExtract)Sys.GetInstance(typeof(frmDBExtract));
			}
		}

		protected frmDBExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string txtRec = "";
		int[] CodeArray = null;
		int intFile;
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		clsDRWrapper rsCom = new clsDRWrapper();
		int intLenOfArray;
		bool boolAbortDbExtract;
		bool boolFromSales;
		string strprefix = "";

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			int x;
			int y;
			bool boolAddIt = false;
			if (!lstCodes.SelectedItems.Any())
			{
				MessageBox.Show("No Code selected to add to Extract", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (y = 0; y <= lstCodes.Items.Count - 1; y++)
			{
				// check to make sure that its not already in the list
				boolAddIt = false;
				//FC:FINAL:MSH - i.issue #1138: item can be selected and not checked at the same time
				//if (lstCodes.Selected(y))
				if (lstCodes.Items[y].Selected)
				{
					boolAddIt = true;
					for (x = 0; x <= lstSavedCodes.Items.Count - 1; x++)
					{
						//FC:FINAL:MSH - i.issue #1138: need to use 'Text' property for getting text value
						//if (Conversion.Val(lstSavedCodes.Items[x].ToString()) == Conversion.Val(lstCodes.Items[y].ToString())) boolAddIt = false;
						if (Conversion.Val(lstSavedCodes.Items[x].Text) == Conversion.Val(lstCodes.Items[y].Text))
						{
							boolAddIt = false;
						}
					}
					// x
					//FC:FINAL:MSH - i.issue #1138: need to use 'Text' property for getting text value
					//if (boolAddIt) lstSavedCodes.AddItem(Strings.Mid(lstCodes.Items[y].ToString(), 1, 4));
					if (boolAddIt)
					{
						lstSavedCodes.AddItem(Strings.Mid(lstCodes.Items[y].Text, 1, 4));
					}
				}
			}
			// y
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			// Dim intOldIndex As Integer
			int x;
			if (!lstSavedCodes.SelectedItems.Any())
			{
				MessageBox.Show("There is no code selected to remove from the extract", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// intOldIndex = lstSavedCodes.listindex
			// 
			// lstSavedCodes.RemoveItem (lstSavedCodes.listindex)
			// If lstSavedCodes.ListCount > 0 Then
			// If lstSavedCodes.ListCount <= intOldIndex Then
			// lstSavedCodes.listindex = lstSavedCodes.ListCount - 1
			// Else
			// lstSavedCodes.listindex = intOldIndex
			// End If
			// End If
			for (x = (lstSavedCodes.Items.Count - 1); x >= 0; x--)
			{
				//FC:FINAL:MSH - i.issue #1138: item can be selected and not checked at the same time
				//if (lstSavedCodes.Selected(x))
				if (lstSavedCodes.Items[x].Selected)
				{
					lstSavedCodes.Items.RemoveAt(x);
				}
			}
			// x
		}

		private void cmdDone_Click()
		{
			string strHeaders;
			string strTemp = "";
			int x;
			bool boolOK;
			if (chkBPOption.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CustomizedInfo.MostRecentBP = true;
			}
			else
			{
				modGlobalVariables.Statics.CustomizedInfo.MostRecentBP = false;
			}
			boolAbortDbExtract = false;
			Frmassessmentprogress.InstancePtr.Initialize("Not all records have been extracted to the file. " + "\r\n" + "Do you still wish to cancel?");
			Frmassessmentprogress.InstancePtr.Label2.Text = "Creating Extract. Please Wait.";
			Frmassessmentprogress.InstancePtr.Text = "Database Extract";
			Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = true;
			//FC:FINAL:RPU:#i1321 - Show the form Modeless
			Frmassessmentprogress.InstancePtr.Show(FormShowEnum.Modeless);
			FCFileSystem.FileClose();
			//FC:FINAL:MSH - i.issue #1138: different comparing between original app and web app
			//if (cmbtRecords.Text == "Regular")
			if (cmbtRecords.Text != "Regular")
			{
				boolFromSales = true;
				strprefix = "SR";
			}
			else
			{
				boolFromSales = false;
				strprefix = "";
			}
			strHeaders = "";
			if (lstSavedCodes.Items.Count > 0)
			{
				CodeArray = new int[lstSavedCodes.Items.Count + 1];
				for (x = 0; x <= (lstSavedCodes.Items.Count - 1); x++)
				{
					//FC:FINAL:MSH - i.issue #1138: need to use 'Text' property for getting text value
					//CodeArray[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(lstSavedCodes.Items[x].ToString()));
					CodeArray[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(lstSavedCodes.Items[x].Text)));
				}
				// x
				intLenOfArray = lstSavedCodes.Items.Count;
			}
			if (chkColumnHeaders.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (chkInclude[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp = GetDescFromCode_8(0, true);
					strHeaders += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[18].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp = GetDescFromCode_8(18, true);
					strHeaders += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				for (x = 1; x <= 17; x++)
				{
					if (chkInclude[FCConvert.ToInt16(x)].CheckState == Wisej.Web.CheckState.Checked)
					{
						switch (x)
						{
							case 2:
								{
									// location
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "Street Number" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Apartment" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Street Name" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									break;
								}
							case 6:
								{
									// mailing address
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "Address 1" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "Address 2" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "Address 3" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "City" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "State" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "Zip" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + "Zip4" + FCConvert.ToString(Convert.ToChar(34)) + ",";
									break;
								}
							default:
								{
									strTemp = GetDescFromCode_6(x, true);
									strHeaders += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
									break;
								}
						}
						//end switch
					}
				}
				// x
				if (intLenOfArray > 0)
				{
					for (x = 0; x <= (intLenOfArray - 1); x++)
					{
						strTemp = GetDescFromCode_21(CodeArray[x], true);
						strHeaders += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
					// x
				}
				if (strHeaders != string.Empty)
				{
					strHeaders = Strings.Mid(strHeaders, 1, strHeaders.Length - 1);
				}
			}
			boolOK = false;
			if (cmbtcreate.Text == "1 Record Per Card")
			{
				boolOK = ExtractToDataBase(ref strHeaders);
            }
			else
			{
				boolOK = AggregateExtractToDatabase(ref strHeaders);
			}
			if (boolOK)
			{
				MessageBox.Show("Extract Completed", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				FillTxtRec();
				//! Load frmShowResults;
				frmShowResults.InstancePtr.Init(ref txtRec);
				frmShowResults.InstancePtr.Show(App.MainForm);
                //FC:FINAL:AM:#1666 - download the file
                FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "tsdbase.asc"));
			}
		}

		private bool AggregateExtractToDatabase(ref string strHeaders)
		{
			bool AggregateExtractToDatabase = false;
			// Dim dbTemp As DAO.Database
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper RSMast = new clsDRWrapper();
			clsDRWrapper rsMisc = new clsDRWrapper();
			clsDRWrapper RSDWL = new clsDRWrapper();
			clsDRWrapper rsOut = new clsDRWrapper();
			clsDRWrapper rsCode = new clsDRWrapper();
			// Dim rsCom As clsDRWrapper
			// Dim REC As String
			int intRes;
			int NumRecords;
			int intCnt;
			string strOutput = "";
			string strReturn = "";
			StreamWriter ts;
			//FileSystemObject fso = new FileSystemObject();
			int lngTotLand = 0;
			int lngTotBldg = 0;
			int lngUID;
			string strTemp = "";
			int lngCurrRec;
			// This is by account.  The extracttodatabase does the same thing but makes one record per card
			string strMasterJoin;
			strMasterJoin = modREMain.GetMasterJoin();
			AggregateExtractToDatabase = false;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			intCnt = 0;
			if (cmbRange.Text != "Sale Date")
			{
				if (Strings.Trim(txtStart.Text) == string.Empty && Strings.Trim(txtEnd.Text) == string.Empty && (cmbRange.Text != "From Extract"))
					cmbRange.Text = "All Accounts";
			}
			else if (cmbRange.Text == "Sale Date")
			{
				if (Strings.Trim(t2kStart.Text) == string.Empty && Strings.Trim(t2kEnd.Text) == string.Empty)
					cmbRange.Text = "All Accounts";
			}
			if (!boolFromSales)
			{
				if (cmbRange.Text == "All Accounts")
				{
					// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 order by rsaccount,rscard", strREDatabase)
					rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Account Number")
				{
					// account
					// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and  rscard = 1 and rsaccount between " & Val(txtStart.Text) & " and " & Val(txtEnd.Text) & " order by rsaccount,rscard", strREDatabase)
					rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount > 0 and  rscard = 1 and rsaccount between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Name")
				{
					// name
					// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and rsname between '" & Trim(txtStart.Text) & "' and '" & Trim(txtEnd.Text) & "zzz' order by rsname ,rsaccount,rscard", strREDatabase)
					rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and rsname between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "zzz' order by rsname ,rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Map Lot")
				{
					// maplot
					// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and rsmaplot between '" & Trim(txtStart.Text) & "' and '" & Trim(txtEnd.Text) & "' order by rsmaplot,rsaccount,rscard", strREDatabase)
					rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and rsmaplot between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' order by rsmaplot,rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "From Extract")
				{
					// extract
					// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rsaccount in (select ACCOUNTNUMBER from extracttable where userid = " & lngUID & " ) order by rsaccount,rscard", strREDatabase)
					rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount > 0 and rsaccount in (select ACCOUNTNUMBER from extracttable where userid = " + FCConvert.ToString(lngUID) + " ) order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Sale Date")
				{
					// saledate
					if (Information.IsDate(t2kStart.Text))
					{
						if (Information.IsDate(t2kEnd.Text))
						{
							// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and  rscard = 1 and saledate between '" & t2kStart.Text & "' and '" & t2kEnd.Text & "' order by saledate,rsaccount,rscard", strREDatabase)
							rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount > 0 and  rscard = 1 and saledate between '" + t2kStart.Text + "' and '" + t2kEnd.Text + "' order by saledate,rsaccount,rscard", modGlobalVariables.strREDatabase);
						}
						else
						{
							MessageBox.Show("Invalid end date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return AggregateExtractToDatabase;
						}
					}
					else
					{
						MessageBox.Show("Invalid start date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return AggregateExtractToDatabase;
					}
				}
			}
			else
			{
				if (cmbRange.Text == "All Accounts")
				{
					rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 order by rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Account Number")
				{
					// account
					rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and  rscard = 1 and rsaccount between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " order by rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Name")
				{
					// name
					rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and rsname between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' order by rsname ,rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Map Lot")
				{
					// maplot
					rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and rsmaplot between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' order by rsmaplot,rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "From Extract")
				{
					// extract
					rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0  and rscard = 1 and saleid in (select saleid from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,saleid, rscard", modGlobalVariables.strREDatabase);
				}
				else if (cmbRange.Text == "Sale Date")
				{
					// saledate
					if (Information.IsDate(t2kStart.Text))
					{
						if (Information.IsDate(t2kEnd.Text))
						{
							rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and rscard = 1 and  saledate between #" + t2kStart.Text + "# and #" + t2kEnd.Text + "# order by saledate,rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
						}
						else
						{
							MessageBox.Show("Invalid end date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return AggregateExtractToDatabase;
						}
					}
					else
					{
						MessageBox.Show("Invalid start date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
			if (rsTemp.EndOfFile())
			{
				Frmassessmentprogress.InstancePtr.Unload();
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return AggregateExtractToDatabase;
			}
			rsTemp.MoveLast();
			NumRecords = rsTemp.RecordCount();
			rsTemp.MoveFirst();
			// intFile = FreeFile
			FCFileSystem.FileClose();
			// Open "tsdbase.ASC" For Output Access Write Lock Write As #intFile
			//FC:FINAL:RPU:#i1322 - save the file to UserData to avoid exeception
			//ts = File.CreateText("tsdbase.asc");
			ts = File.CreateText(Path.Combine(FCFileSystem.Statics.UserDataFolder, "tsdbase.asc"));
			if (strHeaders != string.Empty)
			{
				// Print #intFile, strHeaders
				ts.WriteLine(strHeaders);
			}
			lngCurrRec = 0;
			while (!rsTemp.EndOfFile())
			{
				lngCurrRec += 1;
				strOutput = "";
				//Application.DoEvents();
				boolAbortDbExtract = !Frmassessmentprogress.InstancePtr.stillcalc;
				if (boolAbortDbExtract)
				{
					Frmassessmentprogress.InstancePtr.Unload();
                    if(ts != null)
                    {
                        ts.Close();
                    }
					return AggregateExtractToDatabase;
				}
				// Set rsCom = dbtemp.OpenRecordset("select * from commercial where rsaccount = " & rstemp.fields("rsaccount") & " and rscard = " & rstemp.fields("rscard"),strredatabase)
				if (!boolFromSales)
				{
					modREMain.OpenCOMMERCIALTable(ref rsCom, rsTemp.Get_Fields_Int32("rsaccount"), rsTemp.Get_Fields_Int32("rscard"));
				}
				else
				{
					modREMain.opensrcommercialtable_54(ref rsCom, rsTemp.Get_Fields_Int32("rsaccount"), rsTemp.Get_Fields_Int32("rscard"), FCConvert.ToString(rsTemp.Get_Fields_DateTime("saledate")));
				}
				if (chkInclude[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					// account
					// Write #intFile, rsTemp.Get_Fields("rsaccount");
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rsaccount") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[18].CheckState == Wisej.Web.CheckState.Checked)
				{
					// card number
					// Write #intFile, rsTemp.Get_Fields("rscard");
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rscard") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					// name
					// Write #intFile, rsTemp.fields("rsname") & " ";
					strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("rsname"));
					strTemp = strTemp.Replace("  ", " ");
					strTemp = strTemp.Replace("  ", " ");
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rsTemp.Get_Fields_String("rsname")).Replace("  ", " ") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[2].CheckState == Wisej.Web.CheckState.Checked)
				{
					// location
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rslocnumalph") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rslocapt") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rslocstreet") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[3].CheckState == Wisej.Web.CheckState.Checked)
				{
					// map/lot
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsmaplot") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[4].CheckState == Wisej.Web.CheckState.Checked)
				{
					// ref 1
					// Write #intFile, rsTemp.Get_Fields("rsref1");
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsref1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[5].CheckState == Wisej.Web.CheckState.Checked)
				{
					// ref2
					// Write #intFile, rsTemp.Get_Fields("rsref2");
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsref2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[6].CheckState == Wisej.Web.CheckState.Checked)
				{
					// mailing address
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsaddr1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsaddr2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					if (!boolFromSales)
					{
						// TODO Get_Fields: Field [rsaddress3] not found!! (maybe it is an alias?)
						strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields("rsaddress3") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
					else
					{
						strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsaddr3") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsstate") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rszip") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rszip4") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[7].CheckState == Wisej.Web.CheckState.Checked)
				{
					// telephone
					strTemp = "";
					if (!boolFromSales)
					{
						rsMisc.OpenRecordset("select top 1 * from centralphonenumbers where partyid = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ownerpartyid"))) + " order by phoneorder", "CentralParties");
						if (!rsMisc.EndOfFile())
						{
							if (FCConvert.ToString(rsMisc.Get_Fields_String("phonenumber")) != "(000)000-0000")
							{
								strTemp = FCConvert.ToString(rsMisc.Get_Fields_String("Phonenumber"));
							}
						}
					}
					strTemp = Strings.Trim(strTemp);
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[8].CheckState == Wisej.Web.CheckState.Checked)
				{
					// acreage
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(piacres) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(piacres) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[9].CheckState == Wisej.Web.CheckState.Checked)
				{
					// current land
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("Select sum(rllandval) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(rllandval) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					lngTotLand = FCConvert.ToInt32(RSMast.Get_Fields("thesum"));
					// Write #intFile, lngTotLand;
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(lngTotLand) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[10].CheckState == Wisej.Web.CheckState.Checked)
				{
					// current bldg
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(rlbldgval) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(rlbldgval) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					lngTotBldg = FCConvert.ToInt32(RSMast.Get_Fields("thesum"));
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(lngTotBldg) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[11].CheckState == Wisej.Web.CheckState.Checked)
				{
					// current total
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(rlbldgval + rllandval) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(rlbldgval + rllandval) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[12].CheckState == Wisej.Web.CheckState.Checked)
				{
					// last years land
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(lastlandval) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(lastlandval) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[13].CheckState == Wisej.Web.CheckState.Checked)
				{
					// last years bldg
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(lastbldgval) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(lastbldgval) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[14].CheckState == Wisej.Web.CheckState.Checked)
				{
					// last years total
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(lastlandval + lastbldgval) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(lastlandval + lastbldgval) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[15].CheckState == Wisej.Web.CheckState.Checked)
				{
					// exemption
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("select sum(rlexemption) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(rlexemption) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[16].CheckState == Wisej.Web.CheckState.Checked)
				{
					// net assessment
					if (!boolFromSales)
					{
						RSMast.OpenRecordset("Select sum(lastlandval + lastbldgval - rlexemption) as TheSum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSMast.OpenRecordset("select sum(lastlandval + lastbldgval - rlexemption) as TheSum from srmaster where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saleid = " + rsTemp.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
					}
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSMast.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkInclude[17].CheckState == Wisej.Web.CheckState.Checked)
				{
					if (!boolFromSales)
					{
						RSDWL.OpenRecordset("select sum(disfla) as TheSum from dwelling where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					}
					else
					{
						RSDWL.OpenRecordset("select sum(disfla) as TheSum from srdwelling where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and saledate = '" + rsTemp.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
					}
                    // TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
                    //FC:FINAL:CHN: Incorrect comparing (add using value from original).
                    // if (RSDWL.Get_Fields("thesum") != IntPtr.Zero)
                    if (RSDWL.Get_Fields("thesum") != 1)
                    {
                        // TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
                        strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSDWL.Get_Fields("thesum") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
					else
					{
						// Write #intFile, str(0);
						strOutput += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
				}
				if (intLenOfArray > 0)
				{
					for (intCnt = 0; intCnt <= (intLenOfArray - 1); intCnt++)
					{
						rsCode.OpenRecordset("select * from reportcodes where code = " + FCConvert.ToString(CodeArray[intCnt]), modGlobalVariables.strREDatabase);
						if (rsCode.EndOfFile())
						{
							MessageBox.Show("Code " + FCConvert.ToString(CodeArray[intCnt]) + " is not found in the code table." + "\r\n" + "This code will not be used in the extract.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							// If rsCode.Fields("Code") = 317 Then
							// rsCode.Edit
							// rsCode.Fields("SpecialCase") = True
							// rsCode.Update
							// End If
							// If rsCode.Fields("FieldName") = "dibasement" Then
							// rsCode.Edit
							// rsCode.Fields("FieldName") = "dibsmt"
							// rsCode.Update
							// 
							// End If
							// If rsCode.Fields("FieldName") = "diwetbasement" Then
							// rsCode.Edit
							// rsCode.Fields("FieldName") = "diwetbsmt"
							// rsCode.Update
							// End If
							if (FCConvert.ToBoolean(rsCode.Get_Fields_Boolean("SpecialCase")))
							{
								if (!boolFromSales)
								{
									//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
									strReturn = HandleSpecialCase_72(rsTemp, CodeArray[intCnt], rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard")));
								}
								else
								{
									//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
									strReturn = HandleSpecialCase_234(rsTemp, CodeArray[intCnt], rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard")), rsTemp.Get_Fields_Int32("saleid"));
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								if (Conversion.Val(rsCode.Get_Fields("code")) == 68)
								{
									strOutput += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(rsTemp.Get_Fields_String("rszip") + " " + rsTemp.Get_Fields_String("rszip4")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
								}
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								else if (Conversion.Val(rsCode.Get_Fields("code")) == 75)
								{
									strReturn = FCConvert.ToString(rsTemp.Get_Fields_String("email"));
								}
								else
								{
									if (!boolFromSales)
									{
										//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
										strReturn = HandleRegular_234(CodeArray[intCnt], rsCode, rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard")), true);
									}
									else
									{
										//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
										strReturn = HandleRegular_720(CodeArray[intCnt], rsCode, rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard")), true, rsTemp.Get_Fields_Int32("saleid"));
									}
								}
							}
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + strReturn + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
					}
					// intCnt
				}
				rsTemp.MoveNext();
				//Application.DoEvents();
				if (rsTemp.EndOfFile() == false)
				{
					Frmassessmentprogress.InstancePtr.ProgressBar1.Value = FCConvert.ToInt32(FCConvert.ToDouble(lngCurrRec) / rsTemp.RecordCount());
					Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(FCConvert.ToDouble(lngCurrRec) / rsTemp.RecordCount(), "##.00") + "%";
				}
				else
				{
					Frmassessmentprogress.InstancePtr.ProgressBar1.Value = 100;
					Frmassessmentprogress.InstancePtr.lblpercentdone.Text = "100%";
				}
				// Write #intFile,
				if (Strings.Trim(strOutput) != string.Empty)
				{
					// strip off the last ,
					strOutput = Strings.Mid(strOutput, 1, strOutput.Length - 1);
				}
				ts.WriteLine(strOutput);
			}
			// Close #intFile
			ts.Close();
			Frmassessmentprogress.InstancePtr.Unload();
			AggregateExtractToDatabase = true;
			rsTemp.DisposeOf();
			RSMast.DisposeOf();
			rsMisc.DisposeOf();
			RSDWL.DisposeOf();
			rsOut.DisposeOf();
			rsCode.DisposeOf();
			return AggregateExtractToDatabase;
		}

		private bool ExtractToDataBase(ref string strHeaders)
		{
			bool ExtractToDataBase = false;
			// Dim dbTemp As DAO.Database
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper RSMast = new clsDRWrapper();
			clsDRWrapper RSDWL = new clsDRWrapper();
			clsDRWrapper rsOut = new clsDRWrapper();
			clsDRWrapper rsMisc = new clsDRWrapper();
			clsDRWrapper rsCode = new clsDRWrapper();
			string strTemp = "";
            // Dim rsCom As clsDRWrapper
            // Dim REC As String
            // Dim lngLocation As String
            //FileSystemObject fso = new FileSystemObject();
            StreamWriter ts = null;
			int intRes;
			string strOutput = "";
			int intCnt;
			string strMasterQuery;
			strMasterQuery = modREMain.GetMasterJoin();
			int lngUID;
			int lngCurrRec = 0;
			bool boolComLoaded = false;
			cParty tParty = new cParty();
			cPartyController tPartyCont = new cPartyController();
			// This is by card.  The aggregateextracttodatabase does the same thing but makes one record per account
			try
			{
				// On Error GoTo ErrorHandler
				ExtractToDataBase = false;
				lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				intCnt = 0;
				rsCode.OpenRecordset("select * from reportcodes order by code", modGlobalVariables.strREDatabase);
				if (cmbRange.Text != "From Extract" && cmbRange.Text != "Sale Date")
				{
					if (Strings.Trim(txtStart.Text) == string.Empty && Strings.Trim(txtEnd.Text) == string.Empty)
						cmbRange.Text = "All Accounts";
				}
				else if (cmbRange.Text == "Sale Date")
				{
					if (Strings.Trim(t2kStart.Text) == string.Empty && Strings.Trim(t2kEnd.Text) == string.Empty)
						cmbRange.Text = "All Accounts";
				}
				if (!boolFromSales)
				{
					if (cmbRange.Text == "All Accounts")
					{
						/*? 11 */// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 order by rsaccount,rscard", strREDatabase)
						rsTemp.OpenRecordset(strMasterQuery + " where not rsdeleted = 1 and rsaccount > 0 order by rsaccount,rscard", modGlobalVariables.strREDatabase);
						/*? 12 */
					}
					else if (cmbRange.Text == "Account Number")
					{
						// account
						rsTemp.OpenRecordset(strMasterQuery + " where not rsdeleted = 1 and rsaccount > 0 and rsaccount between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " order by rsaccount,rscard", modGlobalVariables.strREDatabase);
						// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rsaccount between " & Val(txtStart.Text) & " and " & Val(txtEnd.Text) & " order by rsaccount,rscard", strREDatabase)
						/*? 14 */
					}
					else if (cmbRange.Text == "Name")
					{
						// name
						rsTemp.OpenRecordset(strMasterQuery + " where not rsdeleted = 1 and rsaccount > 0 and rsname between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "zzz' order by rsname ,rsaccount,rscard", modGlobalVariables.strREDatabase);
						// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rsname between '" & Trim(txtStart.Text) & "' and '" & Trim(txtEnd.Text) & "zzz' order by rsname ,rsaccount,rscard", strREDatabase)
						/*? 16 */
					}
					else if (cmbRange.Text == "Map Lot")
					{
						// maplot
						// 17            Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rsmaplot between '" & Trim(txtStart.Text) & "' and '" & Trim(txtEnd.Text) & "' order by rsmaplot,rsaccount,rscard", strREDatabase)
						rsTemp.OpenRecordset(strMasterQuery + " where not rsdeleted = 1 and rsaccount > 0 and rsmaplot between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' order by rsmaplot,rsaccount,rscard", modGlobalVariables.strREDatabase);
						/*? 18 */
					}
					else if (cmbRange.Text == "From Extract")
					{
						// extract
						// 19            Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rsaccount in (select ACCOUNTNUMBER from extracttable where userid = " & lngUID & ") order by rsaccount,rscard", strREDatabase)
						rsTemp.OpenRecordset(strMasterQuery + " where not rsdeleted = 1 and rsaccount > 0 and rsaccount in (select ACCOUNTNUMBER from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
					else if (cmbRange.Text == "Sale Date")
					{
						// saledate
						if (Information.IsDate(t2kStart.Text))
						{
							if (Information.IsDate(t2kEnd.Text))
							{
								// Call rsTemp.OpenRecordset("select * from master where not rsdeleted = 1 and rsaccount > 0 and rsaccount in (select rsaccount from master where rscard = 1 and saledate between '" & t2kStart.Text & "' and '" & t2kEnd.Text & "') order by saledate,rsaccount,rscard", strREDatabase)
								rsTemp.OpenRecordset(strMasterQuery + " where not rsdeleted = 1 and rsaccount > 0 and rsaccount in (select rsaccount from master where rscard = 1 and saledate between '" + t2kStart.Text + "' and '" + t2kEnd.Text + "') order by saledate,rsaccount,rscard", modGlobalVariables.strREDatabase);
							}
							else
							{
								MessageBox.Show("Invalid end date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return ExtractToDataBase;
							}
						}
						else
						{
							MessageBox.Show("Invalid start date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return ExtractToDataBase;
						}
					}
				}
				else
				{
					if (cmbRange.Text == "All Accounts")
					{
						rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 order by rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
						/*? 22 */
					}
					else if (cmbRange.Text == "Account Number")
					{
						// account
						rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and rsaccount between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " order by rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
						/*? 24 */
					}
					else if (cmbRange.Text == "Name")
					{
						// name
						rsTemp.OpenRecordset("select * from SRmaster where not rsdeleted = 1 and rsaccount > 0 and rsname between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "zzz' order by rsname ,rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
						/*? 26 */
					}
					else if (cmbRange.Text == "Map Lot")
					{
						// maplot
						rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and rsmaplot between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' order by rsmaplot,rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
						/*? 28 */
					}
					else if (cmbRange.Text == "From Extract")
					{
						// extract
						rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and SALEID in (select saleid from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
						/*? 30 */
					}
					else if (cmbRange.Text == "Sale Date")
					{
						// saledate
						if (Information.IsDate(t2kStart.Text))
						{
							if (Information.IsDate(t2kEnd.Text))
							{
								rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount > 0 and saledate between '" + t2kStart.Text + "' and '" + t2kEnd.Text + "' order by saledate,rsaccount,saleid,rscard", modGlobalVariables.strREDatabase);
							}
							else
							{
								MessageBox.Show("Invalid end date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return ExtractToDataBase;
							}
						}
						else
						{
							MessageBox.Show("Invalid start date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return ExtractToDataBase;
						}
					}
				}
				if (!rsTemp.EndOfFile())
				{
					// 37        intFile = FreeFile
					// 38        Open "tsdbase.ASC" For Output Access Write Lock Write As #intFile
					//FC:FINAL:RPU:#i1322 - Use FCFileSystem and save the file to UserData to avoid exeception
					//if (File.Exists("tsdbase.asc"))
					//{
					//    File.Delete("tsdbase.asc");
					//}
					//ts = File.CreateText("tsdbase.asc");
					if (FCFileSystem.FileExists("tsdbase.asc"))
					{
						FCFileSystem.DeleteFile("tsdbase.asc");
					}
					ts = File.CreateText(Path.Combine(FCFileSystem.Statics.UserDataFolder, "tsdbase.asc"));
					// REC = ""
					if (strHeaders != string.Empty)
					{
						// Print #intFile, strHeaders
						// Write #intFile, strHeaders
						ts.WriteLine(strHeaders);
					}
					lngCurrRec = 0;
					while (!rsTemp.EndOfFile())
					{
						lngCurrRec += 1;
						strOutput = "";
						// Set RSOUT = DBTemp.OpenRecordset("select * from outbuilding where rsaccount = " & rstemp.fields("rsaccount") & " and rscard = " & rstemp.fields("rscard"),strredatabase)
						//Application.DoEvents();
						boolAbortDbExtract = !Frmassessmentprogress.InstancePtr.stillcalc;
						if (boolAbortDbExtract)
						{
							Frmassessmentprogress.InstancePtr.Unload();
                            //FC:FINAL:AM:#1665 - Update the form
                            FCUtils.ApplicationUpdate(this);
							return ExtractToDataBase;
						}
						// 
						// 44            If Not boolFromSales Then
						// 45                Call rsCom.OpenRecordset("select * from commercial where rsaccount = " & rsTemp.Fields("rsaccount") & " and rscard = " & rsTemp.Fields("rscard"), strREDatabase)
						// Else
						// 46                Call rsCom.OpenRecordset("select * from srcommercial where rsaccount = " & rsTemp.Fields("rsaccount") & " and rscard = " & rsTemp.Fields("rscard") & " and saledate = '" & rsTemp.Fields("saledate") & "'", strREDatabase)
						// End If
						if (chkInclude[0].CheckState == Wisej.Web.CheckState.Checked)
						{
							// account
							// 48               Write #intFile, rsTemp.Get_Fields("rsaccount");
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rsaccount") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[18].CheckState == Wisej.Web.CheckState.Checked)
						{
							// card number
							// 50                Write #intFile, rsTemp.Get_Fields("rscard");
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rscard") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[1].CheckState == Wisej.Web.CheckState.Checked)
						{
							// name
							// 52                Write #intFile, rsTemp.fields("rsname") & " ";
							strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("rsname"));
							strTemp = strTemp.Replace("  ", " ");
							strTemp = strTemp.Replace("  ", " ");
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[2].CheckState == Wisej.Web.CheckState.Checked)
						{
							// location
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rslocnumalph") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rslocapt") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rslocstreet") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[3].CheckState == Wisej.Web.CheckState.Checked)
						{
							// map/lot
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsmaplot") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[4].CheckState == Wisej.Web.CheckState.Checked)
						{
							// ref 1
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsref1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[5].CheckState == Wisej.Web.CheckState.Checked)
						{
							// ref2
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsref2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[6].CheckState == Wisej.Web.CheckState.Checked)
						{
							// mailing address
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsaddr1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsaddr2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							if (!boolFromSales)
							{
								// TODO Get_Fields: Field [rsaddress3] not found!! (maybe it is an alias?)
								strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields("rsaddress3") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							}
							else
							{
								strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
							}
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsaddr3") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rsstate") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rszip") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("rszip4") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[7].CheckState == Wisej.Web.CheckState.Checked)
						{
							// telephone
							strTemp = "";
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select top 1 * from centralphonenumbers where partyid = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ownerpartyid"))) + " order by phoneorder", "CentralParties");
								if (!rsMisc.EndOfFile())
								{
									if (FCConvert.ToString(rsMisc.Get_Fields_String("phonenumber")) != "(000)000-0000")
									{
										strTemp = FCConvert.ToString(rsMisc.Get_Fields_String("Phonenumber"));
									}
								}
							}
							strTemp = Strings.Trim(strTemp);
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[8].CheckState == Wisej.Web.CheckState.Checked)
						{
							// acreage
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Double("piacres"))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[9].CheckState == Wisej.Web.CheckState.Checked)
						{
							// current land
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rllandval") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[10].CheckState == Wisej.Web.CheckState.Checked)
						{
							// current bldg
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rlbldgval") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[11].CheckState == Wisej.Web.CheckState.Checked)
						{
							// current total
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rsTemp.Get_Fields_Int32("rllandval") + rsTemp.Get_Fields_Int32("rlbldgval")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[12].CheckState == Wisej.Web.CheckState.Checked)
						{
							// last years land
							// 81                Write #intFile, rsTemp.Get_Fields("lastlandval");
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("lastlandval") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[13].CheckState == Wisej.Web.CheckState.Checked)
						{
							// last years bldg
							// 83                Write #intFile, rsTemp.Get_Fields("lastbldgval");
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("lastbldgval") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[14].CheckState == Wisej.Web.CheckState.Checked)
						{
							// last years total
							// 85                Write #intFile, (rsTemp.Get_Fields("lastlandval") + rsTemp.fields("lastbldgval"));
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rsTemp.Get_Fields_Int32("lastlandval") + rsTemp.Get_Fields_Int32("lastbldgval")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[15].CheckState == Wisej.Web.CheckState.Checked)
						{
							// exemption
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_Int32("rlexemption") + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[16].CheckState == Wisej.Web.CheckState.Checked)
						{
							// net assessment
							strOutput += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rsTemp.Get_Fields_Int32("lastlandval") + rsTemp.Get_Fields_Int32("lastbldgval") - rsTemp.Get_Fields_Int32("rlexemption")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						if (chkInclude[17].CheckState == Wisej.Web.CheckState.Checked)
						{
							if (FCConvert.ToString(rsTemp.Get_Fields_String("rsdwellingcode")) == "Y")
							{
								if (!boolFromSales)
								{
									RSDWL.OpenRecordset("select disfla from dwelling where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and rscard = " + rsTemp.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
								}
								else
								{
									RSDWL.OpenRecordset("select disfla from srdwelling where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and rscard = " + rsTemp.Get_Fields_Int32("rscard") + " and saledate = '" + rsTemp.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!RSDWL.EndOfFile())
								{
									strOutput += FCConvert.ToString(Convert.ToChar(34)) + RSDWL.Get_Fields_Int32("disfla") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								}
								else
								{
									// 97                        Write #intFile, str(0);
									strOutput += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + ",";
								}
							}
							else
							{
								// 98                    Write #intFile, str(0);
								strOutput += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + ",";
							}
						}
						boolComLoaded = false;
						if (intLenOfArray > 0)
						{
							for (intCnt = 0; intCnt <= (intLenOfArray - 1); intCnt++)
							{
								// 101                    Call rsCode.OpenRecordset("select * from reportcodes where code = " & CodeArray(intCnt), strREDatabase)
								// 102                    If rsCode.EndOfFile Then
								if (!rsCode.FindFirst("code = " + FCConvert.ToString(CodeArray[intCnt])))
								{
									MessageBox.Show("Code " + FCConvert.ToString(CodeArray[intCnt]) + " is not found in the code table." + "\r\n" + "This code will not be used in the extract.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
									if (Conversion.Val(rsCode.Get_Fields("code")) >= 300 || Conversion.Val(rsCode.Get_Fields("code")) <= 399)
									{
										if (!boolComLoaded)
										{
											if (!boolFromSales)
											{
												rsCom.OpenRecordset("select * from commercial where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and rscard = " + rsTemp.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
											}
											else
											{
												rsCom.OpenRecordset("select * from srcommercial where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and rscard = " + rsTemp.Get_Fields_Int32("rscard") + " and saledate = '" + rsTemp.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
											}
											boolComLoaded = true;
										}
									}
									// 104                        If rsCode.Fields("Code") = 317 Then
									// 105                            rsCode.Edit
									// 106                            rsCode.Fields("SpecialCase") = True
									// 107                            rsCode.Update
									// End If
									// 108                            If rsCode.Fields("FieldName") = "dibasement" Then
									// 109                                rsCode.Edit
									// 110                                rsCode.Fields("FieldName") = "dibsmt"
									// 111                                rsCode.Update
									// 
									// End If
									// 112                            If rsCode.Fields("FieldName") = "diwetbasement" Then
									// 113                                rsCode.Edit
									// 114                                rsCode.Fields("FieldName") = "diwetbsmt"
									// 115                                rsCode.Update
									// End If
									if (FCConvert.ToBoolean(rsCode.Get_Fields_Boolean("SpecialCase")))
									{
										if (!boolFromSales)
										{
											//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
											strOutput += FCConvert.ToString(Convert.ToChar(34)) + HandleSpecialCase_72(rsTemp, CodeArray[intCnt], rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard"))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
										}
										else
										{
											//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
											strOutput += FCConvert.ToString(Convert.ToChar(34)) + HandleSpecialCase_234(rsTemp, CodeArray[intCnt], rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard")), rsTemp.Get_Fields_Int32("saleid")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
										if (Conversion.Val(rsCode.Get_Fields("code")) == 68)
										{
											strOutput += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(rsTemp.Get_Fields_String("rszip") + " " + rsTemp.Get_Fields_String("rszip4")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
										}
										// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
										else if (Conversion.Val(rsCode.Get_Fields("code")) == 75)
										{
											strOutput += FCConvert.ToString(Convert.ToChar(34)) + rsTemp.Get_Fields_String("email") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										}
										else
										{
											if (!boolFromSales)
											{
												//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
												strOutput += FCConvert.ToString(Convert.ToChar(34)) + HandleRegular_72(CodeArray[intCnt], rsCode, rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard"))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
											}
											else
											{
												//FC:FINAL:MSH - i.issue #1138: unboxing with 'short' for correct method call
												strOutput += FCConvert.ToString(Convert.ToChar(34)) + HandleRegular_639(CodeArray[intCnt], rsCode, rsTemp.Get_Fields_Int32("rsaccount"), FCConvert.ToInt16(rsTemp.Get_Fields_Int32("rscard")), rsTemp.Get_Fields_Int32("saleid")) + FCConvert.ToString(Convert.ToChar(34)) + ",";
											}
										}
									}
								}
							}
							// intCnt
						}
						rsTemp.MoveNext();
						//Application.DoEvents();
						if (rsTemp.EndOfFile() == false)
						{
							Frmassessmentprogress.InstancePtr.ProgressBar1.Value = FCConvert.ToInt32(FCConvert.ToDouble(lngCurrRec) / rsTemp.RecordCount());
							Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(FCConvert.ToDouble(lngCurrRec) / rsTemp.RecordCount(), "##.00") + "%";
						}
						else
						{
							Frmassessmentprogress.InstancePtr.ProgressBar1.Value = 100;
							Frmassessmentprogress.InstancePtr.lblpercentdone.Text = "100%";
						}
						//FC:FINAL:RPU:#i1321 - Update the form to report progress
						FCUtils.ApplicationUpdate(Frmassessmentprogress.InstancePtr);
						// 130            Write #intFile,
						if (Strings.Trim(strOutput) != string.Empty)
						{
							// strip off the last ,
							strOutput = Strings.Mid(strOutput, 1, strOutput.Length - 1);
						}
						ts.WriteLine(strOutput);
					}
					// 131        Close #intFile
					//ts.Close();
				}
				else
				{
					MessageBox.Show("No matches found.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					Frmassessmentprogress.InstancePtr.Unload();
					rsTemp.DisposeOf();
					RSMast.DisposeOf();
					RSDWL.DisposeOf();
					rsOut.DisposeOf();
					rsMisc.DisposeOf();
					rsCode.DisposeOf();
					return ExtractToDataBase;
				}
				Frmassessmentprogress.InstancePtr.Unload();
				// tsFile.Close
				ExtractToDataBase = true;
				rsTemp.DisposeOf();
				RSMast.DisposeOf();
				RSDWL.DisposeOf();
				rsOut.DisposeOf();
				rsMisc.DisposeOf();
				rsCode.DisposeOf();
				return ExtractToDataBase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				rsTemp.DisposeOf();
				RSMast.DisposeOf();
				RSDWL.DisposeOf();
				rsOut.DisposeOf();
				rsMisc.DisposeOf();
				rsCode.DisposeOf();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ExtractToDatabase line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
            finally
            {
                if(ts != null)
                {
                    ts.Close();
                }
            }
			return ExtractToDataBase;
		}

		private void cmdExit_Click()
		{
			this.Unload();
		}

		private void frmDBExtract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDBExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDBExtract properties;
			//frmDBExtract.ScaleWidth	= 9300;
			//frmDBExtract.ScaleHeight	= 7770;
			//frmDBExtract.LinkTopic	= "Form1";
			//frmDBExtract.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper rsTemp = new clsDRWrapper();
			// Dim dbTemp As DAO.Database
			clsDRWrapper rsCost = new clsDRWrapper();
			string strItem = "";
			FCUtils.EraseSafe(CodeArray);
			intLenOfArray = 0;
			if (modGlobalVariables.Statics.CustomizedInfo.MostRecentBP)
			{
				chkBPOption.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkBPOption.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modREMain.Statics.boolShortRealEstate)
			{
				chkInclude[9].Enabled = false;
				chkInclude[10].Enabled = false;
				chkInclude[11].Enabled = false;
				chkInclude[17].Enabled = false;
			}
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			lblTo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Application.DoEvents();
			if (!modREMain.Statics.boolShortRealEstate)
			{
				rsTemp.OpenRecordset("select * from reportcodes where (code < 25)or (CODE = 36) OR (code = 48) or (code = 49) or (code = 50) or (code between 52 and 63) or (code = 65) or (code = 67) or (code = 68) or (code between 72 and 76)  or ((code >= 200) and (code < 402)) order by code", modGlobalVariables.strREDatabase);
			}
			else
			{
				rsTemp.OpenRecordset("select * from reportcodes where (CODE > 9 AND code < 22)or (CODE = 36) OR (code = 48) or (code = 49) or (code = 50) or (code between 52 and 63) or (code = 65) or (code = 67) or (code = 68) or (code between 72 and  76)  order by code", modGlobalVariables.strREDatabase);
			}
			while (!rsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				strItem = Strings.Format(Conversion.Str(rsTemp.Get_Fields("Code")), "@@@") + "  ";
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				if (rsTemp.Get_Fields("Code") == 3)
				{
					rsCost.OpenRecordset("select * from costrecord where crecordnumber = 1091", modGlobalVariables.strREDatabase);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				else if (rsTemp.Get_Fields("Code") == 4)
				{
					rsCost.OpenRecordset("select * from costrecord where crecordnumber = 1092", modGlobalVariables.strREDatabase);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					else if (rsTemp.Get_Fields("Code") == 10)
				{
					modREMain.OpenCRTable(ref rsCost, 1330);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if (rsTemp.Get_Fields("Code") == 11)
				{
					modREMain.OpenCRTable(ref rsCost, 1340);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (rsTemp.Get_Fields("Code") == 210)
				{
					modREMain.OpenCRTable(ref rsCost, 1520);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (rsTemp.Get_Fields("Code") == 211)
				{
					modREMain.OpenCRTable(ref rsCost, 1530);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (rsTemp.Get_Fields("Code") == 221)
				{
					modREMain.OpenCRTable(ref rsCost, 1570);
					strItem += Strings.Trim(FCConvert.ToString(rsCost.Get_Fields_String("ClDesc")));
				}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (rsTemp.Get_Fields("Code") == 400)
				{
					strItem += "Outbuildings (x10 As Entered)";
				}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (rsTemp.Get_Fields("Code") == 401)
				{
					strItem += "MOHO typ/yr/unit/grd/phy";
				}
				else
				{
					strItem += rsTemp.Get_Fields_String("Description");
				}
				lstCodes.AddItem(strItem);
				rsTemp.MoveNext();
			}
		}
		// vbPorter upgrade warning: Code As short	OnWriteFCConvert.ToInt32(
		private string HandleSpecialCase_72(clsDRWrapper rsMst, int Code, int lngAcct, short intCard, int lngSaleID = 0)
		{
			return HandleSpecialCase(ref rsMst, ref Code, ref lngAcct, ref intCard, lngSaleID);
		}

		private string HandleSpecialCase_234(clsDRWrapper rsMst, int Code, int lngAcct, short intCard, int lngSaleID = 0)
		{
			return HandleSpecialCase(ref rsMst, ref Code, ref lngAcct, ref intCard, lngSaleID);
		}

		private string HandleSpecialCase(ref clsDRWrapper rsMst, ref int Code, ref int lngAcct, ref short intCard, int lngSaleID = 0)
		{
			string HandleSpecialCase = "";
			clsDRWrapper rsMisc = new clsDRWrapper();
			string strTemp = "";
			// vbPorter upgrade warning: strReturn As string	OnWrite(string, double)
			string strReturn;
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				HandleSpecialCase = "";
				switch (Code)
				{
					case 7:
						{
							// Set rsmisc = dbtemp.OpenRecordset("select pitopography1,pitopography2 from master where rsaccount = " & lngAcct & " and rscard = " & intCard,strredatabase)
							// Write #intFile, ((rsmisc.Get_Fields("pitopography1") * 10) + rsmisc.fields("pitopography2"));
							// 2            Write #intFile, ((rsMst.Get_Fields("pitopography1") * 10) + rsMst.fields("pitopography2"));
							//FC:FINAL:MSH - i.issue #1138: can't implicitly convert int to string
							strReturn = FCConvert.ToString(rsMst.Get_Fields_Int32("pitopography1")) + FCConvert.ToString(rsMst.Get_Fields_Int32("pitopography2"));
							break;
						}
					case 8:
						{
							//FC:FINAL:MSH - i.issue #1138: can't implicitly convert int to string
							strReturn = FCConvert.ToString(rsMst.Get_Fields_Int32("piutilities1")) + FCConvert.ToString(rsMst.Get_Fields_Int32("piutilities2"));
							break;
						}
					case 15:
						{
							// Set rsmisc = dbtemp.OpenRecordset("select * from master where rsaccount = " & lngAcct & " and rscard = " & intCard,strredatabase)
							// If IsDate(rsmisc.fields("SaleDate")) Then
							if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsMst.Get_Fields_DateTime("saledate")))
							{
								//FC:FINAL:MSH - i.issue #1138: '!=' can't be applied for types datetime and int
								if (Information.IsDate(rsMst.Get_Fields("SaleDate") + "") && (rsMst.Get_Fields_DateTime("saledate").ToOADate() != 0))
								{
									strReturn = Strings.Format(rsMst.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
								}
								else
								{
									// 7                Write #intFile, " ";
								}
								// Else
								// 8                Write #intFile, " ";
							}
							break;
						}
					case 18:
						{
							// exempt codes
							strReturn = rsMst.Get_Fields_Int32("riexemptcd1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsMst.Get_Fields_Int32("riexemptcd2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsMst.Get_Fields_Int32("riexemptcd3");
							break;
						}
					case 24:
						{
							int intZone = 0;
							int intNeig = 0;
							/*? 14 */// 
							intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMst.Get_Fields_Int32("pizone") + "")));
							intNeig = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMst.Get_Fields_Int32("pineighborhood") + "")));
							strReturn = FCConvert.ToString(modGlobalVariables.Statics.LandTRec.Schedule(ref intNeig, ref intZone));
							break;
						}
					case 50:
						{
							// Set rsmisc = dbTemp.OpenRecordset("select * from outbuilding where rsaccount = " & lngAcct & " and rscard = " & intCard,strredatabase)
							// 21            If Not boolFromSales Then
							// 22                Call rsMisc.OpenRecordset("select * from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
							// Else
							// 23                Call rsMisc.OpenRecordset("select * from srmaster where rsaccount = " & lngAcct & " and rscard = " & intCard & " and saleid = " & lngSaleID, strREDatabase)
							// End If
							if (!rsMst.EndOfFile())
							{
								// Write #intFile, CStr(rsmisc.Get_Fields("oidateinspected") & "");
								// 25                Write #intFile, CStr(rsmisc.Get_Fields("dateinspected") & "");
								if (Information.IsDate(rsMst.Get_Fields("dateinspected")))
								{
									//FC:FINAL:MSH - i.issue #1138: '!=' can't be applied for types datetime and int
									if (rsMst.Get_Fields_DateTime("dateinspected").ToOADate() != 0)
									{
										strReturn = FCConvert.ToString(rsMst.Get_Fields_DateTime("dateinspected")) + "";
									}
								}
							}
							else
							{
								// 26                Write #intFile, "";
							}
							break;
						}
					case 52:
						{
							// 27          Write #intFile, Val(rsMst.Get_Fields("rslocnumalph") & "");
							strReturn = FCConvert.ToString(Conversion.Val(rsMst.Get_Fields_String("rslocnumalph") + ""));
							break;
						}
					case 61:
						{
							// book & page
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct) + " and [current] = 1 order by line desc", modGlobalVariables.strREDatabase);
							}
							else
							{
								rsMisc.OpenRecordset("select * from srbookpage where account = " + FCConvert.ToString(lngAcct) + " and [current] = 1 and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "' order by line desc", modGlobalVariables.strREDatabase);
							}
							if (!rsMisc.EndOfFile())
							{
								strTemp = "";
								if (!modGlobalVariables.Statics.CustomizedInfo.MostRecentBP)
								{
									while (!rsMisc.EndOfFile())
									{
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										strTemp += " B" + rsMisc.Get_Fields("book") + "P" + rsMisc.Get_Fields("page");
										if (Information.IsDate(rsMisc.Get_Fields("bpdate")))
										{
											//FC:FINAL:MSH - i.issue #1138: incorrect converting from datetime to int
											//if (FCConvert.ToInt32(rsMisc.Get_Fields("bpdate")) != 0)
											if (rsMisc.Get_Fields_DateTime("bpdate").ToOADate() != 0)
											{
												strTemp += " " + Strings.Format(rsMisc.Get_Fields_String("bpdate"), "MM/dd/yy");
											}
										}
										rsMisc.MoveNext();
									}
								}
								else
								{
									if (!rsMisc.EndOfFile())
									{
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										strTemp += " B" + rsMisc.Get_Fields("book") + "P" + rsMisc.Get_Fields("page");
										if (Information.IsDate(rsMisc.Get_Fields("bpdate")))
										{
											//FC:FINAL:MSH - i.issue #1138: incorrect converting from datetime to int
											//if (FCConvert.ToInt32(rsMisc.Get_Fields("bpdate")) != 0)
											if (rsMisc.Get_Fields_DateTime("bpdate").ToOADate() != 0)
											{
												strTemp += " " + Strings.Format(rsMisc.Get_Fields_String("bpdate"), "MM/dd/yy");
											}
										}
									}
								}
								strTemp = Strings.Trim(strTemp);
								// 39                Write #intFile, strTemp;
								strReturn = strTemp;
								// Else
								// 40                Write #intFile, "";
							}
							break;
						}
					case 62:
						{
							// ref1 without gemini account number
							strTemp = rsMst.Get_Fields_String("rsref1") + Strings.StrDup(23, " ");
							strTemp = Strings.Mid(strTemp, 1, 23);
							strTemp = Strings.Replace(strTemp, FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
							strTemp = Strings.Trim(strTemp);
							// 45            Write #intFile, strTemp;
							strReturn = strTemp;
							break;
						}
					case 63:
						{
							// gemini account number
							strTemp = rsMst.Get_Fields_String("rsref1") + Strings.StrDup(36, " ");
							strTemp = Strings.Mid(strTemp, 24, 12);
							strTemp = Strings.Replace(strTemp, FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
							strTemp = Strings.Trim(strTemp);
							// 50            Write #intFile, strTemp;
							strReturn = strTemp;
							break;
						}
					case 65:
						{
							// tax amount
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							}
							else
							{
								rsMisc.OpenRecordset("select sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from srmaster where rsaccount = " + FCConvert.ToString(lngAcct) + " and saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
							}
							if (!rsMisc.EndOfFile())
							{
								//FC:FINAL:MSH - i.issue #1138: '-' can't be applied for types string and string
								//strTemp = Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("landsum")))) + FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("bldgsum")))) - FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("exemptsum"))))) * (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.BillRate)))) / 1000), "0.00");
								strTemp = Strings.Format((Conversion.Val(rsMisc.Get_Fields("landsum")) + Conversion.Val(rsMisc.Get_Fields("bldgsum")) - Conversion.Val(rsMisc.Get_Fields("exemptsum"))) * (modGlobalVariables.Statics.CustomizedInfo.BillRate / 1000), "0.00");
							}
							else
							{
								strTemp = "0.00";
							}
							// 57            Write #intFile, strTemp;
							strReturn = strTemp;
							break;
						}
					case 67:
						{
							// date updated
							//FC:FINAL:MSH - i.issue #1138: '==' can't be applied for types datetime and int
							if (rsMst.Get_Fields_DateTime("hlupdate").ToOADate() == 0)
							{
								strTemp = "";
							}
							else
							{
								strTemp = FCConvert.ToString(rsMst.Get_Fields_DateTime("hlupdate")) + "";
							}
							// 61            Write #intFile, strTemp;
							strReturn = strTemp;
							break;
						}
					case 74:
						{
							// date created
							if (Information.IsDate(rsMst.Get_Fields("datecreated")))
							{
								//FC:FINAL:MSH - i.issue #1138: '!=' can't be applied for types datetime and int
								if (rsMst.Get_Fields_DateTime("datecreated").ToOADate() != 0)
								{
									strTemp = Strings.Format(rsMst.Get_Fields_DateTime("datecreated"), "MM/dd/yyyy");
								}
								else
								{
									strTemp = "";
								}
							}
							else
							{
								strTemp = "";
							}
							strReturn = strTemp;
							break;
						}
					case 201:
						{
							if (cmbtcreate.Text == "1 Record Per Account")
							{
								if (!boolFromSales)
								{
									rsMisc.OpenRecordset("Select sum(diunitsdwelling) as TheSum from dwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsMisc.OpenRecordset("select sum(diunitsdwelling) as TheSum from srdwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount") + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!rsMisc.EndOfFile())
								{
									// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
									strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
								}
							}
							break;
						}
					case 203:
						{
							if (cmbtcreate.Text == "1 Record Per Account")
							{
								if (!boolFromSales)
								{
									rsMisc.OpenRecordset("select sum(diunitsother)as TheSum from dwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsMisc.OpenRecordset("select sum(diunitsother) as TheSum from srdwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount") + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!rsMisc.EndOfFile())
								{
									// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
									strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
									// Else
									// 75                Write #intFile, " ";
								}
							}
							break;
						}
					case 208:
						{
							if (cmbtcreate.Text == "1 Record Per Account")
							{
								if (!boolFromSales)
								{
									rsMisc.OpenRecordset("select sum(disfmasonry) as TheSum from dwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsMisc.OpenRecordset("select sum(disfmasonry) as TheSum from srdwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount") + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!rsMisc.EndOfFile())
								{
									// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
									strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
								}
							}
							break;
						}
					case 218:
						{
							if (cmbtcreate.Text == "1 Record Per Account")
							{
								if (!boolFromSales)
								{
									rsMisc.OpenRecordset("select sum(disfbsmtliving) as TheSum from dwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsMisc.OpenRecordset("select sum(disfbsmtliving) as TheSum from srdwelling where rsaccount = " + rsMst.Get_Fields_Int32("rsaccount") + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!rsMisc.EndOfFile())
								{
									// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
									strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
									// Else
									// 89                Write #intFile, " ";
								}
							}
							break;
						}
					case 220:
						{
							clsDRWrapper clsDL = new clsDRWrapper();
							if (!boolFromSales)
							{
								clsDL.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
							}
							else
							{
								clsDL.OpenRecordset("select * from srdwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard) + " and saledate = #" + rsMst.Get_Fields_DateTime("saledate") + "#", modGlobalVariables.strREDatabase);
							}
							if (!clsDL.EndOfFile())
							{
								// 95                Write #intFile, clsDL.Get_Fields("dibsmtfingrade1");
								// 96                Write #intFile, clsDL.Get_Fields("dibsmtfingrade2");
								strReturn = clsDL.Get_Fields_Int32("dibsmtfingrade1") + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + clsDL.Get_Fields_Int32("dibsmtfingrade2");
								// Else
								// 97                Write #intFile, "";
								// 98                Write #intFile, "";
							}
							else
							{
								strReturn = FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34));
							}
							break;
						}
					case 238:
						{
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
							}
							else
							{
								rsMisc.OpenRecordset("select * from srdwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard) + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
							}
							if (!rsMisc.EndOfFile())
							{
								strReturn = rsMisc.Get_Fields_Int32("digrade1") + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + rsMisc.Get_Fields_Int32("digrade2");
							}
							else
							{
								strReturn = FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34));
							}
							break;
						}
					case 239:
						{
							if (cmbtcreate.Text == "1 Record Per Account")
							{
								if (!boolFromSales)
								{
									rsMisc.OpenRecordset("select sum(disqft) as TheSum from dwelling where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsMisc.OpenRecordset("select sum(disqft) as TheSum from srdwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!rsMisc.EndOfFile())
								{
									// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
									strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
								}
							}
							break;
						}
					case 249:
						{
							// Set rsmisc = dbTemp.OpenRecordset("select * from outbuilding where rsaccount = " & lngAcct & " and rscard = " & intCard,strredatabase)
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
							}
							else
							{
								rsMisc.OpenRecordset("select * from srmaster where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard) + " and saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
							}
							if (!rsMisc.EndOfFile())
							{
								// Write #intFile, CStr(rsmisc.Get_Fields("oidateinspected") & "");
								// 118                Write #intFile, CStr(rsmisc.Get_Fields("dateinspected") & "");
								strReturn = FCConvert.ToString(rsMisc.Get_Fields_DateTime("dateinspected"));
								// Else
								// 119                Write #intFile, "";
							}
							break;
						}
					case 301:
						{
							// Set rsmisc = dbtemp.OpenRecordset("select * from commercial where rsaccount = " & lngAcct & " and rscard = " & intCard,strredatabase)
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("occ1"));
								// Else
								// 122                Write #intFile, " ";
							}
							break;
						}
					case 302:
						{
							if (cmbtcreate.Text == "1 Record Per Account")
							{
								if (!boolFromSales)
								{
									rsMisc.OpenRecordset("select sum(dwel1 + dwel2) as TheSum from commercial where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsMisc.OpenRecordset("select sum(dwel1 + dwel2) as TheSum from srcommercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
								}
								if (!rsMisc.EndOfFile())
								{
									// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
									strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
									// Else
									// 129                Write #intFile, " ";
								}
							}
							break;
						}
					case 303:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("dwel1"));
								// Else
								// 132                Write #intFile, " ";
							}
							break;
						}
					case 304:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1class"));
								// Else
								// 135                Write #intFile, " ";
							}
							break;
						}
					case 305:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1quality"));
								// Else
								// 138                Write #intFile, " ";
							}
							break;
						}
					case 306:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1grade"));
								// Else
								// 141                Write #intFile, " ";
							}
							break;
						}
					case 307:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1extwalls"));
								// Else
								// 144                Write #intFile, " ";
							}
							break;
						}
					case 308:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1stories"));
								// Else
								// 147                Write #intFile, " ";
							}
							break;
						}
					case 309:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1height"));
							}
							break;
						}
					case 310:
						{
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select sum(c1floor + c2floor) as TheSum from commercial where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							}
							else
							{
								rsMisc.OpenRecordset("select sum(c1floor + c2floor) as TheSum from srcommercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
							}
							if (!rsMisc.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
								// Else
								// 156                Write #intFile, " ";
							}
							break;
						}
					case 311:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1floor"));
								// Else
								// 159                Write #intFile, " ";
							}
							break;
						}
					case 312:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1perimeter"));
								// Else
								// 162                Write #intFile, " ";
							}
							break;
						}
					case 313:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1heat"));
								// Else
								// 165                Write #intFile, " ";
							}
							break;
						}
					case 314:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1built"));
								// Else
								// 168                Write #intFile, " ";
							}
							break;
						}
					case 315:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1remodel"));
								// Else
								// 171                Write #intFile, " ";
							}
							break;
						}
					case 316:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1condition"));
							}
							break;
						}
					case 317:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1phys"));
							}
							break;
						}
					case 318:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("c1funct"));
							}
							break;
						}
					case 319:
						{
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(rsCom.Get_Fields_Int32("cmecon"));
							}
							break;
						}
					case 320:
						{
							// sqft by account
							if (!boolFromSales)
							{
								rsMisc.OpenRecordset("select sum(c1floor  * c1stories  + (c2floor  * c2stories )) as TheSum from commercial where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							}
							else
							{
								rsMisc.OpenRecordset("select sum(c1floor  * c1stories  + (c2floor * c2stories )) as TheSum from srcommercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and saledate = '" + rsMst.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
							}
							if (!rsMisc.EndOfFile())
							{
								// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
								strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
							}
							break;
						}
					case 321:
						{
							// sqft by card
							if (!rsCom.EndOfFile())
							{
								strReturn = FCConvert.ToString(Conversion.Val(rsCom.Get_Fields_Int32("c1floor")) * Conversion.Val(rsCom.Get_Fields_Int32("c1stories")));
							}
							break;
						}
					case 400:
						{
							strReturn = WriteOutbuildings(ref lngAcct, ref intCard, lngSaleID);
							break;
						}
					case 401:
						{
							strReturn = writemoho(ref lngAcct, ref intCard);
							break;
						}
				}
				//end switch
				HandleSpecialCase = strReturn;
				return HandleSpecialCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleSpecialCase line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleSpecialCase;
		}
		// vbPorter upgrade warning: Code As short	OnWriteFCConvert.ToInt32(
		private string HandleRegular_72(int Code, clsDRWrapper rsCode, int lngAcct, short intCard, bool boolSum = false, int lngSaleID = 0)
		{
			return HandleRegular(ref Code, ref rsCode, ref lngAcct, ref intCard, boolSum, lngSaleID);
		}

		private string HandleRegular_234(int Code, clsDRWrapper rsCode, int lngAcct, short intCard, bool boolSum = false, int lngSaleID = 0)
		{
			return HandleRegular(ref Code, ref rsCode, ref lngAcct, ref intCard, boolSum, lngSaleID);
		}

		private string HandleRegular_639(int Code, clsDRWrapper rsCode, int lngAcct, short intCard, int lngSaleID = 0)
		{
			return HandleRegular(ref Code, ref rsCode, ref lngAcct, ref intCard, false, lngSaleID);
		}

		private string HandleRegular_720(int Code, clsDRWrapper rsCode, int lngAcct, short intCard, bool boolSum = false, int lngSaleID = 0)
		{
			return HandleRegular(ref Code, ref rsCode, ref lngAcct, ref intCard, boolSum, lngSaleID);
		}

		private string HandleRegular(ref int Code, ref clsDRWrapper rsCode, ref int lngAcct, ref short intCard, bool boolSum = false, int lngSaleID = 0)
		{
			string HandleRegular = "";
			clsDRWrapper rsMisc = new clsDRWrapper();
			string strReturn;
			// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
			cParty tParty = new cParty();
			cPartyController tPartyCont = new cPartyController();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				HandleRegular = "";
				// 1    If rsCode.Fields("FieldName") = "dibasement" Then
				// 2        rsCode.Edit
				// 3        rsCode.Fields("FieldName") = "dibsmt"
				// 4        rsCode.Update
				// 
				// End If
				// 5    If rsCode.Fields("FieldName") = "diwetbasement" Then
				// 6        rsCode.Edit
				// 7        rsCode.Fields("FieldName") = "diwetbsmt"
				// 8        rsCode.Update
				// End If
				if (!boolSum || !rsCode.Get_Fields_Boolean("summable"))
				{
					if (!boolFromSales)
					{
						if (Strings.LCase(rsCode.Get_Fields_String("fieldname")) == "rssecowner")
						{
							rsMisc.OpenRecordset("select secownerpartyid from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = 1", modGlobalVariables.strREDatabase);
							if (!rsMisc.EndOfFile())
							{
								if (Conversion.Val(rsMisc.Get_Fields_Int32("secownerpartyid")) > 0)
								{
									tParty = tPartyCont.GetParty(rsMisc.Get_Fields_Int32("secownerpartyid"), true);
									if (!(tParty == null))
									{
										strReturn = tParty.FullNameLastFirst;
									}
									else
									{
										strReturn = "";
									}
									HandleRegular = strReturn;
									return HandleRegular;
								}
								else
								{
									strReturn = "";
									HandleRegular = strReturn;
									return HandleRegular;
								}
							}
							else
							{
								strReturn = "";
								HandleRegular = strReturn;
								return HandleRegular;
							}
						}
						else
						{
							rsMisc.OpenRecordset("select " + rsCode.Get_Fields_String("FieldName") + " from " + rsCode.Get_Fields_String("TableName") + " where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						if (Strings.UCase(rsCode.Get_Fields_String("tablename")) == "MASTER")
						{
							rsMisc.OpenRecordset("select " + rsCode.Get_Fields_String("fieldname") + " from sr" + rsCode.Get_Fields_String("tablename") + " where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard) + " and saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select " + rsCode.Get_Fields_String("fieldname") + " from sr" + rsCode.Get_Fields_String("tablename") + " where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
						}
					}
					if (!rsMisc.EndOfFile())
					{
						// 16        Write #intFile, rsmisc.Get_Fields(rsCode.Get_Fields("FieldName"));
						strReturn = rsMisc.Get_Fields(rsCode.Get_Fields_String("fieldname") + "") + "";
					}
					else
					{
						// 17        Write #intFile, "";
						strReturn = "";
					}
				}
				else
				{
					if (!boolFromSales)
					{
						rsMisc.OpenRecordset("select sum(" + rsCode.Get_Fields_String("fieldname") + ") as thesum from " + rsCode.Get_Fields_String("tablename") + " where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
					}
					else
					{
						rsMisc.OpenRecordset("select sum(" + rsCode.Get_Fields_String("fieldname") + ") as thesum from sr" + rsCode.Get_Fields_String("tablename") + " where rsaccount = " + FCConvert.ToString(lngAcct) + " and saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
					}
					if (!rsMisc.EndOfFile())
					{
						// 22            Write #intFile, CStr(Val(rsmisc.Get_Fields("thesum") & ""));  'val causes the quotes not to be put around the data
						// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
						strReturn = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + ""));
					}
					else
					{
						// 23            Write #intFile, "";
						strReturn = "";
					}
				}
				HandleRegular = strReturn;
				return HandleRegular;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleRegular line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleRegular;
		}

		private string writemoho(ref int ActNum, ref short cardnum)
		{
			string writemoho = "";
			clsDRWrapper rsOut = null;
			string strReturn;
			int x;
			strReturn = "";
			writemoho = "";
			// Set rsOut = dbtemp.OpenRecordset("select * from outbuilding where rsaccount = " & ActNum & " and rscard = " & Cardnum,strredatabase)
			modREMain.OpenOutBuildingTable(ref rsOut, ActNum, cardnum);
			if (rsOut.EndOfFile())
			{
				// Write #intFile, " ";
				// Write #intFile, " ";
				// Write #intFile, " ";
				// Write #intFile, " ";
				// Write #intFile, " ";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34));
				return writemoho;
			}
			for (x = 1; x <= 10; x++)
			{
				// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
				if (rsOut.Get_Fields("oitype" + FCConvert.ToString(x)) > 699 && rsOut.Get_Fields("oitype" + FCConvert.ToString(x)) < 999)
				{
					if (chkDescription.CheckState == Wisej.Web.CheckState.Checked)
					{
						// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x))))))
						{
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + modGlobalVariables.Statics.CostRec.ClDesc + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						else
						{
							// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
					}
					else
					{
						// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
					// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
					strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsOut.Get_Fields("oitype" + FCConvert.ToString(x)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
					// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
					strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsOut.Get_Fields("oiyear" + FCConvert.ToString(x)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
					// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
					strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsOut.Get_Fields("oiunits" + FCConvert.ToString(x)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
					// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
					strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsOut.Get_Fields("oigradecd" + FCConvert.ToString(x)) + rsOut.Get_Fields("oigradepct" + FCConvert.ToString(x)) + FCConvert.ToString(Convert.ToChar(34));
					// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
					strReturn += FCConvert.ToString(Convert.ToChar(34)) + rsOut.Get_Fields("oipctphys" + FCConvert.ToString(x)) + FCConvert.ToString(Convert.ToChar(34));
					break;
				}
			}
			// x
			if (Strings.Trim(strReturn) != string.Empty)
			{
				strReturn = Strings.Mid(strReturn, 2, strReturn.Length - 2);
				// get rid of first and last quote since calling function adds it
			}
			else
			{
				strReturn = FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strReturn += FCConvert.ToString(Convert.ToChar(34));
			}
			writemoho = strReturn;
			return writemoho;
		}

		private string WriteOutbuildings(ref int ActNum, ref short cardnum, int lngSaleID = 0)
		{
			string WriteOutbuildings = "";
			clsDRWrapper rsOut = new clsDRWrapper();
			string strReturn;
			int x;
			string strSaleDate = "";
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				WriteOutbuildings = "";
				for (x = 1; x <= 100; x++)
				{
					strReturn += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				// x
				strReturn = Strings.Mid(strReturn, 2, strReturn.Length - 3);
				if (lngSaleID == 0)
				{
					rsOut.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(ActNum) + " and rscard = " + FCConvert.ToString(cardnum), modGlobalVariables.strREDatabase);
				}
				else
				{
					rsOut.OpenRecordset("select saledate from srmaster where saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
					if (!rsOut.EndOfFile())
					{
						if (Information.IsDate(rsOut.Get_Fields("saledate")))
						{
							//FC:FINAL:MSH - i.issue #1138: incorrect converting from datetime to int
							//if (FCConvert.ToInt32(rsOut.Get_Fields("saledate")) != 0)
							if (rsOut.Get_Fields_DateTime("saledate").ToOADate() != 0)
							{
								strSaleDate = FCConvert.ToString(rsOut.Get_Fields_DateTime("saledate"));
								rsOut.OpenRecordset("select * from sroutbuilding where rsaccount = " + FCConvert.ToString(ActNum) + " and rscard = " + FCConvert.ToString(cardnum) + " and saledate = #" + strSaleDate + "#", modGlobalVariables.strREDatabase);
							}
							else
							{
								WriteOutbuildings = strReturn;
								return WriteOutbuildings;
							}
						}
						else
						{
							WriteOutbuildings = strReturn;
							return WriteOutbuildings;
						}
					}
				}
				if (!rsOut.EndOfFile())
				{
					strReturn = "";
					for (x = 1; x <= 10; x++)
					{
						if (chkDescription.CheckState == Wisej.Web.CheckState.Checked)
						{
							// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x))))))
							{
								strReturn += FCConvert.ToString(Convert.ToChar(34)) + modGlobalVariables.Statics.CostRec.ClDesc + FCConvert.ToString(Convert.ToChar(34)) + ",";
							}
							else
							{
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
							}
						}
						else
						{
							// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oiyear" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [Oiunits] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("Oiunits" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oigradecd" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oigradepct" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oicond" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oipctphys" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
						strReturn += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oipctfunct" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
						if (FCConvert.ToBoolean(rsOut.Get_Fields("oiusesound" + FCConvert.ToString(x))))
						{
							// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + "TRUE" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						else
						{
							// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
							strReturn += FCConvert.ToString(Convert.ToChar(34)) + "FALSE" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
					}
					// x
					strReturn = Strings.Mid(strReturn, 2, strReturn.Length - 3);
					// get rid of first and last quotes and last comma
				}
				WriteOutbuildings = strReturn;
				return WriteOutbuildings;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In WriteOutbuildings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return WriteOutbuildings;
		}
		// vbPorter upgrade warning: intCode As short	OnWriteFCConvert.ToInt32(
		private string GetDescFromCode_6(int intCode, bool boolCheckBox = false, bool boolForHeaders = false)
		{
			return GetDescFromCode(ref intCode, boolCheckBox, boolForHeaders);
		}

		private string GetDescFromCode_8(int intCode, bool boolCheckBox = false, bool boolForHeaders = false)
		{
			return GetDescFromCode(ref intCode, boolCheckBox, boolForHeaders);
		}

		private string GetDescFromCode_21(int intCode, bool boolForHeaders = false)
		{
			return GetDescFromCode(ref intCode, false, boolForHeaders);
		}

		private string GetDescFromCode(ref int intCode, bool boolCheckBox = false, bool boolForHeaders = false)
		{
			string GetDescFromCode = "";
			string strTemp = "";
			int x;
			if (boolCheckBox)
			{
				switch (intCode)
				{
					case 0:
						{
							strTemp = "Account Number";
							break;
						}
					case 1:
						{
							strTemp = "Owner's Name";
							break;
						}
					case 2:
						{
							strTemp = "Location";
							break;
						}
					case 3:
						{
							strTemp = "Map/Lot";
							break;
						}
					case 4:
						{
							strTemp = "Reference 1";
							break;
						}
					case 5:
						{
							strTemp = "Reference 2";
							break;
						}
					case 6:
						{
							strTemp = "Address 1" + "\r\n";
							strTemp += "Address 2" + "\r\n";
							strTemp += "Address 3" + "\r\n";
							strTemp += "City" + "\r\n";
							strTemp += "State" + "\r\n";
							strTemp += "Zipcode" + "\r\n";
							strTemp += "4 Digit Zip";
							break;
						}
					case 7:
						{
							strTemp = "Telephone Number";
							break;
						}
					case 8:
						{
							strTemp = "Acreage";
							break;
						}
					case 9:
						{
							strTemp = "Current Land Value";
							break;
						}
					case 10:
						{
							strTemp = "Current Building Value";
							break;
						}
					case 11:
						{
							strTemp = "Current Total Value";
							break;
						}
					case 12:
						{
							strTemp = "Last Year's Land Value (Billing Value)";
							break;
						}
					case 13:
						{
							strTemp = "Last Year's Building Value (Billing Value)";
							break;
						}
					case 14:
						{
							strTemp = "Last Year's Total (Billing Value)";
							break;
						}
					case 15:
						{
							strTemp = "Exemption Amount";
							break;
						}
					case 16:
						{
							strTemp = "Net Assessment";
							break;
						}
					case 17:
						{
							strTemp = "Square Foot Living Area (SFLA)";
							break;
						}
					case 18:
						{
							strTemp = "Card Number";
							break;
						}
				}
				//end switch
			}
			else
			{
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper rsCst = null;
				rsTemp.OpenRecordset("select * from reportcodes where code = " + FCConvert.ToString(intCode), modGlobalVariables.strREDatabase);
				switch (intCode)
				{
					case 3:
						{
							rsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1091", modGlobalVariables.strREDatabase);
							strTemp = Strings.Trim(FCConvert.ToString(rsTemp.GetData("ClDesc")));
							break;
						}
					case 4:
						{
							rsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1092", modGlobalVariables.strREDatabase);
							strTemp = Strings.Trim(rsTemp.GetData("ClDesc") + "");
							break;
						}
					case 10:
						{
							modREMain.OpenCRTable(ref rsCst, 1330);
							strTemp = Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
							break;
						}
					case 11:
						{
							modREMain.OpenCRTable(ref rsCst, 1340);
							strTemp = Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
							break;
						}
					case 18:
						{
							if (boolForHeaders)
							{
								strTemp = "Exempt Code 1" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt Code 2" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt Code 3";
							}
							else
							{
								strTemp = "Exempt Code 1,Exempt Code 2,Exempt Code 3";
							}
							break;
						}
					case 210:
						{
							modREMain.OpenCRTable(ref rsCst, 1520);
							strTemp = Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
							break;
						}
					case 211:
						{
							modREMain.OpenCRTable(ref rsCst, 1530);
							strTemp = Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
							break;
						}
					case 220:
						{
							if (boolForHeaders)
							{
								strTemp = "Finished Bsmt Grade" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Finished Bsmt Factor";
							}
							else
							{
								strTemp = "Finished Bsmt Grade,Finished Bsmt Factor";
							}
							break;
						}
					case 221:
						{
							modREMain.OpenCRTable(ref rsCst, 1570);
							strTemp = Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
							break;
						}
					case 238:
						{
							if (boolForHeaders)
							{
								strTemp = "Grade" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Factor";
							}
							else
							{
								strTemp = "Grade,Factor";
							}
							break;
						}
					case 400:
						{
							if (boolForHeaders)
							{
								strTemp = "";
								for (x = 1; x <= 10; x++)
								{
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Type" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Year" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Units" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Grade" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Factor" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Condition" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Physical" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "Functional" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "UseSound" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
									strTemp += FCConvert.ToString(Convert.ToChar(34)) + "SoundValue" + FCConvert.ToString(x) + FCConvert.ToString(Convert.ToChar(34)) + ",";
								}
								// x
								strTemp = Strings.Mid(strTemp, 2, strTemp.Length - 3);
							}
							else
							{
								strTemp = "Outbuildings (X10) typ/yr/unit/grd/fct/cnd/phys/func/sound";
							}
							break;
						}
					case 401:
						{
							strTemp = "MOHO typ/yr/unit/grd/phy";
							break;
						}
					default:
						{
							strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("Description"));
							break;
						}
				}
				//end switch
			}
			GetDescFromCode = strTemp;
			return GetDescFromCode;
		}

		private void FillTxtRec()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			int intCnt;
			clsDRWrapper rsCst = null;
			clsDRWrapper rsCost = new clsDRWrapper();
			string strItem = "";
			string strTemp = "";
			//FC:FINAL:AM:#1666 - download instead the file
			//txtRec = "The Extract is a comma delimited file named TSDBASE.ASC." + "\r\n" + "It is saved in " + Environment.CurrentDirectory + "\r\n" + "It is in the following order:" + "\r\n" + "\r\n";
			txtRec = "The Extract is a comma delimited file named TSDBASE.ASC." + "\r\n" + "It is in the following order:" + "\r\n" + "\r\n";
			int x;
			for (x = 0; x <= 17; x++)
			{
				if (x == 1 && chkInclude[18].Checked)
				{
					txtRec += "Card Number" + "\r\n";
				}
				if (chkInclude[FCConvert.ToInt16(x)].Checked)
				{
					txtRec += GetDescFromCode_6(x, true) + "\r\n";
				}
			}
			// x
			if (intLenOfArray > 0)
			{
				for (intCnt = 0; intCnt <= (intLenOfArray - 1); intCnt++)
				{
					rsTemp.OpenRecordset("select * from reportcodes where code = " + FCConvert.ToString(CodeArray[intCnt]), modGlobalVariables.strREDatabase);
					if (rsTemp.EndOfFile())
					{
					}
					else
					{
						strItem = "";
						if (FCConvert.ToInt32(rsTemp.GetData("code")) == 3)
						{
							rsCost.OpenRecordset("select * from costrecord where crecordnumber = 1091", modGlobalVariables.strREDatabase);
							strItem += Strings.Trim(FCConvert.ToString(rsCost.GetData("ClDesc")));
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 4)
						{
							rsCost.OpenRecordset("select * from costrecord where crecordnumber = 1092", modGlobalVariables.strREDatabase);
							strItem += Strings.Trim(rsCost.GetData("ClDesc") + "");
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 10)
						{
							modREMain.OpenCRTable(ref rsCst, 1330);
							strItem += Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 11)
						{
							modREMain.OpenCRTable(ref rsCst, 1340);
							strItem += Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 18)
						{
							strItem += "Exempt Code 1" + "\r\n" + "Exempt Code 2" + "\r\n" + "Exempt Code 3";
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 210)
						{
							modREMain.OpenCRTable(ref rsCst, 1520);
							strItem += Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 211)
						{
							modREMain.OpenCRTable(ref rsCst, 1530);
							strItem += Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 221)
						{
							modREMain.OpenCRTable(ref rsCst, 1570);
							strItem += Strings.Trim(rsCst.Get_Fields_String("cldesc") + "");
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 400)
						{
							strTemp = "";
							for (x = 1; x <= 10; x++)
							{
								strTemp += "Type " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Year " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Units " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Grade " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Factor " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Condition " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Physical " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Functional " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Use Sound Value " + FCConvert.ToString(x) + "\r\n";
								strTemp += "Sound Value " + FCConvert.ToString(x) + "\r\n";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
							strItem += strTemp;
						}
						else if (FCConvert.ToInt32(rsTemp.GetData("code")) == 401)
						{
							strItem += "MOHO typ/yr/unit/grd/phy";
						}
						else
						{
							strItem += rsTemp.GetData("Description");
						}
						txtRec += strItem + "\r\n";
					}
				}
				// intCnt
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:AM:#1665 - enable cancel button
            //cmdDone_Click();
            this.ShowLoader = false;
            FCUtils.StartTask(this, () =>
            {
                cmdDone_Click();
                FCUtils.UnlockUserInterface();
            });
        }

		private void mnuCopyExtract_Click(object sender, System.EventArgs e)
		{
			modGlobalRoutines.NewExtractToDisk();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index > 0 && Index < 4)
			{
				txtStart.Visible = true;
				txtEnd.Visible = true;
				t2kStart.Visible = false;
				t2kEnd.Visible = false;
				lblTo.Visible = true;
			}
			else if (Index == 5)
			{
				txtStart.Visible = false;
				txtEnd.Visible = false;
				t2kStart.Visible = true;
				t2kEnd.Visible = true;
				lblTo.Visible = true;
			}
			else
			{
				txtStart.Visible = false;
				txtEnd.Visible = false;
				t2kStart.Visible = false;
				t2kEnd.Visible = false;
				lblTo.Visible = false;
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void OptRecords_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// If Index = 0 Then
			// If optRange(5).Value Then
			// optRange(0).Value = True
			// txtStart.Text = ""
			// txtEnd.Text = ""
			// txtStart.Visible = False
			// txtEnd.Visible = False
			// lblTo.Visible = False
			// End If
			// optRange(5).Enabled = False
			// Else
			// optRange(5).Enabled = True
			// End If
		}

		private void OptRecords_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtRecords.SelectedIndex;
			OptRecords_CheckedChanged(index, sender, e);
		}
	}
}
