﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmListing.
	/// </summary>
	public partial class frmListing : BaseForm
	{
        private string cmbOrder_Item0_Text = "Account";
        private string cmbOrder_Item1_Text = "Name";
        private string cmbOrder_Item2_Text = "Map/Lot";
        private string cmbOrder_Item3_Text = "Location";
        public frmListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmListing InstancePtr
		{
			get
			{
				return (frmListing)Sys.GetInstance(typeof(frmListing));
			}
		}

		protected frmListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdOkay_Click()
		{
			string strFieldName = "";
			string strResponse = "";
			// vbPorter upgrade warning: minaccount As string	OnRead(int, string)
			string minaccount = "";
			// vbPorter upgrade warning: maxaccount As string	OnRead(string, int)
			string maxaccount = "";
			bool boolTotTaxable = false;
			short x = 0;
			short intOrder = 0;
			string strStart = "";
			string strEnd = "";
			string strTemp = "";
			bool boolEmail;
			int intChoice;
			if (Grid.Row < 0)
				return;
			modGlobalVariables.Statics.gintMinAccountRange = 1;
			modGlobalVariables.Statics.gintMaxAccountRange = 9999999;
			modPrintRoutines.Statics.gstrMinAccountRange = "a";
			modPrintRoutines.Statics.gstrMaxAccountRange = "ZZZZZZ";
			modGlobalVariables.Statics.boolRange = false;
			boolEmail = false;
			intChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, 0))));
			if (chkInclusive.CheckState == Wisej.Web.CheckState.Checked)
			{
				modRegistry.SaveRegistryKey("REReportRangeInclusive", FCConvert.ToString(true));
			}
			else
			{
				modRegistry.SaveRegistryKey("REReportRangeInclusive", FCConvert.ToString(false));
			}
			if ((cmbPrint.Text == "From Extract" && cmbPrint.Items.Contains("From Extract")) || intChoice == 6)
			{
				frmGetGroupsToPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (modGlobalVariables.Statics.CancelledIt)
					return;
			}
			if (intChoice == 18)
			{
				frmReportViewer.InstancePtr.Init(rptDuplicateMaps.InstancePtr);
				return;
			}
			if (intChoice == 11 || intChoice == 12)
			{
				if (cmbPrint.Text == "Range" || cmbPrint.Text == "Single")
				{
					modGlobalVariables.Statics.boolRange = true;
					if (cmbOrder.Text == cmbOrder_Item0_Text)
					{
						if (Conversion.Val(txtFrom.Text) < 1)
						{
							MessageBox.Show("This is an invalid account range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						minaccount = FCConvert.ToString(Conversion.Val(txtFrom.Text));
						modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(minaccount);
						modPrintRoutines.Statics.gstrMinAccountRange = FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange);
					}
					else
					{
						minaccount = Strings.Trim(modGlobalRoutines.escapequote_2(txtFrom.Text));
						modPrintRoutines.Statics.gstrMinAccountRange = minaccount;
					}
					if (cmbPrint.Text == "Range")
					{
						if (cmbOrder.Text == cmbOrder_Item0_Text)
						{
							if ((Conversion.Val(txtTo.Text) < 1) || (Conversion.Val(txtTo.Text) < Conversion.Val(minaccount)))
							{
								MessageBox.Show("This is an invalid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							maxaccount = FCConvert.ToString(Conversion.Val(txtTo.Text));
							modPrintRoutines.Statics.gstrMaxAccountRange = maxaccount;
							modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(maxaccount);
						}
						else
						{
							maxaccount = Strings.Trim(modGlobalRoutines.escapequote_2(txtTo.Text));
							if (chkInclusive.CheckState == Wisej.Web.CheckState.Checked)
							{
								maxaccount += "zzzz";
							}
							modPrintRoutines.Statics.gstrMaxAccountRange = maxaccount;
						}
					}
					else
					{
						txtTo.Text = txtFrom.Text;
						minaccount = txtFrom.Text;
						maxaccount = minaccount;
						if (cmbOrder.Text != cmbOrder_Item0_Text)
						{
							if (chkInclusive.CheckState == Wisej.Web.CheckState.Checked)
							{
								maxaccount += "zzzz";
								txtTo.Text = txtFrom.Text + "zzzz";
							}
						}
						modPrintRoutines.Statics.gstrMaxAccountRange = maxaccount;
					}
				}
				if (intChoice == 11)
				{
					if (chkShowDeleted.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (!Information.IsDate(t2kDeletedSince.Text))
						{
							MessageBox.Show("You must choose a valid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					if (chkChanges.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (chkShowDeleted.CheckState == Wisej.Web.CheckState.Checked)
						{
							rptValueComparison.InstancePtr.Init(true, true, cmbOrder.Text == cmbOrder_Item1_Text, modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract", modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, t2kDeletedSince.Text);
						}
						else
						{
							rptValueComparison.InstancePtr.Init(true, false, cmbOrder.Text == cmbOrder_Item1_Text, modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract", modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange);
						}
					}
					else
					{
						if (chkShowDeleted.CheckState == Wisej.Web.CheckState.Checked)
						{
							rptValueComparison.InstancePtr.Init(false, true, cmbOrder.Text == cmbOrder_Item1_Text, modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract", modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, t2kDeletedSince.Text);
						}
						else
						{
							rptValueComparison.InstancePtr.Init(false, false, cmbOrder.Text == cmbOrder_Item1_Text, modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract", modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange);
						}
					}
					return;
				}
				if (intChoice == 12)
				{
					if (chkChanges.CheckState == Wisej.Web.CheckState.Checked)
					{
						rptAssessmentComparison.InstancePtr.Init(true, cmbOrder.Text == cmbOrder_Item1_Text, modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract", modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange);
					}
					else
					{
						rptAssessmentComparison.InstancePtr.Init(false, cmbOrder.Text == cmbOrder_Item1_Text, modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract", modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange);
					}
					return;
				}
			}
			if (intChoice == 9)
			{
				frmaudit.InstancePtr.Show(App.MainForm);
				// Unload Me
				return;
			}
			else if (intChoice == 10)
			{
				frmReportViewer.InstancePtr.Init(rptREPPAudit.InstancePtr, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "REPPAudit");
				// Unload Me
				return;
			}
			if (intChoice == 8)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					intOrder = 0;
				}
				else if (cmbOrder.Text == cmbOrder_Item1_Text)
				{
					intOrder = 1;
				}
				else
				{
					intOrder = 2;
				}
				if (cmbPrevious.Text == "Account")
				{
					if (Conversion.Val(txtAccount.Text) <= 0)
					{
						MessageBox.Show("You must enter a valid account to print this report.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					rptPreviousOwners.InstancePtr.Init(ref intOrder, true, FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
				}
				else
				{
					if (!Information.IsDate(txtAccount.Text))
					{
						MessageBox.Show("You must enter a valid start date to print this report.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						strStart = Strings.Format(txtAccount.Text, "MM/dd/yyyy");
					}
					if (!Information.IsDate(txtDate.Text))
					{
						strEnd = strStart;
					}
					else
					{
						strEnd = Strings.Format(txtDate.Text, "MM/dd/yyyy");
					}
					rptPreviousOwners.InstancePtr.Init(ref intOrder, false, 0, strStart, strEnd);
				}
				return;
			}
			if (intChoice == 38)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptDuplicateRecords.InstancePtr.Init(0, this.Modal);
				}
				else if (cmbOrder.Text == cmbOrder_Item1_Text)
				{
					rptDuplicateRecords.InstancePtr.Init(1, this.Modal);
				}
					else if (cmbOrder.Text == cmbOrder_Item2_Text)
				{
					rptDuplicateRecords.InstancePtr.Init(2, this.Modal);
				}
			}
			if (cmbOrder.Text == cmbOrder_Item0_Text)
			{
				// by account
				modPrintRoutines.Statics.gstrFieldName = "RSAccount";
				strFieldName = "Account";
				x = 1;
			}
			else if (cmbOrder.Text == cmbOrder_Item1_Text)
			{
				modPrintRoutines.Statics.gstrFieldName = "RSName";
				strFieldName = "Name";
				x = 2;
			}
			else if (cmbOrder.Text == cmbOrder_Item2_Text)
			{
				modPrintRoutines.Statics.gstrFieldName = "RSMapLot";
				strFieldName = "Map / Lot";
				x = 3;
			}
			else if (cmbOrder.Text == cmbOrder_Item3_Text)
			{
				modPrintRoutines.Statics.gstrFieldName = "RSLocStreet";
				strFieldName = "Location";
				x = 4;
			}
			if (intChoice == 28 || intChoice == 29)
			{
				intOrder = 0;
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					intOrder = 0;
				}
				else if (cmbOrder.Text == cmbOrder_Item1_Text)
				{
					intOrder = 1;
				}
				else if (cmbOrder.Text == cmbOrder_Item2_Text)
				{
					intOrder = 2;
				}
				else if (cmbOrder.Text == cmbOrder_Item3_Text)
				{
					intOrder = 3;
				}
				if (intChoice == 28)
				{
					rptTaxAcquired.InstancePtr.Init(modGlobalVariables.CNSTREPORTTYPETAXACQUIRED, intOrder);
				}
				else if (intChoice == 29)
				{
					rptTaxAcquired.InstancePtr.Init(modGlobalVariables.CNSTREPORTTYPEBANKRUPTCY, intOrder);
				}
			}
			if (intChoice == 24)
			{
				//! Load dlgTaxNotice;
				dlgTaxNotice.InstancePtr.Init(ref x);
				// Unload Me
				return;
			}
			if (intChoice == 26)
			{
				// Call rptAssessCodes.Show
				rptAssessCodes.InstancePtr.Init();
				// Unload Me
				return;
			}
			if (intChoice == 25)
			{
				// rptAssAnalysis.Show
				// Call frmReportViewer.Init(rptAssAnalysis, , , , , , , , , , True, "AssessmentAnalysis")
				rptAssAnalysis.InstancePtr.Init();
				// Unload Me
				return;
			}
			if (intChoice == 21)
			{
				// rptDeletedAccounts.Show
				strStart = "";
				strEnd = "";
				if (Information.IsDate(t2kDateStart.Text))
				{
					strStart = t2kDateStart.Text;
				}
				if (Information.IsDate(t2kDateEnd.Text))
				{
					strEnd = t2kDateEnd.Text;
				}
				if (strStart != string.Empty && strEnd != string.Empty)
				{
					if (DateAndTime.DateDiff("d", DateAndTime.DateValue(strStart), DateAndTime.DateValue(strEnd)) < 0)
					{
						MessageBox.Show("This is an invalid date range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				rptDeletedAccounts.InstancePtr.Init(strStart, strEnd);
				// Unload Me
				return;
			}
			if (intChoice == 23)
			{
				// rptZoneNeighborhoodDifference.Show
				rptZoneNeighborhoodDifference.InstancePtr.Init();
				// Unload Me
				return;
			}
			if (cmbPrint.Text == "Range" && cmbPrint.Items.Contains("Range") && Frame1.Visible)
			{
				modGlobalVariables.Statics.boolRange = true;
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					if (Conversion.Val(txtFrom.Text) < 1)
					{
						MessageBox.Show("This is an invalid account range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					minaccount = FCConvert.ToString(Conversion.Val(txtFrom.Text));
				}
				else
				{
					minaccount = Strings.Trim(modGlobalRoutines.escapequote_2(txtFrom.Text));
				}
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					if ((Conversion.Val(txtTo.Text) < 1) || (Conversion.Val(txtTo.Text) < Conversion.Val(minaccount)))
					{
						MessageBox.Show("This is an invalid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					maxaccount = FCConvert.ToString(Conversion.Val(txtTo.Text));
				}
				else
				{
					maxaccount = Strings.Trim(modGlobalRoutines.escapequote_2(txtTo.Text));
					if (chkInclusive.CheckState == Wisej.Web.CheckState.Checked)
					{
						maxaccount += "zzzz";
					}
				}
				modPrintRoutines.Statics.gstrMinAccountRange = minaccount;
				modPrintRoutines.Statics.gstrMaxAccountRange = maxaccount;
				if (cmbOrder.Text != cmbOrder_Item2_Text)
				{
					modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(minaccount)));
					modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(maxaccount)));
				}
			}
			else if (cmbPrint.Text == "Single")
			{
				modGlobalVariables.Statics.boolRange = true;
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					if (Conversion.Val(txtFrom.Text) < 1)
					{
						MessageBox.Show("This is an invalid account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					minaccount = FCConvert.ToString(Conversion.Val(txtFrom.Text));
					maxaccount = minaccount;
					modPrintRoutines.Statics.gstrMinAccountRange = minaccount;
					modPrintRoutines.Statics.gstrMaxAccountRange = maxaccount;
					modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(minaccount)));
					modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(maxaccount)));
				}
				else
				{
					minaccount = Strings.Trim(modGlobalRoutines.escapequote_2(txtFrom.Text));
					maxaccount = minaccount;
					if (chkInclusive.CheckState == Wisej.Web.CheckState.Checked)
					{
						maxaccount += "zzzz";
						txtTo.Text = txtFrom.Text + "zzzz";
					}
					modPrintRoutines.Statics.gstrMinAccountRange = minaccount;
					modPrintRoutines.Statics.gstrMaxAccountRange = maxaccount;
				}
			}
			else
			{
				modGlobalVariables.Statics.boolRange = false;
			}
			if (intChoice == 40)
			{
				cExemptByAccountReport ebaReport = new cExemptByAccountReport();
				cExemptByAccountController ebaController = new cExemptByAccountController();
				ebaReport.TownNumber = 0;
				if (!(cmbPrint.Text == "Select Exemptions"))
				{
					ebaReport.ByRange = modGlobalVariables.Statics.boolRange;
					ebaReport.RangeType = "Account";
					ebaReport.MinRange = modPrintRoutines.Statics.gstrMinAccountRange;
					ebaReport.MaxRange = modPrintRoutines.Statics.gstrMaxAccountRange;
				}
				else
				{
					ebaReport.ByRange = true;
					ebaReport.RangeType = "Exempt";
					clsDRWrapper clsLoad = new clsDRWrapper();
					clsLoad.OpenRecordset("select * from exemptcode order by description,code", "RealEstate");
					string strSpecificList = "";
					string strSeparator = "";
					string strReturn = "";
					strSeparator = "";
					strTemp = "";
					strSpecificList = "";
					while (!clsLoad.EndOfFile())
					{
						// strTemp = strTemp & strSeparator & "#" & Val(clsLoad.Fields("code")) & ";" & clsLoad.Fields("description") & vbTab & Val(clsLoad.Fields("code")) & "|"
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						strSpecificList += strSeparator + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + ";" + clsLoad.Get_Fields_String("description");
						strSeparator = "|";
						clsLoad.MoveNext();
					}
					strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strSpecificList, "Exempt Codes", "", true);
					if (strReturn == "")
					{
						return;
					}
					ebaReport.ExemptionsToUse = strReturn;
				}
				frmWait.InstancePtr.Init("Loading records");
				ebaController.FillReport(ref ebaReport);
				frmWait.InstancePtr.Unload();
				if (ebaReport.ExemptionRecords.ItemCount() > 0)
				{
					rptExemptionsByAccount.InstancePtr.Init(ref ebaReport);
				}
				else
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			if (intChoice == 22)
			{
				Inputhighestassess:
				;
				strResponse = Interaction.InputBox("Enter the number of highest assessments to show" + "\r\n" + "Valid numbers are from 10 to 1000", "Highest Assessments", null/*, -1, -1*/);
				if (strResponse == string.Empty)
					return;
				if (Conversion.Val(strResponse) < 10 || Conversion.Val(strResponse) > 1000)
					goto Inputhighestassess;
				modGlobalVariables.Statics.CancelledIt = false;
				//! Load frmWhichExemptions;
				frmWhichExemptions.InstancePtr.Text = "Total or Taxable";
				frmWhichExemptions.InstancePtr.Label1.Text = "Do you want to use total value or total taxable value?";
				frmWhichExemptions.InstancePtr.Label2.Text = "Total value ignores exemptions.  Total taxable value is after exemptions are subtracted.";
				//frmWhichExemptions.InstancePtr.Frame1.Text = "Use";
				frmWhichExemptions.InstancePtr.cmbtBilOrCor.Clear();
				frmWhichExemptions.InstancePtr.cmbtBilOrCor.Items.Add("Taxable");
				frmWhichExemptions.InstancePtr.cmbtBilOrCor.Items.Add("Total");
				//FC:FINAL:RPU:#i1314 - Select the first item
				frmWhichExemptions.InstancePtr.cmbtBilOrCor.SelectedIndex = 0;
				frmWhichExemptions.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (modGlobalVariables.Statics.CancelledIt)
					return;
				boolTotTaxable = modGlobalVariables.Statics.boolBillingorCorrelated;
				//! Load frmWhichAssessment;
				frmWhichAssessment.InstancePtr.Text = "Billing or Current";
				frmWhichAssessment.InstancePtr.Label1.Text = "Do you want highest valuations based on current or billing amounts?";
				frmWhichExemptions.InstancePtr.cmbtBilOrCor.Clear();
				frmWhichExemptions.InstancePtr.cmbtBilOrCor.Items.Add("Current");
				frmWhichAssessment.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (modGlobalVariables.Statics.CancelledIt)
					return;
				rptHighestAssessment.InstancePtr.InitHighestAssessment(FCConvert.ToInt16(Conversion.Val(strResponse)), cmbOrder.Text == cmbOrder_Item0_Text, ref modGlobalVariables.Statics.boolBillingorCorrelated, ref boolTotTaxable);
				// Unload Me
				return;
			}
			if (intChoice == 30)
			{
				rptREPPAssociation.InstancePtr.Init(cmbOrder.Text == cmbOrder_Item1_Text);
			}
			if (!modREMain.Statics.boolShortRealEstate)
			{
				if (intChoice == 2 || intChoice == 4 || intChoice == 5 || intChoice == 27)
				{
					frmWhichAssessment.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					if (!modGlobalVariables.Statics.boolBillingorCorrelated)
					{
						clsDRWrapper rsLoad = new clsDRWrapper();
						rsLoad.OpenRecordset("select * from status", modGlobalVariables.strREDatabase);
						if (!rsLoad.EndOfFile())
						{
							if (Information.IsDate(rsLoad.Get_Fields("currentexemptions")))
							{
								if (rsLoad.Get_Fields_DateTime("currentexemptions").ToOADate() != 0)
								{
									bool boolAsk = false;
									boolAsk = false;
									if (Information.IsDate(rsLoad.Get_Fields("Batchcalc")))
									{
										if (rsLoad.Get_Fields_DateTime("batchcalc").ToOADate() != 0)
										{
											if (DateAndTime.DateDiff("n", (DateTime)rsLoad.Get_Fields_DateTime("batchcalc"), (DateTime)rsLoad.Get_Fields_DateTime("currentexemptions")) < 0)
											{
												boolAsk = true;
											}
										}
									}
									if (Information.IsDate(rsLoad.Get_Fields("calculated")))
									{
										if (rsLoad.Get_Fields_DateTime("calculated").ToOADate() != 0)
										{
											if (DateAndTime.DateDiff("n", (DateTime)rsLoad.Get_Fields_DateTime("calculated"), (DateTime)rsLoad.Get_Fields_DateTime("currentexemptions")) < 0)
											{
												boolAsk = true;
											}
										}
									}
									if (boolAsk)
									{
										if (MessageBox.Show("Current exemptions have not been recalculated since account(s) have been recalculated" + "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
										{
											return;
										}
									}
								}
							}
							else
							{
								if (MessageBox.Show("Current exemptions have not been recalculated since account(s) have been recalculated" + "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
								{
									return;
								}
							}
						}
					}
				}
			}
			if (intChoice == 0)
			{
				rptInputCard.InstancePtr.Start(cmbPrint.Text == "From Extract");
			}
			else if (intChoice == 1)
			{
				rptListingNameLocMap.InstancePtr.Start(cmbPrint.Text == "From Extract", cmbInfo.Text == "By Account");
			}
			else if (intChoice == 2)
			{
				rptListingNameLocMapAsses.InstancePtr.Start(cmbPrint.Text == "From Extract", cmbInfo.Text == "By Account");
			}
			else if (intChoice == 3)
			{
				rptListingNameAddrLocMap.InstancePtr.Start(cmbPrint.Text == "From Extract", cmbInfo.Text == "By Account");
			}
			else if (intChoice == 4)
			{
				rptListingNameAddrLocMapAsses.InstancePtr.Start(cmbPrint.Text == "From Extract", cmbInfo.Text == "By Account");
			}
			else if (intChoice == 5)
			{
				rptListingNameAddrLocMapAssesRef.InstancePtr.Start(cmbPrint.Text == "From Extract", cmbInfo.Text == "By Account");
			}
			else if (intChoice == 6)
			{
				rptExtractReport.InstancePtr.Start();
			}
			else if (intChoice == 7)
			{
				// frmCustomReport.Show
				// frmReportViewer.Show
				frmCustomReport.InstancePtr.Start(ref modGlobalVariables.Statics.boolRange, cmbPrint.Text == "From Extract");
				// Call dlgCustom.Start(boolRange, optprint(2).Value)
				// Call rptCustomReport.Start(boolRange, optprint(2).Value)
			}
			else if (intChoice == 13)
			{
				strTemp = "Property Cards will print with the following options:" + "\r\n" + "\r\n";
				strTemp += "Always show current year billing values = ";
				if (modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentOnPropertyCard)
				{
					strTemp += "True";
				}
				else
				{
					strTemp += "False";
				}
				strTemp += "\r\n" + "Always show current year current values = ";
				if (modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentCurrentOnPropertyCard)
				{
					strTemp += "True";
				}
				else
				{
					strTemp += "False";
				}
				strTemp += "\r\n" + "Show previous assessments = ";
				if (modGlobalVariables.Statics.CustomizedInfo.boolNeverShowPreviousBillingOnPropertyCard)
				{
					strTemp += "False";
				}
				else
				{
					strTemp += "True";
				}
				strTemp += "\r\n" + "Show Comments = ";
				if (modGlobalVariables.Statics.CustomizedInfo.boolDontShowCommentsOnPropertyCard)
				{
					strTemp += "False";
				}
				else
				{
					strTemp += "True";
				}
				strTemp += "\r\n" + "\r\n";
				strTemp += "If these parameters are incorrect, choose cancel and go to the customize screen to change them.";
				if (MessageBox.Show(strTemp, "Property Card", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.Cancel)
				{
					return;
				}
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptPropertyCard.InstancePtr.Init(modGlobalVariables.Statics.boolRange, x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract");
				}
				else
				{
					rptPropertyCard.InstancePtr.Init(modGlobalVariables.Statics.boolRange, x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract");
				}
			}
			else if (intChoice == 39)
			{
				bool boolPreview = false;
				boolPreview = MessageBox.Show("Would you like to preview the pages?", "View Pages?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
				rptpdfPropertyCard.InstancePtr.Unload();
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptpdfPropertyCard.InstancePtr.Init(modGlobalVariables.Statics.boolRange, x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract", false, boolPreview);
				}
				else
				{
					rptpdfPropertyCard.InstancePtr.Init(modGlobalVariables.Statics.boolRange, x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract", false, boolPreview);
				}
			}
			else if (intChoice == 16)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptComment.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract");
				}
				else
				{
					rptComment.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract");
				}
			}
			else if (intChoice == 17)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptTreeGrowthBreakdown.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract");
				}
				else
				{
					rptTreeGrowthBreakdown.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract");
				}
			}
			else if (intChoice == 19)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptPictures.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract");
				}
				else
				{
					rptPictures.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract");
				}
			}
			else if (intChoice == 20)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptPictures.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract", true, chkBldgValue.CheckState == Wisej.Web.CheckState.Checked);
				}
				else
				{
					rptPictures.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract", true, chkBldgValue.CheckState == Wisej.Web.CheckState.Checked);
				}
			}
			else if (intChoice == 27)
			{
				rptListingNameAddrLocMapAssesRef.InstancePtr.Start(cmbPrint.Text == "From Extract", cmbInfo.Text == "By Account", true);
			}
			else if (intChoice == 31)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptMapLotHistory.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange));
				}
				else
				{
					rptMapLotHistory.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange);
				}
			}
			else if (intChoice == 32)
			{
				if (chkArchiveEntries.CheckState == Wisej.Web.CheckState.Checked)
				{
					rptAuditArchiveReport.InstancePtr.Init("Select * from AuditChanges order by dateupdated,timeupdated", this.Modal, true, "Changes Audit");
				}
				else
				{
					rptAuditArchiveReport.InstancePtr.Init("Select * from AuditChanges order by dateupdated,timeupdated", this.Modal, false, "Changes Audit");
				}
			}
			else if (intChoice == 33)
			{
				frmChangesAuditReport.InstancePtr.Show(App.MainForm);
			}
			else if (intChoice == 34)
			{
				rptSketches.InstancePtr.Init(false, false, x);
			}
			else if (intChoice == 35)
			{
				rptSketches.InstancePtr.Init(true, chkBldgValue.CheckState == Wisej.Web.CheckState.Checked, x);
			}
			else if (intChoice == 36)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptRevocableLivingTrust.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract");
				}
				else
				{
					rptRevocableLivingTrust.InstancePtr.Init(ref modGlobalVariables.Statics.boolRange, ref x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract");
				}
			}
			else if (intChoice == 37)
			{
				if (cmbOrder.Text == cmbOrder_Item0_Text)
				{
					rptInterestedParty.InstancePtr.Init(modGlobalVariables.Statics.boolRange, x, Conversion.Str(modGlobalVariables.Statics.gintMinAccountRange), Conversion.Str(modGlobalVariables.Statics.gintMaxAccountRange), cmbPrint.Text == "From Extract");
				}
				else
				{
					rptInterestedParty.InstancePtr.Init(modGlobalVariables.Statics.boolRange, x, modPrintRoutines.Statics.gstrMinAccountRange, modPrintRoutines.Statics.gstrMaxAccountRange, cmbPrint.Text == "From Extract");
				}
			}
            else if (intChoice == 41)
            {
                string subTitle = "";
                
                var query = new REListingQuery();
                query.ByRange = modGlobalVariables.Statics.boolRange;
                query.FromExtract = (cmbPrint.Text == "From Extract" && cmbPrint.Items.Contains("From Extract"));
                if (query.ByRange)
                {
                    if (cmbOrder.Text == cmbOrder_Item0_Text)
                    {
                        query.MinimumAccount = modGlobalVariables.Statics.gintMinAccountRange;
                        query.MaximumAccount = modGlobalVariables.Statics.gintMaxAccountRange;
                    }
                    else
                    {
                        query.RangeStart = modPrintRoutines.Statics.gstrMinAccountRange;
                        query.RangeEnd = modPrintRoutines.Statics.gstrMaxAccountRange;
                    }
                }
                switch (cmbOrder.Text.ToLower())
                {
                    case "account":
                        query.FieldForOrdering = REListingField.Account;
                        subTitle = "Account List by Account";
                        break;
                    case "name":
                        query.FieldForOrdering = REListingField.Name;
                        subTitle = "Account List by Name";
                        break;
                    case @"map/lot":
                        query.FieldForOrdering = REListingField.Map;
                        subTitle = @"Account List by Map/Lot";
                        break;
                }

                if (query.FromExtract)
                {
                    var lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                    var rsLoad = new clsDRWrapper();
                    rsLoad.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + lngUID,
                        "RealEstate");
                    if (!rsLoad.EndOfFile())
                    {
                        if (!String.IsNullOrWhiteSpace(rsLoad.Get_Fields_String("Title")))
                        {
                            subTitle = rsLoad.Get_Fields_String("Title");
                        }
                    }
                }
                var queryHandler = new REPropertyBriefsQueryHandler();
                var properties = queryHandler.ExecuteQuery(query);
                var deedNameReport = new rptDeedNames();
                deedNameReport.Init(subTitle,properties);
            }
			if (!(intChoice == 7))
			{
				modGlobalVariables.Statics.boolRange = false;
			}
			// Unload Me
		}

		private void chkShowDeleted_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkShowDeleted.CheckState == Wisej.Web.CheckState.Checked)
			{
				t2kDeletedSince.Visible = true;
			}
			else
			{
				t2kDeletedSince.Visible = false;
			}
		}

		private void frmListing_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuPrintPreview_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmListing_Load(object sender, System.EventArgs e)
		{
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			strTemp = modRegistry.GetRegistryKey("REReportRangeInclusive");
			if (Strings.Trim(strTemp) != string.Empty)
			{
				if (FCConvert.CBool(strTemp))
				{
					chkInclusive.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkInclusive.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkInclusive.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			SetChkInclusiveToolTip();
			FillGrid();
            //FC:FINAL:BSE #1814 adjust frame top to remove empty space
            adjustFramePosition = true;

            txtAccount.AllowOnlyNumericInput();
        }

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			string selectedText_cmbOrder = cmbOrder.Text;
			int lngRow;
			int Index;
			lngRow = Grid.Row;
			if (lngRow < 0)
				return;
			int x;
			Index = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, 0))));
			t2kDateEnd.Visible = false;
			t2kDateStart.Visible = false;
			lblEndDate.Visible = false;
			lblStartDate.Visible = false;
			framComparison.Visible = false;
			//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
			lblOrder.Visible = true;
			cmbOrder.Visible = true;
			//cmbOrder.Text = "Order By";
			chkArchiveEntries.Visible = false;
			chkBldgValue.Visible = false;
			if (Index != 40)
            {
                //FC:FINAL:CHN - issue 1685: Fix ComboBox working after redesign from RadioButtons.
                var oldValue = cmbPrint.Text as string;
                cmbPrint.Clear();
                cmbPrint.Items.AddRange(new string[] { "All Accounts",
                "Range",
                "From Extract",
                "Single"});
                if (cmbPrint.Items.Contains(oldValue))
                {
                    cmbPrint.SelectedItem = oldValue;
                }
                else
                {
                    cmbPrint.SelectedIndex = 0;
                }
            }
			//if (Index==7) cmbOrder.Text = "Range Of";
			if ((Index < 1) || (Index > 5) || (Index == 18) || (Index == 19) || (Index == 20) || (Index == 21) || (Index == 22) || (Index == 23) || Index == 31 || Index == 36 || Index == 38 || Index == 40)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
			}
			else
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = true;
				cmbInfo.Visible = true;
			}
			if (Index == 6)
			{
				Frame5.Visible = false;
				cmbPrint.Text = "From Extract";
				cmbPrint.Enabled = false;
				Frame1.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
				cmbOrder.Items.Add(cmbOrder_Item1_Text);
				cmbOrder.Items.Add(cmbOrder_Item2_Text);
				cmbOrder.Items.Add(cmbOrder_Item3_Text);
			}
			else if (Index < 8 || Index == 13 || Index == 16 || Index == 17 || Index == 27 || Index == 36 || Index == 37 || Index == 39)
			{
				Frame5.Visible = false;
				Frame1.Visible = true;
				cmbPrint.Enabled = true;
				cmbOrder.Clear();
				cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
                cmbOrder.Items.Add(cmbOrder_Item3_Text);
            }
			else if (Index == 8)
			{
				Frame5.Visible = true;
				Frame1.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Date";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Account";
                cmbOrder_Item3_Text = "Location";
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
				cmbOrder.Items.Add(cmbOrder_Item1_Text);
				if (cmbOrder.Text == "Location")
					selectedText_cmbOrder = cmbOrder.Text = "Account";
				if (cmbPrevious.Text == "Account")
				{
					if (cmbOrder.Text == "Map/Lot")
						selectedText_cmbOrder = cmbOrder.Text = "Account";
				}
				else
				{
					cmbOrder.Items.Add(cmbOrder_Item2_Text);
				}
			}
			else if (Index == 11 || Index == 12)
			{
				chkShowDeleted.Visible = false;
				t2kDeletedSince.Visible = false;
				Frame5.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				Frame1.Visible = true;
				cmbPrint.Enabled = true;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Map/Lot";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
				framComparison.Visible = true;
				if (Index == 11)
				{
					chkShowDeleted.Visible = true;
					if (chkShowDeleted.CheckState == Wisej.Web.CheckState.Checked)
					{
						t2kDeletedSince.Visible = true;
					}
				}
			}
			else if (Index == 19 || Index == 20)
			{
				chkShowDeleted.Visible = false;
				t2kDeletedSince.Visible = false;
				Frame5.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Map/Lot";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
				framComparison.Visible = false;
				if (Index == 20)
				{
					chkBldgValue.Visible = true;
				}
			}
			else if (Index == 21)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame1.Visible = false;
				Frame5.Visible = false;
				framComparison.Visible = false;
				t2kDateEnd.Visible = true;
				t2kDateStart.Visible = true;
				lblEndDate.Visible = true;
				lblStartDate.Visible = true;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
                cmbOrder.Items.Add(cmbOrder_Item3_Text);
			}
			else if (Index == 22 || Index == 30)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame1.Visible = false;
				Frame5.Visible = false;
				framComparison.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
				if (cmbOrder.Text == "Map/Lot" || (cmbOrder.Text == "Location"))
					selectedText_cmbOrder = cmbOrder.Text = "Account";
			}
			else if (Index == 23 || Index == 25 || Index == 26)
			{
				Frame1.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = false;
				cmbOrder.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame5.Visible = false;
			}
			else if (Index == 24)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame1.Visible = false;
				Frame5.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
                cmbOrder.Items.Add(cmbOrder_Item3_Text);
			}
			else if (Index == 28 || Index == 29)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame1.Visible = false;
				Frame5.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
                cmbOrder.Items.Add(cmbOrder_Item3_Text);
			}
			else if (Index == 31)
			{
				Frame5.Visible = false;
				Frame1.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
			}
			else if (Index == 32 || Index == 33)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame1.Visible = false;
				Frame5.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = false;
				cmbOrder.Visible = false;
				if (Index == 32)
				{
					chkArchiveEntries.Visible = true;
				}
			}
			else if (Index == 35 || Index == 34)
			{
				chkShowDeleted.Visible = false;
				t2kDeletedSince.Visible = false;
				Frame5.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				Frame1.Visible = false;
				cmbPrint.Enabled = true;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Map/Lot";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
				if (Index == 35)
				{
					chkBldgValue.Visible = true;
				}
			}
			else if (Index == 38)
			{
				Frame5.Visible = false;
				Frame1.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Date";
                cmbOrder_Item1_Text = "Type";
                cmbOrder_Item2_Text = "Account";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
			}
			else if (Index == 40)
			{
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = true;
				cmbOrder.Visible = true;
				Frame1.Visible = true;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbInfo
				lblInfo.Visible = false;
				cmbInfo.Visible = false;
				Frame5.Visible = false;
				chkInclusive.Visible = false;
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
				selectedText_cmbOrder = cmbOrder.Text = "Account";
				if (!cmbPrint.Items.Contains("Select Exemptions"))
				{
					cmbPrint.Items.Add("Select Exemptions");
				}
				if (cmbPrint.Text == "From Extract")
					cmbPrint.Text = "All Accounts";
				cmbPrint.Items.Remove("From Extract");
			}
            else if (Index == 41)
            {
                lblOrder.Visible = true;
                cmbOrder.Visible = true;
                Frame1.Visible = true;
                lblInfo.Visible = false;
                cmbInfo.Visible = false;
                Frame5.Visible = false;
                chkInclusive.Visible = false;
                cmbOrder.Clear();
                cmbOrder_Item0_Text = "Account";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                //FC:FINAL:MSH - i.issue #1173: restore missing items
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
            }
			else if (Index > 8)
			{
				Frame5.Visible = false;
				Frame1.Visible = false;
				//FC:FINAL:MSH - i.issue #1166: change visibility of label to visibility of cmbOrder
				lblOrder.Visible = false;
				cmbOrder.Visible = false;
			}
			//FC:FINAL:CHN - issue #1646: Add restoring values from input fields (Fix difference between Web ComboBox and original app RadioButtons).
			var txtFromValue = txtFrom.Text;
			var txtToValue = txtTo.Text;
			cmbOrder.Text = selectedText_cmbOrder;
			if (cmbOrder.SelectedIndex == -1)
			{
				cmbOrder.SelectedIndex = 0;
			}
			txtFrom.Text = txtFromValue;
			txtTo.Text = txtToValue;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			cmdOkay_Click();
		}

		public void mnuPrintPreview_Click()
		{
			mnuPrintPreview_Click(cmdPrintPreview, new System.EventArgs());
		}

		private void optOrder_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			txtFrom.Text = "";
			txtTo.Text = "";
			if (cmbPrint.Text == "Single")
			{
				lblFrom.Text = cmbOrder.Text;
			}
			if (cmbPrint.Text == "Single" || cmbPrint.Text == "Range")
			{
				if (Index == 0)
				{
					chkInclusive.Visible = false;
				}
				else
				{
					chkInclusive.Visible = true;
				}
			}
			SetChkInclusiveToolTip();
		}

		private void optOrder_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbOrder.SelectedIndex;
			optOrder_CheckedChanged(index, sender, e);
		}

		private void optPrevious_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			string selectedText_cmbOrder = cmbOrder.Text;
			if (Index == 0)
			{
				// account
				if (Information.IsDate(txtAccount.Text))
					txtAccount.Text = "";
				if (cmbOrder.Text == "Map/Lot")
					selectedText_cmbOrder = cmbOrder.Text = "Account";
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Date";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Map/Lot";
                cmbOrder_Item3_Text = "Location";
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
				txtDate.Visible = false;
				lblstart.Visible = false;
				lblend.Visible = false;
			}
			else if (Index == 1)
			{
				cmbOrder.Clear();
                cmbOrder_Item0_Text = "Date";
                cmbOrder_Item1_Text = "Name";
                cmbOrder_Item2_Text = "Account";
                cmbOrder_Item3_Text = "Location";
                cmbOrder.Items.Add(cmbOrder_Item0_Text);
                cmbOrder.Items.Add(cmbOrder_Item1_Text);
                cmbOrder.Items.Add(cmbOrder_Item2_Text);
				txtDate.Visible = true;
				lblstart.Visible = true;
				lblend.Visible = true;
			}
			cmbOrder.Text = selectedText_cmbOrder;
			if (cmbOrder.SelectedIndex == -1)
			{
				cmbOrder.SelectedIndex = 0;
			}
		}

		private void optPrevious_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbPrevious.SelectedIndex;
			optPrevious_CheckedChanged(index, sender, e);
		}

		private void SetChkInclusiveToolTip()
		{
			string strTemp;
			strTemp = ToolTip1.GetToolTip(chkInclusive);
			if (cmbPrint.Text == "Range")
			{
				// range
				if (cmbOrder.Text == cmbOrder_Item1_Text)
				{
					// name
					strTemp = "If option is checked a range of a to b would also include all names starting with b. If unchecked the range is strictly names between a and b.";
				}
				else if (cmbOrder.Text == cmbOrder_Item2_Text)
				{
					// map lot
					strTemp = "If option is checked a range of 001 to 002 would also include maps starting with 002. If unchecked the range is strictly maps between 001 and 002.";
				}
				else if (cmbOrder.Text == cmbOrder_Item3_Text)
				{
					// location
					strTemp = "If option is checked a range of a to b would also include all locations starting with b. If unchecked the range is strictly locations between a and b.";
				}
			}
			else
			{
				// single
				if (cmbOrder.Text == cmbOrder_Item1_Text)
				{
					// name
					strTemp = "If option is checked and you entered Gray for example, records will include any names starting with Gray. If unchecked only records with Gray as the entire name will be loaded";
				}
				else if (cmbOrder.Text == cmbOrder_Item2_Text)
				{
					// map lot
					strTemp = "If option is checked and you entered 001 for example, records will include any maps that start with 001.  If unchecked only records with a maplot of exactly 001 will be loaded";
				}
				else if (cmbOrder.Text == cmbOrder_Item3_Text)
				{
					// location
					strTemp = "If option is checked and you entered Main for example, records will include any locations that start with Main (Main, Main Street etc.) If unchecked only locations of exactly Main will be loaded (I.E. Not Main st.)";
				}
			}
			ToolTip1.SetToolTip(chkInclusive, strTemp);
		}

		private void optPrint_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 1:
					{
						// range
						lblTo.Visible = true;
						lblFrom.Visible = true;
						txtTo.Visible = true;
						txtFrom.Visible = true;
						lblTo.Text = "To";
						lblFrom.Text = "From";
						chkInclusive.Visible = true;
						chkInclusive.Text = "Records start with";
						if (cmbOrder.Text == cmbOrder_Item0_Text)
						{
							chkInclusive.Visible = false;
						}
						break;
					}
				case 3:
					{
						// individual
						txtTo.Visible = false;
						lblTo.Visible = false;
						lblFrom.Visible = true;
						txtFrom.Visible = true;
						chkInclusive.Visible = true;
						if (cmbOrder.Text == cmbOrder_Item0_Text)
						{
							lblFrom.Text = cmbOrder.Text;
							chkInclusive.Visible = false;
						}
						else if (cmbOrder.Text == cmbOrder_Item1_Text)
						{
							lblFrom.Text = cmbOrder.Text;
						}
						else if (cmbOrder.Text == cmbOrder_Item2_Text)
						{
							lblFrom.Text = cmbOrder.Text;
						}
						else if (cmbOrder.Text == cmbOrder_Item3_Text)
						{
							lblFrom.Text = cmbOrder.Text;
						}
						chkInclusive.Text = "Records start with";
						break;
					}
				default:
					{
						lblTo.Visible = false;
						lblFrom.Visible = false;
						txtTo.Visible = false;
						txtFrom.Visible = false;
						chkInclusive.Visible = false;
						break;
					}
			}
			//end switch
			// 
			SetChkInclusiveToolTip();
		}

		private void optPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbPrint.SelectedIndex;
			//FC:FINAL:MSH - i.issue #1435: in original app if selected 'Exemptions By Account' will be displayed 5 print variants, one of which 
			// will be disabled. In this case returned index will be incorrect, because in web app displayed only 4 items
			int tempFormatChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, 0))));
			if (tempFormatChoice == 40)
			{
				if (index >= 2)
				{
					index++;
				}
			}
			optPrint_CheckedChanged(index, sender, e);
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			if (cmbPrevious.Text == "Date Range")
			{
				if (txtAccount.Text == "")
					txtAccount.Text = "00/00/0000";
				txtAccount.SelectionStart = 0;
				txtAccount.SelectionLength = 1;
			}
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (cmbPrevious.Text == "Date Range")
			{
				if (KeyCode == Keys.Left)
				{
					txtAccount.SelectionLength = 0;
					if (txtAccount.SelectionLength == 1)
						txtAccount.SelectionStart = txtAccount.SelectionStart - 1;
					if (txtAccount.SelectionStart == 3)
						txtAccount.SelectionStart = 2;
					if (txtAccount.SelectionStart == 6)
						txtAccount.SelectionStart = 5;
				}
				else if (KeyCode == Keys.Right)
				{
					txtAccount.SelectionLength = 0;
					if (txtAccount.SelectionLength == 1)
						txtAccount.SelectionStart = txtAccount.SelectionStart - 1;
					if (KeyCode == Keys.Right && txtAccount.SelectionStart == 0 && Information.Err().Number != 0)
					{
						KeyCode = (Keys)0;
						txtAccount.SelectionStart = 1;
					}
				}
			}
		}

		private void txtAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbPrevious.Text == "Date Range")
			{
				if (KeyAscii == Keys.Back)
				{
					KeyAscii = (Keys)0;
					txtAccount.SelectionLength = 0;
					txtAccount.SelectionStart = txtAccount.SelectionStart - 1;
					if (txtAccount.SelectionStart == 2)
						txtAccount.SelectionStart = 1;
					if (txtAccount.SelectionStart == 5)
						txtAccount.SelectionStart = 4;
					return;
				}
				if (Information.IsNumeric(FCConvert.ToString(Convert.ToChar(KeyAscii))) == false)
				{
					KeyAscii = (Keys)0;
					Interaction.Beep();
					return;
				}
				if (txtAccount.SelectionStart == 2)
					txtAccount.SelectionStart = 3;
				if (txtAccount.SelectionStart == 5)
					txtAccount.SelectionStart = 6;
				txtAccount.SelectionLength = 1;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAccount_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (cmbPrevious.Text == "Date Range")
			{
				if (txtAccount.Text == "")
				{
					txtAccount.Text = "00/00/0000";
					txtAccount.SelectionStart = 0;
				}
				else if (txtAccount.Text.Length != 10)
				{
					txtAccount.Text = "00/00/0000";
					txtAccount.SelectionStart = 0;
				}
				else if (txtAccount.SelectionStart == 10)
				{
					if (Information.IsDate(txtAccount.Text) == true)
						return;
					if (txtAccount.Text == "00/00/0000")
						return;
					MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtAccount.SelectionStart = 0;
				}
				else if (KeyCode != Keys.Left)
				{
					if (txtAccount.SelectionStart == 2)
						txtAccount.SelectionStart = 3;
					if (txtAccount.SelectionStart == 5)
						txtAccount.SelectionStart = 6;
				}
				txtAccount.SelectionLength = 1;
			}
		}

		private void FillGrid()
		{
			int lngRow = 0;
			int intCounter;
			bool boolShow = false;
			Grid.ColHidden(0, true);
			Grid.Rows = 0;
			for (intCounter = 0; intCounter <= 39; intCounter++)
			{
				Grid.Rows += 1;
				boolShow = true;
				lngRow = Grid.Rows - 1;
				switch (intCounter)
				{
					case 0:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(0));
							Grid.TextMatrix(lngRow, 1, "Input Card");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 1:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(1));
							Grid.TextMatrix(lngRow, 1, "Name/Location/Map Lot");
							break;
						}
					case 2:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(2));
							Grid.TextMatrix(lngRow, 1, "Name/Location/Map Lot/Assessment");
							break;
						}
					case 3:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(3));
							Grid.TextMatrix(lngRow, 1, "Name/Address/Location/Map Lot");
							break;
						}
					case 4:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(4));
							Grid.TextMatrix(lngRow, 1, "Name/Address/Location/Map Lot/Assessment");
							break;
						}
					case 5:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(5));
							Grid.TextMatrix(lngRow, 1, "Name/Address/Location/Map Lot/Assessment/Reference");
							break;
						}
					case 6:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(27));
							Grid.TextMatrix(lngRow, 1, "Name/Address/Location/Map Lot/Assessment/Book Page");
							break;
						}
					case 7:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(6));
							Grid.TextMatrix(lngRow, 1, "Extract Report");
							break;
						}
					case 8:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(7));
							Grid.TextMatrix(lngRow, 1, "Custom Report");
							break;
						}
					case 9:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(25));
							Grid.TextMatrix(lngRow, 1, "Assessment Analysis");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 10:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(26));
							Grid.TextMatrix(lngRow, 1, "Assessment by Code");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 11:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(8));
							Grid.TextMatrix(lngRow, 1, "Previous Owner List");
							break;
						}
					case 12:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(37));
							Grid.TextMatrix(lngRow, 1, "Interested Parties (Multiple Owners)");
							break;
						}
					case 13:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(9));
							Grid.TextMatrix(lngRow, 1, "Audit of Billing Amounts");
							break;
						}
					case 14:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(10));
							Grid.TextMatrix(lngRow, 1, "RE/PP Audit Summary");
							break;
						}
					case 15:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(11));
							Grid.TextMatrix(lngRow, 1, "Assessed Values Comparison");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 16:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(12));
							Grid.TextMatrix(lngRow, 1, "Assessment Comparison");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 17:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(16));
							Grid.TextMatrix(lngRow, 1, "Comments");
							break;
						}
					case 18:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(17));
							Grid.TextMatrix(lngRow, 1, "Tree Growth Breakdown");
							break;
						}
					case 19:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(18));
							Grid.TextMatrix(lngRow, 1, "Duplicate Map/Lots");
							break;
						}
					case 20:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(13));
							Grid.TextMatrix(lngRow, 1, "Property Card");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 21:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(19));
							Grid.TextMatrix(lngRow, 1, "Picture Report");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 22:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(20));
							Grid.TextMatrix(lngRow, 1, "No Picture Report");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 23:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(34));
							Grid.TextMatrix(lngRow, 1, "Sketches Report");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 24:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(35));
							Grid.TextMatrix(lngRow, 1, "No Sketches Report");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 25:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(21));
							Grid.TextMatrix(lngRow, 1, "Deleted Accounts");
							break;
						}
					case 26:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(22));
							Grid.TextMatrix(lngRow, 1, "Highest Assessments");
							break;
						}
					case 27:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(23));
							Grid.TextMatrix(lngRow, 1, "Zone/Neighborhood Comparison");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 28:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(24));
							Grid.TextMatrix(lngRow, 1, "Tax Notices");
							break;
						}
					case 29:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(28));
							Grid.TextMatrix(lngRow, 1, "Tax Acquired");
							break;
						}
					case 30:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(29));
							Grid.TextMatrix(lngRow, 1, "In Bankruptcy");
							break;
						}
					case 31:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(36));
							Grid.TextMatrix(lngRow, 1, "Revocable Living Trusts");
							break;
						}
					case 32:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(30));
							Grid.TextMatrix(lngRow, 1, "RE/PP Associations");
							break;
						}
					case 33:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(31));
							Grid.TextMatrix(lngRow, 1, "Map/Lot History");
							break;
						}
					case 34:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(32));
							Grid.TextMatrix(lngRow, 1, "Changes Audit");
							break;
						}
					case 35:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(33));
							Grid.TextMatrix(lngRow, 1, "Changes Audit Archive");
							break;
						}
					case 36:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(38));
							Grid.TextMatrix(lngRow, 1, "Duplicate Records");
							break;
						}
					case 37:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(39));
							Grid.TextMatrix(lngRow, 1, "Property PDF");
							if (modREMain.Statics.boolShortRealEstate)
								boolShow = false;
							break;
						}
					case 38:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(40));
							Grid.TextMatrix(lngRow, 1, "Exemptions By Account");
							break;
						}
                    case 39:
                    {
                        Grid.TextMatrix(lngRow, 0, "41");
                        Grid.TextMatrix(lngRow, 1, "Deed Names");
                        break;
                    }
				}
				//end switch
				if (!boolShow)
				{
					Grid.RemoveItem(lngRow);
				}
			}
			// intCounter
			Grid.Row = 1;
			Grid.Col = 1;
		}
        //FC:FINAL:BSE #1814 adjust frame top to remove empty space 
        private bool adjustFramePosition = false;
         private void Frame1_VisibleChanged(object sender, EventArgs e)
        {
            if (! adjustFramePosition)
            {
                return;
            }

            if (Frame1.Visible)
            {
                Frame2.Top += Frame1.Height;
                cmbOrder.Top += Frame1.Height;
                lblOrder.Top += Frame1.Height;
            }
            else
            {
                Frame2.Top = Frame2.Top - Frame1.Height;
                cmbOrder.Top = cmbOrder.Top - Frame1.Height;
                lblOrder.Top = lblOrder.Top - Frame1.Height;
            }
        }
        
	}
}
