﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptZoneSchedule.
	/// </summary>
	public partial class rptZoneSchedule : BaseSectionReport
	{
		public static rptZoneSchedule InstancePtr
		{
			get
			{
				return (rptZoneSchedule)Sys.GetInstance(typeof(rptZoneSchedule));
			}
		}

		protected rptZoneSchedule _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptZoneSchedule()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Zone Schedule";
		}
		// nObj = 1
		//   0	rptZoneSchedule	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolFromForm;
		const int CNSTGRIDZONECOLAUTOID = 0;
		const int CNSTGRIDZONECOLCODE = 1;
		const int CNSTGRIDZONECOLDESCRIPTION = 2;
		const int CNSTGRIDZONECOLSHORTDESCRIPTION = 3;
		const int CNSTGRIDZONECOLSECDESCRIPTION = 4;
		const int CNSTGRIDZONECOLSECSHORT = 5;
		const int CNSTGRIDZONECOLTOWNCODE = 6;
		const int CNSTGRIDNEIGHCOLAUTOID = 0;
		const int CNSTGRIDNEIGHCOLCODE = 2;
		const int CNSTGRIDNEIGHCOLDESC = 1;
		const int CNSTGRIDNEIGHCOLLANDFACTOR = 3;
		const int CNSTGRIDNEIGHCOLBLDGFACTOR = 4;
		const int CNSTGRIDNEIGHCOLSCHEDULE = 5;
		const int CNSTGRIDNEIGHCOLZONE = 6;
		const int CNSTGRIDNEIGHCOLTOWNCODE = 7;
		const int CNSTGRIDNEIGHCOLPARENT = 8;
		private clsDRWrapper rsReport = new clsDRWrapper();
		private int lngRow;
		private int lngMax;
		// Private rsNeigh As New clsDRWrapper
		public void Init(bool boolFromScreen = false)
		{
			boolFromForm = boolFromScreen;
			string strSQL = "";
			if (!boolFromScreen)
			{
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strSQL = "select * from zones inner join (zoneschedule inner join neighborhood on (neighborhood.code = zoneschedule.neighborhood) and (neighborhood.townnumber = zoneschedule.townnumber)) on (zones.code = zoneschedule.zone) and (zones.townnumber = zoneschedule.townnumber) where zones.townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " order by code,neighborhood";
				}
				else
				{
					strSQL = "Select * from ZONES inner join (zoneschedule inner join neighborhood on (neighborhood.code = zoneschedule.neighborhood)) on (zones.code = zoneschedule.zone) order by zones.code,neighborhood.description";
				}
				rsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (rsReport.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				lngMax = frmZones.InstancePtr.GridNeigh.Rows - 1;
				if (lngMax == 0)
				{
					MessageBox.Show("No zones", "No Zones", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				lngRow = 1;
				// If Not CustomizedInfo.boolRegionalTown Then
				// Call rsNeigh.OpenRecordset("select * from neighborhood order by code", strREDatabase)
				// Else
				// Call rsNeigh.OpenRecordset("select * from neighborhood where townnumber = " & CustomizedInfo.CurrentTownNumber & " order by code", strREDatabase)
				// End If
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Zones");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolFromForm)
			{
				eArgs.EOF = rsReport.EndOfFile();
				if (!eArgs.EOF)
				{
					// TODO Get_Fields: Field [zones.code] not found!! (maybe it is an alias?)
					this.Fields["GroupHeader1"].Value = FCConvert.ToString(rsReport.Get_Fields("zones.code"));
				}
			}
			else
			{
				TRYAGAIN:
				;
				eArgs.EOF = lngRow > lngMax;
				if (!eArgs.EOF)
				{
					if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						if (Conversion.Val(frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLTOWNCODE)) != modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber)
						{
							lngRow += 1;
							goto TRYAGAIN;
						}
					}
					this.Fields["GroupHeader1"].Value = frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLZONE);
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				txtMuni.Text = modRegionalTown.GetTownKeyName_2(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			}
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolFromForm)
			{
				if (!rsReport.EndOfFile())
				{
					txtBldgFactor.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_Double("bldgfactor")));
					txtLandFactor.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_Double("landfactor")));
					txtSchedule.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_Int32("schedule")));
					txtneighborhood.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_Int32("neighborhood")));
					// TODO Get_Fields: Field [neighborhood.description] not found!! (maybe it is an alias?)
					txtNeighborhoodDesc.Text = FCConvert.ToString(rsReport.Get_Fields("neighborhood.description"));
					rsReport.MoveNext();
				}
			}
			else
			{
				if (lngRow <= lngMax)
				{
					txtBldgFactor.Text = FCConvert.ToString(Conversion.Val(frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLBLDGFACTOR)));
					txtLandFactor.Text = FCConvert.ToString(Conversion.Val(frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLLANDFACTOR)));
					txtSchedule.Text = FCConvert.ToString(Conversion.Val(frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLSCHEDULE)));
					txtneighborhood.Text = frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE);
					txtNeighborhoodDesc.Text = frmZones.InstancePtr.GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLDESC);
					lngRow += 1;
				}
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtZone.Text = FCConvert.ToString(this.Fields["GroupHeader1"].Value);
			if (!boolFromForm)
			{
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select description from zones where code = " +
                        FCConvert.ToString(Conversion.Val(this.Fields["GroupHeader1"].Value)),
                        modGlobalVariables.strREDatabase);
                    if (!rsLoad.EndOfFile())
                    {
                        txtDescription.Text = FCConvert.ToString(rsLoad.Get_Fields_String("description"));
                    }
                    else
                    {
                        txtDescription.Text = "";
                    }
                }
            }
			else
			{
				int lngZone = 0;
				int lngTemp;
				lngZone = FCConvert.ToInt32(Math.Round(Conversion.Val(this.Fields["GroupHeader1"].Value)));
				if (lngZone > 0)
				{
					txtDescription.Text = "";
					for (lngTemp = 1; lngTemp <= frmZones.InstancePtr.GridZone.Rows - 1; lngTemp++)
					{
						if (Conversion.Val(frmZones.InstancePtr.GridZone.TextMatrix(lngTemp, CNSTGRIDZONECOLCODE)) == lngZone)
						{
							txtDescription.Text = frmZones.InstancePtr.GridZone.TextMatrix(lngTemp, CNSTGRIDZONECOLDESCRIPTION);
							break;
						}
					}
					// lngTemp
				}
				else
				{
					txtDescription.Text = "";
				}
			}
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("GroupHeader1");
			GroupHeader1.DataField = "GroupHeader1";
		}
	}
}
