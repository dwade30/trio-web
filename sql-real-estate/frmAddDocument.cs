﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmAddDocument.
	/// </summary>
	public partial class frmAddDocument : BaseForm
	{
		public frmAddDocument()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
            if (_InstancePtr == null)
                _InstancePtr = this;
		}

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmAddDocument InstancePtr
        {
            get
            {
                return (frmAddDocument)Sys.GetInstance(typeof(frmAddDocument));
            }
        }

        protected frmAddDocument _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           3/25/2008
		// This form will be used to link new documents to accounts.  It will be called
		// using it's init function.  The 2 arguments that need to be passed are the name of the
		// link field in the LinkedDocuments table and the value to be saved into that field
		// ********************************************************
		bool blnSaved;
		// boolean keepign track of if a document was added or not
		
		int referenceId;
		
        private CentralDocumentService docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			// Browse through your directories for an already existing file
			string strCurDir;
			strCurDir = FCFileSystem.Statics.UserDataFolder;
			// save the current directory so we can set it back
			Information.Err().Clear();
			// clear any errors
			// MDIParent.InstancePtr.CommonDialog1.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
			/*? On Error Resume Next  */
			MDIParent.InstancePtr.CommonDialog1.Filter = "All Image Files |*.pdf;*.psd;*.j2k;*.j2c;*.gif;*.jpg;*.pcx;*.wmf;*.wbmp;*.bmp;*.tif;*.tga;*.pgx;*.ras;*.pnm;*.png;*.ico|PDF (*.pdf)|*.pdf|PhotoShop (*.psd)|*.psd|JPEG 2000 (*.j2k)|*.j2k;*.j2c|JPEG (*.jpg)|*.jpg|PCX (*.pcx)|*.pcx|WMF (*.wmf)|*.wmf|Wireless Bitmap (*.wbmp)|*.wbmp|Bitmap (*.bmp)|*.bmp|TIF (*.tif)|*.tif|TGA (*.tga)|*.tga|Gif (*.gif)|*.gif |PGX (*.pgx)|*.pgx|RAS (*.ras)|*.ras|PNM (*.pnm)|*.pnm|PNG (*.png)|*.png|Icon (*.ico)|*.ico";
			// set filter for pdf files
			//FC:FINAL:MSH - i.issue #1466: restore CancelError to prevent selecting file if 'Ok' not pressed
			MDIParent.InstancePtr.CommonDialog1.CancelError = true;
			// .CommonDialog1.FileName = "*.pdf"
			try
			{
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
			}
			catch
			{
			}

            if (Information.Err().Number != 0) return;

            lblFile.Text = MDIParent.InstancePtr.CommonDialog1.FileName;
				
            fcViewerPanel1.LoadFileFromApplication(MDIParent.InstancePtr.CommonDialog1.FileName);
            FCUtils.ApplicationUpdate(fcViewerPanel1);

        }

		private void cmdScan_Click(object sender, System.EventArgs e)
		{
			string strFile;
			strFile = frmScanDocument.InstancePtr.Init();
			// Show the global scan screen
            if (strFile == "") return;

            // if a file is returned then a docuemnt was scanned and saved
            lblFile.Text = strFile;
        }

		private void frmAddDocument_Activated(object sender, System.EventArgs e)
		{
			if (modMDIParent.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmAddDocument_Load(object sender, System.EventArgs e)
		{

			blnSaved = false;

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmAddDocument_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public bool Init( int lngValue)
		{
			bool ret = false;
			referenceId = lngValue;
			this.Show(FCForm.FormShowEnum.Modal);
			// show form
			ret = blnSaved;
			// return whether or not a document was saved
			return ret;
		}
		// vbPorter upgrade warning: strText As string	OnWrite(VB.Label)

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
            if (lblFile.Text != "")
            { // if a file has been selected

                var doc = docService.MakeCentralDocumentFromFile(fcViewerPanel1.FileName, txtDescription.Text.Trim(), "", referenceId, "", "RealEstate",Guid.NewGuid(), fcViewerPanel1.ItemData);
               
                var cmd = new SaveCommand {Document = doc};

                docService.SaveCentralDocument(cmd);

            }
            else
            {
                MessageBox.Show("You must select a document before you may continue.", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
			blnSaved = true;
			// set saved variable to true so the View screen knows to refresh
			this.Unload();
			// return to the view screen
		}

		private void SetCustomFormColors()
		{
			lblFile.ForeColor = Color.Blue;
			// set file label forecolor to blue to make it show up
		}
	}
}
