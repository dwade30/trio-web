﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptAuditTotals.
	/// </summary>
	partial class srptAuditTotals
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptAuditTotals));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCountTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountHom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountHom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountTotBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandHom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandHom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandTotBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgHom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgTotBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgHom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptHom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptTotBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptHom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessHom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessTotBillable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessHom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCountNoVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountHom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountHom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountTotBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandHom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandHom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTotBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgHom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTotBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgHom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptHom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTotBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptHom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessHom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessTotBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessHom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountNoVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.txtCountTotExempt,
				this.txtCountHom,
				this.txtCountExempt,
				this.txtCountBillable,
				this.txtCountHom2,
				this.txtCountTotBillable,
				this.txtCountTotal,
				this.txtLandTotExempt,
				this.txtLandHom,
				this.txtLandExempt,
				this.txtLandBillable,
				this.txtLandHom2,
				this.txtLandTotBillable,
				this.txtLandTotal,
				this.txtBldgTotal,
				this.txtBldgBillable,
				this.txtBldgHom2,
				this.txtBldgTotBillable,
				this.txtBldgTotExempt,
				this.txtBldgHom,
				this.txtBldgExempt,
				this.txtExemptTotal,
				this.txtExemptBillable,
				this.txtExemptHom2,
				this.txtExemptTotBillable,
				this.txtExemptTotExempt,
				this.txtExemptHom,
				this.txtExemptExempt,
				this.txtAssessTotal,
				this.txtAssessBillable,
				this.txtAssessHom2,
				this.txtAssessTotBillable,
				this.txtAssessTotExempt,
				this.txtAssessHom,
				this.txtAssessExempt,
				this.SubReport1,
				this.Label13,
				this.txtCountNoVal,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5
			});
			this.Detail.Height = 2.802083F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "";
			this.Label1.Text = "Totally Exempt w homesteads";
			this.Label1.Top = 0.71875F;
			this.Label1.Width = 1.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "";
			this.Label2.Text = "Totally Exempt w/o homesteads";
			this.Label2.Top = 0.53125F;
			this.Label2.Width = 2.125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "";
			this.Label3.Text = "Total";
			this.Label3.Top = 0.90625F;
			this.Label3.Width = 1.25F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "";
			this.Label4.Text = "Billable";
			this.Label4.Top = 1.28125F;
			this.Label4.Width = 1.125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Text = "Count";
			this.Label5.Top = 0.34375F;
			this.Label5.Width = 0.5625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.75F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Text = "Land";
			this.Label6.Top = 0.34375F;
			this.Label6.Width = 1.0625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.875F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: right";
			this.Label7.Text = "Building";
			this.Label7.Top = 0.34375F;
			this.Label7.Width = 1.125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "text-align: right";
			this.Label8.Text = "Exemption";
			this.Label8.Top = 0.34375F;
			this.Label8.Width = 1.125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "text-align: right";
			this.Label9.Text = "Assessment";
			this.Label9.Top = 0.34375F;
			this.Label9.Width = 1.1875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "";
			this.Label10.Text = "Totally Exempt w homesteads";
			this.Label10.Top = 1.46875F;
			this.Label10.Width = 1.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.0625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "";
			this.Label11.Text = "Total";
			this.Label11.Top = 1.65625F;
			this.Label11.Width = 1.4375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "";
			this.Label12.Text = "Total";
			this.Label12.Top = 2.4375F;
			this.Label12.Width = 1.25F;
			// 
			// txtCountTotExempt
			// 
			this.txtCountTotExempt.Height = 0.1875F;
			this.txtCountTotExempt.Left = 2.125F;
			this.txtCountTotExempt.Name = "txtCountTotExempt";
			this.txtCountTotExempt.Style = "text-align: right";
			this.txtCountTotExempt.Text = "Field1";
			this.txtCountTotExempt.Top = 0.53125F;
			this.txtCountTotExempt.Width = 0.5625F;
			// 
			// txtCountHom
			// 
			this.txtCountHom.Height = 0.1875F;
			this.txtCountHom.Left = 2.125F;
			this.txtCountHom.Name = "txtCountHom";
			this.txtCountHom.Style = "text-align: right";
			this.txtCountHom.Text = "Field2";
			this.txtCountHom.Top = 0.71875F;
			this.txtCountHom.Width = 0.5625F;
			// 
			// txtCountExempt
			// 
			this.txtCountExempt.Height = 0.1875F;
			this.txtCountExempt.Left = 2.125F;
			this.txtCountExempt.Name = "txtCountExempt";
			this.txtCountExempt.Style = "text-align: right";
			this.txtCountExempt.Text = "Field3";
			this.txtCountExempt.Top = 0.90625F;
			this.txtCountExempt.Width = 0.5625F;
			// 
			// txtCountBillable
			// 
			this.txtCountBillable.Height = 0.1875F;
			this.txtCountBillable.Left = 2.125F;
			this.txtCountBillable.Name = "txtCountBillable";
			this.txtCountBillable.Style = "text-align: right";
			this.txtCountBillable.Text = "Field4";
			this.txtCountBillable.Top = 1.28125F;
			this.txtCountBillable.Width = 0.5625F;
			// 
			// txtCountHom2
			// 
			this.txtCountHom2.Height = 0.1875F;
			this.txtCountHom2.Left = 2.125F;
			this.txtCountHom2.Name = "txtCountHom2";
			this.txtCountHom2.Style = "text-align: right";
			this.txtCountHom2.Text = "Field5";
			this.txtCountHom2.Top = 1.46875F;
			this.txtCountHom2.Width = 0.5625F;
			// 
			// txtCountTotBillable
			// 
			this.txtCountTotBillable.Height = 0.1875F;
			this.txtCountTotBillable.Left = 2.125F;
			this.txtCountTotBillable.Name = "txtCountTotBillable";
			this.txtCountTotBillable.Style = "text-align: right";
			this.txtCountTotBillable.Text = "Field6";
			this.txtCountTotBillable.Top = 1.65625F;
			this.txtCountTotBillable.Width = 0.5625F;
			// 
			// txtCountTotal
			// 
			this.txtCountTotal.Height = 0.1875F;
			this.txtCountTotal.Left = 2.125F;
			this.txtCountTotal.Name = "txtCountTotal";
			this.txtCountTotal.Style = "text-align: right";
			this.txtCountTotal.Text = "Field7";
			this.txtCountTotal.Top = 2.4375F;
			this.txtCountTotal.Width = 0.5625F;
			// 
			// txtLandTotExempt
			// 
			this.txtLandTotExempt.Height = 0.1875F;
			this.txtLandTotExempt.Left = 2.75F;
			this.txtLandTotExempt.Name = "txtLandTotExempt";
			this.txtLandTotExempt.Style = "text-align: right";
			this.txtLandTotExempt.Text = "Field8";
			this.txtLandTotExempt.Top = 0.53125F;
			this.txtLandTotExempt.Width = 1.0625F;
			// 
			// txtLandHom
			// 
			this.txtLandHom.Height = 0.1875F;
			this.txtLandHom.Left = 2.75F;
			this.txtLandHom.Name = "txtLandHom";
			this.txtLandHom.Style = "text-align: right";
			this.txtLandHom.Text = "Field9";
			this.txtLandHom.Top = 0.71875F;
			this.txtLandHom.Width = 1.0625F;
			// 
			// txtLandExempt
			// 
			this.txtLandExempt.Height = 0.1875F;
			this.txtLandExempt.Left = 2.75F;
			this.txtLandExempt.Name = "txtLandExempt";
			this.txtLandExempt.Style = "text-align: right";
			this.txtLandExempt.Text = "Field10";
			this.txtLandExempt.Top = 0.90625F;
			this.txtLandExempt.Width = 1.0625F;
			// 
			// txtLandBillable
			// 
			this.txtLandBillable.Height = 0.1875F;
			this.txtLandBillable.Left = 2.75F;
			this.txtLandBillable.Name = "txtLandBillable";
			this.txtLandBillable.Style = "text-align: right";
			this.txtLandBillable.Text = "Field11";
			this.txtLandBillable.Top = 1.28125F;
			this.txtLandBillable.Width = 1.0625F;
			// 
			// txtLandHom2
			// 
			this.txtLandHom2.Height = 0.1875F;
			this.txtLandHom2.Left = 2.75F;
			this.txtLandHom2.Name = "txtLandHom2";
			this.txtLandHom2.Style = "text-align: right";
			this.txtLandHom2.Text = "Field12";
			this.txtLandHom2.Top = 1.46875F;
			this.txtLandHom2.Width = 1.0625F;
			// 
			// txtLandTotBillable
			// 
			this.txtLandTotBillable.Height = 0.1875F;
			this.txtLandTotBillable.Left = 2.75F;
			this.txtLandTotBillable.Name = "txtLandTotBillable";
			this.txtLandTotBillable.Style = "text-align: right";
			this.txtLandTotBillable.Text = "Field13";
			this.txtLandTotBillable.Top = 1.65625F;
			this.txtLandTotBillable.Width = 1.0625F;
			// 
			// txtLandTotal
			// 
			this.txtLandTotal.Height = 0.1875F;
			this.txtLandTotal.Left = 2.75F;
			this.txtLandTotal.Name = "txtLandTotal";
			this.txtLandTotal.Style = "text-align: right";
			this.txtLandTotal.Text = "Field14";
			this.txtLandTotal.Top = 2.4375F;
			this.txtLandTotal.Width = 1.0625F;
			// 
			// txtBldgTotal
			// 
			this.txtBldgTotal.Height = 0.1875F;
			this.txtBldgTotal.Left = 3.875F;
			this.txtBldgTotal.Name = "txtBldgTotal";
			this.txtBldgTotal.Style = "text-align: right";
			this.txtBldgTotal.Text = "Field15";
			this.txtBldgTotal.Top = 2.4375F;
			this.txtBldgTotal.Width = 1.125F;
			// 
			// txtBldgBillable
			// 
			this.txtBldgBillable.Height = 0.1875F;
			this.txtBldgBillable.Left = 3.875F;
			this.txtBldgBillable.Name = "txtBldgBillable";
			this.txtBldgBillable.Style = "text-align: right";
			this.txtBldgBillable.Text = "Field16";
			this.txtBldgBillable.Top = 1.28125F;
			this.txtBldgBillable.Width = 1.125F;
			// 
			// txtBldgHom2
			// 
			this.txtBldgHom2.Height = 0.1875F;
			this.txtBldgHom2.Left = 3.875F;
			this.txtBldgHom2.Name = "txtBldgHom2";
			this.txtBldgHom2.Style = "text-align: right";
			this.txtBldgHom2.Text = "Field17";
			this.txtBldgHom2.Top = 1.46875F;
			this.txtBldgHom2.Width = 1.125F;
			// 
			// txtBldgTotBillable
			// 
			this.txtBldgTotBillable.Height = 0.1875F;
			this.txtBldgTotBillable.Left = 3.875F;
			this.txtBldgTotBillable.Name = "txtBldgTotBillable";
			this.txtBldgTotBillable.Style = "text-align: right";
			this.txtBldgTotBillable.Text = "Field18";
			this.txtBldgTotBillable.Top = 1.65625F;
			this.txtBldgTotBillable.Width = 1.125F;
			// 
			// txtBldgTotExempt
			// 
			this.txtBldgTotExempt.Height = 0.1875F;
			this.txtBldgTotExempt.Left = 3.875F;
			this.txtBldgTotExempt.Name = "txtBldgTotExempt";
			this.txtBldgTotExempt.Style = "text-align: right";
			this.txtBldgTotExempt.Text = "Field19";
			this.txtBldgTotExempt.Top = 0.53125F;
			this.txtBldgTotExempt.Width = 1.125F;
			// 
			// txtBldgHom
			// 
			this.txtBldgHom.Height = 0.1875F;
			this.txtBldgHom.Left = 3.875F;
			this.txtBldgHom.Name = "txtBldgHom";
			this.txtBldgHom.Style = "text-align: right";
			this.txtBldgHom.Text = "Field20";
			this.txtBldgHom.Top = 0.71875F;
			this.txtBldgHom.Width = 1.125F;
			// 
			// txtBldgExempt
			// 
			this.txtBldgExempt.Height = 0.1875F;
			this.txtBldgExempt.Left = 3.875F;
			this.txtBldgExempt.Name = "txtBldgExempt";
			this.txtBldgExempt.Style = "text-align: right";
			this.txtBldgExempt.Text = "Field21";
			this.txtBldgExempt.Top = 0.90625F;
			this.txtBldgExempt.Width = 1.125F;
			// 
			// txtExemptTotal
			// 
			this.txtExemptTotal.Height = 0.1875F;
			this.txtExemptTotal.Left = 5.0625F;
			this.txtExemptTotal.Name = "txtExemptTotal";
			this.txtExemptTotal.Style = "text-align: right";
			this.txtExemptTotal.Text = "Field22";
			this.txtExemptTotal.Top = 2.4375F;
			this.txtExemptTotal.Width = 1.125F;
			// 
			// txtExemptBillable
			// 
			this.txtExemptBillable.Height = 0.1875F;
			this.txtExemptBillable.Left = 5.0625F;
			this.txtExemptBillable.Name = "txtExemptBillable";
			this.txtExemptBillable.Style = "text-align: right";
			this.txtExemptBillable.Text = "Field23";
			this.txtExemptBillable.Top = 1.28125F;
			this.txtExemptBillable.Width = 1.125F;
			// 
			// txtExemptHom2
			// 
			this.txtExemptHom2.Height = 0.1875F;
			this.txtExemptHom2.Left = 5.0625F;
			this.txtExemptHom2.Name = "txtExemptHom2";
			this.txtExemptHom2.Style = "text-align: right";
			this.txtExemptHom2.Text = "Field24";
			this.txtExemptHom2.Top = 1.46875F;
			this.txtExemptHom2.Width = 1.125F;
			// 
			// txtExemptTotBillable
			// 
			this.txtExemptTotBillable.Height = 0.1875F;
			this.txtExemptTotBillable.Left = 5.0625F;
			this.txtExemptTotBillable.Name = "txtExemptTotBillable";
			this.txtExemptTotBillable.Style = "text-align: right";
			this.txtExemptTotBillable.Text = "Field25";
			this.txtExemptTotBillable.Top = 1.65625F;
			this.txtExemptTotBillable.Width = 1.125F;
			// 
			// txtExemptTotExempt
			// 
			this.txtExemptTotExempt.Height = 0.1875F;
			this.txtExemptTotExempt.Left = 5.0625F;
			this.txtExemptTotExempt.Name = "txtExemptTotExempt";
			this.txtExemptTotExempt.Style = "text-align: right";
			this.txtExemptTotExempt.Text = "Field26";
			this.txtExemptTotExempt.Top = 0.53125F;
			this.txtExemptTotExempt.Width = 1.125F;
			// 
			// txtExemptHom
			// 
			this.txtExemptHom.Height = 0.1875F;
			this.txtExemptHom.Left = 5.0625F;
			this.txtExemptHom.Name = "txtExemptHom";
			this.txtExemptHom.Style = "text-align: right";
			this.txtExemptHom.Text = "Field27";
			this.txtExemptHom.Top = 0.71875F;
			this.txtExemptHom.Width = 1.125F;
			// 
			// txtExemptExempt
			// 
			this.txtExemptExempt.Height = 0.1875F;
			this.txtExemptExempt.Left = 5.0625F;
			this.txtExemptExempt.Name = "txtExemptExempt";
			this.txtExemptExempt.Style = "text-align: right";
			this.txtExemptExempt.Text = "Field28";
			this.txtExemptExempt.Top = 0.90625F;
			this.txtExemptExempt.Width = 1.125F;
			// 
			// txtAssessTotal
			// 
			this.txtAssessTotal.Height = 0.1875F;
			this.txtAssessTotal.Left = 6.25F;
			this.txtAssessTotal.Name = "txtAssessTotal";
			this.txtAssessTotal.Style = "text-align: right";
			this.txtAssessTotal.Text = "Field29";
			this.txtAssessTotal.Top = 2.4375F;
			this.txtAssessTotal.Width = 1.1875F;
			// 
			// txtAssessBillable
			// 
			this.txtAssessBillable.Height = 0.1875F;
			this.txtAssessBillable.Left = 6.25F;
			this.txtAssessBillable.Name = "txtAssessBillable";
			this.txtAssessBillable.Style = "text-align: right";
			this.txtAssessBillable.Text = "Field30";
			this.txtAssessBillable.Top = 1.28125F;
			this.txtAssessBillable.Width = 1.1875F;
			// 
			// txtAssessHom2
			// 
			this.txtAssessHom2.Height = 0.1875F;
			this.txtAssessHom2.Left = 6.25F;
			this.txtAssessHom2.Name = "txtAssessHom2";
			this.txtAssessHom2.Style = "text-align: right";
			this.txtAssessHom2.Text = "Field31";
			this.txtAssessHom2.Top = 1.46875F;
			this.txtAssessHom2.Width = 1.1875F;
			// 
			// txtAssessTotBillable
			// 
			this.txtAssessTotBillable.Height = 0.1875F;
			this.txtAssessTotBillable.Left = 6.25F;
			this.txtAssessTotBillable.Name = "txtAssessTotBillable";
			this.txtAssessTotBillable.Style = "text-align: right";
			this.txtAssessTotBillable.Text = "Field32";
			this.txtAssessTotBillable.Top = 1.65625F;
			this.txtAssessTotBillable.Width = 1.1875F;
			// 
			// txtAssessTotExempt
			// 
			this.txtAssessTotExempt.Height = 0.1875F;
			this.txtAssessTotExempt.Left = 6.25F;
			this.txtAssessTotExempt.Name = "txtAssessTotExempt";
			this.txtAssessTotExempt.Style = "text-align: right";
			this.txtAssessTotExempt.Text = "Field33";
			this.txtAssessTotExempt.Top = 0.53125F;
			this.txtAssessTotExempt.Width = 1.1875F;
			// 
			// txtAssessHom
			// 
			this.txtAssessHom.Height = 0.1875F;
			this.txtAssessHom.Left = 6.25F;
			this.txtAssessHom.Name = "txtAssessHom";
			this.txtAssessHom.Style = "text-align: right";
			this.txtAssessHom.Text = "Field34";
			this.txtAssessHom.Top = 0.71875F;
			this.txtAssessHom.Width = 1.1875F;
			// 
			// txtAssessExempt
			// 
			this.txtAssessExempt.Height = 0.1875F;
			this.txtAssessExempt.Left = 6.25F;
			this.txtAssessExempt.Name = "txtAssessExempt";
			this.txtAssessExempt.Style = "text-align: right";
			this.txtAssessExempt.Text = "Field35";
			this.txtAssessExempt.Top = 0.90625F;
			this.txtAssessExempt.Width = 1.1875F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.09375F;
			this.SubReport1.Width = 7.5F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "";
			this.Label13.Text = "No Valuation";
			this.Label13.Top = 2.0625F;
			this.Label13.Width = 1.875F;
			// 
			// txtCountNoVal
			// 
			this.txtCountNoVal.Height = 0.1875F;
			this.txtCountNoVal.Left = 2.125F;
			this.txtCountNoVal.Name = "txtCountNoVal";
			this.txtCountNoVal.Style = "text-align: right";
			this.txtCountNoVal.Text = "Field5";
			this.txtCountNoVal.Top = 2.0625F;
			this.txtCountNoVal.Width = 0.5625F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 2.75F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "text-align: right";
			this.Field2.Text = "0";
			this.Field2.Top = 2.0625F;
			this.Field2.Width = 1.0625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 3.875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "text-align: right";
			this.Field3.Text = "0";
			this.Field3.Top = 2.0625F;
			this.Field3.Width = 1.125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 5.0625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "text-align: right";
			this.Field4.Text = "0";
			this.Field4.Top = 2.0625F;
			this.Field4.Width = 1.125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 6.25F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "text-align: right";
			this.Field5.Text = "0";
			this.Field5.Top = 2.0625F;
			this.Field5.Width = 1.1875F;
			// 
			// srptAuditTotals
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountHom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountHom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountTotBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandHom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandHom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTotBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgHom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTotBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgHom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptHom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTotBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptHom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessHom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessTotBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessHom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountNoVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountHom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountHom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountTotBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandHom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandHom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandTotBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgHom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgTotBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgHom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptHom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptTotBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptHom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessHom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessTotBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessHom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessExempt;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountNoVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
	}
}
