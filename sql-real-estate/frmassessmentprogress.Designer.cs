﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for Frmassessmentprogress.
	/// </summary>
	partial class Frmassessmentprogress : BaseForm
	{
		public fecherFoundation.FCButton cmdcancexempt;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblpercentdone;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmassessmentprogress));
            this.cmdcancexempt = new fecherFoundation.FCButton();
            this.ProgressBar1 = new fecherFoundation.FCProgressBar();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblpercentdone = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdcancexempt)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 184);
            this.BottomPanel.Size = new System.Drawing.Size(336, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdcancexempt);
            this.ClientArea.Controls.Add(this.ProgressBar1);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblpercentdone);
            this.ClientArea.Size = new System.Drawing.Size(356, 314);
            this.ClientArea.Controls.SetChildIndex(this.lblpercentdone, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.ProgressBar1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdcancexempt, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(356, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(203, 28);
            this.HeaderText.Text = "Calculating Values";
            // 
            // cmdcancexempt
            // 
            this.cmdcancexempt.AppearanceKey = "actionButton";
            this.cmdcancexempt.Location = new System.Drawing.Point(138, 144);
            this.cmdcancexempt.Name = "cmdcancexempt";
            this.cmdcancexempt.Size = new System.Drawing.Size(75, 40);
            this.cmdcancexempt.TabIndex = 2;
            this.cmdcancexempt.Text = "Cancel";
            this.cmdcancexempt.Click += new System.EventHandler(this.cmdcancexempt_Click);
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(20, 106);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(316, 18);
            this.ProgressBar1.TabIndex = 3;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 68);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(316, 18);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "DONE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Label3.Visible = false;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(316, 18);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "CALCULATING EXEMPTIONS, PLEASE WAIT";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblpercentdone
            // 
            this.lblpercentdone.Location = new System.Drawing.Point(20, 46);
            this.lblpercentdone.Name = "lblpercentdone";
            this.lblpercentdone.Size = new System.Drawing.Size(316, 15);
            this.lblpercentdone.TabIndex = 1;
            this.lblpercentdone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frmassessmentprogress
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(356, 374);
            this.ControlBox = false;
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "Frmassessmentprogress";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Calculate Exemptions";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.Frmassessmentprogress_Load);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdcancexempt)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
