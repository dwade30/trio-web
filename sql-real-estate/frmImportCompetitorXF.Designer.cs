//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportCompetitor.
	/// </summary>
	partial class frmImportCompetitorXF
	{
		public FCGrid gridMultiOwner;
		public fecherFoundation.FCTextBox txtPercentChg;
		public fecherFoundation.FCCheckBox chkFlagChanges;
		public fecherFoundation.FCComboBox cmbUpdateType;
		public FCCommonDialog CommonDialog1;
		public FCGrid GridLoad;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridGood;
		public fecherFoundation.FCCheckBox chkShowChangesOnly;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid GridNew;
		public fecherFoundation.FCCheckBox chkShowAddedOnly;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCButton cmdUnselect;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCCheckBox chkMarkDeleted;
		public FCGrid GridNotUpdated;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public FCGrid GridBadAccounts;
		public fecherFoundation.FCCheckBox chkUndeleteAccounts;
		public fecherFoundation.FCComboBox cmbImportType;
		public FCGrid gridPreviousOwners;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblCurAssess;
		public fecherFoundation.FCLabel lblCurExempt;
		public fecherFoundation.FCLabel lblCurBldg;
		public fecherFoundation.FCLabel lblCurLand;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuLoad;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAccountsToUpdate;
		public fecherFoundation.FCToolStripMenuItem mnuPrintChanges;
		public fecherFoundation.FCToolStripMenuItem mnuPrintBad;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNew;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNotUpdated;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSummary;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridMultiOwner = new fecherFoundation.FCGrid();
			this.txtPercentChg = new fecherFoundation.FCTextBox();
			this.chkFlagChanges = new fecherFoundation.FCCheckBox();
			this.cmbUpdateType = new fecherFoundation.FCComboBox();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.GridLoad = new fecherFoundation.FCGrid();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.GridGood = new fecherFoundation.FCGrid();
			this.chkShowChangesOnly = new fecherFoundation.FCCheckBox();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.GridNew = new fecherFoundation.FCGrid();
			this.chkShowAddedOnly = new fecherFoundation.FCCheckBox();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.cmdUnselect = new fecherFoundation.FCButton();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.chkMarkDeleted = new fecherFoundation.FCCheckBox();
			this.GridNotUpdated = new fecherFoundation.FCGrid();
			this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
			this.GridBadAccounts = new fecherFoundation.FCGrid();
			this.chkUndeleteAccounts = new fecherFoundation.FCCheckBox();
			this.cmbImportType = new fecherFoundation.FCComboBox();
			this.gridPreviousOwners = new fecherFoundation.FCGrid();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblCurAssess = new fecherFoundation.FCLabel();
			this.lblCurExempt = new fecherFoundation.FCLabel();
			this.lblCurBldg = new fecherFoundation.FCLabel();
			this.lblCurLand = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuPrintAccountsToUpdate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintChanges = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintBad = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintNotUpdated = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintSummary = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLoad = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdUpdate = new fecherFoundation.FCButton();
			this.cmdLoadRecords = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridMultiOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFlagChanges)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLoad)).BeginInit();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridGood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowChangesOnly)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAddedOnly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnselect)).BeginInit();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNotUpdated)).BeginInit();
			this.SSTab1_Page4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridBadAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUndeleteAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridPreviousOwners)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadRecords)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdUpdate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 588);
			this.BottomPanel.Size = new System.Drawing.Size(985, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.gridMultiOwner);
			this.ClientArea.Controls.Add(this.txtPercentChg);
			this.ClientArea.Controls.Add(this.chkFlagChanges);
			this.ClientArea.Controls.Add(this.cmbUpdateType);
			this.ClientArea.Controls.Add(this.GridLoad);
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.cmbImportType);
			this.ClientArea.Controls.Add(this.gridPreviousOwners);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblCurAssess);
			this.ClientArea.Controls.Add(this.lblCurExempt);
			this.ClientArea.Controls.Add(this.lblCurBldg);
			this.ClientArea.Controls.Add(this.lblCurLand);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1005, 606);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCurLand, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCurBldg, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCurExempt, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCurAssess, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
			this.ClientArea.Controls.SetChildIndex(this.Shape1, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridPreviousOwners, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbImportType, 0);
			this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridLoad, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbUpdateType, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkFlagChanges, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtPercentChg, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridMultiOwner, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdLoadRecords);
			this.TopPanel.Size = new System.Drawing.Size(1005, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoadRecords, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(307, 28);
			this.HeaderText.Text = "Import Real Estate for Billing";
			// 
			// gridMultiOwner
			// 
			this.gridMultiOwner.Cols = 6;
			this.gridMultiOwner.ColumnHeadersVisible = false;
			this.gridMultiOwner.FixedRows = 0;
			this.gridMultiOwner.Location = new System.Drawing.Point(645, 90);
			this.gridMultiOwner.Name = "gridMultiOwner";
			this.gridMultiOwner.Rows = 0;
			this.gridMultiOwner.Size = new System.Drawing.Size(80, 41);
			this.gridMultiOwner.TabIndex = 25;
			this.gridMultiOwner.Visible = false;
			// 
			// txtPercentChg
			// 
			this.txtPercentChg.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercentChg.Location = new System.Drawing.Point(682, 30);
			this.txtPercentChg.Name = "txtPercentChg";
			this.txtPercentChg.Size = new System.Drawing.Size(60, 40);
			this.txtPercentChg.TabIndex = 17;
			this.txtPercentChg.Text = "0";
			this.txtPercentChg.Validating += new System.ComponentModel.CancelEventHandler(this.txtPercentChg_Validating);
			// 
			// chkFlagChanges
			// 
			this.chkFlagChanges.Location = new System.Drawing.Point(369, 36);
			this.chkFlagChanges.Name = "chkFlagChanges";
			this.chkFlagChanges.Size = new System.Drawing.Size(252, 22);
			this.chkFlagChanges.TabIndex = 16;
			this.chkFlagChanges.Text = "Flag accounts with changes in value >";
			this.chkFlagChanges.CheckedChanged += new System.EventHandler(this.chkFlagChanges_CheckedChanged);
			// 
			// cmbUpdateType
			// 
			this.cmbUpdateType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbUpdateType.Items.AddRange(new object[] {
            "Update Amounts",
            "Update Information Only"});
			this.cmbUpdateType.Location = new System.Drawing.Point(369, 90);
			this.cmbUpdateType.Name = "cmbUpdateType";
			this.cmbUpdateType.Size = new System.Drawing.Size(256, 40);
			this.cmbUpdateType.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.cmbUpdateType, "Choose Update Information Only if you do not want to update amounts");
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			// 
			// GridLoad
			// 
			this.GridLoad.Cols = 10;
			this.GridLoad.ColumnHeadersVisible = false;
			this.GridLoad.FixedCols = 0;
			this.GridLoad.FixedRows = 0;
			this.GridLoad.Location = new System.Drawing.Point(745, 90);
			this.GridLoad.Name = "GridLoad";
			this.GridLoad.RowHeadersVisible = false;
			this.GridLoad.Rows = 0;
			this.GridLoad.Size = new System.Drawing.Size(114, 18);
			this.GridLoad.TabIndex = 6;
			this.GridLoad.Visible = false;
			// 
			// SSTab1
			// 
			this.SSTab1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Controls.Add(this.SSTab1_Page4);
			this.SSTab1.Location = new System.Drawing.Point(30, 150);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(947, 438);
			this.SSTab1.TabIndex = 1;
			this.SSTab1.Text = "Update";
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.GridGood);
			this.SSTab1_Page1.Controls.Add(this.chkShowChangesOnly);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Size = new System.Drawing.Size(945, 402);
			this.SSTab1_Page1.Text = "Update";
			// 
			// GridGood
			// 
			this.GridGood.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridGood.Cols = 1;
			this.GridGood.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridGood.FixedCols = 0;
			this.GridGood.Location = new System.Drawing.Point(4, 60);
			this.GridGood.Name = "GridGood";
			this.GridGood.RowHeadersVisible = false;
			this.GridGood.Rows = 1;
			this.GridGood.Size = new System.Drawing.Size(936, 337);
			this.GridGood.TabIndex = 2;
			// 
			// chkShowChangesOnly
			// 
			this.chkShowChangesOnly.Location = new System.Drawing.Point(20, 20);
			this.chkShowChangesOnly.Name = "chkShowChangesOnly";
			this.chkShowChangesOnly.Size = new System.Drawing.Size(201, 22);
			this.chkShowChangesOnly.TabIndex = 19;
			this.chkShowChangesOnly.Text = "Show changed accounts only";
			this.ToolTip1.SetToolTip(this.chkShowChangesOnly, "This option refers to changed values and affects the Accounts To Update report.  " +
        "For a list of any changes made use the Changes Report.");
			this.chkShowChangesOnly.CheckedChanged += new System.EventHandler(this.chkShowChangesOnly_CheckedChanged);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.GridNew);
			this.SSTab1_Page2.Controls.Add(this.chkShowAddedOnly);
			this.SSTab1_Page2.Controls.Add(this.cmdSelect);
			this.SSTab1_Page2.Controls.Add(this.cmdUnselect);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Size = new System.Drawing.Size(945, 402);
			this.SSTab1_Page2.Text = "New Accounts";
			// 
			// GridNew
			// 
			this.GridNew.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridNew.Cols = 1;
			this.GridNew.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridNew.FixedCols = 0;
			this.GridNew.Location = new System.Drawing.Point(3, 60);
			this.GridNew.Name = "GridNew";
			this.GridNew.RowHeadersVisible = false;
			this.GridNew.Rows = 1;
			this.GridNew.Size = new System.Drawing.Size(936, 337);
			this.GridNew.TabIndex = 3;
			this.GridNew.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridNew_ValidateEdit);
			this.GridNew.CurrentCellChanged += new System.EventHandler(this.GridNew_RowColChange);
			// 
			// chkShowAddedOnly
			// 
			this.chkShowAddedOnly.Location = new System.Drawing.Point(20, 20);
			this.chkShowAddedOnly.Name = "chkShowAddedOnly";
			this.chkShowAddedOnly.Size = new System.Drawing.Size(187, 22);
			this.chkShowAddedOnly.TabIndex = 20;
			this.chkShowAddedOnly.Text = "Show accounts to add only";
			this.chkShowAddedOnly.CheckedChanged += new System.EventHandler(this.chkShowAddedOnly_CheckedChanged);
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Location = new System.Drawing.Point(250, 10);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(120, 40);
			this.cmdSelect.TabIndex = 21;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// cmdUnselect
			// 
			this.cmdUnselect.AppearanceKey = "actionButton";
			this.cmdUnselect.Location = new System.Drawing.Point(400, 10);
			this.cmdUnselect.Name = "cmdUnselect";
			this.cmdUnselect.Size = new System.Drawing.Size(150, 40);
			this.cmdUnselect.TabIndex = 22;
			this.cmdUnselect.Text = "Un Select All";
			this.cmdUnselect.Click += new System.EventHandler(this.cmdUnselect_Click);
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.chkMarkDeleted);
			this.SSTab1_Page3.Controls.Add(this.GridNotUpdated);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Size = new System.Drawing.Size(945, 402);
			this.SSTab1_Page3.Text = "Not Updated";
			// 
			// chkMarkDeleted
			// 
			this.chkMarkDeleted.Location = new System.Drawing.Point(20, 20);
			this.chkMarkDeleted.Name = "chkMarkDeleted";
			this.chkMarkDeleted.Size = new System.Drawing.Size(300, 22);
			this.chkMarkDeleted.TabIndex = 23;
			this.chkMarkDeleted.Text = "Mark all Not Updateds as deleted upon update";
			// 
			// GridNotUpdated
			// 
			this.GridNotUpdated.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridNotUpdated.Cols = 1;
			this.GridNotUpdated.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridNotUpdated.FixedCols = 0;
			this.GridNotUpdated.Location = new System.Drawing.Point(4, 60);
			this.GridNotUpdated.Name = "GridNotUpdated";
			this.GridNotUpdated.RowHeadersVisible = false;
			this.GridNotUpdated.Rows = 1;
			this.GridNotUpdated.Size = new System.Drawing.Size(936, 337);
			this.GridNotUpdated.TabIndex = 4;
			// 
			// SSTab1_Page4
			// 
			this.SSTab1_Page4.Controls.Add(this.GridBadAccounts);
			this.SSTab1_Page4.Controls.Add(this.chkUndeleteAccounts);
			this.SSTab1_Page4.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page4.Name = "SSTab1_Page4";
			this.SSTab1_Page4.Size = new System.Drawing.Size(945, 402);
			this.SSTab1_Page4.Text = "Bad Accounts";
			// 
			// GridBadAccounts
			// 
			this.GridBadAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridBadAccounts.Cols = 1;
			this.GridBadAccounts.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridBadAccounts.FixedCols = 0;
			this.GridBadAccounts.Location = new System.Drawing.Point(4, 60);
			this.GridBadAccounts.Name = "GridBadAccounts";
			this.GridBadAccounts.RowHeadersVisible = false;
			this.GridBadAccounts.Rows = 1;
			this.GridBadAccounts.Size = new System.Drawing.Size(936, 337);
			this.GridBadAccounts.TabIndex = 5;
			// 
			// chkUndeleteAccounts
			// 
			this.chkUndeleteAccounts.Location = new System.Drawing.Point(20, 20);
			this.chkUndeleteAccounts.Name = "chkUndeleteAccounts";
			this.chkUndeleteAccounts.Size = new System.Drawing.Size(422, 22);
			this.chkUndeleteAccounts.TabIndex = 24;
			this.chkUndeleteAccounts.Text = "If only error is Deleted status, un-delete account and transfer values";
			// 
			// cmbImportType
			// 
			this.cmbImportType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbImportType.Location = new System.Drawing.Point(745, 90);
			this.cmbImportType.Name = "cmbImportType";
			this.cmbImportType.Size = new System.Drawing.Size(209, 40);
			this.cmbImportType.TabIndex = 26;
			this.cmbImportType.Visible = false;
			// 
			// gridPreviousOwners
			// 
			this.gridPreviousOwners.Cols = 6;
			this.gridPreviousOwners.ColumnHeadersVisible = false;
			this.gridPreviousOwners.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.gridPreviousOwners.FixedCols = 0;
			this.gridPreviousOwners.FixedRows = 0;
			this.gridPreviousOwners.Location = new System.Drawing.Point(265, 30);
			this.gridPreviousOwners.Name = "gridPreviousOwners";
			this.gridPreviousOwners.RowHeadersVisible = false;
			this.gridPreviousOwners.Rows = 0;
			this.gridPreviousOwners.Size = new System.Drawing.Size(80, 41);
			this.gridPreviousOwners.TabIndex = 26;
			this.gridPreviousOwners.Visible = false;
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(255, 255, 0);
			this.Shape1.Location = new System.Drawing.Point(813, 40);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(17, 18);
			this.Shape1.TabIndex = 27;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(766, 44);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(33, 18);
			this.Label5.TabIndex = 18;
			this.Label5.Text = "% AS";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 111);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(120, 16);
			this.Label4.TabIndex = 14;
			this.Label4.Text = "CURRENT NET";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 84);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(120, 16);
			this.Label3.TabIndex = 13;
			this.Label3.Text = "CURRENT EXEMPT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 57);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(120, 16);
			this.Label2.TabIndex = 12;
			this.Label2.Text = "CURRENT BUILDING";
			// 
			// lblCurAssess
			// 
			this.lblCurAssess.Location = new System.Drawing.Point(163, 111);
			this.lblCurAssess.Name = "lblCurAssess";
			this.lblCurAssess.Size = new System.Drawing.Size(82, 18);
			this.lblCurAssess.TabIndex = 11;
			this.lblCurAssess.Text = "0";
			this.lblCurAssess.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblCurExempt
			// 
			this.lblCurExempt.Location = new System.Drawing.Point(163, 84);
			this.lblCurExempt.Name = "lblCurExempt";
			this.lblCurExempt.Size = new System.Drawing.Size(82, 18);
			this.lblCurExempt.TabIndex = 10;
			this.lblCurExempt.Text = "0";
			this.lblCurExempt.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblCurBldg
			// 
			this.lblCurBldg.Location = new System.Drawing.Point(163, 57);
			this.lblCurBldg.Name = "lblCurBldg";
			this.lblCurBldg.Size = new System.Drawing.Size(82, 18);
			this.lblCurBldg.TabIndex = 9;
			this.lblCurBldg.Text = "0";
			this.lblCurBldg.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblCurLand
			// 
			this.lblCurLand.Location = new System.Drawing.Point(163, 30);
			this.lblCurLand.Name = "lblCurLand";
			this.lblCurLand.Size = new System.Drawing.Size(82, 18);
			this.lblCurLand.TabIndex = 8;
			this.lblCurLand.Text = "0";
			this.lblCurLand.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(120, 16);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "CURRENT LAND";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintAccountsToUpdate,
            this.mnuPrintChanges,
            this.mnuPrintBad,
            this.mnuPrintNew,
            this.mnuPrintNotUpdated,
            this.mnuPrintSummary});
			this.MainMenu1.Name = null;
			// 
			// mnuPrintAccountsToUpdate
			// 
			this.mnuPrintAccountsToUpdate.Index = 0;
			this.mnuPrintAccountsToUpdate.Name = "mnuPrintAccountsToUpdate";
			this.mnuPrintAccountsToUpdate.Text = "Print Accounts To Update Report";
			this.mnuPrintAccountsToUpdate.Click += new System.EventHandler(this.mnuPrintAccountsToUpdate_Click);
			// 
			// mnuPrintChanges
			// 
			this.mnuPrintChanges.Index = 1;
			this.mnuPrintChanges.Name = "mnuPrintChanges";
			this.mnuPrintChanges.Text = "Print Changes Report";
			this.mnuPrintChanges.Click += new System.EventHandler(this.mnuPrintChanges_Click);
			// 
			// mnuPrintBad
			// 
			this.mnuPrintBad.Index = 2;
			this.mnuPrintBad.Name = "mnuPrintBad";
			this.mnuPrintBad.Text = "Print Bad Account Report";
			this.mnuPrintBad.Click += new System.EventHandler(this.mnuPrintBad_Click);
			// 
			// mnuPrintNew
			// 
			this.mnuPrintNew.Index = 3;
			this.mnuPrintNew.Name = "mnuPrintNew";
			this.mnuPrintNew.Text = "Print New Account Report";
			this.mnuPrintNew.Click += new System.EventHandler(this.mnuPrintNew_Click);
			// 
			// mnuPrintNotUpdated
			// 
			this.mnuPrintNotUpdated.Index = 4;
			this.mnuPrintNotUpdated.Name = "mnuPrintNotUpdated";
			this.mnuPrintNotUpdated.Text = "Print Not Updated Report";
			this.mnuPrintNotUpdated.Click += new System.EventHandler(this.mnuPrintNotUpdated_Click);
			// 
			// mnuPrintSummary
			// 
			this.mnuPrintSummary.Index = 5;
			this.mnuPrintSummary.Name = "mnuPrintSummary";
			this.mnuPrintSummary.Text = "Print Summary Report";
			this.mnuPrintSummary.Click += new System.EventHandler(this.mnuPrintSummary_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "";
			// 
			// mnuLoad
			// 
			this.mnuLoad.Index = -1;
			this.mnuLoad.Name = "mnuLoad";
			this.mnuLoad.Text = "Load Records";
			this.mnuLoad.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = -1;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = -1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = -1;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Update TRIO";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = -1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdUpdate
			// 
			this.cmdUpdate.AppearanceKey = "acceptButton";
			this.cmdUpdate.Location = new System.Drawing.Point(400, 30);
			this.cmdUpdate.Name = "cmdUpdate";
			this.cmdUpdate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdUpdate.Size = new System.Drawing.Size(150, 48);
			this.cmdUpdate.TabIndex = 0;
			this.cmdUpdate.Text = "Update TRIO";
			this.cmdUpdate.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// cmdLoadRecords
			// 
			this.cmdLoadRecords.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoadRecords.Location = new System.Drawing.Point(873, 29);
			this.cmdLoadRecords.Name = "cmdLoadRecords";
			this.cmdLoadRecords.Size = new System.Drawing.Size(104, 24);
			this.cmdLoadRecords.TabIndex = 1002;
			this.cmdLoadRecords.Text = "Load Records";
			this.cmdLoadRecords.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// frmImportCompetitorXF
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1005, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmImportCompetitorXF";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Real Estate for Billing";
			this.Load += new System.EventHandler(this.frmImportCompetitor_Load);
			this.Resize += new System.EventHandler(this.frmImportCompetitor_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportCompetitor_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridMultiOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFlagChanges)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLoad)).EndInit();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			this.SSTab1_Page1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridGood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowChangesOnly)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			this.SSTab1_Page2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAddedOnly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnselect)).EndInit();
			this.SSTab1_Page3.ResumeLayout(false);
			this.SSTab1_Page3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNotUpdated)).EndInit();
			this.SSTab1_Page4.ResumeLayout(false);
			this.SSTab1_Page4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridBadAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUndeleteAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridPreviousOwners)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadRecords)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdUpdate;
		private FCButton cmdLoadRecords;
	}
}