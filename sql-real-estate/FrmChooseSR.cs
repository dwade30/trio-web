﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for FrmChooseSR.
	/// </summary>
	public partial class FrmChooseSR : BaseForm
	{
		public FrmChooseSR()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static FrmChooseSR InstancePtr
		{
			get
			{
				return (FrmChooseSR)Sys.GetInstance(typeof(FrmChooseSR));
			}
		}

		protected FrmChooseSR _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int[] intAutoID = new int[999 + 1];
		int intScreenPointer;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			FrmChooseSR.InstancePtr.Unload();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void FrmChooseSR_Activated(object sender, System.EventArgs e)
		{
			intScreenPointer = FCConvert.ToInt32(FCGlobal.Screen.MousePointer);
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void FrmChooseSR_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void FrmChooseSR_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//FrmChooseSR properties;
			//FrmChooseSR.ScaleWidth	= 6930;
			//FrmChooseSR.ScaleHeight	= 5400;
			//FrmChooseSR.LinkTopic	= "Form1";
			//FrmChooseSR.LockControls	= true;
			//End Unmaped Properties
			string strRec = "";
			string strName = "";
			string strMapLot = "";
			string strSDate = "";
			//FC:FINAL:MSH - issue #1738: add columns into ListView for correct displaying data (WiseJ doesn't display tabs and whitespaces)
			List1.Clear();
			List1.Columns.Add("");
			List1.Columns.Add("");
			List1.Columns.Add("");
			int listWidth = List1.Width;
			List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.15);
			List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.32);
			List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.28);
			List1.Columns[3].Width = FCConvert.ToInt32(listWidth * 0.24);
			List1.GridLineStyle = GridLineStyle.None;
			while (!modGlobalVariables.Statics.gRstemp.EndOfFile())
			{
				strRec = Strings.Format((modGlobalVariables.Statics.gRstemp.Get_Fields_Int32("rsaccount") + ""), "000000");
				//FC:FINAL:MSH - issue #1738: add tabs for parsing data into columns
				//strRec += "  ";
				strRec += "\t";
				strName = Strings.Trim(modGlobalVariables.Statics.gRstemp.Get_Fields_String("rsname") + "");
				if (strName.Length > 25)
					strName = Strings.Mid(strName, 1, 25);
				if (strName.Length < 25)
					strName += Strings.StrDup(25 - strName.Length, " ");
				strRec += strName;
				//FC:FINAL:MSH - issue #1738: add tabs for parsing data into columns
				//strRec += " ";
				strRec += "\t";
				strMapLot = Strings.Trim(modGlobalVariables.Statics.gRstemp.Get_Fields_String("rsmaplot") + "");
				strRec += strMapLot + Strings.StrDup(17 - Strings.Mid(strMapLot, 1, 17).Length, " ");
				//FC:FINAL:MSH - issue #1738: add tabs for parsing data into columns
				//strRec += "  ";
				strRec += "\t";
                //FC:FINAL:CHN: Remove showing Time from Date column.
                // strSDate = Strings.Trim(modGlobalVariables.Statics.gRstemp.Get_Fields_DateTime("SaleDate") + "");
                strSDate = Strings.Trim(modGlobalVariables.Statics.gRstemp.Get_Fields_DateTime("SaleDate").ToShortDateString());
                strRec += strSDate + Strings.StrDup(10 - Strings.Mid(strSDate, 1, 10).Length, " ");
				List1.AddItem(strRec);
				intAutoID[List1.ListCount] = FCConvert.ToInt32(modGlobalVariables.Statics.gRstemp.Get_Fields_Int32("Id"));
				// End If
				modGlobalVariables.Statics.gRstemp.MoveNext();
			}
		}

		private void List1_DoubleClick(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gRstemp.OpenRecordset("select * from srmaster where id = " + FCConvert.ToString(intAutoID[List1.SelectedIndex + 1]), modGlobalVariables.strREDatabase);
			modGlobalVariables.Statics.gRstemp.Edit();
			FCGlobal.Screen.MousePointer = (MousePointerConstants)intScreenPointer;
			FrmChooseSR.InstancePtr.Unload();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
