﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation;
using System;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmWhichExemptions.
	/// </summary>
	partial class frmWhichExemptions : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtBilOrCor;
		public fecherFoundation.FCLabel lbltBilOrCor;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbtBilOrCor = new fecherFoundation.FCComboBox();
			this.lbltBilOrCor = new fecherFoundation.FCLabel();
			this.Command1 = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 431);
			this.BottomPanel.Size = new System.Drawing.Size(490, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Command1);
			this.ClientArea.Controls.Add(this.cmbtBilOrCor);
			this.ClientArea.Controls.Add(this.lbltBilOrCor);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(490, 371);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(490, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(333, 30);
			this.HeaderText.Text = "Billing or Correlated Amounts";
			// 
			// cmbtBilOrCor
			// 
			this.cmbtBilOrCor.AutoSize = false;
			this.cmbtBilOrCor.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbtBilOrCor.FormattingEnabled = true;
			this.cmbtBilOrCor.Items.AddRange(new object[] {
				"Billing",
				"Current"
			});
			this.cmbtBilOrCor.Location = new System.Drawing.Point(162, 140);
			this.cmbtBilOrCor.Name = "cmbtBilOrCor";
			this.cmbtBilOrCor.Size = new System.Drawing.Size(193, 40);
			this.cmbtBilOrCor.TabIndex = 3;
			this.cmbtBilOrCor.Text = "Billing";
			// 
			// lbltBilOrCor
			// 
			this.lbltBilOrCor.AutoSize = true;
			this.lbltBilOrCor.Location = new System.Drawing.Point(30, 154);
			this.lbltBilOrCor.Name = "lbltBilOrCor";
			this.lbltBilOrCor.Size = new System.Drawing.Size(71, 15);
			this.lbltBilOrCor.TabIndex = 2;
			this.lbltBilOrCor.Text = "BASED ON";
			// 
			// Command1
			// 
			this.Command1.AppearanceKey = "actionButton";
			this.Command1.ForeColor = System.Drawing.Color.White;
			this.Command1.Location = new System.Drawing.Point(30, 200);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(87, 40);
			this.Command1.TabIndex = 4;
			this.Command1.Text = "OK";
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 70);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(436, 46);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "SELECTING BASED ON CURRENT AMOUNTS WILL ALLOW YOU TO DO AN AUDIT WITHOUT CHANGING" + " ANY VALUES.  SELECTING BASED ON BILLING WILL AFFECT BILLING AMOUNTS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(428, 31);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "DO YOU WISH TO CALCULATE EXEMPTIONS BASED ON BILLING AMOUNTS OR CURRENT AMOUNTS? " + "";
			// 
			// frmWhichExemptions
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(490, 539);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmWhichExemptions";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = " Billing or Correlated Amounts";
			this.Load += new EventHandler(this.frmWhichExemptions_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
