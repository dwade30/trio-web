﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cCommercialLocationMultiplier
	{
		//=========================================================
		private int lngRecordID;
		private int lngLocationID;
		private double dblLocationMultiplier;
		private int intClassCategory;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int CommercialLocationID
		{
			set
			{
				lngLocationID = value;
				IsUpdated = true;
			}
			get
			{
				int CommercialLocationID = 0;
				CommercialLocationID = lngLocationID;
				return CommercialLocationID;
			}
		}

		public double Multiplier
		{
			set
			{
				dblLocationMultiplier = value;
			}
			get
			{
				double Multiplier = 0;
				Multiplier = dblLocationMultiplier;
				return Multiplier;
			}
		}

		public short ClassCategory
		{
			set
			{
				intClassCategory = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short ClassCategory = 0;
				ClassCategory = FCConvert.ToInt16(intClassCategory);
				return ClassCategory;
			}
		}
	}
}
