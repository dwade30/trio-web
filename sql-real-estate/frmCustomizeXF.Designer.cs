//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	partial class frmCustomizeXF
	{
		public fecherFoundation.FCComboBox cmbVisionAddress;
		public fecherFoundation.FCLabel lblVisionAddress;
		public fecherFoundation.FCComboBox cmbSplitValuation;
		public fecherFoundation.FCLabel lblSplitValuation;
		public fecherFoundation.FCComboBox cmbOwner;
		public fecherFoundation.FCComboBox cmbDivisions;
		public fecherFoundation.FCComboBox cmbMatch;
		public fecherFoundation.FCComboBox cmbPPMatch;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCCheckBox chkPreviousOwners;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkOmitTrailingZeroes;
		public fecherFoundation.FCCheckBox chkLot;
		public fecherFoundation.FCCheckBox chkUnit;
		public fecherFoundation.FCCheckBox chkBlock;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkMultiOwner;
		public fecherFoundation.FCCheckBox chkCRLF;
		public fecherFoundation.FCTextBox txtExtraCharacters;
		public fecherFoundation.FCTextBox txtBlankRecords;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtPPExtraCharacters;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbVisionAddress = new fecherFoundation.FCComboBox();
            this.lblVisionAddress = new fecherFoundation.FCLabel();
            this.cmbSplitValuation = new fecherFoundation.FCComboBox();
            this.lblSplitValuation = new fecherFoundation.FCLabel();
            this.cmbOwner = new fecherFoundation.FCComboBox();
            this.cmbDivisions = new fecherFoundation.FCComboBox();
            this.cmbMatch = new fecherFoundation.FCComboBox();
            this.cmbPPMatch = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.Frame9 = new fecherFoundation.FCFrame();
            this.chkPreviousOwners = new fecherFoundation.FCCheckBox();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.chkOmitTrailingZeroes = new fecherFoundation.FCCheckBox();
            this.chkLot = new fecherFoundation.FCCheckBox();
            this.chkUnit = new fecherFoundation.FCCheckBox();
            this.chkBlock = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkMultiOwner = new fecherFoundation.FCCheckBox();
            this.chkCRLF = new fecherFoundation.FCCheckBox();
            this.txtExtraCharacters = new fecherFoundation.FCTextBox();
            this.txtBlankRecords = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtPPExtraCharacters = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousOwners)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOmitTrailingZeroes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMultiOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCRLF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(758, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Size = new System.Drawing.Size(758, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(758, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbVisionAddress
            // 
            this.cmbVisionAddress.AutoSize = false;
            this.cmbVisionAddress.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbVisionAddress.FormattingEnabled = true;
            this.cmbVisionAddress.Items.AddRange(new object[] {
            "Use address from Vision",
            "Use address in TRIO system"});
            this.cmbVisionAddress.Location = new System.Drawing.Point(137, 630);
            this.cmbVisionAddress.Name = "cmbVisionAddress";
            this.cmbVisionAddress.Size = new System.Drawing.Size(443, 40);
            this.cmbVisionAddress.TabIndex = 2;
            this.cmbVisionAddress.Text = "Use address from Vision";
            this.ToolTip1.SetToolTip(this.cmbVisionAddress, null);
            // 
            // lblVisionAddress
            // 
            this.lblVisionAddress.AutoSize = true;
            this.lblVisionAddress.Location = new System.Drawing.Point(20, 644);
            this.lblVisionAddress.Name = "lblVisionAddress";
            this.lblVisionAddress.Size = new System.Drawing.Size(60, 15);
            this.lblVisionAddress.TabIndex = 3;
            this.lblVisionAddress.Text = "PARTIES";
            this.ToolTip1.SetToolTip(this.lblVisionAddress, null);
            // 
            // cmbSplitValuation
            // 
            this.cmbSplitValuation.AutoSize = false;
            this.cmbSplitValuation.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSplitValuation.FormattingEnabled = true;
            this.cmbSplitValuation.Items.AddRange(new object[] {
            "Save land, building , exempt values as % of property total",
            "Save land, building , exempt values as property total"});
            this.cmbSplitValuation.Location = new System.Drawing.Point(136, 580);
            this.cmbSplitValuation.Name = "cmbSplitValuation";
            this.cmbSplitValuation.Size = new System.Drawing.Size(444, 40);
            this.cmbSplitValuation.TabIndex = 0;
            this.cmbSplitValuation.Text = "Save land, building , exempt values as % of property total";
            this.ToolTip1.SetToolTip(this.cmbSplitValuation, null);
            // 
            // lblSplitValuation
            // 
            this.lblSplitValuation.AutoSize = true;
            this.lblSplitValuation.Location = new System.Drawing.Point(20, 594);
            this.lblSplitValuation.Name = "lblSplitValuation";
            this.lblSplitValuation.Size = new System.Drawing.Size(96, 15);
            this.lblSplitValuation.TabIndex = 1;
            this.lblSplitValuation.Text = "MULTI-OWNER";
            this.ToolTip1.SetToolTip(this.lblSplitValuation, null);
            // 
            // cmbOwner
            // 
            this.cmbOwner.AutoSize = false;
            this.cmbOwner.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbOwner.FormattingEnabled = true;
            this.cmbOwner.Items.AddRange(new object[] {
            "Owner as of April 1st",
            "Old owner C/O new owner",
            "Current Owner"});
            this.cmbOwner.Location = new System.Drawing.Point(20, 30);
            this.cmbOwner.Name = "cmbOwner";
            this.cmbOwner.Size = new System.Drawing.Size(342, 40);
            this.cmbOwner.TabIndex = 18;
            this.cmbOwner.Text = "Owner as of April 1st";
            this.ToolTip1.SetToolTip(this.cmbOwner, null);
            // 
            // cmbDivisions
            // 
            this.cmbDivisions.AutoSize = false;
            this.cmbDivisions.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDivisions.FormattingEnabled = true;
            this.cmbDivisions.Items.AddRange(new object[] {
            "Divisions are 3 characters (001-001)",
            "Divisions are 4 characters (0001-0001)",
            "Don\'t format (Divisions are 1 character min.)"});
            this.cmbDivisions.Location = new System.Drawing.Point(135, 30);
            this.cmbDivisions.Name = "cmbDivisions";
            this.cmbDivisions.Size = new System.Drawing.Size(405, 40);
            this.cmbDivisions.TabIndex = 11;
            this.cmbDivisions.Text = "Divisions are 3 characters (001-001)";
            this.ToolTip1.SetToolTip(this.cmbDivisions, null);
            // 
            // cmbMatch
            // 
            this.cmbMatch.AutoSize = false;
            this.cmbMatch.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMatch.FormattingEnabled = true;
            this.cmbMatch.Items.AddRange(new object[] {
            "Match TRIO Account",
            "Match Account",
            "Match Map Lot"});
            this.cmbMatch.Location = new System.Drawing.Point(447, 30);
            this.cmbMatch.Name = "cmbMatch";
            this.cmbMatch.Size = new System.Drawing.Size(221, 40);
            this.cmbMatch.TabIndex = 4;
            this.cmbMatch.Text = "Match Map Lot";
            this.ToolTip1.SetToolTip(this.cmbMatch, null);
            // 
            // cmbPPMatch
            // 
            this.cmbPPMatch.AutoSize = false;
            this.cmbPPMatch.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPPMatch.FormattingEnabled = true;
            this.cmbPPMatch.Items.AddRange(new object[] {
            "Match Open 1 Field",
            "Match TRIO Account"});
            this.cmbPPMatch.Location = new System.Drawing.Point(420, 30);
            this.cmbPPMatch.Name = "cmbPPMatch";
            this.cmbPPMatch.Size = new System.Drawing.Size(248, 40);
            this.cmbPPMatch.TabIndex = 30;
            this.cmbPPMatch.Text = "Match Open 1 Field";
            this.ToolTip1.SetToolTip(this.cmbPPMatch, null);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxLeftBorder";
            this.Frame1.Controls.Add(this.cmbSplitValuation);
            this.Frame1.Controls.Add(this.lblSplitValuation);
            this.Frame1.Controls.Add(this.cmbVisionAddress);
            this.Frame1.Controls.Add(this.lblVisionAddress);
            this.Frame1.Controls.Add(this.cmbMatch);
            this.Frame1.Controls.Add(this.Frame9);
            this.Frame1.Controls.Add(this.Frame5);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(688, 690);
            this.Frame1.TabIndex = 22;
            this.Frame1.Text = "Real Estate";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // Frame9
            // 
            this.Frame9.Controls.Add(this.chkPreviousOwners);
            this.Frame9.Controls.Add(this.cmbOwner);
            this.Frame9.Location = new System.Drawing.Point(20, 434);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(382, 136);
            this.Frame9.TabIndex = 32;
            this.Frame9.Text = "Owner Information";
            this.ToolTip1.SetToolTip(this.Frame9, "Save owner as owner as of April 1st, as old owner c/o new owner, or use current o" +
        "wner info only");
            // 
            // chkPreviousOwners
            // 
            this.chkPreviousOwners.Location = new System.Drawing.Point(20, 85);
            this.chkPreviousOwners.Name = "chkPreviousOwners";
            this.chkPreviousOwners.Size = new System.Drawing.Size(342, 27);
            this.chkPreviousOwners.TabIndex = 17;
            this.chkPreviousOwners.Text = "Import sale records as previous owner data";
            this.ToolTip1.SetToolTip(this.chkPreviousOwners, null);
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.chkOmitTrailingZeroes);
            this.Frame5.Controls.Add(this.cmbDivisions);
            this.Frame5.Controls.Add(this.chkLot);
            this.Frame5.Controls.Add(this.chkUnit);
            this.Frame5.Controls.Add(this.chkBlock);
            this.Frame5.Location = new System.Drawing.Point(20, 249);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(560, 175);
            this.Frame5.TabIndex = 27;
            this.Frame5.Text = "Map Lot";
            this.ToolTip1.SetToolTip(this.Frame5, null);
            // 
            // chkOmitTrailingZeroes
            // 
            this.chkOmitTrailingZeroes.Location = new System.Drawing.Point(20, 133);
            this.chkOmitTrailingZeroes.Name = "chkOmitTrailingZeroes";
            this.chkOmitTrailingZeroes.Size = new System.Drawing.Size(487, 27);
            this.chkOmitTrailingZeroes.TabIndex = 10;
            this.chkOmitTrailingZeroes.Text = "Omit Trailing Zeroes               (001-001 instead of 001-001-000)";
            this.ToolTip1.SetToolTip(this.chkOmitTrailingZeroes, null);
            // 
            // chkLot
            // 
            this.chkLot.Location = new System.Drawing.Point(20, 63);
            this.chkLot.Name = "chkLot";
            this.chkLot.Size = new System.Drawing.Size(83, 27);
            this.chkLot.TabIndex = 8;
            this.chkLot.Text = "Use Lot";
            this.ToolTip1.SetToolTip(this.chkLot, null);
            // 
            // chkUnit
            // 
            this.chkUnit.Location = new System.Drawing.Point(20, 99);
            this.chkUnit.Name = "chkUnit";
            this.chkUnit.Size = new System.Drawing.Size(90, 27);
            this.chkUnit.TabIndex = 9;
            this.chkUnit.Text = "Use Unit";
            this.ToolTip1.SetToolTip(this.chkUnit, null);
            // 
            // chkBlock
            // 
            this.chkBlock.Location = new System.Drawing.Point(20, 30);
            this.chkBlock.Name = "chkBlock";
            this.chkBlock.Size = new System.Drawing.Size(101, 27);
            this.chkBlock.TabIndex = 7;
            this.chkBlock.Text = "Use Block";
            this.ToolTip1.SetToolTip(this.chkBlock, null);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkMultiOwner);
            this.Frame2.Controls.Add(this.chkCRLF);
            this.Frame2.Controls.Add(this.txtExtraCharacters);
            this.Frame2.Controls.Add(this.txtBlankRecords);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Location = new System.Drawing.Point(20, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(407, 209);
            this.Frame2.TabIndex = 24;
            this.Frame2.Text = "File Format";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // chkMultiOwner
            // 
            this.chkMultiOwner.Location = new System.Drawing.Point(20, 167);
            this.chkMultiOwner.Name = "chkMultiOwner";
            this.chkMultiOwner.Size = new System.Drawing.Size(237, 27);
            this.chkMultiOwner.TabIndex = 3;
            this.chkMultiOwner.Text = "Use Multi-Owner File Format";
            this.ToolTip1.SetToolTip(this.chkMultiOwner, null);
            // 
            // chkCRLF
            // 
            this.chkCRLF.Location = new System.Drawing.Point(20, 30);
            this.chkCRLF.Name = "chkCRLF";
            this.chkCRLF.Size = new System.Drawing.Size(235, 27);
            this.chkCRLF.TabIndex = 0;
            this.chkCRLF.Text = "Records separated by CRLF";
            this.ToolTip1.SetToolTip(this.chkCRLF, null);
            // 
            // txtExtraCharacters
            // 
            this.txtExtraCharacters.AutoSize = false;
            this.txtExtraCharacters.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtraCharacters.LinkItem = null;
            this.txtExtraCharacters.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtExtraCharacters.LinkTopic = null;
            this.txtExtraCharacters.Location = new System.Drawing.Point(20, 67);
            this.txtExtraCharacters.Name = "txtExtraCharacters";
            this.txtExtraCharacters.Size = new System.Drawing.Size(60, 40);
            this.txtExtraCharacters.TabIndex = 1;
            this.txtExtraCharacters.Text = "0";
            this.ToolTip1.SetToolTip(this.txtExtraCharacters, null);
            // 
            // txtBlankRecords
            // 
            this.txtBlankRecords.AutoSize = false;
            this.txtBlankRecords.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlankRecords.LinkItem = null;
            this.txtBlankRecords.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBlankRecords.LinkTopic = null;
            this.txtBlankRecords.Location = new System.Drawing.Point(20, 117);
            this.txtBlankRecords.Name = "txtBlankRecords";
            this.txtBlankRecords.Size = new System.Drawing.Size(60, 40);
            this.txtBlankRecords.TabIndex = 2;
            this.txtBlankRecords.Text = "0";
            this.ToolTip1.SetToolTip(this.txtBlankRecords, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(117, 81);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(270, 16);
            this.Label1.TabIndex = 26;
            this.Label1.Text = "EXTRA CHARACTERS (PAST END OF RECORD)";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(117, 131);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(270, 16);
            this.Label2.TabIndex = 25;
            this.Label2.Text = "BLANK RECORDS AT BEGINNING OF FILE";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Frame4
            // 
            this.Frame4.AppearanceKey = "groupBoxLeftBorder";
            this.Frame4.Controls.Add(this.Frame7);
            this.Frame4.Controls.Add(this.cmbPPMatch);
            this.Frame4.Location = new System.Drawing.Point(30, 730);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(688, 139);
            this.Frame4.TabIndex = 21;
            this.Frame4.Text = "Personal Property";
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtPPExtraCharacters);
            this.Frame7.Controls.Add(this.Label3);
            this.Frame7.Location = new System.Drawing.Point(20, 30);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(381, 90);
            this.Frame7.TabIndex = 29;
            this.Frame7.Text = "File Format";
            this.ToolTip1.SetToolTip(this.Frame7, null);
            // 
            // txtPPExtraCharacters
            // 
            this.txtPPExtraCharacters.AutoSize = false;
            this.txtPPExtraCharacters.BackColor = System.Drawing.SystemColors.Window;
            this.txtPPExtraCharacters.LinkItem = null;
            this.txtPPExtraCharacters.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPPExtraCharacters.LinkTopic = null;
            this.txtPPExtraCharacters.Location = new System.Drawing.Point(20, 30);
            this.txtPPExtraCharacters.Name = "txtPPExtraCharacters";
            this.txtPPExtraCharacters.Size = new System.Drawing.Size(60, 40);
            this.txtPPExtraCharacters.TabIndex = 18;
            this.txtPPExtraCharacters.Text = "0";
            this.ToolTip1.SetToolTip(this.txtPPExtraCharacters, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(102, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(270, 16);
            this.Label3.TabIndex = 30;
            this.Label3.Text = "EXTRA CHARACTERS (PAST END OF RECORD)";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(280, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmCustomize
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(758, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCustomize";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmCustomize_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomize_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousOwners)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOmitTrailingZeroes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMultiOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCRLF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}