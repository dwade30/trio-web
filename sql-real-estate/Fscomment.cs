﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmFSComment.
	/// </summary>
	public partial class frmFSComment : BaseForm
	{
		public frmFSComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFSComment InstancePtr
		{
			get
			{
				return (frmFSComment)Sys.GetInstance(typeof(frmFSComment));
			}
		}

		protected frmFSComment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// vbPorter upgrade warning: InsertModeOn As FixedString	OnWrite(string)
		char InsertModeOn;
		int[] CurrentComment = new int[50 + 1];
		int CurrX;
		string NoAdd = "";
		int intCommentTotal;
		int intWhereFrom;
		bool boolEditable;
		// vbPorter upgrade warning: intScreenID As short	OnWriteFCConvert.ToInt32(
		public void Init(int intScreenID)
		{
			intWhereFrom = intScreenID;
			this.Show(App.MainForm);
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			rtbC1.Text = "";
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}
		
		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			rptComments.InstancePtr.Init(rtbC1.Text + " ", ref modGNBas.Statics.gintCardNumber, ref modGlobalVariables.Statics.gintLastAccountNumber);
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void cmdQuit_Click()
		{
			// Close #35
			// Close #36
			this.Unload();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			SaveCC();
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void SaveCC()
		{
			string Msg = "";
			try
			{
				// On Error GoTo ErrorRoutine
				clsDRWrapper temp = modDataTypes.Statics.CC;
				modREMain.OpenCCTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.CC = temp;
				if (Strings.Trim(rtbC1.Text + "") != "")
				{
					modDataTypes.Statics.CC.Set_Fields("ccOMMENT", rtbC1.Text + " ");
					modDataTypes.Statics.CC.Set_Fields("crecordNumber", modGlobalVariables.Statics.gintLastAccountNumber);
					modDataTypes.Statics.CC.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.CC.Update();
				}
				else
				{
					modDataTypes.Statics.CC.Delete();
                    modDataTypes.Statics.CC.Update();
                }
				// intCommentTotal = GetRecordCount("COMMREC", "CRecordNumber")
				// lblCommentAccount.Caption = "Comment " & CurrX & " of " & intCommentTotal & " for Account " & gintLastAccountNumber
				lblCommentAccount.Text = "Comment for Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber);
				MessageBox.Show("Comment Saved", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				/*? On Error GoTo 0 */
				return;
			}
			catch
			{
				// ErrorRoutine:
				modGlobalRoutines.ErrorRoutine();
				/*? Resume; */
			}
		}

		private void GetComment()
		{
			clsDRWrapper temp = modDataTypes.Statics.CC;
			modREMain.OpenCCTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.CC = temp;
			lblCommentAccount.Text = "Comment for Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber);
			rtbC1.Text = Strings.Trim(modDataTypes.Statics.CC.Get_Fields_String("ccomment") + " ");
			// If CurrX = 1 Then
			// cmdPrevious.Visible = False
			// mnuPrevious.Enabled = False
			// Else
			// cmdPrevious.Visible = True
			// mnuPrevious.Enabled = True
			// End If
			// If CurrX >= intCommentTotal Then
			// cmdNext.Caption = "&Add Page"
			// Else
			// cmdNext.Caption = "&Next"
			// End If
			// On Error GoTo 0
			return;
			ErrorRoutine:
			;
			modGlobalRoutines.ErrorRoutine();
			/*? Resume; */
		}

		private void BuildComment()
		{
			// lblCommentAccount.Caption = "Comment " & CurrX & " of " & intCommentTotal & " for Account " & gintLastAccountNumber
			lblCommentAccount.Text = "Comment for Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber);
			rtbC1.Text = string.Empty;
			// If CurrX = 1 Then
			// cmdPrevious.Visible = False
			// mnuPrevious.Enabled = False
			// Else
			// cmdPrevious.Visible = True
			// mnuPrevious.Enabled = True
			// End If
			// 
			// If CurrX >= intCommentTotal Then
			// cmdNext.Caption = "&Add Page"
			// Else
			// cmdNext.Caption = "&Next"
			// End If
		}

		private void frmFSComment_Activated(object sender, System.EventArgs e)
		{
			// If Not ValidPermissions(Me, CNSTSAVEACCOUNTS, False) Then
			if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTCOMMENTS, false))
			{
				cmdDelete.Enabled = false;
				cmdSave.Enabled = false;
				//mnuSaveExit.Enabled = false;
				rtbC1.Locked = true;
			}
			if (modMDIParent.FormExist(this))
				return;
			InsertModeOn = 'Y';
			rtbC1.Focus();
		}

		private void frmFSComment_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				// Close #35
				KeyCode = (Keys)0;
				frmFSComment.InstancePtr.Unload();
			}
		}

		private void frmFSComment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFSComment properties;
			//frmFSComment.ScaleWidth	= 9045;
			//frmFSComment.ScaleHeight	= 7110;
			//frmFSComment.LinkTopic	= "Form1";
			//frmFSComment.LockControls	= true;
			//rtbC1 properties;
			//rtbC1.ScrollBars	= 2;
			//rtbC1.AutoVerbMenu	= true;
			//rtbC1.TextRTF	= $"Fscomment.frx":058A;
			//End Unmaped Properties
			// 
			// CurrX = 1
			// intCommentTotal = GetRecordCount("COMMREC", "CRecordNumber")
			GetComment();
			modGlobalFunctions.SetFixedSize(this);
			//Application.DoEvents();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			switch (intWhereFrom)
			{
				case modGlobalVariables.CNSTSHORTSCREEN:
					{
						if (modGlobalRoutines.Formisloaded_2("frmSHREBLUpdate"))
						{
							frmSHREBLUpdate.InstancePtr.Focus();
						}
						break;
					}
				case modGlobalVariables.CNSTLONGSCREEN:
					{
						if (modGlobalRoutines.Formisloaded_2("frmnewreproperty"))
						{
							frmNewREProperty.InstancePtr.Focus();
						}
						break;
					}
			}
			//end switch
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}
		// Private Sub mnuNext_Click()
		// Call cmdNext_Click
		// End Sub
		//
		// Private Sub mnuPrevious_Click()
		// Call cmdPrevious_Click
		// End Sub
		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdQuit_Click();
		}
		// Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
		// mnuPrint.Visible = False
		// End Sub
		// Private Sub mnuCopy_Click()
		// Clipboard.SetText (rtbC1.SelText)
		// End Sub
		// Private Sub mnuPaste_Click()
		// rtbC1.Text = Clipboard.GetText()
		// End Sub
		// Private Sub mnuPrintCurrent_Click()
		// Open "LPT1" For Output As #40 Len = 80
		// Print #40, rtbC1.Text
		// Close #40
		// End Sub
		//
		// Private Sub mnuPrintSelectedText_Click()
		// Open "LPT1" For Output As #40 Len = 80
		// Print #40, rtbC1.SelText
		// Close #40
		// End Sub
		private void rtbC1_Change()
		{
			if (InsertModeOn == 'N')
				rtbC1.SelLength = 1;
		}

		private void rtbC1_KeyDown(ref short KeyCode, ref short Shift)
		{
			if (KeyCode == FCConvert.ToInt16(Keys.Insert))
			{
				if (InsertModeOn == 'Y')
				{
					InsertModeOn = 'N';
					rtbC1.SelLength = 1;
				}
				else
				{
					InsertModeOn = 'Y';
					rtbC1.SelLength = 0;
				}
			}
		}
		// Private Sub rtbC1_KeyUp(KeyCode As Integer, Shift As Integer)
		// If KeyCode = 93 Then PopupMenu mnuPrint
		//
		// End Sub
		// Private Sub rtbC1_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
		// If Button = 2 Then
		// If rtbC1.SelText <> "" Then mnuPrintSelectedText.Enabled = True
		// PopupMenu mnuPrint
		// End If
		// End Sub
	}
}
