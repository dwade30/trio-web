﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewCostFiles.
	/// </summary>
	public partial class frmNewCostFiles : BaseForm
	{
		public frmNewCostFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNewCostFiles InstancePtr
		{
			get
			{
				return (frmNewCostFiles)Sys.GetInstance(typeof(frmNewCostFiles));
			}
		}

		protected frmNewCostFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intNumCols;
		int intNumVisCols;
		const int COLCODE = 0;
		const int COLDESC = 1;
		const int COLSDESC = 2;
		const int COLUNIT = 3;
		const int COLBASE = 4;
		const int COLAUTOID = 5;
		bool boolChanges;
		string strLastCategory = "";
		string strLastType = "";
		int lngLastTownCode;
		int lngCurrentTownNumber;
		string strStartType;
		string strStartCat;

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void cmbCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (Strings.UCase(strLastCategory) == Strings.UCase(cmbCategory.Text))
				return;
			if (Strings.UCase(cmbCategory.Text) == "DWELLING")
			{
				cmbType.Clear();
				cmbType.AddItem("Economic Code");
				cmbType.AddItem("Exterior");
				cmbType.AddItem("Heat");
				cmbType.AddItem("Style");
				cmbType.SelectedIndex = 0;
			}
			else if (Strings.UCase(cmbCategory.Text) == "PROPERTY")
			{
				cmbType.Clear();
				cmbType.AddItem("Neighborhood");
				cmbType.SelectedIndex = 0;
			}
		}

		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (Strings.UCase(strLastType) == Strings.UCase(cmbType.Text))
				return;
			LoadCodes();
			SetDescriptions();
			strLastCategory = cmbCategory.Text;
			strLastType = cmbType.Text;
		}

		private void frmNewCostFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						return;
					}
			}
			//end switch
		}

		public void Init(string strCategory, string strType)
		{
			int x;
			strLastCategory = "";
			strLastType = "";
			lngLastTownCode = 0;
			strStartCat = strCategory;
			strStartType = strType;
			this.Text = strCategory + " " + strType;
			//FC:FINAL:CHN - issue #1609: Neighborhood Form redesign.
			this.HeaderText.Text = strType;
			// this.Show(FCForm.FormShowEnum.Modal);
			this.Show(App.MainForm);
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			// DoEvents
			// If lngLastTownCode = Val(gridTownCode.TextMatrix(0, 0)) Then Exit Sub
			// lngLastTownCode = Val(gridTownCode.TextMatrix(0, 0))
			// LoadCodes
			// SetDescriptions
			// strLastCategory = cmbCategory.Text
			// lngLastTownCode = Val(gridTownCode.TextMatrix(0, 0))
			// MsgBox "Town code = " & Val(gridTownCode.TextMatrix(0, 0)) & " last code = " & lngLastTownCode
			gridTownCode.EndEdit();
			//Application.DoEvents();
			cmbType.Focus();
			//Application.DoEvents();
		}

		private void gridTownCode_Leave(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			// MsgBox "Town code = " & Val(gridTownCode.TextMatrix(0, 0)) & " last code = " & lngLastTownCode
			if (lngLastTownCode == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
				return;
			LoadCodes();
			// SetDescriptions
			// strLastCategory = cmbCategory.Text
			lngLastTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void frmNewCostFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNewCostFiles properties;
			//frmNewCostFiles.ScaleWidth	= 9225;
			//frmNewCostFiles.ScaleHeight	= 7785;
			//frmNewCostFiles.LinkTopic	= "Form1";
			//frmNewCostFiles.LockControls	= true;
			//End Unmaped Properties
			int x;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			cmbCategory.Clear();
			cmbCategory.AddItem("Dwelling");
			cmbCategory.AddItem("Property");
			cmbCategory.SelectedIndex = 0;
			cmbType.SelectedIndex = 0;
			intNumCols = 6;
			SetupGrid();
			SetupGridTownCode();
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
			intNumVisCols = 4;
			boolChanges = false;
			SetDescriptions();
			LoadCodes();
			for (x = 0; x <= cmbCategory.Items.Count - 1; x++)
			{
				if (Strings.UCase(cmbCategory.Items[x].ToString()) == Strings.UCase(strStartCat))
				{
					cmbCategory.SelectedIndex = x;
					break;
				}
			}
			// x
			//Application.DoEvents();
			for (x = 0; x <= cmbType.Items.Count - 1; x++)
			{
				if (Strings.UCase(cmbType.Items[x].ToString()) == Strings.UCase(strStartType))
				{
					cmbType.SelectedIndex = x;
					break;
				}
			}
			// x
		}

		private void frmNewCostFiles_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridTownCode();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
			// mark as edited
			boolChanges = true;
			if (Conversion.Val(Grid.TextMatrix(Grid.Row, COLUNIT)) < 0)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, COLUNIT, modGlobalConstants.Statics.TRIOCOLORRED);
			}
			else
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, COLUNIT, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
			if (Conversion.Val(Grid.TextMatrix(Grid.Row, COLBASE)) < 0)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, COLBASE, modGlobalConstants.Statics.TRIOCOLORRED);
			}
			else
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, COLBASE, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			Grid.Rows += 1;
			Grid.RowData(Grid.Rows - 1, false);
			Grid.TextMatrix(Grid.Rows - 1, COLUNIT, "0.00");
			Grid.TextMatrix(Grid.Rows - 1, COLBASE, "0.00");
			Grid.TopRow = Grid.Rows - 1;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			GridDeleted.AddItem(Grid.TextMatrix(Grid.Row, COLAUTOID));
			// add the id of this item
			Grid.RemoveItem(Grid.Row);
		}

		private void SetupGrid()
		{
			Grid.Cols = intNumCols;
			Grid.ColHidden(COLAUTOID, true);
			Grid.TextMatrix(0, COLCODE, "Code");
			Grid.TextMatrix(0, COLDESC, "Description");
			Grid.TextMatrix(0, COLSDESC, "Short Description");
			Grid.TextMatrix(0, COLUNIT, "Unit");
			Grid.TextMatrix(0, COLBASE, "Base");
			//FC:FINAL:CHN - issue #1609: Neighborhood Form redesign.
			Grid.ColAlignment(COLCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(COLSDESC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, COLCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, COLDESC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, COLSDESC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, COLUNIT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, COLBASE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			if (Strings.UCase(cmbCategory.Text) == "DWELLING")
			{
				if ((Strings.UCase(cmbType.Text) == "STYLE") || (Strings.UCase(cmbType.Text) == "EXTERIOR") || (Strings.UCase(cmbType.Text) == "ECONOMIC CODE"))
				{
					intNumVisCols = 4;
					Grid.TextMatrix(0, COLUNIT, "Factor");
					Grid.ColWidth(COLUNIT, FCConvert.ToInt32(0.12 * GridWidth));
					Grid.ColHidden(COLBASE, true);
					Grid.ColHidden(COLUNIT, false);
				}
				else if (Strings.UCase(cmbType.Text) == "HEAT")
				{
					intNumVisCols = 5;
					Grid.TextMatrix(0, COLUNIT, "Unit");
					Grid.TextMatrix(0, COLBASE, "Base");
					Grid.ColWidth(COLUNIT, FCConvert.ToInt32(0.1 * GridWidth));
					Grid.ColWidth(COLBASE, FCConvert.ToInt32(0.1 * GridWidth));
					Grid.ColHidden(COLUNIT, false);
					Grid.ColHidden(COLBASE, false);
				}
			}
			else if (Strings.UCase(cmbCategory.Text) == "PROPERTY")
			{
				if (Strings.UCase(cmbType.Text) == "NEIGHBORHOOD")
				{
					intNumVisCols = 3;
					Grid.ColHidden(COLUNIT, true);
					Grid.ColHidden(COLBASE, true);
				}
			}
			switch (intNumVisCols)
			{
				case 3:
					{
						Grid.ColWidth(COLCODE, FCConvert.ToInt32(0.1 * GridWidth));
						Grid.ColWidth(COLDESC, FCConvert.ToInt32(0.6 * GridWidth));
						break;
					}
				case 4:
					{
						Grid.ColWidth(COLCODE, FCConvert.ToInt32(0.1 * GridWidth));
						Grid.ColWidth(COLDESC, FCConvert.ToInt32(0.5 * GridWidth));
						Grid.ColWidth(COLSDESC, FCConvert.ToInt32(0.24 * GridWidth));
						break;
					}
				case 5:
					{
						Grid.ColWidth(COLCODE, FCConvert.ToInt32(0.1 * GridWidth));
						Grid.ColWidth(COLDESC, FCConvert.ToInt32(0.42 * GridWidth));
						Grid.ColWidth(COLSDESC, FCConvert.ToInt32(0.24 * GridWidth));
						break;
					}
			}
			//end switch
		}

		private void SetDescriptions()
		{
			ResizeGrid();
			if (Strings.UCase(cmbCategory.Text) == "DWELLING")
			{
				if ((Strings.UCase(cmbType.Text) == "STYLE") || (Strings.UCase(cmbType.Text) == "EXTERIOR") || (Strings.UCase(cmbType.Text) == "ECONOMIC CODE"))
				{
					Grid.TextMatrix(0, COLUNIT, "Factor");
				}
				else if (Strings.UCase(cmbType.Text) == "HEAT")
				{
					Grid.TextMatrix(0, COLUNIT, "Unit");
					Grid.TextMatrix(0, COLBASE, "Base");
				}
			}
		}

		private void LoadCodes()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			int lngTownCode = 0;
			//Application.DoEvents();
			if (GridDeleted.Rows > 0 || boolChanges)
			{
				intResponse = MessageBox.Show("Changes have been made.  Do you want to save first?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.Cancel)
					return;
				if (intResponse == DialogResult.Yes)
				{
					SaveCodes();
				}
			}
			GridDeleted.Rows = 0;
			boolChanges = false;
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			}
			else
			{
				lngTownCode = 0;
			}
			this.Text = cmbCategory.Text + " " + cmbType.Text;
			//FC:FINAL:CHN - issue #1609: Neighborhood Form redesign.
			this.HeaderText.Text = cmbType.Text;
			Grid.Rows = 1;
			if (Strings.UCase(cmbCategory.Text) == "DWELLING")
			{
				if (Strings.UCase(cmbType.Text) == "STYLE")
				{
					clsLoad.OpenRecordset("select * from dwellingstyle where isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode) + " order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortdescription") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("factor"))) + "\t" + "\t" + clsLoad.Get_Fields_Int32("id"));
						Grid.RowData(Grid.Rows - 1, false);
						if (Conversion.Val(clsLoad.Get_Fields_Double("factor")) < 0)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Rows - 1, COLUNIT, modGlobalConstants.Statics.TRIOCOLORRED);
						}
						clsLoad.MoveNext();
					}
				}
				else if (Strings.UCase(cmbType.Text) == "HEAT")
				{
					clsLoad.OpenRecordset("select * from dwellingheat where isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode) + " order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
						Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortdescription") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("unit"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("base"))) + "\t" + clsLoad.Get_Fields_Int32("id"));
						Grid.RowData(Grid.Rows - 1, false);
						// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
						if (Conversion.Val(clsLoad.Get_Fields("unit")) < 0)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Rows - 1, COLUNIT, modGlobalConstants.Statics.TRIOCOLORRED);
						}
						// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
						if (Conversion.Val(clsLoad.Get_Fields("base")) < 0)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Rows - 1, COLBASE, modGlobalConstants.Statics.TRIOCOLORRED);
						}
						clsLoad.MoveNext();
					}
				}
				else if (Strings.UCase(cmbType.Text) == "EXTERIOR")
				{
					clsLoad.OpenRecordset("select * from dwellingexterior where isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode) + " order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortdescription") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("factor"))) + "\t" + "\t" + clsLoad.Get_Fields_Int32("id"));
						Grid.RowData(Grid.Rows - 1, false);
						if (Conversion.Val(clsLoad.Get_Fields_Double("factor")) < 0)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Rows - 1, COLUNIT, modGlobalConstants.Statics.TRIOCOLORRED);
						}
						clsLoad.MoveNext();
					}
				}
				else if (Strings.UCase(cmbType.Text) == "ECONOMIC CODE")
				{
					clsLoad.OpenRecordset("select * from economiccode where isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode) + " order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortdescription") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("factor"))) + "\t" + "\t" + clsLoad.Get_Fields_Int32("id"));
						Grid.RowData(Grid.Rows - 1, false);
						if (Conversion.Val(clsLoad.Get_Fields_Double("factor")) < 0)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Rows - 1, COLUNIT, modGlobalConstants.Statics.TRIOCOLORRED);
						}
						clsLoad.MoveNext();
					}
				}
			}
			else if (Strings.UCase(cmbCategory.Text) == "PROPERTY")
			{
				if (Strings.UCase(cmbType.Text) == "NEIGHBORHOOD")
				{
					clsLoad.OpenRecordset("select * from neighborhood where townnumber  = " + FCConvert.ToString(lngTownCode) + " order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortdescription") + "\t" + "\t" + "\t" + clsLoad.Get_Fields_Int32("id"));
						Grid.RowData(Grid.Rows - 1, false);
						clsLoad.MoveNext();
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool SaveCodes()
		{
			bool SaveCodes = false;
			string strTableName = "";
			int x;
			string strSQL = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngTownNumber;
			try
			{
				lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				modGlobalVariables.Statics.boolCostRecChanged = true;
				if (Strings.UCase(strLastCategory) == "DWELLING")
				{
					if (Strings.UCase(strLastType) == "STYLE")
					{
						strTableName = "dwellingstyle";
					}
					else if (Strings.UCase(strLastType) == "HEAT")
					{
						strTableName = "dwellingheat";
					}
					else if (Strings.UCase(strLastType) == "EXTERIOR")
					{
						strTableName = "dwellingexterior";
					}
					else if (Strings.UCase(strLastType) == "ECONOMIC CODE")
					{
						strTableName = "EconomicCode";
					}
				}
				else if (Strings.UCase(strLastCategory) == "PROPERTY")
				{
					if (Strings.UCase(strLastType) == "NEIGHBORHOOD")
					{
						strTableName = "neighborhood";
					}
				}
				// delete deleted items first
				for (x = 0; x <= GridDeleted.Rows - 1; x++)
				{
					strSQL = "delete from " + strTableName + " where id = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(x, 0)));
					clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
				}
				// x
				GridDeleted.Rows = 0;
				// now save the changes
				x = Grid.FindRow(true);
				while (x >= 0)
				{
					// if this is new then id = 0 and will never update anything so insert
					if (Conversion.Val(Grid.TextMatrix(x, COLCODE)) == 0 && Grid.TextMatrix(x, COLCODE) != "0")
					{
						// no blank codes allowed
						Grid.RemoveItem(x);
					}
					else
					{
						if (Strings.UCase(strLastCategory) == "DWELLING")
						{
							if ((Strings.UCase(strLastType) == "STYLE") || (Strings.UCase(strLastType) == "EXTERIOR") || (Strings.UCase(strLastType) == "ECONOMIC CODE"))
							{
								if (Conversion.Val(Grid.TextMatrix(x, COLAUTOID)) == 0)
								{
									// new
									strSQL = "insert into " + strTableName + " (code,Description,shortDescription,factor,townnumber) values (";
									strSQL += FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCODE))) + ",'" + Strings.Trim(Grid.TextMatrix(x, COLDESC)) + "','" + Strings.Trim(Grid.TextMatrix(x, COLSDESC)) + "'," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLUNIT))) + "," + FCConvert.ToString(lngTownNumber);
									strSQL += ")";
								}
								else
								{
									// update
									strSQL = "update " + strTableName + " set code = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCODE))) + ",description = '" + Strings.Trim(Grid.TextMatrix(x, COLDESC)) + "'";
									strSQL += ",Shortdescription = '" + Strings.Trim(Grid.TextMatrix(x, COLSDESC)) + "',factor = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLUNIT)));
									strSQL += " where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLAUTOID)));
								}
							}
							else if (Strings.UCase(strLastType) == "HEAT")
							{
								if (Conversion.Val(Grid.TextMatrix(x, COLAUTOID)) == 0)
								{
									// new
									strSQL = "insert into " + strTableName + " (code,description,shortdescription,unit,base,townnumber) values (";
									strSQL += FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCODE))) + ",'" + Strings.Trim(Grid.TextMatrix(x, COLDESC)) + "','" + Strings.Trim(Grid.TextMatrix(x, COLSDESC)) + "'," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLUNIT))) + "," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLBASE))) + "," + FCConvert.ToString(lngTownNumber);
									strSQL += ")";
								}
								else
								{
									// update
									strSQL = "update " + strTableName + " set code = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCODE))) + ",description = '" + Strings.Trim(Grid.TextMatrix(x, COLDESC)) + "'";
									strSQL += ",shortdescription = '" + Strings.Trim(Grid.TextMatrix(x, COLSDESC)) + "',unit = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLUNIT))) + ",base = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLBASE)));
									strSQL += " where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLAUTOID)));
								}
							}
						}
						else if (Strings.UCase(strLastCategory) == "PROPERTY")
						{
							if (Strings.UCase(strLastType) == "NEIGHBORHOOD")
							{
								if (Conversion.Val(Grid.TextMatrix(x, COLAUTOID)) == 0)
								{
									strSQL = "insert into " + strTableName + " (code,description,shortdescription,townnumber) values (";
									strSQL += FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCODE))) + ",'" + modGlobalFunctions.EscapeQuotes(Strings.Trim(Grid.TextMatrix(x, COLDESC))) + "','" + modGlobalFunctions.EscapeQuotes(Strings.Trim(Grid.TextMatrix(x, COLSDESC))) + "'," + FCConvert.ToString(lngTownNumber);
									strSQL += ")";
								}
								else
								{
									strSQL = "update " + strTableName + " set code = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLCODE))) + ",description = '" + modGlobalFunctions.EscapeQuotes(Strings.Trim(Grid.TextMatrix(x, COLDESC))) + "',shortdescription = '" + modGlobalFunctions.EscapeQuotes(Strings.Trim(Grid.TextMatrix(x, COLSDESC))) + "', townnumber = " + FCConvert.ToString(lngTownNumber);
									strSQL += " where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, COLAUTOID)));
								}
							}
						}
						clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
						Grid.RowData(x, false);
					}
					x = Grid.FindRow(true);
				}
				boolChanges = false;
				SaveCodes = true;
				return SaveCodes;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveCodes");
			}
			return SaveCodes;
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			rptNewCostFiles.InstancePtr.Init(strLastCategory, ref strLastType);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//Application.DoEvents();
			SaveCodes();
			//Application.DoEvents();
			MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//Application.DoEvents();
			if (SaveCodes())
			{
				//Application.DoEvents();
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				mnuExit_Click();
			}
		}
	}
}
