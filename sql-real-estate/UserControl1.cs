﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for ToolTipCtl.
	/// </summary>
	public partial class ToolTipCtl : fecherFoundation.FCUserControl
	{
		public ToolTipCtl()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			UserControl_Initialize();
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private int lngMinHeight;
		private int lngLeftBorder;
		private int lngTopBorder;
		private string strCurrentTip = string.Empty;
		private bool boolControlMouseEvent;
		// whether the mouse move event has been handled by a control or just the form
		private int lngLastX;
		private int lngLastY;
		private bool boolPersistent;
		private bool boolDisable;

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			HideTip();
		}

		private void Timer1_Tick(object sender, System.EventArgs e)
		{
			this.Extender.Visible = true;
			Timer1.Enabled = false;
		}

		private void UserControl_Initialize()
		{
			lngMinHeight = 1000;
            //FC:FINAL:MSH - issue #1633: change z-order of elements for displaying them
            lblToolTip.BringToFront();
            cmdOK.BringToFront();
		}
		// Public Sub UserControl_ReadProperties(PropBag As PropertyBag)
		// PropBag.ReadProperty("MinHeight") = lngMinHeight
		// PropBag.ReadProperty("TipText") = lblToolTip.Caption
		// End Sub
		public bool Disabled
		{
			set
			{
				boolDisable = value;
				if (boolDisable)
				{
					HideTip();
				}
			}
			get
			{
				bool Disabled = false;
				Disabled = boolDisable;
				return Disabled;
			}
		}

		public double FontSize
		{
			set
			{
				lblToolTip.Font = new Font(lblToolTip.Font.FontFamily, FCConvert.ToSingle(value));
			}
			// vbPorter upgrade warning: 'Return' As double	OnWriteFCConvert.ToSingle(
			get
			{
				double FontSize = 0;
				FontSize = lblToolTip.Font.Size;
				return FontSize;
			}
		}

		public bool Persistent
		{
			set
			{
				boolPersistent = value;
			}
			get
			{
				bool Persistent = false;
				Persistent = boolPersistent;
				return Persistent;
			}
		}
		// vbPorter upgrade warning: lngBelow As int	OnWrite(int, double)
		public void Update(string strTipName, int lngX, int lngY, string strTipText, int lngLeftOf = -1, int lngBelow = -1, int lngRightOf = -1, int lngAbove = -1, int lngMinimumHeight = 1000, bool boolStayOnMouseOver = false)
		{
			ControlMouseOver = true;
			// If boolDisable Then Exit Sub
			boolPersistent = boolStayOnMouseOver;
			// If CurrentTip <> strTipName And (lngX <> LastX Or lngY <> LastY) Then
			LastX = lngX;
			LastY = lngY;
			CurrentTip = strTipName;
			TipText = strTipText;
			this.Extender.Visible = true;
            this.BringToFront();
			// Timer1.Enabled = True
			if (lngLeftOf >= 0)
			{
				this.LeftOf = lngLeftOf;
			}
			if (lngBelow >= 0)
			{
				this.Below = lngBelow;
			}
			if (lngAbove >= 0)
			{
				this.Above = lngAbove;
			}
			if (lngRightOf >= 0)
			{
				this.RightOf = lngRightOf;
			}
			MinHeight = lngMinimumHeight;
			// End If
		}
		// Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
		// If Not boolPersistent Then
		// HideTip
		// End If
		// End Sub
		private void ToolTipCtl_Resize(object sender, System.EventArgs e)
		{
            //FC:FINAL:MSH - issue #1633: use original size params for correct calculations
			if (this.WidthOriginal > 0)
			{
				Shape1.WidthOriginal = this.WidthOriginal - Shape1.LeftOriginal;
				lblToolTip.WordWrap = true;
                //FC:FINAL:MSH - issue #1633: turning on AutoSize will trigger auto recalculating of label width (in VB6 width won't be recalculated)
				//lblToolTip.AutoSize = false;
				lblToolTip.WidthOriginal = Shape1.WidthOriginal - (2 * (lblToolTip.LeftOriginal - Shape1.LeftOriginal)) - 300;
                //FC:FINAL:AM:#1873 - measure the text to set the correct height
                Size size = System.Windows.Forms.TextRenderer.MeasureText(lblToolTip.Text, lblToolTip.Font);
                lblToolTip.Height = size.Height * (size.Width / lblToolTip.Width + 1) + 20;
				//lblToolTip.AutoSize = true;
				if (lblToolTip.HeightOriginal < 230)
				{
					lblToolTip.WordWrap = false;
					Shape1.WidthOriginal = lblToolTip.WidthOriginal + (2 * (lblToolTip.LeftOriginal - Shape1.LeftOriginal));
				}
			}
			if (this.HeightOriginal > 0)
			{
				Shape1.HeightOriginal = this.HeightOriginal - Shape1.TopOriginal;
			}
			if (lblToolTip.HeightOriginal + 315 + (lblToolTip.TopOriginal - Shape1.TopOriginal) > Shape1.HeightOriginal)
			{
				Shape1.HeightOriginal = (lblToolTip.HeightOriginal + (lblToolTip.TopOriginal - Shape1.TopOriginal)) + 315;
			}
			else if (lblToolTip.HeightOriginal + 315 + (lblToolTip.TopOriginal - Shape1.TopOriginal) < Shape1.HeightOriginal)
			{
				Shape1.HeightOriginal = (lblToolTip.HeightOriginal + (lblToolTip.TopOriginal - Shape1.TopOriginal)) + 315;
			}
			if (Shape1.HeightOriginal < lngMinHeight)
			{
				Shape1.HeightOriginal = lngMinHeight;
			}
			Shape1.HeightOriginal = Shape1.HeightOriginal + cmdOK.HeightOriginal + 60;
			cmdOK.TopOriginal = Shape1.HeightOriginal - cmdOK.HeightOriginal - 30;
			if (this.WidthOriginal > 0)
			{
				cmdOK.LeftOriginal = FCConvert.ToInt32((Shape1.WidthOriginal - cmdOK.WidthOriginal) / 2.0);
			}
			if (Shape1.HeightOriginal + Shape1.TopOriginal + 30 > this.HeightOriginal)
			{
				this.HeightOriginal = Shape1.HeightOriginal + Shape1.TopOriginal + 30;
			}
			else if (Shape1.HeightOriginal + Shape1.TopOriginal + 30 < this.HeightOriginal)
			{
				if (Shape1.HeightOriginal + Shape1.TopOriginal + 30 >= lngMinHeight)
				{
					this.HeightOriginal = Shape1.HeightOriginal + Shape1.TopOriginal + 30;
				}
				else
				{
					Shape1.HeightOriginal = lngMinHeight - Shape1.TopOriginal - 30;
					this.HeightOriginal = lngMinHeight;
				}
			}
			int lngTemp;
			// If UserControl.Extender.Left + UserControl.Width > lngLeftBorder Then
			CheckPosition();
			// End If
		}

		public void UserControl_Resize()
		{
			ToolTipCtl_Resize(this, new System.EventArgs());
		}

		public int TextBottom
		{
			get
			{
				int TextBottom = 0;
				TextBottom = lblToolTip.Top + lblToolTip.Height;
				return TextBottom;
			}
		}

		public string TipText
		{
			set
			{
				lblToolTip.Text = value;
				//Application.DoEvents();
				UserControl_Resize();
			}
			get
			{
				string TipText = "";
				TipText = lblToolTip.Text;
				lblToolTip.Refresh();
				return TipText;
			}
		}

		private void CheckPosition()
		{
			int lngTemp = 0;
			if (lngLeftBorder >= 0)
			{
				lngTemp = 0;
				if (this.Extender.Left + this.Width > lngLeftBorder)
				{
					lngTemp = lngLeftBorder - this.Width;
					if (lngTemp < 0)
						lngTemp = 0;
					this.Extender.Left = lngTemp;
				}
			}
			if (lngTopBorder >= 0)
			{
				lngTemp = 0;
				if (this.Extender.Top + this.Height > lngTopBorder)
				{
					lngTemp = lngTopBorder - this.Height;
					if (lngTemp < 0)
						lngTemp = 0;
					this.Extender.Top = lngTemp;
				}
				else if (this.Extender.Top + this.Height + 30 < lngTopBorder)
				{
					// can be a little off but > 30 is too much
					lngTemp = lngTopBorder - this.Height;
					if (lngTemp < 0)
						lngTemp = 0;
					this.Extender.Top = lngTemp;
				}
			}
		}

		public int MinHeight
		{
			set
			{
				bool boolRSize;
				boolRSize = false;
				if (value != lngMinHeight)
				{
					boolRSize = true;
				}
				lngMinHeight = value;
				if (boolRSize)
				{
					UserControl_Resize();
				}
			}
			get
			{
				int MinHeight = 0;
				MinHeight = lngMinHeight;
				return MinHeight;
			}
		}

		public void HideTip()
		{
			CurrentTip = "";
			Timer1.Enabled = false;
			this.Extender.Visible = false;
			ControlMouseOver = false;
		}

		public int LeftOf
		{
			set
			{
				int lngTemp;
				lngLeftBorder = value - 30;
				if (lngLeftBorder < 0)
					lngLeftBorder = 0;
				lngTemp = value - 30 - this.Width;
				if (lngTemp < 0)
					lngTemp = 0;
				this.Extender.Left = lngTemp;
			}
		}

		public int RightOf
		{
			set
			{
				int lngTemp;
				lngTemp = value + 30;
				this.Extender.Left = lngTemp;
				lngLeftBorder = -1;
			}
		}

		public int Below
		{
			set
			{
				int lngTemp;
				lngTemp = value + 50;
				this.Extender.Top = lngTemp;
				lngTopBorder = -1;
			}
		}

		public int Above
		{
			set
			{
				int lngTemp;
				lngTopBorder = value - 30;
				lngTemp = lngTopBorder - this.Height;
				if (lngTopBorder < 0)
					lngTopBorder = 0;
				if (lngTemp < 0)
					lngTemp = 0;
				this.Extender.Top = lngTemp;
			}
		}

		public string CurrentTip
		{
			set
			{
				strCurrentTip = value;
			}
			get
			{
				string CurrentTip = "";
				CurrentTip = strCurrentTip;
				return CurrentTip;
			}
		}

		public bool ControlMouseOver
		{
			set
			{
				boolControlMouseEvent = value;
			}
			get
			{
				bool ControlMouseOver = false;
				ControlMouseOver = boolControlMouseEvent;
				return ControlMouseOver;
			}
		}

		public int LastX
		{
			set
			{
				lngLastX = value;
			}
			get
			{
				int LastX = 0;
				LastX = lngLastX;
				return LastX;
			}
		}

		public int LastY
		{
			set
			{
				lngLastY = value;
			}
			get
			{
				int LastY = 0;
				LastY = lngLastY;
				return LastY;
			}
		}

		private void ToolTipCtl_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//ToolTipCtl properties;
			//ToolTipCtl.BackStyle	= 0;
			//ToolTipCtl.ScaleWidth	= 4800;
			//ToolTipCtl.ScaleHeight	= 2400;
			//ToolTipCtl.ControlContainer	= true;
			//End Unmaped Properties
		}
	}
}
