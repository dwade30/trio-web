﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLabels.
	/// </summary>
	public partial class rptLabels : BaseSectionReport
	{
		public FCGrid Grid;

		public static rptLabels InstancePtr
		{
			get
			{
				return (rptLabels)Sys.GetInstance(typeof(rptLabels));
			}
		}

		protected rptLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
                rsTemp = null;
				RSMster?.Dispose();
                RSMster = null;
				clsBookPage?.Dispose();
                clsBookPage = null;
            }
			base.Dispose(disposing);
		}

		public rptLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Labels";
			//FC:FINAL:AM:#i1216 - use FCGrid for the grid
			Grid = new FCGrid();
			Grid.FixedCols = 0;
			Grid.Cols = 10;
			Grid.FixedRows = 0;
			Grid.Rows = 0;
		}
		// nObj = 1
		//   0	rptLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intNumberofDuplicates;
		int intDuplicateCount;
		bool boolSaveMatrix;
		clsDRWrapper rsTemp;
		// Dim dbTemp As DAO.Database
		clsDRWrapper RSMster = new clsDRWrapper();
		clsDRWrapper clsBookPage = new clsDRWrapper();
		bool boolIncAcct;
		bool boolIncRef;
		bool boolIncAddr;
		bool boolIncAcres;
		bool boolIncSecOwner;
		bool boolIncMap;
		bool boolIncETax;
		bool boolIncRef2;
		bool boolIncLoc;
		bool boolIncOwner;
		bool boolByAccount;
		string strLine1 = "";
		string strLine2 = "";
		string strLine3 = "";
		string strLine4 = "";
		string strLine5 = "";
		string strLine6 = "";
		string[] strMLine1 = new string[3 + 1];
		string[] strMLine2 = new string[3 + 1];
		string[] strMLine3 = new string[3 + 1];
		string[] strMLine4 = new string[3 + 1];
		string[] strMLine5 = new string[3 + 1];
		string[] strMLine6 = new string[3 + 1];
		int intLabelNum;
		int intPrintType;
		int intPrintWhat;
		int intOrdBy;
		string strOrder = "";
		string strMin = "";
		string strMax = "";
		float snglTax;
		string strTaxYear;
		string strCard = "";
		bool boolUseBookPage;
		int intMaxLines;
		string strLeftBlank;
		bool boolFirstLabel;
		// are you printing the first or
		// second label of a change of assessment?
		bool boolStartedPrinting;
		bool boolElimDups;
		int lngCurRow;
		int intLabelKind;
		int hdlPrinter;
		string REC;
		string strFirstLabel;
		bool boolFirstLabelMade;
		bool EightLinesPerInch;
		bool boolShowIt;
		string strMLot1 = "";
		string strMLot2 = "";
		string strMLot3 = "";
		string strMLot4 = "";
		bool boolShowSaleData;
		string tempRec = "";
		// vbPorter upgrade warning: intCharactersWide As short --> As int	OnWrite(int, double)
		int intCharactersWide;
		float intFontSize;
		int intCPI;
		bool boolUseDotmatrix;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			string strTemp = "";
			int x;
			string strLastName = "";
			string strLastAddr1 = "";
			string strMapLot = "";
			string strZip = "";
			string strLocation = "";
			string strLocNum = "";
			// vbPorter upgrade warning: strAcres As Variant --> As string
			string strAcres = "";
			int lngAuto = 0;
			int cnt;
			int const_printtoolid = 0;
			
			int intReturn = 0;
			string strTempOrder;
			int lngID;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = RSMster.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			try
			{
				// On Error GoTo ErrorHandler
				boolUseDotmatrix = true;
				lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				boolUseBookPage = modGlobalVariables.Statics.CustomizedInfo.Ref1BookPage == 2;
				intLabelNum = 2;
				// will then be switched to 1
				boolFirstLabel = true;
				boolFirstLabelMade = true;
				strFirstLabel = "";
				strTempOrder = strOrder;
				if (Strings.UCase(strTempOrder) == "RSLOCSTREET")
				{
					strTempOrder = "rslocstreet,rslocnumalph  ";
				}
				if (!boolShowIt)
				{
					// we are here because F2 was pressed on the maintenance screen
					strLeftBlank = " ";
					intDuplicateCount = 0;
					if (boolUseBookPage)
					{
						clsBookPage.OpenRecordset("select * from bookpage where current order by account,line desc", modGlobalVariables.strREDatabase);
					}
					boolByAccount = true;
					RSMster.OpenRecordset(strMasterJoin + " where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
					intPrintWhat = 0;
					// dont want to open another recordset below
					boolIncOwner = true;
					boolIncAddr = true;
					if (!modGlobalVariables.Statics.gboolPrintAsMailing)
					{
						boolIncMap = true;
						boolIncRef = true;
						boolIncAcct = true;
						intPrintType = 6;
					}
					else
					{
						boolIncMap = false;
						boolIncRef = false;
						boolIncAcct = false;
						intPrintType = 1;
						strTemp = modRegistry.GetRegistryKey("REF2LabelCPI");
						if (Conversion.Val(strTemp) == 12)
						{
							intCPI = 12;
						}
						else if (Conversion.Val(strTemp) == 17)
						{
							intCPI = 17;
						}
						else
						{
							intCPI = 10;
						}
						strTemp = modRegistry.GetRegistryKey("REF2LabelType");
						if ((strTemp == "4014") || (strTemp == "4065"))
						{
							intCharactersWide = (intCPI * 4) - 2;
						}
						else
						{
							intCharactersWide = FCConvert.ToInt32((intCPI * 3.5) - 2);
							strLeftBlank = "";
						}
					}
					boolIncAcct = true;
					boolElimDups = false;
					const_printtoolid = 9950;
					//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
					//{
					//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
					//	{
					//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
					//		this.Toolbar.Tools(cnt).Enabled = true;
					//	}
					//} // cnt
					// Dim clsP As New clsReportPrinterFunctions
					// Dim strF As String
					// strF = clsP.GetFont(Me.FCGlobal.Printer.DeviceName, intFontSize)
					// If strF <> vbNullString Then
					// txtLabel1.Font.Name = strF
					// End If
					// 
				}
				else
				{
					if (!boolUseDotmatrix)
					{
						//if (!this.Document.Printer.PrintDialog(this.Handle.ToInt32()))
						//{
						//	this.Unload();
						//	return;
						//}
					}
					// If boolDotMatrix Then
					const_printtoolid = 9950;
					//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
					//{
					//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
					//	{
					//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
					//		this.Toolbar.Tools(cnt).Enabled = true;
					//	}
					//} // cnt
					// End If
				}
				if (TWRE0000.rptLabels.InstancePtr.Document.Printer != null)
				{
					if (boolUseDotmatrix)
					{
						//this.Document.Printer.RenderMode = 1;
					}
				}
				// If intPrintType = 4 Then
				// rptLabels.Printer.PaperWidth = 1440 * 8
				// rptLabels.Printer.PaperHeight = 1440 / 2
				// PrintWidth = 1440 * 6
				// PageBottomMargin = 0
				// PageLeftMargin = 1440 / 8
				// PageRightMargin = 0
				// Else
				// rptLabels.Printer.PaperWidth = 1440 * 8 'not actually 5 in. wide, but we're not printing the whole width anyway
				// rptLabels.Printer.PaperHeight = 1440 '* 6  'length of a fanfolded sheet
				// PrintWidth = 1440 * 5
				// PageBottomMargin = 0
				// PageTopMargin = 0
				// PageLeftMargin = 1440 / 8 '16th of an inch
				// PageRightMargin = 0
				// End If
				REC = "";
				if (intLabelKind == 4013 || intLabelKind == 4030)
				{
					// And Not boolUseDotmatrix Then
					// If EightLinesPerInch Then
					this.PageSettings.Margins.Bottom = 0;
					this.PageSettings.Margins.Top = 0;
					this.Detail.Height = 1;
					// End If
					if (EightLinesPerInch)
					{
						intMaxLines = 6;
					}
					else
					{
						intMaxLines = 5;
					}
					txtLabel1.Height = Detail.Height - 1 / 1440f;
					this.PageSettings.PaperHeight = 1;
				}
				else if (intLabelKind == 4065)
				{
					// And Not boolUseDotmatrix Then
					this.PageSettings.Margins.Bottom = 0;
					this.PageSettings.Margins.Top = 0;
					this.Detail.Height = 1;
					txtLabel1.Height = Detail.Height - 1 / 1440f;
					if (EightLinesPerInch)
					{
						intMaxLines = 6;
					}
					else
					{
						intMaxLines = 5;
					}
					this.PageSettings.PaperHeight = 1;
				}
				else if (intLabelKind == 4014)
				{
					// And Not boolUseDotmatrix Then
					this.Detail.Height = 1.5F;
					txtLabel1.Height = Detail.Height - 1 / 1440f;
					this.PageSettings.PaperHeight = 1.5F;
				}

				if ((intLabelKind == 4013 || intLabelKind == 4030) && intPrintType == 3)
				{
					txtland2.Left -= 720 / 1440f;
					txtBldg2.Left -= 720 / 1440f;
					txtExempt2.Left -= 720 / 1440f;
					txtTotal2.Left -= 720 / 1440f;
					txtTax.Left -= 720 / 1440f;
					lblLand.Left -= 720 / 1440f;
					lblBldg.Left -= 720 / 1440f;
					lblExempt.Left -= 720 / 1440f;
					lblTotal.Left -= 720 / 1440f;
				}

				if (!boolUseDotmatrix)
				{
					if (intPrintType == 4)
					{
						PrintWidth = 6;
						this.PageSettings.Margins.Bottom = 0;
						this.PageSettings.Margins.Left = FCConvert.ToSingle(1440.0 / 8 / 1440);
						this.PageSettings.Margins.Right = 0;
						Detail.Height = 720 / 1440F;
						PageSettings.PaperHeight = FCConvert.ToSingle(0.5);
					}
					else
					{
						this.PageSettings.Margins.Bottom = 0;
						this.PageSettings.Margins.Top = 0;
						this.PageSettings.Margins.Right = 0;
						this.PageSettings.Margins.Left = FCConvert.ToSingle(1440.0 / 8 / 1440);
						int lngY;
						if (intLabelKind != 4030)
						{
							PrintWidth = 5;
						}
						else
						{
							PrintWidth = 7.125F - 181 / 1440f;
						}
						if (intLabelKind != 4014)
						{
							PageSettings.PaperHeight = 1;
						}
						else
						{
							PageSettings.PaperHeight = FCConvert.ToSingle(1.5);
						}
					}
					if (intReturn > 0)
					{
						FCGlobal.Printer.PaperSize = FCConvert.ToInt16(intReturn);
					}
				}
				if (intLabelKind == 4030)
				{
					this.Detail.ColumnCount = 2;
					this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
				}
				if ((intLabelKind == 4030 || intLabelKind == 4013 || intLabelKind == 4065) && !EightLinesPerInch)
				{
					intMaxLines = 5;
				}
				else
				{
					intMaxLines = 6;
				}
				switch (intPrintType)
				{
					case 1:
					case 3:
					case 5:
					case 7:
						{
							boolByAccount = true;
							txtYear.Visible = false;
							txtland.Visible = false;
							txtbldg.Visible = false;
							txtexempt.Visible = false;
							txttotal.Visible = false;
							break;
						}
					case 2:
						{
							// boolByAccount = False
							txtYear.Visible = false;
							txtland.Visible = false;
							txtbldg.Visible = false;
							txtexempt.Visible = false;
							txttotal.Visible = false;
							break;
						}
					case 4:
						{
							txtYear.Visible = false;
							txtland.Visible = false;
							txtbldg.Visible = false;
							txtexempt.Visible = false;
							txttotal.Visible = false;
							Detail.CanGrow = true;
							Detail.CanShrink = true;
							Detail.Height = 720 / 1440f;

							Detail.CanGrow = false;
							Detail.CanShrink = false;
							txtLabel1.CanGrow = true;
							txtLabel1.CanShrink = true;
							txtLabel2.CanGrow = true;
							txtLabel2.CanShrink = true;
							txtlabel3.CanGrow = true;
							txtlabel3.CanShrink = true;
							txtLabel1.Height = 630 / 1440f;
							txtLabel1.Width = 7200 / 1440f;

							txtLabel1.Top = 90 / 1440f;
							txtLabel2.Top = 90 / 1440f;
							txtlabel3.Top = 90 / 1440f;

							txtLabel2.Height = 630 / 1440f;
							txtLabel2.Width = 7200 / 1440f;
							// 
							// txtLabel2.Top = 90
							txtlabel3.Height = 630 / 1440f;
							txtlabel3.Width = 7200 / 1440f;
							// 
							// txtlabel3.Top = 90
							txtLabel1.CanGrow = false;
							txtLabel1.CanShrink = false;
							txtLabel2.CanGrow = false;
							txtLabel2.CanShrink = false;
							txtlabel3.CanGrow = false;
							txtlabel3.CanShrink = false;
							boolByAccount = false;
							// vbPorter upgrade warning: intResp As short, int --> As DialogResult
							DialogResult intResp;
							intResp = MessageBox.Show("Would you like one label per account?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
							if (intResp == DialogResult.Yes)
								boolByAccount = true;
							break;
						}
				}
				//end switch
				if (boolByAccount)
				{
					strCard = "rscard = 1 and ";
				}
				else
				{
					strCard = "";
				}
				if (boolElimDups)
				{
					switch (intPrintWhat)
					{
						case 1:
							{
								RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + " rsaccount > 0 order by rsname,rsaddr1", modGlobalVariables.strREDatabase);
								break;
							}
						case 2:
							{
								if (intOrdBy == 1)
								{
									// by account
									RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount >= " + strMin + " and rsaccount <= " + strMax + " order by rsname,rsaddr1", modGlobalVariables.strREDatabase);
								}
								else
								{
									// by other
									if (Strings.StrComp(strMin, strMax, CompareConstants.vbTextCompare) == 0)
									{
										RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + " " + strOrder + " like '" + strMin + "*' order by rsname,rsaddr1", modGlobalVariables.strREDatabase);
									}
									else
									{
										strMax += "z";
										RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "upper(" + strOrder + ") >= upper('" + strMin + "') and upper(" + strOrder + ") <= upper('" + strMax + "') order by rsname,rsaddr1", modGlobalVariables.strREDatabase);
									}
								}
								break;
							}
						case 3:
							{
								RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngID) + ") order by rsname,rsaddr1", modGlobalVariables.strREDatabase);
								break;
							}
						case 4:
							{
								RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngID) + ")) order by rsname,rsaddr1", modGlobalVariables.strREDatabase);
								break;
							}
					}
					//end switch
					Grid.Rows = 0;
					strLastName = "";
					strLastAddr1 = "";
					while (!RSMster.EndOfFile())
					{
						if (Strings.UCase(strLastName) == Strings.UCase(Strings.Trim(RSMster.Get_Fields_String("rsname") + "")))
						{
							if (Strings.UCase(strLastAddr1) == Strings.UCase(Strings.Trim(RSMster.Get_Fields_String("rsaddr1") + "")))
							{
								goto SkipAdd;
							}
						}
						strLastName = Strings.Trim(RSMster.Get_Fields_String("rsname") + "");
						strLastAddr1 = Strings.Trim(RSMster.Get_Fields_String("rsaddr1") + "");
						strMapLot = Strings.Trim(RSMster.Get_Fields_String("rsmaplot") + "");
						strZip = FCConvert.ToString(RSMster.Get_Fields_String("rszip"));
						strLocation = FCConvert.ToString(RSMster.Get_Fields_String("rslocstreet"));
						strLocNum = FCConvert.ToString(RSMster.Get_Fields_String("rslocnumalph"));
						strAcres = Strings.Format(Conversion.Val(RSMster.Get_Fields_Double("piacres") + ""), "0.00");
						lngAuto = FCConvert.ToInt32(RSMster.Get_Fields_Int32("id"));
						Grid.AddItem(strLastName + "\t" + strLastAddr1 + "\t" + FCConvert.ToString(lngAuto) + "\t" + RSMster.Get_Fields_Int32("rsaccount") + "\t" + strMapLot + "\t" + strZip + "\t" + FCConvert.ToString(Conversion.Val(strLocNum)) + "\t" + strLocation + "\t" + RSMster.Get_Fields_Int32("rscard") + "\t" + strAcres);
						SkipAdd:
						;
						RSMster.MoveNext();
					}
					if (Strings.UCase(strOrder) == "RSNAME")
					{
						// do nothing, it's already in this order
					}
					else if (Strings.UCase(strOrder) == "RSZIP")
					{
						Grid.RowSel = 0;
						Grid.Row = 0;
						Grid.ColSel = 5;
						Grid.Col = 5;
						Grid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					}
					else if (Strings.UCase(strOrder) == "RSMAPLOT")
					{
						Grid.Row = 0;
						Grid.RowSel = 0;
						Grid.ColSel = 4;
						Grid.Col = 4;
						Grid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					}
					else if (Strings.UCase(strOrder) == "RSLOCSTREET")
					{
						Grid.ColSort(6, FCGrid.SortSettings.flexSortNumericAscending);
						Grid.ColSort(7, FCGrid.SortSettings.flexSortStringAscending);
						Grid.RowSel = 0;
						Grid.Row = 0;
						Grid.ColSel = 6;
						Grid.Col = 6;
						Grid.Sort = FCGrid.SortSettings.flexSortCustom;
						Grid.ColSel = 7;
						Grid.Col = 7;
						Grid.Sort = FCGrid.SortSettings.flexSortCustom;
					}
					else if (Strings.UCase(strOrder) == "RSACCOUNT")
					{
						Grid.RowSel = 0;
						Grid.Row = 0;
						Grid.ColSel = 3;
						Grid.Col = 3;
						Grid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					}
				}
				else
				{
					if (intPrintType == 4 && boolByAccount)
					{
						// assessment labels by account
						// Call clsTemp.CreateStoredProcedure("AssessmentLabelQuery", "select rsaccount,sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not rsdeleted = 1 group by rsaccount", strREDatabase)
						string strAssessmentLabelquery = "";
						strAssessmentLabelquery = "(select rsaccount,sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not isnull(rsdeleted,0) = 1 group by rsaccount) assessmentlabelquery";
						switch (intPrintWhat)
						{
							case 1:
								{
									// Call RSMster.OpenRecordset("select * from master inner join assessmentlabelquery on (assessmentlabelquery.rsaccount = master.rsaccount) where (not master.rsdeleted) and " & strCard & " master.rsaccount > 0 order by master." & strOrder, strREDatabase)
									// Call RSMster.OpenRecordset(strMasterJoinQuery & " inner join assessmentlabelquery on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not mj.rsdeleted) and " & strCard & " mj.rsaccount > 0 order by mj." & strOrder, strREDatabase)
									RSMster.OpenRecordset("select * from " + strMasterJoinQuery + " inner join " + strAssessmentLabelquery + " on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not isnull(mj.rsdeleted,0) = 1) and " + strCard + " mj.rsaccount > 0 order by mj." + strOrder, modGlobalVariables.strREDatabase);
									break;
								}
							case 2:
								{
									if (intOrdBy == 1)
									{
										// by account
										RSMster.OpenRecordset("select * from " + strMasterJoinQuery + " inner join " + strAssessmentLabelquery + " on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not isnull(mj.rsdeleted,0) = 1) and " + strCard + "mj.rsaccount >= " + strMin + " and mj.rsaccount <= " + strMax + " order by mj.rsaccount", modGlobalVariables.strREDatabase);
									}
									else
									{
										// by other
										if (Strings.StrComp(strMin, strMax, CompareConstants.vbTextCompare) == 0)
										{
											RSMster.OpenRecordset("select * from " + strMasterJoinQuery + " inner join " + strAssessmentLabelquery + " on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not isnull(mj.rsdeleted,0) = 1) and " + strCard + "mj." + strOrder + " like '" + strMin + "*' order by mj." + strTempOrder, modGlobalVariables.strREDatabase);
										}
										else
										{
											strMax += "z";
											RSMster.OpenRecordset("select * from " + strMasterJoinQuery + " inner join " + strAssessmentLabelquery + " on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not isnull(mj.rsdeleted,0) = 1) and " + strCard + "upper(mj." + strOrder + ") >= upper('" + strMin + "') and upper(mj." + strOrder + ") <= upper('" + strMax + "') order by mj." + strTempOrder, modGlobalVariables.strREDatabase);
										}
									}
									break;
								}
							case 3:
								{
									RSMster.OpenRecordset("select * from " + strMasterJoinQuery + " inner join " + strAssessmentLabelquery + " on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not isnull(mj.rsdeleted,0) = 1) and " + strCard + "mj.rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngID) + ") order by mj." + strTempOrder, modGlobalVariables.strREDatabase);
									break;
								}
							case 4:
								{
									RSMster.OpenRecordset("select * from " + strMasterJoinQuery + " inner join " + strAssessmentLabelquery + " on (assessmentlabelquery.rsaccount = mj.rsaccount) where (not isnull(mj.rsdeleted,0) = 1) and " + strCard + "mj.rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngID) + ")) order by mj." + strTempOrder, modGlobalVariables.strREDatabase);
									break;
								}
						}
						//end switch
					}
					else
					{
						switch (intPrintWhat)
						{
							case 1:
								{
									RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + " rsaccount > 0 order by " + strTempOrder, modGlobalVariables.strREDatabase);
									break;
								}
							case 2:
								{
									if (intOrdBy == 1)
									{
										// by account
										RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount >= " + strMin + " and rsaccount <= " + strMax + " order by rsaccount", modGlobalVariables.strREDatabase);
									}
									else
									{
										// by other
										if (Strings.StrComp(strMin, strMax, CompareConstants.vbTextCompare) == 0)
										{
											RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + strOrder + " like '" + strMin + "*' order by " + strTempOrder, modGlobalVariables.strREDatabase);
										}
										else
										{
											strMax += "z";
											RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "upper(" + strOrder + ") >= upper('" + strMin + "') and upper(" + strOrder + ") <= upper('" + strMax + "') order by " + strTempOrder, modGlobalVariables.strREDatabase);
										}
									}
									break;
								}
							case 3:
								{
									RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngID) + ") order by " + strTempOrder, modGlobalVariables.strREDatabase);
									break;
								}
							case 4:
								{
									RSMster.OpenRecordset(strMasterJoin + " where (not isnull(rsdeleted,0) = 1) and " + strCard + "rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngID) + ")) order by " + strTempOrder, modGlobalVariables.strREDatabase);
									break;
								}
						}
						//end switch
					}
				}
				return;
				lngCurRow = 0;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Data Initialize function", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Dim x As Integer
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ErrorHandler
				CheckEOF:
				;
				if (intLabelNum == 2)
				{
					intLabelNum = 1;
				}
				else
				{
					intLabelNum = 2;
				}
				if (boolElimDups)
				{
					eArgs.EOF = (lngCurRow == Grid.Rows);
				}
				else
				{
					eArgs.EOF = RSMster.EndOfFile();
				}
				if (eArgs.EOF)
				{
					REC += tempRec;
					// in the case of 2 across labels, if we end with an odd number of labels, temprec has the last label
					return;
				}
				//FC:FINAL:RPU:#i1029 - If intFontSize == 0 made it a value very small but it can't be 0, because FontSize is in em
				if (intFontSize == 0)
				{
					intFontSize = 0.001f;
				}
				txtLabel1.Font = new Font(txtLabel1.Font.Name, intFontSize);
				if (intPrintType == 3)
				{
					txtland2.Font = new Font(txtland2.Font.Name, intFontSize);
					txtBldg2.Font = new Font(txtBldg2.Font.Name, intFontSize);
					txtExempt2.Font = new Font(txtExempt2.Font.Name, intFontSize);
					txtTotal2.Font = new Font(txtTotal2.Font.Name, intFontSize);
					txtTax.Font = new Font(txtTax.Font.Name, intFontSize);
					txtexempt.Font = new Font(txtexempt.Font.Name, intFontSize);
					txtland2.Font = new Font(txtland2.Font.Name, intFontSize);
					lblLand.Font = new Font(lblLand.Font.Name, intFontSize);
					txtYear.Font = new Font(txtYear.Font.Name, intFontSize);
					txtland.Font = new Font(txtland.Font.Name, intFontSize);
					txtbldg.Font = new Font(txtbldg.Font.Name, intFontSize);
					txtLabel2.Font = new Font(txtLabel2.Font.Name, intFontSize);
					txtlabel3.Font = new Font(txtlabel3.Font.Name, intFontSize);
					lblTotal.Font = new Font(lblTotal.Font.Name, intFontSize);
					lblExempt.Font = new Font(lblExempt.Font.Name, intFontSize);
					lblBldg.Font = new Font(lblBldg.Font.Name, intFontSize);
				}
				if ((intPrintType != 3) || (boolFirstLabel))
				{
					txtLabel1.Text = MakeLabel();
					boolFirstLabel = false;
					lblLand.Visible = false;
					lblBldg.Visible = false;
					lblTotal.Visible = false;
					lblExempt.Visible = false;
					txtland2.Visible = false;
					txtBldg2.Visible = false;
					txtExempt2.Visible = false;
					txtTotal2.Visible = false;
					txtTax.Visible = false;
					if (intPrintType != 3)
					{
						if (boolElimDups && intDuplicateCount == 0)
						{
							lngCurRow += 1;
							intDuplicateCount = intNumberofDuplicates;
						}
						else if (intDuplicateCount == 0)
						{
							intDuplicateCount = intNumberofDuplicates;
							RSMster.MoveNext();
						}
						else
						{
							intDuplicateCount -= 1;
							if (intDuplicateCount < 0)
								intDuplicateCount = 0;
						}
					}
				}
				else
				{
					if (!boolUseDotmatrix)
					{
						lblLand.Visible = true;
						lblBldg.Visible = true;
						lblTotal.Visible = true;
						lblExempt.Visible = true;
						txtland2.Visible = true;
						txtBldg2.Visible = true;
						txtExempt2.Visible = true;
						txtTotal2.Visible = true;
					}
					txtLabel1.Text = MakeLabel();
					boolFirstLabel = true;
					if (boolElimDups)
					{
						lngCurRow += 1;
					}
					else
					{
						RSMster.MoveNext();
					}
				}
				if (boolFirstLabelMade)
				{
					if (REC == string.Empty)
					{
						strFirstLabel = tempRec;
					}
					else
					{
						strFirstLabel = REC;
					}
					REC = "";
					if (intLabelKind != 4030 || intLabelNum == 2)
					{
						boolFirstLabelMade = false;
					}
				}
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fetch Data function", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string MakeLabel()
		{
			string MakeLabel = "";
			int x;
			int intCharLen = 0;
			try
			{
				// On Error GoTo ErrorHandler
				switch (intPrintType)
				{
					case 1:
						{
							MakeMailing();
							break;
						}
					case 2:
						{
							MakeAddressCard();
							break;
						}
					case 3:
						{
							MakeChangeAssessment();
							break;
						}
					case 4:
						{
							// assessmentlabels card
							makeassessmentcard();
							break;
						}
					case 5:
						{
							MakeCustom();
							break;
						}
					case 6:
						{
							MakeF2Label();
							break;
						}
					case 7:
						{
							MakeCardLabel();
							break;
						}
				}
				//end switch
				if (intPrintType != 4)
				{
					MakeLabel = strLine1 + "\r\n";
					MakeLabel = MakeLabel + strLine2;
					MakeLabel = MakeLabel + "\r\n";
					MakeLabel = MakeLabel + strLine3 + "\r\n";
					MakeLabel = MakeLabel + strLine4 + "\r\n";
					MakeLabel = MakeLabel + strLine5 + "\r\n";
					if (intLabelKind == 4030)
					{
						strMLine1[intLabelNum] = strLine1;
						strMLine2[intLabelNum] = strLine2;
						strMLine3[intLabelNum] = strLine3;
						strMLine4[intLabelNum] = strLine4;
						strMLine5[intLabelNum] = strLine5;
					}
					if (boolUseDotmatrix && (intLabelKind == 4013 || intLabelKind == 4030 || intLabelKind == 4065) && !EightLinesPerInch)
					{
					}
					else
					{
						MakeLabel = MakeLabel + strLine6;
						if (boolUseDotmatrix && EightLinesPerInch)
						{
							MakeLabel = MakeLabel + "\r\n" + "\r\n";
						}
					}
				}
				else
				{
					if (boolUseDotmatrix)
					{
						MakeLabel = strLine1 + "\r\n" + strLine2 + "\r\n";
					}
					else
					{
						MakeLabel = strLine2;
						MakeLabel = strLine1 + "\r\n" + strLine2 + "\r\n";
					}
				}
				if (intLabelKind == 4030)
				{
					intCharLen = intCharactersWide;
					if (intLabelNum == 2)
					{
						tempRec = "";
						if (boolUseDotmatrix)
						{
							REC += Strings.Mid(strMLine1[1] + Strings.StrDup(intCharLen, " "), 1, intCharLen) + "  " + strMLine1[2] + "\r\n";
							REC += Strings.Mid(strMLine2[1] + Strings.StrDup(intCharLen, " "), 1, intCharLen) + "  " + strMLine2[2] + "\r\n";
							REC += Strings.Mid(strMLine3[1] + Strings.StrDup(intCharLen, " "), 1, intCharLen) + "  " + strMLine3[2] + "\r\n";
							REC += Strings.Mid(strMLine4[1] + Strings.StrDup(intCharLen, " "), 1, intCharLen) + "  " + strMLine4[2] + "\r\n";
							REC += Strings.Mid(strMLine5[1] + Strings.StrDup(intCharLen, " "), 1, intCharLen) + "  " + strMLine5[2] + "\r\n";
							REC += "\r\n";
						}
					}
					else
					{
						tempRec = MakeLabel;
					}
				}
				else
				{
					REC += MakeLabel + "\r\n";
				}
				return MakeLabel;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Make Label", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeLabel;
		}

		
		public void LabelOptions(ref bool boolElim, ref short intWhatPrint, ref short intType, ref short intOrderBy, ref int bytWhatInclude, string minstring = "", string maxstring = "", string yearstring = "", float sngTax = 0, short LabelType = 4014, int intNumDuplicates = 0)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// vbPorter upgrade warning: intRes As short, int --> As DialogResult
				DialogResult intRes;
                boolUseDotmatrix = true;
				string strRep;
				bool boolSetPrinterFont;
				boolSetPrinterFont = false;
				strLeftBlank = " ";

					intCPI = 10;
				
				intNumberofDuplicates = intNumDuplicates;
				intDuplicateCount = intNumberofDuplicates;
				intLabelKind = LabelType;
				boolByAccount = false;
				if (intType == 2)
				{
					// if address labels for cards
					intRes = MessageBox.Show("Would you like one label per account?  Choosing NO will create one label for every card.", "Labels per account or card", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
					if (intRes == DialogResult.Yes)
					{
						boolByAccount = true;
					}
					else
					{
						boolByAccount = false;
					}
				}
				boolUseBookPage = modGlobalVariables.Statics.CustomizedInfo.Ref1BookPage == 2;
				boolShowSaleData = false;
				if (intType == 7)
				{
					if (MessageBox.Show("Do you want book/page and sale information to print?", "Print sale info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						boolShowSaleData = true;
					}
				}
				if (boolShowSaleData || modGlobalVariables.Statics.CustomizedInfo.Ref1BookPage == 2)
				{
					clsBookPage.OpenRecordset("select * from bookpage where [current] = 1 order by account,line desc", modGlobalVariables.strREDatabase);
				}
				switch (intLabelKind)
				{
					case 4013:
						{
							// 3.5 inches
							boolSetPrinterFont = true;
							strLeftBlank = "";
							intCharactersWide = FCConvert.ToInt32((3.5 * intCPI) - 3);
							// three characters shy of full label width
							// ask about using 8 lines per inch
							// If boolDotMatrix Then
							intRes = MessageBox.Show("Would you like to print 6 lines of data?  This will require that you set your printer to 8 lines per inch." + "\r\n" + "Preview will not show all lines", "Lines per inch", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
							if (intRes == DialogResult.Yes)
							{
								EightLinesPerInch = true;
								boolUseDotmatrix = true;
							}
							else
							{
								EightLinesPerInch = false;
							}
							// End If
							switch (intCPI)
							{
								case 10:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 12);
										break;
									}
								case 12:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 10);
										break;
									}
								case 17:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 7);
										break;
									}
							}
							//end switch
							break;
						}
					case 4065:
						{
							// 4 inches
							strLeftBlank = "";
							intCharactersWide = (4 * intCPI) - 2;
							// ask about using 8 lines per inch
							if (boolUseDotmatrix)
							{
								intRes = MessageBox.Show("Would you like to print 6 lines of data?  This will require that you set your printer to 8 lines per inch." + "\r\n" + "Preview will not show all lines", "Lines per inch", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
								if (intRes == DialogResult.Yes)
								{
									EightLinesPerInch = true;
								}
								else
								{
									EightLinesPerInch = false;
								}
							}
							switch (intCPI)
							{
								case 10:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 12);
										break;
									}
								case 12:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 10);
										break;
									}
								case 17:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 7);
										break;
									}
							}
							//end switch
							break;
						}
					case 4014:
						{
							// 4 inches
							intCharactersWide = (4 * intCPI) - 2;
							switch (intCPI)
							{
								case 10:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 12);
										break;
									}
								case 12:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 10);
										break;
									}
								case 17:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 7);
										break;
									}
							}
							//end switch
							break;
						}
					case 4030:
						{
							// 3.5 inches
							intCharactersWide = FCConvert.ToInt32((3.5 * intCPI) - 2);
							switch (intCPI)
							{
								case 10:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 12);
										break;
									}
								case 12:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 10);
										break;
									}
								case 17:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 7);
										break;
									}
							}
							//end switch
							break;
						}
					case 1:
						{
							// assessment label
							boolUseDotmatrix = true;
							intCharactersWide = (5 * intCPI) - 2;
							switch (intCPI)
							{
								case 10:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 12);
										break;
									}
								case 12:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 10);
										break;
									}
								case 17:
									{
										txtLabel1.Font = new Font(txtLabel1.Font.Name, 7);
										break;
									}
							}
							//end switch
							break;
						}
				}
				//end switch
				intFontSize = txtLabel1.Font.Size;
				boolElimDups = boolElim;
				boolShowIt = true;
				// show preview
				intPrintType = intType;
				intPrintWhat = intWhatPrint;
				intOrdBy = intOrderBy;
				strTaxYear = yearstring;
				snglTax = sngTax;
				if (!Information.IsNothing(minstring))
				{
					strMin = minstring;
					strMax = maxstring;
				}
				if ((bytWhatInclude & 512) > 0)
				{
					boolIncOwner = true;
				}
				else
				{
					boolIncOwner = false;
				}
				if ((bytWhatInclude & 256) > 0)
				{
					boolIncAcres = true;
				}
				else
				{
					boolIncAcres = false;
				}
				if ((bytWhatInclude & 128) > 0)
				{
					boolIncSecOwner = true;
				}
				else
				{
					boolIncSecOwner = false;
				}
				if ((bytWhatInclude & 64) > 0)
				{
					boolIncLoc = true;
				}
				else
				{
					boolIncLoc = false;
				}
				if ((bytWhatInclude & 32) > 0)
				{
					boolIncRef2 = true;
				}
				else
				{
					boolIncRef2 = false;
				}
				if ((bytWhatInclude & 16) > 0)
				{
					boolIncAcct = true;
				}
				else
				{
					boolIncAcct = false;
				}
				if ((bytWhatInclude & 8) > 0)
				{
					boolIncRef = true;
				}
				else
				{
					boolIncRef = false;
				}
				if ((bytWhatInclude & 4) > 0)
				{
					boolIncAddr = true;
				}
				else
				{
					boolIncAddr = false;
				}
				if ((bytWhatInclude & 2) > 0)
				{
					boolIncMap = true;
				}
				else
				{
					boolIncMap = false;
				}
				if ((bytWhatInclude & 1) > 0)
				{
					boolIncETax = true;
				}
				else
				{
					boolIncETax = false;
				}
				switch (intOrderBy)
				{
					case 1:
						{
							// account
							strOrder = "RSACCOUNT";
							break;
						}
					case 2:
						{
							// map lot
							strOrder = "RSMAPLOT";
							break;
						}
					case 3:
						{
							// zip
							strOrder = "RSZIP";
							break;
						}
					case 4:
						{
							// name
							strOrder = "RSNAME";
							break;
						}
					case 5:
						{
							// location
							strOrder = "RSLOCSTREET";
							break;
						}
				}
				//end switch
				//vbPorterConverter2.ShowModeless(this);
				frmReportViewer.InstancePtr.Init(this._InstancePtr);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Function Label Options", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void MakeMailing()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int intTemp = 0;
				// vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
				string strTemp;
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strNam = "";
				string strA1 = "";
				string strA2 = "";
				string strA3 = "";
				string strSecOwn = "";
				string strZip = "";
				string strZip4 = "";
				string strST = "";
				string strML = "";
				// vbPorter upgrade warning: lngAct As int	OnWrite(string)	OnRead(string)
				int lngAct = 0;
				int intCharLen;
				int intMaxMapLen = 0;
				string[] strLine = new string[6 + 1];
				int intCLine = 0;
				string strCountry = "";
				intCharLen = intCharactersWide;
				if (!boolElimDups)
				{
					strNam = RSMster.Get_Fields_String("rsname") + "";
					strA1 = RSMster.Get_Fields_String("rsaddr1") + "";
					/*? 3 */// 4        Call escapequote(strNam)
					// 5        Call escapequote(strA1)
				}
				else
				{
					strNam = Grid.TextMatrix(lngCurRow, 0);
					strA1 = Grid.TextMatrix(lngCurRow, 1);
					// 8        Call escapequote(strNam)
					// 9        Call escapequote(strA1)
				}
				if (boolElimDups)
				{
					clsTemp.OpenRecordset("select * from master where rsaccount = " + Grid.TextMatrix(lngCurRow, 3) + " and rscard = " + Grid.TextMatrix(lngCurRow, 8), modGlobalVariables.strREDatabase);
					strA2 = FCConvert.ToString(clsTemp.Get_Fields_String("rsaddr2"));
					strA3 = FCConvert.ToString(clsTemp.Get_Fields_String("rsaddr3"));
					strCountry = FCConvert.ToString(clsTemp.Get_Fields_String("country"));
					strSecOwn = FCConvert.ToString(clsTemp.Get_Fields_String("rssecowner"));
					strZip = FCConvert.ToString(clsTemp.Get_Fields_String("rszip"));
					strZip4 = FCConvert.ToString(clsTemp.Get_Fields_String("rszip4"));
					strST = FCConvert.ToString(clsTemp.Get_Fields_String("rsstate"));
					strML = FCConvert.ToString(clsTemp.Get_Fields_String("rsmaplot"));
					lngAct = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("rsaccount"));
				}
				else
				{
					strA2 = RSMster.Get_Fields_String("rsaddr2") + "";
					strA3 = RSMster.Get_Fields_String("rsaddr3") + "";
					strSecOwn = RSMster.Get_Fields_String("rssecowner") + "";
					strZip = RSMster.Get_Fields_String("rszip") + "";
					strZip4 = RSMster.Get_Fields_String("rszip4") + "";
					strST = RSMster.Get_Fields_String("rsstate") + "";
					strML = RSMster.Get_Fields_String("rsmaplot") + "";
					lngAct = FCConvert.ToInt32(RSMster.Get_Fields_Int32("rsaccount") + "");
					strCountry = FCConvert.ToString(RSMster.Get_Fields_String("Country"));
				}
				strCountry = Strings.Trim(strCountry);
				if (Strings.LCase(strCountry) == "united state")
				{
					strCountry = "";
				}
				strTemp = Strings.StrDup(38, " ");
				strLine2 = Strings.StrDup(intCharactersWide, " ");
				strLine1 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				// 34    Mid(strLine2, 1) = Left(" " & strNam & "", intCharLen - 8)
				// Mid(strLine2, 39, 6) = Format(RSMster.fields("rsaccount") & "", "@@@@@@")
				// 35    Mid(strLine2, intCharLen - 6, 6) = Format(lngAct & "", "@@@@@@")
				// 
				// If intLabelKind <> 4013 And intLabelKind <> 4030 Then
				// 36        If boolDotMatrix Then
				// 37               strLine2 = String(intCharLen, " ")
				// 38              Mid(strLine2, 1) = Left(" " & strNam & "", 30)
				// Mid(strLine2, 32) = Format(RSMster.fields("rsaccount") & "", "@@@@@@")
				// 39              Mid(strLine2, 32) = Format(lngAct & "", "@@@@@@")
				// End If
				// Else
				strLine2 = Strings.StrDup(intCharLen, " ");
				// Mid(strLine2, 1) = Left(strLeftBlank & strNam & "", intCharLen)
				strLine2 = Strings.Left(strLeftBlank + strNam + Strings.StrDup(intCharLen, " "), intCharLen);
				if (!modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom && !modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount)
				{
					strTemp = FCConvert.ToString(lngAct);
					intTemp = strTemp.Length;
					fecherFoundation.Strings.MidSet(ref strLine2, intCharLen - intTemp, " " + strTemp);
				}
				// End If
				// If intLabelKind <> 4013 And intLabelKind <> 4030 Then
				// intCharLen = 39
				// Else
				// intCharLen = 31
				// End If
				// If Trim(RSMster.fields("rssecowner") & "") <> vbNullString Then
				if (Strings.Trim(strSecOwn + "") != string.Empty && boolIncSecOwner && (Strings.Trim(strA2) == "" || strCountry == ""))
				{
					// strLine3 = Mid(" " & Trim(RSMster.fields("rssecowner")), 1, 38)
					strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(strSecOwn), 1, intCharLen);
					strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(strA1 + ""), 1, intCharLen);
					// If Trim(RSMster.fields("rsaddr2") & "") <> vbNullString Then
					if (Strings.Trim(strA2 + "") != string.Empty)
					{
						strLine5 = Strings.Mid(strLeftBlank + Strings.Trim(strA2), 1, intCharLen);
						strLine6 = Strings.Mid(strLeftBlank + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " + Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharLen);
					}
					else
					{
						strLine5 = Strings.Mid(strLeftBlank + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " + Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharLen);
						if (strCountry != "")
						{
							strLine6 = strLeftBlank + strCountry;
						}
					}
				}
				else
				{
					strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(strA1 + ""), 1, intCharLen);
					if (Strings.Trim(strA2 + "") != string.Empty)
					{
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(strA2), 1, intCharLen);
						strLine5 = Strings.Mid(strLeftBlank + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " + Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharLen);
						if (strCountry != "")
						{
							strLine6 = strLeftBlank + strCountry;
						}
					}
					else
					{
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + "  " + Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + ""), 1, intCharLen);
						if (strCountry != "")
						{
							strLine6 = strLeftBlank + strCountry;
						}
					}
				}
				if (Strings.Trim(strLine6) != string.Empty && intMaxLines < 6)
				{
					if (Strings.Trim(strLine6) != string.Empty)
					{
						if (!boolUseDotmatrix)
						{
							strLine3 = strLine4;
							strLine4 = strLine5;
							strLine5 = strLine6;
							strLine6 = "";
						}
					}
				}
				int intStart = 0;
				if (boolIncMap)
				{
					strLine1 = strLeftBlank + "MapLot: " + Strings.Mid(Strings.Trim(strML + ""), 1, intCharLen - 1);
					if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						// If strline6 <> vbNullString Then
						strLine1 = strLine2;
						strLine2 = strLine3;
						strLine3 = strLine4;
						strLine4 = strLine5;
						strLine5 = strLine6;
						strLine6 = string.Empty;
						strLine[1] = strLine1;
						strLine[2] = strLine2;
						strLine[3] = strLine3;
						strLine[4] = strLine4;
						strLine[5] = strLine5;
						strMLot1 = "";
						strMLot2 = "";
						strMLot3 = "";
						strMLot4 = "";
						intMaxMapLen = ParseMapLot(strML + "", ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
						// End If
						if (FCConvert.CBool(modRegistry.GetRegistryKey_3("RELimitMapLotToFour", FCConvert.ToString(false))))
						{
							if (strMLot1.Length > 4)
							{
								strMLot2 = Strings.Mid(strMLot1, 5) + strMLot2;
								strMLot1 = Strings.Mid(strMLot1, 1, 4);
							}
							if (strMLot2.Length > 4)
							{
								strMLot3 = Strings.Mid(strMLot2, 5) + strMLot3;
								strMLot2 = Strings.Mid(strMLot2, 1, 4);
							}
							if (strMLot3.Length > 4)
							{
								strMLot4 = Strings.Mid(strMLot3, 5) + strMLot4;
								strMLot3 = Strings.Mid(strMLot3, 1, 4);
							}
							// intTemp = intCharLen - intMaxMapLen
							intTemp = strMLot4.Length;
							if (intTemp < 4)
							{
								intTemp = 4;
							}
							intTemp = intCharLen - intTemp;
						}
						else
						{
							intTemp = intCharLen - intMaxMapLen;
						}
						// put a space between account and maplot lines if there aren't 4 maplot lines
						// otherwise the 4013 labels don't have enough lines to skip a line
						if (Strings.Trim(strMLot4) != string.Empty)
						{
							if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
							{
								intStart = 1;
							}
							else
							{
								intStart = 2;
							}
							intCLine = intStart;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[2] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot1 + "");
							strLine[2] = strTemp;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[3] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot2 + "");
							strLine[3] = strTemp;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[4] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot3 + "");
							strLine[4] = strTemp;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[5] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot4 + "");
							strLine[5] = strTemp;
						}
						else
						{
							if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
							{
								intStart = 1;
							}
							else
							{
								intStart = 3;
							}
							intCLine = intStart;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[intCLine] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot1 + "");
							strLine[intCLine] = strTemp;
							intCLine += 1;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[intCLine] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot2 + "");
							strLine[intCLine] = strTemp;
							intCLine += 1;
							strTemp = Strings.StrDup(intCharLen, " ");
							fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strLine[intCLine] + "", intTemp - 2));
							fecherFoundation.Strings.MidSet(ref strTemp, intTemp + 1, intMaxMapLen, strMLot3 + "");
							strLine[intCLine] = strTemp;
						}
						if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
						{
							strTemp = FCConvert.ToString(lngAct);
							intTemp = strTemp.Length;
							strLine[5] = Strings.Left(strLine[5] + Strings.StrDup(intCharLen, " "), intCharLen);
							fecherFoundation.Strings.MidSet(ref strLine[5], intCharLen - intTemp, " " + strTemp);
						}
						strLine1 = Strings.RTrim(strLine[1]);
						strLine2 = Strings.RTrim(strLine[2]);
						strLine3 = Strings.RTrim(strLine[3]);
						strLine4 = Strings.RTrim(strLine[4]);
						strLine5 = Strings.RTrim(strLine[5]);
						strLine6 = Strings.RTrim(strLine[6]);
					}
				}
				else
				{
					if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom && !modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount)
					{
						strTemp = FCConvert.ToString(lngAct);
						intTemp = strTemp.Length;
						strLine5 = Strings.Left(strLine5 + Strings.StrDup(intCharLen, " "), intCharLen);
						fecherFoundation.Strings.MidSet(ref strLine5, intCharLen - intTemp, " " + strTemp);
					}
					strLine1 = Strings.RTrim(strLine2);
					strLine2 = Strings.RTrim(strLine3);
					strLine3 = Strings.RTrim(strLine4);
					strLine4 = Strings.RTrim(strLine5);
					strLine5 = Strings.RTrim(strLine6);
					strLine6 = "";
				}
				if (intMaxLines < 6 && strLine6 != string.Empty)
				{
					strLine6 = string.Empty;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Line " + Information.Erl() + " in MakeMailing", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeAddressCard()
		{
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
			string strTemp = "";
			string strRefBookPage;
			int intCharLen;
			int intMaxMapLen;
			int intNumChars = 0;
			int intCurLine = 0;
			string[] strL = new string[6 + 1];
			// vbPorter upgrade warning: lngAct As int	OnRead(string)
			int lngAct;
			try
			{
				// On Error GoTo ErrorHandler
				intCharLen = intCharactersWide;
				strLine1 = Strings.StrDup(intCharLen, " ");
				strLine2 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				// If intLabelKind <> 4013 And intLabelKind <> 4030 Then
				// intCharLen = 39
				// Else
				// intCharLen = 33
				// End If
				strRefBookPage = "";
				if (boolUseBookPage)
				{
					if (!(clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile()))
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (clsBookPage.Get_Fields("account") == RSMster.Get_Fields_Int32("rsaccount"))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							while (clsBookPage.FindNextRecord("account", RSMster.Get_Fields_Int32("rsaccount")))
							{
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strRefBookPage += " B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							}
							// ElseIf clsBookPage.FindFirstRecord("account", RSMster.Fields("rsaccount")) Then
						}
						else if (clsBookPage.FindFirst("account = " + RSMster.Get_Fields_Int32("rsaccount")))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							while (clsBookPage.FindNextRecord("account", RSMster.Get_Fields_Int32("rsaccount")))
							{
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strRefBookPage += " B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							}
						}
					}
				}
				else
				{
					strRefBookPage = RSMster.Get_Fields_String("RSref1") + "";
				}
				// If boolDotMatrix Then
				// 1
				// strLine1 = String(38, " ")
				// 2        Mid(strLine1, 1) = Left(" " & RSMster.fields("rsname") & "", 30)
				// 3        Mid(strLine1, 32, 6) = Format(RSMster.fields("rsaccount") & "", "@@@@@@")
				strLine1 = Strings.StrDup(intCharLen, " ");
				lngAct = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields_Int32("rsaccount"))));
				if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					fecherFoundation.Strings.MidSet(ref strLine1, 1, Strings.Left(strLeftBlank + RSMster.Get_Fields_String("rsname") + "", intCharLen));
				}
				else
				{
					fecherFoundation.Strings.MidSet(ref strLine1, 1, Strings.Left(strLeftBlank + RSMster.Get_Fields_String("rsname") + "", intCharLen - 7));
					fecherFoundation.Strings.MidSet(ref strLine1, intCharLen - 6, 6, Strings.Format(RSMster.Get_Fields_Int32("rsaccount") + "", "@@@@@@"));
				}
				// Else
				// 4        Mid(strLine1, 1) = Left(" " & RSMster.fields("rsname") & "", 37)
				// 5        Mid(strLine1, 39, 6) = Format(RSMster.fields("rsaccount") & "", "@@@@@@")
				// End If
				if ((Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rssecowner"))) != string.Empty) && boolIncSecOwner)
				{
					strLine2 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rssecowner"))), 1, intCharLen - 1);
					if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))) != string.Empty)
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))), 1, intCharLen - 1);
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen - 1);
						if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))) != string.Empty)
						{
							strLine2 = strLine3;
							strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen - 1);
						}
					}
					else
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen - 1);
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen - 1);
					}
				}
				else
				{
					strLine2 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))), 1, intCharLen - 1);
					if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))) != string.Empty)
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen - 1);
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen - 1);
					}
					else
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen - 1);
					}
				}
				if (strLine4 == string.Empty)
				{
					strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(strRefBookPage), 1, intCharLen - 1);
					if (!modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						strLine5 = strLeftBlank + "MapLot: " + Strings.Mid(Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot"))), 1, 30);
					}
					else
					{
						stripmiddlespaces(ref strLine4);
					}
				}
				else
				{
					strLine5 = Strings.Mid(strLeftBlank + Strings.Trim(strRefBookPage), 1, intCharLen - 1);
					if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						stripmiddlespaces(ref strLine5);
					}
					else
					{
						strLine6 = strLeftBlank + "MapLot: " + Strings.Mid(Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot"))), 1, 30);
					}
				}
				strL[1] = strLine1;
				strL[2] = strLine2;
				strL[3] = strLine3;
				strL[4] = strLine4;
				strL[5] = strLine5;
				strL[6] = strLine6;
				if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
				{
					strL[6] = "";
					strMLot1 = "";
					strMLot2 = "";
					strMLot3 = "";
					strMLot4 = "";
					// 34            Call ParseMapLot(RSMster.fields("rsmaplot") & "", strMLot1, strMLot2, strMLot3, strMLot4)
					// If CBool(GetRegistryKey("RELimitMapLotToFour")) Then
					// If Len(Trim(strMLot1)) > 4 Then
					// strMLot2 = Mid(strMLot1, 5) & strMLot2
					// End If
					// If Len(Trim(strMLot2)) > 4 Then
					// strMLot3 = Mid(strMLot2, 5) & strMLot3
					// End If
					// If Len(Trim(strMLot3)) > 4 Then
					// strMLot4 = Mid(strMLot3, 5) & strMLot4
					// End If
					// End If
					// 35            If Trim(strMLot4) <> vbNullString Then
					// 36                strTemp = String(intCharLen, " ")
					// 37                Mid(strTemp, 1, intCharLen - 7) = Left(strLine2 & "", intCharLen - 7)
					// 38                Mid(strTemp, intCharLen - 3, 4) = strMLot1 & ""
					// 39                strLine2 = strTemp
					// 40                strTemp = String(intCharLen, " ")
					// 41                Mid(strTemp, 1, intCharLen - 7) = Left(strLine3 & "", intCharLen - 7)
					// 42                Mid(strTemp, intCharLen - 3, 4) = strMLot2 & ""
					// 43                strLine3 = strTemp
					// 44                strTemp = String(intCharLen, " ")
					// 45                Mid(strTemp, 1, intCharLen - 7) = Left(strLine4 & "", intCharLen - 7)
					// 46                Mid(strTemp, intCharLen - 3, 4) = strMLot3 & ""
					// 47                strLine4 = strTemp
					// 48                strTemp = String(intCharLen, " ")
					// 49                Mid(strTemp, 1, intCharLen - 7) = Left(strLine5 & "", intCharLen - 7)
					// 50                Mid(strTemp, intCharLen - 3, 4) = strMLot4 & ""
					// 51                strLine5 = strTemp
					// 
					// Else
					// strTemp = String(intCharLen, " ")
					// 52                Mid(strTemp, 1, intCharLen - 7) = Left(strLine3 & "", intCharLen - 7)
					// 53                Mid(strTemp, intCharLen - 3, 4) = strMLot1 & ""
					// 54                strLine3 = strTemp
					// 55                strTemp = String(intCharLen, " ")
					// 56                Mid(strTemp, 1, intCharLen - 7) = Left(strLine4 & "", intCharLen - 7)
					// 57                Mid(strTemp, intCharLen - 3, 4) = strMLot2 & ""
					// 58                strLine4 = strTemp
					// 59                strTemp = String(intCharLen, " ")
					// 60                Mid(strTemp, 1, intCharLen - 7) = Left(strLine5 & "", intCharLen - 7)
					// 61                Mid(strTemp, intCharLen - 3, 4) = strMLot3 & ""
					// 62                strLine5 = strTemp
					// End If
					intMaxMapLen = ParseMapLot(RSMster.Get_Fields_String("rsmaplot"), ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
					if (FCConvert.CBool(modRegistry.GetRegistryKey_3("RELimitMapLotToFour", FCConvert.ToString(false))))
					{
						if (strMLot1.Length > 4)
						{
							strMLot2 += Strings.Mid(strMLot1, 5);
							strMLot1 = Strings.Mid(strMLot1, 1, 4);
						}
						if (strMLot2.Length > 4)
						{
							strMLot3 += Strings.Mid(strMLot2, 5);
							strMLot2 = Strings.Mid(strMLot2, 1, 4);
						}
						if (strMLot3.Length > 4)
						{
							strMLot4 += Strings.Mid(strMLot3, 5);
							strMLot3 = Strings.Mid(strMLot3, 1, 4);
							if (strMLot4.Length > 4)
								intMaxMapLen = strMLot4.Length;
						}
					}
					intNumChars = intCharLen;
					// intTemp = intNumChars - intMaxMapLen
					// intTemp = 4
					// If Len(strMLot4) > 4 Then intTemp = Len(strMLot4)
					modProperty.Statics.intTemp = Strings.Trim(strMLot1).Length;
					if (Strings.Trim(strMLot2).Length > modProperty.Statics.intTemp)
					{
						modProperty.Statics.intTemp = Strings.Trim(strMLot2).Length;
					}
					if (Strings.Trim(strMLot3).Length > modProperty.Statics.intTemp)
					{
						modProperty.Statics.intTemp = Strings.Trim(strMLot3).Length;
					}
					if (Strings.Trim(strMLot4).Length > modProperty.Statics.intTemp)
					{
						modProperty.Statics.intTemp = Strings.Trim(strMLot4).Length;
					}
					modProperty.Statics.intTemp = intNumChars - modProperty.Statics.intTemp;
					if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
					{
						intCurLine = 1;
					}
					else if (Strings.Trim(strMLot4) != string.Empty)
					{
						intCurLine = 2;
					}
					else
					{
						intCurLine = 3;
					}
					// If Trim(strMLot4) <> vbNullString Then
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, modProperty.Statics.intTemp - 2, Strings.Left(strL[intCurLine] + "", modProperty.Statics.intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, modProperty.Statics.intTemp, intMaxMapLen, strMLot1 + "");
					strL[intCurLine] = strTemp;
					intCurLine += 1;
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, modProperty.Statics.intTemp - 2, Strings.Left(strL[intCurLine] + "", modProperty.Statics.intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, modProperty.Statics.intTemp, intMaxMapLen, strMLot2 + "");
					strL[intCurLine] = strTemp;
					intCurLine += 1;
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, modProperty.Statics.intTemp - 2, Strings.Left(strL[intCurLine] + "", modProperty.Statics.intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, modProperty.Statics.intTemp, intMaxMapLen, strMLot3 + "");
					strL[intCurLine] = strTemp;
					intCurLine += 1;
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, modProperty.Statics.intTemp - 2, Strings.Left(strL[intCurLine] + "", modProperty.Statics.intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, modProperty.Statics.intTemp, intMaxMapLen, strMLot4 + "");
					strL[intCurLine] = strTemp;
					// Else
					// 47                strTemp = String(intNumChars, " ")
					// 48                Mid(strTemp, 1, intTemp - 2) = Left(strLine3 & "", intTemp - 2)
					// 49                Mid(strTemp, intTemp, intMaxMapLen) = strMLot1 & ""
					// 50                strLine3 = strTemp
					// 51                strTemp = String(intNumChars, " ")
					// 52                Mid(strTemp, 1, intTemp - 2) = Left(strLine4 & "", intTemp - 2)
					// 53                Mid(strTemp, intTemp, intMaxMapLen) = strMLot2 & ""
					// 54                strLine4 = strTemp
					// 55                strTemp = String(intNumChars, " ")
					// 56                Mid(strTemp, 1, intTemp - 2) = Left(strLine5 & "", intTemp - 2)
					// 57                Mid(strTemp, intTemp, intMaxMapLen) = strMLot3 & ""
					// 58                strLine5 = strTemp
					// End If
				}
				if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					strTemp = FCConvert.ToString(lngAct);
					modProperty.Statics.intTemp = strTemp.Length;
					strL[5] = Strings.Left(strL[5] + Strings.StrDup(intCharLen, " "), intCharLen);
					fecherFoundation.Strings.MidSet(ref strL[5], intCharLen - modProperty.Statics.intTemp, " " + strTemp);
				}
				if ((Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rssecowner"))) != string.Empty) && boolIncSecOwner && intMaxLines < 6)
				{
					if (Strings.Trim(strL[6]) != string.Empty)
					{
						if (!boolUseDotmatrix)
						{
							strL[2] = strL[3];
							strL[3] = strL[4];
							strL[4] = strL[5];
							strL[5] = strL[6];
							strL[6] = "";
						}
					}
				}
				if (Strings.Trim(strL[1]) == string.Empty)
				{
					strL[1] = strL[2];
					strL[2] = strL[3];
					strL[3] = strL[4];
					strL[4] = strL[5];
					strL[5] = strL[5];
					strL[6] = "";
				}
				if (Strings.Trim(strL[2]) == string.Empty)
				{
					strL[2] = strL[3];
					strL[3] = strL[4];
					strL[4] = strL[5];
					strL[5] = strL[6];
					strL[6] = "";
				}
				if (Strings.Trim(strL[3]) == string.Empty)
				{
					strL[3] = strL[4];
					strL[4] = strL[5];
					strL[5] = strL[6];
					strL[6] = "";
				}
				if (Strings.Trim(strL[4]) == string.Empty)
				{
					strL[4] = strL[5];
					strL[5] = strL[6];
					strL[6] = "";
				}
				if (Strings.Trim(strL[5]) == string.Empty)
				{
					strL[5] = strL[6];
					strL[6] = "";
				}
				if (intMaxLines < 6 && Strings.Trim(strL[6]) != string.Empty)
				{
					strL[6] = string.Empty;
				}
				strLine1 = Strings.RTrim(strL[1]);
				strLine2 = Strings.RTrim(strL[2]);
				strLine3 = Strings.RTrim(strL[3]);
				strLine4 = Strings.RTrim(strL[4]);
				strLine5 = Strings.RTrim(strL[5]);
				strLine6 = Strings.RTrim(strL[6]);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in Make Address Card");
			}
		}

		private void makeassessmentcard()
		{
			// vbPorter upgrade warning: tValue As int	OnWriteFCConvert.ToDouble(
			int tValue = 0;
			try
			{
				// On Error GoTo ErrorHandler
				// vbPorter upgrade warning: strTemp As string	OnWrite(int, string)
				string strTemp;
				double dblTemp = 0;
				int intCharLen;
				int intExemptChars = 0;
				int intTemp;
				short intLandchars = 0;
				short intBldgchars = 0;
				int intExemptLeft = 0;
				int intLandLeft = 0;
				int intBldgLeft = 0;
				int intTotalLeft;
				int intTotalChars = 0;
				int lngLand = 0;
				int lngbldg = 0;
				// vbPorter upgrade warning: lngExempt As int	OnRead(string)
				int lngExempt = 0;
				intCharLen = intCharactersWide;
				strLine1 = "";
				strLine2 = "";
				if (boolByAccount)
				{
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					tValue = FCConvert.ToInt32(Conversion.Val(RSMster.Get_Fields("landsum") + "") + Conversion.Val(RSMster.Get_Fields("bldgsum") + "") - Conversion.Val(RSMster.Get_Fields("exemptsum") + ""));
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields("landsum") + "")));
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields("bldgsum") + "")));
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields("exemptsum") + "")));
					strLine2 = " " + "ACCT:" + Strings.Format(RSMster.Get_Fields_Int32("rsaccount"), "@@@@@@") + "-" + FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rscard"))) + "  " + "Map/Lot:" + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot")));
				}
				else
				{
					tValue = FCConvert.ToInt32(Conversion.Val(RSMster.Get_Fields_Int32("lastlandval")) + Conversion.Val(RSMster.Get_Fields_Int32("lastbldgval")) - Conversion.Val(RSMster.Get_Fields_Int32("rlexemption")));
					lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields_Int32("lastlandval") + "")));
					lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields_Int32("lastbldgval") + "")));
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields_Int32("rlexemption") + "")));
					strLine2 = " " + "ACCT:" + Strings.Format(RSMster.Get_Fields_Int32("rsaccount"), "@@@@@@") + "-" + FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rscard"))) + "  " + "Map/Lot:" + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot")));
				}
				// txtYear.Text = strTaxYear & ""
				// txtland.DataValue = lngLand
				// txtbldg.DataValue = lngBldg
				// txttotal.DataValue = Tvalue
				// txtexempt.Width = 1080
				if (boolIncETax)
				{
					dblTemp = FCConvert.ToDouble(tValue) * (snglTax / 1000);
					if (intCPI > 10)
					{
						strLine2 += "  Est. Tax: " + Strings.Format(dblTemp, "#,###,##0.00");
					}
					else
					{
						strLine2 += "  E.Tax " + Strings.Format(dblTemp, "#,###,##0.00");
					}
				}
				strTemp = FCConvert.ToString(lngExempt);
				switch (intCPI)
				{
					case 10:
						{
							intLandLeft = 7;
							intLandchars = 11;
							intBldgLeft = 19;
							intBldgchars = 11;
							intExemptLeft = 30;
							intExemptChars = 7;
							intTotalLeft = 37;
							intTotalChars = 11;
							// If Len(Format(strTemp, "#,###,###,##0")) > 7 Then
							// txtexempt.Width = 1440
							// End If
							break;
						}
					case 12:
						{
							intLandLeft = 8;
							intLandchars = 14;
							intBldgLeft = 23;
							intBldgchars = 13;
							intExemptLeft = 37;
							intExemptChars = 8;
							intTotalLeft = 46;
							intTotalChars = 12;
							// If Len(Format(strTemp, "#,###,###,##0")) > 8 Then
							// txtexempt.Width = 1440
							// End If
							break;
						}
					case 17:
						{
							intLandLeft = 11;
							intLandchars = 20;
							intBldgLeft = 32;
							intBldgchars = 19;
							intExemptLeft = 52;
							intExemptChars = 11;
							intTotalLeft = 64;
							intTotalChars = 15;
							// If Len(Format(strTemp, "#,###,###,##0")) > 12 Then
							// txtexempt.Width = 1440
							// End If
							break;
						}
				}
				//end switch
				strTemp = Strings.Format(strTemp, "#,###,###,##0");
				// txtexempt.Text = strTemp
				strLine1 = Strings.StrDup(intCharLen, " ");
				fecherFoundation.Strings.MidSet(ref strLine1, 1, 5, strTaxYear);
				// 11            Mid(strLine1, 7, 10) = Format(Val(RSMster.Fields("landsum") & ""), "##,###,##0")
				fecherFoundation.Strings.MidSet(ref strLine1, intLandLeft, intLandchars, modGlobalRoutines.NumtoString(ref lngLand, ref intLandchars));
				// 12            Mid(strLine1, 18, 11) = Format(Val(RSMster.Fields("bldgsum") & ""), "###,###,##0")
				fecherFoundation.Strings.MidSet(ref strLine1, intBldgLeft, intBldgchars, modGlobalRoutines.NumtoString(ref lngbldg, ref intBldgchars));
				// strTemp = Format(Val(RSMster.Fields("exemptsum") & ""), "#,###,###,##0")
				strTemp = modGlobalRoutines.NumtoString_6(lngExempt, 12);
				if (Strings.Trim(strTemp).Length > intExemptChars)
				{
					strTemp = Strings.Trim(strTemp);
					strTemp = " " + strTemp;
				}
				else
				{
					strTemp = Strings.Right(strTemp, intExemptChars);
				}
				fecherFoundation.Strings.MidSet(ref strLine1, intExemptLeft, Strings.Len(strTemp), strTemp);
				intTemp = strTemp.Length - intExemptChars;
				fecherFoundation.Strings.MidSet(ref strLine1, intExemptLeft + Strings.Len(strTemp), intTotalChars - intTemp, modGlobalRoutines.NumtoString_6(tValue, FCConvert.ToInt16(intTotalChars - intTemp)));
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in Make Assessment Card");
			}
		}

		private void MakeCardLabel()
		{
			string strNam;
			string strSecOwn = "";
			string strML;
			string strST;
			string strSale = "";
			string strBookPage = "";
			string[] strTempLine = new string[6 + 1];
			int intIndex;
			try
			{
				// On Error GoTo ErrorHandler
				strNam = FCConvert.ToString(RSMster.Get_Fields_String("rsname"));
				if (boolIncSecOwner)
				{
					strSecOwn = Strings.Trim(RSMster.Get_Fields_String("rssecowner") + "");
				}
				else
				{
					strSecOwn = "";
				}
				strML = Strings.Trim(RSMster.Get_Fields_String("rsmaplot") + "");
				strST = Strings.Trim(RSMster.Get_Fields_String("rslocnumalph") + " " + RSMster.Get_Fields_String("rslocstreet"));
				if (boolShowSaleData)
				{
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(RSMster.Get_Fields_DateTime("saledate")))
					{
						if (Information.IsDate(RSMster.Get_Fields("saledate")))
						{
							if (RSMster.Get_Fields_DateTime("saledate").ToOADate() != 0)
							{
								strSale = Strings.Trim(Strings.Format(RSMster.Get_Fields_DateTime("saledate"), "MM/dd/yyyy") + " $" + Strings.Format(FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("pisaleprice") + "")), "#,###,##0"));
							}
							else
							{
								strSale = "";
							}
						}
						else
						{
							strSale = "";
						}
					}
					else
					{
						strSale = "";
					}
					if (!(clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile()))
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (clsBookPage.Get_Fields("account") == RSMster.Get_Fields_Int32("rsaccount"))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strBookPage = Strings.Trim("B " + clsBookPage.Get_Fields("book") + " P " + clsBookPage.Get_Fields("Page"));
							// ElseIf clsBookPage.FindFirstRecord("account", Val(RSMster.Fields("rsaccount"))) Then
						}
						else if (clsBookPage.FindFirst("account = " + FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rsaccount")))))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strBookPage = Strings.Trim("B " + clsBookPage.Get_Fields("book") + " P " + clsBookPage.Get_Fields("Page"));
						}
						else
						{
							strBookPage = "";
						}
					}
					else
					{
						strBookPage = "";
					}
				}
				else
				{
					strSale = "";
					strBookPage = "";
				}
				FCUtils.EraseSafe(strTempLine);
				strTempLine[1] = strML;
				strSale = Strings.Trim(strBookPage + " " + strSale);
				intIndex = 3;
				if ((strSale != string.Empty) && (strSecOwn != string.Empty) && (strST != string.Empty))
				{
					intIndex = 2;
				}
				strTempLine[intIndex] = strNam;
				intIndex += 1;
				if (strSecOwn == string.Empty && (strSale == string.Empty))
				{
					strTempLine[5] = strST;
				}
				else
				{
					if (strSecOwn != string.Empty)
					{
						strTempLine[intIndex] = strSecOwn;
						intIndex += 1;
					}
					if (strST != string.Empty)
					{
						strTempLine[intIndex] = strST;
						intIndex += 1;
					}
					if (strSale != string.Empty)
					{
						strTempLine[intIndex] = strSale;
						intIndex += 1;
					}
				}
				strLine1 = strTempLine[1];
				strLine2 = strTempLine[2];
				strLine3 = strTempLine[3];
				strLine4 = strTempLine[4];
				strLine5 = strTempLine[5];
				strLine6 = "";
				if (intCharactersWide > 0)
				{
					strLine1 = Strings.Trim(" " + Strings.Mid(strLine1, 1, intCharactersWide));
					strLine2 = Strings.Trim(" " + Strings.Mid(strLine2, 1, intCharactersWide));
					strLine3 = Strings.Trim(" " + Strings.Mid(strLine3, 1, intCharactersWide));
					strLine4 = Strings.Trim(" " + Strings.Mid(strLine4, 1, intCharactersWide));
					strLine5 = Strings.Trim(" " + Strings.Mid(strLine5, 1, intCharactersWide));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeCardLabel", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeCustom()
		{
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
			string strTemp = "";
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string strNam = "";
                string strA1 = "";
                string strA2 = "";
                string strA3 = "";
                string strSecOwn = "";
                string strZip = "";
                string strZip4 = "";
                string strST = "";
                string strML = "";
                // vbPorter upgrade warning: lngAct As int	OnWrite(string)	OnRead(string)
                int lngAct = 0;
                string strR1 = "";
                string strR2 = "";
                string strLC = "";
                // vbPorter upgrade warning: strRefBookPage As Variant --> As string
                string strRefBookPage = "";
                string strAcres = "";
                int intCharLen;
                int intMaxMap;
                int intCurLine = 0;
                string[] strL = new string[6 + 1];
                int x;
                string strCountry = "";
                try
                {
                    // On Error GoTo ErrorHandler
                    for (x = 1; x <= 6; x++)
                    {
                        strL[x] = "";
                    }

                    // x
                    intCharLen = intCharactersWide;
                    if (!boolElimDups)
                    {
                        strNam = RSMster.Get_Fields_String("rsname") + "";
                        strA1 = RSMster.Get_Fields_String("rsaddr1") + "";
                        strAcres = "Acres " + Strings.Format(Conversion.Val(RSMster.Get_Fields_Double("piacres") + ""),
                            "0.00");
                        // 3        Call escapequote(strNam)
                        // 4        Call escapequote(strA1)
                    }
                    else
                    {
                        strNam = Grid.TextMatrix(lngCurRow, 0);
                        strA1 = Grid.TextMatrix(lngCurRow, 1);
                        strAcres = "Acres " + Grid.TextMatrix(lngCurRow, 9);
                        // 7        Call escapequote(strNam)
                        // 8        Call escapequote(strA1)
                    }

                    // If intLabelKind <> 4013 And intLabelKind <> 4030 Then
                    // intCharLen = 39
                    // Else
                    // intCharLen = 33
                    // End If
                    if (boolElimDups)
                    {
                        clsTemp.OpenRecordset(
                            "select * from master where rsaccount  = " + Grid.TextMatrix(lngCurRow, 3) +
                            " and rscard = " + Grid.TextMatrix(lngCurRow, 8), modGlobalVariables.strREDatabase);
                        strRefBookPage = "";
                        if (boolUseBookPage)
                        {
                            if (!(clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile()))
                            {
                                // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                                if (clsBookPage.Get_Fields("account") == RSMster.Get_Fields_Int32("rsaccount"))
                                {
                                    // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                    // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                    strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" +
                                                     clsBookPage.Get_Fields("Page");
                                    while (clsBookPage.FindNextRecord("account", RSMster.Get_Fields_Int32("rsaccount")))
                                    {
                                        // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                        strRefBookPage +=
                                            " B" + clsBookPage.Get_Fields("book") + "P" +
                                            clsBookPage.Get_Fields("Page");
                                    }

                                    // ElseIf clsBookPage.FindFirstRecord("account", GRID1.TextMatrix(lngCurRow, 3)) Then
                                }
                                else if (clsBookPage.FindFirst("account = " + Grid.TextMatrix(lngCurRow, 3)))
                                {
                                    // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                    // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                    strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" +
                                                     clsBookPage.Get_Fields("Page");
                                    while (clsBookPage.FindNextRecord("account", Grid.TextMatrix(lngCurRow, 3)))
                                    {
                                        // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                        strRefBookPage +=
                                            " B" + clsBookPage.Get_Fields("book") + "P" +
                                            clsBookPage.Get_Fields("Page");
                                    }
                                }
                            }
                        }
                        else
                        {
                            strRefBookPage = clsTemp.Get_Fields_String("RSref1") + "";
                        }

                        strA2 = FCConvert.ToString(clsTemp.Get_Fields_String("rsaddr2"));
                        strA3 = FCConvert.ToString(clsTemp.Get_Fields_String("rsaddr3"));
                        strSecOwn = FCConvert.ToString(clsTemp.Get_Fields_String("rssecowner"));
                        strZip = FCConvert.ToString(clsTemp.Get_Fields_String("rszip"));
                        strZip4 = FCConvert.ToString(clsTemp.Get_Fields_String("rszip4"));
                        strST = FCConvert.ToString(clsTemp.Get_Fields_String("rsstate"));
                        strCountry = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("country")));
                        strML = FCConvert.ToString(clsTemp.Get_Fields_String("rsmaplot"));
                        lngAct = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("rsaccount"));
                        strR1 = strRefBookPage;
                        strR2 = FCConvert.ToString(clsTemp.Get_Fields_String("rsref2"));
                        if (Conversion.Val(clsTemp.Get_Fields_String("rslocnumalph")) > 0)
                        {
                            strLC = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("rslocnumalph"))) + " " +
                                    clsTemp.Get_Fields_String("rslocstreet");
                        }
                        else
                        {
                            strLC = FCConvert.ToString(clsTemp.Get_Fields_String("rslocstreet"));
                        }
                    }
                    else
                    {
                        strRefBookPage = "";
                        if (boolUseBookPage)
                        {
                            if (!(clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile()))
                            {
                                // If clsBookPage.FindFirstRecord("account", RSMster.Fields("rsaccount")) Then
                                if (clsBookPage.FindFirst("account = " + RSMster.Get_Fields_Int32("rsaccount")))
                                {
                                    // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                    // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                    strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" +
                                                     clsBookPage.Get_Fields("Page");
                                    while (clsBookPage.FindNextRecord("account", RSMster.Get_Fields_Int32("rsaccount")))
                                    {
                                        // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                        strRefBookPage +=
                                            " B" + clsBookPage.Get_Fields("book") + "P" +
                                            clsBookPage.Get_Fields("Page");
                                    }
                                }
                            }
                        }
                        else
                        {
                            strRefBookPage = RSMster.Get_Fields_String("RSref1") + "";
                        }

                        strA2 = RSMster.Get_Fields_String("rsaddr2") + "";
                        strA3 = RSMster.Get_Fields_String("rsaddr3") + "";
                        strSecOwn = RSMster.Get_Fields_String("rssecowner") + "";
                        strZip = RSMster.Get_Fields_String("rszip") + "";
                        strZip4 = RSMster.Get_Fields_String("rszip4") + "";
                        strST = RSMster.Get_Fields_String("rsstate") + "";
                        strCountry = FCConvert.ToString(RSMster.Get_Fields_String("country"));
                        strML = RSMster.Get_Fields_String("rsmaplot") + "";
                        lngAct = FCConvert.ToInt32(RSMster.Get_Fields_Int32("rsaccount") + "");
                        strR1 = strRefBookPage;
                        strR2 = RSMster.Get_Fields_String("rsref2") + "";
                        strLC = RSMster.Get_Fields_String("rslocstreet") + "";
                        if (Conversion.Val(RSMster.Get_Fields_String("rslocnumalph") + "") > 0)
                        {
                            strLC = Strings.Trim(RSMster.Get_Fields_String("rslocnumalph") + "") + " " +
                                    RSMster.Get_Fields_String("rslocstreet") + "";
                        }
                        else
                        {
                            strLC = RSMster.Get_Fields_String("rslocstreet") + "";
                        }
                    }

                    strLine1 = Strings.StrDup(intCharLen, " ");
                    strLine2 = "";
                    strLine3 = "";
                    strLine4 = "";
                    strLine5 = "";
                    strLine6 = "";
                    strL[1] = Strings.StrDup(intCharLen, " ");
                    if (boolIncOwner)
                    {
                        fecherFoundation.Strings.MidSet(ref strLine1, 1,
                            Strings.Left(strLeftBlank + strNam + "", intCharLen));
                        strL[1] = strLine1;
                        intCurLine = 2;
                    }
                    else
                    {
                        intCurLine = 1;
                    }

                    if (boolIncSecOwner && Strings.Trim(strSecOwn) != string.Empty && intCurLine <= intMaxLines)
                    {
                        strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strSecOwn), 1, intCharLen);
                        intCurLine += 1;
                    }

                    if (boolIncAddr)
                    {
                        if (Strings.Trim(strA1) != string.Empty && intCurLine <= intMaxLines)
                        {
                            strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strA1), 1, intCharLen);
                            intCurLine += 1;
                        }

                        if (Strings.Trim(strA2) != string.Empty && intCurLine <= intMaxLines)
                        {
                            strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strA2), 1, intCharLen);
                            intCurLine += 1;
                        }

                        if (Strings.Trim(strA3 + strST + strZip + strZip4) != string.Empty && intCurLine <= intMaxLines)
                        {
                            strL[intCurLine] =
                                Strings.Mid(
                                    strLeftBlank +
                                    Strings.Trim(Strings.Trim(strA3 + "") + " " + Strings.Trim(strST + "") + " " +
                                                 Strings.Trim(strZip + "") + " " + Strings.Trim(strZip4 + "")), 1,
                                    intCharLen);
                            intCurLine += 1;
                        }

                        if (strCountry != string.Empty && intCurLine <= intMaxLines)
                        {
                            strL[intCurLine] = strLeftBlank + strCountry;
                            intCurLine += 1;
                        }
                    }

                    if (boolIncMap && !modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide &&
                        intCurLine <= intMaxLines)
                    {
                        strL[intCurLine] = strLeftBlank + "Maplot: " +
                                           Strings.Mid(Strings.Trim(strML + ""), 1, intCharLen - 9);
                        intCurLine += 1;
                    }

                    if (boolIncRef && Strings.Trim(strR1) != string.Empty && intCurLine <= intMaxLines)
                    {
                        strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strR1 + ""), 1, intCharLen);
                        intCurLine += 1;
                        if (boolIncMap && modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide &&
                            intCurLine <= intMaxLines)
                        {
                            stripmiddlespaces(ref strL[intCurLine]);
                        }
                    }

                    if (boolIncLoc && Strings.Trim(strLC) != string.Empty && intCurLine <= intMaxLines)
                    {
                        strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strLC + ""), 1, intCharLen);
                        intCurLine += 1;
                    }

                    if (boolIncRef2 && Strings.Trim(strR2) != string.Empty && intCurLine <= intMaxLines)
                    {
                        strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strR2 + ""), 1, intCharLen);
                        intCurLine += 1;
                    }

                    if (boolIncAcres && Strings.Trim(strAcres) != string.Empty && intCurLine <= intMaxLines)
                    {
                        strL[intCurLine] = Strings.Mid(strLeftBlank + Strings.Trim(strAcres + ""), 1, intCharLen);
                        intCurLine += 1;
                    }

                    if (boolIncAcct && !modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
                    {
                        strL[1] = Strings.Left(strL[1] + "", intCharLen - 7);
                        if (strL[1].Length < intCharLen - 7)
                        {
                            strL[1] += Strings.StrDup(intCharLen - 7 - strL[1].Length, " ");
                        }

                        strL[1] += " " + Strings.Format(lngAct, "@@@@@@");
                        // 69        Mid(strL(1), intCharLen - 7, 7) = " " & Format(lngAct & "", "@@@@@@")
                    }

                    // If boolIncSecOwner And Trim(strSecOwn) <> vbNullString Then
                    // 43        strLine2 = Mid(" " & Trim(strSecOwn), 1, intCharLen)
                    // If boolIncAddr Then
                    // 44            strLine3 = Mid(" " & Trim(strA1), 1, intCharLen)
                    // 45            If Trim(strA2 & "") <> vbNullString And Trim(strA1) <> vbNullString Then
                    // 46                strLine4 = Mid(" " & Trim(strA2), 1, intCharLen)
                    // 47                strLine5 = Mid(" " & Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, 38)
                    // If boolIncRef Then
                    // 48                    strLine6 = Mid(" " & Trim(strR1 & ""), 1, intCharLen)
                    // If boolDotMatrix Then
                    // 49                       Call stripmiddlespaces(strLine6)
                    // End If
                    // Else
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 50                            strLine6 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // End If
                    // End If
                    // 
                    // Else
                    // If Trim(strA2) <> vbNullString Then strLine3 = Mid(" " & Trim(strA2), 1, intCharLen)
                    // 51                strLine4 = Mid(" " & Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, intCharLen)
                    // 
                    // If boolIncRef And Trim(strR1) <> vbNullString Then
                    // 52                    strLine5 = Mid(" " & Trim(strR1 & ""), 1, intCharLen)
                    // If Not boolDotMatrix Then
                    // If boolIncMap Then
                    // 53                            strLine6 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // Else
                    // 54                        Call stripmiddlespaces(strLine5)
                    // End If
                    // Else
                    // If Not boolDotMatrix Then
                    // If boolIncMap Then
                    // 55                            strLine5 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // End If
                    // End If
                    // End If
                    // Else
                    // 
                    // If boolIncRef Then
                    // 56                strLine3 = Mid(" " & Trim(strR1 & ""), 1, intCharLen)
                    // If boolDotMatrix Then
                    // 57                    Call stripmiddlespaces(strLine3)
                    // End If
                    // If boolIncRef2 Then
                    // 58                strLine4 = Mid(" " & Trim(strR2 & ""), 1, intCharLen)
                    // End If
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 59                        strLine5 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // End If
                    // Else
                    // If boolIncRef2 Then
                    // 60                    strLine3 = Mid(" " & Trim(strR2 & ""), 1, intCharLen)
                    // End If
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 61                        strLine4 = Mid(" " & Trim(strML & ""), 1, intCharLen)
                    // End If
                    // End If
                    // End If
                    // 
                    // If boolIncLoc Then
                    // 62                strLine6 = Mid(" " & Trim(strLC & ""), 1, intCharLen)
                    // End If
                    // End If
                    // If strLine2 = vbNullString Then
                    // 63                strLine2 = strLine3
                    // 64                strLine3 = strLine4
                    // 65                strLine4 = strLine5
                    // 66                strLine5 = strLine6
                    // 67                strLine6 = ""
                    // End If
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // ElseIf boolIncAddr Then
                    // 68        strLine2 = Mid(" " & Trim(strA1), 1, intCharLen)
                    // If Trim(strA2 & "") <> vbNullString And Trim(strA1) <> vbNullString Then
                    // 69            strLine3 = Mid(" " & Trim(strA2), 1, intCharLen)
                    // 70            strLine4 = Mid(" " & Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, intCharLen)
                    // If boolIncRef And Trim(strR1) <> vbNullString Then
                    // 71         strLine5 = Mid(" " & Trim(strR1 & ""), 1, intCharLen)
                    // If boolDotMatrix Then
                    // 72            Call stripmiddlespaces(strLine5)
                    // End If
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 73                strLine6 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // End If
                    // Else
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 74                strLine5 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // End If
                    // End If
                    // 
                    // Else
                    // If Trim(strA2) <> vbNullString Then strLine2 = Mid(" " & Trim(strA2), 1, intCharLen)
                    // 75            strLine3 = Mid(" " & Trim(strA3 & "") & " " & Trim(strSt & "") & "  " & Trim(strZip & "") & " " & Trim(strZip4 & ""), 1, intCharLen)
                    // 
                    // If boolIncRef Then
                    // 76                strLine4 = Mid(" " & Trim(strR1 & ""), 1, intCharLen)
                    // If Not boolDotMatrix Then
                    // 77                    strLine5 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // Else
                    // 78                    Call stripmiddlespaces(strLine4)
                    // End If
                    // Else
                    // If Not boolDotMatrix Then
                    // 79                    strLine4 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen)
                    // End If
                    // End If
                    // End If
                    // If strLine2 = vbNullString Then
                    // 80            strLine2 = strLine3
                    // 81            strLine3 = strLine4
                    // 82            strLine4 = strLine5
                    // 83            strLine5 = strLine6
                    // 84            strLine6 = ""
                    // End If
                    // 
                    // Else
                    // If boolIncRef Then
                    // 85            strLine2 = Mid(" " & Trim(strR1 & ""), 1, intCharLen)
                    // If boolDotMatrix Then
                    // 86                Call stripmiddlespaces(strLine2)
                    // End If
                    // If boolIncRef2 Then
                    // 87            strLine3 = Mid(" " & Trim(strR2 & ""), 1, intCharLen)
                    // End If
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 88                    strLine4 = " MapLot: " & Mid(Trim(strML & ""), 1, intCharLen - 9)
                    // End If
                    // End If
                    // Else
                    // If boolIncRef2 Then
                    // 89                strLine2 = Mid(" " & Trim(strR2 & ""), 1, intCharLen)
                    // End If
                    // If boolIncMap Then
                    // If Not boolDotMatrix Then
                    // 90                    strLine3 = Mid(" " & Trim(strML & ""), 1, intCharLen)
                    // End If
                    // End If
                    // End If
                    // 
                    // If boolIncLoc Then
                    // 91            strLine5 = Mid(" " & Trim(strLC & ""), 1, intCharLen)
                    // End If
                    // End If
                    // 
                    // If strLine5 = vbNullString Then
                    // 92        strLine5 = strLine6
                    // 93        strLine6 = ""
                    // End If
                    // If strLine4 = vbNullString Then
                    // 94        strLine4 = strLine5
                    // 95        strLine5 = strLine6
                    // 96        strLine6 = ""
                    // End If
                    // If strLine3 = vbNullString Then
                    // 97        strLine3 = strLine4
                    // 98        strLine4 = strLine5
                    // 99        strLine5 = strLine6
                    // 100        strLine6 = ""
                    // End If
                    // If strLine2 = vbNullString Then
                    // 101        strLine2 = strLine3
                    // 102        strLine3 = strLine4
                    // 103        strLine4 = strLine5
                    // 104        strLine5 = strLine6
                    // 105        strLine6 = ""
                    // End If
                    // 
                    // 
                    // strLine1 = strL(1)
                    // strLine2 = strL(2)
                    // strLine3 = strL(3)
                    // strLine4 = strL(4)
                    // strLine5 = strL(5)
                    // strLine6 = strL(6)
                    if (intLabelKind == 4013 || intLabelKind == 4030)
                    {
                        strL[1] = Strings.Mid(strL[1], 1, intCharLen);
                        strL[2] = Strings.Mid(strL[2], 1, intCharLen);
                        strL[3] = Strings.Mid(strL[3], 1, intCharLen);
                        strL[4] = Strings.Mid(strL[4], 1, intCharLen);
                        strL[5] = Strings.Mid(strL[5], 1, intCharLen);
                        strL[6] = Strings.Mid(strL[6], 1, intCharLen);
                    }

                    if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
                    {
                        strMLot1 = "";
                        strMLot2 = "";
                        strMLot3 = "";
                        strMLot4 = "";
                        intMaxMap = ParseMapLot(strML + "", ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
                        // End If
                        // If Trim(strMLot4) <> vbNullString Then
                        if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
                        {
                            intCurLine = 1;
                        }
                        else if (Strings.Trim(strMLot4) != string.Empty)
                        {
                            intCurLine = 2;
                        }
                        else
                        {
                            intCurLine = 3;
                        }

                        strTemp = Strings.StrDup(intCharLen, " ");
                        fecherFoundation.Strings.MidSet(ref strTemp, 1, intCharLen,
                            Strings.Left(strL[intCurLine] + "", intCharLen));
                        fecherFoundation.Strings.MidSet(ref strTemp, intCharLen - intMaxMap, intMaxMap + 1,
                            " " + strMLot1 + "");
                        strL[intCurLine] = strTemp;
                        intCurLine += 1;
                        strTemp = Strings.StrDup(intCharLen, " ");
                        fecherFoundation.Strings.MidSet(ref strTemp, 1, intCharLen,
                            Strings.Left(strL[intCurLine] + "", intCharLen));
                        fecherFoundation.Strings.MidSet(ref strTemp, intCharLen - intMaxMap, intMaxMap + 1,
                            " " + strMLot2 + "");
                        strL[intCurLine] = strTemp;
                        intCurLine += 1;
                        strTemp = Strings.StrDup(intCharLen, " ");
                        fecherFoundation.Strings.MidSet(ref strTemp, 1, intCharLen,
                            Strings.Left(strL[intCurLine] + "", intCharLen));
                        fecherFoundation.Strings.MidSet(ref strTemp, intCharLen - intMaxMap, intMaxMap + 1,
                            " " + strMLot3 + "");
                        strL[intCurLine] = strTemp;
                        strTemp = Strings.StrDup(intCharLen, " ");
                        fecherFoundation.Strings.MidSet(ref strTemp, 1, intCharLen,
                            Strings.Left(strL[intCurLine] + "", intCharLen));
                        fecherFoundation.Strings.MidSet(ref strTemp, intCharLen - intMaxMap, intMaxMap + 1,
                            " " + strMLot4 + "");
                        strL[intCurLine] = strTemp;
                        // Else
                        // 124                strTemp = String(intCharLen, " ")
                        // 125                Mid(strTemp, 1, intCharLen) = Left(strLine3 & "", intCharLen)
                        // 126                Mid(strTemp, intCharLen - intMaxMap, intMaxMap + 1) = strMLot1 & ""
                        // 127                strLine3 = strTemp
                        // 128                strTemp = String(intCharLen, " ")
                        // 129                Mid(strTemp, 1, intCharLen) = Left(strLine4 & "", intCharLen)
                        // 130                Mid(strTemp, intCharLen - intMaxMap, intMaxMap + 1) = " " & strMLot2 & ""
                        // 131                strLine4 = strTemp
                        // 132                strTemp = String(intCharLen, " ")
                        // 133                Mid(strTemp, 1, intCharLen) = Left(strLine5 & "", intCharLen)
                        // 134                Mid(strTemp, intCharLen - intMaxMap, intMaxMap + 1) = " " & strMLot3 & ""
                        // 135                strLine5 = strTemp
                        // End If
                    }

                    if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
                    {
                        strTemp = FCConvert.ToString(lngAct);
                        modProperty.Statics.intTemp = strTemp.Length;
                        strL[5] = Strings.Left(strL[5] + Strings.StrDup(intCharLen, " "), intCharLen);
                        fecherFoundation.Strings.MidSet(ref strL[5], intCharLen - modProperty.Statics.intTemp,
                            " " + strTemp);
                    }

                    strLine1 = Strings.RTrim(strL[1]);
                    strLine2 = Strings.RTrim(strL[2]);
                    strLine3 = Strings.RTrim(strL[3]);
                    strLine4 = Strings.RTrim(strL[4]);
                    strLine5 = Strings.RTrim(strL[5]);
                    strLine6 = Strings.RTrim(strL[6]);
                    return;
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                                    Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() +
                                    " in Make Custom");
                }
            }
        }

		private void MakeChangeAssessment()
		{
			// vbPorter upgrade warning: lngTotal As int	OnWriteFCConvert.ToDouble(
			int lngTotal = 0;
			// vbPorter upgrade warning: strTemp As string	OnWrite(float, string)
			string strTemp = "";
			string strTemp2 = "";
			int intCharLen;
			int intTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				intCharLen = intCharactersWide;
				if (intCharLen == 0)
				{
					intCharLen = 40;
				}
				strLine1 = "";
				strLine2 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				if (boolFirstLabel)
				{
					intTemp = FCConvert.ToString(FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rsaccount"))) + "-" + FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rscard")))).Length;
					strLine1 = Strings.Mid(" " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("RSNAME"))), 1, intCharLen - intTemp - 1) + " " + (FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rsaccount"))) + "-" + FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rscard"))));
					strLine2 = Strings.Mid(" " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rssecowner"))), 1, intCharLen);
					strLine3 = Strings.Mid(" " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))), 1, intCharLen);
					strLine4 = Strings.Mid(" " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen);
					strLine5 = Strings.Mid(" " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen);
				}
				else
				{
					strLine1 = Strings.StrDup(intCharLen, " ");
					lngTotal = FCConvert.ToInt32(Conversion.Val(RSMster.Get_Fields_Int32("lastlandval")) + Conversion.Val(RSMster.Get_Fields_Int32("lastbldgval")) - Conversion.Val(RSMster.Get_Fields_Int32("rlexemption")));
					strLine2 = Strings.StrDup(intCharLen, " ");
					intTemp = Strings.Trim(RSMster.Get_Fields_String("rsmaplot") + "").Length;
					fecherFoundation.Strings.MidSet(ref strLine2, 1, 28, Strings.Mid(" MAP/LOT:" + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot"))), 1, 28));
					strLine2 = Strings.Left(" " + "MAP/LOT:" + Strings.Trim(RSMster.Get_Fields_String("rsmaplot") + "") + Strings.StrDup(28, " "), 28);
					strLine3 = Strings.StrDup(intCharLen, " ");
					strLine4 = Strings.StrDup(intCharLen, " ");
					/*? 12 */
					if (boolIncETax)
					{
						// strline6 = " Estimated Tax:"
						if (boolUseDotmatrix)
						{
							strLine5 = Strings.StrDup(intCharLen, " ");
						}
						else
						{
							strLine5 = Strings.StrDup(16, " ");
						}
						// Mid(strLine5, 1, 15) = " Estimated Tax:"
						fecherFoundation.Strings.MidSet(ref strLine5, 1, 15, " Estimated Tax:");
						// strLine5 = strLine5 & "       " & Format((snglTax / 1000) * lngTotal, "##,###,##0")
						txtTax.Value = (snglTax / 1000) * lngTotal;
						strTemp = FCConvert.ToString((snglTax / 1000) * lngTotal);
						if (boolUseDotmatrix)
						{
							strTemp = Strings.Format(strTemp, "###,###,##0.00");
							fecherFoundation.Strings.MidSet(ref strLine5, Strings.Len(strLine5) - Strings.Len(strTemp), Strings.Len(strTemp), strTemp);
						}
						if (!boolUseDotmatrix)
							txtTax.Visible = true;
					}
					fecherFoundation.Strings.MidSet(ref strLine1, 1, 28, Strings.Mid(" ACCOUNT:" + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_Int32("rsaccount"))) + "-" + FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rscard"))), 1, 28));
					// strLine1 = strLine1 & " Land  " & Format(RSMster.fields("lastlandval") & "", "##,###,##0")
					// strLine2 = strLine2 & " BLDG  " & Format(RSMster.fields("lastbldgval") & "", "##,###,##0")
					// strLine3 = String(26, " ")
					fecherFoundation.Strings.MidSet(ref strLine3, 1, 28, Strings.Mid(" " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rslocstreet"))), 1, 28));
					// strLine3 = strLine3 & " Totl: " & Format((RSMster.fields("lastlandval") + RSMster.fields("lastbldgval")) & "", "#,###,###,##0")
					if (boolUseDotmatrix)
					{
						strTemp = Strings.StrDup(18, " ");
						fecherFoundation.Strings.MidSet(ref strTemp, 1, 5, "Land ");
						strTemp2 = Strings.Format(FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("lastlandval") + "")), "###,###,###");
						fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(strTemp2), Strings.Len(strTemp2), strTemp2);
						fecherFoundation.Strings.MidSet(ref strLine1, Strings.Len(strLine1) - Strings.Len(strTemp), Strings.Len(strTemp), strTemp);
						strTemp = Strings.StrDup(18, " ");
						strTemp2 = Strings.Format(FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("lastbldgval") + "")), "###,###,###");
						fecherFoundation.Strings.MidSet(ref strTemp, 1, 5, "Bldg ");
						fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(strTemp2), Strings.Len(strTemp2), strTemp2);
						fecherFoundation.Strings.MidSet(ref strLine2, Strings.Len(strLine2) - Strings.Len(strTemp), Strings.Len(strTemp), strTemp);
						strTemp = Strings.StrDup(18, " ");
						strTemp2 = Strings.Format(FCConvert.ToString(Conversion.Val(RSMster.Get_Fields_Int32("rlexemption") + "")), "###,###,###");
						fecherFoundation.Strings.MidSet(ref strTemp, 1, 5, "Exmt ");
						fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(strTemp2), Strings.Len(strTemp2), strTemp2);
						fecherFoundation.Strings.MidSet(ref strLine3, Strings.Len(strLine3) - Strings.Len(strTemp), Strings.Len(strTemp), strTemp);
						strTemp = Strings.StrDup(18, " ");
						strTemp2 = Strings.Format(lngTotal, "###,###,###");
						fecherFoundation.Strings.MidSet(ref strTemp, 1, 5, "Tot ");
						fecherFoundation.Strings.MidSet(ref strTemp, Strings.Len(strTemp) - Strings.Len(strTemp2), Strings.Len(strTemp2), strTemp2);
						fecherFoundation.Strings.MidSet(ref strLine4, Strings.Len(strLine4) - Strings.Len(strTemp), Strings.Len(strTemp), strTemp);
					}
					else
					{
						txtland2.Value = Conversion.Val(RSMster.Get_Fields_Int32("lastlandval"));
						txtBldg2.Value = Conversion.Val(RSMster.Get_Fields_Int32("lastbldgval"));
						txtExempt2.Value = Conversion.Val(RSMster.Get_Fields_Int32("rlexemption"));
						txtTotal2.Value = lngTotal;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Line " + Information.Erl() + " in MakeChangeAssessment", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

	

	
		private short ParseMapLot(string strMapLot, ref string strOut1, ref string strOut2, ref string strOut3, ref string strOut4)
		{
			short ParseMapLot = 0;
			int intPosition;
			int x;
			string[] strAry = null;
			// returns max length of a maplot division but a minimum of 4 chars
			intPosition = Strings.InStr(1, strMapLot, "-", CompareConstants.vbTextCompare);
			x = 1;
			if (intPosition > 0 && intPosition < 5)
			{
				strAry = Strings.Split(strMapLot, "-", -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) > 3)
				{
					strAry[3] += "-" + strAry[4];
					strAry[4] = "";
				}
				if (Information.UBound(strAry, 1) > 4)
				{
					strAry[3] += "-" + strAry[5];
					strAry[5] = "";
				}
				strOut1 = strAry[0];
				x = Strings.Trim(strOut1).Length;
				if (Information.UBound(strAry, 1) > 0)
				{
					strOut2 = strAry[1];
					if (Strings.Trim(strOut2).Length > x)
						x = Strings.Trim(strOut2).Length;
				}
				if (Information.UBound(strAry, 1) > 1)
				{
					strOut3 = strAry[2];
					if (Strings.Trim(strOut3).Length > x)
						x = Strings.Trim(strOut3).Length;
				}
				if (Information.UBound(strAry, 1) > 2)
				{
					strOut4 = strAry[3];
					if (Strings.Trim(strOut4).Length > x)
						x = Strings.Trim(strOut4).Length;
				}
				if (x < 4)
					x = 4;
				ParseMapLot = FCConvert.ToInt16(x);
				// Do While (Len(strMapLot) > 0)
				// If intPosition = 0 Then
				// intPosition = Len(strMapLot) + 1
				// End If
				// Select Case x
				// Case 1
				// strOut1 = Mid(strMapLot, 1, intPosition - 1)
				// Case 2
				// strOut2 = Mid(strMapLot, 1, intPosition - 1)
				// Case 3
				// strOut3 = Mid(strMapLot, 1, intPosition - 1)
				// Case 4
				// strOut4 = Mid(strMapLot, 1, intPosition - 1)
				// End Select
				// If intPosition > Len(strMapLot) Then Exit Do
				// strMapLot = Mid(strMapLot, intPosition + 1)
				// intPosition = InStr(1, strMapLot, "-")
				// 
				// x = x + 1
				// Loop
			}
			else
			{
				strOut1 = Strings.Mid(strMapLot, 1, 4);
				strOut2 = Strings.Mid(strMapLot, 5, 4);
				strOut3 = Strings.Mid(strMapLot, 9, 4);
				strOut4 = Strings.Mid(strMapLot, 13, 4);
				ParseMapLot = 4;
			}
			strOut1 = Strings.Trim(strOut1);
			strOut2 = Strings.Trim(strOut2);
			strOut3 = Strings.Trim(strOut3);
			strOut4 = Strings.Trim(strOut4);
			return ParseMapLot;
		}

		private void stripmiddlespaces(ref string strToStrip)
		{
			int x;
			int intTemp = 0;
			x = 1;
			while (x < strToStrip.Length)
			{
				intTemp = Strings.InStr(x, strToStrip, " ", CompareConstants.vbBinaryCompare);
				if (intTemp == 0)
					break;
				if (intTemp == strToStrip.Length)
					break;
				if (Strings.Mid(strToStrip, intTemp + 1, 1) == " ")
				{
					strToStrip = Strings.Mid(strToStrip, 1, intTemp - 1) + Strings.Mid(strToStrip, intTemp + 1);
					x = intTemp;
				}
				else
				{
					x += 1;
				}
			}
		}

		private void MakeF2Label()
		{
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
			string strTemp;
			string strRefBookPage;
			int intMaxMapLen;
			int intNumChars = 0;
			int intTemp = 0;
			// vbPorter upgrade warning: intCharLen As short --> As int	OnWrite(int, double)
			int intCharLen = 0;
			int intCurLine = 0;
			string[] strL = new string[6 + 1];
			// vbPorter upgrade warning: lngAct As int	OnRead(string)
			int lngAct;
			string strCountry;
			// commented out the ref2 parts.  Need to ask user if they want to sub ref2 for the address
			try
			{
				// On Error GoTo ErrorHandler
				strTemp = modRegistry.GetRegistryKey("REF2LabelCPI");
				if (Conversion.Val(strTemp) == 12)
				{
					intCPI = 12;
				}
				else if (Conversion.Val(strTemp) == 17)
				{
					intCPI = 17;
				}
				else
				{
					intCPI = 10;
				}
				strTemp = modRegistry.GetRegistryKey("REF2LabelType");
				if ((strTemp == "4014") || (strTemp == "4065"))
				{
					intCharLen = (intCPI * 4) - 2;
					strLeftBlank = " ";
				}
				else
				{
					intCharLen = FCConvert.ToInt32((intCPI * 3.5) - 2);
					strLeftBlank = "";
				}
				strLine1 = Strings.StrDup(intCharLen, " ");
				strLine2 = "";
				strLine3 = "";
				strLine4 = "";
				strLine5 = "";
				strLine6 = "";
				lngAct = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMster.Get_Fields_Int32("rsaccount"))));
				strRefBookPage = "";
				if (boolUseBookPage)
				{
					if (!(clsBookPage.EndOfFile() && clsBookPage.BeginningOfFile()))
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (clsBookPage.Get_Fields("account") == RSMster.Get_Fields_Int32("rsaccount"))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							while (clsBookPage.FindNextRecord("account", RSMster.Get_Fields_Int32("rsaccount")))
							{
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strRefBookPage += " B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							}
							// ElseIf clsBookPage.FindFirstRecord("account", RSMster.Fields("rsaccount")) Then
						}
						else if (clsBookPage.FindFirst("account = " + RSMster.Get_Fields_Int32("rsaccount")))
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strRefBookPage = "B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							while (clsBookPage.FindNextRecord("account", RSMster.Get_Fields_Int32("rsaccount")))
							{
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strRefBookPage += " B" + clsBookPage.Get_Fields("book") + "P" + clsBookPage.Get_Fields("Page");
							}
						}
					}
				}
				else
				{
					strRefBookPage = RSMster.Get_Fields_String("RSref1") + "";
				}
				strCountry = Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("country")));
				if (Strings.LCase(strCountry) == "united states")
				{
					strCountry = "";
				}
				// If boolDotMatrix Then
				// 1        strLine1 = String(intCharLen, " ")
				// 2        Mid(strLine1, 1) = strLeftBlank & RSMster.fields("rsname") & ""
				strLine1 = Strings.Left(strLeftBlank + RSMster.Get_Fields_String("rsname") + Strings.StrDup(intCharLen, " "), intCharLen);
				// 3        Mid(strLine1, 32, 6) = Format(RSMster.fields("rsaccount") & "", "@@@@@@")
				strTemp = " " + RSMster.Get_Fields_Int32("rsaccount");
				if (!modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					fecherFoundation.Strings.MidSet(ref strLine1, intCharLen - Strings.Len(strTemp) + 1, strTemp);
					if (strLine1.Length > intCharLen)
					{
						strLine1 = Strings.Mid(strLine1, 1, intCharLen);
					}
				}
				// Else
				// 4        Mid(strLine1, 1) = Left(" " & RSMster.fields("rsname") & "", 37)
				// 5        Mid(strLine1, 39, 6) = Format(RSMster.fields("rsaccount") & "", "@@@@@@")
				// End If
				if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rssecowner"))) != string.Empty)
				{
					strLine2 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rssecowner"))), 1, intCharLen);
					// If Trim(RSMster.fields("rsref2") & "") <> vbNullString Then
					// strLine3 = Mid(" " & Trim(RSMster.fields("rsref2")), 1, 38)
					// 
					// Else
					if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))) != string.Empty)
					{
						if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))) != string.Empty)
						{
							strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))), 1, intCharLen);
							strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen);
							strLine5 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen);
						}
						else
						{
							strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))), 1, intCharLen);
							strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen);
						}
					}
					else
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen);
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen);
					}
					// End If
				}
				else
				{
					// If Trim(RSMster.fields("rsref2") & "") <> vbNullString Then
					// strLine2 = Mid(" " & Trim(RSMster.fields("rsref2")), 1, 38)
					// If Trim(RSMster.fields("rsaddr1") & "") <> vbNullString Then
					// strLine3 = Mid(" " & Trim(RSMster.fields("rsaddr1")), 1, 38)
					// strLine4 = Mid(" " & Trim(RSMster.fields("rsaddr3") & "") & " " & Trim(RSMster.fields("rsstate") & "") & "  " & Trim(RSMster.fields("rszip") & "") & " " & Trim(RSMster.fields("rszip4") & ""), 1, 38)
					// Else
					// strLine3 = Mid(" " & Trim(RSMster.fields("rsaddr2") & ""), 1, 38)
					// strLine4 = Mid(" " & Trim(RSMster.fields("rsaddr3") & ""), 1, 38)
					// End If
					// Else
					strLine2 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr1"))), 1, intCharLen);
					if (Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))) != string.Empty)
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr2"))), 1, intCharLen);
						strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen);
					}
					else
					{
						strLine3 = Strings.Mid(strLeftBlank + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsaddr3"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsstate"))) + "  " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip"))) + " " + Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rszip4"))), 1, intCharLen);
					}
					// End If
				}
				if (strLine4 == string.Empty)
				{
					strLine4 = strLeftBlank + strCountry;
				}
				else if (strLine5 == string.Empty)
				{
					strLine5 = strLeftBlank + strCountry;
				}
				if (strLine4 == string.Empty)
				{
					strLine4 = Strings.Mid(strLeftBlank + Strings.Trim(strRefBookPage), 1, intCharLen);
					if (!modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						strLine5 = strLeftBlank + "MapLot: " + Strings.Mid(Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot"))), 1, intCharLen - 9);
					}
					else
					{
						stripmiddlespaces(ref strLine4);
					}
				}
				else if (strLine5 == string.Empty)
				{
					strLine5 = Strings.Mid(strLeftBlank + Strings.Trim(strRefBookPage), 1, intCharLen);
					if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						stripmiddlespaces(ref strLine5);
					}
					else
					{
						strLine6 = strLeftBlank + "MapLot: " + Strings.Mid(Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot"))), 1, intCharLen - 9);
					}
					/*? 23 */
				}
				else
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
					{
						strLine6 = strLeftBlank + "MapLot: " + Strings.Mid(Strings.Trim(FCConvert.ToString(RSMster.Get_Fields_String("rsmaplot"))), 1, intCharLen - 9);
					}
					else
					{
						strLine6 = Strings.Mid(strLeftBlank + Strings.Trim(strRefBookPage), 1, intCharLen);
					}
				}
				strL[1] = strLine1;
				strL[2] = strLine2;
				strL[3] = strLine3;
				strL[4] = strLine4;
				strL[5] = strLine5;
				strL[6] = strLine6;
				if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
				{
					strL[6] = "";
					strMLot1 = "";
					strMLot2 = "";
					strMLot3 = "";
					strMLot4 = "";
					intMaxMapLen = ParseMapLot(RSMster.Get_Fields_String("rsmaplot"), ref strMLot1, ref strMLot2, ref strMLot3, ref strMLot4);
					if (FCConvert.CBool(modRegistry.GetRegistryKey_3("RELimitMapLotToFour", FCConvert.ToString(false))))
					{
						if (strMLot1.Length > 4)
						{
							strMLot2 += Strings.Mid(strMLot1, 5);
							strMLot1 = Strings.Mid(strMLot1, 1, 4);
						}
						if (strMLot2.Length > 4)
						{
							strMLot3 += Strings.Mid(strMLot2, 5);
							strMLot2 = Strings.Mid(strMLot2, 1, 4);
						}
						if (strMLot3.Length > 4)
						{
							strMLot4 += Strings.Mid(strMLot3, 5);
							strMLot3 = Strings.Mid(strMLot3, 1, 4);
							if (strMLot4.Length > 4)
								intMaxMapLen = strMLot4.Length;
						}
					}
					intNumChars = intCharLen;
					// intTemp = intNumChars - intMaxMapLen
					// intTemp = 4
					// If Len(strMLot4) > 4 Then intTemp = Len(strMLot4)
					intTemp = Strings.Trim(strMLot1).Length;
					if (Strings.Trim(strMLot2).Length > intTemp)
					{
						intTemp = Strings.Trim(strMLot2).Length;
					}
					if (Strings.Trim(strMLot3).Length > intTemp)
					{
						intTemp = Strings.Trim(strMLot3).Length;
					}
					if (Strings.Trim(strMLot4).Length > intTemp)
					{
						intTemp = Strings.Trim(strMLot4).Length;
					}
					intTemp = intNumChars - intTemp;
					if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
					{
						intCurLine = 1;
					}
					else if (Strings.Trim(strMLot4) != string.Empty)
					{
						intCurLine = 2;
					}
					else
					{
						intCurLine = 3;
					}
					// 30            If Trim(strMLot4) <> vbNullString Then
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strL[intCurLine] + "", intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, intTemp, intMaxMapLen, strMLot1 + "");
					strL[intCurLine] = strTemp;
					intCurLine += 1;
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strL[intCurLine] + "", intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, intTemp, intMaxMapLen, strMLot2 + "");
					strL[intCurLine] = strTemp;
					intCurLine += 1;
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strL[intCurLine] + "", intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, intTemp, intMaxMapLen, strMLot3 + "");
					strL[intCurLine] = strTemp;
					intCurLine += 1;
					strTemp = Strings.StrDup(intNumChars, " ");
					fecherFoundation.Strings.MidSet(ref strTemp, 1, intTemp - 2, Strings.Left(strL[intCurLine] + "", intTemp - 2));
					fecherFoundation.Strings.MidSet(ref strTemp, intTemp, intMaxMapLen, strMLot4 + "");
					strL[intCurLine] = strTemp;
					// Else
					// 47                strTemp = String(intNumChars, " ")
					// 48                Mid(strTemp, 1, intTemp - 2) = Left(strLine3 & "", intTemp - 2)
					// 49                Mid(strTemp, intTemp, intMaxMapLen) = strMLot1 & ""
					// 50                strLine3 = strTemp
					// 51                strTemp = String(intNumChars, " ")
					// 52                Mid(strTemp, 1, intTemp - 2) = Left(strLine4 & "", intTemp - 2)
					// 53                Mid(strTemp, intTemp, intMaxMapLen) = strMLot2 & ""
					// 54                strLine4 = strTemp
					// 55                strTemp = String(intNumChars, " ")
					// 56                Mid(strTemp, 1, intTemp - 2) = Left(strLine5 & "", intTemp - 2)
					// 57                Mid(strTemp, intTemp, intMaxMapLen) = strMLot3 & ""
					// 58                strLine5 = strTemp
					// End If
				}
				if (modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom)
				{
					strTemp = FCConvert.ToString(lngAct);
					intTemp = strTemp.Length;
					strL[5] = Strings.Left(strL[5] + Strings.StrDup(intCharLen, " "), intCharLen);
					fecherFoundation.Strings.MidSet(ref strL[5], intCharLen - intTemp, " " + strTemp);
				}
				strLine1 = Strings.RTrim(strL[1]);
				strLine2 = Strings.RTrim(strL[2]);
				strLine3 = Strings.RTrim(strL[3]);
				strLine4 = Strings.RTrim(strL[4]);
				strLine5 = Strings.RTrim(strL[5]);
				strLine6 = Strings.RTrim(strL[6]);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeF2label");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngtempwidth;
			int lngtempheight;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			modGlobalRoutines.SetPrintProperties(this);
			if (boolShowIt)
			{
				// If boolMaxForms Then
				// Me.WindowState = vbMaximized
				// Else
				// lngtempwidth = MDIParent.Width - MDIParent.Grid.Width - 200
				// If lngtempwidth < 1 Then Exit Sub
				// lngtempheight = MDIParent.Grid.Height ' - 500
				// If lngtempheight < 1 Then Exit Sub
				// 
				// Me.Left = MDIParent.Grid.Left + MDIParent.Grid.Width + 50 ' from 100
				// Me.Top = MDIParent.Top + 600
				// 
				// Me.Width = MDIParent.Width - MDIParent.Grid.Width - 200  ' from 500
				// Me.Height = MDIParent.Grid.Height ' - 500
				// Me.Zoom = -1
				// End If
				// boolSaveMatrix = boolDotMatrix
				// boolDotMatrix = True
			}
			else
			{
				// boolSaveMatrix = boolDotMatrix
				// boolDotMatrix = True
				// Call PrintTheHardWay
				boolUseDotmatrix = true;
			}
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
		}

		
	}
}
