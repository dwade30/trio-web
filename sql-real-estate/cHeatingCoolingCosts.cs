﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cHeatingCoolingCost
	{
		//=========================================================
		private int lngRecordID;
		private int lngSectionID;
		private int lngHeatingCoolingID;
		private double dblMildClimateCost;
		private double dblModerateClimateCost;
		private double dblExtremeClimateCost;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int SectionID
		{
			set
			{
				lngSectionID = value;
				IsUpdated = true;
			}
			get
			{
				int SectionID = 0;
				SectionID = lngSectionID;
				return SectionID;
			}
		}

		public int HeatingCoolingID
		{
			set
			{
				lngHeatingCoolingID = value;
				IsUpdated = true;
			}
			get
			{
				int HeatingCoolingID = 0;
				HeatingCoolingID = lngHeatingCoolingID;
				return HeatingCoolingID;
			}
		}

		public double MildClimateCost
		{
			set
			{
				dblMildClimateCost = value;
				IsUpdated = true;
			}
			get
			{
				double MildClimateCost = 0;
				MildClimateCost = dblMildClimateCost;
				return MildClimateCost;
			}
		}

		public double ModerateClimateCost
		{
			set
			{
				dblModerateClimateCost = value;
				IsUpdated = true;
			}
			get
			{
				double ModerateClimateCost = 0;
				ModerateClimateCost = dblModerateClimateCost;
				return ModerateClimateCost;
			}
		}

		public double ExtremeClimateCost
		{
			set
			{
				dblExtremeClimateCost = value;
				IsUpdated = true;
			}
			get
			{
				double ExtremeClimateCost = 0;
				ExtremeClimateCost = dblExtremeClimateCost;
				return ExtremeClimateCost;
			}
		}
	}
}
