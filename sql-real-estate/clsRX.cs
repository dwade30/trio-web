﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	public class clsRX
	{
		//=========================================================
		private struct typAcctInfo
		{
			public int Account;
			public int CARD;
			public string Maplot;
			public string Name;
			public string SecOwner;
			public string PreviousOwner;
			public string Address1;
			public string Address2;
			public string City;
			public string State;
			public string Zip;
			public string Zip4;
			public string StreetNumber;
			public string StreetName;
			public string Phone;
			public string Email;
			public string BookPage;
			public string Ref1;
			public string Ref2;
			public int Land;
			public int Building;
			public int totalexemption;
			public string ExemptionType1;
			public int Exemption1;
			public string ExemptionType2;
			public int Exemption2;
			public string ExemptionType3;
			public int Exemption3;
			public int SalePrice;
			public string SaleDate;
			public string SaleType;
			public string SaleFinancing;
			public string SaleVerified;
			public string SaleValidity;
			public double TotalAcres;
			public double SoftAcres;
			public double MixedAcres;
			public double HardAcres;
			public double OtherAcres;
			public int SoftValue;
			public int MixedValue;
			public int HardValue;
			public int OtherValue;
			public int TreeGrowthYear;
			public bool TaxAcquired;
			public bool Bankruptcy;
			public string Neighborhood;
			public string Zone;
			public string Topography1;
			public string Topography2;
			public string Utilities1;
			public string Utilities2;
			public string StreetType;
			public string LastInspectionDate;
			public string Entrance;
			public string InspectionInfo;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public typAcctInfo(int unusedParam)
			{
				this.Account = 0;
				this.CARD = 0;
				this.Maplot = string.Empty;
				this.Name = string.Empty;
				this.SecOwner = string.Empty;
				this.PreviousOwner = string.Empty;
				this.Address1 = string.Empty;
				this.Address2 = string.Empty;
				this.City = string.Empty;
				this.State = string.Empty;
				this.Zip = string.Empty;
				this.Zip4 = string.Empty;
				this.StreetNumber = string.Empty;
				this.StreetName = string.Empty;
				this.Phone = string.Empty;
				this.Email = string.Empty;
				this.BookPage = string.Empty;
				this.Ref1 = string.Empty;
				this.Ref2 = string.Empty;
				this.Land = 0;
				this.Building = 0;
				this.totalexemption = 0;
				this.ExemptionType1 = string.Empty;
				this.Exemption1 = 0;
				this.ExemptionType2 = string.Empty;
				this.Exemption2 = 0;
				this.ExemptionType3 = string.Empty;
				this.Exemption3 = 0;
				this.SalePrice = 0;
				this.SaleDate = string.Empty;
				this.SaleType = string.Empty;
				this.SaleFinancing = string.Empty;
				this.SaleVerified = string.Empty;
				this.SaleValidity = string.Empty;
				this.TotalAcres = 0;
				this.SoftAcres = 0;
				this.MixedAcres = 0;
				this.HardAcres = 0;
				this.OtherAcres = 0;
				this.SoftValue = 0;
				this.MixedValue = 0;
				this.HardValue = 0;
				this.OtherValue = 0;
				this.TreeGrowthYear = 0;
				this.TaxAcquired = false;
				this.Bankruptcy = false;
				this.Neighborhood = string.Empty;
				this.Zone = string.Empty;
				this.Topography1 = string.Empty;
				this.Topography2 = string.Empty;
				this.Utilities1 = string.Empty;
				this.Utilities2 = string.Empty;
				this.StreetType = string.Empty;
				this.LastInspectionDate = string.Empty;
				this.Entrance = string.Empty;
				this.InspectionInfo = string.Empty;
			}
		};

		private struct typLandData
		{
			public string LandType1;
			public string Units1;
			public double InfluencePercentage1;
			public string InfluenceCode1;
			public string LandType2;
			public string Units2;
			public double InfluencePercentage2;
			public string InfluenceCode2;
			public string LandType3;
			public string Units3;
			public double InfluencePercentage3;
			public string InfluenceCode3;
			public string LandType4;
			public string Units4;
			public double InfluencePercentage4;
			public string InfluenceCode4;
			public string LandType5;
			public string Units5;
			public double InfluencePercentage5;
			public string InfluenceCode5;
			public string LandType6;
			public string Units6;
			public double InfluencePercentage6;
			public string InfluenceCode6;
			public string LandType7;
			public string Units7;
			public double InfluencePercentage7;
			public string InfluenceCode7;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public typLandData(int unusedParam)
			{
				this.LandType1 = string.Empty;
				this.Units1 = string.Empty;
				this.InfluencePercentage1 = 0;
				this.InfluenceCode1 = string.Empty;
				this.LandType2 = string.Empty;
				this.Units2 = string.Empty;
				this.InfluencePercentage2 = 0;
				this.InfluenceCode2 = string.Empty;
				this.LandType3 = string.Empty;
				this.Units3 = string.Empty;
				this.InfluencePercentage3 = 0;
				this.InfluenceCode3 = string.Empty;
				this.LandType4 = string.Empty;
				this.Units4 = string.Empty;
				this.InfluencePercentage4 = 0;
				this.InfluenceCode4 = string.Empty;
				this.LandType5 = string.Empty;
				this.Units5 = string.Empty;
				this.InfluencePercentage5 = 0;
				this.InfluenceCode5 = string.Empty;
				this.LandType6 = string.Empty;
				this.Units6 = string.Empty;
				this.InfluencePercentage6 = 0;
				this.InfluenceCode6 = string.Empty;
				this.LandType7 = string.Empty;
				this.Units7 = string.Empty;
				this.InfluencePercentage7 = 0;
				this.InfluenceCode7 = string.Empty;
			}
		};

		private struct typDwellingData
		{
			public string BuildingType;
			public short DwellingUnits;
			public short OtherUnits;
			public double Stories;
			public string ExteriorWalls;
			public string RoofSurface;
			public int SqftMasonryTrim;
			public short YearBuilt;
			public short YearRemodeled;
			public string FoundationType;
			public string BasementType;
			public short BasementCars;
			public string WetBasement;
			public int SqftBasementLivingArea;
			public string FinishedBasementGrade;
			public double FinishedBasementFactor;
			public string HeatType;
			public short PercentageHeated;
			public string CoolType;
			public short PercentageCooled;
			public string KitchenStyle;
			public string BathStyle;
			public short NumberRooms;
			public short NumberBedRooms;
			public short NumberBaths;
			public short NumberHalfBaths;
			public short AdditionalFixtures;
			public short NumberFireplaces;
			public string Layout;
			public string Attic;
			public int SFLA;
			public short PercentUnfinished;
			public string Grade;
			public double Factor;
			public int Sqft;
			public string Condition;
			public short PhysicalGood;
			public short FunctionalGood;
			public string FunctionalCode;
			public short EconomicGood;
			// vbPorter upgrade warning: EconomicCode As string	OnWrite(string, short)
			public string EconomicCode;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public typDwellingData(int unusedParam)
			{
				this.BuildingType = string.Empty;
				this.DwellingUnits = 0;
				this.OtherUnits = 0;
				this.Stories = 0;
				this.ExteriorWalls = string.Empty;
				this.RoofSurface = string.Empty;
				this.SqftMasonryTrim = 0;
				this.YearBuilt = 0;
				this.YearRemodeled = 0;
				this.FoundationType = string.Empty;
				this.BasementType = string.Empty;
				this.BasementCars = 0;
				this.WetBasement = string.Empty;
				this.SqftBasementLivingArea = 0;
				this.FinishedBasementGrade = string.Empty;
				this.FinishedBasementFactor = 0;
				this.HeatType = string.Empty;
				this.PercentageHeated = 0;
				this.CoolType = string.Empty;
				this.PercentageCooled = 0;
				this.KitchenStyle = string.Empty;
				this.BathStyle = string.Empty;
				this.NumberRooms = 0;
				this.NumberBedRooms = 0;
				this.NumberBaths = 0;
				this.NumberHalfBaths = 0;
				this.AdditionalFixtures = 0;
				this.NumberFireplaces = 0;
				this.Layout = string.Empty;
				this.Attic = string.Empty;
				this.SFLA = 0;
				this.PercentUnfinished = 0;
				this.Grade = string.Empty;
				this.Factor = 0;
				this.Sqft = 0;
				this.Condition = string.Empty;
				this.PhysicalGood = 0;
				this.FunctionalGood = 0;
				this.FunctionalCode = string.Empty;
				this.EconomicGood = 0;
				this.EconomicCode = string.Empty;
			}
		};

		private struct typCommercial
		{
			public double EconomicFactor;
			public string BuildingType1;
			public short NumberDwellingUnits1;
			public string BuildingClassQuality1;
			public double GradeFactor1;
			public string ExteriorWalls1;
			public double Stories1;
			public short StoriesHeight1;
			public string HeatingCoolingType1;
			public short YearBuilt1;
			public short YearRemodeled1;
			public string Condition1;
			public short PhysicalGood1;
			public short FunctionalGood1;
			public string BuildingType2;
			public short NumberDwellingUnits2;
			public string BuildingClassQuality2;
			public double GradeFactor2;
			public string ExteriorWalls2;
			public double Stories2;
			public short StoriesHeight2;
			public string HeatingCoolingType2;
			public short YearBuilt2;
			public short YearRemodeled2;
			public string Condition2;
			public short PhysicalGood2;
			public short FunctionalGood2;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public typCommercial(int unusedParam)
			{
				this.EconomicFactor = 0;
				this.BuildingType1 = string.Empty;
				this.NumberDwellingUnits1 = 0;
				this.BuildingClassQuality1 = string.Empty;
				this.GradeFactor1 = 0;
				this.ExteriorWalls1 = string.Empty;
				this.Stories1 = 0;
				this.StoriesHeight1 = 0;
				this.HeatingCoolingType1 = string.Empty;
				this.YearBuilt1 = 0;
				this.YearRemodeled1 = 0;
				this.Condition1 = string.Empty;
				this.PhysicalGood1 = 0;
				this.FunctionalGood1 = 0;
				this.BuildingType2 = string.Empty;
				this.NumberDwellingUnits2 = 0;
				this.BuildingClassQuality2 = string.Empty;
				this.GradeFactor2 = 0;
				this.ExteriorWalls2 = string.Empty;
				this.Stories2 = 0;
				this.StoriesHeight2 = 0;
				this.HeatingCoolingType2 = string.Empty;
				this.YearBuilt2 = 0;
				this.YearRemodeled2 = 0;
				this.Condition2 = string.Empty;
				this.PhysicalGood2 = 0;
				this.FunctionalGood2 = 0;
			}
		};

		private struct typOutbuilding
		{
			public string BuildingType;
			// vbPorter upgrade warning: YearBuilt As short	OnWrite(short, string)
			public short YearBuilt;
			public string Units;
			public string Grade;
			public double GradeFactor;
			public string Condition;
			public short PhysicalPercent;
			public short FunctionPercent;
			public string SoundValue;
			// boolean
			public int SoundValueAmount;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public typOutbuilding(int unusedParm)
			{
				this.BuildingType = string.Empty;
				this.YearBuilt = 0;
				this.Units = string.Empty;
				this.Grade = string.Empty;
				this.GradeFactor = 0;
				this.Condition = string.Empty;
				this.PhysicalPercent = 0;
				this.FunctionPercent = 0;
				this.SoundValue = string.Empty;
				this.SoundValueAmount = 0;
			}
		};

		private string strSavePath = string.Empty;
		private clsDRWrapper rsMain = new clsDRWrapper();
		private clsDRWrapper rsInfluence = new clsDRWrapper();
		private bool boolShowSuccessMessage;
		private bool boolFileCreated;
		// vbPorter upgrade warning: intTownCode As short	OnWriteFCConvert.ToInt32(
		public bool Init(string strDestPath, bool modalDialog, bool boolShowMessage = true, int intTownCode = 0)
		{
			//FileSystemObject fso = new FileSystemObject();
			bool Init = false;
			try
			{
				// On Error GoTo ErrorHandler
				Init = false;
				boolFileCreated = false;
				boolShowSuccessMessage = boolShowMessage;
				if (Strings.UCase(Strings.Right("    " + strDestPath, 3)) == "VB1")
				{
					MessageBox.Show("Cannot use vb1 files", "Bad File Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return Init;
				}
				strSavePath = strDestPath;
				if (File.Exists(strSavePath))
				{
					File.Delete(strSavePath);
				}
				modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = intTownCode;
				modGlobalVariables.Statics.LandTypes.Init();
				modGlobalVariables.Statics.clsCostRecords.InitCostFiles();
				rsInfluence.OpenRecordset("select * from costrecord where crecordnumber between 1441 and 1449 order by crecordnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown || intTownCode == 0)
				{
					rsMain.OpenRecordset("SELECT * from master where rscard = 1 and not rsdeleted = 1 order by rsaccount", modGlobalVariables.strREDatabase);
				}
				else
				{
					rsMain.OpenRecordset("select * from master where rscard = 1 and ritrancode = " + FCConvert.ToString(intTownCode) + " and not rsdeleted = 1 order by rsaccount", modGlobalVariables.strREDatabase);
				}
				CreateExtract(modalDialog);
				Init = boolFileCreated;
				return Init;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Init;
		}

		private string FillMasterRec()
		{
			string FillMasterRec = "";
			string strReturn;
			int lngAcct;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			typAcctInfo MInfo = new typAcctInfo(0);
			string strTemp = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				FillMasterRec = "";
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("rsaccount"))));
				MInfo.Account = lngAcct;
				MInfo.CARD = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("rscard"))));
				MInfo.Maplot = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rsmaplot"))), 20, false);
				MInfo.Name = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rsname"))), 50, false);
				MInfo.SecOwner = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rssecowner"))), 50, false);
				MInfo.PreviousOwner = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rspreviousmaster"))), 50, false);
				MInfo.Address1 = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rsaddr1"))), 50, false);
				MInfo.Address2 = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rsaddr2"))), 50, false);
				MInfo.City = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rsaddr3"))), 50, false);
				MInfo.State = Strings.Right("  " + Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rsstate"))), 2);
				MInfo.Zip = Strings.Right("     " + rsMain.Get_Fields_String("rszip"), 5);
				MInfo.Zip4 = Strings.Right("    " + rsMain.Get_Fields_String("rszip4"), 4);
				MInfo.StreetNumber = modGlobalFunctions.PadStringWithSpaces(rsMain.Get_Fields_String("rslocnumalph") + Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("rslocapt"))), 6, false);
				MInfo.StreetName = modGlobalFunctions.PadStringWithSpaces(rsMain.Get_Fields_String("rslocstreet"), 50, false);
				rsTemp.OpenRecordset("select * from phonenumbers where phonecode = " + FCConvert.ToString(modREConstants.CNSTPHONETYPEMASTER) + " and parentid = " + FCConvert.ToString(lngAcct) + " order by id", modGlobalVariables.strREDatabase);
				if (!rsTemp.EndOfFile())
				{
					strTemp = Strings.Right("0000000000" + rsTemp.Get_Fields_String("phonenumber"), 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
					MInfo.Phone = strTemp;
				}
				else
				{
					MInfo.Phone = "(000)000-0000";
				}
				MInfo.Email = modGlobalFunctions.PadStringWithSpaces(rsMain.Get_Fields_String("email"), 40);
				strTemp = modGlobalFunctions.GetCurrentBookPageString(lngAcct, 3);
				strTemp = modGlobalFunctions.PadStringWithSpaces(strTemp, 30, false);
				MInfo.BookPage = strTemp;
				MInfo.Ref1 = modGlobalFunctions.PadStringWithSpaces(rsMain.Get_Fields_String("rsref1"), 50, false);
				MInfo.Ref2 = modGlobalFunctions.PadStringWithSpaces(rsMain.Get_Fields_String("rsref2"), 50, false);
				rsTemp.OpenRecordset("select sum(piacres) as totacres,sum(rssoft) as softacres,sum(rssoftvalue) as softvalue,sum(rsmixed) as mixedacres,sum(rsmixedvalUE) as mixedvalue,sum(rshard) as hardacres,sum(rshardvalUE) as hardvalue,sum(rsother) as otheracres,sum(rsothervalue) as othervalue,sum(rlexemption) as exemptsum,sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum from master where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				MInfo.Land = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("landsum"))));
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				MInfo.Building = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("bldgsum"))));
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				MInfo.totalexemption = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("exemptsum"))));
				// TODO Get_Fields: Field [totacres] not found!! (maybe it is an alias?)
				MInfo.TotalAcres = Conversion.Val(rsTemp.Get_Fields("totacres"));
				// TODO Get_Fields: Field [softacres] not found!! (maybe it is an alias?)
				MInfo.SoftAcres = Conversion.Val(rsTemp.Get_Fields("softacres"));
				// TODO Get_Fields: Field [mixedacres] not found!! (maybe it is an alias?)
				MInfo.MixedAcres = Conversion.Val(rsTemp.Get_Fields("mixedacres"));
				// TODO Get_Fields: Field [Hardacres] not found!! (maybe it is an alias?)
				MInfo.HardAcres = Conversion.Val(rsTemp.Get_Fields("Hardacres"));
				// TODO Get_Fields: Field [Otheracres] not found!! (maybe it is an alias?)
				MInfo.OtherAcres = Conversion.Val(rsTemp.Get_Fields("Otheracres"));
				// TODO Get_Fields: Field [softvalue] not found!! (maybe it is an alias?)
				MInfo.SoftValue = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("softvalue"))));
				// TODO Get_Fields: Field [Mixedvalue] not found!! (maybe it is an alias?)
				MInfo.MixedValue = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("Mixedvalue"))));
				// TODO Get_Fields: Field [hardvalue] not found!! (maybe it is an alias?)
				MInfo.HardValue = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("hardvalue"))));
				// TODO Get_Fields: Field [othervalue] not found!! (maybe it is an alias?)
				MInfo.OtherValue = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("othervalue"))));
				rsTemp.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.Statics.strRECostFileDatabase);
				if (Conversion.Val(rsMain.Get_Fields_Int32("riexemptcd1")) > 0)
				{
					// If rsTemp.FindFirstRecord("Code", Val(rsMain.Fields("riexemptcd1"))) Then
					if (rsTemp.FindFirst("code = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("riexemptcd1")))))
					{
						MInfo.ExemptionType1 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("description"), 30, false);
					}
					else
					{
						MInfo.ExemptionType1 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.ExemptionType1 = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("riexemptcd2")) > 0)
				{
					// If rsTemp.FindFirstRecord("Code", Val(rsMain.Fields("riexemptcd2"))) Then
					if (rsTemp.FindFirst("code = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("riexemptcd2")))))
					{
						MInfo.ExemptionType2 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("description"), 30, false);
					}
					else
					{
						MInfo.ExemptionType2 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.ExemptionType2 = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("riexemptcd3")) > 0)
				{
					// If rsTemp.FindFirstRecord("Code", Val(rsMain.Fields("riexemptcd3"))) Then
					if (rsTemp.FindFirst("code = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("riexemptcd3")))))
					{
						MInfo.ExemptionType3 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("description"), 30, false);
					}
					else
					{
						MInfo.ExemptionType3 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.ExemptionType3 = Strings.StrDup(30, " ");
				}
				MInfo.Exemption1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("exemptval1"))));
				MInfo.Exemption2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("exemptval2"))));
				MInfo.Exemption3 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("exemptval3"))));
				MInfo.SalePrice = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("pisaleprice"))));
				if (Information.IsDate(rsMain.Get_Fields("saledate")))
				{
					if (rsMain.Get_Fields_DateTime("saledate").ToOADate() != 0)
					{
						MInfo.SaleDate = Strings.Format(rsMain.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
					}
					else
					{
						MInfo.SaleDate = "00/00/0000";
					}
				}
				else
				{
					MInfo.SaleDate = "00/00/0000";
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1350 and crecordnumber < 1390 order by crecordnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1350 and crecordnumber < 1390 and townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " order by crecordnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pisaletype")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", Val(rsMain.Fields("pisaletype")) + 1350) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("pisaletype")) + 1350)))
					{
						MInfo.SaleType = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.SaleType = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.SaleType = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pisalefinancing")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", Val(rsMain.Fields("pisalefinancing")) + 1360) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("pisalefinancing")) + 1360)))
					{
						MInfo.SaleFinancing = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.SaleFinancing = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.SaleFinancing = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pisaleverified")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", Val(rsMain.Fields("pisaleverified")) + 1370) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("pisaleverified")) + 1370)))
					{
						MInfo.SaleVerified = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.SaleVerified = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.SaleVerified = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pisalevalidity")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", Val(rsMain.Fields("pisalevalidity")) + 1380) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("pisalevalidity")) + 1380)))
					{
						MInfo.SaleValidity = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.SaleValidity = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.SaleValidity = Strings.StrDup(30, " ");
				}
				MInfo.TreeGrowthYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int32("pistreetCODE"))));
				MInfo.TaxAcquired = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("taxacquired"));
				MInfo.Bankruptcy = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("inbankruptcy"));
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					rsTemp.OpenRecordset("select * from neighborhood order by code", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("select * from neighborhood where  townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pineighborhood")) > 0)
				{
					// If rsTemp.FindFirstRecord("code", Val(rsMain.Fields("pineighborhood"))) Then
					if (rsTemp.FindFirst("code = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("pineighborhood")))))
					{
						MInfo.Neighborhood = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("description"), 30, false);
					}
					else
					{
						MInfo.Neighborhood = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.Neighborhood = Strings.StrDup(30, " ");
				}
				// Call rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1010 and crecordnumber < 1110", strRECostFileDatabase)
				if (Conversion.Val(rsMain.Get_Fields_Int32("pizone")) > 0)
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsMain.Get_Fields_Int32("pizone"))), "Property", "Zone");
					}
					else
					{
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsMain.Get_Fields_Int32("pizone"))), "Property", "Zone", FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("ritrancode"))));
					}
					MInfo.Zone = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					// If rsTemp.FindFirstRecord("crecordnumber", 1000 + Val(rsMain.Fields("pizone"))) Then
					// MInfo.Zone = PadStringWithSpaces(rsTemp.Fields("cldesc"), 30, False)
					// Else
					// MInfo.Zone = String(30, " ")
					// End If
				}
				else
				{
					MInfo.Zone = Strings.StrDup(30, " ");
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1300 and crecordnumber < 1310", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1300 and crecordnumber < 1310 and townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pitopography1")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", 1300 + Val(rsMain.Fields("pitopography1"))) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(1300 + Conversion.Val(rsMain.Get_Fields_Int32("pitopography1")))))
					{
						MInfo.Topography1 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.Topography1 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.Topography1 = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pitopography2")) > 0)
				{
					// If rsTemp.FindFirstRecord("Crecordnumber", 1300 + Val(rsMain.Fields("pitopography2"))) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(1300 + Conversion.Val(rsMain.Get_Fields_Int32("pitopography2")))))
					{
						MInfo.Topography2 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.Topography2 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.Topography2 = Strings.StrDup(30, " ");
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1310 and crecordnumber < 1320", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber > 1310 and crecordnumber < 1320 and townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piutilities1")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", 1310 + Val(rsMain.Fields("piutilities1"))) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(1310 + Conversion.Val(rsMain.Get_Fields_Int32("piutilities1")))))
					{
						MInfo.Utilities1 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.Utilities1 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.Utilities1 = Strings.StrDup(30, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piutilities2")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", 1310 + Val(rsMain.Fields("piutilities2"))) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(1310 + Conversion.Val(rsMain.Get_Fields_Int32("piutilities2")))))
					{
						MInfo.Utilities2 = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.Utilities2 = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.Utilities2 = Strings.StrDup(30, " ");
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber >1320 and crecordnumber < 1330", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber >1320 and crecordnumber < 1330 AND townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("pistreet")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", 1320 + Val(rsMain.Fields("pistreet"))) Then
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(1320 + Conversion.Val(rsMain.Get_Fields_Int32("pistreet")))))
					{
						MInfo.StreetType = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.StreetType = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.StreetType = Strings.StrDup(30, " ");
				}
				if (Information.IsDate(rsMain.Get_Fields("dateinspected")))
				{
					if (rsMain.Get_Fields_DateTime("dateinspected").ToOADate() != 0)
					{
						MInfo.LastInspectionDate = Strings.Format(rsMain.Get_Fields_DateTime("dateinspected"), "MM/dd/yyyy");
					}
					else
					{
						MInfo.LastInspectionDate = "00/00/0000";
					}
				}
				else
				{
					MInfo.LastInspectionDate = "00/00/0000";
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber >1700 and crecordnumber < 1720", modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("select * from costrecord where crecordnumber >1700 and crecordnumber < 1720 and townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				// TODO Get_Fields: Check the table for the column [Entrancecode] and replace with corresponding Get_Field method
				if (Conversion.Val(rsMain.Get_Fields("Entrancecode")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", Val(rsMain.Fields("entrancecode")) + 1700) Then
					// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields("entrancecode")) + 1700)))
					{
						MInfo.Entrance = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.Entrance = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.Entrance = Strings.StrDup(30, " ");
				}
				// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
				if (Conversion.Val(rsMain.Get_Fields("informationcode")) > 0)
				{
					// If rsTemp.FindFirstRecord("crecordnumber", Val(rsMain.Fields("informationcode")) + 1710) Then
					// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
					if (rsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields("informationcode")) + 1710)))
					{
						MInfo.InspectionInfo = modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("cldesc"), 30, false);
					}
					else
					{
						MInfo.InspectionInfo = Strings.StrDup(30, " ");
					}
				}
				else
				{
					MInfo.InspectionInfo = Strings.StrDup(30, " ");
				}
				strReturn = "";
				strReturn = Strings.Format(MInfo.Account, "000000");
				strReturn += "01";
				// for now just do one record for each account
				strReturn += MInfo.Maplot;
				strReturn += MInfo.Name;
				strReturn += MInfo.SecOwner;
				strReturn += MInfo.PreviousOwner;
				strReturn += MInfo.Address1;
				strReturn += MInfo.Address2;
				strReturn += MInfo.City;
				strReturn += MInfo.State;
				strReturn += MInfo.Zip;
				strReturn += MInfo.Zip4;
				strReturn += MInfo.StreetNumber;
				strReturn += MInfo.StreetName;
				strReturn += MInfo.Phone;
				strReturn += MInfo.Email;
				strReturn += MInfo.BookPage;
				strReturn += MInfo.Ref1;
				strReturn += MInfo.Ref2;
				strReturn += Strings.Format(MInfo.Land, "00000000000");
				// 11
				strReturn += Strings.Format(MInfo.Building, "00000000000");
				strReturn += Strings.Format(MInfo.totalexemption, "00000000000");
				strReturn += MInfo.ExemptionType1;
				strReturn += Strings.Format(MInfo.Exemption1, "00000000000");
				strReturn += MInfo.ExemptionType2;
				strReturn += Strings.Format(MInfo.Exemption2, "00000000000");
				strReturn += MInfo.ExemptionType3;
				strReturn += Strings.Format(MInfo.Exemption3, "00000000000");
				strReturn += Strings.Format(MInfo.SalePrice, "00000000000");
				// 11
				strReturn += MInfo.SaleDate;
				strReturn += MInfo.SaleType;
				strReturn += MInfo.SaleFinancing;
				strReturn += MInfo.SaleVerified;
				strReturn += MInfo.SaleValidity;
				strReturn += Strings.Format(MInfo.TotalAcres, "00000000.00");
				// 11
				strReturn += Strings.Format(MInfo.SoftAcres, "00000000.00");
				strReturn += Strings.Format(MInfo.MixedAcres, "00000000.00");
				strReturn += Strings.Format(MInfo.HardAcres, "00000000.00");
				strReturn += Strings.Format(MInfo.OtherAcres, "00000000.00");
				strReturn += Strings.Format(MInfo.SoftValue, "00000000000");
				strReturn += Strings.Format(MInfo.MixedValue, "00000000000");
				strReturn += Strings.Format(MInfo.HardValue, "00000000000");
				strReturn += Strings.Format(MInfo.OtherValue, "00000000000");
				strReturn += Strings.Format(MInfo.TreeGrowthYear, "0000");
				if (MInfo.TaxAcquired)
				{
					strReturn += "YES";
				}
				else
				{
					strReturn += "NO ";
				}
				if (MInfo.Bankruptcy)
				{
					strReturn += "YES";
				}
				else
				{
					strReturn += "NO ";
				}
				strReturn += MInfo.Neighborhood;
				strReturn += MInfo.Zone;
				strReturn += MInfo.Topography1;
				strReturn += MInfo.Topography2;
				strReturn += MInfo.Utilities1;
				strReturn += MInfo.Utilities2;
				strReturn += MInfo.StreetType;
				strReturn += MInfo.LastInspectionDate;
				strReturn += MInfo.Entrance;
				strReturn += MInfo.InspectionInfo;
				FillMasterRec = strReturn;
				return FillMasterRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillMasterRec", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillMasterRec;
		}

		private string FillLandRec()
		{
			string FillLandRec = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strReturn;
				int lngAcct;
				//FC:FINAL:RPU - Use custom constructor to initialize fields
				typLandData LInfo = new typLandData(0);
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				string[] strAry = null;
				strReturn = "";
				FillLandRec = "";
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland1type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland1type")))))
					{
						LInfo.LandType1 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage1 = Conversion.Val(rsMain.Get_Fields_Double("piland1inf"));
						if (LInfo.InfluencePercentage1 > 999.99)
						{
							LInfo.InfluencePercentage1 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units1 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units1 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland1unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland1unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units1 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units1 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units1 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units1 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland1unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland1infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland1infcode")))))
						{
							LInfo.InfluenceCode1 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode1 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType1 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode1 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage1 = 0;
						LInfo.Units1 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType1 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode1 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage1 = 0;
					LInfo.Units1 = Strings.StrDup(9, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland2type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland2type")))))
					{
						LInfo.LandType2 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage2 = Conversion.Val(rsMain.Get_Fields_Double("piland2inf"));
						if (LInfo.InfluencePercentage2 > 999.99)
						{
							LInfo.InfluencePercentage2 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units2 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units2 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland2unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland2unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units2 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units2 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units2 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units2 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland2unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland2infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland2infcode")))))
						{
							LInfo.InfluenceCode2 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode2 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType2 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode2 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage2 = 0;
						LInfo.Units2 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType2 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode2 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage2 = 0;
					LInfo.Units2 = Strings.StrDup(9, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland3type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland3type")))))
					{
						LInfo.LandType3 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage3 = Conversion.Val(rsMain.Get_Fields_Double("piland3inf"));
						if (LInfo.InfluencePercentage3 > 999.99)
						{
							LInfo.InfluencePercentage3 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units3 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units3 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland3unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland3unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units3 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units3 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units3 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units3 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland3unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland3infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland3infcode")))))
						{
							LInfo.InfluenceCode3 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode3 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType3 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode3 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage3 = 0;
						LInfo.Units3 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType3 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode3 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage3 = 0;
					LInfo.Units3 = Strings.StrDup(9, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland4type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland4type")))))
					{
						LInfo.LandType4 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage4 = Conversion.Val(rsMain.Get_Fields_Double("piland4inf"));
						if (LInfo.InfluencePercentage4 > 999.99)
						{
							LInfo.InfluencePercentage4 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units4 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units4 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland4unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland4unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units4 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units4 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units4 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units4 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland4unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland4infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland4infcode")))))
						{
							LInfo.InfluenceCode4 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode4 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType4 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode4 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage4 = 0;
						LInfo.Units4 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType4 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode4 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage4 = 0;
					LInfo.Units4 = Strings.StrDup(9, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland5type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland5type")))))
					{
						LInfo.LandType5 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage5 = Conversion.Val(rsMain.Get_Fields_Double("piland5inf"));
						if (LInfo.InfluencePercentage5 > 999.99)
						{
							LInfo.InfluencePercentage5 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units5 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units5 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland5unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland5unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units5 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units5 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units5 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units5 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland5unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland5infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland5infcode")))))
						{
							LInfo.InfluenceCode5 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode5 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType5 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode5 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage5 = 0;
						LInfo.Units5 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType5 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode5 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage5 = 0;
					LInfo.Units5 = Strings.StrDup(9, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland6type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland6type")))))
					{
						LInfo.LandType6 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage6 = Conversion.Val(rsMain.Get_Fields_Double("piland6inf"));
						if (LInfo.InfluencePercentage6 > 999.99)
						{
							LInfo.InfluencePercentage6 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units6 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units6 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland6unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland6unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units6 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units6 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units6 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units6 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland6unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland6infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland6infcode")))))
						{
							LInfo.InfluenceCode6 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode6 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType6 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode6 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage6 = 0;
						LInfo.Units6 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType6 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode6 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage6 = 0;
					LInfo.Units6 = Strings.StrDup(9, " ");
				}
				if (Conversion.Val(rsMain.Get_Fields_Int32("piland7type")) > 0)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(rsMain.Get_Fields_Int32("piland7type")))))
					{
						LInfo.LandType7 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.LandTypes.Description, 30, false);
						LInfo.InfluencePercentage7 = Conversion.Val(rsMain.Get_Fields_Double("piland7inf"));
						if (LInfo.InfluencePercentage7 > 999.99)
						{
							LInfo.InfluencePercentage7 = 0;
						}
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
							case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
								{
									LInfo.Units7 = Strings.StrDup(9, "0");
									break;
								}
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									LInfo.Units7 = Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland7unitsa")), "0000") + "x" + Strings.Format(Conversion.Val(rsMain.Get_Fields_String("piland7unitsb")), "0000");
									break;
								}
							case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							case modREConstants.CNSTLANDTYPESITE:
							case modREConstants.CNSTLANDTYPELINEARFEET:
							case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
								{
									LInfo.Units7 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsb"))), "000"))), "000000.00");
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									LInfo.Units7 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsb"))), "000"))), "000000000");
									break;
								}
							case modREConstants.CNSTLANDTYPEACRES:
							case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
								{
									LInfo.Units7 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsb"))), "000"))), "000000.00");
									break;
								}
							default:
								{
									LInfo.Units7 = Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_String("piland7unitsb"))), "000"))), "000000.00");
									break;
								}
						}
						//end switch
						// If rsInfluence.FindFirstRecord("crecordnumber", Val(rsMain.Fields("piland7infcode"))) Then
						if (rsInfluence.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(rsMain.Get_Fields_Int32("piland7infcode")))))
						{
							LInfo.InfluenceCode7 = modGlobalFunctions.PadStringWithSpaces(rsInfluence.Get_Fields_String("cldesc"), 30, false);
						}
						else
						{
							LInfo.InfluenceCode7 = Strings.StrDup(30, " ");
						}
					}
					else
					{
						LInfo.LandType7 = Strings.StrDup(30, " ");
						LInfo.InfluenceCode7 = Strings.StrDup(30, " ");
						LInfo.InfluencePercentage7 = 0;
						LInfo.Units7 = Strings.StrDup(9, " ");
					}
				}
				else
				{
					LInfo.LandType7 = Strings.StrDup(30, " ");
					LInfo.InfluenceCode7 = Strings.StrDup(30, " ");
					LInfo.InfluencePercentage7 = 0;
					LInfo.Units7 = Strings.StrDup(9, " ");
				}
				strReturn = "";
				strReturn += LInfo.LandType1;
				strReturn += LInfo.Units1;
				strReturn += Strings.Format(LInfo.InfluencePercentage1, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode1;
				strReturn += LInfo.LandType2;
				strReturn += LInfo.Units2;
				strReturn += Strings.Format(LInfo.InfluencePercentage2, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode2;
				strReturn += LInfo.LandType3;
				strReturn += LInfo.Units3;
				strReturn += Strings.Format(LInfo.InfluencePercentage3, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode3;
				strReturn += LInfo.LandType4;
				strReturn += LInfo.Units4;
				strReturn += Strings.Format(LInfo.InfluencePercentage4, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode4;
				strReturn += LInfo.LandType5;
				strReturn += LInfo.Units5;
				strReturn += Strings.Format(LInfo.InfluencePercentage5, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode5;
				strReturn += LInfo.LandType6;
				strReturn += LInfo.Units6;
				strReturn += Strings.Format(LInfo.InfluencePercentage6, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode6;
				strReturn += LInfo.LandType7;
				strReturn += LInfo.Units7;
				strReturn += Strings.Format(LInfo.InfluencePercentage7, "000.00");
				// 6 chars
				strReturn += LInfo.InfluenceCode7;
				FillLandRec = strReturn;
				return FillLandRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillLandRec", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillLandRec;
		}

		private string FillDwellingRec()
		{
			int lngAcct = 0;
			string FillDwellingRec = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strReturn;
				//FC:FINAL:RPU - Use custom constructor to initialize fields
				typDwellingData DInfo = new typDwellingData(0);
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper rsDwell = new clsDRWrapper();
				strReturn = "";
				FillDwellingRec = "";
				lngAcct = FCConvert.ToInt32(rsMain.Get_Fields_Int32("rsaccount"));
				rsDwell.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = 1", modGlobalVariables.strREDatabase);
				if (!rsDwell.EndOfFile())
				{
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("distyle"))), "DWELLING", "STYLE", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.BuildingType = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.BuildingType = Strings.StrDup(30, " ");
					}
					DInfo.DwellingUnits = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("diunitsdwelling"))));
					DInfo.OtherUnits = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("diunitsother"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("distories")) + 1480), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						if (Conversion.Val(modGlobalVariables.Statics.CostRec.CBase) > 0)
						{
							DInfo.Stories = Conversion.Val(modGlobalVariables.Statics.CostRec.CBase) / 100;
						}
						else
						{
							DInfo.Stories = Conversion.Val(rsDwell.Get_Fields_Int32("distories"));
						}
					}
					else
					{
						DInfo.Stories = Conversion.Val(rsDwell.Get_Fields_Int32("distories"));
					}
					if (DInfo.Stories > 99)
					{
						DInfo.Stories = 1;
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("diextwalls"))), "DWELLING", "EXTERIOR", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.ExteriorWalls = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.ExteriorWalls = Strings.StrDup(30, " ");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("diroof")) + 1510), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.RoofSurface = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.RoofSurface = Strings.StrDup(30, " ");
					}
					DInfo.SqftMasonryTrim = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("disfmasonry"))));
					DInfo.YearBuilt = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("diyearbuilt"))));
					DInfo.YearRemodeled = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("diyearremodel"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("difoundation")) + 1540), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.FoundationType = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.FoundationType = Strings.StrDup(30, " ");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dibsmt")) + 1550), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.BasementType = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.BasementType = Strings.StrDup(30, " ");
					}
					DInfo.BasementCars = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dibsmtgar"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("diwetbsmt")) + 1560), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.WetBasement = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.WetBasement = Strings.StrDup(30, " ");
					}
					DInfo.SqftBasementLivingArea = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("disfbsmtliving"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dibsmtfingrade1")) + 1670), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.FinishedBasementGrade = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.FinishedBasementGrade = Strings.StrDup(30, " ");
					}
					DInfo.FinishedBasementFactor = Conversion.Val(rsDwell.Get_Fields_Int32("dibsmtfingrade2")) / 100;
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("diheat"))), "DWELLING", "HEAT", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.HeatType = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.HeatType = Strings.StrDup(30, " ");
					}
					DInfo.PercentageHeated = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dipctheat"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dicool")) + 1600), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.CoolType = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.CoolType = Strings.StrDup(30, " ");
					}
					DInfo.PercentageCooled = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dipctcool"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dikitchens")) + 1610), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.KitchenStyle = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.KitchenStyle = Strings.StrDup(30, " ");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dibaths")) + 1620), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.BathStyle = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.BathStyle = Strings.StrDup(30, " ");
					}
					DInfo.NumberRooms = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dirooms"))));
					DInfo.NumberBedRooms = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dibedrooms"))));
					DInfo.NumberBaths = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("difullbaths"))));
					DInfo.NumberHalfBaths = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dihalfbaths"))));
					DInfo.AdditionalFixtures = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("diaddnfixtures"))));
					DInfo.NumberFireplaces = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("difireplaces"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dilayout")) + 1630), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.Layout = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.Layout = Strings.StrDup(30, " ");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("diattic")) + 1640), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.Attic = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.Attic = Strings.StrDup(30, " ");
					}
					DInfo.SFLA = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("disfla"))));
					DInfo.PercentUnfinished = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dipctunfinished"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("digrade1")) + 1670), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.Grade = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.Grade = Strings.StrDup(30, " ");
					}
					DInfo.Factor = Conversion.Val(rsDwell.Get_Fields_Int32("digrade2")) / 100;
					DInfo.Sqft = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("disqft"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dicondition")) + 1460), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.Condition = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.Condition = Strings.StrDup(30, " ");
					}
					DInfo.PhysicalGood = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dipctphys"))));
					DInfo.FunctionalGood = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dipctfunct"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("difunctcode")) + 1680), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.FunctionalCode = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.FunctionalCode = Strings.StrDup(30, " ");
					}
					DInfo.EconomicGood = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDwell.Get_Fields_Int32("dipctecon"))));
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsDwell.Get_Fields_Int32("dieconcode"))), "DWELLING", "ECONOMIC", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
					{
						DInfo.EconomicCode = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
					}
					else
					{
						DInfo.EconomicCode = Strings.StrDup(30, " ");
					}
				}
				else
				{
					DInfo.BuildingType = Strings.StrDup(30, " ");
					DInfo.DwellingUnits = 0;
					DInfo.OtherUnits = 0;
					DInfo.Stories = 0;
					DInfo.ExteriorWalls = Strings.StrDup(30, " ");
					DInfo.RoofSurface = Strings.StrDup(30, " ");
					DInfo.SqftMasonryTrim = 0;
					DInfo.YearBuilt = 0;
					DInfo.YearRemodeled = 0;
					DInfo.FoundationType = Strings.StrDup(30, " ");
					DInfo.BasementType = Strings.StrDup(30, " ");
					DInfo.BasementCars = 0;
					DInfo.WetBasement = Strings.StrDup(30, " ");
					DInfo.SqftBasementLivingArea = 0;
					DInfo.FinishedBasementGrade = Strings.StrDup(30, " ");
					DInfo.FinishedBasementFactor = 0;
					DInfo.HeatType = Strings.StrDup(30, " ");
					DInfo.PercentageHeated = 0;
					DInfo.CoolType = Strings.StrDup(30, " ");
					DInfo.PercentageCooled = 0;
					DInfo.KitchenStyle = Strings.StrDup(30, " ");
					DInfo.BathStyle = Strings.StrDup(30, " ");
					DInfo.NumberRooms = 0;
					DInfo.NumberBedRooms = 0;
					DInfo.NumberBaths = 0;
					DInfo.NumberHalfBaths = 0;
					DInfo.AdditionalFixtures = 0;
					DInfo.NumberFireplaces = 0;
					DInfo.Layout = Strings.StrDup(30, " ");
					DInfo.Attic = Strings.StrDup(30, " ");
					DInfo.SFLA = 0;
					DInfo.PercentUnfinished = 0;
					DInfo.Grade = Strings.StrDup(30, " ");
					DInfo.Factor = 0;
					DInfo.Sqft = 0;
					DInfo.Condition = Strings.StrDup(30, " ");
					DInfo.PhysicalGood = 0;
					DInfo.FunctionalGood = 0;
					DInfo.FunctionalCode = Strings.StrDup(30, " ");
					DInfo.EconomicCode = FCConvert.ToString(0);
					DInfo.EconomicCode = Strings.StrDup(30, " ");
				}
				strReturn = "";
				strReturn += DInfo.BuildingType;
				strReturn += Strings.Format(DInfo.DwellingUnits, "00");
				// 2
				strReturn += Strings.Format(DInfo.OtherUnits, "00");
				// 2
				strReturn += Strings.Format(DInfo.Stories, "0.00");
				strReturn += DInfo.ExteriorWalls;
				strReturn += DInfo.RoofSurface;
				strReturn += Strings.Format(DInfo.SqftMasonryTrim, "00000");
				// 5
				strReturn += Strings.Format(DInfo.YearBuilt, "0000");
				strReturn += Strings.Format(DInfo.YearRemodeled, "0000");
				strReturn += DInfo.FoundationType;
				strReturn += DInfo.BasementType;
				strReturn += Strings.Format(DInfo.BasementCars, "00");
				// 2
				strReturn += DInfo.WetBasement;
				strReturn += Strings.Format(DInfo.SqftBasementLivingArea, "00000");
				// 5
				strReturn += DInfo.FinishedBasementGrade;
				strReturn += Strings.Format(DInfo.FinishedBasementFactor, "00.00");
				// 5
				strReturn += DInfo.HeatType;
				strReturn += Strings.Format(DInfo.PercentageHeated, "000.00");
				// 6
				strReturn += DInfo.CoolType;
				strReturn += Strings.Format(DInfo.PercentageCooled, "000.00");
				strReturn += DInfo.KitchenStyle;
				strReturn += DInfo.BathStyle;
				strReturn += Strings.Format(DInfo.NumberRooms, "000");
				// 3
				strReturn += Strings.Format(DInfo.NumberBedRooms, "00");
				// 2
				strReturn += Strings.Format(DInfo.NumberBaths, "00");
				strReturn += Strings.Format(DInfo.NumberHalfBaths, "00");
				strReturn += Strings.Format(DInfo.AdditionalFixtures, "000");
				strReturn += Strings.Format(DInfo.NumberFireplaces, "00");
				strReturn += DInfo.Layout;
				strReturn += DInfo.Attic;
				strReturn += Strings.Format(DInfo.SFLA, "000000");
				// 6
				strReturn += Strings.Format(DInfo.PercentUnfinished, "000");
				strReturn += DInfo.Grade;
				strReturn += Strings.Format(DInfo.Factor, "00.00");
				// 5
				strReturn += Strings.Format(DInfo.Sqft, "000000");
				// 6
				strReturn += DInfo.Condition;
				strReturn += Strings.Format(DInfo.PhysicalGood, "000");
				strReturn += Strings.Format(DInfo.FunctionalGood, "000");
				strReturn += DInfo.FunctionalCode;
				strReturn += Strings.Format(DInfo.EconomicGood, "000");
				strReturn += DInfo.EconomicCode;
				FillDwellingRec = strReturn;
				return FillDwellingRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillDwellingRec Account " + FCConvert.ToString(lngAcct), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillDwellingRec;
		}

		private string FillOutBuildingRec()
		{
			string FillOutBuildingRec = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strReturn;
				int lngAcct;
				typOutbuilding[] OInfo = new typOutbuilding[10 + 1];
				string strTemp = "";
				clsDRWrapper rsOut = new clsDRWrapper();
				int intTemp = 0;
				int x;
				string[] strAry = null;
				int intWidth = 0;
				// vbPorter upgrade warning: intLength As short --> As int	OnWriteFCConvert.ToDouble(
				int intLength = 0;
				strReturn = "";
				FillOutBuildingRec = "";
				lngAcct = FCConvert.ToInt32(rsMain.Get_Fields_Int32("rsaccount"));
				rsOut.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = 1", modGlobalVariables.strREDatabase);
				if (!rsOut.EndOfFile())
				{
					for (x = 1; x <= 10; x++)
					{
						// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [OITYPE] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x))) > 0 && Conversion.Val(rsOut.Get_Fields("OITYPE" + FCConvert.ToString(x))) < 3000)
						{
							// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x)))), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
							{
								OInfo[x].BuildingType = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
							}
							else
							{
								OInfo[x].BuildingType = Strings.StrDup(30, " ");
							}
							// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
							OInfo[x].YearBuilt = FCConvert.ToInt16(Math.Round(Conversion.Val(rsOut.Get_Fields("oiyear" + FCConvert.ToString(x)))));
							// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
							if (!FCConvert.ToBoolean(rsOut.Get_Fields("oiusesound" + FCConvert.ToString(x))))
							{
								OInfo[x].SoundValue = "NO ";
								OInfo[x].SoundValueAmount = 0;
								// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
								OInfo[x].PhysicalPercent = FCConvert.ToInt16(Math.Round(Conversion.Val(rsOut.Get_Fields("oipctphys" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
								OInfo[x].GradeFactor = Conversion.Val(rsOut.Get_Fields("oigradepct" + FCConvert.ToString(x))) / 100;
								// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
								if (Conversion.Val(rsOut.Get_Fields("oigradecd" + FCConvert.ToString(x))) > 0)
								{
									// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
									if (Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x))) < 700)
									{
										// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
										if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsOut.Get_Fields("oigradecd" + FCConvert.ToString(x))) + 1670), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
										{
											OInfo[x].Grade = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
										}
										else
										{
											OInfo[x].Grade = Strings.StrDup(30, " ");
										}
									}
									else
									{
										// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
										intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOut.Get_Fields("oigradecd" + FCConvert.ToString(x)))));
										if (intTemp == 1)
											intTemp = 2;
										if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1670 + intTemp - 1), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
										{
											if (Conversion.Val(modGlobalVariables.Statics.CostRec.CSDesc) == 0)
											{
												OInfo[x].Grade = modGlobalFunctions.PadStringWithSpaces(Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2), 30, false);
											}
											else
											{
												strAry = Strings.Split(modGlobalVariables.Statics.CostRec.CSDesc, " ", -1, CompareConstants.vbTextCompare);
												if (Information.UBound(strAry, 1) > 0)
												{
													OInfo[x].Grade = modGlobalFunctions.PadStringWithSpaces(Strings.Mid(strAry[1], 1, 2), 30, false);
												}
												else
												{
													OInfo[x].Grade = modGlobalFunctions.PadStringWithSpaces(Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2), 30, false);
												}
											}
										}
										else
										{
											OInfo[x].Grade = Strings.StrDup(30, " ");
										}
									}
								}
								else
								{
									OInfo[x].Grade = modGlobalFunctions.PadStringWithSpaces("Same As Dwelling", 30, false);
								}
								// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
								if (Conversion.Val(rsOut.Get_Fields("oicond" + FCConvert.ToString(x))) > 0)
								{
									// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
									if (Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x))) < 700)
									{
										// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
										if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsOut.Get_Fields("oicond" + FCConvert.ToString(x))) + 1460), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
										{
											OInfo[x].Condition = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.CostRec.ClDesc, 30, false);
										}
										else
										{
											OInfo[x].Condition = Strings.StrDup(30, " ");
										}
									}
									else
									{
										// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
										if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsOut.Get_Fields("oicond" + FCConvert.ToString(x))) + 1790), "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
										{
											OInfo[x].Condition = modGlobalFunctions.PadStringWithSpaces(Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 4), 30, false);
										}
										else
										{
											OInfo[x].Condition = Strings.StrDup(30, " ");
										}
									}
								}
								else
								{
									OInfo[x].Condition = Strings.StrDup(30, " ");
								}
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (Conversion.Val(rsOut.Get_Fields("oitype" + FCConvert.ToString(x))) < 700)
								{
									// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
									OInfo[x].Units = Strings.Format(FCConvert.ToString(Conversion.Val(rsOut.Get_Fields("oiunits" + FCConvert.ToString(x)))), "0000000");
									// 7
								}
								else
								{
									// parse it out
									// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
									intWidth = fecherFoundation.FCUtils.iDiv(Conversion.Val(rsOut.Get_Fields("oiunits" + FCConvert.ToString(x))), 100);
									// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
									intLength = FCConvert.ToInt32(Conversion.Val(rsOut.Get_Fields("oiunits" + FCConvert.ToString(x))) - (intWidth * 100));
									if (intWidth == 0 || FCConvert.ToBoolean(intLength - 0))
									{
										OInfo[x].Units = "000x000";
									}
									else
									{
										OInfo[x].Units = Strings.Format(intWidth, "000") + "x" + Strings.Format(intLength, "000");
									}
								}
							}
							else
							{
								OInfo[x].SoundValue = "YES";
								// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
								OInfo[x].SoundValueAmount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))));
								OInfo[x].Condition = Strings.StrDup(30, " ");
								OInfo[x].Grade = Strings.StrDup(30, " ");
								OInfo[x].GradeFactor = 0;
								OInfo[x].PhysicalPercent = 0;
								OInfo[x].Units = Strings.StrDup(7, "0");
							}
						}
						else
						{
							OInfo[x].BuildingType = Strings.StrDup(30, " ");
							OInfo[x].Condition = Strings.StrDup(30, " ");
							OInfo[x].FunctionPercent = 0;
							OInfo[x].Grade = Strings.StrDup(30, " ");
							OInfo[x].GradeFactor = 0;
							OInfo[x].PhysicalPercent = 0;
							OInfo[x].SoundValue = "NO ";
							OInfo[x].SoundValueAmount = 0;
							OInfo[x].Units = Strings.StrDup(7, "0");
							OInfo[x].YearBuilt = FCConvert.ToInt16("0000");
						}
					}
					// x
				}
				else
				{
					for (x = 1; x <= 10; x++)
					{
						OInfo[x].BuildingType = Strings.StrDup(30, " ");
						OInfo[x].Condition = Strings.StrDup(30, " ");
						OInfo[x].FunctionPercent = 0;
						OInfo[x].Grade = Strings.StrDup(30, " ");
						OInfo[x].GradeFactor = 0;
						OInfo[x].PhysicalPercent = 0;
						OInfo[x].SoundValue = "NO ";
						OInfo[x].SoundValueAmount = 0;
						OInfo[x].Units = Strings.StrDup(7, "0");
						OInfo[x].YearBuilt = FCConvert.ToInt16("0000");
					}
					// x
				}
				strReturn = "";
				for (x = 1; x <= 10; x++)
				{
					strReturn += OInfo[x].BuildingType;
					strReturn += Strings.Format(OInfo[x].YearBuilt, "0000");
					strReturn += OInfo[x].Units;
					strReturn += OInfo[x].Grade;
					strReturn += Strings.Format(OInfo[x].GradeFactor, "00.00");
					// 5
					strReturn += OInfo[x].Condition;
					strReturn += Strings.Format(OInfo[x].PhysicalPercent, "000");
					strReturn += OInfo[x].SoundValue;
					strReturn += Strings.Format(OInfo[x].SoundValueAmount, "000000000");
					// 9
				}
				// x
				FillOutBuildingRec = strReturn;
				return FillOutBuildingRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillOutBuildingRec", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillOutBuildingRec;
		}

		private string FillCommercialRec()
		{
			string FillCommercialRec = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strReturn;
				int lngAcct;
				//FC:FINAL:RPU - Use custom constructor to initialize fields
				typCommercial CInfo = new typCommercial(0);
				string strTemp = "";
				clsDRWrapper rsCom = new clsDRWrapper();
				// vbPorter upgrade warning: intTemp As short --> As int	OnWriteFCConvert.ToDouble(
				int intTemp = 0;
				// vbPorter upgrade warning: intGroup As short --> As int	OnWriteFCConvert.ToDouble(
				int intGroup = 0;
				int intClass = 0;
				strReturn = "";
				FillCommercialRec = "";
				lngAcct = FCConvert.ToInt32(rsMain.Get_Fields_Int32("rsaccount"));
				rsCom.OpenRecordset("select * from commercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = 1", modGlobalVariables.strREDatabase);
				if (!rsCom.EndOfFile())
				{
					if (Conversion.Val(rsCom.Get_Fields_Int32("occ1")) > 0)
					{
						CInfo.EconomicFactor = Conversion.Val(rsCom.Get_Fields_Int32("cmecon"));
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsCom.Get_Fields_Int32("occ1"))), "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
						{
							CInfo.BuildingType1 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
							intGroup = FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.ComCostRec.CBase) / 100);
							intTemp = FCConvert.ToInt32((Conversion.Val(modGlobalVariables.Statics.ComCostRec.CUnit) / 100) + ((intClass * 4) - 4) + Conversion.Val(rsCom.Get_Fields_Int32("c1quality")) - 1);
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
							{
								CInfo.BuildingClassQuality1 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
							}
							else
							{
								CInfo.BuildingClassQuality1 = Strings.StrDup(30, " ");
							}
							intTemp = FCConvert.ToInt32(140 + (20 * intGroup) + Conversion.Val(rsCom.Get_Fields_Int32("c1heat")) - 10);
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
							{
								CInfo.HeatingCoolingType1 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
							}
							else
							{
								CInfo.HeatingCoolingType1 = Strings.StrDup(30, " ");
							}
						}
						else
						{
							CInfo.BuildingType1 = Strings.StrDup(30, " ");
							CInfo.BuildingClassQuality1 = Strings.StrDup(30, " ");
							CInfo.HeatingCoolingType1 = Strings.StrDup(30, " ");
						}
						CInfo.NumberDwellingUnits1 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("dwel1"))));
						CInfo.GradeFactor1 = Conversion.Val(rsCom.Get_Fields_Int32("c1grade"));
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(340 + Conversion.Val(rsCom.Get_Fields_Int32("c1extwalls"))), "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
						{
							CInfo.ExteriorWalls1 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
						}
						else
						{
							CInfo.ExteriorWalls1 = Strings.StrDup(30, " ");
						}
						CInfo.Stories1 = Conversion.Val(rsCom.Get_Fields_Int32("c1stories"));
						CInfo.StoriesHeight1 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c1height"))));
						CInfo.YearBuilt1 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c1built"))));
						CInfo.YearRemodeled1 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c1remodel"))));
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(350 + Conversion.Val(rsCom.Get_Fields_Int32("c1condition"))), "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
						{
							CInfo.Condition1 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
						}
						else
						{
							CInfo.Condition1 = Strings.StrDup(30, " ");
						}
						CInfo.PhysicalGood1 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c1phys"))));
						CInfo.FunctionalGood1 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c1funct"))));
					}
					else
					{
						CInfo.BuildingClassQuality1 = Strings.StrDup(30, " ");
						CInfo.BuildingType1 = Strings.StrDup(30, " ");
						CInfo.Condition1 = Strings.StrDup(30, " ");
						CInfo.EconomicFactor = 0;
						CInfo.ExteriorWalls1 = Strings.StrDup(30, " ");
						CInfo.FunctionalGood1 = 0;
						CInfo.GradeFactor1 = 0;
						CInfo.HeatingCoolingType1 = Strings.StrDup(30, " ");
						CInfo.NumberDwellingUnits1 = 0;
						CInfo.PhysicalGood1 = 0;
						CInfo.Stories1 = 0;
						CInfo.StoriesHeight1 = 0;
						CInfo.YearBuilt1 = 0;
						CInfo.YearRemodeled1 = 0;
					}
					if (Conversion.Val(rsCom.Get_Fields_Int32("occ2")) > 0)
					{
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(rsCom.Get_Fields_Int32("occ2"))), "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
						{
							CInfo.BuildingType2 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
							intGroup = FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.ComCostRec.CBase) / 100);
							intTemp = FCConvert.ToInt32((Conversion.Val(modGlobalVariables.Statics.ComCostRec.CUnit) / 100) + ((intClass * 4) - 4) + Conversion.Val(rsCom.Get_Fields_Int32("c2quality")) - 1);
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
							{
								CInfo.BuildingClassQuality2 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
							}
							else
							{
								CInfo.BuildingClassQuality2 = Strings.StrDup(30, " ");
							}
							intTemp = FCConvert.ToInt32(140 + (20 * intGroup) + Conversion.Val(rsCom.Get_Fields_Int32("c2heat")) - 10);
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
							{
								CInfo.HeatingCoolingType2 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
							}
							else
							{
								CInfo.HeatingCoolingType2 = Strings.StrDup(30, " ");
							}
						}
						else
						{
							CInfo.BuildingType2 = Strings.StrDup(30, " ");
							CInfo.BuildingClassQuality2 = Strings.StrDup(30, " ");
							CInfo.HeatingCoolingType2 = Strings.StrDup(30, " ");
						}
						CInfo.NumberDwellingUnits2 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("dwel2"))));
						CInfo.GradeFactor2 = Conversion.Val(rsCom.Get_Fields_Int32("c2grade"));
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(340 + Conversion.Val(rsCom.Get_Fields_Int32("c2extwalls"))), "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
						{
							CInfo.ExteriorWalls2 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
						}
						else
						{
							CInfo.ExteriorWalls2 = Strings.StrDup(30, " ");
						}
						CInfo.Stories2 = Conversion.Val(rsCom.Get_Fields_Int32("c2stories"));
						CInfo.StoriesHeight2 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c2height"))));
						CInfo.YearBuilt2 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c2built"))));
						CInfo.YearRemodeled2 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c2remodel"))));
						if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(350 + Conversion.Val(rsCom.Get_Fields_Int32("c2condition"))), "COMMERCIAL", "COMMERCIAL", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber))
						{
							CInfo.Condition2 = modGlobalFunctions.PadStringWithSpaces(modGlobalVariables.Statics.ComCostRec.ClDesc, 30, false);
						}
						else
						{
							CInfo.Condition2 = Strings.StrDup(30, " ");
						}
						CInfo.PhysicalGood2 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c2phys"))));
						CInfo.FunctionalGood2 = FCConvert.ToInt16(Math.Round(Conversion.Val(rsCom.Get_Fields_Int32("c2funct"))));
					}
					else
					{
						CInfo.BuildingClassQuality2 = Strings.StrDup(30, " ");
						CInfo.BuildingType2 = Strings.StrDup(30, " ");
						CInfo.Condition2 = Strings.StrDup(30, " ");
						CInfo.ExteriorWalls2 = Strings.StrDup(30, " ");
						CInfo.FunctionalGood2 = 0;
						CInfo.GradeFactor2 = 0;
						CInfo.HeatingCoolingType2 = Strings.StrDup(30, " ");
						CInfo.NumberDwellingUnits2 = 0;
						CInfo.PhysicalGood2 = 0;
						CInfo.Stories2 = 0;
						CInfo.StoriesHeight2 = 0;
						CInfo.YearBuilt2 = 0;
						CInfo.YearRemodeled2 = 0;
					}
				}
				else
				{
					CInfo.BuildingClassQuality1 = Strings.StrDup(30, " ");
					CInfo.BuildingClassQuality2 = Strings.StrDup(30, " ");
					CInfo.BuildingType1 = Strings.StrDup(30, " ");
					CInfo.BuildingType2 = Strings.StrDup(30, " ");
					CInfo.Condition1 = Strings.StrDup(30, " ");
					CInfo.Condition2 = Strings.StrDup(30, " ");
					CInfo.EconomicFactor = 0;
					CInfo.ExteriorWalls1 = Strings.StrDup(30, " ");
					CInfo.ExteriorWalls2 = Strings.StrDup(30, " ");
					CInfo.FunctionalGood1 = 0;
					CInfo.FunctionalGood2 = 0;
					CInfo.GradeFactor1 = 0;
					CInfo.GradeFactor2 = 0;
					CInfo.HeatingCoolingType1 = Strings.StrDup(30, " ");
					CInfo.HeatingCoolingType2 = Strings.StrDup(30, " ");
					CInfo.NumberDwellingUnits1 = 0;
					CInfo.NumberDwellingUnits2 = 0;
					CInfo.PhysicalGood1 = 0;
					CInfo.PhysicalGood2 = 0;
					CInfo.Stories1 = 0;
					CInfo.Stories2 = 0;
					CInfo.StoriesHeight1 = 0;
					CInfo.StoriesHeight2 = 0;
					CInfo.YearBuilt1 = 0;
					CInfo.YearBuilt2 = 0;
					CInfo.YearRemodeled1 = 0;
					CInfo.YearRemodeled2 = 0;
				}
				strReturn = "";
				strReturn += Strings.Format(CInfo.EconomicFactor, "000");
				strReturn += CInfo.BuildingType1;
				strReturn += Strings.Format(CInfo.NumberDwellingUnits1, "000");
				strReturn += CInfo.BuildingClassQuality1;
				strReturn += Strings.Format(CInfo.GradeFactor1, "000.00");
				// 6
				strReturn += CInfo.ExteriorWalls1;
				strReturn += Strings.Format(CInfo.Stories1, "00");
				strReturn += Strings.Format(CInfo.StoriesHeight1, "00");
				strReturn += CInfo.HeatingCoolingType1;
				strReturn += Strings.Format(CInfo.YearBuilt1, "0000");
				strReturn += Strings.Format(CInfo.YearRemodeled1, "0000");
				strReturn += CInfo.Condition1;
				strReturn += Strings.Format(CInfo.PhysicalGood1, "000");
				strReturn += Strings.Format(CInfo.FunctionalGood1, "000");
				strReturn += CInfo.BuildingType2;
				strReturn += Strings.Format(CInfo.NumberDwellingUnits2, "000");
				strReturn += CInfo.BuildingClassQuality2;
				strReturn += Strings.Format(CInfo.GradeFactor2, "000.00");
				// 6
				strReturn += CInfo.ExteriorWalls2;
				strReturn += Strings.Format(CInfo.Stories2, "00");
				strReturn += Strings.Format(CInfo.StoriesHeight2, "00");
				strReturn += CInfo.HeatingCoolingType2;
				strReturn += Strings.Format(CInfo.YearBuilt2, "0000");
				strReturn += Strings.Format(CInfo.YearRemodeled2, "0000");
				strReturn += CInfo.Condition2;
				strReturn += Strings.Format(CInfo.PhysicalGood2, "000");
				strReturn += Strings.Format(CInfo.FunctionalGood2, "000");
				FillCommercialRec = strReturn;
				return FillCommercialRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillCommercialRec", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommercialRec;
		}

		private void CreateExtract(bool modalDialog)
		{
			bool boolFileOpen = false;
			//TextStream ts = null;
			StreamWriter ts = null;
			////FileSystemObject fso = new FileSystemObject();
			string strRec = "";
			string strTemp = "";
			int lngTotRecs = 0;
			int lngCount;
			try
			{
				// On Error GoTo ErrorHandler
				boolFileOpen = false;
				//ts = fso.CreateTextFile(strSavePath, true, false);
				ts = File.CreateText(strSavePath);
				boolFileOpen = true;
				frmWait.InstancePtr.Init("Saving Account");
				if (!rsMain.EndOfFile())
				{
					lngTotRecs = rsMain.RecordCount();
				}
				lngCount = 0;
				while (!rsMain.EndOfFile())
				{
					// frmWait.lblCounter.Caption = rsMain.Fields("rsaccount")
					lngCount += 1;
					frmWait.InstancePtr.lblMessage.Text = "Processing " + FCConvert.ToString(lngCount) + " / " + FCConvert.ToString(lngTotRecs);
					frmWait.InstancePtr.lblMessage.Refresh();
					// frmWait.lblCounter.Refresh
					//Application.DoEvents();
					strRec = FillMasterRec();
					if (strRec != string.Empty)
					{
						// ts.WriteLine (strRec)
					}
					else
					{
						frmWait.InstancePtr.Unload();
						ts.Close();
						MessageBox.Show("Error trying to fill account information" + "\r\n" + "Cannot continue", "Task Stopped", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strTemp = FillLandRec();
					if (strTemp != string.Empty)
					{
						strRec += strTemp;
						// ts.WriteLine (strRec)
					}
					else
					{
						frmWait.InstancePtr.Unload();
						ts.Close();
						MessageBox.Show("Error trying to fill land information" + "\r\n" + "Cannot continue", "Task Stopped", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strTemp = FillDwellingRec();
					if (strTemp != string.Empty)
					{
						strRec += strTemp;
						// ts.WriteLine (strRec)
					}
					else
					{
						frmWait.InstancePtr.Unload();
						ts.Close();
						MessageBox.Show("Error trying to fill dwelling information" + "\r\n" + "Cannot continue", "Task Stopped", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strTemp = FillCommercialRec();
					if (strTemp != string.Empty)
					{
						strRec += strTemp;
						// ts.WriteLine (strRec)
					}
					else
					{
						frmWait.InstancePtr.Unload();
						ts.Close();
						MessageBox.Show("Error trying to fill commercial information" + "\r\n" + "Cannot continue", "Task Stopped", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strTemp = FillOutBuildingRec();
					if (strTemp != string.Empty)
					{
						strRec += strTemp;
						// ts.WriteLine (strRec)
					}
					else
					{
						frmWait.InstancePtr.Unload();
						ts.Close();
						MessageBox.Show("Error trying to fill outbuilding information" + "\r\n" + "Cannot continue", "Task Stopped", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					ts.WriteLine(strRec);
					rsMain.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				ts.Close();
				boolFileOpen = false;
				boolFileCreated = true;
				if (boolShowSuccessMessage)
				{
					MessageBox.Show("Extract completed successfully" + "\r\n" + "File Name " + strSavePath, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
					rptInfoExtract.InstancePtr.Init(strSavePath, modalDialog);
					// ShowLayout
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
				{
					ts.Close();
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CreateExtract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void ShowLayout(bool modalDialog)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strOutput;
				strOutput = "";
				// masterinfo
				// strOutput = strOutput & "Field" & vbTab & "Type" & vbTab & "Size" & vbNewLine
				strOutput += "Account" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Card" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Map/Lot" + "\t" + "String" + "\t" + "20" + "\r\n";
				strOutput += "Name" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Second Owner" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Previous Owner" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Address 1" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Address 2" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "City" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "State" + "\t" + "String" + "\t" + "2" + "\r\n";
				strOutput += "Zip" + "\t" + "String" + "\t" + "5" + "\r\n";
				strOutput += "Zip Extension" + "\t" + "String" + "\t" + "4" + "\r\n";
				strOutput += "Street Number" + "\t" + "String" + "\t" + "6" + "\r\n";
				strOutput += "Street Name" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Phone" + "\t" + "String" + "\t" + "13" + "\r\n";
				strOutput += "E-mail" + "\t" + "String" + "\t" + "40" + "\r\n";
				strOutput += "Book & Page" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Reference 1" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Reference 2" + "\t" + "String" + "\t" + "50" + "\r\n";
				strOutput += "Land Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Building Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Exemption" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Exempt 1 Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Exempt 1 Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Exempt 2 Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Exempt 2 Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Exempt 3 Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Exempt 3 Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Sale Price" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Sale Date" + "\t" + "String" + "\t" + "10" + "\r\n";
				strOutput += "Sale Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Sale Financing" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Sale Verified" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Sale Validity" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Total Acres" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Soft Acres" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Mixed Acres" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Hard Acres" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Other Acres" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Soft Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Mixed Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Hard Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Other Value" + "\t" + "Numeric" + "\t" + "11" + "\r\n";
				strOutput += "Tree Growth Year" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Tax Acquired" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "In Bankruptcy" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Neighborhood" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Zone" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Topography 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Topography 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Utilities 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Utilities 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Street Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Inspection Date" + "\t" + "String" + "\t" + "10" + "\r\n";
				strOutput += "Entrance" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Inspection Info" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += " " + "\t" + "\t" + "\r\n";
				strOutput += "Land Data" + "\t" + "\t" + "\r\n";
				strOutput += "Land Type 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 1" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Land Type 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 2" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Land Type 3" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 3" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Land Type 4" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 4" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Land Type 5" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 5" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Land Type 6" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 6" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Land Type 7" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Units 7" + "\t" + "String" + "\t" + "9" + "\r\n";
				strOutput += "Influence %" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Influence Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += " " + "\t" + "\t" + "\r\n";
				strOutput += "Dwelling Data" + "\t" + "\t" + "\r\n";
				strOutput += "Building Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Dwelling Units" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Other Units" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Stories" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Exterior" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Roof Surface" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Sqft Masonry Trim" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Year Built" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Year Remodeled" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Foundation Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Basement Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Basement Cars" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Wet Basement" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Sqft Bsmt Living" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Fin. Bsmt Grade" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Fin. Bsmt Factor" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Heat Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Percent Heated" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Cool Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Percent Cooled" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Kitchen Style" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Bath Style" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Number of Rooms" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Bedrooms" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Baths" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Half Baths" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Addn. Fixtures" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Fireplaces" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Layout" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Attic" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Sqft Living" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Pct Unfinished" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Grade" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Square Feet" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Condition" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Percent" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Functional Pct" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Functional Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Economic Percent" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Economic Code" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += " " + "\t" + "\t" + "\r\n";
				strOutput += "Commercial Data" + "\t" + "\t" + "\r\n";
				strOutput += "Economic Factor" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Building Type 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Dwelling Units 1" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Class/Quality 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 1" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Exterior 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Stories 1" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Stories Height" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Heat/Cool Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 1" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Year Remodeled" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Condition 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 1" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Functional Pct 1" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Building Type 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Dwelling Units 2" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Class/Quality 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 2" + "\t" + "Numeric" + "\t" + "6" + "\r\n";
				strOutput += "Exterior 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Stories 2" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Stories Height" + "\t" + "Numeric" + "\t" + "2" + "\r\n";
				strOutput += "Heat/Cool Type" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 2" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Year Remodeled" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Condition 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 2" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += "Functional Pct 2" + "\t" + "Numeric" + "\t" + "3" + "\r\n";
				strOutput += " " + "\t" + "\t" + "\r\n";
				strOutput += "Outbuilding/Addition Data" + "\t" + "\t" + "\r\n";
				strOutput += "Building Type 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 1" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 1" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 1" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 1" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 1" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 1" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built " + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 2" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 2" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 2" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 2" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 2" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 3" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 3" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 3" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 3" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 3" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 3" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 3" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 3" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 4" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 4" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 4" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 4" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 4" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 4" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 4" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 4" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 5" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 5" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 5" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 5" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 5" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 5" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 5" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 5" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 6" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 6" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 6" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 6" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 6" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 6" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 6" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 6" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 7" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 7" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 7" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 7" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 7" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 7" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 7" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 7" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 8" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 8" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 8" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 8" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 8" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 8" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 8" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 8" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 9" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 9" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 9" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 9" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 9" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 9" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 9" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 9" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				strOutput += "Building Type 10" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Year Built 10" + "\t" + "Numeric" + "\t" + "4" + "\r\n";
				strOutput += "Units 10" + "\t" + "String" + "\t" + "7" + "\r\n";
				strOutput += "Grade 10" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Grade Factor 10" + "\t" + "Numeric" + "\t" + "5" + "\r\n";
				strOutput += "Condition 10" + "\t" + "String" + "\t" + "30" + "\r\n";
				strOutput += "Physical Pct 10" + "\t" + "String" + "\t" + "3" + "\r\n";
				strOutput += "Use Sound Value" + "\t" + "Yes/No" + "\t" + "3" + "\r\n";
				strOutput += "Sound Value 10" + "\t" + "Numeric" + "\t" + "9" + "\r\n";
				rptREService.InstancePtr.Init(ref strOutput, modalDialog);
				// frmShowResults.Init (strOutput)
				// frmShowResults.Show
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ShowLayout", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
