﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptHighestAssessment.
	/// </summary>
	public partial class rptHighestAssessment : BaseSectionReport
	{
		public static rptHighestAssessment InstancePtr
		{
			get
			{
				return (rptHighestAssessment)Sys.GetInstance(typeof(rptHighestAssessment));
			}
		}

		protected rptHighestAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				rsAssess?.Dispose();
				rsAssessAccounts?.Dispose();
                rsAssess = null;
                rsAssessAccounts = null;
            }
			base.Dispose(disposing);
		}

		public rptHighestAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Highest Assessments";
		}
		// nObj = 1
		//   0	rptHighestAssessment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAssess = new clsDRWrapper();
		clsDRWrapper rsAssessAccounts = new clsDRWrapper();
		string FieldName = "";
		string strLandField = "";
		string strBldgField = "";
		bool boolByName;
		int intPageNum;
		bool boolUseExempt;
		double dblTotLand;
		double dblTotBldg;
		double dblTotExempt;
		double dblTotTot;
		// vbPorter upgrade warning: intNumValuations As short	OnWriteFCConvert.ToDouble(
		public void InitHighestAssessment(short intNumValuations, bool boolByAccount, ref bool boolBilling, ref bool boolUseTaxable)
		{
			// numvaluations is a number from 10 to 1000.  If not boolbyaccount then we are
			// grouping by name
			string strTemp = "";
			string strREFullDBName;
			string strMasterJoin;
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                strREFullDBName = clsLoad.Get_GetFullDBName("RealEstate");
                strMasterJoin = modREMain.GetMasterJoinForJoin();
                dblTotTot = 0;
                dblTotExempt = 0;
                dblTotBldg = 0;
                dblTotLand = 0;
                boolByName = !boolByAccount;
                boolUseExempt = boolUseTaxable;
                if (boolBilling)
                {
                    strLandField = "lastlandval";
                    strBldgField = "lastbldgval";
                }
                else
                {
                    strLandField = "rllandval";
                    strBldgField = "rlbldgval";
                }

                if (boolByAccount)
                {
                    if (boolUseExempt)
                    {
                        rsAssessAccounts.OpenRecordset(
                            "select  top " + FCConvert.ToString(intNumValuations) + " sum(" + strLandField +
                            ") + sum(" + strBldgField +
                            ") - sum(rlexemption) as totval,rsaccount from master where not rsdeleted = 1 group by rsaccount order by sum(" +
                            strLandField + ") + sum(" + strBldgField + ") - sum(rlexemption) desc",
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsAssessAccounts.OpenRecordset(
                            "select  top " + FCConvert.ToString(intNumValuations) + " sum(" + strLandField +
                            ") + sum(" + strBldgField +
                            ")  as totval,rsaccount from master where not rsdeleted = 1 group by rsaccount order by sum(" +
                            strLandField + ") + sum(" + strBldgField + ") desc", modGlobalVariables.strREDatabase);
                    }

                    FieldName = "rsaccount";
                    strTemp = "ACCOUNT NUMBER";
                    lblNumOf.Visible = false;
                }
                else
                {
                    if (boolUseExempt)
                    {
                        rsAssessAccounts.OpenRecordset(
                            "Select top " + FCConvert.ToString(intNumValuations) + " rsname, sum(" + strLandField +
                            ") + sum(" + strBldgField + ") - sum(rlexemption) as totval from " + strMasterJoin +
                            " where not rsdeleted = 1 group by rsname order by sum(" + strLandField + ") + sum(" +
                            strBldgField + ") - sum(rlexemption) desc", modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsAssessAccounts.OpenRecordset(
                            "Select top " + FCConvert.ToString(intNumValuations) + " rsname, sum(" + strLandField +
                            ") + sum(" + strBldgField + ")  as totval from " + strMasterJoin +
                            " where not rsdeleted = 1 group by rsname order by sum(" + strLandField + ") + sum(" +
                            strBldgField + ") desc", modGlobalVariables.strREDatabase);
                    }

                    FieldName = "rsname";
                    strTemp = "NAME";
                    lblAccount.Text = "ACCTS";
                }

                Title1.Text = "HIGHEST " + FCConvert.ToString(intNumValuations) + " VALUATIONS";
                Title2.Text = "BY " + strTemp;
            }

            // Me.Show
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "HighestAssessment");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = false;
			if (rsAssessAccounts.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intPageNum = 1;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngLandTot;
			int lngBldgTot;
			int lngTotTot;
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                int intNumAccts = 0;
                string strName = "";
                string strREFullDBName;
                string strMasterJoin;
                string strMasterJoinJoin;
                strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
                strMasterJoin = modREMain.GetMasterJoin();
                strMasterJoinJoin = modREMain.GetMasterJoinForJoin();
                if (!boolByName)
                {
                    // Call rsAssess.OpenRecordset("Select * from master where not rsdeleted = 1 and " & FieldName & " = " & rsAssessAccounts.GetData(FieldName), strREDatabase)
                    rsAssess.OpenRecordset(
                        strMasterJoin + " where not rsdeleted = 1 and " + FieldName + " = " +
                        rsAssessAccounts.GetData(FieldName), modGlobalVariables.strREDatabase);
                    rsTemp.OpenRecordset(
                        "select sum (rlexemption) as totexempt, sum(" + strLandField + ") as totland,sum(" +
                        strBldgField + ") as totbldg from master where not rsdeleted = 1 and " + FieldName + " = " +
                        rsAssessAccounts.Get_Fields(FieldName), modGlobalVariables.strREDatabase);
                    txtAccount.Text = FCConvert.ToString(rsAssess.GetData("rsaccount"));
                }
                else
                {
                    strName = FCConvert.ToString(rsAssessAccounts.Get_Fields_String("rsname"));
                    modGlobalRoutines.escapequote(ref strName);
                    rsAssess.OpenRecordset(
                        "Select count(*) as NumAccts from " + strMasterJoinJoin + " where rsname = '" + strName +
                        "' and rscard = 1 and not rsdeleted = 1", modGlobalVariables.strREDatabase);
                    intNumAccts = FCConvert.ToInt32(rsAssess.GetData("NumAccts"));
                    strName = FCConvert.ToString(rsAssessAccounts.GetData(FieldName));
                    modGlobalRoutines.escapequote(ref strName);
                    rsAssess.OpenRecordset(
                        strMasterJoin + " where not rsdeleted = 1 and " + FieldName + " = '" + strName + "'",
                        modGlobalVariables.strREDatabase);
                    txtAccount.Text = intNumAccts.ToString();
                    rsTemp.OpenRecordset(
                        "select sum (rlexemption) as totexempt, sum(" + strLandField + ") as totland,sum(" +
                        strBldgField + ") as totbldg from " + strMasterJoinJoin + " where not rsdeleted   = 1 and " +
                        FieldName + " = '" + strName + "'", modGlobalVariables.strREDatabase);
                }

                txtName.Text = FCConvert.ToString(rsAssess.GetData("rsname"));
                txtTotal.Text = Strings.Format(rsAssessAccounts.GetData("totval"), "#,###,###,##0");
                txtLand.Text = Strings.Format(rsTemp.GetData("totland"), "##,###,##0");
                // TODO Get_Fields: Field [totland] not found!! (maybe it is an alias?)
                dblTotLand += Conversion.Val(rsTemp.Get_Fields("totland"));
                // TODO Get_Fields: Field [totbldg] not found!! (maybe it is an alias?)
                dblTotBldg += Conversion.Val(rsTemp.Get_Fields("totbldg"));
                txtBldg.Text = Strings.Format(rsTemp.GetData("totbldg"), "#,###,###,##0");
                if (boolUseExempt)
                {
                    txtExempt.Text = Strings.Format(rsTemp.GetData("totexempt"), "##,###,##0");
                    // TODO Get_Fields: Field [totexempt] not found!! (maybe it is an alias?)
                    dblTotExempt += Conversion.Val(rsTemp.Get_Fields("totexempt"));
                }
                else
                {
                    txtExempt.Text = "N/A";
                }

                rsAssessAccounts.MoveNext();
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// txtMuniname = MuniName
			// txtDate = Date
			// txtPage = intPageNum
			intPageNum += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtLandTot.Text = Strings.Format(dblTotLand, "#,###,###,##0");
			txtBldgTot.Text = Strings.Format(dblTotBldg, "#,###,###,##0");
			if (boolUseExempt)
			{
				txtExemptTot.Text = Strings.Format(dblTotExempt, "#,###,###,##0");
				dblTotTot = dblTotLand + dblTotBldg - dblTotExempt;
			}
			else
			{
				dblTotTot = dblTotLand + dblTotBldg;
			}
			txtTotTot.Text = Strings.Format(dblTotTot, "#,###,###,###,##0");
		}

		
	}
}
