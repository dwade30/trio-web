﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialLocation
	{
		//=========================================================
		private int lngRecordID;
		private string strLocationState = string.Empty;
		private string strLocationCity = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string State
		{
			set
			{
				strLocationState = value;
			}
			get
			{
				string State = "";
				State = strLocationState;
				return State;
			}
		}

		public string Location
		{
			set
			{
				strLocationCity = value;
			}
			get
			{
				string Location = "";
				Location = strLocationCity;
				return Location;
			}
		}
	}
}
