﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptCommercialCard.
	/// </summary>
	partial class srptCommercialCard
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCommercialCard));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOccupancy1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtClassCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExteriorCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHeatCost11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtClassCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClassCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClassCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClassCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQualityCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQualityCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQualityCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQualityCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtConditionCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtOccupancy2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEntranceCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtInformationCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateInspected = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClass1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClass2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeFactor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGradeFactor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExterior1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExterior2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStories1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStories2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtArea1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtArea2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPerimeter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPerimeter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCool1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCool2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearBuilt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearBuilt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemodeled1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemodeled2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconomic = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtQuality1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQuality2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtHeight1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeight2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtEntrance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOccupancy1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOccupancy2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateInspected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeFactor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeFactor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExterior1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExterior2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArea1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArea2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerimeter1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerimeter2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomic)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuality1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuality2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntrance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtOccupancy1,
            this.Label2,
            this.Label3,
            this.txtClassCost5,
            this.Label4,
            this.Label5,
            this.txtExteriorCost1,
            this.txtExteriorCost2,
            this.txtExteriorCost3,
            this.txtExteriorCost4,
            this.txtExteriorCost5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.txtHeatCost11,
            this.txtHeatCost12,
            this.txtHeatCost13,
            this.txtHeatCost14,
            this.txtHeatCost15,
            this.txtHeatCost16,
            this.txtHeatCost17,
            this.txtHeatCost18,
            this.txtHeatCost19,
            this.txtHeatCost20,
            this.txtHeatCost21,
            this.txtHeatCost22,
            this.txtHeatCost23,
            this.txtHeatCost24,
            this.txtHeatCost25,
            this.txtHeatCost26,
            this.Label11,
            this.Label12,
            this.Label13,
            this.txtClassCost1,
            this.txtClassCost2,
            this.txtClassCost3,
            this.txtClassCost4,
            this.txtQualityCost1,
            this.txtQualityCost2,
            this.txtQualityCost3,
            this.txtQualityCost4,
            this.txtExteriorCost6,
            this.txtExteriorCost7,
            this.txtExteriorCost8,
            this.txtExteriorCost9,
            this.txtConditionCost1,
            this.txtConditionCost2,
            this.txtConditionCost3,
            this.txtConditionCost6,
            this.txtConditionCost7,
            this.txtConditionCost8,
            this.txtConditionCost4,
            this.txtConditionCost9,
            this.txtConditionCost5,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Line2,
            this.txtOccupancy2,
            this.Label17,
            this.txtEntranceCost1,
            this.txtEntranceCost2,
            this.txtEntranceCost3,
            this.txtEntranceCost6,
            this.txtEntranceCost7,
            this.txtEntranceCost8,
            this.txtEntranceCost4,
            this.txtEntranceCost5,
            this.txtEntranceCost9,
            this.Label18,
            this.txtInformationCost1,
            this.txtInformationCost2,
            this.txtInformationCost3,
            this.txtInformationCost6,
            this.txtInformationCost7,
            this.txtInformationCost8,
            this.txtInformationCost4,
            this.txtInformationCost5,
            this.txtInformationCost9,
            this.Label43,
            this.txtDateInspected,
            this.txtUnits1,
            this.txtUnits2,
            this.txtClass1,
            this.txtClass2,
            this.txtGradeFactor1,
            this.txtGradeFactor2,
            this.txtExterior1,
            this.txtExterior2,
            this.txtStories1,
            this.txtStories2,
            this.txtArea1,
            this.txtArea2,
            this.txtPerimeter1,
            this.txtPerimeter2,
            this.txtHeatCool1,
            this.txtHeatCool2,
            this.txtYearBuilt1,
            this.txtYearBuilt2,
            this.txtRemodeled1,
            this.txtRemodeled2,
            this.txtCondition1,
            this.txtCondition2,
            this.txtPhysical1,
            this.txtPhysical2,
            this.txtFunctional1,
            this.txtFunctional2,
            this.txtEconomic,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line8,
            this.Line11,
            this.Line12,
            this.Line14,
            this.Line13,
            this.Line15,
            this.Line16,
            this.Line17,
            this.Line18,
            this.txtQuality1,
            this.txtQuality2,
            this.Line1,
            this.txtHeight1,
            this.txtHeight2,
            this.Line9,
            this.Line10,
            this.txtEntrance,
            this.txtInformation,
            this.Line19,
            this.Line20,
            this.Line21,
            this.Line3,
            this.Image1});
			this.Detail.Height = 5.125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.125F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 6pt";
			this.Label1.Text = "Occupancy Code";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.75F;
			// 
			// txtOccupancy1
			// 
			this.txtOccupancy1.CanGrow = false;
			this.txtOccupancy1.Height = 0.125F;
			this.txtOccupancy1.Left = 1.3125F;
			this.txtOccupancy1.MultiLine = false;
			this.txtOccupancy1.Name = "txtOccupancy1";
			this.txtOccupancy1.Style = "font-size: 6pt";
			this.txtOccupancy1.Text = null;
			this.txtOccupancy1.Top = 0F;
			this.txtOccupancy1.Width = 1.125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.125F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 6pt";
			this.Label2.Text = "No. of Dwelling Units";
			this.Label2.Top = 0.125F;
			this.Label2.Width = 0.875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.125F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 6pt";
			this.Label3.Text = "Building Class/Quality";
			this.Label3.Top = 0.25F;
			this.Label3.Width = 0.875F;
			// 
			// txtClassCost5
			// 
			this.txtClassCost5.Height = 0.125F;
			this.txtClassCost5.Left = 0F;
			this.txtClassCost5.Name = "txtClassCost5";
			this.txtClassCost5.Style = "font-size: 6pt";
			this.txtClassCost5.Text = "4.";
			this.txtClassCost5.Top = 0.875F;
			this.txtClassCost5.Width = 0.625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.125F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 6pt";
			this.Label4.Text = "Grade Factor";
			this.Label4.Top = 1F;
			this.Label4.Width = 0.875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.125F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 6pt";
			this.Label5.Text = "Exterior Walls";
			this.Label5.Top = 1.125F;
			this.Label5.Width = 0.875F;
			// 
			// txtExteriorCost1
			// 
			this.txtExteriorCost1.Height = 0.125F;
			this.txtExteriorCost1.Left = 0F;
			this.txtExteriorCost1.Name = "txtExteriorCost1";
			this.txtExteriorCost1.Style = "font-size: 6pt";
			this.txtExteriorCost1.Text = "1.";
			this.txtExteriorCost1.Top = 1.25F;
			this.txtExteriorCost1.Width = 0.625F;
			// 
			// txtExteriorCost2
			// 
			this.txtExteriorCost2.Height = 0.125F;
			this.txtExteriorCost2.Left = 0F;
			this.txtExteriorCost2.Name = "txtExteriorCost2";
			this.txtExteriorCost2.Style = "font-size: 6pt";
			this.txtExteriorCost2.Text = "2.";
			this.txtExteriorCost2.Top = 1.375F;
			this.txtExteriorCost2.Width = 0.625F;
			// 
			// txtExteriorCost3
			// 
			this.txtExteriorCost3.Height = 0.125F;
			this.txtExteriorCost3.Left = 0F;
			this.txtExteriorCost3.Name = "txtExteriorCost3";
			this.txtExteriorCost3.Style = "font-size: 6pt";
			this.txtExteriorCost3.Text = "3.";
			this.txtExteriorCost3.Top = 1.5F;
			this.txtExteriorCost3.Width = 0.625F;
			// 
			// txtExteriorCost4
			// 
			this.txtExteriorCost4.Height = 0.125F;
			this.txtExteriorCost4.Left = 0F;
			this.txtExteriorCost4.Name = "txtExteriorCost4";
			this.txtExteriorCost4.Style = "font-size: 6pt";
			this.txtExteriorCost4.Text = "4.";
			this.txtExteriorCost4.Top = 1.625F;
			this.txtExteriorCost4.Width = 0.625F;
			// 
			// txtExteriorCost5
			// 
			this.txtExteriorCost5.Height = 0.125F;
			this.txtExteriorCost5.Left = 0F;
			this.txtExteriorCost5.Name = "txtExteriorCost5";
			this.txtExteriorCost5.Style = "font-size: 6pt";
			this.txtExteriorCost5.Text = "4.";
			this.txtExteriorCost5.Top = 1.75F;
			this.txtExteriorCost5.Width = 0.625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.125F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 6pt";
			this.Label6.Text = "Stories/Height";
			this.Label6.Top = 1.875F;
			this.Label6.Width = 0.875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.125F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 6pt";
			this.Label7.Text = "Ground Floor Area";
			this.Label7.Top = 2F;
			this.Label7.Width = 0.875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.125F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 6pt";
			this.Label8.Text = "Perimeter Units/Fl";
			this.Label8.Top = 2.125F;
			this.Label8.Width = 0.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.125F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 6pt";
			this.Label9.Text = "Heating/Cooling";
			this.Label9.Top = 2.25F;
			this.Label9.Width = 0.875F;
			// 
			// txtHeatCost11
			// 
			this.txtHeatCost11.Height = 0.125F;
			this.txtHeatCost11.Left = 0F;
			this.txtHeatCost11.Name = "txtHeatCost11";
			this.txtHeatCost11.Style = "font-size: 6pt";
			this.txtHeatCost11.Text = "1.";
			this.txtHeatCost11.Top = 2.375F;
			this.txtHeatCost11.Width = 0.625F;
			// 
			// txtHeatCost12
			// 
			this.txtHeatCost12.Height = 0.125F;
			this.txtHeatCost12.Left = 0F;
			this.txtHeatCost12.Name = "txtHeatCost12";
			this.txtHeatCost12.Style = "font-size: 6pt";
			this.txtHeatCost12.Text = "2.";
			this.txtHeatCost12.Top = 2.5F;
			this.txtHeatCost12.Width = 0.625F;
			// 
			// txtHeatCost13
			// 
			this.txtHeatCost13.Height = 0.125F;
			this.txtHeatCost13.Left = 0F;
			this.txtHeatCost13.Name = "txtHeatCost13";
			this.txtHeatCost13.Style = "font-size: 6pt";
			this.txtHeatCost13.Text = "3.";
			this.txtHeatCost13.Top = 2.625F;
			this.txtHeatCost13.Width = 0.625F;
			// 
			// txtHeatCost14
			// 
			this.txtHeatCost14.Height = 0.125F;
			this.txtHeatCost14.Left = 0F;
			this.txtHeatCost14.Name = "txtHeatCost14";
			this.txtHeatCost14.Style = "font-size: 6pt";
			this.txtHeatCost14.Text = "4.";
			this.txtHeatCost14.Top = 2.75F;
			this.txtHeatCost14.Width = 0.625F;
			// 
			// txtHeatCost15
			// 
			this.txtHeatCost15.Height = 0.125F;
			this.txtHeatCost15.Left = 0F;
			this.txtHeatCost15.Name = "txtHeatCost15";
			this.txtHeatCost15.Style = "font-size: 6pt";
			this.txtHeatCost15.Text = "4.";
			this.txtHeatCost15.Top = 2.875F;
			this.txtHeatCost15.Width = 0.625F;
			// 
			// txtHeatCost16
			// 
			this.txtHeatCost16.Height = 0.125F;
			this.txtHeatCost16.Left = 0F;
			this.txtHeatCost16.Name = "txtHeatCost16";
			this.txtHeatCost16.Style = "font-size: 6pt";
			this.txtHeatCost16.Text = "1.";
			this.txtHeatCost16.Top = 3F;
			this.txtHeatCost16.Width = 0.625F;
			// 
			// txtHeatCost17
			// 
			this.txtHeatCost17.Height = 0.125F;
			this.txtHeatCost17.Left = 0F;
			this.txtHeatCost17.Name = "txtHeatCost17";
			this.txtHeatCost17.Style = "font-size: 6pt";
			this.txtHeatCost17.Text = "2.";
			this.txtHeatCost17.Top = 3.125F;
			this.txtHeatCost17.Width = 0.625F;
			// 
			// txtHeatCost18
			// 
			this.txtHeatCost18.Height = 0.125F;
			this.txtHeatCost18.Left = 0F;
			this.txtHeatCost18.Name = "txtHeatCost18";
			this.txtHeatCost18.Style = "font-size: 6pt";
			this.txtHeatCost18.Text = "3.";
			this.txtHeatCost18.Top = 3.25F;
			this.txtHeatCost18.Width = 0.625F;
			// 
			// txtHeatCost19
			// 
			this.txtHeatCost19.Height = 0.125F;
			this.txtHeatCost19.Left = 0.6875F;
			this.txtHeatCost19.Name = "txtHeatCost19";
			this.txtHeatCost19.Style = "font-size: 6pt";
			this.txtHeatCost19.Text = "4.";
			this.txtHeatCost19.Top = 2.375F;
			this.txtHeatCost19.Width = 0.625F;
			// 
			// txtHeatCost20
			// 
			this.txtHeatCost20.Height = 0.125F;
			this.txtHeatCost20.Left = 0.6875F;
			this.txtHeatCost20.Name = "txtHeatCost20";
			this.txtHeatCost20.Style = "font-size: 6pt";
			this.txtHeatCost20.Text = "4.";
			this.txtHeatCost20.Top = 2.5F;
			this.txtHeatCost20.Width = 0.625F;
			// 
			// txtHeatCost21
			// 
			this.txtHeatCost21.Height = 0.125F;
			this.txtHeatCost21.Left = 0.6875F;
			this.txtHeatCost21.Name = "txtHeatCost21";
			this.txtHeatCost21.Style = "font-size: 6pt";
			this.txtHeatCost21.Text = "1.";
			this.txtHeatCost21.Top = 2.625F;
			this.txtHeatCost21.Width = 0.625F;
			// 
			// txtHeatCost22
			// 
			this.txtHeatCost22.Height = 0.125F;
			this.txtHeatCost22.Left = 0.6875F;
			this.txtHeatCost22.Name = "txtHeatCost22";
			this.txtHeatCost22.Style = "font-size: 6pt";
			this.txtHeatCost22.Text = "2.";
			this.txtHeatCost22.Top = 2.75F;
			this.txtHeatCost22.Width = 0.625F;
			// 
			// txtHeatCost23
			// 
			this.txtHeatCost23.Height = 0.125F;
			this.txtHeatCost23.Left = 0.6875F;
			this.txtHeatCost23.Name = "txtHeatCost23";
			this.txtHeatCost23.Style = "font-size: 6pt";
			this.txtHeatCost23.Text = "3.";
			this.txtHeatCost23.Top = 2.875F;
			this.txtHeatCost23.Width = 0.625F;
			// 
			// txtHeatCost24
			// 
			this.txtHeatCost24.Height = 0.125F;
			this.txtHeatCost24.Left = 0.6875F;
			this.txtHeatCost24.Name = "txtHeatCost24";
			this.txtHeatCost24.Style = "font-size: 6pt";
			this.txtHeatCost24.Text = "4.";
			this.txtHeatCost24.Top = 3F;
			this.txtHeatCost24.Width = 0.625F;
			// 
			// txtHeatCost25
			// 
			this.txtHeatCost25.Height = 0.125F;
			this.txtHeatCost25.Left = 0.6875F;
			this.txtHeatCost25.Name = "txtHeatCost25";
			this.txtHeatCost25.Style = "font-size: 6pt";
			this.txtHeatCost25.Text = "4.";
			this.txtHeatCost25.Top = 3.125F;
			this.txtHeatCost25.Width = 0.625F;
			// 
			// txtHeatCost26
			// 
			this.txtHeatCost26.Height = 0.125F;
			this.txtHeatCost26.Left = 0.6875F;
			this.txtHeatCost26.Name = "txtHeatCost26";
			this.txtHeatCost26.Style = "font-size: 6pt";
			this.txtHeatCost26.Text = "4.";
			this.txtHeatCost26.Top = 3.25F;
			this.txtHeatCost26.Width = 0.625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.125F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 6pt";
			this.Label11.Text = "Year Built";
			this.Label11.Top = 3.375F;
			this.Label11.Width = 0.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.125F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 6pt";
			this.Label12.Text = "Year Remodeled";
			this.Label12.Top = 3.5F;
			this.Label12.Width = 0.875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.125F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 6pt";
			this.Label13.Text = "Condition";
			this.Label13.Top = 3.625F;
			this.Label13.Width = 0.875F;
			// 
			// txtClassCost1
			// 
			this.txtClassCost1.Height = 0.125F;
			this.txtClassCost1.Left = 0F;
			this.txtClassCost1.Name = "txtClassCost1";
			this.txtClassCost1.Style = "font-size: 6pt";
			this.txtClassCost1.Text = "1.";
			this.txtClassCost1.Top = 0.375F;
			this.txtClassCost1.Width = 0.625F;
			// 
			// txtClassCost2
			// 
			this.txtClassCost2.Height = 0.125F;
			this.txtClassCost2.Left = 0F;
			this.txtClassCost2.Name = "txtClassCost2";
			this.txtClassCost2.Style = "font-size: 6pt";
			this.txtClassCost2.Text = "2.";
			this.txtClassCost2.Top = 0.5F;
			this.txtClassCost2.Width = 0.625F;
			// 
			// txtClassCost3
			// 
			this.txtClassCost3.Height = 0.125F;
			this.txtClassCost3.Left = 0F;
			this.txtClassCost3.Name = "txtClassCost3";
			this.txtClassCost3.Style = "font-size: 6pt";
			this.txtClassCost3.Text = "3.";
			this.txtClassCost3.Top = 0.625F;
			this.txtClassCost3.Width = 0.625F;
			// 
			// txtClassCost4
			// 
			this.txtClassCost4.Height = 0.125F;
			this.txtClassCost4.Left = 0F;
			this.txtClassCost4.Name = "txtClassCost4";
			this.txtClassCost4.Style = "font-size: 6pt";
			this.txtClassCost4.Text = "4.";
			this.txtClassCost4.Top = 0.75F;
			this.txtClassCost4.Width = 0.625F;
			// 
			// txtQualityCost1
			// 
			this.txtQualityCost1.Height = 0.125F;
			this.txtQualityCost1.Left = 0.6875F;
			this.txtQualityCost1.Name = "txtQualityCost1";
			this.txtQualityCost1.Style = "font-size: 6pt";
			this.txtQualityCost1.Text = "1.";
			this.txtQualityCost1.Top = 0.375F;
			this.txtQualityCost1.Width = 0.625F;
			// 
			// txtQualityCost2
			// 
			this.txtQualityCost2.Height = 0.125F;
			this.txtQualityCost2.Left = 0.6875F;
			this.txtQualityCost2.Name = "txtQualityCost2";
			this.txtQualityCost2.Style = "font-size: 6pt";
			this.txtQualityCost2.Text = "2.";
			this.txtQualityCost2.Top = 0.5F;
			this.txtQualityCost2.Width = 0.625F;
			// 
			// txtQualityCost3
			// 
			this.txtQualityCost3.Height = 0.125F;
			this.txtQualityCost3.Left = 0.6875F;
			this.txtQualityCost3.Name = "txtQualityCost3";
			this.txtQualityCost3.Style = "font-size: 6pt";
			this.txtQualityCost3.Text = "3.";
			this.txtQualityCost3.Top = 0.625F;
			this.txtQualityCost3.Width = 0.625F;
			// 
			// txtQualityCost4
			// 
			this.txtQualityCost4.Height = 0.125F;
			this.txtQualityCost4.Left = 0.6875F;
			this.txtQualityCost4.Name = "txtQualityCost4";
			this.txtQualityCost4.Style = "font-size: 6pt";
			this.txtQualityCost4.Text = "4.";
			this.txtQualityCost4.Top = 0.75F;
			this.txtQualityCost4.Width = 0.625F;
			// 
			// txtExteriorCost6
			// 
			this.txtExteriorCost6.Height = 0.125F;
			this.txtExteriorCost6.Left = 0.6875F;
			this.txtExteriorCost6.Name = "txtExteriorCost6";
			this.txtExteriorCost6.Style = "font-size: 6pt";
			this.txtExteriorCost6.Text = "1.";
			this.txtExteriorCost6.Top = 1.25F;
			this.txtExteriorCost6.Width = 0.625F;
			// 
			// txtExteriorCost7
			// 
			this.txtExteriorCost7.Height = 0.125F;
			this.txtExteriorCost7.Left = 0.6875F;
			this.txtExteriorCost7.Name = "txtExteriorCost7";
			this.txtExteriorCost7.Style = "font-size: 6pt";
			this.txtExteriorCost7.Text = "2.";
			this.txtExteriorCost7.Top = 1.375F;
			this.txtExteriorCost7.Width = 0.625F;
			// 
			// txtExteriorCost8
			// 
			this.txtExteriorCost8.Height = 0.125F;
			this.txtExteriorCost8.Left = 0.6875F;
			this.txtExteriorCost8.Name = "txtExteriorCost8";
			this.txtExteriorCost8.Style = "font-size: 6pt";
			this.txtExteriorCost8.Text = "3.";
			this.txtExteriorCost8.Top = 1.5F;
			this.txtExteriorCost8.Width = 0.625F;
			// 
			// txtExteriorCost9
			// 
			this.txtExteriorCost9.Height = 0.125F;
			this.txtExteriorCost9.Left = 0.6875F;
			this.txtExteriorCost9.Name = "txtExteriorCost9";
			this.txtExteriorCost9.Style = "font-size: 6pt";
			this.txtExteriorCost9.Text = "4.";
			this.txtExteriorCost9.Top = 1.625F;
			this.txtExteriorCost9.Width = 0.625F;
			// 
			// txtConditionCost1
			// 
			this.txtConditionCost1.Height = 0.125F;
			this.txtConditionCost1.Left = 0F;
			this.txtConditionCost1.Name = "txtConditionCost1";
			this.txtConditionCost1.Style = "font-size: 6pt";
			this.txtConditionCost1.Text = "1.";
			this.txtConditionCost1.Top = 3.75F;
			this.txtConditionCost1.Width = 0.625F;
			// 
			// txtConditionCost2
			// 
			this.txtConditionCost2.Height = 0.125F;
			this.txtConditionCost2.Left = 0F;
			this.txtConditionCost2.Name = "txtConditionCost2";
			this.txtConditionCost2.Style = "font-size: 6pt";
			this.txtConditionCost2.Text = "2.";
			this.txtConditionCost2.Top = 3.875F;
			this.txtConditionCost2.Width = 0.625F;
			// 
			// txtConditionCost3
			// 
			this.txtConditionCost3.Height = 0.125F;
			this.txtConditionCost3.Left = 0F;
			this.txtConditionCost3.Name = "txtConditionCost3";
			this.txtConditionCost3.Style = "font-size: 6pt";
			this.txtConditionCost3.Text = "3.";
			this.txtConditionCost3.Top = 4F;
			this.txtConditionCost3.Width = 0.625F;
			// 
			// txtConditionCost6
			// 
			this.txtConditionCost6.Height = 0.125F;
			this.txtConditionCost6.Left = 0.6875F;
			this.txtConditionCost6.Name = "txtConditionCost6";
			this.txtConditionCost6.Style = "font-size: 6pt";
			this.txtConditionCost6.Text = "1.";
			this.txtConditionCost6.Top = 3.75F;
			this.txtConditionCost6.Width = 0.625F;
			// 
			// txtConditionCost7
			// 
			this.txtConditionCost7.Height = 0.125F;
			this.txtConditionCost7.Left = 0.6875F;
			this.txtConditionCost7.Name = "txtConditionCost7";
			this.txtConditionCost7.Style = "font-size: 6pt";
			this.txtConditionCost7.Text = "2.";
			this.txtConditionCost7.Top = 3.875F;
			this.txtConditionCost7.Width = 0.625F;
			// 
			// txtConditionCost8
			// 
			this.txtConditionCost8.Height = 0.125F;
			this.txtConditionCost8.Left = 0.6875F;
			this.txtConditionCost8.Name = "txtConditionCost8";
			this.txtConditionCost8.Style = "font-size: 6pt";
			this.txtConditionCost8.Text = "3.";
			this.txtConditionCost8.Top = 4F;
			this.txtConditionCost8.Width = 0.625F;
			// 
			// txtConditionCost4
			// 
			this.txtConditionCost4.Height = 0.125F;
			this.txtConditionCost4.Left = 0F;
			this.txtConditionCost4.Name = "txtConditionCost4";
			this.txtConditionCost4.Style = "font-size: 6pt";
			this.txtConditionCost4.Text = "4.";
			this.txtConditionCost4.Top = 4.125F;
			this.txtConditionCost4.Width = 0.625F;
			// 
			// txtConditionCost9
			// 
			this.txtConditionCost9.Height = 0.125F;
			this.txtConditionCost9.Left = 0.6875F;
			this.txtConditionCost9.Name = "txtConditionCost9";
			this.txtConditionCost9.Style = "font-size: 6pt";
			this.txtConditionCost9.Text = "4.";
			this.txtConditionCost9.Top = 4.125F;
			this.txtConditionCost9.Width = 0.625F;
			// 
			// txtConditionCost5
			// 
			this.txtConditionCost5.Height = 0.125F;
			this.txtConditionCost5.Left = 0F;
			this.txtConditionCost5.Name = "txtConditionCost5";
			this.txtConditionCost5.Style = "font-size: 6pt";
			this.txtConditionCost5.Text = "4.";
			this.txtConditionCost5.Top = 4.25F;
			this.txtConditionCost5.Width = 0.625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.125F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 6pt";
			this.Label14.Text = "Physical % Good";
			this.Label14.Top = 4.375F;
			this.Label14.Width = 0.875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.125F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 6pt";
			this.Label15.Text = "Functional % Good";
			this.Label15.Top = 4.5F;
			this.Label15.Width = 0.875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.125F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 6pt";
			this.Label16.Text = "Economic % Good";
			this.Label16.Top = 4.625F;
			this.Label16.Width = 0.875F;
			// 
			// Line2
			// 
			this.Line2.Height = 4.625F;
			this.Line2.Left = 2.4375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 0F;
			this.Line2.X1 = 2.4375F;
			this.Line2.X2 = 2.4375F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 4.625F;
			// 
			// txtOccupancy2
			// 
			this.txtOccupancy2.CanGrow = false;
			this.txtOccupancy2.Height = 0.125F;
			this.txtOccupancy2.Left = 2.5F;
			this.txtOccupancy2.MultiLine = false;
			this.txtOccupancy2.Name = "txtOccupancy2";
			this.txtOccupancy2.Style = "font-size: 6pt";
			this.txtOccupancy2.Text = null;
			this.txtOccupancy2.Top = 0F;
			this.txtOccupancy2.Width = 1.125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.125F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 6pt";
			this.Label17.Text = "Entrance Code";
			this.Label17.Top = 3F;
			this.Label17.Width = 0.875F;
			// 
			// txtEntranceCost1
			// 
			this.txtEntranceCost1.Height = 0.125F;
			this.txtEntranceCost1.Left = 3.625F;
			this.txtEntranceCost1.Name = "txtEntranceCost1";
			this.txtEntranceCost1.Style = "font-size: 6pt";
			this.txtEntranceCost1.Text = "1.";
			this.txtEntranceCost1.Top = 3.25F;
			this.txtEntranceCost1.Width = 0.6875F;
			// 
			// txtEntranceCost2
			// 
			this.txtEntranceCost2.Height = 0.125F;
			this.txtEntranceCost2.Left = 3.625F;
			this.txtEntranceCost2.Name = "txtEntranceCost2";
			this.txtEntranceCost2.Style = "font-size: 6pt";
			this.txtEntranceCost2.Text = "2.";
			this.txtEntranceCost2.Top = 3.375F;
			this.txtEntranceCost2.Width = 0.6875F;
			// 
			// txtEntranceCost3
			// 
			this.txtEntranceCost3.Height = 0.125F;
			this.txtEntranceCost3.Left = 3.625F;
			this.txtEntranceCost3.Name = "txtEntranceCost3";
			this.txtEntranceCost3.Style = "font-size: 6pt";
			this.txtEntranceCost3.Text = "3.";
			this.txtEntranceCost3.Top = 3.5F;
			this.txtEntranceCost3.Width = 0.6875F;
			// 
			// txtEntranceCost6
			// 
			this.txtEntranceCost6.Height = 0.125F;
			this.txtEntranceCost6.Left = 4.375F;
			this.txtEntranceCost6.Name = "txtEntranceCost6";
			this.txtEntranceCost6.Style = "font-size: 6pt";
			this.txtEntranceCost6.Text = "1.";
			this.txtEntranceCost6.Top = 3.25F;
			this.txtEntranceCost6.Width = 0.75F;
			// 
			// txtEntranceCost7
			// 
			this.txtEntranceCost7.Height = 0.125F;
			this.txtEntranceCost7.Left = 4.375F;
			this.txtEntranceCost7.Name = "txtEntranceCost7";
			this.txtEntranceCost7.Style = "font-size: 6pt";
			this.txtEntranceCost7.Text = "2.";
			this.txtEntranceCost7.Top = 3.375F;
			this.txtEntranceCost7.Width = 0.75F;
			// 
			// txtEntranceCost8
			// 
			this.txtEntranceCost8.Height = 0.125F;
			this.txtEntranceCost8.Left = 4.375F;
			this.txtEntranceCost8.Name = "txtEntranceCost8";
			this.txtEntranceCost8.Style = "font-size: 6pt";
			this.txtEntranceCost8.Text = "3.";
			this.txtEntranceCost8.Top = 3.5F;
			this.txtEntranceCost8.Width = 0.75F;
			// 
			// txtEntranceCost4
			// 
			this.txtEntranceCost4.Height = 0.125F;
			this.txtEntranceCost4.Left = 3.625F;
			this.txtEntranceCost4.Name = "txtEntranceCost4";
			this.txtEntranceCost4.Style = "font-size: 6pt";
			this.txtEntranceCost4.Text = "4.";
			this.txtEntranceCost4.Top = 3.625F;
			this.txtEntranceCost4.Width = 0.6875F;
			// 
			// txtEntranceCost5
			// 
			this.txtEntranceCost5.Height = 0.125F;
			this.txtEntranceCost5.Left = 3.625F;
			this.txtEntranceCost5.Name = "txtEntranceCost5";
			this.txtEntranceCost5.Style = "font-size: 6pt";
			this.txtEntranceCost5.Text = "4.";
			this.txtEntranceCost5.Top = 3.75F;
			this.txtEntranceCost5.Width = 0.6875F;
			// 
			// txtEntranceCost9
			// 
			this.txtEntranceCost9.Height = 0.125F;
			this.txtEntranceCost9.Left = 4.375F;
			this.txtEntranceCost9.Name = "txtEntranceCost9";
			this.txtEntranceCost9.Style = "font-size: 6pt";
			this.txtEntranceCost9.Text = "4.";
			this.txtEntranceCost9.Top = 3.625F;
			this.txtEntranceCost9.Width = 0.75F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.125F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 6pt";
			this.Label18.Text = "Information Code";
			this.Label18.Top = 3.875F;
			this.Label18.Width = 0.875F;
			// 
			// txtInformationCost1
			// 
			this.txtInformationCost1.Height = 0.125F;
			this.txtInformationCost1.Left = 3.625F;
			this.txtInformationCost1.Name = "txtInformationCost1";
			this.txtInformationCost1.Style = "font-size: 6pt";
			this.txtInformationCost1.Text = "1.";
			this.txtInformationCost1.Top = 4.125F;
			this.txtInformationCost1.Width = 0.6875F;
			// 
			// txtInformationCost2
			// 
			this.txtInformationCost2.Height = 0.125F;
			this.txtInformationCost2.Left = 3.625F;
			this.txtInformationCost2.Name = "txtInformationCost2";
			this.txtInformationCost2.Style = "font-size: 6pt";
			this.txtInformationCost2.Text = "2.";
			this.txtInformationCost2.Top = 4.25F;
			this.txtInformationCost2.Width = 0.6875F;
			// 
			// txtInformationCost3
			// 
			this.txtInformationCost3.Height = 0.125F;
			this.txtInformationCost3.Left = 3.625F;
			this.txtInformationCost3.Name = "txtInformationCost3";
			this.txtInformationCost3.Style = "font-size: 6pt";
			this.txtInformationCost3.Text = "3.";
			this.txtInformationCost3.Top = 4.375F;
			this.txtInformationCost3.Width = 0.6875F;
			// 
			// txtInformationCost6
			// 
			this.txtInformationCost6.Height = 0.125F;
			this.txtInformationCost6.Left = 4.375F;
			this.txtInformationCost6.Name = "txtInformationCost6";
			this.txtInformationCost6.Style = "font-size: 6pt";
			this.txtInformationCost6.Text = "1.";
			this.txtInformationCost6.Top = 4.125F;
			this.txtInformationCost6.Width = 0.75F;
			// 
			// txtInformationCost7
			// 
			this.txtInformationCost7.Height = 0.125F;
			this.txtInformationCost7.Left = 4.375F;
			this.txtInformationCost7.Name = "txtInformationCost7";
			this.txtInformationCost7.Style = "font-size: 6pt";
			this.txtInformationCost7.Text = "2.";
			this.txtInformationCost7.Top = 4.25F;
			this.txtInformationCost7.Width = 0.75F;
			// 
			// txtInformationCost8
			// 
			this.txtInformationCost8.Height = 0.125F;
			this.txtInformationCost8.Left = 4.375F;
			this.txtInformationCost8.Name = "txtInformationCost8";
			this.txtInformationCost8.Style = "font-size: 6pt";
			this.txtInformationCost8.Text = "3.";
			this.txtInformationCost8.Top = 4.375F;
			this.txtInformationCost8.Width = 0.75F;
			// 
			// txtInformationCost4
			// 
			this.txtInformationCost4.Height = 0.125F;
			this.txtInformationCost4.Left = 3.625F;
			this.txtInformationCost4.Name = "txtInformationCost4";
			this.txtInformationCost4.Style = "font-size: 6pt";
			this.txtInformationCost4.Text = "4.";
			this.txtInformationCost4.Top = 4.5F;
			this.txtInformationCost4.Width = 0.6875F;
			// 
			// txtInformationCost5
			// 
			this.txtInformationCost5.Height = 0.125F;
			this.txtInformationCost5.Left = 3.625F;
			this.txtInformationCost5.Name = "txtInformationCost5";
			this.txtInformationCost5.Style = "font-size: 6pt";
			this.txtInformationCost5.Text = "4.";
			this.txtInformationCost5.Top = 4.625F;
			this.txtInformationCost5.Width = 0.6875F;
			// 
			// txtInformationCost9
			// 
			this.txtInformationCost9.Height = 0.125F;
			this.txtInformationCost9.Left = 4.375F;
			this.txtInformationCost9.Name = "txtInformationCost9";
			this.txtInformationCost9.Style = "font-size: 6pt";
			this.txtInformationCost9.Text = "4.";
			this.txtInformationCost9.Top = 4.5F;
			this.txtInformationCost9.Width = 0.75F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.19F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 1.3125F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-size: 8.5pt";
			this.Label43.Text = "Date Inspected";
			this.Label43.Top = 4.96875F;
			this.Label43.Width = 0.9375F;
			// 
			// txtDateInspected
			// 
			this.txtDateInspected.Height = 0.19F;
			this.txtDateInspected.Left = 2.25F;
			this.txtDateInspected.Name = "txtDateInspected";
			this.txtDateInspected.Text = null;
			this.txtDateInspected.Top = 4.96875F;
			this.txtDateInspected.Width = 1.0625F;
			// 
			// txtUnits1
			// 
			this.txtUnits1.CanGrow = false;
			this.txtUnits1.Height = 0.125F;
			this.txtUnits1.Left = 1.3125F;
			this.txtUnits1.MultiLine = false;
			this.txtUnits1.Name = "txtUnits1";
			this.txtUnits1.Style = "font-size: 6pt";
			this.txtUnits1.Text = null;
			this.txtUnits1.Top = 0.125F;
			this.txtUnits1.Width = 1.125F;
			// 
			// txtUnits2
			// 
			this.txtUnits2.CanGrow = false;
			this.txtUnits2.Height = 0.125F;
			this.txtUnits2.Left = 2.5F;
			this.txtUnits2.MultiLine = false;
			this.txtUnits2.Name = "txtUnits2";
			this.txtUnits2.Style = "font-size: 6pt";
			this.txtUnits2.Text = null;
			this.txtUnits2.Top = 0.125F;
			this.txtUnits2.Width = 1.125F;
			// 
			// txtClass1
			// 
			this.txtClass1.CanGrow = false;
			this.txtClass1.Height = 0.125F;
			this.txtClass1.Left = 1.3125F;
			this.txtClass1.MultiLine = false;
			this.txtClass1.Name = "txtClass1";
			this.txtClass1.Style = "font-size: 6pt";
			this.txtClass1.Text = null;
			this.txtClass1.Top = 0.25F;
			this.txtClass1.Width = 1.125F;
			// 
			// txtClass2
			// 
			this.txtClass2.CanGrow = false;
			this.txtClass2.Height = 0.125F;
			this.txtClass2.Left = 2.5F;
			this.txtClass2.MultiLine = false;
			this.txtClass2.Name = "txtClass2";
			this.txtClass2.Style = "font-size: 6pt";
			this.txtClass2.Text = null;
			this.txtClass2.Top = 0.25F;
			this.txtClass2.Width = 1.125F;
			// 
			// txtGradeFactor1
			// 
			this.txtGradeFactor1.CanGrow = false;
			this.txtGradeFactor1.Height = 0.125F;
			this.txtGradeFactor1.Left = 1.3125F;
			this.txtGradeFactor1.MultiLine = false;
			this.txtGradeFactor1.Name = "txtGradeFactor1";
			this.txtGradeFactor1.Style = "font-size: 6pt";
			this.txtGradeFactor1.Text = null;
			this.txtGradeFactor1.Top = 1F;
			this.txtGradeFactor1.Width = 1.125F;
			// 
			// txtGradeFactor2
			// 
			this.txtGradeFactor2.CanGrow = false;
			this.txtGradeFactor2.Height = 0.125F;
			this.txtGradeFactor2.Left = 2.5F;
			this.txtGradeFactor2.MultiLine = false;
			this.txtGradeFactor2.Name = "txtGradeFactor2";
			this.txtGradeFactor2.Style = "font-size: 6pt";
			this.txtGradeFactor2.Text = null;
			this.txtGradeFactor2.Top = 1F;
			this.txtGradeFactor2.Width = 1.125F;
			// 
			// txtExterior1
			// 
			this.txtExterior1.CanGrow = false;
			this.txtExterior1.Height = 0.125F;
			this.txtExterior1.Left = 1.3125F;
			this.txtExterior1.MultiLine = false;
			this.txtExterior1.Name = "txtExterior1";
			this.txtExterior1.Style = "font-size: 6pt";
			this.txtExterior1.Text = null;
			this.txtExterior1.Top = 1.125F;
			this.txtExterior1.Width = 1.125F;
			// 
			// txtExterior2
			// 
			this.txtExterior2.CanGrow = false;
			this.txtExterior2.Height = 0.125F;
			this.txtExterior2.Left = 2.5F;
			this.txtExterior2.MultiLine = false;
			this.txtExterior2.Name = "txtExterior2";
			this.txtExterior2.Style = "font-size: 6pt";
			this.txtExterior2.Text = null;
			this.txtExterior2.Top = 1.125F;
			this.txtExterior2.Width = 1.125F;
			// 
			// txtStories1
			// 
			this.txtStories1.CanGrow = false;
			this.txtStories1.Height = 0.125F;
			this.txtStories1.Left = 1.3125F;
			this.txtStories1.MultiLine = false;
			this.txtStories1.Name = "txtStories1";
			this.txtStories1.Style = "font-size: 6pt";
			this.txtStories1.Text = null;
			this.txtStories1.Top = 1.875F;
			this.txtStories1.Width = 0.5625F;
			// 
			// txtStories2
			// 
			this.txtStories2.CanGrow = false;
			this.txtStories2.Height = 0.125F;
			this.txtStories2.Left = 2.5F;
			this.txtStories2.MultiLine = false;
			this.txtStories2.Name = "txtStories2";
			this.txtStories2.Style = "font-size: 6pt";
			this.txtStories2.Text = null;
			this.txtStories2.Top = 1.875F;
			this.txtStories2.Width = 0.5625F;
			// 
			// txtArea1
			// 
			this.txtArea1.CanGrow = false;
			this.txtArea1.Height = 0.125F;
			this.txtArea1.Left = 1.3125F;
			this.txtArea1.MultiLine = false;
			this.txtArea1.Name = "txtArea1";
			this.txtArea1.Style = "font-size: 6pt";
			this.txtArea1.Text = null;
			this.txtArea1.Top = 2F;
			this.txtArea1.Width = 1.125F;
			// 
			// txtArea2
			// 
			this.txtArea2.CanGrow = false;
			this.txtArea2.Height = 0.125F;
			this.txtArea2.Left = 2.5F;
			this.txtArea2.MultiLine = false;
			this.txtArea2.Name = "txtArea2";
			this.txtArea2.Style = "font-size: 6pt";
			this.txtArea2.Text = null;
			this.txtArea2.Top = 2F;
			this.txtArea2.Width = 1.125F;
			// 
			// txtPerimeter1
			// 
			this.txtPerimeter1.CanGrow = false;
			this.txtPerimeter1.Height = 0.125F;
			this.txtPerimeter1.Left = 1.3125F;
			this.txtPerimeter1.MultiLine = false;
			this.txtPerimeter1.Name = "txtPerimeter1";
			this.txtPerimeter1.Style = "font-size: 6pt";
			this.txtPerimeter1.Text = null;
			this.txtPerimeter1.Top = 2.125F;
			this.txtPerimeter1.Width = 1.125F;
			// 
			// txtPerimeter2
			// 
			this.txtPerimeter2.CanGrow = false;
			this.txtPerimeter2.Height = 0.125F;
			this.txtPerimeter2.Left = 2.5F;
			this.txtPerimeter2.MultiLine = false;
			this.txtPerimeter2.Name = "txtPerimeter2";
			this.txtPerimeter2.Style = "font-size: 6pt";
			this.txtPerimeter2.Text = null;
			this.txtPerimeter2.Top = 2.125F;
			this.txtPerimeter2.Width = 1.125F;
			// 
			// txtHeatCool1
			// 
			this.txtHeatCool1.CanGrow = false;
			this.txtHeatCool1.Height = 0.125F;
			this.txtHeatCool1.Left = 1.3125F;
			this.txtHeatCool1.MultiLine = false;
			this.txtHeatCool1.Name = "txtHeatCool1";
			this.txtHeatCool1.Style = "font-size: 6pt";
			this.txtHeatCool1.Text = null;
			this.txtHeatCool1.Top = 2.25F;
			this.txtHeatCool1.Width = 1.125F;
			// 
			// txtHeatCool2
			// 
			this.txtHeatCool2.CanGrow = false;
			this.txtHeatCool2.Height = 0.125F;
			this.txtHeatCool2.Left = 2.5F;
			this.txtHeatCool2.MultiLine = false;
			this.txtHeatCool2.Name = "txtHeatCool2";
			this.txtHeatCool2.Style = "font-size: 6pt";
			this.txtHeatCool2.Text = null;
			this.txtHeatCool2.Top = 2.25F;
			this.txtHeatCool2.Width = 1.125F;
			// 
			// txtYearBuilt1
			// 
			this.txtYearBuilt1.CanGrow = false;
			this.txtYearBuilt1.Height = 0.125F;
			this.txtYearBuilt1.Left = 1.3125F;
			this.txtYearBuilt1.MultiLine = false;
			this.txtYearBuilt1.Name = "txtYearBuilt1";
			this.txtYearBuilt1.Style = "font-size: 6pt";
			this.txtYearBuilt1.Text = null;
			this.txtYearBuilt1.Top = 3.375F;
			this.txtYearBuilt1.Width = 1.125F;
			// 
			// txtYearBuilt2
			// 
			this.txtYearBuilt2.CanGrow = false;
			this.txtYearBuilt2.Height = 0.125F;
			this.txtYearBuilt2.Left = 2.5F;
			this.txtYearBuilt2.MultiLine = false;
			this.txtYearBuilt2.Name = "txtYearBuilt2";
			this.txtYearBuilt2.Style = "font-size: 6pt";
			this.txtYearBuilt2.Text = null;
			this.txtYearBuilt2.Top = 3.375F;
			this.txtYearBuilt2.Width = 1.125F;
			// 
			// txtRemodeled1
			// 
			this.txtRemodeled1.CanGrow = false;
			this.txtRemodeled1.Height = 0.125F;
			this.txtRemodeled1.Left = 1.3125F;
			this.txtRemodeled1.MultiLine = false;
			this.txtRemodeled1.Name = "txtRemodeled1";
			this.txtRemodeled1.Style = "font-size: 6pt";
			this.txtRemodeled1.Text = null;
			this.txtRemodeled1.Top = 3.5F;
			this.txtRemodeled1.Width = 1.125F;
			// 
			// txtRemodeled2
			// 
			this.txtRemodeled2.CanGrow = false;
			this.txtRemodeled2.Height = 0.125F;
			this.txtRemodeled2.Left = 2.5F;
			this.txtRemodeled2.MultiLine = false;
			this.txtRemodeled2.Name = "txtRemodeled2";
			this.txtRemodeled2.Style = "font-size: 6pt";
			this.txtRemodeled2.Text = null;
			this.txtRemodeled2.Top = 3.5F;
			this.txtRemodeled2.Width = 1.125F;
			// 
			// txtCondition1
			// 
			this.txtCondition1.CanGrow = false;
			this.txtCondition1.Height = 0.125F;
			this.txtCondition1.Left = 1.3125F;
			this.txtCondition1.MultiLine = false;
			this.txtCondition1.Name = "txtCondition1";
			this.txtCondition1.Style = "font-size: 6pt";
			this.txtCondition1.Text = null;
			this.txtCondition1.Top = 3.625F;
			this.txtCondition1.Width = 1.125F;
			// 
			// txtCondition2
			// 
			this.txtCondition2.CanGrow = false;
			this.txtCondition2.Height = 0.125F;
			this.txtCondition2.Left = 2.5F;
			this.txtCondition2.MultiLine = false;
			this.txtCondition2.Name = "txtCondition2";
			this.txtCondition2.Style = "font-size: 6pt";
			this.txtCondition2.Text = null;
			this.txtCondition2.Top = 3.625F;
			this.txtCondition2.Width = 1.125F;
			// 
			// txtPhysical1
			// 
			this.txtPhysical1.CanGrow = false;
			this.txtPhysical1.Height = 0.125F;
			this.txtPhysical1.Left = 1.3125F;
			this.txtPhysical1.MultiLine = false;
			this.txtPhysical1.Name = "txtPhysical1";
			this.txtPhysical1.Style = "font-size: 6pt";
			this.txtPhysical1.Text = null;
			this.txtPhysical1.Top = 4.375F;
			this.txtPhysical1.Width = 1.125F;
			// 
			// txtPhysical2
			// 
			this.txtPhysical2.CanGrow = false;
			this.txtPhysical2.Height = 0.125F;
			this.txtPhysical2.Left = 2.5F;
			this.txtPhysical2.MultiLine = false;
			this.txtPhysical2.Name = "txtPhysical2";
			this.txtPhysical2.Style = "font-size: 6pt";
			this.txtPhysical2.Text = null;
			this.txtPhysical2.Top = 4.375F;
			this.txtPhysical2.Width = 1.125F;
			// 
			// txtFunctional1
			// 
			this.txtFunctional1.CanGrow = false;
			this.txtFunctional1.Height = 0.125F;
			this.txtFunctional1.Left = 1.3125F;
			this.txtFunctional1.MultiLine = false;
			this.txtFunctional1.Name = "txtFunctional1";
			this.txtFunctional1.Style = "font-size: 6pt";
			this.txtFunctional1.Text = null;
			this.txtFunctional1.Top = 4.5F;
			this.txtFunctional1.Width = 1.125F;
			// 
			// txtFunctional2
			// 
			this.txtFunctional2.CanGrow = false;
			this.txtFunctional2.Height = 0.125F;
			this.txtFunctional2.Left = 2.5F;
			this.txtFunctional2.MultiLine = false;
			this.txtFunctional2.Name = "txtFunctional2";
			this.txtFunctional2.Style = "font-size: 6pt";
			this.txtFunctional2.Text = null;
			this.txtFunctional2.Top = 4.5F;
			this.txtFunctional2.Width = 1.125F;
			// 
			// txtEconomic
			// 
			this.txtEconomic.CanGrow = false;
			this.txtEconomic.Height = 0.125F;
			this.txtEconomic.Left = 1.875F;
			this.txtEconomic.MultiLine = false;
			this.txtEconomic.Name = "txtEconomic";
			this.txtEconomic.Style = "font-size: 6pt; text-align: center";
			this.txtEconomic.Text = null;
			this.txtEconomic.Top = 4.625F;
			this.txtEconomic.Width = 1.125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.125F;
			this.Line4.Width = 3.625F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 3.625F;
			this.Line4.Y1 = 0.125F;
			this.Line4.Y2 = 0.125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.25F;
			this.Line5.Width = 3.625F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 3.625F;
			this.Line5.Y1 = 0.25F;
			this.Line5.Y2 = 0.25F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1F;
			this.Line6.Width = 3.625F;
			this.Line6.X1 = 0F;
			this.Line6.X2 = 3.625F;
			this.Line6.Y1 = 1F;
			this.Line6.Y2 = 1F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.125F;
			this.Line8.Width = 3.625F;
			this.Line8.X1 = 0F;
			this.Line8.X2 = 3.625F;
			this.Line8.Y1 = 1.125F;
			this.Line8.Y2 = 1.125F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 0F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 2.125F;
			this.Line11.Width = 3.625F;
			this.Line11.X1 = 0F;
			this.Line11.X2 = 3.625F;
			this.Line11.Y1 = 2.125F;
			this.Line11.Y2 = 2.125F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 2.25F;
			this.Line12.Width = 3.625F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 3.625F;
			this.Line12.Y1 = 2.25F;
			this.Line12.Y2 = 2.25F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 0F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 3.5F;
			this.Line14.Width = 3.625F;
			this.Line14.X1 = 0F;
			this.Line14.X2 = 3.625F;
			this.Line14.Y1 = 3.5F;
			this.Line14.Y2 = 3.5F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 3.375F;
			this.Line13.Width = 3.625F;
			this.Line13.X1 = 0F;
			this.Line13.X2 = 3.625F;
			this.Line13.Y1 = 3.375F;
			this.Line13.Y2 = 3.375F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 0F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 3.625F;
			this.Line15.Width = 3.625F;
			this.Line15.X1 = 0F;
			this.Line15.X2 = 3.625F;
			this.Line15.Y1 = 3.625F;
			this.Line15.Y2 = 3.625F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 4.375F;
			this.Line16.Width = 3.625F;
			this.Line16.X1 = 0F;
			this.Line16.X2 = 3.625F;
			this.Line16.Y1 = 4.375F;
			this.Line16.Y2 = 4.375F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 4.5F;
			this.Line17.Width = 3.625F;
			this.Line17.X1 = 0F;
			this.Line17.X2 = 3.625F;
			this.Line17.Y1 = 4.5F;
			this.Line17.Y2 = 4.5F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 0F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 4.625F;
			this.Line18.Width = 3.625F;
			this.Line18.X1 = 0F;
			this.Line18.X2 = 3.625F;
			this.Line18.Y1 = 4.625F;
			this.Line18.Y2 = 4.625F;
			// 
			// txtQuality1
			// 
			this.txtQuality1.CanGrow = false;
			this.txtQuality1.Height = 0.125F;
			this.txtQuality1.Left = 1.3125F;
			this.txtQuality1.MultiLine = false;
			this.txtQuality1.Name = "txtQuality1";
			this.txtQuality1.Style = "font-size: 6pt";
			this.txtQuality1.Text = null;
			this.txtQuality1.Top = 0.375F;
			this.txtQuality1.Width = 1.125F;
			// 
			// txtQuality2
			// 
			this.txtQuality2.CanGrow = false;
			this.txtQuality2.Height = 0.125F;
			this.txtQuality2.Left = 2.5F;
			this.txtQuality2.MultiLine = false;
			this.txtQuality2.Name = "txtQuality2";
			this.txtQuality2.Style = "font-size: 6pt";
			this.txtQuality2.Text = null;
			this.txtQuality2.Top = 0.375F;
			this.txtQuality2.Width = 1.125F;
			// 
			// Line1
			// 
			this.Line1.Height = 4.75F;
			this.Line1.Left = 1.3125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 0F;
			this.Line1.X1 = 1.3125F;
			this.Line1.X2 = 1.3125F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 4.75F;
			// 
			// txtHeight1
			// 
			this.txtHeight1.CanGrow = false;
			this.txtHeight1.Height = 0.125F;
			this.txtHeight1.Left = 1.875F;
			this.txtHeight1.MultiLine = false;
			this.txtHeight1.Name = "txtHeight1";
			this.txtHeight1.Style = "font-size: 6pt";
			this.txtHeight1.Text = null;
			this.txtHeight1.Top = 1.875F;
			this.txtHeight1.Width = 0.5625F;
			// 
			// txtHeight2
			// 
			this.txtHeight2.CanGrow = false;
			this.txtHeight2.Height = 0.125F;
			this.txtHeight2.Left = 3.0625F;
			this.txtHeight2.MultiLine = false;
			this.txtHeight2.Name = "txtHeight2";
			this.txtHeight2.Style = "font-size: 6pt";
			this.txtHeight2.Text = null;
			this.txtHeight2.Top = 1.875F;
			this.txtHeight2.Width = 0.5625F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 0F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 1.875F;
			this.Line9.Width = 3.625F;
			this.Line9.X1 = 0F;
			this.Line9.X2 = 3.625F;
			this.Line9.Y1 = 1.875F;
			this.Line9.Y2 = 1.875F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 0F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 2F;
			this.Line10.Width = 3.625F;
			this.Line10.X1 = 0F;
			this.Line10.X2 = 3.625F;
			this.Line10.Y1 = 2F;
			this.Line10.Y2 = 2F;
			// 
			// txtEntrance
			// 
			this.txtEntrance.Height = 0.125F;
			this.txtEntrance.Left = 3.9375F;
			this.txtEntrance.Name = "txtEntrance";
			this.txtEntrance.Style = "font-size: 6pt; font-weight: bold";
			this.txtEntrance.Text = "1.";
			this.txtEntrance.Top = 3.125F;
			this.txtEntrance.Width = 1.25F;
			// 
			// txtInformation
			// 
			this.txtInformation.Height = 0.125F;
			this.txtInformation.Left = 3.9375F;
			this.txtInformation.Name = "txtInformation";
			this.txtInformation.Style = "font-size: 6pt; font-weight: bold";
			this.txtInformation.Text = "1.";
			this.txtInformation.Top = 4F;
			this.txtInformation.Width = 1.25F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 0F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 4.75F;
			this.Line19.Width = 5.25F;
			this.Line19.X1 = 0F;
			this.Line19.X2 = 5.25F;
			this.Line19.Y1 = 4.75F;
			this.Line19.Y2 = 4.75F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 3.625F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 3.875F;
			this.Line20.Width = 1.625F;
			this.Line20.X1 = 3.625F;
			this.Line20.X2 = 5.25F;
			this.Line20.Y1 = 3.875F;
			this.Line20.Y2 = 3.875F;
			// 
			// Line21
			// 
			this.Line21.Height = 0F;
			this.Line21.Left = 3.625F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 3F;
			this.Line21.Width = 1.625F;
			this.Line21.X1 = 3.625F;
			this.Line21.X2 = 5.25F;
			this.Line21.Y1 = 3F;
			this.Line21.Y2 = 3F;
			// 
			// Line3
			// 
			this.Line3.Height = 4.75F;
			this.Line3.Left = 3.625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 0F;
			this.Line3.X1 = 3.625F;
			this.Line3.X2 = 3.625F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 4.75F;
			// 
			// Image1
			// 
			this.Image1.Height = 1.5F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 3.6875F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image1.Top = 0.75F;
			this.Image1.Width = 1.5F;
			// 
			// srptCommercialCard
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 8.5F;
			this.PageSettings.PaperWidth = 11F;
			this.PrintWidth = 5.25F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOccupancy1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClassCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQualityCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConditionCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOccupancy2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateInspected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeFactor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGradeFactor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExterior1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExterior2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArea1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArea2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerimeter1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerimeter2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconomic)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuality1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuality2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntrance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOccupancy1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClassCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClassCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClassCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClassCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClassCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualityCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualityCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualityCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualityCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtConditionCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOccupancy2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCost9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCost9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateInspected;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClass1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClass2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeFactor1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGradeFactor2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExterior1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExterior2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStories1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStories2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtArea1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtArea2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPerimeter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPerimeter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCool1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCool2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearBuilt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearBuilt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemodeled1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemodeled2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconomic;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuality1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuality2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeight1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeight2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntrance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformation;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
	}
}
