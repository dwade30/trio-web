﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.CentralDocuments;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Text;
using SharedApplication;
using SharedApplication.RealEstate;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;
using System.Threading;

namespace TWRE0000
{
    /// <summary>
    /// Summary description for frmNewREProperty.
    /// </summary>
    public partial class frmNewREProperty : BaseForm
    {
        private DateTime imgSketchLastModified = DateTime.MinValue;
        private IPropertyPicturesViewModel pictureViewModel = null;
        private IPropertySketchesViewModel sketchesViewModel = null;
        public frmNewREProperty()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblMRRLEXEMPTION = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblMRRLBLDGVAL = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lbl2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lbl1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblMRRLLANDVAL = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblLocation = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblMapLotCopy = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label3 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label6 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label4 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            txtMROISoundValue = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            chkSound = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
            txtMROIPctFunct1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROIUnits1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROIGradePct1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROIGradeCd1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROICond1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROIYear1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROIType1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtMROIPctPhys1 = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            Label65 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label2.AddControlArrayElement(Label2_9, 9);
            Label2.AddControlArrayElement(Label2_1, 1);
            Label2.AddControlArrayElement(Label2_8, 8);
            Label2.AddControlArrayElement(Label2_7, 7);
            Label2.AddControlArrayElement(Label2_6, 6);
            Label2.AddControlArrayElement(Label2_5, 5);
            Label2.AddControlArrayElement(Label2_4, 4);
            Label2.AddControlArrayElement(Label2_3, 3);
            Label2.AddControlArrayElement(Label2_2, 2);
            Label2.AddControlArrayElement(Label2_0, 0);
            Label8.AddControlArrayElement(Label8_8, 8);
            Label8.AddControlArrayElement(Label8_7, 7);
            Label8.AddControlArrayElement(Label8_36, 36);
            Label8.AddControlArrayElement(Label8_10, 10);
            Label8.AddControlArrayElement(Label8_11, 11);
            Label8.AddControlArrayElement(Label8_12, 12);
            Label8.AddControlArrayElement(Label8_13, 13);
            Label8.AddControlArrayElement(Label8_14, 14);
            Label8.AddControlArrayElement(Label8_15, 15);
            Label8.AddControlArrayElement(Label8_16, 16);
            Label8.AddControlArrayElement(Label8_17, 17);
            Label8.AddControlArrayElement(Label8_18, 18);
            Label8.AddControlArrayElement(Label8_19, 19);
            Label8.AddControlArrayElement(Label8_20, 20);
            Label8.AddControlArrayElement(Label8_6, 6);
            lblMRRLEXEMPTION.AddControlArrayElement(lblMRRLEXEMPTION_30, 30);
            lblMRRLBLDGVAL.AddControlArrayElement(lblMRRLBLDGVAL_29, 29);
            lblDate.AddControlArrayElement(lblDate_5, 5);
            lblDate.AddControlArrayElement(lblDate_4, 4);
            lblDate.AddControlArrayElement(lblDate_7, 7);
            lblDate.AddControlArrayElement(lblDate_0, 0);
            lblDate.AddControlArrayElement(lblDate_2, 2);
            lbl2.AddControlArrayElement(lbl2_3, 3);
            lbl1.AddControlArrayElement(lbl1_2, 2);
            lblMRRLLANDVAL.AddControlArrayElement(lblMRRLLANDVAL_28, 28);
            lblLocation.AddControlArrayElement(lblLocation_0, 0);
            lblMapLotCopy.AddControlArrayElement(lblMapLotCopy_0, 0);
            lblMapLotCopy.AddControlArrayElement(lblMapLotCopy_1, 1);
            lblMapLotCopy.AddControlArrayElement(lblMapLotCopy_2, 2);
            Label3.AddControlArrayElement(Label3_31, 31);
            Label3.AddControlArrayElement(Label3_30, 30);
            Label3.AddControlArrayElement(Label3_29, 29);
            Label3.AddControlArrayElement(Label3_25, 25);
            Label3.AddControlArrayElement(Label3_24, 24);
            Label3.AddControlArrayElement(Label3_23, 23);
            Label3.AddControlArrayElement(Label3_22, 22);
            Label3.AddControlArrayElement(Label3_21, 21);
            Label3.AddControlArrayElement(Label3_20, 20);
            Label3.AddControlArrayElement(Label3_19, 19);
            Label3.AddControlArrayElement(Label3_18, 18);
            Label3.AddControlArrayElement(Label3_17, 17);
            Label3.AddControlArrayElement(Label3_16, 16);
            Label3.AddControlArrayElement(Label3_15, 15);
            Label3.AddControlArrayElement(Label3_14, 14);
            Label3.AddControlArrayElement(Label3_13, 13);
            Label3.AddControlArrayElement(Label3_12, 12);
            Label3.AddControlArrayElement(Label3_11, 11);
            Label3.AddControlArrayElement(Label3_10, 10);
            Label3.AddControlArrayElement(Label3_9, 9);
            Label3.AddControlArrayElement(Label3_8, 8);
            Label3.AddControlArrayElement(Label3_7, 7);
            Label3.AddControlArrayElement(Label3_6, 6);
            Label3.AddControlArrayElement(Label3_5, 5);
            Label3.AddControlArrayElement(Label3_4, 4);
            Label3.AddControlArrayElement(Label3_3, 3);
            Label3.AddControlArrayElement(Label3_2, 2);
            Label3.AddControlArrayElement(Label3_1, 1);
            Label3.AddControlArrayElement(Label3_32, 32);
            Label6.AddControlArrayElement(Label6_0, 0);
            Label4.AddControlArrayElement(Label4_0, 0);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_9, 9);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_8, 8);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_7, 7);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_6, 6);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_5, 5);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_4, 4);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_3, 3);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_2, 2);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_1, 1);
            txtMROISoundValue.AddControlArrayElement(txtMROISoundValue_0, 0);
            chkSound.AddControlArrayElement(chkSound_9, 9);
            chkSound.AddControlArrayElement(chkSound_8, 8);
            chkSound.AddControlArrayElement(chkSound_7, 7);
            chkSound.AddControlArrayElement(chkSound_6, 6);
            chkSound.AddControlArrayElement(chkSound_5, 5);
            chkSound.AddControlArrayElement(chkSound_4, 4);
            chkSound.AddControlArrayElement(chkSound_3, 3);
            chkSound.AddControlArrayElement(chkSound_2, 2);
            chkSound.AddControlArrayElement(chkSound_1, 1);
            chkSound.AddControlArrayElement(chkSound_0, 0);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_3, 3);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_1, 1);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_2, 2);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_4, 4);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_5, 5);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_6, 6);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_7, 7);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_8, 8);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_9, 9);
            txtMROIPctFunct1.AddControlArrayElement(txtMROIPctFunct1_0, 0);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_3, 3);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_6, 6);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_1, 1);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_2, 2);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_4, 4);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_5, 5);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_7, 7);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_8, 8);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_9, 9);
            txtMROIUnits1.AddControlArrayElement(txtMROIUnits1_0, 0);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_8, 8);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_1, 1);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_2, 2);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_3, 3);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_4, 4);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_5, 5);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_6, 6);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_7, 7);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_9, 9);
            txtMROIGradePct1.AddControlArrayElement(txtMROIGradePct1_0, 0);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_9, 9);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_0, 0);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_1, 1);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_2, 2);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_3, 3);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_4, 4);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_5, 5);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_6, 6);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_7, 7);
            txtMROIGradeCd1.AddControlArrayElement(txtMROIGradeCd1_8, 8);
            txtMROICond1.AddControlArrayElement(txtMROICond1_1, 1);
            txtMROICond1.AddControlArrayElement(txtMROICond1_2, 2);
            txtMROICond1.AddControlArrayElement(txtMROICond1_3, 3);
            txtMROICond1.AddControlArrayElement(txtMROICond1_4, 4);
            txtMROICond1.AddControlArrayElement(txtMROICond1_5, 5);
            txtMROICond1.AddControlArrayElement(txtMROICond1_6, 6);
            txtMROICond1.AddControlArrayElement(txtMROICond1_7, 7);
            txtMROICond1.AddControlArrayElement(txtMROICond1_8, 8);
            txtMROICond1.AddControlArrayElement(txtMROICond1_9, 9);
            txtMROICond1.AddControlArrayElement(txtMROICond1_0, 0);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_1, 1);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_2, 2);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_3, 3);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_4, 4);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_5, 5);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_6, 6);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_7, 7);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_8, 8);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_9, 9);
            txtMROIYear1.AddControlArrayElement(txtMROIYear1_0, 0);
            txtMROIType1.AddControlArrayElement(txtMROIType1_1, 1);
            txtMROIType1.AddControlArrayElement(txtMROIType1_2, 2);
            txtMROIType1.AddControlArrayElement(txtMROIType1_3, 3);
            txtMROIType1.AddControlArrayElement(txtMROIType1_4, 4);
            txtMROIType1.AddControlArrayElement(txtMROIType1_5, 5);
            txtMROIType1.AddControlArrayElement(txtMROIType1_6, 6);
            txtMROIType1.AddControlArrayElement(txtMROIType1_7, 7);
            txtMROIType1.AddControlArrayElement(txtMROIType1_8, 8);
            txtMROIType1.AddControlArrayElement(txtMROIType1_9, 9);
            txtMROIType1.AddControlArrayElement(txtMROIType1_0, 0);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_0, 0);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_9, 9);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_8, 8);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_7, 7);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_6, 6);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_5, 5);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_4, 4);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_3, 3);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_2, 2);
            txtMROIPctPhys1.AddControlArrayElement(txtMROIPctPhys1_1, 1);
            Label65.AddControlArrayElement(Label65_18, 18);
            Label65.AddControlArrayElement(Label65_8, 8);
            Label65.AddControlArrayElement(Label65_9, 9);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            
            //FC:FINAL:AM:#1637 - decrease the height of rows
            GridDwelling1.RowHeight(-1, 365);
            GridDwelling2.RowHeight(-1, 365);
            GridDwelling3.RowHeight(-1, 365);
            //FC:FINAL:AM:#3294 - allow only numbers
            cmbCardNumber.AllowOnlyNumericInput();
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmNewREProperty InstancePtr
        {
            get
            {
                return (frmNewREProperty)Sys.GetInstance(typeof(frmNewREProperty));
            }
        }

        protected frmNewREProperty _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        bool boolEditDocuments;
        private bool boolViewDocuments;
        private bool boolSaving;
        private bool boolUpdateSketch;
        private bool boolSaleChanged;
        private bool boolNameChanged;
        private bool boolCalcDataChanged;
        private bool boolBookPageChanged;
        private bool boolLoading;
        private bool booleditland;
        private bool boolAddingACard;
        double dblSTDOVRate;
        bool boolCalculating;
        int TotalSquareFeet;
        double dblEffTaxRate;
        int lngOriginalOwnerID;
        int intTotalNumberOfControls;
        clsAuditControlInformation[] clsControlInfo = null;
        // Class to keep track of control information
        clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
        double dblPixelsPerScrollUnits;
        // vbPorter upgrade warning: dtOriginalSaleDate As DateTime	OnWrite(short, string, DateTime)
        DateTime dtOriginalSaleDate;
        int lngPOOwnerID;
        int lngPOSecOwnerID;
        string strPOAddr1;
        string strPOAddr2;
        string strPOCity;
        string strPOState;
        string strPOZip;
        int lngCurrentPendingID;
        //string[] SketchArray = new string[16 + 1];
        //int intCurrentSketch;
        //int lngCurrentWinSketchID;
        const int BPBookCol = 1;
        const int BPPageCol = 2;
        const int BPSaleDateCol = 3;
        const int BPCheckCol = 0;
        const int CNSTNOEXEMPT = -1;
        const int CNSTEXEMPTCOLCODE = 0;
        const int CNSTEXEMPTCOLPERCENT = 1;
        const int CNSTSALECOLPRICE = 0;
        const int CNSTSALECOLDATE = 1;
        const int CNSTSALECOLSALE = 2;
        const int CNSTSALECOLFINANCING = 3;
        const int CNSTSALECOLVERIFIED = 4;
        const int CNSTSALECOLVALIDITY = 5;
        // vbPorter upgrade warning: dtMinPendingDate As DateTime	OnWrite(string)
        public DateTime dtMinPendingDate;
        // WinSkt.WinSkt appWinSketch = new WinSkt.WinSkt();
        dynamic appWinSketch;
        // Dim WithEvents clsPropEvents As clsPropertyEvents
        const int colOCCCode = 0;
        const int colSF = 1;
        const int colGN = 2;
        const int colActRent = 3;
        const int colActSFYear = 4;
        const int colMarketSFYear = 5;
        const int colMarketRent = 6;
        const int colECode = 0;
        const int colGross = 1;
        const int colPerSF = 2;
        const int colAmount = 3;
        private bool boolDataChanged;
        private cREAccount theAccount = new cREAccount();
        private string strPODeedName1 = "";
        private string strPODeedName2 = "";
        private void cmdChangeOwnership_Click(object sender, System.EventArgs e)
        {
            // 
            // Call ChangeOwnership
        }

        private void cmdEditOwner_Click(object sender, System.EventArgs e)
        {
            if (!(theAccount == null))
            {
                if (theAccount.OwnerID > 0)
                {
                    int lngReturn;
                    int lngID = theAccount.OwnerID;
                    lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngID);
                    cPartyController tCont = new cPartyController();
                    cParty tParty = theAccount.OwnerParty;
                    tCont.LoadParty(ref tParty, theAccount.OwnerID);
                    ShowOwner();
                    ShowAddress();
                }
            }
        }

        private void cmdEditSecOwner_Click(object sender, System.EventArgs e)
        {
            if (!(theAccount == null))
            {
                if (theAccount.SecOwnerID > 0)
                {
                    int lngReturn;
                    int lngID = theAccount.SecOwnerID;
                    lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngID);
                    cPartyController tCont = new cPartyController();
                    cParty tParty = theAccount.SecOwnerParty;
                    tCont.LoadParty(ref tParty, theAccount.SecOwnerID);
                    ShowSecOwner();
                }
            }
        }

        private void cmdRemoveOwner_Click(object sender, System.EventArgs e)
        {
            theAccount.OwnerID = 0;
            if (!(theAccount.OwnerParty == null))
            {
                theAccount.OwnerParty.Clear();
            }
            ShowOwner();
            ShowAddress();
        }

        private void cmdRemoveSecOwner_Click(object sender, System.EventArgs e)
        {
            theAccount.SecOwnerID = 0;
            if (!(theAccount.SecOwnerParty == null))
            {
                theAccount.SecOwnerParty.Clear();
            }
            ShowSecOwner();
        }

        private void ShowOwner()
        {
            if (theAccount.OwnerID > 0)
            {
                if (!(theAccount.OwnerParty == null))
                {
                    lblOwner1.Text = theAccount.OwnerParty.FullNameLastFirst;
                }
                else
                {
                    lblOwner1.Text = "";
                }
                txtOwnerID.Text = FCConvert.ToString(theAccount.OwnerID);
            }
            else
            {
                lblOwner1.Text = "";
                txtOwnerID.Text = FCConvert.ToString(0);
            }
        }

        private void ShowAddress()
        {
            if (!(theAccount.OwnerParty == null))
            {
                cPartyAddress tAdd;
                tAdd = theAccount.OwnerParty.GetPrimaryAddress();
                // Set tAdd = theAccount.OwnerParty.GetAddress("RE", theAccount.Account, Format(Now, "MM/dd/yyyy"))
                if (!(tAdd == null))
                {
                    lblAddress.Text = tAdd.GetFormattedAddress();
                }
                else
                {
                    lblAddress.Text = "";
                }
            }
            else
            {
                lblAddress.Text = "";
            }
        }

        private void ShowSecOwner()
        {
            if (theAccount.SecOwnerID > 0)
            {
                if (!(theAccount.SecOwnerParty == null))
                {
                    lbl2ndOwner.Text = theAccount.SecOwnerParty.FullNameLastFirst;
                }
                else
                {
                    lbl2ndOwner.Text = "";
                }
                txt2ndOwnerID.Text = FCConvert.ToString(theAccount.SecOwnerID);
            }
            else
            {
                lbl2ndOwner.Text = "";
                txt2ndOwnerID.Text = FCConvert.ToString(0);
            }
        }

        private void cmdSearchOwner_Click(object sender, System.EventArgs e)
        {
            int lngPrice = 0;
            int intType = 0;
            int intFinancing = 0;
            int intVerified = 0;
            int intValidity = 0;
            // vbPorter upgrade warning: dtDate As DateTime	OnWriteFCConvert.ToInt16(
            DateTime dtDate;
            clsDRWrapper clsGroup = new clsDRWrapper();
            cPartyController tCont = new cPartyController();
            bool boolUpdateDeedName = false;

            try
            {
                // On Error GoTo ErrorHandler
                int lngReturnID;
                lngReturnID = frmCentralPartySearch.InstancePtr.Init();
                if (lngReturnID != theAccount.OwnerID && lngReturnID > 0)
                {
                    if (!modGlobalVariables.Statics.boolInPendingMode)
                    {
                        if (lngOriginalOwnerID == 0)
                        {
                            boolUpdateDeedName = true;
                        }
                        if (lngOriginalOwnerID > 0 && lngReturnID != lngOriginalOwnerID)
                        {
                            int intReturn = 0;
                            intReturn = frmOwnershipChangeReason.InstancePtr.Init();
                            if (intReturn >= 0)
                            {
                                if (intReturn == 0)
                                {
                                    boolNameChanged = true;
                                    clsGroup.OpenRecordset("select * from moduleassociation where module = 'RE' and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "CentralData");
                                    if (!clsGroup.EndOfFile())
                                    {
                                        BringToFront();
                                        //Application.DoEvents();
                                        frmUpdateGroup.InstancePtr.Init(clsGroup.Get_Fields_Int32("groupnumber"), ref modGlobalVariables.Statics.gintLastAccountNumber);
                                    }
                                    //Application.DoEvents();
                                    string strTemp = "";
                                    cParty tParty1 = theAccount.OwnerParty;
                                    tCont.LoadParty(ref tParty1, lngPOOwnerID, true);
                                    strTemp = strPODeedName1;
                                    if (strPODeedName2.Trim() != "")
                                    {
                                        strTemp = Strings.Mid(strTemp + ", " + strPODeedName2, 1, 50);
                                    }
                                    txtMRRSPrevMaster.Text = strTemp;
                                    theAccount.SecOwnerID = 0;
                                    theAccount.SecOwnerParty.Clear();
                                    boolUpdateDeedName = true;
                                    theAccount.DeedName1 = "";
                                    theAccount.DeedName2 = "";
                                    intType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
                                    intFinancing = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
                                    intValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
                                    intVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
                                    if (Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)) > 0)
                                    {
                                        lngPrice = FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)));
                                    }
                                    // 26                    dtDate = Format(Date, "MM/dd/yyyy")
                                    dtDate = DateTime.FromOADate(0);
                                    if (frmGetSaleInfo.InstancePtr.Init(true, ref lngPrice, ref dtDate, ref intType, ref intFinancing, ref intVerified, ref intValidity, "Change of Ownership"))
                                    {
                                        boolSaleChanged = true;
                                        SaleGrid.TextMatrix(2, CNSTSALECOLDATE, Strings.Format(dtDate, "MM/dd/yyyy"));
                                        SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(lngPrice, "#,###,###,##0"));
                                        SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY, FCConvert.ToString(intValidity));
                                        SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED, FCConvert.ToString(intVerified));
                                        SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING, FCConvert.ToString(intFinancing));
                                        SaleGrid.TextMatrix(2, CNSTSALECOLSALE, FCConvert.ToString(intType));
                                    }
                                    ShowSecOwner();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    // Dim tCont As New cPartyController
                    cParty tParty = theAccount.OwnerParty;
                    tCont.LoadParty(ref tParty, lngReturnID);
                    theAccount.OwnerID = lngReturnID;
                    if (boolUpdateDeedName)
                    {
                        theAccount.DeedName1 = theAccount.OwnerParty.FullNameLastFirst;
                        ShowDeeds();
                    }
                    ShowOwner();
                    ShowAddress();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SearchOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void cmdSearchSecOwner_Click(object sender, System.EventArgs e)
        {
            int lngReturnID;
            var boolUpdateDeedName = false;
            lngReturnID = frmCentralPartySearch.InstancePtr.Init();
            if (lngReturnID != theAccount.SecOwnerID && lngReturnID > 0)
            {

                boolUpdateDeedName = true;
                cPartyController tCont = new cPartyController();
                cParty tParty = theAccount.SecOwnerParty;
                tCont.LoadParty(ref tParty, lngReturnID);
                theAccount.SecOwnerID = lngReturnID;
                ShowSecOwner();
                if (boolUpdateDeedName)
                {
                    theAccount.DeedName2 = theAccount.SecOwnerParty.FullNameLastFirst;
                    ShowDeeds();
                }
            }
        }

        private void frmNewREProperty_Load(object sender, System.EventArgs e)
        {
            imSketch.Image = null;
            imgPicture.Image = null;
            LoadForm();
        }

        private void LoadForm()
        {
            if (!modGlobalConstants.Statics.gboolCL || !modGlobalVariables.Statics.boolRegRecords)
            {
                mnuRECLComment.Visible = false;
                mnuCollectionsNote.Visible = false;
            }
            boolSaving = false;
            boolUpdateSketch = false;
            boolSaleChanged = false;
            boolNameChanged = false;
            boolCalcDataChanged = false;
            boolLoading = true;
            modGlobalVariables.Statics.TownName = Strings.Trim(modGlobalConstants.Statics.MuniName);
            // strOriginalName = ""
            lngOriginalOwnerID = 0;
            //GetPictureLocation();
            SetupGridTranCode();
            SetupGridLand();
            SetupGridPropertyCode();
            SetupGridLandCode();
            SetupGridBldgCode();
            // SetupGridPhones
            SetupGridExempts();
            SetupSaleGrid();
            SetupGridNeighborhood();
            SetupGridZone();
            SetupGridUtilities();
            SetupGridStreet();
            SetupGridSecZone();
            SetupGridTopography();
            SetupcmbPrintSize();
            SetupcmbCardNumber();
            // SetTRIOColors frmnewreproperty
            cmbCardNumber.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            lblNumCards.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            lblNumCards.Font = new Font(lblNumCards.Font, FontStyle.Bold);
            lblDwel.Font = new Font(lblDwel.Font.FontFamily, 14);
            lblDwel.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            lblComment.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
            //FC:FINAL:BSE #3303 label color should be red
            lblInterestedPartiesPresent.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
            lblPendingFlag.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            lblPicturePresent.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
            lblSketchPresent.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
            lblcomm.Font = new Font(lblcomm.Font.FontFamily, 18);
            lblcomm.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            lblOut.Font = new Font(lblOut.Font.FontFamily, 18);
            lblOut.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            lblMRHLAcctNum.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            Label10.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            Label11.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            SetupBPGrid();
            modGlobalFunctions.SetFixedSize(this);
            SetupIncomeGrids();
            cmdPrevSketch.Image = MDIParent.InstancePtr.ImageList1.Images[4 - 1];
            cmdPrevSketch.DisabledPicture = MDIParent.InstancePtr.ImageList1.Images[5 - 1];
            cmdNextSketch.Image = MDIParent.InstancePtr.ImageList1.Images[6 - 1];
            cmdNextSketch.DisabledPicture = MDIParent.InstancePtr.ImageList1.Images[7 - 1];
            cmdPrevPicture.Image = MDIParent.InstancePtr.ImageList1.Images[4 - 1];
            cmdPrevPicture.DisabledPicture = MDIParent.InstancePtr.ImageList1.Images[5 - 1];
            cmdNextPicture.Image = MDIParent.InstancePtr.ImageList1.Images[6 - 1];
            cmdNextPicture.DisabledPicture = MDIParent.InstancePtr.ImageList1.Images[7 - 1];
            modGNBas.Statics.intNewTab = 0;
            SSTab1.SelectedIndex = modGNBas.Statics.intNewTab;
            //if (!modGlobalVariables.Statics.boolRegRecords)
            //{
            //    mnuDeleteAccount.Text = "Delete Sale Record";
            //}
            if (modGlobalConstants.Statics.gboolCL && modGlobalVariables.Statics.boolRegRecords && !modGlobalVariables.Statics.boolInArchiveMode)
            {
                mnuPrintAccountInformationSheet.Visible = true;
                clsDRWrapper rsTemp = new clsDRWrapper();
                rsTemp.OpenRecordset("select * from comments where type = 'RE' and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    if (rsTemp.Get_Fields_Boolean("showinre") && Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("comment"))) != "")
                    {
                        MessageBox.Show(FCConvert.ToString(rsTemp.Get_Fields_String("Comment")), "Collections Comment", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                LoadAccount(modGlobalVariables.Statics.gintLastAccountNumber);
                ShowAccount();
                LoadCard(modGlobalVariables.Statics.gintLastAccountNumber, 1);
                ShowCard();
                lngOriginalOwnerID = theAccount.OwnerID;
                lngPOOwnerID = lngOriginalOwnerID;
                lngPOSecOwnerID = theAccount.SecOwnerID;
                strPODeedName1 = theAccount.DeedName1;
                strPODeedName2 = theAccount.DeedName2;
                cPartyAddress tAddr;
                if (!(theAccount.OwnerParty == null))
                {
                    tAddr = theAccount.OwnerParty.GetPrimaryAddress();
                    if (!(tAddr == null))
                    {
                        strPOAddr1 = tAddr.Address1;
                        strPOAddr2 = tAddr.Address2;
                        strPOCity = tAddr.City;
                        strPOState = tAddr.State;
                        strPOZip = tAddr.Zip;
                    }
                }
                if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate") + ""))
                {
                    dtOriginalSaleDate = (DateTime)modDataTypes.Statics.MR.Get_Fields_DateTime("saledate");
                }
                else
                {
                    dtOriginalSaleDate = DateTime.FromOADate(0);
                }
            }
        }

        //public void GetPictureLocation()
        //{
        //    modGNBas.Statics.PictureLocation = Interaction.GetSetting("TWGNENTY", "REAL_ESTATE", "PICTURE_FOLDER", "");
        //}

        private void SetupGridTranCode()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strTemp = "";
            string strDesc = "";
            clsLoad.OpenRecordset("select * from tbltrancode where code = 0", modGlobalVariables.strREDatabase);
            if (clsLoad.EndOfFile())
            {
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;None" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 None" + "\t" + "0|";
                    }
                }
                else
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;Combined" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 Combined" + "\t" + "0|";
                    }
                }
            }
            clsLoad.OpenRecordset("select * from tblTranCode order by code", modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                strDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strDesc = clsLoad.Get_Fields("code") + " " + strDesc;
                }
                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                strTemp += "#" + clsLoad.Get_Fields("code") + ";" + strDesc + "\t" + clsLoad.Get_Fields("code") + "|";
                clsLoad.MoveNext();
            }
            if (strTemp != string.Empty)
            {
                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
            }
            gridTranCode.ColComboList(0, strTemp);
            gridTranCode.TextMatrix(0, 0, FCConvert.ToString(0));
        }

        private void ResizeGridTranCode()
        {

        }

        private void ResizeGridNeighborhood()
        {

        }

        private void ResizeGridZone()
        {

        }

        private void ResizeGridTopography()
        {

        }

        private void ResizeGridSecZone()
        {

        }

        private void ResizeGridStreet()
        {

        }

        private void ResizeGridUtilities()
        {

        }

        private void ResizeGridLandCode()
        {

        }

        private void ResizeGridBldgCode()
        {
            //FC:FINAL:DDU:#i961 - resolved in design
            //gridBldgCode.Height = (gridBldgCode.RowHeight(0) + 60)/FCScreen.TwipsPerPixelY;
        }

        private void ResizeGridPropertyCode()
        {
            //FC:FINAL:DDU:#i961 - resolved in design
            //gridPropertyCode.Height = (gridPropertyCode.RowHeight(0) + 60)/FCScreen.TwipsPerPixelY;
        }

        private void ResizeGridExempts()
        {
            int GridWidth = 0;
            GridWidth = GridExempts.WidthOriginal;
            GridExempts.ColWidth(CNSTEXEMPTCOLCODE, FCConvert.ToInt32(0.78 * GridWidth));
        }

        private void SetupGridLand()
        {
            string strTemp = "";
            string strDesc = "";
            clsDRWrapper clsLoad = new clsDRWrapper();
            int lngTownNumber = 0;
            string strShort = "";
            if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                lngTownNumber = 0;
            }
            else
            {
                lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                if (lngTownNumber == 0)
                    lngTownNumber = 1;
            }
            GridLand.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 7, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GridLand.ExtendLastCol = true;
            GridLand.TextMatrix(0, 0, "Land Type");
            GridLand.TextMatrix(0, 1, "Units");
            GridLand.TextMatrix(0, 2, "Influence");
            GridLand.TextMatrix(0, 3, "Influence Code");
            modGlobalVariables.Statics.LandTypes.Init();
            if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                strTemp = "#0;No Land" + "\t" + "" + "|";  
            }
            else
            {
                strTemp = "#0;0 No Land" + "\t" + "" + "|";  
            }
            while (!modGlobalVariables.Statics.LandTypes.EndOfLandTypes())
            {
                if (modGlobalVariables.Statics.CustomizedInfo.ShowShortDesc)
                {
                    strShort = "\t" + modGlobalVariables.Statics.LandTypes.ShortDescription;
                }
                else
                {
                    strShort = "";
                }
                if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    strTemp += "#" + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code) + ";" + modGlobalVariables.Statics.LandTypes.Description + strShort;
                }
                else
                {
                    strTemp += "#" + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code) + ";" + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code) + " " + modGlobalVariables.Statics.LandTypes.Description + strShort;
                }
                switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                {
                    case modREConstants.CNSTLANDTYPEFRONTFOOT:
                        {
                            strTemp += "\t" + "Front Foot";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPESQUAREFEET:
                        {
                            strTemp += "\t" + "Square Feet";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                        {
                            strTemp += "\t" + "Fractional Acreage";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEACRES:
                        {
                            strTemp += "\t" + "Acres";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPESITE:
                        {
                            strTemp += "\t" + "Site";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
                        {
                            strTemp += "\t" + "";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                        {
                            strTemp += "\t" + "Improvements";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPELINEARFEET:
                        {
                            strTemp += "\t" + "Linear Feet";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                        {
                            strTemp += "\t" + "Front Foot - Width";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                        {
                            strTemp += "\t" + "Linear Feet - Width";
                            break;
                        }
                }
                
                strTemp += "|";
                modGlobalVariables.Statics.LandTypes.MoveNext();
            }
            if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                strTemp += "#99;Extra Influence";
            }
            else
            {
                strTemp += "#99;99 Extra Influence";
            }
            // take off last |
            // If strTemp <> vbNullString Then
            // strTemp = Mid(strTemp, 1, Len(strTemp) - 1)
            // End If
            GridLand.ColComboList(0, strTemp);
            if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                strTemp = "#0;None" + "\t" + "0|";
            }
            else
            {
                strTemp = "#0;0 None" + "\t" + "0|";
            }
            clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1441 and 1449 and townnumber = " + FCConvert.ToString(lngTownNumber) + " order by crecordnumber", modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                strDesc = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    strTemp += "#" + FCConvert.ToString(clsLoad.Get_Fields_Int32("crecordnumber") - 1440) + ";" + strDesc + "\t" + FCConvert.ToString(clsLoad.Get_Fields_Int32("crecordnumber") - 1440) + "|";
                }
                else
                {
                    strTemp += "#" + FCConvert.ToString(clsLoad.Get_Fields_Int32("crecordnumber") - 1440) + ";" + FCConvert.ToString(clsLoad.Get_Fields_Int32("crecordnumber") - 1440) + " " + strDesc + "\t" + FCConvert.ToString(clsLoad.Get_Fields_Int32("crecordnumber") - 1440) + "|";
                }
                clsLoad.MoveNext();
            }
            if (strTemp != string.Empty)
            {
                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
            }
            GridLand.ColComboList(3, strTemp);
        }

        private void SetupGridLandCode()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strTemp = "";
            string strDesc = "";
            try
            {
                // On Error GoTo ErrorHandler
                clsLoad.OpenRecordset("select * from tbllandcode where code = 0", modGlobalVariables.strREDatabase);
                if (clsLoad.EndOfFile())
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;None" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 None" + "\t" + "0|";
                    }
                }
                clsLoad.OpenRecordset("select * from tbllandCode order by code", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    strDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strDesc = clsLoad.Get_Fields("code") + " " + strDesc;
                    }
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strTemp += "#" + clsLoad.Get_Fields("code") + ";" + strDesc + "\t" + clsLoad.Get_Fields("code") + "|";
                    clsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                gridLandCode.ColComboList(0, strTemp);
                gridLandCode.TextMatrix(0, 0, FCConvert.ToString(0));
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridLandCode in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridBldgCode()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strTemp = "";
            string strDesc = "";
            try
            {
                // On Error GoTo ErrorHandler
                clsLoad.OpenRecordset("select * from tblbldgcode where code = 0", modGlobalVariables.strREDatabase);
                if (clsLoad.EndOfFile())
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;None" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 None" + "\t" + "0|";
                    }
                }
                clsLoad.OpenRecordset("select * from tblbldgCode order by code", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    strDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strDesc = clsLoad.Get_Fields("code") + " " + strDesc;
                    }
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strTemp += "#" + clsLoad.Get_Fields("code") + ";" + strDesc + "\t" + clsLoad.Get_Fields("code") + "|";
                    clsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                gridBldgCode.ColComboList(0, strTemp);
                gridBldgCode.TextMatrix(0, 0, FCConvert.ToString(0));
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridBldgCode in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridNeighborhood()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strTemp = "";
            int lngTownNumber = 0;
            string strDesc = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    lngTownNumber = 0;
                }
                else
                {
                    lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                rsLoad.OpenRecordset("select * from neighborhood where code = 0 and townnumber = " + FCConvert.ToString(lngTownNumber), modGlobalVariables.strREDatabase);
                if (rsLoad.EndOfFile())
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;None" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 None" + "\t" + "0|";
                    }
                }
                rsLoad.OpenRecordset("select * from neighborhood where townnumber = " + FCConvert.ToString(lngTownNumber) + " order by code", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    strDesc = FCConvert.ToString(rsLoad.Get_Fields_String("description"));
                    strDesc = Strings.Trim(Strings.Left(strDesc + Strings.StrDup(23, " "), 23));
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp += "#" + rsLoad.Get_Fields("code") + ";" + strDesc + "\t" + rsLoad.Get_Fields("code") + "|";
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp += "#" + rsLoad.Get_Fields("code") + ";" + rsLoad.Get_Fields("code") + " " + strDesc + "|";
                    }
                    rsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                GridNeighborhood.ColComboList(0, strTemp);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridNeighborhood", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridTopography()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            int lngTownNumber = 0;
            string strDesc = "";
            string strTemp = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    lngTownNumber = 0;
                }
                else
                {
                    lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                rsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1301 and 1309 and townnumber = " + FCConvert.ToString(lngTownNumber) + " order by crecordnumber", modGlobalVariables.strREDatabase);
                if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    strTemp = "#0;None" + "\t" + "0|";
                }
                else
                {
                    strTemp = "#0;0 None" + "\t" + "0|";
                }
                while (!rsLoad.EndOfFile())
                {
                    strDesc = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("cldesc")));
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strDesc = FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1300) + " " + strDesc;
                    }
                    strDesc = Strings.Trim(Strings.Left(strDesc + Strings.StrDup(23, " "), 23));
                    strTemp += "#" + FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1300) + ";" + strDesc + "\t" + FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1300) + "|";
                    rsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                GridTopography.ColComboList(0, strTemp);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridTopography", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridUtilities()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            int lngTownNumber = 0;
            string strDesc = "";
            string strTemp = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    lngTownNumber = 0;
                }
                else
                {
                    lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                rsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1311 and 1319 and townnumber = " + FCConvert.ToString(lngTownNumber) + " order by crecordnumber", modGlobalVariables.strREDatabase);
                if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    strTemp = "#0;None" + "\t" + "0|";
                }
                else
                {
                    strTemp = "#0;0 None" + "\t" + "0|";
                }
                while (!rsLoad.EndOfFile())
                {
                    strDesc = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("cldesc")));
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strDesc = FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1310) + " " + strDesc;
                    }
                    strDesc = Strings.Trim(Strings.Left(strDesc + Strings.StrDup(23, " "), 23));
                    strTemp += "#" + FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1310) + ";" + strDesc + "\t" + FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1310) + "|";
                    rsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                GridUtilities.ColComboList(0, strTemp);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridUtilities", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridStreet()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            int lngTownNumber = 0;
            string strDesc = "";
            string strTemp = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    lngTownNumber = 0;
                }
                else
                {
                    lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                rsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1321 and 1329 and townnumber = " + FCConvert.ToString(lngTownNumber) + " order by crecordnumber", modGlobalVariables.strREDatabase);
                if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    strTemp = "#0;None" + "\t" + "0|";
                }
                else
                {
                    strTemp = "#0;0 None" + "\t" + "0|";
                }
                while (!rsLoad.EndOfFile())
                {
                    strDesc = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("cldesc")));
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strDesc = FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1320) + " " + strDesc;
                    }
                    strDesc = Strings.Trim(Strings.Left(strDesc + Strings.StrDup(23, " "), 23));
                    strTemp += "#" + FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1320) + ";" + strDesc + "\t" + FCConvert.ToString(rsLoad.Get_Fields_Int32("crecordnumber") - 1320) + "|";
                    rsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                GridStreet.ColComboList(0, strTemp);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridStreet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridZone()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strTemp = "";
            int lngTownNumber = 0;
            string strDesc = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    lngTownNumber = 0;
                }
                else
                {
                    lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                rsLoad.OpenRecordset("select * from zones where code = 0 and townnumber = " + FCConvert.ToString(lngTownNumber), modGlobalVariables.strREDatabase);
                if (rsLoad.EndOfFile())
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;None" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 None|";
                    }
                }
                rsLoad.OpenRecordset("select * from zones where townnumber = " + FCConvert.ToString(lngTownNumber) + " order by code", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    strDesc = FCConvert.ToString(rsLoad.Get_Fields_String("description"));
                    strDesc = Strings.Trim(Strings.Left(strDesc + Strings.StrDup(23, " "), 23));
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp += "#" + rsLoad.Get_Fields("code") + ";" + strDesc + "\t" + rsLoad.Get_Fields("code") + "|";
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp += "#" + rsLoad.Get_Fields("code") + ";" + rsLoad.Get_Fields("code") + " " + strDesc + "|";
                    }
                    rsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                GridZone.ColComboList(0, strTemp);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridZone", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridSecZone()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strTemp = "";
            int lngTownNumber = 0;
            string strDesc = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    lngTownNumber = 0;
                }
                else
                {
                    lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                rsLoad.OpenRecordset("select * from zones where code = 0 and townnumber = " + FCConvert.ToString(lngTownNumber), modGlobalVariables.strREDatabase);
                if (rsLoad.EndOfFile())
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strTemp = "#0;" + "None" + "\t" + "0|";
                    }
                    else
                    {
                        strTemp = "#0;0 None" + "\t" + "0|";
                    }
                }
                rsLoad.OpenRecordset("select * from zones where townnumber = " + FCConvert.ToString(lngTownNumber) + " order by code", modGlobalVariables.strREDatabase);
                while (!rsLoad.EndOfFile())
                {
                    strDesc = FCConvert.ToString(rsLoad.Get_Fields_String("secondarydescription"));
                    strDesc = Strings.Trim(Strings.Left(strDesc + Strings.StrDup(23, " "), 23));
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp += "#" + rsLoad.Get_Fields("code") + ";" + strDesc + "\t" + rsLoad.Get_Fields("code") + "|";
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp += "#" + rsLoad.Get_Fields("code") + ";" + rsLoad.Get_Fields("code") + " " + strDesc + "|";
                    }
                    rsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                GridSecZone.ColComboList(0, strTemp);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridSecZone", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupGridPropertyCode()
        {
            string strTemp = "";
            if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                strTemp = "#0;None" + "\t" + " ";
                strTemp += "|#101;Rural" + "\t" + "Vacant Land|";
                strTemp += "#102;Urban" + "\t" + "Vacant Land|";
                strTemp += "#103;Oceanfront" + "\t" + "Vacant Land|";
                strTemp += "#104;Lake/Pond front" + "\t" + "Vacant Land|";
                strTemp += "#105;Stream/Riverfront" + "\t" + "Vacant Land|";
                strTemp += "#106;Agricultural" + "\t" + "Vacant Land|";
                strTemp += "#107;Commercial Zone" + "\t" + "Vacant Land|";
                strTemp += "#120;Other" + "\t" + "Vacant Land|";
                strTemp += "#201;Rural" + "\t" + "Single Family|";
                strTemp += "#202;Urban" + "\t" + "Single Family|";
                strTemp += "#203;Oceanfront" + "\t" + "Single Family|";
                strTemp += "#204;Lake/Pond front" + "\t" + "Single Family|";
                strTemp += "#205;Stream/Riverfront" + "\t" + "Single Family|";
                strTemp += "#206;Mobile Home" + "\t" + "Single Family|";
                strTemp += "#220;Other" + "\t" + "Single Family|";
                strTemp += "#301;Mixed Use" + "\t" + "Commercial|";
                strTemp += "#302;Apt/2-3 Unit" + "\t" + "Commercial|";
                strTemp += "#303;Apt/4+" + "\t" + "Commercial|";
                strTemp += "#304;Bank" + "\t" + "Commercial|";
                strTemp += "#305;Restaurant" + "\t" + "Commercial|";
                strTemp += "#306;Medical" + "\t" + "Commercial|";
                strTemp += "#307;Office" + "\t" + "Commercial|";
                strTemp += "#308;Retail" + "\t" + "Commercial|";
                strTemp += "#309;Automotive" + "\t" + "Commercial|";
                strTemp += "#310;Marina" + "\t" + "Commercial|";
                strTemp += "#311;Warehouse" + "\t" + "Commercial|";
                strTemp += "#312;Hotel/Motel/Inn" + "\t" + "Commercial|";
                strTemp += "#313;Nursing Home" + "\t" + "Commercial|";
                strTemp += "#314;Shopping Mall" + "\t" + "Commercial|";
                strTemp += "#320;Other" + "\t" + "Commercial|";
                strTemp += "#401;Gas and Oil" + "\t" + "Industrial|";
                strTemp += "#402;Utility" + "\t" + "Industrial|";
                strTemp += "#403;Gravel Pit" + "\t" + "Industrial|";
                strTemp += "#404;Lumber/Saw Mill" + "\t" + "Industrial|";
                strTemp += "#405;Pulp/Paper Mill" + "\t" + "Industrial|";
                strTemp += "#406;Light Manufacture" + "\t" + "Industrial|";
                strTemp += "#407;Heavy Manufacture" + "\t" + "Industrial|";
                strTemp += "#420;Manufacture Other" + "\t" + "Industrial|";
                strTemp += "#501;Government" + "\t" + "Misc Codes|";
                strTemp += "#502;Condominium" + "\t" + "Misc Codes|";
                strTemp += "#503;Timeshare Unit" + "\t" + "Misc Codes|";
                strTemp += "#504;Non-Profit" + "\t" + "Misc Codes|";
                strTemp += "#505;Mobile Home Park" + "\t" + "Misc Codes|";
                strTemp += "#506;Airport" + "\t" + "Misc Codes|";
                strTemp += "#507;Conservation" + "\t" + "Misc Codes|";
                strTemp += "#508;Current Use Classification" + "\t" + "Misc Codes|";
                strTemp += "#520;Other" + "\t" + "Misc Codes";
            }
            else
            {
                strTemp = "#0;0 None" + "\t" + " ";
                strTemp += "|#101;101 Rural" + "\t" + "Vacant Land|";
                strTemp += "#102;102 Urban" + "\t" + "Vacant Land|";
                strTemp += "#103;103 Oceanfront" + "\t" + "Vacant Land|";
                strTemp += "#104;104 Lake/Pond front" + "\t" + "Vacant Land|";
                strTemp += "#105;105 Stream/Riverfront" + "\t" + "Vacant Land|";
                strTemp += "#106;106 Agricultural" + "\t" + "Vacant Land|";
                strTemp += "#107;107 Commercial Zone" + "\t" + "Vacant Land|";
                strTemp += "#120;120 Other" + "\t" + "Vacant Land|";
                strTemp += "#201;201 Rural" + "\t" + "Single Family|";
                strTemp += "#202;202 Urban" + "\t" + "Single Family|";
                strTemp += "#203;203 Oceanfront" + "\t" + "Single Family|";
                strTemp += "#204;204 Lake/Pond front" + "\t" + "Single Family|";
                strTemp += "#205;205 Stream/Riverfront" + "\t" + "Single Family|";
                strTemp += "#206;206 Mobile Home" + "\t" + "Single Family|";
                strTemp += "#220;220 Other" + "\t" + "Single Family|";
                strTemp += "#301;301 Mixed Use" + "\t" + "Commercial|";
                strTemp += "#302;302 Apt/2-3 Unit" + "\t" + "Commercial|";
                strTemp += "#303;303 Apt/4+" + "\t" + "Commercial|";
                strTemp += "#304;304 Bank" + "\t" + "Commercial|";
                strTemp += "#305;305 Restaurant" + "\t" + "Commercial|";
                strTemp += "#306;306 Medical" + "\t" + "Commercial|";
                strTemp += "#307;307 Office" + "\t" + "Commercial|";
                strTemp += "#308;308 Retail" + "\t" + "Commercial|";
                strTemp += "#309;309 Automotive" + "\t" + "Commercial|";
                strTemp += "#310;310 Marina" + "\t" + "Commercial|";
                strTemp += "#311;311 Warehouse" + "\t" + "Commercial|";
                strTemp += "#312;312 Hotel/Motel/Inn" + "\t" + "Commercial|";
                strTemp += "#313;313 Nursing Home" + "\t" + "Commercial|";
                strTemp += "#314;314 Shopping Mall" + "\t" + "Commercial|";
                strTemp += "#320;320 Other" + "\t" + "Commercial|";
                strTemp += "#401;401 Gas and Oil" + "\t" + "Industrial|";
                strTemp += "#402;402 Utility" + "\t" + "Industrial|";
                strTemp += "#403;403 Gravel Pit" + "\t" + "Industrial|";
                strTemp += "#404;404 Lumber/Saw Mill" + "\t" + "Industrial|";
                strTemp += "#405;405 Pulp/Paper Mill" + "\t" + "Industrial|";
                strTemp += "#406;406 Light Manufacture" + "\t" + "Industrial|";
                strTemp += "#407;407 Heavy Manufacture" + "\t" + "Industrial|";
                strTemp += "#420;420 Manufacture Other" + "\t" + "Industrial|";
                strTemp += "#501;501 Government" + "\t" + "Misc Codes|";
                strTemp += "#502;502 Condominium" + "\t" + "Misc Codes|";
                strTemp += "#503;503 Timeshare Unit" + "\t" + "Misc Codes|";
                strTemp += "#504;504 Non-Profit" + "\t" + "Misc Codes|";
                strTemp += "#505;505 Mobile Home Park" + "\t" + "Misc Codes|";
                strTemp += "#506;506 Airport" + "\t" + "Misc Codes|";
                strTemp += "#507;507 Conservation" + "\t" + "Misc Codes|";
                strTemp += "#508;508 Current Use Classification" + "\t" + "Misc Codes|";
                strTemp += "#520;520 Other" + "\t" + "Misc Codes";
            }
            gridPropertyCode.ColComboList(0, strTemp);
            gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(0));
        }

        private void SetupGridExempts()
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strTemp = "";
            // If GridExempts.Visible = False Then
            // Exit Sub
            // End If
            GridExempts.Cols = 2;
            // third column holds the code
            GridExempts.Rows = 4;
            // .ColHidden(3) = True
            GridExempts.TextMatrix(0, CNSTEXEMPTCOLCODE, "Exemption");
            GridExempts.TextMatrix(0, CNSTEXEMPTCOLPERCENT, "Percent");
            GridExempts.TextMatrix(1, CNSTEXEMPTCOLPERCENT, "100");
            GridExempts.TextMatrix(2, CNSTEXEMPTCOLPERCENT, "100");
            GridExempts.TextMatrix(3, CNSTEXEMPTCOLPERCENT, "100");
            clsTemp.OpenRecordset("select * from exemptcode order by DESCRIPTION", modGlobalVariables.strREDatabase);
            strTemp = "#-1;No Exempt|";
            while (!clsTemp.EndOfFile())
            {
                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                strTemp += "#" + clsTemp.Get_Fields("code") + ";" + clsTemp.Get_Fields_String("description") + "\t" + clsTemp.Get_Fields("code") + "|";
                clsTemp.MoveNext();
            }
            if (strTemp != string.Empty)
            {
                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
            }
            GridExempts.ColComboList(CNSTEXEMPTCOLCODE, strTemp);
            //GridExempts.ShowComboButton = true;
            GridExempts.TextMatrix(1, CNSTEXEMPTCOLCODE, FCConvert.ToString(CNSTNOEXEMPT));
            GridExempts.TextMatrix(2, CNSTEXEMPTCOLCODE, FCConvert.ToString(CNSTNOEXEMPT));
            GridExempts.TextMatrix(3, CNSTEXEMPTCOLCODE, FCConvert.ToString(CNSTNOEXEMPT));
        }

        private void SetupSaleGrid()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strList = "";
            string strData = "";
            string strCode = "";
            try
            {
                // On Error GoTo ErrorHandler
                SaleGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeRestrictColumns;
                SaleGrid.MergeRow(0, true);
                //SaleGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                //FC:FINAL:DSE Move text over the middle column
                //SaleGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0, 0, 5, "Sale Data");
                SaleGrid.TextMatrix(0, CNSTSALECOLFINANCING, "Sale Data");
                SaleGrid.TextMatrix(1, CNSTSALECOLPRICE, "Price");
                SaleGrid.TextMatrix(1, CNSTSALECOLDATE, "Date");
                SaleGrid.TextMatrix(1, CNSTSALECOLSALE, "Type");
                SaleGrid.TextMatrix(1, CNSTSALECOLFINANCING, "Financing");
                SaleGrid.TextMatrix(1, CNSTSALECOLVERIFIED, "Verified");
                SaleGrid.TextMatrix(1, CNSTSALECOLVALIDITY, "Validity");
                SaleGrid.ColEditMask(CNSTSALECOLDATE, "##/##/####");
                clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1351 and 1359 order by crecordnumber", modGlobalVariables.strREDatabase);
                strList = "#0;N/A";
                while (!clsLoad.EndOfFile())
                {
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350) + " ";
                    }
                    else
                    {
                        strCode = "";
                    }
                    strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
                    clsLoad.MoveNext();
                }
                SaleGrid.ColComboList(CNSTSALECOLSALE, strList);
                clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1361 and 1369 order by crecordnumber", modGlobalVariables.strREDatabase);
                strList = "#0;N/A";
                while (!clsLoad.EndOfFile())
                {
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360) + " ";
                    }
                    else
                    {
                        strCode = "";
                    }
                    strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
                    clsLoad.MoveNext();
                }
                SaleGrid.ColComboList(CNSTSALECOLFINANCING, strList);
                clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1381 and 1389 order by crecordnumber", modGlobalVariables.strREDatabase);
                strList = "#0;N/A";
                while (!clsLoad.EndOfFile())
                {
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380) + " ";
                    }
                    else
                    {
                        strCode = "";
                    }
                    strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
                    clsLoad.MoveNext();
                }
                SaleGrid.ColComboList(CNSTSALECOLVALIDITY, strList);
                clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1371 and 1379 order by crecordnumber", modGlobalVariables.strREDatabase);
                strList = "#0;N/A";
                while (!clsLoad.EndOfFile())
                {
                    if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                    {
                        strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370) + " ";
                    }
                    else
                    {
                        strCode = "";
                    }
                    strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
                    clsLoad.MoveNext();
                }
                SaleGrid.ColComboList(CNSTSALECOLVERIFIED, strList);
                SaleGrid.Row = 2;
                SaleGrid.Col = 0;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupSaleGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void FillSaleGrid()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strList = "";
            string strData = "";
            string strCode = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!(theAccount == null))
                {
                    SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(theAccount.SalePrice, "#,###,###,##0"));
                    if (Information.IsDate(theAccount.SaleDate))
                    {
                        if (theAccount.SaleDate.ToOADate() != 0)
                        {
                            SaleGrid.TextMatrix(2, CNSTSALECOLDATE, Strings.Format(theAccount.SaleDate, "MM/dd/yyyy"));
                        }
                        else
                        {
                            SaleGrid.TextMatrix(2, CNSTSALECOLDATE, "");
                        }
                    }
                    else
                    {
                        SaleGrid.TextMatrix(2, CNSTSALECOLDATE, "");
                    }
                    SaleGrid.TextMatrix(2, CNSTSALECOLSALE, FCConvert.ToString(theAccount.SaleType));
                    SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING, FCConvert.ToString(theAccount.SaleFinancing));
                    SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY, FCConvert.ToString(theAccount.SaleValidity));
                    SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED, FCConvert.ToString(theAccount.SaleVerified));
                    if (!modREMain.Statics.boolShortRealEstate)
                    {
                        SaleGrid.Row = 2;
                        SaleGrid.Col = 0;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillSaleGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetupcmbPrintSize()
        {
            cmbPrintSize.Clear();
            cmbPrintSize.AddItem("Size of Picture");
            cmbPrintSize.AddItem("Fit to Page");
            cmbPrintSize.AddItem("4 X 6");
            cmbPrintSize.AddItem("3 X 5");
            int intReturn = 0;
            intReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("PicturePrintSize", "RE"))));
            cmbPrintSize.SelectedIndex = intReturn;
        }

        private void SetupcmbCardNumber()
        {
            cmbCardNumber.Clear();
            clsDRWrapper tLoad = new clsDRWrapper();
            tLoad.OpenRecordset("select rscard from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
            if (!tLoad.EndOfFile())
            {
                // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(Math.Round(Conversion.Val(tLoad.RecordCount())));
                for (var x = 1; x <= modGNBas.Statics.gintMaxCards; x++)
                {
                    cmbCardNumber.AddItem(x.ToString());
                    cmbCardNumber.ItemData(cmbCardNumber.NewIndex, x);
                }
                //while (!tLoad.EndOfFile())
                //{
                //    cmbCardNumber.AddItem(tLoad.Get_Fields_Int32("rscard").ToString());
                //    tLoad.MoveNext();
                //}
                cmbCardNumber.Text = cmbCardNumber.Items[0].ToString();
            }

        }

        private void SetupBPGrid()
        {
            BPGrid.Rows = 1;
            BPGrid.TextMatrix(0, BPCheckCol, "");
            BPGrid.TextMatrix(0, BPBookCol, "Book");
            BPGrid.TextMatrix(0, BPPageCol, "Page");
            BPGrid.TextMatrix(0, BPSaleDateCol, "Date");
            BPGrid.ColEditMask(BPSaleDateCol, "##/##/####");
            BPGrid.ColDataType(BPCheckCol, FCGrid.DataTypeSettings.flexDTBoolean);
            //FC:FINAL:CHN - issue #1632: Account tab changes: change Grid alignments.
            BPGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            BPGrid.ColAlignment(BPBookCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            BPGrid.ColAlignment(BPPageCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void ResizeBPGrid()
        {
            int GridWidth = 0;
            GridWidth = BPGrid.Width * FCScreen.TwipsPerPixelX;
            BPGrid.ColWidth(BPCheckCol, FCConvert.ToInt32(0.1 * GridWidth));
            BPGrid.ColWidth(BPBookCol, FCConvert.ToInt32(0.243 * GridWidth));
            BPGrid.ColWidth(BPPageCol, FCConvert.ToInt32(0.243 * GridWidth));
        }

        private void FillBPGrid()
        {
            clsDRWrapper clsBP = new clsDRWrapper();
            string strWhere;
            string strBP = "";
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                strBP = "BookPage";
            }
            else
            {
                strBP = "SRBookPage";
            }
            strWhere = " order by line";
            if (!modGlobalVariables.Statics.boolRegRecords)
                strWhere = " and saledate = #" + modGlobalVariables.Statics.gcurrsaledate + "#";
            clsBP.OpenRecordset("select * from " + strBP + " where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = 1" + strWhere, modGlobalVariables.strREDatabase);
            if (!clsBP.EndOfFile())
            {
                BPGrid.Rows = 1;
                while (!clsBP.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                    BPGrid.AddItem(clsBP.Get_Fields_Boolean("Current") + "\t" + clsBP.Get_Fields("book") + "\t" + clsBP.Get_Fields("Page") + "\t" + clsBP.Get_Fields_String("BPDate"));
                    clsBP.MoveNext();
                }
                BPGrid.AddItem(FCConvert.ToString(true));
            }
            else
            {
                BPGrid.Rows = 1;
                BPGrid.AddItem(FCConvert.ToString(true));
            }
            BPGrid.Col = 1;
            BPGrid.Row = BPGrid.Rows - 1;
            AutoPositionBPGrid();
        }

        private void SetupIncomeGrids()
        {
            GridExpenses.Rows = 2;
            GridExpenses.ExtendLastCol = true;
            GridExpenses.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            GridExpenses.TextMatrix(0, 0, "Expenses");
            //GridExpenses.TextMatrix(0, 1, "Expenses");
            //GridExpenses.TextMatrix(0, 2, "Expenses");
            //GridExpenses.TextMatrix(0, 3, "Expenses");
            //GridExpenses.MergeRow(0, true);
            GridExpenses.MergeRow(0, true, 0, 3);
            GridExpenses.MergeRow(1, true);
            //GridExpenses.MergeCompare = FCGrid.MergeCompareSettings.flexMCTrimNoCase;
            GridExpenses.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
            GridExpenses.ExtendLastCol = true;
            GridExpenses.TextMatrix(1, 0, "Code");
            GridExpenses.TextMatrix(1, 1, "% Gross");
            GridExpenses.TextMatrix(1, 2, "$/SF");
            GridExpenses.TextMatrix(1, 3, "Amount");
            Grid1.Rows = 2;
            Grid1.FixedRows = 2;
            Grid1.Cols = 7;
            Grid1.ExtendLastCol = true;
            //Grid1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            Grid1.TextMatrix(0, 0, "Occupancy");
            Grid1.TextMatrix(1, 0, "Code");
            Grid1.TextMatrix(0, 1, "Square");
            Grid1.TextMatrix(1, 1, "Footage");
            Grid1.TextMatrix(0, 2, "Gross");
            Grid1.TextMatrix(1, 2, "Or Net");
            Grid1.TextMatrix(0, 3, "Actual");
            Grid1.TextMatrix(1, 3, "Rent / Mo.");
            Grid1.TextMatrix(0, 4, "Rent / SF / Year");
            Grid1.TextMatrix(0, 5, "Rent / SF / Year");
            Grid1.MergeRow(0, true, 4, 5);
            //Grid1.MergeCompare = FCGrid.MergeCompareSettings.flexMCTrimNoCase;
            Grid1.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
            Grid1.TextMatrix(1, 4, "Actual");
            Grid1.TextMatrix(1, 5, "Market");
            Grid1.TextMatrix(0, 6, "Annual");
            Grid1.TextMatrix(1, 6, "Market Rent");
            Grid1.ColComboList(colGN, "Gross|Net");
            GridValues.Rows = 9;
            GridValues.Cols = 3;
            GridValues.ExtendLastCol = true;
            //FC:FINAL:AM:#1644 - remove font settings
            //GridValues.Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 2, 8, 2, "Courier New");
            //GridValues.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 2, 8, 2, 10);
            GridValues.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, 4, 1, GridValues.BackColorFixed);
            GridValues.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 8, 1, GridValues.BackColorFixed);
            GridValues.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, GridValues.BackColorFixed);
            GridValues.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 0, 1, 2, Information.RGB(200, 0, 0));
            GridValues.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 3, 0, 3, 2, Information.RGB(200, 0, 0));
            GridValues.TextMatrix(0, 0, "Potential Gross Income");
            GridValues.TextMatrix(1, 0, "Less Vacancy");
            GridValues.TextMatrix(2, 0, "Effective Gross Income");
            GridValues.TextMatrix(3, 0, "Less Expenses");
            GridValues.TextMatrix(4, 0, "Effective Net Income");
            GridValues.TextMatrix(5, 0, "   Overall Rate  (Percent)");
            GridValues.TextMatrix(6, 0, "   Effective Tax Rate  (Percent)");
            GridValues.TextMatrix(7, 0, "Capitalization Rate");
            GridValues.TextMatrix(8, 0, "Indicated Market Value");
            GridValues.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

        private void ResizeDwellingGrids()
        {
            int GridWidth = 0;
            GridWidth = GridDwelling1.WidthOriginal;
            GridDwelling1.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.75));
            GridDwelling1.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.25));
            //GridDwelling1.Height = (GridDwelling1.RowHeight(0) * GridDwelling1.Rows + 60) / FCScreen.TwipsPerPixelY;
            GridWidth = GridDwelling2.WidthOriginal;
            GridDwelling2.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.75));
            GridDwelling2.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.25));
            //GridDwelling2.Height = (GridDwelling2.RowHeight(0) * GridDwelling2.Rows + 60) / FCScreen.TwipsPerPixelY;
            GridWidth = GridDwelling3.WidthOriginal;
            GridDwelling3.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.75));
            GridDwelling3.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.25));
            //GridDwelling3.Height = (GridDwelling3.RowHeight(0) * GridDwelling3.Rows + 60)/FCScreen.TwipsPerPixelY;
        }

        private void ResizeIncomeGrids()
        {
            int GridWidth = 0;
            GridWidth = GridExpenses.WidthOriginal;
            GridExpenses.ColWidth(0, FCConvert.ToInt32(0.15 * GridWidth));
            GridExpenses.ColWidth(1, FCConvert.ToInt32(0.26 * GridWidth));
            GridExpenses.ColWidth(2, FCConvert.ToInt32(0.26 * GridWidth));
            GridExpenses.ColWidth(3, FCConvert.ToInt32(0.29 * GridWidth));
            GridWidth = Grid1.WidthOriginal;
            Grid1.ColWidth(0, FCConvert.ToInt32(0.11 * GridWidth));
            Grid1.ColWidth(1, FCConvert.ToInt32(0.15 * GridWidth));
            Grid1.ColWidth(2, FCConvert.ToInt32(0.11 * GridWidth));
            Grid1.ColWidth(3, FCConvert.ToInt32(0.15 * GridWidth));
            Grid1.ColWidth(4, FCConvert.ToInt32(0.14 * GridWidth));
            Grid1.ColWidth(5, FCConvert.ToInt32(0.14 * GridWidth));
            Grid1.ColWidth(6, FCConvert.ToInt32(0.16 * GridWidth));
            GridWidth = GridValues.WidthOriginal;
            GridValues.ColWidth(0, FCConvert.ToInt32(0.6 * GridWidth));
            GridValues.ColWidth(1, FCConvert.ToInt32(0.15 * GridWidth));
            GridValues.ColWidth(2, FCConvert.ToInt32(0.25 * GridWidth));
        }

        private void AutoPositionBPGrid()
        {
            BPGrid.ShowCell(BPGrid.Rows - 1, 0);
        }

        private void ShowAccount()
        {
            try
            {
                // On Error GoTo ErrorHandler
                lblMRHLAcctNum.Text = "Account   " + FCConvert.ToString(Conversion.Val(FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " "));
                if (!(theAccount == null))
                {
                    string strCRLF = "";
                    cParty tParty = new cParty();
                    strCRLF = "";
                    string strAddress = "";
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        cPartyAddress tAddr;
                        txtOwnerID.Text = FCConvert.ToString(theAccount.OwnerID);
                        txt2ndOwnerID.Text = FCConvert.ToString(theAccount.SecOwnerID);
                        if (theAccount.OwnerID > 0)
                        {
                            tParty = theAccount.OwnerParty;
                            lblOwner1.Text = tParty.FullNameLastFirst;
                            strAddress = "";
                            tAddr = tParty.GetPrimaryAddress();
                            // Set tAddr = tParty.GetAddress("RE", theAccount.Account, Format(Now, "MM/dd/yyyy"))
                            if (!(tAddr == null))
                            {
                                strAddress = tAddr.GetFormattedAddress();
                            }
                            lblAddress.Text = strAddress;
                        }
                        else
                        {
                            lblOwner1.Text = "";
                            lblAddress.Text = "";
                        }
                        if (theAccount.SecOwnerID > 0)
                        {
                            tParty = theAccount.SecOwnerParty;
                            if (!(tParty == null))
                            {
                                lbl2ndOwner.Text = tParty.FullNameLastFirst;
                            }
                            else
                            {
                                lbl2ndOwner.Text = "";
                            }
                        }
                        else
                        {
                            lbl2ndOwner.Text = "";
                        }
                    }

                    txtDeedName1.Text = theAccount.DeedName1;
                    txtDeedName2.Text = theAccount.DeedName2;
                    if (theAccount.TaxAcquired)
                    {
                        chkTaxAcquired.CheckState = Wisej.Web.CheckState.Checked;
                    }
                    else
                    {
                        chkTaxAcquired.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                    if (theAccount.InBankruptcy)
                    {
                        chkBankruptcy.CheckState = Wisej.Web.CheckState.Checked;
                    }
                    else
                    {
                        chkBankruptcy.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                    if (theAccount.RevocableTrust)
                    {
                        chkRLivingTrust.CheckState = Wisej.Web.CheckState.Checked;
                    }
                    else
                    {
                        chkRLivingTrust.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                    txtMRRSPrevMaster.Text = theAccount.PreviousOwner;
                    gridTranCode.TextMatrix(0, 0, FCConvert.ToString(theAccount.TranCode));
                    if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        SetupGridNeighborhood();
                        SetupGridZone();
                        SetupGridUtilities();
                        SetupGridStreet();
                        SetupGridSecZone();
                        SetupGridTopography();
                    }
                    FillSaleGrid();
                    FillExemptGrid();
                }
                return;
            }
            catch
            {
                // ErrorHandler:
            }
        }

        private void ShowCard(bool boolReShow = false)
        {
            try
            {
                // On Error GoTo ErrorHandler
                // vbPorter upgrade warning: x As short --> As int	OnWrite(double, short)
                int x;
                modGlobalVariables.Statics.HoldDwelling = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "");
                if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C")
                {
                    mnuDwellingSqft.Text = "Commercial";
                }
                txtMRRSLocnumalph.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " ");
                txtMRRSLocStreet.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + " ");
                txtMRRSLocapt.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocapt") + " ");
                txtMRRSRef1.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + " ";
                txtMRRSRef2.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref2") + " ";
                gridLandCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rilandcode") + "")));
                gridBldgCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ribldgcode") + "")));
                gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("propertycode") + "")));
                lblMRRLLANDVAL[28].Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + ""));
                if (Conversion.Val(lblMRRLLANDVAL[28].Text) != 0)
                {
                    lblMRRLLANDVAL[28].Text = "$" + Strings.Format(lblMRRLLANDVAL[28].Text, "###,###,##0");
                }
                lblMRRLBLDGVAL[29].Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + ""));
                if (Conversion.Val(lblMRRLBLDGVAL[29].Text) != 0)
                {
                    lblMRRLBLDGVAL[29].Text = "$" + Strings.Format(lblMRRLBLDGVAL[29].Text, "###,###,##0");
                }
                lblMRRLEXEMPTION[30].Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption") + ""));
                if (Conversion.Val(lblMRRLEXEMPTION[30].Text) != 0)
                {
                    lblMRRLEXEMPTION[30].Text = "$" + Strings.Format(lblMRRLEXEMPTION[30].Text, "###,###,##0");
                }
                lblTaxable.Text = "$" + Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + "") - Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption") + ""), "#,###,##0");
                txtMRRSMAPLOT.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + " ");
                GridNeighborhood.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood") + "")));
                txtMRPIStreetCode.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("pistreetcode") + "");
                // TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
                txtMRPIXCoord.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields("pixcoord") + "");
                // TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
                txtMRPIYCoord.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields("piycoord") + "");
                GridZone.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pizone") + "")));
                GridSecZone.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone") + "")));
                if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("zoneoverride")))
                {
                    chkZoneOverride.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkZoneOverride.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                GridTopography.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pitopography1") + "")));
                GridTopography.TextMatrix(1, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pitopography2") + "")));
                GridUtilities.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piutilities1") + "")));
                GridUtilities.TextMatrix(1, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piutilities2") + "")));
                GridUtilities.Row = 0;
                GridTopography.Row = 0;
                GridStreet.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pistreet") + "")));
                txtMRPIOpen1.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "");
                txtMRPIOpen2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "");
                modGNBas.Statics.Resp1 = 0;
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("piland1type") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland1unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland1unitsb") + "";
                    FixLands();
                    // txtMRPIInfCode1(0).Text = Trim(MR.fields("piland1infcode") & "")
                    GridLand.TextMatrix(1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland1infcode") + ""));
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland1inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland1inf", "100");
                    }
                    if (Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland1inf") + "")) > 0)
                    {
                        // txtMRPILand1Inf(0).Text = Trim(MR.fields("piland1inf") & "")
                        GridLand.TextMatrix(1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland1inf") + ""));
                    }
                    else
                    {
                        // txtMRPILand1Inf(0).Text = "0"
                        GridLand.TextMatrix(1, 2, "0");
                    }
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("piland2type") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland2unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland2unitsb") + "";
                    FixLands();
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland2inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland2inf", "100");
                    }
                    if (Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland2inf") + "")) > 0)
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland2inf") + ""));
                    }
                    else
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, "0");
                    }
                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland2infcode") + ""));
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("piland3type") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland3unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland3unitsb") + "";
                    FixLands();
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland3inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland3inf", "100");
                    }
                    if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland3inf") + "") > 0)
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland3inf") + ""));
                    }
                    else
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, "0");
                    }
                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland3infcode") + ""));
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("piland4type") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland4unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland4unitsb") + "";
                    FixLands();
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland4inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland4inf", "100");
                    }
                    if (Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland4inf") + "")) > 0)
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland4inf") + ""));
                    }
                    else
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, "0");
                    }
                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland4infcode") + ""));
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("PILAND5TYPE") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland5unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland5unitsb") + "";
                    FixLands();
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland5inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland5inf", "100");
                    }
                    if (Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland5inf") + "")) > 0)
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland5inf") + ""));
                    }
                    else
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, "0");
                    }
                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland5infcode") + ""));
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("piland6type") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland6unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland6unitsb") + "";
                    FixLands();
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland6inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland6inf", "100");
                    }
                    if (Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland6inf") + "")) > 0)
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland6inf") + ""));
                    }
                    else
                    {
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, "0");
                    }
                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland6infcode") + ""));
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                modGNBas.Statics.Resp2 = modDataTypes.Statics.MR.Get_Fields_Int32("piland7type") + "";
                if (Conversion.Val(modGNBas.Statics.Resp2) > 0)
                {
                    modGNBas.Statics.Resp3 = modDataTypes.Statics.MR.Get_Fields_String("piland7unitsa") + "";
                    modGNBas.Statics.Resp4 = modDataTypes.Statics.MR.Get_Fields_String("piland7unitsb") + "";
                    FixLands();
                    if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland7inf") + "") == string.Empty)
                    {
                        modDataTypes.Statics.MR.Set_Fields("piland7inf", "100");
                    }
                    if (Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland7inf") + "")) > 0)
                    {
                        //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                        //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 2, Strings.Trim(modDataTypes.MR.Get_Fields("piland7inf") + ""));
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Double("piland7inf") + ""));
                    }
                    else
                    {
                        //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                        //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 2, "0");
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 2, "0");
                    }
                    //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                    //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 3, Strings.Trim(modDataTypes.MR.Get_Fields("piland7infcode") + ""));
                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 3, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piland7infcode") + ""));
                    modGNBas.Statics.Resp1 = Conversion.Val(modGNBas.Statics.Resp1) + 1;
                }
                for (x = FCConvert.ToInt32(Conversion.Val(modGNBas.Statics.Resp1) + 1); x <= 7; x++)
                {
                    GridLand.Col = 1;
                    GridLand.Row = x;
                    GridLand.TextMatrix(x, 3, "0");
                    GridLand.TextMatrix(x, 2, "100");
                    GridLand.TextMatrix(x, 1, "");
                    GridLand.TextMatrix(x, 0, "");
                    /*? 165 */
                }
                // x
                GridLand.Col = 0;
                GridLand.Row = 1;
                CalculateAcres();
                FillDwellingScreen();
                FillOutBuildingScreen();
                FillCommercialScreen();
                FillBPGrid();
                FillSaleGrid();
                EnableDisableExemptGrid();
                // Call LoadPictureInformation
                if (!boolReShow)
                {
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        LoadIncomeApproach(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
                        UpdateFlags(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
                    }
                    int counter;
                    FillControlInformationClass();
                    // This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
                    FillControlInformationClassFromGrid();
                    // This will initialize the old data so we have somethign to compare the new data against when you save
                    for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
                    {
                        clsControlInfo[counter].FillOldValue(this);
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ShowCard", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void LoadAccount(int lngAcct)
        {
            try
            {
                // On Error GoTo ErrorHandler
                cREAccountController tCont = new cREAccountController();
                tCont.LoadAccount(ref theAccount, lngAcct);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void LoadCard(int lngAcct, int intCard)
        {
            try
            {
                // On Error GoTo ErrorHandler
                modGlobalVariables.Statics.gintLastAccountNumber = lngAcct;
                modGNBas.Statics.gintCardNumber = intCard;
                clsDRWrapper temp = modDataTypes.Statics.MR;
                modREMain.OpenMasterTable(ref temp, lngAcct, intCard);
                modDataTypes.Statics.MR = temp;
                txtMRRSRef1.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref1");
                txtMRRSRef2.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref2");
                temp = modDataTypes.Statics.DWL;
                modREMain.OpenDwellingTable_27(ref temp, lngAcct, intCard, theAccount.AccountID);
                modDataTypes.Statics.DWL = temp;
                temp = modDataTypes.Statics.OUT;
                modREMain.OpenOutBuildingTable(ref temp, lngAcct, intCard);
                modDataTypes.Statics.OUT = temp;
                temp = modDataTypes.Statics.CMR;
                modREMain.OpenCOMMERCIALTable(ref temp, lngAcct, intCard);
                modDataTypes.Statics.CMR = temp;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadCard", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void FixLands()
        {
            object hold;
            string ZERO;
            ZERO = "";
            //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
            //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 0, modGNBas.Resp2 + "");
            GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 0, modGNBas.Statics.Resp2 + "");
            //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
            //switch (FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 0)))
            switch (FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 0))))
            {
                case 0:
                    {
                        //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                        //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 1, "");
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 1, "");
                        break;
                    }
                case 99:
                    {
                        //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                        //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 1, "");
                        GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 1, "");
                        break;
                    }
                default:
                    {
                        //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                        //modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 0))));
                        modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 0))));
                        switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                        {
                            case modREConstants.CNSTLANDTYPEFRONTFOOT:
                            case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                                {
                                    //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                                    //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 1, modGNBas.Resp3 + "x" + modGNBas.Resp4);
                                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 1, modGNBas.Statics.Resp3 + "x" + modGNBas.Statics.Resp4);
                                    break;
                                }
                            case modREConstants.CNSTLANDTYPESQUAREFEET:
                                {
                                    //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int
                                    //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 1, Strings.Format(FCConvert.ToDouble(modGNBas.Resp3 + FCConvert.ToDouble(modGNBas.Resp4, "#,###,##0"));
                                    //FC:FINAL:MSH - i.issue #1224: invalid cast exception
                                    //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1) + 1, 1, Strings.Format(FCConvert.ToDouble(modGNBas.Resp3 + FCConvert.ToDouble(modGNBas.Resp4, "#,###,##0"));
                                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 1, Strings.Format(FCConvert.ToString(modGNBas.Statics.Resp3) + FCConvert.ToString(modGNBas.Statics.Resp4), "#,###,##0"));
                                    break;
                                }
                            case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                            case modREConstants.CNSTLANDTYPEACRES:
                            case modREConstants.CNSTLANDTYPESITE:
                            case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                            case modREConstants.CNSTLANDTYPELINEARFEET:
                            case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                {
                                    //FC:FINAL:MSH - i.issue #1066: incorrect converting from object to int. Conversion to int unnecessary, because it will return results different from results in original app
                                    //GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Resp1 + 1, 1, Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(FCConvert.ToDouble(modGNBas.Resp3) + FCConvert.ToDouble(modGNBas.Resp4)))) / 100, "0.00"));
                                    GridLand.TextMatrix(FCConvert.ToInt32(modGNBas.Statics.Resp1) + 1, 1, Strings.Format(Conversion.Val(FCConvert.ToString(modGNBas.Statics.Resp3) + FCConvert.ToString(modGNBas.Statics.Resp4)) / 100, "0.00"));
                                    break;
                                }
                        }
                        //end switch
                        break;
                    }
            }
            //end switch
        }

        private void CalculateAcres()
        {
            int LandType = 0;
            int fnx;
            double Acreage;
            // vbPorter upgrade warning: hAcreage As double	OnWrite(short, string, double)
            double hAcreage = 0;
            double softtotal;
            double hardtotal;
            double mixedtotal;
            try
            {
                // On Error GoTo ErrorHandler
                txtMRPIAcres.Text = "0";
                Acreage = 0;
                softtotal = 0;
                hardtotal = 0;
                mixedtotal = 0;
                for (fnx = 1; fnx <= 7; fnx++)
                {
                    hAcreage = 0;
                    LandType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridLand.TextMatrix(fnx, 0))));
                    if (Conversion.Val(GridLand.TextMatrix(fnx, 1)) != 0 && LandType != 0 && LandType != 99)
                    {
                        if (modGlobalVariables.Statics.LandTypes.FindCode(LandType))
                        {
                            switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                            {
                                case modREConstants.CNSTLANDTYPEFRONTFOOT:
                                case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                                    {
                                        hAcreage = FCConvert.ToDouble(Strings.Format((Conversion.Val(Strings.Mid(GridLand.TextMatrix(fnx, 1), 1, 3)) * Conversion.Val(Strings.Right(GridLand.TextMatrix(fnx, 1), 3)) / 43560), "#,##0.00"));
                                        break;
                                    }
                                case modREConstants.CNSTLANDTYPESQUAREFEET:
                                    {
                                        hAcreage = FCConvert.ToDouble(Strings.Format((FCConvert.ToDouble(GridLand.TextMatrix(fnx, 1)) / 43560), "#,##0.00"));
                                        break;
                                    }
                                case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                                case modREConstants.CNSTLANDTYPEACRES:
                                case modREConstants.CNSTLANDTYPESITE:
                                case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                                case modREConstants.CNSTLANDTYPELINEARFEET:
                                case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                    {
                                        hAcreage = FCConvert.ToDouble(Strings.Format(FCConvert.ToDouble(GridLand.TextMatrix(fnx, 1)), "#,##0.00"));
                                        break;
                                    }
                            }
                            //end switch
                            if (!modGlobalVariables.Statics.LandTypes.IsFarmTreeGrowth())
                            {
                                if (modGlobalVariables.Statics.LandTypes.IsSoftWood())
                                {
                                    softtotal += hAcreage;
                                }
                                else if (modGlobalVariables.Statics.LandTypes.IsHardWood())
                                {
                                    hardtotal += hAcreage;
                                }
                                else if (modGlobalVariables.Statics.LandTypes.IsMixedWood())
                                {
                                    mixedtotal += hAcreage;
                                }
                            }
                            if (LandType != 99)
                            {
                                if (modGlobalVariables.Statics.LandTypes.Factor > 0.01)
                                {
                                    if (LandType > 0)
                                        hAcreage *= modGlobalVariables.Statics.LandTypes.Factor;
                                }
                                else
                                {
                                    hAcreage = 0;
                                }
                            }
                            Acreage += hAcreage;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                // fnx
                txtMRPIAcres.Text = Strings.Format(Acreage, "#,##0.00");
                modDataTypes.Statics.MR.Set_Fields("rshard", hardtotal);
                modDataTypes.Statics.MR.Set_Fields("rssoft", softtotal);
                modDataTypes.Statics.MR.Set_Fields("rsmixed", mixedtotal);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalculateAcres", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void FillDwellingScreen()
        {
            string hold = "";
            string strTemp = "";
            GridDwelling1.TextMatrix(0, 0, "Building Style");
            GridDwelling1.TextMatrix(0, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("distyle") + ""));
            GridDwelling1.TextMatrix(1, 0, "Dwelling Units");
            GridDwelling1.TextMatrix(1, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsdwelling") + ""));
            GridDwelling1.TextMatrix(2, 0, "Other Units");
            GridDwelling1.TextMatrix(2, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsother") + ""));
            GridDwelling1.TextMatrix(3, 0, "Stories");
            GridDwelling1.TextMatrix(3, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("distories") + ""));
            GridDwelling1.TextMatrix(4, 0, "Exterior Walls");
            GridDwelling1.TextMatrix(4, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diextwalls") + ""));
            GridDwelling1.TextMatrix(5, 0, "Roof Surface");
            GridDwelling1.TextMatrix(5, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diroof") + ""));
            GridDwelling1.TextMatrix(6, 0, "SF Masonry Trim");
            GridDwelling1.TextMatrix(6, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("disfmasonry") + ""));
            clsDRWrapper temp = modDataTypes.Statics.CR;
            modREMain.OpenCRTable(ref temp, 1520);
            modDataTypes.Statics.CR = temp;
            if (Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "") != string.Empty)
            {
                GridDwelling1.TextMatrix(7, 0, modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "");
            }
            else
            {
                GridDwelling1.TextMatrix(7, 0, "Open 3");
            }
            GridDwelling1.TextMatrix(7, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen3") + ""));
            temp = modDataTypes.Statics.CR;
            modREMain.OpenCRTable(ref temp, 1530);
            modDataTypes.Statics.CR = temp;
            if (Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "") != string.Empty)
            {
                GridDwelling1.TextMatrix(8, 0, modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "");
            }
            else
            {
                GridDwelling1.TextMatrix(8, 0, "Open 4");
            }
            GridDwelling1.TextMatrix(8, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen4") + ""));
            GridDwelling1.TextMatrix(9, 0, "Year Built");
            GridDwelling1.TextMatrix(9, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diyearbuilt") + ""));
            GridDwelling1.TextMatrix(10, 0, "Year Remodeled");
            GridDwelling1.TextMatrix(10, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diyearremodel") + ""));
            GridDwelling1.TextMatrix(11, 0, "Foundation");
            GridDwelling1.TextMatrix(11, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("difoundation") + ""));
            GridDwelling1.TextMatrix(12, 0, "Basement");
            GridDwelling1.TextMatrix(12, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmt") + ""));
            GridDwelling1.TextMatrix(13, 0, "Bsmt Garage");
            GridDwelling1.TextMatrix(13, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtgar") + ""));
            GridDwelling1.TextMatrix(14, 0, "Wet Basement");
            GridDwelling1.TextMatrix(14, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diwetbsmt") + ""));
            GridDwelling2.TextMatrix(0, 0, "SF Basement Living");
            GridDwelling2.TextMatrix(0, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("disfbsmtliving") + ""));
            GridDwelling2.TextMatrix(1, 0, "Finished Bsmt Grade");
            GridDwelling2.TextMatrix(1, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1") + ""));
            GridDwelling2.TextMatrix(2, 0, "Finished Bsmt Factor");
            GridDwelling2.TextMatrix(2, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2") + ""));
            // 
            temp = modDataTypes.Statics.CR;
            modREMain.OpenCRTable(ref temp, 1570);
            modDataTypes.Statics.CR = temp;
            if (Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "") != string.Empty)
            {
                GridDwelling2.TextMatrix(3, 0, Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + ""));
            }
            else
            {
                GridDwelling2.TextMatrix(3, 0, "Open 5");
            }
            GridDwelling2.TextMatrix(3, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen5") + ""));
            GridDwelling2.TextMatrix(4, 0, "Heat Type");
            GridDwelling2.TextMatrix(4, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diheat") + ""));
            GridDwelling2.TextMatrix(5, 0, "Percent Heated");
            GridDwelling2.TextMatrix(5, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctheat") + ""));
            GridDwelling2.TextMatrix(6, 0, "Cool Type");
            GridDwelling2.TextMatrix(6, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dicool") + ""));
            GridDwelling2.TextMatrix(7, 0, "Percent Cooled");
            GridDwelling2.TextMatrix(7, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctcool") + ""));
            GridDwelling2.TextMatrix(8, 0, "Kitchen Style");
            GridDwelling2.TextMatrix(8, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dikitchens") + ""));
            GridDwelling2.TextMatrix(9, 0, "Bath(s) Style");
            GridDwelling2.TextMatrix(9, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dibaths") + ""));
            GridDwelling2.TextMatrix(10, 0, "No. of Rooms");
            GridDwelling2.TextMatrix(10, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("DIROOMS") + ""));
            GridDwelling2.TextMatrix(11, 0, "No. of Bedrooms");
            GridDwelling2.TextMatrix(11, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dibedrooms") + ""));
            GridDwelling2.TextMatrix(12, 0, "No. of Full Baths");
            GridDwelling2.TextMatrix(12, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("difullbaths") + ""));
            GridDwelling2.TextMatrix(13, 0, "No. of Half Baths");
            GridDwelling2.TextMatrix(13, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dihalfbaths") + ""));
            GridDwelling2.TextMatrix(14, 0, "No. of Additional Fixtures");
            GridDwelling2.TextMatrix(14, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diaddnfixtures") + ""));
            GridDwelling2.TextMatrix(15, 0, "No. of Fireplaces");
            GridDwelling2.TextMatrix(15, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("difireplaces") + ""));
            GridDwelling3.TextMatrix(0, 0, "Layout");
            GridDwelling3.TextMatrix(0, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dilayout") + ""));
            GridDwelling3.TextMatrix(1, 0, "Attic");
            GridDwelling3.TextMatrix(1, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diattic") + ""));
            GridDwelling3.TextMatrix(2, 0, "Insulation");
            GridDwelling3.TextMatrix(2, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diinsulation") + ""));
            GridDwelling3.TextMatrix(3, 0, "Percent Unfinished");
            GridDwelling3.TextMatrix(3, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished") + ""));
            GridDwelling3.TextMatrix(4, 0, "Grade");
            GridDwelling3.TextMatrix(4, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1") + ""));
            GridDwelling3.TextMatrix(5, 0, "Factor");
            GridDwelling3.TextMatrix(5, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2") + ""));
            GridDwelling3.TextMatrix(6, 0, "Square Footage");
            GridDwelling3.TextMatrix(6, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("disqft") + ""));
            GridDwelling3.TextMatrix(7, 0, "Condition");
            GridDwelling3.TextMatrix(7, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dicondition") + ""));
            GridDwelling3.TextMatrix(8, 0, "Physical % Good");
            GridDwelling3.TextMatrix(8, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctphys") + ""));
            GridDwelling3.TextMatrix(9, 0, "Functional % Good");
            GridDwelling3.TextMatrix(9, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctfunct") + ""));
            GridDwelling3.TextMatrix(10, 0, "Functional Code");
            GridDwelling3.TextMatrix(10, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("difunctcode") + ""));
            GridDwelling3.TextMatrix(11, 0, "Economic % Good");
            GridDwelling3.TextMatrix(11, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctecon") + ""));
            GridDwelling3.TextMatrix(12, 0, "Economic Code");
            GridDwelling3.TextMatrix(12, 1, Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dieconcode") + ""));
            if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + " " == "C")
            {
                MessageBox.Show("There is no Dwelling data for that card. The screen will default to the Property Tab.", "No Commercial Data Available", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + " " == "Y")
            {
                // Do Nothing
            }
            else
            {
                object answer;
            }
        }

        public void FillOutBuildingScreen()
        {
            string hold = "";
            string strTemp = "";
            int intWhatBldg;
            int x;
            intWhatBldg = 0;
            for (x = 1; x <= 10; x++)
            {
                // TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(x)) + "") > 0)
                {
                    // TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
                    txtMROIType1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
                    txtMROIYear1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oiyear" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
                    txtMROIUnits1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oiunits" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
                    txtMROIGradeCd1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oigradecd" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
                    txtMROIGradePct1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oigradepct" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
                    txtMROICond1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oicond" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
                    txtMROIPctPhys1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oipctphys" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
                    txtMROIPctFunct1[FCConvert.ToInt16(intWhatBldg)].Text = Strings.Trim(modDataTypes.Statics.OUT.Get_Fields("oipctfunct" + FCConvert.ToString(x)) + "");
                    // TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
                    if (FCConvert.CBool((modDataTypes.Statics.OUT.Get_Fields("oiusesound" + FCConvert.ToString(x)) + "")))
                    {
                        chkSound[FCConvert.ToInt16(intWhatBldg)].CheckState = Wisej.Web.CheckState.Checked;
                        // TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
                        txtMROISoundValue[FCConvert.ToInt16(intWhatBldg)].Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oisoundvalue" + FCConvert.ToString(x)) + ""));
                    }
                    else
                    {
                        chkSound[FCConvert.ToInt16(intWhatBldg)].CheckState = Wisej.Web.CheckState.Unchecked;
                        txtMROISoundValue[FCConvert.ToInt16(intWhatBldg)].Text = "";
                    }
                    intWhatBldg += 1;
                }
            }
            // x
            if (intWhatBldg < 10)
            {
                for (x = intWhatBldg; x <= 9; x++)
                {
                    txtMROIType1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROIYear1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROIUnits1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROIGradeCd1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROIGradePct1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROICond1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROIPctPhys1[FCConvert.ToInt16(x)].Text = "0";
                    txtMROIPctFunct1[FCConvert.ToInt16(x)].Text = "0";
                    chkSound[FCConvert.ToInt16(x)].CheckState = Wisej.Web.CheckState.Unchecked;
                    txtMROISoundValue[FCConvert.ToInt16(x)].Text = "";
                }
                // x
            }
            DateTime tempdate;
            if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("dateinspected")))
            {
                if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("dateinspected")))
                {
                    tempdate = (DateTime)modDataTypes.Statics.MR.Get_Fields_DateTime("dateinspected");
                    if (tempdate.ToOADate() == 0)
                    {
                        T2KDateInspected.Text = "";
                    }
                    else
                    {
                        T2KDateInspected.Text = Strings.Format(tempdate, "MM/dd/yyyy");
                    }
                }
            }
            // TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
            MaskEdbox3.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields("entrancecode") + "");
            // TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
            MaskEdBox1.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields("informationcode") + "");
        }

        public void FillCommercialScreen()
        {
            string hold = "";
            string strTemp = "";
            mebOcc1.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("OCC1") + "");
            mebDwel1.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("DWEL1") + "");
            mebC1Class.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class") + "");
            mebC1Quality.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality") + "");
            mebC1Grade.Text = Strings.Format(Conversion.Val(Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1grade") + "")) / 100, "0.00");
            mebC1ExteriorWalls.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1extwalls") + "");
            mebC1Stories.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories") + "");
            mebC1Height.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1height") + "");
            mebC1Floor.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1floor") + "");
            mebC1Perimeter.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1perimeter") + "");
            mebC1Heat.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1heat") + "");
            mebC1Built.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1built") + "");
            mebC1Remodel.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1remodel") + "");
            mebC1Condition.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition") + "");
            mebC1Phys.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1phys") + "");
            mebC1Funct.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c1funct") + "");
            mebCMEcon.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("cmecon") + "");
            mebOcc2.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("OCC2") + "");
            mebDwel2.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("DWEL2") + "");
            mebC2Class.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2class") + "");
            mebC2Quality.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2quality") + "");
            mebc2grade.Text = Strings.Format(Conversion.Val(Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2grade") + "")) / 100, "0.00");
            mebC2ExtWalls.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2extwalls") + "");
            mebC2Stories.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories") + "");
            mebC2Height.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2height") + "");
            mebC2Floor.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2floor") + "");
            mebC2Perimeter.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2perimeter") + "");
            mebC2Heat.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2heat") + "");
            mebC2Built.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2built") + "");
            mebC2Remodel.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2remodel") + "");
            mebC2Condition.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2condition") + "");
            mebC2Phys.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2phys") + "");
            mebC2Funct.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("c2funct") + "");
            mebCMEcon.Text = Strings.Trim(modDataTypes.Statics.CMR.Get_Fields_Int32("cmecon") + "");
        }

        private void FillExemptGrid()
        {
            int x;
            for (x = 1; x <= 3; x++)
            {
                if (theAccount.Get_ExemptCode(x) > 0)
                {
                    GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE, FCConvert.ToString(theAccount.Get_ExemptCode(x)));
                    GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT, FCConvert.ToString(theAccount.Get_ExemptPct(x)));
                }
                else
                {
                    GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE, FCConvert.ToString(CNSTNOEXEMPT));
                    GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT, FCConvert.ToString(100));
                }
            }
            // x
        }

        private void EnableDisableExemptGrid()
        {
            if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
            {
                if (booleditland)
                {
                    GridExempts.ForeColor = Color.Black;
                    GridExempts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                }
            }
            else
            {
                GridExempts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
                GridExempts.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        private void LoadIncomeApproach(int lngAcct, int intCard)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            int x;
            // load Grid1 first
            Grid1.Rows = 2;
            clsLoad.OpenRecordset("Select * from incomeapproach where account = " + FCConvert.ToString(lngAcct) + " and card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                Grid1.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("OccupancyCode"))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Int32("SquareFootage")), "#,###,##0") + "\t" + clsLoad.Get_Fields_String("GrossorNet") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Int32("actualrent")), "#,##0") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("actualsfyear")), "##0.00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("marketsfyear")), "##0.00"));
                ReCalcRow_2(Grid1.Rows - 1);
                clsLoad.MoveNext();
            }
            // Then load Expenses
            GridExpenses.Rows = 2;
            clsLoad.OpenRecordset("select * from incomeexpense where account = " + FCConvert.ToString(lngAcct) + " and card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
            while (!clsLoad.EndOfFile())
            {
                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                GridExpenses.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Code"))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("percgross")), "##0.00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("$SF")), "##0.00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields("amount")), "#,###,##0"));
                clsLoad.MoveNext();
            }
            // Then load Grid Value
            if (Grid1.Rows > 2)
            {
                if (Conversion.Val(Grid1.TextMatrix(2, colOCCCode)) > 0)
                {
                    clsLoad.OpenRecordset("select * from occupancycodes where code = " + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(2, colOCCCode))), modGlobalVariables.strREDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        GridValues.TextMatrix(5, 2, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("rate"))), "##0.00"));
                    }
                }
            }
            clsLoad.OpenRecordset("select overallrate from defaults", modGlobalVariables.strREDatabase);
            if (!clsLoad.EndOfFile())
            {
                dblSTDOVRate = Conversion.Val(clsLoad.Get_Fields_Double("overallrate"));
            }
            else
            {
                dblSTDOVRate = 0;
            }
            clsLoad.OpenRecordset("select * from incomevalue where account = " + FCConvert.ToString(lngAcct) + " and card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
            if (!clsLoad.EndOfFile())
            {
                GridValues.TextMatrix(1, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("vacancy"))), "##0.00"));
                GridValues.TextMatrix(5, 1, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("overallrate"))), "##0.00"));
                if (Conversion.Val(GridValues.TextMatrix(5, 1)) > 0)
                {
                    GridValues.TextMatrix(5, 2, GridValues.TextMatrix(5, 1));
                }
            }
            // effective taxrate
            string BillRate = "";
            double dblTownPerc = 0;
            // what % does this town valuate at? most are 100%
            double dblBRate = 0;
            dblTownPerc = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
            dblBRate = modGlobalRoutines.Round((modGlobalVariables.Statics.CustomizedInfo.BillRate / 1000) * dblTownPerc, 4);
            // want in percentage not per 1000
            GridValues.TextMatrix(6, 2, Strings.Format(dblBRate * 100, "##0.00"));
            // capitalization rate
            GridValues.TextMatrix(7, 2, Strings.Format(Conversion.Val(GridValues.TextMatrix(5, 2)) + Conversion.Val(GridValues.TextMatrix(6, 2)), "##0.00"));
            // ReCalc
            ReCalcTotal();
        }

        private void UpdateFlags(int lngAcct, int intCard)
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strTemp = "";
            string strTemp2 = "";
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                clsTemp.OpenRecordset("Select * from commrec where crecordnumber > 0 and crecordnumber = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                if (!clsTemp.EndOfFile())
                {
                    lblComment.Visible = true;
                }
                else
                {
                    lblComment.Visible = false;
                }
                lblPendingFlag.Visible = false;
                clsTemp.OpenRecordset("select account from pending where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                if (!clsTemp.EndOfFile())
                {
                    lblPendingFlag.Visible = true;
                }
                clsTemp.OpenRecordset("select account from pendingchanges where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                if (!clsTemp.EndOfFile())
                {
                    lblPendingFlag.Visible = true;
                }
                clsTemp.OpenRecordset("select * from picturerecord where mraccountnumber = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), "twre0000.vb1");
                if (!clsTemp.EndOfFile())
                {
                    lblPicturePresent.Visible = true;
                }
                else
                {
                    lblPicturePresent.Visible = false;
                }
                CheckForInterestedParties(modGlobalVariables.Statics.gintLastAccountNumber);
                if (!(modDataTypes.Statics.MR == null))
                {
                    if (!modDataTypes.Statics.MR.EndOfFile())
                    {
                        strTemp = Convert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("id"), 16).ToUpper();
                        if (strTemp.Length < 7)
                        {
                            strTemp = Strings.StrDup(7 - strTemp.Length, "0") + strTemp;
                        }
                        //FC:FINAL:DSE:#830 Path should not be relative to Environment, but to deployment
                        //strTemp2 = Environment.CurrentDirectory + "\\sketches\\" + strTemp;
                        strTemp2 = Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "sketches", strTemp);
                        strTemp = FCFileSystem.Dir(strTemp2 + "*.skt", 0);
                        if (strTemp != string.Empty)
                        {
                            lblSketchPresent.Visible = true;
                        }
                        else
                        {
                            lblSketchPresent.Visible = false;
                        }
                    }
                    else
                    {
                        lblSketchPresent.Visible = false;
                    }
                }
                else
                {
                    lblSketchPresent.Visible = false;
                }
            }
            else
            {
                lblComment.Visible = false;
                lblPendingFlag.Visible = false;
                lblSketchPresent.Visible = false;
                lblPicturePresent.Visible = false;
            }
        }

        private void FillControlInformationClass()
        {
            int x;
            int intCol;
            intTotalNumberOfControls = 0;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            if (!modGlobalVariables.Statics.boolRegRecords)
            {
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSName";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Name";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSSecOwner";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Second Owner";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSPrevMaster";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Previous Owner";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSAddr1";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 1";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSAddr2";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 2";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSAddr3";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "City";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSState";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "State";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSZip";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Zip";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSZip4";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Zip Ext";
                intTotalNumberOfControls += 1;


            }
            else
            {
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtOwnerID";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Owner";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txt2ndOwnerID";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Second Owner";
                intTotalNumberOfControls += 1;

                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtDeedName1";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Deed Name 1";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtDeedName2";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Deed Name 2";
                intTotalNumberOfControls += 1;
            }
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSLocNumAlph";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Street Number";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSLocApt";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Apartment";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSLocStreet";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Street";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSRef1";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Ref 1";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSRef2";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Ref 2";
            intTotalNumberOfControls += 1;

            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIOpen1";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Open 1";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIOpen2";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Open 2";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "maskedbox3";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Entrance Code";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "maskedbox1";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Information Code";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "t2kdateInspected";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Inspection Date";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSMapLot";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "MapLot";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIStreetCode";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Tree Growth Year";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIXCoord";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = Label8[12].Text;
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIYCoord";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = Label8[13].Text;
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkZoneOverride";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Price as Sec Zone";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkRLivingTrust";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Revocable Living Trust";
            intTotalNumberOfControls += 1;
            for (x = 0; x <= 9; x++)
            {
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIType1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Type";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIYear1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Year";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIUnits1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Units";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIGradeCD1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Grade";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIGradePCT1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Grade Pct";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROICond1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Condition";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIPctPhys1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Phys Pct";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROIPctFunct1";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Outbuilding " + FCConvert.ToString(x + 1) + " Func Pct";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "chkSound";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Use Sound Value " + FCConvert.ToString(x + 1);
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "txtMROISoundValue";
                clsControlInfo[intTotalNumberOfControls].Index = x;
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Sound Value " + FCConvert.ToString(x + 1);
                intTotalNumberOfControls += 1;
            }
            // x
            // commercial
            for (x = 1; x <= 2; x++)
            {
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebocc" + FCConvert.ToString(x);
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Occupancy " + FCConvert.ToString(x) + " Code";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebDwel" + FCConvert.ToString(x);
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Dwelling Units";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "class";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Class";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Quality";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Quality";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Grade";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Grade Factor";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                if (x == 1)
                {
                    //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Exteriorwalls";
                }
                else
                {
                    //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Extwalls";
                }
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Exterior Walls";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "stories";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Stories";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Height";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Height";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Floor";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Floor";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Perimeter";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Perimeter";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Heat";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Heating Cooling";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Built";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Year Built";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Remodel";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Remodel";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Condition";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Condition";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Phys";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Phys. Pct";
                intTotalNumberOfControls += 1;
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "mebc" + FCConvert.ToString(x) + "Funct";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
                clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. " + FCConvert.ToString(x) + " Func. Pct";
                intTotalNumberOfControls += 1;
            }
            // x
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "mebcmecon";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Comm. Econ. Pct";
            intTotalNumberOfControls += 1;
        }

        private void FillControlInformationClassFromGrid()
        {
            int intRows;
            int intCols;
            for (intRows = 1; intRows <= 3; intRows++)
            {
                for (intCols = 0; intCols <= 1; intCols++)
                {
                    Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "GridExempts";
                    clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                    clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
                    clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
                    if (intCols == CNSTEXEMPTCOLCODE)
                    {
                        clsControlInfo[intTotalNumberOfControls].DataDescription = "Exempt Code " + FCConvert.ToString(intRows);
                    }
                    else
                    {
                        clsControlInfo[intTotalNumberOfControls].DataDescription = "Exempt Percent " + FCConvert.ToString(intRows);
                    }
                    intTotalNumberOfControls += 1;
                }
                // intCols
            }
            // intRows
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridTranCode";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Tran Code ";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridLandCode";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Land Code ";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridBldgCode";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Building Code ";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridPropertyCode";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Property Code";
            intTotalNumberOfControls += 1;
            for (intRows = 1; intRows <= BPGrid.Rows - 1; intRows++)
            {
                for (intCols = BPBookCol; intCols <= BPPageCol; intCols++)
                {
                    Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "BPGrid";
                    clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                    clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
                    clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
                    switch (intCols)
                    {
                        case BPBookCol:
                            {
                                clsControlInfo[intTotalNumberOfControls].DataDescription = "BP Row " + FCConvert.ToString(intRows) + " Book";
                                break;
                            }
                        case BPPageCol:
                            {
                                clsControlInfo[intTotalNumberOfControls].DataDescription = "BP Row " + FCConvert.ToString(intRows) + " Page";
                                break;
                            }
                    }
                    //end switch
                    intTotalNumberOfControls += 1;
                }
                // intCols
            }
            // intRows
            for (intCols = CNSTSALECOLPRICE; intCols <= CNSTSALECOLVALIDITY; intCols++)
            {
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "SaleGrid";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                clsControlInfo[intTotalNumberOfControls].GridRow = 2;
                clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
                switch (intCols)
                {
                    case CNSTSALECOLPRICE:
                        {
                            clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Price";
                            break;
                        }
                    case CNSTSALECOLDATE:
                        {
                            clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Date";
                            break;
                        }
                    case CNSTSALECOLFINANCING:
                        {
                            clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Financing";
                            break;
                        }
                    case CNSTSALECOLSALE:
                        {
                            clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Type";
                            break;
                        }
                    case CNSTSALECOLVALIDITY:
                        {
                            clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Validity";
                            break;
                        }
                    case CNSTSALECOLVERIFIED:
                        {
                            clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Verified";
                            break;
                        }
                }
                //end switch
                intTotalNumberOfControls += 1;
            }
            // intCols
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridNeighborhood";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Neighborhood";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridZone";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Zone";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridSecZone";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Secondary Zone";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridStreet";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Street";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridTopography";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Topography 1";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridTopography";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 1;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Topography 2";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridUtilities";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 0;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Utilities 1";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            //FC:FINAL:DSE:#i830 Autoinstantiating variables 
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "GridUtilities";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
            clsControlInfo[intTotalNumberOfControls].GridRow = 1;
            clsControlInfo[intTotalNumberOfControls].GridCol = 0;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Utilities 2";
            intTotalNumberOfControls += 1;
            for (intRows = 0; intRows <= GridDwelling1.Rows - 1; intRows++)
            {
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "GridDwelling1";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
                clsControlInfo[intTotalNumberOfControls].GridCol = 1;
                clsControlInfo[intTotalNumberOfControls].DataDescription = GridDwelling1.TextMatrix(intRows, 0);
                intTotalNumberOfControls += 1;
            }
            // intRows
            for (intRows = 0; intRows <= GridDwelling2.Rows - 1; intRows++)
            {
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "GridDwelling2";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
                clsControlInfo[intTotalNumberOfControls].GridCol = 1;
                clsControlInfo[intTotalNumberOfControls].DataDescription = GridDwelling2.TextMatrix(intRows, 0);
                intTotalNumberOfControls += 1;
            }
            // intRows
            for (intRows = 0; intRows <= GridDwelling3.Rows - 1; intRows++)
            {
                Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "GridDwelling3";
                clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
                clsControlInfo[intTotalNumberOfControls].GridCol = 1;
                clsControlInfo[intTotalNumberOfControls].DataDescription = GridDwelling3.TextMatrix(intRows, 0);
                intTotalNumberOfControls += 1;
            }
            // intRows
            for (intRows = 1; intRows <= GridLand.Rows - 1; intRows++)
            {
                for (intCols = 0; intCols <= GridLand.Cols - 1; intCols++)
                {
                    Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    //FC:FINAL:DSE:#i830 Autoinstantiating variables 
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "GridLand";
                    clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
                    clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
                    clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
                    switch (intCols)
                    {
                        case 0:
                            {
                                // type
                                clsControlInfo[intTotalNumberOfControls].DataDescription = "Land " + FCConvert.ToString(intRows) + " Type";
                                break;
                            }
                        case 1:
                            {
                                // units
                                clsControlInfo[intTotalNumberOfControls].DataDescription = "Land " + FCConvert.ToString(intRows) + " Units";
                                break;
                            }
                        case 2:
                            {
                                // influence
                                clsControlInfo[intTotalNumberOfControls].DataDescription = "Land " + FCConvert.ToString(intRows) + " Influence";
                                break;
                            }
                        case 3:
                            {
                                // influence code
                                clsControlInfo[intTotalNumberOfControls].DataDescription = "Land " + FCConvert.ToString(intRows) + " Influence Code";
                                break;
                            }
                    }
                    //end switch
                    intTotalNumberOfControls += 1;
                }
                // intCols
            }
            // intRows
        }

        private void ReCalcRow_2(int lngRow)
        {
            ReCalcRow(lngRow);
        }

        private void ReCalcRow(int lngRow)
        {
            // recalculate based on changes made to lngRow
            int lngSF = 0;
            // square footage
            int lngMarket = 0;
            int x;
            if (Conversion.Val(Grid1.TextMatrix(lngRow, colSF)) > 0)
            {
                lngSF = FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(lngRow, colSF)));
            }
            else
            {
                lngSF = 0;
            }
            if (lngSF > 0)
            {
                if (Conversion.Val(Grid1.TextMatrix(lngRow, colActRent)) > 0)
                {
                    //FC:FINAL:MSH - i.issue #1224: invalid cast exception
                    //Grid1.TextMatrix(lngRow, colActSFYear, Strings.Format(FCConvert.ToDouble(12 * FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(lngRow, colActRent)))) / lngSF, "##0.00"));
                    Grid1.TextMatrix(lngRow, colActSFYear, Strings.Format(FCConvert.ToDouble((12 * FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(lngRow, colActRent))))) / lngSF, "##0.00"));
                }
                else
                {
                    Grid1.TextMatrix(lngRow, colActSFYear, FCConvert.ToString(0));
                }
            }
            else
            {
                Grid1.TextMatrix(lngRow, colActSFYear, FCConvert.ToString(0));
            }
            if (Conversion.Val(Grid1.TextMatrix(lngRow, colMarketSFYear)) > 0)
            {
                if (Conversion.Val(Grid1.TextMatrix(lngRow, colMarketSFYear)) > 0)
                {
                    Grid1.TextMatrix(lngRow, colMarketRent, Strings.Format(lngSF * FCConvert.ToDouble(Grid1.TextMatrix(lngRow, colMarketSFYear)), "#,###,##0"));
                }
                else
                {
                    Grid1.TextMatrix(lngRow, colMarketRent, FCConvert.ToString(0));
                }
            }
            else
            {
                Grid1.TextMatrix(lngRow, colMarketRent, FCConvert.ToString(0));
            }
            lngMarket = 0;
            for (x = 2; x <= Grid1.Rows - 1; x++)
            {
                if (Conversion.Val(Grid1.TextMatrix(x, colOCCCode)) > 0)
                {
                    if (Conversion.Val(Grid1.TextMatrix(x, colMarketRent)) > 0)
                    {
                        lngMarket += FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(x, colMarketRent)));
                    }
                }
            }
            // x
            GridValues.TextMatrix(0, 2, Strings.Format(lngMarket, "#,###,##0"));
            for (x = 2; x <= GridExpenses.Rows - 1; x++)
            {
                ReCalcExpenseRow(ref x);
            }
            // x
            ReCalcTotal();
        }

        private void ReCalcExpenseRow(ref int lngRow)
        {
            double dblPercGross = 0;
            double dblPerSF = 0;
            int lngSF = 0;
            int lngGross = 0;
            // vbPorter upgrade warning: lngAmount As int	OnWriteFCConvert.ToDouble(
            int lngAmount = 0;
            if (Conversion.Val(GridValues.TextMatrix(0, 2)) > 0)
            {
                lngGross = FCConvert.ToInt32(FCConvert.ToDouble(GridValues.TextMatrix(0, 2)));
            }
            else
            {
                lngGross = 0;
            }
            if (Conversion.Val(txtTotalSF.Text) > 0)
            {
                lngSF = FCConvert.ToInt32(FCConvert.ToDouble(txtTotalSF.Text));
            }
            else
            {
                lngSF = 0;
            }
            dblPercGross = Conversion.Val(GridExpenses.TextMatrix(lngRow, 1)) / 100;
            if (lngSF > 0)
            {
                dblPerSF = modGlobalRoutines.Round(dblPercGross * lngGross / lngSF, 2);
            }
            else
            {
                dblPerSF = 0;
            }
            lngAmount = FCConvert.ToInt32(dblPercGross * lngGross);
            GridExpenses.TextMatrix(lngRow, colAmount, Strings.Format(lngAmount, "#,###,##0"));
            GridExpenses.TextMatrix(lngRow, colPerSF, Strings.Format(dblPerSF, "#,##0.00"));
        }

        private void ReCalcTotal()
        {
            int x;
            int lngTotSF = 0;
            int lngMarketRent = 0;
            int lngTotAmount = 0;
            int lngEGross = 0;
            double dblCapRate;
            int lngENet = 0;
            double dblVacancy = 0;
            // vbPorter upgrade warning: lngVacancy As int	OnWriteFCConvert.ToDouble(
            int lngVacancy = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            // first recalc total SF
            lngTotSF = 0;
            lngMarketRent = 0;
            for (x = 2; x <= Grid1.Rows - 1; x++)
            {
                if (Conversion.Val(Grid1.TextMatrix(x, colOCCCode)) > 0)
                {
                    if (Conversion.Val(Grid1.TextMatrix(x, colSF)) > 0)
                    {
                        lngTotSF += FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(x, colSF)));
                    }
                    if (Conversion.Val(Grid1.TextMatrix(x, colMarketRent)) > 0)
                    {
                        lngMarketRent += FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(x, colMarketRent)));
                    }
                }
            }
            // x
            txtTotalSF.Text = Strings.Format(lngTotSF, "#,###,##0");
            GridValues.TextMatrix(0, 2, Strings.Format(lngMarketRent, "#,###,##0"));
            lngTotAmount = 0;
            for (x = 2; x <= GridExpenses.Rows - 1; x++)
            {
                if (Conversion.Val(GridExpenses.TextMatrix(x, colECode)) > 0)
                {
                    if (Conversion.Val(GridExpenses.TextMatrix(x, colAmount)) > 0)
                    {
                        lngTotAmount += FCConvert.ToInt32(FCConvert.ToDouble(GridExpenses.TextMatrix(x, colAmount)));
                    }
                }
            }
            // x
            txtTotalExpense.Text = Strings.Format(lngTotAmount, "#,###,##0");
            lngEGross = 0;
            if (Conversion.Val(GridValues.TextMatrix(1, 1)) > 0)
            {
                dblVacancy = Conversion.Val(GridValues.TextMatrix(1, 1)) / 100;
                lngVacancy = FCConvert.ToInt32(modGlobalRoutines.Round((dblVacancy * lngMarketRent), 0));
                lngEGross = lngMarketRent - lngVacancy;
                GridValues.TextMatrix(2, 2, Strings.Format(lngEGross, "#,###,##0"));
                GridValues.TextMatrix(1, 2, Strings.Format(lngVacancy, "#,###,##0"));
            }
            else
            {
                GridValues.TextMatrix(1, 1, "0.00");
                GridValues.TextMatrix(1, 2, FCConvert.ToString(0));
                lngEGross = lngMarketRent;
                GridValues.TextMatrix(2, 2, GridValues.TextMatrix(0, 2));
            }
            if (Grid1.Rows > 2)
            {
                if (Conversion.Val(Grid1.TextMatrix(2, colOCCCode)) > 0)
                {
                    clsLoad.OpenRecordset("select * from occupancycodes where code = " + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(2, colOCCCode))), modGlobalVariables.strREDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        GridValues.TextMatrix(5, 2, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("rate"))), "##0.00"));
                    }
                }
            }
            if (Conversion.Val(GridValues.TextMatrix(5, 1)) > 0)
            {
                GridValues.TextMatrix(5, 2, GridValues.TextMatrix(5, 1));
            }
            GridValues.TextMatrix(3, 2, txtTotalExpense.Text);
            GridValues.TextMatrix(4, 2, Strings.Format(lngEGross - lngTotAmount, "#,###,##0"));
            lngENet = lngEGross - lngTotAmount;
            GridValues.TextMatrix(7, 2, Strings.Format(Conversion.Val(GridValues.TextMatrix(5, 2)) + Conversion.Val(GridValues.TextMatrix(6, 2)), "##0.00"));
            if (Conversion.Val(GridValues.TextMatrix(7, 2)) / 100 > 0 && lngENet > 0)
            {
                //GridValues.TextMatrix(8, 2, Strings.Format(modGlobalRoutines.RERound_2(FCConvert.ToInt32(modGlobalRoutines.Round(lngENet / (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(GridValues.TextMatrix(7, 2))),) / 100), 0))), "#,###,##0"));
                // FC:FINAL:VGE - #i1050 Replacing FCConvert.ToInt32 to Math.Floor
                //GridValues.TextMatrix(8, 2, Strings.Format(modGlobalRoutines.RERound_2(FCConvert.ToInt32(modGlobalRoutines.Round(lngENet / (Math.Floor(Conversion.Val(GridValues.TextMatrix(7, 2))) / 100), 0))), "#,###,##0"));
                //FC:FINAL:MSH - i.issue #1080: Math.Floor truncates part after comma and value will be calculated incorrect
                GridValues.TextMatrix(8, 2, Strings.Format(modGlobalRoutines.RERound_2(FCConvert.ToInt32(modGlobalRoutines.Round(lngENet * 1.0 / (Conversion.Val(GridValues.TextMatrix(7, 2)) / 100), 0))), "#,###,##0"));
            }
            else
            {
                GridValues.TextMatrix(8, 2, FCConvert.ToString(0));
            }
        }

        private void frmNewREProperty_Resize(object sender, System.EventArgs e)
        {
            ResizeBPGrid();
            ResizeIncomeGrids();
            ResizeDwellingGrids();
            ResizePic();
            ResizeGridLand();
            ResizeSketch();
            ResizeGridExempts();
            // ResizeGridPhones
            ResizeGridTranCode();
            ResizeGridNeighborhood();
            ResizeGridZone();
            ResizeGridTopography();
            ResizeGridSecZone();
            ResizeGridStreet();
            ResizeGridUtilities();
            ResizeGridLandCode();
            ResizeGridBldgCode();
            ResizeGridPropertyCode();
            ResizeSaleGrid();
        }

        private void ResizePic()
        {
            double dblRatio;
            // vbPorter upgrade warning: TempHeight As int	OnWriteFCConvert.ToDouble(
            int TempHeight = 0;
            // vbPorter upgrade warning: TempWidth As int	OnWriteFCConvert.ToDouble(
            int TempWidth = 0;
            try
            {
                if (imgPicture.Image == null)
                    return;
                dblRatio = (double)imgPicture.Image.Width / imgPicture.Image.Height;
                imgPicture.Height = framPicture.Height - 200 / FCScreen.TwipsPerPixelY;
                imgPicture.Width = framPicture.Width - 200 / FCScreen.TwipsPerPixelX;
                if (imgPicture.Image.Height > imgPicture.Image.Width)
                {
                    TempWidth = FCConvert.ToInt32(imgPicture.Height * dblRatio);
                    if (TempWidth > imgPicture.Width)
                    {
                        imgPicture.Height = FCConvert.ToInt32((1 / dblRatio) * imgPicture.Width);
                    }
                    else
                    {
                        imgPicture.Width = TempWidth;
                    }
                }
                else
                {
                    TempHeight = FCConvert.ToInt32((1 / dblRatio) * imgPicture.Width);
                    if (TempHeight > imgPicture.Height)
                    {
                        imgPicture.Width = FCConvert.ToInt32(imgPicture.Height * dblRatio);
                    }
                    else
                    {
                        imgPicture.Height = TempHeight;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ResizePic", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ResizeGridLand()
        {
            int GridWidth = 0;
            int GridHeight;
            GridWidth = GridLand.WidthOriginal;
            GridLand.ColWidth(0, FCConvert.ToInt32(0.44 * GridWidth));
            GridLand.ColWidth(1, FCConvert.ToInt32(0.15 * GridWidth));
            GridLand.ColWidth(2, FCConvert.ToInt32(0.14 * GridWidth));
            //GridLand.Height = (GridLand.RowHeight(0) * GridLand.Rows + 50) / FCScreen.TwipsPerPixelY;
        }

        private void ResizeSketch()
        {
            //no need to resize the sketch
            return;
            double dblRatio;
            int TempHeight;
            double TempWidth;
            try
            {
                // On Error GoTo ErrorHandler
                if (imSketch.Image.Width == 0 || imSketch.Image.Height == 0)
                    return;
                imSketch.SizeMode = PictureBoxSizeMode.Normal;
                dblRatio = (double)imSketch.Image.Width / imSketch.Image.Height;
                imSketch.Width = framSketch.Width - 200 / FCScreen.TwipsPerPixelX;
                imSketch.Height = FCConvert.ToInt32(imSketch.Width / dblRatio);
                imSketch.SizeMode = PictureBoxSizeMode.StretchImage;
                if (imSketch.Height * 2 < 32000 / FCScreen.TwipsPerPixelY)
                {
                    //vsSketch.Maximum = imSketch.Height * 2;
                    dblPixelsPerScrollUnits = 0.5;
                }
                else
                {
                    if (imSketch.Height > 32000 / FCScreen.TwipsPerPixelY)
                    {
                        // vsSketch.Maximum = 32000;
                        //dblPixelsPerScrollUnits = FCConvert.ToDouble(vsSketch.Maximum / imSketch.Width;
                    }
                    else
                    {
                        //vsSketch.Maximum = imSketch.Height;
                        //dblPixelsPerScrollUnits = 1;
                    }
                }
                //vsSketch.SmallChange = FCConvert.ToInt32(vsSketch.Maximum / 100.0);
                //vsSketch.LargeChange = FCConvert.ToInt32(vsSketch.Maximum / 4.0);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ResizeSketch. Line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ResizeSaleGrid()
        {
            int GridWidth = 0;
            try
            {
                // On Error GoTo ErrorHandler
                GridWidth = SaleGrid.WidthOriginal;
                /*Width * FCScreen.TwipsPerPixelX;*/
                SaleGrid.ColWidth(CNSTSALECOLPRICE, FCConvert.ToInt32(0.1 * GridWidth));
                SaleGrid.ColWidth(CNSTSALECOLDATE, FCConvert.ToInt32(0.1 * GridWidth));
                SaleGrid.ColWidth(CNSTSALECOLSALE, FCConvert.ToInt32(0.2 * GridWidth));
                SaleGrid.ColWidth(CNSTSALECOLFINANCING, FCConvert.ToInt32(0.2 * GridWidth));
                SaleGrid.ColWidth(CNSTSALECOLVERIFIED, FCConvert.ToInt32(0.2 * GridWidth));
                //SaleGrid.Height = (SaleGrid.RowHeight(0) * 3 + 50) / FCScreen.TwipsPerPixelY;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeSaleGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void GridToolTip_DblClick(object sender, System.EventArgs e)
        {
            if (GridToolTip.Row > 0)
            {
                string vbPorterVar = FCConvert.ToString(GridToolTip.Tag);
                if (vbPorterVar == "Open1")
                {
                    txtMRPIOpen1.Text = FCConvert.ToString(Conversion.Val(GridToolTip.TextMatrix(GridToolTip.Row, 0)));
                }
                else if (vbPorterVar == "Open2")
                {
                    txtMRPIOpen2.Text = FCConvert.ToString(Conversion.Val(GridToolTip.TextMatrix(GridToolTip.Row, 0)));
                }
                else if (vbPorterVar == "State")
                {
                    // txtMRRSState.Text = GridToolTip.TextMatrix(GridToolTip.Row, 1)
                }
                ToolTip.HideTip();
            }
        }

        private void mnuAddACard_Click(object sender, System.EventArgs e)
        {
            cmdAddACard_Click();
        }

        private void cmdAddACard_Click()
        {
            // VB6 Bad Scope Dim:
            string strGuid = "";
            int intCounter;
            clsDRWrapper rsTemp = new clsDRWrapper();
            int lngCards;

            modGlobalVariables.Statics.AddButton = true;
            modGlobalVariables.Statics.SaveButton = false;
            modGlobalVariables.Statics.AddingACard = true;
            if (modGlobalVariables.Statics.DataChanged)
            {
                if (MessageBox.Show("Save changes to card: " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (CheckValues())
                    {
                        // Call SaveData2
                        SaveCard();
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    }
                }
            }
            modGNBas.Statics.gintCardNumber = modGNBas.Statics.gintMaxCards + 1;
            lngCards = modGNBas.Statics.gintMaxCards + 1;
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                rsTemp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber - 1), modGlobalVariables.strREDatabase);
            }
            else
            {
                rsTemp.OpenRecordset("Select * from srmaster where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber - 1) + " and saleid = " + FCConvert.ToString(modGlobalVariables.Statics.glngSaleID), modGlobalVariables.strREDatabase);
            }
            if (rsTemp.EndOfFile())
            {
                rsTemp.AddNew();
                rsTemp.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                rsTemp.Set_Fields("rscard", modGNBas.Statics.gintCardNumber - 1);
                rsTemp.Set_Fields("rsdeleted", false);
                rsTemp.Set_Fields("datecreated", modDataTypes.Statics.MR.Get_Fields_DateTime("datecreated"));
                if (!modGlobalVariables.Statics.boolRegRecords)
                {
                    rsTemp.Set_Fields("saleid", modDataTypes.Statics.MR.Get_Fields_Int32("saleid"));
                    rsTemp.Set_Fields("saledate", modDataTypes.Statics.MR.Get_Fields_DateTime("saledate"));
                }
                rsTemp.Update();
            }
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                if (!CheckAccountNumber(modGNBas.Statics.gintCardNumber))
                    return;
            }

            modGNBas.Statics.gintMaxCards += 1;
            lblNumCards.Text = modGNBas.Statics.gintMaxCards.ToString();
            cmbCardNumber.AddItem(modGNBas.Statics.gintMaxCards.ToString());
            clsDRWrapper temp = modDataTypes.Statics.MR;
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.MR = temp;
                cCreateGUID cGuid = new cCreateGUID();
                strGuid = cGuid.CreateGUID();
                modREMain.OpenMasterTable(ref rsTemp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
                temp = modDataTypes.Statics.MR;
                modGlobalRoutines.CopyMasterInfo(ref rsTemp, ref temp);
                modDataTypes.Statics.MR = temp;
                modDataTypes.Statics.MR.Set_Fields("CardID", strGuid);
                // now get AccountID
                modDataTypes.Statics.MR.Set_Fields("AccountID", theAccount.AccountID);
            }
            else
            {
                temp = modDataTypes.Statics.MR;
                modREMain.OpenMasterTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, temp.Get_Fields_Int32("saleid"));
                modREMain.OpenMasterTable_27(ref rsTemp, modGlobalVariables.Statics.gintLastAccountNumber, 1, modGlobalVariables.Statics.glngSaleID);
                modGlobalRoutines.CopyMasterInfo(ref rsTemp, ref temp);
                modDataTypes.Statics.MR = temp;
            }
            temp = modDataTypes.Statics.DWL;
            modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, theAccount.AccountID);
            modDataTypes.Statics.DWL = temp;
            temp = modDataTypes.Statics.CMR;
            modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
            modDataTypes.Statics.CMR = temp;
            temp = modDataTypes.Statics.OUT;
            modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
            modDataTypes.Statics.OUT = temp;
            // Call FillTheScreen
            // update the records in case someone adds another card before saving
            // mr.update
            // mr.edit
            // dwl.update
            // dwl.edit
            // Cmr.update
            // Cmr.edit
            // OUT.Update
            // OUT.Edit
            modGlobalVariables.Statics.HoldDwelling = "N";
            // MR.Fields("ritrancode") = Val(txtMRRITranCode.Text)
            modDataTypes.Statics.MR.Set_Fields("ritrancode", FCConvert.ToString(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
            modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "N");
            boolAddingACard = true;
            cmbCardNumber_Validate(false);
            cmbCardNumber.Text = modGNBas.Statics.gintMaxCards.ToString();
            modGlobalVariables.Statics.DataChanged = true;
            // Call FillScreenWithoutLoadingMR
            ShowCard();
            boolAddingACard = false;
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
        }

        private void mnuAddExpense_Click(object sender, System.EventArgs e)
        {
            GridExpenses.AddItem("");
        }

        public void mnuAddNewAccount_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.AddCYAEntry_80("RE", "Created New Account while in " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "In Long Screen", "From Menu");
            lngOriginalOwnerID = 0;
            Acct_Click();
        }

        public void mnuAddNewAccount_Click()
        {
            mnuAddNewAccount_Click(mnuAddNewAccount, new System.EventArgs());
        }

        public int Acct_Click(int AcctNum = 0, bool boolNoCYA = false)
        {
            int Acct_Click = 0;
            int yy;
            int lngCards;
            clsDRWrapper clsTemp = new clsDRWrapper();
            Acct_Click = 0;
            if (!modGlobalVariables.Statics.boolRegRecords)
            {
                MessageBox.Show("This function is not allowed when viewing sales records", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return Acct_Click;
            }

            modGNBas.Statics.response = "X";
            modGlobalVariables.Statics.AddNewAccountFlag = true;
            ClearAllValues();
            if (AcctNum != 0)
            {
                lblMRHLAcctNum.Text = "Account " + FCConvert.ToString(AcctNum);
                modGlobalVariables.Statics.gintLastAccountNumber = AcctNum;
            }
            else
            {
                modGlobalVariables.Statics.gintLastAccountNumber = modGlobalRoutines.GetNextAccountToCreate();
            }
            Acct_Click = modGlobalVariables.Statics.gintLastAccountNumber;
            modGlobalVariables.Statics.CurrentAccount = modGlobalVariables.Statics.gintLastAccountNumber;
            if (!boolNoCYA)
            {
                modGlobalFunctions.AddCYAEntry_26("RE", "Created New Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "In Long Screen");
            }
            clsDRWrapper temp = modDataTypes.Statics.MR;
            modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
            modDataTypes.Statics.MR = temp;
            modDataTypes.Statics.MR.Set_Fields("RSAccount", modGlobalVariables.Statics.gintLastAccountNumber);
            modDataTypes.Statics.MR.Set_Fields("RSCard", 1);
            cCreateGUID cGuid = new cCreateGUID();
            string strGuid;
            strGuid = cGuid.CreateGUID();
            modDataTypes.Statics.MR.Set_Fields("CardID", strGuid);
            modDataTypes.Statics.MR.Set_Fields("AccountID", strGuid);
            modDataTypes.Statics.MR.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
            modDataTypes.Statics.MR.Set_Fields("rsdeleted", false);
            theAccount = new cREAccount();
            theAccount.Account = modGlobalVariables.Statics.gintLastAccountNumber;
            theAccount.AccountID = strGuid;
            temp = modDataTypes.Statics.MR;
            modREMain.SaveMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
            modDataTypes.Statics.MR = temp;
            LoadAccount(modGlobalVariables.Statics.gintLastAccountNumber);
            LoadCard(modGlobalVariables.Statics.gintLastAccountNumber, 1);
            modDataTypes.Statics.MR.Set_Fields("piland1inf", 100);
            modDataTypes.Statics.MR.Set_Fields("piland2inf", 100);
            modDataTypes.Statics.MR.Set_Fields("piland3inf", 100);
            modDataTypes.Statics.MR.Set_Fields("piland4inf", 100);
            modDataTypes.Statics.MR.Set_Fields("piland5inf", 100);
            modDataTypes.Statics.MR.Set_Fields("piland6inf", 100);
            modDataTypes.Statics.MR.Set_Fields("piland7inf", 100);
            ShowAccount();
            ShowCard();
            modGNBas.Statics.gintCardNumber = 1;
            modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", " ");
            //UpDownCard.Maximum = 1;
            cmbCardNumber.Text = FCConvert.ToString(1);
            if (cmbCardNumber.ListCount < 1)
            {
                cmbCardNumber.AddItem("1");
                cmbCardNumber.ItemData(cmbCardNumber.NewIndex, 1);
            }

            lblNumCards.Text = "1";
            cmbCardNumber.Text = "1";
            modGNBas.Statics.gboolLoadingDone = true;
            return Acct_Click;
        }

        private void mnuAddOcc_Click(object sender, System.EventArgs e)
        {
            Grid1.AddItem("");
        }

        private void mnuComment_Click(object sender, System.EventArgs e)
        {
            modGNBas.Statics.CommentAccount = modGlobalVariables.Statics.gintLastAccountNumber;
            modGNBas.Statics.CommentName = lblOwner1.Text;
            frmFSComment.InstancePtr.Init(modGlobalVariables.CNSTLONGSCREEN);
        }

        private void mnuDeleteCard_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsTemp;
            clsDRWrapper dbTemp = new clsDRWrapper();
            clsDRWrapper clsTemp = new clsDRWrapper();
            // vbPorter upgrade warning: intResp As short, int --> As DialogResult
            DialogResult intResp;
            if (modGNBas.Statics.gintCardNumber == 1/*&& UpDownCard.Maximum > 1*/)
            {
                intResp = MessageBox.Show("This will permanently delete the data for card 1." + "\r\n" + "If you have values that depend on values from the first card, you will have to change them." + "\r\n" + "Do you wish to continue?", "Real Estate", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }
            else
            {
                intResp = MessageBox.Show("This will remove Card #" + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + ". The data will be permanently lost. Are you sure you wish to continue?", "Real Estate", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }
            if (intResp == DialogResult.Yes)
            {
                // If gintCardNumber = 1 And VScrollCard.Max < 2 Then
                if (modGNBas.Statics.gintCardNumber == 1/*&& UpDownCard.Maximum < 2*/)
                {
                    MessageBox.Show("This is the last card and cannot be permanently deleted." + "\r\n" + "The account will be marked as a deleted account" + "\r\n" + "If you need to permanently delete the account, this must be done in File Maintenance", "Last Card", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    mnuDeleteAccount_Click();
                    return;
                }
                else
                {
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        modGlobalFunctions.AddCYAEntry_26("RE", "Delete Card", "Permanently deleted card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " from account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber));
                    }
                    else
                    {
                        modGlobalFunctions.AddCYAEntry_26("RE", "Delete Card", "Permanently deleted card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " from sales account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber));
                    }
                }
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    clsTemp.Execute("delete from summrecord where srecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and cardnumber = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                }
                modGNBas.Statics.gintMaxCards -= 1;
                var cardGuid = Guid.Parse(modDataTypes.Statics.MR.Get_Fields_String("CardId"));
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    
                    dbTemp.Execute("DELETE FROM Master where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM Commercial where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM Dwelling where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM OutBuilding where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM IncomeExpense where Account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and Card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM IncomeValue where Account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and Card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM IncomeApproach where Account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and Card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                    dbTemp.Execute(
                        "Delete from Documents where ReferenceGroup = 'RealEstate' and ReferenceType = 'PropertyPicture' and ReferenceId = " +
                        modGlobalVariables.Statics.gintLastAccountNumber + " and AltReference = '" +
                        cardGuid.ToString() + "'", "CentralDocuments");
                    dbTemp.Execute("DELETE FROM PictureRecord where MRAccountNumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);

                }
                else
                {
                    dbTemp.Execute("DELETE FROM srMaster where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM srCommercial where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM srDwelling where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
                    dbTemp.Execute("DELETE FROM srOutBuilding where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and RSCard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
                }
                // now change the card numbers
                int intCounter;

                modGNBas.Statics.gintCardNumber = 1;
                lblNumCards.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
                SetupcmbCardNumber();
                cmbCardNumber.Text = FCConvert.ToString(1);
                RefreshCard(1);

            }
        }

        private void mnuDeleteOutbuilding_Click(object sender, System.EventArgs e)
        {
            short x;
            if (MessageBox.Show("This will clear all outbuilding information" + "\r\n" + "Do you want to continue?", "Clear Outbuildings?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                modGlobalVariables.Statics.DataChanged = true;
                modGlobalFunctions.AddCYAEntry_26("RE", "Cleared outbuilding info", "Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
                for (x = 9; x >= 0; x--)
                {
                    txtMROIType1[x].Text = "";
                    cleartherestoftheline(ref x);
                }
                // x
            }
        }

        private void mnuDelExpense_Click(object sender, System.EventArgs e)
        {
            if (GridExpenses.Rows > 1)
            {
                if (GridExpenses.Row > 0 && GridExpenses.Row < GridExpenses.Rows)
                {
                    // kosher row number
                    GridExpenses.RemoveItem(GridExpenses.Row);
                }
            }
            ReCalcTotal();
        }

        private void mnuDelOcc_Click(object sender, System.EventArgs e)
        {
            if (Grid1.Rows > 2)
            {
                if (Grid1.Row > 1 && Grid1.Row < Grid1.Rows)
                {
                    // kosher row number
                    Grid1.RemoveItem(Grid1.Row);
                }
            }
            ReCalcTotal();
        }

        private void MakeTemporarySketchFiles()
        {
            if (sketchesViewModel == null)
            {
                return;
            }

            if (!sketchesViewModel.Sketches.HasCurrentSketch())
            {
                return;
            }

            var sketch = sketchesViewModel.Sketches.CurrentSketch();
            var sketchData = sketchesViewModel.GetCurrentSketch();
            string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketch.SketchIdentifier.ToString() + ".skt");
            using (var fs = new FileStream(sketchFileNamePath, FileMode.Create, FileAccess.Write))
            {
                fs.Write(sketchData, 0, sketchData.Length);                
            }
        }
        private void mnuEditSketch_Click(object sender, System.EventArgs e)
        {
            string strTemp = "";
            string strCard = "";
            string strSkt = "";
            //FileSystemObject fso = new FileSystemObject();
            string strWDrive = "";
            double dblSkRatio;
            try
            {
                // On Error GoTo ErrorHandler
                if (sketchesViewModel.Sketches.HasCurrentSketch())
                {
                    var currentSketch = sketchesViewModel.Sketches.CurrentSketch();
                    
                    strTemp = currentSketch.SketchIdentifier.ToString();
                        string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", strTemp + ".skt");

                        if (File.Exists(sketchFileNamePath))
                        {
                            File.Delete(sketchFileNamePath);
                        }

                        MakeTemporarySketchFiles();

                        sketchesViewModel.Sketches.AddPendingSketch(new SketchInfo()
                        {
                            Account = currentSketch.Account,
                            Calculations =  currentSketch.Calculations,
                            Card = currentSketch.Card,
                            CardIdentifier = currentSketch.CardIdentifier,
                            DateUpdated = DateTime.Now,
                            Id = currentSketch.Id,
                            MediaType = currentSketch.MediaType,
                            SequenceNumber = currentSketch.SequenceNumber,
                            SketchIdentifier = currentSketch.SketchIdentifier
                        });
                    if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
                        {
                            return;
                        }

                        using (MemoryStream zipMemoryStream = new MemoryStream())
                        {
                            var sketchFileName = Path.GetFileName(sketchFileNamePath);
                            using (var archive = new ZipArchive(zipMemoryStream, ZipArchiveMode.Create, true))
                            {

                                dynamic sketchData = new DynamicObject();
                                sketchData.action = "edit";
                                sketchData.fileName = sketchFileName;
                                var uploadUrl = Application.StartupUrl;
                                uploadUrl = uploadUrl.EndsWith("/") ? uploadUrl : uploadUrl + "/";
                                uploadUrl = uploadUrl + "FileUpload.ashx";
                                sketchData.uploadUrl = uploadUrl;
                                sketchData.serverDestinationFolder = Path.GetDirectoryName(sketchFileNamePath);
                                sketchData.data = new DynamicObject();
                                sketchData.data.fileNo =
                                    FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
                                cPartyAddress tAddr;
                                tAddr = theAccount.OwnerParty.GetPrimaryAddress();
                                if (!(tAddr == null))
                                {
                                    sketchData.data.city = tAddr.City;
                                    sketchData.data.state = tAddr.State;
                                    sketchData.data.zipCode = tAddr.Zip;
                                    sketchData.data.borrower = theAccount.OwnerParty.FullNameLastFirst;
                                    sketchData.data.clientAddress =
                                        Strings.Trim(tAddr.Address1 + " " + tAddr.Address2);
                                }

                                string sketchDataSerialized = WisejSerializer.Serialize(sketchData);


                                var sketchFileEntry = archive.CreateEntry(sketchFileName);
                                using (var zipStream = sketchFileEntry.Open())
                                {
                                    byte[] sktFileBytes = File.ReadAllBytes(sketchFileNamePath);
                                    zipStream.Write(sktFileBytes, 0, sktFileBytes.Length);
                                }

                                var dataFileEntry = archive.CreateEntry(sketchFileName.Replace(".skt", ".data"));
                                using (var zipStream = dataFileEntry.Open())
                                {
                                    byte[] sktDataBytes = Encoding.ASCII.GetBytes(sketchDataSerialized);
                                    zipStream.Write(sktDataBytes, 0, sktDataBytes.Length);
                                }
                            }

                            zipMemoryStream.Position = 0;
                            string downloadLink = FCUtils.GetDownloadLink(zipMemoryStream,
                                sketchFileName.Replace(".skt", ".trio-skt"));
                            clsPrinterFunctions.DownloadTRIOSketchFile(downloadLink);
                            //FCUtils.Download(zipMemoryStream, sketchFileName.Replace(".skt", ".trio-skt"));
                        }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Edit Sketch", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuPendingTransfer_Click(object sender, System.EventArgs e)
        {
            frmTransferAccount.InstancePtr.Unload();
            frmTransferAccount.InstancePtr.Init(0);
        }

        private void mnuPicMakePrimary_Click(object sender, System.EventArgs e)
        {
            int lngAuto;
            clsDRWrapper clsSave = new clsDRWrapper();
            int intPNum;
            if (!pictureViewModel.Pictures.HasCurrentPicture())
            {
                MessageBox.Show("There is no current picture to make primary", "No Current Picture", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var pic = pictureViewModel.Pictures.CurrentPicture();
            lngAuto = pic.Id;
            intPNum = pic.PicNum.GetValueOrDefault();
            // renumber the pictures
            
            clsSave.Execute("Update picturerecord set picnum = picnum + 1 where MRACCOUNTNUMBER = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
            clsSave.Execute("update picturerecord set picnum = 1 where id = " + FCConvert.ToString(lngAuto), modGlobalVariables.strREDatabase);
            clsSave.Execute("update picturerecord set picnum = picnum - 1 where mraccountnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and picnum > " + FCConvert.ToString(intPNum), modGlobalVariables.strREDatabase);
            MessageBox.Show("Picture Moved", "Moved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            imgPicture.Image = null;
            lblPicName.Text = "Unable to find File";
            pictureViewModel.Pictures.SetToFirstPicture();
            NewLoadPictureInformation();
        }

        private void mnuPicMakeSecondary_Click(object sender, System.EventArgs e)
        {
            int lngAuto;
            clsDRWrapper clsSave = new clsDRWrapper();
            int intPNum;
            if (!pictureViewModel.Pictures.HasCurrentPicture())
            {
                MessageBox.Show("There is no current picture to make secondary", "No Current Picture", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var pic = pictureViewModel.Pictures.CurrentPicture();
            lngAuto = pic.Id;
            intPNum = pic.PicNum.GetValueOrDefault();
            if (intPNum == 1)
            {
                clsSave.Execute("update picturerecord set picnum = 1 where picnum = 2 and mraccountnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsSave.Execute("update picturerecord set picnum = 2 where id = " + FCConvert.ToString(lngAuto), modGlobalVariables.strREDatabase);
                MessageBox.Show("Picture Moved", "Moved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                imgPicture.Image = null;
                lblPicName.Text = "Unable to find File";
                NewLoadPictureInformation();
                return;
            }
            // renumber the pictures
            
            clsSave.Execute("Update picturerecord set picnum = picnum + 1 where picnum > 1 and MRACCOUNTNUMBER = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
            clsSave.Execute("update picturerecord set picnum = 2 where id = " + FCConvert.ToString(lngAuto), modGlobalVariables.strREDatabase);
            clsSave.Execute("update picturerecord set picnum = picnum - 1 where mraccountnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and picnum > " + FCConvert.ToString(intPNum), modGlobalVariables.strREDatabase);
            MessageBox.Show("Picture Moved", "Moved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            imgPicture.Image = null;
            lblPicName.Text = "Unable to find File";
            pictureViewModel.Pictures.SetToFirstPicture();
            NewLoadPictureInformation();
        }

        private void mnuPreviousAccountFromSearch_Click(object sender, System.EventArgs e)
        {
            sketchesViewModel = null;
            pictureViewModel = null;
            string tstring;
            int intCounter;
            int oldrow;
            GridLand.Row = 0;
            BPGrid.Row = 0;
            BPGrid.Col = 0;
            GridExempts.Row = 0;
            GridExempts.Col = 0;
            GridZone.Row = -1;
            GridSecZone.Row = -1;
            GridTopography.Row = -1;
            GridUtilities.Row = -1;
            GridStreet.Row = -1;
            GridLand.Col = 0;
            GridLand.Row = 1;
            SaleGrid.Col = 0;
            // If BPGrid.Rows > 1 Then
            BPGrid.Row = BPGrid.Rows - 1;
            //Application.DoEvents();
            if (modGlobalVariables.Statics.DataChanged == true && !modGlobalVariables.Statics.boolInPendingMode && cmdSave.Enabled)
            {
                if (MessageBox.Show("Data has been changed but not saved. Would you like to save it now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SaveBPGrid();
                    clsDRWrapper temp = modDataTypes.Statics.MR;
                    if (FillRecordsetFromScreen(ref temp))
                    {
                        modDataTypes.Statics.MR = temp;
                        SaveCard();
                    }
                    else
                    {
                        modDataTypes.Statics.MR = temp;
                    }
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                }
            }
            modGlobalVariables.Statics.DataChanged = false;
            imSketch.Image = null;
            
            lblSketchNumber.Text = "";
            imgPicture.Image = null;
            lblPicName.Text = "Unable to find File";
            txtPicDesc.Text = "Enter Comment or Description Here";
            SSTab1.SelectedIndex = 0;
            oldrow = frmRESearch.InstancePtr.Grid.Row;
        keepsearching:
            ;
            modGNBas.Statics.LongScreenSearch = true;
            if (frmRESearch.InstancePtr.Grid.Row > 1)
            {
                frmRESearch.InstancePtr.Grid.Row -= 1;
            }
            else
            {
                frmRESearch.InstancePtr.Grid.Row = oldrow;
                return;
            }
            tstring = frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 7);
            if (tstring == "DEL")
            {
                goto keepsearching;
            }
            modGlobalVariables.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 5))));
            modGlobalVariables.Statics.gcurrsaledate = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 3));
            modGlobalVariables.Statics.gintLastAccountNumber = modGlobalVariables.Statics.CurrentAccount;
            modGNBas.Statics.response = "X";
            modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber));
            clsDRWrapper tLoad = new clsDRWrapper();
            tLoad.OpenRecordset("select count(rscard) as thecount from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
            if (!tLoad.EndOfFile())
            {
                // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(Math.Round(Conversion.Val(tLoad.Get_Fields("thecount"))));
            }
            if (modGNBas.Statics.gintMaxCards < 1)
                modGNBas.Statics.gintMaxCards = 1;
            LoadForm();

            lblNumCards.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
            cmbCardNumber.Text = FCConvert.ToString(1);
            cmbCardNumber_Validate(false);
            // FillTheScreen
            lngOriginalOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ownerpartyid"))));
            lngPOOwnerID = lngOriginalOwnerID;
            lngPOSecOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("secownerpartyid"))));
            strPODeedName1 = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");
            strPODeedName2 = modDataTypes.Statics.MR.Get_Fields_String("DeedName2");
            // TODO Get_Fields: Field [OwnerAddress1] not found!! (maybe it is an alias?)
            strPOAddr1 = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("OwnerAddress1")));
            // TODO Get_Fields: Field [owneraddress2] not found!! (maybe it is an alias?)
            strPOAddr2 = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("owneraddress2")));
            // TODO Get_Fields: Field [ownercity] not found!! (maybe it is an alias?)
            strPOCity = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("ownercity")));
            // TODO Get_Fields: Field [ownerstate] not found!! (maybe it is an alias?)
            strPOState = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("ownerstate")));
            // TODO Get_Fields: Field [Ownerzip] not found!! (maybe it is an alias?)
            strPOZip = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("Ownerzip")));
            if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate") + ""))
            {
                dtOriginalSaleDate = (DateTime)modDataTypes.Statics.MR.Get_Fields_DateTime("saledate");
            }
            else
            {
                dtOriginalSaleDate = DateTime.FromOADate(0);
            }
            GridNeighborhood.Row = 0;
            GridZone.Row = 0;
            GridSecZone.Row = 0;
            GridTopography.Row = 0;
            GridUtilities.Row = 0;
            GridStreet.Row = 0;
            GridLand.Col = 0;
            GridLand.Row = 1;
            SaleGrid.Col = 0;
            // If BPGrid.Rows > 1 Then
            BPGrid.Row = BPGrid.Rows - 1;
            // End If
            GridExempts.Row = 1;
            SaleGrid.Row = 2;
        }

        private void mnuPrint1pagepropertycard_Click(object sender, System.EventArgs e)
        {
            if (modGlobalVariables.Statics.DataChanged)
            {
                if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }
            modProperty.Statics.RUNTYPE = "D";
            modProperty.Statics.boolFromProperty = true;
            modGlobalVariables.Statics.boolUseArrays = true;
            mnuPrint1pagepropertycard.Enabled = false;
            modGlobalRoutines.Check_Arrays();
            boolCalculating = true;
            mnuSave_Click();
            // must save values before calculating
            modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
            modGlobalVariables.Statics.gintMinAccountRange = modGlobalVariables.Statics.gintLastAccountNumber;
            modGlobalVariables.Statics.gintMaxAccountRange = modGlobalVariables.Statics.gintLastAccountNumber;
            modPrintRoutines.Statics.gstrFieldName = "RSAccount";
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            rpt1PagePropertyCard.InstancePtr.Init(true, false, true);
            boolCalculating = false;
            mnuPrint1pagepropertycard.Enabled = true;
            modGlobalVariables.Statics.DataChanged = false;
            modGlobalVariables.Statics.intCurrentCard = 1;
            optCardNumber_MouseDown_242(modGlobalVariables.Statics.intCurrentCard - 1, 1, 0, 1, 1);
            modSpeedCalc.Statics.boolCalcErrors = false;
            modSpeedCalc.Statics.CalcLog = "";
        }

        private void mnuPrintAccountInformationSheet_Click(object sender, System.EventArgs e)
        {
            rptAccountInformationSheet.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, DateTime.Now);
        }

        private void mnuPrintAllPropertyCards_Click(object sender, System.EventArgs e)
        {
            rptPropertyCard.InstancePtr.Init(true, 1, modGlobalVariables.Statics.gintLastAccountNumber.ToString(), modGlobalVariables.Statics.gintLastAccountNumber.ToString());
        }

        private void mnuPrintCard_Click(object sender, System.EventArgs e)
        {
            rptInputCard.InstancePtr.Start(false, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
        }

        private void mnuPrintCards_Click(object sender, System.EventArgs e)
        {
            rptInputCard.InstancePtr.Start(false, modGlobalVariables.Statics.gintLastAccountNumber);
        }

        private void mnuPrintIncomeApproach_Click(object sender, System.EventArgs e)
        {
            if (modGlobalVariables.Statics.boolRegRecords)
            {
                rptIncomeApproach.InstancePtr.Init(ref modGlobalVariables.Statics.gintLastAccountNumber, ref modGNBas.Statics.gintCardNumber, txtDeedName1.Text, true);
            }
        }

        private void mnuPrintInterestedParties_Click(object sender, System.EventArgs e)
        {
            rptInterestedParty.InstancePtr.Init(true, 1, modGlobalVariables.Statics.gintLastAccountNumber.ToString(), modGlobalVariables.Statics.gintLastAccountNumber.ToString(), false);
        }

        private void mnuPrintLabel_Click(object sender, System.EventArgs e)
        {
            int intLType;
            modGlobalVariables.Statics.gboolPrintAsMailing = false;
            intLType = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("REF2LabelType"))));
            if (intLType != modLabels.CNSTLBLTYPEDYMO30256 && intLType != modLabels.CNSTLBLTYPEDYMO30252)
            {
                rptLabels.InstancePtr.PrintReport(false);
            }
            else
            {
                rptLaserLabels.InstancePtr.Init(true, 2, 2, 1, intLType, 640, FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "", 0, "", 0, 0, 1, "", false, true);
            }
            if (txtMRRSMAPLOT.Enabled)
                txtMRRSMAPLOT.Focus();
            // End: code Moved
        }

        private void mnuPrintMailing_Click(object sender, System.EventArgs e)
        {
            int intLType;
            modGlobalVariables.Statics.gboolPrintAsMailing = true;
            intLType = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("REF2LabelType"))));
            if (intLType != modLabels.CNSTLBLTYPEDYMO30256 && intLType != modLabels.CNSTLBLTYPEDYMO30252)
            {
                rptLabels.InstancePtr.PrintReport(false);
            }
            else
            {
                rptLaserLabels.InstancePtr.Init(true, 2, 1, 1, intLType, 640, FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "", 0, "", 0, 0, 1, "", false, true);
            }
        }

        private void mnuPrintPropertyCard_Click(object sender, System.EventArgs e)
        {
            rptPropertyCard.InstancePtr.Init(false, 1, Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber), Conversion.Str(modGNBas.Statics.gintCardNumber), false, true);
        }

        //private bool SaveRange(string strFilename, int lngStart, int lngEnd, GrapeCity.ActiveReports.Document.SectionDocument document)
        //{
        //    bool SaveRange = false;
        //    try
        //    {
        //        // On Error GoTo ErrorHandler
        //        if (strFilename != "")
        //        {
        //            if (lngEnd >= lngStart)
        //            {
        //                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pe =
        //                    new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
        //                pe.Export(document, strFilename, FCConvert.ToString(lngStart) + "-" + FCConvert.ToString(lngEnd));
        //            }
        //        }

        //        SaveRange = true;
        //        return SaveRange;
        //    }
        //    catch (ThreadAbortException exc)
        //    {

        //    }
        //    catch (Exception ex)
        //    {
        //        // ErrorHandler:
        //    }
        //    return SaveRange;
        //}

        private void mnuPrintPropertyCardsPDF_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Would you like to preview the pages?", "View Pages?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                //var testprint = new rptpdfPropertyCard();

                //testprint.ReportEnd += Testprint_ReportEnd;
                //testprint.Init(true, 1, Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber), Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber), false, false, true);

                rptpdfPropertyCard.InstancePtr.Init(true, 1, Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber), Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber), false, false, true);
            }
            else
            {
                rptpdfPropertyCard.InstancePtr.Init(true, 1, Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber), Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber));
            }
        }

        //private void Testprint_ReportEnd(object sender, EventArgs e)
        //{
        //    var strSaveDir = FCFileSystem.Statics.UserDataFolder + "\\RealEstate\\PropertyCards\\";
        //    var report = (rptpdfPropertyCard)sender;
        //    var document = report.Document;
        //    SaveRange(strSaveDir + "test.pdf", 1, 2, document);
        //}

        private void mnuPrintScreen_Click(object sender, System.EventArgs e)
        {
            double dblRatio = 0;
            int lngOWidth = 0;
            int lngOHeight = 0;
            try
            {
                CommonDialog1.Show();
            }
            catch
            {
            }
            if (Information.Err().Number == 0)
            {
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                dblRatio = FCConvert.ToDouble(Height / Width);
                lngOWidth = Width;
                lngOHeight = Height;
                Width = FCConvert.ToInt32(7.5 * 1440) / FCScreen.TwipsPerPixelX;
                // fit on a piece of paper
                Height = FCConvert.ToInt32(dblRatio * Width);
                //Application.DoEvents();
                FCGlobal.Printer.Print();
                FCGlobal.Printer.EndDoc();
                FCGlobal.Screen.MousePointer = 0;
                Width = lngOWidth;
                Height = lngOHeight;
                //Application.DoEvents();
            }
            else
            {
                MessageBox.Show("Print was cancelled by you.", "Print Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            Information.Err().Clear();
            /*? On Error GoTo 0 */
            FCGlobal.Screen.MousePointer = 0;
        }

        public void mnuPrintScreen_Click()
        {
            mnuPrintScreen_Click(mnuPrintScreen, new System.EventArgs());
        }

        private void mnuSavePending_Click(object sender, System.EventArgs e)
        {
            // compare the current copy of the recordset with the database
            clsDRWrapper clsExecute = new clsDRWrapper();
            int lngTransactionID;
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            /*? On Error Resume Next  */
            try
            {
                gridPropertyCode.Row = -1;
                gridLandCode.Row = -1;
                gridBldgCode.Row = -1;
                gridTranCode.Row = -1;
                GridNeighborhood.Row = -1;
                GridZone.Row = -1;
                GridSecZone.Row = -1;
                GridTopography.Row = -1;
                GridUtilities.Row = -1;
                GridStreet.Row = -1;
                GridLand.Row = 0;
                //FC:FINAL:AM:#3238 - code not needed anymore; controls are already validated when pressing the save button
                //if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING1")
                //{
                //	frmNewREProperty.InstancePtr.GridDwelling1.Col = 0;
                //}
                //else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING2")
                //{
                //	frmNewREProperty.InstancePtr.GridDwelling2.Col = 0;
                //}
                //else if (frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPIInfCode1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPILand1Inf" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "mebPILand1Units" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPILand1Type" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIType1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIYear1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIUnits1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIGradeCd1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIGradePct1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROICond1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIPctPhys1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIPctFunct1")
                //{
                //	FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, this.ActiveControl.GetIndex(), false);
                //}
                //else
                //{
                //	FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, false);
                //}
                try
                {
                    // On Error GoTo ErrorHandler
                    clsDRWrapper temp = modDataTypes.Statics.MR;
                    FillRecordsetFromScreen(ref temp);
                    temp.Set_Fields("RSMapLot",theAccount.MapLot);
                    temp.Set_Fields("DeedName1", theAccount.DeedName1);
                    temp.Set_Fields("DeedName2",theAccount.DeedName2);
                    modDataTypes.Statics.MR = temp;
                    CheckPending(ref dtMinPendingDate);
                    modGlobalVariables.Statics.DataChanged = false;
                    // Call clsExecute.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Created Pending Change','Created a pending change from the long screen for account " & gintLastAccountNumber & " card " & gintCardNumber & "')", strredatabase)
                    modGlobalFunctions.AddCYAEntry_26("RE", "Created Pending Change", "Created a pending change from the long screen for account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    MessageBox.Show("Pending changes saved in pending activity list.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save pending changes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            catch
            {
            }
        }

        public void mnuSavePending_Click()
        {
            mnuSavePending_Click(mnuSavePending, new System.EventArgs());
        }

        private void txt2ndOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txt2ndOwnerID.Text) != theAccount.SecOwnerID)
            {
                cParty tParty = new cParty();
                cPartyController tCont = new cPartyController();
                if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txt2ndOwnerID.Text))))
                {
                    e.Cancel = true;
                    MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txt2ndOwnerID.Text = FCConvert.ToString(theAccount.SecOwnerID);
                    return;
                }
                theAccount.SecOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(txt2ndOwnerID.Text)));
                tParty = theAccount.SecOwnerParty;
                tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txt2ndOwnerID.Text)));
                ShowSecOwner();
            }
        }

        private void txtMROICond1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROICond1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROICond1.GetIndex((FCTextBox)sender);
            txtMROICond1_KeyPress(index, sender, e);
        }

        private void txtMROIGradeCd1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIGradeCd1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIGradeCd1.GetIndex((FCTextBox)sender);
            txtMROIGradeCd1_KeyPress(index, sender, e);
        }

        private void txtMROIGradePct1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIGradePct1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIGradePct1.GetIndex((FCTextBox)sender);
            txtMROIGradePct1_KeyPress(index, sender, e);
        }

        private void txtMROIPctFunct1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIPctFunct1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIPctFunct1.GetIndex((FCTextBox)sender);
            txtMROIPctFunct1_KeyPress(index, sender, e);
        }

        private void txtMROIPctPhys1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIPctPhys1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIPctPhys1.GetIndex((FCTextBox)sender);
            txtMROIPctPhys1_KeyPress(index, sender, e);
        }

        private void txtMROISoundValue_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROISoundValue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROISoundValue.GetIndex((FCTextBox)sender);
            txtMROISoundValue_KeyPress(index, sender, e);
        }

        private void txtMROIUnits1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIUnits1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIUnits1.GetIndex((FCTextBox)sender);
            txtMROIUnits1_KeyPress(index, sender, e);
        }

        private void txtMROIYear1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIYear1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIYear1.GetIndex((FCTextBox)sender);
            txtMROIYear1_KeyPress(index, sender, e);
        }

        private void txtMRRSPrevMaster_Enter(object sender, System.EventArgs e)
        {
            txtMRRSPrevMaster.SelectionStart = 0;
            txtMRRSPrevMaster.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSRef1_Enter(object sender, System.EventArgs e)
        {
            txtMRRSRef1.SelectionStart = 0;
            txtMRRSRef1.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSRef1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9)
            {
                KeyCode = (Keys)0;
                string strTemp = "";
                GridToolTip.Visible = false;
                strTemp = "The Reference 1 field may be used to record any additional property information.  If the TRIO tax billing system is used, Reference 1 can be optionally printed in the commitment book.";
                ToolTip.Update("Ref1", txtMRRSRef1.Left, txtMRRSRef1.Top, strTemp, -1, -1, txtMRRSRef1.Left + Frame3.Left, txtMRRSRef1.Top + SSTab1.Top + Frame3.Top);
            }
        }

        private void txtMRRSRef2_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRRSRef2.SelectionStart = 0;
            txtMRRSRef2.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSRef2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9)
            {
                KeyCode = (Keys)0;
                string strTemp = "";
                GridToolTip.Visible = false;
                strTemp = "The Reference 2 field may be used to record any additional property information.  If the TRIO tax billing system is used, Reference 2 can be optionally printed in the commitment book.";
                ToolTip.Update("Ref2", txtMRRSRef2.Left, txtMRRSRef2.Top, strTemp, -1, -1, txtMRRSRef2.Left + Frame3.Left, txtMRRSRef2.Top + SSTab1.Top + Frame3.Top);
            }
        }

        private void gridTranCode_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
        }

        private void gridTranCode_ComboCloseUp(object sender, EventArgs e)
        {
            //FC:FINAL:MSH - i.issue #1026: mandatory ending editing of Grid combobox will miss selected value
            //gridTranCode.EndEdit();
            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                SetupGridNeighborhood();
                SetupGridZone();
                SetupGridUtilities();
                SetupGridStreet();
                SetupGridSecZone();
                SetupGridTopography();
            }
        }

        private void chkTaxAcquired_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkBankruptcy.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }

        private void chkBankruptcy_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked && !modGlobalConstants.Statics.gboolCL)
            {
                chkTaxAcquired.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }

        private void chkRLivingTrust_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9)
            {
                KeyCode = (Keys)0;
                string strTemp = "";
                strTemp = "This information is used for the Veteran Exemptions worksheet for the MVR";
                GridToolTip.Visible = false;
                ToolTip.Update("LivingTrust", chkRLivingTrust.Left, chkRLivingTrust.Top, strTemp, chkRLivingTrust.Left, chkRLivingTrust.Top + Frame3.Top + SSTab1.Top);
            }
        }

        private void GridExempts_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
        }

        private void GridExempts_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridExempts[e.ColumnIndex, e.RowIndex];
            int lngMrow;
            if (boolCalculating == true)
            {
                return;
            }
            lngMrow = GridExempts.GetFlexRowIndex(e.RowIndex);
            if (lngMrow < 1)
            {
                //ToolTip1.SetToolTip(GridExempts, "");
                cell.ToolTipText = "";
            }
            else
            {
                if (Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE)) == CNSTNOEXEMPT)
                {
                    //ToolTip1.SetToolTip(GridExempts, "");
                    cell.ToolTipText = "";
                }
                else
                {
                    //ToolTip1.SetToolTip(GridExempts, FCConvert.ToString(Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE))) + "  " + GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngMrow, CNSTEXEMPTCOLCODE) + "     Value: " + Strings.Format(Conversion.Val(theAccount.Get_ExemptVal(lngMrow)), "#,###,##0"));
                    cell.ToolTipText = FCConvert.ToString(Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE))) + "  " + GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngMrow, CNSTEXEMPTCOLCODE) + "     Value: " + Strings.Format(Conversion.Val(theAccount.Get_ExemptVal(lngMrow)), "#,###,##0");
                }
            }
        }

        private void BPGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (BPGrid.Rows < 2)
            {
                BPGrid.AddItem(FCConvert.ToString(true));
                AutoPositionBPGrid();
            }
            else if ((Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol)) != string.Empty))
            {
                BPGrid.AddItem(FCConvert.ToString(true));
                AutoPositionBPGrid();
            }
        }

        private void BPGrid_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            // ToolTip.HideTip
            if (KeyCode == Keys.Delete)
            {
                // delete key
                if (BPGrid.Row > 0)
                {
                    boolBookPageChanged = true;
                    BPGrid.RemoveItem(BPGrid.Row);
                    if (BPGrid.Rows < 2)
                    {
                        BPGrid.AddItem(FCConvert.ToString(true));
                        AutoPositionBPGrid();
                    }
                    else if ((Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol)) != string.Empty))
                    {
                        BPGrid.AddItem(FCConvert.ToString(true));
                        AutoPositionBPGrid();
                    }
                }
            }
            if (KeyCode == Keys.F9)
            {
                //FC:FINAL:AM:#1873 - remove Shift
                //if (e.Shift)
                {
                    KeyCode = 0;
                    string strTemp = "";
                    GridToolTip.Visible = false;
                    strTemp = "Mark book page entries that are current by placing a check mark in the field to the left";
                    ToolTip.Update("BookPage", BPGrid.Left, BPGrid.Top, strTemp, BPGrid.Left + Frame3.Left + SSTab1.Left, BPGrid.Top + Frame3.Top + SSTab1.Top);
                }
            }
        }

        private void BPGrid_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.F9)
            {
                //FC:FINAL:AM:#1873 - remove Shift
                //if (e.Shift)
                {
                    KeyCode = 0;
                    string strTemp = "";
                    strTemp = "Mark book page entries that are current by placing a check mark in the field to the left";
                    GridToolTip.Visible = false;
                    ToolTip.Update("BookPage", BPGrid.Left, BPGrid.Top, strTemp, BPGrid.Left + Frame3.Left + SSTab1.Left, BPGrid.Top + Frame3.Top + SSTab1.Top);
                }
            }
        }

        private void BPGrid_RowColChange(object sender, System.EventArgs e)
        {
            BPGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            if (BPGrid.Row == BPGrid.Rows - 1)
            {
                if (BPGrid.Col == BPGrid.Cols - 1)
                {
                    BPGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
                }
            }
        }

        private void BPGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
            boolBookPageChanged = true;
            if (BPGrid.Rows < 2)
            {
                BPGrid.AddItem(FCConvert.ToString(true));
                AutoPositionBPGrid();
            }
            else if ((Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol)) != string.Empty))
            {
                BPGrid.AddItem(FCConvert.ToString(true));
                AutoPositionBPGrid();
            }
        }

        private void SaleGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (SaleGrid.Row != 2)
                return;
            //Application.DoEvents();
            switch (SaleGrid.Col)
            {
                case CNSTSALECOLDATE:
                    {
                        if (!Information.IsDate(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLDATE)))
                        {
                            theAccount.SaleDate = DateTime.FromOADate(0);
                        }
                        else
                        {
                            if (theAccount.SaleDate.ToOADate() != DateAndTime.DateValue(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLDATE)).ToOADate())
                            {
                                if (!modGlobalVariables.Statics.boolInPendingMode)
                                {
                                    boolSaleChanged = true;
                                }
                            }
                            theAccount.SaleDate = DateAndTime.DateValue(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLDATE));
                            modDataTypes.Statics.OUT.Set_Fields("saledate", theAccount.SaleDate);
                            modDataTypes.Statics.CMR.Set_Fields("saledate", theAccount.SaleDate);
                            modDataTypes.Statics.DWL.Set_Fields("saledate", theAccount.SaleDate);
                        }
                        break;
                    }
                case CNSTSALECOLFINANCING:
                    {
                        theAccount.SaleFinancing = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLFINANCING))));
                        break;
                    }
                case CNSTSALECOLPRICE:
                    {
                        if (Information.IsNumeric(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLPRICE)))
                        {
                            SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLPRICE), "###,###,###,##0"));
                            if (Conversion.Val(theAccount.SalePrice) != FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLPRICE))))
                            {
                                // If Val(MR.Fields("pisaleprice") & "") <> CLng(SaleGrid.TextMatrix(Row, CNSTSALECOLPRICE)) Then
                                boolSaleChanged = true;
                            }
                            // MR.Fields("pisaleprice") = CLng(SaleGrid.TextMatrix(Row, CNSTSALECOLPRICE))
                            theAccount.SalePrice = FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLPRICE)));
                        }
                        else
                        {
                            SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLPRICE, FCConvert.ToString(0));
                        }
                        break;
                    }
                case CNSTSALECOLSALE:
                    {
                        // MR.Fields("PISALETYPE") = Val(SaleGrid.TextMatrix(Row, CNSTSALECOLSALE))
                        theAccount.SaleType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLSALE))));
                        break;
                    }
                case CNSTSALECOLVALIDITY:
                    {
                        if (theAccount.SaleValidity != Conversion.Val(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLVALIDITY)))
                        {
                            // If Val(MR.Fields("pisalevalidity" & "")) <> Val(SaleGrid.TextMatrix(Row, CNSTSALECOLVALIDITY)) Then
                            if (Conversion.Val(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLVALIDITY)) == 1)
                            {
                                boolSaleChanged = true;
                            }
                        }
                        // MR.Fields("pisalevalidity") = Val(SaleGrid.TextMatrix(Row, CNSTSALECOLVALIDITY))
                        theAccount.SaleValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLVALIDITY))));
                        break;
                    }
                case CNSTSALECOLVERIFIED:
                    {
                        // MR.Fields("pisaleverified") = Val(SaleGrid.TextMatrix(Row, CNSTSALECOLVERIFIED))
                        theAccount.SaleVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(SaleGrid.Row, CNSTSALECOLVERIFIED))));
                        break;
                    }
            }
            //end switch
        }

        private void SaleGrid_RowColChange(object sender, System.EventArgs e)
        {
            if (SaleGrid.Row == SaleGrid.Rows - 1 && SaleGrid.Col == CNSTSALECOLVALIDITY)
            {
                SaleGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                SaleGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void txtMRRSMAPLOT_TextChanged(object sender, System.EventArgs e)
        {
            lblMapLotCopy[0].Text = txtMRRSMAPLOT.Text;
            lblMapLotCopy[1].Text = txtMRRSMAPLOT.Text;
            lblMapLotCopy[2].Text = txtMRRSMAPLOT.Text;
        }

        private void txtMRRSMAPLOT_DoubleClick(object sender, System.EventArgs e)
        {
            Support.SendKeys("{F1}", false);
        }

        private void txtMRRSMAPLOT_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRRSMAPLOT.SelectionStart = 0;
            txtMRRSMAPLOT.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSMAPLOT_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9)
            {
                KeyCode = (Keys)0;
                string strTemp = "";
                GridToolTip.Visible = false;
                strTemp = "The map/lot field should be formatted in a consistent manner. Most commonly used is a 3 digit map, 3 digit lot and a 3 digit sub lot with the digits separated by dashes: Map/Lot:  015-054-003  -or-  022-029-A or R01-063-003-001  Four digits may be used: Map/Lot: 0001-0054-0023. It is important that whatever format is chosen the map/lot numbers be entered consistently to insure that sort and search routines work correctly";
                // Call ToolTip.Update("MapLot", CLng(X), CLng(Y), strTemp, , txtMRRSMAPLOT.Top + txtMRRSMAPLOT.Height, txtMRRSMAPLOT.Left)
                ToolTip.Update("MapLot", txtMRRSMAPLOT.Left, txtMRRSMAPLOT.Top, strTemp, -1, FCConvert.ToInt32(SSTab1.Top + (SSTab1.TabHeight / 14.4 / 2)), txtMRRSMAPLOT.Left);
            }
        }

        private void MaskEdBox1_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        public void selecteverything()
        {
            int tlength = 0;
            string tstring = "";
            if (FCGlobal.Screen.ActiveControl is FCTextBox)
                tstring = FCGlobal.Screen.ActiveControl.Text + "";
            // End If
            if (FCGlobal.Screen.ActiveControl is FCTextBox)
            {
                tlength = tstring.Length;
                (FCGlobal.Screen.ActiveControl as FCTextBox).SelectionStart = 0;
                (FCGlobal.Screen.ActiveControl as FCTextBox).SelectionLength = tlength;
            }
        }

        private void MaskEdBox1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                        if (!modHelpFiles.Statics.Helping)
                        {
                            modHelpFiles.Statics.Helping = true;
                            KeyCode = (Keys)0;
                            modHelpFiles.HelpFiles_2("PROPERTY", 146);
                            //Application.DoEvents();
                            if (Conversion.Val(modGNBas.Statics.response) != 0)
                                MaskEdBox1.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                            frmHelp.InstancePtr.Unload();
                        }
                        break;
                    }
            }
            //end switch
        }

        private void MaskEdbox3_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void MaskEdbox3_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("PROPERTY", 145);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    MaskEdbox3.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        private void txtMRRSLocnumalph_DoubleClick(object sender, System.EventArgs e)
        {
            Support.SendKeys("{F1}", false);
        }

        private void txtMRRSLocnumalph_Enter(object sender, System.EventArgs e)
        {
            txtMRRSLocnumalph.SelectionStart = 0;
            txtMRRSLocnumalph.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSLocnumalph_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
                modHelpFiles.HelpFiles_2("Property", 9);
                frmHelp.InstancePtr.Unload();
            }
        }

        private void txtMRRSLocapt_DoubleClick(object sender, System.EventArgs e)
        {
            Support.SendKeys("{F1}", false);
        }

        private void txtMRRSLocapt_Enter(object sender, System.EventArgs e)
        {
            txtMRRSLocapt.SelectionStart = 0;
            txtMRRSLocapt.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSLocStreet_DoubleClick(object sender, System.EventArgs e)
        {
            Support.SendKeys("{F1}", false);
        }

        private void txtMRRSLocStreet_Enter(object sender, System.EventArgs e)
        {
            txtMRRSLocStreet.SelectionStart = 0;
            txtMRRSLocStreet.SelectionLength = modGNBas.Statics.OverType;
        }

        private void txtMRRSLocStreet_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
                modHelpFiles.HelpFiles_2("Property", 10);
                frmHelp.InstancePtr.Unload();
            }
        }

        public void txtOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txtOwnerID.Text) != theAccount.OwnerID)
            {
                cParty tParty = new cParty();
                cPartyController tCont = new cPartyController();
                if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text))))
                {
                    e.Cancel = true;
                    MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtOwnerID.Text = FCConvert.ToString(theAccount.OwnerID);
                    return;
                }
                theAccount.OwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtOwnerID.Text)));
                tParty = theAccount.OwnerParty;
                tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text)));
                ShowOwner();
                ShowAddress();
            }
        }

        //private void UpDownCard_ValueChanged(object sender, System.EventArgs e)
        //{
        //	//cmbCardNumber.Text = FCConvert.ToString(FCConvert.ToInt32(UpDownCard.Value);
        //	cmbCardNumber_Validate(false);
        //}

        private void cmbCardNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbCardNumber_Validate(false);
        }

        private void cmbCardNumber_Validating_2(bool Cancel)
        {
            cmbCardNumber_Validating(new object(), new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void cmbCardNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // If Val(cmbCardNumber.Text) < 0 Or Val(cmbCardNumber.Text) > VScrollCard.Max Then
            if (Conversion.Val(cmbCardNumber.Text) < 0 || Conversion.Val(cmbCardNumber.Text) > modGNBas.Statics.gintMaxCards)
            {
                e.Cancel = true;
                cmbCardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
                return;
            }
            if (modGNBas.Statics.gintCardNumber == Conversion.Val(cmbCardNumber.Text))
                return;
            if (modGlobalVariables.Statics.DataChanged)
            {
                if (modGlobalVariables.Statics.boolInPendingMode)
                {
                    if (MessageBox.Show("Save pending changes to card?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        mnuSavePending_Click();
                    }
                }
                else
                {
                    if (MessageBox.Show("Save changes to card: " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (CheckValues())
                        {
                            // Call SaveData2
                            SaveCard();
                            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        }
                    }
                }
            }

            if (!boolAddingACard)
            {
                RefreshCard(Convert.ToInt32(Conversion.Val(cmbCardNumber.Text)));
            }
            //RefreshCard(Convert.ToInt32(Conversion.Val(cmbCardNumber.Text)));
        }

        public void RefreshCard(int intCardToUse)
        {
            SSTab1.SelectedIndex = 0;
            modGlobalVariables.Statics.DataChanged = false;
            boolCalcDataChanged = false;
            modGNBas.Statics.gintCardNumber = intCardToUse;
            imgPicture.Image = null;
            imSketch.Image = null;
            lblSketchNumber.Text = "";
            LoadCard(theAccount.Account, modGNBas.Statics.gintCardNumber);
            ShowCard();
            if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "Y")
            {
                if (SSTab1.SelectedIndex == 2)
                    SSTab1.SelectedIndex = 1;
            }
            else if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "C")
            {
                if (SSTab1.SelectedIndex == 1)
                    SSTab1.SelectedIndex = 2;
            }
            else
            {
                // the account has neither data
                if ((SSTab1.SelectedIndex == 1) || (SSTab1.SelectedIndex == 2))
                {
                    SSTab1.SelectedIndex = 0;
                }
            }
        }
        public void cmbCardNumber_Validate(bool Cancel)
        {
            cmbCardNumber_Validating(cmbCardNumber, new System.ComponentModel.CancelEventArgs(Cancel));
        }

        public bool CheckValues()
        {
            bool CheckValues = false;
            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                int lngTemp = 0;
                lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                if (lngTemp < 1)
                {
                    lngTemp = frmPickRegionalTown.InstancePtr.Init(-1, false, "A town has not been specified");
                    if (lngTemp > 0)
                    {
                        gridTranCode.TextMatrix(0, 0, FCConvert.ToString(lngTemp));
                    }
                }
            }
            CheckValues = true;
            return CheckValues;
        ErrorHandler:
            ;
            return CheckValues;
        }

        private bool FillRecordsetFromScreen(ref clsDRWrapper TRec)
        {
            bool FillRecordsetFromScreen = false;
            // vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
            string strTemp;
            // vbPorter upgrade warning: dtTempDate As DateTime	OnWrite(string, short)
            DateTime dtTempDate = default(DateTime);
            // vbPorter upgrade warning: strDate As string	OnWrite(string, short)
            string strDate = "";
            clsDRWrapper rsOwners = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                FillRecordsetFromScreen = false;
                int x;
                modGlobalVariables.Statics.CurrentAccount = modGlobalVariables.Statics.gintLastAccountNumber;
                theAccount.MapLot = txtMRRSMAPLOT.Text;
                for (x = 1; x <= 3; x++)
                {
                    if (Conversion.Val(GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE)) > -1)
                    {
                        theAccount.Set_ExemptCode(x, FCConvert.ToInt32(Math.Round(Conversion.Val(GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE)))));
                        theAccount.Set_ExemptPct(x, FCConvert.ToInt32(Math.Round(Conversion.Val(GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT)))));
                        TRec.Set_Fields("riexemptcd" + x, Conversion.Val(GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE)));
                        TRec.Set_Fields("exemptpct" + x, Conversion.Val(GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT)));
                    }
                    else
                    {
                        theAccount.Set_ExemptCode(x, 0);
                        theAccount.Set_ExemptPct(x, 100);
                        TRec.Set_Fields("riexemptcd" + x, 0);
                        TRec.Set_Fields("exemptpct" + x, 100);
                    }
                }

                theAccount.DeedName1 = txtDeedName1.Text.Trim();
                theAccount.DeedName2 = txtDeedName2.Text.Trim();
                if (chkZoneOverride.CheckState == Wisej.Web.CheckState.Checked)
                {
                    TRec.Set_Fields("zoneoverride", true);
                }
                else
                {
                    TRec.Set_Fields("zoneoverride", false);
                }
                TRec.Set_Fields("pistreetcode", Conversion.Val(txtMRPIStreetCode.Text + ""));
                TRec.Set_Fields("pixcoord", Strings.Trim(txtMRPIXCoord.Text));
                TRec.Set_Fields("piycoord", Strings.Trim(txtMRPIYCoord.Text));
                TRec.Set_Fields("entrancecode", Conversion.Val(MaskEdbox3.Text + ""));
                TRec.Set_Fields("informationcode", Conversion.Val(MaskEdBox1.Text));
                strTemp = T2KDateInspected.Text;
                if (Information.IsDate(strTemp))
                {
                    TRec.Set_Fields("dateinspected", Strings.Format(strTemp, "MM/dd/yyyy"));
                }
                else
                {
                    TRec.Set_Fields("dateinspected", 0);
                }
                TRec.Set_Fields("rslocstreet", txtMRRSLocStreet.Text);
                TRec.Set_Fields("rslocnumalph", txtMRRSLocnumalph.Text);
                TRec.Set_Fields("rslocapt", txtMRRSLocapt.Text);
                TRec.Set_Fields("rsref1", txtMRRSRef1.Text);
                TRec.Set_Fields("rsref2", txtMRRSRef2.Text);
                TRec.Set_Fields("ritrancode", Conversion.Val(gridTranCode.TextMatrix(0, 0)));
                TRec.Set_Fields("pineighborhood", Conversion.Val(GridNeighborhood.TextMatrix(0, 0)));
                TRec.Set_Fields("pizone", Conversion.Val(GridZone.TextMatrix(0, 0)));
                TRec.Set_Fields("piseczone", Conversion.Val(GridSecZone.TextMatrix(0, 0)));
                TRec.Set_Fields("pitopography1", Conversion.Val(GridTopography.TextMatrix(0, 0)));
                TRec.Set_Fields("pitopography2", Conversion.Val(GridTopography.TextMatrix(1, 0)));
                TRec.Set_Fields("piutilities1", Conversion.Val(GridUtilities.TextMatrix(0, 0)));
                TRec.Set_Fields("piutilities2", Conversion.Val(GridUtilities.TextMatrix(1, 0)));
                TRec.Set_Fields("pistreet", Conversion.Val(GridStreet.TextMatrix(0, 0)));
                TRec.Set_Fields("ribldgcode", Conversion.Val(gridBldgCode.TextMatrix(0, 0)));
                TRec.Set_Fields("rilandcode", Conversion.Val(gridLandCode.TextMatrix(0, 0)));
                TRec.Set_Fields("propertycode", Conversion.Val(gridPropertyCode.TextMatrix(0, 0)));
                theAccount.TranCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                theAccount.PreviousOwner = txtMRRSPrevMaster.Text;
                if (Information.IsDate(T2KDateInspected.Text))
                {
                    TRec.Set_Fields("dateinspected", T2KDateInspected.Text);
                }
                else
                {
                    TRec.Set_Fields("dateinspected", 0);
                }
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    theAccount.TaxAcquired = chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked;
                    theAccount.InBankruptcy = chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked;
                    if (chkRLivingTrust.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        theAccount.RevocableTrust = true;
                    }
                    else
                    {
                        theAccount.RevocableTrust = false;
                    }
                }
                for (x = 1; x <= 7; x++)
                {
                    TRec.Set_Fields("piland" + x + "type", Conversion.Val(GridLand.TextMatrix(x, 0)));
                    TRec.Set_Fields("piland" + x + "infcode", Conversion.Val(GridLand.TextMatrix(x, 3)));
                    TRec.Set_Fields("piland" + x + "inf", Conversion.Val(GridLand.TextMatrix(x, 2)));
                    if (Conversion.Val(GridLand.TextMatrix(x, 0)) == 0 || Conversion.Val(GridLand.TextMatrix(x, 0)) == 99)
                    {
                        TRec.Set_Fields("piland" + x + "unitsa", "000");
                        TRec.Set_Fields("piland" + x + "unitsb", "000");
                    }
                    else
                    {
                        if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(GridLand.TextMatrix(x, 0))))
                        {
                            switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                            {
                                case modREConstants.CNSTLANDTYPEFRONTFOOT:
                                case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                                    {
                                        // 000x000
                                        if (Strings.Trim(GridLand.TextMatrix(x, 1)) != string.Empty)
                                        {
                                            strTemp = GridLand.TextMatrix(x, 1);
                                            if (Strings.InStr(1, strTemp, "x", CompareConstants.vbTextCompare) < 1)
                                            {
                                                TRec.Set_Fields("piland" + x + "unitsa", "000");
                                                TRec.Set_Fields("piland" + x + "unitsb", "000");
                                            }
                                            else
                                            {
                                                modProperty.Statics.intTemp = Strings.InStr(1, strTemp, "x", CompareConstants.vbTextCompare);
                                                TRec.Set_Fields("piland" + x + "unitsa", Strings.Format(Strings.Mid(strTemp, 1, modProperty.Statics.intTemp - 1), "000"));
                                                TRec.Set_Fields("piland" + x + "unitsb", Strings.Format(Strings.Mid(strTemp, modProperty.Statics.intTemp + 1), "000"));
                                            }
                                        }
                                        else
                                        {
                                            TRec.Set_Fields("piland" + x + "unitsa", "000");
                                            TRec.Set_Fields("piland" + x + "unitsb", "000");
                                        }
                                        // Case 16 To 20
                                        break;
                                    }
                                case modREConstants.CNSTLANDTYPESQUAREFEET:
                                    {
                                        // commas
                                        if (Conversion.Val(GridLand.TextMatrix(x, 1)) > 0)
                                        {
                                            strTemp = FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(GridLand.TextMatrix(x, 1))));
                                            strTemp = Strings.Format(strTemp, "000000");
                                            TRec.Set_Fields("piland" + x + "unitsb", Strings.Right(strTemp, 3));
                                            TRec.Set_Fields("piland" + x + "unitsa", Strings.Mid(strTemp, 1, strTemp.Length - 3));
                                        }
                                        else
                                        {
                                            TRec.Set_Fields("piland" + x + "unitsa", "000");
                                            TRec.Set_Fields("piland" + x + "unitsb", "000");
                                        }
                                        // Case 21 To 46
                                        break;
                                    }
                                case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                                case modREConstants.CNSTLANDTYPEACRES:
                                case modREConstants.CNSTLANDTYPESITE:
                                case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                                case modREConstants.CNSTLANDTYPELINEARFEET:
                                case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                    {
                                        // decimal
                                        if (Conversion.Val(GridLand.TextMatrix(x, 1)) > 0)
                                        {
                                            //FC:FINAL:MSH - i.issue #1066: incorrect converting. Value can be double and converting from double-string to int will throw an exception
                                            //strTemp = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(GridLand.TextMatrix(x, 1)))) * 100, "000000"); // get rid of decimal
                                            strTemp = Strings.Format(Conversion.Val(GridLand.TextMatrix(x, 1)) * 100, "000000");
                                            // get rid of decimal
                                            TRec.Set_Fields("piland" + x + "unitsb", Strings.Right(strTemp, 3));
                                            TRec.Set_Fields("piland" + x + "unitsa", Strings.Mid(strTemp, 1, strTemp.Length - 3));
                                        }
                                        else
                                        {
                                            TRec.Set_Fields("piland" + x + "unitsa", "000");
                                            TRec.Set_Fields("piland" + x + "unitsb", "000");
                                        }
                                        break;
                                    }
                                case 99:
                                    {
                                        TRec.Set_Fields("piland" + x + "unitsa", "000");
                                        TRec.Set_Fields("piland" + x + "unitsb", "000");
                                        break;
                                    }
                            }
                            //end switch
                        }
                    }
                }
                // x
                TRec.Set_Fields("hlupdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                TRec.Set_Fields("rspropertycode", "Y");
                TRec.Set_Fields("rsoutbuildingcode", "N");
                TRec.Set_Fields("rsdwellingcode", modGlobalVariables.Statics.HoldDwelling);
                TRec.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                TRec.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                theAccount.Account = modGlobalVariables.Statics.gintLastAccountNumber;
                TRec.Set_Fields("PIopen1", Conversion.Val(txtMRPIOpen1.Text));
                TRec.Set_Fields("PIopen2", Conversion.Val(txtMRPIOpen2.Text));
                if (Conversion.Val(txtMRPIAcres.Text + "") > 0)
                {
                    TRec.Set_Fields("piacres", FCConvert.ToDouble(txtMRPIAcres.Text));
                }
                else
                {
                    TRec.Set_Fields("piacres", 0);
                }
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    if (Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
                    {
                        TRec.Set_Fields("SaleDate", SaleGrid.TextMatrix(2, CNSTSALECOLDATE));
                        theAccount.SaleDate = FCConvert.ToDateTime(SaleGrid.TextMatrix(2, CNSTSALECOLDATE));
                    }
                    else
                    {
                        TRec.Set_Fields("SaleDate", 0);
                        theAccount.SaleDate = DateTime.FromOADate(0);
                    }
                    TRec.Set_Fields("pisaleprice", Conversion.CLng(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)));
                    TRec.Set_Fields("pisalevalidity", Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY)));
                    TRec.Set_Fields("pisalefinancing", Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING)));
                    TRec.Set_Fields("pisaleverified", Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED)));
                    TRec.Set_Fields("pisaletype", Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE)));
                    theAccount.SalePrice = Conversion.CLng(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE));
                    theAccount.SaleValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
                    theAccount.SaleFinancing = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
                    theAccount.SaleVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
                    theAccount.SaleType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
                }
                if (boolNameChanged)
                {
                    boolNameChanged = false;
                    // owner changed so save in previous owner table
                    // if saledate is different, then record that too
                    if (boolSaleChanged)
                    {
                        if (Information.IsDate(TRec.Get_Fields("saledate") + ""))
                        {
                            dtTempDate = FCConvert.ToDateTime(Strings.Format(TRec.Get_Fields_DateTime("saledate"), "MM/dd/yyyy"));
                            if (dtTempDate.ToOADate() != dtOriginalSaleDate.ToOADate())
                            {
                                strDate = "'" + Strings.Format(TRec.Get_Fields_DateTime("saledate"), "MM/dd/yyyy") + "'";
                                dtTempDate = FCConvert.ToDateTime(Strings.Format(TRec.Get_Fields_DateTime("saledate"), "MM/dd/yyyy"));
                            }
                            else
                            {
                                strDate = FCConvert.ToString(0);
                                dtTempDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                                object strInput = dtTempDate;
                                if (frmInput.InstancePtr.Init(ref strInput, "Sale Date", "Enter the date for the change of ownership", 1700, false, modGlobalConstants.InputDTypes.idtDate, FCConvert.ToString(dtTempDate)))
                                {
                                    //FC:FINAL:DDU:converted back to correct variable
                                    dtTempDate = FCConvert.ToDateTime(strInput);
                                    strDate = "'" + Strings.Format(dtTempDate, "MM/dd/yyyy") + "'";
                                    TRec.Set_Fields("saledate", dtTempDate);
                                    theAccount.SaleDate = dtTempDate;
                                }
                                else
                                {
                                    dtTempDate = DateTime.FromOADate(0);
                                }
                            }
                        }
                        else
                        {
                            strDate = FCConvert.ToString(0);
                            dtTempDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                            object strInput = dtTempDate;
                            if (frmInput.InstancePtr.Init(ref strInput, "Sale Date", "Enter the date for the change of ownership", 1700, false, modGlobalConstants.InputDTypes.idtDate, FCConvert.ToString(dtTempDate)))
                            {
                                //FC:FINAL:DDU:converted back to correct variable
                                dtTempDate = FCConvert.ToDateTime(strInput);
                                strDate = "'" + Strings.Format(dtTempDate, "MM/dd/yyyy") + "'";
                                TRec.Set_Fields("saledate", dtTempDate);
                                theAccount.SaleDate = dtTempDate;
                            }
                            else
                            {
                                dtTempDate = DateTime.FromOADate(0);
                            }
                        }
                    }
                    else
                    {
                        strDate = FCConvert.ToString(0);
                        dtTempDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                        object strInput = dtTempDate;
                        if (frmInput.InstancePtr.Init(ref strInput, "Transfer Date", "Enter the date for the change of ownership", 1700, false, modGlobalConstants.InputDTypes.idtDate, FCConvert.ToString(dtTempDate)))
                        {
                            // TRec.Fields("saledate") = dtTempDate
                        }
                        else
                        {
                            dtTempDate = DateTime.FromOADate(0);
                        }
                    }
                    if (dtTempDate.ToOADate() != 0)
                    {
                        strDate = Strings.Format(dtTempDate, "MM/dd/yyyy");
                        rsOwners.OpenRecordset("select * from previousowner where account = 0", modGlobalVariables.strREDatabase);
                        int lngAutoID = 0;
                        rsOwners.AddNew();
                        rsOwners.Set_Fields("account", modGlobalVariables.Statics.gintLastAccountNumber);
                        rsOwners.Set_Fields("Name", strPODeedName1);
                        rsOwners.Set_Fields("SecOwner", strPODeedName2);
                        rsOwners.Set_Fields("address1", strPOAddr1);
                        rsOwners.Set_Fields("address2", strPOAddr2);
                        rsOwners.Set_Fields("city", strPOCity);
                        rsOwners.Set_Fields("state", strPOState);
                        rsOwners.Set_Fields("zip", strPOZip);
                        rsOwners.Set_Fields("zip4", "");
                        if (Information.IsDate(strDate))
                        {
                            rsOwners.Set_Fields("saledate", strDate);
                        }
                        else
                        {
                            rsOwners.Set_Fields("saledate", 0);
                        }
                        rsOwners.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                        rsOwners.Update();
                        lngAutoID = FCConvert.ToInt32(rsOwners.Get_Fields_Int32("id"));
                        rsOwners.Execute("update owners set associd = " + FCConvert.ToString(lngAutoID) + " where associd = 0 and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
                    }
                }

                lngPOOwnerID = theAccount.OwnerID;
                lngPOSecOwnerID = theAccount.SecOwnerID;
                strPODeedName1 = theAccount.DeedName1;
                strPODeedName2 = theAccount.DeedName2;
                cPartyAddress tAddr;
                if (!(theAccount.OwnerParty == null))
                {
                    tAddr = theAccount.OwnerParty.GetPrimaryAddress();
                    if (!(tAddr == null))
                    {
                        strPOAddr1 = tAddr.Address1;
                        strPOAddr2 = tAddr.Address2;
                        strPOCity = tAddr.City;
                        strPOState = tAddr.State;
                        strPOZip = tAddr.Zip;
                    }
                }
                else
                {
                    strPOAddr1 = "";
                    strPOAddr2 = "";
                    strPOCity = "";
                    strPOState = "";
                    strPOZip = "";
                }
                if (Information.IsDate(TRec.Get_Fields("saledate") + ""))
                {
                    dtTempDate = FCConvert.ToDateTime(Strings.Format(TRec.Get_Fields_DateTime("saledate"), "MM/dd/yyyy"));
                    if (dtTempDate.ToOADate() != 0)
                    {
                        dtOriginalSaleDate = FCConvert.ToDateTime(Strings.Format(TRec.Get_Fields_DateTime("saledate"), "MM/dd/yyyy"));
                    }
                    else
                    {
                        dtOriginalSaleDate = DateTime.FromOADate(0);
                    }
                }
                else
                {
                    if (dtTempDate.ToOADate() != 0)
                    {
                        dtOriginalSaleDate = dtTempDate;
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            TRec.Set_Fields("saledate", dtTempDate);
                            theAccount.SaleDate = dtTempDate;
                            SaleGrid.TextMatrix(2, CNSTSALECOLDATE, Strings.Format(dtTempDate, "MM/dd/yyyy"));
                        }
                    }
                    else
                    {
                        dtOriginalSaleDate = DateTime.FromOADate(0);
                    }
                }
                // "ReDwelling"
                modDataTypes.Statics.DWL.Set_Fields("rsaccount", theAccount.Account);
                modDataTypes.Statics.DWL.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.CMR.Set_Fields("cmecon", FCConvert.ToString(Conversion.Val(Strings.Trim(mebCMEcon.Text + ""))));
                modDataTypes.Statics.CMR.Set_Fields("rsaccount", theAccount.Account);
                modDataTypes.Statics.CMR.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.OUT.Set_Fields("rsaccount", theAccount.Account);
                modDataTypes.Statics.OUT.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                UpdateOutbuilding();
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype1")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype2")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype3")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype4")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype5")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype6")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype7")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype8")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype9")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype10")) != 0)
                    TRec.Set_Fields("rsoutbuildingcode", "Y");
                FillRecordsetFromScreen = true;
                return FillRecordsetFromScreen;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillRecordsetFromScreen", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return FillRecordsetFromScreen;
        }

        private void UpdateOutbuilding()
        {
            int x;
            for (x = 0; x <= 9; x++)
            {
                //FC:FINAL:MSH - incorrect order of operations. In VB6 at first will be executed '+' and after will be executed '&'
                modDataTypes.Statics.OUT.Set_Fields("OItype" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIType1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oiunits" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIUnits1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oiyear" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIYear1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oicond" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROICond1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oigradecd" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIGradeCd1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oigradepct" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIGradePct1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oipctfunct" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIPctFunct1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oipctphys" + (x + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIPctPhys1[FCConvert.ToInt16(x)].Text + ""))));
                modDataTypes.Statics.OUT.Set_Fields("oisoundvalue" + (x + 1), FCConvert.ToString(Conversion.Val(txtMROISoundValue[FCConvert.ToInt16(x)].Text + "")));
                if (chkSound[FCConvert.ToInt16(x)].CheckState == Wisej.Web.CheckState.Checked)
                {
                    modDataTypes.Statics.OUT.Set_Fields("oiusesound" + (x + 1), true);
                }
                else
                {
                    modDataTypes.Statics.OUT.Set_Fields("oiusesound" + (x + 1), false);
                }
            }
            // x
        }

        private bool SaveCard()
        {
            bool SaveCard = false;
            try
            {
                // On Error GoTo ErrorHandler
                bool boolReturn;
                boolReturn = false;
                modDataTypes.Statics.MR.Set_Fields("rsref1", txtMRRSRef1.Text);
                modDataTypes.Statics.MR.Set_Fields("rsref2", txtMRRSRef2.Text);
                clsDRWrapper temp = modDataTypes.Statics.MR;
                modREMain.SaveMasterTable(ref temp, theAccount.Account, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.MR = temp;
                temp = modDataTypes.Statics.OUT;
                modREMain.SaveOutBuildingTable(ref temp, theAccount.Account, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.OUT = temp;
                if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C")
                {
                    temp = modDataTypes.Statics.CMR;
                    modREMain.SaveCommercialTable(ref temp, theAccount.Account, modGNBas.Statics.gintCardNumber);
                    modDataTypes.Statics.CMR = temp;
                }
                if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "Y")
                {
                    temp = modDataTypes.Statics.DWL;
                    modREMain.SaveDwellingTable(ref temp, theAccount.Account, modGNBas.Statics.gintCardNumber);
                    modDataTypes.Statics.DWL = temp;
                }
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    if (pictureViewModel != null)
                    {
                        if (pictureViewModel.Pictures.HasCurrentPicture())
                        {
                            pictureViewModel.Pictures.CurrentPicture().Description = txtPicDesc.Text.Trim();
                            pictureViewModel.SaveCurrentPicture();                            
                        }
                    }
                }
                cREAccountController tCont = new cREAccountController();
                boolReturn = FCConvert.ToBoolean(tCont.SaveAccount(theAccount));
                if (boolReturn)
                {
                    modGlobalVariables.Statics.DataChanged = false;
                }
                SaveCard = boolReturn;
                return SaveCard;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveCard", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return SaveCard;
        }

        public void CheckPending(ref DateTime dtPendingDate)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper clsOrig = new clsDRWrapper();
                bool boolfirstChange;
                int lngTransactionID;
                int x;
                // checks all of the tables
                boolfirstChange = true;
                lngTransactionID = 0;
                if (lngCurrentPendingID > 0)
                {
                    clsOrig.Execute("delete * from pendingchanges where transactionnumber = " + FCConvert.ToString(lngCurrentPendingID), modGlobalVariables.strREDatabase, false);
                    boolfirstChange = false;
                }
                lngTransactionID = lngCurrentPendingID;
                clsOrig.OpenRecordset("select * from master where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                if (!clsOrig.EndOfFile())
                {
                    clsDRWrapper temp = modDataTypes.Statics.MR;
                    for (x = 0; x <= modDataTypes.Statics.MR.FieldsCount - 1; x++)
                    {
                        if (Strings.LCase(modDataTypes.Statics.MR.Get_FieldsIndexName(x)) != "id")
                        {
                            modGlobalRoutines.CompareFieldValues(ref temp, ref clsOrig, temp.Get_FieldsIndexName(x), "Master", dtPendingDate, lngTransactionID, boolfirstChange);
                        }
                    }
                    modDataTypes.Statics.MR = temp;
                    // x
                }
                clsOrig.OpenRecordset("select * from dwelling where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                if (!clsOrig.EndOfFile())
                {
                    clsDRWrapper temp = modDataTypes.Statics.DWL;
                    for (x = 0; x <= modDataTypes.Statics.DWL.FieldsCount - 1; x++)
                    {
                        modGlobalRoutines.CompareFieldValues(ref temp, ref clsOrig, temp.Get_FieldsIndexName(x), "Dwelling", dtPendingDate, lngTransactionID, boolfirstChange);
                    }
                    modDataTypes.Statics.DWL = temp;
                    // x
                }
                clsOrig.OpenRecordset("select * from commercial where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                if (!clsOrig.EndOfFile())
                {
                    clsDRWrapper temp = modDataTypes.Statics.CMR;
                    for (x = 0; x <= modDataTypes.Statics.CMR.FieldsCount - 1; x++)
                    {
                        modGlobalRoutines.CompareFieldValues(ref temp, ref clsOrig, temp.Get_FieldsIndexName(x), "Commercial", dtPendingDate, lngTransactionID, boolfirstChange);
                    }
                    modDataTypes.Statics.CMR = temp;
                    // x
                }
                clsOrig.OpenRecordset("select * from outbuilding where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                if (!clsOrig.EndOfFile())
                {
                    clsDRWrapper temp = modDataTypes.Statics.OUT;
                    for (x = 0; x <= modDataTypes.Statics.OUT.FieldsCount - 1; x++)
                    {
                        modGlobalRoutines.CompareFieldValues(ref temp, ref clsOrig, temp.Get_FieldsIndexName(x), "Outbuilding", dtPendingDate, lngTransactionID, boolfirstChange);
                    }
                    modDataTypes.Statics.OUT = temp;
                    // x
                }
                SaveBPPending(ref lngTransactionID, ref dtPendingDate, ref boolfirstChange);
                // Call SaveGridPhonesPending(lngTransactionID, dtPendingDate, boolfirstChange)
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In check pending", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SaveBPPending(ref int lngPendID, ref DateTime dtPendingDate, ref bool boolfirstChange)
        {
            clsDRWrapper clsSave = new clsDRWrapper();
            string strSQL = "";
            // vbPorter upgrade warning: strSale As string	OnWrite(string, short)
            string strSale = "";
            int x;
            string strBool = "";
            if (!boolBookPageChanged)
                return;
            clsSave.Execute("delete from pendingchanges where transactionnumber = " + FCConvert.ToString(lngPendID) + " and tablename = 'bookpage'", modGlobalVariables.strREDatabase, false);
            for (x = 1; x <= BPGrid.Rows - 1; x++)
            {
                if (x >= BPGrid.Rows)
                    break;
                if (Strings.Trim(BPGrid.TextMatrix(x, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(x, BPPageCol)) == "")
                {
                    BPGrid.RemoveItem(x);
                }
            }
            // x
            if (BPGrid.Rows > 1)
            {
                if (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) == "")
                    BPGrid.RemoveItem(BPGrid.Rows - 1);
            }

            BPGrid.AddItem(FCConvert.ToString(true));
            if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate")))
            {
                //FC:FINAL:MSH - i.issue #1026: incorrect convert from datetime to int
                //if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0)
                if (FCConvert.ToDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") as object).ToOADate() != 0)
                {
                    strSale = modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "";
                }
                else
                {
                    strSale = FCConvert.ToString(0);
                }
            }
            else
            {
                strSale = FCConvert.ToString(0);
            }
            if (boolfirstChange || lngPendID == 0)
            {
                clsSave.OpenRecordset("select * from pendingchanges where transactionnumber = -3", modGlobalVariables.strREDatabase);
                clsSave.AddNew();
                lngPendID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("id"));
                boolfirstChange = false;
                clsSave.Update();
            }
            for (x = 1; x <= BPGrid.Rows - 2; x++)
            {
                strSQL = "Insert into pendingchanges (TransactionNumber,Account,Card,CardId,TableName,mindate,FieldName,FieldValue,FieldType)";
                strSQL += " values (" + FCConvert.ToString(lngPendID) + "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "," + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "," + modDataTypes.Statics.MR.Get_Fields_Int32("id") + ",'BookPage',#" + FCConvert.ToString(dtPendingDate) + "#";
                clsSave.Execute(strSQL + ",'Book','" + BPGrid.TextMatrix(x, BPBookCol) + "',10)", modGlobalVariables.strREDatabase, false);
                clsSave.Execute(strSQL + ",'Page','" + BPGrid.TextMatrix(x, BPPageCol) + "',10)", modGlobalVariables.strREDatabase, false);
                clsSave.Execute(strSQL + ",'BPDate','" + Strings.Trim(BPGrid.TextMatrix(x, BPSaleDateCol)) + "',10)", modGlobalVariables.strREDatabase, false);
                clsSave.Execute(strSQL + ",'SaleDate','" + strSale + "',8)", modGlobalVariables.strREDatabase, false);
                if (BPGrid.TextMatrix(x, BPCheckCol) == string.Empty)
                {
                    //strBool = "FALSE";
                    strBool = "0";
                }
                else
                {
                    if (FCConvert.CBool(BPGrid.TextMatrix(x, BPCheckCol)) == true)
                    {
                        //strBool = "TRUE";
                        strBool = "1";
                    }
                    else
                    {
                        //strBool = "FALSE";
                        strBool = "0";
                    }
                }
                clsSave.Execute(strSQL + ",'Current','" + strBool + "',1)", modGlobalVariables.strREDatabase, false);
            }
            // x
        }

        private void gridLandCode_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
        }

        private void gridLandCode_Enter(object sender, System.EventArgs e)
        {
            SSTab1.SelectedIndex = 1;
        }

        private void GridNeighborhood_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
            boolCalcDataChanged = true;
        }

        private void GridNeighborhood_KeyDownEvent(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            string strTemp = "";
                            strTemp = "The Neighborhood Code and Zoning Use Code are used together to direct the system to the correct Land Schedule. In order for this to work properly the Zone/Neigborhood Table must be setup for all combinations of Neighborhood Codes and Zoning Use Codes that are used.  This will ensure that the system is directed to the correct Land Schedule as well as the correct Land Factor and Building Factor.  Both factors default to 100 (percent) but can be changed if needed.  If a value other than 100 is entered for the Land Factor the values in the designated Land Schedule are multiplied by that factor.  If a Building Factor other than 100 the system will apply that factor to all buildings and outbuildings on that property as an economic factor.  Neighborhood codes can be edited by going to 3. Cost File Update > 1. Update > 1. Property > 1. Neighborhood.";
                            GridToolTip.Visible = false;
                            ToolTip.Update("Neighborhood", GridNeighborhood.Left, GridNeighborhood.Top, strTemp, Label8[10].Left + Frame5.Left + SSTab1.Left, Label8[10].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridNeighborhood_KeyDownEdit(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            string strTemp = "";
                            strTemp = "The Neighborhood Code and Zoning Use Code are used together to direct the system to the correct Land Schedule. In order for this to work properly the Zone/Neigborhood Table must be setup for all combinations of Neighborhood Codes and Zoning Use Codes that are used.  This will ensure that the system is directed to the correct Land Schedule as well as the correct Land Factor and Building Factor.  Both factors default to 100 (percent) but can be changed if needed.  If a value other than 100 is entered for the Land Factor the values in the designated Land Schedule are multiplied by that factor.  If a Building Factor other than 100 the system will apply that factor to all buildings and outbuildings on that property as an economic factor.  Neighborhood codes can be edited by going to 3. Cost File Update > 1. Update > 1. Property > 1. Neighborhood.";
                            GridToolTip.Visible = false;
                            ToolTip.Update("Neighborhood", GridNeighborhood.Left, GridNeighborhood.Top, strTemp, Label8[10].Left + Frame5.Left + SSTab1.Left, Label8[10].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void gridBldgCode_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
        }

        private void gridPropertyCode_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
        }

        private void gridPropertyCode_ComboCloseUp(object sender, EventArgs e)
        {
            gridPropertyCode.Row = -1;
            //Application.DoEvents();
        }

        private void gridPropertyCode_ComboDropDown(object sender, System.EventArgs e)
        {
            int x;
            int lngID;
            // ToolTip.HideTip
            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridPropertyCode.TextMatrix(0, 0))));
            if (lngID > 0)
            {
                for (x = 0; x <= gridPropertyCode.ComboCount - 1; x++)
                {
                    if (Conversion.Val(gridPropertyCode.ComboData(x)) == lngID)
                    {
                        gridPropertyCode.ComboIndex = x;
                    }
                }
                // x
            }
        }

        private void gridPropertyCode_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (e.KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "State Property Type Code";
                            GridToolTip.Visible = false;
                            ToolTip.Update("PropertyCode", gridPropertyCode.Left, gridPropertyCode.Top, strTemp, -1, gridPropertyCode.Top + gridPropertyCode.Height + Frame5.Top + SSTab1.Top, gridPropertyCode.Left + SSTab1.Left + Frame5.Left);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridNeighborhood_ComboDropDown(object sender, System.EventArgs e)
        {
            int x;
            int lngID;
            // ToolTip.HideTip
            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeighborhood.TextMatrix(0, 0))));
            if (lngID > 0)
            {
                for (x = 0; x <= GridNeighborhood.ComboCount - 1; x++)
                {
                    if (Conversion.Val(GridNeighborhood.ComboData(x)) == lngID)
                    {
                        GridNeighborhood.ComboIndex = x;
                    }
                }
                // x
            }
        }

        private void txtMRPIStreetCode_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRPIStreetCode.SelectionStart = 0;
            txtMRPIStreetCode.SelectionLength = txtMRPIStreetCode.Text.Length;
        }

        private void txtMRPIStreetCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            // If KeyCode = vbKeyUp Then SendKeys ("+{TAB}")
            // If KeyCode = vbKeyDown Then SendKeys ("{TAB}")
            if (KeyCode == Keys.F9)
            {
                string strTemp = "";
                strTemp = "Used for storing the year that the property was first classified under the Maine tree growth tax law";
                ToolTip.Update("TreeGrowthYear", txtMRPIStreetCode.Left, txtMRPIStreetCode.Top, strTemp, Label8[11].Left + Frame5.Left + SSTab1.Left, Label8[11].Top + Frame5.Top + SSTab1.Top);
            }
            KeyCode = (Keys)0;
        }

        private void txtMRPIXCoord_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRPIXCoord.SelectionStart = 0;
            txtMRPIXCoord.SelectionLength = txtMRPIXCoord.Text.Length;
        }

        private void txtMRPIXCoord_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9)
            {
                string strTemp = "";
                strTemp = "This field, while originally intended to be used for an X coordinate, is user definable.  To change this field's title go to 3. Cost File Update > 1. Update > 1. Property > 4. Other and edit the Long Description for record 1091.";
                GridToolTip.Visible = false;
                ToolTip.Update("XCoord", txtMRPIXCoord.Left, txtMRPIXCoord.Top, strTemp, Label8[12].Left + SSTab1.Left + Frame5.Left, Label8[12].Top + SSTab1.Top + Frame5.Top);
            }
            KeyCode = (Keys)0;
        }

        private void txtMRPIYCoord_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRPIYCoord.SelectionStart = 0;
            txtMRPIYCoord.SelectionLength = txtMRPIYCoord.Text.Length;
        }

        private void txtMRPIYCoord_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F9)
            {
                GridToolTip.Visible = false;
                ToolTip.Update("YCoord", txtMRPIYCoord.Left, txtMRPIYCoord.Top, "This field, while originally intended to be used for a Y coordinate, is user definable.  To change this field's title go to 3. Cost File Update > 1. Update > 1. Property > 4. Other and edit the Long Description for record 1092.", Label8[13].Left + Frame5.Left + SSTab1.Left, Label8[13].Top + SSTab1.Top + Frame5.Top);
            }
            KeyCode = (Keys)0;
        }

        private void GridZone_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
            boolCalcDataChanged = true;
        }

        private void GridZone_ComboDropDown(object sender, System.EventArgs e)
        {
            int x;
            int lngID;
            // ToolTip.HideTip
            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridZone.TextMatrix(0, 0))));
            if (lngID > 0)
            {
                for (x = 0; x <= GridZone.ComboCount - 1; x++)
                {
                    if (Conversion.Val(GridZone.ComboData(x)) == lngID)
                    {
                        GridZone.ComboIndex = x;
                    }
                }
                // x
            }
        }

        private void GridZone_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "This field is used to indicate the zoning or use of the parcel.  Where zoning is in effect, the actual zone names should be entered in to the system.  Where zoning is not in effect, land uses such as residential, commercial, etc. may be used.  The Zoning Use titles are printed on the valuation report.  Together with the Neighborhood Code, the Zoning Use Code is used by the system to indicate the appropriate Land Schedule for each card, based on information in the Zone/Neigborhood Table.  The Zone Table also holds a Land Factor, by which land values are multiplied and a Building Factor, which is applied to all buildings as an economic factor.  Zones can be edited by going to 3, Cost File Update > 1. Update > 1. Property > 2. Zone.";
                            ToolTip.Update("Zone", GridZone.Left, GridZone.Top, strTemp, Label8[14].Left + Frame5.Left + SSTab1.Left, Label8[14].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridZone_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "This field is used to indicate the zoning or use of the parcel.  Where zoning is in effect, the actual zone names should be entered in to the system.  Where zoning is not in effect, land uses such as residential, commercial, etc. may be used.  The Zoning Use titles are printed on the valuation report.  Together with the Neighborhood Code, the Zoning Use Code is used by the system to indicate the appropriate Land Schedule for each card, based on information in the Zone/Neigborhood Table.  The Zone Table also holds a Land Factor, by which land values are multiplied and a Building Factor, which is applied to all buildings as an economic factor.  Zones can be edited by going to 3, Cost File Update > 1. Update > 1. Property > 2. Zone.";
                            ToolTip.Update("Zone", GridZone.Left, GridZone.Top, strTemp, Label8[14].Left + Frame5.Left + SSTab1.Left, Label8[14].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridSecZone_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
            boolCalcDataChanged = true;
        }

        private void GridSecZone_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "The Secondary Zone has two different uses. If something is entered into this field, the valuation report prints the Secondary Zone in addition to the primary Zoning Use code. The land valuation is based on the Zoning Use code only. If the property is zoned for one use, but you wish to value based on a different use check the " + FCConvert.ToString(Convert.ToChar(34)) + "Price as secondary zone" + FCConvert.ToString(Convert.ToChar(34)) + " option. For example a convenience store in a residential neighborhood may be coded residential, but is grandfathered or has a variance for commercial use. Zones can be edited by going to 3, Cost File Update > 1. Update > 1. Property > 2. Zone.";
                            GridToolTip.Visible = false;
                            ToolTip.Update("SecondaryZone", GridSecZone.Left, GridSecZone.Top, strTemp, Label8[15].Left + Frame5.Left + SSTab1.Left, Label8[15].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridSecZone_ComboDropDown(object sender, System.EventArgs e)
        {
            int x;
            int lngID;
            // ToolTip.HideTip
            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridSecZone.TextMatrix(0, 0))));
            if (lngID > 0)
            {
                for (x = 0; x <= GridSecZone.ComboCount - 1; x++)
                {
                    if (Conversion.Val(GridSecZone.ComboData(x)) == lngID)
                    {
                        GridSecZone.ComboIndex = x;
                    }
                }
                // x
            }
        }

        private void GridTopography_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
            boolCalcDataChanged = true;
        }

        private void GridTopography_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
        {
            if (e.NewRow == GridTopography.Rows - 1)
            {
                GridTopography.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridTopography.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridTopography_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "The Topography fields are used to enter comments about the topography of the parcel. The descriptions for these codes can be edited in 3. Cost File Update > 1. Update > 1. Property > 4. Other in records 1301 through 1309.";
                            GridToolTip.Visible = false;
                            ToolTip.Update("Topography", GridTopography.Left, GridTopography.Top, strTemp, Label8[16].Left + Frame5.Left + SSTab1.Left, Label8[16].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridTopography_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            GridToolTip.Visible = false;
                            strTemp = "The Topography fields are used to enter comments about the topography of the parcel. The descriptions for these codes can be edited in 3. Cost File Update > 1. Update > 1. Property > 4. Other in records 1301 through 1309.";
                            ToolTip.Update("Topography", GridTopography.Left, GridTopography.Top, strTemp, Label8[16].Left + Frame5.Left + SSTab1.Left, Label8[16].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridTopography_RowColChange(object sender, System.EventArgs e)
        {
            if (GridTopography.Row == GridTopography.Rows - 1)
            {
                GridTopography.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridTopography.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridUtilities_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
            boolCalcDataChanged = true;
        }

        private void GridUtilities_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
        {
            if (e.NewRow == GridUtilities.Rows - 1)
            {
                GridUtilities.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridUtilities.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridUtilities_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "The Utility field is used to enter comments about the utilities (typically water and sewer) servicing the parcel. The descriptions for these codes can be edited in 3. Cost File Update > 1. Update > 1. Property > 4. Other in records 1311 through 1319.";
                            ToolTip.Update("Utilities", GridUtilities.Left, GridUtilities.Top, strTemp, Label8[17].Left + Frame5.Left + SSTab1.Left, Label8[17].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridUtilities_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "The Utility field is used to enter comments about the utilities (typically water and sewer) servicing the parcel. The descriptions for these codes can be edited in 3. Cost File Update > 1. Update > 1. Property > 4. Other in records 1311 through 1319.";
                            ToolTip.Update("Utilities", GridUtilities.Left, GridUtilities.Top, strTemp, Label8[17].Left + Frame5.Left + SSTab1.Left, Label8[17].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridUtilities_RowColChange(object sender, System.EventArgs e)
        {
            if (GridUtilities.Row == GridUtilities.Rows - 1)
            {
                GridUtilities.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridUtilities.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridStreet_ClickEvent(object sender, System.EventArgs e)
        {
            modGlobalVariables.Statics.DataChanged = true;
        }

        private void GridStreet_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "Used to describe the type of street or road allowing access to the parcel";
                            GridToolTip.Visible = false;
                            ToolTip.Update("StreetCode", GridStreet.Left, GridStreet.Top, strTemp, Label8[18].Left + Frame5.Left + SSTab1.Left, Label8[18].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridStreet_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            KeyCode = 0;
                            string strTemp = "";
                            strTemp = "Used to describe the type of street or road allowing access to the parcel";
                            GridToolTip.Visible = false;
                            ToolTip.Update("StreetCode", GridStreet.Left, GridStreet.Top, strTemp, Label8[18].Left + Frame5.Left + SSTab1.Left, Label8[18].Top + Frame5.Top + SSTab1.Top);
                        }
                        break;
                    }
            }
            //end switch
        }

        private void txtMRPIOpen1_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRPIOpen1.SelectionStart = 0;
            txtMRPIOpen1.SelectionLength = txtMRPIOpen1.Text.Length;
        }

        private void txtMRPIOpen1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            // If KeyCode = vbKeyUp Then SendKeys ("+{TAB}")
            // If KeyCode = vbKeyDown Then SendKeys ("{TAB}")
            if (KeyCode == Keys.F9)
            {
                KeyCode = (Keys)0;
                string strTemp = "";
                bool boolSetup = false;
                boolSetup = false;
                if (ToolTip.CurrentTip != "Open1")
                {
                    boolSetup = true;
                }
                strTemp = "This field, which is referred to as Open 1, is a four digit user defined field and can be used to describe additional features or information about the parcel. If desired this field may be setup to accept a one digit code (1-9), each of which would describe a specific feature of the parcel, or to accept up to a four digit description.";
                strTemp += " If codes are used, they may be setup to apply economic factors to all buildings on the parcel. For example, if this field is setup as " + FCConvert.ToString(Convert.ToChar(34)) + "Classification" + FCConvert.ToString(Convert.ToChar(34)) + " so that 1=Residential 2=Commercial etc. an economic factor of 0.85 may be applied to all commercial property, which would reduce the building valuation by a factor of 0.85 for all of the buildings on parcels where  " + FCConvert.ToString(Convert.ToChar(34)) + "0002" + FCConvert.ToString(Convert.ToChar(34)) + " is entered into this field. The factor (in this example 0.85) is entered in the Unit column and is applied to the valuation as an economic factor.  The description of this field is edited in cost record 1330 and the codes are setup in cost records 1331 through 1339.  These records are accessed by going to 3.";
                strTemp += "Cost File Update > 1. Update > 1. Property > 4. Other.";
                ToolTip.Update("Open1", txtMRPIOpen1.Left, txtMRPIOpen1.Top, strTemp, Label8[19].Left + SSTab1.Left + Frame5.Left + 30, -1, -1, Label8[19].Top + SSTab1.Top + Frame5.Top + Label8[19].Height + Label8[19].Height, 1000, true);
                if (boolSetup)
                {
                    SetupGridToolTip_2("Open1");
                }
            }
        }

        public void txtMRPIOpen1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // MR.Fields("piopen1") = Val(txtMRPIOpen1.Text & "")
            txtMRPIOpen1.Text = FCConvert.ToString(Conversion.Val(txtMRPIOpen1.Text + ""));
        }

        private void txtMRPIOpen2_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMRPIOpen2.SelectionStart = 0;
            txtMRPIOpen2.SelectionLength = txtMRPIOpen2.Text.Length;
        }

        private void txtMRPIOpen2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            // If KeyCode = vbKeyUp Then SendKeys ("+{TAB}")
            // If KeyCode = vbKeyDown Then SendKeys ("{TAB}")
            if (KeyCode == Keys.F9)
            {
                KeyCode = (Keys)0;
                string strTemp = "";
                bool boolSetup = false;
                boolSetup = false;
                if (ToolTip.CurrentTip != "Open2")
                {
                    boolSetup = true;
                }
                strTemp = "This field, which is referred to as Open2, is a four digit user defined field and can be used to describe additional features or information about the parcel. If desired this field may be setup to accept a one digit code (1-9), each of which would describe a specific feature of the parcel, or to accept up to a four digit description.";
                strTemp += " If codes are used, they may be setup to apply economic factors to all buildings on the parcel. For example, if this field is setup as " + FCConvert.ToString(Convert.ToChar(34)) + "Classification" + FCConvert.ToString(Convert.ToChar(34)) + " so that 1=Residential 2=Commercial etc. an economic factor of 0.85 may be applied to all commercial property, which would reduce the building valuation by a factor of 0.85 for all of the buildings on parcels where  " + FCConvert.ToString(Convert.ToChar(34)) + "0002" + FCConvert.ToString(Convert.ToChar(34)) + " is entered into this field. The factor (in this example 0.85) is entered in the Unit column and is applied to the valuation as an economic factor.  The description of this field is edited in cost record 1340 and the codes are setup in cost records 1341 through 1349.  These records are accessed by going to 3.";
                strTemp += "Cost File Update > 1. Update > 1. Property > 4. Other.";
                ToolTip.Update("Open2", txtMRPIOpen2.Left, txtMRPIOpen2.Top, strTemp, Label8[20].Left + Frame5.Left + SSTab1.Left, -1, -1, Label8[20].Top + SSTab1.Top + Frame5.Top + Label8[20].Height + Label8[20].Height, 1000, true);
                if (boolSetup)
                {
                    SetupGridToolTip_2("Open2");
                }
            }
        }

        public void txtMRPIOpen2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // MR.Fields("piopen2") = Val(txtMRPIOpen2.Text)
            txtMRPIOpen2.Text = FCConvert.ToString(Conversion.Val(txtMRPIOpen2.Text + ""));
        }

        private void GridLand_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            GridLand.EditMask = "";
            if (GridLand.Col == 0)
            {
                if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0)) == 0)
                {
                    GridLand.RemoveItem(GridLand.Row);
                    GridLand.Rows += 1;
                    GridLand.TextMatrix(GridLand.Rows - 1, 1, "");
                    GridLand.TextMatrix(GridLand.Rows - 1, 0, "0");
                    GridLand.TextMatrix(GridLand.Rows - 1, 2, "100");
                    GridLand.TextMatrix(GridLand.Rows - 1, 3, "0");
                    if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0)) > 0)
                    {
                        GridLand.Col = 1;
                    }
                    CalculateAcres();
                }
            }
            else if (GridLand.Col == 1)
            {
                if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0)) > 0 && Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0)) < 99)
                {
                    modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0))));
                    switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                    {
                        // Select Case Val(GridLand.TextMatrix(Row, 0))
                        // Case 0
                        // Case 11 To 15
                        case modREConstants.CNSTLANDTYPEFRONTFOOT:
                        case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                            {
                                // Case 16 To 20
                                break;
                            }
                        case modREConstants.CNSTLANDTYPESQUAREFEET:
                            {
                                if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 1)) == 0)
                                {
                                    GridLand.TextMatrix(GridLand.Row, 1, "0");
                                }
                                else
                                {
                                    GridLand.TextMatrix(GridLand.Row, 1, Strings.Format(GridLand.TextMatrix(GridLand.Row, GridLand.Col), "#,###,###"));
                                }
                                // Case 21 To 46
                                break;
                            }
                        case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                        case modREConstants.CNSTLANDTYPEACRES:
                        case modREConstants.CNSTLANDTYPESITE:
                        case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                        case modREConstants.CNSTLANDTYPELINEARFEET:
                        case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                            {
                                if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 1)) == 0)
                                {
                                    GridLand.TextMatrix(GridLand.Row, 1, "0.00");
                                }
                                else
                                {
                                    GridLand.TextMatrix(GridLand.Row, 1, Strings.Format(GridLand.TextMatrix(GridLand.Row, GridLand.Col), "0.00"));
                                }
                                // Case 99
                                break;
                            }
                        case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
                            {
                                break;
                            }
                    }
                    //end switch
                }
                CalculateAcres();
            }

            modGlobalVariables.Statics.DataChanged = true;
        }

        private void GridLand_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (GridLand.Col == 1)
            {
                if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0)) == 0)
                {
                    e.Cancel = true;
                }
                else if (Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0)) == 99)
                {
                    e.Cancel = true;
                }
                else
                {
                    modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0))));
                    switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                    {
                        case modREConstants.CNSTLANDTYPEFRONTFOOT:
                        case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                            {
                                GridLand.EditMask = "000x000";
                                break;
                            }
                        case modREConstants.CNSTLANDTYPESQUAREFEET:
                            {
                                GridLand.EditMask = "";
                                break;
                            }
                        case modREConstants.CNSTLANDTYPEACRES:
                        case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                        case modREConstants.CNSTLANDTYPESITE:
                        case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                        case modREConstants.CNSTLANDTYPELINEARFEET:
                        case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                            {
                                GridLand.EditMask = "";
                                break;
                            }
                        default:
                            {
                                GridLand.EditMask = "";
                                break;
                            }
                    }
                    //end switch
                }
            }
            else
            {
                GridLand.EditMask = "";
                if (GridLand.Col == 0)
                {
                    FillLandCombo();
                }
            }
        }

        private void GridLand_ComboDropDown(object sender, System.EventArgs e)
        {
            // ToolTip.HideTip
            int x;
            int lngID = 0;
            int intCol;
            intCol = GridLand.Col;
            if (intCol == 0)
            {
                lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridLand.TextMatrix(GridLand.Row, 0))));
                if (lngID > 0)
                {
                    for (x = 0; x <= GridLand.ComboCount - 1; x++)
                    {
                        if (Conversion.Val(GridLand.ComboData(x)) == lngID)
                        {
                            GridLand.ComboIndex = x;
                        }
                    }
                    // x
                }
            }
        }

        private void GridLand_KeyDownEvent(object sender, KeyEventArgs e)
        {
            int intPreviousType;
            int Row;
            string strTemp = "";
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            GridToolTip.Visible = false;
                            KeyCode = 0;
                            int lngCol;
                            lngCol = GridLand.Col;
                            // Dim strTemp As String
                            switch (lngCol)
                            {
                                case 0:
                                    {
                                        // land type
                                        strTemp = "The Land Code indicates the type of land to be valued. Front foot types use the front foot and depth method, where land is valued at a price per front foot adjusted by a depth factor. A standard depth is entered in each Land Schedule.";
                                        strTemp += " Front foot width types add standard width as well as standard depth.";
                                        strTemp += " Square footage types value land by the square foot. Fractional acreage types use the fractional acreage formula, which may be based on the square root (or other exponential factor) of the acreage. In this case, a standard lot size may also be entered on each pricing schedule. Acreage types use price per acre (or other unit) to value land. Other units include price per site, price per lot improvement, price per linear foot etc. In addition, if an Extra Influence type is selected in this field, the system applies the factor entered in this line to the land valuation calculated in the line above. In this way, a portion of the land may be adjusted by two (or more) factors.  Land Types can be modified by going to 3. Cost File Update > 1. Update > 2. Land Types";
                                        ToolTip.Update("LandType", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top + 30);
                                        break;
                                    }
                                case 1:
                                    {
                                        // units
                                        strTemp = "Enter the number of units in these fields. Note that if the Land Code is of type front foot, the field formats itself as front X depth. If the land code is of type square feet, the field formats as 000,000. If the code is of type fractional acres, acres, improvements, linear feet or site the field formats as 0.00";
                                        ToolTip.Update("LandUnits", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top);
                                        break;
                                    }
                                case 2:
                                    {
                                        // influence
                                        strTemp = "The influence factor is a percentage by which each portion of the land is adjusted. The field defaults to 100 but may be changed. It should be noted that more than one factor may be applied to a land entry by putting the additional factor on the line below with a Land Type of " + FCConvert.ToString(Convert.ToChar(34)) + "Extra Influence" + FCConvert.ToString(Convert.ToChar(34));
                                        ToolTip.Update("LandInfluenceFactor", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top);
                                        break;
                                    }
                                case 3:
                                    {
                                        // influence code
                                        strTemp = "The Influence Code records the reason for an influence factor of other than 100";
                                        ToolTip.Update("LandInfluenceCode", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top);
                                        break;
                                    }
                                default:
                                    {
                                        ToolTip.HideTip();
                                        break;
                                    }
                            }
                            //end switch
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridLand_KeyDownEdit(object sender, KeyEventArgs e)
        {
            int intPreviousType;
            int intRow;
            string strTemp = "";
            int x;
            int y;
            Keys KeyCode = e.KeyCode;
            // ToolTip.HideTip
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:AM:#1873 - remove Shift
                        //if (e.Shift)
                        {
                            GridToolTip.Visible = false;
                            KeyCode = 0;
                            int lngCol;
                            lngCol = GridLand.Col;
                            // Dim strTemp As String
                            switch (lngCol)
                            {
                                case 0:
                                    {
                                        // land type
                                        strTemp = "The Land Code indicates the type of land to be valued. Front foot types use the front foot and depth method, where land is valued at a price per front foot adjusted by a depth factor. A standard depth is entered in each Land Schedule.";
                                        strTemp += " Front foot width types add standard width as well as standard depth.";
                                        strTemp += " Square footage types value land by the square foot. Fractional acreage types use the fractional acreage formula, which may be based on the square root (or other exponential factor) of the acreage. In this case, a standard lot size may also be entered on each pricing schedule. Acreage types use price per acre (or other unit) to value land. Other units include price per site, price per lot improvement, price per linear foot etc. In addition, if an Extra Influence type is selected in this field, the system applies the factor entered in this line to the land valuation calculated in the line above. In this way, a portion of the land may be adjusted by two (or more) factors.  Land Types can be modified by going to 3. Cost File Update > 1. Update > 2. Land Types";
                                        ToolTip.Update("LandType", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top + 30);
                                        break;
                                    }
                                case 1:
                                    {
                                        // units
                                        strTemp = "Enter the number of units in these fields. Note that if the Land Code is of type front foot, the field formats itself as front X depth. If the land code is of type square feet, the field formats as 000,000. If the code is of type fractional acres, acres, improvements, linear feet or site the field formats as 0.00";
                                        ToolTip.Update("LandUnits", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top);
                                        break;
                                    }
                                case 2:
                                    {
                                        // influence
                                        strTemp = "The influence factor is a percentage by which each portion of the land is adjusted. The field defaults to 100 but may be changed. It should be noted that more than one factor may be applied to a land entry by putting the additional factor on the line below with a Land Type of " + FCConvert.ToString(Convert.ToChar(34)) + "Extra Influence" + FCConvert.ToString(Convert.ToChar(34));
                                        ToolTip.Update("LandInfluenceFactor", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top);
                                        break;
                                    }
                                case 3:
                                    {
                                        // influence code
                                        strTemp = "The Influence Code records the reason for an influence factor of other than 100";
                                        ToolTip.Update("LandInfluenceCode", GridLand.Left, GridLand.Top, strTemp, -1, -1, Frame5.Left + SSTab1.Left, GridLand.Top + SSTab1.Top + Frame5.Top);
                                        break;
                                    }
                                default:
                                    {
                                        ToolTip.HideTip();
                                        break;
                                    }
                            }
                            //end switch
                        }
                        break;
                    }
                case Keys.Return:
                    {
                        KeyCode = 0;
                        if (GridLand.Col < 3)
                        {
                            // If Col = 0 And Val(GridLand.TextMatrix(Row, Col)) > 0 Then
                            if (GridLand.Col == 0 && Conversion.Val(GridLand.EditText) > 0)
                            {
                                GridLand.Col = 1;
                            }
                            else if (GridLand.Col == 0 && Conversion.Val(GridLand.EditText) == 0)
                            {
                                // Call GridLand_ValidateEdit(Row, Col, False)
                                GridLand.Col = 1;
                            }
                            else if (GridLand.Col == 2 || GridLand.Col == 1)
                            {
                                GridLand.Col = GridLand.Col + 1;
                            }
                        }
                        else
                        {
                            if (GridLand.Row < 7)
                            {
                                GridLand.Row = GridLand.Row + 1;
                                GridLand.Col = 0;
                            }
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridLand_RowColChange(object sender, System.EventArgs e)
        {
            if (GridLand.Row == GridLand.Rows - 1 && GridLand.Col == GridLand.Cols - 1)
            {
                GridLand.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridLand.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridLand_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int x;
            string strTemp = "";
            int intPType;
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridLand.GetFlexRowIndex(e.RowIndex);
            int col = GridLand.GetFlexColIndex(e.ColumnIndex);
            switch (col)
            {
                case 0:
                    {
                        if (Conversion.Val(GridLand.EditText) == 0)
                        {
                        }
                        else if (Conversion.Val(GridLand.EditText) == 99)
                        {
                            GridLand.TextMatrix(row, 1, "");
                        }
                        else
                        {
                            if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(GridLand.EditText))))
                            {
                                intPType = modGlobalVariables.Statics.LandTypes.UnitsType;
                                if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(GridLand.TextMatrix(row, 0)))))
                                {
                                    switch (intPType)
                                    {
                                        // Case 11 To 15
                                        case modREConstants.CNSTLANDTYPEFRONTFOOT:
                                        case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                                            {
                                                // If Val(GridLand.TextMatrix(Row, 0)) < 11 Or Val(GridLand.TextMatrix(Row, 0)) > 15 Then
                                                if (modGlobalVariables.Statics.LandTypes.UnitsType != modREConstants.CNSTLANDTYPEFRONTFOOT && modGlobalVariables.Statics.LandTypes.UnitsType != modREConstants.CNSTLANDTYPEFRONTFOOTSCALED)
                                                {
                                                    // changed types
                                                    // Select Case Val(GridLand.TextMatrix(Row, 0))
                                                    switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                                                    {
                                                        // Case 16 To 20
                                                        case modREConstants.CNSTLANDTYPESQUAREFEET:
                                                            {
                                                                strTemp = GridLand.TextMatrix(row, 1);
                                                                strTemp = Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare);
                                                                strTemp = Strings.Format(strTemp, "000000");
                                                                if (Conversion.Val(strTemp) > 0)
                                                                {
                                                                    strTemp = Strings.Mid(strTemp, 1, 3) + "x" + Strings.Mid(strTemp, 4);
                                                                }
                                                                else
                                                                {
                                                                    strTemp = "";
                                                                }
                                                                GridLand.TextMatrix(row, 1, strTemp);
                                                                // Case 21 To 46
                                                                break;
                                                            }
                                                        case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                                                        case modREConstants.CNSTLANDTYPEACRES:
                                                        case modREConstants.CNSTLANDTYPESITE:
                                                        case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                                                        case modREConstants.CNSTLANDTYPELINEARFEET:
                                                        case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                                            {
                                                                strTemp = GridLand.TextMatrix(row, 1);
                                                                strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
                                                                strTemp = Strings.Format(strTemp, "000000");
                                                                if (Conversion.Val(strTemp) > 0)
                                                                {
                                                                    strTemp = Strings.Mid(strTemp, 1, 3) + "x" + Strings.Mid(strTemp, 4);
                                                                }
                                                                else
                                                                {
                                                                    strTemp = "";
                                                                }
                                                                GridLand.TextMatrix(row, 1, strTemp);
                                                                break;
                                                            }
                                                        default:
                                                            {
                                                                GridLand.TextMatrix(row, 1, "");
                                                                break;
                                                            }
                                                    }
                                                    //end switch
                                                }
                                                // Case 16 To 20
                                                break;
                                            }
                                        case modREConstants.CNSTLANDTYPESQUAREFEET:
                                            {
                                                // If Val(GridLand.TextMatrix(Row, 0)) < 16 Or Val(GridLand.TextMatrix(Row, 0)) > 20 Then
                                                if (modGlobalVariables.Statics.LandTypes.UnitsType != modREConstants.CNSTLANDTYPESQUAREFEET)
                                                {
                                                    // changed types
                                                    // Select Case Val(GridLand.TextMatrix(Row, 0))
                                                    switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                                                    {
                                                        // Case 11 To 15
                                                        case modREConstants.CNSTLANDTYPEFRONTFOOT:
                                                        case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                                                            {
                                                                strTemp = GridLand.TextMatrix(row, 1);
                                                                strTemp = Strings.Replace(strTemp, "x", "", 1, -1, CompareConstants.vbTextCompare);
                                                                strTemp = FCConvert.ToString(Conversion.Val(strTemp));
                                                                strTemp = Strings.Format(strTemp, "#,###,##0");
                                                                GridLand.TextMatrix(row, 1, strTemp);
                                                                // Case 21 To 46
                                                                break;
                                                            }
                                                        case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                                                        case modREConstants.CNSTLANDTYPEACRES:
                                                        case modREConstants.CNSTLANDTYPESITE:
                                                        case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                                                        case modREConstants.CNSTLANDTYPELINEARFEET:
                                                        case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                                            {
                                                                strTemp = GridLand.TextMatrix(row, 1);
                                                                strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
                                                                strTemp = FCConvert.ToString(Conversion.Val(strTemp));
                                                                strTemp = Strings.Format(strTemp, "#,###,##0");
                                                                GridLand.TextMatrix(row, 1, strTemp);
                                                                break;
                                                            }
                                                        default:
                                                            {
                                                                GridLand.TextMatrix(row, 1, "");
                                                                break;
                                                            }
                                                    }
                                                    //end switch
                                                }
                                                // Case 21 To 46
                                                break;
                                            }
                                        case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                                        case modREConstants.CNSTLANDTYPEACRES:
                                        case modREConstants.CNSTLANDTYPESITE:
                                        case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                                        case modREConstants.CNSTLANDTYPELINEARFEET:
                                        case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                            {
                                                // If Val(GridLand.TextMatrix(Row, 0)) < 21 Or Val(GridLand.TextMatrix(Row, 0)) > 46 Then
                                                if (modGlobalVariables.Statics.LandTypes.UnitsType != modREConstants.CNSTLANDTYPEACRES && modGlobalVariables.Statics.LandTypes.UnitsType != modREConstants.CNSTLANDTYPEFRACTIONALACREAGE)
                                                {
                                                    // changed types
                                                    // Select Case Val(GridLand.TextMatrix(Row, 0))
                                                    switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                                                    {
                                                        // Case 11 To 15
                                                        case modREConstants.CNSTLANDTYPEFRONTFOOT:
                                                        case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                                                            {
                                                                strTemp = GridLand.TextMatrix(row, 1);
                                                                strTemp = Strings.Replace(strTemp, "x", "", 1, -1, CompareConstants.vbTextCompare);
                                                                strTemp = FCConvert.ToString(Conversion.Val(strTemp));
                                                                strTemp = "000" + strTemp;
                                                                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2) + "." + Strings.Right(strTemp, 2);
                                                                GridLand.TextMatrix(row, 1, Strings.Format(FCConvert.ToString(Conversion.Val(strTemp)), "0.00"));
                                                                // Case 16 To 20
                                                                break;
                                                            }
                                                        case modREConstants.CNSTLANDTYPESQUAREFEET:
                                                            {
                                                                strTemp = GridLand.TextMatrix(row, 1);
                                                                strTemp = Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare);
                                                                // strTemp = Val(strTemp)
                                                                strTemp = "000" + strTemp;
                                                                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2) + "." + Strings.Right(strTemp, 2);
                                                                GridLand.TextMatrix(row, 1, Strings.Format(FCConvert.ToString(Conversion.Val(strTemp)), "0.00"));
                                                                break;
                                                            }
                                                        case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                                                        case modREConstants.CNSTLANDTYPELINEARFEET:
                                                        case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                                                            {
                                                                break;
                                                            }
                                                        default:
                                                            {
                                                                GridLand.TextMatrix(row, 1, "");
                                                                break;
                                                            }
                                                    }
                                                    //end switch
                                                }
                                                break;
                                            }
                                    }
                                    //end switch
                                }
                            }
                        }
                        break;
                    }
            }
            //end switch
        }

        private void GridDwelling1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (GridDwelling1.Row == 0)
            {
                GridDwelling1.EditMaxLength = 3;
            }
            else if (GridDwelling1.Row >= 1 && GridDwelling1.Row <= 3)
            {
                GridDwelling1.EditMaxLength = 1;
            }
            else if (GridDwelling1.Row == 4)
            {
                GridDwelling1.EditMaxLength = 2;
            }
            else if (GridDwelling1.Row == 5)
            {
                GridDwelling1.EditMaxLength = 1;
            }
            else if (GridDwelling1.Row >= 6 && GridDwelling1.Row <= 10)
            {
                GridDwelling1.EditMaxLength = 4;
            }
            else if (GridDwelling1.Row >= 11 && GridDwelling1.Row <= 14)
            {
                GridDwelling1.EditMaxLength = 1;
            }
        }

        private void GridDwelling1_Enter(object sender, System.EventArgs e)
        {
            SSTab1.SelectedIndex = 2;
        }

        private void GridDwelling1_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("GRIDDWELLING1", GridDwelling1.Row);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                {
                    GridDwelling1.TextMatrix(GridDwelling1.Row, 1, Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                    switch (GridDwelling1.Row)
                    {
                        case 0:
                            {
                                // building style
                                modDataTypes.Statics.DWL.Set_Fields("distyle", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 1:
                            {
                                // dwelling units
                                modDataTypes.Statics.DWL.Set_Fields("diunitsdwelling", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 2:
                            {
                                // other units
                                modDataTypes.Statics.DWL.Set_Fields("diunitsother", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 3:
                            {
                                // stories
                                modDataTypes.Statics.DWL.Set_Fields("distories", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 4:
                            {
                                // exterior wall
                                modDataTypes.Statics.DWL.Set_Fields("diextwalls", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 5:
                            {
                                // roof surface
                                modDataTypes.Statics.DWL.Set_Fields("diroof", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 6:
                            {
                                // sf masonry trim
                                modDataTypes.Statics.DWL.Set_Fields("disfmasonry", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 7:
                            {
                                // open3
                                modDataTypes.Statics.DWL.Set_Fields("diopen3", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 8:
                            {
                                // open4
                                modDataTypes.Statics.DWL.Set_Fields("diopen4", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 9:
                            {
                                // yearbuilt
                                modDataTypes.Statics.DWL.Set_Fields("diyearbuilt", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 10:
                            {
                                // year remodelled
                                modDataTypes.Statics.DWL.Set_Fields("diyearremodel", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 11:
                            {
                                // foundation
                                modDataTypes.Statics.DWL.Set_Fields("difoundation", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 12:
                            {
                                // basement
                                modDataTypes.Statics.DWL.Set_Fields("dibsmt", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 13:
                            {
                                // bsmt garage
                                modDataTypes.Statics.DWL.Set_Fields("dibsmtgar", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 14:
                            {
                                // wet basement
                                modDataTypes.Statics.DWL.Set_Fields("diwetbsmt", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                    }
                    //end switch
                }
                frmHelp.InstancePtr.Unload();
                return;
            }
            if (GridDwelling1.Row == 0)
            {
                if (KeyCode == Keys.Up)
                {
                    KeyCode = 0;
                    GridDwelling3.Row = GridDwelling3.Rows - 1;
                    GridDwelling3.Focus();
                    // GridDwelling3.EditCell
                }
            }
            else if (GridDwelling1.Row == GridDwelling1.Rows - 1)
            {
                if (KeyCode == Keys.Down || KeyCode == Keys.Return)
                {
                    KeyCode = 0;
                    GridDwelling2.Row = 0;
                    GridDwelling2.Focus();
                    // GridDwelling2.EditCell
                }
            }
            else if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                GridDwelling1.Row += 1;
            }
        }

        private void GridDwelling1_KeyDownEdit(object sender, KeyEventArgs e)
        {
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (e.KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("GRIDDWELLING1", GridDwelling1.Row);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                {
                    GridDwelling1.TextMatrix(GridDwelling1.Row, 1, Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                    switch (GridDwelling1.Row)
                    {
                        case 0:
                            {
                                // building style
                                modDataTypes.Statics.DWL.Set_Fields("distyle", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 1:
                            {
                                // dwelling units
                                modDataTypes.Statics.DWL.Set_Fields("diunitsdwelling", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 2:
                            {
                                // other units
                                modDataTypes.Statics.DWL.Set_Fields("diunitsother", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 3:
                            {
                                // stories
                                modDataTypes.Statics.DWL.Set_Fields("distories", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 4:
                            {
                                // exterior wall
                                modDataTypes.Statics.DWL.Set_Fields("diextwalls", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 5:
                            {
                                // roof surface
                                modDataTypes.Statics.DWL.Set_Fields("diroof", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 6:
                            {
                                // sf masonry trim
                                modDataTypes.Statics.DWL.Set_Fields("disfmasonry", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 7:
                            {
                                // open3
                                modDataTypes.Statics.DWL.Set_Fields("diopen3", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 8:
                            {
                                // open4
                                modDataTypes.Statics.DWL.Set_Fields("diopen4", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 9:
                            {
                                // yearbuilt
                                modDataTypes.Statics.DWL.Set_Fields("diyearbuilt", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 10:
                            {
                                // year remodelled
                                modDataTypes.Statics.DWL.Set_Fields("diyearremodel", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 11:
                            {
                                // foundation
                                modDataTypes.Statics.DWL.Set_Fields("difoundation", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 12:
                            {
                                // basement
                                modDataTypes.Statics.DWL.Set_Fields("dibsmt", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 13:
                            {
                                // bsmt garage
                                modDataTypes.Statics.DWL.Set_Fields("dibsmtgar", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 14:
                            {
                                // wet basement
                                modDataTypes.Statics.DWL.Set_Fields("diwetbsmt", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                    }
                    //end switch
                }
                frmHelp.InstancePtr.Unload();
                return;
            }
            else if (e.KeyCode == Keys.Return)
            {
                if (GridDwelling1.Row == GridDwelling1.Rows - 1)
                {
                    GridDwelling2.Row = 0;
                    Support.SendKeys("{TAB}", false);
                }
                else
                {
                    Support.SendKeys("{TAB}", false);
                }
            }
        }

        private void GridDwelling1_KeyUpEdit(object sender, KeyEventArgs e)
        {
            if (GridDwelling1.EditText.Length >= GridDwelling1.EditMaxLength)
            {
                if (GridDwelling1.Row < GridDwelling1.Rows - 1)
                {
                    GridDwelling1.Row = GridDwelling1.Row + 1;
                }
                else
                {
                    GridDwelling2.Row = 0;
                    GridDwelling2.Focus();
                }
            }
        }

        private void GridDwelling1_RowColChange(object sender, System.EventArgs e)
        {
            if (GridDwelling1.Row == GridDwelling1.Rows - 1)
            {
                GridDwelling1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridDwelling1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridDwelling1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridDwelling1.GetFlexRowIndex(e.RowIndex);
            int col = GridDwelling1.GetFlexColIndex(e.ColumnIndex);
            switch (row)
            {
                case 0:
                    {
                        // building style
                        modDataTypes.Statics.DWL.Set_Fields("distyle", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 1:
                    {
                        // dwelling units
                        modDataTypes.Statics.DWL.Set_Fields("diunitsdwelling", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 2:
                    {
                        // other units
                        modDataTypes.Statics.DWL.Set_Fields("diunitsother", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 3:
                    {
                        // stories
                        modDataTypes.Statics.DWL.Set_Fields("distories", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 4:
                    {
                        // exterior wall
                        modDataTypes.Statics.DWL.Set_Fields("diextwalls", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 5:
                    {
                        // roof surface
                        modDataTypes.Statics.DWL.Set_Fields("diroof", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 6:
                    {
                        // sf masonry trim
                        modDataTypes.Statics.DWL.Set_Fields("disfmasonry", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 7:
                    {
                        // open3
                        modDataTypes.Statics.DWL.Set_Fields("diopen3", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 8:
                    {
                        // open4
                        modDataTypes.Statics.DWL.Set_Fields("diopen4", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 9:
                    {
                        // yearbuilt
                        modDataTypes.Statics.DWL.Set_Fields("diyearbuilt", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 10:
                    {
                        // year remodelled
                        modDataTypes.Statics.DWL.Set_Fields("diyearremodel", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 11:
                    {
                        // foundation
                        modDataTypes.Statics.DWL.Set_Fields("difoundation", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 12:
                    {
                        // basement
                        modDataTypes.Statics.DWL.Set_Fields("dibsmt", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 13:
                    {
                        // bsmt garage
                        modDataTypes.Statics.DWL.Set_Fields("dibsmtgar", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
                case 14:
                    {
                        // wet basement
                        modDataTypes.Statics.DWL.Set_Fields("diwetbsmt", FCConvert.ToString(Conversion.Val(GridDwelling1.EditText)));
                        break;
                    }
            }
            //end switch
        }

        private void GridDwelling2_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (GridDwelling2.Row == 0)
            {
                GridDwelling2.EditMaxLength = 5;
            }
            else if ((GridDwelling2.Row == 1) || (GridDwelling2.Row == 6) || (GridDwelling2.Row == 8) || (GridDwelling2.Row == 9) || (GridDwelling2.Row == 15))
            {
                GridDwelling2.EditMaxLength = 1;
            }
            else if ((GridDwelling2.Row == 2) || (GridDwelling2.Row == 4) || (GridDwelling2.Row == 5) || (GridDwelling2.Row == 7))
            {
                GridDwelling2.EditMaxLength = 3;
            }
            else if (GridDwelling2.Row == 3)
            {
                GridDwelling2.EditMaxLength = 4;
            }
            else if (GridDwelling2.Row >= 10 && GridDwelling2.Row <= 14)
            {
                GridDwelling2.EditMaxLength = 2;
            }
        }

        private void GridDwelling2_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("GRIDDWELLING2", GridDwelling2.Row);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                {
                    GridDwelling2.TextMatrix(GridDwelling2.Row, 1, Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                    switch (GridDwelling2.Row)
                    {
                        case 0:
                            {
                                // sf basement living
                                modDataTypes.Statics.DWL.Set_Fields("disfbsmtliving", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 1:
                            {
                                // finished bsmt grade
                                modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade1", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 2:
                            {
                                // factor
                                modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade2", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 3:
                            {
                                // open5
                                modDataTypes.Statics.DWL.Set_Fields("diopen5", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 4:
                            {
                                // heat type
                                modDataTypes.Statics.DWL.Set_Fields("diheat", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 5:
                            {
                                // heat percent
                                modDataTypes.Statics.DWL.Set_Fields("dipctheat", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 6:
                            {
                                // cool type
                                modDataTypes.Statics.DWL.Set_Fields("dicool", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 7:
                            {
                                // cool percent
                                modDataTypes.Statics.DWL.Set_Fields("dipctcool", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 8:
                            {
                                // kitchen
                                modDataTypes.Statics.DWL.Set_Fields("dikitchens", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 9:
                            {
                                // bath sytle
                                modDataTypes.Statics.DWL.Set_Fields("dibaths", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 10:
                            {
                                // #rooms
                                modDataTypes.Statics.DWL.Set_Fields("dirooms", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 11:
                            {
                                // #bedrooms
                                modDataTypes.Statics.DWL.Set_Fields("dibedrooms", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 12:
                            {
                                // #fullbaths
                                modDataTypes.Statics.DWL.Set_Fields("difullbaths", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 13:
                            {
                                // #halfbaths
                                modDataTypes.Statics.DWL.Set_Fields("dihalfbaths", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 14:
                            {
                                // #addfixtures
                                modDataTypes.Statics.DWL.Set_Fields("diaddnfixtures", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 15:
                            {
                                // #Fireplaces
                                modDataTypes.Statics.DWL.Set_Fields("difireplaces", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                    }
                    //end switch
                }
                frmHelp.InstancePtr.Unload();
                return;
            }
            if (GridDwelling2.Row == 0)
            {
                if (KeyCode == Keys.Up)
                {
                    KeyCode = 0;
                    GridDwelling1.Row = GridDwelling1.Rows - 1;
                    GridDwelling1.Focus();
                    // GridDwelling1.EditCell
                }
            }
            else if (GridDwelling2.Row == GridDwelling2.Rows - 1)
            {
                if (KeyCode == Keys.Down || KeyCode == Keys.Return)
                {
                    KeyCode = 0;
                    GridDwelling3.Row = 0;
                    GridDwelling3.Focus();
                    // GridDwelling3.EditCell
                }
            }
            else if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                GridDwelling2.Row += 1;
            }
        }

        private void GridDwelling2_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("GRIDDWELLING2", GridDwelling2.Row);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                {
                    GridDwelling2.TextMatrix(GridDwelling2.Row, 1, Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                    switch (GridDwelling2.Row)
                    {
                        case 0:
                            {
                                // sf basement living
                                modDataTypes.Statics.DWL.Set_Fields("disfbsmtliving", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 1:
                            {
                                // finished bsmt grade
                                modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade1", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 2:
                            {
                                // factor
                                modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade2", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 3:
                            {
                                // open5
                                modDataTypes.Statics.DWL.Set_Fields("diopen5", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 4:
                            {
                                // heat type
                                modDataTypes.Statics.DWL.Set_Fields("diheat", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 5:
                            {
                                // heat percent
                                modDataTypes.Statics.DWL.Set_Fields("dipctheat", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 6:
                            {
                                // cool type
                                modDataTypes.Statics.DWL.Set_Fields("dicool", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 7:
                            {
                                // cool percent
                                modDataTypes.Statics.DWL.Set_Fields("dipctcool", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 8:
                            {
                                // kitchen
                                modDataTypes.Statics.DWL.Set_Fields("dikitchens", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 9:
                            {
                                // bath sytle
                                modDataTypes.Statics.DWL.Set_Fields("dibaths", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 10:
                            {
                                // #rooms
                                modDataTypes.Statics.DWL.Set_Fields("dirooms", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 11:
                            {
                                // #bedrooms
                                modDataTypes.Statics.DWL.Set_Fields("dibedrooms", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 12:
                            {
                                // #fullbaths
                                modDataTypes.Statics.DWL.Set_Fields("difullbaths", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 13:
                            {
                                // #halfbaths
                                modDataTypes.Statics.DWL.Set_Fields("dihalfbaths", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 14:
                            {
                                // #addfixtures
                                modDataTypes.Statics.DWL.Set_Fields("diaddnfixtures", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 15:
                            {
                                // #Fireplaces
                                modDataTypes.Statics.DWL.Set_Fields("difireplaces", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                    }
                    //end switch
                }
                frmHelp.InstancePtr.Unload();
                return;
            }
            else if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                if (GridDwelling2.Row == GridDwelling2.Rows - 1)
                {
                    GridDwelling3.Row = 0;
                    Support.SendKeys("{TAB}", false);
                }
                else
                {
                    Support.SendKeys("{TAB}", false);
                }
            }
        }

        private void GridDwelling2_KeyUpEdit(object sender, KeyEventArgs e)
        {
            if (GridDwelling2.EditText.Length >= GridDwelling2.EditMaxLength)
            {
                if (GridDwelling2.Row < GridDwelling2.Rows - 1)
                {
                    GridDwelling2.Row = GridDwelling2.Row + 1;
                }
                else
                {
                    GridDwelling3.Row = 0;
                    GridDwelling3.Focus();
                }
            }
        }

        private void GridDwelling2_RowColChange(object sender, System.EventArgs e)
        {
            if (GridDwelling2.Row == GridDwelling2.Rows - 1)
            {
                GridDwelling2.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridDwelling2.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void GridDwelling2_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridDwelling2.GetFlexRowIndex(e.RowIndex);
            int col = GridDwelling2.GetFlexColIndex(e.ColumnIndex);
            switch (row)
            {
                case 0:
                    {
                        // sf basement living
                        modDataTypes.Statics.DWL.Set_Fields("disfbsmtliving", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 1:
                    {
                        // finished bsmt grade
                        modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade1", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 2:
                    {
                        // factor
                        modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade2", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 3:
                    {
                        // open5
                        modDataTypes.Statics.DWL.Set_Fields("diopen5", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 4:
                    {
                        // heat type
                        modDataTypes.Statics.DWL.Set_Fields("diheat", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 5:
                    {
                        // heat percent
                        modDataTypes.Statics.DWL.Set_Fields("dipctheat", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 6:
                    {
                        // cool type
                        modDataTypes.Statics.DWL.Set_Fields("dicool", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 7:
                    {
                        // cool percent
                        modDataTypes.Statics.DWL.Set_Fields("dipctcool", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 8:
                    {
                        // kitchen
                        modDataTypes.Statics.DWL.Set_Fields("dikitchens", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 9:
                    {
                        // bath sytle
                        modDataTypes.Statics.DWL.Set_Fields("dibaths", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 10:
                    {
                        // #rooms
                        modDataTypes.Statics.DWL.Set_Fields("dirooms", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 11:
                    {
                        // #bedrooms
                        modDataTypes.Statics.DWL.Set_Fields("dibedrooms", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 12:
                    {
                        // #fullbaths
                        modDataTypes.Statics.DWL.Set_Fields("difullbaths", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 13:
                    {
                        // #halfbaths
                        modDataTypes.Statics.DWL.Set_Fields("dihalfbaths", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 14:
                    {
                        // #addfixtures
                        modDataTypes.Statics.DWL.Set_Fields("diaddnfixtures", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
                case 15:
                    {
                        // #Fireplaces
                        modDataTypes.Statics.DWL.Set_Fields("difireplaces", FCConvert.ToString(Conversion.Val(GridDwelling2.EditText)));
                        break;
                    }
            }
            //end switch
        }

        private void GridDwelling3_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            switch (GridDwelling3.Row)
            {
                case 0:
                case 1:
                case 2:
                case 4:
                case 7:
                case 10:
                    {
                        GridDwelling3.EditMaxLength = 1;
                        break;
                    }
                case 3:
                    {
                        GridDwelling3.EditMaxLength = 2;
                        break;
                    }
                case 5:
                case 8:
                case 9:
                case 11:
                    {
                        GridDwelling3.EditMaxLength = 3;
                        break;
                    }
                case 6:
                    {
                        GridDwelling3.EditMaxLength = 5;
                        break;
                    }
                case 12:
                    {
                        GridDwelling3.EditMaxLength = 0;
                        break;
                    }
            }
            //end switch
        }

        private void GridDwelling3_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("GRIDDWELLING3", GridDwelling3.Row);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                {
                    GridDwelling3.TextMatrix(GridDwelling3.Row, 1, Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                    switch (GridDwelling3.Row)
                    {
                        case 0:
                            {
                                // Layout
                                modDataTypes.Statics.DWL.Set_Fields("dilayout", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 1:
                            {
                                // attic
                                modDataTypes.Statics.DWL.Set_Fields("diattic", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 2:
                            {
                                // insulation
                                modDataTypes.Statics.DWL.Set_Fields("diinsulation", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 3:
                            {
                                // percent unfinished
                                modDataTypes.Statics.DWL.Set_Fields("dipctunfinished", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 4:
                            {
                                // grade
                                modDataTypes.Statics.DWL.Set_Fields("digrade1", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 5:
                            {
                                // factor
                                modDataTypes.Statics.DWL.Set_Fields("digrade2", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 6:
                            {
                                // square footage
                                modDataTypes.Statics.DWL.Set_Fields("disqft", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 7:
                            {
                                // condition
                                modDataTypes.Statics.DWL.Set_Fields("dicondition", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 8:
                            {
                                // physical % good
                                modDataTypes.Statics.DWL.Set_Fields("dipctphys", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 9:
                            {
                                // Functional % good
                                modDataTypes.Statics.DWL.Set_Fields("dipctfunct", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 10:
                            {
                                // functional code
                                modDataTypes.Statics.DWL.Set_Fields("difunctcode", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 11:
                            {
                                // Econ good
                                modDataTypes.Statics.DWL.Set_Fields("dipctecon", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 12:
                            {
                                // econ code
                                modDataTypes.Statics.DWL.Set_Fields("dieconcode", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                    }
                    //end switch
                }
                frmHelp.InstancePtr.Unload();
                return;
            }
            if (GridDwelling3.Row == 0)
            {
                if (KeyCode == Keys.Up)
                {
                    KeyCode = 0;
                    GridDwelling2.Row = GridDwelling2.Rows - 1;
                    GridDwelling2.Focus();
                    // GridDwelling2.EditCell
                }
            }
            else if (GridDwelling3.Row == GridDwelling3.Rows - 1)
            {
                if (KeyCode == Keys.Down || KeyCode == Keys.Return)
                {
                    KeyCode = 0;
                    GridDwelling1.Row = 0;
                    GridDwelling1.Focus();
                    // GridDwelling1.EditCell
                }
            }
            else if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                GridDwelling3.Row += 1;
            }
        }

        private void GridDwelling3_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("GRIDDWELLING3", GridDwelling3.Row);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                {
                    GridDwelling3.TextMatrix(GridDwelling3.Row, 1, Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                    switch (GridDwelling3.Row)
                    {
                        case 0:
                            {
                                // Layout
                                modDataTypes.Statics.DWL.Set_Fields("dilayout", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 1:
                            {
                                // attic
                                modDataTypes.Statics.DWL.Set_Fields("diattic", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 2:
                            {
                                // insulation
                                modDataTypes.Statics.DWL.Set_Fields("diinsulation", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 3:
                            {
                                // percent unfinished
                                modDataTypes.Statics.DWL.Set_Fields("dipctunfinished", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 4:
                            {
                                // grade
                                modDataTypes.Statics.DWL.Set_Fields("digrade1", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 5:
                            {
                                // factor
                                modDataTypes.Statics.DWL.Set_Fields("digrade2", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 6:
                            {
                                // square footage
                                modDataTypes.Statics.DWL.Set_Fields("disqft", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 7:
                            {
                                // condition
                                modDataTypes.Statics.DWL.Set_Fields("dicondition", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 8:
                            {
                                // physical % good
                                modDataTypes.Statics.DWL.Set_Fields("dipctphys", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 9:
                            {
                                // Functional % good
                                modDataTypes.Statics.DWL.Set_Fields("dipctfunct", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 10:
                            {
                                // functional code
                                modDataTypes.Statics.DWL.Set_Fields("difunctcode", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 11:
                            {
                                // Econ good
                                modDataTypes.Statics.DWL.Set_Fields("dipctecon", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                        case 12:
                            {
                                // econ code
                                modDataTypes.Statics.DWL.Set_Fields("dieconcode", Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)));
                                break;
                            }
                    }
                    //end switch
                }
                frmHelp.InstancePtr.Unload();
                return;
            }
            else if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                if (GridDwelling3.Row == GridDwelling3.Rows - 1)
                {
                    GridDwelling1.Row = 0;
                    Support.SendKeys("{TAB}", false);
                }
                else
                {
                    Support.SendKeys("{TAB}", false);
                }
            }
        }

        private void GridDwelling3_KeyUpEdit(object sender, KeyEventArgs e)
        {
            if (GridDwelling3.EditText.Length >= GridDwelling3.EditMaxLength && GridDwelling3.EditMaxLength > 0)
            {
                if (GridDwelling3.Row < GridDwelling3.Rows - 1)
                {
                    GridDwelling3.Row = GridDwelling3.Row + 1;
                }
                else
                {
                    GridDwelling1.Row = 0;
                    GridDwelling1.Focus();
                }
            }
        }

        private void GridDwelling3_RowColChange(object sender, System.EventArgs e)
        {

        }

        private void GridDwelling3_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridDwelling3.GetFlexRowIndex(e.RowIndex);
            int col = GridDwelling3.GetFlexColIndex(e.ColumnIndex);
            switch (row)
            {
                case 0:
                    {
                        // Layout
                        modDataTypes.Statics.DWL.Set_Fields("dilayout", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 1:
                    {
                        // attic
                        modDataTypes.Statics.DWL.Set_Fields("diattic", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 2:
                    {
                        // insulation
                        modDataTypes.Statics.DWL.Set_Fields("diinsulation", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 3:
                    {
                        // percent unfinished
                        modDataTypes.Statics.DWL.Set_Fields("dipctunfinished", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 4:
                    {
                        // grade
                        modDataTypes.Statics.DWL.Set_Fields("digrade1", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 5:
                    {
                        // factor
                        modDataTypes.Statics.DWL.Set_Fields("digrade2", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 6:
                    {
                        // square footage
                        modDataTypes.Statics.DWL.Set_Fields("disqft", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 7:
                    {
                        // condition
                        modDataTypes.Statics.DWL.Set_Fields("dicondition", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 8:
                    {
                        // physical % good
                        modDataTypes.Statics.DWL.Set_Fields("dipctphys", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 9:
                    {
                        // Functional % good
                        modDataTypes.Statics.DWL.Set_Fields("dipctfunct", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 10:
                    {
                        // functional code
                        modDataTypes.Statics.DWL.Set_Fields("difunctcode", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 11:
                    {
                        // Econ good
                        modDataTypes.Statics.DWL.Set_Fields("dipctecon", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
                case 12:
                    {
                        // econ code
                        modDataTypes.Statics.DWL.Set_Fields("dieconcode", FCConvert.ToString(Conversion.Val(GridDwelling3.EditText)));
                        break;
                    }
            }
            //end switch
        }

        private void mebOcc1_Enter(object sender, System.EventArgs e)
        {
            SSTab1.SelectedIndex = 3;
            selecteverything();
        }

        private void mebOcc1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebOcc1.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebOcc1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("OCC1", FCConvert.ToString(Conversion.Val(mebOcc1.Text + "")));
        }

        private void mebOcc2_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebOcc2_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebOcc2.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebOcc2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("OCC2", FCConvert.ToString(Conversion.Val(mebOcc2.Text + "")));
        }

        private void mebDwel1_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebDwel1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebDwel1.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebDwel1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("DWEL1", FCConvert.ToString(Conversion.Val(mebDwel1.Text + "")));
        }

        private void mebDwel2_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebDwel2_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebDwel2.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebDwel2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("DWEL2", FCConvert.ToString(Conversion.Val(mebDwel2.Text + "")));
        }

        private void mebC1Class_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Class_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Class.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Class_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1class", FCConvert.ToString(Conversion.Val(mebC1Class.Text + "")));
        }

        private void mebC2Class_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Class_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Class.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Class_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2class", FCConvert.ToString(Conversion.Val(mebC2Class.Text + "")));
        }

        private void mebC1Quality_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Quality_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Quality.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Quality_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1quality", FCConvert.ToString(Conversion.Val(mebC1Quality.Text + "")));
        }

        private void mebC2Quality_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Quality_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Quality.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Quality_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2quality", FCConvert.ToString(Conversion.Val(mebC2Quality.Text + "")));
        }

        private void mebC1Grade_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Grade_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Grade.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Grade_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mebC1Grade.Text = Strings.Format(Conversion.Val(mebC1Grade.Text), "0.00");
            modDataTypes.Statics.CMR.Set_Fields("c1grade", Conversion.Val(mebC1Grade.Text + "") * 100);
        }

        private void mebc2grade_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebc2grade_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebc2grade.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebc2grade_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mebc2grade.Text = Strings.Format(Conversion.Val(mebc2grade.Text), "0.00");
            modDataTypes.Statics.CMR.Set_Fields("c2grade", Conversion.Val(mebc2grade.Text + "") * 100);
        }

        private void mebC1ExteriorWalls_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1ExteriorWalls_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2(Strings.UCase(FCGlobal.Screen.ActiveControl.Name));
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1ExteriorWalls.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1ExteriorWalls_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1extwalls", FCConvert.ToString(Conversion.Val(mebC1ExteriorWalls.Text + "")));
        }

        private void mebC2ExtWalls_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2ExtWalls_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2ExtWalls.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2ExtWalls_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2extwalls", FCConvert.ToString(Conversion.Val(mebC2ExtWalls.Text + "")));
        }

        private void mebC1Stories_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Stories_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Stories.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Stories_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1stories", FCConvert.ToString(Conversion.Val(mebC1Stories.Text + "")));
        }

        private void mebC2Stories_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Stories_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Stories.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Stories_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2stories", FCConvert.ToString(Conversion.Val(mebC2Stories.Text + "")));
        }

        private void mebC1Height_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Height_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Height.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Height_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1height", FCConvert.ToString(Conversion.Val(mebC1Height.Text + "")));
        }

        private void mebC2Height_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Height_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Height.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Height_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2height", FCConvert.ToString(Conversion.Val(mebC2Height.Text + "")));
        }

        private void mebC1Floor_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Floor_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Floor.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Floor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1floor", FCConvert.ToString(Conversion.Val(mebC1Floor.Text + "")));
        }

        private void mebC2Floor_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Floor_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Floor.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Floor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2floor", FCConvert.ToString(Conversion.Val(mebC2Floor.Text + "")));
        }

        private void mebC1Perimeter_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Perimeter_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Perimeter.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Perimeter_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1perimeter", FCConvert.ToString(Conversion.Val(mebC1Perimeter.Text + "")));
        }

        private void mebC2Perimeter_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Perimeter_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Perimeter.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Perimeter_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2perimeter", FCConvert.ToString(Conversion.Val(mebC2Perimeter.Text + "")));
        }

        private void mebC1Heat_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Heat_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Heat.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Heat_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1heat", FCConvert.ToString(Conversion.Val(mebC1Heat.Text + "")));
        }

        private void mebC2Heat_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Heat_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Heat.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Heat_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2heat", FCConvert.ToString(Conversion.Val(mebC2Heat.Text + "")));
        }

        private void mebC1Built_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Built_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Built.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Built_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1built", FCConvert.ToString(Conversion.Val(mebC1Built.Text + "")));
        }

        private void mebC2Built_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Built_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Built.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Built_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2built", FCConvert.ToString(Conversion.Val(mebC2Built.Text + "")));
        }

        private void mebC1Remodel_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Remodel_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Remodel.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Remodel_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1remodel", FCConvert.ToString(Conversion.Val(mebC1Remodel.Text + "")));
        }

        private void mebC2Remodel_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Remodel_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Remodel.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Remodel_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2remodel", FCConvert.ToString(Conversion.Val(mebC2Remodel.Text + "")));
        }

        private void mebC1Condition_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Condition_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Condition.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Condition_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1condition", FCConvert.ToString(Conversion.Val(mebC1Condition.Text + "")));
        }

        private void mebC2Condition_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Condition_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Condition.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Condition_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2condition", FCConvert.ToString(Conversion.Val(mebC2Condition.Text + "")));
        }

        private void mebC1Phys_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Phys_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Phys.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Phys_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1phys", FCConvert.ToString(Conversion.Val(mebC1Phys.Text + "")));
        }

        private void mebC2Phys_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Phys_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Phys.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Phys_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2phys", FCConvert.ToString(Conversion.Val(mebC2Phys.Text + "")));
        }

        private void mebC1Funct_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC1Funct_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC1Funct.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC1Funct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c1funct", FCConvert.ToString(Conversion.Val(mebC1Funct.Text + "")));
        }

        private void mebC2Funct_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebC2Funct_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebC2Funct.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebC2Funct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            modDataTypes.Statics.CMR.Set_Fields("c2funct", FCConvert.ToString(Conversion.Val(mebC2Funct.Text + "")));
        }

        private void mebCMEcon_Enter(object sender, System.EventArgs e)
        {
            selecteverything();
        }

        private void mebCMEcon_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    mebCMEcon.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
        }

        public void mebCMEcon_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(Strings.Trim(mebCMEcon.Text)) > 100)
            {
                MessageBox.Show("You have entered an economic percent greater than 100", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            else
            {
                modDataTypes.Statics.CMR.Set_Fields("cmecon", FCConvert.ToString(Conversion.Val(mebCMEcon.Text + "")));
            }
        }

        private void txtMROIType1_Enter(short Index, object sender, System.EventArgs e)
        {
            if (Index == 0)
                SSTab1.SelectedIndex = 4;
            selecteverything();
            // If (Trim(txtMROIType1(Index).Text) = vbNullString) Then
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (Index > 0)
                {
                    // If Trim(txtMROIType1(Index - 1).Text) = vbNullString Then
                    if (Conversion.Val(txtMROIType1[FCConvert.ToInt16(Index - 1)].Text + "") == 0)
                    {
                        if (txtMROIType1[FCConvert.ToInt16(Index - 1)].Enabled)
                            txtMROIType1[FCConvert.ToInt16(Index - 1)].Focus();
                    }
                }
            }
        }

        private void txtMROIType1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIType1.GetIndex((FCTextBox)sender);
            txtMROIType1_Enter(index, sender, e);
        }

        private void txtMROIType1_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                    // If Index > 0 Then
                    // txtMROIType1(0).SetFocus
                    // End If
                }
                else
                {
                    Support.SendKeys("{tab}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMROIType1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            short index = txtMROIType1.GetIndex((FCTextBox)sender);
            txtMROIType1_KeyPress(index, sender, e);
        }

        private void txtMROIType1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    if (Conversion.Val(txtMROIType1[FCConvert.ToInt16(Index + 1)].Text + "") != 0)
                    {
                        if (txtMROIType1[FCConvert.ToInt16(Index + 1)].Enabled)
                            txtMROIType1[FCConvert.ToInt16(Index + 1)].Focus();
                    }
                    else
                    {
                        if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
                        {
                            if (Index > 0)
                            {
                                txtMROIType1[0].Focus();
                            }
                        }
                        else
                        {
                            if (txtMROIType1[FCConvert.ToInt16(Index + 1)].Enabled)
                                txtMROIType1[FCConvert.ToInt16(Index + 1)].Focus();
                        }
                    }
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIType1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIType1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                // If Val(txtMROIType1(Index).Text) > 699 Then
                // Call HelpFiles("OUTBUILDING", 294)
                // Else
                // Call HelpFiles(Screen.ActiveControl.Name)
                // End If
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    modHelpFiles.HelpFiles_2("outbuilding", Conversion.Val(txtMROIType1[Index].Text));
                }
                else
                {
                    modHelpFiles.HelpFiles_20("outbuilding", Conversion.Val(txtMROIType1[Index].Text), FCConvert.ToInt16(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                }
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROIType1[Index].Text = FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(modGNBas.Statics.response))));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIType1[Index].SelectionStart == txtMROIType1[Index].MaxLength)
            {
                if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + "")) == 0)
                {
                    txtMROIType1_Validate(Index, false);
                }
                else
                {
                    if (txtMROIYear1[Index].Enabled)
                        txtMROIYear1[Index].Focus();
                }
            }
        }

        private void txtMROIType1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIType1.GetIndex((FCTextBox)sender);
            txtMROIType1_KeyUp(index, sender, e);
        }

        private void txtMROIType1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order 
            modDataTypes.Statics.OUT.Set_Fields("OITYPE" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + ""))));
        }

        private void txtMROIType1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIType1.GetIndex((FCTextBox)sender);
            txtMROIType1_Leave(index, sender, e);
        }
        // vbPorter upgrade warning: Index As short	OnWriteFCConvert.ToInt32(
        public void cleartherestoftheline(ref short Index)
        {
            txtMROIYear1[Index].Text = string.Empty;
            txtMROIUnits1[Index].Text = string.Empty;
            txtMROIGradeCd1[Index].Text = string.Empty;
            txtMROIGradePct1[Index].Text = string.Empty;
            txtMROICond1[Index].Text = string.Empty;
            txtMROIPctPhys1[Index].Text = string.Empty;
            txtMROIPctFunct1[Index].Text = string.Empty;
            txtMROISoundValue[Index].Text = string.Empty;
            chkSound[Index].CheckState = Wisej.Web.CheckState.Unchecked;
        }

        public void txtMROIType1_Validating_6(short Index, bool Cancel)
        {
            txtMROIType1_Validating(Index, new object(), new System.ComponentModel.CancelEventArgs(Cancel));
        }

        public void txtMROIType1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OITYPE" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIType1[Index].Text + ""))));
            if (Conversion.Val(Strings.Trim(txtMROIType1[Index].Text) + "") == 0)
            {
                cleartherestoftheline(ref Index);
                txtMROIType1[Index].Text = "";
                if (Index < 9)
                {
                    condenseoutbuilding();
                    selecteverything();
                }
                else
                {
                    if (MaskEdbox3.Enabled)
                        MaskEdbox3.Focus();
                }
            }
        }

        public void txtMROIType1_Validate(short Index, bool Cancel)
        {
            txtMROIType1_Validating(Index, txtMROIType1[Index], new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void txtMROIType1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROIType1.GetIndex((FCTextBox)sender);
            txtMROIType1_Validating(index, sender, e);
        }

        private void txtMROIYear1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Strings.Trim(txtMROIType1[Index].Text) == string.Empty || (Conversion.Val(txtMROIType1[Index].Text + "") == 0))
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROIYear1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIYear1.GetIndex((FCTextBox)sender);
            txtMROIYear1_Enter(index, sender, e);
        }

        private void txtMROIYear1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROIYear1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROIYear1[FCConvert.ToInt16(Index + 1)].Focus();
                    // Else: MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIYear1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIYear1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                //FC:FINAL:AM:#3276 - use correct name
                //modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                modHelpFiles.HelpFiles("txtMROIYear1");
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIYear1[Index].SelectionStart == txtMROIYear1[Index].MaxLength)
            {
                if (txtMROIUnits1[Index].Enabled)
                    txtMROIUnits1[Index].Focus();
            }
        }

        private void txtMROIYear1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIYear1.GetIndex((FCTextBox)sender);
            txtMROIYear1_KeyUp(index, sender, e);
        }

        private void txtMROIYear1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIYEAR" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIYear1[Index].Text + ""))));
        }

        private void txtMROIYear1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIYear1.GetIndex((FCTextBox)sender);
            txtMROIYear1_Leave(index, sender, e);
        }

        public void txtMROIYear1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIYEAR" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIYear1[Index].Text + "")));
        }

        private void txtMROIYear1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROIYear1.GetIndex((FCTextBox)sender);
            txtMROIYear1_Validating(index, sender, e);
        }

        private void txtMROIUnits1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROIUnits1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIUnits1.GetIndex((FCTextBox)sender);
            txtMROIUnits1_Enter(index, sender, e);
        }

        private void txtMROIUnits1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (Strings.InStr(1, txtMROIUnits1[Index].Text, "*", CompareConstants.vbTextCompare) > 0)
            {
                if (Conversion.Val(txtMROIType1[Index].Text) < 700)
                {
                    txtMROIUnits1[Index].MaxLength = 7;
                }
                else
                {
                    txtMROIUnits1[Index].MaxLength = 5;
                }
            }
            else
            {
                if (Conversion.Val(txtMROIType1[Index].Text) < 700)
                {
                    txtMROIUnits1[Index].MaxLength = 7;
                }
                else
                {
                    txtMROIUnits1[Index].MaxLength = 4;
                }
            }
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROIUnits1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROIUnits1[FCConvert.ToInt16(Index + 1)].Focus();
                    KeyCode = (Keys)0;
                    // Else
                    // MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIUnits1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIUnits1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                //modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                modHelpFiles.HelpFiles("txtMROIUnits1");
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROIUnits1[Index].Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIUnits1[Index].SelectionStart == txtMROIUnits1[Index].MaxLength)
            {
                if (txtMROIGradeCd1[Index].Enabled)
                    txtMROIGradeCd1[Index].Focus();
            }
        }

        private void txtMROIUnits1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIUnits1.GetIndex((FCTextBox)sender);
            txtMROIUnits1_KeyUp(index, sender, e);
        }

        private void txtMROIUnits1_Leave(short Index, object sender, System.EventArgs e)
        {
            if (Conversion.Val(txtMROIType1[Index].Text) > 0)
            {
                if (Conversion.Val(txtMROIType1[Index].Text) < 700)
                {
                    if (Conversion.Val(txtMROIUnits1[Index].Text) > 0)
                    {
                        // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), ConvertStringtoValue_2(txtMROIUnits1[Index].Text));
                        // TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
                        txtMROIUnits1[Index].Text = FCConvert.ToString(modDataTypes.Statics.OUT.Get_Fields("oiunits" + FCConvert.ToString(Index + 1)));
                    }
                    else
                    {
                        // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), 0);
                    }
                }
                else
                {
                    if (Conversion.Val(txtMROIUnits1[Index].Text) > 0)
                    {
                        int intpos = 0;
                        string strTemp = "";
                        string strTemp2 = "";
                        intpos = Strings.InStr(1, txtMROIUnits1[Index].Text, "*", CompareConstants.vbTextCompare);
                        if (intpos > 0)
                        {
                            strTemp = Strings.Mid(txtMROIUnits1[Index].Text, 1, intpos - 1);
                            strTemp2 = Strings.Mid(txtMROIUnits1[Index].Text, intpos + 1);
                            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                            modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), FCConvert.ToString(Conversion.Val(strTemp + Strings.Format(strTemp2, "00"))));
                            txtMROIUnits1[Index].Text = FCConvert.ToString(Conversion.Val(strTemp + Strings.Format(strTemp2, "00")));
                        }
                        else
                        {
                            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                            modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIUnits1[Index].Text)));
                        }
                    }
                    else
                    {
                        // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), 0);
                    }
                }
            }
            else
            {
                // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                modDataTypes.Statics.OUT.Set_Fields("OIUNITS" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIUnits1[Index].Text + "")));
            }
        }

        private void txtMROIUnits1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIUnits1.GetIndex((FCTextBox)sender);
            txtMROIUnits1_Leave(index, sender, e);
        }

        public void txtMROIUnits1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txtMROIType1[Index].Text) > 0)
            {
                if (Conversion.Val(txtMROIType1[Index].Text) < 700)
                {
                    if (Conversion.Val(txtMROIUnits1[Index].Text) > 0)
                    {
                        // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), ConvertStringtoValue_2(txtMROIUnits1[Index].Text));
                        // TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
                        txtMROIUnits1[Index].Text = FCConvert.ToString(modDataTypes.Statics.OUT.Get_Fields("oiunits" + FCConvert.ToString(Index + 1)));
                    }
                    else
                    {
                        // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), 0);
                    }
                }
                else
                {
                    if (Conversion.Val(txtMROIUnits1[Index].Text) > 0)
                    {
                        int intpos = 0;
                        string strTemp = "";
                        string strTemp2 = "";
                        intpos = Strings.InStr(1, txtMROIUnits1[Index].Text, "*", CompareConstants.vbTextCompare);
                        if (intpos > 0)
                        {
                            strTemp = Strings.Mid(txtMROIUnits1[Index].Text, 1, intpos - 1);
                            strTemp2 = Strings.Mid(txtMROIUnits1[Index].Text, intpos + 1);
                            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                            modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), FCConvert.ToString(Conversion.Val(strTemp + Strings.Format(strTemp2, "00"))));
                            txtMROIUnits1[Index].Text = FCConvert.ToString(Conversion.Val(strTemp + Strings.Format(strTemp2, "00")));
                        }
                        else
                        {
                            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                            modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIUnits1[Index].Text)));
                        }
                    }
                    else
                    {
                        // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + (Index + 1), 0);
                    }
                }
            }
            else
            {
                // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
                modDataTypes.Statics.OUT.Set_Fields("OIUNITS" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIUnits1[Index].Text + "")));
            }
        }

        private void txtMROIUnits1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROIUnits1.GetIndex((FCTextBox)sender);
            txtMROIUnits1_Validating(index, sender, e);
        }

        private void txtMROIGradeCd1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROIGradeCd1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIGradeCd1.GetIndex((FCTextBox)sender);
            txtMROIGradeCd1_Enter(index, sender, e);
        }

        private void txtMROIGradeCd1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROIGradeCd1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROIGradeCd1[FCConvert.ToInt16(Index + 1)].Focus();
                    // Else
                    // MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIGradeCd1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIGradeCd1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                //modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name, Conversion.Val(txtMROIType1[Index].Text));
                modHelpFiles.HelpFiles("txtMROIGradeCd1", Conversion.Val(txtMROIType1[Index].Text));
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROIGradeCd1[Index].Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIGradeCd1[Index].SelectionStart == txtMROIGradeCd1[Index].MaxLength)
            {
                if (txtMROIGradePct1[Index].Enabled)
                    txtMROIGradePct1[Index].Focus();
            }
        }

        private void txtMROIGradeCd1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIGradeCd1.GetIndex((FCTextBox)sender);
            txtMROIGradeCd1_KeyUp(index, sender, e);
        }

        private void txtMROIGradeCd1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIGRADECD" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIGradeCd1[Index].Text + ""))));
        }

        private void txtMROIGradeCd1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIGradeCd1.GetIndex((FCTextBox)sender);
            txtMROIGradeCd1_Leave(index, sender, e);
        }

        private void txtMROIGradePct1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROIGradePct1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIGradePct1.GetIndex((FCTextBox)sender);
            txtMROIGradePct1_Enter(index, sender, e);
        }

        private void txtMROIGradePct1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROIGradePct1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROIGradePct1[FCConvert.ToInt16(Index + 1)].Focus();
                    // Else
                    // MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIGradePct1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIGradePct1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("OUTBUILDING", -1);
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROIGradePct1[Index].Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIGradePct1[Index].SelectionStart == txtMROIGradePct1[Index].MaxLength)
            {
                if (txtMROICond1[Index].Enabled)
                    txtMROICond1[Index].Focus();
            }
        }

        private void txtMROIGradePct1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIGradePct1.GetIndex((FCTextBox)sender);
            txtMROIGradePct1_KeyUp(index, sender, e);
        }

        private void txtMROIGradePct1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIGRADEPCT" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIGradePct1[Index].Text + ""))));
        }

        private void txtMROIGradePct1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIGradePct1.GetIndex((FCTextBox)sender);
            txtMROIGradePct1_Leave(index, sender, e);
        }

        public void txtMROIGradePct1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIGRADEPCT" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIGradePct1[Index].Text + ""))));
        }

        private void txtMROIGradePct1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROIGradePct1.GetIndex((FCTextBox)sender);
            txtMROIGradePct1_Validating(index, sender, e);
        }

        private void txtMROICond1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROICond1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROICond1.GetIndex((FCTextBox)sender);
            txtMROICond1_Enter(index, sender, e);
        }

        private void txtMROICond1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROICond1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROICond1[FCConvert.ToInt16(Index + 1)].Focus();
                    // Else
                    // MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROICond1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROICond1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                if (Conversion.Val(txtMROIType1[Index].Text) > 699)
                {
                    modHelpFiles.HelpFiles_2("OUTBUILDING", -3);
                }
                else
                {
                    //modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                    modHelpFiles.HelpFiles("txtMROICond1");
                }
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROICond1[Index].Text = FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(modGNBas.Statics.response))));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROICond1[Index].SelectionStart == txtMROICond1[Index].MaxLength)
            {
                if (txtMROIPctPhys1[Index].Enabled)
                    txtMROIPctPhys1[Index].Focus();
            }
        }

        private void txtMROICond1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROICond1.GetIndex((FCTextBox)sender);
            txtMROICond1_KeyUp(index, sender, e);
        }

        private void txtMROICond1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OICOND" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROICond1[Index].Text + ""))));
        }

        private void txtMROICond1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROICond1.GetIndex((FCTextBox)sender);
            txtMROICond1_Leave(index, sender, e);
        }

        public void txtMROICond1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OICOND" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROICond1[Index].Text + "")));
        }

        private void txtMROICond1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROICond1.GetIndex((FCTextBox)sender);
            txtMROICond1_Validating(index, sender, e);
        }

        private void txtMROIPctPhys1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROIPctPhys1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIPctPhys1.GetIndex((FCTextBox)sender);
            txtMROIPctPhys1_Enter(index, sender, e);
        }

        private void txtMROIPctPhys1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROIPctPhys1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROIPctPhys1[FCConvert.ToInt16(Index + 1)].Focus();
                    // Else
                    // MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIPctPhys1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIPctPhys1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                //modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                modHelpFiles.HelpFiles("txtMROIPctPhys1");
                //Application.DoEvents();
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROIPctPhys1[Index].Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIPctPhys1[Index].SelectionStart == txtMROIPctPhys1[Index].MaxLength)
            {
                if (txtMROIPctFunct1[Index].Enabled)
                    txtMROIPctFunct1[Index].Focus();
            }
        }

        private void txtMROIPctPhys1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIPctPhys1.GetIndex((FCTextBox)sender);
            txtMROIPctPhys1_KeyUp(index, sender, e);
        }

        private void txtMROIPctPhys1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIPCTPHYS" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIPctPhys1[Index].Text + ""))));
        }

        private void txtMROIPctPhys1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIPctPhys1.GetIndex((FCTextBox)sender);
            txtMROIPctPhys1_Leave(index, sender, e);
        }

        public void txtMROIPctPhys1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIPCTPHYS" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIPctPhys1[Index].Text + "")));
        }

        private void txtMROIPctPhys1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROIPctPhys1.GetIndex((FCTextBox)sender);
            txtMROIPctPhys1_Validating(index, sender, e);
        }

        private void txtMROIPctFunct1_Enter(short Index, object sender, System.EventArgs e)
        {
            selecteverything();
            if (Conversion.Val(txtMROIType1[Index].Text + "") == 0)
            {
                if (txtMROIType1[Index].Enabled)
                    txtMROIType1[Index].Focus();
            }
        }

        private void txtMROIPctFunct1_Enter(object sender, System.EventArgs e)
        {
            short index = txtMROIPctFunct1.GetIndex((FCTextBox)sender);
            txtMROIPctFunct1_Enter(index, sender, e);
        }

        private void txtMROIPctFunct1_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (Index < 9)
                {
                    // If (Trim(txtMROIType1(Index + 1).Text & "") <> "") Then
                    if (txtMROIPctFunct1[FCConvert.ToInt16(Index + 1)].Enabled)
                        txtMROIPctFunct1[FCConvert.ToInt16(Index + 1)].Focus();
                    // Else
                    // MaskEdBox3.SetFocus
                    // End If
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (Index > 0)
                {
                    if (txtMROIPctFunct1[FCConvert.ToInt16(Index - 1)].Enabled)
                        txtMROIPctFunct1[FCConvert.ToInt16(Index - 1)].Focus();
                }
                else
                {
                    // If MaskEdBox2.Enabled Then MaskEdBox2.SetFocus
                }
            }
            else if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                modHelpFiles.Statics.Helping = true;
                //modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
                modHelpFiles.HelpFiles("txtMROIPctFunct1");
                if (Conversion.Val(modGNBas.Statics.response) != 0)
                    txtMROIPctFunct1[Index].Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
                frmHelp.InstancePtr.Unload();
            }
            else if (txtMROIPctFunct1[Index].SelectionStart == txtMROIPctFunct1[Index].MaxLength)
            {
                if (Index < 9)
                {
                    txtMROIType1[FCConvert.ToInt16(Index + 1)].Focus();
                }
                else
                {
                    txtMROIType1[0].Focus();
                }
            }
        }

        private void txtMROIPctFunct1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            short index = txtMROIPctFunct1.GetIndex((FCTextBox)sender);
            txtMROIPctFunct1_KeyUp(index, sender, e);
        }

        private void txtMROIPctFunct1_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIPCTFUNCT" + (Index + 1), FCConvert.ToString(Conversion.Val(Strings.Trim(txtMROIPctFunct1[Index].Text + ""))));
        }

        private void txtMROIPctFunct1_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROIPctFunct1.GetIndex((FCTextBox)sender);
            txtMROIPctFunct1_Leave(index, sender, e);
        }

        public void txtMROIPctFunct1_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIPCTFUNCT" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROIPctFunct1[Index].Text + "")));
        }

        private void txtMROIPctFunct1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROIPctFunct1.GetIndex((FCTextBox)sender);
            txtMROIPctFunct1_Validating(index, sender, e);
        }

        private void txtMROISoundValue_Leave(short Index, object sender, System.EventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIsoundvalue" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROISoundValue[Index].Text + "")));
        }

        private void txtMROISoundValue_Leave(object sender, System.EventArgs e)
        {
            short index = txtMROISoundValue.GetIndex((FCTextBox)sender);
            txtMROISoundValue_Leave(index, sender, e);
        }

        private void txtMROISoundValue_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
        {
            // FC:FINAL:VGE - #i1051 Surrounding Index + 1 with parenthesis to match VB6 operations order
            modDataTypes.Statics.OUT.Set_Fields("OIsoundvalue" + (Index + 1), FCConvert.ToString(Conversion.Val(txtMROISoundValue[Index].Text + "")));
        }

        private void txtMROISoundValue_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            short index = txtMROISoundValue.GetIndex((FCTextBox)sender);
            txtMROISoundValue_Validating(index, sender, e);
        }

        private void Grid1_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            switch (Grid1.Col)
            {
                case colOCCCode:
                case colSF:
                case colActRent:
                case colMarketSFYear:
                    {
                        if (Grid1.Col == colSF)
                        {
                            Grid1.TextMatrix(Grid1.Row, Grid1.Col, Strings.Format(Grid1.TextMatrix(Grid1.Row, Grid1.Col), "#,###,##0"));
                        }
                        else if (Grid1.Col == colActRent)
                        {
                            Grid1.TextMatrix(Grid1.Row, Grid1.Col, Strings.Format(Grid1.TextMatrix(Grid1.Row, Grid1.Col), "###0.00"));
                        }
                        else if (Grid1.Col == colMarketSFYear)
                        {
                            Grid1.TextMatrix(Grid1.Row, Grid1.Col, Strings.Format(Grid1.TextMatrix(Grid1.Row, Grid1.Col), "###0.00"));
                        }
                        else if (Grid1.Col == colOCCCode)
                        {
                        }
                        ReCalcRow(Grid1.Row);
                        break;
                    }
            }
            //end switch
        }

        private void Grid1_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                        if (!modHelpFiles.Statics.Helping)
                        {
                            modHelpFiles.Statics.Helping = true;
                            KeyCode = 0;
                            // call help screen
                            // 1 means occupancy codes
                            modHelpFiles.HelpFiles_2("INCOMEAPPROACH", 1);
                            if (Conversion.Val(modGNBas.Statics.response) != 0)
                                Grid1.TextMatrix(Grid1.Row, 0, FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)))));
                            frmHelp.InstancePtr.Unload();
                            return;
                        }
                        break;
                    }
            }
            //end switch
        }

        private void Grid1_RowColChange(object sender, System.EventArgs e)
        {
            int Col;
            int Row = 0;
            Grid1.Editable = FCGrid.EditableSettings.flexEDNone;
            Col = Grid1.Col;
            Row = Grid1.Row;
            if (Row < 2)
                return;
            //FC:FINAL:MSH - i.issue #1050: for last row in grid modified will be canceled with this comparing
            //if (Row > Grid1.Rows - 1) return;
            switch (Col)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 5:
                    {
                        Grid1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                        break;
                    }
                default:
                    {
                        Grid1.Editable = FCGrid.EditableSettings.flexEDNone;
                        break;
                    }
            }
            //end switch
        }

        private void GridExpenses_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            double dblPercGross = 0;
            double dblPerSF = 0;
            // vbPorter upgrade warning: lngAmount As int	OnWrite(double, int)
            int lngAmount = 0;
            int lngGross = 0;
            int lngSF = 0;
            if (Conversion.Val(GridValues.TextMatrix(0, 2)) > 0)
            {
                lngGross = FCConvert.ToInt32(FCConvert.ToDouble(GridValues.TextMatrix(0, 2)));
            }
            else
            {
                lngGross = 0;
            }
            if (Conversion.Val(txtTotalSF.Text) > 0)
            {
                lngSF = FCConvert.ToInt32(FCConvert.ToDouble(txtTotalSF.Text));
            }
            else
            {
                lngSF = 0;
            }
            switch (GridExpenses.Col)
            {
                case colGross:
                    {
                        dblPercGross = modGlobalRoutines.Round(Conversion.Val(GridExpenses.TextMatrix(GridExpenses.Row, GridExpenses.Col)), 1) / 100;
                        if (lngSF > 0)
                        {
                            dblPerSF = modGlobalRoutines.Round(dblPercGross * lngGross / lngSF, 2);
                        }
                        else
                        {
                            dblPerSF = 0;
                        }
                        lngAmount = FCConvert.ToInt32(dblPercGross * lngGross);
                        dblPercGross *= 100;
                        // for display purposes
                        break;
                    }
                case colPerSF:
                    {
                        dblPerSF = modGlobalRoutines.Round(Conversion.Val(GridExpenses.TextMatrix(GridExpenses.Row, GridExpenses.Col)), 2);
                        lngAmount = FCConvert.ToInt32(dblPerSF * lngSF);
                        if (lngGross > 0)
                        {
                            //FC:FINAL:MSH - i.issue #1224: invalid cast exception
                            //dblPercGross = modGlobalRoutines.Round(FCConvert.ToDouble(lngAmount / lngGross * 100, 1);
                            dblPercGross = modGlobalRoutines.Round(FCConvert.ToDouble(lngAmount) / lngGross * 100, 1);
                        }
                        else
                        {
                            dblPercGross = 0;
                        }
                        break;
                    }
                case colAmount:
                    {
                        lngAmount = FCConvert.ToInt32(Math.Round(Conversion.Val(GridExpenses.TextMatrix(GridExpenses.Row, GridExpenses.Col))));
                        if (lngSF > 0)
                        {
                            //FC:FINAL:MSH - i.issue #1224: invalid cast exception
                            //dblPerSF = modGlobalRoutines.Round(FCConvert.ToDouble(lngAmount / lngSF, 2);
                            dblPerSF = modGlobalRoutines.Round(FCConvert.ToDouble(lngAmount) / lngSF, 2);
                        }
                        else
                        {
                            dblPerSF = 0;
                        }
                        if (lngGross > 0)
                        {
                            //FC:FINAL:MSH - i.issue #1224: invalid cast exception
                            //dblPercGross = modGlobalRoutines.Round(FCConvert.ToDouble(lngAmount / lngGross * 100, 1);
                            dblPercGross = modGlobalRoutines.Round(FCConvert.ToDouble(lngAmount) / lngGross * 100, 1);
                        }
                        else
                        {
                            dblPercGross = 0;
                        }
                        break;
                    }
            }
            //end switch
            GridExpenses.TextMatrix(GridExpenses.Row, colGross, Strings.Format(dblPercGross, "##0.0"));
            GridExpenses.TextMatrix(GridExpenses.Row, colPerSF, Strings.Format(dblPerSF, "###0.00"));
            GridExpenses.TextMatrix(GridExpenses.Row, colAmount, Strings.Format(lngAmount, "#,###,##0"));
            ReCalcTotal();
        }

        private void GridExpenses_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.F9:
                    {
                        //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                        if (!modHelpFiles.Statics.Helping)
                        {
                            modHelpFiles.Statics.Helping = true;
                            KeyCode = 0;
                            // call help screen
                            // 2 means expense codes
                            modHelpFiles.HelpFiles_2("INCOMEAPPROACH", 2);
                            if (Conversion.Val(modGNBas.Statics.response) != 0)
                                GridExpenses.TextMatrix(GridExpenses.Row, 0, FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(modGNBas.Statics.response)))));
                            frmHelp.InstancePtr.Unload();
                        }
                        return;
                    }
            }
            //end switch
        }

        private void GridValues_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            double dblVacancy = 0;
            int lngMarket = 0;
            double dblOVRate = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            switch (GridValues.Col)
            {
                case 1:
                    {
                        switch (GridValues.Row)
                        {
                            case 1:
                                {
                                    // vacancy
                                    dblVacancy = modGlobalRoutines.Round(Conversion.Val(GridValues.TextMatrix(1, 1)), 2);
                                    GridValues.TextMatrix(1, 1, Strings.Format(dblVacancy, "##0.00"));
                                    if (Conversion.Val(GridValues.TextMatrix(0, 2)) > 0)
                                    {
                                        lngMarket = FCConvert.ToInt32(FCConvert.ToDouble(GridValues.TextMatrix(0, 2)));
                                    }
                                    else
                                    {
                                        lngMarket = 0;
                                    }
                                    dblVacancy /= 100;
                                    GridValues.TextMatrix(1, 2, Strings.Format(modGlobalRoutines.Round((lngMarket * dblVacancy), 0), "#,###,##0"));
                                    break;
                                }
                            case 5:
                                {
                                    // overall rate
                                    dblOVRate = modGlobalRoutines.Round(Conversion.Val(GridValues.TextMatrix(5, 1)), 2);
                                    GridValues.TextMatrix(5, 1, Strings.Format(dblOVRate, "#0.00"));
                                    if (dblOVRate > 0)
                                    {
                                        GridValues.TextMatrix(5, 2, GridValues.TextMatrix(5, 1));
                                    }
                                    else
                                    {
                                        if (Grid1.Rows > 2)
                                        {
                                            if (Conversion.Val(Grid1.TextMatrix(2, colOCCCode)) > 0)
                                            {
                                                clsLoad.OpenRecordset("select * from occupancycodes where code = " + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(2, colOCCCode))), modGlobalVariables.strREDatabase);
                                                if (!clsLoad.EndOfFile())
                                                {
                                                    GridValues.TextMatrix(5, 2, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("rate"))), "##0.00"));
                                                }
                                                else
                                                {
                                                    GridValues.TextMatrix(5, 2, "0.00");
                                                }
                                            }
                                            else
                                            {
                                                GridValues.TextMatrix(5, 2, "0.00");
                                            }
                                        }
                                        else
                                        {
                                            GridValues.TextMatrix(5, 2, "0.00");
                                        }
                                        dblOVRate = Conversion.Val(GridValues.TextMatrix(5, 2));
                                    }
                                    GridValues.TextMatrix(7, 2, Strings.Format(dblOVRate + FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(GridValues.TextMatrix(6, 2)))), "##0.00"));
                                    break;
                                }
                        }
                        //end switch
                        break;
                    }
            }
            //end switch
            ReCalcTotal();
        }

        private void GridValues_RowColChange(object sender, System.EventArgs e)
        {
            int Col;
            int Row;
            Col = GridValues.MouseCol;
            Row = GridValues.MouseRow;
            switch (Col)
            {
                case 1:
                    {
                        switch (Row)
                        {
                            case 1:
                            case 5:
                                {
                                    GridValues.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                                    break;
                                }
                            default:
                                {
                                    GridValues.Editable = FCGrid.EditableSettings.flexEDNone;
                                    break;
                                }
                        }
                        //end switch
                        break;
                    }
                default:
                    {
                        GridValues.Editable = FCGrid.EditableSettings.flexEDNone;
                        break;
                    }
            }
            //end switch
        }

        private void cmdPrevPicture_Click(object sender, System.EventArgs e)
        {
            if (pictureViewModel.Pictures.HasCurrentPicture())
            {
                pictureViewModel.Pictures.CurrentPicture().Description = txtPicDesc.Text;
                pictureViewModel.SaveCurrentPicture();
                if (!pictureViewModel.Pictures.OnFirstPicture())
                {
                    var pictureRecord = pictureViewModel.Pictures.PreviousPicture();
                    Auto_Size_Photo();
                    imgPicture.Tag = pictureRecord.DocumentIdentifier;
                }
            }
            DisableEnablePictureNavigation();
        }

        private void DisableEnablePictureNavigation()
        {
            if (pictureViewModel == null)
            {
                cmdPrevPicture.Enabled = false;
                cmdNextPicture.Enabled = false;
                return;
            }

            if (pictureViewModel.Pictures.HasCurrentPicture())
            {
                if (!pictureViewModel.Pictures.OnLastPicture())
                {
                    cmdNextPicture.Enabled = true;
                }
                else
                {
                    cmdNextPicture.Enabled = false;
                }

                if (!pictureViewModel.Pictures.OnFirstPicture())
                {
                    cmdPrevPicture.Enabled = true;
                }
                else
                {
                    cmdPrevPicture.Enabled = false;
                }
            }
            else
            {
                cmdPrevPicture.Enabled = false;
                cmdNextPicture.Enabled = false;
            }
        }
        private void cmdNextPicture_Click(object sender, System.EventArgs e)
        {
            if (pictureViewModel.Pictures.HasCurrentPicture())
            {
                pictureViewModel.Pictures.CurrentPicture().Description = txtPicDesc.Text;
                pictureViewModel.SaveCurrentPicture();
                if (!pictureViewModel.Pictures.OnLastPicture())
               
                {
                    var pictureRecord = pictureViewModel.Pictures.NextPicture();
                    Auto_Size_Photo();
                    imgPicture.Tag = pictureRecord.DocumentIdentifier;
                }
            }

            DisableEnablePictureNavigation();
        }

        private void Toolbar1_ButtonClick(object sender, fecherFoundation.FCToolBarButtonClickEventArgs e)
        {
            ToolBarButton Button = e.Button;
            string vbPorterVar = Button.Name;
            if (vbPorterVar == "BTNPRINTPICTURE")
            {
                PrintPreviewPicture();
            }
            else if (vbPorterVar == "BTNPICADDPIC")
            {
                mnuAddPicture_Click();
            }
            else if (vbPorterVar == "BTNPICDELETEPIC")
            {
                mnuDeletePicture_Click();
            }
        }

        private void SetupGridToolTip_2(string strType)
        {
            SetupGridToolTip(ref strType);
        }

        private void SetupGridToolTip(ref string strType)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            int lngRow;
            GridToolTip.Tag = "";
            if (strType == "State")
            {
                GridToolTip.Rows = 1;
                GridToolTip.Cols = 2;
                GridToolTip.Visible = true;
                GridToolTip.FixedCols = 0;
                GridToolTip.Editable = FCGrid.EditableSettings.flexEDNone;
                GridToolTip.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                GridToolTip.ScrollTrack = true;
                GridToolTip.ExtendLastCol = true;
                GridToolTip.TextMatrix(0, 0, "State / Province");
                GridToolTip.TextMatrix(0, 1, "Abbreviation");
                GridToolTip.ColWidth(0, FCConvert.ToInt32(0.68 * GridToolTip.Width * FCScreen.TwipsPerPixelX));
                GridToolTip.Height = (GridToolTip.RowHeight(0) * 5 + 30) / FCScreen.TwipsPerPixelY;
                GridToolTip.Tag = strType;
                rsLoad.OpenRecordset("select * from tblstates order by description", modGlobalVariables.Statics.strGNDatabase);
                while (!rsLoad.EndOfFile())
                {
                    GridToolTip.Rows += 1;
                    lngRow = GridToolTip.Rows - 1;
                    GridToolTip.TextMatrix(lngRow, 0, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
                    GridToolTip.TextMatrix(lngRow, 1, FCConvert.ToString(rsLoad.Get_Fields_String("StateCode")));
                    rsLoad.MoveNext();
                }
                GridToolTip.Top = ToolTip.TextBottom + 30 / FCScreen.TwipsPerPixelY;
                ToolTip.MinHeight = GridToolTip.Top + GridToolTip.Height + 120 / FCScreen.TwipsPerPixelY;
            }
            else if ((strType == "Open1") || (strType == "Open2"))
            {
                // vbPorter upgrade warning: intRange As string	OnWriteFCConvert.ToInt16(
                string intRange = "";
                if (strType == "Open1")
                {
                    intRange = FCConvert.ToString(1330);
                }
                else
                {
                    intRange = FCConvert.ToString(1340);
                }
                modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt32(intRange) + 1);
                if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
                {
                    GridToolTip.Visible = false;
                    return;
                }
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    rsLoad.OpenRecordset("select * from costrecord where crecordnumber between " + FCConvert.ToString(FCConvert.ToInt32(intRange) + 1) + " and " + FCConvert.ToString(FCConvert.ToInt32(intRange) + 9) + " order by crecordnumber", modGlobalVariables.strREDatabase);
                }
                else
                {
                    rsLoad.OpenRecordset("select * from costrecord where crecordnumber between " + FCConvert.ToString(FCConvert.ToInt32(intRange) + 1) + " and " + FCConvert.ToString(FCConvert.ToInt32(intRange) + 9) + " and townnumber = " + FCConvert.ToString(Conversion.Val(gridTranCode.TextMatrix(0, 0))) + " order by crecordnumber", modGlobalVariables.strREDatabase);
                }
                GridToolTip.Visible = true;
                if (rsLoad.EndOfFile())
                {
                    GridToolTip.Visible = false;
                    return;
                }
                GridToolTip.Tag = strType;
                GridToolTip.Rows = 1;
                GridToolTip.Cols = 3;
                GridToolTip.FixedCols = 0;
                GridToolTip.Editable = FCGrid.EditableSettings.flexEDNone;
                GridToolTip.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                GridToolTip.ExtendLastCol = true;
                GridToolTip.TextMatrix(0, 0, "Code");
                GridToolTip.TextMatrix(0, 1, "Description");
                GridToolTip.TextMatrix(0, 2, "Factor");
                GridToolTip.ColWidth(0, FCConvert.ToInt32(0.2 * GridToolTip.Width * FCScreen.TwipsPerPixelX));
                GridToolTip.ColWidth(1, FCConvert.ToInt32(0.6 * GridToolTip.Width * FCScreen.TwipsPerPixelX));
                GridToolTip.ScrollTrack = true;
                GridToolTip.Height = (GridToolTip.RowHeight(0) * 5 + 30) / FCScreen.TwipsPerPixelY;
                while (!rsLoad.EndOfFile())
                {
                    if (Conversion.Val(rsLoad.Get_Fields_String("cunit")) != 99999999)
                    {
                        GridToolTip.Rows += 1;
                        lngRow = GridToolTip.Rows - 1;
                        GridToolTip.TextMatrix(lngRow, 0, rsLoad.Get_Fields_Int32("crecordnumber") - FCConvert.ToInt32(intRange));
                        GridToolTip.TextMatrix(lngRow, 1, rsLoad.Get_Fields_String("cldesc"));
                        GridToolTip.TextMatrix(lngRow, 2, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("cunit")) / 100));
                    }
                    rsLoad.MoveNext();
                }
                GridToolTip.Top = ToolTip.TextBottom + 30 / FCScreen.TwipsPerPixelY;
                ToolTip.MinHeight = GridToolTip.Top + GridToolTip.Height + 120 / FCScreen.TwipsPerPixelY;
            }
        }

        private void FillLandCombo()
        {
            string strTemp = "";
            int lngTownNumber = 0;
            clsDRWrapper rsLoad = new clsDRWrapper();
            int intZone;
            int intSecZone;
            int intNeighborhood;
            // vbPorter upgrade warning: intSchedule As int	OnWrite(short, double)
            int intSchedule;
            double dblAmount = 0;
            string strShort = "";
            if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                lngTownNumber = 0;
            }
            else
            {
                lngTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                if (lngTownNumber == 0)
                    lngTownNumber = 1;
            }
            intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(GridZone.TextMatrix(0, 0))));
            intSecZone = FCConvert.ToInt32(Math.Round(Conversion.Val(GridSecZone.TextMatrix(0, 0))));
            intNeighborhood = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeighborhood.TextMatrix(0, 0))));
            intSchedule = 0;
            if (intZone > 0)
            {
                if (!(chkZoneOverride.CheckState == Wisej.Web.CheckState.Checked))
                {
                    intSchedule = FCConvert.ToInt32(modGlobalVariables.Statics.LandTRec.Schedule(ref intNeighborhood, ref intZone));
                }
                else
                {
                    intSchedule = FCConvert.ToInt32(modGlobalVariables.Statics.LandTRec.Schedule(ref intNeighborhood, ref intSecZone));
                }
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    rsLoad.OpenRecordset("select * from landschedule where schedule = " + FCConvert.ToString(intSchedule) + " and code > 0 order by code", modGlobalVariables.strREDatabase);
                }
                else
                {
                    modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
                    rsLoad.OpenRecordset("select * from landschedule where townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " and schedule = " + FCConvert.ToString(intSchedule) + " and code > 0 order by code", modGlobalVariables.strREDatabase);
                }
                if (rsLoad.EndOfFile())
                    intSchedule = 0;
            }
            modGlobalVariables.Statics.LandTypes.Init();
            if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                strTemp = "#0;No Land" + "\t" + "" + "|";  
            }
            else
            {
                strTemp = "#0;0 No Land" + "\t" + "" + "|";  
            }
            while (!modGlobalVariables.Statics.LandTypes.EndOfLandTypes())
            {
                if (modGlobalVariables.Statics.CustomizedInfo.ShowShortDesc)
                {
                    strShort = "\t" + modGlobalVariables.Statics.LandTypes.ShortDescription;
                }
                else
                {
                    strShort = "";
                }
                if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
                {
                    strTemp += "#" + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code) + ";" + modGlobalVariables.Statics.LandTypes.Description + strShort;
                }
                else
                {
                    strTemp += "#" + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code) + ";" + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code) + " " + modGlobalVariables.Statics.LandTypes.Description + strShort;
                }
                switch (modGlobalVariables.Statics.LandTypes.UnitsType)
                {
                    case modREConstants.CNSTLANDTYPEFRONTFOOT:
                        {
                            strTemp += "\t" + "Front Foot";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPESQUAREFEET:
                        {
                            strTemp += "\t" + "Square Feet";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
                        {
                            strTemp += "\t" + "Fractional Acreage";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEACRES:
                        {
                            strTemp += "\t" + "Acres";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPESITE:
                        {
                            strTemp += "\t" + "Site";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
                        {
                            strTemp += "\t" + "";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
                        {
                            strTemp += "\t" + "Improvements";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPELINEARFEET:
                        {
                            strTemp += "\t" + "Linear Feet";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
                        {
                            strTemp += "\t" + "Front Foot - Width";
                            break;
                        }
                    case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
                        {
                            strTemp += "\t" + "Linear Feet - Width";
                            break;
                        }
                }
                //end switch
                dblAmount = 0;
                if (intSchedule > 0)
                {
                    // If rsLoad.FindFirstRecord("code", LandTypes.Code) Then
                    if (rsLoad.FindFirst("code = " + FCConvert.ToString(modGlobalVariables.Statics.LandTypes.Code)))
                    {
                        // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                        dblAmount = Conversion.Val(rsLoad.Get_Fields("amount"));
                        strTemp += "\t" + Strings.Format(dblAmount, "#,###,##0.00");
                    }
                }
                strTemp += "|";
                modGlobalVariables.Statics.LandTypes.MoveNext();
            }
            if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                strTemp += "#99;Extra Influence";
            }
            else
            {
                strTemp += "#99;99 Extra Influence";
            }
            GridLand.ColComboList(0, strTemp);
        }

        public void condenseoutbuilding()
        {
            int forindex;
            int innerindex;
            for (forindex = 0; forindex <= 8; forindex++)
            {
                if (txtMROIType1[FCConvert.ToInt16(forindex)].Text == "")
                {
                    for (innerindex = forindex; innerindex <= 8; innerindex++)
                    {
                        txtMROIType1[FCConvert.ToInt16(innerindex)].Text = txtMROIType1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OITYPE" & Val(innerindex + 1)) = Trim(txtMROIType1(innerindex).Text & "")
                        txtMROIYear1[FCConvert.ToInt16(innerindex)].Text = txtMROIYear1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OIYEAR" & (innerindex + 1)) = Trim(txtMROIYear1(innerindex).Text & "")
                        txtMROIUnits1[FCConvert.ToInt16(innerindex)].Text = txtMROIUnits1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OIUNITS" & (innerindex + 1)) = Trim(txtMROIUnits1(innerindex).Text & "")
                        txtMROIGradeCd1[FCConvert.ToInt16(innerindex)].Text = txtMROIGradeCd1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OIGRADECD" & (innerindex + 1)) = Trim(txtMROIGradeCd1(innerindex).Text & "")
                        txtMROIGradePct1[FCConvert.ToInt16(innerindex)].Text = txtMROIGradePct1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OIGRADEPCT" & (innerindex + 1)) = Trim(txtMROIGradePct1(innerindex).Text & "")
                        txtMROICond1[FCConvert.ToInt16(innerindex)].Text = txtMROICond1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OICOND" & (innerindex + 1)) = Trim(txtMROICond1(innerindex).Text & "")
                        txtMROIPctFunct1[FCConvert.ToInt16(innerindex)].Text = txtMROIPctFunct1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OIPCTFUNCT" & (innerindex + 1)) = Trim(txtMROIPctFunct1(innerindex).Text & "")
                        txtMROIPctPhys1[FCConvert.ToInt16(innerindex)].Text = txtMROIPctPhys1[FCConvert.ToInt16(innerindex + 1)].Text;
                        // OUT.Fields("OIPCTPHYS" & (innerindex + 1)) = Trim(txtMROIPctPhys1(innerindex).Text & "")
                        chkSound[FCConvert.ToInt16(innerindex)].CheckState = chkSound[FCConvert.ToInt16(innerindex + 1)].CheckState;
                        txtMROISoundValue[FCConvert.ToInt16(innerindex)].Text = txtMROISoundValue[FCConvert.ToInt16(innerindex + 1)].Text;
                        if (innerindex == 8)
                        {
                            txtMROIType1[9].Text = "";
                            txtMROIYear1[9].Text = "";
                            txtMROIUnits1[9].Text = "";
                            txtMROIGradeCd1[9].Text = "";
                            txtMROIGradePct1[9].Text = "";
                            txtMROICond1[9].Text = "";
                            txtMROIPctFunct1[9].Text = "";
                            txtMROIPctPhys1[9].Text = "";
                            chkSound[9].CheckState = Wisej.Web.CheckState.Unchecked;
                            txtMROISoundValue[9].Text = "";
                        }
                    }
                    // innerindex
                }
            }
            // forindex
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As double
        private double ConvertStringtoValue_2(string strValue)
        {
            return ConvertStringtoValue(ref strValue);
        }

        private double ConvertStringtoValue(ref string strValue)
        {
            double ConvertStringtoValue = 0;
            string strTemp = "";
            string strTemp2 = "";
            int intpos = 0;
            ConvertStringtoValue = Conversion.Val(strValue);
            if (Conversion.Val(strValue) > 0)
            {
                if (Strings.InStr(1, strValue, "*", CompareConstants.vbTextCompare) > 0)
                {
                    intpos = Strings.InStr(1, strValue, "*", CompareConstants.vbTextCompare);
                    strTemp = Strings.Mid(strValue, 1, intpos - 1);
                    strTemp2 = Strings.Mid(strValue, intpos + 1);
                    ConvertStringtoValue = Conversion.Val(strTemp) * Conversion.Val(strTemp2);
                }
            }
            return ConvertStringtoValue;
        }

        private void ShowCurrentSketch()
        {
            cmdNextSketch.Enabled = false;
            cmdPrevSketch.Enabled = false;
            if (sketchesViewModel == null)
            {
                imSketch.Image = null;
                return;
            }

            if (!sketchesViewModel.Sketches.HasCurrentSketch())
            {
                imSketch.Image = null;
                return;
            }

            if (!sketchesViewModel.Sketches.OnFirstSketch())
            {
                cmdPrevSketch.Enabled = true;
            }

            if (!sketchesViewModel.Sketches.OnLastSketch())
            {
                cmdNextSketch.Enabled = true;
            }
            var sketch = sketchesViewModel.Sketches.CurrentSketch();
            imgSketchLastModified = sketch.DateUpdated;
            var imageData = sketchesViewModel.GetCurrentSketchImage();
            if (imageData != null)
            {
                imgPicture.Image = FCUtils.PictureFromBytes(imageData);
                imSketch.Image = FCUtils.PictureFromBytes(imageData);

                imSketch.Visible = true;
                ResizeSketchImage();             
            }
        }

        public void Auto_Size_Photo()
        {
            try
            {
                var pictureRecord = pictureViewModel.Pictures.CurrentPicture();
                if (pictureRecord != null)
                {
                    if (string.IsNullOrWhiteSpace(pictureRecord.Description))
                    {
                        txtPicDesc.Text = "Enter Comment or Description Here";
                    }
                    else
                    {
                        txtPicDesc.Text = pictureRecord.Description;
                    }

                    var imageData = pictureViewModel.GetCurrentPicture();
                    if (imageData != null)
                    {
                        imgPicture.Image = FCUtils.PictureFromBytes(imageData);


                        imgPicture.Visible = true;

                        ResizePic();
                    }
                }
                
                return;
            }
            catch (Exception ex)
            {
                // PicError:
                MessageBox.Show("Error no. " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Auto Size Photo", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Information.Err(ex).Clear();
                /*? On Error GoTo 0 */
            }
        }

        private void PrintPreviewPicture(object sender, EventArgs e)
        {
            PrintPreviewPicture();
        }

        private void PrintPreviewPicture()
        {
            //string strPath;
            modRegistry.SaveRegistryKey("PicturePrintSize", FCConvert.ToString(cmbPrintSize.SelectedIndex), "RE");
            //strPath = FCConvert.ToString(framPicture.Tag);
            var documentIdentifier = pictureViewModel.Pictures.CurrentPicture()?.DocumentIdentifier ?? Guid.Empty;
            rptPrintPicture.InstancePtr.Init( documentIdentifier,  modGlobalVariables.Statics.gintLastAccountNumber, cmbPrintSize.SelectedIndex);
        }

        private void AddAPicture()
        {
            string strPerm = "";
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strTemp = "";
            string dirTemp = "";
            int intPicNum = 0;
            int intTemp = 0;
            intPicNum = pictureViewModel.Pictures.Count() + 1;
            var ofd = new StreamOpenFileDialog();
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            var files = ofd.UploadedFiles;
            var fileName = ofd.FileName;
            MemoryStream mem = new MemoryStream();
            files[0].InputStream.CopyTo(mem, 1024);

            var bytes = mem.ToArray();
            var resizedBytes = FCUtils.ResizeImageAsJpeg(bytes, 1920, 1080);
            // modGNBas.Statics.DocumentLocationName = fileName;
            fileName = Path.GetFileName(fileName);
            frmPictureChange.InstancePtr.SetImage(resizedBytes);
            frmPictureChange.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            if (FCConvert.ToString(modGNBas.Statics.response) == "YES")
            {
                //var bytes = File.ReadAllBytes(fileName);
                var documentIdentifier = Guid.NewGuid();
                pictureViewModel.AddNewPicture(txtPicDesc.Text, intPicNum,documentIdentifier, fileName, resizedBytes,@"image/jpeg");

                if (!pictureViewModel.Pictures.OnFirstPicture())
                {
                    cmdPrevPicture.Enabled = true;
                }
                //imgPicture.Image = FCUtils.LoadPicture(modGNBas.Statics.DocumentLocationName);
                cmdNextPicture.Enabled = false;
                Auto_Size_Photo();
                imgPicture.Refresh();
                imgPicture.Tag = documentIdentifier;
            }
        }
        private void mnuAddPicture_Click(object sender, System.EventArgs e)
        {
            AddAPicture();
        }

        public void mnuAddPicture_Click()
        {
            mnuAddPicture_Click(mnuAddPicture, new System.EventArgs());
        }

        private void mnuDeletePicture_Click(object sender, System.EventArgs e)
        {
            // PIC.Edit
            if (MessageBox.Show("Are you sure you want to remove this picture?", "Remove Picture?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (pictureViewModel.Pictures.HasCurrentPicture())
                {
                    pictureViewModel.DeleteCurrentPicture();
                }                
                else
                {
                    MessageBox.Show("There is no current picture", "Nothing To Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // PIC.Update
                imgPicture.Image = null;
               // lblPicName.Text = "Unable to find File";
                NewLoadPictureInformation();
            }
        }

        public void mnuDeletePicture_Click()
        {
            mnuDeletePicture_Click(mnuDeletePicture, new System.EventArgs());
        }

        public void NewLoadPictureInformation()
        {
            bool SavePicture;
            try
            {
                // On Error GoTo ErrorHandler
                cmdPrevPicture.Enabled = false;
                SavePicture = false;
                //modGNBas.Statics.TrioDATA = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
                imgPicture.Image = null;
                int intCounter;
                pictureViewModel = StaticSettings.GlobalCommandDispatcher.Send(new GetPropertyPictureViewModel()).Result;
                pictureViewModel.LoadPictures(theAccount.Account, modGNBas.Statics.gintCardNumber, Guid.Parse	( modDataTypes.Statics.MR.Get_Fields_String("CardID")));
                if (pictureViewModel.Pictures.Count() > 0)                
                {
                    
                    if (!pictureViewModel.Pictures.OnLastPicture())
                    {
                        cmdNextPicture.Enabled = true;
                    }
                    else
                    {
                        cmdNextPicture.Enabled = false;
                    }
                    Auto_Size_Photo();
                }
                else
                {
                    cmdNextPicture.Enabled = false;
                }
                // lblName2.Caption = Trim(MR.Fields("RSNAME") & " ")
                if (!(theAccount.OwnerParty == null))
                {
                    lblName2.Text = Strings.Trim(theAccount.OwnerParty.FullName);
                }
                else
                {
                    lblName2.Text = "";
                }
                modGNBas.Statics.LinkPicture = false;
                imgPicture.Visible = true;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In NewLoadPictureInformation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void winSketchImageTimer_Tick(object sender, EventArgs e)
        {
            if (sketchesViewModel == null)
            {
                return;
            }

            if (sketchesViewModel.Sketches.HasPendingSketch())
            {
                var pendingSketch = sketchesViewModel.Sketches.GetPendingSketch();
                var sketchIdentifier = pendingSketch.SketchIdentifier;
               // sketchesViewModel.Sketches.ClearPendingSketch();
                string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt");
                if (File.Exists(sketchFileNamePath))
                {
                    FileInfo fileInfo = new FileInfo(sketchFileNamePath);
                    if (fileInfo.LastWriteTime > pendingSketch.DateUpdated)
                    {
                        sketchesViewModel.Sketches.ClearPendingSketch();
                        if (!sketchesViewModel.Sketches.HasSketch(sketchIdentifier))
                        {
                            CreateSketchFromFiles(sketchIdentifier);
                        }
                        else
                        {
                            UpdateSketchFromFiles(sketchIdentifier);
                        }
                        
                    }
                }
            }
        }

        private void CreateSketchFromFiles(Guid sketchIdentifier)
        {
            string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt");
            var sketchImagePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".bmp");
            var sketchCalcPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt-calculations");
            if (File.Exists(sketchFileNamePath))
            {
                FileInfo fileInfo = new FileInfo(sketchFileNamePath);
                var sketchFile = File.ReadAllBytes(sketchFileNamePath);
                var calculation = "";
                byte[] sketchImageBytes = null;
                if (File.Exists(sketchImagePath))
                {
                    sketchImageBytes = File.ReadAllBytes(sketchImagePath);
                }

                if (File.Exists(sketchCalcPath))
                {
                    calculation = File.ReadAllText(sketchCalcPath);
                }
                sketchesViewModel.AddNewSketch(sketchIdentifier,sketchesViewModel.Sketches.Count() + 1,calculation, "image/bmp", sketchImageBytes,sketchFile);
                DeleteTemporarySketchFiles(sketchIdentifier);
                ShowCurrentSketch();
            }
        }

        private void UpdateSketchFromFiles(Guid sketchIdentifier)
        {
            string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt");
            var sketchImagePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".bmp");
            var sketchCalcPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt-calculations");
            if (File.Exists(sketchFileNamePath))
            {
                FileInfo fileInfo = new FileInfo(sketchFileNamePath);
                var sketchFile = File.ReadAllBytes(sketchFileNamePath);
                var calculation = "";
                byte[] sketchImageBytes = null;
                if (File.Exists(sketchImagePath))
                {
                    sketchImageBytes = File.ReadAllBytes(sketchImagePath);
                }

                if (File.Exists(sketchCalcPath))
                {
                    calculation = File.ReadAllText(sketchCalcPath);
                }
                sketchesViewModel.UpdateSketch(sketchIdentifier,  calculation, "image/bmp", sketchImageBytes, sketchFile);
                DeleteTemporarySketchFiles(sketchIdentifier);
                ShowCurrentSketch();
            }
        }
        private void DeleteTemporarySketchFiles(Guid sketchIdentifier)
        {
            string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt");
            var sketchImagePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".bmp");
            var sketchCalcPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt-calculations");
            var sketchDataPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".data");

            if (File.Exists(sketchFileNamePath))
            {
                File.Delete(sketchFileNamePath);
            }

            if (File.Exists(sketchImagePath))
            {
                File.Delete(sketchImagePath);
            }

            if (File.Exists(sketchCalcPath))
            {
                File.Delete(sketchCalcPath);
            }

            if (File.Exists(sketchDataPath))
            {
                File.Delete(sketchDataPath);
            }
        }
        //private void CheckForUpdatedSketchFile(bool updateImage = true)
        //{
        //    if (intCurrentSketch > 0 && File.Exists(SketchArray[intCurrentSketch]))
        //    {
        //        FileInfo fileInfo = new FileInfo(SketchArray[intCurrentSketch]);
        //        if (fileInfo.LastWriteTime > imgSketchLastModified)
        //        {
        //            imgSketchLastModified = fileInfo.LastWriteTime;
        //            if (updateImage)
        //            {
        //                imSketch.Image = FCUtils.LoadPicture(SketchArray[intCurrentSketch]);
        //            }
        //        }
        //    }
        //}

        //private void UpdateImgSketchLastModified()
        //{
        //    imgSketchLastModified = DateTime.MinValue;
        //    CheckForUpdatedSketchFile(false);
        //}

        private void SSTab1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            /*? On Error Resume Next  */
            try
            {
                SSTab1_Resize();

                clsDRWrapper clsLoad = new clsDRWrapper();
                int lngReturn;
                DateTime dtDate;
                if (!modGNBas.Statics.gboolFormLoaded)
                {
                    SSTab1.SelectedIndex = 0;
                    if (txtMRRSMAPLOT.Enabled && txtMRRSMAPLOT.Visible)
                        txtMRRSMAPLOT.Focus();
                    return;
                }
                modGNBas.Statics.intNewTab = SSTab1.SelectedIndex;
                if (modGNBas.Statics.intNewTab < 2)
                {
                    // not dwelling
                    GridDwelling1.Row = 0;
                    GridDwelling2.Row = 0;
                    GridDwelling3.Row = 0;
                    GridDwelling1.Col = 1;
                    GridDwelling2.Col = 1;
                    GridDwelling3.Col = 1;
                }
                if (modGNBas.Statics.intNewTab > 4 && !modGlobalVariables.Statics.boolRegRecords)
                {
                    MessageBox.Show("This function not available for Sales Records", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    SSTab1.SelectedIndex = 0;
                    return;
                }
                if (modGNBas.Statics.intNewTab == 7)
                {
                    mnuSketch.Visible = true;
                    winSketchImageTimer.Start();
                }
                else
                {
                    mnuSketch.Visible = false;
                    winSketchImageTimer.Stop();
                }
                if (modGNBas.Statics.intNewTab == 6)
                {
                    mnuPictures.Visible = true;
                    if (imgPicture.Image == null || pictureViewModel == null)
                    {
                        NewLoadPictureInformation();
                    }

                    if (pictureViewModel.Pictures.Count() == 0 && mnuAddPicture.Enabled)
                    {
                        // check if it has a pic but couldn't load
                        if (pictureViewModel.Pictures.Count() == 0)
                        {
                            // ask if they want to load a picture
                            if (FCMessageBox.Show("There are no pictures for this card. Would you like to add one?",
                                    MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Add a picture?") == DialogResult.Yes)
                            {
                                AddAPicture();
                            }
                        }
                        
                    }
                }
                else
                {
                    mnuPictures.Visible = false;
                }
                if (modGNBas.Statics.intNewTab == 5)
                {
                    mnuLayout.Visible = true;
                }
                else
                {
                    mnuLayout.Visible = false;
                }
                if (modGNBas.Statics.intNewTab == 7)
                {
                    // sketches
                    if (SSTab1.PreviousTab != modGNBas.Statics.intNewTab)
                    {
                        LoadSketch();
                    }
                    mnuSketch.Visible = true;
                }
                else
                {
                    mnuSketch.Visible = false;
                }
                if (SSTab1.SelectedIndex > 4 && modGNBas.Statics.gboolLoadingDone)
                {
                }
                else
                {
                    // vbPorter upgrade warning: answer As string	OnWrite(DialogResult)
                    DialogResult answer = DialogResult.None;
                    if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == string.Empty)
                        modGlobalVariables.Statics.HoldDwelling = modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode");
                    if (modGNBas.Statics.intNewTab == 2)
                    {
                        //FC:FINAL:MSH - i.issue #1249: comparing object and string will return incorrect result
                        //if (modGlobalVariables.Statics.HoldDwelling == "C")
                        if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == "C")
                        {
                            MessageBox.Show("The information for this card is Commercial only", "Commercial Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            SSTab1.SelectedIndex = 3;
                            if (mebOcc1.Enabled)
                                mebOcc1.Focus();
                        }
                        //FC:FINAL:MSH - i.issue #1249: comparing object and string will return incorrect result
                        //else if (modGlobalVariables.Statics.HoldDwelling == "Y")
                        else if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == "Y")
                        {
                        }
                        else
                        {
                            if (modGlobalVariables.Statics.boolInPendingMode || !GridDwelling1.Enabled)
                            {
                                MessageBox.Show("There is no dwelling data for this card.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                SSTab1.SelectedIndex = 0;
                                if (txtMRRSMAPLOT.Enabled)
                                    txtMRRSMAPLOT.Focus();
                            }
                            else
                            {
                                BringToFront();
                                //Application.DoEvents();
                                //FC:FINAL:MSH - converting to int is incorrect, because 'answer' will has converted to int DialogResult 
                                // and will be compared with string results
                                //answer = FCConvert.ToString(FCConvert.ToInt32(MessageBox.Show("There is no Dwelling data for this card. Would you like to make this card Residential?", "Commercial/Residential", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                                answer = MessageBox.Show("There is no Dwelling data for this card. Would you like to make this card Residential?", "Commercial/Residential", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (answer == DialogResult.Yes)
                                {
                                    modGlobalVariables.Statics.HoldDwelling = "Y";
                                    modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "Y");
                                    clsDRWrapper temp = modDataTypes.Statics.DWL;
                                    modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), theAccount.AccountID);
                                    modDataTypes.Statics.DWL = temp;
                                    // If mebMRDIStyle.Enabled Then mebMRDIStyle.SetFocus
                                    GridDwelling2.TextMatrix(12, 1, FCConvert.ToString(1));
                                    // 1 bath
                                    GridDwelling3.TextMatrix(5, 1, FCConvert.ToString(100));
                                    // factor of 100
                                    GridDwelling1.TextMatrix(1, 1, FCConvert.ToString(1));
                                    modDataTypes.Statics.DWL.Set_Fields("diunitsDWELLING", 1);
                                    modDataTypes.Statics.DWL.Set_Fields("difullbaths", 1);
                                    modDataTypes.Statics.DWL.Set_Fields("digrade2", 100);
                                }
                                else
                                {
                                    SSTab1.SelectedIndex = 0;
                                    if (txtMRRSMAPLOT.Enabled)
                                        txtMRRSMAPLOT.Focus();
                                }
                            }
                        }
                    }
                    else if (modGNBas.Statics.intNewTab == 3)
                    {
                        if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == "Y")
                        {
                            MessageBox.Show("This card has Dwelling data only", "Dwelling Data Only", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            SSTab1.SelectedIndex = 2;
                            // If mebMRDIStyle.Enabled Then mebMRDIStyle.SetFocus
                        }
                        else
                        {
                            if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == "C")
                            {
                                if (modGNBas.Statics.gboolViewCommercial || modGNBas.Statics.gboolCommercial)
                                    return;
                            }
                            if (modGlobalVariables.Statics.boolInPendingMode || mebOcc1.Enabled == false)
                            {
                                MessageBox.Show("There is no commercial data for this card.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                SSTab1.SelectedIndex = 0;
                                if (txtMRRSMAPLOT.Enabled)
                                    txtMRRSMAPLOT.Focus();
                            }
                            else
                            {
                                //FC:FINAL:MSH - converting to int is incorrect, because 'answer' will has converted to int DialogResult 
                                // and will be compared with string results (same with I.Issue #820)
                                //answer = FCConvert.ToString(FCConvert.ToInt32(MessageBox.Show("There is no Commercial Data for this card. Would you like to make this card Commercial?", "Commercial/Residential", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                                answer = MessageBox.Show("There is no Commercial Data for this card. Would you like to make this card Commercial?", "Commercial/Residential", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (answer == DialogResult.Yes)
                                {
                                    if (!modGNBas.Statics.gboolCommercial && !modGNBas.Statics.gboolViewCommercial)
                                    {
                                        MessageBox.Show("Commercial properties have expired" + "\r\n" + "Please contact trio if you wish to renew.", "Expired", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        SSTab1.SelectedIndex = 0;
                                        return;
                                        // End If
                                    }
                                    modGlobalVariables.Statics.HoldDwelling = "C";
                                    if (mebOcc1.Enabled)
                                        mebOcc1.Focus();
                                }
                                else
                                {
                                    SSTab1.SelectedIndex = 0;
                                    if (txtMRRSMAPLOT.Enabled)
                                        txtMRRSMAPLOT.Focus();
                                }
                            }
                        }
                    }
                    // End If  'temporary. if sstab.tab = 4
                }
                if (!modGNBas.Statics.gboolLoadingDone)
                {
                    SSTab1.SelectedIndex = 0;
                    if (txtMRRSMAPLOT.Enabled)
                        txtMRRSMAPLOT.Focus();
                }
            }
            catch
            {
            }
        }
        //FC:FINAL:CHN - issue #1661: Delete free space at TabControl items.
        private void SSTab1_Resize()
        {
            if (SSTab1.SelectedTab == SSTab1_Page1)
            {
                SSTab1.Height = 886;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page2)
            {
                SSTab1.Height = 989;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page3)
            {
                SSTab1.Height = 540;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page4)
            {
                SSTab1.Height = 523;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page5)
            {
                SSTab1.Height = 475;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page6)
            {
                SSTab1.Height = 662;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page7)
            {
                SSTab1.Height = 767;
            }
            else if (SSTab1.SelectedTab == SSTab1_Page8)
            {
                SSTab1.Height = 460;
            }
        }

        private void SSTab1_Enter(object sender, System.EventArgs e)
        {
            switch (SSTab1.SelectedIndex)
            {
                case 0:
                    {
                        // property tab
                        // If txtMRRSMAPLOT.Enabled Then txtMRRSMAPLOT.SetFocus
                        // If txtMRRSName.Enabled Then txtMRRSName.SetFocus
                        break;
                    }
                case 2:
                    {
                        // dwelling tab
                        if (Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"))) == "Y")
                        {
                            // If mebMRDIStyle.Enabled Then mebMRDIStyle.SetFocus
                        }
                        break;
                    }
                case 3:
                    {
                        // commercial tab
                        if (Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"))) == "C")
                        {
                            if (mebOcc1.Enabled)
                                mebOcc1.Focus();
                        }
                        break;
                    }
                case 4:
                    {
                        // OutBuilding tab
                        if (txtMROIType1[0].Enabled)
                            txtMROIType1[0].Focus();
                        break;
                    }
            }
            //end switch
        }

        private void SSTab1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Down)
            {
                if (SSTab1.SelectedIndex == 1)
                {
                    // the dwelling tab
                    KeyCode = (Keys)0;
                    if (ActiveControl.GetName() == "mebMRDIInspectionDate")
                    {
                        // If mebMRDIStyle.Enabled Then mebMRDIStyle.SetFocus
                    }
                    else
                    {
                        Support.SendKeys("{TAB}", false);
                    }
                }
            }
        }

        //private void hsSketch_ValueChanged(object sender, System.EventArgs e)
        //{
        //    //imSketch.Left = FCConvert.ToInt32(-(hsSketch.Value / 2.0));
        //}

        //private void hsSketch_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
        //{
        //    //imSketch.Left = FCConvert.ToInt32(-(hsSketch.Value / 2.0));
        //}

        //private void vsSketch_ValueChanged(object sender, System.EventArgs e)
        //{
        //    //imSketch.Top = FCConvert.ToInt32(-(vsSketch.Value * dblPixelsPerScrollUnits));
        //}

        //private void vsSketch_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
        //{
        //    //imSketch.Top = FCConvert.ToInt32(-(vsSketch.Value * dblPixelsPerScrollUnits));
        //}

        private void cmdPrevSketch_Click(object sender, System.EventArgs e)
        {
            if (!sketchesViewModel.Sketches.HasCurrentSketch() || sketchesViewModel.Sketches.OnFirstSketch())
            {
                return;
            }

            sketchesViewModel.Sketches.PreviousSketch();
            ShowCurrentSketch();
            appWinSketch = null;
        }

        private void cmdNextSketch_Click(object sender, System.EventArgs e)
        {
            if (!sketchesViewModel.Sketches.HasCurrentSketch() || sketchesViewModel.Sketches.OnLastSketch())
            {
                return;
            }

            sketchesViewModel.Sketches.NextSketch();
            ShowCurrentSketch();
            appWinSketch = null;           
        }

        private void ResizeSketchImage()
        {
            if (imSketch.Image != null)
            {
                float scale = 920f / (float)imSketch.Image.Width;
                imSketch.Height = Convert.ToInt32(imSketch.Image.Height * scale);
            }
            else
            {
                imSketch.Height = 300;
            }
        }

        //private void LoadSketch_2(bool boolForceToRun)
        //{
        //    LoadSketch(boolForceToRun);
        //}

        private void ReLoadSketches()
        {
            
            sketchesViewModel = StaticSettings.GlobalCommandDispatcher.Send(new GetPropertySketchesViewModel()).Result;
            sketchesViewModel.LoadSketches(theAccount.Account, modGNBas.Statics.gintCardNumber, Guid.Parse(modDataTypes.Statics.MR.Get_Fields_String("CardID")));
            sketchesViewModel.Sketches.SetToFirstSketch();
            if (sketchesViewModel.Sketches.Count() > 1)
            {
                cmdNextSketch.Enabled = true;
            }
            else
            {
                cmdNextSketch.Enabled = false;
            }

            cmdPrevPicture.Enabled = false;
            ShowCurrentSketch();
        }
        private void LoadSketch()
        {
            if (sketchesViewModel != null)
            {
                if (sketchesViewModel.CardIdentifier != Guid.Parse(modDataTypes.Statics.MR.Get_Fields_String("CardID")))
                {
                    ReLoadSketches();
                }
            }
            else
            {
                ReLoadSketches();
            }
            appWinSketch = null;
           
        }

        public void frmNewREProperty_Activated(object sender, System.EventArgs e)
        {
            int intCounter;
            bool boolTemp = false;
            string strTemp = "";
            clsDRWrapper clsTemp = new clsDRWrapper();
            int lngTemp = 0;
            string strTemp2 = "";
            try
            {
                // On Error GoTo ErrorHandler
                Label8[7].Text = modGlobalVariables.Statics.CustomizedInfo.Ref1Description;
                Label8[8].Text = modGlobalVariables.Statics.CustomizedInfo.Ref2Description;
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    clsTemp.OpenRecordset("Select * from commrec where crecordnumber > 0 and crecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("ccomment"))) != "")
                        {
                            lblComment.Visible = true;
                        }
                        else
                        {
                            lblComment.Visible = false;
                        }
                    }
                    else
                    {
                        lblComment.Visible = false;
                    }
                    lblPendingFlag.Visible = false;
                    clsTemp.OpenRecordset("select account from pending where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        lblPendingFlag.Visible = true;
                    }
                    clsTemp.OpenRecordset("select account from pendingchanges where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        lblPendingFlag.Visible = true;
                    }
                    clsTemp.OpenRecordset("select * from picturerecord where mraccountnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), "twre0000.vb1");
                    if (!clsTemp.EndOfFile())
                    {
                        lblPicturePresent.Visible = true;
                    }
                    else
                    {
                        lblPicturePresent.Visible = false;
                    }
                    CheckForInterestedParties(modGlobalVariables.Statics.gintLastAccountNumber);
                    if (!(modDataTypes.Statics.MR == null))
                    {
                        if (!modDataTypes.Statics.MR.EndOfFile())
                        {
                            strTemp = modDataTypes.Statics.MR.Get_Fields_Int32("id").ToString();
                            if (strTemp.Length < 7)
                            {
                                strTemp = Strings.StrDup(7 - strTemp.Length, "0") + strTemp;
                            }
                            //FC:FINAL:DSE:#830 Path should not be relative to Environment, but to deployment
                            //strTemp2 = Environment.CurrentDirectory + "\\sketches\\" + strTemp;
                            strTemp2 = Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "sketches", strTemp);
                            strTemp = FCFileSystem.Dir(strTemp2 + "*.skt", 0);
                            if (strTemp != string.Empty)
                            {
                                lblSketchPresent.Visible = true;
                            }
                            else
                            {
                                lblSketchPresent.Visible = false;
                            }
                        }
                        else
                        {
                            lblSketchPresent.Visible = false;
                        }
                    }
                    else
                    {
                        lblSketchPresent.Visible = false;
                    }
                }
                else
                {
                    lblComment.Visible = false;
                    lblPendingFlag.Visible = false;
                    lblSketchPresent.Visible = false;
                    lblPicturePresent.Visible = false;
                }
                if (boolUpdateSketch)
                {
                    boolUpdateSketch = false;
                    clsPropEvents_ConvertAndZoomSketch();
                }
                if (modGNBas.Statics.gboolFormLoaded)
                {
                    return;
                }
                lblDate[7].Text = modGlobalVariables.Statics.strTranCodeTownCode + " Code";
                if (modSecurity.ValidPermissions_6(this, modGlobalVariables.RE_Permission_PropertyDocuments, false))
                {
                    boolViewDocuments = true;
                    if (modSecurity.ValidPermissions_6(this, modGlobalVariables.RE_Permission_EditPropertyDocuments, false))
                    {
                        boolEditDocuments = true;
                    }
                    else
                    {
                        boolEditDocuments = false;
                    }
                }
                else
                {
                    boolViewDocuments = false;
                    boolEditDocuments = false;
                }
                if (modGlobalVariables.Statics.boolFromSalesAnalysis || !modGlobalVariables.Statics.boolRegRecords)
                {
                    // 1        lngTemp = MR.Fields("id")
                    lngTemp = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("saleid"));
                    modGlobalVariables.Statics.glngSaleID = lngTemp;
                    mnuViewDocs.Visible = false;
                    boolEditDocuments = false;
                    boolViewDocuments = false;
                }
                if (modGlobalConstants.Statics.gboolCL)
                {
                    chkTaxAcquired.Enabled = false;
                }
                boolBookPageChanged = false;
                // boolGridPhonesChanged = False
                if (!modGlobalVariables.Statics.boolRegRecords)
                {
                    SSTab1.TabPages[5].Enabled = false;
                    SSTab1.TabPages[6].Enabled = false;
                    SSTab1.TabPages[7].Enabled = false;
                    mnuViewDocs.Visible = false;
                    boolEditDocuments = false;
                    boolViewDocuments = false;
                }
                if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "HANCOCK")
                {
                    mnuReassignSketch.Visible = true;
                }
                else
                {
                    mnuReassignSketch.Visible = false;
                }
                if (modGlobalVariables.Statics.boolInPendingMode)
                {
                    SSTab1.TabPages[5].Enabled = false;
                    SSTab1.TabPages[6].Enabled = false;
                    SSTab1.TabPages[7].Enabled = false;
                    Label7.Visible = true;
                    Label7.Text = "Pending";
                    mnuViewDocs.Visible = false;
                    boolEditDocuments = false;
                    boolViewDocuments = false;
                }
                if (boolViewDocuments)
                {
                    mnuViewDocs.Visible = true;
                }
                else
                {
                    mnuViewDocs.Visible = false;
                }
                booleditland = true;
                if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTLONGSCREEN))
                {
                    MessageBox.Show("Your permission setting for this function is set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Unload();
                    return;
                }
                if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTGROUPMAINT, false))
                {
                    mnuViewGroupInformation.Visible = false;
                }
                if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTMORTGAGEMAINT, false))
                {
                    mnuViewMortgageInformation.Visible = false;
                }
                if (SSTab1.SelectedIndex == 4)
                {
                    mnuLayout.Visible = true;
                }
                else
                {
                    mnuLayout.Visible = false;
                }
                lblNumCards.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
                clsTemp.OpenRecordset("select SKdate ,SK from modules ", modGlobalVariables.Statics.strGNDatabase);
                strTemp = FCConvert.ToString(clsTemp.Get_Fields_DateTime("SKdate"));
                if (Information.IsDate(strTemp))
                {
                    boolTemp = modGlobalRoutines.CompareTheDates_24(modGlobalVariables.Statics.strGNDatabase, "modules", "SKdate");
                    SSTab1.TabPages[7].Enabled = SSTab1.TabPages[6].Enabled && boolTemp && clsTemp.Get_Fields_Boolean("SK");
                }
                else
                {
                    SSTab1.TabPages[7].Enabled = false;
                }
                modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber));
                // DoEvents
                // If FormLoaded Then GoTo EndSub
                if (!modGlobalVariables.Statics.boolRegRecords)
                {
                    Label7.Visible = true;
                    mnuAddNewAccount.Enabled = false;
                    cmdLogSale.Enabled = false;
                    cmdComment.Enabled = false;
                    mnuRECLComment.Enabled = false;
                    mnuCollectionsNote.Enabled = false;
                    mnuSeparateCard.Enabled = false;
                }
                else
                {
                    if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTCOMMENTS)) == "F" && modGlobalVariables.Statics.boolRegRecords)
                    {
                        cmdComment.Enabled = true;
                        mnuRECLComment.Enabled = true;
                        mnuCollectionsNote.Enabled = true;
                    }
                    if (!modGlobalVariables.Statics.boolInPendingMode)
                    {
                        Label7.Visible = false;
                        if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTDELCARDS)) == "F")
                        {
                            mnuDeleteCard.Enabled = true;
                        }
                        else
                        {
                            mnuDeleteCard.Enabled = false;
                        }
                        cmdLogSale.Enabled = true;
                    }
                    else
                    {
                        mnuSeparateCard.Enabled = false;
                        Label7.Visible = true;
                        // mnuCopy.Enabled = False
                        mnuSavePending.Enabled = true;
                        cmdComment.Enabled = false;
                        mnuRECLComment.Enabled = false;
                        mnuCollectionsNote.Enabled = false;
                        mnuDeleteCard.Enabled = false;
                        mnuDeleteDwellingData.Enabled = false;
                        mnuDeleteOutbuilding.Enabled = false;
                        cmdLogSale.Enabled = false;
                        mnuAddACard.Enabled = false;
                        mnuAddNewAccount.Enabled = false;
                        mnuDeleteAccount.Enabled = false;
                        mnuDeleteCard.Enabled = false;
                        cmdCalculate.Enabled = false;
                        // mnuSave.Enabled = False
                        // mnuSaveQuit.Enabled = False
                        cmdSave.Text = "Save Pending Changes";
                        mnuSaveQuit.Text = "Save Pending & Exit";
						mnuViewGroupInformation.Visible = false;
                        mnuViewMortgageInformation.Visible = false;
                    }
                }
                if (modGlobalVariables.Statics.boolFromBilling)
                {
                    SSTab1.TabPages[5].Enabled = false;
                    SSTab1.TabPages[6].Enabled = false;
                    SSTab1.TabPages[7].Enabled = false;
                }
                if (!modGNBas.Statics.gboolCommercial && !modGNBas.Statics.gboolViewCommercial)
                {
                    /* Control ctl = new Control(); *///FC:FINAL:MSH - i.issue #1649: in VB6 control contain all controls, include child controls in each control
                                                      //foreach (Control ctl in this.Controls)
                    var formControls = this.GetAllControls();
                    foreach (Control ctl in formControls)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "commercial")
                        {
                            ctl.Enabled = false;
                        }
                    }
                    // ctl
                }
                if (modGNBas.Statics.gboolFormLoaded)
                {
                    // Call FillScreenWithoutLoadingMR
                    goto EndSub;
                }

                modGNBas.Statics.gboolFormLoaded = true;
                if (modGlobalVariables.Statics.ComingFromTheHelpScreen == true)
                {
                    modGlobalVariables.Statics.ComingFromTheHelpScreen = false;
                    return;
                }

                if (modGNBas.Statics.gboolFormLoaded == false || true)
                {
                    modGNBas.Statics.gboolFormLoaded = true;
                    imgDocuments.Visible = mnuViewDocs.Visible && CheckForDocs(modGlobalVariables.Statics.gintLastAccountNumber, "");

                    //FC:FINAL:DSE System locks in chrome when changing visible during Activated event
                    //frmNewREProperty.InstancePtr.Visible = true;
                    int costnumber = 0;
                    FCFileSystem.FileClose(30);
                    costnumber = 1091;
                    // Get #30, costnumber, CR
                    clsDRWrapper temp = modDataTypes.Statics.CR;
                    modREMain.OpenCRTable(ref temp, costnumber);
                    modDataTypes.Statics.CR = temp;
                    var clDesc = modDataTypes.Statics.CR.Get_Fields_String("ClDesc");
                    Label8[12].Text = FCConvert.ToString(clDesc + " ");
                    if (Strings.Left(FCConvert.ToString(clDesc), 1) == ".")
                    {
                        Label8[12].Text = "X Coord";
                    }
                    costnumber = 1092;
                    // Get #30, costnumber, CR
                    temp = modDataTypes.Statics.CR;
                    modREMain.OpenCRTable(ref temp, costnumber);
                    modDataTypes.Statics.CR = temp;
                    Label8[13].Text = FCConvert.ToString(clDesc + " ");
                    if (Strings.Left(FCConvert.ToString(clDesc), 1) == ".")
                    {
                        Label8[13].Text = "Y Coord";
                    }
                    costnumber = 1330;
                    // Get #30, costnumber, CR
                    temp = modDataTypes.Statics.CR;
                    modREMain.OpenCRTable(ref temp, costnumber);
                    Label8[19].Text = FCConvert.ToString(temp.Get_Fields_String("ClDesc") + " ");
                    costnumber = 1340;
                    // Get #30, costnumber, CR
                    modREMain.OpenCRTable(ref temp, costnumber);
                    Label8[20].Text = FCConvert.ToString(temp.Get_Fields_String("ClDesc") + " ");
                    FCFileSystem.FileClose(30);
                    // end of open 1 and open 2 long descriptions
                    costnumber = 1540;
                    modREMain.OpenCRTable(ref temp, costnumber);
                    GridDwelling1.TextMatrix(11, 0, Strings.Trim(temp.Get_Fields_String("cldesc") + ""));
                    costnumber = 1550;
                    modREMain.OpenCRTable(ref temp, costnumber);
                    GridDwelling1.TextMatrix(12, 0, Strings.Trim(temp.Get_Fields_String("cldesc") + ""));
                    modDataTypes.Statics.CR = temp;
                    modGlobalVariables.Statics.FormLoaded = true;
                }

                if (modGNBas.Statics.gintCardNumber == 0)
                {
                    modGNBas.Statics.gintCardNumber = 1;
                    modGlobalVariables.Statics.ActiveCard = modGNBas.Statics.gintCardNumber;
                    // CardNumber = gintCardNumber
                }

                // optCardNumber(gintCardNumber - 1).Value = True
                cmbCardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
                cmbCardNumber_Validate(false);
                if (lngCurrentPendingID <= 0)
                {

                    lblNumCards.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);

                    if (modGlobalVariables.Statics.GetAccountSearch == true)
                    {
                        cmdNextAccountFromSearch.Enabled = true;
                        cmdPreviousAccountFromSearch.Enabled = true;
                        modGlobalVariables.Statics.GetAccountSearch = false;
                    }
                }
                // if lngcurrentpendingid = 0
                //FC:FINAL:DSE System locks in chrome when changing visible during Activated event
                //frmNewREProperty.InstancePtr.Visible = true;
                modGNBas.Statics.gboolFormLoaded = true;
                frmNewREProperty.InstancePtr.BringToFront();
            // DoEvents
            EndSub:
                ;
                modGNBas.Statics.gboolFormLoaded = true;
                if (!boolCalculating)
                {
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    modGlobalVariables.Statics.LandTypes.Init();
                    CalculateAcres();
                }
                //FC:FINAL:MSH - issue #1652: assign missing keydown handlers for grid's EditingControls
                GridDwelling1.EditingControlShowing -= GridDwelling1_EditingControlShowing;
                GridDwelling1.EditingControlShowing += GridDwelling1_EditingControlShowing;
                GridDwelling2.EditingControlShowing -= GridDwelling2_EditingControlShowing;
                GridDwelling2.EditingControlShowing += GridDwelling2_EditingControlShowing;
                GridDwelling3.EditingControlShowing -= GridDwelling3_EditingControlShowing;
                GridDwelling3.EditingControlShowing += GridDwelling3_EditingControlShowing;
                GridStreet.EditingControlShowing -= GridStreet_EditingControlShowing;
                GridStreet.EditingControlShowing += GridStreet_EditingControlShowing;
                GridUtilities.EditingControlShowing -= GridUtilities_EditingControlShowing;
                GridUtilities.EditingControlShowing += GridUtilities_EditingControlShowing;
                GridTopography.EditingControlShowing -= GridTopography_EditingControlShowing;
                GridTopography.EditingControlShowing += GridTopography_EditingControlShowing;
                GridZone.EditingControlShowing -= GridZone_EditingControlShowing;
                GridZone.EditingControlShowing += GridZone_EditingControlShowing;
                GridNeighborhood.EditingControlShowing -= GridNeighborhood_EditingControlShowing;
                GridNeighborhood.EditingControlShowing += GridNeighborhood_EditingControlShowing;
                GridLand.EditingControlShowing -= GridLand_EditingControlShowing;
                GridLand.EditingControlShowing += GridLand_EditingControlShowing;
                BPGrid.EditingControlShowing -= BPGrid_EditingControlShowing;
                BPGrid.EditingControlShowing += BPGrid_EditingControlShowing;

                // corey
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show($"Error Number {FCConvert.ToString(Information.Err(ex).Number)}  {Information.Err(ex).Description}\r\nIn frmnewreproperty Activate\r\nLine {Information.Erl()}", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void BPGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(BPGrid_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(BPGrid_KeyDownEdit);
            }
        }

        private void GridLand_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridLand_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(GridLand_KeyDownEdit);
            }
        }

        private void GridNeighborhood_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridNeighborhood_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(GridNeighborhood_KeyDownEdit);
            }
        }

        private void GridZone_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridZone_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(GridZone_KeyDownEdit);
            }
        }

        private void GridTopography_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridTopography_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(GridTopography_KeyDownEdit);
            }
        }

        private void GridUtilities_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridUtilities_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(GridUtilities_KeyDownEdit);
            }
        }

        private void GridStreet_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridStreet_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(GridStreet_KeyDownEdit);
            }
        }

        private void GridDwelling1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridDwelling1_KeyDownEdit);
                e.Control.KeyUp -= new KeyEventHandler(GridDwelling1_KeyUpEdit);

                e.Control.KeyDown += new KeyEventHandler(GridDwelling1_KeyDownEdit);
                e.Control.KeyUp += new KeyEventHandler(GridDwelling1_KeyUpEdit);
            }
        }

        private void GridDwelling2_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridDwelling2_KeyDownEdit);
                e.Control.KeyUp -= new KeyEventHandler(GridDwelling2_KeyUpEdit);

                e.Control.KeyDown += new KeyEventHandler(GridDwelling2_KeyDownEdit);
                e.Control.KeyUp += new KeyEventHandler(GridDwelling2_KeyUpEdit);
            }
        }

        private void GridDwelling3_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(GridDwelling3_KeyDownEdit);
                e.Control.KeyUp -= new KeyEventHandler(GridDwelling3_KeyUpEdit);

                e.Control.KeyDown += new KeyEventHandler(GridDwelling3_KeyDownEdit);
                e.Control.KeyUp += new KeyEventHandler(GridDwelling3_KeyUpEdit);
            }
        }

        //      private void appWinSketch_UpdateImage(int pageno)
        //{
        //	// Dim strSkt As String
        //	try
        //	{
        //		// On Error GoTo ErrorHandler
        //		// copy picture from winsketch to the clipboard
        //		// then copy the clipboard to the image control
        //		// then save the image controls picture
        //		if (pageno > 0)
        //			return;
        //		// DoEvents
        //		if (appWinSketch.GetImage(0))
        //		{
        //			// clsPropEvents.ConvertImageEvent
        //			if (!modSysInfo.IsNTBased())
        //			{
        //				imSketch.Image = Clipboard.GetImage();
        //				boolUpdateSketch = true;
        //			}
        //			else
        //			{
        //				clsPropEvents_ConvertAndZoomSketch();
        //				boolUpdateSketch = false;
        //			}
        //		}
        //		return;
        //	}
        //	catch (Exception ex)
        //	{
        //		// ErrorHandler:
        //		MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In UpdateImage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //	}
        //}

        private void clsPropEvents_ConvertAndZoomSketch_2(bool boolPicAlreadyClipped)
        {
            return;
            //clsPropEvents_ConvertAndZoomSketch(boolPicAlreadyClipped);
        }

        private void clsPropEvents_ConvertAndZoomSketch(bool boolPicAlreadyClipped = false)
        {
            return;
            //int lngWidth;
            //int lngHeight;
            //double dblRatio;
            //double dblSkRatio;
            //int x;
            //int y;
            //int lngMaxX;
            //int lngMaxY;
            //int lngMinX;
            //int lngMinY;
            //int lngBackCol;
            //int lngHMFDC;
            //// handle to an enhanced metafile device context
            //int lngHMF;
            //// handle to an enhanced metafile
            //int lngHBMP;
            //// handle to a bitmap
            //int lngBMHDC;
            //int lngTemp;
            ////FC:FINAL:RPU - Use custom constructor to initialize fields
            //modAPIsConst.RECT rTemp = new modAPIsConst.RECT(0);
            //byte[] BitmapBits = null;
            //int lngBmX;
            //int lngBmY;
            ////FC:FINAL:RPU - Use custom constructor to initialize fields
            //modREAPI.BITMAP bm = new modREAPI.BITMAP(0);
            //byte[] BMBits = null;
            //int lngNumBits;
            //try
            //{
            //	// On Error GoTo ErrorHandler
            //	// copy picture from winsketch to the clipboard
            //	// then copy the clipboard to the image control
            //	// then save the image controls picture
            //	//Application.DoEvents();
            //	// If appWinSketch.GetImage(0) Then
            //	// lngBMHDC = CreateCompatibleDC(Me.hdc)
            //	//Application.DoEvents();
            //	if (!boolPicAlreadyClipped)
            //	{
            //		imSketch.Image = Clipboard.GetImage();
            //	}
            //	Picture2.Height = 2400 / FCScreen.TwipsPerPixelY;
            //	Picture2.Width = 2400 / FCScreen.TwipsPerPixelY;
            //	// 
            //	// Call PlayMetaFile(Picture2.hdc, Clipboard.GetData)
            //	// copy so we can save in bmp format since image controls dont have a .image property
            //	Picture2.AutoSize = false;
            //	// Picture2.Picture = imSketch.Picture
            //	dblSkRatio = imSketch.Image.Width / imSketch.Image.Height;
            //	// this is the aspect ratio we need to keep
            //	// now we are going to size it the way we want but keep the aspect ratio
            //	// let's try 400 wide
            //	Picture2.Width = 2400 / FCScreen.TwipsPerPixelX;
            //	Picture2.Height = FCConvert.ToInt32((2400 / dblSkRatio) / FCScreen.TwipsPerPixelY);
            //	Picture2.Image = imSketch.Image;
            //	// Call SavePicture(Picture2.Image, SketchArray(intCurrentSketch))
            //	FCUtils.SavePicture(Picture2.Image, FCFileSystem.Statics.UserDataFolder + "\\TempBMP.bmp");
            //	//Application.DoEvents();
            //	if (!modGlobalConstants.Statics.gboolUse256Colors)
            //	{
            //		modBitMap.ConvertBMPToMonochrome_2(FCFileSystem.Statics.UserDataFolder + "\\tempbmp.bmp", SketchArray[intCurrentSketch]);
            //	}
            //	else
            //	{
            //		modBitMap.ConvertBMP256ToMonochrome_2(FCFileSystem.Statics.UserDataFolder + "\\tempbmp.bmp", SketchArray[intCurrentSketch]);
            //	}
            //	imSketch.Image = FCUtils.LoadPicture(SketchArray[intCurrentSketch]);
            //	//Application.DoEvents();
            //	ResizeSketch();
            //	// End If
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	// ErrorHandler:
            //	MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ConvertAndZoomSketch", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            //}
        }

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "RealEstate", ReferenceId = referenceId, ReferenceType = referenceType });

            return docList.Count > 0;
        }

        private bool CheckAccountNumber(int intVal)
        {
            bool CheckAccountNumber = false;
            clsDRWrapper rsTemp = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                var acctNumberString = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
                rsTemp.OpenRecordset("Select rsaccount,rscard from Master where RSAccount = " + acctNumberString + " and RSCard = " + FCConvert.ToString(intVal), modGlobalVariables.strREDatabase);

                if (!rsTemp.EndOfFile())
                {
                    MessageBox.Show($"Account {acctNumberString} already has a card {FCConvert.ToString(intVal)}. The data for this account is corrupt.", "Bad Card Number", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return CheckAccountNumber;
                }

                rsTemp.OpenRecordset("Select rsaccount,rscard from Master where RSAccount = " + acctNumberString + " and RSCard = " + FCConvert.ToString(intVal - 1), modGlobalVariables.strREDatabase);

                if (rsTemp.EndOfFile())
                {
                    MessageBox.Show("Account " + acctNumberString + " with card " + FCConvert.ToString(intVal - 1) + " is not present. Rebuild the Master Record", "Bad Card Number", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return CheckAccountNumber;
                }

                //FC:FINAL:RPU: #i1257 - If we arrived at this point set CheckAccountNumber true as in original
                CheckAccountNumber = true;
            }
            catch
            {
                // ErrorHandler:
                CheckAccountNumber = true;
            }
            finally
            {
                rsTemp.DisposeOf();

            }
            return CheckAccountNumber;
        }

        public void ClearAllValues()
        {
            // DataChanged = True
            int x;
            // Property Screen
            txtMRRSLocnumalph.Text = "";
            txtMRRSLocapt.Text = "";
            txtMRRSLocStreet.Text = "";
            txtMRRSRef1.Text = "";
            txtMRRSRef2.Text = "";
            gridLandCode.TextMatrix(0, 0, FCConvert.ToString(0));
            gridBldgCode.TextMatrix(0, 0, FCConvert.ToString(0));
            gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(0));
            lblMRRLLANDVAL[28].Text = "";
            lblMRRLBLDGVAL[29].Text = "";
            lblMRRLEXEMPTION[30].Text = "";
            lblTaxable.Text = "";
            gridTranCode.TextMatrix(0, 0, FCConvert.ToString(0));
            txtMRRSMAPLOT.Text = "";
            GridNeighborhood.TextMatrix(0, 0, FCConvert.ToString(0));
            txtMRPIStreetCode.Text = "";
            txtMRPIXCoord.Text = "";
            txtMRPIYCoord.Text = "";
            GridZone.TextMatrix(0, 0, FCConvert.ToString(0));
            GridSecZone.TextMatrix(0, 0, FCConvert.ToString(0));
            GridUtilities.TextMatrix(0, 0, FCConvert.ToString(0));
            GridUtilities.TextMatrix(1, 0, FCConvert.ToString(0));
            GridTopography.TextMatrix(0, 0, FCConvert.ToString(0));
            GridTopography.TextMatrix(1, 0, FCConvert.ToString(0));
            GridStreet.TextMatrix(0, 0, FCConvert.ToString(0));
            txtMRPIOpen1.Text = "";
            txtMRPIOpen2.Text = "";
            SaleGrid.TextMatrix(2, CNSTSALECOLDATE, "");
            SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, "");
            // the following code checks the land1type field and creates
            // the appropraite mask for the Units Masked Edit box.
            for (x = 1; x <= 7; x++)
            {
                GridLand.TextMatrix(x, 0, "");
                GridLand.TextMatrix(x, 1, "");
                GridLand.TextMatrix(x, 2, "100");
                GridLand.TextMatrix(x, 3, "0");
            }
            // x
            frmNewREProperty.InstancePtr.txtMRPIAcres.Text = "";
        }

        private void mnuDeleteAccount_Click(object sender, System.EventArgs e)
        {
            string strDate;
            clsDRWrapper clsTemp = new clsDRWrapper();
            clsDRWrapper dbTemp = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                strDate = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate")));

                //Application.DoEvents();
                string strMsg;
                var accountNumberString = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
                strMsg = "This will remove account " + accountNumberString;

                if (!modGlobalVariables.Statics.boolRegRecords)
                {
                    strMsg = "This will remove the sale record for account " + accountNumberString;
                }

                if (MessageBox.Show(strMsg + ". Are you sure you wish to continue?", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        if (!modGlobalRoutines.CheckMortgage(ref modGlobalVariables.Statics.gintLastAccountNumber))
                        {
                            return;
                        }

                        // If Not CheckOutstandingTaxes(gintLastAccountNumber) Then
                        // Exit Sub
                        // End If
                        modGlobalRoutines.checkGroup_8(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), false);
                    }

                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        // Call dbTemp.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Delete Account','Deleted account " & gintLastAccountNumber & "')")
                        modGlobalFunctions.AddCYAEntry_26("RE", "Delete Account", "Deleted account " + accountNumberString);
                        dbTemp.Execute("Update Master Set rsdeleted = 1 where RSAccount = " + accountNumberString, modGlobalVariables.strREDatabase);
                        dbTemp.Execute("update master set HLUPDATE = '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "' where rsaccount = " + accountNumberString, modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        dbTemp.Execute("Update SRmaster set rsdeleted = 1 where saleid = " + modDataTypes.Statics.MR.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
                        dbTemp.Execute("update srmaster set hlupdate = '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "' where saleid = " + modDataTypes.Statics.MR.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
                    }

                    modDataTypes.Statics.MR.Set_Fields("rsdeleted", true);
                    clsTemp.Execute("delete from summrecord where srecordnumber = " + accountNumberString, modGlobalVariables.strREDatabase);

                    if (modGlobalRoutines.Formisloaded_2("frmRESearch"))
                    {
                        frmRESearch.InstancePtr.Unload();
                    }

                    // If Formisloaded("frmGetAccount") Then
                    // frmGetAccount.ZOrder
                    // End If
                    // frmGetAccount.Show
                    //Application.DoEvents();
                    Unload();
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuDeleteAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsTemp.DisposeOf();
                dbTemp.DisposeOf();
            }
        }

        public void mnuDeleteAccount_Click()
        {
            mnuDeleteAccount_Click(mnuDeleteAccount, new System.EventArgs());
        }

        private void mnuDeleteDwellingData_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper dbTemp = new clsDRWrapper();
            /* Control kontrol = new Control(); */
            clsDRWrapper clsTemp = new clsDRWrapper();
            if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "Y")
            {
                if (MessageBox.Show("This will permanently delete the dwelling data." + "\r\n" + "Are you sure you want to delete it?", "Delete Dwelling?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
                modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "N");
                modGlobalVariables.Statics.HoldDwelling = "N";
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    dbTemp.Execute("delete from dwelling where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                    modGlobalFunctions.AddCYAEntry_26("RE", "Delete Dwelling", "Deleted dwelling for account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
                }
                else
                {
                    dbTemp.Execute("delete from srdwelling where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                    modGlobalFunctions.AddCYAEntry_26("RE", "Delete Dwelling", "Deleted dwelling for sale account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
                }
                modDataTypes.Statics.MR.Update();
                modDataTypes.Statics.MR.Edit();
                if (SSTab1.SelectedIndex == 1)
                    SSTab1.SelectedIndex = 0;
                //Application.DoEvents();
                MessageBox.Show("Dwelling data deleted", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C")
            {
                //Application.DoEvents();
                if (MessageBox.Show("This will permanently delete the commercial data." + "\r\n" + "Are you sure you want to delete it?", "Delete Commercial?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
                modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "N");
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    dbTemp.Execute("delete from commercial where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                    modGlobalFunctions.AddCYAEntry_26("RE", "Delete Commercial", "Deleted commercial for account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
                }
                else
                {
                    dbTemp.Execute("delete from srcommercial where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                    modGlobalFunctions.AddCYAEntry_26("RE", "Delete Commercial", "Deleted commercial for sale account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
                }
                //FC:FINAL:MSH - i.issue #1649: in VB6 control contain all controls, include child controls in each control
                //foreach (Control kontrol in this.Controls)
                var formControls = this.GetAllControls();
                foreach (Control kontrol in formControls)
                {
                    if (Strings.UCase(Strings.Mid(kontrol.GetName(), 1, 5)) == "MEBC1" || Strings.UCase(Strings.Mid(kontrol.GetName(), 1, 5)) == "MEBC2")
                    {
                        kontrol.Text = "";
                    }
                }
                if (SSTab1.SelectedIndex == 2)
                    SSTab1.SelectedIndex = 0;
                //Application.DoEvents();
                MessageBox.Show("Commercial data deleted", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                //Application.DoEvents();
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    dbTemp.Execute("delete from commercial where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                    dbTemp.Execute("delete from dwelling where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
                }
                else
                {
                    dbTemp.Execute("delete from srdwelling where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                    dbTemp.Execute("delete from srcommercial where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and rscard = " + modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                }
                MessageBox.Show("The dwelling code of the master file shows neither a dwelling nor a commercial code.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            modGlobalVariables.Statics.HoldDwelling = "N";
            modDataTypes.Statics.MR.Update();
            modDataTypes.Statics.MR.Edit();
        }

        private void mnuSeparateCard_Click(object sender, System.EventArgs e)
        {
            // moves the card to it's own account
            // Dim ff As New FCFileSystemObject
            // Dim rsTemp As clsDRWrapper
            // Dim dbTemp As DAO.Database
            clsDRWrapper clsTemp = new clsDRWrapper();
            // Dim intResp As Integer
            int lngCards;
            int lngNewAcctNum = 0;
            if (!modGlobalVariables.Statics.boolRegRecords)
            {
                MessageBox.Show("This option is not available for Sale Records", "Invalid Action", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // If gintCardNumber = 1 And VScrollCard.Max < 2 Then
            if (modGNBas.Statics.gintCardNumber == 1/*&& UpDownCard.Maximum < 2*/)
            {
                MessageBox.Show("This option can only be applied to multi-card accounts", "Invalid Action", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (MessageBox.Show("This will delete this card from this account and save it as a new account" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                // lngCards = GetMaxCardsAllowed
                // 
                // If GetCardsUsed >= lngCards Then
                // MsgBox "You have already met or exceeded you max card level." & vbNewLine & "Cannot add an account.", vbExclamation
                // Exit Sub
                // End If
                lngNewAcctNum = modGlobalRoutines.GetNextAccountToCreate();
                modGlobalFunctions.AddCYAEntry_80("RE", "Separate Card", "Moved " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " from account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "To new account " + FCConvert.ToString(lngNewAcctNum));
                clsTemp.Execute("update master set rsaccount = " + FCConvert.ToString(lngNewAcctNum) + ",rscard = 1 where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update summrecord set srecordnumber = " + FCConvert.ToString(lngNewAcctNum) + ", cardnumber = 1 where srecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and cardnumber = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update dwelling set rsaccount = " + FCConvert.ToString(lngNewAcctNum) + ",rscard = 1 where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update commercial set rsaccount = " + FCConvert.ToString(lngNewAcctNum) + ",rscard = 1 where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update outbuilding set rsaccount = " + FCConvert.ToString(lngNewAcctNum) + ",rscard = 1 where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update incomevalue set account = " + FCConvert.ToString(lngNewAcctNum) + ",card = 1 where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update incomeapproach set account = " + FCConvert.ToString(lngNewAcctNum) + ",card = 1 where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update incomeexpense set account = " + FCConvert.ToString(lngNewAcctNum) + ",card = 1 where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                clsTemp.Execute("update picturerecord set mraccountnumber = " + FCConvert.ToString(lngNewAcctNum) + ",rscard = 1 where mraccountnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
                modGNBas.Statics.gintMaxCards -= 1;
                MessageBox.Show("Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " saved as card 1 Account " + FCConvert.ToString(lngNewAcctNum), "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // now change the card numbers
                int intCounter;

                modGNBas.Statics.gintCardNumber = 1;
                lblNumCards.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
                cmbCardNumber.Text = FCConvert.ToString(1);
                cmbCardNumber_Validate(false);
                // Call FillTheScreen
                LoadAccount(modGlobalVariables.Statics.gintLastAccountNumber);
                LoadCard(modGlobalVariables.Statics.gintLastAccountNumber, 1);
                ShowAccount();
                ShowCard();
            }
        }

        public void mnuSave_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: intResponse As short, int --> As DialogResult
            DialogResult intResponse;
            clsDRWrapper clsSave = new clsDRWrapper();
            /*? On Error Resume Next  */
            try
            {
                if (boolSaving)
                    return;
                Debug.WriteLine("Save");
                boolSaving = true;
                BPGrid.Row = 0;
                BPGrid.Col = 0;
                GridExempts.Row = 0;
                GridExempts.Col = 0;
                // GridPhones.Row = 0
                SaleGrid.Row = 0;
                GridNeighborhood.Row = -1;
                GridZone.Row = -1;
                GridSecZone.Row = -1;
                GridTopography.Row = -1;
                GridUtilities.Row = -1;
                GridStreet.Row = -1;
                //Application.DoEvents();
                if (modGlobalVariables.Statics.boolInPendingMode)
                {
                    mnuSavePending_Click();
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    boolSaving = false;
                    return;
                }
                if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING1")
                {
                    frmNewREProperty.InstancePtr.GridDwelling1.Col = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING2")
                {
                    frmNewREProperty.InstancePtr.GridDwelling2.Col = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING3")
                {
                    frmNewREProperty.InstancePtr.GridDwelling3.Col = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDLAND")
                {
                    frmNewREProperty.InstancePtr.GridLand.Row = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDTRANCODE")
                {
                    frmNewREProperty.InstancePtr.gridTranCode.Row = -1;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDLANDCODE")
                {
                    frmNewREProperty.InstancePtr.gridLandCode.Row = -1;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDBLDGCODE")
                {
                    frmNewREProperty.InstancePtr.gridBldgCode.Row = -1;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDPROPERTYCODE")
                {
                    frmNewREProperty.InstancePtr.gridPropertyCode.Row = -1;
                }
                else if (frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPIInfCode1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPILand1Inf" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "mebPILand1Units" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPILand1Type" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIType1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIYear1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIUnits1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIGradeCd1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIGradePct1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROICond1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIPctPhys1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIPctFunct1")
                {
                    FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, (short)ActiveControl.GetIndex(), false);
                }
                else
                {
                    //FC:FINAL:MSH - i.issue #1026: for many items _Validate method isn't exist. In this case will be throwed an exception 
                    // and saving will be stoped
                    try
                    {
                        FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, false);
                    }
                    catch
                    {
                    }
                    //FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, false);
                    //Application.DoEvents();
                }
                if (CheckValues())
                {
                    SaveBPGrid();
                    // Call SaveGridPhones
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        mnuSaveIncome_Click();
                    }
                    // Call SaveData2
                    clsDRWrapper temp = modDataTypes.Statics.MR;
                    if (FillRecordsetFromScreen(ref temp))
                    {
                        modDataTypes.Statics.MR = temp;
                        SaveCard();
                    }
                    else
                    {
                        modDataTypes.Statics.MR = temp;
                        return;
                    }
                }
                if (boolCalcDataChanged)
                {
                    clsSave.Execute("update status set DataChanged = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
                    boolCalcDataChanged = false;
                }
                if ((boolSaleChanged) && (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity") + "") == 1) && (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "") > 0) && modGlobalVariables.Statics.boolRegRecords)
                {
                    intResponse = MessageBox.Show("The sale information for this property has changed." + "\r\n" + "Would you like to update the sales record?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (intResponse == DialogResult.Yes)
                    {
                        SaveSalesRec();
                    }
                    boolSaleChanged = false;
                }
                GridNeighborhood.Row = 0;
                GridZone.Row = 0;
                GridSecZone.Row = 0;
                GridTopography.Row = 0;
                GridUtilities.Row = 0;
                GridStreet.Row = 0;
                GridLand.Col = 0;
                GridLand.Row = 1;
                SaleGrid.Col = 0;
                // If BPGrid.Rows > 1 Then
                BPGrid.Row = BPGrid.Rows - 1;
                // End If
                GridExempts.Row = 1;
                // If GridPhones.Rows > 1 Then
                // GridPhones.Row = 1
                // End If
                SaleGrid.Row = 2;
                int x;
                // Set New Information so we can compare
                for (x = 0; x <= intTotalNumberOfControls - 1; x++)
                {
                    clsControlInfo[x].FillNewValue(this);
                }
                // Thsi function compares old and new values and creates change records for any differences
                modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
                // This function takes all the change records and writes them into the AuditChanges table in the database
                clsReportChanges.SaveToAuditChangesTable("Account Maintenance", modGlobalVariables.Statics.gintLastAccountNumber.ToString(), modGNBas.Statics.gintCardNumber.ToString());
                // Reset all information pertianing to changes and start again
                intTotalNumberOfControls = 0;
                clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
                clsReportChanges.Reset();
                // Initialize all the control and old data values
                FillControlInformationClass();
                FillControlInformationClassFromGrid();
                for (x = 0; x <= intTotalNumberOfControls - 1; x++)
                {
                    clsControlInfo[x].FillOldValue(this);
                }
                boolDataChanged = false;
                modGlobalVariables.Statics.DataChanged = false;
                if (!boolCalculating)
                {
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    //Application.DoEvents();
                    MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                boolSaving = false;
            }
            catch (Exception ex)
            {
                //FC:FINAL:MSH - i.issue #1026: if will be throwed an exception - next click on save btn won't be handled
                boolSaving = false;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuSave", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void mnuSave_Click()
        {
            mnuSave_Click(cmdSave, new System.EventArgs());
        }

        private void SaveBPGrid()
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strSQL;
            int x;
            // vbPorter upgrade warning: strSale As string	OnWrite(string, short)
            string strSale = "";
            string strTable = "";
            try
            {
                // On Error GoTo ErrorHandler
                for (x = 1; x <= BPGrid.Rows - 1; x++)
                {
                    if (x >= BPGrid.Rows)
                        break;
                    if (Strings.Trim(BPGrid.TextMatrix(x, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(x, BPPageCol)) == "")
                    {
                        BPGrid.RemoveItem(x);
                    }
                }
                // x
                if (BPGrid.Rows > 1)
                {
                    if (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) == "")
                        BPGrid.RemoveItem(BPGrid.Rows - 1);
                }
                BPGrid.AddItem(FCConvert.ToString(true));
                strSQL = "Delete from ";
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    strSQL += "BookPage";
                    strTable = "BookPage";
                }
                else
                {
                    strSQL += "SRBookPage";
                    strTable = "SRBookPage";
                }
                strSQL += " Where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = 1";
                if (!modGlobalVariables.Statics.boolRegRecords)
                {
                    strSQL += " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'";
                }
                clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
                if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate")))
                {
                    //FC:FINAL:MSH - i.issue #1026: incorrect convert datetime to int
                    //if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0)
                    if (FCConvert.ToDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") as object).ToOADate() != 0)
                    {
                        strSale = modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "";
                    }
                    else
                    {
                        strSale = FCConvert.ToString(0);
                    }
                }
                else
                {
                    strSale = FCConvert.ToString(0);
                }
                for (x = 1; x <= BPGrid.Rows - 2; x++)
                {
                    if (BPGrid.TextMatrix(x, BPCheckCol) == string.Empty)
                    {
                        strSQL = "0";
                    }
                    else
                    {
                        if (FCConvert.CBool(BPGrid.TextMatrix(x, BPCheckCol)) == true)
                        {
                            strSQL = "1";
                        }
                        else
                        {
                            strSQL = "0";
                        }
                    }
                    strSQL += ",'" + BPGrid.TextMatrix(x, BPBookCol) + "'";
                    strSQL += ",'" + BPGrid.TextMatrix(x, BPPageCol) + "'";
                    strSQL += ",'" + Strings.Trim(BPGrid.TextMatrix(x, BPSaleDateCol)) + "'";
                    strSQL += "," + FCConvert.ToString(x);
                    strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
                    strSQL += ",1";
                    if (strSale == "0")
                    {
                        strSQL += ",0";
                    }
                    else
                    {
                        strSQL += ",'" + strSale + "'";
                    }
                    clsTemp.Execute("insert into " + strTable + " ([current],book,page,bpdate,line,account,card,saledate) values (" + strSQL + ")", modGlobalVariables.strREDatabase);
                }
                // x
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "Occured in SaveBPGrid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuSaveIncome_Click()
        {
            clsDRWrapper clsSave = new clsDRWrapper();
            string strSQL = "";
            string strCols;
            int x;
            int lngSF = 0;
            int lngActRent;
            string strGN = "";
            int lngTemp = 0;
            clsSave.Execute("delete  from incomeapproach where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
            strCols = "(OccupancyCode,SquareFootage,GrossOrNet,ActualRent,ActualSFYear,MarketSFYear,Account,Card)";
            for (x = 2; x <= Grid1.Rows - 1; x++)
            {
                if (Conversion.Val(Grid1.TextMatrix(x, colOCCCode)) > 0)
                {
                    if (Conversion.Val(Grid1.TextMatrix(x, colSF)) > 0)
                    {
                        lngSF = FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(x, colSF)));
                        strSQL = "(" + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(x, colOCCCode))) + ",";
                        if (Grid1.TextMatrix(x, colGN) == "Net")
                        {
                            strGN = "Net";
                        }
                        else
                        {
                            strGN = "Gross";
                        }
                        strSQL += FCConvert.ToString(lngSF) + ",'" + strGN + "',";
                        if (Conversion.Val(Grid1.TextMatrix(x, colActRent)) > 0)
                        {
                            strSQL += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(Grid1.TextMatrix(x, colActRent)))) + ",";
                        }
                        else
                        {
                            strSQL += "0,";
                        }
                        strSQL += FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(x, colActSFYear))) + ",";
                        strSQL += FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(x, colMarketSFYear))) + ",";
                        strSQL += FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + ",";
                        strSQL += FCConvert.ToString(modGNBas.Statics.gintCardNumber) + ")";
                        clsSave.Execute("insert into IncomeApproach " + strCols + " values " + strSQL, modGlobalVariables.strREDatabase);
                    }
                }
            }
            // x
            clsSave.Execute("delete from incomeexpense where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
            strCols = "(Code,PercGross,[$SF],Amount,Account,Card)";
            for (x = 2; x <= GridExpenses.Rows - 1; x++)
            {
                if (Conversion.Val(GridExpenses.TextMatrix(x, colECode)) > 0)
                {
                    strSQL = "(";
                    strSQL += FCConvert.ToString(Conversion.Val(GridExpenses.TextMatrix(x, colECode))) + ",";
                    strSQL += FCConvert.ToString(Conversion.Val(GridExpenses.TextMatrix(x, colGross))) + ",";
                    if (Conversion.Val(GridExpenses.TextMatrix(x, colPerSF)) > 0)
                    {
                        strSQL += FCConvert.ToString(Conversion.Val(GridExpenses.TextMatrix(x, colPerSF))) + ",";
                    }
                    else
                    {
                        strSQL += "0,";
                    }
                    if (Conversion.Val(GridExpenses.TextMatrix(x, colAmount)) > 0)
                    {
                        strSQL += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(GridExpenses.TextMatrix(x, colAmount)))) + ",";
                    }
                    else
                    {
                        strSQL += "0,";
                    }
                    strSQL += FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + ",";
                    strSQL += FCConvert.ToString(modGNBas.Statics.gintCardNumber);
                    strSQL += ")";
                    clsSave.Execute("insert into incomeexpense " + strCols + " values " + strSQL, modGlobalVariables.strREDatabase);
                }
            }
            // x
            lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(GridValues.TextMatrix(8, 2))));
            if (lngTemp > 0)
            {
                lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(GridValues.TextMatrix(8, 2)));
            }
            clsSave.Execute("delete from incomevalue where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
            clsSave.Execute("Insert into incomevalue (vacancy,overallrate,indicatedvalue,account,card) values (" + FCConvert.ToString(Conversion.Val(GridValues.TextMatrix(1, 1))) + "," + FCConvert.ToString(Conversion.Val(GridValues.TextMatrix(5, 1))) + "," + FCConvert.ToString(lngTemp) + "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "," + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + ")", modGlobalVariables.strREDatabase);
        }

        private void SaveSalesRec()
        {
            clsDRWrapper BSaveMR;
            int BNewFile;
            clsDRWrapper srdwl;
            clsDRWrapper srout;
            clsDRWrapper srcom;
            int intResponse;
            string tstring = "";
            string t2string = "";
            bool boolSameName = false;
            bool boolTemp;
            int lngSaleID;
            if (!Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
            {
                return;
            }
            BSaveMR = modDataTypes.Statics.MR;
            // corey
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            modDataTypes.Statics.MR.Set_Fields("saledate", SaleGrid.TextMatrix(2, CNSTSALECOLDATE));
            clsDRWrapper temp = modDataTypes.Statics.SRMR;
            modREMain.OpenSRMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
            modDataTypes.Statics.SRMR = temp;
            lngSaleID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.SRMR.Get_Fields_Int32("saleid") + "")));
            if (lngSaleID == 0)
                lngSaleID = FCConvert.ToInt32(modDataTypes.Statics.SRMR.Get_Fields_Int32("id"));
            modDataTypes.Statics.MR.Set_Fields("saledate", SaleGrid.TextMatrix(2, CNSTSALECOLDATE));
            if (modDataTypes.Statics.SRMR.Get_Fields_Int32("rsaccount") > 0)
            {
                // if its > 0 then you have an entry for this account number on this date
                BringToFront();
                //Application.DoEvents();
                modGNBas.Statics.response = FCConvert.ToInt32(MessageBox.Show("There is already an entry for this account number with this sale date." + "\r\n" + "Would you like to overwrite this record?" + "\r\n" + "If not, a new entry will be created.", null, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question));
                if (Conversion.Val(modGNBas.Statics.response) == Conversion.Val(DialogResult.Cancel))
                {
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    return;
                }
                if (Conversion.Val(modGNBas.Statics.response) == Conversion.Val(DialogResult.No))
                {
                    boolSameName = false;
                    while (!modDataTypes.Statics.SRMR.EndOfFile())
                    {
                        tstring = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.SRMR.Get_Fields_String("RSNAME")));
                        lngSaleID = FCConvert.ToInt32(modDataTypes.Statics.SRMR.Get_Fields_Int32("saleid"));
                        t2string = theAccount.DeedName1;
                        if (Strings.StrComp(tstring, t2string, CompareConstants.vbTextCompare) == 0)
                        {
                            boolSameName = true;
                            break;
                        }
                        modDataTypes.Statics.SRMR.MoveNext();
                    }
                    if (boolSameName)
                    {
                        BringToFront();
                        //Application.DoEvents();
                        modGNBas.Statics.response = FCConvert.ToInt32(MessageBox.Show("The existing entry also has the same name as this account." + "\r\n" + "It is not valid for one person to sell or buy the same property twice on the same day." + "\r\n" + "You must overwrite this entry or cancel the save." + "\r\n" + "Is it OK to overwrite the entry?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning));
                        if (Conversion.Val(modGNBas.Statics.response) == Conversion.Val(DialogResult.Cancel))
                        {
                            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                            return;
                        }
                    }
                }
                // use cardnumber 100 so you cant get a match and it will open a new record
            }
            else
            {
                if (Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY)) != 1)
                {
                    BringToFront();
                    //Application.DoEvents();
                    if (MessageBox.Show("This will cause the sales information to be saved in the sales records even though this isn't an arms length sale." + "\r\n" + "Do you wish to continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        return;
                    }
                }
            }
            boolTemp = false;
            if (Conversion.Val(modGNBas.Statics.response) == Conversion.Val(DialogResult.Yes))
                boolTemp = true;
            modGlobalRoutines.SaveSaleRecord(ref boolTemp, ref modGlobalVariables.Statics.gintLastAccountNumber, ref lngSaleID);
            BringToFront();
            //Application.DoEvents();
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
        }

        public void mnuSaveQuit_Click(object sender, System.EventArgs e)
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_InCaseNoActiveControl = 1;
            const int curOnErrorGoToLabel_ErrorHandler = 2;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                // vbPorter upgrade warning: intResponse As short, int --> As DialogResult
                DialogResult intResponse;
                clsDRWrapper clsSave = new clsDRWrapper();
                vOnErrorGoToLabel = curOnErrorGoToLabel_InCaseNoActiveControl;
                /* On Error GoTo InCaseNoActiveControl */
                if (boolSaving)
                    return;
                boolSaving = true;
                Debug.WriteLine("savequit");
                BPGrid.Row = 0;
                BPGrid.Col = 0;
                GridExempts.Row = 0;
                GridExempts.Col = 0;
                SaleGrid.Row = 0;
                GridNeighborhood.Row = -1;
                GridZone.Row = -1;
                GridSecZone.Row = -1;
                GridTopography.Row = -1;
                GridUtilities.Row = -1;
                GridStreet.Row = -1;
                if (modGlobalVariables.Statics.boolInPendingMode)
                {
                    mnuSavePending_Click();
                    boolSaving = false;
                    Unload();
                    return;
                }
                if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING1")
                {
                    frmNewREProperty.InstancePtr.GridDwelling1.Col = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING2")
                {
                    frmNewREProperty.InstancePtr.GridDwelling2.Col = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDDWELLING3")
                {
                    frmNewREProperty.InstancePtr.GridDwelling3.Col = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDTRANCODE")
                {
                    frmNewREProperty.InstancePtr.gridTranCode.Row = -1;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDLAND")
                {
                    frmNewREProperty.InstancePtr.GridLand.Row = 0;
                }
                else if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "GRIDBLDGCODE")
                {
                    gridBldgCode.Row = -1;
                }
                else if (Strings.UCase(ActiveControl.GetName()) == "GRIDLANDCODE")
                {
                    gridLandCode.Row = -1;
                }
                else if (Strings.UCase(ActiveControl.GetName()) == "GRIDPROPERTYCODE")
                {
                    gridPropertyCode.Row = -1;
                }
                else if (frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPIInfCode1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPILand1Inf" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "mebPILand1Units" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMRPILand1Type" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIType1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIYear1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIUnits1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIGradeCd1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIGradePct1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROICond1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIPctPhys1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "txtMROIPctFunct1" || frmNewREProperty.InstancePtr.ActiveControl.GetName() == "optCardNumber")
                {
                    // matt is crazy
                    FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, (short)ActiveControl.GetIndex(), false);
                }
                else
                {
                    //FC:FINAL:MSH - i.issue #1025: for many items _Validate method isn't exist. In this case will be throwed an exception 
                    // and saving will be stoped. We will try to call Validate for ActiveControl and continue if method isn't exist
                    try
                    {
                        FCUtils.CallByName(frmNewREProperty.InstancePtr, frmNewREProperty.InstancePtr.ActiveControl.GetName() + "_Validate", CallType.Method, false);
                    }
                    catch
                    {
                    }
                }
            InCaseNoActiveControl:
                ;
                if (CheckValues())
                {
                    SaveBPGrid();
                    // Call SaveGridPhones
                    if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        mnuSaveIncome_Click();
                    }
                    // Call SaveData2
                    clsDRWrapper temp = modDataTypes.Statics.MR;
                    if (FillRecordsetFromScreen(ref temp))
                    {
                        modDataTypes.Statics.MR = temp;
                        SaveCard();
                    }
                    else
                    {
                        modDataTypes.Statics.MR = temp;
                        return;
                    }
                }
                if (boolCalcDataChanged)
                {
                    clsSave.Execute("update status set DataChanged = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
                    boolCalcDataChanged = false;
                }
                if ((boolSaleChanged) && (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity") + "") == 1) && (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "") > 0) && modGlobalVariables.Statics.boolRegRecords)
                {
                    //Application.DoEvents();
                    intResponse = MessageBox.Show("The sale information for this property has changed." + "\r\n" + "Would you like to update the sales record?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (intResponse == DialogResult.Yes)
                    {
                        SaveSalesRec();
                        if (Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
                        {
                            CopySalesBPGrid(Convert.ToDateTime(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)));
                        }
                    }
                    boolSaleChanged = false;
                }
                modGNBas.Statics.gboolFormLoaded = false;
                // BW 12/21/00 :Code Moved from cmdQuit_Click
                // Because it contined only one line
                modGlobalVariables.Statics.Quit = FCConvert.ToString(true);
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                // If Formisloaded("frmRESearch") Then
                // Unload frmRESearch
                // End If
                if (modGlobalRoutines.Formisloaded_2("frmGetAccount"))
                {
                    frmGetAccount.InstancePtr.BringToFront();
                }
                if (modGlobalVariables.Statics.boolFromBilling)
                {
                    clsDRWrapper clsTemp = new clsDRWrapper();
                    MDIParent.InstancePtr.boolLeavetoBilling = true;
                    MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkNone;
                    MDIParent.InstancePtr.txtCommSource.LinkTopic = "TWBL0000|frmREAudit";
                    MDIParent.InstancePtr.txtCommSource.LinkItem = "txtnotify";
                    MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkAutomatic;
                    clsTemp.OpenRecordset("select sum(rllandval) as landsum,sum(rlbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not rsdeleted = 1 and rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
                    /*? On Error Resume Next  */// because we don't care if it takes too long to process the sent message
                                                // and any errors should be taken care of in billing
                    clsTemp.Execute("update master set lastlandval = rllandval,lastbldgval = rlbldgval where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
                    modGlobalVariables.Statics.boolFromBilling = false;
                    //MDIParent.InstancePtr.Enabled = true;
                    //MDIParent.InstancePtr.Show();
                    // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    MDIParent.InstancePtr.txtCommSource.LinkExecute(FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("landsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("bldgsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("exemptsum"))));
                    Information.Err().Clear();
                    vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                    /* On Error GoTo ErrorHandler */// Call LeavetoBilling
                    return;
                }
                int x;
                // Set New Information so we can compare
                for (x = 0; x <= intTotalNumberOfControls - 1; x++)
                {
                    clsControlInfo[x].FillNewValue(this);
                }
                // Thsi function compares old and new values and creates change records for any differences
                modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
                // This function takes all the change records and writes them into the AuditChanges table in the database
                clsReportChanges.SaveToAuditChangesTable("Account Maintenance", modGlobalVariables.Statics.gintLastAccountNumber.ToString(), modGNBas.Statics.gintCardNumber.ToString());
                // Reset all information pertianing to changes and start again
                intTotalNumberOfControls = 0;
                clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
                clsReportChanges.Reset();
                // Initialize all the control and old data values
                FillControlInformationClass();
                FillControlInformationClassFromGrid();
                for (x = 0; x <= intTotalNumberOfControls - 1; x++)
                {
                    clsControlInfo[x].FillOldValue(this);
                }
                // x
                boolDataChanged = false;
                modGlobalVariables.Statics.DataChanged = false;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                boolSaving = false;
                Unload();
                // BW 12/27/00
                return;
            }
            catch (Exception ex)
            {
            ErrorHandler:
                ;
                boolSaving = false;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuSaveQuit", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: dtSaleDate As DateTime	OnWrite(string)	OnRead(string)
        private void CopySalesBPGrid(DateTime dtSaleDate)
        {
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strSQL;
            int x;
            // vbPorter upgrade warning: strSale As string	OnWrite(DateTime)
            string strSale;
            string strTable;
            try
            {
                // On Error GoTo ErrorHandler
                for (x = 1; x <= BPGrid.Rows - 1; x++)
                {
                    if (x >= BPGrid.Rows)
                        break;
                    if (Strings.Trim(BPGrid.TextMatrix(x, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(x, BPPageCol)) == "")
                    {
                        BPGrid.RemoveItem(x);
                    }
                }
                // x
                if (BPGrid.Rows > 1)
                {
                    if (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) == "")
                        BPGrid.RemoveItem(BPGrid.Rows - 1);
                }
                if (BPGrid.Rows > 2)
                {
                    BPGrid.RowSel = 1;
                    BPGrid.Row = BPGrid.Rows - 1;
                    BPGrid.ColSel = BPPageCol;
                    BPGrid.Col = BPPageCol;
                    BPGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
                    BPGrid.ColSel = BPBookCol;
                    BPGrid.Col = BPBookCol;
                    BPGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
                }
                // now put the blank line back
                BPGrid.AddItem(FCConvert.ToString(true));
                strSQL = "Delete from ";
                strSQL += "SRBookPage";
                strTable = "SRBookPage";
                strSQL += " Where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = 1";
                strSQL += " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'";
                clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
                strSale = FCConvert.ToString(dtSaleDate);
                for (x = 1; x <= BPGrid.Rows - 2; x++)
                {
                    if (BPGrid.TextMatrix(x, BPCheckCol) == string.Empty)
                    {
                        strSQL = "0";
                    }
                    else
                    {
                        if (FCConvert.CBool(BPGrid.TextMatrix(x, BPCheckCol)) == true)
                        {
                            strSQL = "1";
                        }
                        else
                        {
                            strSQL = "0";
                        }
                    }
                    strSQL += ",'" + BPGrid.TextMatrix(x, BPBookCol) + "'";
                    strSQL += ",'" + BPGrid.TextMatrix(x, BPPageCol) + "'";
                    strSQL += ",'" + Strings.Trim(BPGrid.TextMatrix(x, BPSaleDateCol)) + "'";
                    strSQL += "," + FCConvert.ToString(x);
                    strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
                    strSQL += ",1";
                    if (strSale == "0")
                    {
                        strSQL += ",0";
                    }
                    else
                    {
                        strSQL += ",'" + strSale + "'";
                    }
                    clsTemp.Execute("insert into " + strTable + " ([current],book,page,bpdate,line,account,card,saledate) values (" + strSQL + ")", modGlobalVariables.strREDatabase);
                }
                // x
                CopySalesMaplot(dtSaleDate);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "Occured in CopySalesBPGrid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void CopySalesMaplot(DateTime dtSaleDate)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper clsLoad = new clsDRWrapper();
                clsDRWrapper clsSave = new clsDRWrapper();
                string strSQL;
                strSQL = "Delete from SRMapLot where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'";
                clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
                clsLoad.OpenRecordset("select * from maplot where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by line", modGlobalVariables.strREDatabase);
                clsSave.OpenRecordset("select * from srmaplot where account = -1", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    clsSave.AddNew();
                    clsSave.Set_Fields("account", modGlobalVariables.Statics.gintLastAccountNumber);
                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                    clsSave.Set_Fields("line", clsLoad.Get_Fields("line"));
                    clsSave.Set_Fields("maplot", clsLoad.Get_Fields_String("maplot"));
                    clsSave.Set_Fields("mpdate", clsLoad.Get_Fields_String("mpdate"));
                    clsSave.Set_Fields("saledate", dtSaleDate);
                    clsSave.Update();
                    clsLoad.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CopySalesMapLot", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void cmdLogSale_Click(object sender, System.EventArgs e)
        {
            SaveSalesRec();
            if (Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
            {
                CopySalesBPGrid(Convert.ToDateTime(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)));
            }
            MessageBox.Show("Save Operation Complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void mnuCalculate_Click(object sender, System.EventArgs e)
        {
            int NumRecords;
            int lngSaleID;
            try
            {
                // On Error GoTo ErrorHandler
                if (modGlobalVariables.Statics.DataChanged)
                {
                    if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        return;
                    }
                }
                modGlobalVariables.Statics.boolfromcalcandassessment = false;
                modProperty.Statics.boolFromProperty = false;
                cmdCalculate.Enabled = false;
                cmbCardNumber.Focus();
                //Application.DoEvents();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                modProperty.Statics.RUNTYPE = "D";
                modGlobalRoutines.Check_Arrays();
                modSpeedCalc.Statics.boolCalcErrors = false;
                modSpeedCalc.Statics.CalcLog = "";
                modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
                // Call FillCostArray
                // need to open lsr table before calling fillarrays
                // Don't know if 500 is correct record this time
                // 500 is the general table?
                // other records are for individual land schedules?
                boolCalculating = true;
                mnuSave_Click();
                // must save values before calculating
                modDwelling.Statics.lngSFLATotal = 0;
                //Application.DoEvents();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode") + "")));
                }
                // Call OpenLSRTable(LSR, 31)
                // Call FillArrays
                // Call Fill_Some_Arrays(31)
                modGlobalRoutines.Check_Some_Arrays();
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    modDataTypes.Statics.MR.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.CMR.OpenRecordset("select * from commercial where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by rscard", modGlobalVariables.strREDatabase);
                }
                else
                {
                    modDataTypes.Statics.MR.OpenRecordset("select * from srmaster where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'  order by rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.DWL.OpenRecordset("select * from srdwelling where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'  order by rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.OUT.OpenRecordset("select * from sroutbuilding where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'  order by rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.CMR.OpenRecordset("select * from srcommercial where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'  order by rscard", modGlobalVariables.strREDatabase);
                }
                modDataTypes.Statics.MR.MoveLast();
                modDataTypes.Statics.MR.MoveFirst();
                NumRecords = modDataTypes.Statics.MR.RecordCount();
                modDataTypes.Statics.MR.Edit();
                modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
                // Call DisplayCalculate
                Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
                modSpeedCalc.Statics.SummaryListIndex = 0;
                // Load frmNewValuationReport
                // frmNewValuationReport.Hide
                //Application.DoEvents();
                modSpeedCalc.CalcCard();
                boolCalculating = false;
                // Screen.MousePointer = vbDefault
                modGlobalVariables.Statics.DataChanged = false;
                modGlobalVariables.Statics.intCurrentCard = 1;
                optCardNumber_MouseDown_242(modGlobalVariables.Statics.intCurrentCard - 1, 1, 0, 1, 1);
                modSpeedCalc.Statics.boolCalcErrors = false;
                modSpeedCalc.Statics.CalcLog = "";
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                boolCalculating = false;
                cmdCalculate.Enabled = true;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuCalculate_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: Index As short	OnWriteFCConvert.ToInt32(
        private void optCardNumber_MouseDown_242(int Index, short Button, short Shift, float x, float y)
        {
            optCardNumber_MouseDown(Index, ref Button, ref Shift, ref x, ref y);
        }

        private void optCardNumber_MouseDown(int Index, ref short Button, ref short Shift, ref float x, ref float y)
        {
            int intCardNumber = Index + 1;
            if (Button == 1)
            {
                if (modGlobalVariables.Statics.DataChanged)
                {
                    if (MessageBox.Show("Save changes to card: " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (CheckValues())
                        {
                            modGlobalRoutines.SaveData2();
                            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        }
                    }
                }
                boolCalcDataChanged = false;
                modGlobalVariables.Statics.DataChanged = false;
                modGNBas.Statics.gintCardNumber = intCardNumber;
                // now you must reopen the masterrecord for this card
                LoadCard(modGlobalVariables.Statics.CurrentAccount, modGNBas.Statics.gintCardNumber);
                ShowCard();
                // Call OpenMasterTable(MR, CurrentAccount, gintCardNumber)
                if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "Y")
                {
                    if (SSTab1.SelectedIndex == 3)
                        SSTab1.SelectedIndex = 2;
                }
                else if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "C")
                {
                    if (SSTab1.SelectedIndex == 2)
                        SSTab1.SelectedIndex = 3;
                }
                else
                {
                    // the account has neither data
                    if ((SSTab1.SelectedIndex == 2) || (SSTab1.SelectedIndex == 3))
                    {
                        SSTab1.SelectedIndex = 0;
                    }
                }
                modGlobalVariables.Statics.HoldDwelling = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "");
                // Call FillTheScreen
                // optCardNumber(Index).Value = True
                cmbCardNumber.Text = intCardNumber.ToString();
                cmbCardNumber_Validate(false);
            }
        }

        private void mnuValuationReport_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
                modProperty.Statics.RUNTYPE = "P";
                modGlobalVariables.Statics.boolUseArrays = true;
                mnuValuationReport.Enabled = false;
                modGlobalRoutines.Check_Arrays();
                boolCalculating = true;
                mnuSave_Click();
                // must save values before calculating
                modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
                modGlobalVariables.Statics.gintMinAccountRange = modGlobalVariables.Statics.gintLastAccountNumber;
                modGlobalVariables.Statics.gintMaxAccountRange = modGlobalVariables.Statics.gintLastAccountNumber;
                modPrintRoutines.Statics.gstrFieldName = "RSAccount";
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                //FC:FINAL:MSH - move code into action, which will be called at the end of the report, because in web report isn't called as modal window 
                //and data will be reinitialized before closing report (indexes of data will be changed in report and second running this handler will throw an exceptions)
                //rptRangeValuation.InstancePtr.Init(1, 1, -1, true);
                rptRangeValuation.InstancePtr.Init(1, 1, true, -1, true, reportClosed: () =>
                {
                    boolCalculating = false;
                    mnuValuationReport.Enabled = true;
                    modGlobalVariables.Statics.DataChanged = false;
                    modGlobalVariables.Statics.intCurrentCard = 1;
                    optCardNumber_MouseDown_242(modGlobalVariables.Statics.intCurrentCard - 1, 1, 0, 1, 1);
                    modSpeedCalc.Statics.boolCalcErrors = false;
                    modSpeedCalc.Statics.CalcLog = "";
                });
                return;
            }
            catch (Exception ex)
            {
                boolCalculating = false;
                mnuValuationReport.Enabled = true;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuValuationReport_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuViewDocs_Click(object sender, System.EventArgs e)
        {
            ViewDocs();
        }

        private void ViewDocs()
        {
            if (modGlobalVariables.Statics.gintLastAccountNumber <= 0) return;
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("", "RealEstate", "",
                modGlobalVariables.Statics.gintLastAccountNumber, "", true, !boolEditDocuments));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand(){DataGroup = "RealEstate",ReferenceType = "",ReferenceId = modGlobalVariables.Statics.gintLastAccountNumber }).Result;
            imgDocuments.Visible = documentHeaders.Count > 0;
        }

        private void mnuQuit_Click(object sender, System.EventArgs e)
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_ErrorHandler = 1;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                // BW 12/21/00 :Code Moved from cmdQuit_Click
                // Because it contined only one line
                modGNBas.Statics.gboolFormLoaded = false;
                modGlobalVariables.Statics.Quit = FCConvert.ToString(true);
                // If Formisloaded("frmRESearch") Then
                frmRESearch.InstancePtr.Unload();
                // End If
                if (modGlobalRoutines.Formisloaded_2("frmGetAccount"))
                {
                    frmGetAccount.InstancePtr.BringToFront();
                }
                if (modGlobalVariables.Statics.boolFromBilling)
                {
                    clsDRWrapper clsTemp = new clsDRWrapper();
                    MDIParent.InstancePtr.boolLeavetoBilling = true;
                    MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkNone;
                    MDIParent.InstancePtr.txtCommSource.LinkTopic = "TWBL0000|frmREAudit";
                    MDIParent.InstancePtr.txtCommSource.LinkItem = "txtnotify";
                    MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkAutomatic;
                    clsTemp.OpenRecordset("select sum(rllandval) as landsum,sum(rlbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not rsdeleted = 1 and rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
                    /*? On Error Resume Next  */// because we don't care if it takes too long to process the sent message
                                                // and any errors should be taken care of in billing
                    clsTemp.Execute("update master set lastlandval = rllandval,lastbldgval = rlbldgval where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
                    modGlobalVariables.Statics.boolFromBilling = false;
                    //MDIParent.InstancePtr.Enabled = true;
                    //MDIParent.InstancePtr.Show();
                    // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    MDIParent.InstancePtr.txtCommSource.LinkExecute(FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("landsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("bldgsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("exemptsum"))));
                    Information.Err().Clear();
                    vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                    /* On Error GoTo ErrorHandler */// Call LeavetoBilling
                    return;
                }
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                Unload();
                // BW 12/27/00
                return;
            }
            catch (Exception ex)
            {
                //ErrorHandler:;
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuQuit_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void mnuQuit_Click()
        {
            mnuQuit_Click(mnuQuit, new System.EventArgs());
        }

        private void frmNewREProperty_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Insert)
            {
                if (modGNBas.Statics.OverType == 1)
                {
                    modGNBas.Statics.OverType = 0;
                }
                else
                {
                    modGNBas.Statics.OverType = 1;
                }
                if (ActiveControl is FCTextBox)
                {
                    (ActiveControl as FCTextBox).SelectionLength = modGNBas.Statics.OverType;
                }
                return;
            }
            if (KeyCode == Keys.Escape)
            {
                KeyCode = (Keys)0;
                mnuQuit_Click();
                return;
            }
            if (FCGlobal.Screen.ActiveControl is FCTextBox)
            {
                if (Shift == 2)
                {
                    if (KeyCode == Keys.Left || KeyCode == Keys.Right)
                        KeyCode = (Keys)0;
                }
                else
                {
                    if (KeyCode == Keys.Left || KeyCode == Keys.Right)
                        (FCGlobal.Screen.ActiveControl as FCTextBox).SelectionLength = 0;
                }
            }
        }

        private void frmNewREProperty_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // Call fixmaskeditboxlength(KeyAscii)
            if (KeyAscii == Keys.Return)
            {
                if ((ActiveControl.GetName() != "txtMRRSLocapt") && (ActiveControl.GetName() != "txtMRPILand1Type") && (ActiveControl.GetName() != "mebMRDIInspectionDate") && (ActiveControl.GetName() != "mebDateInspected") && (ActiveControl.GetName() != "txtMROIType1") && (ActiveControl.GetName() != "MaskEdBox2"))
                {
                    KeyAscii = (Keys)0;
                    // If FormattingUnits = True Then
                    // FormattingUnits = False
                    // Else
                    Support.SendKeys("{TAB}", false);
                    // End If
                }
            }
            else if (KeyAscii == Keys.Back)
            {
            }
            else if (KeyAscii == Keys.Escape)
            {
            }
            else if ((KeyAscii == Keys.Execute) && (SSTab1.SelectedIndex == 3))
            {
                // If you're on the outbuilding tab then the plus key
                // should put you on the next line
                if (ActiveControl.GetIndex() < 9)
                {
                    txtMROIType1[FCConvert.ToInt16(ActiveControl.GetIndex() + 1)].Focus();
                }
                else
                {
                    if (MaskEdbox3.Enabled)
                        MaskEdbox3.Focus();
                }
                KeyAscii = (Keys)0;
            }
            else
            {
                if (modGNBas.Statics.OverType > 0 && ActiveControl is FCTextBox && Strings.UCase(FCConvert.ToString(ActiveControl.Tag)) != "LAND")
                {
                    (ActiveControl as FCTextBox).SelectionLength = modGNBas.Statics.OverType;
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmNewREProperty_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            try
            {
                // On Error GoTo ENDROUTINE
                // If KeyCode = vbKeyDown Then
                // KeyCode = 0
                // SendKeys "{TAB}"
                // End If
                switch (SSTab1.SelectedIndex)
                {
                    case 1:
                    case 2:
                        {
                            // dwelling and commercial
                            if (((KeyCode >= Keys.D0) && (KeyCode <= Keys.D9)) || ((KeyCode >= Keys.NumPad0) && (KeyCode <= Keys.NumPad9)))
                            {
                                if (frmNewREProperty.InstancePtr.ActiveControl is FCTextBox)
                                {
                                    if ((frmNewREProperty.InstancePtr.ActiveControl as FCTextBox).MaxLength > 0)
                                    {
                                        if ((frmNewREProperty.InstancePtr.ActiveControl as FCTextBox).MaxLength <= frmNewREProperty.InstancePtr.ActiveControl.Text.Length)
                                        {
                                            Support.SendKeys("{TAB}", false);
                                            return;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                }
                //end switch
                if (KeyCode == Keys.Snapshot)
                {
                    // print screen
                    mnuPrintScreen_Click();
                }
                else if (KeyCode == Keys.Return)
                {
                    KeyCode = (Keys)0;
                    return;
                }
                else if (KeyCode == Keys.F9)
                {
                    return;
                }
                else if (frmNewREProperty.InstancePtr.ActiveControl is FCTextBox)
                {
                    if (Strings.UCase(frmNewREProperty.InstancePtr.ActiveControl.GetName()) == "TXTCARDNUMBER")
                        return;
                    // If KeyCode = vbKeyInsert Then SetInsertMode (Screen.ActiveControl.SelLength)
                    // Screen.ActiveControl.SelLength = OverType
                    if (modGlobalVariables.Statics.DataChanged == false)
                    {
                        if (KeyCode == Keys.Return)
                        {
                        }
                        else if (KeyCode == Keys.Tab)
                        {
                        }
                        else if (KeyCode == Keys.Escape)
                        {
                        }
                        else if (KeyCode >= Keys.F1 && KeyCode <= Keys.F16)
                        {
                        }
                        else if (KeyCode == Keys.Back)
                        {
                            modGlobalVariables.Statics.DataChanged = true;
                        }
                        else if (KeyCode <= Keys.Down)
                        {
                        }
                        else
                        {
                            modGlobalVariables.Statics.DataChanged = true;
                        }
                    }
                    if (!modGlobalVariables.Statics.boolInPendingMode)
                    {
                        if ((Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "LAND") || (Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "DWELLING") || (Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "COMMERCIAL") || (Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "OUTBUILDING"))
                        {
                            boolCalcDataChanged = true;
                        }
                    }
                    if ((frmNewREProperty.InstancePtr.ActiveControl as FCTextBox).SelectionStart == (frmNewREProperty.InstancePtr.ActiveControl as FCTextBox).MaxLength)
                    {
                        if (((frmNewREProperty.InstancePtr.ActiveControl.Name != "txtMROIType1") && (frmNewREProperty.InstancePtr.ActiveControl.Name != "mebDateInspected")) && (Conversion.Val(Strings.Trim(frmNewREProperty.InstancePtr.ActiveControl.Text + "")) > 0))
                        {
                            if ((KeyCode != Keys.Up) && (KeyCode != Keys.Down))
                            {
                                KeyCode = (Keys)0;
                                Support.SendKeys("{TAB}", false);
                            }
                        }
                        return;
                    }
                    else
                    {
                        // If KeyCode = vbKeyDown And (SSTab1.Tab > 0) Then
                        // SendKeys "{tab}"
                        // End If
                    }
                }
                else if (frmNewREProperty.InstancePtr.ActiveControl is Global.T2KBackFillWhole || frmNewREProperty.InstancePtr.ActiveControl is Global.T2KDateBox)
                {
                    // If KeyCode = vbKeyInsert Then SetInsertMode (Screen.ActiveControl.SelLength)
                    (frmNewREProperty.InstancePtr.ActiveControl as TextBoxBase).SelectionLength = modGNBas.Statics.OverType;
                    if ((frmNewREProperty.InstancePtr.ActiveControl as TextBoxBase).SelectionStart == (frmNewREProperty.InstancePtr.ActiveControl as TextBoxBase).MaxLength)
                        Support.SendKeys("{TAB}", false);
                }
                else if (frmNewREProperty.InstancePtr.ActiveControl is FCGrid)
                {
                    if (!modGlobalVariables.Statics.boolInPendingMode)
                    {
                        if ((Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "LAND") || (Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "DWELLING") || (Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "COMMERCIAL") || (Strings.UCase(FCConvert.ToString(frmNewREProperty.InstancePtr.ActiveControl.Tag)) == "OUTBUILDING"))
                        {
                            boolCalcDataChanged = true;
                        }
                    }
                }
                if (modGlobalVariables.Statics.DataChanged == false)
                {
                    if (KeyCode == Keys.Return)
                    {
                    }
                    else if (KeyCode == Keys.Tab)
                    {
                    }
                    else if (KeyCode == Keys.Escape)
                    {
                    }
                    else if (KeyCode >= Keys.F1 && KeyCode <= Keys.F16)
                    {
                    }
                    else if (KeyCode <= Keys.Down)
                    {
                    }
                    else
                    {
                        modGlobalVariables.Statics.DataChanged = true;
                    }
                }
            }
            catch
            {
                // ENDROUTINE:
                FCGlobal.Screen.MousePointer = 0;
            }
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            // vbPorter upgrade warning: answer As short, int --> As DialogResult
            DialogResult answer;
            // vbPorter upgrade warning: intResponse As short, int --> As DialogResult
            DialogResult intResponse;
            clsDRWrapper clsSave = new clsDRWrapper();
            // If UnloadMode = vbAppTaskManager Then
            // Cancel = True
            // End If
            // boolFromSalesAnalysis = False
            if (modGlobalVariables.Statics.DataChanged == true && !modGlobalVariables.Statics.boolInPendingMode && cmdSave.Enabled)
            {
                answer = MessageBox.Show("Data has been changed but not saved. Would you like to save it now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    // yes
                    if (CheckValues())
                    {
                        SaveBPGrid();
                        // Call SaveGridPhones
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            mnuSaveIncome_Click();
                        }
                        // Call SaveData2
                        clsDRWrapper temp = modDataTypes.Statics.MR;
                        if (FillRecordsetFromScreen(ref temp))
                        {
                            modDataTypes.Statics.MR = temp;
                            SaveCard();
                            // Else
                            // Exit Sub
                        }
                        else
                        {
                            modDataTypes.Statics.MR = temp;
                        }
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    }
                    if (boolCalcDataChanged)
                    {
                        clsSave.Execute("update status set DataChanged = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
                        boolCalcDataChanged = false;
                    }
                    if ((boolSaleChanged) && (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity") + "") == 1) && (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "") > 0) && modGlobalVariables.Statics.boolRegRecords)
                    {
                        intResponse = MessageBox.Show("The sale information for this property has changed." + "\r\n" + "Would you like to update the sales record?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (intResponse == DialogResult.Yes)
                        {
                            SaveSalesRec();
                        }
                        boolSaleChanged = false;
                    }
                    e.Cancel = false;
                    modGlobalVariables.Statics.DataChanged = false;
                    boolCalcDataChanged = false;
                }
                else if (answer == DialogResult.No)
                {
                    // no
                    e.Cancel = false;
                    modGlobalVariables.Statics.DataChanged = false;
                    boolCalcDataChanged = false;
                }
                else if (answer == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    BringToFront();
                    return;
                }
            }
            modGlobalVariables.Statics.DataChanged = false;
            boolCalcDataChanged = false;
            // Unload frmREProperty2
            if (e.CloseReason == FCCloseReason.FormControlMenu)
            {
                if (modGlobalVariables.Statics.boolFromBilling)
                {
                    e.Cancel = true;
                    return;
                }
                e.Cancel = false;
                modGlobalVariables.Statics.Quit = FCConvert.ToString(true);
            }
            modGNBas.Statics.gboolFormLoaded = false;
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            char[] HOLDNAME = new char[34];
            char[] holdSecN = new char[34];
            char[] holdAdd1 = new char[34];
            char[] holdAdd2 = new char[34];
            char[] holdCity = new char[26];
            char[] holdStat = new char[2];
            char[] holdZip1 = new char[5];
            char[] holdZip2 = new char[4];
            int fnx1;
            clsDRWrapper clsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                modGlobalVariables.Statics.ComingFromTheHelpScreen = false;
                if (modGlobalVariables.Statics.boolFromSalesAnalysis)
                {
                    modGlobalVariables.Statics.boolFromSalesAnalysis = false;
                    if (modGlobalVariables.Statics.boolBillingorCorrelated)
                    {
                        clsTemp.OpenRecordset("select sum(lastlandval) as landsum, sum(lastbldgval) as bldgsum from srmaster where saleid = " + modDataTypes.Statics.MR.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        clsTemp.OpenRecordset("select sum(rllandval) as landsum,sum(rlbldgval) as bldgsum from srmaster where saleid = " + modDataTypes.Statics.MR.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
                    }
                    frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(frmShowSalesAnalysis.InstancePtr.Grid1.Row, 10, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "")));
                    frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(frmShowSalesAnalysis.InstancePtr.Grid1.Row, 2, Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate"), "MM/dd/yyyy"));
                    // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                    frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(frmShowSalesAnalysis.InstancePtr.Grid1.Row, 8, FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("landsum") + "")));
                    // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                    frmShowSalesAnalysis.InstancePtr.Grid1.TextMatrix(frmShowSalesAnalysis.InstancePtr.Grid1.Row, 9, FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("bldgsum") + "")));
                    frmShowSalesAnalysis.InstancePtr.Calculate_Cells();
                    frmShowSalesAnalysis.InstancePtr.BringToFront();
                }
                else if (modGlobalVariables.Statics.boolFromBilling)
                {
                }
                else
                {
                    if (!modGNBas.Statics.gboolExiting)
                    {
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            frmGetAccount.InstancePtr.Show(App.MainForm);
                            frmGetAccount.InstancePtr.cmbtrecordtype.Text = "Regular Records";
                            modGlobalVariables.Statics.boolRegRecords = true;
                        }
                        else
                        {
                            frmGetAccount.InstancePtr.Show(App.MainForm);
                            frmGetAccount.InstancePtr.cmbtrecordtype.Text = "Sales Records";
                            modGlobalVariables.Statics.boolRegRecords = false;
                        }
                    }
                }
                modGNBas.Statics.gboolLoadingDone = false;
                modGlobalVariables.Statics.DataChanged = false;
                boolCalcDataChanged = false;
                frmPictureChange.InstancePtr.Unload();
                lngCurrentPendingID = 0;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In frmREProperty_Unload", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuRECLComment_Click(object sender, System.EventArgs e)
        {
            if (modGlobalVariables.Statics.gintLastAccountNumber > 0)
            {
                frmRECLComment.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber);
            }
        }

        private void mnuCollectionsNote_Click(object sender, System.EventArgs e)
        {
            if (modGlobalVariables.Statics.gintLastAccountNumber > 0)
            {
                frmCLNote.InstancePtr.Init(ref modGlobalVariables.Statics.gintLastAccountNumber);
            }
        }

        private void mnuinterestedparties_Click(object sender, System.EventArgs e)
        {
            frmInterestedParties.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, "RE", 0, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTEDITADDRESSES)) == "F");
            CheckForInterestedParties(modGlobalVariables.Statics.gintLastAccountNumber);
        }

        private void mnuCopyProperty_Click(object sender, System.EventArgs e)
        {
            object CopyNumber = 0;
            string strTemp = "";
            string strTempDwelling;
            string[] strAry = null;
            int x;
            int y;
            clsDRWrapper clsTemp = new clsDRWrapper();
            clsDRWrapper clsTemp2 = new clsDRWrapper();
            strTempDwelling = Strings.Trim(modGlobalVariables.Statics.HoldDwelling + "");
            if (!frmInput.InstancePtr.Init(ref CopyNumber, "Copy from Which Account?", "Input the account number you wish to copy from", 825, false, modGlobalConstants.InputDTypes.idtWholeNumber))
            {
                return;
            }
            if (Information.IsNumeric(CopyNumber))
            {
                strTemp = frmListChoice.InstancePtr.Init("Owner Information;Neighborhood, Location etc.;Land;Dwelling;Outbuildings;Book & Page;Entrance Code, Date", "Copy Property Information", "Choose what information to copy", true);
                if (Conversion.Val(strTemp) < 0)
                {
                    // cancelled
                    return;
                }
                if (FCConvert.ToInt32(CopyNumber) != 0)
                {
                    modREMain.OpenMasterTable(ref modGlobalVariables.Statics.MR2, CopyNumber, 1);
                    modGlobalVariables.Statics.HoldDwelling = strTempDwelling;
                    if (strTemp != string.Empty)
                    {
                        strAry = Strings.Split(strTemp, ";", -1, CompareConstants.vbTextCompare);
                        for (x = 0; x <= Information.UBound(strAry, 1); x++)
                        {
                            if (Conversion.Val(strAry[x]) == 1)
                            {
                                if (theAccount.OwnerID > 0)
                                {
                                    MessageBox.Show("Ownership can only be assigned, not changed with this method." + "\r\n" + "To change ownership select a new owner by clicking on the search button.");
                                }
                                else
                                {
                                    // owner info
                                    modDataTypes.Statics.MR.Set_Fields("OwnerPartyID", FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.MR2.Get_Fields_Int32("ownerpartyid"))));
                                    modDataTypes.Statics.MR.Set_Fields("SecOwnerPartyID", FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.MR2.Get_Fields_Int32("secownerpartyID"))));
                                    theAccount.OwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modGlobalVariables.Statics.MR2.Get_Fields_Int32("ownerpartyid"))));
                                    theAccount.SecOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modGlobalVariables.Statics.MR2.Get_Fields_Int32("secownerpartyid"))));
                                    cParty tParty = new cParty();
                                    cPartyController tCont = new cPartyController();
                                    tParty = theAccount.OwnerParty;
                                    tCont.LoadParty(ref tParty, theAccount.OwnerID);
                                    if (theAccount.SecOwnerID > 0)
                                    {
                                        tParty = theAccount.SecOwnerParty;
                                        tCont.LoadParty(ref tParty, theAccount.SecOwnerID);
                                    }
                                    else
                                    {
                                        theAccount.SecOwnerParty.Clear();
                                    }
                                    modDataTypes.Statics.MR.Set_Fields("DeedName1", modGlobalVariables.Statics.MR2.Get_Fields_String("DeedName1"));
                                    modDataTypes.Statics.MR.Set_Fields("DeedName2", modGlobalVariables.Statics.MR2.Get_Fields_String("DeedName2"));
                                    theAccount.DeedName1 =
                                        modGlobalVariables.Statics.MR2.Get_Fields_String("DeedName1");
                                    theAccount.DeedName2 =
                                        modGlobalVariables.Statics.MR2.Get_Fields_String("DeedName2");
                                }
                            }
                            else if (Conversion.Val(strAry[x]) == 2)
                            {
                                // neighborhood etc
                                txtMRRSMAPLOT.Text = modGlobalVariables.Statics.MR2.Get_Fields_String("rsmaplot") + "";
                                modDataTypes.Statics.MR.Set_Fields("rsmaplot", modGlobalVariables.Statics.MR2.Get_Fields_String("rsmaplot"));
                                modDataTypes.Statics.MR.Set_Fields("rslocnumalph", modGlobalVariables.Statics.MR2.Get_Fields_String("rslocnumalph"));
                                modDataTypes.Statics.MR.Set_Fields("rslocstreet", modGlobalVariables.Statics.MR2.Get_Fields_String("rslocstreet"));
                                modDataTypes.Statics.MR.Set_Fields("rslocapt", modGlobalVariables.Statics.MR2.Get_Fields_String("rslocapt"));
                                modDataTypes.Statics.MR.Set_Fields("rsref1", modGlobalVariables.Statics.MR2.Get_Fields_String("rsref1"));
                                modDataTypes.Statics.MR.Set_Fields("rsref2", modGlobalVariables.Statics.MR2.Get_Fields_String("rsref2"));
                                modDataTypes.Statics.MR.Set_Fields("pineighborhood", modGlobalVariables.Statics.MR2.Get_Fields_Int32("pineighborhood") + "");
                                // TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
                                modDataTypes.Statics.MR.Set_Fields("pixcoord", modGlobalVariables.Statics.MR2.Get_Fields("pixcoord"));
                                // TODO Get_Fields: Check the table for the column [Piycoord] and replace with corresponding Get_Field method
                                modDataTypes.Statics.MR.Set_Fields("PIycoord", modGlobalVariables.Statics.MR2.Get_Fields("Piycoord"));
                                modDataTypes.Statics.MR.Set_Fields("pizone", modGlobalVariables.Statics.MR2.Get_Fields_Int32("pizone"));
                                modDataTypes.Statics.MR.Set_Fields("piseczone", modGlobalVariables.Statics.MR2.Get_Fields_Int32("piseczone"));
                                modDataTypes.Statics.MR.Set_Fields("pitopography1", modGlobalVariables.Statics.MR2.Get_Fields_Int32("pitopography1"));
                                modDataTypes.Statics.MR.Set_Fields("pitopography2", modGlobalVariables.Statics.MR2.Get_Fields_Int32("pitopography2"));
                                modDataTypes.Statics.MR.Set_Fields("piutilities1", modGlobalVariables.Statics.MR2.Get_Fields_Int32("piutilities1"));
                                modDataTypes.Statics.MR.Set_Fields("piutilities2", modGlobalVariables.Statics.MR2.Get_Fields_Int32("piutilities2"));
                                modDataTypes.Statics.MR.Set_Fields("pistreet", modGlobalVariables.Statics.MR2.Get_Fields_Int32("pistreet"));
                                modDataTypes.Statics.MR.Set_Fields("piopen1", modGlobalVariables.Statics.MR2.Get_Fields_Int32("piopen1"));
                                modDataTypes.Statics.MR.Set_Fields("piopen2", modGlobalVariables.Statics.MR2.Get_Fields_Int32("piopen2"));
                                modDataTypes.Statics.MR.Set_Fields("ritrancode", modGlobalVariables.Statics.MR2.Get_Fields_Int32("ritrancode") + "");
                                modDataTypes.Statics.MR.Set_Fields("rilandcode", modGlobalVariables.Statics.MR2.Get_Fields_Int32("rilandcode") + "");
                                modDataTypes.Statics.MR.Set_Fields("ribldgcode", modGlobalVariables.Statics.MR2.Get_Fields_Int32("ribldgcode") + "");
                            }
                            else if (Conversion.Val(strAry[x]) == 3)
                            {
                                // land
                                for (y = 1; y <= 7; y++)
                                {
                                    // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                                    modDataTypes.Statics.MR.Set_Fields("piland" + y + "Type", modGlobalVariables.Statics.MR2.Get_Fields("piland" + FCConvert.ToString(y) + "type"));
                                    // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                                    modDataTypes.Statics.MR.Set_Fields("piland" + y + "UnitsA", modGlobalVariables.Statics.MR2.Get_Fields("piland" + FCConvert.ToString(y) + "UnitsA"));
                                    // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                                    modDataTypes.Statics.MR.Set_Fields("piland" + y + "UnitsB", modGlobalVariables.Statics.MR2.Get_Fields("piland" + FCConvert.ToString(y) + "UnitsB"));
                                    // TODO Get_Fields: Field [Piland] not found!! (maybe it is an alias?)
                                    modDataTypes.Statics.MR.Set_Fields("Piland" + y + "Inf", modGlobalVariables.Statics.MR2.Get_Fields("Piland" + FCConvert.ToString(y) + "Inf"));
                                    // TODO Get_Fields: Field [PIland] not found!! (maybe it is an alias?)
                                    modDataTypes.Statics.MR.Set_Fields("PIland" + y + "InfCode", modGlobalVariables.Statics.MR2.Get_Fields("PIland" + FCConvert.ToString(y) + "InfCode"));
                                }
                                // y
                            }
                            else if (Conversion.Val(strAry[x]) == 4)
                            {
                                // dwelling
                                clsTemp.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(CopyNumber) + " and rscard = 1", modGlobalVariables.strREDatabase);
                                if (!clsTemp.EndOfFile())
                                {
                                    if (strTempDwelling != "C")
                                    {
                                        modGlobalVariables.Statics.HoldDwelling = "Y";
                                        modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "Y");
                                        clsDRWrapper temp = modDataTypes.Statics.DWL;
                                        modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, theAccount.AccountID);
                                        modDataTypes.Statics.DWL = temp;
                                        modDataTypes.Statics.DWL.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                                        modDataTypes.Statics.DWL.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                                        modDataTypes.Statics.DWL.Set_Fields("Distyle", clsTemp.Get_Fields_Int32("distyle"));
                                        modDataTypes.Statics.DWL.Set_Fields("diunitsdwelling", clsTemp.Get_Fields_Int32("diunitsdwelling"));
                                        modDataTypes.Statics.DWL.Set_Fields("diunitsother", clsTemp.Get_Fields_Int32("diunitsother"));
                                        modDataTypes.Statics.DWL.Set_Fields("distories", clsTemp.Get_Fields_Int32("distories"));
                                        modDataTypes.Statics.DWL.Set_Fields("diextwalls", clsTemp.Get_Fields_Int32("diextwalls"));
                                        modDataTypes.Statics.DWL.Set_Fields("diroof", clsTemp.Get_Fields_Int32("diroof"));
                                        modDataTypes.Statics.DWL.Set_Fields("disfmasonry", clsTemp.Get_Fields_Int32("disfmasonry"));
                                        modDataTypes.Statics.DWL.Set_Fields("diopen3", clsTemp.Get_Fields_Int32("diopen3"));
                                        modDataTypes.Statics.DWL.Set_Fields("diopen4", clsTemp.Get_Fields_Int32("diopen4"));
                                        modDataTypes.Statics.DWL.Set_Fields("diyearbuilt", clsTemp.Get_Fields_Int32("diyearbuilt"));
                                        modDataTypes.Statics.DWL.Set_Fields("diyearremodel", clsTemp.Get_Fields_Int32("diyearremodel"));
                                        modDataTypes.Statics.DWL.Set_Fields("difoundation", clsTemp.Get_Fields_Int32("difoundation"));
                                        modDataTypes.Statics.DWL.Set_Fields("dibsmt", clsTemp.Get_Fields_Int32("dibsmt"));
                                        modDataTypes.Statics.DWL.Set_Fields("dibsmtgar", clsTemp.Get_Fields_Int32("dibsmtgar"));
                                        modDataTypes.Statics.DWL.Set_Fields("diwetbsmt", clsTemp.Get_Fields_Int32("diwetbsmt"));
                                        modDataTypes.Statics.DWL.Set_Fields("disfbsmtliving", clsTemp.Get_Fields_Int32("disfbsmtliving"));
                                        modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade1", clsTemp.Get_Fields_Int32("dibsmtfingrade1"));
                                        modDataTypes.Statics.DWL.Set_Fields("dibsmtfingrade2", clsTemp.Get_Fields_Int32("dibsmtfingrade2"));
                                        modDataTypes.Statics.DWL.Set_Fields("diopen5", clsTemp.Get_Fields_Int32("diopen5"));
                                        modDataTypes.Statics.DWL.Set_Fields("diheat", clsTemp.Get_Fields_Int32("diheat"));
                                        modDataTypes.Statics.DWL.Set_Fields("dipctheat", clsTemp.Get_Fields_Int32("dipctheat"));
                                        modDataTypes.Statics.DWL.Set_Fields("dicool", clsTemp.Get_Fields_Int32("dicool"));
                                        modDataTypes.Statics.DWL.Set_Fields("dipctcool", clsTemp.Get_Fields_Int32("dipctcool"));
                                        modDataTypes.Statics.DWL.Set_Fields("dikitchens", clsTemp.Get_Fields_Int32("dikitchens"));
                                        modDataTypes.Statics.DWL.Set_Fields("dibaths", clsTemp.Get_Fields_Int32("dibaths"));
                                        modDataTypes.Statics.DWL.Set_Fields("dirooms", clsTemp.Get_Fields_Int32("dirooms"));
                                        modDataTypes.Statics.DWL.Set_Fields("dibedrooms", clsTemp.Get_Fields_Int32("dibedrooms"));
                                        modDataTypes.Statics.DWL.Set_Fields("difullbaths", clsTemp.Get_Fields_Int32("difullbaths"));
                                        modDataTypes.Statics.DWL.Set_Fields("dihalfbaths", clsTemp.Get_Fields_Int32("dihalfbaths"));
                                        modDataTypes.Statics.DWL.Set_Fields("diaddnfixtures", clsTemp.Get_Fields_Int32("diaddnfixtures"));
                                        modDataTypes.Statics.DWL.Set_Fields("difireplaces", clsTemp.Get_Fields_Int32("difireplaces"));
                                        modDataTypes.Statics.DWL.Set_Fields("dilayout", clsTemp.Get_Fields_Int32("dilayout"));
                                        modDataTypes.Statics.DWL.Set_Fields("diattic", clsTemp.Get_Fields_Int32("diattic"));
                                        modDataTypes.Statics.DWL.Set_Fields("diinsulation", clsTemp.Get_Fields_Int32("diinsulation"));
                                        modDataTypes.Statics.DWL.Set_Fields("dipctunfinished", clsTemp.Get_Fields_Int32("dipctunfinished"));
                                        modDataTypes.Statics.DWL.Set_Fields("digrade1", clsTemp.Get_Fields_Int32("digrade1"));
                                        modDataTypes.Statics.DWL.Set_Fields("digrade2", clsTemp.Get_Fields_Int32("digrade2"));
                                        modDataTypes.Statics.DWL.Set_Fields("disqft", clsTemp.Get_Fields_Int32("disqft"));
                                        modDataTypes.Statics.DWL.Set_Fields("dicondition", clsTemp.Get_Fields_Int32("dicondition"));
                                        modDataTypes.Statics.DWL.Set_Fields("dipctphys", clsTemp.Get_Fields_Int32("dipctphys"));
                                        modDataTypes.Statics.DWL.Set_Fields("difunctcode", clsTemp.Get_Fields_Int32("difunctcode"));
                                        modDataTypes.Statics.DWL.Set_Fields("dipctfunct", clsTemp.Get_Fields_Int32("dipctfunct"));
                                        modDataTypes.Statics.DWL.Set_Fields("dipctecon", clsTemp.Get_Fields_Int32("dipctecon"));
                                        modDataTypes.Statics.DWL.Set_Fields("dieconcode", clsTemp.Get_Fields_Int32("dieconcode"));
                                        modDataTypes.Statics.DWL.Set_Fields("disfla", clsTemp.Get_Fields_Int32("disfla"));
                                    }
                                    else
                                    {
                                        MessageBox.Show("This property is currently commercial.  Cannot copy over dwelling information", "Cannot Copy Dwelling", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                }
                            }
                            else if (Conversion.Val(strAry[x]) == 5)
                            {
                                // outbuildings
                                clsTemp.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(CopyNumber) + " and rscard = 1", modGlobalVariables.strREDatabase);
                                if (!clsTemp.EndOfFile())
                                {
                                    clsDRWrapper temp = modDataTypes.Statics.OUT;
                                    modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                                    modDataTypes.Statics.OUT = temp;
                                    modDataTypes.Statics.MR.Set_Fields("rsoutbuildingcode", "Y");
                                    modDataTypes.Statics.OUT.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                                    modDataTypes.Statics.OUT.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                                    for (y = 1; y <= 10; y++)
                                    {
                                        // TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oitype" + y, clsTemp.Get_Fields("oitype" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oiyear" + y, clsTemp.Get_Fields("oiyear" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + y, clsTemp.Get_Fields("oiunits" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oigradecd" + y, clsTemp.Get_Fields("oigradecd" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oigradepct" + y, clsTemp.Get_Fields("oigradepct" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oicond" + y, clsTemp.Get_Fields("oicond" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oipctphys" + y, clsTemp.Get_Fields("oipctphys" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oipctfunct" + y, clsTemp.Get_Fields("oipctfunct" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oisoundvalue" + y, clsTemp.Get_Fields("oisoundvalue" + FCConvert.ToString(y)));
                                        // TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
                                        modDataTypes.Statics.OUT.Set_Fields("oiusesound" + y, clsTemp.Get_Fields("oiusesound" + FCConvert.ToString(y)));
                                    }
                                    // y
                                }
                            }
                            else if (Conversion.Val(strAry[x]) == 6)
                            {
                                // book / page
                                clsTemp.Execute("delete from bookpage where account = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
                                clsTemp.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(CopyNumber), modGlobalVariables.strREDatabase);
                                clsTemp2.OpenRecordset("select * from bookpage where account = -1", modGlobalVariables.strREDatabase);
                                BPGrid.Rows = 1;
                                while (!clsTemp.EndOfFile())
                                {
                                    clsTemp2.AddNew();
                                    BPGrid.Rows += 1;
                                    // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                    clsTemp2.Set_Fields("BOOK", clsTemp.Get_Fields("book"));
                                    // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                                    clsTemp2.Set_Fields("Page", clsTemp.Get_Fields("Page"));
                                    clsTemp2.Set_Fields("bpdate", clsTemp.Get_Fields_String("bpdate"));
                                    clsTemp2.Set_Fields("account", modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
                                    // TODO Get_Fields: Check the table for the column [card] and replace with corresponding Get_Field method
                                    clsTemp2.Set_Fields("card", clsTemp.Get_Fields("card"));
                                    clsTemp2.Set_Fields("saledate", clsTemp.Get_Fields_DateTime("saledate"));
                                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                                    clsTemp2.Set_Fields("line", clsTemp.Get_Fields("line"));
                                    clsTemp2.Set_Fields("current", clsTemp.Get_Fields_Boolean("current"));
                                    clsTemp2.Update();
                                    if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("current")))
                                    {
                                        BPGrid.TextMatrix(BPGrid.Rows - 1, 0, FCConvert.ToString(true));
                                    }
                                    // TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
                                    BPGrid.TextMatrix(BPGrid.Rows - 1, 1, FCConvert.ToString(clsTemp.Get_Fields("book")));
                                    // TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
                                    BPGrid.TextMatrix(BPGrid.Rows - 1, 2, FCConvert.ToString(clsTemp.Get_Fields("page")));
                                    BPGrid.TextMatrix(BPGrid.Rows - 1, 3, FCConvert.ToString(clsTemp.Get_Fields_String("bpdate")));
                                    clsTemp.MoveNext();
                                }
                            }
                            else if (Conversion.Val(strAry[x]) == 7)
                            {
                                // entrance code
                                // TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
                                modDataTypes.Statics.MR.Set_Fields("ENTRANCEcode", FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.MR2.Get_Fields("entrancecode") + "")));
                                // TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
                                modDataTypes.Statics.MR.Set_Fields("informationcode", FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.MR2.Get_Fields("informationcode") + "")));
                                modDataTypes.Statics.MR.Set_Fields("dateinspected", modGlobalVariables.Statics.MR2.Get_Fields_DateTime("dateinspected"));
                            }
                        }
                        // x
                    }
                    // Call FillScreenWithoutLoadingMR
                    ShowOwner();
                    ShowSecOwner();
                    ShowAddress();
                    ShowDeeds();
                    ShowCard(true);
                    MessageBox.Show("Information Copied", "Copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void mnuNextAccountFromSearch_Click(object sender, System.EventArgs e)
        {
            string tstring;
            int intCounter;
            int oldrow;
            GridLand.Row = 0;
            BPGrid.Row = 0;
            BPGrid.Col = 0;
            GridExempts.Row = 0;
            GridExempts.Col = 0;
            GridZone.Row = -1;
            GridSecZone.Row = -1;
            GridTopography.Row = -1;
            GridUtilities.Row = -1;
            GridStreet.Row = -1;
            GridLand.Col = 0;
            GridLand.Row = 1;
            SaleGrid.Col = 0;
            // If BPGrid.Rows > 1 Then
            BPGrid.Row = BPGrid.Rows - 1;
            //Application.DoEvents();
            sketchesViewModel = null;
            pictureViewModel = null;
            if (modGlobalVariables.Statics.DataChanged == true && !modGlobalVariables.Statics.boolInPendingMode && cmdSave.Enabled)
            {
                if (MessageBox.Show("Data has been changed but not saved. Would you like to save it now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SaveBPGrid();
                    clsDRWrapper temp = modDataTypes.Statics.MR;
                    if (FillRecordsetFromScreen(ref temp))
                    {
                        modDataTypes.Statics.MR = temp;
                        SaveCard();
                    }
                    else
                    {
                        modDataTypes.Statics.MR = temp;
                    }
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                }
            }
            modGlobalVariables.Statics.DataChanged = false;
            boolCalcDataChanged = false;
            imSketch.Image = null;
            
            lblSketchNumber.Text = "";
            imgPicture.Image = null;
            lblPicName.Text = "Unable to find File";
            txtPicDesc.Text = "Enter Comment or Description Here.";
            SSTab1.SelectedIndex = 0;
            oldrow = frmRESearch.InstancePtr.Grid.Row;
        keepsearching:
            ;
            modGNBas.Statics.LongScreenSearch = true;
            if (frmRESearch.InstancePtr.Grid.Row < frmRESearch.InstancePtr.Grid.Rows - 1)
            {
                frmRESearch.InstancePtr.Grid.Row += 1;
            }
            else
            {
                frmRESearch.InstancePtr.Grid.Row = oldrow;
                return;
            }
            tstring = frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 7);
            if (tstring == "DEL")
            {
                goto keepsearching;
            }
            modGlobalVariables.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 5))));
            modGlobalVariables.Statics.gcurrsaledate = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 3));
            modGlobalVariables.Statics.gintLastAccountNumber = modGlobalVariables.Statics.CurrentAccount;
            modGNBas.Statics.response = "X";
            modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", Conversion.Str(modGlobalVariables.Statics.gintLastAccountNumber));
            clsDRWrapper tLoad = new clsDRWrapper();
            tLoad.OpenRecordset("select count(rscard) as thecount from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
            if (!tLoad.EndOfFile())
            {
                // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(Math.Round(Conversion.Val(tLoad.Get_Fields("thecount"))));
            }
            if (modGNBas.Statics.gintMaxCards < 1)
                modGNBas.Statics.gintMaxCards = 1;
            LoadForm();
            lblNumCards.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
            cmbCardNumber.Text = FCConvert.ToString(1);
            cmbCardNumber_Validate(false);
            // FillTheScreen
            lngOriginalOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ownerpartyid"))));
            lngPOOwnerID = lngOriginalOwnerID;
            lngPOSecOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("secownerpartyid"))));
            strPODeedName1 = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");
            strPODeedName2 = modDataTypes.Statics.MR.Get_Fields_String("DeedName2");
            // TODO Get_Fields: Field [OwnerAddress1] not found!! (maybe it is an alias?)
            strPOAddr1 = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("OwnerAddress1")));
            // TODO Get_Fields: Field [owneraddress2] not found!! (maybe it is an alias?)
            strPOAddr2 = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("owneraddress2")));
            // TODO Get_Fields: Field [ownercity] not found!! (maybe it is an alias?)
            strPOCity = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("ownercity")));
            // TODO Get_Fields: Field [ownerstate] not found!! (maybe it is an alias?)
            strPOState = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("ownerstate")));
            // TODO Get_Fields: Field [Ownerzip] not found!! (maybe it is an alias?)
            strPOZip = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields("Ownerzip")));
            if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate") + ""))
            {
                dtOriginalSaleDate = (DateTime)modDataTypes.Statics.MR.Get_Fields_DateTime("saledate");
            }
            else
            {
                dtOriginalSaleDate = DateTime.FromOADate(0);
            }
            GridNeighborhood.Row = 0;
            GridZone.Row = 0;
            GridSecZone.Row = 0;
            GridTopography.Row = 0;
            GridUtilities.Row = 0;
            GridStreet.Row = 0;
            GridLand.Col = 0;
            GridLand.Row = 1;
            SaleGrid.Col = 0;
            // If BPGrid.Rows > 1 Then
            BPGrid.Row = BPGrid.Rows - 1;
            // End If
            GridExempts.Row = 1;
            SaleGrid.Row = 2;
        }

        private void ChangeOwnership()
        {
            if (!(theAccount == null))
            {
                if (theAccount.OwnerID > 0)
                {
                    int lngReturn = 0;
                    lngReturn = frmCentralPartySearch.InstancePtr.Init();
                    if (lngReturn > 0)
                    {
                        if (lngReturn != theAccount.OwnerID)
                        {
                            boolNameChanged = true;
                        }
                    }
                }
            }
        }
        // vbPorter upgrade warning: intSketchNum As short	OnWriteFCConvert.ToInt32(
        private void AddASketch(ref int intSketchNum)
        {
            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
                return;
            }
            string strTemp;
            string strCard = "";
            double dblSkRatio;
            try
            {
                var sketchIdentifier = Guid.NewGuid();
                sketchesViewModel.Sketches.AddPendingSketch(new SketchInfo()
                {
                    DateUpdated = DateTime.MinValue,
                    SequenceNumber = sketchesViewModel.Sketches.Count() + 1,
                    SketchIdentifier = sketchIdentifier,
                    Account = theAccount.Account,
                    CardIdentifier = Guid.Parse(modDataTypes.Statics.MR.Get_Fields_String("CardID")),
                    Card    = modDataTypes.Statics.MR.Get_Fields_Int32("RSCard")
                });
                var sketchImageFileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".bmp");
                imSketch.Image = null;
                imgSketchLastModified = DateTime.MinValue;
                string sketchFileNamePath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "sketches", sketchIdentifier.ToString() + ".skt");
                using (MemoryStream zipMemoryStream = new MemoryStream())
                {
                    using (var archive = new ZipArchive(zipMemoryStream, ZipArchiveMode.Create, true))
                    {
                        var sketchFileName = Path.GetFileName(sketchFileNamePath);
                        dynamic sketchData = new DynamicObject();
                        sketchData.Action = "new";
                        sketchData.FileName = Path.GetFileName(sketchFileNamePath);
                        var uploadUrl = Application.StartupUrl;
                        uploadUrl = uploadUrl.EndsWith("/") ? uploadUrl : uploadUrl + "/";
                        uploadUrl = uploadUrl + "FileUpload.ashx";
                        sketchData.uploadUrl = uploadUrl;
                        sketchData.serverDestinationFolder = Path.GetDirectoryName(sketchFileNamePath);
                        sketchData.ServerPath = sketchFileNamePath;
                        sketchData.data = new DynamicObject();
                        sketchData.data.fileNo = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
                        cPartyAddress tParty;
                        tParty = theAccount.OwnerParty.GetPrimaryAddress();
                        sketchData.data.Borrower = theAccount.OwnerParty.FullNameLastFirst;
                        if (!(tParty == null))
                        {
                            sketchData.data.state = tParty.State;
                            sketchData.data.city = tParty.City;
                            sketchData.data.zipCode = tParty.Zip;
                            sketchData.data.clientAddress = Strings.Trim(tParty.Address1 + " " + tParty.Address2);
                        }
                        else
                        {
                            sketchData.data.city = "";
                            sketchData.data.state = "";
                            sketchData.data.borrower = "";
                            sketchData.data.zipCode = "";
                            sketchData.data.clientAddress = "";
                        }
                        sketchData.data.propertyAddress = Strings.Trim(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocapt")) + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocstreet"));
                        string sketchDataSerialized = WisejSerializer.Serialize(sketchData);

                        var dataFileEntry = archive.CreateEntry(sketchFileName.Replace(".skt", ".data"));
                        
                        using (var zipStream = dataFileEntry.Open())
                        {
                            byte[] sktDataBytes = Encoding.ASCII.GetBytes(sketchDataSerialized);
                            zipStream.Write(sktDataBytes, 0, sktDataBytes.Length);
                        }
                    }
                    zipMemoryStream.Position = 0;
                    string downloadLink = FCUtils.GetDownloadLink(zipMemoryStream, Path.GetFileName(sketchFileNamePath).Replace(".skt", ".trio-skt"));
                    clsPrinterFunctions.DownloadTRIOSketchFile(downloadLink);
                    //FCUtils.Download(zipMemoryStream, Path.GetFileName(sketchFileNamePath).Replace(".skt", ".trio-skt"));
                }
              
               return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In add a sketch. Line " + Information.Erl(), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuNewSketch_Click(object sender, System.EventArgs e)
        {
            int x;
            x = sketchesViewModel.Sketches.Count() + 1;
            AddASketch(ref x);          
        }

        private void mnuEditConfig_Click(object sender, System.EventArgs e)
        {
            frmWinsktCfg.InstancePtr.Show(App.MainForm);
        }

        private void mnuReassignSketch_Click(object sender, System.EventArgs e)
        {
            frmReassignSketch.InstancePtr.Init(ref modGlobalVariables.Statics.gintLastAccountNumber, ref modGNBas.Statics.gintCardNumber);
            ReLoadSketches();
        }

        private void mnuDeleteSketch_Click(object sender, System.EventArgs e)
        {
            if (!sketchesViewModel.Sketches.HasCurrentSketch())
            {
                FCMessageBox.Show("There is no sketch file to delete", MsgBoxStyle.Information | MsgBoxStyle.OkOnly,
                    "No Sketch File");
                return;
            }


            DialogResult intResp;
            intResp = MessageBox.Show("This will permanently delete the picture and its sketch file." + "\r\n" + "Do you wish to continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (intResp != DialogResult.Yes)
                return;

            appWinSketch = null;
            sketchesViewModel.DeleteCurrentSketch();            
            imSketch.Image = null;
            ReLoadSketches();
        }

        private dynamic GetCalcInfoFromCurrentSketch()
        {
            if (!sketchesViewModel.Sketches.HasCurrentSketch())
            {
                return null;
            }

            var sketch = sketchesViewModel.Sketches.CurrentSketch();
            if (!string.IsNullOrWhiteSpace(sketch.Calculations))
            {
                return  WisejSerializer.Parse(sketch.Calculations);
            }

            return null;
        }

        private void mnuAll_Click(object sender, System.EventArgs e)
        {
            var calcInfo = GetCalcInfoFromCurrentSketch();
            if (calcInfo != null)
            {
                TransferOutbuildingSqft(calcInfo);
            }            
        }
        // vbPorter upgrade warning: CalcInfo As object	OnRead(string, string)
        private void TransferCommercialSqft(object[] CalcInfo)
        {
            int x;
            int[] lngSQFT = new int[2 + 1];
            int[] intOCC = new int[2 + 1];
            string[] strOCC = new string[2 + 1];
            bool boolNew;
            string strTemp = "";
            // If CMR Is Nothing Then Exit Sub
            // If CMR.EOF Or CMR.BOF Then Exit Sub
            if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == "Y" || (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "Y")
            {
                MessageBox.Show("This card has a dwelling.  Cannot transfer commercial sqft.", "Cannot transfer sqft", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            boolNew = false;
            if (modDataTypes.Statics.CMR == null)
            {
                boolNew = true;
            }
            else if (modDataTypes.Statics.CMR.EndOfFile() || modDataTypes.Statics.CMR.BeginningOfFile())
            {
                boolNew = true;
            }
            if (boolNew)
            {
                clsDRWrapper temp = modDataTypes.Statics.CMR;
                modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                temp.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                temp.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                modREMain.SaveCommercialTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.CMR = temp;
                modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "C");
                modGlobalVariables.Statics.HoldDwelling = "C";
            }
            lngSQFT[1] = 0;
            lngSQFT[2] = 0;
            intOCC[1] = 0;
            intOCC[2] = 0;
            strOCC[1] = "";
            strOCC[2] = "";
            //if (Information.UBound(CalcInfo, 1) >= 0)
            //{
            //	for (x = 0; x <= Information.UBound(CalcInfo, 1); x++)
            //	{
            if (Strings.UCase(Strings.Trim(FCConvert.ToString(CalcInfo[2]))) == "COMMERCIAL AREA")
            {
                if (lngSQFT[1] == 0)
                {
                    lngSQFT[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(CalcInfo[1])));
                    strTemp = FCConvert.ToString(CalcInfo[0]);
                    strTemp = Strings.Mid(strTemp, 1, Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare));
                    intOCC[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                    strOCC[1] = FCConvert.ToString(CalcInfo[0]);
                }
                else
                {
                    lngSQFT[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(CalcInfo[2])));
                    // intOCC(2) = Val(CalcInfo(x, 0))
                    strTemp = FCConvert.ToString(CalcInfo[0]);
                    strTemp = Strings.Mid(strTemp, 1, Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare));
                    intOCC[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                    strOCC[2] = FCConvert.ToString(CalcInfo[0]);
                }
                //	}
                //}
                // x
                if (lngSQFT[1] > 0)
                {
                    if ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "") != Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2") + "")) || (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "") == 0))
                    {
                        if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "") == 0)
                        {
                            // no buildings set up yet so put the sqft anywhere
                            modDataTypes.Statics.CMR.Set_Fields("occ1", intOCC[1]);
                            modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[1]);
                            modDataTypes.Statics.CMR.Set_Fields("occ2", intOCC[2]);
                            modDataTypes.Statics.CMR.Set_Fields("c2floor", lngSQFT[2]);
                            return;
                        }
                        else if ((intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "")) && (intOCC[2] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2") + "")))
                        {
                            modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[1]);
                            modDataTypes.Statics.CMR.Set_Fields("c2floor", lngSQFT[2]);
                            return;
                        }
                        else if ((intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2") + "")) && (intOCC[2] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "")))
                        {
                            modDataTypes.Statics.CMR.Set_Fields("c2floor", lngSQFT[1]);
                            modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[2]);
                            return;
                        }
                        else if ((lngSQFT[2] == 0) && (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2")) == 0))
                        {
                            if (intOCC[1] > 0)
                            {
                                modDataTypes.Statics.CMR.Set_Fields("occ1", intOCC[1]);
                            }
                            modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[1]);
                            return;
                        }
                        else if ((lngSQFT[2] == 0) && ((intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1"))) || (intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2")))))
                        {
                            if (intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + ""))
                            {
                                modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[1]);
                            }
                            else
                            {
                                modDataTypes.Statics.CMR.Set_Fields("c2floor", lngSQFT[1]);
                            }
                            return;
                        }
                        else if ((intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "") && intOCC[1] > 0) || (intOCC[2] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2") + "") && intOCC[2] > 0))
                        {
                            if (intOCC[1] > 0)
                            {
                                modDataTypes.Statics.CMR.Set_Fields("occ1", intOCC[1]);
                            }
                            if (intOCC[2] > 0)
                            {
                                modDataTypes.Statics.CMR.Set_Fields("occ2", intOCC[2]);
                            }
                            modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[1]);
                            modDataTypes.Statics.CMR.Set_Fields("c2floor", lngSQFT[2]);
                            return;
                        }
                        else if ((intOCC[1] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2") + "") && intOCC[1] > 0) || (intOCC[2] == Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "") && intOCC[2] > 0))
                        {
                            if (intOCC[1] > 0)
                            {
                                modDataTypes.Statics.CMR.Set_Fields("occ2", intOCC[1]);
                            }
                            if (intOCC[2] > 0)
                            {
                                modDataTypes.Statics.CMR.Set_Fields("occ1", intOCC[2]);
                            }
                            modDataTypes.Statics.CMR.Set_Fields("c1floor", lngSQFT[2]);
                            modDataTypes.Statics.CMR.Set_Fields("c2floor", lngSQFT[1]);
                            return;
                        }
                    }
                    // end of if occ1 and occ2 not equal or zero
                    // if you are here then nothing matched and we must ask
                    //! Load frmTransferSQFT;
                    frmTransferSQFT.InstancePtr.GridInfo.Rows = 3;
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(0, 0, "Area #");
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(0, 1, "Code / Description");
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(0, 2, "SQFT");
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(1, 0, FCConvert.ToString(1));
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(1, 1, strOCC[1]);
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(1, 2, FCConvert.ToString(lngSQFT[1]));
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(2, 0, FCConvert.ToString(2));
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(2, 1, strOCC[2]);
                    frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(2, 2, FCConvert.ToString(lngSQFT[2]));
                    frmTransferSQFT.InstancePtr.GridBuildings.Rows = 3;
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(0, 0, "Building");
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(0, 1, "Code");
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(0, 2, "Area #");
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(1, 0, FCConvert.ToString(1));
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "")));
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(2, 0, FCConvert.ToString(2));
                    frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(2, 1, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2") + "")));
                    frmTransferSQFT.InstancePtr.boolFromCommercial = true;
                    frmTransferSQFT.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                }
            }
            return;
        }
        // vbPorter upgrade warning: CalcInfo As object	OnRead(string)
        private void TransferOutbuildingSqft(object[] CalcInfo)
        {
            int x;
            string[] strCode = new string[10 + 1];
            int[] intCode = new int[15 + 1];
            int[] lngSQFT = new int[15 + 1];
            int[] intOutCode = new int[10 + 1];
            int y;
            bool boolCanAuto;
            // can we automatically transfer?
            int intNumBuildings;
            int[] intMatch = new int[10 + 1];
            int intNummatches = 0;
            string strTemp = "";
            bool boolNew;
            // If OUT Is Nothing Then Exit Sub
            // If OUT.EOF Or OUT.BOF Then Exit Sub
            boolNew = false;
            if (modDataTypes.Statics.OUT == null)
            {
                boolNew = true;
            }
            else if (modDataTypes.Statics.OUT.EndOfFile() || modDataTypes.Statics.OUT.BeginningOfFile())
            {
                boolNew = true;
            }
            if (boolNew)
            {
                clsDRWrapper temp = modDataTypes.Statics.OUT;
                modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                temp.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                temp.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.MR.Set_Fields("RSOUTBUILDINGCODE", "Y");
                modREMain.SaveOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.OUT = temp;
            }
            for (x = 1; x <= 15; x++)
            {
                if (x < 11)
                {
                    strCode[x] = "";
                    // TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
                    intOutCode[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(x)))));
                }
                intCode[x] = 0;
                lngSQFT[x] = 0;
            }
            // x
            y = 0;
            //if (Information.UBound(CalcInfo, 1) >= 0)
            //{
            //	for (x = 0; x <= Information.UBound(CalcInfo, 1); x++)
            //	{
            if (Strings.UCase(Strings.Trim(FCConvert.ToString(CalcInfo[2]))) == "OUTBUILDING ARE")
            {
                y += 1;
                lngSQFT[y] = FCConvert.ToInt32(Math.Round(Conversion.Val(CalcInfo[1])));
                strTemp = Strings.Trim(FCConvert.ToString(CalcInfo[0]));
                strTemp = Strings.Mid(strTemp, 1, Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare));
                // intCode(Y) = Val(CalcInfo(x, 0))
                intCode[y] = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                strCode[y] = FCConvert.ToString(CalcInfo[0]);
            }
            //	}
            //	// x
            //}
            intNumBuildings = y;
            // check to see if we can transfer automatically
            // if there is more than one of the same code we can't
            // if there is more than 10 outbuildings we can't
            // if there are uncoded buildings we can't
            // if there were no outbuildings previously we can
            boolCanAuto = true;
            // check for too many buildings
            if (intNumBuildings > 10)
                boolCanAuto = false;
            if (boolCanAuto)
            {
                // check to see if we have buildings set up or not
                if (intOutCode[1] > 0)
                {
                    // check for uncoded buildings
                    for (x = 1; x <= intNumBuildings; x++)
                    {
                        if (lngSQFT[x] > 0 && intCode[x] == 0)
                            boolCanAuto = false;
                    }
                    // x
                    if (boolCanAuto)
                    {
                        // still true so check for dups
                        for (x = 1; x <= intNumBuildings; x++)
                        {
                            for (y = 1; y <= intNumBuildings; y++)
                            {
                                if (x != y)
                                {
                                    if ((intCode[x] == intCode[y]) && intCode[x] != 0)
                                    {
                                        boolCanAuto = false;
                                        break;
                                    }
                                }
                            }
                            // y
                            if (!boolCanAuto)
                                break;
                        }
                        // x
                        if (boolCanAuto)
                        {
                            // check if everything matches okay
                            // there are no dups and no uncodeds
                            // as long as everything in the transfer matches only one building in the account
                            // we can do it automatically
                            FCUtils.EraseSafe(intMatch);
                            for (x = 1; x <= 10; x++)
                            {
                                // x will loop through outbuildings
                                for (y = 1; y <= intNumBuildings; y++)
                                {
                                    // y will loop through the transfer codes
                                    if (intOutCode[x] == intCode[y])
                                    {
                                        // record the location of the match
                                        intMatch[x] = y;
                                    }
                                }
                                // y
                            }
                            // x
                            // now check the matches
                            // if there is a pair of matches we can't auto transfer
                            // if there are unmatched codes we can't auto transfer
                            intNummatches = 0;
                            for (x = 1; x <= 10; x++)
                            {
                                if (intMatch[x] > 0)
                                    intNummatches += 1;
                                for (y = 1; y <= 10; y++)
                                {
                                    if ((intMatch[x] == intMatch[y]) && (intMatch[x] > 0) && (x != y))
                                    {
                                        boolCanAuto = false;
                                        break;
                                    }
                                }
                                // y
                                if (!boolCanAuto)
                                    break;
                            }
                            // x
                            if (intNummatches < intNumBuildings)
                                boolCanAuto = false;
                        }
                    }
                }
                else
                {
                    // no buildings set up before
                    for (x = 1; x <= 10; x++)
                    {
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + x, lngSQFT[x]);
                        txtMROIUnits1[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(lngSQFT[x]);
                        modDataTypes.Statics.OUT.Set_Fields("oitype" + x, intCode[x]);
                        txtMROIType1[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(intCode[x]);
                    }
                    // x
                }
            }
            if (boolCanAuto)
            {
                // safe to automatically transfer
                for (x = 1; x <= 10; x++)
                {
                    if (intMatch[x] > 0)
                    {
                        modDataTypes.Statics.OUT.Set_Fields("oiunits" + x, lngSQFT[intMatch[x]]);
                        txtMROIUnits1[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(lngSQFT[intMatch[x]]);
                    }
                }
                // x
            }
            else
            {
                //Application.DoEvents();
                //! Load frmTransferSQFT;
                frmTransferSQFT.InstancePtr.boolFromCommercial = false;
                // setup and fill the grids
                frmTransferSQFT.InstancePtr.GridInfo.Rows = 1;
                frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(0, 0, "Area #");
                frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(0, 1, "Code / Description");
                frmTransferSQFT.InstancePtr.GridInfo.TextMatrix(0, 2, "SQFT");
                frmTransferSQFT.InstancePtr.GridInfo.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                for (x = 1; x <= intNumBuildings; x++)
                {
                    frmTransferSQFT.InstancePtr.GridInfo.AddItem(FCConvert.ToString(x) + "\t" + strCode[x] + "\t" + FCConvert.ToString(lngSQFT[x]));
                }
                // x
                frmTransferSQFT.InstancePtr.GridBuildings.Rows = 1;
                frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(0, 0, "Building #");
                frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(0, 1, "Code");
                frmTransferSQFT.InstancePtr.GridBuildings.TextMatrix(0, 2, "Area #");
                frmTransferSQFT.InstancePtr.GridBuildings.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                for (x = 1; x <= 10; x++)
                {
                    frmTransferSQFT.InstancePtr.GridBuildings.AddItem(FCConvert.ToString(x) + "\t" + FCConvert.ToString(intOutCode[x]));
                }
                // x
                //Application.DoEvents();
                frmTransferSQFT.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            }
        }

        private void mnuDwellingSqft_Click(object sender, System.EventArgs e)
        {
            var CalcInfo = GetCalcInfoFromCurrentSketch();
            if (CalcInfo == null)
            {
                MessageBox.Show("Could not get sqft information from WinSketch.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Strings.UCase(mnuDwellingSqft.Text) == "DWELLING")
            {
                TransferDwellingSqft(CalcInfo);
            }
            else
            {
                // commercial
                TransferCommercialSqft(CalcInfo);
                mebOcc1.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields("occ1"));
                mebC1Floor.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields("c1floor"));
                mebOcc2.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields("occ2"));
                mebC2Floor.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields("c2floor"));
            }
            //appWinSketch = null;
        }

        private void mnuOutbuildingSQFT_Click(object sender, System.EventArgs e)
        {
            var CalcInfo = GetCalcInfoFromCurrentSketch();
            if (CalcInfo != null)
            {
                TransferOutbuildingSqft(CalcInfo);
            }
            else
            {
                MessageBox.Show("Could not get sqft information from WinSketch.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private dynamic GetCalculations(string sketchCalcuationsFile)
        {
            try
            {
                dynamic sketchCalculations = null;
                if (File.Exists(sketchCalcuationsFile))
                {
                    string sketchCalculationsSerialized = File.ReadAllText(sketchCalcuationsFile);
                    sketchCalculations = WisejSerializer.Parse(sketchCalculationsSerialized);
                }

                return sketchCalculations;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Unable to get sketch Calculations. \r\n Error {ex.Message}", "Get Calculations error", icon: MessageBoxIcon.Error);
                return null;
            }
        }

        private void mnuPrintPicture_Click(object sender, System.EventArgs e)
        {
            PrintPreviewPicture();
            // Call cmdPrint_Click
        }

       
        private void TransferDwellingSqft(object[] CalcInfo)
        {
            // transfer the sqft for the dwelling from winsketch to Real Estate
            // vbPorter upgrade warning: lngSQFT As int	OnWrite(short, double)
            int lngSQFT;
            // vbPorter upgrade warning: lngBasSQFT As int	OnWrite(short, double)
            int lngBasSQFT;
            int x;
            bool boolNew;
            int intBType;
            if (FCConvert.ToString(modGlobalVariables.Statics.HoldDwelling) == "C" || (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "") == "C")
            {
                MessageBox.Show("This card has a commercial building.  Cannot transfer dwelling sqft.", "Cannot transfer sqft", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // if dwelling doesn't exist then create one
            boolNew = false;
            // If DWL Is Nothing Then Exit Sub
            // If DWL.EOF Or DWL.BOF Then Exit Sub
            if (modDataTypes.Statics.DWL == null)
            {
                boolNew = true;
            }
            if (modDataTypes.Statics.DWL.EndOfFile() || modDataTypes.Statics.DWL.BeginningOfFile())
            {
                boolNew = true;
            }
            if (boolNew)
            {
                clsDRWrapper temp = modDataTypes.Statics.DWL;
                modREMain.OpenDwellingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, theAccount.AccountID);
                temp.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
                temp.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
                modREMain.SaveDwellingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
                modDataTypes.Statics.DWL = temp;
                modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", "Y");
                modGlobalVariables.Statics.HoldDwelling = "Y";
            }
            lngSQFT = 0;
            // sqft dwelling
            lngBasSQFT = 0;
            // sqft of basement living
            intBType = 0;
            // test to see if there was anything returned
            //if (Information.UBound(CalcInfo, 1) >= 0)
            //{
            //	for (x = 0; x <= Information.UBound(CalcInfo, 1); x++)
            //	{
            if (Strings.UCase(Strings.Trim(FCConvert.ToString(CalcInfo[2]))) == "DWELLING AREA")
            {
                lngSQFT += FCConvert.ToInt32(Conversion.Val(CalcInfo[1]));
                if (Conversion.Val(CalcInfo[0]) > 0 && Conversion.Val(CalcInfo[0]) < 10)
                {
                    intBType = FCConvert.ToInt32(Math.Round(Conversion.Val(CalcInfo[0])));
                }
            }
            else if (Strings.UCase(Strings.Trim(FCConvert.ToString(CalcInfo[2]))) == "BASEMENT LIVING")
            {
                lngBasSQFT += FCConvert.ToInt32(Conversion.Val(CalcInfo[1]));
            }
            //	}
            //	// x
            //}
            if (lngSQFT > 0)
            {
                // now put this into the dwelling
                modDataTypes.Statics.DWL.Set_Fields("disqft", lngSQFT);
                GridDwelling3.TextMatrix(6, 1, FCConvert.ToString(lngSQFT));
                if (intBType > 0)
                {
                    GridDwelling1.TextMatrix(0, 1, FCConvert.ToString(intBType));
                    modDataTypes.Statics.DWL.Set_Fields("distyle", intBType);
                }
            }
            if (lngBasSQFT > 0)
            {
                modDataTypes.Statics.DWL.Set_Fields("disfbsmtliving", lngBasSQFT);
                GridDwelling2.TextMatrix(0, 1, FCConvert.ToString(lngBasSQFT));
            }
        }

        public void HandlePartialPermission(ref int lngFuncID)
        {
            // takes a function number as a parameter
            // Then gets all the child functions that belong to it and
            // disables the appropriate controls
            clsDRWrapper clsChildList = new clsDRWrapper();
            // Dim dcTemp As New clsDRWrapper
            string strPerm = "";
            int lngChild = 0;
            bool booleditaddresses = false;
            /* Control ctl = new Control(); */
            bool booleditcommercial = false;
            bool booleditdwelling = false;
            bool booleditoutbuilding = false;
            bool booleditsale = false;
            // Set clsChildList = New clsDRWrapper
            modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
            while (!clsChildList.EndOfFile())
            {
                lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
                strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
                switch (lngChild)
                {
                    case modGlobalVariables.CNSTDELCARDS:
                        {
                            if (strPerm == "F")
                            {
                                mnuDeleteCard.Enabled = true;
                            }
                            else
                            {
                                mnuDeleteCard.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTDELACCOUNTS:
                        {
                            if (strPerm == "F")
                            {
                                mnuDeleteAccount.Enabled = true;
                            }
                            else
                            {
                                mnuDeleteAccount.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTDELDWELLCOMM:
                        {
                            if (strPerm == "F")
                            {
                                mnuDeleteDwellingData.Enabled = true;
                                mnuDeleteOutbuilding.Enabled = true;
                            }
                            else
                            {
                                mnuDeleteDwellingData.Enabled = false;
                                mnuDeleteOutbuilding.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTADDCARDS:
                        {
                            if (strPerm == "F")
                            {
                                mnuAddACard.Enabled = true;
                            }
                            else
                            {
                                mnuAddACard.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTADDACCOUNTS:
                        {
                            if (strPerm == "F")
                            {
                                mnuAddNewAccount.Enabled = true;
                            }
                            else
                            {
                                mnuAddNewAccount.Enabled = false;
                                mnuSeparateCard.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTSAVEACCOUNTS:
                        {
                            if (strPerm == "F")
                            {
                                cmdSave.Enabled = true;
                                mnuSaveQuit.Enabled = true;
                            }
                            else
                            {
                                cmdSave.Enabled = false;
                                mnuSaveQuit.Enabled = false;
                                mnuPendingTransfer.Enabled = false;
                                // mnuCopy.Enabled = False
                                mnuCopyProperty.Enabled = false;
                                mnuSeparateCard.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTCOMMENTS:
                        {
                            if (strPerm == "N")
                            {
                                cmdComment.Enabled = false;
                                mnuRECLComment.Enabled = false;
                                mnuCollectionsNote.Enabled = false;
                            }
                            else
                            {
                                cmdComment.Enabled = true;
                                mnuRECLComment.Enabled = true;
                                mnuCollectionsNote.Enabled = true;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTCALCACCOUNTS:
                        {
                            if (strPerm == "F")
                            {
                                cmdCalculate.Enabled = true;
                            }
                            else
                            {
                                cmdCalculate.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTINCOMEAPPROACH:
                        {
                            if (strPerm == "F")
                            {
                                SSTab1.TabPages[5].Enabled = true;
                            }
                            else
                            {
                                SSTab1.TabPages[5].Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTVIEWPICSKETCH:
                        {
                            if (strPerm == "F")
                            {
                                SSTab1.TabPages[6].Enabled = true;
                                mnuAddPicture.Enabled = true;
                                // SSTab1.TabEnabled(6) = True
                            }
                            else if (strPerm == "P")
                            {
                                SSTab1.TabPages[6].Enabled = true;
                                strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTADDPICSKETCH));
                                if (Strings.UCase(strPerm) == "F")
                                {
                                    mnuAddPicture.Enabled = true;
                                    mnuImportPicture.Enabled = true;
                                }
                                else
                                {
                                    mnuAddPicture.Enabled = false;
                                    mnuImportPicture.Enabled = false;
                                }
                                strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTDELPICSKETCH));
                                if (Strings.UCase(strPerm) == "F")
                                {
                                    mnuDeletePicture.Enabled = true;
                                }
                                else
                                {
                                    mnuDeletePicture.Enabled = false;
                                }
                            }
                            else
                            {
                                SSTab1.TabPages[6].Enabled = false;
                                // SSTab1.TabEnabled(6) = False
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTVIEWSKETCHES:
                        {
                            if (strPerm == "F")
                            {
                                SSTab1.TabPages[7].Enabled = true;
                            }
                            else if (strPerm == "P")
                            {
                                SSTab1.TabPages[7].Enabled = true;
                                strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTADDSKETCH));
                                if (Strings.UCase(strPerm) == "F")
                                {
                                    mnuNewSketch.Enabled = true;
                                    mnuEditSketch.Enabled = true;
                                }
                                else
                                {
                                    mnuNewSketch.Enabled = false;
                                    mnuEditSketch.Enabled = false;
                                }
                                strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTDELSKETCH));
                                if (Strings.UCase(strPerm) == "F")
                                {
                                    mnuDeleteSketch.Enabled = true;
                                }
                                else
                                {
                                    mnuDeleteSketch.Enabled = false;
                                }
                            }
                            else
                            {
                                SSTab1.TabPages[7].Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTEDITADDRESSES:
                        {
                            if (strPerm == "F")
                            {
                                booleditaddresses = true;
                            }
                            else
                            {
                                booleditaddresses = false;
                                // mnuCopy.Enabled = False
                                mnuCopyProperty.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTEDITPROPERTY:
                        {
                            if (strPerm == "F")
                            {
                                booleditland = true;
                            }
                            else
                            {
                                booleditland = false;
                                // mnuCopy.Enabled = False
                                mnuCopyProperty.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTDWELLING:
                        {
                            if (strPerm == "F")
                            {
                                booleditdwelling = true;
                                lblDwel.Visible = false;
                                mnuDwellingSqft.Enabled = true;
                            }
                            else
                            {
                                lblDwel.Visible = true;
                                booleditdwelling = false;
                                mnuDwellingSqft.Enabled = false;
                                mnuAll.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTCOMMERCIAL:
                        {
                            if (strPerm == "F")
                            {
                                booleditcommercial = true;
                                lblcomm.Visible = false;
                            }
                            else
                            {
                                booleditcommercial = false;
                                lblcomm.Visible = true;
                                mnuDwellingSqft.Enabled = false;
                                mnuAll.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTOUTBUILDING:
                        {
                            if (strPerm == "F")
                            {
                                booleditoutbuilding = true;
                                lblOut.Visible = false;
                                mnuOutbuildingSQFT.Enabled = true;
                            }
                            else
                            {
                                booleditoutbuilding = false;
                                lblOut.Visible = true;
                                mnuOutbuildingSQFT.Enabled = false;
                                mnuAll.Enabled = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTEDITSALEINFO:
                        {
                            if (strPerm == "F")
                            {
                                booleditsale = true;
                            }
                            else
                            {
                                booleditsale = false;
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTDELPICSKETCH:
                        {
                            if (strPerm != "F")
                            {
                                mnuDeletePicture.Enabled = false;
                                // cmdRemove1.Enabled = False
                                // cmdRemove2.Enabled = False
                                // cmdRemove3.Enabled = False
                            }
                            break;
                        }
                    case modGlobalVariables.CNSTADDPICSKETCH:
                        {
                            if (strPerm != "F")
                            {
                                mnuAddPicture.Enabled = false;
                            }
                            break;
                        }
                }
                //end switch
                clsChildList.MoveNext();
            }
            if (!booleditaddresses || !booleditland)
            {
                // mnuCopy.Enabled = False
                mnuCopyProperty.Enabled = false;
            }
            // If booleditaddresses Or booleditland Then
            // mnuViewDocs.Enabled = True
            // boolEditDocuments = True
            // Else
            // mnuViewDocs.Enabled = False
            // boolEditDocuments = False
            // End If
            if (!booleditaddresses || !booleditland || !booleditsale || !booleditdwelling || !booleditcommercial || !booleditoutbuilding)
            {
                //FC:FINAL:MSH - i.issue #1649: in VB6 control contain all controls, include child controls in each control
                //foreach (Control ctl in this.Controls)
                var formControls = this.GetAllControls();
                foreach (Control ctl in formControls)
                {
                    if (!booleditaddresses)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "address")
                        {
                            ctl.Enabled = false;
                        }
                    }
                    if (!booleditsale)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "sale")
                        {
                            ctl.Enabled = false;
                        }
                    }
                    if (!booleditland)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "land")
                        {
                            ctl.Enabled = false;
                            GridLand.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
                            GridExempts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
                            GridExempts.Enabled = true;
                            GridExempts.Editable = FCGrid.EditableSettings.flexEDNone;
                            BPGrid.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
                        }
                    }
                    if (!booleditdwelling)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "dwelling")
                        {
                            ctl.Enabled = false;
                            ctl.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
                        }
                    }
                    if (!booleditcommercial)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "commercial")
                        {
                            ctl.Enabled = false;
                        }
                    }
                    if (!booleditoutbuilding)
                    {
                        if (FCConvert.ToString(ctl.Tag) == "outbuilding")
                        {
                            ctl.Enabled = false;
                        }
                    }
                }
            }
        }

        private void CheckForInterestedParties(int lngAcct)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                bool boolExists;
                boolExists = false;
                rsLoad.OpenRecordset("select count(Account) as thecount from owners where ModuleCode = 'RE' and account = " + FCConvert.ToString(lngAcct) + " and associd = 0 and isnull(billyear ,0) = 0", "RealEstate");
                if (!rsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsLoad.Get_Fields("thecount")) > 0)
                    {
                        boolExists = true;
                    }
                }
                if (boolExists)
                {
                    lblInterestedPartiesPresent.Visible = true;
                }
                else
                {
                    lblInterestedPartiesPresent.Visible = false;
                }
                return;
            }
            catch
            {
                // ErrorHandler:
            }
        }

        private void ShowDeeds()
        {
            txtDeedName1.Text = theAccount.DeedName1;
            txtDeedName2.Text = theAccount.DeedName2;
        }

        private void mnuViewGroupInformation_Click(object sender, EventArgs e)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            int lngGroupNumber = 0;
            // vbPorter upgrade warning: intRes As short, int --> As DialogResult
            DialogResult intRes;
            try
            {
                // On Error GoTo ErrorHandler
                clsLoad.OpenRecordset("select * from moduleassociation where module = 'RE' and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "CentralData");
                if (!clsLoad.EndOfFile())
                {
                    lngGroupNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
                    clsLoad.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupNumber), "CentralData");
                    if (!clsLoad.EndOfFile())
                    {
                        //! Load frmGroup;
                        frmGroup.InstancePtr.Init(lngGroupNumber);
                        return;
                    }
                    else
                    {
                        //Application.DoEvents();
                        MessageBox.Show("This account is associated with a group but the group data is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    //Application.DoEvents();
                    intRes = MessageBox.Show("This account is not in any group." + "\r\n" + "Do you want to create a new group?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (intRes == DialogResult.Yes)
                    {
                        //! Load frmGroup;
                        frmGroup.InstancePtr.Init(0);
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuGroup_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuViewMortgageInformation_Click(object sender, EventArgs e)
        {
	        frmMortgageHolder.InstancePtr.Unload();
	        //! Load frmMortgageHolder;
	        frmMortgageHolder.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, 2);
	        frmMortgageHolder.InstancePtr.Show(App.MainForm);
        }
    }
}
