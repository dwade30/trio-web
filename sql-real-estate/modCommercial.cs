﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modCommercial
	{
		// Public Sub REAS03_3040_COMMERCIAL()
		// Call REAS03_4400_COMPUTE_COMMERCIAL
		// Call new_compute_commercial
		// If RUNTYPE$ = "D" Then
		// Call REAS03_5600_COMMERCIAL_DATA
		// End If
		// End Sub
		public static void Speed_compute_commercial()
		{
			// Dim CRCUnit&
			// Dim CRCBase&
			double HoldPhysicalPercent = 0;
			double dblHeight = 0;
			double dblGroup;
			// Dim Life                As Integer
			double a = 0;
			double B = 0;
			double C = 0;
			double D = 0;
			double dblPerim;
			clsDRWrapper clsTemp = new clsDRWrapper();
			// Call OpenCOMMERCIALTable(CMR, gintLastAccountNumber, gintCardNumber)
			modDwelling.Statics.intSFLA = modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories") * modDataTypes.Statics.CMR.Get_Fields_Int32("c1floor");
			modDwelling.Statics.intSFLA += modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories") * modDataTypes.Statics.CMR.Get_Fields_Int32("c2floor");
			modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1") + "")));
			if (modProperty.Statics.WKey != 0)
			{
				// Get #6, WKEY, CR
				// Call OpenCRTable(CR, WKEY)
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			}
			else
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				Statics.strOccupancy1 = "";
				Statics.strOccupancy2 = "";
				Statics.strClassQuality1 = "";
				Statics.strHeat1 = "";
				modSpeedCalc.Statics.CalcLog += "There is an incorrect Occupancy Code in Column #1 for Commercial Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
			//dblGroup = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			dblGroup = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			Statics.strOccupancy1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
			//modProperty.Statics.WKey = FCConvert.ToInt32((FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100) + ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class"))) * 4) - 4) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality"))) - 1);
			modProperty.Statics.WKey = FCConvert.ToInt32((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100) + ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class")) * 4) - 4) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality")) - 1);
			// Get #6, WKEY, CR
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class") + "") < 1 || Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class")) > 5)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "The class for commercial property " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " is invalid." + "\r\n";
				return;
			}
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality")) < 1 || Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality")) > 4)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "The quality for commercial property " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " is invalid." + "\r\n";
				return;
			}
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999) || (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999))
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values. (" + modGlobalVariables.Statics.ComCostRec.ClDesc + ",  " + modGlobalVariables.Statics.ComCostRec.CSDesc + ")" + "\r\n";
				return;
			}
			modREASValuations.Statics.Life = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
			//Statics.dblBase1 = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
			Statics.dblBase1 = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
			Statics.strClassQuality1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1heat")) == 0)
			{
				Statics.dblHeat1 = 0;
				Statics.strHeat1 = "NONE";
			}
			else
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(140 + (20 * dblGroup) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1heat")) - 10);
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999)
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values. (" + modGlobalVariables.Statics.ComCostRec.ClDesc + ",  " + modGlobalVariables.Statics.ComCostRec.CSDesc + ")" + "\r\n";
					return;
				}
				else
				{
                    //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
					//Statics.dblHeat1 = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                    Statics.dblHeat1 = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
					Statics.strHeat1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				}
			}
			modProperty.Statics.WKey = FCConvert.ToInt32(140 + (20 * dblGroup) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class")));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
            //Statics.dblTrend = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
            Statics.dblTrend = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			modProperty.Statics.WKey = FCConvert.ToInt32(170 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1class")));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
            //Statics.dblLocation = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
            Statics.dblLocation = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			Statics.dblBase1 = (fecherFoundation.FCUtils.iDiv((Statics.dblBase1 * Statics.dblTrend * Statics.dblLocation * Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1grade")) * 100), 1)) / 10000.0;
			Statics.dblHeat1 = (fecherFoundation.FCUtils.iDiv((Statics.dblHeat1 * Statics.dblTrend * Statics.dblLocation * Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1grade")) * 100), 1)) / 10000.0;
			// corey
			Statics.dblBase1 = modGlobalRoutines.Round(Statics.dblBase1, 2);
			Statics.dblHeat1 = modGlobalRoutines.Round(Statics.dblHeat1, 2);
			Statics.dblTotal1 = Statics.dblBase1 + Statics.dblHeat1;
			dblPerim = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1perimeter") + "");
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1")) > 16 && Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1")) < 27)
			{
				if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories")) != 0)
				{
					// cmr.fields("c1perimeter") = Val(cmr.fields("c1perimeter")) / Val(cmr.fields("c1stories"))
					dblPerim = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1perimeter")) / Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories"));
				}
				else
				{
					dblPerim = 0;
					MessageBox.Show("C1.Perimeter$ = 0's, because C1.Stories$ = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (dblPerim == 0)
			{
				Statics.dblPerimeter = 1;
			}
			else
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(360 + dblGroup);
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999) || (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999))
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
					return;
				}
                //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
                //a = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
                //B = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                a = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
                B = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1floor")) != 0)
				{
					if (a != 0)
					{
						Statics.dblPerimeter = 1 + (((dblPerim / Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1floor"))) - (10 / a)) * B);
					}
					else
					{
						Statics.dblPerimeter = 0;
						MessageBox.Show("dblPerimeter = 0, because A = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					Statics.dblPerimeter = 0;
					MessageBox.Show("dblPerimeter = 0, because C1.Floor$ = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1height")) == 0)
			{
				dblHeight = 1;
			}
			else
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(370 + dblGroup);
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999) || (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999))
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
					return;
				}
                //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
                //C = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
                //D = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                C = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
                D = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                if (D != 0)
				{
					dblHeight = 1 + ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1height")) - C) / D);
				}
				else
				{
					dblHeight = 0;
					MessageBox.Show("Height # = 0, because D = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			modProperty.Statics.WKey = FCConvert.ToInt32(380 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories")));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " (Stories) hasn't been given values." + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
            //Statics.dblSize1 = fecherFoundation.FCUtils.iDiv(((Statics.dblPerimeter * dblHeight * (FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100)) * 1000), 1);
            Statics.dblSize1 = fecherFoundation.FCUtils.iDiv(((Statics.dblPerimeter * dblHeight * (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100)) * 1000), 1);
			Statics.dblSize1 /= 1000;
			Statics.dblAdjustment1 = fecherFoundation.FCUtils.iDiv((Statics.dblTotal1 * Statics.dblSize1 * 100), 1);
			Statics.dblAdjustment1 /= 100;
			Statics.intSQFT1 = FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1floor")) * Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories")));
			Statics.dblReplacement1 = fecherFoundation.FCUtils.iDiv((Statics.intSQFT1 * Statics.dblAdjustment1), 1);
			modDwelling.Statics.intHoldYear = FCConvert.ToInt32(modDataTypes.Statics.CMR.Get_Fields_Int32("c1built"));
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1phys")) == 0)
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(350 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition")));
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				// open the table for the next card intcurrentcard
				// Call OpenCOMMERCIALTable(CMR, gintLastAccountNumber, gintCardNumber)
				modDwelling.Statics.strDwellingCode = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"));
				// ****
				modGlobalVariables.Statics.CRCUnit = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit);
				modGlobalVariables.Statics.CRCBase = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase);
				if (modGlobalVariables.Statics.CRCUnit == FCConvert.ToDouble("99999998") || modGlobalVariables.Statics.CRCUnit == FCConvert.ToDouble("99999999") || modGlobalVariables.Statics.CRCBase == FCConvert.ToDouble("99999998") || modGlobalVariables.Statics.CRCBase == FCConvert.ToDouble("99999999"))
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " (Condition " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition"))) + ") hasn't been given valid values." + "\r\n";
					return;
				}
				HoldPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
				modREASValuations.Statics.HOLDYEAR = modDataTypes.Statics.CMR.Get_Fields_Int32("c1built") + "";
				// Call PhysicalPercentRoutine
				modREASValuations.new_physicalpercentroutine();
				if (modDwelling.Statics.strDwellingCode == "O")
				{
					modREASValuations.Statics.OBPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
					modDwelling.Statics.dblPhysicalPercent = HoldPhysicalPercent;
				}
				// ****
				Statics.dblPhysical1 = modDwelling.Statics.dblPhysicalPercent;
			}
			else
			{
				Statics.dblPhysical1 = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1phys")) / 100;
				modDwelling.Statics.dblPhysicalPercent = Statics.dblPhysical1;
			}
			modDwelling.Statics.dblHoldPhysicalPercent = Statics.dblPhysical1;
			Statics.dblFunctional1 = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1funct")) / 100;
			modDwelling.Statics.dblFunctionalPercent = Statics.dblFunctional1;
			Statics.lngSubTotal1 = fecherFoundation.FCUtils.iDiv((Statics.dblReplacement1 * Statics.dblPhysical1 * Statics.dblFunctional1), 1);
			// 
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				// Call clsTemp.OpenRecordset("select * from summrecord where srecordnumber = " & gintLastAccountNumber & " and cardnumber = " & gintCardNumber, strredatabase)
				// If clsTemp.EndOfFile Then
				// clsTemp.AddNew
				// Call clsTemp.SetData("srecordnumber", gintLastAccountNumber)
				// Call clsTemp.SetData("cardnumber", gintCardNumber)
				// Else
				// clsTemp.Edit
				// End If
				// Call clsTemp.SetData("comval1", lngSubTotal1)
				modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].comval1 = Statics.lngSubTotal1;
			}
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2")) == 0)
			{
				// clear out the values that are in these variables though
				Statics.lngSubTotal2 = 0;
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					// Call clsTemp.SetData("comval2", 0)
					// Call clsTemp.Update
					modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].comval2 = 0;
				}
				goto REAS03_4401_TAG;
			}
			modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2"))));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
            //dblGroup = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
            dblGroup = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			Statics.strOccupancy2 = modGlobalVariables.Statics.ComCostRec.ClDesc;
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
			//modProperty.Statics.WKey = FCConvert.ToInt32((FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100) + ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2class"))) * 4) - 4) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2quality"))) - 1);
			modProperty.Statics.WKey = FCConvert.ToInt32((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100) + ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2class")) * 4) - 4) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2quality")) - 1);
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			// If ((ComCostRec.CBase = 99999999) Or (ComCostRec.CUnit = 99999999)) And Val(CMR.Fields("c2condition")) < 9 And Val(CMR.Fields("c2phys")) = 0 Then
			if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999) || (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999))
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values. (" + modGlobalVariables.Statics.ComCostRec.ClDesc + ",  " + modGlobalVariables.Statics.ComCostRec.CSDesc + ")" + "\r\n";
				return;
			}
			// If ComCostRec.CBase = 99999999 Then
			// Else
			modREASValuations.Statics.Life = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			// End If
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
			//Statics.dblBase2 = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
			Statics.dblBase2 = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
			Statics.strclassquality2 = modGlobalVariables.Statics.ComCostRec.ClDesc;
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2heat")) == 0)
			{
				Statics.dblHeat2 = 0;
				Statics.strHeat2 = "NONE";
			}
			else
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(140 + (20 * dblGroup) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2heat")) - 10);
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999)
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values. (" + modGlobalVariables.Statics.ComCostRec.ClDesc + ",  " + modGlobalVariables.Statics.ComCostRec.CSDesc + ")" + "\r\n";
					return;
				}
                //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
                //Statics.dblHeat2 = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                Statics.dblHeat2 = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
				Statics.strHeat2 = modGlobalVariables.Statics.ComCostRec.ClDesc;
			}
			modProperty.Statics.WKey = FCConvert.ToInt32(140 + (20 * dblGroup) + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2class")));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
            //Statics.dblTrend = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
            Statics.dblTrend = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			modProperty.Statics.WKey = FCConvert.ToInt32(170 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2class")));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
            //Statics.dblLocation = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
            Statics.dblLocation = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
			Statics.dblBase2 = (fecherFoundation.FCUtils.iDiv((Statics.dblBase2 * Statics.dblTrend * Statics.dblLocation * Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2grade")) * 100), 1)) / 10000.0;
			Statics.dblHeat2 = (fecherFoundation.FCUtils.iDiv((Statics.dblHeat2 * Statics.dblTrend * Statics.dblLocation * Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2grade")) * 100), 1)) / 10000.0;
			// corey
			Statics.dblBase2 = modGlobalRoutines.Round(Statics.dblBase2, 2);
			Statics.dblHeat2 = modGlobalRoutines.Round(Statics.dblHeat2, 2);
			Statics.dblTotal2 = Statics.dblBase2 + Statics.dblHeat2;
			dblPerim = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2perimeter") + "");
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2")) > 16 && Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2")) < 27)
			{
				if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories")) != 0)
				{
					// cmr.fields("c2perimeter") = Val(cmr.fields("c2perimeter")) / Val(cmr.fields("c2stories"))
					dblPerim = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2perimeter")) / Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories"));
				}
				else
				{
					// cmr.fields("c2perimeter") = 0
					dblPerim = 0;
					MessageBox.Show("C2.Perimeter = 0's, because C2.Stories = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (dblPerim == 0)
			{
				Statics.dblPerimeter = 1;
			}
			else
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(360 + dblGroup);
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999) || (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999))
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
					return;
				}
                //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
				//a = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
				//B = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                a = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
                B = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2floor")) != 0)
				{
					if (a != 0)
					{
						Statics.dblPerimeter = 1 + (((dblPerim / Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2floor"))) - (10 / a)) * B);
					}
					else
					{
						Statics.dblPerimeter = 0;
						MessageBox.Show("dblPerimeter = 0, because A = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					Statics.dblPerimeter = 0;
					MessageBox.Show("dblPerimeter = 0, because C2.Floor = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2height")) == 0)
			{
				dblHeight = 1;
			}
			else
			{
				modProperty.Statics.WKey = FCConvert.ToInt32(370 + dblGroup);
				// Get #6, WKEY, CR
				// Call OpenCOMMERCIALCOSTTable(CC, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) == 99999999) || (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999))
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
					return;
				}
                //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
				//C = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
				//D = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                C = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
                D = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
                if (D != 0)
				{
					dblHeight = 1 + ((Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2height")) - C) / D);
				}
				else
				{
					dblHeight = 0;
					MessageBox.Show("Height # = 0, because D = 0.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			modProperty.Statics.WKey = FCConvert.ToInt32(380 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories")));
			// Get #6, WKEY, CR
			// Call OpenCOMMERCIALCOSTTable(CC, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
			if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Cannot correctly calculate account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " since commercial cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " hasn't been given values." + "\r\n";
				return;
			}
            //FC:FINAL:MSH - issue #1641: replace wrong conversion to avoid missing data
			//Statics.dblSize2 = (fecherFoundation.FCUtils.iDiv(((Statics.dblPerimeter * dblHeight * (FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100)) * 1000), 1)) / 1000.0;
			Statics.dblSize2 = (fecherFoundation.FCUtils.iDiv(((Statics.dblPerimeter * dblHeight * (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) / 100)) * 1000), 1)) / 1000.0;
			Statics.dblAdjustment2 = fecherFoundation.FCUtils.iDiv((Statics.dblTotal2 * Statics.dblSize2 * 100), 1);
			Statics.dblAdjustment2 /= 100;
			Statics.intSQFT2 = FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2floor") + "") * Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories") + ""));
			Statics.dblReplacement2 = fecherFoundation.FCUtils.iDiv((Statics.intSQFT2 * Statics.dblAdjustment2), 1);
			modDwelling.Statics.intHoldYear = FCConvert.ToInt32(modDataTypes.Statics.CMR.Get_Fields_Int32("c2built"));
			modREASValuations.Statics.HOLDYEAR = FCConvert.ToString(modDwelling.Statics.intHoldYear);
			if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2phys")) == 0)
			{
				if (Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2condition")) == 9)
				{
					Statics.dblPhysical2 = Statics.dblPhysical1;
				}
				else
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(350 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2condition")));
					// Get #6, WKEY, CR
					// Call OpenCOMMERCIALCOSTTable(CC, WKey)
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
					HoldPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
					// open the table for this card?
					// Call OpenCOMMERCIALTable(CMR, gintLastAccountNumber, gintCardNumber)
					modDwelling.Statics.strDwellingCode = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"));
					// ****
					modGlobalVariables.Statics.CRCUnit = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit);
					modGlobalVariables.Statics.CRCBase = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase);
					HoldPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
					// Call PhysicalPercentRoutine
					modREASValuations.new_physicalpercentroutine();
					if (modDwelling.Statics.strDwellingCode == "O")
					{
						modREASValuations.Statics.OBPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
						modDwelling.Statics.dblPhysicalPercent = HoldPhysicalPercent;
					}
					// ****
					Statics.dblPhysical2 = modDwelling.Statics.dblPhysicalPercent;
					modDwelling.Statics.dblPhysicalPercent = HoldPhysicalPercent;
				}
			}
			else
			{
				Statics.dblPhysical2 = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2phys")) / 100;
			}
			modDwelling.Statics.dblHoldPhysicalPercent = Statics.dblPhysical2;
			Statics.dblFunctional2 = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2funct")) / 100;
			if (Statics.dblFunctional2 == 0 && Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2condition")) == 9)
			{
				Statics.dblFunctional2 = Statics.dblFunctional1;
			}
			Statics.lngSubTotal2 = fecherFoundation.FCUtils.iDiv((Statics.dblReplacement2 * Statics.dblPhysical2 * Statics.dblFunctional2), 1);
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				// Call clsTemp.SetData("comval2", lngSubTotal2)
				// Call clsTemp.Update
				modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].comval2 = Statics.lngSubTotal2;
			}
			REAS03_4401_TAG:
			;
			Statics.dblEconomic = modDwelling.Statics.dblEconomicPercent;
			modDwelling.Statics.dblEconomicPercent = Statics.dblEconomic;
			modDwelling.Statics.HoldCondition = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition"))));
			modDwelling.Statics.HoldGrade1 = FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1quality")) + 1);
			modDwelling.Statics.HoldGrade2 = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1grade"))));
			Statics.lngCTotal = fecherFoundation.FCUtils.iDiv(((Statics.lngSubTotal1 + Statics.lngSubTotal2) * Statics.dblEconomic), 1);
			modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] = Statics.lngCTotal;
		}

		public static void Speed_COMMERCIAL_DATA()
		{
			// vbPorter upgrade warning: OccCode2 As Variant --> As double
			double OccCode2;
			try
			{
				// On Error GoTo ErrorHandler
				// With frmNewValuationReport.CommercialGrid(intCurrentCard - 1)
				OccCode2 = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("occ2"));
				if (OccCode2 == 0)
				{
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].OccupancyType[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].AdjustedCost[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].BaseCost[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Built[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ClassQuality[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Condition[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].DwellUnits[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Exterior[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Functional[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatCoolSqft[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatingCool[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Physical[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Remodeled[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ReplacementCost[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].SizeFactor[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Stories[2] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].subtotal[2] = "";
					Statics.dblBase2 = 0;
					Statics.dblHeat2 = 0;
					Statics.dblTotal2 = 0;
					Statics.dblSize2 = 0;
					Statics.dblAdjustment2 = 0;
					Statics.intSQFT2 = 0;
					Statics.dblReplacement2 = 0;
					Statics.lngSubTotal2 = 0;
				}
				if (FCConvert.ToInt32(modDataTypes.Statics.CMR.Get_Fields_Int32("occ1")) == 0)
				{
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].OccupancyType[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].AdjustedCost[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].BaseCost[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Built[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ClassQuality[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Condition[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].DwellUnits[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Exterior[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Functional[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatCoolSqft[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatingCool[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Physical[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Remodeled[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ReplacementCost[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].SizeFactor[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Stories[1] = "";
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].subtotal[1] = "";
					Statics.dblBase1 = 0;
					Statics.dblHeat1 = 0;
					Statics.dblTotal1 = 0;
					Statics.dblSize1 = 0;
					Statics.dblAdjustment1 = 0;
					Statics.intSQFT1 = 0;
					Statics.dblReplacement1 = 0;
					Statics.lngSubTotal1 = 0;
				}
				// .TextMatrix(0, 2) = Trim(strOccupancy1)
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].OccupancyType[1] = Strings.Trim(Statics.strOccupancy1);
				if (OccCode2 != 0)
				{
					// .TextMatrix(0, 3) = Trim(strOccupancy2)
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].OccupancyType[2] = Strings.Trim(Statics.strOccupancy2);
				}
				// .TextMatrix(1, 2) = Trim(strClassQuality1)
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ClassQuality[1] = Strings.Trim(Statics.strClassQuality1);
				if (OccCode2 != 0)
				{
					// .TextMatrix(1, 3) = Trim(strclassquality2)
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ClassQuality[2] = Strings.Trim(Statics.strclassquality2);
				}
				// .TextMatrix(2, 2) = CMR.Fields("dwel1")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].DwellUnits[1] = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("dwel1"));
				if (OccCode2 != 0)
				{
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].DwellUnits[2] = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("dwel2"));
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(340 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1extwalls") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				modProperty.Statics.WORKA1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				modREASValuations.Statics.WORKA2 = "";
				if (OccCode2 != 0)
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(340 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2extwalls") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
					modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.ComCostRec.ClDesc + "";
				}
				// .TextMatrix(3, 2) = Trim(WORKA1)
				// .TextMatrix(3, 3) = Trim(WORKA2)
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Exterior[1] = Strings.Trim(modProperty.Statics.WORKA1);
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Exterior[2] = Strings.Trim(modREASValuations.Statics.WORKA2);
				// .TextMatrix(4, 2) = Val(cmr.fields("c1stories") & "") & " STORY @ " & Val(cmr.fields("c1height") & "") & "'"
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Stories[1] = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories") + "")) + " STORY @ " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1height") + "")) + "'";
				if (OccCode2 != 0)
				{
					// .TextMatrix(4, 3) = Val(cmr.fields("c2stories") & "") & " STORY @ " & Val(cmr.fields("c2height") & "") & "'"
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Stories[2] = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories") + "")) + " STORY @ " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2height") + "")) + "'";
				}
				// .TextMatrix(5, 2) = strHeat1
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatingCool[1] = Statics.strHeat1;
				if (OccCode2 != 0)
				{
					// .TextMatrix(5, 3) = strHeat2
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatingCool[2] = Statics.strHeat2;
				}
				// .TextMatrix(6, 2) = CMR.Fields("c1built")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Built[1] = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c1built"));
				if (OccCode2 != 0)
				{
					// .TextMatrix(6, 3) = CMR.Fields("c2built")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Built[2] = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c2built"));
				}
				// .TextMatrix(7, 2) = CMR.Fields("c1remodel")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Remodeled[1] = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c1remodel"));
				if (OccCode2 != 0)
				{
					// .TextMatrix(7, 3) = CMR.Fields("c2remodel")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Remodeled[2] = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c2remodel"));
				}
				// .TextMatrix(8, 2) = Format(dblBase1, "##0.00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].BaseCost[1] = Strings.Format(Statics.dblBase1, "##0.00");
				if (OccCode2 != 0)
				{
					// .TextMatrix(8, 3) = Format(dblBase2, "##0.00")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].BaseCost[2] = Strings.Format(Statics.dblBase2, "##0.00");
				}
				// .TextMatrix(9, 2) = Format(dblHeat1, "##0.00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatCoolSqft[1] = Strings.Format(Statics.dblHeat1, "##0.00");
				if (OccCode2 != 0)
				{
					// .TextMatrix(9, 3) = Format(dblHeat2, "##0.00")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].HeatCoolSqft[2] = Strings.Format(Statics.dblHeat2, "##0.00");
				}
				// .TextMatrix(10, 2) = Format(dblTotal1, "##0.00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Total[1] = Strings.Format(Statics.dblTotal1, "##0.00");
				if (OccCode2 != 0)
				{
					// .TextMatrix(10, 3) = Format(dblTotal2, "##0.00")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Total[2] = Strings.Format(Statics.dblTotal2, "##0.00");
				}
				// .TextMatrix(11, 2) = Format(dblSize1, "0.000")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].SizeFactor[1] = Strings.Format(Statics.dblSize1, "0.000");
				if (OccCode2 != 0)
				{
					// .TextMatrix(11, 3) = Format(dblSize2, "0.000")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].SizeFactor[2] = Strings.Format(Statics.dblSize2, "0.000");
				}
				// .TextMatrix(12, 2) = Format(dblAdjustment1, "##0.00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].AdjustedCost[1] = Strings.Format(Statics.dblAdjustment1, "##0.00");
				if (OccCode2 != 0)
				{
					// .TextMatrix(12, 3) = Format(dblAdjustment2, "##0.00")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].AdjustedCost[2] = Strings.Format(Statics.dblAdjustment2, "##0.00");
				}
				// .TextMatrix(13, 2) = Format(intSQFT1, "#,##0")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].TotalSqft[1] = Strings.Format(Statics.intSQFT1, "#,##0");
				if (OccCode2 != 0)
				{
					// .TextMatrix(13, 3) = Format(intSQFT2, "#,##0")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].TotalSqft[2] = Strings.Format(Statics.intSQFT2, "#,##0");
				}
				// .TextMatrix(14, 2) = Format(dblReplacement1, "#,###,##0")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ReplacementCost[1] = Strings.Format(Statics.dblReplacement1, "#,###,##0");
				if (OccCode2 != 0)
				{
					// .TextMatrix(14, 3) = Format(dblReplacement2, "#,###,##0")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].ReplacementCost[2] = Strings.Format(Statics.dblReplacement2, "#,###,##0");
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(350 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition")));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				modProperty.Statics.WORKA1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				modREASValuations.Statics.WORKA2 = "";
				if (OccCode2 != 0)
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(350 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2condition")));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
					modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				}
				// .TextMatrix(15, 2) = Trim(WORKA1)
				// .TextMatrix(15, 3) = Trim(WORKA2)
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Condition[1] = Strings.Trim(modProperty.Statics.WORKA1);
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Condition[2] = Strings.Trim(modREASValuations.Statics.WORKA2);
				// .TextMatrix(16, 2) = Format(dblPhysical1, ".00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Physical[1] = Strings.Format(Statics.dblPhysical1, ".00");
				if (OccCode2 != 0)
				{
					// .TextMatrix(16, 3) = Format(dblPhysical2, ".00")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Physical[2] = Strings.Format(Statics.dblPhysical2, ".00");
				}
				// .TextMatrix(17, 2) = Format(dblFunctional1, "0.00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Functional[1] = Strings.Format(Statics.dblFunctional1, "0.00");
				if (OccCode2 != 0)
				{
					// .TextMatrix(17, 3) = Format(dblFunctional2, "0.00")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].Functional[2] = Strings.Format(Statics.dblFunctional2, "0.00");
				}
				// .TextMatrix(18, 2) = Format(lngSubTotal1, "#,###,##0")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].subtotal[1] = Strings.Format(Statics.lngSubTotal1, "#,###,##0");
				if (OccCode2 != 0)
				{
					// .TextMatrix(18, 3) = Format(lngSubTotal2, "#,###,##0")
					modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].subtotal[2] = Strings.Format(Statics.lngSubTotal2, "#,###,##0");
				}
				// .TextMatrix(19, 2) = Format(dblEconomic, "0.00")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].EconFactor = Strings.Format(Statics.dblEconomic, "0.000");
				if (OccCode2 != 0)
				{
					// .TextMatrix(19, 3) = Format(dblEconomic, "0.00")
				}
				// .TextMatrix(20, 2) = Format(lngCTotal, "#,###,##0")
				modSpeedCalc.Statics.CalcCommList[modSpeedCalc.Statics.CalcCommIndex].TotalValue = Strings.Format(Statics.lngCTotal, "#,###,##0");
				// If OccCode2 <> 0 Then
				// .MergeRow(20) = True
				// .MergeCells = flexMergeFree
				// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 20, 2, 20, 3) = flexAlignCenterCenter
				// .TextMatrix(20, 3) = .TextMatrix(20, 2)
				// End If
				// End With
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Commercial Data", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public double dblEconomic;
			public string strClassQuality1 = string.Empty;
			public string strclassquality2 = string.Empty;
			public double dblTrend;
			public double dblLocation;
			public double dblPerimeter;
			public int lngCTotal;
			// First Column
			public string strOccupancy1 = string.Empty;
			public double dblBase1;
			public double dblHeat1;
			public string strHeat1 = "";
			public double dblTotal1;
			public double dblSize1;
			public double dblAdjustment1;
			// vbPorter upgrade warning: intSQFT1 As int	OnWrite(double, short)
			public int intSQFT1;
			public double dblReplacement1;
			public double dblPhysical1;
			public double dblFunctional1;
			public int lngSubTotal1;
			// Second Column
			public string strOccupancy2 = string.Empty;
			public double dblBase2;
			public double dblHeat2;
			public string strHeat2 = "";
			public double dblTotal2;
			public double dblSize2;
			public double dblAdjustment2;
			// vbPorter upgrade warning: intSQFT2 As int	OnWrite(double, short)
			public int intSQFT2;
			public double dblReplacement2;
			public double dblPhysical2;
			public double dblFunctional2;
			public int lngSubTotal2;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
