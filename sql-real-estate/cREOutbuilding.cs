﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace TWRE0000
{
	public class cREOutbuilding
	{
		//=========================================================
		private int lngRecordID;
		private int lngAccountNumber;
		private int intCardNumber;
		private string strAccountID = "";
		private cGenericCollection collOutbuildings = new cGenericCollection();
		private bool boolUpdated;
		private bool boolDeleted;

		public cGenericCollection OutBuildings
		{
			get
			{
				cGenericCollection OutBuildings = null;
				OutBuildings = collOutbuildings;
				return OutBuildings;
			}
		}

		public int AccountNumber
		{
			set
			{
				lngAccountNumber = value;
				IsUpdated = true;
			}
			get
			{
				int AccountNumber = 0;
				AccountNumber = lngAccountNumber;
				return AccountNumber;
			}
		}

		public short CardNumber
		{
			set
			{
				intCardNumber = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short CardNumber = 0;
				CardNumber = FCConvert.ToInt16(intCardNumber);
				return CardNumber;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
	}
}
