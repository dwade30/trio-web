﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage3.
	/// </summary>
	partial class srptMVRPage3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine27a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine27b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine28b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine28c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine29a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine29b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine29c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label139 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label144 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label145 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label146 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine24a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine24b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine24c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label147 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label149 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblDateRange3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label195 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label196 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label197 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSoftwood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedWood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardwood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label198 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label199 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label200 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label202 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label204 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label205 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtSoftWoodAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedWoodAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardWoodAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label211 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label213 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine241 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label214 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblyear9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label216 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label218 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine24d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine27a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine27b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine28b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine28c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine29a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine29b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine29c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label139)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label144)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label145)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label146)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label147)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label149)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label151)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label195)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label196)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label197)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftwood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedWood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardwood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label198)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label199)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label200)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label201)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label202)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label205)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftWoodAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedWoodAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardWoodAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine241)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblyear9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label218)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape33,
            this.Shape31,
            this.Shape32,
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label74,
            this.Label109,
            this.Label110,
            this.Label58,
            this.Shape9,
            this.txtLine25,
            this.Label37,
            this.Label38,
            this.Label111,
            this.Shape10,
            this.txtLine26,
            this.Label112,
            this.Label113,
            this.lblYear1,
            this.Label114,
            this.Shape11,
            this.txtLine27a,
            this.Label115,
            this.Label116,
            this.Label117,
            this.Shape12,
            this.txtLine27b,
            this.Label118,
            this.Label119,
            this.Shape13,
            this.txtLine28b,
            this.Label120,
            this.Label121,
            this.Label122,
            this.Shape14,
            this.txtLine28c,
            this.Label123,
            this.Label124,
            this.Label125,
            this.Label126,
            this.Shape15,
            this.txtLine29a,
            this.Label127,
            this.lblYear2,
            this.Label128,
            this.Shape16,
            this.txtLine29b,
            this.Label129,
            this.lblYear3,
            this.Label130,
            this.Shape17,
            this.txtLine29c,
            this.Label131,
            this.lblYear4,
            this.Label133,
            this.Label134,
            this.Label135,
            this.Shape18,
            this.txtLine30,
            this.Label136,
            this.Label137,
            this.Label138,
            this.Shape19,
            this.txtLine31,
            this.Label139,
            this.Label140,
            this.lblYear5,
            this.Label141,
            this.Shape20,
            this.txtLine32,
            this.Label142,
            this.Label143,
            this.Label144,
            this.Shape21,
            this.txtLine33,
            this.Label145,
            this.Label146,
            this.Label175,
            this.Label176,
            this.Label177,
            this.Label178,
            this.Shape26,
            this.txtLine24a,
            this.Label180,
            this.Shape27,
            this.txtLine24b,
            this.Label182,
            this.Shape28,
            this.txtLine24c,
            this.Label183,
            this.Label147,
            this.Label149,
            this.Label151,
            this.Label186,
            this.Line4,
            this.lblDateRange3,
            this.lblDateRange4,
            this.lblDateRange5,
            this.Label195,
            this.Label132,
            this.Label196,
            this.Label197,
            this.txtSoftwood,
            this.txtMixedWood,
            this.txtHardwood,
            this.Label198,
            this.Label199,
            this.Label200,
            this.Label201,
            this.Label202,
            this.Label203,
            this.Label204,
            this.Label205,
            this.Shape34,
            this.Shape35,
            this.Shape36,
            this.txtSoftWoodAcres,
            this.txtMixedWoodAcres,
            this.txtHardWoodAcres,
            this.Label206,
            this.Label207,
            this.Label208,
            this.lblYear6,
            this.lblYear7,
            this.Label209,
            this.Label210,
            this.Shape25,
            this.txtLine23,
            this.Label211,
            this.Label212,
            this.lblYear8,
            this.Label213,
            this.Shape37,
            this.txtLine241,
            this.Label214,
            this.Line3,
            this.lblyear9,
            this.Label216,
            this.Label217,
            this.Label218,
            this.Shape38,
            this.txtLine24d,
            this.Label219});
			this.Detail.Height = 9.760416F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Shape33
			// 
			this.Shape33.Height = 0.19F;
			this.Shape33.Left = 5.625F;
			this.Shape33.Name = "Shape33";
			this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape33.Top = 6.815001F;
			this.Shape33.Width = 1.5625F;
			// 
			// Shape31
			// 
			this.Shape31.Height = 0.19F;
			this.Shape31.Left = 5.625F;
			this.Shape31.Name = "Shape31";
			this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape31.Top = 6.273334F;
			this.Shape31.Width = 1.5625F;
			// 
			// Shape32
			// 
			this.Shape32.Height = 0.19F;
			this.Shape32.Left = 5.625F;
			this.Shape32.Name = "Shape32";
			this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape32.Top = 6.523334F;
			this.Shape32.Width = 1.5625F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.375F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.375F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 1.1875F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.375F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.0625F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 1F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label74.Text = "LAND CLASSIFIED UNDER THE FARM AND OPEN SPACE TAX LAW PROGRAM";
			this.Label74.Top = 2.8125F;
			this.Label74.Width = 5.5F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.125F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 1F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-family: \'Times New Roman\'; font-size: 7pt; font-weight: bold; text-align: ce" +
    "nter";
			this.Label109.Text = "(36 M.R.S. §§ 1101 to 1121)";
			this.Label109.Top = 3F;
			this.Label109.Width = 5.489583F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1875F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 0F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold; text-align: le" +
    "ft";
			this.Label110.Text = "FARM LAND:";
			this.Label110.Top = 3.125F;
			this.Label110.Width = 1.125F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 5.375F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 9pt";
			this.Label58.Text = "25";
			this.Label58.Top = 3.3125F;
			this.Label58.Width = 0.25F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.19F;
			this.Shape9.Left = 5.625F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 3.3125F;
			this.Shape9.Width = 1.5625F;
			// 
			// txtLine25
			// 
			this.txtLine25.Height = 0.19F;
			this.txtLine25.Left = 5.6875F;
			this.txtLine25.Name = "txtLine25";
			this.txtLine25.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine25.Text = " ";
			this.txtLine25.Top = 3.3125F;
			this.txtLine25.Width = 1.4375F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 0F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 9pt; text-align: left";
			this.Label37.Text = "25.";
			this.Label37.Top = 3.3125F;
			this.Label37.Width = 0.25F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0.375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label38.Text = "Number of parcels classified as Farmland as of April 1,";
			this.Label38.Top = 3.3125F;
			this.Label38.Width = 2.6875F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 5.375F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-size: 9pt";
			this.Label111.Text = "26";
			this.Label111.Top = 3.5625F;
			this.Label111.Width = 0.25F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.19F;
			this.Shape10.Left = 5.625F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 3.5625F;
			this.Shape10.Width = 1.5625F;
			// 
			// txtLine26
			// 
			this.txtLine26.Height = 0.19F;
			this.txtLine26.Left = 5.6875F;
			this.txtLine26.Name = "txtLine26";
			this.txtLine26.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine26.Text = " ";
			this.txtLine26.Top = 3.5625F;
			this.txtLine26.Width = 1.4375F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-size: 9pt; text-align: left";
			this.Label112.Text = "26.";
			this.Label112.Top = 3.5625F;
			this.Label112.Width = 0.25F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1875F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 0.375F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label113.Text = "Number of acres first classified as Farmland for tax year";
			this.Label113.Top = 3.5625F;
			this.Label113.Width = 2.75F;
			// 
			// lblYear1
			// 
			this.lblYear1.Height = 0.1875F;
			this.lblYear1.HyperLink = null;
			this.lblYear1.Left = 3.125F;
			this.lblYear1.Name = "lblYear1";
			this.lblYear1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear1.Text = "2005";
			this.lblYear1.Top = 3.5625F;
			this.lblYear1.Width = 0.4375F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1875F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 5.375F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-size: 9pt";
			this.Label114.Text = "27a";
			this.Label114.Top = 4F;
			this.Label114.Width = 0.25F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.19F;
			this.Shape11.Left = 5.625F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 4F;
			this.Shape11.Width = 1.5625F;
			// 
			// txtLine27a
			// 
			this.txtLine27a.Height = 0.19F;
			this.txtLine27a.Left = 5.6875F;
			this.txtLine27a.Name = "txtLine27a";
			this.txtLine27a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine27a.Text = " ";
			this.txtLine27a.Top = 4F;
			this.txtLine27a.Width = 1.4375F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 0F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-size: 9pt; text-align: left";
			this.Label115.Text = "27.";
			this.Label115.Top = 3.8125F;
			this.Label115.Width = 0.25F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 0.375F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label116.Text = "a. Total number of acres of all land now classified as Farmland";
			this.Label116.Top = 3.8125F;
			this.Label116.Width = 4.625F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 5.375F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-size: 9pt";
			this.Label117.Text = "27b";
			this.Label117.Top = 4.256F;
			this.Label117.Width = 0.25F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.19F;
			this.Shape12.Left = 5.625F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 4.256F;
			this.Shape12.Width = 1.5625F;
			// 
			// txtLine27b
			// 
			this.txtLine27b.Height = 0.19F;
			this.txtLine27b.Left = 5.6875F;
			this.txtLine27b.Name = "txtLine27b";
			this.txtLine27b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine27b.Text = " ";
			this.txtLine27b.Top = 4.256F;
			this.txtLine27b.Width = 1.4375F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 0.375F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; white-space: nowrap";
			this.Label118.Text = "b.  Total number of acres of all land now classified as Farm woodland";
			this.Label118.Top = 5.5625F;
			this.Label118.Width = 4.625F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1875F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 5.375F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-size: 9pt";
			this.Label119.Text = "28b";
			this.Label119.Top = 5.5625F;
			this.Label119.Width = 0.25F;
			// 
			// Shape13
			// 
			this.Shape13.Height = 0.19F;
			this.Shape13.Left = 5.625F;
			this.Shape13.Name = "Shape13";
			this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape13.Top = 5.5625F;
			this.Shape13.Width = 1.5625F;
			// 
			// txtLine28b
			// 
			this.txtLine28b.Height = 0.19F;
			this.txtLine28b.Left = 5.6875F;
			this.txtLine28b.Name = "txtLine28b";
			this.txtLine28b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine28b.Text = " ";
			this.txtLine28b.Top = 5.5625F;
			this.txtLine28b.Width = 1.4375F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1875F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 0F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-size: 9pt; text-align: left";
			this.Label120.Text = "28.";
			this.Label120.Top = 4.5625F;
			this.Label120.Width = 0.25F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 0.375F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label121.Text = "b. Total valuation of all land now classifed as Farmland";
			this.Label121.Top = 4.229167F;
			this.Label121.Width = 4.625F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1875F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 5.375F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-size: 9pt";
			this.Label122.Text = "28c";
			this.Label122.Top = 5.8125F;
			this.Label122.Width = 0.25F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 0.19F;
			this.Shape14.Left = 5.625F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 5.8125F;
			this.Shape14.Width = 1.5625F;
			// 
			// txtLine28c
			// 
			this.txtLine28c.Height = 0.19F;
			this.txtLine28c.Left = 5.6875F;
			this.txtLine28c.Name = "txtLine28c";
			this.txtLine28c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine28c.Text = " ";
			this.txtLine28c.Top = 5.8125F;
			this.txtLine28c.Width = 1.4375F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 0.375F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label123.Text = "c. Total valuation of all land now classified as Farm woodland.";
			this.Label123.Top = 5.8125F;
			this.Label123.Width = 4.625F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1875F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 4.768372E-07F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-size: 9pt; text-align: left";
			this.Label124.Text = "29.";
			this.Label124.Top = 7.044168F;
			this.Label124.Width = 0.25F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1875F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 0.3750005F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label125.Text = "Land withdrawn from Farmland classification (36 M.R.S. § 1112)";
			this.Label125.Top = 7.044168F;
			this.Label125.Width = 4.625F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 5.375F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-size: 9pt";
			this.Label126.Text = "29a";
			this.Label126.Top = 7.294168F;
			this.Label126.Width = 0.25F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.19F;
			this.Shape15.Left = 5.625F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 7.294168F;
			this.Shape15.Width = 1.5625F;
			// 
			// txtLine29a
			// 
			this.txtLine29a.Height = 0.19F;
			this.txtLine29a.Left = 5.6875F;
			this.txtLine29a.Name = "txtLine29a";
			this.txtLine29a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine29a.Text = " ";
			this.txtLine29a.Top = 7.294168F;
			this.txtLine29a.Width = 1.4375F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 0.3750005F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label127.Text = "a.  Total number of parcels withdrawn from";
			this.Label127.Top = 7.294168F;
			this.Label127.Width = 2.1875F;
			// 
			// lblYear2
			// 
			this.lblYear2.Height = 0.1875F;
			this.lblYear2.HyperLink = null;
			this.lblYear2.Left = 2.5F;
			this.lblYear2.Name = "lblYear2";
			this.lblYear2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear2.Text = "2003 MVR";
			this.lblYear2.Top = 7.294168F;
			this.lblYear2.Width = 2F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 5.375F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-size: 9pt";
			this.Label128.Text = "29b";
			this.Label128.Top = 7.544168F;
			this.Label128.Width = 0.25F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.19F;
			this.Shape16.Left = 5.625F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 7.544168F;
			this.Shape16.Width = 1.5625F;
			// 
			// txtLine29b
			// 
			this.txtLine29b.Height = 0.19F;
			this.txtLine29b.Left = 5.6875F;
			this.txtLine29b.Name = "txtLine29b";
			this.txtLine29b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine29b.Text = " ";
			this.txtLine29b.Top = 7.544168F;
			this.txtLine29b.Width = 1.4375F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1875F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 0.3750005F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label129.Text = "b. Total number of acres withdrawn from";
			this.Label129.Top = 7.544168F;
			this.Label129.Width = 2.0625F;
			// 
			// lblYear3
			// 
			this.lblYear3.Height = 0.1875F;
			this.lblYear3.HyperLink = null;
			this.lblYear3.Left = 2.375F;
			this.lblYear3.Name = "lblYear3";
			this.lblYear3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear3.Text = "2003 MVR";
			this.lblYear3.Top = 7.544168F;
			this.lblYear3.Width = 2.125F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1875F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 5.375F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-size: 9pt";
			this.Label130.Text = "29c";
			this.Label130.Top = 7.794168F;
			this.Label130.Width = 0.25F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.19F;
			this.Shape17.Left = 5.625F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 7.794168F;
			this.Shape17.Width = 1.5625F;
			// 
			// txtLine29c
			// 
			this.txtLine29c.Height = 0.19F;
			this.txtLine29c.Left = 5.6875F;
			this.txtLine29c.Name = "txtLine29c";
			this.txtLine29c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine29c.Text = " ";
			this.txtLine29c.Top = 7.794168F;
			this.txtLine29c.Width = 1.4375F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1875F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 0.3750005F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label131.Text = "c.  Total value of penalties assessed by municipality due to the withdrawal";
			this.Label131.Top = 7.794168F;
			this.Label131.Width = 3.8125F;
			// 
			// lblYear4
			// 
			this.lblYear4.Height = 0.1875F;
			this.lblYear4.HyperLink = null;
			this.lblYear4.Left = 1.902F;
			this.lblYear4.Name = "lblYear4";
			this.lblYear4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear4.Text = "2003 MVR";
			this.lblYear4.Top = 7.982F;
			this.lblYear4.Width = 2.0625F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1875F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0.514F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label133.Text = "of classified Farmland from";
			this.Label133.Top = 7.981668F;
			this.Label133.Width = 1.375F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1875F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 0F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold; text-align: le" +
    "ft";
			this.Label134.Text = "OPEN SPACE:";
			this.Label134.Top = 8.206667F;
			this.Label134.Width = 1.125F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1875F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 5.375F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-size: 9pt";
			this.Label135.Text = "30";
			this.Label135.Top = 8.456667F;
			this.Label135.Width = 0.25F;
			// 
			// Shape18
			// 
			this.Shape18.Height = 0.19F;
			this.Shape18.Left = 5.625F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 8.456667F;
			this.Shape18.Width = 1.5625F;
			// 
			// txtLine30
			// 
			this.txtLine30.Height = 0.19F;
			this.txtLine30.Left = 5.6875F;
			this.txtLine30.Name = "txtLine30";
			this.txtLine30.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine30.Text = " ";
			this.txtLine30.Top = 8.456667F;
			this.txtLine30.Width = 1.4375F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1875F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 0F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-size: 9pt; text-align: left";
			this.Label136.Text = "30.";
			this.Label136.Top = 8.456667F;
			this.Label136.Width = 0.25F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1875F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 0.375F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label137.Text = "Number of parcels classified as Open Space as of April 1,";
			this.Label137.Top = 8.456667F;
			this.Label137.Width = 2.8125F;
			// 
			// Label138
			// 
			this.Label138.Height = 0.1875F;
			this.Label138.HyperLink = null;
			this.Label138.Left = 5.375F;
			this.Label138.Name = "Label138";
			this.Label138.Style = "font-size: 9pt";
			this.Label138.Text = "31";
			this.Label138.Top = 8.706667F;
			this.Label138.Width = 0.25F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.19F;
			this.Shape19.Left = 5.625F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 8.706667F;
			this.Shape19.Width = 1.5625F;
			// 
			// txtLine31
			// 
			this.txtLine31.Height = 0.19F;
			this.txtLine31.Left = 5.6875F;
			this.txtLine31.Name = "txtLine31";
			this.txtLine31.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine31.Text = " ";
			this.txtLine31.Top = 8.706667F;
			this.txtLine31.Width = 1.4375F;
			// 
			// Label139
			// 
			this.Label139.Height = 0.1875F;
			this.Label139.HyperLink = null;
			this.Label139.Left = 0F;
			this.Label139.Name = "Label139";
			this.Label139.Style = "font-size: 9pt; text-align: left";
			this.Label139.Text = "31.";
			this.Label139.Top = 8.706667F;
			this.Label139.Width = 0.25F;
			// 
			// Label140
			// 
			this.Label140.Height = 0.1875F;
			this.Label140.HyperLink = null;
			this.Label140.Left = 0.375F;
			this.Label140.Name = "Label140";
			this.Label140.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label140.Text = "Number of acres first classified as Open Space for tax year";
			this.Label140.Top = 8.706667F;
			this.Label140.Width = 2.875F;
			// 
			// lblYear5
			// 
			this.lblYear5.Height = 0.1875F;
			this.lblYear5.HyperLink = null;
			this.lblYear5.Left = 3.1875F;
			this.lblYear5.Name = "lblYear5";
			this.lblYear5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear5.Text = "2004";
			this.lblYear5.Top = 8.706667F;
			this.lblYear5.Width = 0.4375F;
			// 
			// Label141
			// 
			this.Label141.Height = 0.1875F;
			this.Label141.HyperLink = null;
			this.Label141.Left = 5.375F;
			this.Label141.Name = "Label141";
			this.Label141.Style = "font-size: 9pt";
			this.Label141.Text = "32";
			this.Label141.Top = 8.956667F;
			this.Label141.Width = 0.25F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.19F;
			this.Shape20.Left = 5.625F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 8.956667F;
			this.Shape20.Width = 1.5625F;
			// 
			// txtLine32
			// 
			this.txtLine32.Height = 0.19F;
			this.txtLine32.Left = 5.6875F;
			this.txtLine32.Name = "txtLine32";
			this.txtLine32.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine32.Text = " ";
			this.txtLine32.Top = 8.956667F;
			this.txtLine32.Width = 1.4375F;
			// 
			// Label142
			// 
			this.Label142.Height = 0.1875F;
			this.Label142.HyperLink = null;
			this.Label142.Left = 0F;
			this.Label142.Name = "Label142";
			this.Label142.Style = "font-size: 9pt; text-align: left";
			this.Label142.Text = "32.";
			this.Label142.Top = 8.956667F;
			this.Label142.Width = 0.25F;
			// 
			// Label143
			// 
			this.Label143.Height = 0.1875F;
			this.Label143.HyperLink = null;
			this.Label143.Left = 0.375F;
			this.Label143.Name = "Label143";
			this.Label143.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label143.Text = "Total number of acres of land now classified as Open Space.";
			this.Label143.Top = 8.956667F;
			this.Label143.Width = 4.625F;
			// 
			// Label144
			// 
			this.Label144.Height = 0.1875F;
			this.Label144.HyperLink = null;
			this.Label144.Left = 5.375F;
			this.Label144.Name = "Label144";
			this.Label144.Style = "font-size: 9pt";
			this.Label144.Text = "33";
			this.Label144.Top = 9.207F;
			this.Label144.Width = 0.25F;
			// 
			// Shape21
			// 
			this.Shape21.Height = 0.19F;
			this.Shape21.Left = 5.625F;
			this.Shape21.Name = "Shape21";
			this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape21.Top = 9.207F;
			this.Shape21.Width = 1.5625F;
			// 
			// txtLine33
			// 
			this.txtLine33.Height = 0.19F;
			this.txtLine33.Left = 5.688F;
			this.txtLine33.Name = "txtLine33";
			this.txtLine33.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine33.Text = " ";
			this.txtLine33.Top = 9.207F;
			this.txtLine33.Width = 1.4375F;
			// 
			// Label145
			// 
			this.Label145.Height = 0.1875F;
			this.Label145.HyperLink = null;
			this.Label145.Left = 0F;
			this.Label145.Name = "Label145";
			this.Label145.Style = "font-size: 9pt; text-align: left";
			this.Label145.Text = "33.";
			this.Label145.Top = 9.207F;
			this.Label145.Width = 0.25F;
			// 
			// Label146
			// 
			this.Label146.Height = 0.1875F;
			this.Label146.HyperLink = null;
			this.Label146.Left = 0.375F;
			this.Label146.Name = "Label146";
			this.Label146.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label146.Text = "Total valuation of all land now classified as Open Space.";
			this.Label146.Top = 9.207F;
			this.Label146.Width = 4.625F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-3-";
			this.Label175.Top = 9.540001F;
			this.Label175.Width = 1F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 0F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-size: 9pt; text-align: left";
			this.Label176.Text = "24.";
			this.Label176.Top = 1.125F;
			this.Label176.Width = 0.25F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1875F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 0.375F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label177.Text = "Land withdrawn from Tree Growth classification (36 M.R.S. § 581)";
			this.Label177.Top = 1.125F;
			this.Label177.Width = 4.9375F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.1875F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 5.375F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-size: 9pt";
			this.Label178.Text = "24a";
			this.Label178.Top = 1.375F;
			this.Label178.Width = 0.25F;
			// 
			// Shape26
			// 
			this.Shape26.Height = 0.1875F;
			this.Shape26.Left = 5.625F;
			this.Shape26.Name = "Shape26";
			this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape26.Top = 1.375F;
			this.Shape26.Width = 1.5625F;
			// 
			// txtLine24a
			// 
			this.txtLine24a.Height = 0.1875F;
			this.txtLine24a.Left = 5.75F;
			this.txtLine24a.Name = "txtLine24a";
			this.txtLine24a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine24a.Text = " ";
			this.txtLine24a.Top = 1.375F;
			this.txtLine24a.Width = 1.375F;
			// 
			// Label180
			// 
			this.Label180.Height = 0.1875F;
			this.Label180.HyperLink = null;
			this.Label180.Left = 5.375F;
			this.Label180.Name = "Label180";
			this.Label180.Style = "font-size: 9pt";
			this.Label180.Text = "24b";
			this.Label180.Top = 1.625F;
			this.Label180.Width = 0.25F;
			// 
			// Shape27
			// 
			this.Shape27.Height = 0.1875F;
			this.Shape27.Left = 5.625F;
			this.Shape27.Name = "Shape27";
			this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape27.Top = 1.625F;
			this.Shape27.Width = 1.5625F;
			// 
			// txtLine24b
			// 
			this.txtLine24b.Height = 0.1875F;
			this.txtLine24b.Left = 5.75F;
			this.txtLine24b.Name = "txtLine24b";
			this.txtLine24b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine24b.Text = " ";
			this.txtLine24b.Top = 1.625F;
			this.txtLine24b.Width = 1.375F;
			// 
			// Label182
			// 
			this.Label182.Height = 0.1875F;
			this.Label182.HyperLink = null;
			this.Label182.Left = 5.375F;
			this.Label182.Name = "Label182";
			this.Label182.Style = "font-size: 9pt";
			this.Label182.Text = "24c";
			this.Label182.Top = 1.875F;
			this.Label182.Width = 0.25F;
			// 
			// Shape28
			// 
			this.Shape28.Height = 0.1875F;
			this.Shape28.Left = 5.625F;
			this.Shape28.Name = "Shape28";
			this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape28.Top = 1.875F;
			this.Shape28.Width = 1.5625F;
			// 
			// txtLine24c
			// 
			this.txtLine24c.Height = 0.1875F;
			this.txtLine24c.Left = 5.75F;
			this.txtLine24c.Name = "txtLine24c";
			this.txtLine24c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine24c.Text = " ";
			this.txtLine24c.Top = 1.875F;
			this.txtLine24c.Width = 1.375F;
			// 
			// Label183
			// 
			this.Label183.Height = 0.1875F;
			this.Label183.HyperLink = null;
			this.Label183.Left = 0.375F;
			this.Label183.Name = "Label183";
			this.Label183.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label183.Text = "a. Total number of parcels withdrawn from";
			this.Label183.Top = 1.375F;
			this.Label183.Width = 2.5625F;
			// 
			// Label147
			// 
			this.Label147.Height = 0.1875F;
			this.Label147.HyperLink = null;
			this.Label147.Left = 0.375F;
			this.Label147.Name = "Label147";
			this.Label147.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label147.Text = "b. Total number of acres withdrawn from";
			this.Label147.Top = 1.625F;
			this.Label147.Width = 2.5F;
			// 
			// Label149
			// 
			this.Label149.Height = 0.1875F;
			this.Label149.HyperLink = null;
			this.Label149.Left = 0.375F;
			this.Label149.Name = "Label149";
			this.Label149.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label149.Text = "c. Total value of penalties assessed by the municipality due to withdrawal of";
			this.Label149.Top = 1.875F;
			this.Label149.Width = 4.6875F;
			// 
			// Label151
			// 
			this.Label151.Height = 0.1875F;
			this.Label151.HyperLink = null;
			this.Label151.Left = 0.514F;
			this.Label151.Name = "Label151";
			this.Label151.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label151.Text = "classified Tree Growth land from";
			this.Label151.Top = 2.063F;
			this.Label151.Width = 1.875F;
			// 
			// Label186
			// 
			this.Label186.Height = 0.1875F;
			this.Label186.HyperLink = null;
			this.Label186.Left = 1F;
			this.Label186.Name = "Label186";
			this.Label186.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label186.Text = "TREE GROWTH TAX LAW CONTINUED";
			this.Label186.Top = 0.625F;
			this.Label186.Width = 5.5F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 0.625F;
			this.Line4.Y2 = 0.625F;
			// 
			// lblDateRange3
			// 
			this.lblDateRange3.Height = 0.1875F;
			this.lblDateRange3.HyperLink = null;
			this.lblDateRange3.Left = 2.5F;
			this.lblDateRange3.Name = "lblDateRange3";
			this.lblDateRange3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange3.Text = "04/02/04 to 04/01/05";
			this.lblDateRange3.Top = 1.375F;
			this.lblDateRange3.Width = 1.9375F;
			// 
			// lblDateRange4
			// 
			this.lblDateRange4.Height = 0.1875F;
			this.lblDateRange4.HyperLink = null;
			this.lblDateRange4.Left = 2.4375F;
			this.lblDateRange4.Name = "lblDateRange4";
			this.lblDateRange4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange4.Text = "04/02/04 to 04/01/05";
			this.lblDateRange4.Top = 1.625F;
			this.lblDateRange4.Width = 2F;
			// 
			// lblDateRange5
			// 
			this.lblDateRange5.Height = 0.1875F;
			this.lblDateRange5.HyperLink = null;
			this.lblDateRange5.Left = 2.207F;
			this.lblDateRange5.Name = "lblDateRange5";
			this.lblDateRange5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblDateRange5.Text = "04/02/04 and 04/01/05";
			this.lblDateRange5.Top = 2.062F;
			this.lblDateRange5.Width = 2.25F;
			// 
			// Label195
			// 
			this.Label195.Height = 0.1875F;
			this.Label195.HyperLink = null;
			this.Label195.Left = 0.375F;
			this.Label195.Name = "Label195";
			this.Label195.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label195.Text = "d. Per acre rates used for Farm woodland:";
			this.Label195.Top = 6.083333F;
			this.Label195.Width = 2.875F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1875F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0.511F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label132.Text = "(1) Soft wood";
			this.Label132.Top = 6.273F;
			this.Label132.Width = 1.020833F;
			// 
			// Label196
			// 
			this.Label196.Height = 0.1875F;
			this.Label196.HyperLink = null;
			this.Label196.Left = 0.511F;
			this.Label196.Name = "Label196";
			this.Label196.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label196.Text = "(2) Mixed wood";
			this.Label196.Top = 6.543834F;
			this.Label196.Width = 1.0625F;
			// 
			// Label197
			// 
			this.Label197.Height = 0.1875F;
			this.Label197.HyperLink = null;
			this.Label197.Left = 0.511F;
			this.Label197.Name = "Label197";
			this.Label197.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label197.Text = "(3) Hard wood";
			this.Label197.Top = 6.814667F;
			this.Label197.Width = 1.020833F;
			// 
			// txtSoftwood
			// 
			this.txtSoftwood.Height = 0.19F;
			this.txtSoftwood.Left = 5.6875F;
			this.txtSoftwood.Name = "txtSoftwood";
			this.txtSoftwood.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtSoftwood.Text = " ";
			this.txtSoftwood.Top = 6.273334F;
			this.txtSoftwood.Width = 1.4375F;
			// 
			// txtMixedWood
			// 
			this.txtMixedWood.Height = 0.19F;
			this.txtMixedWood.Left = 5.6875F;
			this.txtMixedWood.Name = "txtMixedWood";
			this.txtMixedWood.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtMixedWood.Text = " ";
			this.txtMixedWood.Top = 6.523334F;
			this.txtMixedWood.Width = 1.4375F;
			// 
			// txtHardwood
			// 
			this.txtHardwood.Height = 0.19F;
			this.txtHardwood.Left = 5.6875F;
			this.txtHardwood.Name = "txtHardwood";
			this.txtHardwood.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtHardwood.Text = " ";
			this.txtHardwood.Top = 6.815001F;
			this.txtHardwood.Width = 1.4375F;
			// 
			// Label198
			// 
			this.Label198.Height = 0.1875F;
			this.Label198.HyperLink = null;
			this.Label198.Left = 0.514F;
			this.Label198.Name = "Label198";
			this.Label198.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label198.Text = "(Do not include Farm woodland)";
			this.Label198.Top = 4.416667F;
			this.Label198.Width = 2.125F;
			// 
			// Label199
			// 
			this.Label199.Height = 0.1875F;
			this.Label199.HyperLink = null;
			this.Label199.Left = 5.208333F;
			this.Label199.Name = "Label199";
			this.Label199.Style = "font-size: 9pt";
			this.Label199.Text = "28d(1)";
			this.Label199.Top = 6.273334F;
			this.Label199.Width = 0.4166667F;
			// 
			// Label200
			// 
			this.Label200.Height = 0.1875F;
			this.Label200.HyperLink = null;
			this.Label200.Left = 5.208333F;
			this.Label200.Name = "Label200";
			this.Label200.Style = "font-size: 9pt";
			this.Label200.Text = "28d(2)";
			this.Label200.Top = 6.523334F;
			this.Label200.Width = 0.4166667F;
			// 
			// Label201
			// 
			this.Label201.Height = 0.1875F;
			this.Label201.HyperLink = null;
			this.Label201.Left = 5.208333F;
			this.Label201.Name = "Label201";
			this.Label201.Style = "font-size: 9pt";
			this.Label201.Text = "28d(3)";
			this.Label201.Top = 6.815001F;
			this.Label201.Width = 0.4166667F;
			// 
			// Label202
			// 
			this.Label202.Height = 0.1875F;
			this.Label202.HyperLink = null;
			this.Label202.Left = 0.375F;
			this.Label202.Name = "Label202";
			this.Label202.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label202.Text = "a. Number of Farm woodland acres";
			this.Label202.Top = 4.5625F;
			this.Label202.Width = 3.291667F;
			// 
			// Label203
			// 
			this.Label203.Height = 0.1875F;
			this.Label203.HyperLink = null;
			this.Label203.Left = 0.4791667F;
			this.Label203.Name = "Label203";
			this.Label203.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label203.Text = "(1) Softwood acreage";
			this.Label203.Top = 4.75F;
			this.Label203.Width = 1.395833F;
			// 
			// Label204
			// 
			this.Label204.Height = 0.1875F;
			this.Label204.HyperLink = null;
			this.Label204.Left = 0.4791667F;
			this.Label204.Name = "Label204";
			this.Label204.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label204.Text = "(2) Mixed wood acreage";
			this.Label204.Top = 5.020833F;
			this.Label204.Width = 1.4375F;
			// 
			// Label205
			// 
			this.Label205.Height = 0.1875F;
			this.Label205.HyperLink = null;
			this.Label205.Left = 0.4791667F;
			this.Label205.Name = "Label205";
			this.Label205.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label205.Text = "(3) Hardwood acreage";
			this.Label205.Top = 5.291667F;
			this.Label205.Width = 1.395833F;
			// 
			// Shape34
			// 
			this.Shape34.Height = 0.19F;
			this.Shape34.Left = 5.625F;
			this.Shape34.Name = "Shape34";
			this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape34.Top = 5.291667F;
			this.Shape34.Width = 1.5625F;
			// 
			// Shape35
			// 
			this.Shape35.Height = 0.19F;
			this.Shape35.Left = 5.625F;
			this.Shape35.Name = "Shape35";
			this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape35.Top = 4.75F;
			this.Shape35.Width = 1.5625F;
			// 
			// Shape36
			// 
			this.Shape36.Height = 0.19F;
			this.Shape36.Left = 5.625F;
			this.Shape36.Name = "Shape36";
			this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape36.Top = 5F;
			this.Shape36.Width = 1.5625F;
			// 
			// txtSoftWoodAcres
			// 
			this.txtSoftWoodAcres.Height = 0.19F;
			this.txtSoftWoodAcres.Left = 5.6875F;
			this.txtSoftWoodAcres.Name = "txtSoftWoodAcres";
			this.txtSoftWoodAcres.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtSoftWoodAcres.Text = " ";
			this.txtSoftWoodAcres.Top = 4.75F;
			this.txtSoftWoodAcres.Width = 1.4375F;
			// 
			// txtMixedWoodAcres
			// 
			this.txtMixedWoodAcres.Height = 0.19F;
			this.txtMixedWoodAcres.Left = 5.6875F;
			this.txtMixedWoodAcres.Name = "txtMixedWoodAcres";
			this.txtMixedWoodAcres.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtMixedWoodAcres.Text = " ";
			this.txtMixedWoodAcres.Top = 5F;
			this.txtMixedWoodAcres.Width = 1.4375F;
			// 
			// txtHardWoodAcres
			// 
			this.txtHardWoodAcres.Height = 0.19F;
			this.txtHardWoodAcres.Left = 5.6875F;
			this.txtHardWoodAcres.Name = "txtHardWoodAcres";
			this.txtHardWoodAcres.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtHardWoodAcres.Text = " ";
			this.txtHardWoodAcres.Top = 5.291667F;
			this.txtHardWoodAcres.Width = 1.4375F;
			// 
			// Label206
			// 
			this.Label206.Height = 0.1875F;
			this.Label206.HyperLink = null;
			this.Label206.Left = 5.208333F;
			this.Label206.Name = "Label206";
			this.Label206.Style = "font-size: 9pt";
			this.Label206.Text = "28a(1)";
			this.Label206.Top = 4.75F;
			this.Label206.Width = 0.4166667F;
			// 
			// Label207
			// 
			this.Label207.Height = 0.1875F;
			this.Label207.HyperLink = null;
			this.Label207.Left = 5.208333F;
			this.Label207.Name = "Label207";
			this.Label207.Style = "font-size: 9pt";
			this.Label207.Text = "28a(2)";
			this.Label207.Top = 5F;
			this.Label207.Width = 0.4166667F;
			// 
			// Label208
			// 
			this.Label208.Height = 0.1875F;
			this.Label208.HyperLink = null;
			this.Label208.Left = 5.208333F;
			this.Label208.Name = "Label208";
			this.Label208.Style = "font-size: 9pt";
			this.Label208.Text = "28a(3)";
			this.Label208.Top = 5.291667F;
			this.Label208.Width = 0.4166667F;
			// 
			// lblYear6
			// 
			this.lblYear6.Height = 0.1875F;
			this.lblYear6.HyperLink = null;
			this.lblYear6.Left = 3.0625F;
			this.lblYear6.Name = "lblYear6";
			this.lblYear6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear6.Text = "2005";
			this.lblYear6.Top = 3.3125F;
			this.lblYear6.Width = 0.4375F;
			// 
			// lblYear7
			// 
			this.lblYear7.Height = 0.1875F;
			this.lblYear7.HyperLink = null;
			this.lblYear7.Left = 3.125F;
			this.lblYear7.Name = "lblYear7";
			this.lblYear7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear7.Text = "2005";
			this.lblYear7.Top = 8.456667F;
			this.lblYear7.Width = 0.4375F;
			// 
			// Label209
			// 
			this.Label209.Height = 0.1875F;
			this.Label209.HyperLink = null;
			this.Label209.Left = 0.514F;
			this.Label209.Name = "Label209";
			this.Label209.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label209.Text = "(Do not include Farm woodland)";
			this.Label209.Top = 4F;
			this.Label209.Width = 1.875F;
			// 
			// Label210
			// 
			this.Label210.Height = 0.1875F;
			this.Label210.HyperLink = null;
			this.Label210.Left = 5.375F;
			this.Label210.Name = "Label210";
			this.Label210.Style = "font-size: 9pt";
			this.Label210.Text = "23";
			this.Label210.Top = 0.875F;
			this.Label210.Width = 0.25F;
			// 
			// Shape25
			// 
			this.Shape25.Height = 0.1875F;
			this.Shape25.Left = 5.625F;
			this.Shape25.Name = "Shape25";
			this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape25.Top = 0.875F;
			this.Shape25.Width = 1.5625F;
			// 
			// txtLine23
			// 
			this.txtLine23.Height = 0.1875F;
			this.txtLine23.Left = 5.729167F;
			this.txtLine23.Name = "txtLine23";
			this.txtLine23.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine23.Text = " ";
			this.txtLine23.Top = 0.875F;
			this.txtLine23.Width = 1.375F;
			// 
			// Label211
			// 
			this.Label211.Height = 0.1875F;
			this.Label211.HyperLink = null;
			this.Label211.Left = 0F;
			this.Label211.Name = "Label211";
			this.Label211.Style = "font-size: 9pt; text-align: left";
			this.Label211.Text = "23.";
			this.Label211.Top = 0.875F;
			this.Label211.Width = 0.25F;
			// 
			// Label212
			// 
			this.Label212.Height = 0.1875F;
			this.Label212.HyperLink = null;
			this.Label212.Left = 0.375F;
			this.Label212.Name = "Label212";
			this.Label212.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label212.Text = "Number of forestland acres first classified for tax year";
			this.Label212.Top = 0.875F;
			this.Label212.Width = 2.75F;
			// 
			// lblYear8
			// 
			this.lblYear8.Height = 0.1875F;
			this.lblYear8.HyperLink = null;
			this.lblYear8.Left = 3.013F;
			this.lblYear8.Name = "lblYear8";
			this.lblYear8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear8.Text = "2005";
			this.lblYear8.Top = 0.8750001F;
			this.lblYear8.Width = 0.375F;
			// 
			// Label213
			// 
			this.Label213.Height = 0.1875F;
			this.Label213.HyperLink = null;
			this.Label213.Left = 5.375F;
			this.Label213.Name = "Label213";
			this.Label213.Style = "font-size: 9pt";
			this.Label213.Text = "24-1";
			this.Label213.Top = 2.5625F;
			this.Label213.Width = 0.25F;
			// 
			// Shape37
			// 
			this.Shape37.Height = 0.1875F;
			this.Shape37.Left = 5.625F;
			this.Shape37.Name = "Shape37";
			this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape37.Top = 2.5625F;
			this.Shape37.Width = 1.5625F;
			// 
			// txtLine241
			// 
			this.txtLine241.Height = 0.1875F;
			this.txtLine241.Left = 5.75F;
			this.txtLine241.Name = "txtLine241";
			this.txtLine241.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine241.Text = " ";
			this.txtLine241.Top = 2.5625F;
			this.txtLine241.Width = 1.375F;
			// 
			// Label214
			// 
			this.Label214.Height = 0.1875F;
			this.Label214.HyperLink = null;
			this.Label214.Left = 0.375F;
			this.Label214.Name = "Label214";
			this.Label214.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label214.Text = "Since April 1,";
			this.Label214.Top = 2.5625F;
			this.Label214.Width = 0.875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.8125F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 2.8125F;
			this.Line3.Y2 = 2.8125F;
			// 
			// lblyear9
			// 
			this.lblyear9.Height = 0.1875F;
			this.lblyear9.HyperLink = null;
			this.lblyear9.Left = 1.1875F;
			this.lblyear9.Name = "lblyear9";
			this.lblyear9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblyear9.Text = "2005";
			this.lblyear9.Top = 2.5625F;
			this.lblyear9.Width = 0.375F;
			// 
			// Label216
			// 
			this.Label216.Height = 0.1875F;
			this.Label216.HyperLink = null;
			this.Label216.Left = 1.5F;
			this.Label216.Name = "Label216";
			this.Label216.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label216.Text = "have any Tree Growth acres been transferred to Farmland?";
			this.Label216.Top = 2.5625F;
			this.Label216.Width = 3.5625F;
			// 
			// Label217
			// 
			this.Label217.Height = 0.1875F;
			this.Label217.HyperLink = null;
			this.Label217.Left = 0F;
			this.Label217.Name = "Label217";
			this.Label217.Style = "font-size: 9pt; text-align: left";
			this.Label217.Text = "24-1";
			this.Label217.Top = 2.5625F;
			this.Label217.Width = 0.317F;
			// 
			// Label218
			// 
			this.Label218.Height = 0.1875F;
			this.Label218.HyperLink = null;
			this.Label218.Left = 5.375F;
			this.Label218.Name = "Label218";
			this.Label218.Style = "font-size: 9pt";
			this.Label218.Text = "24d";
			this.Label218.Top = 2.3125F;
			this.Label218.Width = 0.25F;
			// 
			// Shape38
			// 
			this.Shape38.Height = 0.1875F;
			this.Shape38.Left = 5.625F;
			this.Shape38.Name = "Shape38";
			this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape38.Top = 2.3125F;
			this.Shape38.Width = 1.5625F;
			// 
			// txtLine24d
			// 
			this.txtLine24d.Height = 0.1875F;
			this.txtLine24d.Left = 5.75F;
			this.txtLine24d.Name = "txtLine24d";
			this.txtLine24d.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine24d.Text = " ";
			this.txtLine24d.Top = 2.3125F;
			this.txtLine24d.Width = 1.375F;
			// 
			// Label219
			// 
			this.Label219.Height = 0.1875F;
			this.Label219.HyperLink = null;
			this.Label219.Left = 0.375F;
			this.Label219.Name = "Label219";
			this.Label219.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label219.Text = "d. Total number of $500 penalties assessed for non-compliance";
			this.Label219.Top = 2.3125F;
			this.Label219.Width = 3.6875F;
			// 
			// srptMVRPage3
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine27a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine27b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine28b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine28c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine29a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine29b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine29c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label139)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label144)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label145)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label146)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label147)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label149)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label151)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label195)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label196)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label197)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftwood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedWood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardwood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label198)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label199)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label200)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label201)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label202)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label205)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftWoodAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedWoodAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardWoodAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine241)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblyear9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label218)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine24d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine27a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine27b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine28b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine28c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine29a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine29b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine29c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label139;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label140;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label144;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label145;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label146;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine24a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine24b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine24c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label183;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label147;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label149;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label151;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label186;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label195;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label196;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label197;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftwood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedWood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardwood;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label198;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label199;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label200;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label201;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label202;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label203;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label204;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label205;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftWoodAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedWoodAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardWoodAcres;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label207;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label208;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label210;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label211;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label213;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine241;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label214;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblyear9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label216;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label218;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine24d;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label219;
	}
}
