﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPNotUpdatedAccounts.
	/// </summary>
	public partial class rptPPNotUpdatedAccounts : BaseSectionReport
	{
		public rptPPNotUpdatedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Accounts Not Updated";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPPNotUpdatedAccounts InstancePtr
		{
			get
			{
				return (rptPPNotUpdatedAccounts)Sys.GetInstance(typeof(rptPPNotUpdatedAccounts));
			}
		}

		protected rptPPNotUpdatedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPNotUpdatedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCRow >= frmPPImportXF.InstancePtr.GridNotUpdated.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			txtTotal.Text = Strings.Format(frmPPImportXF.InstancePtr.GridNotUpdated.Rows - 1, "#,###,###,##0");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtName.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPOWNER));
			txtTRIOAccount.Text = frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPACCOUNT);
			txtOtherAccount.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPOTHERACCOUNT));
			if (Conversion.Val(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER)) > 0)
			{
				txtLocation.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER) + " " + frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
			}
			else
			{
				txtLocation.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
			}
			if (Conversion.Val(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPBUSINESSCODE)) > 0)
			{
				txtBusinessCode.Text = frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPBUSINESSCODE);
			}
			else
			{
				txtBusinessCode.Text = "";
			}
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
