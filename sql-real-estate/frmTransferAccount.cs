﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTransferAccount.
	/// </summary>
	public partial class frmTransferAccount : BaseForm
	{
		public frmTransferAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label8.AddControlArrayElement(Label8_0, 0);
			this.Label2.AddControlArrayElement(Label2_9, 9);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTransferAccount InstancePtr
		{
			get
			{
				return (frmTransferAccount)Sys.GetInstance(typeof(frmTransferAccount));
			}
		}

		protected frmTransferAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int lngCurrentPendingID;
		const int CNSTSALECOLPRICE = 0;
		const int CNSTSALECOLDATE = 1;
		const int CNSTSALECOLSALE = 2;
		const int CNSTSALECOLFINANCING = 3;
		const int CNSTSALECOLVERIFIED = 4;
		const int CNSTSALECOLVALIDITY = 5;

		private void cmdSearchOwner_Click(object sender, System.EventArgs e)
		{
			int lngReturnID;
			lngReturnID = frmCentralPartySearch.InstancePtr.Init();
			ShowOwner(lngReturnID);
		}

		private void Command2_Click()
		{
		}

		private void cmdSearchSecOwner_Click(object sender, System.EventArgs e)
		{
			int lngReturnID;
			lngReturnID = frmCentralPartySearch.InstancePtr.Init();
			ShowSecOwner(lngReturnID);
		}

		private void frmTransferAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
			else if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
		}

		private void frmTransferAccount_Load(object sender, System.EventArgs e)
		{
			Grid.TextMatrix(0, 0, "Card(s)");
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			
			DefaultInfo();
		}

		private void DefaultInfo()
		{
			string strTemp = "";
			// put in current information
			if (lngCurrentPendingID <= 0)
			{
				T2KTransferDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				strTemp = modDataTypes.Statics.MR.Get_Fields_String("rstelephone") + "";
				strTemp = modGlobalRoutines.PadToString(strTemp, 10);
				// txtPhone.Text = "(" & Mid(strTemp, 1, 3) & ")" & Mid(strTemp, 4, 3) & "-" & Mid(strTemp, 7)
				txtMapLot.Text = modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "";
				txtRef1.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + "";
				txtRef2.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref2") + "";
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + "") > 0)
				{
					txtStreetNumber.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + ""));
				}
				txtApt.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocapt") + "");
				txtStreet.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + "");
			}
		}

		private void frmTransferAccount_Resize(object sender, System.EventArgs e)
		{
			ResizeSaleGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			lngCurrentPendingID = 0;
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (Grid.Rows < 2)
			{
				Grid.AddItem("");
			}
			else
			{
				if (Strings.Trim(Grid.TextMatrix(Grid.Rows - 1, 0)) != string.Empty)
				{
					Grid.AddItem("");
				}
			}
			Grid.Row = Grid.Rows - 1;
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Delete)
			{
				if (Grid.Row < 1)
					return;
				if (Grid.Row == Grid.Rows - 1)
					return;
				KeyCode = 0;
				Grid.RemoveItem(Grid.Row);
				if (Grid.Rows < 2)
					Grid.AddItem("");
			}
		}

		private void Grid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (Grid.Row < Grid.Rows - 1)
				{
					Grid.Row = Grid.Row + 1;
				}
				else
				{
					if (Strings.Trim(Grid.EditText) == string.Empty)
						return;
					Grid.AddItem("");
					Grid.Row = Grid.Rows - 1;
				}
			}
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Grid.Rows < 2)
			{
				Grid.AddItem("");
			}
			else
			{
				if (Strings.Trim(Grid.TextMatrix(Grid.Rows - 1, 0)) != string.Empty)
				{
					Grid.AddItem("");
				}
			}
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				int x;
				string strSQL;
				clsDRWrapper clsSave = new clsDRWrapper();
                var clsTemp = new clsDRWrapper();
				string strTemp;
				string strPhone = "";
				int intTemp;
                string strCards = "";
				SaleGrid.Row = 0;
				if (!Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
				{
					if (MessageBox.Show("If you don't specify a sale date, bills will not be able to use the previous owner information and the sale record will not be updated." + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						return;
					}
				}

                clsSave.OpenRecordset("select * from pending where id = " + lngCurrentPendingID.ToString(),
                    "RealEstate");
                if (!clsSave.EndOfFile())
                {
                    clsSave.Edit();
                }
                else
                {
                    clsSave.AddNew();
                }

				strSQL = "";
                clsSave.Set_Fields("account",modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
                if (cmbTransfer.Text == "Entire Account")
				{
					clsSave.Set_Fields("transferType",0);
                    clsSave.Set_Fields("Cards","");
				}
				else
				{
					clsSave.Set_Fields("TransferType",1);
					// create a space delimited string containing the cards to transfer
					if (Grid.Rows < 3)
					{
						if (Grid.Rows == 1)
						{
							MessageBox.Show("You have chosen to transfer specific cards without specifying the cards to transfer.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						else
						{
							if (Conversion.Val(Grid.TextMatrix(1, 0)) == 0)
							{
								MessageBox.Show("You have chosen to transfer specific cards without specifying the cards to transfer.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
					clsTemp.OpenRecordset("select id,rscard from master where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					for (x = 1; x <= Grid.Rows - 1; x++)
					{
						if (Conversion.Val(Grid.TextMatrix(x, 0)) > 0)
						{
							if (clsTemp.FindFirst("rscard = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 0)))))
                            {
                                strCards += clsTemp.Get_Fields_Int32("id").ToString() + " ";
                            }
							else
							{
								MessageBox.Show("Cannot find card " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 0))) + " for this account." + "\r\n" + "Cannot save transfer.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
					clsSave.Set_Fields("Cards",strCards.Trim());
				}
				if (cmbTransferType.Text == "Same Account")
				{
					clsSave.Set_Fields("TransferWhat",0);
                    clsSave.Set_Fields("ToAccount",0);
				}
				else if (cmbTransferType.Text == "New Account")
				{
					clsSave.Set_Fields("TransferWhat",1);
                    clsSave.Set_Fields("ToAccount",1);
				}
				else if (cmbTransferType.Text == "Existing Account")
				{
					clsSave.Set_Fields("TransferWhat",2);
					if (Conversion.Val(txtAccount.Text) > 0)
					{
						clsTemp.OpenRecordset("select rsaccount from master where not rsdeleted = 1", modGlobalVariables.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							clsSave.Set_Fields("ToAccount",Convert.ToInt32(txtAccount.Text));
						}
						else
						{
							MessageBox.Show("The account you chose to transfer to doesn't exist or is deleted." + "\r\n" + "Cannot save transfer.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						MessageBox.Show("You chose to transfer to an existing account but did not specify an account.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				clsSave.Set_Fields("MinDate",T2KTransferDate.Text);
				clsSave.Set_Fields("OwnerPartyID",Convert.ToInt32(txtOwnerID.Text));
                clsSave.Set_Fields("SecOwnerPartyID",Convert.ToInt32(txt2ndOwnerID.Text));
                clsSave.Set_Fields("maplot",txtMapLot.Text.Trim());
                clsSave.Set_Fields("StreetNumber",Convert.ToInt32(txtStreetNumber.Text));
                clsSave.Set_Fields("apt",txtApt.Text.Trim());
                clsSave.Set_Fields("streetname",txtStreet.Text.Trim());
                clsSave.Set_Fields("ref1",txtRef1.Text);
                clsSave.Set_Fields("ref2",txtRef2.Text);
                clsSave.Set_Fields("book",txtBook.Text);
                clsSave.Set_Fields("page",txtPage.Text);
				int lngSalePrice = 0;
				int intValid = 0;
				int intVerify = 0;
				int intFinance = 0;
				int intSaleType = 0;
				if (!Information.IsNumeric(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)))
				{
					lngSalePrice = 0;
				}
				else
				{
					lngSalePrice = FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)));
				}
				clsSave.Set_Fields("SalePrice",lngSalePrice);
				if (Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
				{
                    clsSave.Set_Fields("SaleDate", Strings.Format(SaleGrid.TextMatrix(2, CNSTSALECOLDATE), "MM/dd/yyyy"));					
				}
				else
				{
					clsSave.Set_Fields("SaleDate","01/01/1900");
				}

				intFinance = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
				intSaleType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
				intValid = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
				intVerify = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
				clsSave.Set_Fields("SaleType",intSaleType);
                clsSave.Set_Fields("SaleFinancing",intFinance);
                clsSave.Set_Fields("SaleVerified",intVerify);
                clsSave.Set_Fields("SaleValidity",intValid);
				if (Information.IsDate(t2kBPDate.Text))
				{
					clsSave.Set_Fields("BPDate",t2kBPDate.Text);
				}
				else
				{
					clsSave.Set_Fields("BPDate","");
				}
                clsSave.Set_Fields("DeedName1",txtDeedName1.Text.Trim());
                clsSave.Set_Fields("DeedName2",txtDeedName2.Text.Trim());
                clsSave.Update();
				
				modGlobalFunctions.AddCYAEntry_26("RE", "Created Pending Transfer", "Created a pending transfer for account " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
				MessageBox.Show("Transfer added to pending transfers.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In add transfer.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optTransfer_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			cmbTransferType.Clear();
			if (Index == 0)
			{
				// transfer whole account
				cmbTransferType.Items.Add("Same Account");
			}
			else
			{
				if (cmbTransferType.Text == "Same Account")
					cmbTransferType.Text = "New Account";
				//optTransferType[0].Enabled = false;
			}
			cmbTransferType.Items.Add("New Account");
			cmbTransferType.Items.Add("Existing Account");
			//FC:FINAL:DDU:#i1625 - fixed combobox index
			if (cmbTransferType.Text == "")
			{
				cmbTransferType.SelectedIndex = 0;
			}
		}

		private void optTransfer_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbTransfer.SelectedIndex;
			optTransfer_CheckedChanged(index, sender, e);
		}

		private void optTransferType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (cmbTransferType.Text)
			{
				case "Existing Account":
					{
						Frame3.Visible = false;
						break;
					}
				default:
					{
						Frame3.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optTransferType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbTransferType.SelectedIndex;
			optTransferType_CheckedChanged(index, sender, e);
		}
		// vbPorter upgrade warning: lngPendingID As int	OnWrite(short, string)
		public void Init(int lngPendingID, bool boolModal = false)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string[] strAry = null;
			int x;
			// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
			cParty tParty = new cParty();
			cPartyController tCont = new cPartyController();
			cPartyAddress tAddr;
			lngCurrentPendingID = lngPendingID;
			if (lngPendingID != 0)
			{
				clsLoad.OpenRecordset("select * from pending where id = " + FCConvert.ToString(lngPendingID), modGlobalVariables.strREDatabase);
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				clsDRWrapper temp = modDataTypes.Statics.MR;
				modREMain.OpenMasterTable(ref temp, clsLoad.Get_Fields("account"), 1);
				modDataTypes.Statics.MR = temp;
				txtOwnerID.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("OwnerPartyID"));
				txt2ndOwnerID.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("SecOwnerPartyID"));
				if (Conversion.Val(clsLoad.Get_Fields_Int32("ownerpartyid")) > 0)
				{
					tParty = tCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("ownerpartyid"))));
					if (!(tParty == null))
					{
						lblOwner1.Text = tParty.FullName;
						tAddr = tParty.GetPrimaryAddress();
						if (!(tAddr == null))
						{
							lblAddress.Text = tAddr.GetFormattedAddress();
						}
					}
				}
				if (Conversion.Val(clsLoad.Get_Fields_Int32("secownerpartyid")) > 0)
				{
					tParty = tCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("secownerpartyid"))));
					if (!(tParty == null))
					{
						lbl2ndOwner.Text = tParty.FullName;
					}
				}
				txtApt.Text = FCConvert.ToString(clsLoad.Get_Fields_String("apt"));
				// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
				txtBook.Text = FCConvert.ToString(clsLoad.Get_Fields("book"));
				txtMapLot.Text = FCConvert.ToString(clsLoad.Get_Fields_String("maplot"));
				// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
				txtPage.Text = FCConvert.ToString(clsLoad.Get_Fields("page"));
				txtRef1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("ref1"));
				txtRef2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("ref2"));
                txtDeedName1.Text = clsLoad.Get_Fields_String("DeedName1");
                txtDeedName2.Text = clsLoad.Get_Fields_String("DeedName2");
				string strSaleDt = "";
				strSaleDt = "";
				if (Information.IsDate(clsLoad.Get_Fields("saledate")))
				{
					if (clsLoad.Get_Fields_DateTime("saledate").ToOADate() != 0)
					{
						strSaleDt = Strings.Format(clsLoad.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
					}
				}
				FillSaleGrid(Conversion.Val(clsLoad.Get_Fields_Int32("saleprice")), strSaleDt, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("Saletype"))), FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("Saleverified"))), FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("Salevalidity"))), FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("salefinancing"))));
				txtStreet.Text = FCConvert.ToString(clsLoad.Get_Fields_String("streetname"));
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				txtStreetNumber.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetnumber")));

				T2KTransferDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("mindate"), "MM/dd/yyyy");

				if (Information.IsDate(clsLoad.Get_Fields("bpdate")))
				{
					t2kBPDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("bpdate"), "MM/dd/yyyy");
				}
				else
				{
					t2kBPDate.Text = "";
				}
				cmbTransfer.SelectedIndex = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("transfertype"));
				cmbTransferType.SelectedIndex = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("transferwhat"));
				if (cmbTransferType.Text == "Existing Account")
				{
					txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("toaccount"));
				}
				if (cmbTransfer.Text == "Specific Cards")
				{
					Grid.Rows = 1;
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("cards"));
					strAry = Strings.Split(strTemp, " ", -1, CompareConstants.vbTextCompare);
					for (x = 0; x <= Information.UBound(strAry, 1); x++)
					{
						if (Conversion.Val(strAry[x]) > 0)
						{
							clsTemp.OpenRecordset("select rscard from master where id = " + strAry[x], modGlobalVariables.strREDatabase);
							if (!clsTemp.EndOfFile())
							{
								Grid.AddItem(FCConvert.ToString(clsTemp.Get_Fields_Int32("rscard")));
							}
						}
					}
					// x
				}
				else
				{
					Grid.Rows = 2;
				}
			}
			else
			{
				FillSaleGrid(0, "", 0, 0, 0, 0);
			}
			if (!boolModal)
			{
				this.Show(App.MainForm);
			}
			else
			{
				this.Show(FCForm.FormShowEnum.Modal);
			}
		}

		private void ResizeSaleGrid()
		{
			int GridWidth = 0;
			try
			{
				// On Error GoTo ErrorHandler
				GridWidth = SaleGrid.WidthOriginal;
				// .ColWidth(0) = 0.3 * GridWidth
				// .Height = .RowHeight(0) * 7 + 50
				SaleGrid.ColWidth(CNSTSALECOLPRICE, FCConvert.ToInt32(0.1 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLDATE, FCConvert.ToInt32(0.1 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLSALE, FCConvert.ToInt32(0.2 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLFINANCING, FCConvert.ToInt32(0.2 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLVERIFIED, FCConvert.ToInt32(0.2 * GridWidth));
				SaleGrid.Height = 300;
				//SaleGrid.RowHeight(0) * 3 + 50;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeSaleGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intSaleType As short --> As int	OnWrite(double, short)
		// vbPorter upgrade warning: intSaleVerified As short --> As int	OnWrite(double, short)
		// vbPorter upgrade warning: intSaleValidity As short --> As int	OnWrite(double, short)
		// vbPorter upgrade warning: intFinancing As short --> As int	OnWrite(double, short)
		private void FillSaleGrid(double dblSalePrice, string strSaleDate, int intSaleType, int intSaleVerified, int intSaleValidity, int intFinancing)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strList = "";
			string strData = "";
			try
			{
				// On Error GoTo ErrorHandler
				SetupSaleGrid();
				SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(dblSalePrice, "#,###,###,##0"));
				SaleGrid.TextMatrix(2, CNSTSALECOLDATE, strSaleDate);
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1351 and 1359 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350) + ";" + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLSALE, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLSALE, FCConvert.ToString(intSaleType));
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1361 and 1369 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360) + ";" + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLFINANCING, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING, FCConvert.ToString(intFinancing));
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1381 and 1389 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380) + ";" + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLVALIDITY, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY, FCConvert.ToString(intSaleValidity));
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1371 and 1379 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370) + ";" + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLVERIFIED, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED, FCConvert.ToString(intSaleVerified));
				/*? 36 */// If Not boolShortRealEstate Then
				SaleGrid.Row = 2;
				SaleGrid.Col = 0;
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillSaleGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupSaleGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				SaleGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeRestrictColumns;
				SaleGrid.MergeRow(0, true);
				SaleGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				SaleGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0, 0, 5, "Sale Data");
				SaleGrid.TextMatrix(1, CNSTSALECOLPRICE, "Price");
				SaleGrid.TextMatrix(1, CNSTSALECOLDATE, "Date");
				SaleGrid.TextMatrix(1, CNSTSALECOLSALE, "Type");
				SaleGrid.TextMatrix(1, CNSTSALECOLFINANCING, "Financing");
				SaleGrid.TextMatrix(1, CNSTSALECOLVERIFIED, "Verified");
				SaleGrid.TextMatrix(1, CNSTSALECOLVALIDITY, "Validity");
                //FC:FINAL:BSE #3262 column header should be aligned right 
                SaleGrid.ColAlignment(CNSTSALECOLPRICE, FCGrid.AlignmentSettings.flexAlignRightCenter);
				SaleGrid.ColEditMask(CNSTSALECOLDATE, "##/##/####");
				SaleGrid.Row = 2;
				SaleGrid.Col = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupSaleGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdRemoveOwner_Click(object sender, System.EventArgs e)
		{
			ShowOwner(0);
		}

		private void cmdRemoveSecOwner_Click(object sender, System.EventArgs e)
		{
			ShowSecOwner(0);
		}

		private void ShowOwner(int lngOwnerID)
		{
			if (lngOwnerID > 0)
			{
				cPartyController tCont = new cPartyController();
				// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				tParty = tCont.GetParty(lngOwnerID);
				if (!(tParty == null))
				{
					lblOwner1.Text = tParty.FullName;
                    if (String.IsNullOrWhiteSpace(txtDeedName1.Text))
                    {
                        txtDeedName1.Text = tParty.FullNameLastFirst;
                    }
					cPartyAddress tAdd;
					tAdd = tParty.GetPrimaryAddress();
					if (!(tAdd == null))
					{
						lblAddress.Text = tAdd.GetFormattedAddress();
					}
					else
					{
						lblAddress.Text = "";
					}
				}
				else
				{
					lblOwner1.Text = "";
					lblAddress.Text = "";
				}
				txtOwnerID.Text = FCConvert.ToString(lngOwnerID);
			}
			else
			{
				lblOwner1.Text = "";
				txtOwnerID.Text = FCConvert.ToString(0);
				lblAddress.Text = "";
			}
		}

		private void ShowSecOwner(int lngOwnerID)
		{
			if (lngOwnerID > 0)
			{
				cPartyController tCont = new cPartyController();
				// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				tParty = tCont.GetParty(lngOwnerID);
				if (!(tParty == null))
				{
					lbl2ndOwner.Text = tParty.FullName;
                    if (String.IsNullOrWhiteSpace(txtDeedName2.Text))
                    {
                        txtDeedName2.Text = tParty.FullNameLastFirst;
                    }
				}
				else
				{
					lbl2ndOwner.Text = "";
				}
				txt2ndOwnerID.Text = FCConvert.ToString(lngOwnerID);
			}
			else
			{
				lbl2ndOwner.Text = "";
				txt2ndOwnerID.Text = FCConvert.ToString(0);
			}
		}
    }
}
