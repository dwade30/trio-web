﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rpt1PagePropertyCard.
	/// </summary>
	partial class rpt1PagePropertyCard
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1PagePropertyCard));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Image3 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccountName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblExempt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblExempt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBldgValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLandValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExempt1Value = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExempt2Value = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExempt3Value = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblExempt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddr3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAcreage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtAddr4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldgValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLandValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt1Value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt2Value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt3Value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcreage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape3,
            this.Image1,
            this.Image2,
            this.Image3,
            this.Label1,
            this.txtAccount,
            this.txtAccountName,
            this.Shape1,
            this.Line1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.lblExempt1,
            this.lblExempt2,
            this.txtBldgValue,
            this.txtLandValue,
            this.txtExempt1Value,
            this.txtExempt2Value,
            this.txtExempt3Value,
            this.lblExempt3,
            this.txtAssessment,
            this.Label8,
            this.txtTax,
            this.Label11,
            this.txtMapLot,
            this.Label12,
            this.txtLocation,
            this.Label13,
            this.txtAddr1,
            this.txtAddr2,
            this.txtAddr3,
            this.Label14,
            this.Label15,
            this.txtBook,
            this.txtPage,
            this.Label16,
            this.Label17,
            this.SubReport1,
            this.Label18,
            this.txtAcreage,
            this.SubReport2,
            this.Label19,
            this.txtDate,
            this.txtAddr4});
            this.Detail.Height = 7.25F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Shape3
            // 
            this.Shape3.Height = 1.197917F;
            this.Shape3.Left = 6.708333F;
            this.Shape3.LineWeight = 2F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.RoundRect;
            this.Shape3.Top = 2.479167F;
            this.Shape3.Width = 2.989583F;
            // 
            // Image1
            // 
            this.Image1.Height = 2.208333F;
            this.Image1.HyperLink = null;
            this.Image1.ImageData = null;
            this.Image1.Left = 0.03125F;
            this.Image1.LineWeight = 1F;
            this.Image1.Name = "Image1";
            this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Image1.Top = 0.2291667F;
            this.Image1.Width = 3.25F;
            // 
            // Image2
            // 
            this.Image2.Height = 2.208333F;
            this.Image2.HyperLink = null;
            this.Image2.ImageData = null;
            this.Image2.Left = 3.364583F;
            this.Image2.LineWeight = 1F;
            this.Image2.Name = "Image2";
            this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Image2.Top = 0.2291667F;
            this.Image2.Width = 3.25F;
            // 
            // Image3
            // 
            this.Image3.Height = 2.208333F;
            this.Image3.HyperLink = null;
            this.Image3.ImageData = null;
            this.Image3.Left = 6.697917F;
            this.Image3.LineWeight = 1F;
            this.Image3.Name = "Image3";
            this.Image3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Image3.Top = 0.2291667F;
            this.Image3.Width = 3.25F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.09375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "Account:";
            this.Label1.Top = 0F;
            this.Label1.Width = 0.7083333F;
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1875F;
            this.txtAccount.Left = 0.875F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 0F;
            this.txtAccount.Width = 0.625F;
            // 
            // txtAccountName
            // 
            this.txtAccountName.Height = 0.1875F;
            this.txtAccountName.Left = 1.625F;
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.Text = null;
            this.txtAccountName.Top = 0F;
            this.txtAccountName.Width = 6.270833F;
            // 
            // Shape1
            // 
            this.Shape1.Height = 0.3958333F;
            this.Shape1.Left = 0.03125F;
            this.Shape1.LineWeight = 2F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.RoundRect;
            this.Shape1.Top = 3.34375F;
            this.Shape1.Width = 2.416667F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.02083333F;
            this.Line1.LineWeight = 3F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 3.8125F;
            this.Line1.Width = 9.864584F;
            this.Line1.X1 = 0.02083333F;
            this.Line1.X2 = 9.885417F;
            this.Line1.Y1 = 3.8125F;
            this.Line1.Y2 = 3.8125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.19F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.09375F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 9pt; font-weight: bold; text-align: right";
            this.Label2.Text = "Total Assessment:";
            this.Label2.Top = 3.375F;
            this.Label2.Width = 1.3125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.19F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.0625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 9pt";
            this.Label3.Text = "Building Value:";
            this.Label3.Top = 2.489583F;
            this.Label3.Width = 1.020833F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.19F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.07291666F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 9pt";
            this.Label4.Text = "Land Value:";
            this.Label4.Top = 2.65625F;
            this.Label4.Width = 1.020833F;
            // 
            // lblExempt1
            // 
            this.lblExempt1.Height = 0.19F;
            this.lblExempt1.HyperLink = null;
            this.lblExempt1.Left = 0.07291666F;
            this.lblExempt1.Name = "lblExempt1";
            this.lblExempt1.Style = "font-size: 9pt";
            this.lblExempt1.Text = "Veteran Exempt:";
            this.lblExempt1.Top = 2.822917F;
            this.lblExempt1.Width = 1.395833F;
            // 
            // lblExempt2
            // 
            this.lblExempt2.Height = 0.19F;
            this.lblExempt2.HyperLink = null;
            this.lblExempt2.Left = 0.07291666F;
            this.lblExempt2.Name = "lblExempt2";
            this.lblExempt2.Style = "font-size: 9pt";
            this.lblExempt2.Text = "Homestead Exempt:";
            this.lblExempt2.Top = 2.989583F;
            this.lblExempt2.Width = 1.395833F;
            // 
            // txtBldgValue
            // 
            this.txtBldgValue.Height = 0.19F;
            this.txtBldgValue.Left = 1.447917F;
            this.txtBldgValue.Name = "txtBldgValue";
            this.txtBldgValue.Style = "font-size: 9pt; text-align: right";
            this.txtBldgValue.Text = "0";
            this.txtBldgValue.Top = 2.5F;
            this.txtBldgValue.Width = 0.9583333F;
            // 
            // txtLandValue
            // 
            this.txtLandValue.Height = 0.19F;
            this.txtLandValue.Left = 1.447917F;
            this.txtLandValue.Name = "txtLandValue";
            this.txtLandValue.Style = "font-size: 9pt; text-align: right";
            this.txtLandValue.Text = "0";
            this.txtLandValue.Top = 2.65625F;
            this.txtLandValue.Width = 0.9583333F;
            // 
            // txtExempt1Value
            // 
            this.txtExempt1Value.Height = 0.19F;
            this.txtExempt1Value.Left = 1.447917F;
            this.txtExempt1Value.Name = "txtExempt1Value";
            this.txtExempt1Value.Style = "font-size: 9pt; text-align: right";
            this.txtExempt1Value.Text = "0";
            this.txtExempt1Value.Top = 2.822917F;
            this.txtExempt1Value.Width = 0.9583333F;
            // 
            // txtExempt2Value
            // 
            this.txtExempt2Value.Height = 0.19F;
            this.txtExempt2Value.Left = 1.447917F;
            this.txtExempt2Value.Name = "txtExempt2Value";
            this.txtExempt2Value.Style = "font-size: 9pt; text-align: right";
            this.txtExempt2Value.Text = "0";
            this.txtExempt2Value.Top = 2.989583F;
            this.txtExempt2Value.Width = 0.9583333F;
            // 
            // txtExempt3Value
            // 
            this.txtExempt3Value.Height = 0.19F;
            this.txtExempt3Value.Left = 1.447917F;
            this.txtExempt3Value.Name = "txtExempt3Value";
            this.txtExempt3Value.Style = "font-size: 9pt; text-align: right";
            this.txtExempt3Value.Text = "0";
            this.txtExempt3Value.Top = 3.15625F;
            this.txtExempt3Value.Width = 0.9583333F;
            // 
            // lblExempt3
            // 
            this.lblExempt3.Height = 0.19F;
            this.lblExempt3.HyperLink = null;
            this.lblExempt3.Left = 0.07291666F;
            this.lblExempt3.Name = "lblExempt3";
            this.lblExempt3.Style = "font-size: 9pt";
            this.lblExempt3.Text = "Exempt 3:";
            this.lblExempt3.Top = 3.15625F;
            this.lblExempt3.Width = 1.395833F;
            // 
            // txtAssessment
            // 
            this.txtAssessment.Height = 0.19F;
            this.txtAssessment.Left = 1.447917F;
            this.txtAssessment.Name = "txtAssessment";
            this.txtAssessment.Style = "font-size: 9pt; text-align: right";
            this.txtAssessment.Text = "0";
            this.txtAssessment.Top = 3.375F;
            this.txtAssessment.Width = 0.9583333F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.19F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.09375F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 9pt; font-weight: bold; text-align: right";
            this.Label8.Text = "Tax:";
            this.Label8.Top = 3.541667F;
            this.Label8.Width = 1.3125F;
            // 
            // txtTax
            // 
            this.txtTax.Height = 0.19F;
            this.txtTax.Left = 1.447917F;
            this.txtTax.Name = "txtTax";
            this.txtTax.Style = "font-size: 9pt; text-align: right";
            this.txtTax.Text = "0";
            this.txtTax.Top = 3.541667F;
            this.txtTax.Width = 0.9583333F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.19F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 8.052083F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 9pt; font-weight: bold";
            this.Label11.Text = "Map / Lot";
            this.Label11.Top = 2.5625F;
            this.Label11.Width = 1.052083F;
            // 
            // txtMapLot
            // 
            this.txtMapLot.Height = 0.7708333F;
            this.txtMapLot.Left = 8.052083F;
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Style = "font-size: 9pt; text-align: left";
            this.txtMapLot.Text = null;
            this.txtMapLot.Top = 2.791667F;
            this.txtMapLot.Width = 1.572917F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.19F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 2.677083F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 9pt";
            this.Label12.Text = "Location:";
            this.Label12.Top = 2.5F;
            this.Label12.Width = 0.6875F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.19F;
            this.txtLocation.Left = 3.427083F;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Style = "font-size: 9pt; text-align: left";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 2.5F;
            this.txtLocation.Width = 2.833333F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.19F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 2.677083F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 9pt";
            this.Label13.Text = "Address:";
            this.Label13.Top = 2.65625F;
            this.Label13.Width = 0.6875F;
            // 
            // txtAddr1
            // 
            this.txtAddr1.Height = 0.19F;
            this.txtAddr1.Left = 3.427083F;
            this.txtAddr1.Name = "txtAddr1";
            this.txtAddr1.Style = "font-size: 9pt; text-align: left";
            this.txtAddr1.Text = null;
            this.txtAddr1.Top = 2.65625F;
            this.txtAddr1.Width = 2.833333F;
            // 
            // txtAddr2
            // 
            this.txtAddr2.Height = 0.19F;
            this.txtAddr2.Left = 3.427083F;
            this.txtAddr2.Name = "txtAddr2";
            this.txtAddr2.Style = "font-size: 9pt; text-align: left";
            this.txtAddr2.Text = null;
            this.txtAddr2.Top = 2.822917F;
            this.txtAddr2.Width = 2.833333F;
            // 
            // txtAddr3
            // 
            this.txtAddr3.Height = 0.19F;
            this.txtAddr3.Left = 3.427083F;
            this.txtAddr3.Name = "txtAddr3";
            this.txtAddr3.Style = "font-size: 9pt; text-align: left";
            this.txtAddr3.Text = null;
            this.txtAddr3.Top = 2.989583F;
            this.txtAddr3.Width = 2.833333F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.19F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.8125F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 9pt; font-weight: bold";
            this.Label14.Text = "Book";
            this.Label14.Top = 2.5625F;
            this.Label14.Width = 0.3645833F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.19F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 7.4375F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 9pt; font-weight: bold";
            this.Label15.Text = "Page";
            this.Label15.Top = 2.5625F;
            this.Label15.Width = 0.3645833F;
            // 
            // txtBook
            // 
            this.txtBook.Height = 0.7916667F;
            this.txtBook.Left = 6.8125F;
            this.txtBook.Name = "txtBook";
            this.txtBook.Style = "font-size: 9pt; text-align: left";
            this.txtBook.Text = null;
            this.txtBook.Top = 2.791667F;
            this.txtBook.Width = 0.5104167F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.7916667F;
            this.txtPage.Left = 7.4375F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-size: 9pt; text-align: left";
            this.txtPage.Text = null;
            this.txtPage.Top = 2.791667F;
            this.txtPage.Width = 0.5104167F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold";
            this.Label16.Text = "Land Detail:";
            this.Label16.Top = 3.885417F;
            this.Label16.Width = 1.229167F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 3.791667F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-weight: bold";
            this.Label17.Text = "Building Detail:";
            this.Label17.Top = 3.885417F;
            this.Label17.Width = 1.229167F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.07291666F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 4.0625F;
            this.SubReport1.Width = 3.677083F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.19F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 2.656F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 9pt";
            this.Label18.Text = "Total Acres:";
            this.Label18.Top = 3.51F;
            this.Label18.Width = 0.75F;
            // 
            // txtAcreage
            // 
            this.txtAcreage.Height = 0.19F;
            this.txtAcreage.Left = 3.426833F;
            this.txtAcreage.Name = "txtAcreage";
            this.txtAcreage.Style = "font-size: 9pt; text-align: right";
            this.txtAcreage.Text = "0";
            this.txtAcreage.Top = 3.51F;
            this.txtAcreage.Width = 0.6875F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.07291666F;
            this.SubReport2.Left = 3.791667F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 4.0625F;
            this.SubReport2.Width = 6.114583F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 8F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-weight: bold; text-align: right";
            this.Label19.Text = "Printed:";
            this.Label19.Top = 0F;
            this.Label19.Width = 0.7083333F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 8.75F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.125F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtAddr4
            // 
            this.txtAddr4.Height = 0.19F;
            this.txtAddr4.Left = 3.427F;
            this.txtAddr4.Name = "txtAddr4";
            this.txtAddr4.Style = "font-size: 9pt; text-align: left";
            this.txtAddr4.Text = null;
            this.txtAddr4.Top = 3.156F;
            this.txtAddr4.Width = 2.833333F;
            // 
            // rpt1PagePropertyCard
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.96875F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldgValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLandValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt1Value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt2Value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExempt3Value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcreage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountName;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1Value;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2Value;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt3Value;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreage;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr4;
    }
}
