﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using GrapeCity.ActiveReports.Document.Section;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
                clsPrint = null;
            }
			base.Dispose(disposing);
		}

		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND frmCustomLabels.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		int intBlankLabelsLeftToPrint;
		clsPrintLabel labLabels = new clsPrintLabel();
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		float lngPrintWidth;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intLabelStartPosition As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strSQL, ref string strReportType, int intLabelType, ref string strPrinterName, ref string strFonttoUse, int intLabelStartPosition = 1)
		{
			string strDBName = "";
			int intReturn;
			int x;
			bool boolUseFont;
			intBlankLabelsLeftToPrint = intLabelStartPosition - 1;
			strFont = strFonttoUse;
			this.Document.Printer.PrinterName = strPrinterName;
			boolPP = false;
			boolMort = false;
			if (strReportType == "MORTGAGEHOLDER")
			{
				strDBName = "CentralData";
				boolMort = true;
			}
			else if (strReportType == "REALESTATE")
			{
				strDBName = "RealEstate";
			}
			else if (strReportType == "PERSONALPROPERTY")
			{
				strDBName = "PersonalProperty";
				boolPP = true;
			}
			rsData.OpenRecordset(strSQL, strDBName);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;
			int intIndex;
			intIndex = labLabels.Get_IndexFromID(intLabelType);
			if (labLabels.Get_IsDymoLabel(intIndex))
			{
				switch (labLabels.Get_ID(intIndex))
				{
					case modLabels.CNSTLBLTYPEDYMO30256:
					{
						strPrinterName = this.Document.Printer.PrinterName;
						this.Document.Printer.DefaultPageSettings.Landscape = true;
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						this.PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
						this.PageSettings.Margins.Bottom = 0;
						this.PageSettings.Margins.Right = 0;
						this.PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
						PrintWidth = 2.31F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
						intLabelWidth = 4F;
						lngPrintWidth = 4F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
						Detail.Height = 2.3125F - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = 4F;
						PageSettings.PaperWidth = FCConvert.ToSingle(2.31);
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						this.Document.Printer.DefaultPageSettings.Landscape = true;
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						break;
					}
					case modLabels.CNSTLBLTYPEDYMO30252:
					{
						strPrinterName = this.Document.Printer.PrinterName;
						this.Document.Printer.DefaultPageSettings.Landscape = true;
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						this.PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
						this.PageSettings.Margins.Bottom = 0;
						this.PageSettings.Margins.Right = 0;
						this.PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
						PrintWidth = 3.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
						intLabelWidth = 3.5F;
						lngPrintWidth = 3.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
						Detail.Height = 1.1F - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = FCConvert.ToSingle(3.5);
						PageSettings.PaperWidth = FCConvert.ToSingle(1.1);
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						this.Document.Printer.DefaultPageSettings.Landscape = true;
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						break;
					}
				}
			}
			else if (labLabels.Get_IsLaserLabel(intIndex))
			{
				this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
				this.PageSettings.Margins.Top += FCConvert.ToSingle(modGlobalVariables.Statics.gdblLabelsAdjustment * 270 / 1440f);
				this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intIndex);
				this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intIndex);
				PrintWidth = (labLabels.Get_PageWidth(intIndex) - labLabels.Get_LeftMargin(intIndex) - labLabels.Get_RightMargin(intIndex));
				intLabelWidth = labLabels.Get_LabelWidth(intIndex);
				Detail.Height = labLabels.Get_LabelHeight(intIndex) + labLabels.Get_VerticalSpace(intIndex);
				if (labLabels.Get_LabelsWide(intIndex) > 0)
				{
					Detail.ColumnCount = labLabels.Get_LabelsWide(intIndex);
					Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intIndex);
				}
				lngPrintWidth = PrintWidth;
			}
			
			if (boolDifferentPageSize)
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			CreateDataFields();
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 4;
			if (boolMort)
				intNumber = 5;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				NewField.CanGrow = false;
				NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = (intRow - 1) * 225 / 1440f;
				NewField.Left = 144 / 1440f;
				// one space
				NewField.Width = (PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing) - 145 / 1440f;
				NewField.Height = 225 / 1440f;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
                NewField.WrapMode = WrapMode.WordWrap;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				Detail.Controls.Add(NewField);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			string vbPorterVar = ""/*Tool.Caption*/;
			if (vbPorterVar == "Print...")
			{
				clsPrint.ReportPrint(this);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			if (intBlankLabelsLeftToPrint <= 0)
			{
				str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("name")));
				str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address1")));
				str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address2")));
				// If Not boolMort Then
				// str4 = Trim(rsData.Fields("City") & "  " & rsData.Fields("state"))
				// End If
				// If boolPP Then
				// str4 = str4 & " " & Trim(rsData.Fields("zip"))
				// If Trim(rsData.Fields("zip4")) <> vbNullString Then
				// str4 = str4 & "-" & Trim(rsData.Fields("zip4"))
				// End If
				// ElseIf boolMort Then
				str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address3")));
				str5 = Strings.Trim(rsData.Get_Fields_String("City") + "  " + rsData.Get_Fields_String("state"));
				str5 += " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip")));
				// If Trim(rsData.Fields("zip4")) <> vbNullString Then
				// str5 = str5 & "-" & Trim(rsData.Fields("zip4"))
				// End If
				// Else
				// str4 = str4 & " " & Trim(rsData.Fields("zip"))
				// If Trim(rsData.Fields("zip4")) <> vbNullString Then
				// str4 = str4 & "-" & Trim(rsData.Fields("zip4"))
				// End If
				// End If
				// condense
				// If boolMort Then
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				// End If
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str1;
									break;
								}
							case 2:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str2;
									break;
								}
							case 3:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str3;
									break;
								}
							case 4:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str4;
									break;
								}
							case 5:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str5;
									break;
								}
						}
						//end switch
					}
				}
				// intControl
				rsData.MoveNext();
			}
			else
			{
				intBlankLabelsLeftToPrint -= 1;
			}
		}

		
	}
}
