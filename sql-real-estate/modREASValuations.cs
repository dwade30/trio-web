﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWRE0000
{
	public class modREASValuations
	{
		public static void new_physicalpercentroutine()
		{
			Statics.MRRSDwellingCode = modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "";
			//FC:FINAL:MSH - i.issue #1191: unboxing object will throw an exception if Life == null. FCConvert.ToInt32 will return 0 in this case
			// without exceptions
			//if (FCConvert.ToInt32(Life > 1000 && MRRSDwellingCode == "C")
			if (FCConvert.ToInt32(Statics.Life) > 1000 && Statics.MRRSDwellingCode == "C")
			{
				Statics.Life = 50;
			}
			// WHigh = (CRCUnit& / 1000000) \ 1
			// WHigh = (CostRec.CUnit / 1000000) \ 1
			modDwelling.Statics.WHigh = FCConvert.ToDouble(Strings.Format((FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 1000000), "0.00"));
			// WLow = (CRCUnit& - (WHigh * 1000000))
			// WLow = (CostRec.CUnit - (WHigh * 1000000))
			modDwelling.Statics.WLow = Conversion.Val(Strings.Right(modGlobalVariables.Statics.CostRec.CUnit, 3));
			if (Statics.MRRSDwellingCode == "C" && modDwelling.Statics.strDwellingCode != "O")
			{
				modDwelling.Statics.WHigh = fecherFoundation.FCUtils.iDiv((modGlobalVariables.Statics.CRCUnit / 1000000.0), 1);
				modDwelling.Statics.WLow = (modGlobalVariables.Statics.CRCUnit - (modDwelling.Statics.WHigh * 1000000));
				Statics.WLife = Statics.Life;
				Statics.WExp = modGlobalVariables.Statics.CRCBase / 100.0;
			}
			else
			{
				// WLife = (CRCBase& \ 100000)
				Statics.WLife = (fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase), 100000));
				// WExp = (CRCBase& - (WLife * 100000)) / 100
				//FC:FINAL:MSH - i.issue #1191: invalid cast exception
				//WExp = (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) - (FCConvert.ToDouble(WLife * 100000)) / 100.0;
				Statics.WExp = (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) - (FCConvert.ToDouble(Statics.WLife) * 100000)) / 100.0;
			}
			Statics.cy = Conversion.Val(Strings.Mid(DateTime.Today.ToShortDateString(), 7, 4));
			if (modGlobalVariables.Statics.DepreciationYear != 0 && Statics.MRRSDwellingCode != "C")
			{
				Statics.cy = modGlobalVariables.Statics.DepreciationYear;
			}
			else if (modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear != 0 && Statics.MRRSDwellingCode == "C")
			{
				if (modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear >= 0)
				{
					Statics.cy = modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear;
				}
				else
				{
				}
			}
			if (Statics.cy < Conversion.Val(Statics.HOLDYEAR))
				Statics.cy = Conversion.Val(Statics.HOLDYEAR);
			//FC:FINAL:MSH - i.issue #1191: invalid cast exception
			//if (cy - Conversion.Val(HOLDYEAR) > FCConvert.ToDouble(WLife) cy = Conversion.Val(HOLDYEAR) + FCConvert.ToDouble(WLife;
			if (Statics.cy - Conversion.Val(Statics.HOLDYEAR) > FCConvert.ToDouble(Statics.WLife))
				Statics.cy = Conversion.Val(Statics.HOLDYEAR) + FCConvert.ToDouble(Statics.WLife);
			if (FCConvert.ToInt32(Statics.WLife) != 0)
			{
				//FC:FINAL:MSH - i.issue #1191: invalid cast exception
				//PhysicalPercent = FCConvert.ToSingle(modDwelling.WHigh - ((modDwelling.WHigh - modDwelling.WLow) * (Math.Pow(((cy - Conversion.Val(HOLDYEAR)) / FCConvert.ToDouble(WLife), WExp))));
				Statics.PhysicalPercent = FCConvert.ToSingle(modDwelling.Statics.WHigh - ((modDwelling.Statics.WHigh - modDwelling.Statics.WLow) * (Math.Pow(((Statics.cy - Conversion.Val(Statics.HOLDYEAR)) / FCConvert.ToDouble(Statics.WLife)), Statics.WExp))));
			}
			else
			{
				Statics.PhysicalPercent = 100;
				// MsgBox "Physical Percent = 1.00, because Life = 0."
			}
			if (Statics.PhysicalPercent < modDwelling.Statics.WLow)
				Statics.PhysicalPercent = FCConvert.ToSingle(modDwelling.Statics.WLow);
			// PhysicalPercent = Int(0.5 + PhysicalPercent)
			if (Statics.MRRSDwellingCode != "O")
			{
				Statics.PhysicalPercent /= 100;
			}
			// PhysicalPercent = (Int(0.5 + (PhysicalPercent * 100))) / 100
			// corey
			// dblPhysicalPercent = 0.5 + (PhysicalPercent * 100)
			modDwelling.Statics.dblPhysicalPercent = FCConvert.ToSingle(Statics.PhysicalPercent * 100);
			modDwelling.Statics.dblPhysicalPercent = modGlobalRoutines.Round(modDwelling.Statics.dblPhysicalPercent, 0) / 100;
			// dblPhysicalPercent = (Int(0.5 + (PhysicalPercent * 100))) / 100
			Statics.PhysicalPercent = FCConvert.ToSingle(modDwelling.Statics.dblPhysicalPercent);
		}

		public static void LandCalculation()
		{
			double widthExp1 = 0;
			double widthExp2 = 0;
			if (Statics.WorkA == "000000" && FCConvert.ToInt32(modProperty.Statics.WORK1) != 89)
				goto REAS25_END_SUB4;
			if (Statics.Schedule == 0)
			{
				Statics.Lexponent = 0.5;
				Statics.Lexponent2 = 0.5;
				widthExp1 = 0.5;
				widthExp2 = 0.5;
			}
			else
			{
				modProperty.FillExponentArrays(ref Statics.Schedule);
				if (modProperty.Statics.WORK1 != 89)
				{
					Statics.Lexponent = modProperty.Statics.LEXP[FCConvert.ToInt16(modProperty.Statics.WORK1) + 10];
					Statics.Lexponent2 = modProperty.Statics.LEX2[FCConvert.ToInt16(modProperty.Statics.WORK1) + 10];
					if (modProperty.Statics.WidthExponentsOne.ContainsKey((FCConvert.ToInt16(modProperty.Statics.WORK1) + 10)))
					{
						widthExp1 = Conversion.Val(modProperty.Statics.WidthExponentsOne[FCConvert.ToInt32(FCConvert.ToInt16(modProperty.Statics.WORK1) + 10)]);
					}
					else
					{
						widthExp1 = 0.5;
					}
					if (modProperty.Statics.WidthExponentsTwo.ContainsKey((FCConvert.ToInt16(modProperty.Statics.WORK1) + 10)))
					{
						widthExp2 = Conversion.Val(modProperty.Statics.WidthExponentsTwo[FCConvert.ToInt32(FCConvert.ToInt16(modProperty.Statics.WORK1) + 10)]);
					}
					else
					{
						widthExp2 = 0.5;
					}
				}
			}
			if (modProperty.Statics.WORK1 == 89)
			{
				REAS25_3750_EXTRA_INFLUENCE();
			}
			else
			{
				if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(modProperty.Statics.WORK1 + 10)))
				{
					switch (modGlobalVariables.Statics.LandTypes.UnitsType)
					{
						case modREConstants.CNSTLANDTYPEFRONTFOOT:
							{
								REAS25_3650_FRONT_FOOT();
								break;
							}
						case modREConstants.CNSTLANDTYPESQUAREFEET:
							{
								REAS25_3675_SQUARE_FOOT();
								break;
							}
						case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
							{
								REAS25_3700_FRACTIONAL_ACREAGE();
								break;
							}
						case modREConstants.CNSTLANDTYPEACRES:
						case modREConstants.CNSTLANDTYPESITE:
						case modREConstants.CNSTLANDTYPELINEARFEET:
						case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
							{
								REAS25_3725_ACREAGE_AND_SITE();
								break;
							}
						case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
							{
								// not currently used this way
								REAS25_3750_EXTRA_INFLUENCE();
								break;
							}
						case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
							{
								CalculateFrontFootAdjusted(ref Statics.Lexponent, ref Statics.Lexponent2, ref widthExp1, ref widthExp2);
								break;
							}
						case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
							{
								CalculateLinearFeetAdjusted();
								break;
							}
					}
					//end switch
				}
				else
				{
					MessageBox.Show("Invalid Land Type " + FCConvert.ToString(modProperty.Statics.WORK1 + 10) + " on Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + ".", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (modProperty.Statics.WORK1 != 89)
			{
				if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(modProperty.Statics.WORK1 + 10)))
				{
					if (!(modGlobalVariables.Statics.CustomizedInfo.boolDontApplyLandFactorToTreeGrowth && modGlobalVariables.Statics.LandTypes.IsTreeGrowth()))
					{
						Statics.Price[Statics.Row] *= FCConvert.ToSingle(modProperty.Statics.LandTrend);
						Statics.Price[Statics.Row] *= FCConvert.ToSingle(modProperty.Statics.LandFactor);
					}
					if (modGlobalVariables.Statics.LandTypes.IsTreeGrowth() && modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth)
					{
						Statics.Price[Statics.Row] *= FCConvert.ToSingle(modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio);
					}
					if (!(modGlobalVariables.Statics.LandTypes.IsTreeGrowth() && modGlobalVariables.Statics.CustomizedInfo.boolDontApplyLandFactorToTreeGrowth))
					{
						Statics.UnitPrice[Statics.Row] = Statics.StreetPrice * modProperty.Statics.LandFactor;
					}
					else
					{
						Statics.UnitPrice[Statics.Row] = Statics.StreetPrice;
					}
				}
				else
				{
					Statics.Price[Statics.Row] *= FCConvert.ToSingle(modProperty.Statics.LandTrend);
					Statics.Price[Statics.Row] *= FCConvert.ToSingle(modProperty.Statics.LandFactor);
					if (Strings.Trim(Statics.LandUnitType) != "C")
					{
						Statics.UnitPrice[Statics.Row] = Statics.StreetPrice * modProperty.Statics.LandFactor;
					}
				}
				Statics.Value[Statics.Row] = Statics.Price[Statics.Row] * Statics.InfluenceFactor;
				if (Strings.Trim(Statics.LandUnitType) == "C")
				{
					if (Statics.WUnits != 0)
					{
						Statics.UnitPrice[Statics.Row] = Statics.Price[Statics.Row] / Statics.WUnits;
					}
					else
					{
						Statics.UnitPrice[Statics.Row] = 0;
						modSpeedCalc.Statics.boolCalcErrors = true;
						modSpeedCalc.Statics.CalcLog += "Unit Price = 0, because Wunits = 0. Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n";
						// Print "UNIT PRICE = 0, BECAUSE WUNITS = 0 "; Work1
					}
				}
			}
			Statics.Value[Statics.Row] = FCConvert.ToInt32(Conversion.Int(Statics.Value[Statics.Row] + 0.5));
			modProperty.Statics.LandTotal[modGNBas.Statics.gintCardNumber] += Statics.Value[Statics.Row];
			// corey. jim wants this figure rounded
			// LandTotal(gintCardNumber) = ((LandTotal(gintCardNumber) / RoundingVar) \ 1) * RoundingVar
			REAS25_END_SUB4:
			;
		}

		public static void REAS25_3650_FRONT_FOOT()
		{
			Statics.WUnits = FCConvert.ToSingle(Conversion.Val(Strings.Mid(Statics.WorkA, 1, 3)));
			Statics.WDepth = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.WorkA, 4, 3))));
			if (Statics.StandardDepth != 0)
			{
				if (Statics.StandardDepth != 0)
				{
					if (Statics.WDepth > Statics.StandardDepth)
					{
						Statics.DepthFactor = FCConvert.ToSingle(Math.Pow((FCConvert.ToDouble(Statics.WDepth) / Statics.StandardDepth), Statics.Lexponent2));
					}
					else
					{
						Statics.DepthFactor = FCConvert.ToSingle(Math.Pow((FCConvert.ToDouble(Statics.WDepth) / Statics.StandardDepth), Statics.Lexponent));
					}
				}
				else
				{
					Statics.DepthFactor = 100;
					MessageBox.Show("Depth Factor = 0, because Standard Depth = 0", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// Print "DEPTH FACTOR = 0, BECAUSE StandardDepth = 0 "; Work1
				}
			}
			else
			{
				Statics.DepthFactor = 0;
			}
			Statics.Price[Statics.Row] = FCConvert.ToSingle(Statics.WUnits * Statics.StreetPrice * Statics.DepthFactor);
		}

		public static void CalculateFrontFootAdjusted(ref double depthExp1, ref double depthExp2, ref double widthExp1, ref double widthExp2)
		{
			Statics.WUnits = FCConvert.ToSingle(Conversion.Val(Strings.Mid(Statics.WorkA, 1, 3)));
			Statics.WDepth = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.WorkA, 4, 3))));
			if (Statics.StandardDepth != 0)
			{
				if (Statics.StandardDepth != 0)
				{
					if (Statics.WDepth > Statics.StandardDepth)
					{
						Statics.DepthFactor = FCConvert.ToSingle(Math.Pow((FCConvert.ToDouble(Statics.WDepth) / Statics.StandardDepth), depthExp2));
					}
					else
					{
						Statics.DepthFactor = FCConvert.ToSingle(Math.Pow((FCConvert.ToDouble(Statics.WDepth) / Statics.StandardDepth), depthExp1));
					}
				}
				else
				{
					Statics.DepthFactor = 100;
					MessageBox.Show("Depth Factor = 0, because Standard Depth = 0", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				Statics.DepthFactor = 0;
			}
			if (Statics.StandardWidth != 0)
			{
				if (Statics.WUnits > Statics.StandardWidth)
				{
					Statics.WidthFactor = Math.Pow((Statics.WUnits / Statics.StandardWidth), widthExp2);
				}
				else
				{
					Statics.WidthFactor = Math.Pow((Statics.WUnits / Statics.StandardWidth), widthExp1);
				}
			}
			else
			{
				Statics.WidthFactor = 0;
			}
			Statics.Price[Statics.Row] = FCConvert.ToSingle(Statics.WUnits * Statics.StreetPrice * Statics.DepthFactor * Statics.WidthFactor);
		}

		public static void REAS25_3675_SQUARE_FOOT()
		{
			Statics.WUnits = FCConvert.ToSingle(Conversion.Val(Statics.WorkA));
			Statics.Price[Statics.Row] = FCConvert.ToSingle(Statics.WUnits * Statics.StreetPrice);
		}

		public static void REAS25_3700_FRACTIONAL_ACREAGE()
		{
			// corey
			try
			{
				// On Error GoTo ErrorHandler
				Statics.WUnits = FCConvert.ToSingle(Conversion.Val(Strings.Mid(Statics.WorkA, 1, Statics.WorkA.Length - 2)) + (Conversion.Val(Strings.Right(Statics.WorkA, 2)) / 100));
				if (Statics.StandardLot != 0)
				{
					if (Statics.WUnits > Statics.StandardLot)
					{
						// If Lexponent2 <> 0 Then
						Statics.Price[Statics.Row] = FCConvert.ToSingle((Math.Pow((Statics.WUnits / Statics.StandardLot), Statics.Lexponent2)) * Statics.StreetPrice);
						// Else
						// MsgBox "Price(ROW) = 0, because Lexponent2 = 0"
						// Print "PRICE(ROW) = 0, BECAUSE LEXPONENT2 = 0 "; Work1
						// INPUT ZZ$
						// Price(ROW) = StreetPrice * LandTrend
						// End If
					}
					else
					{
						// If Lexponent <> 0 Then
						Statics.Price[Statics.Row] = FCConvert.ToSingle((Math.Pow((Statics.WUnits / Statics.StandardLot), Statics.Lexponent)) * Statics.StreetPrice);
						// Else
						// MsgBox "Price(ROW) = 0, because Lexponent = 0"
						// Print "PRICE(ROW) = 0, BECAUSE LEXPONENT = 0 "; Work1
						// INPUT ZZ$
						// Price(ROW) = StreetPrice * LandTrend
						// End If
					}
				}
				else
				{
					Statics.Price[Statics.Row] = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fractional_acreage", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void REAS25_3725_ACREAGE_AND_SITE()
		{
			Statics.WUnits = FCConvert.ToSingle(Conversion.Val(Strings.Mid(Statics.WorkA, 1, Statics.WorkA.Length - 2)) + (Conversion.Val(Strings.Right(Statics.WorkA, 2)) / 100));
			Statics.Price[Statics.Row] = FCConvert.ToSingle(Statics.WUnits * Statics.StreetPrice);
		}

		public static void CalculateLinearFeetAdjusted()
		{
			Statics.WUnits = FCConvert.ToSingle(Conversion.Val(Strings.Mid(Statics.WorkA, 1, Statics.WorkA.Length - 2)) + (Conversion.Val(Strings.Right(Statics.WorkA, 2)) / 100));
			if (Statics.StandardWidth != 0)
			{
				if (Statics.WUnits > Statics.StandardWidth)
				{
					Statics.Price[Statics.Row] = FCConvert.ToSingle((Math.Pow((Statics.WUnits / Statics.StandardWidth), Statics.Lexponent2)) * Statics.StreetPrice);
				}
				else
				{
					Statics.Price[Statics.Row] = FCConvert.ToSingle((Math.Pow((Statics.WUnits / Statics.StandardWidth), Statics.Lexponent)) * Statics.StreetPrice);
				}
			}
			else
			{
				Statics.Price[Statics.Row] = 0;
			}
		}

		public static void REAS25_3750_EXTRA_INFLUENCE()
		{
			Statics.Value[Statics.Row] = Statics.Value[Statics.Row - 1] * Statics.InfluenceFactor;
			// LandTotal(CARD2) = LandTotal(CARD2) - Value(Row - 1)
			modProperty.Statics.LandTotal[modGlobalVariables.Statics.intCurrentCard] -= Statics.Value[Statics.Row - 1];
			Statics.Value[Statics.Row - 1] = 0;
		}

		public class StaticVariables
		{
			// Public a                        As Single
			//=========================================================
			public int Account;
			public float Acreage;
			public string AcreType = string.Empty;
			public float B;
			// vbPorter upgrade warning: BBPhysicalPercent As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			public int BBPhysicalPercent;
			public object BBFunctionalPercent;
			public string BBHoldYear = "";
			public object BBHoldGrade1;
			public object BBHoldGrade2;
			public object BBHoldCondition;
			public float C;
			// Public CARD2                    As Integer
			public string Category = "";
			public string CHANGED = "";
			// vbPorter upgrade warning: cy As Variant --> As double	OnWrite(double, int)
			// Public CurrentCard              As Integer
			// Public CorrelationCode          As String
			// Public CorrCode$
			// Public CNTR                     As Integer
			// Public CLR
			public double cy;
			public float D;
			// Public DAT$
			public string DD = "";
			public float DepthFactor;
			public double WidthFactor;
			// Public DUnit2$
			// Public DUnit1$
			// Public DUnits
			// Public DIFF
			public string DWEL1 = "";
			public string DWEL2 = "";
			// Public DwellingWRCNLD
			public double EconomicPercent;

			public string FirstLandFactor = "";



			public string HHAcreType = "";
			public int HighAccount;
			public object HOLDACCOUNT;
			public float HoldAcreage;
			// vbPorter upgrade warning: HoldDwellingEconomic As object	OnWrite(short, double)	OnReadFCConvert.ToDouble(
			public object HoldDwellingEconomic;
			public object HoldPhysicalPercent;
			// vbPorter upgrade warning: HOLDYEAR As string	OnWrite(int, string)
			public string HOLDYEAR = string.Empty;
			public string HT1 = "";
			public string HT2 = "";
			public string HYear = "";
			public int I;
			public string IDA = "";
			public string IMO = "";
			public string INF = "";
			public float InfluenceFactor;
			public string Inrec = "";
			public string Item = "";
			public string IYR = "";
			public string K1 = "";
            public double LandA;
			public double LandC;
			public double LandP;
			public float LandAcreTotal;
			// vbPorter upgrade warning: LandAcreValue As int	OnWrite(short, float)
			public float LandAcreValue;
			public string LandUnitType = "";
			public double LandV;
			public object LASTRECNUM;
			public double Lexponent;
			public double Lexponent2;
			// vbPorter upgrade warning: Life As object	OnWrite(double, short)
			public object Life;
			public object LIMIT;
			public object LINECOUNT;
			public string LL = "";
			public object LLL;
			public object LOCAT;
			public string LOGIT = "";
			public int LPP;
			public string M = "";
			public string MapLot = "";
			public int MasterKey;
			public int maxcard;
			public object MAXFILES;
            public string Message1 = "";
			public string Message2 = "";
			public object MLBEGIN;
			public object MLEND;
			public string MoHoDone = "";
			public string MROIType = "";
			public string MROIYear = "";
			public string MROIUnits = "";
			public string MROIGradeCD = "";
			public string MROIGradePct = "";
			public string MROICond = "";
			public string MROIPctPhys = "";
			public string MROIPctFunct = "";
			public string MRRSDwellingCode = string.Empty;
			public object NAMEREC;
			public string NET = string.Empty;
			public float NewBuilding;
			public float NewLand;
			public string NN = "";
			public object NNN;
			public string NW = "";
			public object OBFunctionalPercent;
			// vbPorter upgrade warning: OBPhysicalPercent As object	OnWrite(double, int)	OnRead(double, double)
			public double OBPhysicalPercent;
			public string OCC1 = "";
			public string OCC2 = "";
			// vbPorter upgrade warning: OccCode2 As Variant --> As double
			public double OccCode2;
			public float OldBuilding;
			public float OldLand;
			public string ORCode1 = "";
			public string ORCode2 = "";
			public double ORLand;
			public double ORBldg;
			public string OutBuildingPrint = "";
			public int PAGE;
			public double Per;
			public double Perval;
			public float PhysicalPercent;
			public float[] Price = new float[999 + 1];

			
			public double RateFactor;



			public string RecLocation = "";
			public string RecLocation1 = "";
			public string RecLocation2 = "";
			public string RecName = "";
			public string RecName1 = "";
			public string RecName2 = "";
			public string recnum = "";
			public object RecordNumber;
			public string RegularOrExempt = "";
			public string Resume2 = "";
			public int Row;
			public double RSZFactor;
			public double RSZFCTR;
			public string RT = "";
			public string s = "";
			public string SaveYear = string.Empty;
			public int SaleKey;
			public string SalesAnalysis = "";
			public int Schedule;
			public string SEQUENCE = "";
			public int SFLA;
			public int SFLATotal;
			public string ShortRSName = "";
			// vbPorter upgrade warning: StandardDepth As short --> As int	OnWriteFCConvert.ToDouble(
			public int StandardDepth;
			public double StandardWidth;
			public double StandardLot;
			public double StreetPrice;
			
			
			public double[] UnitPrice = new double[999 + 1];


			// vbPorter upgrade warning: Value As float	OnReadFCConvert.ToInt32(
			public float[] Value = new float[999 + 1];
			public int WDepth;
			// vbPorter upgrade warning: WExp As Variant --> As double
			public double WExp;
			public object WLgth;
			// vbPorter upgrade warning: WLife As object	OnWrite(object, int)
			public object WLife;
			
			public string Worked1 = string.Empty;
			public string Worked2 = string.Empty;
			public string WorkA = "";
			// vbPorter upgrade warning: WORKA2 As string	OnWrite(double, string)
			public string WORKA2 = "";
			public string WORKA4 = "";
			//public string WORKA5 = "";
			public string WORKA6 = string.Empty;
			public int WorkNeighborhood;
			public int WorkZone;
			public float WUnits;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
