﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage5.
	/// </summary>
	public partial class srptMVRPage5 : FCSectionReport
	{
		public srptMVRPage5()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage5(MunicipalValuationReturn valuationReturn): this()
        {
            this.valuationReturn = valuationReturn;
        }
		private void InitializeComponentEx()
		{
	
		}
		// nObj = 1
		//   0	srptMVRPage5	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            txtLine34h.Text = valuationReturn.ExemptProperty.Benevolent.ToInteger().FormatAsNumber();
            txtLine34i.Text = valuationReturn.ExemptProperty.LiteraryAndScientific.ToInteger().FormatAsNumber();
            txtLine34j.Text = valuationReturn.ExemptProperty.VeteranOrganizations.ToInteger().FormatAsNumber();
            txtLine38j2.Text = valuationReturn.ExemptProperty.VeteranOrgReimbursableExemption.ToInteger()
                .FormatAsNumber();
            txtLine34l1.Text = valuationReturn.ExemptProperty.NumberOfParsonages.FormatAsNumber();
            txtLine34l2.Text = valuationReturn.ExemptProperty.ExemptValueOfParsonages.ToInteger().FormatAsNumber();
            txtLine34l3.Text = valuationReturn.ExemptProperty.TaxableValueOfParsonages.ToInteger().FormatAsNumber();
            txtLine34l4.Text = valuationReturn.ExemptProperty.ReligiousWorship.ToInteger().FormatAsNumber();
            txtLine34l.Text =
                (valuationReturn.ExemptProperty.ReligiousWorship +
                 valuationReturn.ExemptProperty.ExemptValueOfParsonages).ToInteger().FormatAsNumber();
            txtLine34k.Text = valuationReturn.ExemptProperty.ChamberOfCommerce.ToInteger().FormatAsNumber();
            txtLine34m.Text = valuationReturn.ExemptProperty.FraternalOrganizations.ToInteger().FormatAsNumber();
            txtLine34n.Text = valuationReturn.ExemptProperty.HospitalPersonalProperty.ToInteger().FormatAsNumber();
			
			txtMunicipality.Text = valuationReturn.Municipality;
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
			txtLine34o.Text = valuationReturn.ExemptProperty.LegallyBlind.ToInteger().FormatAsNumber();
			txtLine34p.Text = valuationReturn.ExemptProperty.WaterSupplyTransport.ToInteger().FormatAsNumber();
			txtLine34q.Text = valuationReturn.ExemptProperty.CertifiedAnimalWasteStorage.ToInteger().FormatAsNumber();
			txtLine34r.Text = valuationReturn.ExemptProperty.PollutionControlFacilities.ToInteger().FormatAsNumber();
			txtLine34t.Text = valuationReturn.ExemptProperty.SnowmobileGroomingEquipment.ToInteger().FormatAsNumber();
		}

		
	}
}
