﻿using Autofac;
using SharedApplication;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.MVR;
using TWRE0000.MVR;

namespace TWRE0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmMunicipalValuationReturn>().As<IView<IMunicipalValuationReturnViewModel>>();
            builder.RegisterType<rptMunicipalValuationReturn>().As<IView<IMunicipalValuationReportViewModel>>();
        }
    }
}