﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmIncomeCostFiles.
	/// </summary>
	partial class frmIncomeCostFiles : BaseForm
	{
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCGrid Grid;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIncomeCostFiles));
			this.Command1 = new fecherFoundation.FCButton();
			this.Text1 = new fecherFoundation.FCTextBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 380);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Command1);
			this.ClientArea.Controls.Add(this.Text1);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Size = new System.Drawing.Size(610, 320);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(205, 30);
			this.HeaderText.Text = "Income Approach";
			// 
			// Command1
			// 
			this.Command1.AppearanceKey = "actionButton";
			this.Command1.Location = new System.Drawing.Point(200, 30);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(90, 40);
			this.Command1.TabIndex = 2;
			this.Command1.Text = "Select";
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// Text1
			// 
			this.Text1.AutoSize = false;
			this.Text1.BackColor = System.Drawing.SystemColors.Window;
			this.Text1.LinkItem = null;
			this.Text1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text1.LinkTopic = null;
			this.Text1.Location = new System.Drawing.Point(30, 30);
			this.Text1.Name = "Text1";
			this.Text1.Size = new System.Drawing.Size(150, 40);
			this.Text1.TabIndex = 1;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 90);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(590, 398);
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.Grid.TabIndex = 0;
			this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(467, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(110, 24);
			this.cmdNew.TabIndex = 3;
			this.cmdNew.Text = "Add New Code";
			this.cmdNew.Click += new System.EventHandler(this.mnuAdd_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(260, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(90, 48);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmIncomeCostFiles
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 488);
			this.FillColor = 0;
			this.Name = "frmIncomeCostFiles";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Income Approach";
			this.Load += new System.EventHandler(this.frmIncomeCostFiles_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmIncomeCostFiles_KeyDown);
			this.Resize += new System.EventHandler(this.frmIncomeCostFiles_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdNew;
		public FCButton cmdSave;
	}
}
