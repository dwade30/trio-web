﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPNewAccounts.
	/// </summary>
	public partial class rptPPNewAccounts : BaseSectionReport
	{
		public rptPPNewAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "New Accounts";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPPNewAccounts InstancePtr
		{
			get
			{
				return (rptPPNewAccounts)Sys.GetInstance(typeof(rptPPNewAccounts));
			}
		}

		protected rptPPNewAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPNewAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;
		int lngTotAssess;
		int lngTotCat1;
		int lngTotCat2;
		int lngTotCat3;
		int lngTotCat4;
		int lngTotCat5;
		int lngTotCat6;
		int lngTotCat7;
		int lngTotCat8;
		int lngTotCat9;
		bool boolHideCats;
		string[] strCat = new string[9 + 1];
		int lngTotAccounts;
		bool boolImportsOnly;

		public void Init(bool boolHideCategories)
		{
			boolHideCats = boolHideCategories;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "PPNewAccounts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool boolHidden = false;
			if (boolImportsOnly)
			{
				if (lngCRow < frmPPImportXF.InstancePtr.GridNew.Rows)
				{
					if (frmPPImportXF.InstancePtr.GridNew.RowHidden(lngCRow))
					{
						boolHidden = true;
					}
					else
					{
						boolHidden = false;
					}
					while (boolHidden && lngCRow < frmPPImportXF.InstancePtr.GridNew.Rows)
					{
						if (frmPPImportXF.InstancePtr.GridNew.RowHidden(lngCRow))
						{
							boolHidden = true;
						}
						else
						{
							boolHidden = false;
						}
						if (boolHidden)
						{
							lngCRow += 1;
						}
					}
				}
			}
			eArgs.EOF = lngCRow >= frmPPImportXF.InstancePtr.GridNew.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                int x = 0;
                if (!boolHideCats)
                {
                    // get category descriptions
                    clsLoad.OpenRecordset("select * from ratioTRENDS order by type", "twpp0000.vb1");
                    while (!clsLoad.EndOfFile())
                    {
                        x = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("type"))));
                        strCat[x] = fecherFoundation.Strings.Trim(
                            FCConvert.ToString(clsLoad.Get_Fields("description")));
                        if (strCat[x] == string.Empty)
                        {
                            strCat[x] = "Category " + FCConvert.ToString(x);
                        }

                        clsLoad.MoveNext();
                    }

                    lblCat1.Text = strCat[1];
                    lblCat2.Text = strCat[2];
                    lblCat3.Text = strCat[3];
                    lblCat4.Text = strCat[4];
                    lblCat5.Text = strCat[5];
                    lblCat6.Text = strCat[6];
                    lblCat7.Text = strCat[7];
                    lblCat8.Text = strCat[8];
                    lblCat9.Text = strCat[9];
                }

                lngTotAccounts = 0;
                if (frmPPImportXF.InstancePtr.chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
                {
                    boolImportsOnly = true;
                }
                else
                {
                    boolImportsOnly = false;
                }

                if (frmPPImportXF.InstancePtr.cmbUpdateType.ItemData(frmPPImportXF.InstancePtr.cmbUpdateType
                    .SelectedIndex) == modGlobalVariablesXF.CNSTUPDATEINFOONLY)
                {
                    Label10.Visible = false;
                    txtAssessment.Visible = false;
                    Label11.Visible = false;
                    txtTotalAssessment.Visible = false;
                }

                lblMuniname.Text = modGlobalConstants.Statics.MuniName;
                lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                lngCRow = 1;
                lngTotAssess = 0;
                lngTotCat1 = 0;
                lngTotCat2 = 0;
                lngTotCat3 = 0;
                lngTotCat4 = 0;
                lngTotCat5 = 0;
                lngTotCat6 = 0;
                lngTotCat7 = 0;
                lngTotCat8 = 0;
                lngTotCat9 = 0;
                if (boolHideCats)
                {
                    lblC1.Visible = false;
                    lblC2.Visible = false;
                    lblC3.Visible = false;
                    lblC4.Visible = false;
                    lblC5.Visible = false;
                    lblC6.Visible = false;
                    lblC7.Visible = false;
                    lblC8.Visible = false;
                    lblC9.Visible = false;
                    txtCat1.Visible = false;
                    txtCat2.Visible = false;
                    txtCat3.Visible = false;
                    txtCat4.Visible = false;
                    txtCat5.Visible = false;
                    txtCat6.Visible = false;
                    txtCat7.Visible = false;
                    txtCat8.Visible = false;
                    txtCat9.Visible = false;
                    this.Detail.Height = 390 / 1440F;
                    this.Detail.CanGrow = false;
                    this.Detail.CanShrink = false;
                    lblCat1.Visible = false;
                    lblCat2.Visible = false;
                    lblCat3.Visible = false;
                    lblCat4.Visible = false;
                    lblCat5.Visible = false;
                    lblCat6.Visible = false;
                    lblCat7.Visible = false;
                    lblCat8.Visible = false;
                    lblCat9.Visible = false;
                    txtTotCat1.Visible = false;
                    txtTotCat2.Visible = false;
                    txtTotCat3.Visible = false;
                    txtTotCat4.Visible = false;
                    txtTotCat5.Visible = false;
                    txtTotCat6.Visible = false;
                    txtTotCat7.Visible = false;
                    txttotCat8.Visible = false;
                    txtTotCat9.Visible = false;
                    lbln1.Visible = false;
                    lbln2.Visible = false;
                    lbln3.Visible = false;
                    lbln4.Visible = false;
                    lbln5.Visible = false;
                    lbln6.Visible = false;
                    lbln7.Visible = false;
                    lbln8.Visible = false;
                    lbln9.Visible = false;
                    lblCategory.Visible = false;
                    this.ReportFooter.CanShrink = true;
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngAssess;
			int lngValue;
			int lngExempt;
			int lngCat1 = 0;
			int lngCat2 = 0;
			int lngCat3 = 0;
			int lngCat4 = 0;
			int lngCat5 = 0;
			int lngCat6 = 0;
			int lngCat7 = 0;
			int lngCat8 = 0;
			int lngCat9 = 0;
			lngExempt = 0;
			lngValue = 0;
			lngAssess = 0;
			if (lngCRow < frmPPImportXF.InstancePtr.GridNew.Rows)
			{
				lngTotAccounts += 1;
				txtName.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPOWNER));
				if (Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER)) > 0)
				{
					txtLocation.Text = FCConvert.ToString(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER))) + " " + fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
				}
				else
				{
					txtLocation.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
				}
				if (Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPBUSINESSCODE)) > 0)
				{
					txtBusinessCode.Text = frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPBUSINESSCODE);
				}
				else
				{
					txtBusinessCode.Text = "";
				}
				lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPVALUE))));
				lngAssess = lngValue - lngExempt;
				lngTotAssess += lngAssess;
				txtAssessment.Text = Strings.Format(lngAssess, "#,###,###,##0");
				if (!boolHideCats)
				{
					lngCat1 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT1))));
					lngCat2 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT2))));
					lngCat3 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT3))));
					lngCat4 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT4))));
					lngCat5 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT5))));
					lngCat6 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT6))));
					lngCat7 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT7))));
					lngCat8 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT8))));
					lngCat9 = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPCAT9))));
					lngTotCat1 += lngCat1;
					lngTotCat2 += lngCat2;
					lngTotCat3 += lngCat3;
					lngTotCat4 += lngCat4;
					lngTotCat5 += lngCat5;
					lngTotCat6 += lngCat6;
					lngTotCat7 += lngCat7;
					lngTotCat8 += lngCat8;
					lngTotCat9 += lngCat9;
					txtCat1.Text = Strings.Format(lngCat1, "#,###,###,##0");
					txtCat2.Text = Strings.Format(lngCat2, "#,###,###,##0");
					txtCat3.Text = Strings.Format(lngCat3, "#,###,###,##0");
					txtCat4.Text = Strings.Format(lngCat4, "#,###,###,##0");
					txtCat5.Text = Strings.Format(lngCat5, "#,###,###,##0");
					txtCat6.Text = Strings.Format(lngCat6, "#,###,###,##0");
					txtCat7.Text = Strings.Format(lngCat7, "#,###,###,##0");
					txtCat8.Text = Strings.Format(lngCat8, "#,###,###,##0");
					txtCat9.Text = Strings.Format(lngCat9, "#,###,###,##0");
				}
			}
			else
			{
				txtAssessment.Text = "";
				txtBusinessCode.Text = "";
				txtLocation.Text = "";
				txtName.Text = "";
				if (!boolHideCats)
				{
					txtCat1.Text = "";
					txtCat2.Text = "";
					txtCat3.Text = "";
					txtCat4.Text = "";
					txtCat5.Text = "";
					txtCat6.Text = "";
					txtCat7.Text = "";
					txtCat8.Text = "";
					txtCat9.Text = "";
					lblC1.Visible = false;
					lblC2.Visible = false;
					lblC3.Visible = false;
					lblC4.Visible = false;
					lblC5.Visible = false;
					lblC6.Visible = false;
					lblC7.Visible = false;
					lblC8.Visible = false;
					lblC9.Visible = false;
				}
			}
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalAssessment.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
			txtTotal.Text = Strings.Format(lngTotAccounts, "#,###,###,##0");
			if (!boolHideCats)
			{
				txtTotCat1.Text = Strings.Format(lngTotCat1, "#,###,###,##0");
				txtTotCat2.Text = Strings.Format(lngTotCat2, "#,###,###,##0");
				txtTotCat3.Text = Strings.Format(lngTotCat3, "#,###,###,##0");
				txtTotCat4.Text = Strings.Format(lngTotCat4, "#,###,###,##0");
				txtTotCat5.Text = Strings.Format(lngTotCat5, "#,###,###,##0");
				txtTotCat6.Text = Strings.Format(lngTotCat6, "#,###,###,##0");
				txtTotCat7.Text = Strings.Format(lngTotCat7, "#,###,###,##0");
				txttotCat8.Text = Strings.Format(lngTotCat8, "#,###,###,##0");
				txtTotCat9.Text = Strings.Format(lngTotCat9, "#,###,###,##0");
			}
		}

		
	}
}
