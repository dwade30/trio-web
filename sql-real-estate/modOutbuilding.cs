﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Globalization;

namespace TWRE0000
{
	public class modOutbuilding
	{
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public static void REAS03_4550_CALL_OBSUB_2(short intRow, int intType)
		{
			REAS03_4550_CALL_OBSUB(ref intRow, ref intType);
		}

		public static void REAS03_4550_CALL_OBSUB(ref short intRow, ref int intType)
		{
			double dblRateFactor = 0;
			// vbPorter upgrade warning: lngSub2 As int	OnWrite(short, double)
			int lngSub2;
			if (intType > 699)
				Statics.bolFirstOB = true;
			if (Statics.intOIYear[intRow] == 0)
				Statics.intOIYear[intRow] = Statics.intYearBuilt;
			lngSub2 = 0;
			Statics.contributestosfla[intRow] = false;
			// If intOIUnits(intRow) <> 0 And intOIUnits(intRow) <> 9999 And intType <> 0 Then
			if (Statics.intOIUnits[intRow] != 0 && !Statics.boolUseSoundValue[intRow] && intType != 0)
			{
				if (intType < 700)
				{
					// dblRateFactor = olsfl(intType)
					dblRateFactor = modGlobalVariables.Statics.clsCostRecords.GetValue(ref intType, "Outbuilding", "sqftfactor");
				}
				else
				{
					dblRateFactor = 0.0;
				}
				lngSub2 = FCConvert.ToInt32(Statics.intOIUnits[intRow] * dblRateFactor);
				if (lngSub2 > 0)
					Statics.contributestosfla[intRow] = true;
				modDwelling.Statics.intSFLA += lngSub2;
			}
			REAS03_4600_PROCESS_OUTBUILDING(ref intRow, ref intType);
		}

		public static void REAS03_4600_PROCESS_OUTBUILDING(ref short intRow, ref int intType)
		{
			if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C")
			{
				if (Statics.intOIGradeCd[intRow] != 0 && Statics.intOIGradeCd[intRow] != 9)
					modDwelling.Statics.HoldGrade1 = Statics.intOIGradeCd[intRow];
				if (intType < 700 && modDwelling.Statics.HoldGrade2 == 0 && Statics.intOIGradePct[intRow] == 0)
				{
					Statics.intOIGradePct[intRow] = 100;
				}
				if (Statics.intOIGradePct[intRow] != 0)
					modDwelling.Statics.HoldGrade2 = Statics.intOIGradePct[intRow];
				if (Statics.intOICondition[intRow] != 0)
					modDwelling.Statics.HoldCondition = Statics.intOICondition[intRow];
			}
			if (intType > 699)
			{
				// If intOIUnits(intRow) <> 9999 Then
				if (!Statics.boolUseSoundValue[intRow])
				{
					Statics.bolMOHODone = true;
					modGlobalVariables.Statics.boolMobileHomePresent = true;
					modGlobalVariables.Statics.intLastMobileHomeDone = intRow;
					REAS03_4700_MOHO_COMPUTE(ref intRow);
				}
				else
				{
					// lngRepCostNewLD(intRow) = (CLng(intOIGradeCd(intRow)) * 100000) + (CLng(intOIGradePct(intRow)) * 100)
					Statics.lngRepCostNewLD[intRow] = Statics.lngOISoundValue[intRow];
				}
				// ElseIf intOIUnits(intRow) <> 9999 Then
			}
			else if (!Statics.boolUseSoundValue[intRow])
			{
				REAS03_4605_REGULAR_OUTBUILDING(ref intRow);
			}
			else
			{
				// lngRepCostNewLD(intRow) = CLng(intOIGradeCd(intRow) * 100000) + (CLng(intOIGradePct(intRow)) * 100)
				Statics.lngRepCostNewLD[intRow] = Statics.lngOISoundValue[intRow];
			}
			// put the call to ressizfactor here????
			Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard] += Statics.lngRepCostNewLD[intRow];
			modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] += Statics.lngRepCostNewLD[intRow];
			// ***SUBT& NOT FOUND ANYWHERE????****
			// Dim Subt&
			// If Subt& <> 0 Then
			// If bolFirstOB <> True Then OBHold(intRow) = lngRepCostNewLD(intRow)
			// lngSFLATotal = lngSFLATotal + lngRepCostNewLD(intRow)
			// End If
		}

		public static void REAS03_4605_REGULAR_OUTBUILDING(ref short intRow)
		{
			int intWork4;
			int x;
			// Get #2, intOIType(intRow), CR
			// Call OpenCRTable(CR, intOIType(intRow), dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(Statics.intOIType[intRow]);
			intWork4 = Statics.intOIGradePct[intRow];
			if (Statics.intOIGradeCd[intRow] == 0)
				Statics.intOIGradePct[intRow] = 100;
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				Statics.lngRepCostNew[intRow] = 0;
				if (!Statics.boolUseSoundValue[intRow])
				{
					Information.Err().Raise(20000, null, "All 9's in Cost Record for outbuilding type " + FCConvert.ToString(Statics.intOIType[intRow]) + " Account " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), null, null);
				}
			}
			else
			{
				Statics.lngRepCostNew[intRow] = FCConvert.ToInt32(((Statics.intOIUnits[intRow] * FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase)) * Statics.intOIGradePct[intRow]) / 10000.0);
			}
			if (Statics.intOIGradeCd[intRow] == 9 || Statics.intOIGradeCd[intRow] == 0)
			{
				modProperty.Statics.WKey = 1670 + modDwelling.Statics.HoldGrade1;
			}
			else
			{
				modProperty.Statics.WKey = 1670 + Statics.intOIGradeCd[intRow];
			}
			// Call OpenCRTable(CR, WKey, dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			// lngRepCostNew(intRow) = lngRepCostNew(intRow) * cr.fields("cunit") / 100
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				Statics.lngRepCostNew[intRow] = 0;
				if (!Statics.boolUseSoundValue[intRow])
				{
					Information.Err().Raise(20000, null, "All 9's in Cost Record for grade " + FCConvert.ToString(modProperty.Statics.WKey - 1670) + " Account " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " Outbuilding " + FCConvert.ToString(intRow), null, null);
				}
			}
			else
			{
				Statics.lngRepCostNew[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 100);
			}
			if (Statics.intOIGradeCd[intRow] == 9 || Statics.intOIGradeCd[intRow] == 0)
				Statics.lngRepCostNew[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * FCConvert.ToDouble(modDwelling.Statics.HoldGrade2 / 100.0));
			modProperty.Statics.WKey = 1720 + (Statics.intOIType[intRow] / 50 + 1);
			// Call OpenCRTable(CR, WKey, dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			modDwelling.Statics.dblLife = (fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase), 100000));
			Statics.lngRepCostNew[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 100);
			if (modDwelling.Statics.dblLife != 0)
			{
				modDwelling.Statics.dblExponent = (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) - (modDwelling.Statics.dblLife * 100000)) / 100;
			}
			if (Statics.intOIPctPhysical[intRow] == 0 && (Statics.intOICondition[intRow] == 9 || Statics.intOICondition[intRow] == 0))
			{
				if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C" && !modGlobalVariables.Statics.boolMobileHomePresent && intRow > 1)
				{
					Statics.dblPhysical[intRow] = Statics.dblPhysical[intRow - 1];
					Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * Statics.dblPhysical[intRow]);
				}
				else
				{
					Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * Statics.BBPhysicalPercent);
					Statics.dblPhysical[intRow] = Statics.BBPhysicalPercent;
					// * 100
				}
			}
			else if (Statics.intOIPctPhysical[intRow] == 0 && Statics.intOICondition[intRow] < 9 && Statics.intOICondition[intRow] > 0)
			{
				REAS03_4610_OUTBUILDING_DEPRECIATION(ref intRow);
			}
			else
			{
				Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * Statics.intOIPctPhysical[intRow] / 100.0);
				Statics.dblPhysical[intRow] = Statics.intOIPctPhysical[intRow] / 100.0;
			}
			if (Statics.intOIPctFunctional[intRow] == 0 && (Statics.intOICondition[intRow] == 9 || Statics.intOICondition[intRow] == 0))
			{
				// corey
				if (!Statics.contributestosfla[intRow] || (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" != "Y"))
				{
					Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNewLD[intRow] * Statics.BBFunctionalPercent);
					Statics.dblFunctional[intRow] = Statics.BBFunctionalPercent * 100;
				}
				else
				{
					Statics.dblFunctional[intRow] = Statics.BBFunctionalPercent * 100;
				}
			}
			else
			{
				// lngRepCostNewLD(intRow) = lngRepCostNewLD(intRow) * intOIPctFunctional(intRow) / 100
				Statics.dblFunctional[intRow] = Statics.intOIPctFunctional[intRow];
				// corey. change functional here?
				// If contributestosfla(intRow) Then
				// dblFunctional(intRow) = dblFunctional(intRow) * RSZFactor
				// End If
				// corey 072001
				if (!Statics.contributestosfla[intRow] || (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" == "C") || (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" != "Y"))
				{
					//FC:FINAL:MSH - i.issue #1328: incorrect order of operations (missing data)
					//lngRepCostNewLD[intRow] *= FCConvert.ToInt32(dblFunctional[intRow] / 100;
					Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNewLD[intRow] * Statics.dblFunctional[intRow] / 100);
				}
			}
			Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNewLD[intRow] * modDwelling.Statics.dblEconomicPercent);
			if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C")
			{
				if (Statics.intOIGradeCd[intRow] != 0 && Statics.intOIGradeCd[intRow] != 9)
					modDwelling.Statics.HoldGrade1 = Statics.intOIGradeCd[intRow];
				if (intWork4 != 0)
					modDwelling.Statics.HoldGrade2 = intWork4;
				if (Statics.intOICondition[intRow] != 0)
					modDwelling.Statics.HoldCondition = Statics.intOICondition[intRow];
				if (Statics.intOIPctPhysical[intRow] != 0 && Statics.bolMOHODone == false)
					Statics.BBPhysicalPercent = Statics.intOIPctPhysical[intRow] / 100.0;
				if (Statics.intOIPctPhysical[intRow] == 0)
				{
					if (Statics.dblPhysical[intRow] == 100 && intRow > 1)
						Statics.dblPhysical[intRow] = Statics.dblPhysical[intRow - 1];
					if (Statics.bolMOHODone == false)
						Statics.BBPhysicalPercent = Statics.dblPhysical[intRow] / 100;
				}
				if (Statics.intOIPctFunctional[intRow] != 0)
					Statics.BBFunctionalPercent = Statics.dblFunctional[intRow] / 100;
			}
		}

		public static void REAS03_4610_OUTBUILDING_DEPRECIATION(ref short intRow)
		{
			if (modDwelling.Statics.dblLife != 0)
			{
				// Get #2, (1460 + intOICondition(intRow)), CR
				// Call OpenCRTable(CR, (1460 + intOICondition(intRow)), dbOpenForwardOnly)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1460 + Statics.intOICondition[intRow]));
				// WHigh = (cr.fields("cunit") \ 1000000)
				// WLow = (cr.fields("cunit") - (WHigh * 1000000))
				modDwelling.Statics.WHigh = FCConvert.ToDouble(Strings.Format((fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit), 1000000)), "0.00"));
				// WLow = (CostRec.CUnit - (WHigh * 1000000))
				modDwelling.Statics.WLow = FCConvert.ToDouble(Strings.Right(modGlobalVariables.Statics.CostRec.CUnit, 3));
				Statics.intCY = DateTime.Today.Year;
				if (modGlobalVariables.Statics.DepreciationYear != 0 && modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" != "C")
				{
					Statics.intCY = modGlobalVariables.Statics.DepreciationYear;
				}
				else if (modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear != 0 && modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + "" == "C")
				{
					Statics.intCY = modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear;
				}
				if (Statics.intCY < Statics.intOIYear[intRow])
					Statics.intCY = Statics.intOIYear[intRow];
				if (modDwelling.Statics.dblLife < Statics.intCY - Statics.intOIYear[intRow])
					Statics.intCY = FCConvert.ToInt32(Statics.intOIYear[intRow] + modDwelling.Statics.dblLife);
				Statics.dblPhysical[intRow] = modDwelling.Statics.WHigh - ((modDwelling.Statics.WHigh - modDwelling.Statics.WLow) * (Math.Pow(((Statics.intCY - Statics.intOIYear[intRow]) / modDwelling.Statics.dblLife), modDwelling.Statics.dblExponent)));
				Statics.dblPhysical[intRow] = Conversion.Int(0.5 + Statics.dblPhysical[intRow]);
			}
			else
			{
				modDwelling.Statics.strDwellingCode = "O";
				modDwelling.Statics.intHoldYear = Statics.intOIYear[intRow];
				// Get #2, (1460 + intOICondition(intRow)), CR
				// Call OpenCRTable(CR, (1460 + intOICondition(intRow)), dbOpenForwardOnly)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1460 + Statics.intOICondition[intRow]));
				double HoldPhysicalPercent = 0;
				// corey
				modGlobalVariables.Statics.CRCUnit = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit);
				modGlobalVariables.Statics.CRCBase = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase);
				HoldPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
				modREASValuations.Statics.HOLDYEAR = FCConvert.ToString(Statics.intOIYear[intRow]);
				modGlobalVariables.Statics.boolFromOutbuilding = true;
				// Call PhysicalPercentRoutine
				modREASValuations.new_physicalpercentroutine();
				modGlobalVariables.Statics.boolFromOutbuilding = false;
				if (modDwelling.Statics.strDwellingCode == "O" || modDwelling.Statics.strDwellingCode == "N")
				{
					modREASValuations.Statics.OBPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
					modDwelling.Statics.dblPhysicalPercent = HoldPhysicalPercent;
				}
				// corey
				Statics.dblPhysical[intRow] = FCConvert.ToDouble(modREASValuations.Statics.OBPhysicalPercent) * 100;
			}
			// dblPhysical(intRow) = Int(0.5 + dblPhysical(intRow))
			Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * Statics.dblPhysical[intRow] / 100);
			Statics.dblPhysical[intRow] /= 100;
		}

		public static void REAS03_4700_MOHO_COMPUTE(ref short intRow)
		{
			double OBFunctionalPercent;
			int intWidth;
			int intLength;
			// vbPorter upgrade warning: dblGrade As double	OnReadFCConvert.ToInt32(
			double dblGrade = 0;
			double dblFactor = 0;
			// Get #2, intOIType(intRow), CR
			// Call OpenCRTable(CR, (intOIType(intRow)), dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(Statics.intOIType[intRow]);
			intWidth = Statics.intOIUnits[intRow] / 100;
			intLength = Statics.intOIUnits[intRow] - (intWidth * 100);
			if (Statics.intOIGradeCd[intRow] == 0 && Statics.intOIGradePct[intRow] == 0)
			{
				dblGrade = FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase), 100);
				dblFactor = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100;
			}
			else
			{
				dblGrade = Statics.intOIGradeCd[intRow];
				dblFactor = Statics.intOIGradePct[intRow] / 100.0;
			}
			if (dblGrade == 1)
				dblGrade = 2;
			modDwelling.Statics.HoldGrade1 = FCConvert.ToInt32(dblGrade);
			modDwelling.Statics.HoldGrade2 = FCConvert.ToInt32(dblFactor * 100);
			modDwelling.Statics.HoldCondition = Statics.intOICondition[intRow];
			Statics.intYearBuilt = Statics.intOIYear[intRow];
			if (!modGlobalRoutines.IsOdd_2(FCConvert.ToInt16(intWidth)) && intWidth > 7 && intWidth < 29)
			{
				// subtract one since we use codes 2 to 5 not 1 to 4
				modProperty.Statics.WKey = FCConvert.ToInt32(1740 + dblGrade + ((intWidth - 8) * 2) - 1);
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) != 99999999 && FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) != 99999999)
				{
					Statics.lngRepCostNew[intRow] = fecherFoundation.FCUtils.iDiv(((((FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100) * intLength) + (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) / 100)) * dblFactor), 1);
				}
				else
				{
					Statics.lngRepCostNew[intRow] = 0;
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "Cannot calculate a mobile home of width " + FCConvert.ToString(intWidth) + " because cost record contains all 9's. Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n";
				}
			}
			else
			{
				double dblLowerRec = 0;
				double dblLowerBase = 0;
				double dblHigherRec = 0;
				double dblHigherBase = 0;
				double dblRec = 0;
				double dblBase = 0;
				if (intWidth < 8)
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(1740 + dblGrade - 1);
					// 8 feet wide
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) != 99999999 && FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) != 99999999)
					{
						Statics.lngRepCostNew[intRow] = fecherFoundation.FCUtils.iDiv(((((FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100) * intLength) + (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) / 100)) * dblFactor), 1);
					}
					else
					{
						Statics.lngRepCostNew[intRow] = 0;
						if (!Statics.boolUseSoundValue[intRow])
						{
							modSpeedCalc.Statics.boolCalcErrors = true;
							modSpeedCalc.Statics.CalcLog += "Cannot calculate a mobile home of width " + FCConvert.ToString(intWidth) + " because cost record for width 8 contains all 9's. Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n";
						}
					}
				}
				else if (intWidth > 28)
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(1740 + dblGrade + ((28 - 8) * 2) - 1);
					// 28 feet wide
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) != 99999999 && FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) != 99999999)
					{
						Statics.lngRepCostNew[intRow] = fecherFoundation.FCUtils.iDiv(((((FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100) * intLength) + (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) / 100)) * dblFactor), 1);
					}
					else
					{
						Statics.lngRepCostNew[intRow] = 0;
						if (!Statics.boolUseSoundValue[intRow])
						{
							modSpeedCalc.Statics.boolCalcErrors = true;
							modSpeedCalc.Statics.CalcLog += "Cannot calculate a mobile home of width " + FCConvert.ToString(intWidth) + " because cost record for width 28 contains all 9's. Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n";
						}
					}
				}
				else
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(1740 + dblGrade + (((intWidth - 1) - 8) * 2) - 1);
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					dblLowerRec = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100;
					dblLowerBase = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) / 100;
					modProperty.Statics.WKey = FCConvert.ToInt32(1740 + dblGrade + (((intWidth + 1) - 8) * 2) - 1);
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					dblHigherRec = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100;
					dblHigherBase = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) / 100;
					if (dblLowerRec == 999999.99 || dblLowerBase == 999999.99)
					{
						dblLowerRec = dblHigherRec;
						dblLowerBase = dblHigherBase;
					}
					if (dblHigherRec == 999999.99 || dblHigherBase == 999999.99)
					{
						dblHigherRec = dblLowerRec;
						dblHigherBase = dblLowerBase;
					}
					dblRec = (dblLowerRec + dblHigherRec) / 2;
					dblBase = (dblLowerBase + dblHigherBase) / 2;
					if (dblRec != 999999.99 && dblBase != 999999.99)
					{
						Statics.lngRepCostNew[intRow] = fecherFoundation.FCUtils.iDiv((((dblRec * intLength) + dblBase) * dblFactor), 1);
					}
					else
					{
						Statics.lngRepCostNew[intRow] = 0;
						if (!Statics.boolUseSoundValue[intRow])
						{
							modSpeedCalc.Statics.boolCalcErrors = true;
							modSpeedCalc.Statics.CalcLog += "Cannot calculate a mobile home of width " + FCConvert.ToString(intWidth) + " because cost records for widths " + FCConvert.ToString(intWidth - 1) + " and " + FCConvert.ToString(intWidth + 1) + " contain all 9's. Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "\r\n";
						}
					}
				}
			}
			modProperty.Statics.WKey = 1740;
			// Get #2, WKEY, CR
			// Call OpenCRTable(CR, WKey, dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			Statics.lngRepCostNew[intRow] = fecherFoundation.FCUtils.iDiv((Statics.lngRepCostNew[intRow] * (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100)), 1);
			if (Statics.intOIPctPhysical[intRow] != 0)
			{
				modREASValuations.Statics.OBPhysicalPercent = Statics.intOIPctPhysical[intRow];
			}
			else
			{
				modProperty.Statics.WKey = 1790 + Statics.intOICondition[intRow];
				// Get #2, WKEY, CR
				// Call OpenCRTable(CR, WKey, dbOpenForwardOnly)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				if (modProperty.Statics.WKey == 1790 && FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
				{
					modProperty.Statics.WKey = 1790 + 4;
					// average condition
				}
				modDwelling.Statics.WHigh = (fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit), 1000000));
				// WLow = (CostRec.CUnit - (WHigh * 1000000))
				modDwelling.Statics.WLow = FCConvert.ToDouble(Strings.Right(modGlobalVariables.Statics.CostRec.CUnit, 3));
				modDwelling.Statics.dblLife = (fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase), 100000));
				modDwelling.Statics.dblExponent = (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) - (modDwelling.Statics.dblLife * 100000)) / 100;
				// intCY = Format(Year(Now), "yyyy")
				Statics.intCY = DateTime.Now.Year;
				if (modGlobalVariables.Statics.DepreciationYearMoHo != 0)
					Statics.intCY = modGlobalVariables.Statics.DepreciationYearMoHo;
				if (Statics.intCY < Statics.intOIYear[intRow])
					Statics.intCY = Statics.intOIYear[intRow];
				if (Statics.intCY - Statics.intOIYear[intRow] > modDwelling.Statics.dblLife)
					Statics.intCY = FCConvert.ToInt32(Statics.intOIYear[intRow] + modDwelling.Statics.dblLife);
				if (modDwelling.Statics.dblLife != 0)
				{
					modREASValuations.Statics.OBPhysicalPercent = modDwelling.Statics.WHigh - ((modDwelling.Statics.WHigh - modDwelling.Statics.WLow) * (Math.Pow(((Statics.intCY - Statics.intYearBuilt) / modDwelling.Statics.dblLife), modDwelling.Statics.dblExponent)));
				}
				else
				{
					modREASValuations.Statics.OBPhysicalPercent = 0;
					MessageBox.Show("Outbuilding Physical Percent = 0, because the Base Column of Line " + FCConvert.ToString(modProperty.Statics.WKey) + " in the Dwelling Cost Files is Zero.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			modREASValuations.Statics.OBPhysicalPercent = modREASValuations.Statics.OBPhysicalPercent / 100;
			Statics.dblPhysical[intRow] = FCConvert.ToDouble(modREASValuations.Statics.OBPhysicalPercent);
			Statics.BBPhysicalPercent = FCConvert.ToDouble(modREASValuations.Statics.OBPhysicalPercent);
			Statics.dblFunctional[intRow] = Statics.intOIPctFunctional[intRow];
			OBFunctionalPercent = Statics.dblFunctional[intRow] / 100;
			Statics.BBFunctionalPercent = OBFunctionalPercent;
			Statics.lngRepCostNewLD[intRow] = FCConvert.ToInt32(Statics.lngRepCostNew[intRow] * FCConvert.ToDouble(modREASValuations.Statics.OBPhysicalPercent) * OBFunctionalPercent * modDwelling.Statics.dblEconomicPercent);
			modDwelling.Statics.lngSFLATotal += Statics.lngRepCostNewLD[intRow];
			modDwelling.Statics.intSFLA += (intWidth * intLength);
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		public static double REAS03_6200_REGULAR_OUTBUILDINGS(ref short intRow)
		{
			double REAS03_6200_REGULAR_OUTBUILDINGS = 0;
			REAS03_6200_REGULAR_OUTBUILDINGS = 0;
			if (Statics.intOIGradeCd[intRow] == 9 || Statics.intOIGradeCd[intRow] == 0)
			{
				modProperty.Statics.WKey = 1670 + modDwelling.Statics.HoldGrade1;
			}
			else
			{
				modProperty.Statics.WKey = 1670 + Statics.intOIGradeCd[intRow];
			}
			// Get #2, WKEY, CR
			// Call OpenCRTable(CR, WKey, dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (Statics.intOIGradeCd[intRow] == 0)
				Statics.intOIGradePct[intRow] = 100;
			if (Statics.intOIGradeCd[intRow] == 9 || Statics.intOIGradeCd[intRow] == 0)
			{
				if (Conversion.Val(modREASValuations.Statics.WORKA4) == 0)
				{
					REAS03_6200_REGULAR_OUTBUILDINGS = modDwelling.Statics.HoldGrade2 / 100.0;
				}
				else
				{
					REAS03_6200_REGULAR_OUTBUILDINGS = modDwelling.Statics.HoldGrade2 * Statics.intOIGradePct[intRow] / 10000.0;
				}
			}
			else
			{
				REAS03_6200_REGULAR_OUTBUILDINGS = Statics.intOIGradePct[intRow] / 100.0;
			}
			return REAS03_6200_REGULAR_OUTBUILDINGS;
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		public static double REAS03_6210_MOHOS(ref short intRow)
		{
			double REAS03_6210_MOHOS = 0;
			REAS03_6210_MOHOS = 0;
			if ((Statics.intOIGradeCd[intRow] == 0 && Statics.intOIGradePct[intRow] == 0) || (Statics.intOIGradeCd[intRow] == 9 && Statics.intOIGradePct[intRow] == 100))
			{
				modProperty.Statics.WKey = 1670 + fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase), 100);
				modDwelling.Statics.HoldGrade1 = fecherFoundation.FCUtils.iDiv(FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase), 100);
				REAS03_6210_MOHOS = FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100;
			}
			else
			{
				// only have codes 2 to 5 not 1 to 4
				if (Statics.intOIGradeCd[intRow] == 1)
					Statics.intOIGradeCd[intRow] = 2;
				modProperty.Statics.WKey = 1740 + Statics.intOIGradeCd[intRow] - 1;
				modDwelling.Statics.HoldGrade1 = Statics.intOIGradeCd[intRow];
				REAS03_6210_MOHOS = Statics.intOIGradePct[intRow] / 100.0;
			}
			// Get #2, WKEY, CR
			// Call OpenCRTable(CR, WKey, dbOpenForwardOnly)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			modDwelling.Statics.HoldGrade2 = FCConvert.ToInt32(REAS03_6210_MOHOS * 100);
			return REAS03_6210_MOHOS;
		}

		public static void FillOutbuildingArrays()
		{
			/*? On Error Resume Next  */
			try
			{
				int x;
				for (x = 1; x <= 10; x++)
				{
					Statics.lngOutbuildingTotal[x] = 0;
				}
				// x
				if (modDataTypes.Statics.OUT.Get_Fields_Int32("rsaccount") == modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") && modDataTypes.Statics.OUT.Get_Fields_Int32("rscard") == modDataTypes.Statics.MR.Get_Fields_Int32("rscard"))
				{
					for (x = 1; x <= 10; x++)
					{
						// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
						Statics.intOIType[x] = FCConvert.ToInt16(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
						Statics.intOIYear[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oiyear" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
						Statics.intOIUnits[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oiunits" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
						Statics.intOIGradeCd[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oigradecd" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
						Statics.intOIGradePct[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oigradepct" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
						Statics.intOICondition[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oicond" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
						Statics.intOIPctPhysical[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oipctphys" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
						Statics.intOIPctFunctional[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oipctfunct" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
						Statics.boolUseSoundValue[x] = FCConvert.ToBoolean(modDataTypes.Statics.OUT.Get_Fields("oiusesound" + FCConvert.ToString(x)));
						// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
						Statics.lngOISoundValue[x] = FCConvert.ToInt32(modDataTypes.Statics.OUT.Get_Fields("oisoundvalue" + FCConvert.ToString(x)));
					}
					// x
				}
				else
				{
					for (x = 1; x <= 10; x++)
					{
						Statics.intOIType[x] = 0;
						Statics.intOIYear[x] = 0;
						Statics.intOIUnits[x] = 0;
						Statics.intOIGradeCd[x] = 0;
						Statics.intOIGradePct[x] = 0;
						Statics.intOICondition[x] = 0;
						Statics.intOIPctPhysical[x] = 0;
						Statics.intOIPctFunctional[x] = 0;
						Statics.boolUseSoundValue[x] = false;
						Statics.lngOISoundValue[10] = 0;
					}
					// x
				}
			}
			catch
			{
			}
		}

		public static void CheckOutbuildingDisplay(ref short intRow)
		{
		}

		public static void Speed_COMPUTE_OUTBUILDING()
		{
			FCUtils.EraseSafe(Statics.intOIType);
			FCUtils.EraseSafe(Statics.intOIUnits);
			FCUtils.EraseSafe(Statics.intOIGradeCd);
			FCUtils.EraseSafe(Statics.intOIGradePct);
			FCUtils.EraseSafe(Statics.intOICondition);
			FCUtils.EraseSafe(Statics.lngOISoundValue);
			FCUtils.EraseSafe(Statics.boolUseSoundValue);
			Statics.BBPhysicalPercent = modDwelling.Statics.dblPhysicalPercent;
			Statics.OutEconomic = modDwelling.Statics.dblEconomicPercent;
			if (Statics.BBPhysicalPercent == 0)
				Statics.BBPhysicalPercent = 1;
			Statics.BBFunctionalPercent = modDwelling.Statics.dblFunctionalPercent;
			// 8/7/02 testing
			if (Statics.BBFunctionalPercent == 0)
			{
				if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C")
				{
					Statics.BBFunctionalPercent = 1;
				}
			}
			Statics.BBHoldYear = modDwelling.Statics.intHoldYear;
			Statics.BBHoldGrade1 = modDwelling.Statics.HoldGrade1;
			Statics.BBHoldGrade2 = modDwelling.Statics.HoldGrade2;
			Statics.BBHoldCondition = modDwelling.Statics.HoldCondition;
			Statics.intYearBuilt = modDwelling.Statics.intHoldYear;
			if (modGlobalVariables.Statics.intSFLAOption == 4)
			{
				Statics.HoldSfla = modDwelling.Statics.intSFLA;
				Statics.HoldBValue = modDwelling.Statics.lngSFLATotal;
			}
			// Call fillolsflarray
			// Call OpenOutBuildingTable(OUT, CurrentAccount, intCurrentCard)
			// Call OpenOutBuildingTable(OUT, gintLastAccountNumber, intCurrentCard)
			FillOutbuildingArrays();
			Statics.bolFirstOB = false;
			// 
			modGlobalVariables.Statics.boolMobileHomePresent = false;
			if (Statics.intOIType[1] != 0)
				REAS03_4550_CALL_OBSUB_2(1, Statics.intOIType[1]);
			if (Statics.intOIType[2] != 0)
				REAS03_4550_CALL_OBSUB_2(2, Statics.intOIType[2]);
			if (Statics.intOIType[3] != 0)
				REAS03_4550_CALL_OBSUB_2(3, Statics.intOIType[3]);
			if (Statics.intOIType[4] != 0)
				REAS03_4550_CALL_OBSUB_2(4, Statics.intOIType[4]);
			if (Statics.intOIType[5] != 0)
				REAS03_4550_CALL_OBSUB_2(5, Statics.intOIType[5]);
			if (Statics.intOIType[6] != 0)
				REAS03_4550_CALL_OBSUB_2(6, Statics.intOIType[6]);
			if (Statics.intOIType[7] != 0)
				REAS03_4550_CALL_OBSUB_2(7, Statics.intOIType[7]);
			if (Statics.intOIType[8] != 0)
				REAS03_4550_CALL_OBSUB_2(8, Statics.intOIType[8]);
			if (Statics.intOIType[9] != 0)
				REAS03_4550_CALL_OBSUB_2(9, Statics.intOIType[9]);
			if (Statics.intOIType[10] != 0)
				REAS03_4550_CALL_OBSUB_2(10, Statics.intOIType[10]);
			// put call to ressizfactor here?
			// call ressizfactor then calc new funct percents and new totals
			// call ressizefactor
			modDwelling.Statics.dblFunctionalPercent = modGlobalVariables.Statics.dbldwellfunctional;
			if (Strings.UCase(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"))) == "Y")
			{
				modDwelling.SpeedResidentialSizeFactor();
			}
			// calcdwellfunctpercent
			modGlobalVariables.Statics.dbldwellfunctional = modDwelling.Statics.dblFunctionalPercent;
			// calcobfunctpercents
			// 
			// Dim forindex    As Integer
			// Dim tempvalue   As Long
			// Dim difference  As Long
			// For forindex = 1 To 10
			// If contributestosfla(forindex) Then
			// dblFunctional(forindex) = dblFunctional(forindex) * RSZFactor
			// tempvalue = lngRepCostNewLD(forindex)
			// lngRepCostNewLD(forindex) = lngRepCostNewLD(forindex) * RSZFactor
			// difference = tempvalue - lngRepCostNewLD(forindex)
			// BuildingTotal(intCurrentCard) = BuildingTotal(intCurrentCard) - difference
			// 
			// lngsflatotal(intcurrentcard) = lngsflatotal(intcurrentcard) - difference
			// End If
			// Next forindex
			// calcnewtotals
			// print dwellinginfo
			modDwelling.Statics.intHoldYear = Statics.BBHoldYear;
			modDwelling.Statics.HoldGrade1 = Statics.BBHoldGrade1;
			modDwelling.Statics.HoldGrade2 = Statics.BBHoldGrade2;
			modDwelling.Statics.HoldCondition = Statics.BBHoldCondition;
		}

		public static void Speed_OUTBUILDING_DATA()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// Dim intTemp As Integer
				if (Statics.intOIType[1] != 0)
					SpeedDisplayOutbuildings_2(1);
				if (Statics.intOIType[2] != 0)
					SpeedDisplayOutbuildings_2(2);
				if (Statics.intOIType[3] != 0)
					SpeedDisplayOutbuildings_2(3);
				if (Statics.intOIType[4] != 0)
					SpeedDisplayOutbuildings_2(4);
				if (Statics.intOIType[5] != 0)
					SpeedDisplayOutbuildings_2(5);
				if (Statics.intOIType[6] != 0)
					SpeedDisplayOutbuildings_2(6);
				if (Statics.intOIType[7] != 0)
					SpeedDisplayOutbuildings_2(7);
				if (Statics.intOIType[8] != 0)
					SpeedDisplayOutbuildings_2(8);
				if (Statics.intOIType[9] != 0)
					SpeedDisplayOutbuildings_2(9);
				if (Statics.intOIType[10] != 0)
					SpeedDisplayOutbuildings_2(10);
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows = frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows + 3
				// intTemp = frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows
				// With frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1)
				// .MergeCells = flexMergeSpill
				// End With
				if (modDwelling.Statics.intSFLA != 0 && modGlobalVariables.Statics.intSFLAOption != 5)
				{
					if (modGlobalVariables.Statics.intSFLAOption == 1)
					{
						Statics.dblPerVal = (modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] + modProperty.Statics.LandTotal[modGlobalVariables.Statics.intCurrentCard]) / modDwelling.Statics.intSFLA;
					}
					else if (modGlobalVariables.Statics.intSFLAOption == 2)
					{
						Statics.dblPerVal = (modProperty.Statics.LandTotal[modGlobalVariables.Statics.intCurrentCard] + modDwelling.Statics.lngSFLATotal) / modDwelling.Statics.intSFLA;
					}
					else if (modGlobalVariables.Statics.intSFLAOption == 3 || (modGlobalVariables.Statics.intSFLAOption == 4 && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C"))
					{
						Statics.dblPerVal = modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] / modDwelling.Statics.intSFLA;
					}
					else if (modGlobalVariables.Statics.intSFLAOption == 4)
					{
						Statics.dblPerVal = FCConvert.ToDouble(modDwelling.Statics.lngSFLATotal) / modDwelling.Statics.intSFLA;
					}
					if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C")
					{
						// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 2, 0) = Format(intSFLA, "#,##0") & " " & "SF"
						modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].SFLA = Strings.Format(modDwelling.Statics.intSFLA, "#,##0") + " " + "SF";
						if (modGlobalVariables.Statics.intSFLAOption != 6)
						{
							// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 2, 2) = Format(dblPerVal, "#,##0.00") & " = $/SF (" & intSFLAOption & ")"
							modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].SFLAPerVal = Strings.Format(Statics.dblPerVal, "#,##0.00") + " = $/SF (" + FCConvert.ToString(modGlobalVariables.Statics.intSFLAOption) + ")";
						}
						else
						{
							modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].SFLAPerVal = "";
						}
					}
					else
					{
						// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 2, 0) = Format(intSFLA, "#,##0") & " " & "SFLA"
						modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].SFLA = Strings.Format(modDwelling.Statics.intSFLA, "#,##0") + " " + "SFLA";
						if (modGlobalVariables.Statics.intSFLAOption != 6)
						{
							// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 2, 2) = Format(dblPerVal, "#,##0.00") & " = $/SFLA (" & intSFLAOption & ")"
							modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].SFLAPerVal = Strings.Format(Statics.dblPerVal, "#,##0.00") + " = $/SFLA (" + FCConvert.ToString(modGlobalVariables.Statics.intSFLAOption) + ")";
						}
						else
						{
							modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].SFLAPerVal = "";
						}
					}
				}
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 2, 6) = "OUTBUILDING TOTAL"
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 2, 9) = Format(lngOutbuildingTotal(intCurrentCard), "#,##0")
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 1, 6) = "BUILDING TOTAL"
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).TextMatrix(intTemp - 1, 9) = Format(BuildingTotal(intCurrentCard), "###,###,##0")
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].TotalOutbuilding = Strings.Format(Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard], "#,##0");
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].TotalBuilding = Strings.Format(modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard], "###,###,##0");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Outbuilding Data", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void SpeedDisplayOutbuildings_2(short intRow)
		{
			SpeedDisplayOutbuildings(ref intRow);
		}

		public static void SpeedDisplayOutbuildings(ref short intRow)
		{
			double dblGrade = 0;
			int WKEYTemp;
			int intNumRows;
			string strTemp = "";
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows = frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows + 1
				// frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows = intRow + 1
				// intNumRows = frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1).Rows
				// If intOIType(intRow) > 699 And intOIUnits(intRow) <> 9999 Then
				if (Statics.intOIType[intRow] > 699 && !Statics.boolUseSoundValue[intRow])
				{
					modDwelling.Statics.HoldGrade1 = Statics.intOIGradeCd[intRow];
					modDwelling.Statics.HoldGrade2 = Statics.intOIGradePct[intRow];
					modDwelling.Statics.HoldCondition = Statics.intOICondition[intRow];
				}
				else if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "Y" && FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) != "C")
				{
					if (Statics.intOIGradeCd[intRow] != 0 && Statics.intOIGradeCd[intRow] != 9)
						modDwelling.Statics.HoldGrade1 = Statics.intOIGradeCd[intRow];
					if (Statics.intOIGradePct[intRow] != 0)
						modDwelling.Statics.HoldGrade2 = Statics.intOIGradePct[intRow];
					if (Statics.intOICondition[intRow] != 0)
						modDwelling.Statics.HoldCondition = Statics.intOICondition[intRow];
				}
				// With frmNewValuationReport.OutBuildingGrid(intCurrentCard - 1)
				// .MergeCells = flexMergeSpill
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(Statics.intOIType[intRow]);
				// .TextMatrix(intRow, 0) = CR.Fields("cldesc")
				// .TextMatrix(intRow, 0) = CostRec.ClDesc
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Description[intRow] = modGlobalVariables.Statics.CostRec.ClDesc;
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].OutbuildingType[intRow] = Statics.intOIType[intRow];
				// If intOIYear(intRow) < 1600 Then
				// .TextMatrix(intRow, 1) = "OLD"
				// Else
				if (Statics.intOIYear[intRow] < 1500)
				{
				}
				else
				{
					// .TextMatrix(intRow, 1) = intOIYear(intRow)
					modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Year[intRow] = Statics.intOIYear[intRow];
				}
				// End If
				// If intOIUnits(intRow) = 9999 Then
				if (Statics.boolUseSoundValue[intRow])
				{
					// .TextMatrix(intRow, 2) = "    - - - - S O U N D  V A L U E - - - -    "
					modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Units[intRow] = "    - - - - S O U N D  V A L U E - - - -    ";
					goto REAS03_6101_TAG;
				}
				if (Statics.intOIType[intRow] > 699)
				{
					// .TextMatrix(intRow, 2) = Format(intOIUnits(intRow) \ 100, "#0") & "X" & Format(intOIUnits(intRow) - ((intOIUnits(intRow) \ 100) * 100), "#0")
					modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Units[intRow] = Strings.Format(Statics.intOIUnits[intRow] / 100, "#0") + "X" + Strings.Format(Statics.intOIUnits[intRow] - ((Statics.intOIUnits[intRow] / 100) * 100), "#0");
				}
				else
				{
					// .TextMatrix(intRow, 2) = intOIUnits(intRow)
					modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Units[intRow] = FCConvert.ToString(Statics.intOIUnits[intRow]);
				}
				if (Statics.intOIType[intRow] > 699)
				{
					dblGrade = REAS03_6210_MOHOS(ref intRow);
				}
				else
				{
					dblGrade = REAS03_6200_REGULAR_OUTBUILDINGS(ref intRow);
				}
				// .TextMatrix(intRow, 3) = Mid(CR.Fields("CSDesc"), 1, 2) & Format(dblGrade * 100, "###")
				if (Conversion.Val(modGlobalVariables.Statics.CostRec.CSDesc) == 0)
				{
					// If Val(CR.Fields("CSDesc")) = 0 Then
					// CalcOutList(CalcOutIndex).Grade(intRow) = Mid(CR.Fields("CSDesc"), 1, 2) & Format(dblGrade * 100, "###")
					strTemp = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2);
					if (Strings.Right(strTemp, 1) != " ")
						strTemp += " ";
					modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Grade[intRow] = strTemp + Strings.Format(dblGrade * 100, "###");
				}
				else
				{
					// strAry = Split(CR.Fields("Csdesc"), " ", , vbTextCompare)
					strAry = Strings.Split(modGlobalVariables.Statics.CostRec.CSDesc, " ", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(strAry, 1) > 0)
					{
						modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Grade[intRow] = Strings.Mid(strAry[1], 1, 2) + " " + Strings.Format(dblGrade * 100, "###");
					}
					else
					{
						// CalcOutList(CalcOutIndex).Grade(intRow) = Mid(CR.Fields("CSDesc"), 1, 2) & Format(dblGrade * 100, "###")
						modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Grade[intRow] = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + Strings.Format(dblGrade * 100, "###");
					}
				}
				// .TextMatrix(intRow, 4) = Format(lngRepCostNew(intRow), "###,####,##0")
				//FC:FINAL:MSH - incorrect string format for FCConvert.ToInt32
				//modSpeedCalc.CalcOutList[modSpeedCalc.CalcOutIndex].Recon[intRow] = FCConvert.ToInt32(Strings.Format(lngRepCostNew[intRow], "###,####,##0"));
				int tempRecon;
				if (Int32.TryParse(Strings.Format(Statics.lngRepCostNew[intRow], "###,####,##0"), NumberStyles.Number, CultureInfo.InvariantCulture, out tempRecon))
				{
					modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Recon[intRow] = tempRecon;
				}
				if ((Statics.intOICondition[intRow] == 9 || Statics.intOICondition[intRow] == 0) && (Strings.UCase(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"))) == "Y" || Strings.UCase(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode"))) == "C"))
				{
					modProperty.Statics.WKey = 1460 + modDwelling.Statics.HoldCondition;
				}
				else
				{
					if (Statics.intOICondition[intRow] > 0)
					{
						if (Statics.intOIType[intRow] > 699)
						{
							modProperty.Statics.WKey = 1790 + Statics.intOICondition[intRow];
						}
						else
						{
							modProperty.Statics.WKey = 1460 + Statics.intOICondition[intRow];
						}
					}
					else
					{
						if (modGlobalVariables.Statics.boolMobileHomePresent)
						{
							modProperty.Statics.WKey = 1460 + Statics.intOICondition[modGlobalVariables.Statics.intLastMobileHomeDone];
						}
						else if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") == "Y")
						{
							modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dicondition") + ""));
						}
						else if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") == "C")
						{
							modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition")));
						}
						if (modProperty.Statics.WKey == 1460)
						{
							if (Statics.intOIType[intRow] > 699)
							{
								modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1790);
								if (Conversion.Val(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
								{
									modProperty.Statics.WKey = 1790 + 4;
									MessageBox.Show("No condition given for outbuilding " + FCConvert.ToString(intRow) + " Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "." + "\r\n" + "Assigning average condition.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									modProperty.Statics.WKey = 1790;
								}
							}
							else
							{
								modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1460);
								if (Conversion.Val(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
								{
									modProperty.Statics.WKey = 1460 + 4;
									MessageBox.Show("No condition given for outbuilding " + FCConvert.ToString(intRow) + " Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "." + "\r\n" + "Assigning average condition.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									modProperty.Statics.WKey = 1460;
								}
							}
						}
					}
				}
				WKEYTemp = modProperty.Statics.WKey;
				// Call REAS03_4300_ECONOMIC_PERCENT
				modSpeedCalc.new_reas03_4300_economic_percent();
				modProperty.Statics.WKey = WKEYTemp;
				// Call OpenCRTable(CR, WKey)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				// .TextMatrix(intRow, 5) = Mid(CostRec.CSDesc, 1, 4)
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Condition[intRow] = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 4);
				// .TextMatrix(intRow, 6) = Format((dblPhysical(intRow) * 100) \ 1, "##0") & "%"
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Physcial[intRow] = Strings.Format(fecherFoundation.FCUtils.iDiv((Statics.dblPhysical[intRow] * 100), 1), "##0") + "%";
				// .TextMatrix(intRow, 7) = Format(dblFunctional(intRow) \ 1, "##0") & "%"
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Functional[intRow] = Strings.Format(fecherFoundation.FCUtils.iDiv(Statics.dblFunctional[intRow], 1), "##0") + "%";
				// .TextMatrix(intRow, 8) = Format((dblEconomicPercent * 100) \ 1, "##0") & "%"
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].Economic[intRow] = Strings.Format(fecherFoundation.FCUtils.iDiv((modDwelling.Statics.dblEconomicPercent * 100), 1), "##0") + "%";
				REAS03_6101_TAG:
				;
				// .TextMatrix(intRow, 9) = Format(lngRepCostNewLD(intRow), "#,##0")
				modSpeedCalc.Statics.CalcOutList[modSpeedCalc.Statics.CalcOutIndex].OutbuildingValue[intRow] = Strings.Format(Statics.lngRepCostNewLD[intRow], "#,##0");
				// End With
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Display Outbuildings", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public double dblPerVal;
			public int[] lngOutbuildingTotal = new int[999 + 1];
			public double[] dblPhysical = new double[10 + 1];
			// vbPorter upgrade warning: dblFunctional As double	OnWrite(double, string, int)
			public double[] dblFunctional = new double[10 + 1];
			public double OutEconomic;
			// vbPorter upgrade warning: lngRepCostNew As int	OnWrite(short, double)
			public int[] lngRepCostNew = new int[10 + 1];
			// vbPorter upgrade warning: lngRepCostNewLD As int	OnWrite(int, double)
			public int[] lngRepCostNewLD = new int[10 + 1];
			public bool bolMOHODone;
			public bool bolFirstOB;
			// vbPorter upgrade warning: intCY As short --> As int	OnWrite(int, double)
			public int intCY;
			public int intYearBuilt;
			public double BBPhysicalPercent;
			public double BBFunctionalPercent;
			public int BBHoldYear;
			public int BBHoldGrade1;
			public int BBHoldGrade2;
			public int BBHoldCondition;
			public short[] intOIType = new short[10 + 1];
			// vbPorter upgrade warning: intOIYear As short --> As int	OnRead(int, string, int)
			public int[] intOIYear = new int[10 + 1];
			// vbPorter upgrade warning: intOIUnits As int	OnRead(string)
			public int[] intOIUnits = new int[10 + 1];
			public int[] lngOISoundValue = new int[10 + 1];
			public bool[] boolUseSoundValue = new bool[10 + 1];
			public int[] intOIGradeCd = new int[10 + 1];
			public int[] intOIGradePct = new int[10 + 1];
			public int[] intOICondition = new int[10 + 1];
			public int[] intOIPctPhysical = new int[10 + 1];
			public int[] intOIPctFunctional = new int[10 + 1];
			public bool[] contributestosfla = new bool[10 + 1];
			public int HoldSfla;
			public int HoldBValue;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
