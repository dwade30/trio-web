﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage9.
	/// </summary>
	public partial class srptMVRPage9 : FCSectionReport
	{
        public srptMVRPage9()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage9(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }
		private void InitializeComponentEx()
		{
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMunicipality.Text = valuationReturn.Municipality;
			txtCounty.Text = valuationReturn.County;
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
			lblYear1.Text = (valuationReturn.ReportYear - 1).ToString();
			lblYear2.Text = lblYear1.Text;
			lblYear3.Text = lblYear1.Text;
			lblYear4.Text = lblYear1.Text;
            var lineIndex = 1;
            foreach (var industrialLine in valuationReturn.ValuationInformation.IndustrialMercantileGrowth)
            {
                switch (lineIndex)
                {
                    case 1:
                        txt2a.Text = industrialLine;
                        break;
                    case 2:
                        txt2b.Text = industrialLine;
                        break;
                    case 3:
                        txt2c.Text = industrialLine;
                        break;
                    case 4:
                        txt2d.Text = industrialLine;
                        break;
                    case 5:
                        txt2e.Text = industrialLine;
                        break;
                    case 6:
                        txt2f.Text = industrialLine;
                        break;
                    case 7:
                        txt2g.Text = industrialLine;
                        break;
                    case 8:
                        txt2h.Text = industrialLine;
                        break;
                }

                lineIndex++;                
            }

            lineIndex = 1;
            foreach (var extremeLoss in valuationReturn.ValuationInformation.ExtremeLosses)
            {
                switch (lineIndex)
                {
                    case 1:
                        txt3a.Text = extremeLoss;
                        break;
                    case 2:
                        txt3b.Text = extremeLoss;
                        break;
                    case 3:
                        txt3c.Text = extremeLoss;
                        break;
                    case 4:
                        txt3d.Text = extremeLoss;
                        break;
                    case 5:
                        txt3e.Text = extremeLoss;
                        break;
                    case 6:
                        txt3f.Text = extremeLoss;
                        break;
                    case 7:
                        txt3g.Text = extremeLoss;
                        break;
                    case 8:
                        txt3h.Text = extremeLoss;
                        break;
                }
                lineIndex++;
            }

            lineIndex = 1;
            foreach (var generalChange in valuationReturn.ValuationInformation.GeneralValuationChange)
            {
                switch (lineIndex)
                {
                    case 1:
                        txt4a.Text = generalChange;
                        break;
                    case 2:
                        txt4b.Text = generalChange;
                        break;
                    case 3:
                        txt4c.Text = generalChange;
                        break;
                    case 4:
                        txt4d.Text = generalChange;
                        break;
                    case 5:
                        txt4e.Text = generalChange;
                        break;
                    case 6:
                        txt4f.Text = generalChange;
                        break;
                    case 7:
                        txt4g.Text = generalChange;
                        break;
                    case 8:
                        txt4h.Text = generalChange;
                        break;
                    case 9:
                        txt4i.Text = generalChange;
                        break;
                    case 10:
                        txt4j.Text = generalChange;
                        break;
                }
                lineIndex++;
            }

            var netChanges = valuationReturn.ValuationInformation.ValuationNet;
            txtOneFamilyNew.Text = valuationReturn.ValuationInformation.New.OneFamily.FormatAsNumber();
            txtOneFamilyDemolished.Text = valuationReturn.ValuationInformation.Demolished.OneFamily.FormatAsNumber();
            txtOneFamilyConverted.Text = valuationReturn.ValuationInformation.Converted.OneFamily.FormatAsNumber();
            txtOneFamilyIncrease.Text = valuationReturn.ValuationInformation.ValuationIncrease.OneFamily.ToInteger()
                .FormatAsNumber();
            txtOneFamilyLoss.Text = valuationReturn.ValuationInformation.ValuationDecrease.OneFamily.ToInteger()
                .FormatAsNumber();
            txtOneFamilyNet.Text = netChanges.OneFamily.ToInteger().FormatAsNumber();

            txtTwoFamilyNew.Text = valuationReturn.ValuationInformation.New.TwoFamily.FormatAsNumber();
            txtTwoFamilyDemolished.Text = valuationReturn.ValuationInformation.Demolished.TwoFamily.FormatAsNumber();
            txtTwoFamilyConverted.Text = valuationReturn.ValuationInformation.Converted.TwoFamily.FormatAsNumber();
            txtTwoFamilyIncrease.Text = valuationReturn.ValuationInformation.ValuationIncrease.TwoFamily.ToInteger()
                .FormatAsNumber();
            txtTwoFamilyLoss.Text = valuationReturn.ValuationInformation.ValuationDecrease.TwoFamily.ToInteger()
                .FormatAsNumber();
            txtTwoFamilyNet.Text = netChanges.TwoFamily.ToInteger().FormatAsNumber();

            txtThreeFourFamilyNew.Text = valuationReturn.ValuationInformation.New.ThreeToFourFamily.FormatAsNumber();			
			txtThreeFourFamilyDemolished.Text = valuationReturn.ValuationInformation.Demolished.ThreeToFourFamily.FormatAsNumber();
			txtThreeFourFamilyConverted.Text = valuationReturn.ValuationInformation.Converted.ThreeToFourFamily.FormatAsNumber();
			txtThreeFourFamilyIncrease.Text = valuationReturn.ValuationInformation.ValuationIncrease.ThreeToFourFamily.ToInteger().FormatAsNumber();
			txtThreeFourFamilyLoss.Text = valuationReturn.ValuationInformation.ValuationDecrease.ThreeToFourFamily.ToInteger().FormatAsNumber();
            txtThreeFourFamilyNet.Text = netChanges.ThreeToFourFamily.ToInteger().FormatAsNumber();

            txtOverFourNew.Text = valuationReturn.ValuationInformation.New.FiveFamilyPlus.FormatAsNumber();
            txtOverFourDemolished.Text =
                valuationReturn.ValuationInformation.Demolished.FiveFamilyPlus.FormatAsNumber();
            txtOverFourConverted.Text = valuationReturn.ValuationInformation.Converted.FiveFamilyPlus.FormatAsNumber();
            txtOverFourIncrease.Text = valuationReturn.ValuationInformation.ValuationIncrease.FiveFamilyPlus.ToInteger()
                .FormatAsNumber();
            txtOverFourLoss.Text = valuationReturn.ValuationInformation.ValuationDecrease.FiveFamilyPlus.ToInteger()
                .FormatAsNumber();
            txtOverFourNet.Text = netChanges.FiveFamilyPlus.ToInteger().FormatAsNumber();

            txtMHNew.Text = valuationReturn.ValuationInformation.New.MobileHomes.FormatAsNumber();
            txtMHDemolished.Text = valuationReturn.ValuationInformation.Demolished.MobileHomes.FormatAsNumber();
			txtMHConverted.Text = valuationReturn.ValuationInformation.Converted.MobileHomes.FormatAsNumber();
			txtMHIncrease.Text = valuationReturn.ValuationInformation.ValuationIncrease.MobileHomes.ToInteger().FormatAsNumber();
            txtMHLoss.Text = valuationReturn.ValuationInformation.ValuationDecrease.MobileHomes.ToInteger()
                .FormatAsNumber();
            txtMHNet.Text = netChanges.MobileHomes.ToInteger().FormatAsNumber();

            txtSHNew.Text = valuationReturn.ValuationInformation.New.SeasonalHomes.FormatAsNumber();
            txtSHDemolished.Text = valuationReturn.ValuationInformation.Demolished.SeasonalHomes.FormatAsNumber();
            txtSHConverted.Text = valuationReturn.ValuationInformation.Converted.SeasonalHomes.FormatAsNumber();
			txtSHIncrease.Text = valuationReturn.ValuationInformation.ValuationIncrease.SeasonalHomes.ToInteger().FormatAsNumber();
			txtSHLoss.Text = valuationReturn.ValuationInformation.ValuationDecrease.SeasonalHomes.ToInteger().FormatAsNumber();
            txtSHNet.Text = netChanges.SeasonalHomes.ToInteger().FormatAsNumber();			
		}

		
	}
}
