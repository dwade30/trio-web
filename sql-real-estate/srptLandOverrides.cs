﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptLandOverrides.
	/// </summary>
	public partial class srptLandOverrides : FCSectionReport
	{
		public static srptLandOverrides InstancePtr
		{
			get
			{
				return (srptLandOverrides)Sys.GetInstance(typeof(srptLandOverrides));
			}
		}

		protected srptLandOverrides _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLandCode?.Dispose();
				clsLandOv?.Dispose();
                clsLandOv = null;
                clsLandCode = null;
            }
			base.Dispose(disposing);
		}

		public srptLandOverrides()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptLandOverrides	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLandCode = new clsDRWrapper();
		clsDRWrapper clsLandOv = new clsDRWrapper();
		int lngCode;
		int lngAmount;
		// vbPorter upgrade warning: lngaverage As int	OnWrite(double, short)
		int lngaverage;
		int lngCount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLandOv.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			// Call clsLandCode.OpenRecordset("select * from costrecord where crecordnumber between 1000 and 1009", strredatabase)
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "select rilandcode, sum(hLVALLAND) as thesum, count(rilandcode) as thecount from master where not rsdeleted = 1 and hivallandcode > 2 group by rilandcode order by rilandcode";
			}
			else
			{
				strSQL = "select rilandcode, sum(hLVALLAND) as thesum, count(rilandcode) as thecount from master where not rsdeleted = 1 and hivallandcode > 2 and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " group by rilandcode order by rilandcode";
			}
			clsLandCode.OpenRecordset("select * from tblLandCode order by code", modGlobalVariables.strREDatabase);
			clsLandOv.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!clsLandOv.EndOfFile())
				{
					lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLandOv.GetData("rilandcode"))));
					txtLandCode.Text = lngCode.ToString();
					if (lngCode == 0)
					{
						txtLandCode.Text = txtLandCode.Text + " Uncoded";
					}
					else
					{
						// Call clsLandCode.FindFirstRecord("crecordnumber", 1000 + lngCode)
						// Call clsLandCode.FindFirstRecord("code", lngCode)
						clsLandCode.FindFirst("code = " + FCConvert.ToString(lngCode));
						if (!clsLandCode.NoMatch)
						{
							// txtLandCode.Text = txtLandCode.Text & " " & clsLandCode.GetData("cldesc")
							txtLandCode.Text = txtLandCode.Text + " " + clsLandCode.Get_Fields_String("Description");
						}
						else
						{
							txtLandCode.Text = txtLandCode.Text + " Unknown";
						}
					}
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLandOv.GetData("thecount"))));
					lngAmount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLandOv.GetData("thesum"))));
					if (lngCount > 0)
					{
						lngaverage = FCConvert.ToInt32(FCConvert.ToDouble(lngAmount) / lngCount);
					}
					else
					{
						lngaverage = 0;
					}
				}
				else
				{
					lngCount = 0;
					lngAmount = 0;
					lngaverage = 0;
					txtLandCode.Text = "";
				}
				txtCount.Text = lngCount.ToString();
				txtAmount.Text = Strings.Format(lngAmount, "#,###,###,###,##0");
				txtAve.Text = Strings.Format(lngaverage, "###,###,###,##0");
				clsLandOv.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
