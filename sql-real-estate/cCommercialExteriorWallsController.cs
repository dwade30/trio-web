﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cCommExteriorWallsController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCommercialExteriorWall GetCommercialExteriorWall(int lngId)
		{
			cCommercialExteriorWall GetCommercialExteriorWall = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialExteriorWall extwall = null;
				rsLoad.OpenRecordset("select * from CommercialExteriorWalls where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					extwall = new cCommercialExteriorWall();
					// TODO Get_Fields: Field [FullDescription] not found!! (maybe it is an alias?)
					extwall.FullDescription = FCConvert.ToString(rsLoad.Get_Fields("FullDescription"));
					extwall.ShortDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					extwall.CodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields("CodeNumber"));
					extwall.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					extwall.IsUpdated = false;
				}
				GetCommercialExteriorWall = extwall;
				return GetCommercialExteriorWall;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialExteriorWall;
		}

		public cCommercialExteriorWall GetCommercialExteriorWallByCode(int lngCode)
		{
			cCommercialExteriorWall GetCommercialExteriorWallByCode = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialExteriorWall extwall = null;
				rsLoad.OpenRecordset("select * from CommercialExteriorWalls where codenumber = " + FCConvert.ToString(lngCode), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					extwall = new cCommercialExteriorWall();
					// TODO Get_Fields: Field [FullDescription] not found!! (maybe it is an alias?)
					extwall.FullDescription = FCConvert.ToString(rsLoad.Get_Fields("FullDescription"));
					extwall.ShortDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					extwall.CodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields("CodeNumber"));
					extwall.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					extwall.IsUpdated = false;
				}
				GetCommercialExteriorWallByCode = extwall;
				return GetCommercialExteriorWallByCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialExteriorWallByCode;
		}

		public cGenericCollection GetCommercialExteriorWalls()
		{
			cGenericCollection GetCommercialExteriorWalls = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialExteriorWall extwall;
				rs.OpenRecordset("select * from CommercialExteriorWalls order by FullDescription", "RealEstate");
				while (!rs.EndOfFile())
				{
					extwall = new cCommercialExteriorWall();
					// TODO Get_Fields: Field [FullDescription] not found!! (maybe it is an alias?)
					extwall.FullDescription = FCConvert.ToString(rs.Get_Fields("FullDescription"));
					extwall.ShortDescription = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					extwall.CodeNumber = FCConvert.ToInt32(rs.Get_Fields("CodeNumber"));
					extwall.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					extwall.IsUpdated = false;
					gColl.AddItem(extwall);
					rs.MoveNext();
				}
				GetCommercialExteriorWalls = gColl;
				return GetCommercialExteriorWalls;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialExteriorWalls;
		}

		public cGenericCollection GetCommercialExteriorWallsOrderByCodes()
		{
			cGenericCollection GetCommercialExteriorWallsOrderByCodes = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialExteriorWall extwall;
				rs.OpenRecordset("select * from CommercialExteriorWalls order by CodeNumber", "RealEstate");
				while (!rs.EndOfFile())
				{
					extwall = new cCommercialExteriorWall();
					// TODO Get_Fields: Field [FullDescription] not found!! (maybe it is an alias?)
					extwall.FullDescription = FCConvert.ToString(rs.Get_Fields("FullDescription"));
					extwall.ShortDescription = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					extwall.CodeNumber = FCConvert.ToInt32(rs.Get_Fields("CodeNumber"));
					extwall.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					extwall.IsUpdated = false;
					gColl.AddItem(extwall);
					rs.MoveNext();
				}
				GetCommercialExteriorWallsOrderByCodes = gColl;
				return GetCommercialExteriorWallsOrderByCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialExteriorWallsOrderByCodes;
		}

		public void DeleteCommercialExteriorWall(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialExteriorWalls where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialExteriorWall(ref cCommercialExteriorWall extwall)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (extwall.IsDeleted)
				{
					DeleteCommercialExteriorWall(extwall.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialexteriorwalls where id = " + extwall.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (extwall.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Exterior wall record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("FullDescription", extwall.FullDescription);
				rsSave.Set_Fields("ShortDescription", extwall.ShortDescription);
				rsSave.Set_Fields("CodeNumber", extwall.CodeNumber);
				rsSave.Update();
				extwall.ID = rsSave.Get_Fields_Int32("ID");
				extwall.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
