﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmZones.
	/// </summary>
	partial class frmZones : BaseForm
	{
		public FCGrid GridDeleteZone;
		public FCGrid GridZone;
		public FCGrid gridTownCode;
		public FCGrid GridNeigh;
		public FCGrid GridDeleteNeigh;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddNeighborhood;
		public fecherFoundation.FCToolStripMenuItem mnuAddZone;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrintZoneS;
		public fecherFoundation.FCToolStripMenuItem mnuPrintZoneSchedule;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmZones));
			this.GridDeleteZone = new fecherFoundation.FCGrid();
			this.GridZone = new fecherFoundation.FCGrid();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.GridNeigh = new fecherFoundation.FCGrid();
			this.GridDeleteNeigh = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddNeighborhood = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddZone = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintZoneS = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintZoneSchedule = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdAddNeighborhood = new fecherFoundation.FCButton();
			this.cmdAddZone = new fecherFoundation.FCButton();
			this.cmdPrintZoneS = new fecherFoundation.FCButton();
			this.cmdPrintZoneSchedule = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleteZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNeigh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleteNeigh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintZoneS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintZoneSchedule)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(770, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridDeleteZone);
			this.ClientArea.Controls.Add(this.GridZone);
			this.ClientArea.Controls.Add(this.gridTownCode);
			this.ClientArea.Controls.Add(this.GridNeigh);
			this.ClientArea.Controls.Add(this.GridDeleteNeigh);
			this.ClientArea.Size = new System.Drawing.Size(770, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintZoneSchedule);
			this.TopPanel.Controls.Add(this.cmdPrintZoneS);
			this.TopPanel.Controls.Add(this.cmdAddZone);
			this.TopPanel.Controls.Add(this.cmdAddNeighborhood);
			this.TopPanel.Size = new System.Drawing.Size(770, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintZoneSchedule, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintZoneS, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddZone, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddNeighborhood, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(68, 30);
			this.HeaderText.Text = "Zone";
			// 
			// GridDeleteZone
			// 
			this.GridDeleteZone.AllowSelection = false;
			this.GridDeleteZone.AllowUserToResizeColumns = false;
			this.GridDeleteZone.AllowUserToResizeRows = false;
			this.GridDeleteZone.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDeleteZone.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDeleteZone.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDeleteZone.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteZone.BackColorSel = System.Drawing.Color.Empty;
			this.GridDeleteZone.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDeleteZone.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDeleteZone.ColumnHeadersHeight = 30;
			this.GridDeleteZone.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDeleteZone.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDeleteZone.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDeleteZone.DragIcon = null;
			this.GridDeleteZone.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDeleteZone.FixedRows = 0;
			this.GridDeleteZone.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteZone.FrozenCols = 0;
			this.GridDeleteZone.GridColor = System.Drawing.Color.Empty;
			this.GridDeleteZone.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteZone.Location = new System.Drawing.Point(177, 0);
			this.GridDeleteZone.Name = "GridDeleteZone";
			this.GridDeleteZone.ReadOnly = true;
			this.GridDeleteZone.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDeleteZone.RowHeightMin = 0;
			this.GridDeleteZone.Rows = 0;
			this.GridDeleteZone.ScrollTipText = null;
			this.GridDeleteZone.ShowColumnVisibilityMenu = false;
			this.GridDeleteZone.Size = new System.Drawing.Size(80, 22);
			this.GridDeleteZone.StandardTab = true;
			this.GridDeleteZone.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDeleteZone.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDeleteZone.TabIndex = 1;
			this.GridDeleteZone.Visible = false;
			// 
			// GridZone
			// 
			this.GridZone.AllowSelection = false;
			this.GridZone.AllowUserToResizeColumns = false;
			this.GridZone.AllowUserToResizeRows = false;
			this.GridZone.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridZone.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridZone.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridZone.BackColorBkg = System.Drawing.Color.Empty;
			this.GridZone.BackColorFixed = System.Drawing.Color.Empty;
			this.GridZone.BackColorSel = System.Drawing.Color.Empty;
			this.GridZone.Cols = 7;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridZone.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridZone.ColumnHeadersHeight = 30;
			this.GridZone.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridZone.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridZone.DragIcon = null;
			this.GridZone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridZone.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridZone.ExtendLastCol = true;
			this.GridZone.FixedCols = 0;
			this.GridZone.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridZone.FrozenCols = 0;
			this.GridZone.GridColor = System.Drawing.Color.Empty;
			this.GridZone.GridColorFixed = System.Drawing.Color.Empty;
			this.GridZone.Location = new System.Drawing.Point(30, 30);
			this.GridZone.Name = "GridZone";
			this.GridZone.RowHeadersVisible = false;
			this.GridZone.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridZone.RowHeightMin = 0;
			this.GridZone.Rows = 1;
			this.GridZone.ScrollTipText = null;
			this.GridZone.ShowColumnVisibilityMenu = false;
			this.GridZone.Size = new System.Drawing.Size(710, 234);
			this.GridZone.StandardTab = true;
			this.GridZone.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridZone.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridZone.TabIndex = 3;
			this.GridZone.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridZone_ValidateEdit);
			this.GridZone.CurrentCellChanged += new System.EventHandler(this.GridZone_RowColChange);
			this.GridZone.KeyDown += new Wisej.Web.KeyEventHandler(this.GridZone_KeyDownEvent);
			// 
			// gridTownCode
			// 
			this.gridTownCode.AllowSelection = false;
			this.gridTownCode.AllowUserToResizeColumns = false;
			this.gridTownCode.AllowUserToResizeRows = false;
			this.gridTownCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridTownCode.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorBkg = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorSel = System.Drawing.Color.Empty;
			this.gridTownCode.Cols = 1;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridTownCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.gridTownCode.ColumnHeadersHeight = 30;
			this.gridTownCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridTownCode.ColumnHeadersVisible = false;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridTownCode.DefaultCellStyle = dataGridViewCellStyle6;
			this.gridTownCode.DragIcon = null;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.FrozenCols = 0;
			this.gridTownCode.GridColor = System.Drawing.Color.Empty;
			this.gridTownCode.GridColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.Location = new System.Drawing.Point(437, 0);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridTownCode.RowHeightMin = 0;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.ScrollTipText = null;
			this.gridTownCode.ShowColumnVisibilityMenu = false;
			this.gridTownCode.Size = new System.Drawing.Size(182, 22);
			this.gridTownCode.StandardTab = true;
			this.gridTownCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridTownCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridTownCode.TabIndex = 2;
			this.gridTownCode.Visible = false;
			this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
			// 
			// GridNeigh
			// 
			this.GridNeigh.AllowSelection = false;
			this.GridNeigh.AllowUserToResizeColumns = false;
			this.GridNeigh.AllowUserToResizeRows = false;
			this.GridNeigh.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridNeigh.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridNeigh.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridNeigh.BackColorBkg = System.Drawing.Color.Empty;
			this.GridNeigh.BackColorFixed = System.Drawing.Color.Empty;
			this.GridNeigh.BackColorSel = System.Drawing.Color.Empty;
			this.GridNeigh.Cols = 9;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridNeigh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.GridNeigh.ColumnHeadersHeight = 30;
			this.GridNeigh.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridNeigh.DefaultCellStyle = dataGridViewCellStyle8;
			this.GridNeigh.DragIcon = null;
			this.GridNeigh.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridNeigh.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridNeigh.ExtendLastCol = true;
			this.GridNeigh.FixedCols = 2;
			this.GridNeigh.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridNeigh.FrozenCols = 0;
			this.GridNeigh.GridColor = System.Drawing.Color.Empty;
			this.GridNeigh.GridColorFixed = System.Drawing.Color.Empty;
			this.GridNeigh.Location = new System.Drawing.Point(30, 278);
			this.GridNeigh.Name = "GridNeigh";
			this.GridNeigh.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridNeigh.RowHeightMin = 0;
			this.GridNeigh.Rows = 1;
			this.GridNeigh.ScrollTipText = null;
			this.GridNeigh.ShowColumnVisibilityMenu = false;
			this.GridNeigh.Size = new System.Drawing.Size(710, 210);
			this.GridNeigh.StandardTab = true;
			this.GridNeigh.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridNeigh.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridNeigh.TabIndex = 4;
			this.GridNeigh.ComboCloseUp += new System.EventHandler(this.GridNeigh_ComboCloseUp);
			this.GridNeigh.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridNeigh_ValidateEdit);
			this.GridNeigh.CurrentCellChanged += new System.EventHandler(this.GridNeigh_RowColChange);
			this.GridNeigh.KeyDown += new Wisej.Web.KeyEventHandler(this.GridNeigh_KeyDownEvent);
			// 
			// GridDeleteNeigh
			// 
			this.GridDeleteNeigh.AllowSelection = false;
			this.GridDeleteNeigh.AllowUserToResizeColumns = false;
			this.GridDeleteNeigh.AllowUserToResizeRows = false;
			this.GridDeleteNeigh.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDeleteNeigh.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.BackColorSel = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.Cols = 1;
			dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDeleteNeigh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.GridDeleteNeigh.ColumnHeadersHeight = 30;
			this.GridDeleteNeigh.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDeleteNeigh.ColumnHeadersVisible = false;
			dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDeleteNeigh.DefaultCellStyle = dataGridViewCellStyle10;
			this.GridDeleteNeigh.DragIcon = null;
			this.GridDeleteNeigh.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDeleteNeigh.FixedRows = 0;
			this.GridDeleteNeigh.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.FrozenCols = 0;
			this.GridDeleteNeigh.GridColor = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteNeigh.Location = new System.Drawing.Point(30, 0);
			this.GridDeleteNeigh.Name = "GridDeleteNeigh";
			this.GridDeleteNeigh.ReadOnly = true;
			this.GridDeleteNeigh.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDeleteNeigh.RowHeightMin = 0;
			this.GridDeleteNeigh.Rows = 0;
			this.GridDeleteNeigh.ScrollTipText = null;
			this.GridDeleteNeigh.ShowColumnVisibilityMenu = false;
			this.GridDeleteNeigh.Size = new System.Drawing.Size(80, 22);
			this.GridDeleteNeigh.StandardTab = true;
			this.GridDeleteNeigh.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDeleteNeigh.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDeleteNeigh.TabIndex = 0;
			this.GridDeleteNeigh.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddNeighborhood,
				this.mnuAddZone,
				this.mnuSepar2,
				this.mnuPrintZoneS,
				this.mnuPrintZoneSchedule,
				this.mnuSepar3,
				this.mnuSave,
				this.mnuSaveExit,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddNeighborhood
			// 
			this.mnuAddNeighborhood.Index = 0;
			this.mnuAddNeighborhood.Name = "mnuAddNeighborhood";
			this.mnuAddNeighborhood.Text = "Add Neighborhood";
			this.mnuAddNeighborhood.Click += new System.EventHandler(this.mnuAddNeighborhood_Click);
			// 
			// mnuAddZone
			// 
			this.mnuAddZone.Index = 1;
			this.mnuAddZone.Name = "mnuAddZone";
			this.mnuAddZone.Text = "Add Zone";
			this.mnuAddZone.Click += new System.EventHandler(this.mnuAddZone_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 2;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuPrintZoneS
			// 
			this.mnuPrintZoneS.Index = 3;
			this.mnuPrintZoneS.Name = "mnuPrintZoneS";
			this.mnuPrintZoneS.Text = "Print Zones";
			this.mnuPrintZoneS.Click += new System.EventHandler(this.mnuPrintZoneS_Click);
			// 
			// mnuPrintZoneSchedule
			// 
			this.mnuPrintZoneSchedule.Index = 4;
			this.mnuPrintZoneSchedule.Name = "mnuPrintZoneSchedule";
			this.mnuPrintZoneSchedule.Text = "Print Zone Schedule";
			this.mnuPrintZoneSchedule.Click += new System.EventHandler(this.mnuPrintZoneSchedule_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 5;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 6;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 7;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 8;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 9;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(339, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdAddNeighborhood
			// 
			this.cmdAddNeighborhood.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddNeighborhood.AppearanceKey = "toolbarButton";
			this.cmdAddNeighborhood.Location = new System.Drawing.Point(287, 29);
			this.cmdAddNeighborhood.Name = "cmdAddNeighborhood";
			this.cmdAddNeighborhood.Size = new System.Drawing.Size(136, 24);
			this.cmdAddNeighborhood.TabIndex = 1;
			this.cmdAddNeighborhood.Text = "Add Neighborhood";
			this.cmdAddNeighborhood.Click += new System.EventHandler(this.mnuAddNeighborhood_Click);
			// 
			// cmdAddZone
			// 
			this.cmdAddZone.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddZone.AppearanceKey = "toolbarButton";
			this.cmdAddZone.Location = new System.Drawing.Point(429, 29);
			this.cmdAddZone.Name = "cmdAddZone";
			this.cmdAddZone.Size = new System.Drawing.Size(76, 24);
			this.cmdAddZone.TabIndex = 2;
			this.cmdAddZone.Text = "Add Zone";
			this.cmdAddZone.Click += new System.EventHandler(this.mnuAddZone_Click);
			// 
			// cmdPrintZoneS
			// 
			this.cmdPrintZoneS.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintZoneS.AppearanceKey = "toolbarButton";
			this.cmdPrintZoneS.Location = new System.Drawing.Point(511, 29);
			this.cmdPrintZoneS.Name = "cmdPrintZoneS";
			this.cmdPrintZoneS.Size = new System.Drawing.Size(86, 24);
			this.cmdPrintZoneS.TabIndex = 3;
			this.cmdPrintZoneS.Text = "Print Zones";
			this.cmdPrintZoneS.Click += new System.EventHandler(this.mnuPrintZoneS_Click);
			// 
			// cmdPrintZoneSchedule
			// 
			this.cmdPrintZoneSchedule.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintZoneSchedule.AppearanceKey = "toolbarButton";
			this.cmdPrintZoneSchedule.Location = new System.Drawing.Point(603, 29);
			this.cmdPrintZoneSchedule.Name = "cmdPrintZoneSchedule";
			this.cmdPrintZoneSchedule.Size = new System.Drawing.Size(139, 24);
			this.cmdPrintZoneSchedule.TabIndex = 4;
			this.cmdPrintZoneSchedule.Text = "Print Zone Schedule";
			this.cmdPrintZoneSchedule.Click += new System.EventHandler(this.mnuPrintZoneSchedule_Click);
			// 
			// frmZones
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(770, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmZones";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Zone";
			this.Load += new System.EventHandler(this.frmZones_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmZones_KeyDown);
			this.Resize += new System.EventHandler(this.frmZones_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleteZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNeigh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleteNeigh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintZoneS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintZoneSchedule)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrintZoneSchedule;
		private FCButton cmdPrintZoneS;
		private FCButton cmdAddZone;
		private FCButton cmdAddNeighborhood;
	}
}
