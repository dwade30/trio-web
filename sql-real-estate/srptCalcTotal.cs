﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptCalcTotal.
	/// </summary>
	public partial class srptCalcTotal : FCSectionReport
	{
		public static srptCalcTotal InstancePtr
		{
			get
			{
				return (srptCalcTotal)Sys.GetInstance(typeof(srptCalcTotal));
			}
		}

		protected srptCalcTotal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptCalcTotal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptCalcTotal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intNumLines;
		int intLine;
		

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (intLine > intNumLines);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intNumLines = frmNewValuationReport.InstancePtr.TotalGrid.Rows - 2;
			intLine = 1;

		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intLine <= intNumLines)
			{
				txtCard.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 0);
				txtCLand.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 1);
				txtCBldg.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 2);
				txtCTotal.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 3);
				txtLand.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 4);
				txtBldg.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 5);
				txtTotal.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intLine, 6);
			}
			intLine += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCLandT.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(frmNewValuationReport.InstancePtr.TotalGrid.Rows - 1, 1);
			txtCBLDGT.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(frmNewValuationReport.InstancePtr.TotalGrid.Rows - 1, 2);
			txtCTT.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(frmNewValuationReport.InstancePtr.TotalGrid.Rows - 1, 3);
			txtLandT.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(frmNewValuationReport.InstancePtr.TotalGrid.Rows - 1, 4);
			txtBldgT.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(frmNewValuationReport.InstancePtr.TotalGrid.Rows - 1, 5);
			txtTT.Text = frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(frmNewValuationReport.InstancePtr.TotalGrid.Rows - 1, 6);
		}

		
	}
}
