﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTreeGrowth.
	/// </summary>
	public partial class frmTreeGrowth : BaseForm
	{
		public frmTreeGrowth()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTreeGrowth InstancePtr
		{
			get
			{
				return (frmTreeGrowth)Sys.GetInstance(typeof(frmTreeGrowth));
			}
		}

		protected frmTreeGrowth _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		double dblSoftCost;
		double dblMixedCost;
		double dblHardCost;

		private void frmTreeGrowth_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F6:
					{
						KeyCode = (Keys)0;
						mnuCalculate_Click();
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTreeGrowth_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			clsLoad.OpenRecordset("select * from defaults", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				dblSoftCost = Conversion.Val(clsLoad.Get_Fields_Double("softcost"));
				dblMixedCost = Conversion.Val(clsLoad.Get_Fields_Double("mixedcost"));
				dblHardCost = Conversion.Val(clsLoad.Get_Fields_Double("hardcost"));
			}
			if (modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth)
			{
				Label6.Visible = true;
				lblCertifiedRatio.Visible = true;
				lblCertifiedRatio.Text = Strings.Format(modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio * 100, "0.00");
			}
			else
			{
				lblCertifiedRatio.Visible = false;
				Label6.Visible = false;
			}
			lblSoftValue.Text = FCConvert.ToString(dblSoftCost);
			lblMixedValue.Text = FCConvert.ToString(dblMixedCost);
			lblHardValue.Text = FCConvert.ToString(dblHardCost);
		}

		private void frmTreeGrowth_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeTotalGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuCalculate_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsCalc = new clsDRWrapper();
			double dblPrvSoft = 0;
			int lngPrvSoftVal = 0;
			double dblPrvMixed = 0;
			int lngPrvMixedVal = 0;
			double dblPrvHard = 0;
			int lngPrvHardVal = 0;
			double dblNewSoftVal = 0;
			double dblNewMixedVal = 0;
			double dblNewHardVal = 0;
			// vbPorter upgrade warning: lngTotSoft As int	OnWrite(short, double)
			int lngTotSoft;
			// vbPorter upgrade warning: lngTotMixed As int	OnWrite(short, double)
			int lngTotMixed;
			// vbPorter upgrade warning: lngTotHard As int	OnWrite(short, double)
			int lngTotHard;
			int lngTotPrvSoft;
			int lngTotPrvMixed;
			int lngTotPrvHard;
			int lngCAccount;
			double dblFactor;
			try
			{
				// On Error GoTo ErrorHandler
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				SetupGrid();
				SetupTotalGrid();
				cmbOption.Visible = false;
				lblOption.Visible = false;
				framGrid.Visible = true;

				clsCalc.OpenRecordset("Select * from master where not rsdeleted = 1 and ((cast (rssoft as int)> 0) or (cast(rsmixed as int) > 0) or (cast(rshard as int) > 0)) order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				lngTotSoft = 0;
				lngTotMixed = 0;
				lngTotHard = 0;
				lngTotPrvSoft = 0;
				lngTotPrvMixed = 0;
				lngTotPrvHard = 0;
				dblFactor = 1;
				if (modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth)
				{
					dblFactor = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
				}
				if (!clsCalc.EndOfFile())
				{
					while (!clsCalc.EndOfFile())
					{
						dblPrvSoft = Conversion.Val(clsCalc.Get_Fields_Double("RSsoft"));
						lngPrvSoftVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCalc.Get_Fields_Int32("rssoftvalue"))));
						dblPrvMixed = Conversion.Val(clsCalc.Get_Fields_Double("rsmixed"));
						lngPrvMixedVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCalc.Get_Fields_Int32("rsmixedvalue"))));
						dblPrvHard = Conversion.Val(clsCalc.Get_Fields_Double("rshard"));
						lngPrvHardVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCalc.Get_Fields_Int32("rshardvalue"))));
						dblNewSoftVal = dblPrvSoft * dblSoftCost * dblFactor;
						dblNewSoftVal = Conversion.Int(dblNewSoftVal + 0.5);
						dblNewMixedVal = dblPrvMixed * dblMixedCost * dblFactor;
						dblNewMixedVal = Conversion.Int(dblNewMixedVal + 0.5);
						dblNewHardVal = dblPrvHard * dblHardCost * dblFactor;
						dblNewHardVal = Conversion.Int(dblNewHardVal + 0.5);
						lngTotPrvSoft += lngPrvSoftVal;
						lngTotPrvMixed += lngPrvMixedVal;
						lngTotPrvHard += lngPrvHardVal;
						lngTotSoft += FCConvert.ToInt32(dblNewSoftVal);
						lngTotMixed += FCConvert.ToInt32(dblNewMixedVal);
						lngTotHard += FCConvert.ToInt32(dblNewHardVal);
						clsCalc.Edit();
						clsCalc.Set_Fields("rssoftvalue", dblNewSoftVal);
						clsCalc.Set_Fields("rsmixedvalue", dblNewMixedVal);
						clsCalc.Set_Fields("rshardvalue", dblNewHardVal);
						clsCalc.Set_Fields("lastlandval", modGlobalRoutines.RERound_2(FCConvert.ToInt32(dblNewSoftVal + dblNewMixedVal + dblNewHardVal + FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsCalc.Get_Fields_Int32("rsothervalue")))))));
						clsCalc.Set_Fields("piacres", dblPrvSoft + dblPrvMixed + dblPrvHard + Conversion.Val(clsCalc.Get_Fields_Double("rsother")));
						clsCalc.Update();
						if (cmbOption.Text == "Detailed report with summary totals")
						{
							Grid.AddItem(clsCalc.Get_Fields_Int32("rsaccount") + "\t" + clsCalc.Get_Fields_String("deedname1") + "\t" + Strings.Format(lngPrvSoftVal, "#,###,##0") + "\t" + Strings.Format(lngPrvMixedVal, "#,###,##0") + "\t" + Strings.Format(lngPrvHardVal, "#,###,##0") + "\t" + Strings.Format(dblNewSoftVal, "#,###,##0") + "\t" + Strings.Format(dblNewMixedVal, "#,###,##0") + "\t" + Strings.Format(dblNewHardVal, "#,###,##0"));
						}
						clsCalc.MoveNext();
					}
					TotalGrid.TextMatrix(1, 4, Strings.Format(lngTotSoft, "#,###,##0"));
					TotalGrid.TextMatrix(1, 5, Strings.Format(lngTotMixed, "#,###,##0"));
					TotalGrid.TextMatrix(1, 6, Strings.Format(lngTotHard, "#,###,##0"));
					TotalGrid.TextMatrix(1, 1, Strings.Format(lngTotPrvSoft, "#,###,##0"));
					TotalGrid.TextMatrix(1, 2, Strings.Format(lngTotPrvMixed, "#,###,##0"));
					TotalGrid.TextMatrix(1, 3, Strings.Format(lngTotPrvHard, "#,###,##0"));
					mnuPrint.Enabled = true;
					cmdPrint.Visible = true;
				}
				else
				{
					MessageBox.Show("You have no accounts to calculate.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Calculate.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuCalculate_Click()
		{
			mnuCalculate_Click(mnuCalculate, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupTotalGrid()
		{
			TotalGrid.ExtendLastCol = true;
			TotalGrid.Cols = 7;
			TotalGrid.TextMatrix(1, 0, "Total");
			TotalGrid.TextMatrix(0, 1, "Prev Soft");
			TotalGrid.TextMatrix(1, 1, "0");
			TotalGrid.TextMatrix(0, 2, "Prev Mixed");
			TotalGrid.TextMatrix(1, 2, "0");
			TotalGrid.TextMatrix(0, 3, "Prev Hard");
			TotalGrid.TextMatrix(1, 3, "0");
			TotalGrid.TextMatrix(0, 4, "New Soft");
			TotalGrid.TextMatrix(1, 4, "0");
			TotalGrid.TextMatrix(0, 5, "New Mixed");
			TotalGrid.TextMatrix(1, 5, "0");
			TotalGrid.TextMatrix(0, 6, "New Hard");
			TotalGrid.TextMatrix(1, 6, "0");
		}

		private void ResizeTotalGrid()
		{
			int GridWidth = 0;
			GridWidth = TotalGrid.WidthOriginal;
			TotalGrid.ColWidth(0, FCConvert.ToInt32(GridWidth / 7.0));
			TotalGrid.ColWidth(1, FCConvert.ToInt32(GridWidth / 7.0));
			TotalGrid.ColWidth(2, FCConvert.ToInt32(GridWidth / 7.0));
			TotalGrid.ColWidth(3, FCConvert.ToInt32(GridWidth / 7.0));
			TotalGrid.ColWidth(4, FCConvert.ToInt32(GridWidth / 7.0));
			TotalGrid.ColWidth(5, FCConvert.ToInt32(GridWidth / 7.0));
		}

		private void SetupGrid()
		{
			Grid.ExtendLastCol = true;
			// If optOption(0) Then
			// detail and summary totals
			Grid.Cols = 8;
			Grid.Rows = 2;
			Grid.FixedCols = 0;
			Grid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			Grid.MergeRow(0, true);
			//FC:FINAL:MSH - i.issue #1388: change FixedRows value after cells alignment for correct alignment
			Grid.FixedRows = 2;
			//FC:FINAL:MSH - i.issue #1388: change Rows value after initializing Fixed Cells for correct calculation RowCount
			Grid.Rows = 2;
			Grid.TextMatrix(1, 0, "Account");
			Grid.TextMatrix(1, 1, "Name");
			//FC:FINAL:MSH - i.issue #1388: add text to central column(as in original)
			//Grid.TextMatrix(0, 2, "Previous");
			Grid.TextMatrix(0, 3, "Previous");
			//FC:FINAL:AM:#i1373 - merge the titles
			//Grid.TextMatrix(0, 3, "Previous");
			//Grid.TextMatrix(0, 4, "Previous");
			Grid.TextMatrix(1, 2, "Soft");
			Grid.TextMatrix(1, 3, "Mixed");
			Grid.TextMatrix(1, 4, "Hard");
			//FC:FINAL:MSH - i.issue #1388: add text to central column(as in original)
			//Grid.TextMatrix(0, 5, "New");
			Grid.TextMatrix(0, 6, "New");
			//FC:FINAL:AM:#i1373 - merge the titles
			//Grid.TextMatrix(0, 6, "New");
			//Grid.TextMatrix(0, 7, "New");
			Grid.TextMatrix(1, 5, "Soft");
			Grid.TextMatrix(1, 6, "Mixed");
			Grid.TextMatrix(1, 7, "Hard");
			// Else
			// just summary totals
			// End If
			ResizeGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			// If optOption(0) Then
			Grid.ColWidth(0, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.33 * GridWidth));
			Grid.ColWidth(2, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(3, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(4, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(5, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(6, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(7, FCConvert.ToInt32(0.09 * GridWidth));
			// Else
			// End If
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rptTreeGrowthCalc.Show , MDIParent
			frmReportViewer.InstancePtr.Init(rptTreeGrowthCalc.InstancePtr);
		}

		private void cmdCalculate_Click(object sender, EventArgs e)
		{
			mnuCalculate_Click();
		}
	}
}
