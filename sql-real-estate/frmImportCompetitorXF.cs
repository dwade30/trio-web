﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	public partial class frmImportCompetitorXF : BaseForm
	{
		public frmImportCompetitorXF()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmImportCompetitorXF InstancePtr
		{
			get
			{
				return (frmImportCompetitorXF)Sys.GetInstance(typeof(frmImportCompetitorXF));
			}
		}

		protected frmImportCompetitorXF _InstancePtr = null;
		//=========================================================
		const int CNSTHOMESTEADCODE = 1;
		const int CNSTVISIONINDEX = 1;
		const int CNSTGRIDGOOD = 0;
		const int CNSTGRIDNEW = 1;
		const int CNSTGRIDUNTOUCHED = 2;
		const int CNSTGRIDBAD = 3;
		const int CNSTGRIDNONE = -1;
		const int CNSTCMBUPDATEAMOUNTS = 0;
		const int CNSTCMBUPDATEINFOONLY = 1;
		double lngCurrentLand;
		double lngCurrentBldg;
		double lngCurrentExempt;
		double lngCurrentAssess;
		bool boolXferAmounts;
		bool boolXferInfoOnly;
		int lngtotex;
		// vbPorter upgrade warning: arParties As object()	OnWriteFCConvert.ToInt32(
		private object[] arParties = null;
		cPartyUtil tPU;
		clsDRWrapper rsExempts = new clsDRWrapper();

		private void chkFlagChanges_CheckedChanged(object sender, System.EventArgs e)
		{
			UpdatePercChanges();
		}

		private void UpdatePercChanges()
		{
			int lngRow;
			// vbPorter upgrade warning: dblPerc As double	OnWrite(string, double)
			double dblPerc = 0;
			int intExceed = 0;
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				// go through and highlight rows that fall within the limits
				if (GridGood.Rows > 1)
				{
					GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, GridGood.Rows - 1, GridGood.Cols - 1, Color.White);
					for (lngRow = 1; lngRow <= GridGood.Rows - 1; lngRow++)
					{
						if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALLAND)) != Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE)))
						{
							if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALLAND)) == 0)
							{
								GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDEXCEED, FCConvert.ToString(1));
							}
							else if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE)) == 0)
							{
								GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDEXCEED, FCConvert.ToString(-1));
							}
							else
							{
								dblPerc = FCConvert.ToDouble(Strings.Format(Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE)) / Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALLAND)), "0.00"));
								if (dblPerc > 1)
								{
									dblPerc -= 1;
									intExceed = 1;
								}
								else
								{
									dblPerc = 1 - dblPerc;
									intExceed = -1;
								}
								dblPerc *= 100;
								if (dblPerc > Conversion.Val(txtPercentChg.Text))
								{
									GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDEXCEED, FCConvert.ToString(intExceed));
									GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								}
								else
								{
									GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDEXCEED, FCConvert.ToString(0));
								}
							}
						}
						else
						{
							GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDEXCEED, FCConvert.ToString(0));
						}
						if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALBLDG)) != Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE)))
						{
							if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALBLDG)) == 0)
							{
								GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGEXCEED, FCConvert.ToString(1));
							}
							else if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE)) == 0)
							{
								GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGEXCEED, FCConvert.ToString(-1));
							}
							else
							{
								dblPerc = FCConvert.ToDouble(Strings.Format(Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE)) / Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALBLDG)), "0.00"));
								if (dblPerc > 1)
								{
									dblPerc -= 1;
									intExceed = 1;
								}
								else
								{
									dblPerc = 1 - dblPerc;
									intExceed = -1;
								}
								dblPerc *= 100;
								if (dblPerc > Conversion.Val(txtPercentChg.Text))
								{
									GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
									GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGEXCEED, FCConvert.ToString(intExceed));
								}
								else
								{
									GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGEXCEED, FCConvert.ToString(0));
								}
							}
						}
						else
						{
							GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGEXCEED, FCConvert.ToString(0));
						}
					}
					// lngRow
				}
			}
			else
			{
				if (GridGood.Rows > 1)
				{
					GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, GridGood.Rows - 1, GridGood.Cols - 1, Color.White);
				}
			}
		}

		private void chkShowAddedOnly_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			if (chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 1; x <= (GridNew.Rows - 1); x++)
				{
					if (FCConvert.CBool(GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW)))
					{
						GridNew.RowHidden(x, false);
					}
					else
					{
						GridNew.RowHidden(x, true);
					}
				}
				// x
			}
			else
			{
				for (x = 1; x <= (GridNew.Rows - 1); x++)
				{
					GridNew.RowHidden(x, false);
				}
				// x
			}
		}

		private void chkShowChangesOnly_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngLand = 0;
			int lngOldLand = 0;
			int lngBldg = 0;
			int lngOldBldg = 0;
			// vbPorter upgrade warning: lngExempt As int	OnWriteFCConvert.ToDouble(
			int lngExempt = 0;
			int lngOldExempt = 0;
			if (chkShowChangesOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 1; x <= (GridGood.Rows - 1); x++)
				{
					lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
					lngOldLand = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTORIGINALLAND))));
					lngBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE))));
					lngOldBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTORIGINALBLDG))));
					lngExempt = FCConvert.ToInt32(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3)));
					lngOldExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTORIGINALEXEMPTION))));
					if ((lngLand == lngOldLand) && (lngBldg == lngOldBldg) && (lngExempt == lngOldExempt))
					{
						GridGood.RowHidden(x, true);
					}
					else
					{
						GridGood.RowHidden(x, false);
					}
				}
				// x
			}
			else
			{
				for (x = 1; x <= (GridGood.Rows - 1); x++)
				{
					GridGood.RowHidden(x, false);
				}
				// x
			}
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridNew.Rows - 1); x++)
			{
				GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(true));
				GridNew.RowHidden(x, false);
			}
			// x
		}

		private void cmdUnselect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridNew.Rows - 1); x++)
			{
				GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(false));
				if (chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
				{
					GridNew.RowHidden(x, true);
				}
			}
			// x
		}

		private void frmImportCompetitor_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportCompetitor_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportCompetitor properties;
			//frmImportCompetitor.ScaleWidth	= 10725;
			//frmImportCompetitor.ScaleHeight	= 8235;
			//frmImportCompetitor.LinkTopic	= "Form1";
			//frmImportCompetitor.LockControls	= -1  'True;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lngtotex = 0;
				tPU = new cPartyUtil();
				// tPU.ConnectionString = clsLoad.ConnectionInformation("CentralParties")
				SetupcmbUpdateType();
				cmbUpdateType.SelectedIndex = 0;
				// default to update everything
				Shape1.FillColor(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT));
				clsLoad.OpenRecordset("select sum(lastlandval) as totland,sum(lastbldgval) as totbldg,sum(rlexemption) as totexempt from master where not rsdeleted = 1 ", "twre0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					lngCurrentLand = Conversion.Val(clsLoad.Get_Fields("totland"));
					lngCurrentBldg = Conversion.Val(clsLoad.Get_Fields("totbldg"));
					lngCurrentExempt = Conversion.Val(clsLoad.Get_Fields("totexempt"));
					lngCurrentAssess = lngCurrentLand + lngCurrentBldg - lngCurrentExempt;
				}
				else
				{
					lngCurrentLand = 0;
					lngCurrentBldg = 0;
					lngCurrentExempt = 0;
					lngCurrentAssess = 0;
				}
				lblCurLand.Text = Strings.Format(lngCurrentLand, "#,###,###,##0");
				lblCurBldg.Text = Strings.Format(lngCurrentBldg, "#,###,###,##0");
				lblCurExempt.Text = Strings.Format(lngCurrentExempt, "#,###,###,##0");
				lblCurAssess.Text = Strings.Format(lngCurrentAssess, "#,###,###,##0");
				SetupCmbImportType();
				cmbImportType.SelectedIndex = 0;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupCmbImportType()
		{
			cmbImportType.Clear();
			cmbImportType.AddItem("Vision");
			cmbImportType.ItemData(cmbImportType.NewIndex, CNSTVISIONINDEX);
		}

		private void SetupcmbUpdateType()
		{
			cmbUpdateType.Clear();
			cmbUpdateType.AddItem("Update Amounts");
			cmbUpdateType.ItemData(cmbUpdateType.NewIndex, CNSTCMBUPDATEAMOUNTS);
			cmbUpdateType.AddItem("Update Information Only");
			cmbUpdateType.ItemData(cmbUpdateType.NewIndex, CNSTCMBUPDATEINFOONLY);
		}

		private void frmImportCompetitor_Resize(object sender, System.EventArgs e)
		{
			GridBadAccounts.AutoSize(0, GridBadAccounts.Cols - 1);
			GridGood.AutoSize(0, GridGood.Cols - 1);
			GridNew.AutoSize(0, GridNew.Cols - 1);
			GridNotUpdated.AutoSize(0, GridNotUpdated.Cols - 1);
		}

		private void GridNew_RowColChange(object sender, System.EventArgs e)
		{
			switch (GridNew.Col)
			{
				case modGridConstantsXF.CNSTUSEROW:
					{
						GridNew.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						GridNew.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void GridNew_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridNew.Col == modGridConstantsXF.CNSTUSEROW)
			{
				if (chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (FCConvert.CBool(GridNew.TextMatrix(GridNew.Row, modGridConstantsXF.CNSTUSEROW)))
					{
						// use the opposite since the textmatrix is what it used to be
						// edit text is messed up
						GridNew.RowHidden(GridNew.Row, true);
					}
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuLoad_Click(object sender, System.EventArgs e)
		{
			if (cmbImportType.Items.Count < 1 || cmbImportType.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a type of import before proceeding", "Type Not Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// initialize and clear grids
			SetupGridLoad();
			SetupGridGood();
			SetupGridBadAccounts();
			SetupGridNew();
			SetupGridNotUpdated();
			rsExempts.OpenRecordset("select * from exemptcode order by code", "RealEstate");
			FCUtils.StartTask(this, () =>
			{
				switch (cmbImportType.ItemData(cmbImportType.SelectedIndex))
				{
					case CNSTVISIONINDEX:
						{
							ImportVision();
							break;
						}
				}
				//end switch
				UpdatePercChanges();
				GridBadAccounts.AutoSize(0, GridBadAccounts.Cols - 1);
				GridGood.AutoSize(0, GridGood.Cols - 1);
				GridNew.AutoSize(0, GridNew.Cols - 1);
				GridNotUpdated.AutoSize(0, GridNotUpdated.Cols - 1);
				this.EndWait();
			});
		}

		private void ImportVision()
		{
			// imports the files created by vision appraisal
			// fills gridload with each record then examines each record to see which grid to place it in
			string strSrcFile;
			StreamReader ts;
			string strReturn = "";
			// holds string returned by read routine
			int VisionRecordSize;
			modREFileFormatsXF.VisionAccountInfo AccountInfo = new modREFileFormatsXF.VisionAccountInfo(1);
			modREFileFormatsXF.VisionOwnerRecord OwnerRec = new modREFileFormatsXF.VisionOwnerRecord(1);
			modREFileFormatsXF.VisionSupplemental SuppRec = new modREFileFormatsXF.VisionSupplemental(1);
			modREFileFormatsXF.VisionSaleHistory SaleRec = new modREFileFormatsXF.VisionSaleHistory(1);
			modREFileFormatsXF.VisionCurrentMultiOwner MultiOwnerRec = new modREFileFormatsXF.VisionCurrentMultiOwner(1);
			modREFileFormatsXF.ConvertedData REInfo = new modREFileFormatsXF.ConvertedData(1);
			modREFileFormatsXF.VisionExemptions[] Exempts = new modREFileFormatsXF.VisionExemptions[8 + 1];
			for (int i = 0; i < Exempts.Length; i++)
			{
				Exempts[i] = new modREFileFormatsXF.VisionExemptions(1);
			}
			modREFileFormatsXF.VisionOtherAssessments OtherAss = new modREFileFormatsXF.VisionOtherAssessments(1);
			modREFileFormatsXF.VisionAssessedValues AssessedRec = new modREFileFormatsXF.VisionAssessedValues(1);
			modREFileFormatsXF.VisionAdditionalParcelInfo ParcelInfoRec = new modREFileFormatsXF.VisionAdditionalParcelInfo(1);
			int x;
			int intNumExempts = 0;
			// vbPorter upgrade warning: intExemptCode As int	OnRead(string)
			int intExemptCode = 0;
			int lngTotalValLeft = 0;
			// vbPorter upgrade warning: lngCurExempt As int	OnWrite(int, double)
			int lngCurExempt = 0;
			int lngCRecord;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)
			int lngTemp;
			FileInfo fl;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// first let's find the file
				gridMultiOwner.Rows = 0;
				gridPreviousOwners.Rows = 0;
				// CommonDialog1.Flags = vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				CommonDialog1.InitDir = Environment.CurrentDirectory;
				CommonDialog1.ShowOpen();
				strSrcFile = CommonDialog1.FileName;
				this.ShowWait();
				if (!FCFileSystem.FileExists(strSrcFile))
				{
					MessageBox.Show("File " + strSrcFile + " cannot be found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.EndWait();
					return;
				}
				this.Refresh();
				//App.DoEvents();
				//frmWait.InstancePtr.Init("Loading Data", boolShowCounter: true);
				lngCRecord = 0;
				FillGridLoad();
				// records are 2353 in size
				VisionRecordSize = 2353;
				// in bytes
				if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUseMultiOwner)
				{
					VisionRecordSize = 2356;
					// 3 extra bytes for the percentage ownership
				}
				// legit file should be a multiple of that (may have to account for crlf though
				// will have to test to see
				fl = new FileInfo(strSrcFile);
				lngTemp = FCConvert.ToInt32(fl.Length);
				if (!modGlobalVariablesXF.Statics.CustomizedInfo.boolUsesCRLF)
				{
					lngTemp = lngTemp % (VisionRecordSize);
				}
				else
				{
					lngTemp = lngTemp % (VisionRecordSize + 2);
				}
				fl = null;
				ts = FCFileSystem.OpenText(strSrcFile);//, IOMode.ForReading, false, Tristate.TristateUseDefault);
				char[] buffer = new char[lngTemp];
				if (lngTemp != 0 && modGlobalVariablesXF.Statics.CustomizedInfo.intExtraChars == 0)
				{
					//strReturn = ts.Read(strReturn.ToCharArray(), 0, lngTemp);
					ts.Read(buffer, 0, lngTemp);
					strReturn = new string(buffer);
				}
				if (modGlobalVariablesXF.Statics.CustomizedInfo.intBlankRecords > 0)
				{
					if (!modGlobalVariablesXF.Statics.CustomizedInfo.boolUsesCRLF)
					{
						//strReturn = ts.Read (modGlobalVariables.Statics.CustomizedInfo.intBlankRecords * VisionRecordSize);
						ts.Read(buffer, 0, modGlobalVariablesXF.Statics.CustomizedInfo.intBlankRecords * VisionRecordSize);
						strReturn = new string(buffer);
						// skip the first record since it is garbage
					}
					else
					{
						strReturn = ts.ReadLine();
					}
				}
				string strGuid = "";
				cCreateGUID tGuid = new cCreateGUID();
				int exemptsPosition = Strings.Len(Exempts[0]);
				while (!ts.EndOfStream)
				{
					lngCRecord += 1;
					strGuid = tGuid.CreateGUID();
					//frmWait.InstancePtr.lblMessage.Text = "Loading Record " + FCConvert.ToString(lngCRecord);
					this.UpdateWait("Loading Record " + FCConvert.ToString(lngCRecord));
					//App.DoEvents();
					ClearRecord(ref REInfo);
					REInfo.GUID = strGuid;
					//strReturn = ts.Read(Marshal.SizeOf(AccountInfo));
					buffer = new char[Strings.Len(AccountInfo)];
					ts.Read(buffer, 0, Strings.Len(AccountInfo));
					strReturn = new string(buffer);
					GetAccountInfo(ref strReturn, ref REInfo);
					//strReturn = ts.Read(Marshal.SizeOf(OwnerRec));
					buffer = new char[Strings.Len(OwnerRec)];
					ts.Read(buffer, 0, Strings.Len(OwnerRec));
					strReturn = new string(buffer);
					GetOwnerInfo(ref strReturn, ref REInfo);
					//strReturn = ts.Read(Marshal.SizeOf(SuppRec));
					buffer = new char[Strings.Len(SuppRec)];
					ts.Read(buffer, 0, Strings.Len(SuppRec));
					strReturn = new string(buffer);
					// not used, just read it and throw away
					if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LEBANON")
					{
						strTemp = Strings.Left(strReturn, 10);
						strTemp = fecherFoundation.Strings.Trim(strTemp);
						strTemp = Strings.Right("0000000000" + strTemp, 10);
						if (fecherFoundation.Strings.Trim(Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare)) == string.Empty)
						{
							REInfo.PhoneNumber = "";
						}
						else
						{
							REInfo.PhoneNumber = strTemp;
						}
					}
					if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "YORK")
					{
						// the maplot is in user3
						strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strReturn, 61, 30));
						REInfo.MapLot = strTemp;
					}
					// 6 sale histories
					//strReturn = ts.Read(Marshal.SizeOf(SaleRec));
					buffer = new char[Strings.Len(SaleRec)];
					ts.Read(buffer, 0, Strings.Len(SaleRec));
					strReturn = new string(buffer);
					GetSaleInfo(ref strReturn, ref REInfo, true);
					for (x = 1; x <= 5; x++)
					{
						//App.DoEvents();
						// ignore the rest
						//strReturn = ts.Read(Marshal.SizeOf(SaleRec));
						buffer = new char[Strings.Len(SaleRec)];
						ts.Read(buffer, 0, Strings.Len(SaleRec));
						strReturn = new string(buffer);
						GetSaleInfo(ref strReturn, ref REInfo, false);
					}
					// x
					// FOR KENNebunkport the assessments are in other assessments
					// read the exempt records but process later, after we know what the assessment is
					for (x = 1; x <= 8; x++)
					{
						//App.DoEvents();
						//strReturn = ts.Read(Marshal.SizeOf(Exempts[0]));
						buffer = new char[exemptsPosition];
						ts.Read(buffer, 0, exemptsPosition);
						strReturn = new string(buffer);
						// If UCase(MuniName) <> "KENNEBUNKPORT" Then
						GetExemptInfo(ref strReturn, ref Exempts[x - 1]);
						// Else
						// Exempts(x - 1).Amount = ""
						// Exempts(x - 1).Code = ""
						// Exempts(x - 1).Year = ""
						// End If
					}
					// x
					// other assessments.  Pretty much ignore
					// there are 4 of them
					for (x = 1; x <= 4; x++)
					{
						//App.DoEvents();
						//strReturn = ts.Read(Marshal.SizeOf(OtherAss));
						buffer = new char[Strings.Len(OtherAss)];
						ts.Read(buffer, 0, Strings.Len(OtherAss));
						strReturn = new string(buffer);
						// If UCase(MuniName) <> "KENNEBUNKPORT" Then
						// Call GetOtherAmountsInfo(strReturn, REInfo)
						// Else
						GetOtherAmountsInfo(ref strReturn, ref REInfo, ref Exempts[x - 1]);
						// End If
					}
					// x
					// 12 occurences of assessed values
					for (x = 1; x <= 12; x++)
					{
						//App.DoEvents();
						//strReturn = ts.Read(Marshal.SizeOf(AssessedRec));
						buffer = new char[Strings.Len(AssessedRec)];
						ts.Read(buffer, 0, Strings.Len(AssessedRec));
						strReturn = new string(buffer);
						GetAssessmentInfo(ref strReturn, ref REInfo, ref Exempts[0]);
					}
					// x
					//strReturn = ts.Read(Marshal.SizeOf(ParcelInfoRec));
					buffer = new char[Strings.Len(ParcelInfoRec)];
					ts.Read(buffer, 0, Strings.Len(ParcelInfoRec));
					strReturn = new string(buffer);
					GetAdditionalInfo(ref strReturn, ref REInfo);
					// current owner
					if (!modGlobalVariablesXF.Statics.CustomizedInfo.boolUseMultiOwner)
					{
						//strReturn = ts.Read(Marshal.SizeOf(OwnerRec));
						buffer = new char[Strings.Len(OwnerRec)];
						ts.Read(buffer, 0, Strings.Len(OwnerRec));
						strReturn = new string(buffer);
					}
					else
					{
						//strReturn = ts.Read(Marshal.SizeOf(MultiOwnerRec));
						buffer = new char[Strings.Len(MultiOwnerRec)];
						ts.Read(buffer, 0, Strings.Len(MultiOwnerRec));
						strReturn = new string(buffer);
					}
					GetCurrentOwnerInfo(ref strReturn, ref REInfo);
					if (!ts.EndOfStream)
					{
						if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUsesCRLF)
						{
							strReturn = ts.ReadLine();
							// Get rid of CRLF, could also have read two chars
						}
						if (modGlobalVariablesXF.Statics.CustomizedInfo.intExtraChars > 0)
						{
                            //FC:FINAL:AM:#3533 - throw exception if end of stream
                            if (!ts.EndOfStream)
                            {
                                //strReturn = ts.Read(modGlobalVariables.Statics.CustomizedInfo.intExtraChars);
                                buffer = new char[modGlobalVariablesXF.Statics.CustomizedInfo.intExtraChars];
                                ts.Read(buffer, 0, modGlobalVariablesXF.Statics.CustomizedInfo.intExtraChars);
                                strReturn = new string(buffer);
                            }
                            else
                            {
                                throw new Exception("Input past end of file");
                            }
						}
					}
					lngTotalValLeft = REInfo.LandValue + REInfo.BldgValue;
					// now process the exemptions
					intNumExempts = 0;
					if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "CAMDEN")
					{
						lngTemp = 0;
						for (x = 0; x <= 7; x++)
						{
							//App.DoEvents();
							if (Conversion.Val(Exempts[x].Amount) > modGlobalConstants.EleventyBillion)
							{
								lngTemp = 0;
								Exempts[x].Amount = "0";
							}
							else
							{
								if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "SKOWHEGAN")
								{
									lngTemp += FCConvert.ToInt32(Conversion.Val(Exempts[x].Amount));
								}
								else
								{
									if (Conversion.Val(Exempts[x].Code) == 52 || Conversion.Val(Exempts[x].Code) == 54
										|| Conversion.Val(Exempts[x].Code) == 56 || Conversion.Val(Exempts[x].Code) == 58
										|| Conversion.Val(Exempts[x].Code) == 60 || Conversion.Val(Exempts[x].Code) == 62
										|| Conversion.Val(Exempts[x].Code) == 64 || Conversion.Val(Exempts[x].Code) == 66
										|| Conversion.Val(Exempts[x].Code) == 68 || Conversion.Val(Exempts[x].Code) == 70
										|| Conversion.Val(Exempts[x].Code) == 72 || Conversion.Val(Exempts[x].Code) == 74
										|| Conversion.Val(Exempts[x].Code) == 76 || Conversion.Val(Exempts[x].Code) == 78
										|| Conversion.Val(Exempts[x].Code) == 81)
									{
										// totally exempt
										Exempts[x].Amount = "0";
										break;
									}
									else
									{
										lngTemp += FCConvert.ToInt32(Exempts[x].Amount);
										break;
									}
								}
							}
						}
						// x
						for (x = 0; x <= 7; x++)
						{
							//App.DoEvents();
							if (fecherFoundation.Strings.Trim(Exempts[x].Code) != string.Empty)
							{
								intExemptCode = TranslateExempt(fecherFoundation.Strings.Trim(Exempts[x].Code));
								if (intExemptCode > 0)
								{
									intNumExempts += 1;
									if (intNumExempts > 3)
									{
										// too many
										REInfo.TooManyExempt = true;
									}
									else
									{
										if (Conversion.Val(Exempts[x].Amount) == 0)
										{
											if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "TP") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "LS") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "BC") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "AL") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "CH") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "PA") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "ST") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Exempts[x].Code)) == "CE"))
											{
												// totally exempt
												lngCurExempt = lngTotalValLeft;
											}
											else
											{
												// regular exemption
												if (rsExempts.FindFirstRecord("Code", intExemptCode))
												{
													if (Conversion.Val(rsExempts.Get_Fields("amount")) == 0)
													{
														lngCurExempt = lngTotalValLeft;
													}
													else
													{
														lngCurExempt = 0;
													}
												}
												else
												{
													lngCurExempt = 0;
												}
												// If UCase(MuniName) = "KENNEBUNKPORT" And Val(Trim(Exempts(x).Code)) = 50 Then
												// lngCurExempt = lngTotalValLeft
												// ElseIf (UCase(MuniName) = "SABATTUS" Or UCase(MuniName) = "BOOTHBAY") And Val(Trim(Exempts(x).Code)) = 98 Then
												// lngCurExempt = lngTotalValLeft
												// ElseIf UCase(MuniName) = "ARUNDEL" Then
												// lngCurExempt = lngTotalValLeft
												// ElseIf UCase(MuniName) = "ROCKPORT" Then
												// lngCurExempt = lngTotalValLeft
												// ElseIf UCase(MuniName) = "BOOTHBAY HARBOR" Then
												// lngCurExempt = lngTotalValLeft
												// ElseIf UCase(MuniName) = "SKOWHEGAN" Then
												// Select Case Val(Exempts(x).Code)
												// Case 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 81
												// lngCurExempt = lngTotalValLeft
												// Case Else
												// lngCurExempt = 0
												// End Select
												// ElseIf LCase(MuniName) = "gardiner" Or LCase(MuniName) = "winthrop" Or UCase(MuniName) = "RICHMOND" Or UCase(MuniName) = "WESTBROOK" Or UCase(MuniName) = "GORHAM" Or UCase(MuniName) = "TOPSHAM" Or UCase(MuniName) = "WEST BATH" Or UCase(MuniName) = "CHEBEAGUE" Or UCase(MuniName) = "YORK" Or UCase(MuniName) = "SOUTH THOMASTON" Or UCase(MuniName) = "CORNISH" Or UCase(MuniName) = "MT. DESERT" Or LCase(MuniName) = "raymond" Then
												// lngCurExempt = lngTotalValLeft
												// Else
												// lngCurExempt = 0
												// End If
											}
										}
										else
										{
											// just apply the exemption
											lngCurExempt = FCConvert.ToInt32(Conversion.Val(Exempts[x].Amount) / 100);
											// If UCase(MuniName) = "SKOWHEGAN" Then
											// skowhegan shows full exemption not actual
											if (lngCurExempt > lngTotalValLeft)
											{
												lngtotex += (lngCurExempt - lngTotalValLeft);
												lngCurExempt = lngTotalValLeft;
											}
											// End If
										}
										lngTotalValLeft -= lngCurExempt;
										switch (intNumExempts)
										{
											case 1:
												{
													REInfo.ExemptCode1 = FCConvert.ToString(intExemptCode);
													REInfo.ExemptAmount1 = lngCurExempt;
													break;
												}
											case 2:
												{
													REInfo.ExemptCode2 = FCConvert.ToString(intExemptCode);
													REInfo.ExemptAmount2 = lngCurExempt;
													break;
												}
											case 3:
												{
													REInfo.ExemptCode3 = FCConvert.ToString(intExemptCode);
													REInfo.ExemptAmount3 = lngCurExempt;
													break;
												}
										}
										//end switch
									}
								}
								else if (intExemptCode < 0)
								{
									// unknown code
									if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "SABATTUS")
									{
										REInfo.BadExempt = true;
									}
									else
									{
										// if the amount is totally exempt, make this a 50
										if (lngTemp == lngTotalValLeft && x == 0)
										{
											if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SABATTUS")
											{
												REInfo.ExemptCode1 = FCConvert.ToString(98);
											}
											else
											{
												REInfo.ExemptCode1 = FCConvert.ToString(50);
											}
											REInfo.ExemptAmount1 = lngTotalValLeft;
										}
										else
										{
											REInfo.BadExempt = true;
										}
									}
								}
							}
						}
						// x
					}
					// if muniname <> arundel and <> camden
					if (lngTotalValLeft < 0)
					{
						// error in exempt amount
						REInfo.BadExemptAmount = true;
					}
					// record is filled, now put in list
					PutInList(ref REInfo);
				}
				ts.Close();
				frmWait.InstancePtr.Unload();
				// gridload is loaded
				// now copy entries from gridload to appropriate grids (new,good,bad)
				ProcessGridLoad();
				MessageBox.Show("Load Complete", "Loaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show(fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ImportVision", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int TranslateExempt(string strCode)
		{
			int TranslateExempt = 0;
			TranslateExempt = 0;
			// If LCase(MuniName) <> "gardiner" And LCase(MuniName) <> "winthrop" And LCase(MuniName) <> "westbrook" And UCase(MuniName) <> "ROCKPORT" And UCase(MuniName) <> "RICHMOND" And UCase(MuniName) <> "GORHAM" And UCase(MuniName) <> "BOOTHBAY HARBOR" And UCase(MuniName) <> "WEST BATH" And UCase(MuniName) <> "CHEBEAUGE" And UCase(MuniName) <> "CHEBEAGUE" And UCase(MuniName) <> "SKOWHEGAN" And UCase(MuniName) <> "BOOTHBAY" And UCase(MuniName) <> "KENNEBUNKPORT" And UCase(MuniName) <> "SABATTUS" And UCase(MuniName) <> "YORK" And UCase(MuniName) <> "ARUNDEL" And UCase(MuniName) <> "SOUTH THOMASTON" And UCase(MuniName) <> "CORNISH" And UCase(MuniName) <> "MT. DESERT" And UCase(MuniName) <> "TOPSHAM" Then
			if ((fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "eliot") || (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "camden") || (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "lebanon"))
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "AL")
				{
					TranslateExempt = 4;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "BC")
				{
					TranslateExempt = 3;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "BL")
				{
					TranslateExempt = 10;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "CE")
				{
					TranslateExempt = 8;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "CH")
				{
					TranslateExempt = 5;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "H1")
				{
					TranslateExempt = 30;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "H2")
				{
					TranslateExempt = 31;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "H3")
				{
					TranslateExempt = 32;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "LS")
				{
					TranslateExempt = 2;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "PA")
				{
					TranslateExempt = 6;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "ST")
				{
					TranslateExempt = 7;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "TP")
				{
					TranslateExempt = 1;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "V1R")
				{
					TranslateExempt = 15;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "V2N")
				{
					TranslateExempt = 16;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "V2R")
				{
					TranslateExempt = 16;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "VRT")
				{
					TranslateExempt = 17;
				}
				else
				{
					TranslateExempt = -1;
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "westbrook")
			{
				if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "he")
				{
					TranslateExempt = 1;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "i2")
				{
					TranslateExempt = 3;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "o2")
				{
					TranslateExempt = 4;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "iv")
				{
					TranslateExempt = 5;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "ik")
				{
					TranslateExempt = 6;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "w2i")
				{
					TranslateExempt = 7;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "wki")
				{
					TranslateExempt = 8;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "wvi")
				{
					TranslateExempt = 9;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "w2o")
				{
					TranslateExempt = 10;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "wko")
				{
					TranslateExempt = 11;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "ov")
				{
					TranslateExempt = 12;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "b")
				{
					TranslateExempt = 2;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "ii")
				{
					TranslateExempt = 13;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "pgo")
				{
					TranslateExempt = 14;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "m")
				{
					TranslateExempt = 18;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "ok")
				{
					TranslateExempt = 15;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "w1i")
				{
					TranslateExempt = 16;
				}
				else if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strCode)) == "wii")
				{
					TranslateExempt = 17;
				}
				else
				{
					if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) > 0)
					{
						if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9000 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9009)
						{
							// us
							TranslateExempt = 50;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9010 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9019)
						{
							// state
							TranslateExempt = 51;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9020 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9029)
						{
							// county
							TranslateExempt = 52;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9030 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9039)
						{
							// town
							TranslateExempt = 53;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9040 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9049)
						{
							// private school
							TranslateExempt = 54;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9050 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9059)
						{
							// hospital
							TranslateExempt = 55;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9060 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9069)
						{
							// religious
							TranslateExempt = 56;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9070 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9079)
						{
							// water district
							TranslateExempt = 57;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9080 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9089)
						{
							// fraternal
							TranslateExempt = 58;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9090 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9099)
						{
							// merc/pollu
							TranslateExempt = 59;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9100 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9109)
						{
							// charitable
							TranslateExempt = 60;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9200 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9209)
						{
							// non profit
							TranslateExempt = 61;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9230 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9239)
						{
							// lit/sci
							TranslateExempt = 62;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9300 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9309)
						{
							// cemetery
							TranslateExempt = 63;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9400 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9409)
						{
							// common/open
							TranslateExempt = 64;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9500)
						{
							// rail line
							TranslateExempt = 65;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) >= 9600 && Conversion.Val(fecherFoundation.Strings.Trim(strCode)) <= 9609)
						{
							// housing authority
							TranslateExempt = 66;
						}
						else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9920)
						{
							// und condo
							TranslateExempt = 67;
						}
						else
						{
							TranslateExempt = -1;
						}
					}
					else
					{
						TranslateExempt = -1;
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "sabattus")
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "98")
				{
					TranslateExempt = 98;
				}
				else
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "york")
			{
				if (Conversion.Val(strCode) < 9000)
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 9000)
					{
						// us
						TranslateExempt = 8;
					}
					else if (Conversion.Val(strCode) == 9010)
					{
						// state
						TranslateExempt = 7;
					}
					else if (Conversion.Val(strCode) == 9020)
					{
						// chamber of commerce
						TranslateExempt = 34;
					}
					else if (Conversion.Val(strCode) == 9030 || Conversion.Val(strCode) == 9035)
					{
						// town
						TranslateExempt = 11;
					}
					else if (Conversion.Val(strCode) == 9031)
					{
						// charity
						TranslateExempt = 47;
					}
					else if (Conversion.Val(strCode) == 9200)
					{
						// historical
						TranslateExempt = 48;
					}
					else if (Conversion.Val(strCode) == 9050)
					{
						// water
						TranslateExempt = 10;
					}
					else if (Conversion.Val(strCode) == 9051)
					{
						TranslateExempt = 51;
					}
					else if (Conversion.Val(strCode) == 9060)
					{
						// church
						TranslateExempt = 2;
					}
					else
					{
						// misc totally exempt
						TranslateExempt = 91;
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "south thomaston")
			{
				if (Conversion.Val(strCode) < 9000)
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 9000)
					{
						// us
						TranslateExempt = 41;
					}
					else if (Conversion.Val(strCode) == 9010)
					{
						// state
						TranslateExempt = 42;
					}
					else if (Conversion.Val(strCode) == 9020)
					{
						// airport
						TranslateExempt = 46;
					}
					else if (Conversion.Val(strCode) == 9030 || Conversion.Val(strCode) == 9035)
					{
						// town
						TranslateExempt = 44;
					}
					else if (Conversion.Val(strCode) == 9031)
					{
						// charity
						TranslateExempt = 48;
					}
					else if (Conversion.Val(strCode) == 9040)
					{
						// land org
						TranslateExempt = 36;
					}
					else if (Conversion.Val(strCode) == 9050)
					{
						// water
						TranslateExempt = 43;
					}
					else if (Conversion.Val(strCode) == 9060)
					{
						// church
						TranslateExempt = 52;
					}
					else if (Conversion.Val(strCode) == 9300)
					{
						// schools
						TranslateExempt = 61;
					}
					else
					{
						TranslateExempt = 99;
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "richmond")
			{
				if (Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999))
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 900 || Conversion.Val(strCode) == 9000)
					{
						// us
						TranslateExempt = 24;
					}
					else if (Conversion.Val(strCode) == 901 || Conversion.Val(strCode) == 9010)
					{
						// state
						TranslateExempt = 25;
					}
					else if (Conversion.Val(strCode) == 903 || Conversion.Val(strCode) == 9030)
					{
						// town
						TranslateExempt = 13;
					}
					else if (Conversion.Val(strCode) == 9033)
					{
						// town
						TranslateExempt = 13;
					}
					else if (Conversion.Val(strCode) == 905 || Conversion.Val(strCode) == 9050)
					{
						// ?
						TranslateExempt = 77;
					}
					else if (Conversion.Val(strCode) == 906 || Conversion.Val(strCode) == 9060)
					{
						// church
						TranslateExempt = 19;
					}
					else if (Conversion.Val(strCode) == 907 || Conversion.Val(strCode) == 9070)
					{
						// utilities
						TranslateExempt = 78;
					}
					else if (Conversion.Val(strCode) == 920 || Conversion.Val(strCode) == 9200)
					{
						// am legion
						TranslateExempt = 22;
					}
					else if (Conversion.Val(strCode) == 930 || Conversion.Val(strCode) == 9300)
					{
						// utilities
						TranslateExempt = 79;
					}
					else if (Conversion.Val(strCode) == 9400)
					{
						// cemetery
						TranslateExempt = 99;
					}
					else if (Conversion.Val(strCode) == 9500)
					{
						// grange
						TranslateExempt = 81;
					}
					else if (Conversion.Val(strCode) == 9900)
					{
						TranslateExempt = 83;
					}
					else
					{
						TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gorham")
			{
				if (Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999))
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 900 || Conversion.Val(strCode) == 9000)
					{
						// us
						TranslateExempt = 70;
					}
					else if (Conversion.Val(strCode) == 910 || Conversion.Val(strCode) == 9100)
					{
						// char / benev ?
						TranslateExempt = 71;
					}
					else if (Conversion.Val(strCode) == 9036)
					{
						// waste
						TranslateExempt = 72;
					}
					else if (Conversion.Val(strCode) == 901 || Conversion.Val(strCode) == 9010)
					{
						// state
						TranslateExempt = 42;
					}
					else if (Conversion.Val(strCode) == 906 || Conversion.Val(strCode) == 9060)
					{
						// church
						TranslateExempt = 73;
					}
					else if (Conversion.Val(strCode) == 9038)
					{
						// portland water
						TranslateExempt = 74;
					}
					else if (Conversion.Val(strCode) == 903 || Conversion.Val(strCode) == 9030)
					{
						// town
						TranslateExempt = 75;
					}
					else if (Conversion.Val(strCode) == 9033)
					{
						// town
						TranslateExempt = 76;
					}
					else if (Conversion.Val(strCode) == 905 || Conversion.Val(strCode) == 9050)
					{
						// county roal llc. quasi munic?
						TranslateExempt = 77;
					}
					else if (Conversion.Val(strCode) == 907 || Conversion.Val(strCode) == 9070)
					{
						// masonic
						TranslateExempt = 78;
					}
					else if (Conversion.Val(strCode) == 909 || Conversion.Val(strCode) == 9090)
					{
						// seminary
						TranslateExempt = 79;
					}
					else if (Conversion.Val(strCode) == 920 || Conversion.Val(strCode) == 9200)
					{
						// housing authority
						TranslateExempt = 80;
					}
					else if (Conversion.Val(strCode) == 9015)
					{
						// university
						TranslateExempt = 81;
					}
					else
					{
						TranslateExempt = 105;
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "rockport")
			{
				if (Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999))
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					// Select Case Val(strCode)
					// Case 900, 9000
					// us
					// TranslateExempt = 70
					// Case 910, 9100
					// char / benev ?
					// TranslateExempt = 71
					// Case 9036
					// waste
					// TranslateExempt = 72
					// Case 901, 9010
					// state
					// TranslateExempt = 42
					// Case 906, 9060
					// church
					// TranslateExempt = 73
					// Case 9038
					// portland water
					// TranslateExempt = 74
					// Case 903, 9030
					// town
					// TranslateExempt = 75
					// Case 9033
					// town
					// TranslateExempt = 76
					// Case 905, 9050
					// county roal llc. quasi munic?
					// TranslateExempt = 77
					// Case 907, 9070
					// masonic
					// TranslateExempt = 78
					// Case 909, 9090
					// seminary
					// TranslateExempt = 79
					// Case 920, 9200
					// housing authority
					// TranslateExempt = 80
					// Case 9015
					// university
					// TranslateExempt = 81
					// Case Else
					// TranslateExempt = 105
					// End Select
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "mt. desert")
			{
				if (Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999))
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 900 || Conversion.Val(strCode) == 9000)
					{
						TranslateExempt = 4;
					}
					else if (Conversion.Val(strCode) == 901 || Conversion.Val(strCode) == 9010)
					{
						TranslateExempt = 42;
					}
					else if (Conversion.Val(strCode) == 903 || Conversion.Val(strCode) == 9030)
					{
						TranslateExempt = 8;
					}
					else if (Conversion.Val(strCode) == 904 || Conversion.Val(strCode) == 9040)
					{
						TranslateExempt = 7;
					}
					else if (Conversion.Val(strCode) == 906 || Conversion.Val(strCode) == 9060)
					{
						TranslateExempt = 11;
					}
					else if (Conversion.Val(strCode) == 907 || Conversion.Val(strCode) == 9070)
					{
						TranslateExempt = 4;
					}
					else if (Conversion.Val(strCode) == 908 || Conversion.Val(strCode) == 9080)
					{
						TranslateExempt = 9;
					}
					else if (Conversion.Val(strCode) == 910 || Conversion.Val(strCode) == 920
						|| Conversion.Val(strCode) == 9100 || Conversion.Val(strCode) == 9200)
					{
						TranslateExempt = 10;
					}
					else if (Conversion.Val(strCode) == 930 || Conversion.Val(strCode) == 9300)
					{
						TranslateExempt = 6;
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "boothbay harbor")
			{
				if (Information.IsNumeric(strCode) && Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999) && Conversion.Val(strCode) != 1903)
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 0)
					{
						if (fecherFoundation.Strings.UCase(strCode) == "H1")
						{
							TranslateExempt = 13;
						}
						else if (fecherFoundation.Strings.UCase(strCode) == "H2")
						{
							TranslateExempt = 13;
						}
						else if (fecherFoundation.Strings.UCase(strCode) == "H3")
						{
							TranslateExempt = 13;
						}
						else if (fecherFoundation.Strings.UCase(strCode) == "TG")
						{
							TranslateExempt = 0;
						}
					}
				else
					{
						// strCode = strCode
						if (Conversion.Val(strCode) == 1903)
						{
							TranslateExempt = 6;
						}
						else if (Conversion.Val(strCode) == 9000)
						{
							TranslateExempt = 4;
						}
						else if (Conversion.Val(strCode) == 9030)
						{
							TranslateExempt = 6;
						}
						else if (Conversion.Val(strCode) == 9200)
						{
							TranslateExempt = 8;
						}
						else if (Conversion.Val(strCode) == 9050)
						{
							TranslateExempt = 8;
						}
						else if (Conversion.Val(strCode) == 9070)
						{
							TranslateExempt = 14;
						}
						else if (Conversion.Val(strCode) == 9100)
						{
							TranslateExempt = 14;
						}
						else if (Conversion.Val(strCode) == 9031)
						{
							TranslateExempt = 5;
						}
						else if (Conversion.Val(strCode) == 9060)
						{
							TranslateExempt = 9;
						}
						else
						{
							TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
						}
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "cornish")
			{
				if (Conversion.Val(strCode) < 9000)
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 9000)
					{
						// us
						TranslateExempt = 41;
					}
					else if (Conversion.Val(strCode) == 9010)
					{
						// state
						TranslateExempt = 42;
					}
					else if (Conversion.Val(strCode) == 9020)
					{
						// US
						TranslateExempt = 41;
					}
					else if (Conversion.Val(strCode) == 9030 || Conversion.Val(strCode) == 9035)
					{
						// town
						TranslateExempt = 44;
					}
					else if (Conversion.Val(strCode) == 9100)
					{
						// charity
						TranslateExempt = 48;
					}
					else if (Conversion.Val(strCode) == 9040)
					{
						// library
						TranslateExempt = 49;
					}
					else if (Conversion.Val(strCode) == 9050)
					{
						// water
						TranslateExempt = 43;
					}
					else if (Conversion.Val(strCode) == 9060)
					{
						// church
						TranslateExempt = 52;
					}
					else if (Conversion.Val(strCode) == 9070)
					{
						// cemetary
						TranslateExempt = 31;
					}
					else if (Conversion.Val(strCode) == 9300)
					{
						// schools
						TranslateExempt = 61;
					}
					else
					{
						TranslateExempt = 99;
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
			{
				if (Conversion.Val(strCode) < 9000)
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strCode + "0000", 3))));
					// Select Case Val(strCode)
					// Case 9200, 9010, 9300, 9060, 9030
					// TranslateExempt = Val(Left(strCode, 3))
					// Case Else
					// TranslateExempt = Val(strCode)
					// End Select
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "winthrop")
			{
				if (Conversion.Val(strCode) < 9000)
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(strCode) == 9200 || Conversion.Val(strCode) == 9010
						|| Conversion.Val(strCode) == 9300 || Conversion.Val(strCode) == 9060
						|| Conversion.Val(strCode) == 9030)
						{
							TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strCode, 3))));
						}
						else
						{
							TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
						}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "west bath")
			{
				if (Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999))
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9000")
					{
						TranslateExempt = 40;
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9010")
					{
						TranslateExempt = 41;
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9030")
					{
						TranslateExempt = 43;
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "903V")
					{
						TranslateExempt = 44;
					}
					else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "910R") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9100"))
					{
						TranslateExempt = 47;
					}
					else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9080") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "903C"))
					{
						TranslateExempt = 50;
					}
					else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "906R") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9060"))
					{
						TranslateExempt = 51;
					}
					else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "920V") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "910V") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "930V") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9300") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9200"))
					{
						TranslateExempt = 60;
					}
					else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "940V") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "9400"))
					{
						TranslateExempt = 65;
					}
					else
					{
						TranslateExempt = 0;
					}
				}
			}
			else if ((fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "arundel") || (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "skowhegan") || (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "boothbay") || (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "chebeague"))
			{
				TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "topsham")
			{
				if (Conversion.Val(strCode) < 9000 && (Conversion.Val(strCode) < 900 || Conversion.Val(strCode) > 999))
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
				}
				else
				{
					if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9000)
					{
						TranslateExempt = 24;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9020)
					{
						TranslateExempt = 902;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9030)
					{
						TranslateExempt = 13;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9033)
					{
						TranslateExempt = 14;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9050)
					{
						TranslateExempt = 21;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9060)
					{
						TranslateExempt = 19;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9070)
					{
						TranslateExempt = 28;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9080)
					{
						TranslateExempt = 908;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9100)
					{
						TranslateExempt = 21;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9400)
					{
						TranslateExempt = 99;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9900)
					{
						TranslateExempt = 990;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9910)
					{
						TranslateExempt = 991;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 9920)
					{
						TranslateExempt = 992;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 900)
					{
						TranslateExempt = 24;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 901)
					{
						TranslateExempt = 25;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 903)
					{
						TranslateExempt = 13;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 904)
					{
						TranslateExempt = 17;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 905)
					{
						TranslateExempt = 21;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 906)
					{
						TranslateExempt = 19;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 910)
					{
						TranslateExempt = 21;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 920)
					{
						TranslateExempt = 22;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 930)
					{
						TranslateExempt = 26;
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(strCode)) == 950)
					{
						TranslateExempt = 21;
					}
					else
					{
						TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(strCode)));
					}
				}
			}
			else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "kennebunkport")
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "HM")
				{
					TranslateExempt = 1;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "11")
				{
					TranslateExempt = 11;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "12")
				{
					TranslateExempt = 12;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "13")
				{
					TranslateExempt = 13;
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strCode)) == "50")
				{
					TranslateExempt = 50;
				}
				else
				{
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(strCode))));
				}
			}
			else
			{
				if (Information.IsNumeric(strCode))
				{
					// TranslateExempt = Val(strCode)
					TranslateExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strCode, 3))));
				}
				else
				{
					if ((fecherFoundation.Strings.LCase(strCode) == "h2") || (fecherFoundation.Strings.LCase(strCode) == "h3"))
					{
						strCode = Strings.Left(strCode, 1);
					}
					else if ((fecherFoundation.Strings.LCase(strCode) == "hm2") || (fecherFoundation.Strings.LCase(strCode) == "hm3"))
					{
						strCode = Strings.Left(strCode, 2);
					}
					if (rsExempts.FindFirstRecord("ShortDescription", "'" + strCode + "'"))
					{
						TranslateExempt = FCConvert.ToInt16(rsExempts.Get_Fields("code"));
					}
					else
					{
						TranslateExempt = -1;
					}
				}
			}
			return TranslateExempt;
		}

		private bool GetAccountInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord)
		{
			bool GetAccountInfo = false;
			// fills account info in rerecord from strdataread
			string strMapLot = "";
			string strTemp = "";
			GetAccountInfo = false;
			RERecord.StreetName = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 37, 25));
			RERecord.StreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 62, 10)))));
			RERecord.CompetitionAccount = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 72, 20));
			// If LCase(MuniName) = "westbrook" Then
			// If RERecord.TrioAccount = 0 Then
			// RERecord.TrioAccount = Val(RERecord.CompetitionAccount)
			// End If
			// End If
			GetAccountInfo = true;
			return GetAccountInfo;
		}

		private bool GetCurrentOwnerInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord)
		{
			bool GetCurrentOwnerInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp = "";
				string strNewOwner = "";
				GetCurrentOwnerInfo = false;
				if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUseMultiOwner)
				{
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 241, 3));
					RERecord.PercentageOwned = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
					if (Conversion.Val(strTemp) < 100)
					{
						// a multiowner account
						RERecord.MultiOwner = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 40));
					}
				}
				else
				{
					RERecord.PercentageOwned = 100;
				}
				if (modGlobalVariablesXF.Statics.CustomizedInfo.intOwnerOption != modMainXF.CNSTREOWNERINFOOLD || (modGlobalVariablesXF.Statics.CustomizedInfo.boolUseMultiOwner && RERecord.PercentageOwned < 100))
				{
					if (modGlobalVariablesXF.Statics.CustomizedInfo.intOwnerOption == modMainXF.CNSTREOWNERINFONEW || (modGlobalVariablesXF.Statics.CustomizedInfo.boolUseMultiOwner && RERecord.PercentageOwned < 100))
					{
						strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 40));
						RERecord.Name = strTemp;
						strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 41, 40));
						RERecord.SecOwner = strTemp;
					}
					else
					{
						strNewOwner = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 40));
					}
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 81, 30));
					RERecord.Address1 = strTemp;
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 111, 30));
					RERecord.Address2 = strTemp;
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 181, 20));
					RERecord.City = strTemp;
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 201, 2));
					RERecord.State = strTemp;
					strTemp = fecherFoundation.Strings.RTrim(Strings.Mid(strDataRead, 203, 20));
					RERecord.Zip4 = "";
					if (strTemp.Length >= 9)
					{
						RERecord.Zip = Strings.Mid(strTemp, 1, 5);
						RERecord.Zip4 = Strings.Mid(strTemp, 7, 4);
					}
					else
					{
						RERecord.Zip = fecherFoundation.Strings.Trim(Strings.Left(strTemp + "     ", 5));
						if (strTemp.Length > 5)
						{
							RERecord.Zip4 = Strings.Mid(strTemp, 6);
						}
					}
					if (modGlobalVariablesXF.Statics.CustomizedInfo.intOwnerOption == modMainXF.CNSTREOWNERINFOOLDCONEW)
					{
						if (fecherFoundation.Strings.Trim(RERecord.Address2) == string.Empty)
						{
							RERecord.Address2 = RERecord.Address1;
							RERecord.Address1 = "C/O " + strNewOwner;
						}
						else if (fecherFoundation.Strings.Trim(RERecord.Address1) == string.Empty)
						{
							RERecord.Address1 = "C/O " + strNewOwner;
						}
						else if (fecherFoundation.Strings.Trim(RERecord.SecOwner) == string.Empty)
						{
							RERecord.SecOwner = "C/O " + strNewOwner;
						}
						else
						{
							RERecord.Name += ", " + RERecord.SecOwner;
							RERecord.SecOwner = "C/O " + strNewOwner;
						}
					}
				}
				GetCurrentOwnerInfo = true;
				return GetCurrentOwnerInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCurrentOwnerInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				frmWait.InstancePtr.Show();
			}
			return GetCurrentOwnerInfo;
		}

		private bool GetOwnerInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord)
		{
			bool GetOwnerInfo = false;
			// gets owner info such as name and mailing address
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetOwnerInfo = false;
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 40));
				RERecord.Name = strTemp;
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 41, 40));
				RERecord.SecOwner = strTemp;
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 81, 30));
				RERecord.Address1 = strTemp;
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 111, 30));
				RERecord.Address2 = strTemp;
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 181, 20));
				RERecord.City = strTemp;
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 201, 2));
				RERecord.State = strTemp;
				strTemp = fecherFoundation.Strings.RTrim(Strings.Mid(strDataRead, 203, 20));
				RERecord.Zip4 = "";
				if (strTemp.Length >= 9)
				{
					RERecord.Zip = Strings.Mid(strTemp, 1, 5);
					RERecord.Zip4 = Strings.Mid(strTemp, 7, 4);
				}
				else
				{
					RERecord.Zip = fecherFoundation.Strings.Trim(Strings.Left(strTemp + "     ", 5));
					if (strTemp.Length > 5)
					{
						RERecord.Zip4 = Strings.Mid(strTemp, 6);
					}
				}
				GetOwnerInfo = true;
				return GetOwnerInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetOwnerInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				frmWait.InstancePtr.Show();
			}
			return GetOwnerInfo;
		}

		private bool GetSaleInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord, bool boolFirst = true)
		{
			bool GetSaleInfo = false;
			// VB6 Bad Scope Dim:
			int lngRow = 0;
			string strTemp;
			string[] strAry = null;
			string strDate;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetSaleInfo = false;
				// bookpage
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 10));
				// get rid of extra spaces or it will mess up the split
				strTemp = Strings.Replace(strTemp, "  ", " ", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "  ", " ", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "  ", " ", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "  ", " ", 1, -1, CompareConstants.vbTextCompare);
				strDate = Strings.Mid(strDataRead, 11, 2) + "/" + Strings.Mid(strDataRead, 13, 2) + "/" + Strings.Mid(strDataRead, 15, 4);
				if (boolFirst)
				{
					RERecord.BookPage = "";
					RERecord.Book = "";
					RERecord.Page = "";
					if (strTemp.Length > 4)
					{
						strAry = Strings.Split(strTemp, " ", -1, CompareConstants.vbTextCompare);
						if (Information.UBound(strAry, 1) < 1)
						{
							// error
							return GetSaleInfo;
						}
						RERecord.BookPage = strTemp;
						RERecord.Book = fecherFoundation.Strings.Trim(strAry[0]);
						RERecord.Page = fecherFoundation.Strings.Trim(strAry[1]);
					}
				}
				if (Information.IsDate(strDate))
				{
					gridPreviousOwners.Rows += 1;
					lngRow = gridPreviousOwners.Rows - 1;
					gridPreviousOwners.TextMatrix(lngRow, modGridConstantsXF.CNSTGRIDPREVOCOLMAPLOT, RERecord.MapLot);
					gridPreviousOwners.TextMatrix(lngRow, modGridConstantsXF.CNSTGRIDPREVOCOLSALEDATE, strDate);
					gridPreviousOwners.TextMatrix(lngRow, modGridConstantsXF.CNSTGRIDPREVOCOLTRIOACCOUNT, FCConvert.ToString(RERecord.TrioAccount));
					gridPreviousOwners.TextMatrix(lngRow, modGridConstantsXF.CNSTGRIDPREVOCOLVISIONACCOUNT, RERecord.CompetitionAccount);
					gridPreviousOwners.TextMatrix(lngRow, modGridConstantsXF.CNSTGRIDPREVOCOLGUID, RERecord.GUID);
					if (lngRow > 0)
					{
						if (gridPreviousOwners.TextMatrix(lngRow - 1, modGridConstantsXF.CNSTGRIDPREVOCOLGUID) == RERecord.GUID)
						{
							gridPreviousOwners.TextMatrix(lngRow - 1, modGridConstantsXF.CNSTGRIDPREVOCOLOWNER, fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 19, 40)));
						}
					}
				}
				GetSaleInfo = true;
				return GetSaleInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetSaleInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				frmWait.InstancePtr.Show();
			}
			return GetSaleInfo;
		}

		private bool GetExemptInfo(ref string strDataRead, ref modREFileFormatsXF.VisionExemptions ExemptRecord)
		{
			bool GetExemptInfo = false;
			// vbPorter upgrade warning: strTemp As string	OnRead(FixedString)
			string strTemp;
			string strCode;
			GetExemptInfo = false;
			ExemptRecord.Amount = "";
			ExemptRecord.Code = "";
			ExemptRecord.Year = "";
			strCode = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 5, 4));
			ExemptRecord.Code = fecherFoundation.Strings.Trim(strCode);
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 4));
			ExemptRecord.Year = strTemp;
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 9, 11));
			ExemptRecord.Amount = strTemp;
			GetExemptInfo = true;
			return GetExemptInfo;
		}

		private bool GetOtherAmountsInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord, ref modREFileFormatsXF.VisionExemptions ExemptRecord)
		{
			bool GetOtherAmountsInfo = false;
			int lngTemp = 0;
			string strCode = "";
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetOtherAmountsInfo = false;
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SKOWHEGAN")
				{
					// do nothing
				}
				else
				{
					// If UCase(MuniName) <> "KENNEBUNKPORT" Then
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strDataRead, 14, 11))));
					if (lngTemp > 0)
					{
						RERecord.OtherValue += lngTemp;
					}
					// Else
					// ExemptRecord.Amount = ""
					// ExemptRecord.Code = ""
					// ExemptRecord.Year = ""
					// 
					// strCode = Trim(Mid(strDataRead, 8, 2))
					// ExemptRecord.Code = Trim(strCode)
					// strTemp = Trim(Mid(strDataRead, 10, 4))
					// ExemptRecord.Year = strTemp
					// strTemp = Trim(Mid(strDataRead, 14, 11))
					// ExemptRecord.Amount = strTemp
					// If Val(ExemptRecord.Code) > 0 Then
					// strTemp = strTemp
					// End If
				}
				GetOtherAmountsInfo = true;
				return GetOtherAmountsInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetOtherAmountsInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				frmWait.InstancePtr.Show();
			}
			return GetOtherAmountsInfo;
		}

		private bool GetAssessmentInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord, ref modREFileFormatsXF.VisionExemptions ExemptRecord)
		{
			bool GetAssessmentInfo = false;
			// vbPorter upgrade warning: strTemp As string	OnRead(FixedString)
			string strTemp = "";
			int lngTemp;
			double dblTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetAssessmentInfo = false;
				if (fecherFoundation.Strings.UCase(Strings.Mid(strDataRead, 5, 1)) == "A")
				{
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 14, 9));
					dblTemp = Conversion.Val(strTemp) / 100;
					RERecord.Acreage += dblTemp;
				}
				else if (fecherFoundation.Strings.UCase(Strings.Mid(strDataRead, 5, 1)) == "S")
				{
					// square footage.  There are 43560 sqft to an acre
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 14, 9));
					dblTemp = Conversion.Val(strTemp) / 100;
					RERecord.Acreage = FCConvert.ToDouble(Strings.Format(dblTemp / 43560, "0.00"));
				}
				if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "arundel" && fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "eliot" && fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "camden" && fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "skowhegan" && fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "lebanon")
				{
					// If LCase(MuniName) = "winthrop" Or LCase(MuniName) = "westbrook" Or UCase(MuniName) = "RICHMOND" Or UCase(MuniName) = "GORHAM" Or UCase(MuniName) = "TOPSHAM" Or UCase(MuniName) = "WEST BATH" Or UCase(MuniName) = "CHEBEAGUE" Or UCase(MuniName) = "SOUTH THOMASTON" Or UCase(MuniName) = "MT. DESERT" Or UCase(MuniName) = "SABATTUS" Or UCase(MuniName) = "KENNEBUNKPORT" Or UCase(MuniName) = "YORK" Or UCase(MuniName) = "BOOTHBAY" Or UCase(MuniName) = "CORNISH" Or UCase(MuniName) = "BOOTHBAY HARBOR" Then
					if (fecherFoundation.Strings.UCase(Strings.Mid(strDataRead, 6, 6)) == "EXEMPT")
					{
						if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SABATTUS" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "BOOTHBAY")
						{
							ExemptRecord.Code = "98";
							ExemptRecord.Amount = "0";
						}
						else if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "kennebunkport" || fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "rockport")
						{
							ExemptRecord.Code = "50";
							ExemptRecord.Amount = "0";
							// ElseIf LCase(MuniName) = "winthrop" Or LCase(MuniName) = "westbrook" Or UCase(MuniName) = "RICHMOND" Or UCase(MuniName) = "GORHAM" Or UCase(MuniName) = "TOPSHAM" Or UCase(MuniName) = "WEST BATH" Or UCase(MuniName) = "CHEBEAGUE" Or UCase(MuniName) = "YORK" Or UCase(MuniName) = "SOUTH THOMASTON" Or UCase(MuniName) = "CORNISH" Or UCase(MuniName) = "MT. DESERT" Or UCase(MuniName) = "BOOTHBAY HARBOR" Then
						}
						else
						{
							strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 4));
							if (!(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "WEST BATH"))
							{
								// Or LCase(MuniName) = "winthrop" Then
								if (!(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MT. DESERT"))
								{
									ExemptRecord.Code = Conversion.Val(strTemp).ToString();
									ExemptRecord.Amount = "0";
								}
								else
								{
									if (Conversion.Val(ExemptRecord.Code) > 0 && Conversion.Val(ExemptRecord.Amount) > 0)
									{
										// do nothing
									}
									else
									{
										ExemptRecord.Code = Conversion.Val(strTemp).ToString();
										ExemptRecord.Amount = "0";
									}
								}
							}
							else
							{
								ExemptRecord.Code = strTemp;
								ExemptRecord.Amount = "0";
							}
							// ElseIf LCase(MuniName) = "kennebunkport" Or LCase(MuniName) = "rockport" Then
							// ExemptRecord.Code = 50
							// ExemptRecord.Amount = 0
						}
					}
				}
				strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 23, 9));
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				RERecord.LandValue += lngTemp;
				if (fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 32, 1)) != "-")
				{
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 32, 9));
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 32, 10));
				}
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				if (lngTemp >= 0)
				{
					RERecord.BldgValue += lngTemp;
				}
				else
				{
					RERecord.BldgValue = 0;
					RERecord.LandValue += lngTemp;
					if (RERecord.LandValue < 0)
						RERecord.LandValue = 0;
				}
				GetAssessmentInfo = true;
				return GetAssessmentInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetAssessmentInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				frmWait.InstancePtr.Show();
			}
			return GetAssessmentInfo;
		}

		private string ProcessMapLotSegment(ref string strSegment, ref string strSegment2)
		{
			string ProcessMapLotSegment = "";
			// takes something like 1A or A and outputs 001A or A
			string strTemp = "";
			string strTemp2 = "";
			int intDivWidth;
			int x = 0;
			string strBlankDivision;
			intDivWidth = modGlobalVariablesXF.Statics.CustomizedInfo.intDivisionSize;
			if (intDivWidth == 1)
			{
				// not formatting
				ProcessMapLotSegment = fecherFoundation.Strings.Trim(strSegment + strSegment2);
				return ProcessMapLotSegment;
			}
			if (fecherFoundation.Strings.Trim(strSegment + strSegment2) == string.Empty)
			{
				ProcessMapLotSegment = Strings.StrDup(intDivWidth, "0");
				return ProcessMapLotSegment;
			}
			strBlankDivision = Strings.StrDup(intDivWidth, "0");
			if (strSegment2 == strBlankDivision)
			{
				strSegment2 = "";
			}
			if (modMainXF.MyIsNumeric(fecherFoundation.Strings.Trim(strSegment) + fecherFoundation.Strings.Trim(strSegment2)) && fecherFoundation.Strings.Trim(strSegment2) != string.Empty)
			{
				strTemp = strBlankDivision + fecherFoundation.Strings.Trim(strSegment);
				strTemp = Strings.Right(strTemp, intDivWidth);
				if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "winthrop" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "CORNISH" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MT. DESERT")
				{
					// If strTemp <> strBlankDivision Or Not CustomizedInfo.boolOmitTrailingZeroes Then
					if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "winthrop")
					{
						strTemp += "-" + fecherFoundation.Strings.Trim(strSegment2);
					}
					else
					{
						strTemp2 = Strings.Right(strBlankDivision + fecherFoundation.Strings.Trim(strSegment2), intDivWidth);
						strTemp += "-" + fecherFoundation.Strings.Trim(strSegment2);
					}
					// Else
					// strTemp = Trim(strSegment2)
					// End If
				}
				else
				{
					if (strTemp == strBlankDivision && fecherFoundation.Strings.Trim(strSegment2) != "" && fecherFoundation.Strings.Trim(strSegment2) != strBlankDivision)
					{
						strTemp = fecherFoundation.Strings.Trim(strSegment2);
					}
					else
					{
						strTemp += fecherFoundation.Strings.Trim(strSegment2);
					}
				}
				ProcessMapLotSegment = strTemp;
				return ProcessMapLotSegment;
			}
			else
			{
				// strSegment = strSegment & strSegment2
				if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "winthrop")
				{
					// strTemp = strTemp & "-" & Trim(strSegment2)
					strTemp = fecherFoundation.Strings.Trim(strSegment);
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolOmitTrailingZeroes)
					{
						if (Strings.Mid(strTemp + strBlankDivision, 2, strTemp.Length - 1) == Strings.StrDup(strTemp.Length - 1, "0") && !modMainXF.MyIsNumeric(Strings.Left(strTemp, 1)))
						{
							strTemp = Strings.Left(strTemp, 1);
						}
						else
						{
							strTemp = fecherFoundation.Strings.Trim(strTemp);
						}
						ProcessMapLotSegment = strTemp;
						if (strSegment2 != "")
						{
							ProcessMapLotSegment = strTemp + "-" + strSegment2;
						}
						return ProcessMapLotSegment;
					}
				}
				else
				{
					strSegment += strSegment2;
				}
			}
			if (modMainXF.MyIsNumeric(fecherFoundation.Strings.Trim(strSegment)))
			{
				strTemp = strBlankDivision + strSegment;
				strTemp = Strings.Right(strTemp, intDivWidth);
				ProcessMapLotSegment = strTemp;
				return ProcessMapLotSegment;
			}
			else
			{
				strTemp = fecherFoundation.Strings.Trim(strSegment);
				x = 1;
				strTemp2 = "";
				if (Conversion.Val(strTemp) > 0)
				{
					while (Information.IsNumeric(Strings.Mid(strTemp, x, 1)))
					{
						strTemp2 += Strings.Mid(strTemp, x, 1);
						x += 1;
					}
				}
				else
				{
					while (Information.IsNumeric(Strings.Mid(strTemp, x, 1)))
					{
						x += 1;
					}
				}
				strTemp2 = strBlankDivision + strTemp2;
				strTemp2 = Strings.Right(strTemp2, intDivWidth);
				if (Conversion.Val(strTemp2) > 0)
				{
					strTemp = strTemp2 + fecherFoundation.Strings.Trim(Strings.Mid(strTemp, x));
					ProcessMapLotSegment = strTemp;
				}
				else
				{
					// no leading number
					if (Conversion.Val(strTemp) != 0 || x == 1)
					{
						if (!(x == 1))
						{
							ProcessMapLotSegment = fecherFoundation.Strings.Trim(strTemp);
						}
						else
						{
							if (Strings.Mid(strTemp + strBlankDivision, 2, strTemp.Length - 1) == Strings.StrDup(strTemp.Length - 1, "0"))
							{
								ProcessMapLotSegment = Strings.Left(strTemp, 1);
							}
							else
							{
								ProcessMapLotSegment = fecherFoundation.Strings.Trim(strTemp);
							}
						}
					}
					else
					{
						ProcessMapLotSegment = fecherFoundation.Strings.Trim(Strings.Mid(strTemp, x));
					}
				}
			}
			return ProcessMapLotSegment;
		}

		private bool GetAdditionalInfo(ref string strDataRead, ref modREFileFormatsXF.ConvertedData RERecord)
		{
			bool GetAdditionalInfo = false;
			string strTemp = "";
			string strTemp2 = "";
			string strMapLot = "";
			int intEnd = 0;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetAdditionalInfo = false;
				if (!(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "YORK"))
				{
					// parse maplot stuff
					strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 7));
					// strTemp = "000" & strTemp
					// strTemp = Right(strTemp, 3)
					// map cut
					strTemp2 = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 8, 3));
					// If strTemp2 <> vbNullString Then
					// If Val(strTemp) > 0 Then
					// strTemp = strTemp & strTemp2
					// Else
					// strTemp = strTemp2
					// End If
					// End If
					strTemp = ProcessMapLotSegment(ref strTemp, ref strTemp2);
					strMapLot = strTemp + "-";
					// now block
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUsesBlock)
					{
						strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 11, 7));
						// strTemp = "000" & strTemp
						// strTemp = Right(strTemp, 3)
						// block cut
						strTemp2 = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 18, 3));
						// If strTemp2 <> vbNullString Then
						// If Val(strTemp) > 0 Then
						// strTemp = strTemp & strTemp2
						// Else
						// strTemp = strTemp2
						// End If
						// End If
						strTemp = ProcessMapLotSegment(ref strTemp, ref strTemp2);
						strMapLot += strTemp + "-";
					}
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUsesLot)
					{
						strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 21, 7));
						// strTemp = "000" & strTemp
						// strTemp = Right(strTemp, 3)
						// lot cut
						strTemp2 = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 28, 3));
						// If strTemp2 <> vbNullString Then
						// If Val(strTemp) > 0 Then
						// strTemp = strTemp & strTemp2
						// Else
						// strTemp = strTemp2
						// End If
						// End If
						strTemp = ProcessMapLotSegment(ref strTemp, ref strTemp2);
						strMapLot += strTemp + "-";
					}
					// now unit
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUsesUnits)
					{
						strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 31, 7));
						// use unit not lot
						// strTemp = "000" & strTemp
						// strTemp = Right(strTemp, 3)
						strTemp2 = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 38, 3));
						// If strTemp2 <> vbNullString Then
						// If Val(strTemp) > 0 Then
						// strTemp = strTemp & strTemp2
						// Else
						// strTemp = strTemp2
						// End If
						// End If
						strTemp = ProcessMapLotSegment(ref strTemp, ref strTemp2);
						strMapLot += strTemp;
					}
					if (strMapLot != string.Empty)
					{
						if (Strings.Right(strMapLot, 1) == "-")
						{
							strMapLot = Strings.Mid(strMapLot, 1, strMapLot.Length - 1);
						}
					}
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolOmitTrailingZeroes)
					{
						// cut out the extra zeroes
						if (fecherFoundation.Strings.Trim(strMapLot) != string.Empty)
						{
							strTemp = "";
							strAry = Strings.Split(strMapLot, "-", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 0)
							{
								intEnd = 0;
								for (x = (Information.UBound(strAry, 1)); x >= 1; x--)
								{
									strTemp2 = Strings.Replace(strAry[x], "0", "", 1, -1, CompareConstants.vbTextCompare);
									if (fecherFoundation.Strings.Trim(strTemp2) != string.Empty)
									{
										intEnd = x;
										break;
									}
								}
								// x
								for (x = 0; x <= intEnd; x++)
								{
									strTemp += strAry[x] + "-";
								}
								// x
								if (strTemp != string.Empty)
								{
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
									// get rid of last -
								}
								strMapLot = strTemp;
							}
						}
					}
					RERecord.MapLot = strMapLot;
				}
				else
				{
				}
				GetAdditionalInfo = true;
				return GetAdditionalInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetAdditionalInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				frmWait.InstancePtr.Show();
			}
			return GetAdditionalInfo;
		}

		private void ClearRecord(ref modREFileFormatsXF.ConvertedData RERecord)
		{
			// just clears the records
			RERecord.Acreage = 0;
			RERecord.Address1 = "";
			RERecord.Address2 = "";
			RERecord.BadExempt = false;
			RERecord.BadExemptAmount = false;
			RERecord.BadMapLot = false;
			RERecord.BldgValue = 0;
			RERecord.Book = "";
			RERecord.BookPage = "";
			RERecord.City = "";
			RERecord.CompetitionAccount = "";
			RERecord.ExemptAmount1 = 0;
			RERecord.ExemptAmount2 = 0;
			RERecord.ExemptAmount3 = 0;
			RERecord.ExemptCode1 = FCConvert.ToString(0);
			RERecord.ExemptCode2 = FCConvert.ToString(0);
			RERecord.ExemptCode3 = FCConvert.ToString(0);
			RERecord.LandValue = 0;
			RERecord.MapLot = "";
			RERecord.Name = "";
			RERecord.OtherValue = 0;
			RERecord.Page = "";
			RERecord.SecOwner = "";
			RERecord.State = "";
			RERecord.StreetName = "";
			RERecord.StreetNumber = 0;
			RERecord.TooManyExempt = false;
			RERecord.TrioAccount = 0;
			RERecord.UpdatedTooMany = false;
			RERecord.Zip = "";
			RERecord.Zip4 = "";
		}

		private void PutInList(ref modREFileFormatsXF.ConvertedData RERecord)
		{
			// puts record in list.  Searches to see if it is already in the list
			int lngRow;
			int lngTemp = 0;
			lngRow = FindInList(ref RERecord.MapLot, 0, RERecord.CompetitionAccount, RERecord.Name, RERecord.PercentageOwned);
			if (lngRow < 0)
			{
				GridLoad.Rows += 1;
				lngRow = GridLoad.Rows - 1;
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTRIOACCOUNT, FCConvert.ToString(0));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED, FCConvert.ToString(false));
			}
			else
			{
				// if there is more than one, try not to get a deleted one
				if (GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED) == string.Empty)
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED, FCConvert.ToString(false));
				while (FCConvert.CBool(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED)))
				{
					//App.DoEvents();
					lngTemp = FindInList(ref RERecord.MapLot, lngRow + 1, RERecord.CompetitionAccount, RERecord.Name, RERecord.PercentageOwned);
					if (lngTemp > lngRow)
					{
						lngRow = lngTemp;
						if (GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED) == string.Empty)
							GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED, FCConvert.ToString(false));
					}
					else
					{
						break;
					}
				}
			}
			// now fill the new row
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTEMPGUID, RERecord.GUID);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDACREAGE, FCConvert.ToString(RERecord.Acreage));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS1, RERecord.Address1);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS2, RERecord.Address2);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADEXEMPT, FCConvert.ToString(RERecord.BadExempt));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCConvert.ToString(RERecord.BadExemptAmount));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADMAPLOT, FCConvert.ToString(RERecord.BadMapLot));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE, FCConvert.ToString(RERecord.BldgValue));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBOOK, RERecord.Book);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTCITY, RERecord.City);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCConvert.ToString(RERecord.ExemptAmount1));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCConvert.ToString(RERecord.ExemptAmount2));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCConvert.ToString(RERecord.ExemptAmount3));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTCODE1, RERecord.ExemptCode1);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTCODE2, RERecord.ExemptCode2);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTCODE3, RERecord.ExemptCode3);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE, FCConvert.ToString(RERecord.LandValue));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTMAPLOT, RERecord.MapLot);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTNEVERUPDATED, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOWNER, RERecord.Name);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPHONENUMBER, RERecord.PhoneNumber);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOTHERAMOUNT, FCConvert.ToString(RERecord.OtherValue));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPAGE, RERecord.Page);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSECOWNER, RERecord.SecOwner);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTATE, RERecord.State);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREET, RERecord.StreetName);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREETNUM, FCConvert.ToString(RERecord.StreetNumber));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTOTALNET, FCConvert.ToString((RERecord.LandValue + RERecord.BldgValue) - (RERecord.ExemptAmount1 + RERecord.ExemptAmount2 + RERecord.ExemptAmount3)));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTOTALVALUE, FCConvert.ToString(RERecord.LandValue + RERecord.BldgValue));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUPDATED, FCConvert.ToString(Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUPDATED)) + 1));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTVISIONACCOUNT, RERecord.CompetitionAccount);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP, RERecord.Zip);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP4, RERecord.Zip4);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTMULTIOWNERPERCENT, FCConvert.ToString(RERecord.PercentageOwned));
			if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDACREAGE)) != Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDACREAGE)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDADDRESS1)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS1)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDADDRESS2)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS2)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDCITY)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTCITY)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDOWNER)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOWNER)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSECOWNER)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSECOWNER)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTATE)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTATE)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTREET)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREET)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTREETNUM)) != Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREETNUM)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDZIP)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			else if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDZIP4)) != fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP4)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(true));
			}
			if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE)) != Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALLAND)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASVALCHANGE, FCConvert.ToString(true));
			}
			else if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE)) != Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALBLDG)))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASVALCHANGE, FCConvert.ToString(true));
			}
			else if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALEXEMPTION)) != (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3))))
			{
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASVALCHANGE, FCConvert.ToString(true));
			}
		}

		private int FindInMultiList(string strMapLot, ref int lngAccount)
		{
			int FindInMultiList = 0;
			// returns -1 if not found otherwise returns the row
			int x;
			FindInMultiList = -1;
			if (gridMultiOwner.Rows == 0)
				return FindInMultiList;
			for (x = 0; x <= gridMultiOwner.Rows - 1; x++)
			{
				//App.DoEvents();
				if (Conversion.Val(gridMultiOwner.TextMatrix(x, modGridConstantsXF.CNSTGRIDMULTICOLACCOUNT)) == lngAccount || fecherFoundation.Strings.UCase(strMapLot) == fecherFoundation.Strings.UCase(gridMultiOwner.TextMatrix(x, modGridConstantsXF.CNSTGRIDMULTICOLMAPLOT)))
				{
					FindInMultiList = x;
					return FindInMultiList;
				}
			}
			// x
			return FindInMultiList;
		}

		private int FindInList(ref string strMapLot, int lngStart = 0, string strAccount = "", string strName = "", int lngPerc = 100)
		{
			int FindInList = 0;
			// searches through the grid fo find the maplot
			// returns -1 if not found
			int x;
			int y;
			FindInList = -1;
			if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByTRIOAccount && Conversion.Val(strAccount) == 0)
			{
				return FindInList;
			}
			if (!modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByAccount && !modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByTRIOAccount && lngPerc == 100)
			{
				// match by maplot
				for (x = lngStart; x <= GridLoad.Rows - 1; x++)
				{
					//App.DoEvents();
					if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT))) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(strMapLot)))
					{
						FindInList = x;
						return FindInList;
					}
				}
				// x
			}
			else
			{
				if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByAccount && lngPerc == 100)
				{
					// match by vision account
					for (x = lngStart; x <= GridLoad.Rows - 1; x++)
					{
						//App.DoEvents();
						if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT))) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(strAccount)))
						{
							// If lngPerc < 100 Then
							// If Trim(UCase(strName)) = Trim(UCase(GridLoad.TextMatrix(x, CNSTOWNER))) Then
							// FindInList = x
							// Exit Function
							// End If
							// Else
							FindInList = x;
							return FindInList;
							// End If
						}
					}
					// x
				}
				else
				{
					// match by trio account or is a multiowner. Multiowners must use trio account
					for (x = lngStart; x <= GridLoad.Rows - 1; x++)
					{
						//App.DoEvents();
						if (Conversion.Val(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTTRIOACCOUNT)) == Conversion.Val(strAccount))
						{
							if ((lngPerc == 100) || (Conversion.Val(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTUPDATED)) == 0))
							{
								FindInList = x;
								return FindInList;
							}
							else
							{
								// is a multiowner and the accounts are all the same.  Try to match by something else
								for (y = lngStart; y <= GridLoad.Rows - 1; y++)
								{
									//App.DoEvents();
									if ((fecherFoundation.Strings.UCase(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT)) == fecherFoundation.Strings.UCase(strMapLot)) && (fecherFoundation.Strings.UCase(strName) == fecherFoundation.Strings.UCase(GridLoad.TextMatrix(y, modGridConstantsXF.CNSTOWNER))) && (Conversion.Val(GridLoad.TextMatrix(y, modGridConstantsXF.CNSTUPDATED)) == 0) && y != x)
									{
										FindInList = y;
										return FindInList;
									}
								}
								// y
								return FindInList;
							}
						}
					}
					// x
				}
			}
			return FindInList;
		}

		private void FillGridLoad()
		{
			// fills the grid with all records in trio database
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngRow = 0;
			string strTemp = "";
			string strMasterJoin;
			strMasterJoin = modMainXF.GetREMasterJoin();
			clsLoad.OpenRecordset(strMasterJoin + " where rscard = 1 order by rsmaplot", "twre0000.vb1");
			GridLoad.Rows = 0;
			while (!clsLoad.EndOfFile())
			{
				//App.DoEvents();
				GridLoad.Rows += 1;
				lngRow = GridLoad.Rows - 1;
				InitializeGridLoadRow(ref lngRow);
				// fill in some data in case it isn't updated.
				// Call clsTemp.OpenRecordset("select * from phonenumbers where PARENTID = " & clsLoad.Fields("rsaccount"), "TWRE0000.VB1")
				// If Not clsTemp.EndOfFile Then
				// strTemp = Right("0000000000" & clsTemp.Fields("PHONENUMBER"), 10)
				// .TextMatrix(lngRow, CNSTPHONENUMBER) = Mid(strTemp, 1, 3) & "-" & Mid(strTemp, 4, 3) & "-" & Mid(strTemp, 7, 4)
				// End If
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTRIOACCOUNT, FCConvert.ToString(clsLoad.Get_Fields("rsaccount")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTMAPLOT, FCConvert.ToString(clsLoad.Get_Fields("rsmaplot")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOWNER, FCConvert.ToString(clsLoad.Get_Fields("rsname")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSECOWNER, FCConvert.ToString(clsLoad.Get_Fields("rssecowner")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOWNERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("ownerpartyid"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSECOWNERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("secownerpartyid"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS1, FCConvert.ToString(clsLoad.Get_Fields("rsaddr1")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS2, FCConvert.ToString(clsLoad.Get_Fields("rsaddr2")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREET, FCConvert.ToString(clsLoad.Get_Fields("rslocstreet")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREETNUM, FCConvert.ToString(clsLoad.Get_Fields("rslocnumalph")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTCITY, FCConvert.ToString(clsLoad.Get_Fields("rsaddr3")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTATE, FCConvert.ToString(clsLoad.Get_Fields("rsstate")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP, FCConvert.ToString(clsLoad.Get_Fields("rszip")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP4, FCConvert.ToString(clsLoad.Get_Fields("rszip4")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED, FCConvert.ToString(clsLoad.Get_Fields("rsdeleted")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("lastlandval"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("lastbldgval"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("ExemptVal1"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptval2"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptval3"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALLAND, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("lastlandval"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALBLDG, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("lastbldgval"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTORIGINALEXEMPTION, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("rlexemption"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDACREAGE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("piacres"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDADDRESS1, FCConvert.ToString(clsLoad.Get_Fields("rsaddr1")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDADDRESS2, FCConvert.ToString(clsLoad.Get_Fields("rsaddr2")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDCITY, FCConvert.ToString(clsLoad.Get_Fields("rsaddr3")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDOWNER, FCConvert.ToString(clsLoad.Get_Fields("rsname")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSECOWNER, FCConvert.ToString(clsLoad.Get_Fields("rssecowner")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDOWNERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("ownerpartyid"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSECOWNERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("secownerpartyid"))));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTATE, FCConvert.ToString(clsLoad.Get_Fields("rsstate")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTREET, FCConvert.ToString(clsLoad.Get_Fields("rslocstreet")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTREETNUM, FCConvert.ToString(clsLoad.Get_Fields("rslocnumalph")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDZIP, FCConvert.ToString(clsLoad.Get_Fields("rszip")));
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDZIP4, FCConvert.ToString(clsLoad.Get_Fields("rszip4")));
				if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByAccount)
				{
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTVISIONACCOUNT, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("rsref2"))));
				}
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTNEVERUPDATED, FCConvert.ToString(true));
				clsLoad.MoveNext();
			}
		}

		private void InitializeGridLoadRow(ref int lngRow)
		{
			// just puts intial values in row
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS1, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTADDRESS2, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADEXEMPT, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADMAPLOT, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBLDGVALUE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTCITY, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTCODE1, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTCODE2, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTEXEMPTCODE3, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDACREAGE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTLANDVALUE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTMAPLOT, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTNEVERUPDATED, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOWNER, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPHONENUMBER, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSECOWNER, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTATE, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREET, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSTREETNUM, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTOTALNET, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTOTALVALUE, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTRIOACCOUNT, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUPDATED, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTVISIONACCOUNT, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTZIP4, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDACREAGE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDADDRESS1, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDADDRESS2, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDBOOK, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDPAGE, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDCITY, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDOWNER, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSECOWNER, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTATE, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTREET, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSTREETNUM, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDZIP, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDZIP4, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASACHANGE, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTHASVALCHANGE, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOWNERID, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTSECOWNERID, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDOWNERID, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTOLDSECOWNERID, FCConvert.ToString(0));
		}

		private void SetupGridLoad()
		{
			// sets up grid load
			GridLoad.Rows = 0;
			GridLoad.Cols = modGridConstantsXF.CNSTNUMCOLS;
			GridLoad.ColDataType(modGridConstantsXF.CNSTBADEXEMPT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTBADMAPLOT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTBLDGVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTCITY, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE1, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE2, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE3, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTLANDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridLoad.ColDataType(modGridConstantsXF.CNSTLANDVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTMAPLOT, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTTOTALNET, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTTOTALVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTTRIOACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTVISIONACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTZIP, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTORIGINALBLDG, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTORIGINALEXEMPTION, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTORIGINALLAND, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDCITY, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDZIP, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDBOOK, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTOLDPAGE, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTHASACHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTHASVALCHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTMULTIOWNERNAME, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTMULTIOWNERPERCENT, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPHONENUMBER, FCGrid.DataTypeSettings.flexDTString);
		}

		private void SetupGridGood()
		{
			GridGood.Rows = 1;
			GridGood.Cols = modGridConstantsXF.CNSTNUMCOLS;
			GridGood.ColDataType(modGridConstantsXF.CNSTBADEXEMPT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTBADMAPLOT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTBLDGVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTCITY, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE1, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE2, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE3, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTLANDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridGood.ColDataType(modGridConstantsXF.CNSTLANDVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTMAPLOT, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTTOTALNET, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTTOTALVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTTRIOACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTVISIONACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTZIP, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTORIGINALLAND, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTORIGINALBLDG, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTORIGINALEXEMPTION, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDCITY, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDZIP, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDBOOK, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTOLDPAGE, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTHASACHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTHASVALCHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTMULTIOWNERNAME, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTMULTIOWNERPERCENT, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPHONENUMBER, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColHidden(modGridConstantsXF.CNSTMULTIOWNERPERCENT, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTHASVALCHANGE, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTHASACHANGE, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTMULTIOWNERNAME, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTLANDEXCEED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTBLDGEXCEED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTBADEXEMPT, true);
			// can't be here if it's bad
			GridGood.ColHidden(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTBADMAPLOT, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTDELETED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTNEVERUPDATED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTUPDATED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTUSEROW, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTORIGINALBLDG, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTORIGINALEXEMPTION, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTORIGINALLAND, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOTHERAMOUNT, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDOWNER, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDSECOWNER, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDADDRESS1, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDADDRESS2, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDCITY, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDSTATE, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDZIP, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDZIP4, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDSTREETNUM, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDSTREET, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDACREAGE, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDBOOK, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDPAGE, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOWNERID, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTSECOWNERID, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDOWNERID, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTOLDSECOWNERID, true);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTADDRESS1, "Address1");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTADDRESS2, "Address2");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTBLDGVALUE, "Building");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTCITY, "City");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT1, "Exempt1 Val");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT2, "Exempt2 Val");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT3, "Exempt3 Val");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE1, "Exempt1 Code");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE2, "Exempt2 Code");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE3, "Exempt3 Code");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTLANDACREAGE, "Acres");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTLANDVALUE, "Land");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTMAPLOT, "Map Lot");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTOWNER, "Owner");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTSECOWNER, "Second Owner");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTSTATE, "ST");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTSTREET, "Street Name");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTSTREETNUM, "ST #");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTTOTALNET, "Total Net");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTTOTALVALUE, "Total Assessed");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTTRIOACCOUNT, "Account");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTVISIONACCOUNT, "Other Account");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTZIP, "Zip");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTZIP4, "Zip 4");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTBOOK, "Book");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPAGE, "Page");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPHONENUMBER, "Phone");
		}

		private void SetupGridNew()
		{
			GridNew.Rows = 1;
			GridNew.Cols = modGridConstantsXF.CNSTNUMCOLS;
			GridNew.ColDataType(modGridConstantsXF.CNSTBADEXEMPT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTBADMAPLOT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTBLDGVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTCITY, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE1, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE2, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE3, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTLANDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridNew.ColDataType(modGridConstantsXF.CNSTLANDVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTMAPLOT, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTTOTALNET, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTTOTALVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTTRIOACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTVISIONACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTZIP, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTORIGINALLAND, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTORIGINALBLDG, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTORIGINALEXEMPTION, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTHASACHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTHASVALCHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTMULTIOWNERNAME, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTMULTIOWNERPERCENT, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPHONENUMBER, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColHidden(modGridConstantsXF.CNSTMULTIOWNERPERCENT, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTMULTIOWNERNAME, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTHASVALCHANGE, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTHASACHANGE, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDOWNER, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDSECOWNER, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDADDRESS1, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDADDRESS2, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDCITY, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDSTATE, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDZIP, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDZIP4, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDSTREETNUM, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDSTREET, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDACREAGE, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDBOOK, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDPAGE, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTLANDEXCEED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTBLDGEXCEED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTBADEXEMPT, true);
			// can't be here if it's bad
			GridNew.ColHidden(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTBADMAPLOT, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTDELETED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTNEVERUPDATED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTUPDATED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTORIGINALLAND, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTORIGINALBLDG, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTORIGINALEXEMPTION, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOTHERAMOUNT, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOWNERID, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTSECOWNERID, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDOWNERID, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTOLDSECOWNERID, true);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPHONENUMBER, "Phone");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTADDRESS1, "Address1");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTADDRESS2, "Address2");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTBLDGVALUE, "Building");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTCITY, "City");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT1, "Exempt1 Val");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT2, "Exempt2 Val");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT3, "Exempt3 Val");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE1, "Exempt1 Code");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE2, "Exempt2 Code");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE3, "Exempt3 Code");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTLANDACREAGE, "Acres");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTLANDVALUE, "Land");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTMAPLOT, "Map Lot");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTOWNER, "Owner");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTSECOWNER, "Second Owner");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTSTATE, "ST");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTSTREET, "Street Name");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTSTREETNUM, "ST #");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTTOTALNET, "Total Net");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTTOTALVALUE, "Total Assessed");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTTRIOACCOUNT, "Account");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTVISIONACCOUNT, "Other Account");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTZIP, "Zip");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTZIP4, "Zip 4");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTUSEROW, "Import");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTBOOK, "Book");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPAGE, "Page");
			GridNew.Row = 0;
			GridNew.Col = 1;
		}

		private void SetupGridBadAccounts()
		{
			GridBadAccounts.Rows = 1;
			GridBadAccounts.Cols = modGridConstantsXF.CNSTNUMCOLS;
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTBADEXEMPT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTBADMAPLOT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTBLDGVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTCITY, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE1, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE2, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE3, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTLANDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTLANDVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTMAPLOT, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTTOTALNET, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTTOTALVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTTRIOACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTVISIONACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTZIP, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTORIGINALLAND, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTORIGINALBLDG, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTORIGINALEXEMPTION, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTHASACHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTHASVALCHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTMULTIOWNERNAME, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTMULTIOWNERPERCENT, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPHONENUMBER, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTMULTIOWNERPERCENT, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTHASVALCHANGE, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTHASACHANGE, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTUSEROW, true);
			// can't use it if its bad
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTNEVERUPDATED, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTORIGINALLAND, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTORIGINALBLDG, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTORIGINALEXEMPTION, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTOTHERAMOUNT, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTLANDEXCEED, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTBLDGEXCEED, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTMULTIOWNERNAME, true);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPHONENUMBER, "Phone");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTADDRESS1, "Address1");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTADDRESS2, "Address2");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTBLDGVALUE, "Building");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTBOOK, "Book");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTCITY, "City");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT1, "Exempt1 Val");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT2, "Exempt2 Val");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT3, "Exempt3 Val");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE1, "Exempt1 Code");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE2, "Exempt2 Code");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE3, "Exempt3 Code");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTLANDACREAGE, "Acres");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTLANDVALUE, "Land");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTMAPLOT, "Map Lot");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTOWNER, "Owner");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPAGE, "Page");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTSECOWNER, "Second Owner");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTSTATE, "ST");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTSTREET, "Street Name");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTSTREETNUM, "ST #");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTTOTALNET, "Total Net");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTTOTALVALUE, "Total Assessed");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTTRIOACCOUNT, "Account");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTVISIONACCOUNT, "Other Account");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTZIP, "Zip");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTZIP4, "Zip 4");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTBADEXEMPT, "Bad Exempt");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTBADEXEMPTAMOUNT, "Bad Ex Amount");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTBADMAPLOT, "Bad MapLot");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTDELETED, "Deleted");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTUPDATED, "Times Updated");
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (GridBadAccounts.Cols - 1); x++)
			{
				switch (x)
				{
					case modGridConstantsXF.CNSTPHONENUMBER:
					case modGridConstantsXF.CNSTADDRESS1:
					case modGridConstantsXF.CNSTADDRESS2:
					case modGridConstantsXF.CNSTBLDGVALUE:
					case modGridConstantsXF.CNSTBOOK:
					case modGridConstantsXF.CNSTCITY:
						{
							break;
						}
					case modGridConstantsXF.CNSTEXEMPTAMOUNT1:
					case modGridConstantsXF.CNSTEXEMPTAMOUNT2:
					case modGridConstantsXF.CNSTEXEMPTAMOUNT3:
					case modGridConstantsXF.CNSTEXEMPTCODE1:
					case modGridConstantsXF.CNSTEXEMPTCODE2:
						{
							break;
						}
					case modGridConstantsXF.CNSTEXEMPTCODE3:
					case modGridConstantsXF.CNSTLANDACREAGE:
					case modGridConstantsXF.CNSTLANDVALUE:
					case modGridConstantsXF.CNSTMAPLOT:
					case modGridConstantsXF.CNSTOWNER:
					case modGridConstantsXF.CNSTPAGE:
					case modGridConstantsXF.CNSTSECOWNER:
						{
							break;
						}
					case modGridConstantsXF.CNSTSTATE:
					case modGridConstantsXF.CNSTSTREET:
					case modGridConstantsXF.CNSTSTREETNUM:
					case modGridConstantsXF.CNSTTOTALNET:
					case modGridConstantsXF.CNSTTOTALVALUE:
					case modGridConstantsXF.CNSTTRIOACCOUNT:
					case modGridConstantsXF.CNSTVISIONACCOUNT:
						{
							break;
						}
					case modGridConstantsXF.CNSTZIP:
					case modGridConstantsXF.CNSTZIP4:
					case modGridConstantsXF.CNSTBADEXEMPT:
					case modGridConstantsXF.CNSTBADEXEMPTAMOUNT:
					case modGridConstantsXF.CNSTBADMAPLOT:
					case modGridConstantsXF.CNSTDELETED:
					case modGridConstantsXF.CNSTUPDATED:
						{
							break;
						}
					default:
						{
							GridBadAccounts.ColHidden(x, true);
							break;
						}
				}
				//end switch
			}
			// x
		}

		private void SetupGridNotUpdated()
		{
			GridNotUpdated.Rows = 1;
			GridNotUpdated.Cols = modGridConstantsXF.CNSTNUMCOLS;
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTBADEXEMPT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTBADMAPLOT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTBLDGVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTCITY, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT1, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT2, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTEXEMPTAMOUNT3, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE1, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE2, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTEXEMPTCODE3, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTLANDACREAGE, FCGrid.DataTypeSettings.flexDTDouble);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTLANDVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTMAPLOT, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTSECOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTSTREETNUM, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTTOTALNET, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTTOTALVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTTRIOACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTVISIONACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTZIP, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTORIGINALLAND, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTORIGINALBLDG, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTORIGINALEXEMPTION, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTHASACHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTHASVALCHANGE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTMULTIOWNERNAME, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTMULTIOWNERPERCENT, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPHONENUMBER, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTMULTIOWNERPERCENT, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTMULTIOWNERNAME, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTHASVALCHANGE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTHASACHANGE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTBOOK, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPAGE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTLANDEXCEED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTBLDGEXCEED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTBADEXEMPT, true);
			// can't be here if it's bad
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTBADEXEMPTAMOUNT, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTBADMAPLOT, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTDELETED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTNEVERUPDATED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTUPDATED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTVISIONACCOUNT, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTUSEROW, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTEXEMPTAMOUNT1, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTEXEMPTAMOUNT2, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTEXEMPTAMOUNT3, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTEXEMPTCODE1, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTEXEMPTCODE2, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTEXEMPTCODE3, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTLANDACREAGE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTLANDVALUE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTTOTALNET, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTTOTALVALUE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTVISIONACCOUNT, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTBLDGVALUE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTORIGINALLAND, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTORIGINALBLDG, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTORIGINALEXEMPTION, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOTHERAMOUNT, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDOWNER, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDSECOWNER, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDADDRESS1, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDADDRESS2, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDCITY, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDSTATE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDZIP, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDZIP4, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDSTREETNUM, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDSTREET, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDACREAGE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDBOOK, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDPAGE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOWNERID, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTSECOWNERID, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDOWNERID, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTOLDSECOWNERID, true);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPHONENUMBER, "Phone");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTADDRESS1, "Address1");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTADDRESS2, "Address2");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTBLDGVALUE, "Building");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTCITY, "City");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT1, "Exempt1 Val");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT2, "Exempt2 Val");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTAMOUNT3, "Exempt3 Val");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE1, "Exempt1 Code");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE2, "Exempt2 Code");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTEXEMPTCODE3, "Exempt3 Code");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTLANDACREAGE, "Acres");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTLANDVALUE, "Land");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTMAPLOT, "Map Lot");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTOWNER, "Owner");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTSECOWNER, "Second Owner");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTSTATE, "ST");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTSTREET, "Street Name");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTSTREETNUM, "ST #");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTTOTALNET, "Total Net");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTTOTALVALUE, "Total Assessed");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTTRIOACCOUNT, "Account");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTVISIONACCOUNT, "Other Account");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTZIP, "Zip");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTZIP4, "Zip 4");
		}

		private void ProcessGridLoad()
		{
			int lngRow;
			int intWhichGrid;
			// 0 is good,1 is new,2 is untouched, 3 is bad
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngCurRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// check each record to see which table it belongs to
				for (lngRow = 0; lngRow <= GridLoad.Rows - 1; lngRow++)
				{
					intWhichGrid = CNSTGRIDGOOD;
					if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTTRIOACCOUNT)) < 1)
					{
						// new account
						intWhichGrid = CNSTGRIDNEW;
					}
					else if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUPDATED)) < 1)
					{
						intWhichGrid = CNSTGRIDUNTOUCHED;
					}
					// not all are mutually exclusive.  Could have new account that is bad
					if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUPDATED)) > 1)
					{
						intWhichGrid = CNSTGRIDBAD;
					}
					if (FCConvert.CBool(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADEXEMPT)))
					{
						intWhichGrid = CNSTGRIDBAD;
					}
					if (FCConvert.CBool(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADEXEMPTAMOUNT)))
					{
						intWhichGrid = CNSTGRIDBAD;
					}
					if (FCConvert.CBool(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTBADMAPLOT)))
					{
						intWhichGrid = CNSTGRIDBAD;
					}
					if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED)) != string.Empty)
					{
						if (FCConvert.CBool(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTDELETED)))
						{
							if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTUPDATED)) < 1)
							{
								intWhichGrid = CNSTGRIDNONE;
								// just drop it. This is a deleted account that wasn't updated
							}
							else
							{
								intWhichGrid = CNSTGRIDBAD;
							}
						}
					}
					if (intWhichGrid == CNSTGRIDGOOD)
					{
						GridGood.Rows += 1;
						lngCurRow = GridGood.Rows - 1;
						// copy over to another table
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							//App.DoEvents();
							GridGood.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
						GridGood.TextMatrix(lngCurRow, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(true));
					}
					else if (intWhichGrid == CNSTGRIDNEW)
					{
						GridNew.Rows += 1;
						lngCurRow = GridNew.Rows - 1;
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							//App.DoEvents();
							GridNew.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
						GridNew.TextMatrix(lngCurRow, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(false));
					}
					else if (intWhichGrid == CNSTGRIDUNTOUCHED)
					{
						GridNotUpdated.Rows += 1;
						lngCurRow = GridNotUpdated.Rows - 1;
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							//App.DoEvents();
							GridNotUpdated.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
					}
					else if (intWhichGrid == CNSTGRIDBAD)
					{
						GridBadAccounts.Rows += 1;
						lngCurRow = GridBadAccounts.Rows - 1;
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							//App.DoEvents();
							GridBadAccounts.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
					}
					else if (intWhichGrid == CNSTGRIDNONE)
					{
						// don't do anything
					}
				}
				// lngRow
				// clear the grid
				GridLoad.Rows = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ProcessGridLoad", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPrintAccountsToUpdate_Click(object sender, System.EventArgs e)
		{
			if (GridGood.Rows < 2)
			{
				MessageBox.Show("There are no accounts to update", "No Updated Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				UpdatePercChanges();
			}
			frmReportViewer.InstancePtr.Init(rptUpdatedAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "AccountsToUpdate");
		}

		private void mnuPrintBad_Click(object sender, System.EventArgs e)
		{
			if (GridBadAccounts.Rows < 2)
			{
				MessageBox.Show("There are no bad accounts to report", "No Bad Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptBadAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "BadAccounts");
		}

		private void mnuPrintChanges_Click(object sender, System.EventArgs e)
		{
			if (GridGood.Rows < 2)
			{
				MessageBox.Show("There are no changed accounts", "No Changed Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptChangedAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "Changes");
		}

		private void mnuPrintNew_Click(object sender, System.EventArgs e)
		{
			if (GridNew.Rows < 2)
			{
				MessageBox.Show("There are no new accounts to report", "No New Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptNewAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "NewAccounts");
		}

		private void mnuPrintNotUpdated_Click(object sender, System.EventArgs e)
		{
			if (GridNotUpdated.Rows < 2)
			{
				MessageBox.Show("There are no accounts that were not updated", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptNotUpdatedAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "NotUpdated");
		}

		private void mnuPrintSummary_Click(object sender, System.EventArgs e)
		{
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				UpdatePercChanges();
			}
			rptRESummary.InstancePtr.Init(chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// perform the actual transfer
			if (cmbUpdateType.ItemData(cmbUpdateType.SelectedIndex) == CNSTCMBUPDATEAMOUNTS)
			{
				boolXferAmounts = true;
				boolXferInfoOnly = false;
			}
			else if (cmbUpdateType.ItemData(cmbUpdateType.SelectedIndex) == CNSTCMBUPDATEINFOONLY)
			{
				boolXferAmounts = false;
				boolXferInfoOnly = true;
			}
			if (TransferToRE())
			{
				//mnuExit_Click();
				return;
			}
		}

		private void txtPercentChg_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				UpdatePercChanges();
			}
		}

		private bool TransferToRE()
		{
			bool TransferToRE = false;
			// performs the actual transfer
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x = 0;
			int lngAcct = 0;
			int lngExempt1 = 0;
			int lngExempt2 = 0;
			int lngExempt3 = 0;
			int lngExemption = 0;
			string strSQL = "";
			bool boolMatch = false;
			// vbPorter upgrade warning: intLine As int	OnWriteFCConvert.ToDouble(
			int intLine = 0;
			int[] intAry = new int[50];
			string strTemp = "";
			int intTemp;
			int lngHomestead = 0;
			int intCD1 = 0;
			int intCD2 = 0;
			int intCD3 = 0;
			int y;
			// vbPorter upgrade warning: lngAcctToUse As int	OnWrite(double, int)
			int lngAcctToUse = 0;
			int lngTempRow = 0;
			// vbPorter upgrade warning: lngTempLand As int	OnWrite(int, double)
			int lngTempLand = 0;
			// vbPorter upgrade warning: lngTempBldg As int	OnWrite(int, double)
			int lngTempBldg = 0;
			// vbPorter upgrade warning: lngTempExempt As int	OnWrite(int, double)
			int lngTempExempt = 0;
			int lngTempExempt1 = 0;
			int lngTempExempt2 = 0;
			int lngTempExempt3 = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp = 0;
			int lngPercentToUse = 0;
			int lngSaleIndex;
			cParty tParty;
			cPartyController tPartyCont = new cPartyController();
			cParty tempParty;
			cPartyAddress tAddr;
			bool boolAddAddress = false;
			FCCollection vArray = new FCCollection();
			cPartyPhoneNumber tPhone;
			string[] tempArray = null;
			// vbPorter upgrade warning: intIndex As int	OnWriteFCConvert.ToInt32(
			int intIndex = 0;
			cParty testParty;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				TransferToRE = false;
				tPartyCont.ConnectionName = "CentralParties";
				Array.Resize(ref arParties, 0 + 1);
				if (boolXferAmounts)
				{
					modGlobalFunctions.AddCYAEntry_6("RE", "Transferred info and amounts to RE from other assessment prog");
					clsTemp.Execute("update master set lastlandval= 0,lastbldgval = 0,rlexemption = 0,rsothervalue = 0,rssoftvalue = 0,rsmixedvalue = 0,rshardvalue = 0 where rscard > 1", "twre0000.vb1");
					clsTemp.OpenRecordset("select * from EXEMPTCODE WHERE Category = " + FCConvert.ToString(CNSTHOMESTEADCODE), "twre0000.vb1");
					x = 1;
					while (!clsTemp.EndOfFile())
					{
						intAry[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("code"))));
						x += 1;
						clsTemp.MoveNext();
					}
				}
				else if (boolXferInfoOnly)
				{
					modGlobalFunctions.AddCYAEntry_6("RE", "Transferred info only to RE from other assessment prog");
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmWait.InstancePtr.Init("Please Wait", false, 100, true);
				// sort the grid so that we can do a find next
				GridGood.Col = modGridConstantsXF.CNSTTRIOACCOUNT;
				GridGood.RowSel = GridGood.Row;
				GridGood.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				GridNew.Col = modGridConstantsXF.CNSTMAPLOT;
				GridNew.RowSel = GridNew.Row;
				GridNew.Sort = FCGrid.SortSettings.flexSortStringAscending;
				clsSave.OpenRecordset("select * from master where rscard = 1 order by rsaccount", "twre0000.vb1");
				// go through updated accounts first
				for (x = 1; x <= GridGood.Rows - 1; x++)
				{
					//App.DoEvents();
					lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTTRIOACCOUNT))));
					boolAddAddress = false;
					frmWait.InstancePtr.lblMessage.Text = "Updating " + FCConvert.ToString(lngAcct);
					frmWait.InstancePtr.lblMessage.Refresh();
					if (x > 1)
					{
						if (clsSave.FindNextRecord("rsaccount", lngAcct))
						{
							// should never fail since these are in the good grid
							clsSave.Edit();
						}
					}
					else
					{
						if (clsSave.FindFirstRecord("rsaccount", lngAcct))
						{
							// should never fail since these are in the good grid
							clsSave.Edit();
						}
					}
					if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOLDOWNER))) != fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOWNER))))
					{
						if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOWNER))) != "")
						{
							// Set tParty = New cParty
							vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOWNER)), false);
							if (vArray.Count >= 1)
							{
								// If UBound(vArray) >= 0 Then
								tempParty = vArray[1];
								tParty = new cParty();
								if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
								{
									if (tempParty.PartyType == 0)
									{
										if (tempParty.LastName == "")
										{
											if (Strings.InStr(1, tempParty.FirstName, ",", CompareConstants.vbBinaryCompare) <= 0)
											{
												intIndex = Strings.InStr(1, tempParty.FirstName, " ", CompareConstants.vbBinaryCompare);
												if (intIndex > 0)
												{
													if (intIndex + 1 < tempParty.FirstName.Length)
													{
														vArray = tPU.SplitName(Strings.Mid(tempParty.FirstName, 1, intIndex) + "," + Strings.Mid(tempParty.FirstName, intIndex + 1), false);
														tempParty = vArray[1];
													}
												}
											}
										}
									}
								}
								tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
								tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								tParty.Designation = tempParty.Designation;
								tParty.FirstName = tempParty.FirstName;
								tParty.LastName = tempParty.LastName;
								tParty.MiddleName = tempParty.MiddleName;
								tParty.PartyGUID = tempParty.PartyGUID;
								tParty.PartyType = tempParty.PartyType;
								tAddr = new cPartyAddress();
								tAddr.AddressType = "Primary";
								tAddr.Address1 = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1));
								tAddr.Address2 = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2));
								tAddr.Address3 = "";
								tAddr.City = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTCITY));
								tAddr.State = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSTATE));
								tAddr.Zip = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP));
								if (fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4)) != "")
								{
									tAddr.Zip = tAddr.Zip + " " + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4));
								}
								tParty.Addresses.Add(tAddr);
								if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LEBANON")
								{
									strTemp = GridGood.TextMatrix(x, modGridConstantsXF.CNSTPHONENUMBER);
									// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
									if (strTemp != string.Empty)
									{
										tPhone = new cPartyPhoneNumber();
										tPhone.PhoneOrder = 1;
										tPhone.PhoneNumber = strTemp;
										tParty.PhoneNumbers.Add(tPhone);
									}
								}
								testParty = tPartyCont.FindFirstMatchingParty(ref tParty);
								if (!(testParty == null))
								{
									if (!tPartyCont.PartiesHaveSameAddress(ref tParty, ref testParty))
									{
										tPartyCont.SaveParty(ref tParty);
									}
									else
									{
										tParty = testParty;
									}
								}
								else
								{
									tPartyCont.SaveParty(ref tParty);
								}
								clsSave.Set_Fields("OwnerPartyID", tParty.ID);
								Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
								arParties[Information.UBound(arParties)] = tParty.ID;
							}
							else
							{
								clsSave.Set_Fields("OwnerPartyID", 0);
							}
						}
						else
						{
							clsSave.Set_Fields("OwnerpartyID", 0);
						}
					}
					else
					{
						// same person but maybe different address
						if (!modGlobalVariablesXF.Statics.CustomizedInfo.UsePartyAddressFromSystem)
						{
							if (Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOWNERID)) > 0)
							{
								tParty = tPartyCont.GetParty(FCConvert.ToInt32(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOWNERID)));
								if (!(tParty == null))
								{
									if (tParty.Addresses.Count > 0)
									{
										boolAddAddress = true;
										foreach (cPartyAddress tAddr_foreach in tParty.Addresses)
										{
											tAddr = tAddr_foreach;
											if (fecherFoundation.Strings.Trim(tAddr.Address1) != "" && fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.Address1)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1))))
											{
												if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.Address2)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2))))
												{
													if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.City)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTCITY))))
													{
														if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.State)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSTATE))))
														{
															if (tAddr.Zip == GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP) || tAddr.Zip == GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP) + "-" + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4)))
															{
																boolAddAddress = false;
																break;
															}
														}
													}
												}
											}
											tAddr = null;
										}
									}
									else
									{
										boolAddAddress = true;
									}
									if (boolAddAddress)
									{
										foreach (cPartyAddress tAddr_foreach in tParty.Addresses)
										{
											tAddr = tAddr_foreach;
											if (tAddr.AddressType == "Primary")
											{
												tAddr.Address1 = GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1);
												tAddr.Address2 = GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2);
												tAddr.Address3 = "";
												tAddr.City = GridGood.TextMatrix(x, modGridConstantsXF.CNSTCITY);
												tAddr.State = GridGood.TextMatrix(x, modGridConstantsXF.CNSTSTATE);
												tAddr.Zip = GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP);
												if (fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4)) != "")
												{
													tAddr.Zip = tAddr.Zip + "-" + GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4);
												}
												break;
											}
											tAddr = null;
										}
										tPartyCont.SaveParty(ref tParty);
									}
								}
							}
						}
					}
					if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTOLDSECOWNER))) != fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER))))
					{
						if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER))) != "")
						{
							vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER)), false);
							// If UBound(vArray) >= 0 Then
							if (vArray.Count >= 1)
							{
								tempParty = vArray[1];
								tParty = new cParty();
								if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
								{
									if (tempParty.PartyType == 0)
									{
										if (tempParty.LastName == "")
										{
											if (Strings.InStr(1, tempParty.FirstName, ",", CompareConstants.vbBinaryCompare) <= 0)
											{
												intIndex = Strings.InStr(1, tempParty.FirstName, " ", CompareConstants.vbBinaryCompare);
												if (intIndex > 0)
												{
													if (intIndex + 1 < tempParty.FirstName.Length)
													{
														vArray = tPU.SplitName(Strings.Mid(tempParty.FirstName, 1, intIndex) + "," + Strings.Mid(tempParty.FirstName, intIndex + 1), false);
														tempParty = vArray[1];
													}
												}
											}
										}
									}
								}
								tParty.PartyGUID = tempParty.PartyGUID;
								tParty.PartyType = tempParty.PartyType;
								tParty.FirstName = tempParty.FirstName;
								tParty.MiddleName = tempParty.MiddleName;
								tParty.LastName = tempParty.LastName;
								tParty.Designation = tempParty.Designation;
								tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
								tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								tPartyCont.SaveParty(ref tParty);
								clsSave.Set_Fields("SecOwnerPartyID", tParty.ID);
								Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
								arParties[Information.UBound(arParties)] = tParty.ID;
							}
							else
							{
								clsSave.Set_Fields("SecOwnerPartyID", 0);
							}
						}
						else
						{
							clsSave.Set_Fields("SecOwnerPartyID", 0);
						}
					}
					clsSave.Set_Fields("rsmaplot", fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT)));
					clsSave.Set_Fields("rslocnumalph", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSTREETNUM))));
					clsSave.Set_Fields("rslocstreet", fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTSTREET)));
					clsSave.Set_Fields("HLUpdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByAccount)
					{
						clsSave.Set_Fields("RSref2", fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT)));
					}
					if (fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) != "" && fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) != "")
					{
						// check to see if the book page is already recorded
						clsTemp.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct) + " order by line", "twre0000.vb1");
						if (clsTemp.EndOfFile())
						{
							// none at all
							strSQL = "insert into bookpage (book,page,account,card,line,SALEDATE,[current]) values (";
							strSQL += "'" + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) + "'";
							strSQL += ",'" + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) + "'";
							strSQL += "," + FCConvert.ToString(lngAcct);
							strSQL += ",1";
							strSQL += ",1";
							strSQL += ",0";
							strSQL += ",1";
							strSQL += ")";
							clsTemp.Execute(strSQL, "twre0000.vb1");
						}
						else
						{
							boolMatch = false;
							while (!clsTemp.EndOfFile())
							{
								if ((fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields("book"))) == fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBOOK))) && (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields("page"))) == fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPAGE))))
								{
									boolMatch = true;
									break;
								}
								clsTemp.MoveNext();
							}
							if (!boolMatch)
							{
								clsTemp.MoveLast();
								intLine = FCConvert.ToInt16(Conversion.Val(clsTemp.Get_Fields("line")) + 1);
								clsTemp.Execute("update bookpage set [current] = 0 where account = " + FCConvert.ToString(lngAcct), "twre0000.vb1");
								strSQL = "insert into bookpage (book,page,account,card,line,SALEDATE,[current]) values (";
								strSQL += "'" + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) + "'";
								strSQL += ",'" + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) + "'";
								strSQL += "," + FCConvert.ToString(lngAcct);
								strSQL += ",1";
								strSQL += "," + FCConvert.ToString(intLine);
								strSQL += ",0";
								strSQL += ",1";
								strSQL += ")";
								clsTemp.Execute(strSQL, "twre0000.vb1");
							}
						}
					}
					if (boolXferAmounts)
					{
						lngExempt1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1))));
						lngExempt2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2))));
						lngExempt3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3))));
						lngExemption = lngExempt1 + lngExempt2 + lngExempt3;
						if (modGlobalVariablesXF.Statics.CustomizedInfo.boolUseMultiOwner && modGlobalVariablesXF.Statics.CustomizedInfo.boolSplitValuation)
						{
							if (Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTMULTIOWNERPERCENT)) < 100 && Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTMULTIOWNERPERCENT)) > 0)
							{
								lngTempRow = FindInMultiList(GridGood.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT), ref lngAcct);
								if (lngTempRow == -1)
								{
									gridMultiOwner.Rows += 1;
									lngTempRow = gridMultiOwner.Rows - 1;
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLACCOUNT, FCConvert.ToString(lngAcct));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLPERCENT, FCConvert.ToString(100));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLMAPLOT, GridGood.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG, FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE))));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND, FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT, FCConvert.ToString(lngExemption));
								}
								lngPercentToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTMULTIOWNERPERCENT))));
								if (lngPercentToUse > Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLPERCENT)))
								{
									lngPercentToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLPERCENT))));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLPERCENT, FCConvert.ToString(0));
									lngTempLand = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND))));
									lngTempBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG))));
									lngTempExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT))));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND, FCConvert.ToString(0));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG, FCConvert.ToString(0));
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT, FCConvert.ToString(0));
								}
								else
								{
									gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLPERCENT, FCConvert.ToString(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLPERCENT)) - lngPercentToUse));
									lngTempLand = FCConvert.ToInt32((lngPercentToUse / 100.0) * Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE)));
									lngTempBldg = FCConvert.ToInt32((lngPercentToUse / 100.0) * Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE)));
									lngTempExempt = FCConvert.ToInt32((lngPercentToUse / 100.0) * lngExemption);
									if (lngTempLand > Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND)))
									{
										lngTempLand = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND))));
										gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND, FCConvert.ToString(0));
									}
									else
									{
										gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND, FCConvert.ToString(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLLAND)) - lngTempLand));
									}
									if (lngTempBldg > Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG)))
									{
										lngTempBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG))));
										gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG, FCConvert.ToString(0));
									}
									else
									{
										gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG, FCConvert.ToString(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLBLDG)) - lngTempBldg));
									}
									if (lngTempExempt > Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT)))
									{
										lngTempExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT))));
										gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT, FCConvert.ToString(0));
									}
									else
									{
										gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT, FCConvert.ToString(Conversion.Val(gridMultiOwner.TextMatrix(lngTempRow, modGridConstantsXF.CNSTGRIDMULTICOLEXEMPT)) - lngTempExempt));
									}
								}
								if (lngTempExempt == 0)
								{
									lngTempExempt1 = 0;
									lngTempExempt2 = 0;
									lngTempExempt3 = 0;
								}
								else
								{
									if (lngTempExempt1 > 0)
									{
										if (lngTempExempt2 == 0 && lngTempExempt3 == 0)
										{
											lngTempExempt1 = lngTempExempt;
										}
										else
										{
											lngTemp = FCConvert.ToInt32((lngPercentToUse / 100.0) * lngTempExempt1);
											lngTempExempt1 = lngTemp;
											if (lngTempExempt2 > 0)
											{
												if (lngTempExempt3 == 0)
												{
													lngTempExempt2 = lngTempExempt - lngTemp;
												}
												else
												{
													lngTemp = FCConvert.ToInt32((lngPercentToUse / 100.0) * lngTempExempt2);
													lngTempExempt2 = lngTemp;
													lngTempExempt3 = lngTempExempt - lngTempExempt1 - lngTempExempt2;
												}
											}
										}
									}
								}
								lngExemption = lngTempExempt;
								lngExempt1 = lngTempExempt1;
								lngExempt2 = lngTempExempt2;
								lngExempt3 = lngTempExempt3;
								GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE, FCConvert.ToString(lngTempLand));
								GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE, FCConvert.ToString(lngTempBldg));
							}
						}
						if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "SABATTUS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "CAMDEN")
						{
							clsSave.Set_Fields("homesteadvalue", 0);
							clsSave.Set_Fields("hashomestead", false);
							lngHomestead = 0;
							clsSave.Set_Fields("rlexemption", lngExemption);
							clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
							clsSave.Set_Fields("riexemptcd2", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
							clsSave.Set_Fields("riexemptcd3", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
							clsSave.Set_Fields("ExemptVal1", lngExempt1);
							clsSave.Set_Fields("ExemptVal2", lngExempt2);
							clsSave.Set_Fields("ExemptVal3", lngExempt3);
							clsSave.Set_Fields("exemptpct1", 100);
							clsSave.Set_Fields("exemptpct2", 100);
							clsSave.Set_Fields("exemptpct3", 100);
							intCD1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
							intCD2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
							intCD3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
							for (y = 1; y < intAry.Length; y++)
							{
								if (intCD1 == intAry[y] && intCD1 > 0)
								{
									lngHomestead += lngExempt1;
									clsSave.Set_Fields("hashomestead", true);
								}
								if (intCD2 == intAry[y] && intCD2 > 0)
								{
									lngHomestead += lngExempt2;
									clsSave.Set_Fields("hashomestead", true);
								}
								if (intCD3 == intAry[y] && intCD3 > 0)
								{
									lngHomestead += lngExempt3;
									clsSave.Set_Fields("hashomestead", true);
								}
							}
							// y
							clsSave.Set_Fields("homesteadvalue", lngHomestead);
						}
						else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SABATTUS")
						{
							// allow only the totally exempts
							clsSave.Set_Fields("homesteadvalue", 0);
							clsSave.Set_Fields("hashomestead", false);
							lngHomestead = 0;
							if (Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1)) == 98)
							{
								clsSave.Set_Fields("rlexemption", lngExemption);
								clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
								clsSave.Set_Fields("Exemptval1", lngExempt1);
								clsSave.Set_Fields("riexemptcd2", 0);
								clsSave.Set_Fields("riexemptcd3", 0);
								clsSave.Set_Fields("exemptval2", 0);
								clsSave.Set_Fields("exemptval3", 0);
								clsSave.Set_Fields("homesteadvalue", 0);
								clsSave.Set_Fields("hashomestead", false);
								clsSave.Set_Fields("exemptpct1", 100);
								clsSave.Set_Fields("exemptpct2", 100);
								clsSave.Set_Fields("exemptpct3", 100);
							}
							else
							{
								clsSave.Set_Fields("rlexemption", lngExemption);
								clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
								clsSave.Set_Fields("riexemptcd2", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
								clsSave.Set_Fields("riexemptcd3", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
								clsSave.Set_Fields("ExemptVal1", lngExempt1);
								clsSave.Set_Fields("ExemptVal2", lngExempt2);
								clsSave.Set_Fields("ExemptVal3", lngExempt3);
								clsSave.Set_Fields("exemptpct1", 100);
								clsSave.Set_Fields("exemptpct2", 100);
								clsSave.Set_Fields("exemptpct3", 100);
								intCD1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
								intCD2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
								intCD3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
								for (y = 1; y < intAry.Length; y++)
								{
									if (intCD1 == intAry[y] && intCD1 > 0)
									{
										lngHomestead += lngExempt1;
										clsSave.Set_Fields("hashomestead", true);
									}
									if (intCD2 == intAry[y] && intCD2 > 0)
									{
										lngHomestead += lngExempt2;
										clsSave.Set_Fields("hashomestead", true);
									}
									if (intCD3 == intAry[y] && intCD3 > 0)
									{
										lngHomestead += lngExempt3;
										clsSave.Set_Fields("hashomestead", true);
									}
								}
								// y
								clsSave.Set_Fields("homesteadvalue", lngHomestead);
							}
						}
						clsSave.Set_Fields("lastlandval", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
						clsSave.Set_Fields("lastbldgval", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE))));
						clsSave.Set_Fields("piacres", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDACREAGE))));
						clsSave.Set_Fields("RsOther", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDACREAGE))));
						clsSave.Set_Fields("rsothervalue", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
						clsSave.Set_Fields("rssoft", 0);
						clsSave.Set_Fields("rssoftvalue", 0);
						clsSave.Set_Fields("rshard", 0);
						clsSave.Set_Fields("rshardvalue", 0);
						clsSave.Set_Fields("rsmixed", 0);
						clsSave.Set_Fields("rsmixedvalue", 0);
					}
					clsSave.Update();
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolImportPreviousOwner)
					{
						for (lngSaleIndex = 0; lngSaleIndex <= gridPreviousOwners.Rows - 1; lngSaleIndex++)
						{
							if (gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLGUID) == GridGood.TextMatrix(x, modGridConstantsXF.CNSTTEMPGUID))
							{
								if (fecherFoundation.Strings.Trim(gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLOWNER)) != "")
								{
									if (Information.IsDate(gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLSALEDATE)))
									{
										AddPrevOwnerRec(gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLSALEDATE), gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLOWNER), lngAcct);
									}
								}
							}
						}
						// lngSaleIndex
					}
				}
				// x
				// now add all the new accounts
				// there is no matching, only adding
				if (GridNew.Rows > 1)
				{
					if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "westbrook" || Conversion.Val(GridNew.TextMatrix(1, modGridConstantsXF.CNSTVISIONACCOUNT)) == 0)
					{
						clsSave.OpenRecordset("select max(rsaccount) as maxacct from master", "twre0000.vb1");
						if (!clsSave.EndOfFile())
						{
							lngAcctToUse = FCConvert.ToInt32(Conversion.Val(clsSave.Get_Fields("maxacct")) + 1);
						}
						else
						{
							lngAcctToUse = 1;
						}
					}
					else
					{
						if (x > 1)
						{
							lngAcctToUse = FCConvert.ToInt32(Conversion.Val(GridGood.TextMatrix(x - 1, modGridConstantsXF.CNSTVISIONACCOUNT)) + 1);
						}
						else
						{
							lngAcctToUse = 1;
						}
					}
				}
				clsSave.OpenRecordset("select * from master where rsaccount = -99", "twre0000.vb1");
				for (x = 1; x <= GridNew.Rows - 1; x++)
				{
					//App.DoEvents();
					if (FCConvert.CBool(GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW)))
					{
						if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "westbrook" && Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT)) > 0)
						{
							lngAcctToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT))));
						}
						lngAcct = lngAcctToUse;
						if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByTRIOAccount)
						{
							if (Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT)) != 0)
							{
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT))));
			}
							clsTemp.OpenRecordset("select rsaccount from master where rsaccount = " + FCConvert.ToString(lngAcct), "twre0000.vb1");
							if (clsTemp.EndOfFile() && lngAcct > 0)
							{
								// doesn't exist. It is safe to leave lngacct alone and create this account
							}
							else
							{
								// already exists or is 0. Can't create a new one with this account number
								// 10/11/2005. Can't take max anymore
								lngAcctToUse = GetNextAccountToCreate();
								lngAcct = lngAcctToUse;
								// Call clsTemp.OpenRecordset("select max(rsaccount) as maxacct from master", "twre0000.vb1")
								// If Not clsTemp.EndOfFile Then
								// lngAcctToUse = Val(clsTemp.Fields("maxacct")) + 1
								// Else
								// lngAcctToUse = 1
								// End If
								// lngAcct = lngAcctToUse
							}
						}
						frmWait.InstancePtr.lblMessage.Text = "Updating " + FCConvert.ToString(lngAcct);
						frmWait.InstancePtr.lblMessage.Refresh();
						clsSave.AddNew();
						// extra stuff to do since this is a new account
						if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LEBANON")
						{
							// Call clsTemp.Execute("delete from phonenumbers where PARENTID = " & lngAcct, "TWRE0000.vb1")
							// strTemp = .TextMatrix(x, CNSTPHONENUMBER)
							// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
							// If strTemp <> vbNullString Then
							// clsSave.Fields("rstelephone") = strTemp
							// Call clsTemp.Execute("insert into phonenumbers (Phonecode,Parentid,PhoneNumber) values (0," & lngAcct & ",'" & strTemp & "')", "twre0000.vb1")
							// End If
						}
						clsSave.Set_Fields("rsaccount", lngAcct);
						GridNew.TextMatrix(x, modGridConstantsXF.CNSTTRIOACCOUNT, FCConvert.ToString(lngAcct));
						clsSave.Set_Fields("AccountID", GridNew.TextMatrix(x, modGridConstantsXF.CNSTTEMPGUID));
						clsSave.Set_Fields("CardID", GridNew.TextMatrix(x, modGridConstantsXF.CNSTTEMPGUID));
						clsSave.Set_Fields("rscard", 1);
						clsSave.Set_Fields("rsdeleted", false);
						clsSave.Set_Fields("DateCreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
						if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTOWNER))) != "")
						{
							// Set tParty = New cParty
							vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTOWNER)), false);
							if (vArray.Count >= 1)
							{
								// If UBound(vArray) >= 0 Then
								tempParty = vArray[1];
								tParty = new cParty();
								if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
								{
									if (tempParty.PartyType == 0)
									{
										if (tempParty.LastName == "")
										{
											if (Strings.InStr(1, tempParty.FirstName, ",", CompareConstants.vbBinaryCompare) <= 0)
											{
												intIndex = Strings.InStr(1, tempParty.FirstName, " ", CompareConstants.vbBinaryCompare);
												if (intIndex > 0)
												{
													if (intIndex + 1 < tempParty.FirstName.Length)
													{
														vArray = tPU.SplitName(Strings.Mid(tempParty.FirstName, 1, intIndex) + "," + Strings.Mid(tempParty.FirstName, intIndex + 1), false);
														tempParty = vArray[1];
													}
												}
											}
										}
									}
								}
								tParty.FirstName = tempParty.FirstName;
								tParty.LastName = tempParty.LastName;
								tParty.MiddleName = tempParty.MiddleName;
								tParty.Designation = tempParty.Designation;
								tParty.PartyGUID = tempParty.PartyGUID;
								tParty.PartyType = tempParty.PartyType;
								// Set tParty = vArray(0)
								tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
								tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								tAddr = new cPartyAddress();
								tAddr.AddressType = "Primary";
								tAddr.Address1 = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1));
								tAddr.Address2 = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2));
								tAddr.Address3 = "";
								tAddr.City = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTCITY));
								tAddr.State = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTSTATE));
								tAddr.Zip = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTZIP));
								if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTZIP4)) != "")
								{
									tAddr.Zip = tAddr.Zip + " " + fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTZIP4));
								}
								tParty.Addresses.Add(tAddr);
								if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LEBANON")
								{
									strTemp = GridNew.TextMatrix(x, modGridConstantsXF.CNSTPHONENUMBER);
									// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
									if (strTemp != string.Empty)
									{
										tPhone = new cPartyPhoneNumber();
										tPhone.PhoneOrder = 1;
										tPhone.PhoneNumber = strTemp;
										tParty.PhoneNumbers.Add(tPhone);
									}
								}
								tPartyCont.SaveParty(ref tParty);
								clsSave.Set_Fields("OwnerPartyID", tParty.ID);
								Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
								arParties[Information.UBound(arParties)] = tParty.ID;
							}
							else
							{
								clsSave.Set_Fields("OwnerPartyID", 0);
							}
						}
						else
						{
							clsSave.Set_Fields("OwnerpartyID", 0);
						}
						if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER))) != "")
						{
							vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER)), false);
							if (vArray.Count >= 1)
							{
								// If UBound(vArray) >= 0 Then
								tempParty = vArray[1];
								tParty = new cParty();
								if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
								{
									if (tempParty.PartyType == 0)
									{
										if (tempParty.LastName == "")
										{
											if (Strings.InStr(1, tempParty.FirstName, ",", CompareConstants.vbBinaryCompare) <= 0)
											{
												intIndex = Strings.InStr(1, tempParty.FirstName, " ", CompareConstants.vbBinaryCompare);
												if (intIndex > 0)
												{
													if (intIndex + 1 < tempParty.FirstName.Length)
													{
														vArray = tPU.SplitName(Strings.Mid(tempParty.FirstName, 1, intIndex) + "," + Strings.Mid(tempParty.FirstName, intIndex + 1), false);
														tempParty = vArray[1];
													}
												}
											}
										}
									}
								}
								tParty.PartyGUID = tempParty.PartyGUID;
								tParty.PartyType = tempParty.PartyType;
								tParty.FirstName = tempParty.FirstName;
								tParty.MiddleName = tempParty.MiddleName;
								tParty.LastName = tempParty.LastName;
								tParty.Designation = tempParty.Designation;
								tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
								tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								tPartyCont.SaveParty(ref tParty);
								clsSave.Set_Fields("SecOwnerPartyID", tParty.ID);
								Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
								arParties[Information.UBound(arParties)] = tParty.ID;
							}
							else
							{
								clsSave.Set_Fields("SecOwnerPartyID", 0);
							}
						}
						else
						{
							clsSave.Set_Fields("SecOwnerPartyID", 0);
						}
						clsSave.Set_Fields("rsmaplot", fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT)));
						clsSave.Set_Fields("rslocnumalph", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTSTREETNUM))));
						clsSave.Set_Fields("rslocstreet", fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTSTREET)));
						clsSave.Set_Fields("HLUpdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
						if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByAccount)
						{
							clsSave.Set_Fields("RSref2", fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT)));
						}
						clsSave.Set_Fields("Hivalbldgcode", 1);
						clsSave.Set_Fields("HivalLandcode", 1);
						// clsSave.Fields("rstelephone") = "0000000000"
						clsSave.Set_Fields("saledate", null);
						clsSave.Set_Fields("inbankruptcy", false);
						clsSave.Set_Fields("taxacquired", false);
						clsSave.Set_Fields("dateinspected", null);
						clsSave.Set_Fields("entrancecode", 0);
						clsSave.Set_Fields("informationcode", 0);
						clsSave.Set_Fields("rspropertycode", 0);
						clsSave.Set_Fields("rsDwellingCode", "N");
						clsSave.Set_Fields("rsoutbuildingcode", "-");
						if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) != "" && fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) != "")
						{
							// check to see if the book page is already recorded
							clsTemp.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct) + " order by line", "twre0000.vb1");
							if (clsTemp.EndOfFile())
							{
								// none at all
								strSQL = "insert into bookpage (book,page,account,card,line,SALEDATE,[current]) values (";
								strSQL += "'" + fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) + "'";
								strSQL += ",'" + fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) + "'";
								strSQL += "," + FCConvert.ToString(lngAcct);
								strSQL += ",1";
								strSQL += ",1";
								strSQL += ",0";
								strSQL += ",1";
								strSQL += ")";
								clsTemp.Execute(strSQL, "twre0000.vb1");
							}
							else
							{
								boolMatch = false;
								while (!clsTemp.EndOfFile())
								{
									if ((fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields("book"))) == fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTBOOK))) && (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields("page"))) == fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPAGE))))
									{
										boolMatch = true;
										break;
									}
									clsTemp.MoveNext();
								}
								if (!boolMatch)
								{
									clsTemp.MoveLast();
									intLine = FCConvert.ToInt16(Conversion.Val(clsTemp.Get_Fields("line")) + 1);
									clsTemp.Execute("update bookpage set [current] = 0 where account = " + FCConvert.ToString(lngAcct), "twre0000.vb1");
									strSQL = "insert into bookpage (book,page,account,card,line,SALEDATE,[current]) values (";
									strSQL += "'" + fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) + "'";
									strSQL += ",'" + fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) + "'";
									strSQL += "," + FCConvert.ToString(lngAcct);
									strSQL += ",1";
									strSQL += "," + FCConvert.ToString(intLine);
									strSQL += ",0";
									strSQL += ",1";
									strSQL += ")";
									clsTemp.Execute(strSQL, "twre0000.vb1");
								}
							}
						}
						if (boolXferAmounts)
						{
							lngExempt1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1))));
							lngExempt2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2))));
							lngExempt3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3))));
							lngExemption = lngExempt1 + lngExempt2 + lngExempt3;
							clsSave.Set_Fields("lastlandval", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
							clsSave.Set_Fields("lastbldgval", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE))));
							clsSave.Set_Fields("piacres", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDACREAGE))));
							clsSave.Set_Fields("RsOther", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDACREAGE))));
							clsSave.Set_Fields("rsothervalue", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
							clsSave.Set_Fields("rssoft", 0);
							clsSave.Set_Fields("rssoftvalue", 0);
							clsSave.Set_Fields("rshard", 0);
							clsSave.Set_Fields("rshardvalue", 0);
							clsSave.Set_Fields("rsmixed", 0);
							clsSave.Set_Fields("rsmixedvalue", 0);
							if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "SABATTUS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "CAMDEN")
							{
								clsSave.Set_Fields("homesteadvalue", 0);
								clsSave.Set_Fields("hashomestead", false);
								clsSave.Set_Fields("rlexemption", lngExemption);
								clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
								clsSave.Set_Fields("riexemptcd2", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
								clsSave.Set_Fields("riexemptcd3", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
								clsSave.Set_Fields("ExemptVal1", lngExempt1);
								clsSave.Set_Fields("ExemptVal2", lngExempt2);
								clsSave.Set_Fields("ExemptVal3", lngExempt3);
								clsSave.Set_Fields("EXEMPTPCT1", 100);
								clsSave.Set_Fields("EXEMPTPCT2", 100);
								clsSave.Set_Fields("EXEMPTPCT3", 100);
								lngHomestead = 0;
								intCD1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
								intCD2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
								intCD3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
								for (y = 1; y < intAry.Length; y++)
								{
									if (intCD1 == intAry[y] && intCD1 > 0)
									{
										lngHomestead += lngExempt1;
										clsSave.Set_Fields("hashomestead", true);
									}
									if (intCD2 == intAry[y] && intCD2 > 0)
									{
										lngHomestead += lngExempt2;
										clsSave.Set_Fields("hashomestead", true);
									}
									if (intCD3 == intAry[y] && intCD3 > 0)
									{
										lngHomestead += lngExempt3;
										clsSave.Set_Fields("hashomestead", true);
									}
								}
								// y
								clsSave.Set_Fields("homesteadvalue", lngHomestead);
							}
							else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SABATTUS")
							{
								// allow only the totally exempts
								clsSave.Set_Fields("homesteadvalue", 0);
								clsSave.Set_Fields("hashomestead", false);
								if (Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1)) == 98)
								{
									clsSave.Set_Fields("rlexemption", lngExemption);
									clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
									clsSave.Set_Fields("Exemptval1", lngExempt1);
									clsSave.Set_Fields("riexemptcd2", 0);
									clsSave.Set_Fields("riexemptcd3", 0);
									clsSave.Set_Fields("exemptval2", 0);
									clsSave.Set_Fields("exemptval3", 0);
									clsSave.Set_Fields("exemptpct1", 100);
									clsSave.Set_Fields("exemptpct2", 100);
									clsSave.Set_Fields("exemptpct3", 100);
									clsSave.Set_Fields("homesteadvalue", 0);
									clsSave.Set_Fields("hashomestead", false);
								}
								else
								{
									clsSave.Set_Fields("rlexemption", lngExemption);
									clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
									clsSave.Set_Fields("riexemptcd2", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
									clsSave.Set_Fields("riexemptcd3", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
									clsSave.Set_Fields("ExemptVal1", lngExempt1);
									clsSave.Set_Fields("ExemptVal2", lngExempt2);
									clsSave.Set_Fields("ExemptVal3", lngExempt3);
									clsSave.Set_Fields("EXEMPTPCT1", 100);
									clsSave.Set_Fields("EXEMPTPCT2", 100);
									clsSave.Set_Fields("EXEMPTPCT3", 100);
									lngHomestead = 0;
									intCD1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
									intCD2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
									intCD3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
									for (y = 1; y < intAry.Length; y++)
									{
										if (intCD1 == intAry[y] && intCD1 > 0)
										{
											lngHomestead += lngExempt1;
											clsSave.Set_Fields("hashomestead", true);
										}
										if (intCD2 == intAry[y] && intCD2 > 0)
										{
											lngHomestead += lngExempt2;
											clsSave.Set_Fields("hashomestead", true);
										}
										if (intCD3 == intAry[y] && intCD3 > 0)
										{
											lngHomestead += lngExempt3;
											clsSave.Set_Fields("hashomestead", true);
										}
									}
									// y
								}
								clsSave.Set_Fields("homesteadvalue", lngHomestead);
							}
						}
						clsSave.Update();
						if (modGlobalVariablesXF.Statics.CustomizedInfo.boolImportPreviousOwner)
						{
							for (lngSaleIndex = 0; lngSaleIndex <= gridPreviousOwners.Rows - 1; lngSaleIndex++)
							{
								if (gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLGUID) == GridNew.TextMatrix(x, modGridConstantsXF.CNSTTEMPGUID))
								{
									if (fecherFoundation.Strings.Trim(gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLOWNER)) != "")
									{
										if (Information.IsDate(gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLSALEDATE)))
										{
											AddPrevOwnerRec(gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLSALEDATE), gridPreviousOwners.TextMatrix(lngSaleIndex, modGridConstantsXF.CNSTGRIDPREVOCOLOWNER), lngAcct);
										}
									}
								}
							}
							// lngSaleIndex
						}
						GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(false));
						lngAcctToUse += 1;
					}
				}
				// x
				if (chkMarkDeleted.CheckState == Wisej.Web.CheckState.Checked)
				{
					frmWait.InstancePtr.Unload();
					if (MessageBox.Show("Are you sure you want to mark all not updated accounts as deleted?" + "\r\n" + "They can be undeleted individually at any time.", "Delete Not Updated Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("RE", "TWXF deleted accounts that were not updated");
						frmWait.InstancePtr.Init("Deleting", false, 100, true);
						for (x = 1; x <= GridNotUpdated.Rows - 1; x++)
						{
							//App.DoEvents();
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTTRIOACCOUNT))));
							frmWait.InstancePtr.lblMessage.Text = "Deleting " + FCConvert.ToString(lngAcct);
							frmWait.InstancePtr.lblMessage.Refresh();
							clsSave.Execute("Update master set rsdeleted = 1 where rsaccount = " + FCConvert.ToString(lngAcct), "TWRE0000.vb1", false);
							clsSave.Execute("delete from summrecord where SRECORDNUMBER = " + FCConvert.ToString(lngAcct), "twre0000.vb1", false);
							// Call clsSave.Execute("delete from phonenumbers where PARENTID = " & lngAcct, "twre0000.vb1")
						}
						// x
					}
				}
				if (chkUndeleteAccounts.CheckState == Wisej.Web.CheckState.Checked)
				{
					frmWait.InstancePtr.Unload();
					GridBadAccounts.Col = modGridConstantsXF.CNSTTRIOACCOUNT;
					GridBadAccounts.RowSel = GridBadAccounts.Row;
					GridBadAccounts.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					clsSave.OpenRecordset("select * from master where rscard = 1 order by rsaccount", "twre0000.vb1");
					if (MessageBox.Show("Are you sure you want to un-delete accounts marked deleted in TRIO but not in the import file?", "Un-Delete Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("RE", "TWXF Undeleted Accounts that were not deleted in import file");
						frmWait.InstancePtr.Init("Un-Deleting", false, 100, true);
						for (x = 1; x <= GridBadAccounts.Rows - 1; x++)
						{
							//App.DoEvents();
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTTRIOACCOUNT))));
							if (FCConvert.CBool(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTDELETED)))
							{
								if (!(FCConvert.CBool(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBADEXEMPT)) || FCConvert.CBool(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBADEXEMPTAMOUNT)) || FCConvert.CBool(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBADMAPLOT)) || Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTUPDATED)) > 1))
								{
									// deleted was the only error
									frmWait.InstancePtr.lblMessage.Text = "Un-Deleting " + FCConvert.ToString(lngAcct);
									frmWait.InstancePtr.lblMessage.Refresh();
									if (x > 1)
									{
										if (clsSave.FindNextRecord("rsaccount", lngAcct))
										{
											// should never fail
											clsSave.Edit();
										}
									}
									else
									{
										if (clsSave.FindFirstRecord("rsaccount", lngAcct))
										{
											// should never fail
											clsSave.Edit();
										}
									}
									clsSave.Set_Fields("rsdeleted", false);
									if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOLDOWNER))) != fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOWNER))))
									{
										if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOWNER))) != "")
										{
											// Set tParty = New cParty
											vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOWNER)), false);
											if (vArray.Count > 1)
											{
												// If UBound(vArray) > 0 Then
												tempParty = vArray[1];
												tParty = new cParty();
												if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
												{
													if (tempParty.PartyType == 0)
													{
														if (tempParty.LastName == "")
														{
															if (Strings.InStr(1, tempParty.FirstName, ",", CompareConstants.vbBinaryCompare) <= 0)
															{
																intIndex = Strings.InStr(1, tempParty.FirstName, " ", CompareConstants.vbBinaryCompare);
																if (intIndex > 0)
																{
																	if (intIndex + 1 < tempParty.FirstName.Length)
																	{
																		vArray = tPU.SplitName(Strings.Mid(tempParty.FirstName, 1, intIndex) + "," + Strings.Mid(tempParty.FirstName, intIndex + 1), false);
																		tempParty = vArray[1];
																	}
																}
															}
														}
													}
												}
												tParty.PartyGUID = tempParty.PartyGUID;
												tParty.PartyType = tempParty.PartyType;
												tParty.FirstName = tempParty.FirstName;
												tParty.MiddleName = tempParty.MiddleName;
												tParty.LastName = tempParty.LastName;
												tParty.Designation = tempParty.Designation;
												tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
												tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
												tAddr = new cPartyAddress();
												tAddr.AddressType = "Primary";
												tAddr.Address1 = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1));
												tAddr.Address2 = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2));
												tAddr.Address3 = "";
												tAddr.City = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTCITY));
												tAddr.State = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSTATE));
												tAddr.Zip = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTZIP));
												if (fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTZIP4)) != "")
												{
													tAddr.Zip = tAddr.Zip + " " + fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTZIP4));
												}
												tParty.Addresses.Add(tAddr);
												if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LEBANON")
												{
													strTemp = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPHONENUMBER);
													// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
													if (strTemp != string.Empty)
													{
														tPhone = new cPartyPhoneNumber();
														tPhone.PhoneOrder = 1;
														tPhone.PhoneNumber = strTemp;
														tParty.PhoneNumbers.Add(tPhone);
													}
												}
												tPartyCont.SaveParty(ref tParty);
												clsSave.Set_Fields("OwnerPartyID", tParty.ID);
												Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
												arParties[Information.UBound(arParties)] = tParty.ID;
											}
											else
											{
												clsSave.Set_Fields("OwnerPartyID", 0);
											}
										}
										else
										{
											clsSave.Set_Fields("OwnerpartyID", 0);
										}
									}
									else
									{
										// same person but maybe different address
										if (Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOWNERID)) > 0)
										{
											tParty = tPartyCont.GetParty(FCConvert.ToInt32(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOWNERID)));
											if (!(tParty == null))
											{
												if (tParty.Addresses.Count > 0)
												{
													boolAddAddress = true;
													foreach (cPartyAddress tAddr_foreach in tParty.Addresses)
													{
														tAddr = tAddr_foreach;
														if (fecherFoundation.Strings.Trim(tAddr.Address1) != "" && fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.Address1)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1))))
														{
															boolAddAddress = false;
															break;
														}
														tAddr = null;
													}
												}
												else
												{
													boolAddAddress = true;
												}
												if (boolAddAddress)
												{
													tAddr = new cPartyAddress();
													tAddr.Address1 = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1);
													tAddr.Address2 = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2);
													tAddr.Address3 = "";
													tAddr.City = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTCITY);
													tAddr.State = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSTATE);
													tAddr.Zip = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTZIP);
													if (fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTZIP4)) != "")
													{
														tAddr.Zip = tAddr.Zip + "-" + GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTZIP4);
													}
													if (tParty.Addresses.Count > 0)
													{
														tAddr.ProgModule = "RE";
														tAddr.ModAccountID = lngAcct;
														tAddr.AddressType = "Override";
													}
												}
											}
										}
									}
									if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTOLDSECOWNER))) != fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER))))
									{
										if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER))) != "")
										{
											vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSECOWNER)), false);
											if (vArray.Count > 1)
											{
												// If UBound(vArray) > 0 Then
												tempParty = vArray[1];
												tParty = new cParty();
												if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "gardiner")
												{
													if (tempParty.PartyType == 0)
													{
														if (tempParty.LastName == "")
														{
															if (Strings.InStr(1, tempParty.FirstName, ",", CompareConstants.vbBinaryCompare) <= 0)
															{
																intIndex = Strings.InStr(1, tempParty.FirstName, " ", CompareConstants.vbBinaryCompare);
																if (intIndex > 0)
																{
																	if (intIndex + 1 < tempParty.FirstName.Length)
																	{
																		vArray = tPU.SplitName(Strings.Mid(tempParty.FirstName, 1, intIndex) + "," + Strings.Mid(tempParty.FirstName, intIndex + 1), false);
																		tempParty = vArray[1];
																	}
																}
															}
														}
													}
												}
												tParty.PartyGUID = tempParty.PartyGUID;
												tParty.PartyType = tempParty.PartyType;
												tParty.FirstName = tempParty.FirstName;
												tParty.MiddleName = tempParty.MiddleName;
												tParty.LastName = tempParty.LastName;
												tParty.Designation = tempParty.Designation;
												tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
												tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
												tPartyCont.SaveParty(ref tParty);
												clsSave.Set_Fields("SecOwnerPartyID", tParty.ID);
												Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
												arParties[Information.UBound(arParties)] = tParty.ID;
											}
											else
											{
												clsSave.Set_Fields("SecOwnerPartyID", 0);
											}
										}
										else
										{
											clsSave.Set_Fields("SecOwnerPartyID", 0);
										}
									}
									clsSave.Set_Fields("rsmaplot", fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTMAPLOT)));
									clsSave.Set_Fields("rslocnumalph", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSTREETNUM))));
									clsSave.Set_Fields("rslocstreet", fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTSTREET)));
									clsSave.Set_Fields("HLUpdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
									if (modGlobalVariablesXF.Statics.CustomizedInfo.boolMatchByAccount)
									{
										clsSave.Set_Fields("RSref2", fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTVISIONACCOUNT)));
									}
									if (fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) != "" && fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) != "")
									{
										// check to see if the book page is already recorded
										clsTemp.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct) + " order by line", "twre0000.vb1");
										if (clsTemp.EndOfFile())
										{
											// none at all
											strSQL = "insert into bookpage (book,page,account,card,line,SALEDATE,[current]) values (";
											strSQL += "'" + fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) + "'";
											strSQL += ",'" + fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) + "'";
											strSQL += "," + FCConvert.ToString(lngAcct);
											strSQL += ",1";
											strSQL += ",1";
											strSQL += ",0";
											strSQL += ",1";
											strSQL += ")";
											clsTemp.Execute(strSQL, "twre0000.vb1");
										}
										else
										{
											boolMatch = false;
											while (!clsTemp.EndOfFile())
											{
												if ((fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields("book"))) == fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBOOK))) && (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields("page"))) == fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPAGE))))
												{
													boolMatch = true;
													break;
												}
												clsTemp.MoveNext();
											}
											if (!boolMatch)
											{
												clsTemp.MoveLast();
												intLine = FCConvert.ToInt16(Conversion.Val(clsTemp.Get_Fields("line")) + 1);
												clsTemp.Execute("update bookpage set [current] = 0 where account = " + FCConvert.ToString(lngAcct), "twre0000.vb1");
												strSQL = "insert into bookpage (book,page,account,card,line,SALEDATE,[current]) values (";
												strSQL += "'" + fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBOOK)) + "'";
												strSQL += ",'" + fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPAGE)) + "'";
												strSQL += "," + FCConvert.ToString(lngAcct);
												strSQL += ",1";
												strSQL += "," + FCConvert.ToString(intLine);
												strSQL += ",0";
												strSQL += ",1";
												strSQL += ")";
												clsTemp.Execute(strSQL, "twre0000.vb1");
											}
										}
									}
									if (boolXferAmounts)
									{
										lngExempt1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1))));
										lngExempt2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2))));
										lngExempt3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3))));
										lngExemption = lngExempt1 + lngExempt2 + lngExempt3;
										if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "SABATTUS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "CAMDEN")
										{
											clsSave.Set_Fields("homesteadvalue", 0);
											clsSave.Set_Fields("hashomestead", false);
											lngHomestead = 0;
											clsSave.Set_Fields("rlexemption", lngExemption);
											clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
											clsSave.Set_Fields("riexemptcd2", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
											clsSave.Set_Fields("riexemptcd3", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
											clsSave.Set_Fields("ExemptVal1", lngExempt1);
											clsSave.Set_Fields("ExemptVal2", lngExempt2);
											clsSave.Set_Fields("ExemptVal3", lngExempt3);
											clsSave.Set_Fields("exemptpct1", 100);
											clsSave.Set_Fields("exemptpct2", 100);
											clsSave.Set_Fields("exemptpct3", 100);
											intCD1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
											intCD2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
											intCD3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
											for (y = 1; y < intAry.Length; y++)
											{
												if (intCD1 == intAry[y] && intCD1 > 0)
												{
													lngHomestead += lngExempt1;
													clsSave.Set_Fields("hashomestead", true);
												}
												if (intCD2 == intAry[y] && intCD2 > 0)
												{
													lngHomestead += lngExempt2;
													clsSave.Set_Fields("hashomestead", true);
												}
												if (intCD3 == intAry[y] && intCD3 > 0)
												{
													lngHomestead += lngExempt3;
													clsSave.Set_Fields("hashomestead", true);
												}
											}
											// y
											clsSave.Set_Fields("homesteadvalue", lngHomestead);
										}
										else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SABATTUS")
										{
											// allow only the totally exempts
											clsSave.Set_Fields("homesteadvalue", 0);
											clsSave.Set_Fields("hashomestead", false);
											lngHomestead = 0;
											if (Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1)) == 98)
											{
												clsSave.Set_Fields("rlexemption", lngExemption);
												clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
												clsSave.Set_Fields("Exemptval1", lngExemption);
												clsSave.Set_Fields("riexemptcd2", 0);
												clsSave.Set_Fields("riexemptcd3", 0);
												clsSave.Set_Fields("exemptval2", 0);
												clsSave.Set_Fields("exemptval3", 0);
												clsSave.Set_Fields("exemptpct1", 100);
												clsSave.Set_Fields("exemptpct2", 100);
												clsSave.Set_Fields("exemptpct3", 100);
												clsSave.Set_Fields("homesteadvalue", 0);
												clsSave.Set_Fields("hashomestead", false);
											}
											else
											{
												clsSave.Set_Fields("rlexemption", lngExemption);
												clsSave.Set_Fields("riexemptcd1", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
												clsSave.Set_Fields("riexemptcd2", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
												clsSave.Set_Fields("riexemptcd3", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
												clsSave.Set_Fields("ExemptVal1", lngExempt1);
												clsSave.Set_Fields("ExemptVal2", lngExempt2);
												clsSave.Set_Fields("ExemptVal3", lngExempt3);
												clsSave.Set_Fields("exemptpct1", 100);
												clsSave.Set_Fields("exemptpct2", 100);
												clsSave.Set_Fields("exemptpct3", 100);
												intCD1 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE1))));
												intCD2 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE2))));
												intCD3 = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTCODE3))));
												for (y = 1; y < intAry.Length; y++)
												{
													if (intCD1 == intAry[y] && intCD1 > 0)
													{
														lngHomestead += lngExempt1;
														clsSave.Set_Fields("hashomestead", true);
													}
													if (intCD2 == intAry[y] && intCD2 > 0)
													{
														lngHomestead += lngExempt2;
														clsSave.Set_Fields("hashomestead", true);
													}
													if (intCD3 == intAry[y] && intCD3 > 0)
													{
														lngHomestead += lngExempt3;
														clsSave.Set_Fields("hashomestead", true);
													}
												}
												// y
												clsSave.Set_Fields("homesteadvalue", lngHomestead);
											}
										}
										clsSave.Set_Fields("lastlandval", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
										clsSave.Set_Fields("lastbldgval", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE))));
										clsSave.Set_Fields("piacres", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTLANDACREAGE))));
										clsSave.Set_Fields("RsOther", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTLANDACREAGE))));
										clsSave.Set_Fields("rsothervalue", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
										clsSave.Set_Fields("rssoft", 0);
										clsSave.Set_Fields("rssoftvalue", 0);
										clsSave.Set_Fields("rshard", 0);
										clsSave.Set_Fields("rshardvalue", 0);
										clsSave.Set_Fields("rsmixed", 0);
										clsSave.Set_Fields("rsmixedvalue", 0);
									}
									clsSave.Update();
								}
							}
						}
						// x
					}
				}
				frmWait.InstancePtr.Unload();
				TransferToRE = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Transfer Complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return TransferToRE;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In TransferToRE", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return TransferToRE;
		}

		private void AddPrevOwnerRec(string strDate, string strName, int lngAccount)
		{
			if (Information.IsDate(strDate))
			{
				if (lngAccount > 0)
				{
					if (strName != "")
					{
						clsDRWrapper rsSave = new clsDRWrapper();
						rsSave.OpenRecordset("select * from previousOWNER where account = " + FCConvert.ToString(lngAccount) + " and saledate = #" + strDate + "#", "twre0000.vb1");
						if (rsSave.EndOfFile())
						{
							rsSave.AddNew();
							rsSave.Set_Fields("Account", lngAccount);
							rsSave.Set_Fields("Name", strName);
							rsSave.Set_Fields("DateCreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							rsSave.Set_Fields("SaleDate", strDate);
							rsSave.Update();
						}
					}
				}
			}
		}

		public int GetNextAccountToCreate()
		{
			int GetNextAccountToCreate = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x = 0;
			int lngLastAccount;
			GetNextAccountToCreate = 0;
			clsLoad.OpenRecordset("select rsaccount from master where rsaccount > 0 group by rsaccount order by rsaccount", "twre0000.vb1");
			lngLastAccount = 0;
			while (!clsLoad.EndOfFile())
			{
				x = lngLastAccount + 1;
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "BANGOR" || (x < 10000 || x > 10999))
				{
					if (lngLastAccount + 1 != FCConvert.ToInt32(clsLoad.Get_Fields("rsaccount")))
					{
						GetNextAccountToCreate = lngLastAccount + 1;
						return GetNextAccountToCreate;
					}
				}
				lngLastAccount = FCConvert.ToInt32(clsLoad.Get_Fields("rsaccount"));
				clsLoad.MoveNext();
			}
			if (lngLastAccount == 0)
			{
				GetNextAccountToCreate = 1;
			}
			else
			{
				GetNextAccountToCreate = lngLastAccount + 1;
			}
			return GetNextAccountToCreate;
		}
	}
}
