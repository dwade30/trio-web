﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLandCodes.
	/// </summary>
	partial class rptLandCodes
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLandCodes));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtLandCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAsessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtLandCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAsessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLandCode,
				this.txtCount,
				this.txtAsessment,
				this.txtAcres
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtLandCode
			// 
			this.txtLandCode.CanGrow = false;
			this.txtLandCode.Height = 0.19F;
			this.txtLandCode.Left = 0F;
			this.txtLandCode.MultiLine = false;
			this.txtLandCode.Name = "txtLandCode";
			this.txtLandCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtLandCode.Text = "Field1";
			this.txtLandCode.Top = 0.03125F;
			this.txtLandCode.Width = 2F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 3.6875F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtCount.Text = "Field2";
			this.txtCount.Top = 0.03125F;
			this.txtCount.Width = 0.625F;
			// 
			// txtAsessment
			// 
			this.txtAsessment.Height = 0.19F;
			this.txtAsessment.Left = 4.6875F;
			this.txtAsessment.Name = "txtAsessment";
			this.txtAsessment.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAsessment.Text = "Field3";
			this.txtAsessment.Top = 0.03125F;
			this.txtAsessment.Width = 1.5F;
			// 
			// txtAcres
			// 
			this.txtAcres.Height = 0.19F;
			this.txtAcres.Left = 6.25F;
			this.txtAcres.Name = "txtAcres";
			this.txtAcres.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAcres.Text = "Field4";
			this.txtAcres.Top = 0.03125F;
			this.txtAcres.Width = 1.1875F;
			// 
			// rptLandCodes
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtLandCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAsessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAsessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcres;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
