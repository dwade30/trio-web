﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cStoryHeightController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cStoryHeightMultiplier GetStoryHeightMultiplier(int lngId)
		{
			cStoryHeightMultiplier GetStoryHeightMultiplier = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cStoryHeightMultiplier heightMult = null;
				rsLoad.OpenRecordset("select * from StoryHeightMultipliers where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					heightMult = new cStoryHeightMultiplier();
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					heightMult.SectionID = FCConvert.ToInt32(rsLoad.Get_Fields("SectionID"));
					// TODO Get_Fields: Field [SqftMultiplier] not found!! (maybe it is an alias?)
					heightMult.SqftMultiplier = rsLoad.Get_Fields("SqftMultiplier");
					heightMult.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Field [WallHeight] not found!! (maybe it is an alias?)
					heightMult.WallHeight = rsLoad.Get_Fields("WallHeight");
					heightMult.IsUpdated = false;
				}
				GetStoryHeightMultiplier = heightMult;
				return GetStoryHeightMultiplier;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetStoryHeightMultiplier;
		}

		public void DeleteStoryHeightMultiplier(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from StoryHeightMultipliers where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveStoryHeightMultiplier(ref cStoryHeightMultiplier shMultiplier)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (shMultiplier.IsDeleted)
				{
					DeleteStoryHeightMultiplier(shMultiplier.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from storyheightmultipliers where id = " + shMultiplier.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (shMultiplier.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Story height multiplier record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("SectionID", shMultiplier.SectionID);
				rsSave.Set_Fields("Multiplier", shMultiplier.SqftMultiplier);
				rsSave.Set_Fields("WallHeight", shMultiplier.WallHeight);
				rsSave.Update();
				shMultiplier.ID = rsSave.Get_Fields_Int32("ID");
				shMultiplier.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
