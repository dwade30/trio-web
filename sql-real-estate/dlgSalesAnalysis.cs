﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for dlgSalesAnalysis.
	/// </summary>
	public partial class dlgSalesAnalysis : BaseForm
	{
		public dlgSalesAnalysis()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            //FC:FINAL:AM:#3359 - allow only digits
            this.txtPercent.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static dlgSalesAnalysis InstancePtr
		{
			get
			{
				return (dlgSalesAnalysis)Sys.GetInstance(typeof(dlgSalesAnalysis));
			}
		}

		protected dlgSalesAnalysis _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intWhich;

		private void cmdCancel_Click()
		{
			this.Unload();
			return;
		}

		private void cmdOkay_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolCorr = false;
			int lngUID;
			if (cmbval.Text == "Billing")
			{
				modRegistry.SaveRegistryKey("SalesAnalysisValues", "Billing", "RE");
			}
			else
			{
				modRegistry.SaveRegistryKey("SalesAnalysisValues", "Current", "RE");
			}
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			clsTemp.OpenRecordset("Select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0", modGlobalVariables.strREDatabase);
			if (cmbmean.Text == "Mean")
			{
				intWhich = 1;
			}
			else if (cmbmean.Text == "Median")
			{
				intWhich = 2;
			}
			else if (cmbmean.Text == "Mid-Quartile Mean")
			{
				intWhich = 3;
			}
			else if (cmbmean.Text == "Weighted Mean")
			{
				intWhich = 4;
			}
			if (cmbval.Text == "Billing")
			{
				boolCorr = false;
			}
			else if (cmbval.Text == "Correlated")
			{
				boolCorr = true;
			}
			if (!clsTemp.EndOfFile())
			{
				if (Strings.UCase(FCConvert.ToString(clsTemp.GetData("exttype"))) == "SALE")
				{
					// Call frmGetGroupsToPrint.Show(vbModal)
					frmShowSalesAnalysis.InstancePtr.Init(ref boolCorr, FCConvert.ToInt32(Conversion.Val(txtSaleFactor.Text)), FCConvert.ToInt32(Conversion.Val(txtLandFactor.Text)), FCConvert.ToInt32(Conversion.Val(txtBldgFactor.Text)), FCConvert.ToInt32(Conversion.Val(txtPercent.Text)), ref intWhich, cmbValidity.ItemData(cmbValidity.SelectedIndex));
                    //FC:FINAL:AM:#3368 - update client side in order to activate the correct form
                    FCUtils.ApplicationUpdate(App.MainForm);
                    this.Unload();
					return;
				}
				else
				{
					MessageBox.Show("There is an extract, but it was performed on regular files, not sales files." + "\r\n" + "Please create another extract and make sure to choose for sales analysis.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Unload();
					return;
				}
			}
			else
			{
				MessageBox.Show("There is no current extract to use." + "\r\n" + "Please create an extract for listings/labels and make sure you choose for sales analysis.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Unload();
				return;
			}
		}

		public void cmdOkay_Click()
		{
			cmdOkay_Click(cmdOkay, new System.EventArgs());
		}

		private void dlgSalesAnalysis_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void dlgSalesAnalysis_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//dlgSalesAnalysis properties;
			//dlgSalesAnalysis.ScaleWidth	= 5910;
			//dlgSalesAnalysis.ScaleHeight	= 4125;
			//dlgSalesAnalysis.LinkTopic	= "Form1";
			//dlgSalesAnalysis.LockControls	= true;
			//End Unmaped Properties
			txtPercent.Text = "15";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			FillValidityCombo();
			string strTemp;
			strTemp = modRegistry.GetRegistryKey("SalesAnalysisValues", "RE");
			if (Strings.UCase(strTemp) == "BILLING")
			{
				cmbval.Text = "Billing";
			}
			else
			{
				cmbval.Text = "Correlated";
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdOkay_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
			return;
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void txtBldgFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBldgFactor.Text) <= 0)
			{
				txtBldgFactor.Text = FCConvert.ToString(0);
			}
			if (Conversion.Val(txtBldgFactor.Text) > 400)
			{
				txtBldgFactor.Text = FCConvert.ToString(400);
			}
		}

		private void txtLandFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtLandFactor.Text) <= 0)
			{
				txtLandFactor.Text = FCConvert.ToString(0);
			}
			if (Conversion.Val(txtLandFactor.Text) > 400)
			{
				txtLandFactor.Text = FCConvert.ToString(400);
			}
		}

		private void txtSaleFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtSaleFactor.Text) == 0)
			{
				txtSaleFactor.Text = FCConvert.ToString(0);
			}
			if (Conversion.Val(txtSaleFactor.Text) > 300)
			{
				txtSaleFactor.Text = FCConvert.ToString(300);
			}
			if (Conversion.Val(txtSaleFactor.Text) < -300)
			{
				txtSaleFactor.Text = FCConvert.ToString(-300);
			}
		}

		private void FillValidityCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: intNum As short --> As int	OnWriteFCConvert.ToDouble(
			int intNum = 0;
			string strDesc = "";
			string strTemp = "";
			clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1381 and 1389 order by crecordnumber", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				intNum = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380);
				strDesc = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
				strTemp = Strings.Replace(strDesc, ".", " ", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Trim(strTemp);
				if (strTemp != string.Empty)
				{
					cmbValidity.Items[intNum] = FCConvert.ToString(intNum) + " " + strDesc;
				}
				clsLoad.MoveNext();
			}
			cmbValidity.SelectedIndex = 0;
		}

		private void txtPercent_KeyPress(object sender, KeyPressEventArgs e)
		{
			//if (!char.IsDigit(e.KeyChar) && txtPercent.Text.Length > 0)
			//{
			//	txtPercent.Text = txtPercent.Text.Remove(txtPercent.Text.Length - 1);
			//}
			int value = 0;
			if (int.TryParse(txtPercent.Text, out value))
			{
				if (value < 1)
				{
					txtPercent.Text = "1";
				}
				else if (value > 49)
				{
					txtPercent.Text = "49";
				}
			}
		}
	}
}
