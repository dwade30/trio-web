﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptSaleData.
	/// </summary>
	public partial class srptSaleData : FCSectionReport
	{
		public static srptSaleData InstancePtr
		{
			get
			{
				return (srptSaleData)Sys.GetInstance(typeof(srptSaleData));
			}
		}

		protected srptSaleData _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptSaleData()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptSaleData	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (Conversion.Val(this.UserData) == -10)
			{
				// signal to not print this report
				// Unload Me
				this.Close();
				return;
			}
			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x = 0;
			if (!modDataTypes.Statics.MR.EndOfFile())
			{
				x = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				if (x < 0)
				{
					// from calc menu not F9 calc
					txtSaleDate.Text = Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
					txtSalePrice.Text = Strings.Format(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice"), "#,###,###,##0");
					modProperty.Statics.WKey = FCConvert.ToInt32(1350 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("PISALETYPE")));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtSaleType.Text = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WKey = FCConvert.ToInt32(1360 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalefinancing")));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtFinancing.Text = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WKey = FCConvert.ToInt32(1370 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleverified")));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtVerified.Text = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WKey = FCConvert.ToInt32(1380 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity")));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtValidity.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				else
				{
					// With frmNewValuationReport.SaleGrid(x)
					// txtSaleDate.Text = .TextMatrix(0, 1)
					// txtSalePrice.Text = .TextMatrix(1, 1)
					// txtSaleType.Text = .TextMatrix(2, 1)
					// txtFinancing.Text = .TextMatrix(3, 1)
					// txtVerified.Text = .TextMatrix(4, 1)
					// txtValidity.Text = .TextMatrix(5, 1)
					txtSaleDate.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].SaleDate;
					txtSalePrice.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].SalePrice;
					txtSaleType.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].SaleType;
					txtFinancing.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Financing;
					txtVerified.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Verified;
					txtValidity.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Validity;
				}
			}
		}

		
	}
}
