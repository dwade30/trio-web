﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public partial class frmCustomizeXF : BaseForm
	{
		public frmCustomizeXF()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomizeXF InstancePtr
		{
			get
			{
				return (frmCustomizeXF)Sys.GetInstance(typeof(frmCustomizeXF));
			}
		}

		protected frmCustomizeXF _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cREXferSettingsController xSetCont = new cREXferSettingsController();

		private void frmCustomize_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomize_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomize properties;
			//frmCustomize.FillStyle	= 0;
			//frmCustomize.ScaleWidth	= 10725;
			//frmCustomize.ScaleHeight	= 8145;
			//frmCustomize.LinkTopic	= "Form2";
			//frmCustomize.LockControls	= -1  'True;
			//frmCustomize.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadInfo();
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cREXferSetting custInfo;
				custInfo = xSetCont.LoadSettings();
				if (custInfo.UsePartyAddressFromSystem)
				{
					cmbVisionAddress.Text = "Use address in TRIO system";
				}
				else
				{
					cmbVisionAddress.Text = "Use address from Vision";
				}
				if (custInfo.boolImportPreviousOwner)
				{
					chkPreviousOwners.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkPreviousOwners.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (custInfo.boolSplitValuation)
				{
					cmbSplitValuation.Text = "Save land, building , exempt values as % of property total";
				}
				else
				{
					cmbSplitValuation.Text = "Save land, building , exempt values as property total";
				}
				if (custInfo.boolUseMultiOwner)
				{
					chkMultiOwner.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkMultiOwner.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (custInfo.boolUsesBlock)
				{
					chkBlock.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkBlock.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (custInfo.boolUsesCRLF)
				{
					chkCRLF.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkCRLF.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (custInfo.boolUsesLot)
				{
					chkLot.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkLot.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (custInfo.boolUsesUnits)
				{
					chkUnit.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkUnit.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtExtraCharacters.Text = FCConvert.ToString(custInfo.intExtraChars);
				if (custInfo.boolMatchByAccount)
				{
					cmbMatch.Text = "Match Account";
				}
				else if (custInfo.boolMatchByTRIOAccount)
				{
					cmbMatch.Text = "Match TRIO Account";
				}
				else
				{
					cmbMatch.Text = "Match Map Lot";
				}
				txtBlankRecords.Text = FCConvert.ToString(custInfo.intBlankRecords);
				if (custInfo.boolPPMatchByOpen1)
				{
					cmbPPMatch.Text = "Match Open 1 Field";
				}
				else
				{
					cmbPPMatch.Text = "Match TRIO Account";
				}
				if (custInfo.boolOmitTrailingZeroes)
				{
					chkOmitTrailingZeroes.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkOmitTrailingZeroes.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (custInfo.intDivisionSize == 4)
				{
					cmbDivisions.Text = "Divisions are 4 characters (0001-0001)";
				}
				else if (custInfo.intDivisionSize == 1)
				{
					cmbDivisions.Text = "Don't format (Divisions are 1 character min.)";
				}
				else
				{
					cmbDivisions.Text = "Divisions are 3 characters (001-001)";
				}
				txtPPExtraCharacters.Text = FCConvert.ToString(custInfo.intPPExtraCharacters);
				if (custInfo.intOwnerOption == modMainXF.CNSTREOWNERINFONEW)
				{
					cmbOwner.Text = "Owner as of April 1st";
				}
				else if (custInfo.intOwnerOption == modMainXF.CNSTREOWNERINFOOLDCONEW)
				{
					cmbOwner.Text = "Old owner C/O new owner";
				}
				else if (custInfo.intOwnerOption == modMainXF.CNSTREOWNERINFONEW)
				{
					cmbOwner.Text = "Current Owner";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				//FC:FINAL:AM:#3157 - don't close form after save
                //mnuExit_Click();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			cREXferSetting custInfo = new cREXferSetting();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				custInfo.boolUsesBlock = chkBlock.CheckState == Wisej.Web.CheckState.Checked;
				custInfo.boolUsesCRLF = chkCRLF.CheckState == Wisej.Web.CheckState.Checked;
				custInfo.boolUsesLot = chkLot.CheckState == Wisej.Web.CheckState.Checked;
				custInfo.boolUsesUnits = chkUnit.CheckState == Wisej.Web.CheckState.Checked;
				custInfo.intExtraChars = FCConvert.ToInt32(Math.Round(Conversion.Val(txtExtraCharacters.Text)));
				custInfo.boolUseMultiOwner = chkMultiOwner.CheckState == Wisej.Web.CheckState.Checked;
				custInfo.boolSplitValuation = cmbSplitValuation.Text == "Save land, building , exempt values as % of property total";
				if (cmbOwner.Text == "Owner as of April 1st")
				{
					custInfo.intOwnerOption = modMainXF.CNSTREOWNERINFOOLD;
				}
				else if (cmbOwner.Text == "Old owner C/O new owner")
				{
					custInfo.intOwnerOption = modMainXF.CNSTREOWNERINFOOLDCONEW;
				}
				else
				{
					custInfo.intOwnerOption = modMainXF.CNSTREOWNERINFONEW;
				}
				if (cmbMatch.Text == "Match Map Lot")
				{
					custInfo.boolMatchByAccount = false;
					custInfo.boolMatchByTRIOAccount = false;
				}
				else if (cmbMatch.Text == "Match Account")
				{
					custInfo.boolMatchByAccount = true;
					custInfo.boolMatchByTRIOAccount = false;
				}
				else
				{
					custInfo.boolMatchByTRIOAccount = true;
					custInfo.boolMatchByAccount = false;
				}
				custInfo.intBlankRecords = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBlankRecords.Text)));
				custInfo.boolPPMatchByOpen1 = cmbPPMatch.Text == "Match Open 1 Field";
				custInfo.boolPPMatchByTRIOaccount = cmbPPMatch.Text == "Match TRIO Account";
				custInfo.boolOmitTrailingZeroes = chkOmitTrailingZeroes.CheckState == Wisej.Web.CheckState.Checked;
				if (cmbDivisions.Text == "Divisions are 3 characters (001-001)")
				{
					custInfo.intDivisionSize = 3;
				}
				else if (cmbDivisions.Text == "Divisions are 4 characters (0001-0001)")
				{
					custInfo.intDivisionSize = 4;
				}
				else
				{
					custInfo.intDivisionSize = 1;
				}
				custInfo.intPPExtraCharacters = FCConvert.ToInt32(Math.Round(Conversion.Val(txtPPExtraCharacters.Text)));
				custInfo.boolImportPreviousOwner = chkPreviousOwners.CheckState == Wisej.Web.CheckState.Checked;
				custInfo.UsePartyAddressFromSystem = cmbVisionAddress.Text == "Use address in TRIO system";
				xSetCont.SaveSettings(custInfo);
				// refresh global setting object
				modMainXF.LoadCustomizedInfo();
				MessageBox.Show("Customized Information Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}
	}
}
