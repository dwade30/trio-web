﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmAddDocument.
	/// </summary>
	partial class frmAddDocument : BaseForm
	{
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.fcViewerPanel1 = new fecherFoundation.FCViewerPanel();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.cmdScan = new fecherFoundation.FCButton();
			this.lblFile = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdScan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 588);
			this.BottomPanel.Size = new System.Drawing.Size(504, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fcViewerPanel1);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.cmdBrowse);
			//this.ClientArea.Controls.Add(this.cmdScan);
			this.ClientArea.Controls.Add(this.lblFile);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(504, 528);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(504, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(175, 30);
			this.HeaderText.Text = "Add Document";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = -1;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = -1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = -1;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// fcViewerPanel1
			// 
			this.fcViewerPanel1.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.fcViewerPanel1.Location = new System.Drawing.Point(30, 163);
			this.fcViewerPanel1.Name = "fcViewerPanel1";
			this.fcViewerPanel1.Size = new System.Drawing.Size(400, 250);
			this.fcViewerPanel1.TabIndex = 16;
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
			this.txtDescription.Location = new System.Drawing.Point(30, 56);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(400, 40);
			this.txtDescription.TabIndex = 11;
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(30, 433);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(100, 40);
			this.cmdBrowse.TabIndex = 12;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// cmdScan
			// 
			this.cmdScan.AppearanceKey = "actionButton";
			this.cmdScan.Location = new System.Drawing.Point(281, 433);
			this.cmdScan.Name = "cmdScan";
			this.cmdScan.Size = new System.Drawing.Size(149, 40);
			this.cmdScan.TabIndex = 13;
			this.cmdScan.Text = "Scan Document";
			this.cmdScan.Click += new System.EventHandler(this.cmdScan_Click);
			// 
			// lblFile
			// 
			this.lblFile.Location = new System.Drawing.Point(30, 116);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(400, 27);
			this.lblFile.TabIndex = 14;
			this.lblFile.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 20);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(400, 16);
			this.Label1.TabIndex = 15;
			this.Label1.Text = "DOCUMENT DESCRIPTION";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(191, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(122, 48);
			this.cmdSave.TabIndex = 9;
			this.cmdSave.Text = "Process";
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmAddDocument
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(457, 696);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmAddDocument";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Add Document";
			this.Load += new System.EventHandler(this.frmAddDocument_Load);
			this.Activated += new System.EventHandler(this.frmAddDocument_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAddDocument_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdScan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCViewerPanel fcViewerPanel1;
		public FCTextBox txtDescription;
		public FCButton cmdBrowse;
		public FCButton cmdScan;
		public FCLabel lblFile;
		public FCLabel Label1;
		private FCButton cmdSave;
	}
}
