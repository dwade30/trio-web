﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmWinsktCfg.
	/// </summary>
	partial class frmWinsktCfg : BaseForm
	{
		public fecherFoundation.FCFrame framPhrases;
		public FCGrid GridPhrases;
		public FCGrid Grid;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuEditPhrases;
		public fecherFoundation.FCToolStripMenuItem mnuSepar5;
		public fecherFoundation.FCButton cmdAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuMove;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuCopyLoad;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuReload;
		public fecherFoundation.FCToolStripMenuItem mnuSepar6;
		public fecherFoundation.FCToolStripMenuItem mnuLoadNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveToServer;
		public fecherFoundation.FCButton cmdSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWinsktCfg));
			this.framPhrases = new fecherFoundation.FCFrame();
			this.GridPhrases = new fecherFoundation.FCGrid();
			this.Grid = new fecherFoundation.FCGrid();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuEditPhrases = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar5 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMove = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCopyLoad = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuReload = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar6 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLoadNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveToServer = new fecherFoundation.FCToolStripMenuItem();
			this.cmdAddRow = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framPhrases)).BeginInit();
			this.framPhrases.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridPhrases)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 562);
			this.BottomPanel.Size = new System.Drawing.Size(654, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framPhrases);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Size = new System.Drawing.Size(654, 502);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAddRow);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(654, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddRow, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(299, 30);
			this.HeaderText.Text = "Edit Config & Phrase Files";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// framPhrases
			// 
			this.framPhrases.AppearanceKey = "groupBoxNoBorders";
			this.framPhrases.Controls.Add(this.GridPhrases);
			this.framPhrases.Location = new System.Drawing.Point(30, 30);
			this.framPhrases.Name = "framPhrases";
			this.framPhrases.Size = new System.Drawing.Size(326, 462);
			this.framPhrases.TabIndex = 1;
			this.framPhrases.Text = "Phrases";
			this.ToolTip1.SetToolTip(this.framPhrases, null);
			this.framPhrases.Visible = false;
			// 
			// GridPhrases
			// 
			this.GridPhrases.AllowSelection = false;
			this.GridPhrases.AllowUserToResizeColumns = false;
			this.GridPhrases.AllowUserToResizeRows = false;
			this.GridPhrases.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridPhrases.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridPhrases.BackColorBkg = System.Drawing.Color.Empty;
			this.GridPhrases.BackColorFixed = System.Drawing.Color.Empty;
			this.GridPhrases.BackColorSel = System.Drawing.Color.Empty;
			this.GridPhrases.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridPhrases.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridPhrases.ColumnHeadersHeight = 30;
			this.GridPhrases.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridPhrases.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridPhrases.DragIcon = null;
			this.GridPhrases.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridPhrases.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridPhrases.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
			this.GridPhrases.ExtendLastCol = true;
			this.GridPhrases.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridPhrases.FrozenCols = 0;
			this.GridPhrases.GridColor = System.Drawing.Color.Empty;
			this.GridPhrases.GridColorFixed = System.Drawing.Color.Empty;
			this.GridPhrases.Location = new System.Drawing.Point(0, 30);
			this.GridPhrases.Name = "GridPhrases";
			this.GridPhrases.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridPhrases.RowHeightMin = 0;
			this.GridPhrases.Rows = 1;
			this.GridPhrases.ScrollTipText = null;
			this.GridPhrases.ShowColumnVisibilityMenu = false;
			this.GridPhrases.Size = new System.Drawing.Size(276, 406);
			this.GridPhrases.StandardTab = true;
			this.GridPhrases.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridPhrases.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridPhrases.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.GridPhrases, null);
			this.GridPhrases.KeyDown += new Wisej.Web.KeyEventHandler(this.GridPhrases_KeyDownEvent);
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 11;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
			this.Grid.DragIcon = null;
			this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
			this.Grid.ExtendLastCol = true;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 30);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(596, 454);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Grid, "To move a row click and drag the left most part of the row to its new position");
			this.Grid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.Grid_MouseDownEvent);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuEditPhrases,
				this.mnuSepar5,
				this.mnuMove,
				this.mnuSepar4,
				this.mnuCopyLoad,
				this.mnuSepar3,
				this.mnuReload,
				this.mnuSepar6,
				this.mnuLoadNew,
				this.mnuSepar2,
				this.mnuSaveToServer
			});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuEditPhrases
			// 
			this.mnuEditPhrases.Index = 0;
			this.mnuEditPhrases.Name = "mnuEditPhrases";
			this.mnuEditPhrases.Text = "Edit Phrases";
			this.mnuEditPhrases.Click += new System.EventHandler(this.mnuEditPhrases_Click);
			// 
			// mnuSepar5
			// 
			this.mnuSepar5.Index = 1;
			this.mnuSepar5.Name = "mnuSepar5";
			this.mnuSepar5.Text = "-";
			// 
			// mnuMove
			// 
			this.mnuMove.Index = 2;
			this.mnuMove.Name = "mnuMove";
			this.mnuMove.Text = "Move Selected Rows";
			this.mnuMove.Click += new System.EventHandler(this.mnuMove_Click);
			// 
			// mnuSepar4
			// 
			this.mnuSepar4.Index = 3;
			this.mnuSepar4.Name = "mnuSepar4";
			this.mnuSepar4.Text = "-";
			// 
			// mnuCopyLoad
			// 
			this.mnuCopyLoad.Index = 4;
			this.mnuCopyLoad.Name = "mnuCopyLoad";
			this.mnuCopyLoad.Text = "Copy & Load From Server";
			this.mnuCopyLoad.Click += new System.EventHandler(this.mnuCopyLoad_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 5;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuReload
			// 
			this.mnuReload.Index = 6;
			this.mnuReload.Name = "mnuReload";
			this.mnuReload.Text = "Reload Config File";
			this.mnuReload.Click += new System.EventHandler(this.mnuReload_Click);
			// 
			// mnuSepar6
			// 
			this.mnuSepar6.Index = 7;
			this.mnuSepar6.Name = "mnuSepar6";
			this.mnuSepar6.Text = "-";
			// 
			// mnuLoadNew
			// 
			this.mnuLoadNew.Index = 8;
			this.mnuLoadNew.Name = "mnuLoadNew";
			this.mnuLoadNew.Text = "Load from Cost Records";
			this.mnuLoadNew.Click += new System.EventHandler(this.mnuLoadNew_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 9;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSaveToServer
			// 
			this.mnuSaveToServer.Index = 10;
			this.mnuSaveToServer.Name = "mnuSaveToServer";
			this.mnuSaveToServer.Text = "Save & Copy To Server";
			this.mnuSaveToServer.Click += new System.EventHandler(this.mnuSaveToServer_Click);
			// 
			// cmdAddRow
			// 
			this.cmdAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddRow.AppearanceKey = "toolbarButton";
			this.cmdAddRow.Location = new System.Drawing.Point(397, 29);
			this.cmdAddRow.Name = "cmdAddRow";
			this.cmdAddRow.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
			this.cmdAddRow.Size = new System.Drawing.Size(73, 24);
			this.cmdAddRow.TabIndex = 1;
			this.cmdAddRow.Text = "Add Row";
			this.ToolTip1.SetToolTip(this.cmdAddRow, null);
			this.cmdAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(474, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.Delete;
			this.cmdDelete.Size = new System.Drawing.Size(151, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete Selected Rows";
			this.ToolTip1.SetToolTip(this.cmdDelete, null);
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(256, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmWinsktCfg
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(654, 670);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmWinsktCfg";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Config & Phrase Files";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmWinsktCfg_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmWinsktCfg_KeyDown);
			this.Resize += new System.EventHandler(this.frmWinsktCfg_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framPhrases)).EndInit();
			this.framPhrases.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridPhrases)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
