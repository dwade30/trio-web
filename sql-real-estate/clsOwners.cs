﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class clsOwners
	{
		//=========================================================
		// vbPorter upgrade warning: ID As int	OnRead(string)
		public int ID;
		public int ClientID;
		public int MiscID;
		public string FirstName = "";
		public string LastName = "";
		public string MiddleName = "";
		public string Desig = "";
		public string FullName = "";
		public string Address1 = "";
		public string Address2 = "";
		public string City = "";
		public string StateProvince = "";
		public string Country = "";
		public string Zip = "";
		public string ZipExt = "";
		public string PhoneNumber1 = "";
		public string PhoneDescription1 = "";
		public string PhoneNumber2 = "";
		public string PhoneDescription2 = "";
		public string EMail = "";
	}
}
