﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using System.IO;
using Global;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPrintPicture.
	/// </summary>
	public partial class rptPrintPicture : BaseSectionReport
	{
		public static rptPrintPicture InstancePtr
		{
			get
			{
				return (rptPrintPicture)Sys.GetInstance(typeof(rptPrintPicture));
			}
		}

		protected rptPrintPicture _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptPrintPicture()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Print Picture";
		}
		// nObj = 1
		//   0	rptPrintPicture	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strPicFile;
		int intPicSize;

        private CentralDocument centralDocument;
		// vbPorter upgrade warning: intPrintSize As short	OnWriteFCConvert.ToInt32(
		public void Init(Guid documentIdentifier , int lngAcct, int intPrintSize)
        {
            centralDocument = StaticSettings.GlobalCommandDispatcher.Send(new GetCentralDocumentByIdentifier(documentIdentifier))
                .Result;
			if (centralDocument == null)
			{
				MessageBox.Show("Image not found", "Image Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			intPicSize = intPrintSize;
			lblAccount.Text = "Account " + FCConvert.ToString(lngAcct);
			lblFile.Text = "";
			//strPicFile = strPicture;
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Picture");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngOrigWidth = 0;
			int lngOrigHeight = 0;
			float lngNewWidth = 0;
			float lngNewHeight = 0;
            Image1.Image = FCUtils.PictureFromBytes(centralDocument.ItemData);
            //using (var stream = new MemoryStream(centralDocument.ItemData))
            //{
            //    Image1.Image = new Bitmap(stream);
            //}

            switch (intPicSize)
			{
				case 1:
					{
                        // full size
                        Image1.Height = Detail.Height - (1 / 1440f);
                        Image1.Width = PrintWidth - (1 / 1440f);
                        Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
						break;
					}
				case 2:
					{
						// 4 x 6
						Image1.Height = 4;
						Image1.Width = 6;
						Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
						break;
					}
				case 3:
					{
						// 3 x 5
						Image1.Height = 3;
						Image1.Width = 5;
						Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
						break;
					}
				default:
					{
						// pic size
						float lngDPI = 0;
						lngDPI = 140;
						lngOrigWidth = Image1.Image.Width;
						lngOrigHeight = Image1.Image.Height;
						lngNewWidth = lngOrigWidth / lngDPI;
						lngNewHeight = lngOrigHeight / lngDPI;
                        Image1.Height = Detail.Height - (1 / 1440f);
                        Image1.Width = PrintWidth - (1 / 1440f);
                        if (lngNewWidth < Image1.Width)
						{
							Image1.Width = lngNewWidth;
						}
						if (lngNewHeight < Image1.Height)
						{
							Image1.Height = lngNewHeight;
						}
						Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
						break;
					}
			}
			//end switch
		}

		
	}
}
