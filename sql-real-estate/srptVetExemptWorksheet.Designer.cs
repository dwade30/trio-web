﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptVetExemptWorksheet.
	/// </summary>
	partial class srptVetExemptWorksheet
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptVetExemptWorksheet));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtValueTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtValue12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtValue9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.TXTMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumExempts3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExempts11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumExemptsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumExempts1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumExempts2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumExempts12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			((System.ComponentModel.ISupportInitialize)(this.txtValueTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TXTMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExemptsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label187)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValueTotal,
            this.Shape30,
            this.txtValue12,
            this.txtValue11,
            this.txtValue10,
            this.Shape29,
            this.txtValue9,
            this.txtValue8,
            this.txtValue7,
            this.txtValue6,
            this.txtValue5,
            this.txtValue4,
            this.txtValue3,
            this.txtValue2,
            this.txtValue1,
            this.txtNumExempts5,
            this.Shape20,
            this.lblTitle,
            this.Label3,
            this.TXTMuniname,
            this.Label4,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label28,
            this.Label31,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Label35,
            this.Label36,
            this.Label39,
            this.Label40,
            this.Label41,
            this.txtCYear,
            this.Label42,
            this.Label43,
            this.Label44,
            this.Label45,
            this.Label46,
            this.Label48,
            this.Label49,
            this.Label50,
            this.Label51,
            this.Label52,
            this.Label93,
            this.Label94,
            this.Label95,
            this.Label96,
            this.Label97,
            this.Label99,
            this.Label100,
            this.Label101,
            this.Label102,
            this.Label103,
            this.txtNumExempts3,
            this.txtNumExempts4,
            this.txtNumExempts6,
            this.txtNumExempts7,
            this.txtNumExempts8,
            this.txtNumExempts9,
            this.txtNumExempts10,
            this.txtNumExempts11,
            this.txtNumExemptsTotal,
            this.Line4,
            this.Label114,
            this.Label115,
            this.Label116,
            this.Label117,
            this.Label118,
            this.Label123,
            this.txtNumExempts1,
            this.Label124,
            this.Label125,
            this.Label126,
            this.Label127,
            this.Label132,
            this.txtNumExempts2,
            this.Label133,
            this.Label134,
            this.Label135,
            this.Label136,
            this.Label137,
            this.Label138,
            this.Label140,
            this.Label141,
            this.Label142,
            this.Label143,
            this.Label175,
            this.Label176,
            this.txtCYear2,
            this.Label178,
            this.Label179,
            this.Label180,
            this.Label181,
            this.Label182,
            this.txtNumExempts12,
            this.Label183,
            this.Label184,
            this.Shape15,
            this.Shape16,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Shape21,
            this.Shape22,
            this.Shape24,
            this.Shape25,
            this.Shape26,
            this.Shape27,
            this.Shape28,
            this.Label185,
            this.Label186,
            this.Label187,
            this.Shape12,
            this.Shape13,
            this.Shape11,
            this.Shape10,
            this.Shape9,
            this.Shape8,
            this.Shape7,
            this.Shape5,
            this.Shape4,
            this.Shape3,
            this.Shape2,
            this.Shape14,
            this.Shape1});
			this.Detail.Height = 9.895833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtValueTotal
			// 
			this.txtValueTotal.Height = 0.1875F;
			this.txtValueTotal.Left = 6.25F;
			this.txtValueTotal.Name = "txtValueTotal";
			this.txtValueTotal.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValueTotal.Text = "0";
			this.txtValueTotal.Top = 9.3125F;
			this.txtValueTotal.Width = 1.0625F;
			// 
			// Shape30
			// 
			this.Shape30.Height = 2.15625F;
			this.Shape30.Left = 0.02083333F;
			this.Shape30.Name = "Shape30";
			this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape30.Top = 6.760417F;
			this.Shape30.Width = 7.354167F;
			// 
			// txtValue12
			// 
			this.txtValue12.Height = 0.1875F;
			this.txtValue12.Left = 6.25F;
			this.txtValue12.Name = "txtValue12";
			this.txtValue12.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue12.Text = "0";
			this.txtValue12.Top = 8.3125F;
			this.txtValue12.Width = 1.0625F;
			// 
			// txtValue11
			// 
			this.txtValue11.Height = 0.1875F;
			this.txtValue11.Left = 6.25F;
			this.txtValue11.Name = "txtValue11";
			this.txtValue11.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue11.Text = "0";
			this.txtValue11.Top = 7.75F;
			this.txtValue11.Width = 1.0625F;
			// 
			// txtValue10
			// 
			this.txtValue10.Height = 0.1875F;
			this.txtValue10.Left = 6.25F;
			this.txtValue10.Name = "txtValue10";
			this.txtValue10.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue10.Text = "0";
			this.txtValue10.Top = 7.375F;
			this.txtValue10.Width = 1.0625F;
			// 
			// Shape29
			// 
			this.Shape29.Height = 5.75F;
			this.Shape29.Left = 0.02083333F;
			this.Shape29.Name = "Shape29";
			this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape29.Top = 0.8645833F;
			this.Shape29.Width = 7.354167F;
			// 
			// txtValue9
			// 
			this.txtValue9.Height = 0.1875F;
			this.txtValue9.Left = 6.25F;
			this.txtValue9.Name = "txtValue9";
			this.txtValue9.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue9.Text = "0";
			this.txtValue9.Top = 6.125F;
			this.txtValue9.Width = 1.0625F;
			// 
			// txtValue8
			// 
			this.txtValue8.Height = 0.1875F;
			this.txtValue8.Left = 6.25F;
			this.txtValue8.Name = "txtValue8";
			this.txtValue8.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue8.Text = "0";
			this.txtValue8.Top = 5.75F;
			this.txtValue8.Width = 1.0625F;
			// 
			// txtValue7
			// 
			this.txtValue7.Height = 0.1875F;
			this.txtValue7.Left = 6.25F;
			this.txtValue7.Name = "txtValue7";
			this.txtValue7.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue7.Text = "0";
			this.txtValue7.Top = 5.0625F;
			this.txtValue7.Width = 1.0625F;
			// 
			// txtValue6
			// 
			this.txtValue6.Height = 0.1875F;
			this.txtValue6.Left = 6.25F;
			this.txtValue6.Name = "txtValue6";
			this.txtValue6.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue6.Text = "0";
			this.txtValue6.Top = 4.375F;
			this.txtValue6.Width = 1.0625F;
			// 
			// txtValue5
			// 
			this.txtValue5.Height = 0.1875F;
			this.txtValue5.Left = 6.25F;
			this.txtValue5.Name = "txtValue5";
			this.txtValue5.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue5.Text = "0";
			this.txtValue5.Top = 3.6875F;
			this.txtValue5.Width = 1.0625F;
			// 
			// txtValue4
			// 
			this.txtValue4.Height = 0.1875F;
			this.txtValue4.Left = 6.25F;
			this.txtValue4.Name = "txtValue4";
			this.txtValue4.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue4.Text = "0";
			this.txtValue4.Top = 3.3125F;
			this.txtValue4.Width = 1.0625F;
			// 
			// txtValue3
			// 
			this.txtValue3.Height = 0.1875F;
			this.txtValue3.Left = 6.25F;
			this.txtValue3.Name = "txtValue3";
			this.txtValue3.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue3.Text = "0";
			this.txtValue3.Top = 2.625F;
			this.txtValue3.Width = 1.0625F;
			// 
			// txtValue2
			// 
			this.txtValue2.Height = 0.1875F;
			this.txtValue2.Left = 6.25F;
			this.txtValue2.Name = "txtValue2";
			this.txtValue2.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue2.Text = "0";
			this.txtValue2.Top = 2.25F;
			this.txtValue2.Width = 1.0625F;
			// 
			// txtValue1
			// 
			this.txtValue1.Height = 0.1875F;
			this.txtValue1.Left = 6.25F;
			this.txtValue1.Name = "txtValue1";
			this.txtValue1.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtValue1.Text = "0";
			this.txtValue1.Top = 1.5625F;
			this.txtValue1.Width = 1.0625F;
			// 
			// txtNumExempts5
			// 
			this.txtNumExempts5.Height = 0.1875F;
			this.txtNumExempts5.Left = 4F;
			this.txtNumExempts5.Name = "txtNumExempts5";
			this.txtNumExempts5.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts5.Text = "0";
			this.txtNumExempts5.Top = 3.6875F;
			this.txtNumExempts5.Width = 0.5625F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.1875F;
			this.Shape20.Left = 4F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 3.6875F;
			this.Shape20.Width = 0.5625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.25F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold; text-align: c" +
    "enter; ddo-char-set: 0";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.0625F;
			this.lblTitle.Width = 4.8125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Times New Roman\'";
			this.Label3.Text = "MUNICIPALITY";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 1.25F;
			// 
			// TXTMuniname
			// 
			this.TXTMuniname.Height = 0.1875F;
			this.TXTMuniname.Left = 2.4375F;
			this.TXTMuniname.Name = "TXTMuniname";
			this.TXTMuniname.Style = "font-family: \'Times New Roman\'";
			this.TXTMuniname.Text = null;
			this.TXTMuniname.Top = 0.3125F;
			this.TXTMuniname.Width = 2.3125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label4.Text = "The following information is necessary in order to calculate reimbursement. (36 M" +
    ".R.S. § 653)";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 5F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Times New Roman\'; font-weight: bold";
			this.Label6.Text = "Section 1: This section is only for those veterans that served during a federally" +
    " recognized war period.";
			this.Label6.Top = 0.9375F;
			this.Label6.Width = 6.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label7.Text = "Revocable Living Trusts:";
			this.Label7.Top = 2F;
			this.Label7.Width = 1.625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.3125F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 7pt; font-weight: bold; text-align: center";
			this.Label8.Text = "NUMBER OF EXEMPTIONS";
			this.Label8.Top = 1.1875F;
			this.Label8.Width = 0.8125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.188F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.263F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 7pt; font-weight: bold; text-align: center";
			this.Label11.Text = "EXEMPT VALUE";
			this.Label11.Top = 1.313F;
			this.Label11.Width = 0.812F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label12.Text = "3. All other veterans (or their widows) who are the beneficiary of ";
			this.Label12.Top = 2.625F;
			this.Label12.Width = 3.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.1875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label13.Text = "revocable living trusts. $6,000 adjusted by the certified ratio (§ 653(1)(C) or (" +
    "D))";
			this.Label13.Top = 2.8125F;
			this.Label13.Width = 4.25F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label14.Text = "4. WW I veteran (or their widows) enlisted as Maine resident.";
			this.Label14.Top = 3.3125F;
			this.Label14.Width = 3.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.1875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label15.Text = "$7,000 adjusted by the certified ratio (§ 653(1)(C-1) or (D-2))";
			this.Label15.Top = 3.5F;
			this.Label15.Width = 4.5F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.0625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label16.Text = "5. WW I veteran (or their widows) enlisted as non-Maine resident.";
			this.Label16.Top = 3.6875F;
			this.Label16.Width = 3.4375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.1875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label17.Text = "$7,000 adjusted by the certified ratio (§ 653(1)(C-1) or (D-2))";
			this.Label17.Top = 3.875F;
			this.Label17.Width = 4.375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.0625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label18.Text = "WW I Veterans:";
			this.Label18.Top = 3.0625F;
			this.Label18.Width = 1.625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.0625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label19.Text = "6. Paraplegic status veteran or their unremarried widow.";
			this.Label19.Top = 4.375F;
			this.Label19.Width = 3.4375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.1875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label20.Text = "$50,000 adjusted by the certified ratio (§ 653(1)(D-1))";
			this.Label20.Top = 4.5625F;
			this.Label20.Width = 3.375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.0625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label21.Text = "7. Qualifying Shareholder of Cooperative Housing Corporations";
			this.Label21.Top = 5.0625F;
			this.Label21.Width = 3.3125F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.1875F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label22.Text = "$6,000 adjusted by the certified ratio  (§ 653(2))";
			this.Label22.Top = 5.25F;
			this.Label22.Width = 3.375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.0625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label24.Text = "12. Veteran (or their widow) who served during the period from";
			this.Label24.Top = 8.3125F;
			this.Label24.Width = 3.25F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.25F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label25.Text = "February 27, 1961 and August 5, 1964, but did not serve prior";
			this.Label25.Top = 8.5F;
			this.Label25.Width = 3.8125F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.25F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label26.Text = "to February 1, 1955 or after August 4, 1964.  $6,000 adjusted by the certified ra" +
    "tio  (§ 653(1)(C)(1) or (D)";
			this.Label26.Top = 8.6875F;
			this.Label26.Width = 5.625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.0625F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label28.Text = "Paraplegic Veterans:";
			this.Label28.Top = 4.125F;
			this.Label28.Width = 1.625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.0625F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label31.Text = "All Other Veterans:";
			this.Label31.Top = 5.5F;
			this.Label31.Width = 1.625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0.0625F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label32.Text = "8. All other veterans (or their widows) enlisted as Maine ";
			this.Label32.Top = 5.75F;
			this.Label32.Width = 3.5F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.25F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label33.Text = "residents. $6,000 adjusted by the certified ratio  (§ 653(1)(C)(1))";
			this.Label33.Top = 5.9375F;
			this.Label33.Width = 4.1875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.0625F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label34.Text = "9. All other veterans (or their widows) enlisted as non-Maine";
			this.Label34.Top = 6.125F;
			this.Label34.Width = 3.5F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.25F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label35.Text = "residents. $6,000 adjusted by the certified ratio  (§ 653(1)(C)(1))";
			this.Label35.Top = 6.3125F;
			this.Label35.Width = 4.1875F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.0625F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label36.Text = "Section 2: This section is only for those veterans that did not serve during a fe" +
    "derally recognized war period";
			this.Label36.Top = 6.8125F;
			this.Label36.Width = 6F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0.0625F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label39.Text = "10. Veteran (or their widow) disabled in the line of duty.";
			this.Label39.Top = 7.375F;
			this.Label39.Width = 3.3125F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 0.25F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label40.Text = "$6,000 adjusted by the certified ratio  (§ 653(1)(C)(2) or (D))";
			this.Label40.Top = 7.5625F;
			this.Label40.Width = 3.3125F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.0625F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold";
			this.Label41.Text = "Total number of ALL veteran exemptions granted in ";
			this.Label41.Top = 9.0625F;
			this.Label41.Width = 2.75F;
			// 
			// txtCYear
			// 
			this.txtCYear.Height = 0.1875F;
			this.txtCYear.Left = 2.8125F;
			this.txtCYear.Name = "txtCYear";
			this.txtCYear.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.txtCYear.Text = null;
			this.txtCYear.Top = 9.0625F;
			this.txtCYear.Width = 0.5F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 3.4375F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label42.Text = "40t(3)A";
			this.Label42.Top = 2.625F;
			this.Label42.Width = 0.5625F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 3.4375F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label43.Text = "40t(4)A";
			this.Label43.Top = 3.3125F;
			this.Label43.Width = 0.5625F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 3.4375F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label44.Text = "40t(5)A";
			this.Label44.Top = 3.6875F;
			this.Label44.Width = 0.5625F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 3.4375F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label45.Text = "40t(6)A";
			this.Label45.Top = 4.375F;
			this.Label45.Width = 0.5625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 3.4375F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label46.Text = "40t(7)A";
			this.Label46.Top = 5.0625F;
			this.Label46.Width = 0.5625F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 3.4375F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label48.Text = "40t(8)A";
			this.Label48.Top = 5.75F;
			this.Label48.Width = 0.5625F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1875F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 3.4375F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label49.Text = "40t(9)A";
			this.Label49.Top = 6.125F;
			this.Label49.Width = 0.5625F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1875F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 3.4375F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label50.Text = "40t(10)A";
			this.Label50.Top = 7.375F;
			this.Label50.Width = 0.5625F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 3.4375F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label51.Text = "40t(11)A";
			this.Label51.Top = 7.75F;
			this.Label51.Width = 0.5625F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 3.4375F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold; text-align: r" +
    "ight";
			this.Label52.Text = "40t(A)";
			this.Label52.Top = 9.0625F;
			this.Label52.Width = 0.5625F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1875F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 6.0625F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label93.Text = "$";
			this.Label93.Top = 2.625F;
			this.Label93.Visible = false;
			this.Label93.Width = 0.1875F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1875F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 6.0625F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label94.Text = "$";
			this.Label94.Top = 3.3125F;
			this.Label94.Visible = false;
			this.Label94.Width = 0.1875F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1875F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 6.0625F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label95.Text = "$";
			this.Label95.Top = 3.6875F;
			this.Label95.Visible = false;
			this.Label95.Width = 0.1875F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1875F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 6.0625F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label96.Text = "$";
			this.Label96.Top = 4.375F;
			this.Label96.Visible = false;
			this.Label96.Width = 0.1875F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1875F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 6.0625F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label97.Text = "$";
			this.Label97.Top = 5.0625F;
			this.Label97.Visible = false;
			this.Label97.Width = 0.1875F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1875F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 6.0625F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label99.Text = "$";
			this.Label99.Top = 5.75F;
			this.Label99.Visible = false;
			this.Label99.Width = 0.1875F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1875F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 6.0625F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label100.Text = "$";
			this.Label100.Top = 6.125F;
			this.Label100.Visible = false;
			this.Label100.Width = 0.1875F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1875F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 6.0625F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label101.Text = "$";
			this.Label101.Top = 7.375F;
			this.Label101.Visible = false;
			this.Label101.Width = 0.1875F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1875F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 6.0625F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label102.Text = "$";
			this.Label102.Top = 7.75F;
			this.Label102.Visible = false;
			this.Label102.Width = 0.1875F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1875F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 6.0625F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label103.Text = "$";
			this.Label103.Top = 9.3125F;
			this.Label103.Visible = false;
			this.Label103.Width = 0.1875F;
			// 
			// txtNumExempts3
			// 
			this.txtNumExempts3.Height = 0.1875F;
			this.txtNumExempts3.Left = 4F;
			this.txtNumExempts3.Name = "txtNumExempts3";
			this.txtNumExempts3.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts3.Text = "0";
			this.txtNumExempts3.Top = 2.625F;
			this.txtNumExempts3.Width = 0.5625F;
			// 
			// txtNumExempts4
			// 
			this.txtNumExempts4.Height = 0.1875F;
			this.txtNumExempts4.Left = 4F;
			this.txtNumExempts4.Name = "txtNumExempts4";
			this.txtNumExempts4.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts4.Text = "0";
			this.txtNumExempts4.Top = 3.3125F;
			this.txtNumExempts4.Width = 0.5625F;
			// 
			// txtNumExempts6
			// 
			this.txtNumExempts6.Height = 0.1875F;
			this.txtNumExempts6.Left = 4F;
			this.txtNumExempts6.Name = "txtNumExempts6";
			this.txtNumExempts6.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts6.Text = "0";
			this.txtNumExempts6.Top = 4.375F;
			this.txtNumExempts6.Width = 0.5625F;
			// 
			// txtNumExempts7
			// 
			this.txtNumExempts7.Height = 0.1875F;
			this.txtNumExempts7.Left = 4F;
			this.txtNumExempts7.Name = "txtNumExempts7";
			this.txtNumExempts7.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts7.Text = "0";
			this.txtNumExempts7.Top = 5.0625F;
			this.txtNumExempts7.Width = 0.5625F;
			// 
			// txtNumExempts8
			// 
			this.txtNumExempts8.Height = 0.1875F;
			this.txtNumExempts8.Left = 4F;
			this.txtNumExempts8.Name = "txtNumExempts8";
			this.txtNumExempts8.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts8.Text = "0";
			this.txtNumExempts8.Top = 5.75F;
			this.txtNumExempts8.Width = 0.5625F;
			// 
			// txtNumExempts9
			// 
			this.txtNumExempts9.Height = 0.1875F;
			this.txtNumExempts9.Left = 4F;
			this.txtNumExempts9.Name = "txtNumExempts9";
			this.txtNumExempts9.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts9.Text = "0";
			this.txtNumExempts9.Top = 6.125F;
			this.txtNumExempts9.Width = 0.5625F;
			// 
			// txtNumExempts10
			// 
			this.txtNumExempts10.Height = 0.1875F;
			this.txtNumExempts10.Left = 4F;
			this.txtNumExempts10.Name = "txtNumExempts10";
			this.txtNumExempts10.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts10.Text = "0";
			this.txtNumExempts10.Top = 7.375F;
			this.txtNumExempts10.Width = 0.5625F;
			// 
			// txtNumExempts11
			// 
			this.txtNumExempts11.Height = 0.1875F;
			this.txtNumExempts11.Left = 4F;
			this.txtNumExempts11.Name = "txtNumExempts11";
			this.txtNumExempts11.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts11.Text = "0";
			this.txtNumExempts11.Top = 7.75F;
			this.txtNumExempts11.Width = 0.5625F;
			// 
			// txtNumExemptsTotal
			// 
			this.txtNumExemptsTotal.Height = 0.1875F;
			this.txtNumExemptsTotal.Left = 4F;
			this.txtNumExemptsTotal.Name = "txtNumExemptsTotal";
			this.txtNumExemptsTotal.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExemptsTotal.Text = "0";
			this.txtNumExemptsTotal.Top = 9.0625F;
			this.txtNumExemptsTotal.Width = 0.5625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.5625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 0.5625F;
			this.Line4.Y2 = 0.5625F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1875F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 0F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-family: \'Times New Roman\'; font-weight: bold";
			this.Label114.Text = "40t. VETERANS EXEMPTIONS";
			this.Label114.Top = 0.625F;
			this.Label114.Width = 2.125F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 0.0625F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label115.Text = "Widower:";
			this.Label115.Top = 1.3125F;
			this.Label115.Width = 1.625F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 0.0625F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label116.Text = "1. Living male spouse or male parent of a deceased veteran";
			this.Label116.Top = 1.5625F;
			this.Label116.Width = 3.125F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 0.1875F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label117.Text = "$6,000 adjusted by the certified ratio  (§ 653(1)(D))";
			this.Label117.Top = 1.75F;
			this.Label117.Width = 4.25F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 3.4375F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label118.Text = "40t(1)A";
			this.Label118.Top = 1.5625F;
			this.Label118.Width = 0.5625F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 6.0625F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label123.Text = "$";
			this.Label123.Top = 1.5625F;
			this.Label123.Visible = false;
			this.Label123.Width = 0.1875F;
			// 
			// txtNumExempts1
			// 
			this.txtNumExempts1.Height = 0.1875F;
			this.txtNumExempts1.Left = 4F;
			this.txtNumExempts1.Name = "txtNumExempts1";
			this.txtNumExempts1.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts1.Text = "0";
			this.txtNumExempts1.Top = 1.5625F;
			this.txtNumExempts1.Width = 0.5625F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1875F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 5.5625F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label124.Text = "40t(1)B";
			this.Label124.Top = 1.5625F;
			this.Label124.Width = 0.5F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1875F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 0.0625F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label125.Text = "2. Parapalegic veterans (or their widow) who is the beneficiary";
			this.Label125.Top = 2.25F;
			this.Label125.Width = 3.3125F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 0.1875F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label126.Text = "of a revocable living trust. $50,000 adjusted by the certified ratio  (§ 653(1)(D" +
    "-1])";
			this.Label126.Top = 2.4375F;
			this.Label126.Width = 4.1875F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 3.4375F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label127.Text = "40t(2)A";
			this.Label127.Top = 2.25F;
			this.Label127.Width = 0.5625F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1875F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 6.0625F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label132.Text = "$";
			this.Label132.Top = 2.25F;
			this.Label132.Visible = false;
			this.Label132.Width = 0.1875F;
			// 
			// txtNumExempts2
			// 
			this.txtNumExempts2.Height = 0.1875F;
			this.txtNumExempts2.Left = 4F;
			this.txtNumExempts2.Name = "txtNumExempts2";
			this.txtNumExempts2.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts2.Text = "0";
			this.txtNumExempts2.Top = 2.25F;
			this.txtNumExempts2.Width = 0.5625F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1875F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 5.5625F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label133.Text = "40t(2)B";
			this.Label133.Top = 2.25F;
			this.Label133.Width = 0.5F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1875F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 5.5625F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label134.Text = "40t(3)B";
			this.Label134.Top = 2.625F;
			this.Label134.Width = 0.5F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1875F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 5.5625F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label135.Text = "40t(4)B";
			this.Label135.Top = 3.3125F;
			this.Label135.Width = 0.5F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1875F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 5.5625F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label136.Text = "40t(5)B";
			this.Label136.Top = 3.6875F;
			this.Label136.Width = 0.5F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1875F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 5.5625F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label137.Text = "40t(6)B";
			this.Label137.Top = 4.375F;
			this.Label137.Width = 0.5F;
			// 
			// Label138
			// 
			this.Label138.Height = 0.1875F;
			this.Label138.HyperLink = null;
			this.Label138.Left = 5.5625F;
			this.Label138.Name = "Label138";
			this.Label138.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label138.Text = "40t(7)B";
			this.Label138.Top = 5.0625F;
			this.Label138.Width = 0.5F;
			// 
			// Label140
			// 
			this.Label140.Height = 0.1875F;
			this.Label140.HyperLink = null;
			this.Label140.Left = 5.5625F;
			this.Label140.Name = "Label140";
			this.Label140.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label140.Text = "40t(8)B";
			this.Label140.Top = 5.75F;
			this.Label140.Width = 0.5F;
			// 
			// Label141
			// 
			this.Label141.Height = 0.1875F;
			this.Label141.HyperLink = null;
			this.Label141.Left = 5.5625F;
			this.Label141.Name = "Label141";
			this.Label141.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label141.Text = "40t(9)B";
			this.Label141.Top = 6.125F;
			this.Label141.Width = 0.5625F;
			// 
			// Label142
			// 
			this.Label142.Height = 0.1875F;
			this.Label142.HyperLink = null;
			this.Label142.Left = 5.5F;
			this.Label142.Name = "Label142";
			this.Label142.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label142.Text = "40t(10)B";
			this.Label142.Top = 7.375F;
			this.Label142.Width = 0.5625F;
			// 
			// Label143
			// 
			this.Label143.Height = 0.1875F;
			this.Label143.HyperLink = null;
			this.Label143.Left = 5.5F;
			this.Label143.Name = "Label143";
			this.Label143.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label143.Text = "40t(11)B";
			this.Label143.Top = 7.75F;
			this.Label143.Width = 0.5625F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.3125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-6-";
			this.Label175.Top = 9.708333F;
			this.Label175.Width = 1F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 0.0625F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold";
			this.Label176.Text = "Total exempt value of ALL Veterans Exemptions granted in tax year";
			this.Label176.Top = 9.3125F;
			this.Label176.Width = 3.5625F;
			// 
			// txtCYear2
			// 
			this.txtCYear2.Height = 0.1875F;
			this.txtCYear2.Left = 3.625F;
			this.txtCYear2.Name = "txtCYear2";
			this.txtCYear2.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.txtCYear2.Text = null;
			this.txtCYear2.Top = 9.3125F;
			this.txtCYear2.Width = 0.5F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.1875F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 5.5F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-family: \'Times New Roman\'; font-size: 10pt; font-weight: bold; text-align: r" +
    "ight; vertical-align: top";
			this.Label178.Text = "40t(B)";
			this.Label178.Top = 9.3125F;
			this.Label178.Width = 0.5625F;
			// 
			// Label179
			// 
			this.Label179.Height = 0.1875F;
			this.Label179.HyperLink = null;
			this.Label179.Left = 0.0625F;
			this.Label179.Name = "Label179";
			this.Label179.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label179.Text = "11. Veteran (or their widow) who served during the";
			this.Label179.Top = 7.75F;
			this.Label179.Width = 3.3125F;
			// 
			// Label180
			// 
			this.Label180.Height = 0.1875F;
			this.Label180.HyperLink = null;
			this.Label180.Left = 0.25F;
			this.Label180.Name = "Label180";
			this.Label180.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label180.Text = "periods from August 24, 1982 to July 31, 1984 and";
			this.Label180.Top = 7.9375F;
			this.Label180.Width = 3.3125F;
			// 
			// Label181
			// 
			this.Label181.Height = 0.1875F;
			this.Label181.HyperLink = null;
			this.Label181.Left = 3.4375F;
			this.Label181.Name = "Label181";
			this.Label181.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label181.Text = "40t(12)A";
			this.Label181.Top = 8.3125F;
			this.Label181.Width = 0.5625F;
			// 
			// Label182
			// 
			this.Label182.Height = 0.1875F;
			this.Label182.HyperLink = null;
			this.Label182.Left = 6.0625F;
			this.Label182.Name = "Label182";
			this.Label182.Style = "font-family: \'Times New Roman\'; font-size: 9pt; vertical-align: top";
			this.Label182.Text = "$";
			this.Label182.Top = 8.3125F;
			this.Label182.Visible = false;
			this.Label182.Width = 0.1875F;
			// 
			// txtNumExempts12
			// 
			this.txtNumExempts12.Height = 0.1875F;
			this.txtNumExempts12.Left = 4F;
			this.txtNumExempts12.Name = "txtNumExempts12";
			this.txtNumExempts12.Style = "font-family: \'Times New Roman\'; text-align: right";
			this.txtNumExempts12.Text = "0";
			this.txtNumExempts12.Top = 8.3125F;
			this.txtNumExempts12.Width = 0.5625F;
			// 
			// Label183
			// 
			this.Label183.Height = 0.1875F;
			this.Label183.HyperLink = null;
			this.Label183.Left = 5.5F;
			this.Label183.Name = "Label183";
			this.Label183.Style = "font-family: \'Times New Roman\'; font-size: 10pt; vertical-align: top";
			this.Label183.Text = "40t(12)B";
			this.Label183.Top = 8.3125F;
			this.Label183.Width = 0.5625F;
			// 
			// Label184
			// 
			this.Label184.Height = 0.1875F;
			this.Label184.HyperLink = null;
			this.Label184.Left = 0.25F;
			this.Label184.Name = "Label184";
			this.Label184.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label184.Text = "December 20, 1989 to January 31, 1990. $6,000 adjusted by the certified ratio  (§" +
    " 653(1)(C)(1) or (D))";
			this.Label184.Top = 8.125F;
			this.Label184.Width = 5.125F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1875F;
			this.Shape15.Left = 4F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 9.0625F;
			this.Shape15.Width = 0.5625F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.1875F;
			this.Shape16.Left = 4F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 1.5625F;
			this.Shape16.Width = 0.5625F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.1875F;
			this.Shape17.Left = 4F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 2.25F;
			this.Shape17.Width = 0.5625F;
			// 
			// Shape18
			// 
			this.Shape18.Height = 0.1875F;
			this.Shape18.Left = 4F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 2.625F;
			this.Shape18.Width = 0.5625F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.1875F;
			this.Shape19.Left = 4F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 3.3125F;
			this.Shape19.Width = 0.5625F;
			// 
			// Shape21
			// 
			this.Shape21.Height = 0.1875F;
			this.Shape21.Left = 4F;
			this.Shape21.Name = "Shape21";
			this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape21.Top = 4.375F;
			this.Shape21.Width = 0.5625F;
			// 
			// Shape22
			// 
			this.Shape22.Height = 0.1875F;
			this.Shape22.Left = 4F;
			this.Shape22.Name = "Shape22";
			this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape22.Top = 5.0625F;
			this.Shape22.Width = 0.5625F;
			// 
			// Shape24
			// 
			this.Shape24.Height = 0.1875F;
			this.Shape24.Left = 4F;
			this.Shape24.Name = "Shape24";
			this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape24.Top = 5.75F;
			this.Shape24.Width = 0.5625F;
			// 
			// Shape25
			// 
			this.Shape25.Height = 0.1875F;
			this.Shape25.Left = 4F;
			this.Shape25.Name = "Shape25";
			this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape25.Top = 6.125F;
			this.Shape25.Width = 0.5625F;
			// 
			// Shape26
			// 
			this.Shape26.Height = 0.1875F;
			this.Shape26.Left = 4F;
			this.Shape26.Name = "Shape26";
			this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape26.Top = 7.375F;
			this.Shape26.Width = 0.5625F;
			// 
			// Shape27
			// 
			this.Shape27.Height = 0.1875F;
			this.Shape27.Left = 4F;
			this.Shape27.Name = "Shape27";
			this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape27.Top = 7.75F;
			this.Shape27.Width = 0.5625F;
			// 
			// Shape28
			// 
			this.Shape28.Height = 0.1875F;
			this.Shape28.Left = 4F;
			this.Shape28.Name = "Shape28";
			this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape28.Top = 8.3125F;
			this.Shape28.Width = 0.5625F;
			// 
			// Label185
			// 
			this.Label185.Height = 0.1875F;
			this.Label185.HyperLink = null;
			this.Label185.Left = 0.0625F;
			this.Label185.Name = "Label185";
			this.Label185.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold";
			this.Label185.Text = "Cooperative Housing Corporation Veterans:";
			this.Label185.Top = 4.8125F;
			this.Label185.Width = 2.875F;
			// 
			// Label186
			// 
			this.Label186.Height = 0.3125F;
			this.Label186.HyperLink = null;
			this.Label186.Left = 3.875F;
			this.Label186.Name = "Label186";
			this.Label186.Style = "font-family: \'Tahoma\'; font-size: 7pt; font-weight: bold; text-align: center";
			this.Label186.Text = "NUMBER OF EXEMPTIONS";
			this.Label186.Top = 7.0625F;
			this.Label186.Width = 0.8125F;
			// 
			// Label187
			// 
			this.Label187.Height = 0.1875F;
			this.Label187.HyperLink = null;
			this.Label187.Left = 6.263F;
			this.Label187.Name = "Label187";
			this.Label187.Style = "font-size: 7pt; font-weight: bold; text-align: center; vertical-align: middle";
			this.Label187.Text = "EXEMPT VALUE";
			this.Label187.Top = 7.125F;
			this.Label187.Width = 0.8125F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.1875F;
			this.Shape12.Left = 6.0625F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 1.5625F;
			this.Shape12.Width = 1.25F;
			// 
			// Shape13
			// 
			this.Shape13.Height = 0.1875F;
			this.Shape13.Left = 6.0625F;
			this.Shape13.Name = "Shape13";
			this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape13.Top = 2.25F;
			this.Shape13.Width = 1.25F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.1875F;
			this.Shape11.Left = 6.0625F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 2.625F;
			this.Shape11.Width = 1.25F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.1875F;
			this.Shape10.Left = 6.0625F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 3.3125F;
			this.Shape10.Width = 1.25F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1875F;
			this.Shape9.Left = 6.0625F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 3.6875F;
			this.Shape9.Width = 1.25F;
			// 
			// Shape8
			// 
			this.Shape8.Height = 0.1875F;
			this.Shape8.Left = 6.0625F;
			this.Shape8.Name = "Shape8";
			this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape8.Top = 4.375F;
			this.Shape8.Width = 1.25F;
			// 
			// Shape7
			// 
			this.Shape7.Height = 0.1875F;
			this.Shape7.Left = 6.0625F;
			this.Shape7.Name = "Shape7";
			this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape7.Top = 5.0625F;
			this.Shape7.Width = 1.25F;
			// 
			// Shape5
			// 
			this.Shape5.Height = 0.1875F;
			this.Shape5.Left = 6.0625F;
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 5.75F;
			this.Shape5.Width = 1.25F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.1875F;
			this.Shape4.Left = 6.0625F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 6.125F;
			this.Shape4.Width = 1.25F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.1875F;
			this.Shape3.Left = 6.0625F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 7.375F;
			this.Shape3.Width = 1.25F;
			// 
			// Shape2
			// 
			this.Shape2.Height = 0.1875F;
			this.Shape2.Left = 6.0625F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 7.75F;
			this.Shape2.Width = 1.25F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 0.1875F;
			this.Shape14.Left = 6.0625F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 8.3125F;
			this.Shape14.Width = 1.25F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.1875F;
			this.Shape1.Left = 6.0625F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 9.3125F;
			this.Shape1.Width = 1.25F;
			// 
			// srptVetExemptWorksheet
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtValueTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TXTMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExemptsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label140)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumExempts12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label187)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValueTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox TXTMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExemptsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label140;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label181;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumExempts12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label183;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label184;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label185;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label186;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label187;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
	}
}
