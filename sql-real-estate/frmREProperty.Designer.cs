//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmREProperty.
	/// </summary>
	partial class frmREProperty : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMRRLLANDVAL;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lbl1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lbl2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMRRLBLDGVAL;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMRRLEXEMPTION;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMapLotCopy;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLocation;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label6;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label4;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label3;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIPctPhys1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIPctFunct1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROICond1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIGradePct1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIType1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIYear1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIUnits1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIGradeCd1;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkSound;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROISoundValue;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label65;
		public ToolTipCtl ToolTip;
		public FCGrid GridToolTip;
		public fecherFoundation.FCTextBox txtMRRSLocStreet;
		public fecherFoundation.FCTextBox txtMRRSLocapt;
		public fecherFoundation.FCTextBox txtMRRSLocnumalph;
		public fecherFoundation.FCTextBox txtMRRSMAPLOT;
		public fecherFoundation.FCTextBox MaskEdBox1;
		public fecherFoundation.FCTextBox MaskEdbox3;
		public fecherFoundation.FCTextBox txtCardNumber;
		public FCGrid GridDeletedPhones;
		public Global.T2KDateBox T2KDateInspected;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtMRRSSecOwner;
		public fecherFoundation.FCTextBox txtMRRSZip4;
		public fecherFoundation.FCTextBox txtMRRSRef2;
		public fecherFoundation.FCTextBox txtMRRSName;
		public fecherFoundation.FCTextBox txtMRRSAddr1;
		public fecherFoundation.FCTextBox txtMRRSAddr2;
		public fecherFoundation.FCTextBox txtMRRSAddr3;
		public fecherFoundation.FCTextBox txtMRRSZip;
		public fecherFoundation.FCTextBox txtMRRSRef1;
		public fecherFoundation.FCTextBox txtMRRSState;
		public fecherFoundation.FCTextBox txtMRRSPrevMaster;
		public fecherFoundation.FCTextBox txtEMail;
		public fecherFoundation.FCCheckBox chkBankruptcy;
		public fecherFoundation.FCCheckBox chkTaxAcquired;
		public fecherFoundation.FCButton cmdLogSale;
		public fecherFoundation.FCCheckBox chkRLivingTrust;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public FCGrid GridExempts;
		public FCGrid SaleGrid;
		public fecherFoundation.FCGrid BPGrid;
		public FCGrid gridTranCode;
		public fecherFoundation.FCLabel Label8_37;
		public fecherFoundation.FCLabel Label8_8;
		public fecherFoundation.FCLabel Label8_7;
		public fecherFoundation.FCLabel Label8_5;
		public fecherFoundation.FCLabel Label8_4;
		public fecherFoundation.FCLabel Label8_3;
		public fecherFoundation.FCLabel Label8_2;
		public fecherFoundation.FCLabel Label8_1;
		public fecherFoundation.FCLabel Label8_0;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8_28;
		public fecherFoundation.FCLabel lblDate_7;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCLabel lblDate_0;
		public fecherFoundation.FCLabel lbl1_2;
		public fecherFoundation.FCLabel lbl2_3;
		public fecherFoundation.FCLabel lblDate_4;
		public fecherFoundation.FCLabel lblDate_5;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblTaxable;
		public fecherFoundation.FCLabel lblMRRLLANDVAL_28;
		public fecherFoundation.FCLabel lblMRRLBLDGVAL_29;
		public fecherFoundation.FCLabel lblMRRLEXEMPTION_30;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkZoneOverride;
		public fecherFoundation.FCTextBox txtMRPIOpen2;
		public fecherFoundation.FCTextBox txtMRPIYCoord;
		public fecherFoundation.FCTextBox txtMRPIXCoord;
		public fecherFoundation.FCTextBox txtMRPIOpen1;
		public fecherFoundation.FCTextBox txtMRPIStreetCode;
		public fecherFoundation.FCTextBox txtMRPIAcres;
		public FCGrid gridLandCode;
		public FCGrid gridBldgCode;
		public FCGrid GridLand;
		public FCGrid gridPropertyCode;
		public FCGrid GridNeighborhood;
		public FCGrid GridZone;
		public FCGrid GridSecZone;
		public FCGrid GridTopography;
		public FCGrid GridUtilities;
		public FCGrid GridStreet;
		public fecherFoundation.FCLabel Label8_20;
		public fecherFoundation.FCLabel Label8_19;
		public fecherFoundation.FCLabel Label8_18;
		public fecherFoundation.FCLabel Label8_17;
		public fecherFoundation.FCLabel Label8_16;
		public fecherFoundation.FCLabel Label8_15;
		public fecherFoundation.FCLabel Label8_14;
		public fecherFoundation.FCLabel Label8_13;
		public fecherFoundation.FCLabel Label8_12;
		public fecherFoundation.FCLabel Label8_11;
		public fecherFoundation.FCLabel Label8_10;
		public fecherFoundation.FCLabel Label8_36;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel lblMapLotCopy_0;
		public fecherFoundation.FCLabel lblLocation_0;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCLabel lblMapLotCopy_1;
		public fecherFoundation.FCLabel lblDwel;
		public fecherFoundation.FCGrid GridDwelling3;
		public fecherFoundation.FCGrid GridDwelling2;
		public fecherFoundation.FCGrid GridDwelling1;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCLabel Label6_0;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4_0;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox mebC2Height;
		public fecherFoundation.FCTextBox mebC2Funct;
		public fecherFoundation.FCTextBox mebC2Phys;
		public fecherFoundation.FCTextBox mebC2Condition;
		public fecherFoundation.FCTextBox mebC2Remodel;
		public fecherFoundation.FCTextBox mebC2Built;
		public fecherFoundation.FCTextBox mebC2Heat;
		public fecherFoundation.FCTextBox mebC2Perimeter;
		public fecherFoundation.FCTextBox mebC2Floor;
		public fecherFoundation.FCTextBox mebC2Stories;
		public fecherFoundation.FCTextBox mebC2ExtWalls;
		public fecherFoundation.FCTextBox mebC2Quality;
		public fecherFoundation.FCTextBox mebC2Class;
		public fecherFoundation.FCTextBox mebDwel2;
		public fecherFoundation.FCTextBox mebOcc2;
		public fecherFoundation.FCTextBox mebCMEcon;
		public fecherFoundation.FCTextBox mebC1Funct;
		public fecherFoundation.FCTextBox mebC1Phys;
		public fecherFoundation.FCTextBox mebC1Condition;
		public fecherFoundation.FCTextBox mebC1Remodel;
		public fecherFoundation.FCTextBox mebC1Built;
		public fecherFoundation.FCTextBox mebC1Perimeter;
		public fecherFoundation.FCTextBox mebC1Floor;
		public fecherFoundation.FCTextBox mebC1Stories;
		public fecherFoundation.FCTextBox mebC1ExteriorWalls;
		public fecherFoundation.FCTextBox mebC1Quality;
		public fecherFoundation.FCTextBox mebC1Class;
		public fecherFoundation.FCTextBox mebDwel1;
		public fecherFoundation.FCTextBox mebC1Height;
		public fecherFoundation.FCTextBox mebOcc1;
		public fecherFoundation.FCTextBox mebC1Grade;
		public fecherFoundation.FCTextBox mebc2grade;
		public fecherFoundation.FCTextBox mebC1Heat;
		public fecherFoundation.FCLabel lblcomm;
		public fecherFoundation.FCLabel Label3_32;
		public fecherFoundation.FCLabel Label3_1;
		public fecherFoundation.FCLabel Label3_2;
		public fecherFoundation.FCLabel Label3_3;
		public fecherFoundation.FCLabel Label3_4;
		public fecherFoundation.FCLabel Label3_5;
		public fecherFoundation.FCLabel Label3_6;
		public fecherFoundation.FCLabel Label3_7;
		public fecherFoundation.FCLabel Label3_8;
		public fecherFoundation.FCLabel Label3_9;
		public fecherFoundation.FCLabel Label3_10;
		public fecherFoundation.FCLabel Label3_11;
		public fecherFoundation.FCLabel Label3_12;
		public fecherFoundation.FCLabel Label3_13;
		public fecherFoundation.FCLabel Label3_14;
		public fecherFoundation.FCLabel Label3_15;
		public fecherFoundation.FCLabel Label3_16;
		public fecherFoundation.FCLabel Label3_17;
		public fecherFoundation.FCLabel Label3_18;
		public fecherFoundation.FCLabel Label3_19;
		public fecherFoundation.FCLabel Label3_20;
		public fecherFoundation.FCLabel Label3_21;
		public fecherFoundation.FCLabel Label3_22;
		public fecherFoundation.FCLabel Label3_23;
		public fecherFoundation.FCLabel Label3_24;
		public fecherFoundation.FCLabel Label3_25;
		public fecherFoundation.FCLabel Label3_29;
		public fecherFoundation.FCLabel Label3_30;
		public fecherFoundation.FCLabel Label3_31;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_1;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_2;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_3;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_4;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_5;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_6;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_7;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_8;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_9;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_0;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_0;
		public fecherFoundation.FCTextBox txtMROICond1_0;
		public fecherFoundation.FCTextBox txtMROIGradePct1_0;
		public fecherFoundation.FCTextBox txtMROIType1_0;
		public fecherFoundation.FCTextBox txtMROIYear1_0;
		public fecherFoundation.FCTextBox txtMROIUnits1_0;
		public fecherFoundation.FCTextBox txtMROIType1_9;
		public fecherFoundation.FCTextBox txtMROIType1_8;
		public fecherFoundation.FCTextBox txtMROIType1_7;
		public fecherFoundation.FCTextBox txtMROIType1_6;
		public fecherFoundation.FCTextBox txtMROIType1_5;
		public fecherFoundation.FCTextBox txtMROIType1_4;
		public fecherFoundation.FCTextBox txtMROIType1_3;
		public fecherFoundation.FCTextBox txtMROIType1_2;
		public fecherFoundation.FCTextBox txtMROIType1_1;
		public fecherFoundation.FCTextBox txtMROIYear1_9;
		public fecherFoundation.FCTextBox txtMROIYear1_8;
		public fecherFoundation.FCTextBox txtMROIYear1_7;
		public fecherFoundation.FCTextBox txtMROIYear1_6;
		public fecherFoundation.FCTextBox txtMROIYear1_5;
		public fecherFoundation.FCTextBox txtMROIYear1_4;
		public fecherFoundation.FCTextBox txtMROIYear1_3;
		public fecherFoundation.FCTextBox txtMROIYear1_2;
		public fecherFoundation.FCTextBox txtMROIYear1_1;
		public fecherFoundation.FCTextBox txtMROIUnits1_9;
		public fecherFoundation.FCTextBox txtMROIUnits1_8;
		public fecherFoundation.FCTextBox txtMROIUnits1_7;
		public fecherFoundation.FCTextBox txtMROIUnits1_5;
		public fecherFoundation.FCTextBox txtMROIUnits1_4;
		public fecherFoundation.FCTextBox txtMROIUnits1_2;
		public fecherFoundation.FCTextBox txtMROIUnits1_1;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_8;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_7;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_6;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_5;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_4;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_3;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_2;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_1;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_0;
		public fecherFoundation.FCTextBox txtMROIGradePct1_9;
		public fecherFoundation.FCTextBox txtMROIGradePct1_7;
		public fecherFoundation.FCTextBox txtMROIGradePct1_6;
		public fecherFoundation.FCTextBox txtMROIGradePct1_5;
		public fecherFoundation.FCTextBox txtMROIGradePct1_4;
		public fecherFoundation.FCTextBox txtMROIGradePct1_3;
		public fecherFoundation.FCTextBox txtMROIGradePct1_2;
		public fecherFoundation.FCTextBox txtMROIGradePct1_1;
		public fecherFoundation.FCTextBox txtMROICond1_9;
		public fecherFoundation.FCTextBox txtMROICond1_8;
		public fecherFoundation.FCTextBox txtMROICond1_7;
		public fecherFoundation.FCTextBox txtMROICond1_6;
		public fecherFoundation.FCTextBox txtMROICond1_5;
		public fecherFoundation.FCTextBox txtMROICond1_4;
		public fecherFoundation.FCTextBox txtMROICond1_3;
		public fecherFoundation.FCTextBox txtMROICond1_2;
		public fecherFoundation.FCTextBox txtMROICond1_1;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_9;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_8;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_7;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_6;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_5;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_4;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_2;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_1;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_9;
		public fecherFoundation.FCTextBox txtMROIUnits1_6;
		public fecherFoundation.FCTextBox txtMROIGradePct1_8;
		public fecherFoundation.FCTextBox txtMROIUnits1_3;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_3;
		public fecherFoundation.FCCheckBox chkSound_0;
		public fecherFoundation.FCTextBox txtMROISoundValue_0;
		public fecherFoundation.FCCheckBox chkSound_1;
		public fecherFoundation.FCTextBox txtMROISoundValue_1;
		public fecherFoundation.FCCheckBox chkSound_2;
		public fecherFoundation.FCTextBox txtMROISoundValue_2;
		public fecherFoundation.FCCheckBox chkSound_3;
		public fecherFoundation.FCTextBox txtMROISoundValue_3;
		public fecherFoundation.FCCheckBox chkSound_4;
		public fecherFoundation.FCTextBox txtMROISoundValue_4;
		public fecherFoundation.FCCheckBox chkSound_5;
		public fecherFoundation.FCTextBox txtMROISoundValue_5;
		public fecherFoundation.FCCheckBox chkSound_6;
		public fecherFoundation.FCTextBox txtMROISoundValue_6;
		public fecherFoundation.FCCheckBox chkSound_7;
		public fecherFoundation.FCTextBox txtMROISoundValue_7;
		public fecherFoundation.FCCheckBox chkSound_8;
		public fecherFoundation.FCTextBox txtMROISoundValue_8;
		public fecherFoundation.FCCheckBox chkSound_9;
		public fecherFoundation.FCTextBox txtMROISoundValue_9;
		public fecherFoundation.FCLabel lblOut;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_3;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel Label2_6;
		public fecherFoundation.FCLabel Label2_7;
		public fecherFoundation.FCLabel Label2_8;
		public fecherFoundation.FCLabel lblMapLotCopy_2;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCTabPage SSTab1_Page6;
		public fecherFoundation.FCTextBox txtTotalExpense;
		public fecherFoundation.FCTextBox txtTotalSF;
		public fecherFoundation.FCGrid GridValues;
		public fecherFoundation.FCGrid GridExpenses;
		public fecherFoundation.FCGrid Grid1;
		public fecherFoundation.FCLabel lblTotalExpense;
		public fecherFoundation.FCLabel lblTotalSF;
		public fecherFoundation.FCTabPage SSTab1_Page7;
		public fecherFoundation.FCLabel lblSketchNumber;
		public fecherFoundation.FCLabel lblPicName;
		public fecherFoundation.FCLabel lblName2;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCTextBox txtPicDesc;
		public fecherFoundation.FCButton cmdNextPicture;
		public fecherFoundation.FCButton cmdPrevPicture;
		public fecherFoundation.FCFrame framPicture;
		public fecherFoundation.FCPictureBox imgPicture;
		public fecherFoundation.FCFrame framPicDimensions;
		public fecherFoundation.FCComboBox cmbPrintSize;
		public fecherFoundation.FCToolBar Toolbar1;
		private fecherFoundation.FCToolBarButton BTNPICADDPIC;
		private fecherFoundation.FCToolBarButton BTNPICDELETEPIC;
		private fecherFoundation.FCToolBarButton BTNPRINTPICTURE;
		public fecherFoundation.FCTabPage SSTab1_Page8;
		public fecherFoundation.FCPictureBox Picture2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCPanel framSketch;
		public fecherFoundation.FCPictureBox imSketch;
		public Wisej.Web.HScrollBar hsSketch;
		public Wisej.Web.VScrollBar vsSketch;
		public fecherFoundation.FCButton cmdNextSketch;
		public fecherFoundation.FCButton cmdPrevSketch;
		public fecherFoundation.FCLabel Label8_6;
		public fecherFoundation.FCLabel lblDate_2;
		public fecherFoundation.FCLabel lblSketchPresent;
		public fecherFoundation.FCLabel lblPicturePresent;
		public fecherFoundation.FCLabel lblPendingFlag;
		public fecherFoundation.FCLabel Label65_9;
		public fecherFoundation.FCLabel Label65_8;
		public fecherFoundation.FCLabel Label65_18;
		public fecherFoundation.FCLabel lblNumCards;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel lblMRHLAcctNum;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuAddNewAccount;
		public fecherFoundation.FCToolStripMenuItem mnuAddACard;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteAccount;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCard;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteDwellingData;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteOutbuilding;
		public fecherFoundation.FCToolStripMenuItem mnuSeparateCard;
		public fecherFoundation.FCToolStripMenuItem Separator1;
		public fecherFoundation.FCToolStripMenuItem mnuCalculate;
		public fecherFoundation.FCToolStripMenuItem mnuValuationReport;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocs;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuRECLComment;
		public fecherFoundation.FCToolStripMenuItem mnuCollectionsNote;
		public fecherFoundation.FCToolStripMenuItem mnuAudio;
		public fecherFoundation.FCToolStripMenuItem mnuPlayAudio;
		public fecherFoundation.FCToolStripMenuItem mnuPauseAudio;
		public fecherFoundation.FCToolStripMenuItem mnuStopAudio;
		public fecherFoundation.FCToolStripMenuItem mnuGroup;
		public fecherFoundation.FCToolStripMenuItem mnuMortgage;
		public fecherFoundation.FCToolStripMenuItem mnuinterestedparties;
		public fecherFoundation.FCToolStripMenuItem mnuCopyProperty;
		public fecherFoundation.FCToolStripMenuItem mnuPendingTransfer;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSearchName;
		public fecherFoundation.FCToolStripMenuItem mnuSearchLocation;
		public fecherFoundation.FCToolStripMenuItem mnuSearchMapLot;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator4;
		public fecherFoundation.FCToolStripMenuItem mnuPreviousAccountFromSearch;
		public fecherFoundation.FCToolStripMenuItem mnuNextAccountFromSearch;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAccountInformationSheet;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCards;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPropertyCard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAllPropertyCards;
		public fecherFoundation.FCToolStripMenuItem mnuPrint1pagepropertycard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintLabel;
		public fecherFoundation.FCToolStripMenuItem mnuPrintMailing;
		public fecherFoundation.FCToolStripMenuItem mnuPrintIncomeApproach;
		public fecherFoundation.FCToolStripMenuItem mnuPrintScreen;
		public fecherFoundation.FCToolStripMenuItem mnuPrintInterestedParties;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddOcc;
		public fecherFoundation.FCToolStripMenuItem mnuDelOcc;
		public fecherFoundation.FCToolStripMenuItem mnuIncomeSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuAddExpense;
		public fecherFoundation.FCToolStripMenuItem mnuDelExpense;
		public fecherFoundation.FCToolStripMenuItem mnuSketch;
		public fecherFoundation.FCToolStripMenuItem mnuEditSketch;
		public fecherFoundation.FCToolStripMenuItem mnuNewSketch;
		public fecherFoundation.FCToolStripMenuItem mnuEditConfig;
		public fecherFoundation.FCToolStripMenuItem mnuReassignSketch;
		public fecherFoundation.FCToolStripMenuItem mnuSketchSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuImportSQFT;
		public fecherFoundation.FCToolStripMenuItem mnuAll;
		public fecherFoundation.FCToolStripMenuItem mnuDwellingSqft;
		public fecherFoundation.FCToolStripMenuItem mnuOutbuildingSQFT;
		public fecherFoundation.FCToolStripMenuItem mnuSketchSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteSketch;
		public fecherFoundation.FCToolStripMenuItem mnuPictures;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPicture;
		public fecherFoundation.FCToolStripMenuItem mnuPicSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuImportPicture;
		public fecherFoundation.FCToolStripMenuItem mnuAddPicture;
		public fecherFoundation.FCToolStripMenuItem mnuDeletePicture;
		public fecherFoundation.FCToolStripMenuItem mnuSepar7;
		public fecherFoundation.FCToolStripMenuItem mnuPicMakePrimary;
		public fecherFoundation.FCToolStripMenuItem mnuPicMakeSecondary;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnuSavePending;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator3;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmREProperty));
			this.ToolTip = new TWRE0000.ToolTipCtl();
			this.GridToolTip = new fecherFoundation.FCGrid();
			this.txtMRRSLocStreet = new fecherFoundation.FCTextBox();
			this.txtMRRSLocapt = new fecherFoundation.FCTextBox();
			this.txtMRRSLocnumalph = new fecherFoundation.FCTextBox();
			this.txtMRRSMAPLOT = new fecherFoundation.FCTextBox();
			this.MaskEdBox1 = new fecherFoundation.FCTextBox();
			this.MaskEdbox3 = new fecherFoundation.FCTextBox();
			this.txtCardNumber = new fecherFoundation.FCTextBox();
			this.GridDeletedPhones = new fecherFoundation.FCGrid();
			this.T2KDateInspected = new Global.T2KDateBox();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtMRRSSecOwner = new fecherFoundation.FCTextBox();
			this.txtMRRSZip4 = new fecherFoundation.FCTextBox();
			this.txtMRRSRef2 = new fecherFoundation.FCTextBox();
			this.txtMRRSName = new fecherFoundation.FCTextBox();
			this.txtMRRSAddr1 = new fecherFoundation.FCTextBox();
			this.txtMRRSAddr2 = new fecherFoundation.FCTextBox();
			this.txtMRRSAddr3 = new fecherFoundation.FCTextBox();
			this.txtMRRSZip = new fecherFoundation.FCTextBox();
			this.txtMRRSRef1 = new fecherFoundation.FCTextBox();
			this.txtMRRSState = new fecherFoundation.FCTextBox();
			this.txtMRRSPrevMaster = new fecherFoundation.FCTextBox();
			this.txtEMail = new fecherFoundation.FCTextBox();
			this.chkBankruptcy = new fecherFoundation.FCCheckBox();
			this.chkTaxAcquired = new fecherFoundation.FCCheckBox();
			this.cmdLogSale = new fecherFoundation.FCButton();
			this.chkRLivingTrust = new fecherFoundation.FCCheckBox();
			this.GridExempts = new fecherFoundation.FCGrid();
			this.SaleGrid = new fecherFoundation.FCGrid();
			this.BPGrid = new fecherFoundation.FCGrid();
			this.gridTranCode = new fecherFoundation.FCGrid();
			this.Label8_37 = new fecherFoundation.FCLabel();
			this.Label8_8 = new fecherFoundation.FCLabel();
			this.Label8_7 = new fecherFoundation.FCLabel();
			this.Label8_5 = new fecherFoundation.FCLabel();
			this.Label8_4 = new fecherFoundation.FCLabel();
			this.Label8_3 = new fecherFoundation.FCLabel();
			this.Label8_2 = new fecherFoundation.FCLabel();
			this.Label8_1 = new fecherFoundation.FCLabel();
			this.Label8_0 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8_28 = new fecherFoundation.FCLabel();
			this.lblDate_7 = new fecherFoundation.FCLabel();
			this.imgDocuments = new fecherFoundation.FCPictureBox();
			this.lblDate_0 = new fecherFoundation.FCLabel();
			this.lbl1_2 = new fecherFoundation.FCLabel();
			this.lbl2_3 = new fecherFoundation.FCLabel();
			this.lblDate_4 = new fecherFoundation.FCLabel();
			this.lblDate_5 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.lblTaxable = new fecherFoundation.FCLabel();
			this.lblMRRLLANDVAL_28 = new fecherFoundation.FCLabel();
			this.lblMRRLBLDGVAL_29 = new fecherFoundation.FCLabel();
			this.lblMRRLEXEMPTION_30 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.chkZoneOverride = new fecherFoundation.FCCheckBox();
			this.txtMRPIOpen2 = new fecherFoundation.FCTextBox();
			this.txtMRPIYCoord = new fecherFoundation.FCTextBox();
			this.txtMRPIXCoord = new fecherFoundation.FCTextBox();
			this.txtMRPIOpen1 = new fecherFoundation.FCTextBox();
			this.txtMRPIStreetCode = new fecherFoundation.FCTextBox();
			this.txtMRPIAcres = new fecherFoundation.FCTextBox();
			this.gridLandCode = new fecherFoundation.FCGrid();
			this.gridBldgCode = new fecherFoundation.FCGrid();
			this.gridPropertyCode = new fecherFoundation.FCGrid();
			this.GridNeighborhood = new fecherFoundation.FCGrid();
			this.GridZone = new fecherFoundation.FCGrid();
			this.GridSecZone = new fecherFoundation.FCGrid();
			this.GridTopography = new fecherFoundation.FCGrid();
			this.GridUtilities = new fecherFoundation.FCGrid();
			this.GridStreet = new fecherFoundation.FCGrid();
			this.Label8_20 = new fecherFoundation.FCLabel();
			this.Label8_19 = new fecherFoundation.FCLabel();
			this.Label8_18 = new fecherFoundation.FCLabel();
			this.Label8_17 = new fecherFoundation.FCLabel();
			this.Label8_16 = new fecherFoundation.FCLabel();
			this.Label8_15 = new fecherFoundation.FCLabel();
			this.Label8_14 = new fecherFoundation.FCLabel();
			this.Label8_13 = new fecherFoundation.FCLabel();
			this.Label8_12 = new fecherFoundation.FCLabel();
			this.Label8_11 = new fecherFoundation.FCLabel();
			this.Label8_10 = new fecherFoundation.FCLabel();
			this.Label8_36 = new fecherFoundation.FCLabel();
			this.Label22 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.GridLand = new fecherFoundation.FCGrid();
			this.lblLocation_0 = new fecherFoundation.FCLabel();
			this.lblMapLot = new fecherFoundation.FCLabel();
			this.lblMapLotCopy_0 = new fecherFoundation.FCLabel();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.lblMapLotCopy_1 = new fecherFoundation.FCLabel();
			this.lblDwel = new fecherFoundation.FCLabel();
			this.GridDwelling3 = new fecherFoundation.FCGrid();
			this.GridDwelling2 = new fecherFoundation.FCGrid();
			this.GridDwelling1 = new fecherFoundation.FCGrid();
			this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
			this.Label6_0 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4_0 = new fecherFoundation.FCLabel();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.mebC2Height = new fecherFoundation.FCTextBox();
			this.mebC2Funct = new fecherFoundation.FCTextBox();
			this.mebC2Phys = new fecherFoundation.FCTextBox();
			this.mebC2Condition = new fecherFoundation.FCTextBox();
			this.mebC2Remodel = new fecherFoundation.FCTextBox();
			this.mebC2Built = new fecherFoundation.FCTextBox();
			this.mebC2Heat = new fecherFoundation.FCTextBox();
			this.mebC2Perimeter = new fecherFoundation.FCTextBox();
			this.mebC2Floor = new fecherFoundation.FCTextBox();
			this.mebC2Stories = new fecherFoundation.FCTextBox();
			this.mebC2ExtWalls = new fecherFoundation.FCTextBox();
			this.mebC2Quality = new fecherFoundation.FCTextBox();
			this.mebC2Class = new fecherFoundation.FCTextBox();
			this.mebDwel2 = new fecherFoundation.FCTextBox();
			this.mebOcc2 = new fecherFoundation.FCTextBox();
			this.mebCMEcon = new fecherFoundation.FCTextBox();
			this.mebC1Funct = new fecherFoundation.FCTextBox();
			this.mebC1Phys = new fecherFoundation.FCTextBox();
			this.mebC1Condition = new fecherFoundation.FCTextBox();
			this.mebC1Remodel = new fecherFoundation.FCTextBox();
			this.mebC1Built = new fecherFoundation.FCTextBox();
			this.mebC1Perimeter = new fecherFoundation.FCTextBox();
			this.mebC1Floor = new fecherFoundation.FCTextBox();
			this.mebC1Stories = new fecherFoundation.FCTextBox();
			this.mebC1ExteriorWalls = new fecherFoundation.FCTextBox();
			this.mebC1Quality = new fecherFoundation.FCTextBox();
			this.mebC1Class = new fecherFoundation.FCTextBox();
			this.mebDwel1 = new fecherFoundation.FCTextBox();
			this.mebC1Height = new fecherFoundation.FCTextBox();
			this.mebOcc1 = new fecherFoundation.FCTextBox();
			this.mebC1Grade = new fecherFoundation.FCTextBox();
			this.mebc2grade = new fecherFoundation.FCTextBox();
			this.mebC1Heat = new fecherFoundation.FCTextBox();
			this.lblcomm = new fecherFoundation.FCLabel();
			this.Label3_32 = new fecherFoundation.FCLabel();
			this.Label3_1 = new fecherFoundation.FCLabel();
			this.Label3_2 = new fecherFoundation.FCLabel();
			this.Label3_3 = new fecherFoundation.FCLabel();
			this.Label3_4 = new fecherFoundation.FCLabel();
			this.Label3_5 = new fecherFoundation.FCLabel();
			this.Label3_6 = new fecherFoundation.FCLabel();
			this.Label3_7 = new fecherFoundation.FCLabel();
			this.Label3_8 = new fecherFoundation.FCLabel();
			this.Label3_9 = new fecherFoundation.FCLabel();
			this.Label3_10 = new fecherFoundation.FCLabel();
			this.Label3_11 = new fecherFoundation.FCLabel();
			this.Label3_12 = new fecherFoundation.FCLabel();
			this.Label3_13 = new fecherFoundation.FCLabel();
			this.Label3_14 = new fecherFoundation.FCLabel();
			this.Label3_15 = new fecherFoundation.FCLabel();
			this.Label3_16 = new fecherFoundation.FCLabel();
			this.Label3_17 = new fecherFoundation.FCLabel();
			this.Label3_18 = new fecherFoundation.FCLabel();
			this.Label3_19 = new fecherFoundation.FCLabel();
			this.Label3_20 = new fecherFoundation.FCLabel();
			this.Label3_21 = new fecherFoundation.FCLabel();
			this.Label3_22 = new fecherFoundation.FCLabel();
			this.Label3_23 = new fecherFoundation.FCLabel();
			this.Label3_24 = new fecherFoundation.FCLabel();
			this.Label3_25 = new fecherFoundation.FCLabel();
			this.Label3_29 = new fecherFoundation.FCLabel();
			this.Label3_30 = new fecherFoundation.FCLabel();
			this.Label3_31 = new fecherFoundation.FCLabel();
			this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtMROIPctPhys1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_3 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIPctPhys1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_0 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_3 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIType1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_3 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIYear1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_3 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_0 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_3 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_1 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_9 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_8 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_7 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_6 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_5 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_4 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_3 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_2 = new fecherFoundation.FCTextBox();
			this.txtMROICond1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_7 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_5 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_4 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_2 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_1 = new fecherFoundation.FCTextBox();
			this.txtMROIGradeCd1_9 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_6 = new fecherFoundation.FCTextBox();
			this.txtMROIGradePct1_8 = new fecherFoundation.FCTextBox();
			this.txtMROIUnits1_3 = new fecherFoundation.FCTextBox();
			this.txtMROIPctFunct1_3 = new fecherFoundation.FCTextBox();
			this.chkSound_0 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_0 = new fecherFoundation.FCTextBox();
			this.chkSound_1 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_1 = new fecherFoundation.FCTextBox();
			this.chkSound_2 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_2 = new fecherFoundation.FCTextBox();
			this.chkSound_3 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_3 = new fecherFoundation.FCTextBox();
			this.chkSound_4 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_4 = new fecherFoundation.FCTextBox();
			this.chkSound_5 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_5 = new fecherFoundation.FCTextBox();
			this.chkSound_6 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_6 = new fecherFoundation.FCTextBox();
			this.chkSound_7 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_7 = new fecherFoundation.FCTextBox();
			this.chkSound_8 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_8 = new fecherFoundation.FCTextBox();
			this.chkSound_9 = new fecherFoundation.FCCheckBox();
			this.txtMROISoundValue_9 = new fecherFoundation.FCTextBox();
			this.lblOut = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label2_2 = new fecherFoundation.FCLabel();
			this.Label2_3 = new fecherFoundation.FCLabel();
			this.Label2_4 = new fecherFoundation.FCLabel();
			this.Label2_5 = new fecherFoundation.FCLabel();
			this.Label2_6 = new fecherFoundation.FCLabel();
			this.Label2_7 = new fecherFoundation.FCLabel();
			this.Label2_8 = new fecherFoundation.FCLabel();
			this.lblMapLotCopy_2 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.SSTab1_Page6 = new fecherFoundation.FCTabPage();
			this.txtTotalExpense = new fecherFoundation.FCTextBox();
			this.txtTotalSF = new fecherFoundation.FCTextBox();
			this.GridValues = new fecherFoundation.FCGrid();
			this.GridExpenses = new fecherFoundation.FCGrid();
			this.Grid1 = new fecherFoundation.FCGrid();
			this.lblTotalExpense = new fecherFoundation.FCLabel();
			this.lblTotalSF = new fecherFoundation.FCLabel();
			this.SSTab1_Page7 = new fecherFoundation.FCTabPage();
			this.lblSketchNumber = new fecherFoundation.FCLabel();
			this.lblPicName = new fecherFoundation.FCLabel();
			this.lblName2 = new fecherFoundation.FCLabel();
			this.Picture1 = new fecherFoundation.FCPictureBox();
			this.txtPicDesc = new fecherFoundation.FCTextBox();
			this.cmdNextPicture = new fecherFoundation.FCButton();
			this.cmdPrevPicture = new fecherFoundation.FCButton();
			this.framPicture = new fecherFoundation.FCFrame();
			this.imgPicture = new fecherFoundation.FCPictureBox();
			this.framPicDimensions = new fecherFoundation.FCFrame();
			this.cmbPrintSize = new fecherFoundation.FCComboBox();
			this.Toolbar1 = new fecherFoundation.FCToolBar();
			this.BTNPICADDPIC = new fecherFoundation.FCToolBarButton();
			this.BTNPICDELETEPIC = new fecherFoundation.FCToolBarButton();
			this.BTNPRINTPICTURE = new fecherFoundation.FCToolBarButton();
			this.SSTab1_Page8 = new fecherFoundation.FCTabPage();
			this.Picture2 = new fecherFoundation.FCPictureBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.framSketch = new fecherFoundation.FCPanel();
			this.imSketch = new fecherFoundation.FCPictureBox();
			this.hsSketch = new Wisej.Web.HScrollBar();
			this.vsSketch = new Wisej.Web.VScrollBar();
			this.cmdNextSketch = new fecherFoundation.FCButton();
			this.cmdPrevSketch = new fecherFoundation.FCButton();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.Label8_6 = new fecherFoundation.FCLabel();
			this.lblDate_2 = new fecherFoundation.FCLabel();
			this.lblSketchPresent = new fecherFoundation.FCLabel();
			this.lblPicturePresent = new fecherFoundation.FCLabel();
			this.lblPendingFlag = new fecherFoundation.FCLabel();
			this.Label65_9 = new fecherFoundation.FCLabel();
			this.Label65_8 = new fecherFoundation.FCLabel();
			this.Label65_18 = new fecherFoundation.FCLabel();
			this.lblNumCards = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.lblComment = new fecherFoundation.FCLabel();
			this.lblMRHLAcctNum = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuAddNewAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddACard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteDwellingData = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteOutbuilding = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeparateCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuValuationReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuViewDocs = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRECLComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCollectionsNote = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAudio = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPlayAudio = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPauseAudio = new fecherFoundation.FCToolStripMenuItem();
			this.mnuStopAudio = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGroup = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMortgage = new fecherFoundation.FCToolStripMenuItem();
			this.mnuinterestedparties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCopyProperty = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPendingTransfer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintAccountInformationSheet = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintCards = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPropertyCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintAllPropertyCards = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint1pagepropertycard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintLabel = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintMailing = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintIncomeApproach = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintScreen = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintInterestedParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddOcc = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelOcc = new fecherFoundation.FCToolStripMenuItem();
			this.mnuIncomeSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddExpense = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelExpense = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSketch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditSketch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNewSketch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditConfig = new fecherFoundation.FCToolStripMenuItem();
			this.mnuReassignSketch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSketchSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportSQFT = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDwellingSqft = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOutbuildingSQFT = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSketchSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteSketch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPictures = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPicture = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPicSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportPicture = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddPicture = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeletePicture = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar7 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPicMakePrimary = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPicMakeSecondary = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSavePending = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSearchName = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSearchLocation = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSearchMapLot = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPreviousAccountFromSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNextAccountFromSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCalculate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
			this.Separator1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeparator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeparator4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeparator3 = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdCalculate = new fecherFoundation.FCButton();
			this.cmdComment = new fecherFoundation.FCButton();
			this.cmdPreviousAccountFromSearch = new fecherFoundation.FCButton();
			this.cmdNextAccountFromSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridToolTip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDateInspected)).BeginInit();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLogSale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRLivingTrust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExempts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BPGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkZoneOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridLandCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridBldgCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSecZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTopography)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridUtilities)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLand)).BeginInit();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling1)).BeginInit();
			this.SSTab1_Page4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			this.SSTab1_Page5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_9)).BeginInit();
			this.SSTab1_Page6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridValues)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExpenses)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
			this.SSTab1_Page7.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNextPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framPicture)).BeginInit();
			this.framPicture.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framPicDimensions)).BeginInit();
			this.framPicDimensions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).BeginInit();
			this.SSTab1_Page8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framSketch)).BeginInit();
			this.framSketch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imSketch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNextSketch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevSketch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreviousAccountFromSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNextAccountFromSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 1120);
			this.BottomPanel.Size = new System.Drawing.Size(1053, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.txtMRRSLocStreet);
			this.ClientArea.Controls.Add(this.txtMRRSLocapt);
			this.ClientArea.Controls.Add(this.txtMRRSLocnumalph);
			this.ClientArea.Controls.Add(this.txtMRRSMAPLOT);
			this.ClientArea.Controls.Add(this.MaskEdBox1);
			this.ClientArea.Controls.Add(this.MaskEdbox3);
			this.ClientArea.Controls.Add(this.txtCardNumber);
			this.ClientArea.Controls.Add(this.GridDeletedPhones);
			this.ClientArea.Controls.Add(this.T2KDateInspected);
			this.ClientArea.Controls.Add(this.Label8_6);
			this.ClientArea.Controls.Add(this.lblDate_2);
			this.ClientArea.Controls.Add(this.lblSketchPresent);
			this.ClientArea.Controls.Add(this.lblPicturePresent);
			this.ClientArea.Controls.Add(this.lblPendingFlag);
			this.ClientArea.Controls.Add(this.Label65_9);
			this.ClientArea.Controls.Add(this.Label65_8);
			this.ClientArea.Controls.Add(this.Label65_18);
			this.ClientArea.Controls.Add(this.lblNumCards);
			this.ClientArea.Controls.Add(this.Label11);
			this.ClientArea.Controls.Add(this.Label10);
			this.ClientArea.Controls.Add(this.lblComment);
			this.ClientArea.Controls.Add(this.lblMRHLAcctNum);
			this.ClientArea.Controls.Add(this.ToolTip);
			this.ClientArea.Controls.Add(this.GridToolTip);
			this.ClientArea.Size = new System.Drawing.Size(1073, 606);
			this.ClientArea.TabIndex = 0;
			this.ClientArea.Controls.SetChildIndex(this.GridToolTip, 0);
			this.ClientArea.Controls.SetChildIndex(this.ToolTip, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblMRHLAcctNum, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblComment, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label10, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label11, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblNumCards, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label65_18, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label65_8, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label65_9, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPendingFlag, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPicturePresent, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblSketchPresent, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblDate_2, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label8_6, 0);
			this.ClientArea.Controls.SetChildIndex(this.T2KDateInspected, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDeletedPhones, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtCardNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.MaskEdbox3, 0);
			this.ClientArea.Controls.SetChildIndex(this.MaskEdBox1, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRSMAPLOT, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRSLocnumalph, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRSLocapt, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRSLocStreet, 0);
			this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNextAccountFromSearch);
			this.TopPanel.Controls.Add(this.cmdPreviousAccountFromSearch);
			this.TopPanel.Controls.Add(this.cmdComment);
			this.TopPanel.Controls.Add(this.cmdCalculate);
			this.TopPanel.Size = new System.Drawing.Size(1073, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdCalculate, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPreviousAccountFromSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNextAccountFromSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(249, 30);
			this.HeaderText.Text = "Account Maintenance";
			// 
			// ToolTip
			// 
			this.ToolTip.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToolTip.BackgroundImage")));
			this.ToolTip.CurrentTip = null;
			this.ToolTip.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ToolTip.Name = "ToolTip";
			this.ToolTip.Size = new System.Drawing.Size(615, 96);
			this.ToolTip.TabIndex = 282;
			this.ToolTip.UserControlForeColor = System.Drawing.SystemColors.ControlText;
			this.ToolTip.UserControlHeight = 1440;
			this.ToolTip.UserControlWidth = 9225;
			this.ToolTip.Visible = false;
			// 
			// GridToolTip
			// 
			this.GridToolTip.Cols = 10;
			this.GridToolTip.Location = new System.Drawing.Point(13, 19);
			this.GridToolTip.Name = "GridToolTip";
			this.GridToolTip.Rows = 50;
			this.GridToolTip.ShowFocusCell = false;
			this.GridToolTip.Size = new System.Drawing.Size(289, 31);
			this.GridToolTip.TabIndex = 283;
			this.GridToolTip.Visible = false;
			this.GridToolTip.DoubleClick += new System.EventHandler(this.GridToolTip_DblClick);
			// 
			// txtMRRSLocStreet
			// 
			this.txtMRRSLocStreet.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSLocStreet.Location = new System.Drawing.Point(553, 80);
			this.txtMRRSLocStreet.MaxLength = 26;
			this.txtMRRSLocStreet.Name = "txtMRRSLocStreet";
			this.txtMRRSLocStreet.Size = new System.Drawing.Size(277, 40);
			this.txtMRRSLocStreet.TabIndex = 21;
			this.txtMRRSLocStreet.Tag = "address";
			this.txtMRRSLocStreet.Enter += new System.EventHandler(this.txtMRRSLocStreet_Enter);
			this.txtMRRSLocStreet.DoubleClick += new System.EventHandler(this.txtMRRSLocStreet_DoubleClick);
			this.txtMRRSLocStreet.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSLocStreet_Validating);
			this.txtMRRSLocStreet.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSLocStreet_KeyDown);
			this.txtMRRSLocStreet.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLocStreet_KeyUp);
			// 
			// txtMRRSLocapt
			// 
			this.txtMRRSLocapt.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSLocapt.Location = new System.Drawing.Point(483, 80);
			this.txtMRRSLocapt.MaxLength = 1;
			this.txtMRRSLocapt.Name = "txtMRRSLocapt";
			this.txtMRRSLocapt.Size = new System.Drawing.Size(60, 40);
			this.txtMRRSLocapt.TabIndex = 20;
			this.txtMRRSLocapt.Tag = "address";
			this.txtMRRSLocapt.Enter += new System.EventHandler(this.txtMRRSLocapt_Enter);
			this.txtMRRSLocapt.DoubleClick += new System.EventHandler(this.txtMRRSLocapt_DoubleClick);
			this.txtMRRSLocapt.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSLocapt_Validating);
			this.txtMRRSLocapt.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSLocapt_KeyDown);
			this.txtMRRSLocapt.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRRSLocapt_KeyPress);
			// 
			// txtMRRSLocnumalph
			// 
			this.txtMRRSLocnumalph.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSLocnumalph.Location = new System.Drawing.Point(413, 80);
			this.txtMRRSLocnumalph.MaxLength = 6;
			this.txtMRRSLocnumalph.Name = "txtMRRSLocnumalph";
			this.txtMRRSLocnumalph.Size = new System.Drawing.Size(60, 40);
			this.txtMRRSLocnumalph.TabIndex = 19;
			this.txtMRRSLocnumalph.Tag = "address";
			this.txtMRRSLocnumalph.Enter += new System.EventHandler(this.txtMRRSLocnumalph_Enter);
			this.txtMRRSLocnumalph.DoubleClick += new System.EventHandler(this.txtMRRSLocnumalph_DoubleClick);
			this.txtMRRSLocnumalph.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSLocnumalph_Validating);
			this.txtMRRSLocnumalph.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSLocnumalph_KeyDown);
			this.txtMRRSLocnumalph.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLocnumalph_KeyUp);
			// 
			// txtMRRSMAPLOT
			// 
			this.txtMRRSMAPLOT.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSMAPLOT.Location = new System.Drawing.Point(115, 80);
			this.txtMRRSMAPLOT.MaxLength = 17;
			this.txtMRRSMAPLOT.Name = "txtMRRSMAPLOT";
			this.txtMRRSMAPLOT.Size = new System.Drawing.Size(170, 40);
			this.txtMRRSMAPLOT.TabIndex = 17;
			this.txtMRRSMAPLOT.Tag = "address";
			this.txtMRRSMAPLOT.Enter += new System.EventHandler(this.txtMRRSMAPLOT_Enter);
			this.txtMRRSMAPLOT.DoubleClick += new System.EventHandler(this.txtMRRSMAPLOT_DoubleClick);
			this.txtMRRSMAPLOT.TextChanged += new System.EventHandler(this.txtMRRSMAPLOT_TextChanged);
			this.txtMRRSMAPLOT.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSMAPLOT_Validating);
			this.txtMRRSMAPLOT.MouseDown += new Wisej.Web.MouseEventHandler(this.txtMRRSMAPLOT_MouseDown);
			this.txtMRRSMAPLOT.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRRSMAPLOT_MouseMove);
			this.txtMRRSMAPLOT.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSMAPLOT_KeyDown);
			// 
			// MaskEdBox1
			// 
			this.MaskEdBox1.BackColor = System.Drawing.SystemColors.Window;
			this.MaskEdBox1.Location = new System.Drawing.Point(765, 40);
			this.MaskEdBox1.Name = "MaskEdBox1";
			this.MaskEdBox1.Size = new System.Drawing.Size(45, 16);
			this.MaskEdBox1.TabIndex = 13;
			this.MaskEdBox1.Tag = "land";
			this.MaskEdBox1.Enter += new System.EventHandler(this.MaskEdBox1_Enter);
			this.MaskEdBox1.Validating += new System.ComponentModel.CancelEventHandler(this.MaskEdBox1_Validating);
			this.MaskEdBox1.KeyUp += new Wisej.Web.KeyEventHandler(this.MaskEdBox1_KeyUp);
			// 
			// MaskEdbox3
			// 
			this.MaskEdbox3.BackColor = System.Drawing.SystemColors.Window;
			this.MaskEdbox3.Location = new System.Drawing.Point(550, 40);
			this.MaskEdbox3.Name = "MaskEdbox3";
			this.MaskEdbox3.Size = new System.Drawing.Size(45, 16);
			this.MaskEdbox3.TabIndex = 11;
			this.MaskEdbox3.Tag = "land";
			this.MaskEdbox3.Enter += new System.EventHandler(this.MaskEdbox3_Enter);
			this.MaskEdbox3.Validating += new System.ComponentModel.CancelEventHandler(this.MaskEdbox3_Validating);
			this.MaskEdbox3.KeyUp += new Wisej.Web.KeyEventHandler(this.MaskEdbox3_KeyUp);
			// 
			// txtCardNumber
			// 
			this.txtCardNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtCardNumber.Location = new System.Drawing.Point(193, 30);
			this.txtCardNumber.Name = "txtCardNumber";
			this.txtCardNumber.Size = new System.Drawing.Size(44, 40);
			this.txtCardNumber.TabIndex = 2;
			this.txtCardNumber.TextChanged += new System.EventHandler(this.txtCardNumber_TextChanged);
			this.txtCardNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCardNumber_Validating);
			// 
			// GridDeletedPhones
			// 
			this.GridDeletedPhones.ColumnHeadersVisible = false;
			this.GridDeletedPhones.FixedRows = 0;
			this.GridDeletedPhones.Location = new System.Drawing.Point(847, 94);
			this.GridDeletedPhones.Name = "GridDeletedPhones";
			this.GridDeletedPhones.Rows = 0;
			this.GridDeletedPhones.ShowFocusCell = false;
			this.GridDeletedPhones.Size = new System.Drawing.Size(50, 19);
			this.GridDeletedPhones.TabIndex = 22;
			this.GridDeletedPhones.Visible = false;
			// 
			// T2KDateInspected
			// 
			this.T2KDateInspected.Location = new System.Drawing.Point(977, 30);
			this.T2KDateInspected.Mask = "##/##/####";
			this.T2KDateInspected.MaxLength = 10;
			this.T2KDateInspected.Name = "T2KDateInspected";
			this.T2KDateInspected.Size = new System.Drawing.Size(115, 22);
			this.T2KDateInspected.TabIndex = 15;
			this.T2KDateInspected.Tag = "land";
			this.T2KDateInspected.Leave += new System.EventHandler(this.T2KDateInspected_Leave);
			this.T2KDateInspected.Validating += new System.ComponentModel.CancelEventHandler(this.T2KDateInspected_Validate);
			// 
			// SSTab1
			// 
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Controls.Add(this.SSTab1_Page4);
			this.SSTab1.Controls.Add(this.SSTab1_Page5);
			this.SSTab1.Controls.Add(this.SSTab1_Page6);
			this.SSTab1.Controls.Add(this.SSTab1_Page7);
			this.SSTab1.Controls.Add(this.SSTab1_Page8);
			this.SSTab1.Location = new System.Drawing.Point(30, 150);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(1043, 970);
			this.SSTab1.TabIndex = 23;
			this.SSTab1.Text = "Property";
			this.SSTab1.SelectedIndexChanged += new System.EventHandler(this.SSTab1_SelectedIndexChanged);
			this.SSTab1.Enter += new System.EventHandler(this.SSTab1_Enter);
			this.SSTab1.KeyDown += new Wisej.Web.KeyEventHandler(this.SSTab1_KeyDown);
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.Frame3);
			this.SSTab1_Page1.Controls.Add(this.Label1);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page1.Text = "Property";
			// 
			// Frame3
			// 
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.txtMRRSSecOwner);
			this.Frame3.Controls.Add(this.txtMRRSZip4);
			this.Frame3.Controls.Add(this.txtMRRSRef2);
			this.Frame3.Controls.Add(this.txtMRRSName);
			this.Frame3.Controls.Add(this.txtMRRSAddr1);
			this.Frame3.Controls.Add(this.txtMRRSAddr2);
			this.Frame3.Controls.Add(this.txtMRRSAddr3);
			this.Frame3.Controls.Add(this.txtMRRSZip);
			this.Frame3.Controls.Add(this.txtMRRSRef1);
			this.Frame3.Controls.Add(this.txtMRRSState);
			this.Frame3.Controls.Add(this.txtMRRSPrevMaster);
			this.Frame3.Controls.Add(this.txtEMail);
			this.Frame3.Controls.Add(this.chkBankruptcy);
			this.Frame3.Controls.Add(this.chkTaxAcquired);
			this.Frame3.Controls.Add(this.cmdLogSale);
			this.Frame3.Controls.Add(this.chkRLivingTrust);
			this.Frame3.Controls.Add(this.GridExempts);
			this.Frame3.Controls.Add(this.SaleGrid);
			this.Frame3.Controls.Add(this.BPGrid);
			this.Frame3.Controls.Add(this.gridTranCode);
			this.Frame3.Controls.Add(this.Label8_37);
			this.Frame3.Controls.Add(this.Label8_8);
			this.Frame3.Controls.Add(this.Label8_7);
			this.Frame3.Controls.Add(this.Label8_5);
			this.Frame3.Controls.Add(this.Label8_4);
			this.Frame3.Controls.Add(this.Label8_3);
			this.Frame3.Controls.Add(this.Label8_2);
			this.Frame3.Controls.Add(this.Label8_1);
			this.Frame3.Controls.Add(this.Label8_0);
			this.Frame3.Controls.Add(this.Label9);
			this.Frame3.Controls.Add(this.Label8_28);
			this.Frame3.Controls.Add(this.lblDate_7);
			this.Frame3.Controls.Add(this.imgDocuments);
			this.Frame3.Controls.Add(this.lblDate_0);
			this.Frame3.Controls.Add(this.lbl1_2);
			this.Frame3.Controls.Add(this.lbl2_3);
			this.Frame3.Controls.Add(this.lblDate_4);
			this.Frame3.Controls.Add(this.lblDate_5);
			this.Frame3.Controls.Add(this.Label7);
			this.Frame3.Controls.Add(this.lblTaxable);
			this.Frame3.Controls.Add(this.lblMRRLLANDVAL_28);
			this.Frame3.Controls.Add(this.lblMRRLBLDGVAL_29);
			this.Frame3.Controls.Add(this.lblMRRLEXEMPTION_30);
			this.Frame3.Dock = Wisej.Web.DockStyle.Fill;
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(1041, 934);
			this.Frame3.TabIndex = 181;
			this.Frame3.MouseMove += new Wisej.Web.MouseEventHandler(this.Frame3_MouseMove);
			// 
			// txtMRRSSecOwner
			// 
			this.txtMRRSSecOwner.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSSecOwner.Location = new System.Drawing.Point(130, 80);
			this.txtMRRSSecOwner.MaxLength = 50;
			this.txtMRRSSecOwner.Name = "txtMRRSSecOwner";
			this.txtMRRSSecOwner.Size = new System.Drawing.Size(381, 40);
			this.txtMRRSSecOwner.TabIndex = 301;
			this.txtMRRSSecOwner.Tag = "address";
			this.txtMRRSSecOwner.Enter += new System.EventHandler(this.txtMRRSSecOwner_Enter);
			this.txtMRRSSecOwner.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSSecOwner_Validating);
			this.txtMRRSSecOwner.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSSecOwner_KeyDown);
			// 
			// txtMRRSZip4
			// 
			this.txtMRRSZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSZip4.Location = new System.Drawing.Point(582, 280);
			this.txtMRRSZip4.MaxLength = 4;
			this.txtMRRSZip4.Name = "txtMRRSZip4";
			this.txtMRRSZip4.Size = new System.Drawing.Size(68, 40);
			this.txtMRRSZip4.TabIndex = 300;
			this.txtMRRSZip4.Tag = "address";
			this.txtMRRSZip4.Enter += new System.EventHandler(this.txtMRRSZip4_Enter);
			this.txtMRRSZip4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSZip4_Validating);
			this.txtMRRSZip4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSZip4_KeyDown);
			// 
			// txtMRRSRef2
			// 
			this.txtMRRSRef2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSRef2.Location = new System.Drawing.Point(130, 448);
			this.txtMRRSRef2.MaxLength = 44;
			this.txtMRRSRef2.Name = "txtMRRSRef2";
			this.txtMRRSRef2.Size = new System.Drawing.Size(302, 40);
			this.txtMRRSRef2.TabIndex = 299;
			this.txtMRRSRef2.Tag = "address";
			this.txtMRRSRef2.Enter += new System.EventHandler(this.txtMRRSRef2_Enter);
			this.txtMRRSRef2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSRef2_Validating);
			this.txtMRRSRef2.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRRSRef2_MouseMove);
			this.txtMRRSRef2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSRef2_KeyDown);
			// 
			// txtMRRSName
			// 
			this.txtMRRSName.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSName.Location = new System.Drawing.Point(130, 30);
			this.txtMRRSName.MaxLength = 50;
			this.txtMRRSName.Name = "txtMRRSName";
			this.txtMRRSName.Size = new System.Drawing.Size(381, 40);
			this.txtMRRSName.TabIndex = 298;
			this.txtMRRSName.Tag = "address";
			this.txtMRRSName.Enter += new System.EventHandler(this.txtMRRSName_Enter);
			this.txtMRRSName.DoubleClick += new System.EventHandler(this.txtMRRSName_DoubleClick);
			this.txtMRRSName.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSName_Validating);
			this.txtMRRSName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSName_KeyDown);
			this.txtMRRSName.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSName_KeyUp);
			// 
			// txtMRRSAddr1
			// 
			this.txtMRRSAddr1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSAddr1.Location = new System.Drawing.Point(130, 180);
			this.txtMRRSAddr1.MaxLength = 49;
			this.txtMRRSAddr1.Name = "txtMRRSAddr1";
			this.txtMRRSAddr1.Size = new System.Drawing.Size(203, 40);
			this.txtMRRSAddr1.TabIndex = 297;
			this.txtMRRSAddr1.Tag = "address";
			this.txtMRRSAddr1.Enter += new System.EventHandler(this.txtMRRSAddr1_Enter);
			this.txtMRRSAddr1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSAddr1_Validating);
			this.txtMRRSAddr1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSAddr1_KeyDown);
			// 
			// txtMRRSAddr2
			// 
			this.txtMRRSAddr2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSAddr2.Location = new System.Drawing.Point(130, 230);
			this.txtMRRSAddr2.MaxLength = 49;
			this.txtMRRSAddr2.Name = "txtMRRSAddr2";
			this.txtMRRSAddr2.Size = new System.Drawing.Size(168, 40);
			this.txtMRRSAddr2.TabIndex = 296;
			this.txtMRRSAddr2.Tag = "address";
			this.txtMRRSAddr2.Enter += new System.EventHandler(this.txtMRRSAddr2_Enter);
			this.txtMRRSAddr2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSAddr2_Validating);
			this.txtMRRSAddr2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSAddr2_KeyDown);
			// 
			// txtMRRSAddr3
			// 
			this.txtMRRSAddr3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSAddr3.Location = new System.Drawing.Point(130, 280);
			this.txtMRRSAddr3.MaxLength = 24;
			this.txtMRRSAddr3.Name = "txtMRRSAddr3";
			this.txtMRRSAddr3.Size = new System.Drawing.Size(203, 40);
			this.txtMRRSAddr3.TabIndex = 295;
			this.txtMRRSAddr3.Tag = "address";
			this.txtMRRSAddr3.Enter += new System.EventHandler(this.txtMRRSAddr3_Enter);
			this.txtMRRSAddr3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSAddr3_Validating);
			this.txtMRRSAddr3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSAddr3_KeyDown);
			// 
			// txtMRRSZip
			// 
			this.txtMRRSZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSZip.Location = new System.Drawing.Point(494, 280);
			this.txtMRRSZip.MaxLength = 5;
			this.txtMRRSZip.Name = "txtMRRSZip";
			this.txtMRRSZip.Size = new System.Drawing.Size(78, 40);
			this.txtMRRSZip.TabIndex = 294;
			this.txtMRRSZip.Tag = "address";
			this.txtMRRSZip.Enter += new System.EventHandler(this.txtMRRSZip_Enter);
			this.txtMRRSZip.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSZip_Validating);
			this.txtMRRSZip.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSZip_KeyDown);
			// 
			// txtMRRSRef1
			// 
			this.txtMRRSRef1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSRef1.Location = new System.Drawing.Point(130, 398);
			this.txtMRRSRef1.MaxLength = 44;
			this.txtMRRSRef1.Name = "txtMRRSRef1";
			this.txtMRRSRef1.Size = new System.Drawing.Size(302, 40);
			this.txtMRRSRef1.TabIndex = 293;
			this.txtMRRSRef1.Tag = "address";
			this.txtMRRSRef1.Enter += new System.EventHandler(this.txtMRRSRef1_Enter);
			this.txtMRRSRef1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSRef1_Validating);
			this.txtMRRSRef1.MouseDown += new Wisej.Web.MouseEventHandler(this.txtMRRSRef1_MouseDown);
			this.txtMRRSRef1.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRRSRef1_MouseMove);
			this.txtMRRSRef1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSRef1_KeyDown);
			// 
			// txtMRRSState
			// 
			this.txtMRRSState.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSState.Location = new System.Drawing.Point(396, 280);
			this.txtMRRSState.MaxLength = 2;
			this.txtMRRSState.Name = "txtMRRSState";
			this.txtMRRSState.Size = new System.Drawing.Size(51, 40);
			this.txtMRRSState.TabIndex = 292;
			this.txtMRRSState.Tag = "address";
			this.txtMRRSState.Enter += new System.EventHandler(this.txtMRRSState_Enter);
			this.txtMRRSState.Leave += new System.EventHandler(this.txtMRRSState_Leave);
			this.txtMRRSState.DoubleClick += new System.EventHandler(this.txtMRRSState_DoubleClick);
			this.txtMRRSState.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSState_Validating);
			this.txtMRRSState.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSState_KeyDown);
			this.txtMRRSState.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSState_KeyUp);
			// 
			// txtMRRSPrevMaster
			// 
			this.txtMRRSPrevMaster.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRRSPrevMaster.Location = new System.Drawing.Point(131, 130);
			this.txtMRRSPrevMaster.MaxLength = 50;
			this.txtMRRSPrevMaster.Name = "txtMRRSPrevMaster";
			this.txtMRRSPrevMaster.Size = new System.Drawing.Size(381, 40);
			this.txtMRRSPrevMaster.TabIndex = 291;
			this.txtMRRSPrevMaster.Tag = "address";
			this.txtMRRSPrevMaster.Enter += new System.EventHandler(this.txtMRRSPrevMaster_Enter);
			this.txtMRRSPrevMaster.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRSPrevMaster_Validating);
			this.txtMRRSPrevMaster.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSPrevMaster_KeyDown);
			// 
			// txtEMail
			// 
			this.txtEMail.BackColor = System.Drawing.SystemColors.Window;
			this.txtEMail.Location = new System.Drawing.Point(130, 330);
			this.txtEMail.Name = "txtEMail";
			this.txtEMail.Size = new System.Drawing.Size(291, 40);
			this.txtEMail.TabIndex = 290;
			this.txtEMail.Tag = "address";
			// 
			// chkBankruptcy
			// 
			this.chkBankruptcy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkBankruptcy.Location = new System.Drawing.Point(230, 563);
			this.chkBankruptcy.Name = "chkBankruptcy";
			this.chkBankruptcy.Size = new System.Drawing.Size(93, 23);
			this.chkBankruptcy.TabIndex = 289;
			this.chkBankruptcy.Text = "Bankruptcy";
			this.chkBankruptcy.CheckedChanged += new System.EventHandler(this.chkBankruptcy_CheckedChanged);
			// 
			// chkTaxAcquired
			// 
			this.chkTaxAcquired.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkTaxAcquired.Location = new System.Drawing.Point(20, 563);
			this.chkTaxAcquired.Name = "chkTaxAcquired";
			this.chkTaxAcquired.Size = new System.Drawing.Size(105, 23);
			this.chkTaxAcquired.TabIndex = 288;
			this.chkTaxAcquired.Text = "Tax Acquired";
			this.chkTaxAcquired.CheckedChanged += new System.EventHandler(this.chkTaxAcquired_CheckedChanged);
			// 
			// cmdLogSale
			// 
			this.cmdLogSale.AppearanceKey = "actionButton";
			this.cmdLogSale.ForeColor = System.Drawing.Color.White;
			this.cmdLogSale.Location = new System.Drawing.Point(20, 775);
			this.cmdLogSale.Name = "cmdLogSale";
			this.cmdLogSale.Size = new System.Drawing.Size(194, 40);
			this.cmdLogSale.TabIndex = 8;
			this.cmdLogSale.Tag = "sale";
			this.cmdLogSale.Text = "Save as Sale Record";
			this.ToolTip1.SetToolTip(this.cmdLogSale, "Used when validity code isn\'t 1 to force a save in the sale record.");
			this.cmdLogSale.Click += new System.EventHandler(this.cmdLogSale_Click);
			// 
			// chkRLivingTrust
			// 
			this.chkRLivingTrust.Location = new System.Drawing.Point(668, 344);
			this.chkRLivingTrust.Name = "chkRLivingTrust";
			this.chkRLivingTrust.Size = new System.Drawing.Size(162, 23);
			this.chkRLivingTrust.TabIndex = 5;
			this.chkRLivingTrust.Tag = "land";
			this.chkRLivingTrust.Text = "Revocable Living Trust";
			this.chkRLivingTrust.CheckedChanged += new System.EventHandler(this.chkRLivingTrust_CheckedChanged);
			this.chkRLivingTrust.MouseDown += new Wisej.Web.MouseEventHandler(this.chkRLivingTrust_MouseDown);
			this.chkRLivingTrust.MouseMove += new Wisej.Web.MouseEventHandler(this.chkRLivingTrust_MouseMove);
			this.chkRLivingTrust.KeyDown += new Wisej.Web.KeyEventHandler(this.chkRLivingTrust_KeyDown);
			// 
			// GridExempts
			// 
			this.GridExempts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridExempts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridExempts.ExtendLastCol = true;
			this.GridExempts.FixedCols = 0;
			this.GridExempts.Location = new System.Drawing.Point(668, 180);
			this.GridExempts.Name = "GridExempts";
			this.GridExempts.ReadOnly = false;
			this.GridExempts.RowHeadersVisible = false;
			this.GridExempts.Rows = 4;
			this.GridExempts.ShowFocusCell = false;
			this.GridExempts.Size = new System.Drawing.Size(354, 152);
			this.GridExempts.TabIndex = 4;
			this.GridExempts.Tag = "land";
			this.GridExempts.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridExempts_AfterEdit);
			this.GridExempts.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridExempts_ChangeEdit);
			this.GridExempts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridExempts_MouseMoveEvent);
			// 
			// SaleGrid
			// 
			this.SaleGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.SaleGrid.Cols = 6;
			this.SaleGrid.ColumnHeadersHeight = 60;
			this.SaleGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.SaleGrid.ExtendLastCol = true;
			this.SaleGrid.FixedCols = 0;
			this.SaleGrid.FixedRows = 2;
			this.SaleGrid.Location = new System.Drawing.Point(20, 600);
			this.SaleGrid.Name = "SaleGrid";
			this.SaleGrid.ReadOnly = false;
			this.SaleGrid.RowHeadersVisible = false;
			this.SaleGrid.Rows = 3;
			this.SaleGrid.ShowFocusCell = false;
			this.SaleGrid.Size = new System.Drawing.Size(1002, 102);
			this.SaleGrid.StandardTab = false;
			this.SaleGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.SaleGrid.TabIndex = 7;
			this.SaleGrid.Tag = "sale";
			this.SaleGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.SaleGrid_AfterEdit);
			this.SaleGrid.CurrentCellChanged += new System.EventHandler(this.SaleGrid_RowColChange);
			// 
			// BPGrid
			// 
			this.BPGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.BPGrid.Cols = 4;
			this.BPGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.BPGrid.ExtendLastCol = true;
			this.BPGrid.FixedCols = 0;
			this.BPGrid.Location = new System.Drawing.Point(668, 387);
			this.BPGrid.Name = "BPGrid";
			this.BPGrid.ReadOnly = false;
			this.BPGrid.RowHeadersVisible = false;
			this.BPGrid.ShowFocusCell = false;
			this.BPGrid.Size = new System.Drawing.Size(354, 153);
			this.BPGrid.TabIndex = 6;
			this.BPGrid.Tag = "land";
			this.BPGrid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.BPGrid_KeyDownEdit);
			this.BPGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.BPGrid_KeyPressEdit);
			this.BPGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.BPGrid_AfterEdit);
			this.BPGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.BPGrid_BeforeEdit);
			this.BPGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.BPGrid_ValidateEdit);
			this.BPGrid.CurrentCellChanged += new System.EventHandler(this.BPGrid_RowColChange);
			this.BPGrid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.BPGrid_MouseDownEvent);
			this.BPGrid.Leave += new System.EventHandler(this.BPGrid_Leave);
			this.BPGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.BPGrid_KeyDownEvent);
			// 
			// gridTranCode
			// 
			this.gridTranCode.Cols = 1;
			this.gridTranCode.ColumnHeadersVisible = false;
			this.gridTranCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTranCode.ExtendLastCol = true;
			this.gridTranCode.FixedCols = 0;
			this.gridTranCode.FixedRows = 0;
			this.gridTranCode.Location = new System.Drawing.Point(130, 498);
			this.gridTranCode.Name = "gridTranCode";
			this.gridTranCode.ReadOnly = false;
			this.gridTranCode.RowHeadersVisible = false;
			this.gridTranCode.Rows = 1;
			this.gridTranCode.ShowFocusCell = false;
			this.gridTranCode.Size = new System.Drawing.Size(516, 43);
			this.gridTranCode.TabIndex = 302;
			this.gridTranCode.Tag = "land";
			this.gridTranCode.ComboCloseUp += new System.EventHandler(this.gridTranCode_ComboCloseUp);
			this.gridTranCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridTranCode_BeforeEdit);
			// 
			// Label8_37
			// 
			this.Label8_37.Location = new System.Drawing.Point(20, 94);
			this.Label8_37.Name = "Label8_37";
			this.Label8_37.Size = new System.Drawing.Size(85, 16);
			this.Label8_37.TabIndex = 314;
			this.Label8_37.Text = "2ND OWNER";
			this.Label8_37.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_8
			// 
			this.Label8_8.Location = new System.Drawing.Point(20, 462);
			this.Label8_8.Name = "Label8_8";
			this.Label8_8.Size = new System.Drawing.Size(107, 16);
			this.Label8_8.TabIndex = 313;
			this.Label8_8.Text = "REFERENCE 2";
			this.Label8_8.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_7
			// 
			this.Label8_7.Location = new System.Drawing.Point(20, 412);
			this.Label8_7.Name = "Label8_7";
			this.Label8_7.Size = new System.Drawing.Size(85, 16);
			this.Label8_7.TabIndex = 312;
			this.Label8_7.Text = "REFERENCE 1";
			this.Label8_7.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_5
			// 
			this.Label8_5.Location = new System.Drawing.Point(461, 294);
			this.Label8_5.Name = "Label8_5";
			this.Label8_5.Size = new System.Drawing.Size(38, 16);
			this.Label8_5.TabIndex = 311;
			this.Label8_5.Text = "ZIP";
			this.Label8_5.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_4
			// 
			this.Label8_4.Location = new System.Drawing.Point(20, 294);
			this.Label8_4.Name = "Label8_4";
			this.Label8_4.Size = new System.Drawing.Size(54, 16);
			this.Label8_4.TabIndex = 310;
			this.Label8_4.Text = "CITY";
			this.Label8_4.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_3
			// 
			this.Label8_3.Location = new System.Drawing.Point(20, 244);
			this.Label8_3.Name = "Label8_3";
			this.Label8_3.Size = new System.Drawing.Size(70, 16);
			this.Label8_3.TabIndex = 309;
			this.Label8_3.Text = "ADDRESS 2";
			this.Label8_3.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_2
			// 
			this.Label8_2.Location = new System.Drawing.Point(20, 194);
			this.Label8_2.Name = "Label8_2";
			this.Label8_2.Size = new System.Drawing.Size(70, 16);
			this.Label8_2.TabIndex = 308;
			this.Label8_2.Text = "ADDRESS 1";
			this.Label8_2.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_1
			// 
			this.Label8_1.Location = new System.Drawing.Point(346, 294);
			this.Label8_1.Name = "Label8_1";
			this.Label8_1.Size = new System.Drawing.Size(53, 17);
			this.Label8_1.TabIndex = 307;
			this.Label8_1.Text = "STATE";
			this.Label8_1.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_0
			// 
			this.Label8_0.Location = new System.Drawing.Point(20, 44);
			this.Label8_0.Name = "Label8_0";
			this.Label8_0.Size = new System.Drawing.Size(62, 16);
			this.Label8_0.TabIndex = 306;
			this.Label8_0.Text = "NAME";
			this.Label8_0.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 144);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(107, 16);
			this.Label9.TabIndex = 305;
			this.Label9.Text = "PREV OWNER";
			// 
			// Label8_28
			// 
			this.Label8_28.Location = new System.Drawing.Point(20, 344);
			this.Label8_28.Name = "Label8_28";
			this.Label8_28.Size = new System.Drawing.Size(70, 16);
			this.Label8_28.TabIndex = 304;
			this.Label8_28.Text = "E-MAIL";
			this.Label8_28.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// lblDate_7
			// 
			this.lblDate_7.Location = new System.Drawing.Point(20, 514);
			this.lblDate_7.Name = "lblDate_7";
			this.lblDate_7.Size = new System.Drawing.Size(85, 16);
			this.lblDate_7.TabIndex = 303;
			this.lblDate_7.Text = "TRAN CODE";
			// 
			// imgDocuments
			// 
			this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDocuments.FillColor = 16777215;
			this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
			this.imgDocuments.Location = new System.Drawing.Point(954, 8);
			this.imgDocuments.Name = "imgDocuments";
			this.imgDocuments.Size = new System.Drawing.Size(29, 30);
			this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
			this.imgDocuments.Visible = false;
			this.imgDocuments.Click += new System.EventHandler(this.imgDocuments_Click);
			// 
			// lblDate_0
			// 
			this.lblDate_0.Location = new System.Drawing.Point(749, 135);
			this.lblDate_0.Name = "lblDate_0";
			this.lblDate_0.Size = new System.Drawing.Size(60, 16);
			this.lblDate_0.TabIndex = 284;
			this.lblDate_0.Text = "TAXABLE";
			// 
			// lbl1_2
			// 
			this.lbl1_2.Location = new System.Drawing.Point(793, 30);
			this.lbl1_2.Name = "lbl1_2";
			this.lbl1_2.Size = new System.Drawing.Size(90, 15);
			this.lbl1_2.TabIndex = 280;
			this.lbl1_2.Text = "ASSESSMENT";
			this.lbl1_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lbl2_3
			// 
			this.lbl2_3.Location = new System.Drawing.Point(749, 60);
			this.lbl2_3.Name = "lbl2_3";
			this.lbl2_3.Size = new System.Drawing.Size(60, 16);
			this.lbl2_3.TabIndex = 279;
			this.lbl2_3.Text = "LAND";
			// 
			// lblDate_4
			// 
			this.lblDate_4.Location = new System.Drawing.Point(749, 85);
			this.lblDate_4.Name = "lblDate_4";
			this.lblDate_4.Size = new System.Drawing.Size(60, 16);
			this.lblDate_4.TabIndex = 278;
			this.lblDate_4.Text = "BUILDING";
			// 
			// lblDate_5
			// 
			this.lblDate_5.Location = new System.Drawing.Point(749, 110);
			this.lblDate_5.Name = "lblDate_5";
			this.lblDate_5.Size = new System.Drawing.Size(60, 16);
			this.lblDate_5.TabIndex = 277;
			this.lblDate_5.Text = "EXEMPT";
			// 
			// Label7
			// 
			this.Label7.AutoSize = true;
			this.Label7.Location = new System.Drawing.Point(259, 787);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(113, 16);
			this.Label7.TabIndex = 272;
			this.Label7.Text = "SALES RECORD";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.Label7.Visible = false;
			// 
			// lblTaxable
			// 
			this.lblTaxable.BackColor = System.Drawing.Color.Transparent;
			this.lblTaxable.Location = new System.Drawing.Point(842, 180);
			this.lblTaxable.Name = "lblTaxable";
			this.lblTaxable.Size = new System.Drawing.Size(86, 16);
			this.lblTaxable.TabIndex = 285;
			this.lblTaxable.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblMRRLLANDVAL_28
			// 
			this.lblMRRLLANDVAL_28.BackColor = System.Drawing.Color.Transparent;
			this.lblMRRLLANDVAL_28.Location = new System.Drawing.Point(842, 60);
			this.lblMRRLLANDVAL_28.Name = "lblMRRLLANDVAL_28";
			this.lblMRRLLANDVAL_28.Size = new System.Drawing.Size(86, 16);
			this.lblMRRLLANDVAL_28.TabIndex = 281;
			this.lblMRRLLANDVAL_28.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblMRRLBLDGVAL_29
			// 
			this.lblMRRLBLDGVAL_29.BackColor = System.Drawing.Color.Transparent;
			this.lblMRRLBLDGVAL_29.Location = new System.Drawing.Point(842, 100);
			this.lblMRRLBLDGVAL_29.Name = "lblMRRLBLDGVAL_29";
			this.lblMRRLBLDGVAL_29.Size = new System.Drawing.Size(86, 16);
			this.lblMRRLBLDGVAL_29.TabIndex = 276;
			this.lblMRRLBLDGVAL_29.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblMRRLEXEMPTION_30
			// 
			this.lblMRRLEXEMPTION_30.BackColor = System.Drawing.Color.Transparent;
			this.lblMRRLEXEMPTION_30.Location = new System.Drawing.Point(842, 140);
			this.lblMRRLEXEMPTION_30.Name = "lblMRRLEXEMPTION_30";
			this.lblMRRLEXEMPTION_30.Size = new System.Drawing.Size(86, 16);
			this.lblMRRLEXEMPTION_30.TabIndex = 275;
			this.lblMRRLEXEMPTION_30.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(273, 389);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(8, 13);
			this.Label1.TabIndex = 174;
			this.Label1.Visible = false;
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.Frame5);
			this.SSTab1_Page2.Controls.Add(this.lblLocation_0);
			this.SSTab1_Page2.Controls.Add(this.lblMapLot);
			this.SSTab1_Page2.Controls.Add(this.lblMapLotCopy_0);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page2.Text = "Property";
			// 
			// Frame5
			// 
			this.Frame5.AppearanceKey = "groupBoxNoBorders";
			this.Frame5.Controls.Add(this.chkZoneOverride);
			this.Frame5.Controls.Add(this.txtMRPIOpen2);
			this.Frame5.Controls.Add(this.txtMRPIYCoord);
			this.Frame5.Controls.Add(this.txtMRPIXCoord);
			this.Frame5.Controls.Add(this.txtMRPIOpen1);
			this.Frame5.Controls.Add(this.txtMRPIStreetCode);
			this.Frame5.Controls.Add(this.txtMRPIAcres);
			this.Frame5.Controls.Add(this.gridLandCode);
			this.Frame5.Controls.Add(this.gridBldgCode);
			this.Frame5.Controls.Add(this.gridPropertyCode);
			this.Frame5.Controls.Add(this.GridNeighborhood);
			this.Frame5.Controls.Add(this.GridZone);
			this.Frame5.Controls.Add(this.GridSecZone);
			this.Frame5.Controls.Add(this.GridTopography);
			this.Frame5.Controls.Add(this.GridUtilities);
			this.Frame5.Controls.Add(this.GridStreet);
			this.Frame5.Controls.Add(this.Label8_20);
			this.Frame5.Controls.Add(this.Label8_19);
			this.Frame5.Controls.Add(this.Label8_18);
			this.Frame5.Controls.Add(this.Label8_17);
			this.Frame5.Controls.Add(this.Label8_16);
			this.Frame5.Controls.Add(this.Label8_15);
			this.Frame5.Controls.Add(this.Label8_14);
			this.Frame5.Controls.Add(this.Label8_13);
			this.Frame5.Controls.Add(this.Label8_12);
			this.Frame5.Controls.Add(this.Label8_11);
			this.Frame5.Controls.Add(this.Label8_10);
			this.Frame5.Controls.Add(this.Label8_36);
			this.Frame5.Controls.Add(this.Label22);
			this.Frame5.Controls.Add(this.Label12);
			this.Frame5.Controls.Add(this.Label13);
			this.Frame5.Controls.Add(this.GridLand);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(920, 899);
			this.Frame5.TabIndex = 255;
			this.Frame5.MouseMove += new Wisej.Web.MouseEventHandler(this.Frame5_MouseMove);
			// 
			// chkZoneOverride
			// 
			this.chkZoneOverride.Location = new System.Drawing.Point(660, 336);
			this.chkZoneOverride.Name = "chkZoneOverride";
			this.chkZoneOverride.Size = new System.Drawing.Size(172, 23);
			this.chkZoneOverride.TabIndex = 18;
			this.chkZoneOverride.Text = "Price as secondary zone";
			// 
			// txtMRPIOpen2
			// 
			this.txtMRPIOpen2.Appearance = 0;
			this.txtMRPIOpen2.BackColor = System.Drawing.Color.FromName("@table-row-background-odd");
			this.txtMRPIOpen2.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtMRPIOpen2.Location = new System.Drawing.Point(660, 659);
			this.txtMRPIOpen2.MaxLength = 4;
			this.txtMRPIOpen2.Name = "txtMRPIOpen2";
			this.txtMRPIOpen2.Size = new System.Drawing.Size(250, 40);
			this.txtMRPIOpen2.TabIndex = 23;
			this.txtMRPIOpen2.Tag = "land";
			this.txtMRPIOpen2.Enter += new System.EventHandler(this.txtMRPIOpen2_Enter);
			this.txtMRPIOpen2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIOpen2_Validating);
			this.txtMRPIOpen2.MouseDown += new Wisej.Web.MouseEventHandler(this.txtMRPIOpen2_MouseDown);
			this.txtMRPIOpen2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIOpen2_KeyDown);
			// 
			// txtMRPIYCoord
			// 
			this.txtMRPIYCoord.Location = new System.Drawing.Point(660, 182);
			this.txtMRPIYCoord.MaxLength = 4;
			this.txtMRPIYCoord.Name = "txtMRPIYCoord";
			this.txtMRPIYCoord.Size = new System.Drawing.Size(250, 40);
			this.txtMRPIYCoord.TabIndex = 15;
			this.txtMRPIYCoord.Tag = "land";
			this.txtMRPIYCoord.Enter += new System.EventHandler(this.txtMRPIYCoord_Enter);
			this.txtMRPIYCoord.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIYCoord_Validating);
			this.txtMRPIYCoord.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRPIYCoord_MouseMove);
			this.txtMRPIYCoord.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIYCoord_KeyDown);
			// 
			// txtMRPIXCoord
			// 
			this.txtMRPIXCoord.Location = new System.Drawing.Point(660, 132);
			this.txtMRPIXCoord.MaxLength = 4;
			this.txtMRPIXCoord.Name = "txtMRPIXCoord";
			this.txtMRPIXCoord.Size = new System.Drawing.Size(250, 40);
			this.txtMRPIXCoord.TabIndex = 14;
			this.txtMRPIXCoord.Tag = "land";
			this.txtMRPIXCoord.Enter += new System.EventHandler(this.txtMRPIXCoord_Enter);
			this.txtMRPIXCoord.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIXCoord_Validating);
			this.txtMRPIXCoord.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRPIXCoord_MouseMove);
			this.txtMRPIXCoord.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIXCoord_KeyDown);
			// 
			// txtMRPIOpen1
			// 
			this.txtMRPIOpen1.Appearance = 0;
			this.txtMRPIOpen1.BackColor = System.Drawing.Color.FromName("@table-row-background-odd");
			this.txtMRPIOpen1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtMRPIOpen1.Location = new System.Drawing.Point(660, 609);
			this.txtMRPIOpen1.MaxLength = 4;
			this.txtMRPIOpen1.Name = "txtMRPIOpen1";
			this.txtMRPIOpen1.Size = new System.Drawing.Size(250, 40);
			this.txtMRPIOpen1.TabIndex = 22;
			this.txtMRPIOpen1.Tag = "land";
			this.txtMRPIOpen1.Enter += new System.EventHandler(this.txtMRPIOpen1_Enter);
			this.txtMRPIOpen1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIOpen1_Validating);
			this.txtMRPIOpen1.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRPIOpen1_MouseMove);
			this.txtMRPIOpen1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIOpen1_KeyDown);
			// 
			// txtMRPIStreetCode
			// 
			this.txtMRPIStreetCode.Location = new System.Drawing.Point(660, 82);
			this.txtMRPIStreetCode.MaxLength = 4;
			this.txtMRPIStreetCode.Name = "txtMRPIStreetCode";
			this.txtMRPIStreetCode.Size = new System.Drawing.Size(250, 40);
			this.txtMRPIStreetCode.TabIndex = 13;
			this.txtMRPIStreetCode.Tag = "land";
			this.txtMRPIStreetCode.Enter += new System.EventHandler(this.txtMRPIStreetCode_Enter);
			this.txtMRPIStreetCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIStreetCode_Validating);
			this.txtMRPIStreetCode.MouseMove += new Wisej.Web.MouseEventHandler(this.txtMRPIStreetCode_MouseMove);
			this.txtMRPIStreetCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIStreetCode_KeyDown);
			// 
			// txtMRPIAcres
			// 
			this.txtMRPIAcres.BackColor = System.Drawing.SystemColors.Window;
			this.txtMRPIAcres.Location = new System.Drawing.Point(691, 840);
			this.txtMRPIAcres.LockedOriginal = true;
			this.txtMRPIAcres.Name = "txtMRPIAcres";
			this.txtMRPIAcres.ReadOnly = true;
			this.txtMRPIAcres.Size = new System.Drawing.Size(219, 40);
			this.txtMRPIAcres.TabIndex = 256;
			this.txtMRPIAcres.TabStop = false;
			this.txtMRPIAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMRPIAcres.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIAcres_Validating);
			this.txtMRPIAcres.MouseDown += new Wisej.Web.MouseEventHandler(this.txtMRPIAcres_MouseDown);
			// 
			// gridLandCode
			// 
			this.gridLandCode.Cols = 1;
			this.gridLandCode.ColumnHeadersVisible = false;
			this.gridLandCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridLandCode.ExtendLastCol = true;
			this.gridLandCode.FixedCols = 0;
			this.gridLandCode.FixedRows = 0;
			this.gridLandCode.Location = new System.Drawing.Point(115, 30);
			this.gridLandCode.Name = "gridLandCode";
			this.gridLandCode.ReadOnly = false;
			this.gridLandCode.RowHeadersVisible = false;
			this.gridLandCode.Rows = 1;
			this.gridLandCode.ShowFocusCell = false;
			this.gridLandCode.Size = new System.Drawing.Size(318, 42);
			this.gridLandCode.TabIndex = 9;
			this.gridLandCode.Tag = "land";
			this.gridLandCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridLandCode_BeforeEdit);
			this.gridLandCode.Enter += new System.EventHandler(this.gridLandCode_Enter);
			// 
			// gridBldgCode
			// 
			this.gridBldgCode.Cols = 1;
			this.gridBldgCode.ColumnHeadersVisible = false;
			this.gridBldgCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridBldgCode.ExtendLastCol = true;
			this.gridBldgCode.FixedCols = 0;
			this.gridBldgCode.FixedRows = 0;
			this.gridBldgCode.Location = new System.Drawing.Point(115, 82);
			this.gridBldgCode.Name = "gridBldgCode";
			this.gridBldgCode.ReadOnly = false;
			this.gridBldgCode.RowHeadersVisible = false;
			this.gridBldgCode.Rows = 1;
			this.gridBldgCode.ShowFocusCell = false;
			this.gridBldgCode.Size = new System.Drawing.Size(318, 42);
			this.gridBldgCode.TabIndex = 10;
			this.gridBldgCode.Tag = "land";
			this.gridBldgCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridBldgCode_BeforeEdit);
			// 
			// gridPropertyCode
			// 
			this.gridPropertyCode.Cols = 1;
			this.gridPropertyCode.ColumnHeadersVisible = false;
			this.gridPropertyCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridPropertyCode.ExtendLastCol = true;
			this.gridPropertyCode.FixedCols = 0;
			this.gridPropertyCode.FixedRows = 0;
			this.gridPropertyCode.Location = new System.Drawing.Point(115, 134);
			this.gridPropertyCode.Name = "gridPropertyCode";
			this.gridPropertyCode.ReadOnly = false;
			this.gridPropertyCode.RowHeadersVisible = false;
			this.gridPropertyCode.Rows = 1;
			this.gridPropertyCode.ShowFocusCell = false;
			this.gridPropertyCode.Size = new System.Drawing.Size(318, 42);
			this.gridPropertyCode.TabIndex = 11;
			this.gridPropertyCode.Tag = "land";
			this.gridPropertyCode.ComboDropDown += new System.EventHandler(this.gridPropertyCode_ComboDropDown);
			this.gridPropertyCode.ComboCloseUp += new System.EventHandler(this.gridPropertyCode_ComboCloseUp);
			this.gridPropertyCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridPropertyCode_BeforeEdit);
			this.gridPropertyCode.KeyDown += new Wisej.Web.KeyEventHandler(this.gridPropertyCode_KeyDownEvent);
			// 
			// GridNeighborhood
			// 
			this.GridNeighborhood.Cols = 1;
			this.GridNeighborhood.ColumnHeadersVisible = false;
			this.GridNeighborhood.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridNeighborhood.ExtendLastCol = true;
			this.GridNeighborhood.FixedCols = 0;
			this.GridNeighborhood.FixedRows = 0;
			this.GridNeighborhood.Location = new System.Drawing.Point(660, 30);
			this.GridNeighborhood.Name = "GridNeighborhood";
			this.GridNeighborhood.ReadOnly = false;
			this.GridNeighborhood.RowHeadersVisible = false;
			this.GridNeighborhood.Rows = 1;
			this.GridNeighborhood.ShowFocusCell = false;
			this.GridNeighborhood.Size = new System.Drawing.Size(250, 42);
			this.GridNeighborhood.TabIndex = 12;
			this.GridNeighborhood.Tag = "land";
			this.GridNeighborhood.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridNeighborhood_KeyDownEdit);
			this.GridNeighborhood.ComboDropDown += new System.EventHandler(this.GridNeighborhood_ComboDropDown);
			this.GridNeighborhood.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridNeighborhood_MouseMoveEvent);
			this.GridNeighborhood.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridNeighborhood_BeforeEdit);
			this.GridNeighborhood.KeyDown += new Wisej.Web.KeyEventHandler(this.GridNeighborhood_KeyDownEvent);
			// 
			// GridZone
			// 
			this.GridZone.Cols = 1;
			this.GridZone.ColumnHeadersVisible = false;
			this.GridZone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridZone.ExtendLastCol = true;
			this.GridZone.FixedCols = 0;
			this.GridZone.FixedRows = 0;
			this.GridZone.Location = new System.Drawing.Point(660, 232);
			this.GridZone.Name = "GridZone";
			this.GridZone.ReadOnly = false;
			this.GridZone.RowHeadersVisible = false;
			this.GridZone.Rows = 1;
			this.GridZone.ShowFocusCell = false;
			this.GridZone.Size = new System.Drawing.Size(250, 42);
			this.GridZone.TabIndex = 16;
			this.GridZone.Tag = "land";
			this.GridZone.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridZone_KeyDownEdit);
			this.GridZone.ComboDropDown += new System.EventHandler(this.GridZone_ComboDropDown);
			this.GridZone.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridZone_BeforeEdit);
			this.GridZone.KeyDown += new Wisej.Web.KeyEventHandler(this.GridZone_KeyDownEvent);
			// 
			// GridSecZone
			// 
			this.GridSecZone.Cols = 1;
			this.GridSecZone.ColumnHeadersVisible = false;
			this.GridSecZone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridSecZone.ExtendLastCol = true;
			this.GridSecZone.FixedCols = 0;
			this.GridSecZone.FixedRows = 0;
			this.GridSecZone.Location = new System.Drawing.Point(660, 284);
			this.GridSecZone.Name = "GridSecZone";
			this.GridSecZone.ReadOnly = false;
			this.GridSecZone.RowHeadersVisible = false;
			this.GridSecZone.Rows = 1;
			this.GridSecZone.ShowFocusCell = false;
			this.GridSecZone.Size = new System.Drawing.Size(250, 42);
			this.GridSecZone.TabIndex = 17;
			this.GridSecZone.Tag = "land";
			this.GridSecZone.ComboDropDown += new System.EventHandler(this.GridSecZone_ComboDropDown);
			this.GridSecZone.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridSecZone_BeforeEdit);
			this.GridSecZone.KeyDown += new Wisej.Web.KeyEventHandler(this.GridSecZone_KeyDownEvent);
			// 
			// GridTopography
			// 
			this.GridTopography.Cols = 1;
			this.GridTopography.ColumnHeadersVisible = false;
			this.GridTopography.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridTopography.ExtendLastCol = true;
			this.GridTopography.FixedCols = 0;
			this.GridTopography.FixedRows = 0;
			this.GridTopography.Location = new System.Drawing.Point(660, 373);
			this.GridTopography.Name = "GridTopography";
			this.GridTopography.ReadOnly = false;
			this.GridTopography.RowHeadersVisible = false;
			this.GridTopography.ShowFocusCell = false;
			this.GridTopography.Size = new System.Drawing.Size(250, 82);
			this.GridTopography.StandardTab = false;
			this.GridTopography.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridTopography.TabIndex = 19;
			this.GridTopography.Tag = "land";
			this.GridTopography.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridTopography_KeyDownEdit);
			this.GridTopography.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridTopography_BeforeRowColChange);
			this.GridTopography.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridTopography_BeforeEdit);
			this.GridTopography.CurrentCellChanged += new System.EventHandler(this.GridTopography_RowColChange);
			this.GridTopography.KeyDown += new Wisej.Web.KeyEventHandler(this.GridTopography_KeyDownEvent);
			// 
			// GridUtilities
			// 
			this.GridUtilities.Cols = 1;
			this.GridUtilities.ColumnHeadersVisible = false;
			this.GridUtilities.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridUtilities.ExtendLastCol = true;
			this.GridUtilities.FixedCols = 0;
			this.GridUtilities.FixedRows = 0;
			this.GridUtilities.Location = new System.Drawing.Point(660, 465);
			this.GridUtilities.Name = "GridUtilities";
			this.GridUtilities.ReadOnly = false;
			this.GridUtilities.RowHeadersVisible = false;
			this.GridUtilities.ShowFocusCell = false;
			this.GridUtilities.Size = new System.Drawing.Size(250, 82);
			this.GridUtilities.StandardTab = false;
			this.GridUtilities.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridUtilities.TabIndex = 20;
			this.GridUtilities.Tag = "land";
			this.GridUtilities.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridUtilities_KeyDownEdit);
			this.GridUtilities.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridUtilities_BeforeRowColChange);
			this.GridUtilities.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridUtilities_BeforeEdit);
			this.GridUtilities.CurrentCellChanged += new System.EventHandler(this.GridUtilities_RowColChange);
			this.GridUtilities.KeyDown += new Wisej.Web.KeyEventHandler(this.GridUtilities_KeyDownEvent);
			// 
			// GridStreet
			// 
			this.GridStreet.Cols = 1;
			this.GridStreet.ColumnHeadersVisible = false;
			this.GridStreet.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridStreet.ExtendLastCol = true;
			this.GridStreet.FixedCols = 0;
			this.GridStreet.FixedRows = 0;
			this.GridStreet.Location = new System.Drawing.Point(660, 557);
			this.GridStreet.Name = "GridStreet";
			this.GridStreet.ReadOnly = false;
			this.GridStreet.RowHeadersVisible = false;
			this.GridStreet.Rows = 1;
			this.GridStreet.ShowFocusCell = false;
			this.GridStreet.Size = new System.Drawing.Size(250, 42);
			this.GridStreet.TabIndex = 21;
			this.GridStreet.Tag = "land";
			this.GridStreet.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridStreet_KeyDownEdit);
			this.GridStreet.Click += new System.EventHandler(this.GridStreet_ClickEvent);
			this.GridStreet.KeyDown += new Wisej.Web.KeyEventHandler(this.GridStreet_KeyDownEvent);
			// 
			// Label8_20
			// 
			this.Label8_20.AutoSize = true;
			this.Label8_20.Location = new System.Drawing.Point(463, 673);
			this.Label8_20.Name = "Label8_20";
			this.Label8_20.Size = new System.Drawing.Size(91, 16);
			this.Label8_20.TabIndex = 271;
			this.Label8_20.Text = "WARD CODE";
			this.Label8_20.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_19
			// 
			this.Label8_19.AutoSize = true;
			this.Label8_19.Location = new System.Drawing.Point(463, 623);
			this.Label8_19.Name = "Label8_19";
			this.Label8_19.Size = new System.Drawing.Size(132, 16);
			this.Label8_19.TabIndex = 270;
			this.Label8_19.Text = "INSPECTION CODE";
			this.Label8_19.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_18
			// 
			this.Label8_18.Location = new System.Drawing.Point(463, 572);
			this.Label8_18.Name = "Label8_18";
			this.Label8_18.Size = new System.Drawing.Size(110, 16);
			this.Label8_18.TabIndex = 269;
			this.Label8_18.Text = "STREET";
			this.Label8_18.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_17
			// 
			this.Label8_17.Location = new System.Drawing.Point(463, 470);
			this.Label8_17.Name = "Label8_17";
			this.Label8_17.Size = new System.Drawing.Size(110, 16);
			this.Label8_17.TabIndex = 268;
			this.Label8_17.Text = "UTILITIES";
			this.Label8_17.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_16
			// 
			this.Label8_16.Location = new System.Drawing.Point(463, 378);
			this.Label8_16.Name = "Label8_16";
			this.Label8_16.Size = new System.Drawing.Size(110, 16);
			this.Label8_16.TabIndex = 267;
			this.Label8_16.Text = "TOPOGRAPHY";
			this.Label8_16.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_15
			// 
			this.Label8_15.Location = new System.Drawing.Point(463, 299);
			this.Label8_15.Name = "Label8_15";
			this.Label8_15.Size = new System.Drawing.Size(110, 16);
			this.Label8_15.TabIndex = 266;
			this.Label8_15.Text = "SECONDARY ZONE";
			this.Label8_15.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_14
			// 
			this.Label8_14.Location = new System.Drawing.Point(463, 247);
			this.Label8_14.Name = "Label8_14";
			this.Label8_14.Size = new System.Drawing.Size(110, 16);
			this.Label8_14.TabIndex = 265;
			this.Label8_14.Text = "ZONING USE";
			this.Label8_14.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_13
			// 
			this.Label8_13.Location = new System.Drawing.Point(463, 196);
			this.Label8_13.Name = "Label8_13";
			this.Label8_13.Size = new System.Drawing.Size(118, 16);
			this.Label8_13.TabIndex = 264;
			this.Label8_13.Text = "NEW DESCRIPTION 2";
			this.Label8_13.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_12
			// 
			this.Label8_12.Location = new System.Drawing.Point(463, 146);
			this.Label8_12.Name = "Label8_12";
			this.Label8_12.Size = new System.Drawing.Size(118, 16);
			this.Label8_12.TabIndex = 263;
			this.Label8_12.Text = "NEW DESCRIPTION 1";
			this.Label8_12.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_11
			// 
			this.Label8_11.AutoSize = true;
			this.Label8_11.Location = new System.Drawing.Point(463, 96);
			this.Label8_11.Name = "Label8_11";
			this.Label8_11.Size = new System.Drawing.Size(157, 16);
			this.Label8_11.TabIndex = 262;
			this.Label8_11.Text = "TREE GROWTH (YEAR)";
			this.Label8_11.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_10
			// 
			this.Label8_10.Location = new System.Drawing.Point(463, 45);
			this.Label8_10.Name = "Label8_10";
			this.Label8_10.Size = new System.Drawing.Size(118, 16);
			this.Label8_10.TabIndex = 261;
			this.Label8_10.Text = "NEIGHBORHOOD";
			this.Label8_10.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label8_36
			// 
			this.Label8_36.Location = new System.Drawing.Point(603, 854);
			this.Label8_36.Name = "Label8_36";
			this.Label8_36.Size = new System.Drawing.Size(70, 18);
			this.Label8_36.TabIndex = 260;
			this.Label8_36.Text = "ACREAGE";
			this.Label8_36.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// Label22
			// 
			this.Label22.Location = new System.Drawing.Point(20, 149);
			this.Label22.Name = "Label22";
			this.Label22.Size = new System.Drawing.Size(69, 16);
			this.Label22.TabIndex = 259;
			this.Label22.Text = "PROPERTY";
			this.ToolTip1.SetToolTip(this.Label22, "State Property Type Code");
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(20, 45);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(48, 16);
			this.Label12.TabIndex = 258;
			this.Label12.Text = "LAND";
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(20, 97);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(69, 16);
			this.Label13.TabIndex = 257;
			this.Label13.Text = "BUILDING";
			// 
			// GridLand
			// 
			this.GridLand.Cols = 4;
			this.GridLand.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridLand.FixedCols = 0;
			this.GridLand.Location = new System.Drawing.Point(20, 709);
			this.GridLand.Name = "GridLand";
			this.GridLand.ReadOnly = false;
			this.GridLand.RowHeadersVisible = false;
			this.GridLand.Rows = 8;
			this.GridLand.ShowFocusCell = false;
			this.GridLand.Size = new System.Drawing.Size(553, 190);
			this.GridLand.StandardTab = false;
			this.GridLand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridLand.TabIndex = 24;
			this.GridLand.Tag = "land";
			this.GridLand.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridLand_KeyDownEdit);
			this.GridLand.ComboDropDown += new System.EventHandler(this.GridLand_ComboDropDown);
			this.GridLand.ComboCloseUp += new System.EventHandler(this.GridLand_ComboCloseUp);
			this.GridLand.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridLand_AfterEdit);
			this.GridLand.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridLand_BeforeEdit);
			this.GridLand.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridLand_ValidateEdit);
			this.GridLand.CurrentCellChanged += new System.EventHandler(this.GridLand_RowColChange);
			this.GridLand.Validating += new System.ComponentModel.CancelEventHandler(this.GridLand_Validating);
			this.GridLand.KeyDown += new Wisej.Web.KeyEventHandler(this.GridLand_KeyDownEvent);
			// 
			// lblLocation_0
			// 
			this.lblLocation_0.Location = new System.Drawing.Point(222, 45);
			this.lblLocation_0.Name = "lblLocation_0";
			this.lblLocation_0.Size = new System.Drawing.Size(194, 14);
			this.lblLocation_0.TabIndex = 179;
			this.lblLocation_0.Visible = false;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Location = new System.Drawing.Point(434, 45);
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Size = new System.Drawing.Size(104, 14);
			this.lblMapLot.TabIndex = 178;
			this.lblMapLot.Visible = false;
			// 
			// lblMapLotCopy_0
			// 
			this.lblMapLotCopy_0.Location = new System.Drawing.Point(179, 836);
			this.lblMapLotCopy_0.Name = "lblMapLotCopy_0";
			this.lblMapLotCopy_0.Size = new System.Drawing.Size(203, 16);
			this.lblMapLotCopy_0.TabIndex = 194;
			this.lblMapLotCopy_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblMapLotCopy_0.Visible = false;
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.lblMapLotCopy_1);
			this.SSTab1_Page3.Controls.Add(this.lblDwel);
			this.SSTab1_Page3.Controls.Add(this.GridDwelling3);
			this.SSTab1_Page3.Controls.Add(this.GridDwelling2);
			this.SSTab1_Page3.Controls.Add(this.GridDwelling1);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page3.Text = "Dwelling";
			// 
			// lblMapLotCopy_1
			// 
			this.lblMapLotCopy_1.Location = new System.Drawing.Point(20, 451);
			this.lblMapLotCopy_1.Name = "lblMapLotCopy_1";
			this.lblMapLotCopy_1.Size = new System.Drawing.Size(202, 16);
			this.lblMapLotCopy_1.TabIndex = 195;
			this.lblMapLotCopy_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblDwel
			// 
			this.lblDwel.Location = new System.Drawing.Point(20, 30);
			this.lblDwel.Name = "lblDwel";
			this.lblDwel.Size = new System.Drawing.Size(131, 15);
			this.lblDwel.TabIndex = 254;
			this.lblDwel.Text = "VIEW ONLY";
			this.lblDwel.Visible = false;
			// 
			// GridDwelling3
			// 
			this.GridDwelling3.ColumnHeadersVisible = false;
			this.GridDwelling3.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDwelling3.ExtendLastCol = true;
			this.GridDwelling3.FixedRows = 0;
			this.GridDwelling3.Location = new System.Drawing.Point(692, 55);
			this.GridDwelling3.Name = "GridDwelling3";
			this.GridDwelling3.ReadOnly = false;
			this.GridDwelling3.Rows = 13;
			this.GridDwelling3.ShowFocusCell = false;
			this.GridDwelling3.Size = new System.Drawing.Size(316, 386);
			this.GridDwelling3.StandardTab = false;
			this.GridDwelling3.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDwelling3.TabIndex = 27;
			this.GridDwelling3.Tag = "dwelling";
			this.GridDwelling3.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridDwelling3_KeyDownEdit);
			this.GridDwelling3.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.GridDwelling3_KeyUpEdit);
			this.GridDwelling3.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridDwelling3_BeforeEdit);
			this.GridDwelling3.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDwelling3_ValidateEdit);
			this.GridDwelling3.CurrentCellChanged += new System.EventHandler(this.GridDwelling3_RowColChange);
			this.GridDwelling3.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDwelling3_KeyDownEvent);
			// 
			// GridDwelling2
			// 
			this.GridDwelling2.ColumnHeadersVisible = false;
			this.GridDwelling2.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDwelling2.ExtendLastCol = true;
			this.GridDwelling2.FixedRows = 0;
			this.GridDwelling2.Location = new System.Drawing.Point(356, 55);
			this.GridDwelling2.Name = "GridDwelling2";
			this.GridDwelling2.ReadOnly = false;
			this.GridDwelling2.Rows = 16;
			this.GridDwelling2.ShowFocusCell = false;
			this.GridDwelling2.Size = new System.Drawing.Size(316, 386);
			this.GridDwelling2.StandardTab = false;
			this.GridDwelling2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDwelling2.TabIndex = 26;
			this.GridDwelling2.Tag = "dwelling";
			this.GridDwelling2.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridDwelling2_KeyDownEdit);
			this.GridDwelling2.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.GridDwelling2_KeyUpEdit);
			this.GridDwelling2.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridDwelling2_BeforeEdit);
			this.GridDwelling2.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDwelling2_ValidateEdit);
			this.GridDwelling2.CurrentCellChanged += new System.EventHandler(this.GridDwelling2_RowColChange);
			this.GridDwelling2.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDwelling2_KeyDownEvent);
			// 
			// GridDwelling1
			// 
			this.GridDwelling1.ColumnHeadersVisible = false;
			this.GridDwelling1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDwelling1.ExtendLastCol = true;
			this.GridDwelling1.FixedRows = 0;
			this.GridDwelling1.Location = new System.Drawing.Point(20, 55);
			this.GridDwelling1.Name = "GridDwelling1";
			this.GridDwelling1.ReadOnly = false;
			this.GridDwelling1.Rows = 15;
			this.GridDwelling1.ShowFocusCell = false;
			this.GridDwelling1.Size = new System.Drawing.Size(316, 386);
			this.GridDwelling1.StandardTab = false;
			this.GridDwelling1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDwelling1.TabIndex = 25;
			this.GridDwelling1.Tag = "dwelling";
			this.GridDwelling1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridDwelling1_KeyDownEdit);
			this.GridDwelling1.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.GridDwelling1_KeyUpEdit);
			this.GridDwelling1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridDwelling1_BeforeEdit);
			this.GridDwelling1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDwelling1_ValidateEdit);
			this.GridDwelling1.CurrentCellChanged += new System.EventHandler(this.GridDwelling1_RowColChange);
			this.GridDwelling1.Enter += new System.EventHandler(this.GridDwelling1_Enter);
			this.GridDwelling1.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDwelling1_KeyDownEvent);
			// 
			// SSTab1_Page4
			// 
			this.SSTab1_Page4.Controls.Add(this.Label6_0);
			this.SSTab1_Page4.Controls.Add(this.Label5);
			this.SSTab1_Page4.Controls.Add(this.Label4_0);
			this.SSTab1_Page4.Controls.Add(this.Frame4);
			this.SSTab1_Page4.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page4.Name = "SSTab1_Page4";
			this.SSTab1_Page4.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page4.Text = "Commercial";
			// 
			// Label6_0
			// 
			this.Label6_0.Location = new System.Drawing.Point(20, 30);
			this.Label6_0.Name = "Label6_0";
			this.Label6_0.Size = new System.Drawing.Size(202, 14);
			this.Label6_0.TabIndex = 175;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(224, 30);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(194, 14);
			this.Label5.TabIndex = 176;
			// 
			// Label4_0
			// 
			this.Label4_0.Location = new System.Drawing.Point(436, 30);
			this.Label4_0.Name = "Label4_0";
			this.Label4_0.Size = new System.Drawing.Size(104, 14);
			this.Label4_0.TabIndex = 177;
			// 
			// Frame4
			// 
			this.Frame4.AppearanceKey = "groupBoxNoBorders";
			this.Frame4.Controls.Add(this.mebC2Height);
			this.Frame4.Controls.Add(this.mebC2Funct);
			this.Frame4.Controls.Add(this.mebC2Phys);
			this.Frame4.Controls.Add(this.mebC2Condition);
			this.Frame4.Controls.Add(this.mebC2Remodel);
			this.Frame4.Controls.Add(this.mebC2Built);
			this.Frame4.Controls.Add(this.mebC2Heat);
			this.Frame4.Controls.Add(this.mebC2Perimeter);
			this.Frame4.Controls.Add(this.mebC2Floor);
			this.Frame4.Controls.Add(this.mebC2Stories);
			this.Frame4.Controls.Add(this.mebC2ExtWalls);
			this.Frame4.Controls.Add(this.mebC2Quality);
			this.Frame4.Controls.Add(this.mebC2Class);
			this.Frame4.Controls.Add(this.mebDwel2);
			this.Frame4.Controls.Add(this.mebOcc2);
			this.Frame4.Controls.Add(this.mebCMEcon);
			this.Frame4.Controls.Add(this.mebC1Funct);
			this.Frame4.Controls.Add(this.mebC1Phys);
			this.Frame4.Controls.Add(this.mebC1Condition);
			this.Frame4.Controls.Add(this.mebC1Remodel);
			this.Frame4.Controls.Add(this.mebC1Built);
			this.Frame4.Controls.Add(this.mebC1Perimeter);
			this.Frame4.Controls.Add(this.mebC1Floor);
			this.Frame4.Controls.Add(this.mebC1Stories);
			this.Frame4.Controls.Add(this.mebC1ExteriorWalls);
			this.Frame4.Controls.Add(this.mebC1Quality);
			this.Frame4.Controls.Add(this.mebC1Class);
			this.Frame4.Controls.Add(this.mebDwel1);
			this.Frame4.Controls.Add(this.mebC1Height);
			this.Frame4.Controls.Add(this.mebOcc1);
			this.Frame4.Controls.Add(this.mebC1Grade);
			this.Frame4.Controls.Add(this.mebc2grade);
			this.Frame4.Controls.Add(this.mebC1Heat);
			this.Frame4.Controls.Add(this.lblcomm);
			this.Frame4.Controls.Add(this.Label3_32);
			this.Frame4.Controls.Add(this.Label3_1);
			this.Frame4.Controls.Add(this.Label3_2);
			this.Frame4.Controls.Add(this.Label3_3);
			this.Frame4.Controls.Add(this.Label3_4);
			this.Frame4.Controls.Add(this.Label3_5);
			this.Frame4.Controls.Add(this.Label3_6);
			this.Frame4.Controls.Add(this.Label3_7);
			this.Frame4.Controls.Add(this.Label3_8);
			this.Frame4.Controls.Add(this.Label3_9);
			this.Frame4.Controls.Add(this.Label3_10);
			this.Frame4.Controls.Add(this.Label3_11);
			this.Frame4.Controls.Add(this.Label3_12);
			this.Frame4.Controls.Add(this.Label3_13);
			this.Frame4.Controls.Add(this.Label3_14);
			this.Frame4.Controls.Add(this.Label3_15);
			this.Frame4.Controls.Add(this.Label3_16);
			this.Frame4.Controls.Add(this.Label3_17);
			this.Frame4.Controls.Add(this.Label3_18);
			this.Frame4.Controls.Add(this.Label3_19);
			this.Frame4.Controls.Add(this.Label3_20);
			this.Frame4.Controls.Add(this.Label3_21);
			this.Frame4.Controls.Add(this.Label3_22);
			this.Frame4.Controls.Add(this.Label3_23);
			this.Frame4.Controls.Add(this.Label3_24);
			this.Frame4.Controls.Add(this.Label3_25);
			this.Frame4.Controls.Add(this.Label3_29);
			this.Frame4.Controls.Add(this.Label3_30);
			this.Frame4.Controls.Add(this.Label3_31);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(895, 526);
			this.Frame4.TabIndex = 223;
			// 
			// mebC2Height
			// 
			this.mebC2Height.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Height.Location = new System.Drawing.Point(711, 190);
			this.mebC2Height.MaxLength = 2;
			this.mebC2Height.Name = "mebC2Height";
			this.mebC2Height.Size = new System.Drawing.Size(90, 20);
			this.mebC2Height.TabIndex = 52;
			this.mebC2Height.Tag = "commercial";
			this.mebC2Height.Enter += new System.EventHandler(this.mebC2Height_Enter);
			this.mebC2Height.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Height_Validating);
			this.mebC2Height.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Height_KeyUp);
			// 
			// mebC2Funct
			// 
			this.mebC2Funct.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Funct.Location = new System.Drawing.Point(596, 414);
			this.mebC2Funct.MaxLength = 3;
			this.mebC2Funct.Name = "mebC2Funct";
			this.mebC2Funct.Size = new System.Drawing.Size(90, 20);
			this.mebC2Funct.TabIndex = 60;
			this.mebC2Funct.Tag = "commercial";
			this.mebC2Funct.Enter += new System.EventHandler(this.mebC2Funct_Enter);
			this.mebC2Funct.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Funct_Validating);
			this.mebC2Funct.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Funct_KeyUp);
			// 
			// mebC2Phys
			// 
			this.mebC2Phys.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Phys.Location = new System.Drawing.Point(596, 386);
			this.mebC2Phys.MaxLength = 2;
			this.mebC2Phys.Name = "mebC2Phys";
			this.mebC2Phys.Size = new System.Drawing.Size(90, 20);
			this.mebC2Phys.TabIndex = 59;
			this.mebC2Phys.Tag = "commercial";
			this.mebC2Phys.Enter += new System.EventHandler(this.mebC2Phys_Enter);
			this.mebC2Phys.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Phys_Validating);
			this.mebC2Phys.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Phys_KeyUp);
			// 
			// mebC2Condition
			// 
			this.mebC2Condition.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Condition.Location = new System.Drawing.Point(596, 358);
			this.mebC2Condition.MaxLength = 1;
			this.mebC2Condition.Name = "mebC2Condition";
			this.mebC2Condition.Size = new System.Drawing.Size(90, 20);
			this.mebC2Condition.TabIndex = 58;
			this.mebC2Condition.Tag = "commercial";
			this.mebC2Condition.Enter += new System.EventHandler(this.mebC2Condition_Enter);
			this.mebC2Condition.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Condition_Validating);
			this.mebC2Condition.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Condition_KeyUp);
			// 
			// mebC2Remodel
			// 
			this.mebC2Remodel.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Remodel.Location = new System.Drawing.Point(596, 330);
			this.mebC2Remodel.MaxLength = 4;
			this.mebC2Remodel.Name = "mebC2Remodel";
			this.mebC2Remodel.Size = new System.Drawing.Size(90, 20);
			this.mebC2Remodel.TabIndex = 57;
			this.mebC2Remodel.Tag = "commercial";
			this.mebC2Remodel.Enter += new System.EventHandler(this.mebC2Remodel_Enter);
			this.mebC2Remodel.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Remodel_Validating);
			this.mebC2Remodel.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Remodel_KeyUp);
			// 
			// mebC2Built
			// 
			this.mebC2Built.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Built.Location = new System.Drawing.Point(596, 302);
			this.mebC2Built.MaxLength = 4;
			this.mebC2Built.Name = "mebC2Built";
			this.mebC2Built.Size = new System.Drawing.Size(90, 20);
			this.mebC2Built.TabIndex = 56;
			this.mebC2Built.Tag = "commercial";
			this.mebC2Built.Enter += new System.EventHandler(this.mebC2Built_Enter);
			this.mebC2Built.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Built_Validating);
			this.mebC2Built.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Built_KeyUp);
			// 
			// mebC2Heat
			// 
			this.mebC2Heat.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Heat.Location = new System.Drawing.Point(596, 274);
			this.mebC2Heat.MaxLength = 2;
			this.mebC2Heat.Name = "mebC2Heat";
			this.mebC2Heat.Size = new System.Drawing.Size(90, 20);
			this.mebC2Heat.TabIndex = 55;
			this.mebC2Heat.Tag = "commercial";
			this.mebC2Heat.Enter += new System.EventHandler(this.mebC2Heat_Enter);
			this.mebC2Heat.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Heat_Validating);
			this.mebC2Heat.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Heat_KeyUp);
			// 
			// mebC2Perimeter
			// 
			this.mebC2Perimeter.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Perimeter.Location = new System.Drawing.Point(596, 246);
			this.mebC2Perimeter.MaxLength = 4;
			this.mebC2Perimeter.Name = "mebC2Perimeter";
			this.mebC2Perimeter.Size = new System.Drawing.Size(90, 20);
			this.mebC2Perimeter.TabIndex = 54;
			this.mebC2Perimeter.Tag = "commercial";
			this.mebC2Perimeter.Enter += new System.EventHandler(this.mebC2Perimeter_Enter);
			this.mebC2Perimeter.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Perimeter_Validating);
			this.mebC2Perimeter.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Perimeter_KeyUp);
			// 
			// mebC2Floor
			// 
			this.mebC2Floor.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Floor.Location = new System.Drawing.Point(596, 218);
			this.mebC2Floor.MaxLength = 6;
			this.mebC2Floor.Name = "mebC2Floor";
			this.mebC2Floor.Size = new System.Drawing.Size(90, 20);
			this.mebC2Floor.TabIndex = 53;
			this.mebC2Floor.Tag = "commercial";
			this.mebC2Floor.Enter += new System.EventHandler(this.mebC2Floor_Enter);
			this.mebC2Floor.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Floor_Validating);
			this.mebC2Floor.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Floor_KeyUp);
			// 
			// mebC2Stories
			// 
			this.mebC2Stories.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Stories.Location = new System.Drawing.Point(596, 190);
			this.mebC2Stories.MaxLength = 2;
			this.mebC2Stories.Name = "mebC2Stories";
			this.mebC2Stories.Size = new System.Drawing.Size(90, 20);
			this.mebC2Stories.TabIndex = 51;
			this.mebC2Stories.Tag = "commercial";
			this.mebC2Stories.Enter += new System.EventHandler(this.mebC2Stories_Enter);
			this.mebC2Stories.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Stories_Validating);
			this.mebC2Stories.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Stories_KeyUp);
			// 
			// mebC2ExtWalls
			// 
			this.mebC2ExtWalls.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2ExtWalls.Location = new System.Drawing.Point(596, 162);
			this.mebC2ExtWalls.MaxLength = 1;
			this.mebC2ExtWalls.Name = "mebC2ExtWalls";
			this.mebC2ExtWalls.Size = new System.Drawing.Size(90, 20);
			this.mebC2ExtWalls.TabIndex = 50;
			this.mebC2ExtWalls.Tag = "commercial";
			this.mebC2ExtWalls.Enter += new System.EventHandler(this.mebC2ExtWalls_Enter);
			this.mebC2ExtWalls.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2ExtWalls_Validating);
			this.mebC2ExtWalls.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2ExtWalls_KeyUp);
			// 
			// mebC2Quality
			// 
			this.mebC2Quality.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Quality.Location = new System.Drawing.Point(711, 106);
			this.mebC2Quality.MaxLength = 1;
			this.mebC2Quality.Name = "mebC2Quality";
			this.mebC2Quality.Size = new System.Drawing.Size(90, 20);
			this.mebC2Quality.TabIndex = 48;
			this.mebC2Quality.Tag = "commercial";
			this.mebC2Quality.Enter += new System.EventHandler(this.mebC2Quality_Enter);
			this.mebC2Quality.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Quality_Validating);
			this.mebC2Quality.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Quality_KeyUp);
			// 
			// mebC2Class
			// 
			this.mebC2Class.BackColor = System.Drawing.SystemColors.Window;
			this.mebC2Class.Location = new System.Drawing.Point(596, 106);
			this.mebC2Class.MaxLength = 1;
			this.mebC2Class.Name = "mebC2Class";
			this.mebC2Class.Size = new System.Drawing.Size(90, 20);
			this.mebC2Class.TabIndex = 47;
			this.mebC2Class.Tag = "commercial";
			this.mebC2Class.Enter += new System.EventHandler(this.mebC2Class_Enter);
			this.mebC2Class.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Class_Validating);
			this.mebC2Class.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Class_KeyUp);
			// 
			// mebDwel2
			// 
			this.mebDwel2.BackColor = System.Drawing.SystemColors.Window;
			this.mebDwel2.Location = new System.Drawing.Point(596, 78);
			this.mebDwel2.MaxLength = 2;
			this.mebDwel2.Name = "mebDwel2";
			this.mebDwel2.Size = new System.Drawing.Size(90, 20);
			this.mebDwel2.TabIndex = 46;
			this.mebDwel2.Tag = "commercial";
			this.mebDwel2.Enter += new System.EventHandler(this.mebDwel2_Enter);
			this.mebDwel2.Validating += new System.ComponentModel.CancelEventHandler(this.mebDwel2_Validating);
			this.mebDwel2.KeyUp += new Wisej.Web.KeyEventHandler(this.mebDwel2_KeyUp);
			// 
			// mebOcc2
			// 
			this.mebOcc2.BackColor = System.Drawing.SystemColors.Window;
			this.mebOcc2.Location = new System.Drawing.Point(596, 50);
			this.mebOcc2.MaxLength = 3;
			this.mebOcc2.Name = "mebOcc2";
			this.mebOcc2.Size = new System.Drawing.Size(90, 20);
			this.mebOcc2.TabIndex = 45;
			this.mebOcc2.Tag = "commercial";
			this.mebOcc2.Enter += new System.EventHandler(this.mebOcc2_Enter);
			this.mebOcc2.Validating += new System.ComponentModel.CancelEventHandler(this.mebOcc2_Validating);
			this.mebOcc2.KeyUp += new Wisej.Web.KeyEventHandler(this.mebOcc2_KeyUp);
			// 
			// mebCMEcon
			// 
			this.mebCMEcon.BackColor = System.Drawing.SystemColors.Window;
			this.mebCMEcon.Location = new System.Drawing.Point(183, 442);
			this.mebCMEcon.MaxLength = 3;
			this.mebCMEcon.Name = "mebCMEcon";
			this.mebCMEcon.Size = new System.Drawing.Size(90, 20);
			this.mebCMEcon.TabIndex = 44;
			this.mebCMEcon.Tag = "commercial";
			this.mebCMEcon.Enter += new System.EventHandler(this.mebCMEcon_Enter);
			this.mebCMEcon.Validating += new System.ComponentModel.CancelEventHandler(this.mebCMEcon_Validating);
			this.mebCMEcon.KeyUp += new Wisej.Web.KeyEventHandler(this.mebCMEcon_KeyUp);
			// 
			// mebC1Funct
			// 
			this.mebC1Funct.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Funct.Location = new System.Drawing.Point(183, 414);
			this.mebC1Funct.MaxLength = 3;
			this.mebC1Funct.Name = "mebC1Funct";
			this.mebC1Funct.Size = new System.Drawing.Size(90, 20);
			this.mebC1Funct.TabIndex = 43;
			this.mebC1Funct.Tag = "commercial";
			this.mebC1Funct.Enter += new System.EventHandler(this.mebC1Funct_Enter);
			this.mebC1Funct.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Funct_Validating);
			this.mebC1Funct.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Funct_KeyUp);
			// 
			// mebC1Phys
			// 
			this.mebC1Phys.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Phys.Location = new System.Drawing.Point(183, 386);
			this.mebC1Phys.MaxLength = 2;
			this.mebC1Phys.Name = "mebC1Phys";
			this.mebC1Phys.Size = new System.Drawing.Size(90, 20);
			this.mebC1Phys.TabIndex = 42;
			this.mebC1Phys.Tag = "commercial";
			this.mebC1Phys.Enter += new System.EventHandler(this.mebC1Phys_Enter);
			this.mebC1Phys.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Phys_Validating);
			this.mebC1Phys.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Phys_KeyUp);
			// 
			// mebC1Condition
			// 
			this.mebC1Condition.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Condition.Location = new System.Drawing.Point(183, 358);
			this.mebC1Condition.MaxLength = 1;
			this.mebC1Condition.Name = "mebC1Condition";
			this.mebC1Condition.Size = new System.Drawing.Size(90, 20);
			this.mebC1Condition.TabIndex = 41;
			this.mebC1Condition.Tag = "commercial";
			this.mebC1Condition.Enter += new System.EventHandler(this.mebC1Condition_Enter);
			this.mebC1Condition.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Condition_Validating);
			this.mebC1Condition.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Condition_KeyUp);
			// 
			// mebC1Remodel
			// 
			this.mebC1Remodel.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Remodel.Location = new System.Drawing.Point(183, 330);
			this.mebC1Remodel.MaxLength = 4;
			this.mebC1Remodel.Name = "mebC1Remodel";
			this.mebC1Remodel.Size = new System.Drawing.Size(90, 20);
			this.mebC1Remodel.TabIndex = 40;
			this.mebC1Remodel.Tag = "commercial";
			this.mebC1Remodel.Enter += new System.EventHandler(this.mebC1Remodel_Enter);
			this.mebC1Remodel.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Remodel_Validating);
			this.mebC1Remodel.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Remodel_KeyUp);
			// 
			// mebC1Built
			// 
			this.mebC1Built.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Built.Location = new System.Drawing.Point(183, 302);
			this.mebC1Built.MaxLength = 4;
			this.mebC1Built.Name = "mebC1Built";
			this.mebC1Built.Size = new System.Drawing.Size(90, 20);
			this.mebC1Built.TabIndex = 39;
			this.mebC1Built.Tag = "commercial";
			this.mebC1Built.Enter += new System.EventHandler(this.mebC1Built_Enter);
			this.mebC1Built.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Built_Validating);
			this.mebC1Built.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Built_KeyUp);
			// 
			// mebC1Perimeter
			// 
			this.mebC1Perimeter.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Perimeter.Location = new System.Drawing.Point(183, 246);
			this.mebC1Perimeter.MaxLength = 5;
			this.mebC1Perimeter.Name = "mebC1Perimeter";
			this.mebC1Perimeter.Size = new System.Drawing.Size(90, 20);
			this.mebC1Perimeter.TabIndex = 37;
			this.mebC1Perimeter.Tag = "commercial";
			this.mebC1Perimeter.Enter += new System.EventHandler(this.mebC1Perimeter_Enter);
			this.mebC1Perimeter.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Perimeter_Validating);
			this.mebC1Perimeter.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Perimeter_KeyUp);
			// 
			// mebC1Floor
			// 
			this.mebC1Floor.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Floor.Location = new System.Drawing.Point(183, 218);
			this.mebC1Floor.MaxLength = 6;
			this.mebC1Floor.Name = "mebC1Floor";
			this.mebC1Floor.Size = new System.Drawing.Size(90, 20);
			this.mebC1Floor.TabIndex = 36;
			this.mebC1Floor.Tag = "commercial";
			this.mebC1Floor.Enter += new System.EventHandler(this.mebC1Floor_Enter);
			this.mebC1Floor.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Floor_Validating);
			this.mebC1Floor.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Floor_KeyUp);
			// 
			// mebC1Stories
			// 
			this.mebC1Stories.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Stories.Location = new System.Drawing.Point(183, 190);
			this.mebC1Stories.MaxLength = 2;
			this.mebC1Stories.Name = "mebC1Stories";
			this.mebC1Stories.Size = new System.Drawing.Size(90, 20);
			this.mebC1Stories.TabIndex = 34;
			this.mebC1Stories.Tag = "commercial";
			this.mebC1Stories.Enter += new System.EventHandler(this.mebC1Stories_Enter);
			this.mebC1Stories.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Stories_Validating);
			this.mebC1Stories.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Stories_KeyUp);
			// 
			// mebC1ExteriorWalls
			// 
			this.mebC1ExteriorWalls.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1ExteriorWalls.Location = new System.Drawing.Point(183, 162);
			this.mebC1ExteriorWalls.MaxLength = 1;
			this.mebC1ExteriorWalls.Name = "mebC1ExteriorWalls";
			this.mebC1ExteriorWalls.Size = new System.Drawing.Size(90, 20);
			this.mebC1ExteriorWalls.TabIndex = 33;
			this.mebC1ExteriorWalls.Tag = "commercial";
			this.mebC1ExteriorWalls.Enter += new System.EventHandler(this.mebC1ExteriorWalls_Enter);
			this.mebC1ExteriorWalls.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1ExteriorWalls_Validating);
			this.mebC1ExteriorWalls.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1ExteriorWalls_KeyUp);
			// 
			// mebC1Quality
			// 
			this.mebC1Quality.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Quality.Location = new System.Drawing.Point(298, 106);
			this.mebC1Quality.MaxLength = 1;
			this.mebC1Quality.Name = "mebC1Quality";
			this.mebC1Quality.Size = new System.Drawing.Size(90, 20);
			this.mebC1Quality.TabIndex = 31;
			this.mebC1Quality.Tag = "commercial";
			this.mebC1Quality.Enter += new System.EventHandler(this.mebC1Quality_Enter);
			this.mebC1Quality.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Quality_Validating);
			this.mebC1Quality.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Quality_KeyUp);
			// 
			// mebC1Class
			// 
			this.mebC1Class.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Class.Location = new System.Drawing.Point(183, 106);
			this.mebC1Class.MaxLength = 1;
			this.mebC1Class.Name = "mebC1Class";
			this.mebC1Class.Size = new System.Drawing.Size(90, 20);
			this.mebC1Class.TabIndex = 30;
			this.mebC1Class.Tag = "commercial";
			this.mebC1Class.Enter += new System.EventHandler(this.mebC1Class_Enter);
			this.mebC1Class.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Class_Validating);
			this.mebC1Class.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Class_KeyUp);
			// 
			// mebDwel1
			// 
			this.mebDwel1.BackColor = System.Drawing.SystemColors.Window;
			this.mebDwel1.Location = new System.Drawing.Point(183, 78);
			this.mebDwel1.MaxLength = 2;
			this.mebDwel1.Name = "mebDwel1";
			this.mebDwel1.Size = new System.Drawing.Size(90, 20);
			this.mebDwel1.TabIndex = 29;
			this.mebDwel1.Tag = "commercial";
			this.mebDwel1.Enter += new System.EventHandler(this.mebDwel1_Enter);
			this.mebDwel1.Validating += new System.ComponentModel.CancelEventHandler(this.mebDwel1_Validating);
			this.mebDwel1.KeyUp += new Wisej.Web.KeyEventHandler(this.mebDwel1_KeyUp);
			// 
			// mebC1Height
			// 
			this.mebC1Height.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Height.Location = new System.Drawing.Point(298, 190);
			this.mebC1Height.MaxLength = 2;
			this.mebC1Height.Name = "mebC1Height";
			this.mebC1Height.Size = new System.Drawing.Size(90, 20);
			this.mebC1Height.TabIndex = 35;
			this.mebC1Height.Tag = "commercial";
			this.mebC1Height.Enter += new System.EventHandler(this.mebC1Height_Enter);
			this.mebC1Height.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Height_Validating);
			this.mebC1Height.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Height_KeyUp);
			// 
			// mebOcc1
			// 
			this.mebOcc1.BackColor = System.Drawing.SystemColors.Window;
			this.mebOcc1.Location = new System.Drawing.Point(183, 50);
			this.mebOcc1.MaxLength = 3;
			this.mebOcc1.Name = "mebOcc1";
			this.mebOcc1.Size = new System.Drawing.Size(90, 20);
			this.mebOcc1.TabIndex = 28;
			this.mebOcc1.Tag = "commercial";
			this.mebOcc1.Enter += new System.EventHandler(this.mebOcc1_Enter);
			this.mebOcc1.Validating += new System.ComponentModel.CancelEventHandler(this.mebOcc1_Validating);
			this.mebOcc1.KeyUp += new Wisej.Web.KeyEventHandler(this.mebOcc1_KeyUp);
			// 
			// mebC1Grade
			// 
			this.mebC1Grade.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Grade.Location = new System.Drawing.Point(183, 134);
			this.mebC1Grade.MaxLength = 4;
			this.mebC1Grade.Name = "mebC1Grade";
			this.mebC1Grade.Size = new System.Drawing.Size(90, 20);
			this.mebC1Grade.TabIndex = 32;
			this.mebC1Grade.Tag = "commercial";
			this.mebC1Grade.Enter += new System.EventHandler(this.mebC1Grade_Enter);
			this.mebC1Grade.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Grade_Validating);
			this.mebC1Grade.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Grade_KeyUp);
			// 
			// mebc2grade
			// 
			this.mebc2grade.BackColor = System.Drawing.SystemColors.Window;
			this.mebc2grade.Location = new System.Drawing.Point(596, 134);
			this.mebc2grade.MaxLength = 4;
			this.mebc2grade.Name = "mebc2grade";
			this.mebc2grade.Size = new System.Drawing.Size(90, 20);
			this.mebc2grade.TabIndex = 49;
			this.mebc2grade.Tag = "commercial";
			this.mebc2grade.Enter += new System.EventHandler(this.mebc2grade_Enter);
			this.mebc2grade.Validating += new System.ComponentModel.CancelEventHandler(this.mebc2grade_Validating);
			this.mebc2grade.KeyUp += new Wisej.Web.KeyEventHandler(this.mebc2grade_KeyUp);
			// 
			// mebC1Heat
			// 
			this.mebC1Heat.BackColor = System.Drawing.SystemColors.Window;
			this.mebC1Heat.Location = new System.Drawing.Point(183, 274);
			this.mebC1Heat.MaxLength = 2;
			this.mebC1Heat.Name = "mebC1Heat";
			this.mebC1Heat.Size = new System.Drawing.Size(90, 20);
			this.mebC1Heat.TabIndex = 38;
			this.mebC1Heat.Tag = "commercial";
			this.mebC1Heat.Enter += new System.EventHandler(this.mebC1Heat_Enter);
			this.mebC1Heat.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Heat_Validating);
			this.mebC1Heat.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Heat_KeyUp);
			// 
			// lblcomm
			// 
			this.lblcomm.Location = new System.Drawing.Point(360, 14);
			this.lblcomm.Name = "lblcomm";
			this.lblcomm.Size = new System.Drawing.Size(140, 28);
			this.lblcomm.TabIndex = 253;
			this.lblcomm.Text = "VIEW ONLY";
			this.lblcomm.Visible = false;
			// 
			// Label3_32
			// 
			this.Label3_32.Location = new System.Drawing.Point(20, 50);
			this.Label3_32.Name = "Label3_32";
			this.Label3_32.Size = new System.Drawing.Size(130, 14);
			this.Label3_32.TabIndex = 252;
			this.Label3_32.Text = "OCCUPANCY CODE";
			// 
			// Label3_1
			// 
			this.Label3_1.Location = new System.Drawing.Point(20, 78);
			this.Label3_1.Name = "Label3_1";
			this.Label3_1.Size = new System.Drawing.Size(130, 14);
			this.Label3_1.TabIndex = 251;
			this.Label3_1.Text = "# OF DWELLING UNITS";
			// 
			// Label3_2
			// 
			this.Label3_2.Location = new System.Drawing.Point(20, 106);
			this.Label3_2.Name = "Label3_2";
			this.Label3_2.Size = new System.Drawing.Size(130, 14);
			this.Label3_2.TabIndex = 250;
			this.Label3_2.Text = "CLASS & QUALITY";
			// 
			// Label3_3
			// 
			this.Label3_3.Location = new System.Drawing.Point(20, 134);
			this.Label3_3.Name = "Label3_3";
			this.Label3_3.Size = new System.Drawing.Size(130, 14);
			this.Label3_3.TabIndex = 249;
			this.Label3_3.Text = "GRADE FACTOR";
			// 
			// Label3_4
			// 
			this.Label3_4.Location = new System.Drawing.Point(20, 162);
			this.Label3_4.Name = "Label3_4";
			this.Label3_4.Size = new System.Drawing.Size(130, 14);
			this.Label3_4.TabIndex = 248;
			this.Label3_4.Text = "EXTERIOR WALLS";
			// 
			// Label3_5
			// 
			this.Label3_5.Location = new System.Drawing.Point(20, 190);
			this.Label3_5.Name = "Label3_5";
			this.Label3_5.Size = new System.Drawing.Size(130, 14);
			this.Label3_5.TabIndex = 247;
			this.Label3_5.Text = "STORIES / HEIGHT";
			// 
			// Label3_6
			// 
			this.Label3_6.Location = new System.Drawing.Point(20, 218);
			this.Label3_6.Name = "Label3_6";
			this.Label3_6.Size = new System.Drawing.Size(130, 14);
			this.Label3_6.TabIndex = 246;
			this.Label3_6.Text = "BASE FLOOR AREA";
			// 
			// Label3_7
			// 
			this.Label3_7.Location = new System.Drawing.Point(20, 246);
			this.Label3_7.Name = "Label3_7";
			this.Label3_7.Size = new System.Drawing.Size(130, 14);
			this.Label3_7.TabIndex = 245;
			this.Label3_7.Text = "PERIMETER / UNITS";
			// 
			// Label3_8
			// 
			this.Label3_8.Location = new System.Drawing.Point(20, 274);
			this.Label3_8.Name = "Label3_8";
			this.Label3_8.Size = new System.Drawing.Size(130, 14);
			this.Label3_8.TabIndex = 244;
			this.Label3_8.Text = "HEATING / COOLING";
			// 
			// Label3_9
			// 
			this.Label3_9.Location = new System.Drawing.Point(20, 302);
			this.Label3_9.Name = "Label3_9";
			this.Label3_9.Size = new System.Drawing.Size(130, 14);
			this.Label3_9.TabIndex = 243;
			this.Label3_9.Text = "YEAR BUILT";
			// 
			// Label3_10
			// 
			this.Label3_10.Location = new System.Drawing.Point(20, 330);
			this.Label3_10.Name = "Label3_10";
			this.Label3_10.Size = new System.Drawing.Size(130, 14);
			this.Label3_10.TabIndex = 242;
			this.Label3_10.Text = "YEAR REMODELED";
			// 
			// Label3_11
			// 
			this.Label3_11.Location = new System.Drawing.Point(20, 358);
			this.Label3_11.Name = "Label3_11";
			this.Label3_11.Size = new System.Drawing.Size(130, 14);
			this.Label3_11.TabIndex = 241;
			this.Label3_11.Text = "CONDITION";
			// 
			// Label3_12
			// 
			this.Label3_12.Location = new System.Drawing.Point(20, 386);
			this.Label3_12.Name = "Label3_12";
			this.Label3_12.Size = new System.Drawing.Size(130, 14);
			this.Label3_12.TabIndex = 240;
			this.Label3_12.Text = "% GOOD PHYSICAL";
			// 
			// Label3_13
			// 
			this.Label3_13.Location = new System.Drawing.Point(427, 50);
			this.Label3_13.Name = "Label3_13";
			this.Label3_13.Size = new System.Drawing.Size(130, 14);
			this.Label3_13.TabIndex = 239;
			this.Label3_13.Text = "OCCUPANCY CODE";
			// 
			// Label3_14
			// 
			this.Label3_14.Location = new System.Drawing.Point(427, 78);
			this.Label3_14.Name = "Label3_14";
			this.Label3_14.Size = new System.Drawing.Size(130, 14);
			this.Label3_14.TabIndex = 238;
			this.Label3_14.Text = "# OF DWELLING UNITS";
			// 
			// Label3_15
			// 
			this.Label3_15.Location = new System.Drawing.Point(427, 106);
			this.Label3_15.Name = "Label3_15";
			this.Label3_15.Size = new System.Drawing.Size(130, 14);
			this.Label3_15.TabIndex = 237;
			this.Label3_15.Text = "CLASS & QUALITY";
			// 
			// Label3_16
			// 
			this.Label3_16.Location = new System.Drawing.Point(427, 134);
			this.Label3_16.Name = "Label3_16";
			this.Label3_16.Size = new System.Drawing.Size(130, 14);
			this.Label3_16.TabIndex = 236;
			this.Label3_16.Text = "GRADE FACTOR";
			// 
			// Label3_17
			// 
			this.Label3_17.Location = new System.Drawing.Point(427, 162);
			this.Label3_17.Name = "Label3_17";
			this.Label3_17.Size = new System.Drawing.Size(130, 14);
			this.Label3_17.TabIndex = 235;
			this.Label3_17.Text = "EXTERIOR WALLS";
			// 
			// Label3_18
			// 
			this.Label3_18.Location = new System.Drawing.Point(427, 190);
			this.Label3_18.Name = "Label3_18";
			this.Label3_18.Size = new System.Drawing.Size(130, 14);
			this.Label3_18.TabIndex = 234;
			this.Label3_18.Text = "STORIES / HEIGHT";
			// 
			// Label3_19
			// 
			this.Label3_19.Location = new System.Drawing.Point(427, 218);
			this.Label3_19.Name = "Label3_19";
			this.Label3_19.Size = new System.Drawing.Size(130, 14);
			this.Label3_19.TabIndex = 233;
			this.Label3_19.Text = "BASE FLOOR AREA";
			// 
			// Label3_20
			// 
			this.Label3_20.Location = new System.Drawing.Point(427, 246);
			this.Label3_20.Name = "Label3_20";
			this.Label3_20.Size = new System.Drawing.Size(130, 14);
			this.Label3_20.TabIndex = 232;
			this.Label3_20.Text = "PERIMETER / UNITS";
			// 
			// Label3_21
			// 
			this.Label3_21.Location = new System.Drawing.Point(427, 274);
			this.Label3_21.Name = "Label3_21";
			this.Label3_21.Size = new System.Drawing.Size(130, 14);
			this.Label3_21.TabIndex = 231;
			this.Label3_21.Text = "HEATING / COOLING";
			// 
			// Label3_22
			// 
			this.Label3_22.Location = new System.Drawing.Point(427, 302);
			this.Label3_22.Name = "Label3_22";
			this.Label3_22.Size = new System.Drawing.Size(130, 14);
			this.Label3_22.TabIndex = 230;
			this.Label3_22.Text = "YEAR BUILT";
			// 
			// Label3_23
			// 
			this.Label3_23.Location = new System.Drawing.Point(427, 330);
			this.Label3_23.Name = "Label3_23";
			this.Label3_23.Size = new System.Drawing.Size(130, 14);
			this.Label3_23.TabIndex = 229;
			this.Label3_23.Text = "YEAR REMODELED";
			// 
			// Label3_24
			// 
			this.Label3_24.Location = new System.Drawing.Point(427, 358);
			this.Label3_24.Name = "Label3_24";
			this.Label3_24.Size = new System.Drawing.Size(130, 14);
			this.Label3_24.TabIndex = 228;
			this.Label3_24.Text = "CONDITION";
			// 
			// Label3_25
			// 
			this.Label3_25.Location = new System.Drawing.Point(427, 386);
			this.Label3_25.Name = "Label3_25";
			this.Label3_25.Size = new System.Drawing.Size(130, 14);
			this.Label3_25.TabIndex = 227;
			this.Label3_25.Text = "% GOOD PHYSICAL";
			// 
			// Label3_29
			// 
			this.Label3_29.Location = new System.Drawing.Point(20, 414);
			this.Label3_29.Name = "Label3_29";
			this.Label3_29.Size = new System.Drawing.Size(130, 14);
			this.Label3_29.TabIndex = 226;
			this.Label3_29.Text = "FUNCTIONAL";
			// 
			// Label3_30
			// 
			this.Label3_30.Location = new System.Drawing.Point(20, 442);
			this.Label3_30.Name = "Label3_30";
			this.Label3_30.Size = new System.Drawing.Size(130, 14);
			this.Label3_30.TabIndex = 225;
			this.Label3_30.Text = "ECONOMIC";
			// 
			// Label3_31
			// 
			this.Label3_31.Location = new System.Drawing.Point(427, 414);
			this.Label3_31.Name = "Label3_31";
			this.Label3_31.Size = new System.Drawing.Size(130, 14);
			this.Label3_31.TabIndex = 224;
			this.Label3_31.Text = "FUNCTIONAL";
			// 
			// SSTab1_Page5
			// 
			this.SSTab1_Page5.Controls.Add(this.Frame2);
			this.SSTab1_Page5.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page5.Name = "SSTab1_Page5";
			this.SSTab1_Page5.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page5.Text = "Outbuilding";
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_1);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_2);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_3);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_4);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_5);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_6);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_7);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_8);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_9);
			this.Frame2.Controls.Add(this.txtMROIPctPhys1_0);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_0);
			this.Frame2.Controls.Add(this.txtMROICond1_0);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_0);
			this.Frame2.Controls.Add(this.txtMROIType1_0);
			this.Frame2.Controls.Add(this.txtMROIYear1_0);
			this.Frame2.Controls.Add(this.txtMROIUnits1_0);
			this.Frame2.Controls.Add(this.txtMROIType1_9);
			this.Frame2.Controls.Add(this.txtMROIType1_8);
			this.Frame2.Controls.Add(this.txtMROIType1_7);
			this.Frame2.Controls.Add(this.txtMROIType1_6);
			this.Frame2.Controls.Add(this.txtMROIType1_5);
			this.Frame2.Controls.Add(this.txtMROIType1_4);
			this.Frame2.Controls.Add(this.txtMROIType1_3);
			this.Frame2.Controls.Add(this.txtMROIType1_2);
			this.Frame2.Controls.Add(this.txtMROIType1_1);
			this.Frame2.Controls.Add(this.txtMROIYear1_9);
			this.Frame2.Controls.Add(this.txtMROIYear1_8);
			this.Frame2.Controls.Add(this.txtMROIYear1_7);
			this.Frame2.Controls.Add(this.txtMROIYear1_6);
			this.Frame2.Controls.Add(this.txtMROIYear1_5);
			this.Frame2.Controls.Add(this.txtMROIYear1_4);
			this.Frame2.Controls.Add(this.txtMROIYear1_3);
			this.Frame2.Controls.Add(this.txtMROIYear1_2);
			this.Frame2.Controls.Add(this.txtMROIYear1_1);
			this.Frame2.Controls.Add(this.txtMROIUnits1_9);
			this.Frame2.Controls.Add(this.txtMROIUnits1_8);
			this.Frame2.Controls.Add(this.txtMROIUnits1_7);
			this.Frame2.Controls.Add(this.txtMROIUnits1_5);
			this.Frame2.Controls.Add(this.txtMROIUnits1_4);
			this.Frame2.Controls.Add(this.txtMROIUnits1_2);
			this.Frame2.Controls.Add(this.txtMROIUnits1_1);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_8);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_7);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_6);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_5);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_4);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_3);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_2);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_1);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_0);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_9);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_7);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_6);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_5);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_4);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_3);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_2);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_1);
			this.Frame2.Controls.Add(this.txtMROICond1_9);
			this.Frame2.Controls.Add(this.txtMROICond1_8);
			this.Frame2.Controls.Add(this.txtMROICond1_7);
			this.Frame2.Controls.Add(this.txtMROICond1_6);
			this.Frame2.Controls.Add(this.txtMROICond1_5);
			this.Frame2.Controls.Add(this.txtMROICond1_4);
			this.Frame2.Controls.Add(this.txtMROICond1_3);
			this.Frame2.Controls.Add(this.txtMROICond1_2);
			this.Frame2.Controls.Add(this.txtMROICond1_1);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_9);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_8);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_7);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_6);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_5);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_4);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_2);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_1);
			this.Frame2.Controls.Add(this.txtMROIGradeCd1_9);
			this.Frame2.Controls.Add(this.txtMROIUnits1_6);
			this.Frame2.Controls.Add(this.txtMROIGradePct1_8);
			this.Frame2.Controls.Add(this.txtMROIUnits1_3);
			this.Frame2.Controls.Add(this.txtMROIPctFunct1_3);
			this.Frame2.Controls.Add(this.chkSound_0);
			this.Frame2.Controls.Add(this.txtMROISoundValue_0);
			this.Frame2.Controls.Add(this.chkSound_1);
			this.Frame2.Controls.Add(this.txtMROISoundValue_1);
			this.Frame2.Controls.Add(this.chkSound_2);
			this.Frame2.Controls.Add(this.txtMROISoundValue_2);
			this.Frame2.Controls.Add(this.chkSound_3);
			this.Frame2.Controls.Add(this.txtMROISoundValue_3);
			this.Frame2.Controls.Add(this.chkSound_4);
			this.Frame2.Controls.Add(this.txtMROISoundValue_4);
			this.Frame2.Controls.Add(this.chkSound_5);
			this.Frame2.Controls.Add(this.txtMROISoundValue_5);
			this.Frame2.Controls.Add(this.chkSound_6);
			this.Frame2.Controls.Add(this.txtMROISoundValue_6);
			this.Frame2.Controls.Add(this.chkSound_7);
			this.Frame2.Controls.Add(this.txtMROISoundValue_7);
			this.Frame2.Controls.Add(this.chkSound_8);
			this.Frame2.Controls.Add(this.txtMROISoundValue_8);
			this.Frame2.Controls.Add(this.chkSound_9);
			this.Frame2.Controls.Add(this.txtMROISoundValue_9);
			this.Frame2.Controls.Add(this.lblOut);
			this.Frame2.Controls.Add(this.Label2_0);
			this.Frame2.Controls.Add(this.Label2_2);
			this.Frame2.Controls.Add(this.Label2_3);
			this.Frame2.Controls.Add(this.Label2_4);
			this.Frame2.Controls.Add(this.Label2_5);
			this.Frame2.Controls.Add(this.Label2_6);
			this.Frame2.Controls.Add(this.Label2_7);
			this.Frame2.Controls.Add(this.Label2_8);
			this.Frame2.Controls.Add(this.lblMapLotCopy_2);
			this.Frame2.Controls.Add(this.Label2_1);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(859, 442);
			this.Frame2.TabIndex = 211;
			// 
			// txtMROIPctPhys1_1
			// 
			this.txtMROIPctPhys1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_1.Location = new System.Drawing.Point(435, 120);
			this.txtMROIPctPhys1_1.MaxLength = 3;
			this.txtMROIPctPhys1_1.Name = "txtMROIPctPhys1_1";
			this.txtMROIPctPhys1_1.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_1.TabIndex = 77;
			this.txtMROIPctPhys1_1.Tag = "outbuilding";
			this.txtMROIPctPhys1_1.Text = "00";
			this.txtMROIPctPhys1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_1.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_1.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_2
			// 
			this.txtMROIPctPhys1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_2.Location = new System.Drawing.Point(435, 148);
			this.txtMROIPctPhys1_2.MaxLength = 3;
			this.txtMROIPctPhys1_2.Name = "txtMROIPctPhys1_2";
			this.txtMROIPctPhys1_2.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_2.TabIndex = 87;
			this.txtMROIPctPhys1_2.Tag = "outbuilding";
			this.txtMROIPctPhys1_2.Text = "00";
			this.txtMROIPctPhys1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_2.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_2.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_3
			// 
			this.txtMROIPctPhys1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_3.Location = new System.Drawing.Point(435, 176);
			this.txtMROIPctPhys1_3.MaxLength = 3;
			this.txtMROIPctPhys1_3.Name = "txtMROIPctPhys1_3";
			this.txtMROIPctPhys1_3.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_3.TabIndex = 97;
			this.txtMROIPctPhys1_3.Tag = "outbuilding";
			this.txtMROIPctPhys1_3.Text = "00";
			this.txtMROIPctPhys1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_3.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_3.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_4
			// 
			this.txtMROIPctPhys1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_4.Location = new System.Drawing.Point(435, 204);
			this.txtMROIPctPhys1_4.MaxLength = 3;
			this.txtMROIPctPhys1_4.Name = "txtMROIPctPhys1_4";
			this.txtMROIPctPhys1_4.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_4.TabIndex = 107;
			this.txtMROIPctPhys1_4.Tag = "outbuilding";
			this.txtMROIPctPhys1_4.Text = "00";
			this.txtMROIPctPhys1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_4.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_4.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_5
			// 
			this.txtMROIPctPhys1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_5.Location = new System.Drawing.Point(435, 232);
			this.txtMROIPctPhys1_5.MaxLength = 3;
			this.txtMROIPctPhys1_5.Name = "txtMROIPctPhys1_5";
			this.txtMROIPctPhys1_5.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_5.TabIndex = 117;
			this.txtMROIPctPhys1_5.Tag = "outbuilding";
			this.txtMROIPctPhys1_5.Text = "00";
			this.txtMROIPctPhys1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_5.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_5.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_6
			// 
			this.txtMROIPctPhys1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_6.Location = new System.Drawing.Point(435, 260);
			this.txtMROIPctPhys1_6.MaxLength = 3;
			this.txtMROIPctPhys1_6.Name = "txtMROIPctPhys1_6";
			this.txtMROIPctPhys1_6.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_6.TabIndex = 127;
			this.txtMROIPctPhys1_6.Tag = "outbuilding";
			this.txtMROIPctPhys1_6.Text = "00";
			this.txtMROIPctPhys1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_6.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_6.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_7
			// 
			this.txtMROIPctPhys1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_7.Location = new System.Drawing.Point(435, 288);
			this.txtMROIPctPhys1_7.MaxLength = 3;
			this.txtMROIPctPhys1_7.Name = "txtMROIPctPhys1_7";
			this.txtMROIPctPhys1_7.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_7.TabIndex = 137;
			this.txtMROIPctPhys1_7.Tag = "outbuilding";
			this.txtMROIPctPhys1_7.Text = "00";
			this.txtMROIPctPhys1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_7.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_7.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_8
			// 
			this.txtMROIPctPhys1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_8.Location = new System.Drawing.Point(435, 316);
			this.txtMROIPctPhys1_8.MaxLength = 3;
			this.txtMROIPctPhys1_8.Name = "txtMROIPctPhys1_8";
			this.txtMROIPctPhys1_8.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_8.TabIndex = 147;
			this.txtMROIPctPhys1_8.Tag = "outbuilding";
			this.txtMROIPctPhys1_8.Text = "00";
			this.txtMROIPctPhys1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_8.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_8.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_9
			// 
			this.txtMROIPctPhys1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_9.Location = new System.Drawing.Point(435, 344);
			this.txtMROIPctPhys1_9.MaxLength = 3;
			this.txtMROIPctPhys1_9.Name = "txtMROIPctPhys1_9";
			this.txtMROIPctPhys1_9.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_9.TabIndex = 157;
			this.txtMROIPctPhys1_9.Tag = "outbuilding";
			this.txtMROIPctPhys1_9.Text = "00";
			this.txtMROIPctPhys1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_9.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_9.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctPhys1_0
			// 
			this.txtMROIPctPhys1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctPhys1_0.Location = new System.Drawing.Point(435, 92);
			this.txtMROIPctPhys1_0.MaxLength = 3;
			this.txtMROIPctPhys1_0.Name = "txtMROIPctPhys1_0";
			this.txtMROIPctPhys1_0.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctPhys1_0.TabIndex = 67;
			this.txtMROIPctPhys1_0.Tag = "outbuilding";
			this.txtMROIPctPhys1_0.Text = "00";
			this.txtMROIPctPhys1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctPhys1_0.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
			this.txtMROIPctPhys1_0.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
			this.txtMROIPctPhys1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
			this.txtMROIPctPhys1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
			// 
			// txtMROIPctFunct1_0
			// 
			this.txtMROIPctFunct1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_0.Location = new System.Drawing.Point(544, 92);
			this.txtMROIPctFunct1_0.MaxLength = 3;
			this.txtMROIPctFunct1_0.Name = "txtMROIPctFunct1_0";
			this.txtMROIPctFunct1_0.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_0.TabIndex = 68;
			this.txtMROIPctFunct1_0.Tag = "outbuilding";
			this.txtMROIPctFunct1_0.Text = "000";
			this.txtMROIPctFunct1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_0.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_0.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROICond1_0
			// 
			this.txtMROICond1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_0.Location = new System.Drawing.Point(361, 92);
			this.txtMROICond1_0.MaxLength = 1;
			this.txtMROICond1_0.Name = "txtMROICond1_0";
			this.txtMROICond1_0.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_0.TabIndex = 66;
			this.txtMROICond1_0.Tag = "outbuilding";
			this.txtMROICond1_0.Text = "0";
			this.txtMROICond1_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_0.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_0.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROIGradePct1_0
			// 
			this.txtMROIGradePct1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_0.Location = new System.Drawing.Point(277, 92);
			this.txtMROIGradePct1_0.MaxLength = 3;
			this.txtMROIGradePct1_0.Name = "txtMROIGradePct1_0";
			this.txtMROIGradePct1_0.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_0.TabIndex = 65;
			this.txtMROIGradePct1_0.Tag = "outbuilding";
			this.txtMROIGradePct1_0.Text = "000";
			this.txtMROIGradePct1_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_0.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_0.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIType1_0
			// 
			this.txtMROIType1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_0.Location = new System.Drawing.Point(14, 92);
			this.txtMROIType1_0.MaxLength = 3;
			this.txtMROIType1_0.Name = "txtMROIType1_0";
			this.txtMROIType1_0.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_0.TabIndex = 61;
			this.txtMROIType1_0.Tag = "outbuilding";
			this.txtMROIType1_0.Text = "000";
			this.txtMROIType1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_0.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_0.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIYear1_0
			// 
			this.txtMROIYear1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_0.Location = new System.Drawing.Point(80, 92);
			this.txtMROIYear1_0.MaxLength = 4;
			this.txtMROIYear1_0.Name = "txtMROIYear1_0";
			this.txtMROIYear1_0.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_0.TabIndex = 62;
			this.txtMROIYear1_0.Tag = "outbuilding";
			this.txtMROIYear1_0.Text = "0000";
			this.txtMROIYear1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_0.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_0.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIUnits1_0
			// 
			this.txtMROIUnits1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_0.Location = new System.Drawing.Point(156, 92);
			this.txtMROIUnits1_0.MaxLength = 6;
			this.txtMROIUnits1_0.Name = "txtMROIUnits1_0";
			this.txtMROIUnits1_0.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_0.TabIndex = 63;
			this.txtMROIUnits1_0.Tag = "outbuilding";
			this.txtMROIUnits1_0.Text = "0000";
			this.txtMROIUnits1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_0.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_0.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIType1_9
			// 
			this.txtMROIType1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_9.Location = new System.Drawing.Point(14, 344);
			this.txtMROIType1_9.MaxLength = 3;
			this.txtMROIType1_9.Name = "txtMROIType1_9";
			this.txtMROIType1_9.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_9.TabIndex = 151;
			this.txtMROIType1_9.Tag = "outbuilding";
			this.txtMROIType1_9.Text = "000";
			this.txtMROIType1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_9.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_9.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_8
			// 
			this.txtMROIType1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_8.Location = new System.Drawing.Point(14, 316);
			this.txtMROIType1_8.MaxLength = 3;
			this.txtMROIType1_8.Name = "txtMROIType1_8";
			this.txtMROIType1_8.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_8.TabIndex = 141;
			this.txtMROIType1_8.Tag = "outbuilding";
			this.txtMROIType1_8.Text = "000";
			this.txtMROIType1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_8.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_8.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_7
			// 
			this.txtMROIType1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_7.Location = new System.Drawing.Point(14, 288);
			this.txtMROIType1_7.MaxLength = 3;
			this.txtMROIType1_7.Name = "txtMROIType1_7";
			this.txtMROIType1_7.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_7.TabIndex = 131;
			this.txtMROIType1_7.Tag = "outbuilding";
			this.txtMROIType1_7.Text = "000";
			this.txtMROIType1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_7.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_7.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_6
			// 
			this.txtMROIType1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_6.Location = new System.Drawing.Point(14, 260);
			this.txtMROIType1_6.MaxLength = 3;
			this.txtMROIType1_6.Name = "txtMROIType1_6";
			this.txtMROIType1_6.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_6.TabIndex = 121;
			this.txtMROIType1_6.Tag = "outbuilding";
			this.txtMROIType1_6.Text = "000";
			this.txtMROIType1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_6.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_6.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_5
			// 
			this.txtMROIType1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_5.Location = new System.Drawing.Point(14, 232);
			this.txtMROIType1_5.MaxLength = 3;
			this.txtMROIType1_5.Name = "txtMROIType1_5";
			this.txtMROIType1_5.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_5.TabIndex = 111;
			this.txtMROIType1_5.Tag = "outbuilding";
			this.txtMROIType1_5.Text = "000";
			this.txtMROIType1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_5.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_5.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_4
			// 
			this.txtMROIType1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_4.Location = new System.Drawing.Point(14, 204);
			this.txtMROIType1_4.MaxLength = 3;
			this.txtMROIType1_4.Name = "txtMROIType1_4";
			this.txtMROIType1_4.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_4.TabIndex = 101;
			this.txtMROIType1_4.Tag = "outbuilding";
			this.txtMROIType1_4.Text = "000";
			this.txtMROIType1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_4.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_4.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_3
			// 
			this.txtMROIType1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_3.Location = new System.Drawing.Point(14, 176);
			this.txtMROIType1_3.MaxLength = 3;
			this.txtMROIType1_3.Name = "txtMROIType1_3";
			this.txtMROIType1_3.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_3.TabIndex = 91;
			this.txtMROIType1_3.Tag = "outbuilding";
			this.txtMROIType1_3.Text = "000";
			this.txtMROIType1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_3.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_3.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_2
			// 
			this.txtMROIType1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_2.Location = new System.Drawing.Point(14, 148);
			this.txtMROIType1_2.MaxLength = 3;
			this.txtMROIType1_2.Name = "txtMROIType1_2";
			this.txtMROIType1_2.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_2.TabIndex = 81;
			this.txtMROIType1_2.Tag = "outbuilding";
			this.txtMROIType1_2.Text = "000";
			this.txtMROIType1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_2.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_2.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIType1_1
			// 
			this.txtMROIType1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIType1_1.Location = new System.Drawing.Point(14, 120);
			this.txtMROIType1_1.MaxLength = 3;
			this.txtMROIType1_1.Name = "txtMROIType1_1";
			this.txtMROIType1_1.Size = new System.Drawing.Size(59, 20);
			this.txtMROIType1_1.TabIndex = 71;
			this.txtMROIType1_1.Tag = "outbuilding";
			this.txtMROIType1_1.Text = "000";
			this.txtMROIType1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIType1_1.Enter += new System.EventHandler(this.txtMROIType1_Enter);
			this.txtMROIType1_1.Leave += new System.EventHandler(this.txtMROIType1_Leave);
			this.txtMROIType1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
			this.txtMROIType1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
			this.txtMROIType1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
			// 
			// txtMROIYear1_9
			// 
			this.txtMROIYear1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_9.Location = new System.Drawing.Point(80, 344);
			this.txtMROIYear1_9.MaxLength = 4;
			this.txtMROIYear1_9.Name = "txtMROIYear1_9";
			this.txtMROIYear1_9.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_9.TabIndex = 152;
			this.txtMROIYear1_9.Tag = "outbuilding";
			this.txtMROIYear1_9.Text = "0000";
			this.txtMROIYear1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_9.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_9.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_8
			// 
			this.txtMROIYear1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_8.Location = new System.Drawing.Point(80, 316);
			this.txtMROIYear1_8.MaxLength = 4;
			this.txtMROIYear1_8.Name = "txtMROIYear1_8";
			this.txtMROIYear1_8.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_8.TabIndex = 142;
			this.txtMROIYear1_8.Tag = "outbuilding";
			this.txtMROIYear1_8.Text = "0000";
			this.txtMROIYear1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_8.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_8.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_7
			// 
			this.txtMROIYear1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_7.Location = new System.Drawing.Point(80, 288);
			this.txtMROIYear1_7.MaxLength = 4;
			this.txtMROIYear1_7.Name = "txtMROIYear1_7";
			this.txtMROIYear1_7.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_7.TabIndex = 132;
			this.txtMROIYear1_7.Tag = "outbuilding";
			this.txtMROIYear1_7.Text = "0000";
			this.txtMROIYear1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_7.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_7.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_6
			// 
			this.txtMROIYear1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_6.Location = new System.Drawing.Point(80, 260);
			this.txtMROIYear1_6.MaxLength = 4;
			this.txtMROIYear1_6.Name = "txtMROIYear1_6";
			this.txtMROIYear1_6.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_6.TabIndex = 122;
			this.txtMROIYear1_6.Tag = "outbuilding";
			this.txtMROIYear1_6.Text = "0000";
			this.txtMROIYear1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_6.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_6.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_5
			// 
			this.txtMROIYear1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_5.Location = new System.Drawing.Point(80, 232);
			this.txtMROIYear1_5.MaxLength = 4;
			this.txtMROIYear1_5.Name = "txtMROIYear1_5";
			this.txtMROIYear1_5.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_5.TabIndex = 112;
			this.txtMROIYear1_5.Tag = "outbuilding";
			this.txtMROIYear1_5.Text = "0000";
			this.txtMROIYear1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_5.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_5.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_4
			// 
			this.txtMROIYear1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_4.Location = new System.Drawing.Point(80, 204);
			this.txtMROIYear1_4.MaxLength = 4;
			this.txtMROIYear1_4.Name = "txtMROIYear1_4";
			this.txtMROIYear1_4.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_4.TabIndex = 102;
			this.txtMROIYear1_4.Tag = "outbuilding";
			this.txtMROIYear1_4.Text = "0000";
			this.txtMROIYear1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_4.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_4.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_3
			// 
			this.txtMROIYear1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_3.Location = new System.Drawing.Point(80, 176);
			this.txtMROIYear1_3.MaxLength = 4;
			this.txtMROIYear1_3.Name = "txtMROIYear1_3";
			this.txtMROIYear1_3.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_3.TabIndex = 92;
			this.txtMROIYear1_3.Tag = "outbuilding";
			this.txtMROIYear1_3.Text = "0000";
			this.txtMROIYear1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_3.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_3.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_2
			// 
			this.txtMROIYear1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_2.Location = new System.Drawing.Point(80, 148);
			this.txtMROIYear1_2.MaxLength = 4;
			this.txtMROIYear1_2.Name = "txtMROIYear1_2";
			this.txtMROIYear1_2.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_2.TabIndex = 82;
			this.txtMROIYear1_2.Tag = "outbuilding";
			this.txtMROIYear1_2.Text = "0000";
			this.txtMROIYear1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_2.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_2.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIYear1_1
			// 
			this.txtMROIYear1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIYear1_1.Location = new System.Drawing.Point(80, 120);
			this.txtMROIYear1_1.MaxLength = 4;
			this.txtMROIYear1_1.Name = "txtMROIYear1_1";
			this.txtMROIYear1_1.Size = new System.Drawing.Size(68, 20);
			this.txtMROIYear1_1.TabIndex = 72;
			this.txtMROIYear1_1.Tag = "outbuilding";
			this.txtMROIYear1_1.Text = "0000";
			this.txtMROIYear1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIYear1_1.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
			this.txtMROIYear1_1.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
			this.txtMROIYear1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
			this.txtMROIYear1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
			// 
			// txtMROIUnits1_9
			// 
			this.txtMROIUnits1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_9.Location = new System.Drawing.Point(156, 344);
			this.txtMROIUnits1_9.MaxLength = 7;
			this.txtMROIUnits1_9.Name = "txtMROIUnits1_9";
			this.txtMROIUnits1_9.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_9.TabIndex = 153;
			this.txtMROIUnits1_9.Tag = "outbuilding";
			this.txtMROIUnits1_9.Text = "0000";
			this.txtMROIUnits1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_9.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_9.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIUnits1_8
			// 
			this.txtMROIUnits1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_8.Location = new System.Drawing.Point(156, 316);
			this.txtMROIUnits1_8.MaxLength = 7;
			this.txtMROIUnits1_8.Name = "txtMROIUnits1_8";
			this.txtMROIUnits1_8.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_8.TabIndex = 143;
			this.txtMROIUnits1_8.Tag = "outbuilding";
			this.txtMROIUnits1_8.Text = "0000";
			this.txtMROIUnits1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_8.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_8.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIUnits1_7
			// 
			this.txtMROIUnits1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_7.Location = new System.Drawing.Point(156, 288);
			this.txtMROIUnits1_7.MaxLength = 7;
			this.txtMROIUnits1_7.Name = "txtMROIUnits1_7";
			this.txtMROIUnits1_7.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_7.TabIndex = 133;
			this.txtMROIUnits1_7.Tag = "outbuilding";
			this.txtMROIUnits1_7.Text = "0000";
			this.txtMROIUnits1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_7.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_7.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIUnits1_5
			// 
			this.txtMROIUnits1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_5.Location = new System.Drawing.Point(156, 232);
			this.txtMROIUnits1_5.MaxLength = 7;
			this.txtMROIUnits1_5.Name = "txtMROIUnits1_5";
			this.txtMROIUnits1_5.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_5.TabIndex = 113;
			this.txtMROIUnits1_5.Tag = "outbuilding";
			this.txtMROIUnits1_5.Text = "0000";
			this.txtMROIUnits1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_5.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_5.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIUnits1_4
			// 
			this.txtMROIUnits1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_4.Location = new System.Drawing.Point(156, 204);
			this.txtMROIUnits1_4.MaxLength = 7;
			this.txtMROIUnits1_4.Name = "txtMROIUnits1_4";
			this.txtMROIUnits1_4.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_4.TabIndex = 103;
			this.txtMROIUnits1_4.Tag = "outbuilding";
			this.txtMROIUnits1_4.Text = "0000";
			this.txtMROIUnits1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_4.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_4.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIUnits1_2
			// 
			this.txtMROIUnits1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_2.Location = new System.Drawing.Point(156, 148);
			this.txtMROIUnits1_2.MaxLength = 7;
			this.txtMROIUnits1_2.Name = "txtMROIUnits1_2";
			this.txtMROIUnits1_2.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_2.TabIndex = 83;
			this.txtMROIUnits1_2.Tag = "outbuilding";
			this.txtMROIUnits1_2.Text = "0000";
			this.txtMROIUnits1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_2.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_2.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIUnits1_1
			// 
			this.txtMROIUnits1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_1.Location = new System.Drawing.Point(156, 120);
			this.txtMROIUnits1_1.MaxLength = 7;
			this.txtMROIUnits1_1.Name = "txtMROIUnits1_1";
			this.txtMROIUnits1_1.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_1.TabIndex = 73;
			this.txtMROIUnits1_1.Tag = "outbuilding";
			this.txtMROIUnits1_1.Text = "0000";
			this.txtMROIUnits1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_1.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_1.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIGradeCd1_8
			// 
			this.txtMROIGradeCd1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_8.Location = new System.Drawing.Point(236, 316);
			this.txtMROIGradeCd1_8.MaxLength = 1;
			this.txtMROIGradeCd1_8.Name = "txtMROIGradeCd1_8";
			this.txtMROIGradeCd1_8.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_8.TabIndex = 144;
			this.txtMROIGradeCd1_8.Tag = "outbuilding";
			this.txtMROIGradeCd1_8.Text = "0";
			this.txtMROIGradeCd1_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_8.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_8.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_7
			// 
			this.txtMROIGradeCd1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_7.Location = new System.Drawing.Point(236, 288);
			this.txtMROIGradeCd1_7.MaxLength = 1;
			this.txtMROIGradeCd1_7.Name = "txtMROIGradeCd1_7";
			this.txtMROIGradeCd1_7.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_7.TabIndex = 134;
			this.txtMROIGradeCd1_7.Tag = "outbuilding";
			this.txtMROIGradeCd1_7.Text = "0";
			this.txtMROIGradeCd1_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_7.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_7.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_6
			// 
			this.txtMROIGradeCd1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_6.Location = new System.Drawing.Point(236, 260);
			this.txtMROIGradeCd1_6.MaxLength = 1;
			this.txtMROIGradeCd1_6.Name = "txtMROIGradeCd1_6";
			this.txtMROIGradeCd1_6.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_6.TabIndex = 124;
			this.txtMROIGradeCd1_6.Tag = "outbuilding";
			this.txtMROIGradeCd1_6.Text = "0";
			this.txtMROIGradeCd1_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_6.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_6.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_5
			// 
			this.txtMROIGradeCd1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_5.Location = new System.Drawing.Point(236, 232);
			this.txtMROIGradeCd1_5.MaxLength = 1;
			this.txtMROIGradeCd1_5.Name = "txtMROIGradeCd1_5";
			this.txtMROIGradeCd1_5.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_5.TabIndex = 114;
			this.txtMROIGradeCd1_5.Tag = "outbuilding";
			this.txtMROIGradeCd1_5.Text = "0";
			this.txtMROIGradeCd1_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_5.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_5.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_4
			// 
			this.txtMROIGradeCd1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_4.Location = new System.Drawing.Point(236, 204);
			this.txtMROIGradeCd1_4.MaxLength = 1;
			this.txtMROIGradeCd1_4.Name = "txtMROIGradeCd1_4";
			this.txtMROIGradeCd1_4.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_4.TabIndex = 104;
			this.txtMROIGradeCd1_4.Tag = "outbuilding";
			this.txtMROIGradeCd1_4.Text = "0";
			this.txtMROIGradeCd1_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_4.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_4.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_3
			// 
			this.txtMROIGradeCd1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_3.Location = new System.Drawing.Point(235, 176);
			this.txtMROIGradeCd1_3.MaxLength = 1;
			this.txtMROIGradeCd1_3.Name = "txtMROIGradeCd1_3";
			this.txtMROIGradeCd1_3.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_3.TabIndex = 94;
			this.txtMROIGradeCd1_3.Tag = "outbuilding";
			this.txtMROIGradeCd1_3.Text = "0";
			this.txtMROIGradeCd1_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_3.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_3.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_2
			// 
			this.txtMROIGradeCd1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_2.Location = new System.Drawing.Point(236, 148);
			this.txtMROIGradeCd1_2.MaxLength = 1;
			this.txtMROIGradeCd1_2.Name = "txtMROIGradeCd1_2";
			this.txtMROIGradeCd1_2.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_2.TabIndex = 84;
			this.txtMROIGradeCd1_2.Tag = "outbuilding";
			this.txtMROIGradeCd1_2.Text = "0";
			this.txtMROIGradeCd1_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_2.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_2.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_1
			// 
			this.txtMROIGradeCd1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_1.Location = new System.Drawing.Point(236, 120);
			this.txtMROIGradeCd1_1.MaxLength = 1;
			this.txtMROIGradeCd1_1.Name = "txtMROIGradeCd1_1";
			this.txtMROIGradeCd1_1.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_1.TabIndex = 74;
			this.txtMROIGradeCd1_1.Tag = "outbuilding";
			this.txtMROIGradeCd1_1.Text = "0";
			this.txtMROIGradeCd1_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_1.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_1.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradeCd1_0
			// 
			this.txtMROIGradeCd1_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_0.Location = new System.Drawing.Point(236, 92);
			this.txtMROIGradeCd1_0.MaxLength = 1;
			this.txtMROIGradeCd1_0.Name = "txtMROIGradeCd1_0";
			this.txtMROIGradeCd1_0.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_0.TabIndex = 64;
			this.txtMROIGradeCd1_0.Tag = "outbuilding";
			this.txtMROIGradeCd1_0.Text = "0";
			this.txtMROIGradeCd1_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_0.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_0.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIGradePct1_9
			// 
			this.txtMROIGradePct1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_9.Location = new System.Drawing.Point(277, 344);
			this.txtMROIGradePct1_9.MaxLength = 3;
			this.txtMROIGradePct1_9.Name = "txtMROIGradePct1_9";
			this.txtMROIGradePct1_9.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_9.TabIndex = 155;
			this.txtMROIGradePct1_9.Tag = "outbuilding";
			this.txtMROIGradePct1_9.Text = "000";
			this.txtMROIGradePct1_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_9.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_9.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_7
			// 
			this.txtMROIGradePct1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_7.Location = new System.Drawing.Point(277, 288);
			this.txtMROIGradePct1_7.MaxLength = 3;
			this.txtMROIGradePct1_7.Name = "txtMROIGradePct1_7";
			this.txtMROIGradePct1_7.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_7.TabIndex = 135;
			this.txtMROIGradePct1_7.Tag = "outbuilding";
			this.txtMROIGradePct1_7.Text = "000";
			this.txtMROIGradePct1_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_7.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_7.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_6
			// 
			this.txtMROIGradePct1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_6.Location = new System.Drawing.Point(277, 260);
			this.txtMROIGradePct1_6.MaxLength = 3;
			this.txtMROIGradePct1_6.Name = "txtMROIGradePct1_6";
			this.txtMROIGradePct1_6.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_6.TabIndex = 125;
			this.txtMROIGradePct1_6.Tag = "outbuilding";
			this.txtMROIGradePct1_6.Text = "000";
			this.txtMROIGradePct1_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_6.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_6.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_5
			// 
			this.txtMROIGradePct1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_5.Location = new System.Drawing.Point(277, 232);
			this.txtMROIGradePct1_5.MaxLength = 3;
			this.txtMROIGradePct1_5.Name = "txtMROIGradePct1_5";
			this.txtMROIGradePct1_5.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_5.TabIndex = 115;
			this.txtMROIGradePct1_5.Tag = "outbuilding";
			this.txtMROIGradePct1_5.Text = "000";
			this.txtMROIGradePct1_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_5.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_5.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_4
			// 
			this.txtMROIGradePct1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_4.Location = new System.Drawing.Point(277, 204);
			this.txtMROIGradePct1_4.MaxLength = 3;
			this.txtMROIGradePct1_4.Name = "txtMROIGradePct1_4";
			this.txtMROIGradePct1_4.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_4.TabIndex = 105;
			this.txtMROIGradePct1_4.Tag = "outbuilding";
			this.txtMROIGradePct1_4.Text = "000";
			this.txtMROIGradePct1_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_4.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_4.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_3
			// 
			this.txtMROIGradePct1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_3.Location = new System.Drawing.Point(277, 176);
			this.txtMROIGradePct1_3.MaxLength = 3;
			this.txtMROIGradePct1_3.Name = "txtMROIGradePct1_3";
			this.txtMROIGradePct1_3.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_3.TabIndex = 95;
			this.txtMROIGradePct1_3.Tag = "outbuilding";
			this.txtMROIGradePct1_3.Text = "000";
			this.txtMROIGradePct1_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_3.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_3.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_2
			// 
			this.txtMROIGradePct1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_2.Location = new System.Drawing.Point(277, 148);
			this.txtMROIGradePct1_2.MaxLength = 3;
			this.txtMROIGradePct1_2.Name = "txtMROIGradePct1_2";
			this.txtMROIGradePct1_2.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_2.TabIndex = 85;
			this.txtMROIGradePct1_2.Tag = "outbuilding";
			this.txtMROIGradePct1_2.Text = "000";
			this.txtMROIGradePct1_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_2.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_2.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIGradePct1_1
			// 
			this.txtMROIGradePct1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_1.Location = new System.Drawing.Point(277, 120);
			this.txtMROIGradePct1_1.MaxLength = 3;
			this.txtMROIGradePct1_1.Name = "txtMROIGradePct1_1";
			this.txtMROIGradePct1_1.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_1.TabIndex = 75;
			this.txtMROIGradePct1_1.Tag = "outbuilding";
			this.txtMROIGradePct1_1.Text = "000";
			this.txtMROIGradePct1_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_1.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_1.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROICond1_9
			// 
			this.txtMROICond1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_9.Location = new System.Drawing.Point(361, 344);
			this.txtMROICond1_9.MaxLength = 1;
			this.txtMROICond1_9.Name = "txtMROICond1_9";
			this.txtMROICond1_9.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_9.TabIndex = 156;
			this.txtMROICond1_9.Tag = "outbuilding";
			this.txtMROICond1_9.Text = "0";
			this.txtMROICond1_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_9.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_9.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_8
			// 
			this.txtMROICond1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_8.Location = new System.Drawing.Point(361, 316);
			this.txtMROICond1_8.MaxLength = 1;
			this.txtMROICond1_8.Name = "txtMROICond1_8";
			this.txtMROICond1_8.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_8.TabIndex = 146;
			this.txtMROICond1_8.Tag = "outbuilding";
			this.txtMROICond1_8.Text = "0";
			this.txtMROICond1_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_8.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_8.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_7
			// 
			this.txtMROICond1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_7.Location = new System.Drawing.Point(361, 288);
			this.txtMROICond1_7.MaxLength = 1;
			this.txtMROICond1_7.Name = "txtMROICond1_7";
			this.txtMROICond1_7.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_7.TabIndex = 136;
			this.txtMROICond1_7.Tag = "outbuilding";
			this.txtMROICond1_7.Text = "0";
			this.txtMROICond1_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_7.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_7.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_6
			// 
			this.txtMROICond1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_6.Location = new System.Drawing.Point(361, 260);
			this.txtMROICond1_6.MaxLength = 1;
			this.txtMROICond1_6.Name = "txtMROICond1_6";
			this.txtMROICond1_6.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_6.TabIndex = 126;
			this.txtMROICond1_6.Tag = "outbuilding";
			this.txtMROICond1_6.Text = "0";
			this.txtMROICond1_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_6.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_6.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_5
			// 
			this.txtMROICond1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_5.Location = new System.Drawing.Point(361, 232);
			this.txtMROICond1_5.MaxLength = 1;
			this.txtMROICond1_5.Name = "txtMROICond1_5";
			this.txtMROICond1_5.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_5.TabIndex = 116;
			this.txtMROICond1_5.Tag = "outbuilding";
			this.txtMROICond1_5.Text = "0";
			this.txtMROICond1_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_5.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_5.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_4
			// 
			this.txtMROICond1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_4.Location = new System.Drawing.Point(361, 204);
			this.txtMROICond1_4.MaxLength = 1;
			this.txtMROICond1_4.Name = "txtMROICond1_4";
			this.txtMROICond1_4.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_4.TabIndex = 106;
			this.txtMROICond1_4.Tag = "outbuilding";
			this.txtMROICond1_4.Text = "0";
			this.txtMROICond1_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_4.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_4.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_3
			// 
			this.txtMROICond1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_3.Location = new System.Drawing.Point(361, 176);
			this.txtMROICond1_3.MaxLength = 1;
			this.txtMROICond1_3.Name = "txtMROICond1_3";
			this.txtMROICond1_3.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_3.TabIndex = 96;
			this.txtMROICond1_3.Tag = "outbuilding";
			this.txtMROICond1_3.Text = "0";
			this.txtMROICond1_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_3.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_3.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_2
			// 
			this.txtMROICond1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_2.Location = new System.Drawing.Point(361, 148);
			this.txtMROICond1_2.MaxLength = 1;
			this.txtMROICond1_2.Name = "txtMROICond1_2";
			this.txtMROICond1_2.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_2.TabIndex = 86;
			this.txtMROICond1_2.Tag = "outbuilding";
			this.txtMROICond1_2.Text = "0";
			this.txtMROICond1_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_2.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_2.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROICond1_1
			// 
			this.txtMROICond1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROICond1_1.Location = new System.Drawing.Point(361, 120);
			this.txtMROICond1_1.MaxLength = 1;
			this.txtMROICond1_1.Name = "txtMROICond1_1";
			this.txtMROICond1_1.Size = new System.Drawing.Size(40, 20);
			this.txtMROICond1_1.TabIndex = 76;
			this.txtMROICond1_1.Tag = "outbuilding";
			this.txtMROICond1_1.Text = "0";
			this.txtMROICond1_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROICond1_1.Enter += new System.EventHandler(this.txtMROICond1_Enter);
			this.txtMROICond1_1.Leave += new System.EventHandler(this.txtMROICond1_Leave);
			this.txtMROICond1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
			this.txtMROICond1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
			// 
			// txtMROIPctFunct1_9
			// 
			this.txtMROIPctFunct1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_9.Location = new System.Drawing.Point(544, 344);
			this.txtMROIPctFunct1_9.MaxLength = 3;
			this.txtMROIPctFunct1_9.Name = "txtMROIPctFunct1_9";
			this.txtMROIPctFunct1_9.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_9.TabIndex = 158;
			this.txtMROIPctFunct1_9.Tag = "outbuilding";
			this.txtMROIPctFunct1_9.Text = "000";
			this.txtMROIPctFunct1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_9.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_9.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_8
			// 
			this.txtMROIPctFunct1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_8.Location = new System.Drawing.Point(544, 316);
			this.txtMROIPctFunct1_8.MaxLength = 3;
			this.txtMROIPctFunct1_8.Name = "txtMROIPctFunct1_8";
			this.txtMROIPctFunct1_8.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_8.TabIndex = 148;
			this.txtMROIPctFunct1_8.Tag = "outbuilding";
			this.txtMROIPctFunct1_8.Text = "000";
			this.txtMROIPctFunct1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_8.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_8.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_7
			// 
			this.txtMROIPctFunct1_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_7.Location = new System.Drawing.Point(544, 288);
			this.txtMROIPctFunct1_7.MaxLength = 3;
			this.txtMROIPctFunct1_7.Name = "txtMROIPctFunct1_7";
			this.txtMROIPctFunct1_7.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_7.TabIndex = 138;
			this.txtMROIPctFunct1_7.Tag = "outbuilding";
			this.txtMROIPctFunct1_7.Text = "000";
			this.txtMROIPctFunct1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_7.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_7.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_6
			// 
			this.txtMROIPctFunct1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_6.Location = new System.Drawing.Point(544, 260);
			this.txtMROIPctFunct1_6.MaxLength = 3;
			this.txtMROIPctFunct1_6.Name = "txtMROIPctFunct1_6";
			this.txtMROIPctFunct1_6.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_6.TabIndex = 128;
			this.txtMROIPctFunct1_6.Tag = "outbuilding";
			this.txtMROIPctFunct1_6.Text = "000";
			this.txtMROIPctFunct1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_6.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_6.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_5
			// 
			this.txtMROIPctFunct1_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_5.Location = new System.Drawing.Point(544, 232);
			this.txtMROIPctFunct1_5.MaxLength = 3;
			this.txtMROIPctFunct1_5.Name = "txtMROIPctFunct1_5";
			this.txtMROIPctFunct1_5.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_5.TabIndex = 118;
			this.txtMROIPctFunct1_5.Tag = "outbuilding";
			this.txtMROIPctFunct1_5.Text = "000";
			this.txtMROIPctFunct1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_5.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_5.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_4
			// 
			this.txtMROIPctFunct1_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_4.Location = new System.Drawing.Point(544, 204);
			this.txtMROIPctFunct1_4.MaxLength = 3;
			this.txtMROIPctFunct1_4.Name = "txtMROIPctFunct1_4";
			this.txtMROIPctFunct1_4.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_4.TabIndex = 108;
			this.txtMROIPctFunct1_4.Tag = "outbuilding";
			this.txtMROIPctFunct1_4.Text = "000";
			this.txtMROIPctFunct1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_4.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_4.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_2
			// 
			this.txtMROIPctFunct1_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_2.Location = new System.Drawing.Point(544, 148);
			this.txtMROIPctFunct1_2.MaxLength = 3;
			this.txtMROIPctFunct1_2.Name = "txtMROIPctFunct1_2";
			this.txtMROIPctFunct1_2.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_2.TabIndex = 88;
			this.txtMROIPctFunct1_2.Tag = "outbuilding";
			this.txtMROIPctFunct1_2.Text = "000";
			this.txtMROIPctFunct1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_2.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_2.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIPctFunct1_1
			// 
			this.txtMROIPctFunct1_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_1.Location = new System.Drawing.Point(544, 120);
			this.txtMROIPctFunct1_1.MaxLength = 3;
			this.txtMROIPctFunct1_1.Name = "txtMROIPctFunct1_1";
			this.txtMROIPctFunct1_1.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_1.TabIndex = 78;
			this.txtMROIPctFunct1_1.Tag = "outbuilding";
			this.txtMROIPctFunct1_1.Text = "000";
			this.txtMROIPctFunct1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_1.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_1.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// txtMROIGradeCd1_9
			// 
			this.txtMROIGradeCd1_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradeCd1_9.Location = new System.Drawing.Point(236, 344);
			this.txtMROIGradeCd1_9.MaxLength = 1;
			this.txtMROIGradeCd1_9.Name = "txtMROIGradeCd1_9";
			this.txtMROIGradeCd1_9.Size = new System.Drawing.Size(40, 20);
			this.txtMROIGradeCd1_9.TabIndex = 154;
			this.txtMROIGradeCd1_9.Tag = "outbuilding";
			this.txtMROIGradeCd1_9.Text = "0";
			this.txtMROIGradeCd1_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradeCd1_9.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
			this.txtMROIGradeCd1_9.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
			this.txtMROIGradeCd1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
			// 
			// txtMROIUnits1_6
			// 
			this.txtMROIUnits1_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_6.Location = new System.Drawing.Point(156, 260);
			this.txtMROIUnits1_6.MaxLength = 7;
			this.txtMROIUnits1_6.Name = "txtMROIUnits1_6";
			this.txtMROIUnits1_6.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_6.TabIndex = 123;
			this.txtMROIUnits1_6.Tag = "outbuilding";
			this.txtMROIUnits1_6.Text = "0000";
			this.txtMROIUnits1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_6.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_6.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIGradePct1_8
			// 
			this.txtMROIGradePct1_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIGradePct1_8.Location = new System.Drawing.Point(277, 316);
			this.txtMROIGradePct1_8.MaxLength = 3;
			this.txtMROIGradePct1_8.Name = "txtMROIGradePct1_8";
			this.txtMROIGradePct1_8.Size = new System.Drawing.Size(60, 20);
			this.txtMROIGradePct1_8.TabIndex = 145;
			this.txtMROIGradePct1_8.Tag = "outbuilding";
			this.txtMROIGradePct1_8.Text = "000";
			this.txtMROIGradePct1_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtMROIGradePct1_8.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
			this.txtMROIGradePct1_8.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
			this.txtMROIGradePct1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
			this.txtMROIGradePct1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
			// 
			// txtMROIUnits1_3
			// 
			this.txtMROIUnits1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIUnits1_3.Location = new System.Drawing.Point(156, 176);
			this.txtMROIUnits1_3.MaxLength = 7;
			this.txtMROIUnits1_3.Name = "txtMROIUnits1_3";
			this.txtMROIUnits1_3.Size = new System.Drawing.Size(64, 20);
			this.txtMROIUnits1_3.TabIndex = 93;
			this.txtMROIUnits1_3.Tag = "outbuilding";
			this.txtMROIUnits1_3.Text = "0000";
			this.txtMROIUnits1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIUnits1_3.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
			this.txtMROIUnits1_3.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
			this.txtMROIUnits1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
			this.txtMROIUnits1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
			// 
			// txtMROIPctFunct1_3
			// 
			this.txtMROIPctFunct1_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROIPctFunct1_3.Location = new System.Drawing.Point(544, 176);
			this.txtMROIPctFunct1_3.MaxLength = 3;
			this.txtMROIPctFunct1_3.Name = "txtMROIPctFunct1_3";
			this.txtMROIPctFunct1_3.Size = new System.Drawing.Size(58, 20);
			this.txtMROIPctFunct1_3.TabIndex = 98;
			this.txtMROIPctFunct1_3.Tag = "outbuilding";
			this.txtMROIPctFunct1_3.Text = "000";
			this.txtMROIPctFunct1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROIPctFunct1_3.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
			this.txtMROIPctFunct1_3.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
			this.txtMROIPctFunct1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
			this.txtMROIPctFunct1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
			// 
			// chkSound_0
			// 
			this.chkSound_0.Location = new System.Drawing.Point(659, 90);
			this.chkSound_0.Name = "chkSound_0";
			this.chkSound_0.Size = new System.Drawing.Size(22, 22);
			this.chkSound_0.TabIndex = 69;
			// 
			// txtMROISoundValue_0
			// 
			this.txtMROISoundValue_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_0.Location = new System.Drawing.Point(727, 92);
			this.txtMROISoundValue_0.MaxLength = 9;
			this.txtMROISoundValue_0.Name = "txtMROISoundValue_0";
			this.txtMROISoundValue_0.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_0.TabIndex = 70;
			this.txtMROISoundValue_0.Tag = "outbuilding";
			this.txtMROISoundValue_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_0.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_1
			// 
			this.chkSound_1.Location = new System.Drawing.Point(659, 118);
			this.chkSound_1.Name = "chkSound_1";
			this.chkSound_1.Size = new System.Drawing.Size(22, 22);
			this.chkSound_1.TabIndex = 79;
			// 
			// txtMROISoundValue_1
			// 
			this.txtMROISoundValue_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_1.Location = new System.Drawing.Point(727, 120);
			this.txtMROISoundValue_1.MaxLength = 9;
			this.txtMROISoundValue_1.Name = "txtMROISoundValue_1";
			this.txtMROISoundValue_1.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_1.TabIndex = 80;
			this.txtMROISoundValue_1.Tag = "outbuilding";
			this.txtMROISoundValue_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_1.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_2
			// 
			this.chkSound_2.Location = new System.Drawing.Point(659, 146);
			this.chkSound_2.Name = "chkSound_2";
			this.chkSound_2.Size = new System.Drawing.Size(22, 22);
			this.chkSound_2.TabIndex = 89;
			// 
			// txtMROISoundValue_2
			// 
			this.txtMROISoundValue_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_2.Location = new System.Drawing.Point(727, 148);
			this.txtMROISoundValue_2.MaxLength = 9;
			this.txtMROISoundValue_2.Name = "txtMROISoundValue_2";
			this.txtMROISoundValue_2.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_2.TabIndex = 90;
			this.txtMROISoundValue_2.Tag = "outbuilding";
			this.txtMROISoundValue_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_2.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_3
			// 
			this.chkSound_3.Location = new System.Drawing.Point(659, 174);
			this.chkSound_3.Name = "chkSound_3";
			this.chkSound_3.Size = new System.Drawing.Size(22, 22);
			this.chkSound_3.TabIndex = 99;
			// 
			// txtMROISoundValue_3
			// 
			this.txtMROISoundValue_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_3.Location = new System.Drawing.Point(727, 176);
			this.txtMROISoundValue_3.MaxLength = 9;
			this.txtMROISoundValue_3.Name = "txtMROISoundValue_3";
			this.txtMROISoundValue_3.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_3.TabIndex = 100;
			this.txtMROISoundValue_3.Tag = "outbuilding";
			this.txtMROISoundValue_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_3.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_4
			// 
			this.chkSound_4.Location = new System.Drawing.Point(659, 202);
			this.chkSound_4.Name = "chkSound_4";
			this.chkSound_4.Size = new System.Drawing.Size(22, 22);
			this.chkSound_4.TabIndex = 109;
			// 
			// txtMROISoundValue_4
			// 
			this.txtMROISoundValue_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_4.Location = new System.Drawing.Point(727, 204);
			this.txtMROISoundValue_4.MaxLength = 9;
			this.txtMROISoundValue_4.Name = "txtMROISoundValue_4";
			this.txtMROISoundValue_4.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_4.TabIndex = 110;
			this.txtMROISoundValue_4.Tag = "outbuilding";
			this.txtMROISoundValue_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_4.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_5
			// 
			this.chkSound_5.Location = new System.Drawing.Point(659, 230);
			this.chkSound_5.Name = "chkSound_5";
			this.chkSound_5.Size = new System.Drawing.Size(22, 22);
			this.chkSound_5.TabIndex = 119;
			// 
			// txtMROISoundValue_5
			// 
			this.txtMROISoundValue_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_5.Location = new System.Drawing.Point(727, 232);
			this.txtMROISoundValue_5.MaxLength = 9;
			this.txtMROISoundValue_5.Name = "txtMROISoundValue_5";
			this.txtMROISoundValue_5.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_5.TabIndex = 120;
			this.txtMROISoundValue_5.Tag = "outbuilding";
			this.txtMROISoundValue_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_5.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_6
			// 
			this.chkSound_6.Location = new System.Drawing.Point(659, 258);
			this.chkSound_6.Name = "chkSound_6";
			this.chkSound_6.Size = new System.Drawing.Size(22, 22);
			this.chkSound_6.TabIndex = 129;
			// 
			// txtMROISoundValue_6
			// 
			this.txtMROISoundValue_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_6.Location = new System.Drawing.Point(727, 260);
			this.txtMROISoundValue_6.MaxLength = 9;
			this.txtMROISoundValue_6.Name = "txtMROISoundValue_6";
			this.txtMROISoundValue_6.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_6.TabIndex = 130;
			this.txtMROISoundValue_6.Tag = "outbuilding";
			this.txtMROISoundValue_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_6.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_7
			// 
			this.chkSound_7.Location = new System.Drawing.Point(659, 286);
			this.chkSound_7.Name = "chkSound_7";
			this.chkSound_7.Size = new System.Drawing.Size(22, 22);
			this.chkSound_7.TabIndex = 139;
			// 
			// txtMROISoundValue_7
			// 
			this.txtMROISoundValue_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_7.Location = new System.Drawing.Point(727, 288);
			this.txtMROISoundValue_7.MaxLength = 9;
			this.txtMROISoundValue_7.Name = "txtMROISoundValue_7";
			this.txtMROISoundValue_7.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_7.TabIndex = 140;
			this.txtMROISoundValue_7.Tag = "outbuilding";
			this.txtMROISoundValue_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_7.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_8
			// 
			this.chkSound_8.Location = new System.Drawing.Point(659, 314);
			this.chkSound_8.Name = "chkSound_8";
			this.chkSound_8.Size = new System.Drawing.Size(22, 22);
			this.chkSound_8.TabIndex = 149;
			// 
			// txtMROISoundValue_8
			// 
			this.txtMROISoundValue_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_8.Location = new System.Drawing.Point(727, 316);
			this.txtMROISoundValue_8.MaxLength = 9;
			this.txtMROISoundValue_8.Name = "txtMROISoundValue_8";
			this.txtMROISoundValue_8.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_8.TabIndex = 150;
			this.txtMROISoundValue_8.Tag = "outbuilding";
			this.txtMROISoundValue_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_8.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// chkSound_9
			// 
			this.chkSound_9.Location = new System.Drawing.Point(659, 342);
			this.chkSound_9.Name = "chkSound_9";
			this.chkSound_9.Size = new System.Drawing.Size(22, 22);
			this.chkSound_9.TabIndex = 159;
			// 
			// txtMROISoundValue_9
			// 
			this.txtMROISoundValue_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMROISoundValue_9.Location = new System.Drawing.Point(727, 344);
			this.txtMROISoundValue_9.MaxLength = 9;
			this.txtMROISoundValue_9.Name = "txtMROISoundValue_9";
			this.txtMROISoundValue_9.Size = new System.Drawing.Size(118, 20);
			this.txtMROISoundValue_9.TabIndex = 160;
			this.txtMROISoundValue_9.Tag = "outbuilding";
			this.txtMROISoundValue_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMROISoundValue_9.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
			this.txtMROISoundValue_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
			// 
			// lblOut
			// 
			this.lblOut.Location = new System.Drawing.Point(20, 30);
			this.lblOut.Name = "lblOut";
			this.lblOut.Size = new System.Drawing.Size(116, 28);
			this.lblOut.TabIndex = 222;
			this.lblOut.Text = "VIEW ONLY";
			this.lblOut.Visible = false;
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(32, 68);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(37, 14);
			this.Label2_0.TabIndex = 221;
			this.Label2_0.Text = "TYPE";
			// 
			// Label2_2
			// 
			this.Label2_2.Location = new System.Drawing.Point(98, 68);
			this.Label2_2.Name = "Label2_2";
			this.Label2_2.Size = new System.Drawing.Size(33, 14);
			this.Label2_2.TabIndex = 220;
			this.Label2_2.Text = "YEAR";
			// 
			// Label2_3
			// 
			this.Label2_3.Location = new System.Drawing.Point(171, 68);
			this.Label2_3.Name = "Label2_3";
			this.Label2_3.Size = new System.Drawing.Size(37, 14);
			this.Label2_3.TabIndex = 219;
			this.Label2_3.Text = "UNITS";
			// 
			// Label2_4
			// 
			this.Label2_4.Location = new System.Drawing.Point(258, 68);
			this.Label2_4.Name = "Label2_4";
			this.Label2_4.Size = new System.Drawing.Size(50, 14);
			this.Label2_4.TabIndex = 218;
			this.Label2_4.Text = "GRADE";
			// 
			// Label2_5
			// 
			this.Label2_5.Location = new System.Drawing.Point(344, 68);
			this.Label2_5.Name = "Label2_5";
			this.Label2_5.Size = new System.Drawing.Size(78, 14);
			this.Label2_5.TabIndex = 217;
			this.Label2_5.Text = "CONDITION";
			// 
			// Label2_6
			// 
			this.Label2_6.Location = new System.Drawing.Point(434, 68);
			this.Label2_6.Name = "Label2_6";
			this.Label2_6.Size = new System.Drawing.Size(84, 14);
			this.Label2_6.TabIndex = 216;
			this.Label2_6.Text = "PHYSICAL";
			// 
			// Label2_7
			// 
			this.Label2_7.Location = new System.Drawing.Point(531, 68);
			this.Label2_7.Name = "Label2_7";
			this.Label2_7.Size = new System.Drawing.Size(86, 14);
			this.Label2_7.TabIndex = 215;
			this.Label2_7.Text = "FUNCTIONAL";
			// 
			// Label2_8
			// 
			this.Label2_8.Location = new System.Drawing.Point(543, 30);
			this.Label2_8.Name = "Label2_8";
			this.Label2_8.Size = new System.Drawing.Size(60, 14);
			this.Label2_8.TabIndex = 214;
			this.Label2_8.Text = "% GOOD";
			this.Label2_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblMapLotCopy_2
			// 
			this.lblMapLotCopy_2.Location = new System.Drawing.Point(187, 30);
			this.lblMapLotCopy_2.Name = "lblMapLotCopy_2";
			this.lblMapLotCopy_2.Size = new System.Drawing.Size(203, 16);
			this.lblMapLotCopy_2.TabIndex = 213;
			this.lblMapLotCopy_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(627, 68);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.TabIndex = 212;
			this.Label2_1.Text = "SOUND VALUE";
			// 
			// SSTab1_Page6
			// 
			this.SSTab1_Page6.Controls.Add(this.txtTotalExpense);
			this.SSTab1_Page6.Controls.Add(this.txtTotalSF);
			this.SSTab1_Page6.Controls.Add(this.GridValues);
			this.SSTab1_Page6.Controls.Add(this.GridExpenses);
			this.SSTab1_Page6.Controls.Add(this.Grid1);
			this.SSTab1_Page6.Controls.Add(this.lblTotalExpense);
			this.SSTab1_Page6.Controls.Add(this.lblTotalSF);
			this.SSTab1_Page6.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page6.Name = "SSTab1_Page6";
			this.SSTab1_Page6.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page6.Text = "Income App";
			// 
			// txtTotalExpense
			// 
			this.txtTotalExpense.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalExpense.Enabled = false;
			this.txtTotalExpense.Location = new System.Drawing.Point(194, 410);
			this.txtTotalExpense.Name = "txtTotalExpense";
			this.txtTotalExpense.Size = new System.Drawing.Size(73, 40);
			this.txtTotalExpense.TabIndex = 165;
			this.txtTotalExpense.Text = "0";
			this.txtTotalExpense.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTotalSF
			// 
			this.txtTotalSF.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalSF.Enabled = false;
			this.txtTotalSF.Location = new System.Drawing.Point(20, 180);
			this.txtTotalSF.Name = "txtTotalSF";
			this.txtTotalSF.Size = new System.Drawing.Size(73, 40);
			this.txtTotalSF.TabIndex = 162;
			this.txtTotalSF.Text = "0";
			this.txtTotalSF.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// GridValues
			// 
			this.GridValues.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridValues.Cols = 3;
			this.GridValues.ColumnHeadersVisible = false;
			this.GridValues.FixedRows = 0;
			this.GridValues.Location = new System.Drawing.Point(460, 236);
			this.GridValues.Name = "GridValues";
			this.GridValues.Rows = 50;
			this.GridValues.ShowFocusCell = false;
			this.GridValues.Size = new System.Drawing.Size(6346, 155);
			this.GridValues.TabIndex = 164;
			this.GridValues.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridValues_AfterEdit);
			this.GridValues.CurrentCellChanged += new System.EventHandler(this.GridValues_RowColChange);
			// 
			// GridExpenses
			// 
			this.GridExpenses.Cols = 4;
			this.GridExpenses.ColumnHeadersHeight = 60;
			this.GridExpenses.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridExpenses.FixedCols = 0;
			this.GridExpenses.FixedRows = 2;
			this.GridExpenses.Location = new System.Drawing.Point(20, 236);
			this.GridExpenses.Name = "GridExpenses";
			this.GridExpenses.ReadOnly = false;
			this.GridExpenses.RowHeadersVisible = false;
			this.GridExpenses.Rows = 50;
			this.GridExpenses.ShowFocusCell = false;
			this.GridExpenses.Size = new System.Drawing.Size(422, 155);
			this.GridExpenses.TabIndex = 163;
			this.GridExpenses.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridExpenses_AfterEdit);
			this.GridExpenses.KeyDown += new Wisej.Web.KeyEventHandler(this.GridExpenses_KeyDownEvent);
			// 
			// Grid1
			// 
			this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid1.Cols = 10;
			this.Grid1.ColumnHeadersHeight = 60;
			this.Grid1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid1.FixedCols = 0;
			this.Grid1.FixedRows = 2;
			this.Grid1.Location = new System.Drawing.Point(20, 30);
			this.Grid1.Name = "Grid1";
			this.Grid1.ReadOnly = false;
			this.Grid1.RowHeadersVisible = false;
			this.Grid1.Rows = 50;
			this.Grid1.ShowFocusCell = false;
			this.Grid1.Size = new System.Drawing.Size(6786, 138);
			this.Grid1.TabIndex = 161;
			this.Grid1.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid1_AfterEdit);
			this.Grid1.CurrentCellChanged += new System.EventHandler(this.Grid1_RowColChange);
			this.Grid1.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid1_KeyDownEvent);
			// 
			// lblTotalExpense
			// 
			this.lblTotalExpense.Location = new System.Drawing.Point(20, 424);
			this.lblTotalExpense.Name = "lblTotalExpense";
			this.lblTotalExpense.Size = new System.Drawing.Size(105, 18);
			this.lblTotalExpense.TabIndex = 210;
			this.lblTotalExpense.Text = "TOTAL EXPENSE";
			this.lblTotalExpense.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblTotalSF
			// 
			this.lblTotalSF.Location = new System.Drawing.Point(162, 194);
			this.lblTotalSF.Name = "lblTotalSF";
			this.lblTotalSF.Size = new System.Drawing.Size(141, 20);
			this.lblTotalSF.TabIndex = 209;
			this.lblTotalSF.Text = "TOTAL SQUARE FEET";
			// 
			// SSTab1_Page7
			// 
			this.SSTab1_Page7.Controls.Add(this.lblSketchNumber);
			this.SSTab1_Page7.Controls.Add(this.lblPicName);
			this.SSTab1_Page7.Controls.Add(this.lblName2);
			this.SSTab1_Page7.Controls.Add(this.Picture1);
			this.SSTab1_Page7.Controls.Add(this.txtPicDesc);
			this.SSTab1_Page7.Controls.Add(this.cmdNextPicture);
			this.SSTab1_Page7.Controls.Add(this.cmdPrevPicture);
			this.SSTab1_Page7.Controls.Add(this.framPicture);
			this.SSTab1_Page7.Controls.Add(this.framPicDimensions);
			this.SSTab1_Page7.Controls.Add(this.Toolbar1);
			this.SSTab1_Page7.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page7.Name = "SSTab1_Page7";
			this.SSTab1_Page7.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page7.Text = "Picture";
			// 
			// lblSketchNumber
			// 
			this.lblSketchNumber.Location = new System.Drawing.Point(45, 426);
			this.lblSketchNumber.Name = "lblSketchNumber";
			this.lblSketchNumber.Size = new System.Drawing.Size(107, 26);
			this.lblSketchNumber.TabIndex = 192;
			// 
			// lblPicName
			// 
			this.lblPicName.Location = new System.Drawing.Point(20, 424);
			this.lblPicName.Name = "lblPicName";
			this.lblPicName.Size = new System.Drawing.Size(215, 42);
			this.lblPicName.TabIndex = 207;
			// 
			// lblName2
			// 
			this.lblName2.Location = new System.Drawing.Point(20, 380);
			this.lblName2.Name = "lblName2";
			this.lblName2.Size = new System.Drawing.Size(339, 14);
			this.lblName2.TabIndex = 208;
			this.lblName2.Text = "SFGSDF";
			// 
			// Picture1
			// 
			this.Picture1.FillColor = 16777215;
			this.Picture1.Image = ((System.Drawing.Image)(resources.GetObject("Picture1.Image")));
			this.Picture1.Location = new System.Drawing.Point(179, 417);
			this.Picture1.Name = "Picture1";
			this.Picture1.Size = new System.Drawing.Size(27, 18);
			this.Picture1.Visible = false;
			// 
			// txtPicDesc
			// 
			this.txtPicDesc.BackColor = System.Drawing.SystemColors.Window;
			this.txtPicDesc.Location = new System.Drawing.Point(20, 494);
			this.txtPicDesc.Name = "txtPicDesc";
			this.txtPicDesc.Size = new System.Drawing.Size(367, 40);
			this.txtPicDesc.TabIndex = 169;
			this.txtPicDesc.Text = "Enter Comment or Description Here";
			// 
			// cmdNextPicture
			// 
			this.cmdNextPicture.Location = new System.Drawing.Point(312, 424);
			this.cmdNextPicture.Name = "cmdNextPicture";
			this.cmdNextPicture.Size = new System.Drawing.Size(47, 42);
			this.cmdNextPicture.TabIndex = 168;
			this.cmdNextPicture.Click += new System.EventHandler(this.cmdNextPicture_Click);
			// 
			// cmdPrevPicture
			// 
			this.cmdPrevPicture.Location = new System.Drawing.Point(243, 424);
			this.cmdPrevPicture.Name = "cmdPrevPicture";
			this.cmdPrevPicture.Size = new System.Drawing.Size(47, 42);
			this.cmdPrevPicture.TabIndex = 167;
			this.cmdPrevPicture.Click += new System.EventHandler(this.cmdPrevPicture_Click);
			// 
			// framPicture
			// 
			this.framPicture.AppearanceKey = "groupBoxNoBorders";
			this.framPicture.Controls.Add(this.imgPicture);
			this.framPicture.Location = new System.Drawing.Point(0, 65);
			this.framPicture.Name = "framPicture";
			this.framPicture.Size = new System.Drawing.Size(593, 304);
			this.framPicture.TabIndex = 205;
			// 
			// imgPicture
			// 
			this.imgPicture.FillColor = 16777215;
			this.imgPicture.Image = ((System.Drawing.Image)(resources.GetObject("imgPicture.Image")));
			this.imgPicture.Location = new System.Drawing.Point(20, 0);
			this.imgPicture.Name = "imgPicture";
			this.imgPicture.Size = new System.Drawing.Size(541, 295);
			this.imgPicture.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgPicture.Visible = false;
			// 
			// framPicDimensions
			// 
			this.framPicDimensions.Controls.Add(this.cmbPrintSize);
			this.framPicDimensions.Location = new System.Drawing.Point(392, 380);
			this.framPicDimensions.Name = "framPicDimensions";
			this.framPicDimensions.Size = new System.Drawing.Size(265, 86);
			this.framPicDimensions.TabIndex = 206;
			this.framPicDimensions.Text = "Print Size";
			// 
			// cmbPrintSize
			// 
			this.cmbPrintSize.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPrintSize.Location = new System.Drawing.Point(20, 30);
			this.cmbPrintSize.Name = "cmbPrintSize";
			this.cmbPrintSize.Size = new System.Drawing.Size(215, 40);
			this.cmbPrintSize.TabIndex = 170;
			// 
			// Toolbar1
			// 
			this.Toolbar1.Buttons.Add(this.BTNPICADDPIC);
			this.Toolbar1.Buttons.Add(this.BTNPICDELETEPIC);
			this.Toolbar1.Buttons.Add(this.BTNPRINTPICTURE);
			this.Toolbar1.ButtonSize = new System.Drawing.Size(32, 32);
			this.Toolbar1.Location = new System.Drawing.Point(20, 20);
			this.Toolbar1.Name = "Toolbar1";
			this.Toolbar1.Size = new System.Drawing.Size(100, 36);
			this.Toolbar1.TabIndex = 166;
			this.Toolbar1.ButtonClick += new System.EventHandler<fecherFoundation.FCToolBarButtonClickEventArgs>(this.Toolbar1_ButtonClick);
			// 
			// BTNPICADDPIC
			// 
			this.BTNPICADDPIC.ImageSource = "icon-add";
			this.BTNPICADDPIC.Key = "BTNPICADDPIC";
			this.BTNPICADDPIC.Name = "BTNPICADDPIC";
			this.BTNPICADDPIC.ToolTipText = "Add picture";
			this.BTNPICADDPIC.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// BTNPICDELETEPIC
			// 
			this.BTNPICDELETEPIC.ImageSource = "icon-delete";
			this.BTNPICDELETEPIC.Key = "BTNPICDELETEPIC";
			this.BTNPICDELETEPIC.Name = "BTNPICDELETEPIC";
			this.BTNPICDELETEPIC.ToolTipText = "Delete picture from account";
			this.BTNPICDELETEPIC.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// BTNPRINTPICTURE
			// 
			this.BTNPRINTPICTURE.ImageSource = "icon=print1";
			this.BTNPRINTPICTURE.Key = "BTNPRINTPICTURE";
			this.BTNPRINTPICTURE.Name = "BTNPRINTPICTURE";
			this.BTNPRINTPICTURE.ToolTipText = "Print Preview";
			this.BTNPRINTPICTURE.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// SSTab1_Page8
			// 
			this.SSTab1_Page8.Controls.Add(this.Picture2);
			this.SSTab1_Page8.Controls.Add(this.Frame1);
			this.SSTab1_Page8.Controls.Add(this.hsSketch);
			this.SSTab1_Page8.Controls.Add(this.vsSketch);
			this.SSTab1_Page8.Controls.Add(this.cmdNextSketch);
			this.SSTab1_Page8.Controls.Add(this.cmdPrevSketch);
			this.SSTab1_Page8.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page8.Name = "SSTab1_Page8";
			this.SSTab1_Page8.Size = new System.Drawing.Size(1041, 934);
			this.SSTab1_Page8.Text = "Sketch";
			// 
			// Picture2
			// 
			this.Picture2.FillColor = 16777215;
			this.Picture2.Image = ((System.Drawing.Image)(resources.GetObject("Picture2.Image")));
			this.Picture2.Location = new System.Drawing.Point(20, 370);
			this.Picture2.Name = "Picture2";
			this.Picture2.Size = new System.Drawing.Size(52, 33);
			this.Picture2.Visible = false;
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.framSketch);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(542, 324);
			this.Frame1.TabIndex = 200;
			// 
			// framSketch
			// 
			this.framSketch.AppearanceKey = "groupBoxNoBorders";
			this.framSketch.Controls.Add(this.imSketch);
			this.framSketch.Name = "framSketch";
			this.framSketch.Size = new System.Drawing.Size(567, 330);
			this.framSketch.TabIndex = 201;
			// 
			// imSketch
			// 
			this.imSketch.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imSketch.FillColor = 16777215;
			this.imSketch.Image = ((System.Drawing.Image)(resources.GetObject("imSketch.Image")));
			this.imSketch.Location = new System.Drawing.Point(20, 30);
			this.imSketch.Name = "imSketch";
			this.imSketch.Size = new System.Drawing.Size(522, 293);
			// 
			// hsSketch
			// 
			this.hsSketch.LargeChange = 1;
			this.hsSketch.Location = new System.Drawing.Point(20, 340);
			this.hsSketch.Maximum = 32767;
			this.hsSketch.Name = "hsSketch";
			this.hsSketch.Size = new System.Drawing.Size(540, 16);
			this.hsSketch.TabIndex = 202;
			this.hsSketch.TabStop = true;
			this.hsSketch.Visible = false;
			this.hsSketch.Scroll += new Wisej.Web.ScrollEventHandler(this.hsSketch_Scroll);
			this.hsSketch.ValueChanged += new System.EventHandler(this.hsSketch_ValueChanged);
			// 
			// vsSketch
			// 
			this.vsSketch.LargeChange = 1;
			this.vsSketch.Location = new System.Drawing.Point(573, 30);
			this.vsSketch.Maximum = 32767;
			this.vsSketch.Name = "vsSketch";
			this.vsSketch.Size = new System.Drawing.Size(14, 322);
			this.vsSketch.TabIndex = 203;
			this.vsSketch.TabStop = true;
			this.vsSketch.Scroll += new Wisej.Web.ScrollEventHandler(this.vsSketch_Scroll);
			this.vsSketch.ValueChanged += new System.EventHandler(this.vsSketch_ValueChanged);
			// 
			// cmdNextSketch
			// 
			this.cmdNextSketch.Enabled = false;
			this.cmdNextSketch.Location = new System.Drawing.Point(140, 364);
			this.cmdNextSketch.Name = "cmdNextSketch";
			this.cmdNextSketch.Size = new System.Drawing.Size(50, 45);
			this.cmdNextSketch.TabIndex = 172;
			this.ToolTip1.SetToolTip(this.cmdNextSketch, "View next sketch");
			this.cmdNextSketch.Click += new System.EventHandler(this.cmdNextSketch_Click);
			// 
			// cmdPrevSketch
			// 
			this.cmdPrevSketch.Enabled = false;
			this.cmdPrevSketch.Location = new System.Drawing.Point(84, 364);
			this.cmdPrevSketch.Name = "cmdPrevSketch";
			this.cmdPrevSketch.Size = new System.Drawing.Size(50, 45);
			this.cmdPrevSketch.TabIndex = 171;
			this.ToolTip1.SetToolTip(this.cmdPrevSketch, "View previous sketch");
			this.cmdPrevSketch.Click += new System.EventHandler(this.cmdPrevSketch_Click);
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			// 
			// Label8_6
			// 
			this.Label8_6.Location = new System.Drawing.Point(322, 94);
			this.Label8_6.Name = "Label8_6";
			this.Label8_6.Size = new System.Drawing.Size(62, 16);
			this.Label8_6.TabIndex = 18;
			this.Label8_6.Text = "LOCATION";
			this.Label8_6.DoubleClick += new System.EventHandler(this.Label8_DoubleClick);
			// 
			// lblDate_2
			// 
			this.lblDate_2.Location = new System.Drawing.Point(30, 94);
			this.lblDate_2.Name = "lblDate_2";
			this.lblDate_2.Size = new System.Drawing.Size(62, 16);
			this.lblDate_2.TabIndex = 16;
			this.lblDate_2.Text = "MAP  LOT";
			// 
			// lblSketchPresent
			// 
			this.lblSketchPresent.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.lblSketchPresent.Location = new System.Drawing.Point(379, 53);
			this.lblSketchPresent.Name = "lblSketchPresent";
			this.lblSketchPresent.Size = new System.Drawing.Size(14, 16);
			this.lblSketchPresent.TabIndex = 9;
			this.lblSketchPresent.Text = "S";
			this.lblSketchPresent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblSketchPresent, "Indicates that this account has a sketch");
			this.lblSketchPresent.Visible = false;
			// 
			// lblPicturePresent
			// 
			this.lblPicturePresent.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.lblPicturePresent.Location = new System.Drawing.Point(360, 53);
			this.lblPicturePresent.Name = "lblPicturePresent";
			this.lblPicturePresent.Size = new System.Drawing.Size(14, 16);
			this.lblPicturePresent.TabIndex = 8;
			this.lblPicturePresent.Text = "P";
			this.lblPicturePresent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPicturePresent, "Indicates that this account has a picture");
			this.lblPicturePresent.Visible = false;
			// 
			// lblPendingFlag
			// 
			this.lblPendingFlag.Location = new System.Drawing.Point(379, 36);
			this.lblPendingFlag.Name = "lblPendingFlag";
			this.lblPendingFlag.Size = new System.Drawing.Size(14, 16);
			this.lblPendingFlag.TabIndex = 7;
			this.lblPendingFlag.Text = "P";
			this.lblPendingFlag.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPendingFlag, "Indicates that this account has pending activity or transfers.");
			this.lblPendingFlag.Visible = false;
			// 
			// Label65_9
			// 
			this.Label65_9.Location = new System.Drawing.Point(841, 44);
			this.Label65_9.Name = "Label65_9";
			this.Label65_9.Size = new System.Drawing.Size(114, 14);
			this.Label65_9.TabIndex = 14;
			this.Label65_9.Text = "DATE INSPECTED";
			// 
			// Label65_8
			// 
			this.Label65_8.Location = new System.Drawing.Point(419, 44);
			this.Label65_8.Name = "Label65_8";
			this.Label65_8.Size = new System.Drawing.Size(99, 14);
			this.Label65_8.TabIndex = 10;
			this.Label65_8.Text = "ENTRANCE CODE";
			// 
			// Label65_18
			// 
			this.Label65_18.Location = new System.Drawing.Point(609, 44);
			this.Label65_18.Name = "Label65_18";
			this.Label65_18.Size = new System.Drawing.Size(119, 14);
			this.Label65_18.TabIndex = 12;
			this.Label65_18.Text = "INFORMATION CODE";
			// 
			// lblNumCards
			// 
			this.lblNumCards.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.lblNumCards.Location = new System.Drawing.Point(314, 44);
			this.lblNumCards.Name = "lblNumCards";
			this.lblNumCards.Size = new System.Drawing.Size(40, 18);
			this.lblNumCards.TabIndex = 5;
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(287, 44);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(25, 18);
			this.Label11.TabIndex = 4;
			this.Label11.Text = "OF";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(150, 44);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(35, 18);
			this.Label10.TabIndex = 1;
			this.Label10.Text = "CARD";
			// 
			// lblComment
			// 
			this.lblComment.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.lblComment.Location = new System.Drawing.Point(360, 36);
			this.lblComment.Name = "lblComment";
			this.lblComment.Size = new System.Drawing.Size(14, 16);
			this.lblComment.TabIndex = 6;
			this.lblComment.Text = "C";
			this.lblComment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblComment, "Indicates that this account has a comment.");
			this.lblComment.Visible = false;
			// 
			// lblMRHLAcctNum
			// 
			this.lblMRHLAcctNum.Location = new System.Drawing.Point(30, 44);
			this.lblMRHLAcctNum.Name = "lblMRHLAcctNum";
			this.lblMRHLAcctNum.Size = new System.Drawing.Size(115, 16);
			this.lblMRHLAcctNum.TabIndex = 24;
			this.lblMRHLAcctNum.Text = "ACCOUNT";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddNewAccount,
            this.mnuAddACard,
            this.mnuDeleteAccount,
            this.mnuDeleteCard,
            this.mnuDeleteDwellingData,
            this.mnuDeleteOutbuilding,
            this.mnuSeparateCard,
            this.mnuValuationReport,
            this.mnuViewDocs,
            this.mnuOptions,
            this.mnuPrint,
            this.mnuLayout,
            this.mnuSketch,
            this.mnuPictures,
            this.mnuSavePending,
            this.mnuQuit});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuAddNewAccount
			// 
			this.mnuAddNewAccount.Index = 0;
			this.mnuAddNewAccount.Name = "mnuAddNewAccount";
			this.mnuAddNewAccount.Text = "New Account";
			this.mnuAddNewAccount.Click += new System.EventHandler(this.mnuAddNewAccount_Click);
			// 
			// mnuAddACard
			// 
			this.mnuAddACard.Index = 1;
			this.mnuAddACard.Name = "mnuAddACard";
			this.mnuAddACard.Text = "New Card";
			this.mnuAddACard.Click += new System.EventHandler(this.mnuAddACard_Click);
			// 
			// mnuDeleteAccount
			// 
			this.mnuDeleteAccount.Index = 2;
			this.mnuDeleteAccount.Name = "mnuDeleteAccount";
			this.mnuDeleteAccount.Text = "Delete Account";
			this.mnuDeleteAccount.Click += new System.EventHandler(this.mnuDeleteAccount_Click);
			// 
			// mnuDeleteCard
			// 
			this.mnuDeleteCard.Index = 3;
			this.mnuDeleteCard.Name = "mnuDeleteCard";
			this.mnuDeleteCard.Text = "Delete Card";
			this.mnuDeleteCard.Click += new System.EventHandler(this.mnuDeleteCard_Click);
			// 
			// mnuDeleteDwellingData
			// 
			this.mnuDeleteDwellingData.Index = 4;
			this.mnuDeleteDwellingData.Name = "mnuDeleteDwellingData";
			this.mnuDeleteDwellingData.Text = "Delete Dwelling / Commercial Data";
			this.mnuDeleteDwellingData.Click += new System.EventHandler(this.mnuDeleteDwellingData_Click);
			// 
			// mnuDeleteOutbuilding
			// 
			this.mnuDeleteOutbuilding.Index = 5;
			this.mnuDeleteOutbuilding.Name = "mnuDeleteOutbuilding";
			this.mnuDeleteOutbuilding.Text = "Clear Outbuilding Data";
			this.mnuDeleteOutbuilding.Click += new System.EventHandler(this.mnuDeleteOutbuilding_Click);
			// 
			// mnuSeparateCard
			// 
			this.mnuSeparateCard.Index = 6;
			this.mnuSeparateCard.Name = "mnuSeparateCard";
			this.mnuSeparateCard.Text = "Save Card as New Account";
			this.mnuSeparateCard.Click += new System.EventHandler(this.mnuSeparateCard_Click);
			// 
			// mnuValuationReport
			// 
			this.mnuValuationReport.Index = 7;
			this.mnuValuationReport.Name = "mnuValuationReport";
			this.mnuValuationReport.Text = "Valuation Report (Calculates)";
			this.mnuValuationReport.Click += new System.EventHandler(this.mnuValuationReport_Click);
			// 
			// mnuViewDocs
			// 
			this.mnuViewDocs.Index = 8;
			this.mnuViewDocs.Name = "mnuViewDocs";
			this.mnuViewDocs.Text = "View Attached Documents";
			this.mnuViewDocs.Click += new System.EventHandler(this.mnuViewDocs_Click);
			// 
			// mnuOptions
			// 
			this.mnuOptions.Index = 9;
			this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRECLComment,
            this.mnuCollectionsNote,
            this.mnuAudio,
            this.mnuGroup,
            this.mnuMortgage,
            this.mnuinterestedparties,
            this.mnuCopyProperty,
            this.mnuPendingTransfer});
			this.mnuOptions.Name = "mnuOptions";
			this.mnuOptions.Text = "Options";
			// 
			// mnuRECLComment
			// 
			this.mnuRECLComment.Index = 0;
			this.mnuRECLComment.Name = "mnuRECLComment";
			this.mnuRECLComment.Text = "Add RE / CL Comment";
			this.mnuRECLComment.Click += new System.EventHandler(this.mnuRECLComment_Click);
			// 
			// mnuCollectionsNote
			// 
			this.mnuCollectionsNote.Index = 1;
			this.mnuCollectionsNote.Name = "mnuCollectionsNote";
			this.mnuCollectionsNote.Text = "Collections Note";
			this.mnuCollectionsNote.Click += new System.EventHandler(this.mnuCollectionsNote_Click);
			// 
			// mnuAudio
			// 
			this.mnuAudio.Enabled = false;
			this.mnuAudio.Index = 2;
			this.mnuAudio.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPlayAudio,
            this.mnuPauseAudio,
            this.mnuStopAudio});
			this.mnuAudio.Name = "mnuAudio";
			this.mnuAudio.Text = "Audio Message";
			// 
			// mnuPlayAudio
			// 
			this.mnuPlayAudio.Index = 0;
			this.mnuPlayAudio.Name = "mnuPlayAudio";
			this.mnuPlayAudio.Text = "Play";
			// 
			// mnuPauseAudio
			// 
			this.mnuPauseAudio.Index = 1;
			this.mnuPauseAudio.Name = "mnuPauseAudio";
			this.mnuPauseAudio.Text = "Pause";
			// 
			// mnuStopAudio
			// 
			this.mnuStopAudio.Index = 2;
			this.mnuStopAudio.Name = "mnuStopAudio";
			this.mnuStopAudio.Text = "Stop";
			// 
			// mnuGroup
			// 
			this.mnuGroup.Index = 3;
			this.mnuGroup.Name = "mnuGroup";
			this.mnuGroup.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuGroup.Text = "View Group Information";
			this.mnuGroup.Click += new System.EventHandler(this.mnuGroup_Click);
			// 
			// mnuMortgage
			// 
			this.mnuMortgage.Index = 4;
			this.mnuMortgage.Name = "mnuMortgage";
			this.mnuMortgage.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuMortgage.Text = "View Mortgage Information";
			this.mnuMortgage.Click += new System.EventHandler(this.mnuMortgage_Click);
			// 
			// mnuinterestedparties
			// 
			this.mnuinterestedparties.Index = 5;
			this.mnuinterestedparties.Name = "mnuinterestedparties";
			this.mnuinterestedparties.Text = "Interested Parties";
			this.mnuinterestedparties.Click += new System.EventHandler(this.mnuinterestedparties_Click);
			// 
			// mnuCopyProperty
			// 
			this.mnuCopyProperty.Index = 6;
			this.mnuCopyProperty.Name = "mnuCopyProperty";
			this.mnuCopyProperty.Text = "Transfer information from another account";
			this.mnuCopyProperty.Click += new System.EventHandler(this.mnuCopyProperty_Click);
			// 
			// mnuPendingTransfer
			// 
			this.mnuPendingTransfer.Index = 7;
			this.mnuPendingTransfer.Name = "mnuPendingTransfer";
			this.mnuPendingTransfer.Text = "Create Pending Transfer";
			this.mnuPendingTransfer.Click += new System.EventHandler(this.mnuPendingTransfer_Click);
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 10;
			this.mnuPrint.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintAccountInformationSheet,
            this.mnuPrintCard,
            this.mnuPrintCards,
            this.mnuPrintPropertyCard,
            this.mnuPrintAllPropertyCards,
            this.mnuPrint1pagepropertycard,
            this.mnuPrintLabel,
            this.mnuPrintMailing,
            this.mnuPrintIncomeApproach,
            this.mnuPrintScreen,
            this.mnuPrintInterestedParties});
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			// 
			// mnuPrintAccountInformationSheet
			// 
			this.mnuPrintAccountInformationSheet.Index = 0;
			this.mnuPrintAccountInformationSheet.Name = "mnuPrintAccountInformationSheet";
			this.mnuPrintAccountInformationSheet.Text = "Print Account Information Sheet";
			this.mnuPrintAccountInformationSheet.Visible = false;
			this.mnuPrintAccountInformationSheet.Click += new System.EventHandler(this.mnuPrintAccountInformationSheet_Click);
			// 
			// mnuPrintCard
			// 
			this.mnuPrintCard.Index = 1;
			this.mnuPrintCard.Name = "mnuPrintCard";
			this.mnuPrintCard.Text = "Print Card Information";
			this.mnuPrintCard.Click += new System.EventHandler(this.mnuPrintCard_Click);
			// 
			// mnuPrintCards
			// 
			this.mnuPrintCards.Index = 2;
			this.mnuPrintCards.Name = "mnuPrintCards";
			this.mnuPrintCards.Text = "Print All Cards";
			this.mnuPrintCards.Click += new System.EventHandler(this.mnuPrintCards_Click);
			// 
			// mnuPrintPropertyCard
			// 
			this.mnuPrintPropertyCard.Index = 3;
			this.mnuPrintPropertyCard.Name = "mnuPrintPropertyCard";
			this.mnuPrintPropertyCard.Text = "Print Property Card";
			this.mnuPrintPropertyCard.Visible = false;
			this.mnuPrintPropertyCard.Click += new System.EventHandler(this.mnuPrintPropertyCard_Click);
			// 
			// mnuPrintAllPropertyCards
			// 
			this.mnuPrintAllPropertyCards.Index = 4;
			this.mnuPrintAllPropertyCards.Name = "mnuPrintAllPropertyCards";
			this.mnuPrintAllPropertyCards.Text = "Print All Property Cards";
			this.mnuPrintAllPropertyCards.Visible = false;
			this.mnuPrintAllPropertyCards.Click += new System.EventHandler(this.mnuPrintAllPropertyCards_Click);
			// 
			// mnuPrint1pagepropertycard
			// 
			this.mnuPrint1pagepropertycard.Index = 5;
			this.mnuPrint1pagepropertycard.Name = "mnuPrint1pagepropertycard";
			this.mnuPrint1pagepropertycard.Text = "Print One Page Property Card";
			this.mnuPrint1pagepropertycard.Visible = false;
			this.mnuPrint1pagepropertycard.Click += new System.EventHandler(this.mnuPrint1pagepropertycard_Click);
			// 
			// mnuPrintLabel
			// 
			this.mnuPrintLabel.Index = 6;
			this.mnuPrintLabel.Name = "mnuPrintLabel";
			this.mnuPrintLabel.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuPrintLabel.Text = "Print Label";
			this.mnuPrintLabel.Click += new System.EventHandler(this.mnuPrintLabel_Click);
			// 
			// mnuPrintMailing
			// 
			this.mnuPrintMailing.Index = 7;
			this.mnuPrintMailing.Name = "mnuPrintMailing";
			this.mnuPrintMailing.Shortcut = Wisej.Web.Shortcut.ShiftF2;
			this.mnuPrintMailing.Text = "Print Mailing Label";
			this.mnuPrintMailing.Click += new System.EventHandler(this.mnuPrintMailing_Click);
			// 
			// mnuPrintIncomeApproach
			// 
			this.mnuPrintIncomeApproach.Index = 8;
			this.mnuPrintIncomeApproach.Name = "mnuPrintIncomeApproach";
			this.mnuPrintIncomeApproach.Text = "Print Income Approach";
			this.mnuPrintIncomeApproach.Click += new System.EventHandler(this.mnuPrintIncomeApproach_Click);
			// 
			// mnuPrintScreen
			// 
			this.mnuPrintScreen.Index = 9;
			this.mnuPrintScreen.Name = "mnuPrintScreen";
			this.mnuPrintScreen.Text = "Print Screen       (Laser/Inkjet)";
			this.mnuPrintScreen.Click += new System.EventHandler(this.mnuPrintScreen_Click);
			// 
			// mnuPrintInterestedParties
			// 
			this.mnuPrintInterestedParties.Index = 10;
			this.mnuPrintInterestedParties.Name = "mnuPrintInterestedParties";
			this.mnuPrintInterestedParties.Text = "Interested Parties";
			this.mnuPrintInterestedParties.Click += new System.EventHandler(this.mnuPrintInterestedParties_Click);
			// 
			// mnuLayout
			// 
			this.mnuLayout.Index = 11;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddOcc,
            this.mnuDelOcc,
            this.mnuIncomeSepar1,
            this.mnuAddExpense,
            this.mnuDelExpense});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Income Approach";
			// 
			// mnuAddOcc
			// 
			this.mnuAddOcc.Index = 0;
			this.mnuAddOcc.Name = "mnuAddOcc";
			this.mnuAddOcc.Text = "Add Occupancy Code";
			this.mnuAddOcc.Click += new System.EventHandler(this.mnuAddOcc_Click);
			// 
			// mnuDelOcc
			// 
			this.mnuDelOcc.Index = 1;
			this.mnuDelOcc.Name = "mnuDelOcc";
			this.mnuDelOcc.Text = "Delete Occupancy Code";
			this.mnuDelOcc.Click += new System.EventHandler(this.mnuDelOcc_Click);
			// 
			// mnuIncomeSepar1
			// 
			this.mnuIncomeSepar1.Index = 2;
			this.mnuIncomeSepar1.Name = "mnuIncomeSepar1";
			this.mnuIncomeSepar1.Text = "-";
			// 
			// mnuAddExpense
			// 
			this.mnuAddExpense.Index = 3;
			this.mnuAddExpense.Name = "mnuAddExpense";
			this.mnuAddExpense.Text = "Add Expense Code";
			this.mnuAddExpense.Click += new System.EventHandler(this.mnuAddExpense_Click);
			// 
			// mnuDelExpense
			// 
			this.mnuDelExpense.Index = 4;
			this.mnuDelExpense.Name = "mnuDelExpense";
			this.mnuDelExpense.Text = "Delete Expense Code";
			this.mnuDelExpense.Click += new System.EventHandler(this.mnuDelExpense_Click);
			// 
			// mnuSketch
			// 
			this.mnuSketch.Index = 12;
			this.mnuSketch.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditSketch,
            this.mnuNewSketch,
            this.mnuEditConfig,
            this.mnuReassignSketch,
            this.mnuSketchSepar2,
            this.mnuImportSQFT,
            this.mnuSketchSepar1,
            this.mnuDeleteSketch});
			this.mnuSketch.Name = "mnuSketch";
			this.mnuSketch.Text = "Sketch";
			this.mnuSketch.Visible = false;
			// 
			// mnuEditSketch
			// 
			this.mnuEditSketch.Index = 0;
			this.mnuEditSketch.Name = "mnuEditSketch";
			this.mnuEditSketch.Text = "Edit Sketch";
			this.mnuEditSketch.Click += new System.EventHandler(this.mnuEditSketch_Click);
			// 
			// mnuNewSketch
			// 
			this.mnuNewSketch.Index = 1;
			this.mnuNewSketch.Name = "mnuNewSketch";
			this.mnuNewSketch.Text = "New Sketch";
			this.mnuNewSketch.Click += new System.EventHandler(this.mnuNewSketch_Click);
			// 
			// mnuEditConfig
			// 
			this.mnuEditConfig.Index = 2;
			this.mnuEditConfig.Name = "mnuEditConfig";
			this.mnuEditConfig.Text = "Edit Config & Phrase Files";
			this.mnuEditConfig.Click += new System.EventHandler(this.mnuEditConfig_Click);
			// 
			// mnuReassignSketch
			// 
			this.mnuReassignSketch.Index = 3;
			this.mnuReassignSketch.Name = "mnuReassignSketch";
			this.mnuReassignSketch.Text = "Re-assign Sketch File";
			this.mnuReassignSketch.Click += new System.EventHandler(this.mnuReassignSketch_Click);
			// 
			// mnuSketchSepar2
			// 
			this.mnuSketchSepar2.Index = 4;
			this.mnuSketchSepar2.Name = "mnuSketchSepar2";
			this.mnuSketchSepar2.Text = "-";
			// 
			// mnuImportSQFT
			// 
			this.mnuImportSQFT.Index = 5;
			this.mnuImportSQFT.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAll,
            this.mnuDwellingSqft,
            this.mnuOutbuildingSQFT});
			this.mnuImportSQFT.Name = "mnuImportSQFT";
			this.mnuImportSQFT.Text = "Import Square Footage from Sketch";
			this.mnuImportSQFT.Click += new System.EventHandler(this.mnuImportSQFT_Click);
			// 
			// mnuAll
			// 
			this.mnuAll.Index = 0;
			this.mnuAll.Name = "mnuAll";
			this.mnuAll.Text = "All";
			this.mnuAll.Click += new System.EventHandler(this.mnuAll_Click);
			// 
			// mnuDwellingSqft
			// 
			this.mnuDwellingSqft.Index = 1;
			this.mnuDwellingSqft.Name = "mnuDwellingSqft";
			this.mnuDwellingSqft.Text = "Dwelling";
			this.mnuDwellingSqft.Click += new System.EventHandler(this.mnuDwellingSqft_Click);
			// 
			// mnuOutbuildingSQFT
			// 
			this.mnuOutbuildingSQFT.Index = 2;
			this.mnuOutbuildingSQFT.Name = "mnuOutbuildingSQFT";
			this.mnuOutbuildingSQFT.Text = "Outbuilding";
			this.mnuOutbuildingSQFT.Click += new System.EventHandler(this.mnuOutbuildingSQFT_Click);
			// 
			// mnuSketchSepar1
			// 
			this.mnuSketchSepar1.Index = 6;
			this.mnuSketchSepar1.Name = "mnuSketchSepar1";
			this.mnuSketchSepar1.Text = "-";
			// 
			// mnuDeleteSketch
			// 
			this.mnuDeleteSketch.Index = 7;
			this.mnuDeleteSketch.Name = "mnuDeleteSketch";
			this.mnuDeleteSketch.Text = "Delete Sketch";
			this.mnuDeleteSketch.Click += new System.EventHandler(this.mnuDeleteSketch_Click);
			// 
			// mnuPictures
			// 
			this.mnuPictures.Index = 13;
			this.mnuPictures.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPicture,
            this.mnuPicSepar1,
            this.mnuImportPicture,
            this.mnuAddPicture,
            this.mnuDeletePicture,
            this.mnuSepar7,
            this.mnuPicMakePrimary,
            this.mnuPicMakeSecondary});
			this.mnuPictures.Name = "mnuPictures";
			this.mnuPictures.Text = "Pictures";
			this.mnuPictures.Visible = false;
			// 
			// mnuPrintPicture
			// 
			this.mnuPrintPicture.Index = 0;
			this.mnuPrintPicture.Name = "mnuPrintPicture";
			this.mnuPrintPicture.Text = "Print Picture";
			this.mnuPrintPicture.Click += new System.EventHandler(this.mnuPrintPicture_Click);
			// 
			// mnuPicSepar1
			// 
			this.mnuPicSepar1.Index = 1;
			this.mnuPicSepar1.Name = "mnuPicSepar1";
			this.mnuPicSepar1.Text = "-";
			// 
			// mnuImportPicture
			// 
			this.mnuImportPicture.Index = 2;
			this.mnuImportPicture.Name = "mnuImportPicture";
			this.mnuImportPicture.Text = "Import Picture";
			this.mnuImportPicture.Click += new System.EventHandler(this.mnuImportPicture_Click);
			// 
			// mnuAddPicture
			// 
			this.mnuAddPicture.Index = 3;
			this.mnuAddPicture.Name = "mnuAddPicture";
			this.mnuAddPicture.Text = "Add Picture";
			this.mnuAddPicture.Click += new System.EventHandler(this.mnuAddPicture_Click);
			// 
			// mnuDeletePicture
			// 
			this.mnuDeletePicture.Index = 4;
			this.mnuDeletePicture.Name = "mnuDeletePicture";
			this.mnuDeletePicture.Text = "Delete Picture";
			this.mnuDeletePicture.Click += new System.EventHandler(this.mnuDeletePicture_Click);
			// 
			// mnuSepar7
			// 
			this.mnuSepar7.Index = 5;
			this.mnuSepar7.Name = "mnuSepar7";
			this.mnuSepar7.Text = "-";
			// 
			// mnuPicMakePrimary
			// 
			this.mnuPicMakePrimary.Index = 6;
			this.mnuPicMakePrimary.Name = "mnuPicMakePrimary";
			this.mnuPicMakePrimary.Text = "Make Current Picture Primary";
			this.mnuPicMakePrimary.Click += new System.EventHandler(this.mnuPicMakePrimary_Click);
			// 
			// mnuPicMakeSecondary
			// 
			this.mnuPicMakeSecondary.Index = 7;
			this.mnuPicMakeSecondary.Name = "mnuPicMakeSecondary";
			this.mnuPicMakeSecondary.Text = "Make Current Picture Secondary";
			this.mnuPicMakeSecondary.Click += new System.EventHandler(this.mnuPicMakeSecondary_Click);
			// 
			// mnuSavePending
			// 
			this.mnuSavePending.Enabled = false;
			this.mnuSavePending.Index = 14;
			this.mnuSavePending.Name = "mnuSavePending";
			this.mnuSavePending.Text = "Save Pending Changes";
			this.mnuSavePending.Visible = false;
			this.mnuSavePending.Click += new System.EventHandler(this.mnuSavePending_Click);
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 15;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// mnuSearch
			// 
			this.mnuSearch.Index = -1;
			this.mnuSearch.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSearchName,
            this.mnuSearchLocation,
            this.mnuSearchMapLot});
			this.mnuSearch.Name = "mnuSearch";
			this.mnuSearch.Text = "Search";
			// 
			// mnuSearchName
			// 
			this.mnuSearchName.Enabled = false;
			this.mnuSearchName.Index = 0;
			this.mnuSearchName.Name = "mnuSearchName";
			this.mnuSearchName.Text = "Search by Name";
			this.mnuSearchName.Visible = false;
			// 
			// mnuSearchLocation
			// 
			this.mnuSearchLocation.Enabled = false;
			this.mnuSearchLocation.Index = 1;
			this.mnuSearchLocation.Name = "mnuSearchLocation";
			this.mnuSearchLocation.Text = "Search by Location";
			this.mnuSearchLocation.Visible = false;
			// 
			// mnuSearchMapLot
			// 
			this.mnuSearchMapLot.Enabled = false;
			this.mnuSearchMapLot.Index = 2;
			this.mnuSearchMapLot.Name = "mnuSearchMapLot";
			this.mnuSearchMapLot.Text = "Search by Map/Lot";
			this.mnuSearchMapLot.Visible = false;
			// 
			// mnuPreviousAccountFromSearch
			// 
			this.mnuPreviousAccountFromSearch.Enabled = false;
			this.mnuPreviousAccountFromSearch.Index = -1;
			this.mnuPreviousAccountFromSearch.Name = "mnuPreviousAccountFromSearch";
			this.mnuPreviousAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuPreviousAccountFromSearch.Text = "Previous Account from Search";
			this.mnuPreviousAccountFromSearch.Click += new System.EventHandler(this.mnuPreviousAccountFromSearch_Click);
			// 
			// mnuNextAccountFromSearch
			// 
			this.mnuNextAccountFromSearch.Enabled = false;
			this.mnuNextAccountFromSearch.Index = -1;
			this.mnuNextAccountFromSearch.Name = "mnuNextAccountFromSearch";
			this.mnuNextAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuNextAccountFromSearch.Text = "Next Account from Search";
			this.mnuNextAccountFromSearch.Click += new System.EventHandler(this.mnuNextAccountFromSearch_Click);
			// 
			// mnuCalculate
			// 
			this.mnuCalculate.Index = -1;
			this.mnuCalculate.Name = "mnuCalculate";
			this.mnuCalculate.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuCalculate.Text = "Calculate this Account";
			this.mnuCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
			// 
			// mnuComment
			// 
			this.mnuComment.Index = -1;
			this.mnuComment.Name = "mnuComment";
			this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuComment.Text = "Comment";
			this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// mnuSep
			// 
			this.mnuSep.Index = -1;
			this.mnuSep.Name = "mnuSep";
			this.mnuSep.Text = "-";
			// 
			// Separator1
			// 
			this.Separator1.Index = -1;
			this.Separator1.Name = "Separator1";
			this.Separator1.Text = "-";
			// 
			// mnuSeparator2
			// 
			this.mnuSeparator2.Index = -1;
			this.mnuSeparator2.Name = "mnuSeparator2";
			this.mnuSeparator2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = -1;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveQuit
			// 
			this.mnuSaveQuit.Index = -1;
			this.mnuSaveQuit.Name = "mnuSaveQuit";
			this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveQuit.Text = "Save & Exit";
			this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// mnuSeparator4
			// 
			this.mnuSeparator4.Index = -1;
			this.mnuSeparator4.Name = "mnuSeparator4";
			this.mnuSeparator4.Text = "-";
			this.mnuSeparator4.Visible = false;
			// 
			// mnuSeparator3
			// 
			this.mnuSeparator3.Index = -1;
			this.mnuSeparator3.Name = "mnuSeparator3";
			this.mnuSeparator3.Text = "-";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(474, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// cmdCalculate
			// 
			this.cmdCalculate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdCalculate.Location = new System.Drawing.Point(414, 29);
			this.cmdCalculate.Name = "cmdCalculate";
			this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdCalculate.Size = new System.Drawing.Size(154, 24);
			this.cmdCalculate.TabIndex = 1;
			this.cmdCalculate.Text = "Calculate this Account";
			this.cmdCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
			// 
			// cmdComment
			// 
			this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdComment.Location = new System.Drawing.Point(574, 29);
			this.cmdComment.Name = "cmdComment";
			this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdComment.Size = new System.Drawing.Size(78, 24);
			this.cmdComment.TabIndex = 2;
			this.cmdComment.Text = "Comment";
			this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// cmdPreviousAccountFromSearch
			// 
			this.cmdPreviousAccountFromSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPreviousAccountFromSearch.Enabled = false;
			this.cmdPreviousAccountFromSearch.Location = new System.Drawing.Point(658, 29);
			this.cmdPreviousAccountFromSearch.Name = "cmdPreviousAccountFromSearch";
			this.cmdPreviousAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdPreviousAccountFromSearch.Size = new System.Drawing.Size(202, 24);
			this.cmdPreviousAccountFromSearch.TabIndex = 3;
			this.cmdPreviousAccountFromSearch.Text = "Previous Account from Search";
			this.cmdPreviousAccountFromSearch.Click += new System.EventHandler(this.mnuPreviousAccountFromSearch_Click);
			// 
			// cmdNextAccountFromSearch
			// 
			this.cmdNextAccountFromSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNextAccountFromSearch.Enabled = false;
			this.cmdNextAccountFromSearch.Location = new System.Drawing.Point(866, 29);
			this.cmdNextAccountFromSearch.Name = "cmdNextAccountFromSearch";
			this.cmdNextAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F8;
			this.cmdNextAccountFromSearch.Size = new System.Drawing.Size(179, 24);
			this.cmdNextAccountFromSearch.TabIndex = 4;
			this.cmdNextAccountFromSearch.Text = "Next Account from Search";
			this.cmdNextAccountFromSearch.Click += new System.EventHandler(this.mnuNextAccountFromSearch_Click);
			// 
			// frmREProperty
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1073, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmREProperty";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Account Maintenance";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmREProperty_Load);
			this.Activated += new System.EventHandler(this.frmREProperty_Activated);
			this.Resize += new System.EventHandler(this.frmREProperty_Resize);
			this.MouseMove += new Wisej.Web.MouseEventHandler(this.frmREProperty_MouseMove);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmREProperty_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmREProperty_KeyPress);
			this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmREProperty_KeyUp);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridToolTip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDateInspected)).EndInit();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLogSale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRLivingTrust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExempts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BPGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkZoneOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridLandCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridBldgCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSecZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTopography)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridUtilities)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLand)).EndInit();
			this.SSTab1_Page3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDwelling1)).EndInit();
			this.SSTab1_Page4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.SSTab1_Page5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSound_9)).EndInit();
			this.SSTab1_Page6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridValues)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExpenses)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
			this.SSTab1_Page7.ResumeLayout(false);
			this.SSTab1_Page7.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNextPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framPicture)).EndInit();
			this.framPicture.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framPicDimensions)).EndInit();
			this.framPicDimensions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).EndInit();
			this.SSTab1_Page8.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.framSketch)).EndInit();
			this.framSketch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imSketch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNextSketch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevSketch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreviousAccountFromSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNextAccountFromSearch)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdCalculate;
		private FCButton cmdComment;
		private FCButton cmdPreviousAccountFromSearch;
		private FCButton cmdNextAccountFromSearch;
	}
}