﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for dlgTaxNotice.
	/// </summary>
	partial class dlgTaxNotice : BaseForm
	{
		public fecherFoundation.FCComboBox cmbValues;
		public fecherFoundation.FCLabel lblValues;
		public fecherFoundation.FCComboBox cmbAddress;
		public fecherFoundation.FCComboBox cmbtPerPage;
		public fecherFoundation.FCComboBox cmbaccounts;
		public fecherFoundation.FCLabel lblaccounts;
		public fecherFoundation.FCComboBox cmbFormat;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtRate;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkInclude_3;
		public fecherFoundation.FCCheckBox chkInclude_2;
		public fecherFoundation.FCCheckBox chkInclude_1;
		public fecherFoundation.FCCheckBox chkInclude_0;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgTaxNotice));
			this.cmbValues = new fecherFoundation.FCComboBox();
			this.lblValues = new fecherFoundation.FCLabel();
			this.cmbAddress = new fecherFoundation.FCComboBox();
			this.cmbtPerPage = new fecherFoundation.FCComboBox();
			this.cmbaccounts = new fecherFoundation.FCComboBox();
			this.lblaccounts = new fecherFoundation.FCLabel();
			this.cmbFormat = new fecherFoundation.FCComboBox();
			this.lblFormat = new fecherFoundation.FCLabel();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtRate = new fecherFoundation.FCTextBox();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.chkInclude_3 = new fecherFoundation.FCCheckBox();
			this.chkInclude_2 = new fecherFoundation.FCCheckBox();
			this.chkInclude_1 = new fecherFoundation.FCCheckBox();
			this.chkInclude_0 = new fecherFoundation.FCCheckBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(477, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbValues);
			this.ClientArea.Controls.Add(this.lblValues);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.txtRate);
			this.ClientArea.Controls.Add(this.txtYear);
			this.ClientArea.Controls.Add(this.cmbaccounts);
			this.ClientArea.Controls.Add(this.lblaccounts);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbFormat);
			this.ClientArea.Controls.Add(this.lblFormat);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(477, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(477, 60);
			this.TopPanel.Text = "Tax Notice";
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(129, 30);
			this.HeaderText.Text = "Tax Notice";
			// 
			// cmbValues
			// 
			this.cmbValues.AutoSize = false;
			this.cmbValues.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbValues.FormattingEnabled = true;
			this.cmbValues.Items.AddRange(new object[] {
				"Billing",
				"Current"
			});
			this.cmbValues.Location = new System.Drawing.Point(163, 150);
			this.cmbValues.Name = "cmbValues";
			this.cmbValues.Size = new System.Drawing.Size(286, 40);
			this.cmbValues.TabIndex = 0;
			this.cmbValues.Text = "Billing";
			// 
			// lblValues
			// 
			this.lblValues.AutoSize = true;
			this.lblValues.Location = new System.Drawing.Point(30, 164);
			this.lblValues.Name = "lblValues";
			this.lblValues.Size = new System.Drawing.Size(56, 15);
			this.lblValues.TabIndex = 1;
			this.lblValues.Text = "VALUES";
			// 
			// cmbAddress
			// 
			this.cmbAddress.AutoSize = false;
			this.cmbAddress.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAddress.Enabled = false;
			this.cmbAddress.FormattingEnabled = true;
			this.cmbAddress.Items.AddRange(new object[] {
				"Align address for envelope",
				"Eliminate white space"
			});
			this.cmbAddress.Location = new System.Drawing.Point(20, 90);
			this.cmbAddress.Name = "cmbAddress";
			this.cmbAddress.Size = new System.Drawing.Size(300, 40);
			this.cmbAddress.TabIndex = 2;
			this.cmbAddress.Text = "Align address for envelope";
			// 
			// cmbtPerPage
			// 
			this.cmbtPerPage.AutoSize = false;
			this.cmbtPerPage.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbtPerPage.FormattingEnabled = true;
			this.cmbtPerPage.Items.AddRange(new object[] {
				"1",
				"2",
				"3"
			});
			this.cmbtPerPage.Location = new System.Drawing.Point(20, 30);
			this.cmbtPerPage.Name = "cmbtPerPage";
			this.cmbtPerPage.Size = new System.Drawing.Size(300, 40);
			this.cmbtPerPage.TabIndex = 0;
			this.cmbtPerPage.Text = "3";
			this.cmbtPerPage.SelectedIndexChanged += new System.EventHandler(this.cmbtPerPage_SelectedIndexChanged);
			// 
			// cmbaccounts
			// 
			this.cmbaccounts.AutoSize = false;
			this.cmbaccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbaccounts.FormattingEnabled = true;
			this.cmbaccounts.Items.AddRange(new object[] {
				"All",
				"Range",
				"Specific Accounts",
				"From Extract for Listings"
			});
			this.cmbaccounts.Location = new System.Drawing.Point(163, 90);
			this.cmbaccounts.Name = "cmbaccounts";
			this.cmbaccounts.Size = new System.Drawing.Size(286, 40);
			this.cmbaccounts.TabIndex = 23;
			this.cmbaccounts.Text = "All";
			// 
			// lblaccounts
			// 
			this.lblaccounts.AutoSize = true;
			this.lblaccounts.Location = new System.Drawing.Point(30, 104);
			this.lblaccounts.Name = "lblaccounts";
			this.lblaccounts.Size = new System.Drawing.Size(77, 15);
			this.lblaccounts.TabIndex = 24;
			this.lblaccounts.Text = "ACCOUNTS";
			// 
			// cmbFormat
			// 
			this.cmbFormat.AutoSize = false;
			this.cmbFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormat.FormattingEnabled = true;
			this.cmbFormat.Items.AddRange(new object[] {
				"Free Form",
				"Change of Assessment"
			});
			this.cmbFormat.Location = new System.Drawing.Point(163, 30);
			this.cmbFormat.Name = "cmbFormat";
			this.cmbFormat.Size = new System.Drawing.Size(286, 40);
			this.cmbFormat.TabIndex = 25;
			this.cmbFormat.Text = "Change of Assessment";
			this.cmbFormat.SelectedIndexChanged += new System.EventHandler(this.cmbFormat_SelectedIndexChanged);
			// 
			// lblFormat
			// 
			this.lblFormat.AutoSize = true;
			this.lblFormat.Location = new System.Drawing.Point(30, 44);
			this.lblFormat.Name = "lblFormat";
			this.lblFormat.Size = new System.Drawing.Size(60, 15);
			this.lblFormat.TabIndex = 26;
			this.lblFormat.Text = "FORMAT";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.cmbtPerPage);
			this.Frame4.Controls.Add(this.cmbAddress);
			this.Frame4.Location = new System.Drawing.Point(30, 568);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(340, 150);
			this.Frame4.TabIndex = 22;
			this.Frame4.Text = "Notices Per Page";
			// 
			// txtRate
			// 
			this.txtRate.AutoSize = false;
			this.txtRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtRate.LinkItem = null;
			this.txtRate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRate.LinkTopic = null;
			this.txtRate.Location = new System.Drawing.Point(158, 508);
			this.txtRate.Name = "txtRate";
			this.txtRate.Size = new System.Drawing.Size(120, 40);
			this.txtRate.TabIndex = 13;
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(158, 448);
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(120, 40);
			this.txtYear.TabIndex = 12;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.chkInclude_3);
			this.Frame2.Controls.Add(this.chkInclude_2);
			this.Frame2.Controls.Add(this.chkInclude_1);
			this.Frame2.Controls.Add(this.chkInclude_0);
			this.Frame2.Location = new System.Drawing.Point(30, 210);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(248, 218);
			this.Frame2.TabIndex = 18;
			this.Frame2.Text = "Include";
			// 
			// chkInclude_3
			// 
			this.chkInclude_3.Checked = true;
			this.chkInclude_3.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_3.Location = new System.Drawing.Point(20, 171);
			this.chkInclude_3.Name = "chkInclude_3";
			this.chkInclude_3.Size = new System.Drawing.Size(136, 27);
			this.chkInclude_3.TabIndex = 11;
			this.chkInclude_3.Text = "Existing Notice";
			this.chkInclude_3.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
			// 
			// chkInclude_2
			// 
			this.chkInclude_2.Checked = true;
			this.chkInclude_2.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_2.Location = new System.Drawing.Point(20, 124);
			this.chkInclude_2.Name = "chkInclude_2";
			this.chkInclude_2.Size = new System.Drawing.Size(174, 27);
			this.chkInclude_2.TabIndex = 10;
			this.chkInclude_2.Text = "Exemption Amounts";
			this.chkInclude_2.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
			// 
			// chkInclude_1
			// 
			this.chkInclude_1.Checked = true;
			this.chkInclude_1.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_1.Location = new System.Drawing.Point(20, 77);
			this.chkInclude_1.Name = "chkInclude_1";
			this.chkInclude_1.Size = new System.Drawing.Size(132, 27);
			this.chkInclude_1.TabIndex = 9;
			this.chkInclude_1.Text = "Estimated Tax";
			this.chkInclude_1.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
			// 
			// chkInclude_0
			// 
			this.chkInclude_0.Checked = true;
			this.chkInclude_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_0.Location = new System.Drawing.Point(20, 30);
			this.chkInclude_0.Name = "chkInclude_0";
			this.chkInclude_0.Size = new System.Drawing.Size(208, 27);
			this.chkInclude_0.TabIndex = 8;
			this.chkInclude_0.Text = "Totally Exempt Accounts";
			this.chkInclude_0.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 522);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(62, 20);
			this.Label2.TabIndex = 21;
			this.Label2.Text = "TAX RATE";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 462);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(64, 20);
			this.Label1.TabIndex = 20;
			this.Label1.Text = "TAX YEAR";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(340, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(100, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// dlgTaxNotice
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(477, 688);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "dlgTaxNotice";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Tax Notice";
			this.Load += new System.EventHandler(this.dlgTaxNotice_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.dlgTaxNotice_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
