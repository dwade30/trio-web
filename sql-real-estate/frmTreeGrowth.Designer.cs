//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTreeGrowth.
	/// </summary>
	partial class frmTreeGrowth : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOption;
		public fecherFoundation.FCLabel lblOption;
		public fecherFoundation.FCFrame framGrid;
		public FCGrid Grid;
		public FCGrid TotalGrid;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblCertifiedRatio;
		public fecherFoundation.FCLabel lblHardValue;
		public fecherFoundation.FCLabel lblMixedValue;
		public fecherFoundation.FCLabel lblSoftValue;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label4;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuCalculate;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTreeGrowth));
			this.cmbOption = new fecherFoundation.FCComboBox();
			this.lblOption = new fecherFoundation.FCLabel();
			this.framGrid = new fecherFoundation.FCFrame();
			this.Grid = new fecherFoundation.FCGrid();
			this.TotalGrid = new fecherFoundation.FCGrid();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Label6 = new fecherFoundation.FCLabel();
			this.lblCertifiedRatio = new fecherFoundation.FCLabel();
			this.lblHardValue = new fecherFoundation.FCLabel();
			this.lblMixedValue = new fecherFoundation.FCLabel();
			this.lblSoftValue = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCalculate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdCalculate = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framGrid)).BeginInit();
			this.framGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdCalculate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 740);
			this.BottomPanel.Size = new System.Drawing.Size(999, 108);
			this.BottomPanel.Click += new System.EventHandler(this.mnuCalculate_Click);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framGrid);
			this.ClientArea.Controls.Add(this.cmbOption);
			this.ClientArea.Controls.Add(this.lblOption);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Size = new System.Drawing.Size(999, 680);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(999, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(258, 30);
			this.HeaderText.Text = "Calculate Tree Growth";
			// 
			// cmbOption
			// 
			this.cmbOption.AutoSize = false;
			this.cmbOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOption.FormattingEnabled = true;
			this.cmbOption.Items.AddRange(new object[] {
				"Detailed report with summary totals",
				"Summary report only"
			});
			this.cmbOption.Location = new System.Drawing.Point(450, 100);
			this.cmbOption.Name = "cmbOption";
			this.cmbOption.Size = new System.Drawing.Size(519, 40);
			this.cmbOption.TabIndex = 3;
			this.cmbOption.Text = "Detailed report with summary totals";
			// 
			// lblOption
			// 
			this.lblOption.AutoSize = true;
			this.lblOption.Location = new System.Drawing.Point(335, 114);
			this.lblOption.Name = "lblOption";
			this.lblOption.Size = new System.Drawing.Size(55, 15);
			this.lblOption.TabIndex = 2;
			this.lblOption.Text = "OPTION";
			// 
			// framGrid
			// 
			this.framGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.framGrid.AppearanceKey = "groupBoxNoBorders";
			this.framGrid.Controls.Add(this.Grid);
			this.framGrid.Controls.Add(this.TotalGrid);
			this.framGrid.Location = new System.Drawing.Point(0, 263);
			this.framGrid.Name = "framGrid";
			this.framGrid.Size = new System.Drawing.Size(983, 400);
			this.framGrid.TabIndex = 4;
			this.framGrid.Visible = false;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 1);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(948, 270);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			// 
			// TotalGrid
			// 
			this.TotalGrid.AllowSelection = false;
			this.TotalGrid.AllowUserToResizeColumns = false;
			this.TotalGrid.AllowUserToResizeRows = false;
			this.TotalGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.TotalGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.TotalGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.TotalGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.TotalGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.TotalGrid.BackColorSel = System.Drawing.Color.Empty;
			this.TotalGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.TotalGrid.Cols = 8;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.TotalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.TotalGrid.ColumnHeadersHeight = 30;
			this.TotalGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.TotalGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.TotalGrid.DragIcon = null;
			this.TotalGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.TotalGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.TotalGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.TotalGrid.FrozenCols = 0;
			this.TotalGrid.GridColor = System.Drawing.Color.Empty;
			this.TotalGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.TotalGrid.Location = new System.Drawing.Point(30, 288);
			this.TotalGrid.Name = "TotalGrid";
			this.TotalGrid.ReadOnly = true;
			this.TotalGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.TotalGrid.RowHeightMin = 0;
			this.TotalGrid.Rows = 2;
			this.TotalGrid.ScrollTipText = null;
			this.TotalGrid.ShowColumnVisibilityMenu = false;
			this.TotalGrid.Size = new System.Drawing.Size(948, 106);
			this.TotalGrid.StandardTab = true;
			this.TotalGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.TotalGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.TotalGrid.TabIndex = 1;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(922, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(49, 24);
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.Label6);
			this.Frame1.Controls.Add(this.lblCertifiedRatio);
			this.Frame1.Controls.Add(this.lblHardValue);
			this.Frame1.Controls.Add(this.lblMixedValue);
			this.Frame1.Controls.Add(this.lblSoftValue);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(258, 212);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Per Acre Value";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 180);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(119, 19);
			this.Label6.TabIndex = 6;
			this.Label6.Text = "CERTIFIED RATIO";
			// 
			// lblCertifiedRatio
			// 
			this.lblCertifiedRatio.Location = new System.Drawing.Point(155, 180);
			this.lblCertifiedRatio.Name = "lblCertifiedRatio";
			this.lblCertifiedRatio.Size = new System.Drawing.Size(67, 19);
			this.lblCertifiedRatio.TabIndex = 7;
			this.lblCertifiedRatio.Text = "100";
			this.lblCertifiedRatio.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblHardValue
			// 
			this.lblHardValue.Location = new System.Drawing.Point(155, 130);
			this.lblHardValue.Name = "lblHardValue";
			this.lblHardValue.Size = new System.Drawing.Size(66, 20);
			this.lblHardValue.TabIndex = 5;
			this.lblHardValue.Text = "0.00";
			this.lblHardValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblMixedValue
			// 
			this.lblMixedValue.Location = new System.Drawing.Point(155, 80);
			this.lblMixedValue.Name = "lblMixedValue";
			this.lblMixedValue.Size = new System.Drawing.Size(66, 20);
			this.lblMixedValue.TabIndex = 3;
			this.lblMixedValue.Text = "0.00";
			this.lblMixedValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSoftValue
			// 
			this.lblSoftValue.Location = new System.Drawing.Point(155, 30);
			this.lblSoftValue.Name = "lblSoftValue";
			this.lblSoftValue.Size = new System.Drawing.Size(67, 19);
			this.lblSoftValue.TabIndex = 1;
			this.lblSoftValue.Text = "0.00";
			this.lblSoftValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 130);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(92, 20);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "HARDWOOD";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 80);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(104, 20);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "MIXED WOOD";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(84, 19);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "SOFTWOOD";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(335, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(643, 51);
			this.Label4.TabIndex = 1;
			this.Label4.Text = resources.GetString("Label4.Text");
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuCalculate,
				this.mnuPrint,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuCalculate
			// 
			this.mnuCalculate.Index = 0;
			this.mnuCalculate.Name = "mnuCalculate";
			this.mnuCalculate.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuCalculate.Text = "Calculate";
			this.mnuCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
			// 
			// mnuPrint
			// 
			this.mnuPrint.Enabled = false;
			this.mnuPrint.Index = 1;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 2;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 3;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdCalculate
			// 
			this.cmdCalculate.AppearanceKey = "acceptButton";
			this.cmdCalculate.Location = new System.Drawing.Point(394, 30);
			this.cmdCalculate.Name = "cmdCalculate";
			this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCalculate.Size = new System.Drawing.Size(122, 48);
			this.cmdCalculate.TabIndex = 0;
			this.cmdCalculate.Text = "Calculate";
			this.cmdCalculate.Click += new System.EventHandler(this.cmdCalculate_Click);
			// 
			// frmTreeGrowth
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(999, 848);
			this.FillColor = 0;
			this.Name = "frmTreeGrowth";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Calculate Tree Growth";
			this.Load += new System.EventHandler(this.frmTreeGrowth_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTreeGrowth_KeyDown);
			this.Resize += new System.EventHandler(this.frmTreeGrowth_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framGrid)).EndInit();
			this.framGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdCalculate;
		private FCButton cmdPrint;
	}
}