﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPendingXfers.
	/// </summary>
	partial class rptPendingXfers
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPendingXfers));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMinDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtXferTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTransaction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSecondOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewSecondOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSalePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleFinance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleValidity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleVerified = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMinDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXferTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransaction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewSecondOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleFinance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleValidity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleVerified)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.txtAccount,
				this.Label3,
				this.txtMinDate,
				this.Label4,
				this.txtMapLot,
				this.Label5,
				this.txtTransfer,
				this.Label6,
				this.txtXferTo,
				this.Label7,
				this.txtTransaction,
				this.Label8,
				this.txtName,
				this.Label9,
				this.txtSecondOwner,
				this.Label10,
				this.txtAddress1,
				this.Label11,
				this.txtAddress2,
				this.Label12,
				this.txtCity,
				this.Label13,
				this.txtState,
				this.Label14,
				this.txtZip,
				this.Label15,
				this.txtNewName,
				this.Label16,
				this.txtNewSecondOwner,
				this.Label17,
				this.txtNewAddress1,
				this.Label18,
				this.txtNewAddress2,
				this.Label19,
				this.txtNewCity,
				this.Label20,
				this.txtNewState,
				this.Label21,
				this.txtNewZip,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.txtSalePrice,
				this.Label26,
				this.txtSaleDate,
				this.Label27,
				this.txtSaleFinance,
				this.Label28,
				this.txtSaleType,
				this.Label29,
				this.txtSaleValidity,
				this.Label30,
				this.txtSaleVerified,
				this.Line1
			});
			this.Detail.Height = 1.875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label1
			});
			this.PageHeader.Height = 0.5729167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Pending Transfers";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Account";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.6875F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.6875F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.5F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: center";
			this.Label3.Text = "Min Date";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.9375F;
			// 
			// txtMinDate
			// 
			this.txtMinDate.Height = 0.1875F;
			this.txtMinDate.Left = 5.5F;
			this.txtMinDate.Name = "txtMinDate";
			this.txtMinDate.Style = "text-align: center";
			this.txtMinDate.Text = null;
			this.txtMinDate.Top = 0.1875F;
			this.txtMinDate.Width = 0.9375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.25F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Map Lot";
			this.Label4.Top = 0F;
			this.Label4.Width = 0.6875F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 1.9375F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.8125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.8125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: center";
			this.Label5.Text = "Transfer";
			this.Label5.Top = 0F;
			this.Label5.Width = 0.8125F;
			// 
			// txtTransfer
			// 
			this.txtTransfer.Height = 0.1875F;
			this.txtTransfer.Left = 3.8125F;
			this.txtTransfer.Name = "txtTransfer";
			this.txtTransfer.Style = "text-align: center";
			this.txtTransfer.Text = null;
			this.txtTransfer.Top = 0.1875F;
			this.txtTransfer.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.6875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: center";
			this.Label6.Text = "Xfer To";
			this.Label6.Top = 0F;
			this.Label6.Width = 0.75F;
			// 
			// txtXferTo
			// 
			this.txtXferTo.Height = 0.1875F;
			this.txtXferTo.Left = 4.6875F;
			this.txtXferTo.Name = "txtXferTo";
			this.txtXferTo.Style = "text-align: center";
			this.txtXferTo.Text = null;
			this.txtXferTo.Top = 0.1875F;
			this.txtXferTo.Width = 0.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.5F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: center";
			this.Label7.Text = "Transaction";
			this.Label7.Top = 0F;
			this.Label7.Width = 0.9375F;
			// 
			// txtTransaction
			// 
			this.txtTransaction.Height = 0.1875F;
			this.txtTransaction.Left = 6.5F;
			this.txtTransaction.Name = "txtTransaction";
			this.txtTransaction.Style = "text-align: center";
			this.txtTransaction.Text = null;
			this.txtTransaction.Top = 0.1875F;
			this.txtTransaction.Width = 0.9375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Name";
			this.Label8.Top = 0.625F;
			this.Label8.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.875F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0.625F;
			this.txtName.Width = 1.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "2nd Owner";
			this.Label9.Top = 0.8125F;
			this.Label9.Width = 0.875F;
			// 
			// txtSecondOwner
			// 
			this.txtSecondOwner.Height = 0.1875F;
			this.txtSecondOwner.Left = 0.875F;
			this.txtSecondOwner.Name = "txtSecondOwner";
			this.txtSecondOwner.Text = null;
			this.txtSecondOwner.Top = 0.8125F;
			this.txtSecondOwner.Width = 1.875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Address 1";
			this.Label10.Top = 1F;
			this.Label10.Width = 0.875F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.1875F;
			this.txtAddress1.Left = 0.875F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 1F;
			this.txtAddress1.Width = 1.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "Address 2";
			this.Label11.Top = 1.1875F;
			this.Label11.Width = 0.875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1875F;
			this.txtAddress2.Left = 0.875F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 1.1875F;
			this.txtAddress2.Width = 1.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Text = "City";
			this.Label12.Top = 1.375F;
			this.Label12.Width = 0.6875F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1875F;
			this.txtCity.Left = 0.875F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Text = null;
			this.txtCity.Top = 1.375F;
			this.txtCity.Width = 1.875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold";
			this.Label13.Text = "State";
			this.Label13.Top = 1.5625F;
			this.Label13.Width = 0.6875F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1875F;
			this.txtState.Left = 0.875F;
			this.txtState.Name = "txtState";
			this.txtState.Text = null;
			this.txtState.Top = 1.5625F;
			this.txtState.Width = 0.3125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 1.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold";
			this.Label14.Text = "Zip";
			this.Label14.Top = 1.5625F;
			this.Label14.Width = 0.375F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.1875F;
			this.txtZip.Left = 1.625F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Text = null;
			this.txtZip.Top = 1.5625F;
			this.txtZip.Width = 1.125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.8125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold";
			this.Label15.Text = "Name";
			this.Label15.Top = 0.625F;
			this.Label15.Width = 0.6875F;
			// 
			// txtNewName
			// 
			this.txtNewName.Height = 0.1875F;
			this.txtNewName.Left = 3.6875F;
			this.txtNewName.Name = "txtNewName";
			this.txtNewName.Text = null;
			this.txtNewName.Top = 0.625F;
			this.txtNewName.Width = 1.875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.8125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold";
			this.Label16.Text = "2nd Owner";
			this.Label16.Top = 0.8125F;
			this.Label16.Width = 0.875F;
			// 
			// txtNewSecondOwner
			// 
			this.txtNewSecondOwner.Height = 0.1875F;
			this.txtNewSecondOwner.Left = 3.6875F;
			this.txtNewSecondOwner.Name = "txtNewSecondOwner";
			this.txtNewSecondOwner.Text = null;
			this.txtNewSecondOwner.Top = 0.8125F;
			this.txtNewSecondOwner.Width = 1.875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.8125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold";
			this.Label17.Text = "Address 1";
			this.Label17.Top = 1F;
			this.Label17.Width = 0.875F;
			// 
			// txtNewAddress1
			// 
			this.txtNewAddress1.Height = 0.1875F;
			this.txtNewAddress1.Left = 3.6875F;
			this.txtNewAddress1.Name = "txtNewAddress1";
			this.txtNewAddress1.Text = null;
			this.txtNewAddress1.Top = 1F;
			this.txtNewAddress1.Width = 1.875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.8125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold";
			this.Label18.Text = "Address 2";
			this.Label18.Top = 1.1875F;
			this.Label18.Width = 0.875F;
			// 
			// txtNewAddress2
			// 
			this.txtNewAddress2.Height = 0.1875F;
			this.txtNewAddress2.Left = 3.6875F;
			this.txtNewAddress2.Name = "txtNewAddress2";
			this.txtNewAddress2.Text = null;
			this.txtNewAddress2.Top = 1.1875F;
			this.txtNewAddress2.Width = 1.875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.8125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Text = "City";
			this.Label19.Top = 1.375F;
			this.Label19.Width = 0.6875F;
			// 
			// txtNewCity
			// 
			this.txtNewCity.Height = 0.1875F;
			this.txtNewCity.Left = 3.6875F;
			this.txtNewCity.Name = "txtNewCity";
			this.txtNewCity.Text = null;
			this.txtNewCity.Top = 1.375F;
			this.txtNewCity.Width = 1.875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.8125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold";
			this.Label20.Text = "State";
			this.Label20.Top = 1.5625F;
			this.Label20.Width = 0.6875F;
			// 
			// txtNewState
			// 
			this.txtNewState.Height = 0.1875F;
			this.txtNewState.Left = 3.6875F;
			this.txtNewState.Name = "txtNewState";
			this.txtNewState.Text = null;
			this.txtNewState.Top = 1.5625F;
			this.txtNewState.Width = 0.3125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.0625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold";
			this.Label21.Text = "Zip";
			this.Label21.Top = 1.5625F;
			this.Label21.Width = 0.375F;
			// 
			// txtNewZip
			// 
			this.txtNewZip.Height = 0.1875F;
			this.txtNewZip.Left = 4.4375F;
			this.txtNewZip.Name = "txtNewZip";
			this.txtNewZip.Text = null;
			this.txtNewZip.Top = 1.5625F;
			this.txtNewZip.Width = 1.125F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-weight: bold; text-align: center";
			this.Label22.Text = "Original";
			this.Label22.Top = 0.375F;
			this.Label22.Width = 2.75F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 2.8125F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-weight: bold; text-align: center";
			this.Label23.Text = "New";
			this.Label23.Top = 0.375F;
			this.Label23.Width = 2.75F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 5.625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-weight: bold; text-align: center";
			this.Label24.Text = "Sales Information";
			this.Label24.Top = 0.375F;
			this.Label24.Width = 1.8125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 5.625F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold";
			this.Label25.Text = "Price";
			this.Label25.Top = 0.8125F;
			this.Label25.Width = 0.625F;
			// 
			// txtSalePrice
			// 
			this.txtSalePrice.Height = 0.1875F;
			this.txtSalePrice.Left = 6.25F;
			this.txtSalePrice.Name = "txtSalePrice";
			this.txtSalePrice.Style = "text-align: right";
			this.txtSalePrice.Text = null;
			this.txtSalePrice.Top = 0.8125F;
			this.txtSalePrice.Width = 1.25F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 5.625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-weight: bold";
			this.Label26.Text = "Date";
			this.Label26.Top = 0.625F;
			this.Label26.Width = 0.625F;
			// 
			// txtSaleDate
			// 
			this.txtSaleDate.Height = 0.1875F;
			this.txtSaleDate.Left = 6.25F;
			this.txtSaleDate.Name = "txtSaleDate";
			this.txtSaleDate.Style = "text-align: right";
			this.txtSaleDate.Text = null;
			this.txtSaleDate.Top = 0.625F;
			this.txtSaleDate.Width = 1.25F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-weight: bold";
			this.Label27.Text = "Finance";
			this.Label27.Top = 1.1875F;
			this.Label27.Width = 0.625F;
			// 
			// txtSaleFinance
			// 
			this.txtSaleFinance.Height = 0.1875F;
			this.txtSaleFinance.Left = 6.25F;
			this.txtSaleFinance.Name = "txtSaleFinance";
			this.txtSaleFinance.Style = "text-align: left";
			this.txtSaleFinance.Text = null;
			this.txtSaleFinance.Top = 1.1875F;
			this.txtSaleFinance.Width = 1.25F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 5.625F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-weight: bold";
			this.Label28.Text = "Type";
			this.Label28.Top = 1F;
			this.Label28.Width = 0.625F;
			// 
			// txtSaleType
			// 
			this.txtSaleType.Height = 0.1875F;
			this.txtSaleType.Left = 6.25F;
			this.txtSaleType.Name = "txtSaleType";
			this.txtSaleType.Style = "text-align: left";
			this.txtSaleType.Text = null;
			this.txtSaleType.Top = 1F;
			this.txtSaleType.Width = 1.25F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.625F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-weight: bold";
			this.Label29.Text = "Validity";
			this.Label29.Top = 1.5625F;
			this.Label29.Width = 0.625F;
			// 
			// txtSaleValidity
			// 
			this.txtSaleValidity.Height = 0.1875F;
			this.txtSaleValidity.Left = 6.25F;
			this.txtSaleValidity.Name = "txtSaleValidity";
			this.txtSaleValidity.Style = "text-align: left";
			this.txtSaleValidity.Text = null;
			this.txtSaleValidity.Top = 1.5625F;
			this.txtSaleValidity.Width = 1.25F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.625F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-weight: bold";
			this.Label30.Text = "Verified";
			this.Label30.Top = 1.375F;
			this.Label30.Width = 0.625F;
			// 
			// txtSaleVerified
			// 
			this.txtSaleVerified.Height = 0.1875F;
			this.txtSaleVerified.Left = 6.25F;
			this.txtSaleVerified.Name = "txtSaleVerified";
			this.txtSaleVerified.Style = "text-align: left";
			this.txtSaleVerified.Text = null;
			this.txtSaleVerified.Top = 1.375F;
			this.txtSaleVerified.Width = 1.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.8125F;
			this.Line1.Width = 7.375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.8125F;
			this.Line1.Y2 = 1.8125F;
			// 
			// rptPendingXfers
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMinDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXferTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransaction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewSecondOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleFinance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleValidity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleVerified)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtXferTo;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransaction;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewSecondOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewCity;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewState;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalePrice;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleFinance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleValidity;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleVerified;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
