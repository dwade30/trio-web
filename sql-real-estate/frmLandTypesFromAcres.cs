﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandTypesFromAcres.
	/// </summary>
	public partial class frmLandTypesFromAcres : BaseForm
	{
		public frmLandTypesFromAcres()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLandTypesFromAcres InstancePtr
		{
			get
			{
				return (frmLandTypesFromAcres)Sys.GetInstance(typeof(frmLandTypesFromAcres));
			}
		}

		protected frmLandTypesFromAcres _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmLandTypesFromAcres_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLandTypesFromAcres_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLandTypesFromAcres properties;
			//frmLandTypesFromAcres.FillStyle	= 0;
			//frmLandTypesFromAcres.ScaleWidth	= 5880;
			//frmLandTypesFromAcres.ScaleHeight	= 4050;
			//frmLandTypesFromAcres.LinkTopic	= "Form2";
			//frmLandTypesFromAcres.LockControls	= true;
			//frmLandTypesFromAcres.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupCombos();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupCombos()
		{
			cmbHardwood.Clear();
			cmbMixedWood.Clear();
			cmbSoftWood.Clear();
			cmbOther.Clear();
			int intSoftIndex = 0;
			int intHardIndex = 0;
			int intMixedIndex = 0;
			int intOtherIndex = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from landtype where type = " + FCConvert.ToString(modREConstants.CNSTLANDTYPEACRES) + " or type = " + FCConvert.ToString(modREConstants.CNSTLANDTYPEFRACTIONALACREAGE) + " ORDER BY description", modGlobalVariables.strREDatabase);
			while (!rsLoad.EndOfFile())
			{
				cmbHardwood.AddItem(rsLoad.Get_Fields_String("description"));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				cmbHardwood.ItemData(cmbHardwood.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields("code")));
				cmbSoftWood.AddItem(rsLoad.Get_Fields_String("description"));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				cmbSoftWood.ItemData(cmbSoftWood.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields("code")));
				cmbMixedWood.AddItem(rsLoad.Get_Fields_String("description"));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				cmbMixedWood.ItemData(cmbMixedWood.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields("code")));
				cmbOther.AddItem(rsLoad.Get_Fields_String("description"));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				cmbOther.ItemData(cmbOther.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields("code")));
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				if (rsLoad.Get_Fields("category") == modREConstants.CNSTLANDCATHARDWOOD)
				{
					intHardIndex = cmbHardwood.NewIndex;
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (rsLoad.Get_Fields("category") == modREConstants.CNSTLANDCATSOFTWOOD)
				{
					intSoftIndex = cmbSoftWood.NewIndex;
				}
					// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
					else if (rsLoad.Get_Fields("category") == modREConstants.CNSTLANDCATMIXEDWOOD)
				{
					intMixedIndex = cmbMixedWood.NewIndex;
				}
						// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
						else if (rsLoad.Get_Fields("category") == modREConstants.CNSTLANDCATNONE)
				{
					if (intOtherIndex == 0)
					{
						if (Strings.UCase(Strings.Left(rsLoad.Get_Fields_String("description") + "    ", 4)) == "MISC")
						{
							intOtherIndex = cmbOther.NewIndex;
						}
					}
				}
				rsLoad.MoveNext();
			}
			if (cmbOther.Items.Count > 1)
			{
				cmbHardwood.SelectedIndex = intHardIndex;
				cmbSoftWood.SelectedIndex = intSoftIndex;
				cmbMixedWood.SelectedIndex = intMixedIndex;
				cmbOther.SelectedIndex = intOtherIndex;
			}
		}

		private bool CreateLandTypes()
		{
			bool CreateLandTypes = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngTemp;
				int lngOtherCode;
				int lngSoftCode;
				int lngMixedCode;
				int lngHardCode;
				double dblSoft = 0;
				double dblHard = 0;
				double dblMixed = 0;
				double dblOther = 0;
				double dblAcres = 0;
				double dblTemp = 0;
				string strTemp = "";
				string strA = "";
				string strB = "";
				int intNextLandType = 0;
				int intTemp = 0;
				bool boolHasLandTypes = false;
				bool boolReplace = false;
				int x;
				CreateLandTypes = false;
				if (cmbOther.Items.Count < 1)
				{
					MessageBox.Show("There are no land types defined to assign acreage to", "No Land Types", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateLandTypes;
				}
				modGlobalFunctions.AddCYAEntry_8("RE", "Created land types from acreages");
				if (chkReplace.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolReplace = true;
				}
				else
				{
					boolReplace = false;
				}
				lngOtherCode = cmbOther.ItemData(cmbOther.SelectedIndex);
				lngSoftCode = cmbSoftWood.ItemData(cmbSoftWood.SelectedIndex);
				lngMixedCode = cmbMixedWood.ItemData(cmbMixedWood.SelectedIndex);
				lngHardCode = cmbHardwood.ItemData(cmbHardwood.SelectedIndex);
				lngTemp = 0;
				rsLoad.OpenRecordset("select * from master where not rsdeleted = 1 and rscard = 1 order by rsaccount", modGlobalVariables.strREDatabase);
				while (!rsLoad.EndOfFile())
				{
					intTemp = lngTemp + 1;
					lblUpdate.Text = "Checking " + rsLoad.Get_Fields_Int32("rsaccount");
					lblUpdate.Refresh();
					//FC:FINAL:RPU:#i1333 - Update the label
					FCUtils.ApplicationUpdate(lblUpdate);
					if (intTemp > 20)
					{
						// don't lock the system up but don't run doevents every loop either
						intTemp = 0;
						//Application.DoEvents();
					}
					if (Conversion.Val(rsLoad.Get_Fields_Double("piacres")) > 0 || Conversion.Val(rsLoad.Get_Fields_Double("rssoft")) > 0 || Conversion.Val(rsLoad.Get_Fields_Double("rsmixed")) > 0 || Conversion.Val(rsLoad.Get_Fields_Double("rshard")) > 0 || Conversion.Val(rsLoad.Get_Fields_Double("rsother")) > 0)
					{
						intNextLandType = 0;
						boolHasLandTypes = false;
						dblSoft = Conversion.Val(rsLoad.Get_Fields_Double("rssoft"));
						dblMixed = Conversion.Val(rsLoad.Get_Fields_Double("rsmixed"));
						dblHard = Conversion.Val(rsLoad.Get_Fields_Double("rshard"));
						dblOther = Conversion.Val(rsLoad.Get_Fields_Double("rsother"));
						dblAcres = dblSoft + dblMixed + dblHard + dblOther;
						rsLoad.Edit();
						if (dblAcres > 0)
						{
							for (x = 1; x <= 7; x++)
							{
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "type"))));
								if (lngTemp > 0)
								{
									boolHasLandTypes = true;
									if (boolReplace)
									{
										rsLoad.Set_Fields("piland" + x + "type", 0);
										rsLoad.Set_Fields("piland" + x + "UnitsA", "000");
										rsLoad.Set_Fields("piland" + x + "UnitsB", "000");
									}
								}
							}
							// x
							if (!boolHasLandTypes || boolReplace)
							{
								if (dblOther > 0)
								{
									intNextLandType += 1;
									rsLoad.Set_Fields("piland" + intNextLandType + "type", lngOtherCode);
									dblTemp = dblOther * 100;
									strTemp = Strings.Format(dblTemp, "000000");
									strB = Strings.Right(strTemp, 3);
									strA = Strings.Left(strTemp, 3);
									rsLoad.Set_Fields("piland" + intNextLandType + "unitsA", strA);
									rsLoad.Set_Fields("piland" + intNextLandType + "UnitsB", strB);
									rsLoad.Set_Fields("piland" + intNextLandType + "Inf", 100);
								}
								if (dblSoft > 0)
								{
									intNextLandType += 1;
									rsLoad.Set_Fields("piland" + intNextLandType + "type", lngSoftCode);
									dblTemp = dblSoft * 100;
									strTemp = Strings.Format(dblTemp, "000000");
									strB = Strings.Right(strTemp, 3);
									strA = Strings.Left(strTemp, 3);
									rsLoad.Set_Fields("piland" + intNextLandType + "unitsA", strA);
									rsLoad.Set_Fields("piland" + intNextLandType + "UnitsB", strB);
									rsLoad.Set_Fields("piland" + intNextLandType + "Inf", 100);
								}
								if (dblMixed > 0)
								{
									intNextLandType += 1;
									rsLoad.Set_Fields("piland" + intNextLandType + "type", lngMixedCode);
									dblTemp = dblMixed * 100;
									strTemp = Strings.Format(dblTemp, "000000");
									strB = Strings.Right(strTemp, 3);
									strA = Strings.Left(strTemp, 3);
									rsLoad.Set_Fields("piland" + intNextLandType + "unitsA", strA);
									rsLoad.Set_Fields("piland" + intNextLandType + "UnitsB", strB);
									rsLoad.Set_Fields("piland" + intNextLandType + "Inf", 100);
								}
								if (dblHard > 0)
								{
									intNextLandType += 1;
									rsLoad.Set_Fields("piland" + intNextLandType + "type", lngHardCode);
									dblTemp = dblHard * 100;
									strTemp = Strings.Format(dblTemp, "000000");
									strB = Strings.Right(strTemp, 3);
									strA = Strings.Left(strTemp, 3);
									rsLoad.Set_Fields("piland" + intNextLandType + "unitsA", strA);
									rsLoad.Set_Fields("piland" + intNextLandType + "UnitsB", strB);
									rsLoad.Set_Fields("piland" + intNextLandType + "Inf", 100);
								}
							}
						}
						rsLoad.Update();
					}
					rsLoad.MoveNext();
				}
				CreateLandTypes = true;
				MessageBox.Show("Land types created successfully", "Land Types Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return CreateLandTypes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CreateLand");
			}
			return CreateLandTypes;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (CreateLandTypes())
				this.Unload();
		}
	}
}
