﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExemptCodes.
	/// </summary>
	public partial class rptExemptCodes : BaseSectionReport
	{
		public static rptExemptCodes InstancePtr
		{
			get
			{
				return (rptExemptCodes)Sys.GetInstance(typeof(rptExemptCodes));
			}
		}

		protected rptExemptCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptExemptCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:MSH - i.issue #1087: restored missing report title
			this.Name = "Exemptions";
		}
		// nObj = 1
		//   0	rptExemptCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			clsReport.OpenRecordset("select * from ExemptCode order by code", modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!clsReport.EndOfFile())
			{
				//FC:FINAL:AAKV .exception handled
				//txtCode.Text = clsReport.Get_Fields("code");
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				txtCode.Text = clsReport.Get_Fields("code").ToString();
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				txtAmount.Text = Strings.Format(clsReport.Get_Fields("amount"), "#,###,###,##0");
				txtDescription.Text = clsReport.Get_Fields_String("description");
				txtEnlisted.Text = "";
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				strTemp = modGlobalRoutines.GetCategoryByCode_2(clsReport.Get_Fields("category"));
				txtCategory.Text = strTemp;
				if (Conversion.Val(clsReport.Get_Fields_Int16("enlisted")) == modcalcexemptions.CNSTEXEMPTENLISTEDMAINE)
				{
					txtEnlisted.Text = "Maine";
				}
				else if (Conversion.Val(clsReport.Get_Fields_Int16("enlisted")) == modcalcexemptions.CNSTEXEMPTENLISTEDNONMAINE)
				{
					txtEnlisted.Text = "Non-Maine";
				}
				if (Conversion.Val(clsReport.Get_Fields_Int16("applyto")) == modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST)
				{
					txtApplyTo.Text = "Building First";
				}
				else if (Conversion.Val(clsReport.Get_Fields_Int16("applyto")) == modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGONLY)
				{
					txtApplyTo.Text = "Building Only";
				}
				else if (Conversion.Val(clsReport.Get_Fields_Int16("applyto")) == modcalcexemptions.CNSTEXEMPTAPPLYTOLANDFIRST)
				{
					txtApplyTo.Text = "Land First";
				}
				else if (Conversion.Val(clsReport.Get_Fields_Int16("applyto")) == modcalcexemptions.CNSTEXEMPTAPPLYTOLANDONLY)
				{
					txtApplyTo.Text = "Land Only";
				}
				clsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
