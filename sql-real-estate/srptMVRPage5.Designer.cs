﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage5.
	/// </summary>
	partial class srptMVRPage5
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage5));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34o = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34p = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34q = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34r = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34k = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34j = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34l1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34l2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34l3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34l4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34l = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34m = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34n = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34i = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine38j2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label211 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label213 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label214 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label215 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34h = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label218 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label203 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label204 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label205 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine34t = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label219 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34o)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34p)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34q)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34r)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34k)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34j)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34m)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34n)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34i)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine38j2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34h)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label218)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label205)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34t)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label58,
            this.Shape9,
            this.txtLine34o,
            this.Label38,
            this.Label70,
            this.Label71,
            this.Shape10,
            this.txtLine34p,
            this.Label72,
            this.Label73,
            this.Label74,
            this.Shape11,
            this.txtLine34q,
            this.Label75,
            this.Label76,
            this.Label77,
            this.Shape12,
            this.txtLine34r,
            this.Label81,
            this.Label82,
            this.Label175,
            this.Label176,
            this.Shape14,
            this.txtLine34k,
            this.Label177,
            this.Label178,
            this.Label180,
            this.Shape15,
            this.txtLine34j,
            this.Label83,
            this.Label85,
            this.Shape16,
            this.txtLine34l1,
            this.Label88,
            this.Shape35,
            this.txtLine34l2,
            this.Label89,
            this.Shape36,
            this.txtLine34l3,
            this.Label90,
            this.Shape37,
            this.txtLine34l4,
            this.Label91,
            this.Label92,
            this.Shape18,
            this.txtLine34l,
            this.Label93,
            this.Label95,
            this.Shape19,
            this.txtLine34m,
            this.Label96,
            this.Label97,
            this.Label98,
            this.Shape20,
            this.txtLine34n,
            this.Label99,
            this.Label100,
            this.Label101,
            this.Label102,
            this.Label152,
            this.Line4,
            this.Label181,
            this.Label182,
            this.Label184,
            this.Label185,
            this.Label206,
            this.Label207,
            this.Shape39,
            this.txtLine34i,
            this.Label208,
            this.Shape40,
            this.txtLine38j2,
            this.Label209,
            this.Label210,
            this.Label211,
            this.Label212,
            this.Label213,
            this.Label214,
            this.Label215,
            this.Label217,
            this.Shape41,
            this.txtLine34h,
            this.Label218,
            this.Label203,
            this.Label204,
            this.Label205,
            this.Shape47,
            this.txtLine34t,
            this.Label219});
			this.Detail.Height = 9.71875F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.375F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.375F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 1.1875F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.375F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.125F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 5.6875F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 9pt";
			this.Label58.Text = "40o";
			this.Label58.Top = 7.1875F;
			this.Label58.Width = 0.25F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1875F;
			this.Shape9.Left = 5.9375F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 7.1875F;
			this.Shape9.Width = 1.4375F;
			// 
			// txtLine34o
			// 
			this.txtLine34o.Height = 0.1875F;
			this.txtLine34o.Left = 6F;
			this.txtLine34o.Name = "txtLine34o";
			this.txtLine34o.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34o.Text = " ";
			this.txtLine34o.Top = 7.1875F;
			this.txtLine34o.Width = 1.3125F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0.375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label38.Text = "o. Exempt value of real property of all persons determined to be legally blind";
			this.Label38.Top = 7F;
			this.Label38.Width = 4.5625F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 0.5F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label70.Text = "(§ 654-A) ($4,000 adjusted by certified ratio)";
			this.Label70.Top = 7.1875F;
			this.Label70.Width = 4.5625F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 5.6875F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-size: 9pt";
			this.Label71.Text = "40p";
			this.Label71.Top = 7.46875F;
			this.Label71.Width = 0.25F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.1875F;
			this.Shape10.Left = 5.9375F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 7.46875F;
			this.Shape10.Width = 1.4375F;
			// 
			// txtLine34p
			// 
			this.txtLine34p.Height = 0.1875F;
			this.txtLine34p.Left = 6F;
			this.txtLine34p.Name = "txtLine34p";
			this.txtLine34p.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34p.Text = " ";
			this.txtLine34p.Top = 7.46875F;
			this.txtLine34p.Width = 1.3125F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 0.375F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label72.Text = "p. Aqueducts, pipes and conduits of any corporation supplying a municipality";
			this.Label72.Top = 7.46875F;
			this.Label72.Width = 4.5625F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1875F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 0.5F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label73.Text = "with water  (§ 656(1)(A))";
			this.Label73.Top = 7.65625F;
			this.Label73.Width = 4.5625F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 5.6875F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-size: 9pt";
			this.Label74.Text = "40q";
			this.Label74.Top = 8.3125F;
			this.Label74.Width = 0.25F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.1875F;
			this.Shape11.Left = 5.9375F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 8.3125F;
			this.Shape11.Width = 1.4375F;
			// 
			// txtLine34q
			// 
			this.txtLine34q.Height = 0.1875F;
			this.txtLine34q.Left = 6F;
			this.txtLine34q.Name = "txtLine34q";
			this.txtLine34q.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34q.Text = " ";
			this.txtLine34q.Top = 8.3125F;
			this.txtLine34q.Width = 1.3125F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 0.375F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label75.Text = "q. Animal waste storage facilities constructed since April 1, 1999 and certified";
			this.Label75.Top = 7.9375F;
			this.Label75.Width = 5F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1875F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 0.5F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label76.Text = "as exempt by the Commisioner of Agriculture, Conservation and Forestry";
			this.Label76.Top = 8.125F;
			this.Label76.Width = 4.75F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1875F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 5.6875F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-size: 9pt";
			this.Label77.Text = "40r";
			this.Label77.Top = 8.8125F;
			this.Label77.Width = 0.25F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.1875F;
			this.Shape12.Left = 5.9375F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 8.8125F;
			this.Shape12.Width = 1.4375F;
			// 
			// txtLine34r
			// 
			this.txtLine34r.Height = 0.1875F;
			this.txtLine34r.Left = 6F;
			this.txtLine34r.Name = "txtLine34r";
			this.txtLine34r.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34r.Text = " ";
			this.txtLine34r.Top = 8.8125F;
			this.txtLine34r.Width = 1.3125F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1875F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 0.375F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label81.Text = "r. Pollution control facilities that are certified as such by the Commissioner";
			this.Label81.Top = 8.625F;
			this.Label81.Width = 5F;
			// 
			// Label82
			// 
			this.Label82.Height = 0.1875F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 0.375F;
			this.Label82.Name = "Label82";
			this.Label82.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label82.Text = "of Environmental Protection  (§ 656(1)(E))";
			this.Label82.Top = 8.8125F;
			this.Label82.Width = 4.75F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-5-";
			this.Label175.Top = 9.5F;
			this.Label175.Width = 1F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 5.688F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-size: 9pt";
			this.Label176.Text = "40k";
			this.Label176.Top = 2.876F;
			this.Label176.Width = 0.25F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 0.1875F;
			this.Shape14.Left = 5.938F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 2.876F;
			this.Shape14.Width = 1.4375F;
			// 
			// txtLine34k
			// 
			this.txtLine34k.Height = 0.1875F;
			this.txtLine34k.Left = 6F;
			this.txtLine34k.Name = "txtLine34k";
			this.txtLine34k.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34k.Text = " ";
			this.txtLine34k.Top = 2.876F;
			this.txtLine34k.Width = 1.3125F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1875F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 0.375F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label177.Text = "j. Property of the American Legion, Veterans of Foreign Wars, American Veterans,";
			this.Label177.Top = 1.5F;
			this.Label177.Width = 4.75F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.1875F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 0.5F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label178.Text = "Sons of Union Veterans of the Civil War, Disabled American Veterans and";
			this.Label178.Top = 1.6875F;
			this.Label178.Width = 4.9375F;
			// 
			// Label180
			// 
			this.Label180.Height = 0.1875F;
			this.Label180.HyperLink = null;
			this.Label180.Left = 5.5625F;
			this.Label180.Name = "Label180";
			this.Label180.Style = "font-size: 9pt";
			this.Label180.Text = "40 j(1)";
			this.Label180.Top = 2.125F;
			this.Label180.Width = 0.375F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1875F;
			this.Shape15.Left = 5.9375F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 2.125F;
			this.Shape15.Width = 1.4375F;
			// 
			// txtLine34j
			// 
			this.txtLine34j.Height = 0.1875F;
			this.txtLine34j.Left = 6F;
			this.txtLine34j.Name = "txtLine34j";
			this.txtLine34j.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34j.Text = " ";
			this.txtLine34j.Top = 2.125F;
			this.txtLine34j.Width = 1.3125F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1875F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 0.375F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label83.Text = "k. Property of chambers of commerce or boards of trade  (§ 652(1)(F))";
			this.Label83.Top = 2.876F;
			this.Label83.Width = 4.75F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1875F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0.375F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label85.Text = "l. Property of houses of religious worship and parsonages  (§ 652(1)(G))";
			this.Label85.Top = 3.25F;
			this.Label85.Width = 4.75F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.1875F;
			this.Shape16.Left = 5.9375F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 3.625F;
			this.Shape16.Width = 1.4375F;
			// 
			// txtLine34l1
			// 
			this.txtLine34l1.Height = 0.1875F;
			this.txtLine34l1.Left = 6F;
			this.txtLine34l1.Name = "txtLine34l1";
			this.txtLine34l1.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34l1.Text = " ";
			this.txtLine34l1.Top = 3.625F;
			this.txtLine34l1.Width = 1.3125F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1875F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 0.5F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label88.Text = "1) Number of parsonages within this municipality";
			this.Label88.Top = 3.625F;
			this.Label88.Width = 4.75F;
			// 
			// Shape35
			// 
			this.Shape35.Height = 0.1875F;
			this.Shape35.Left = 5.9375F;
			this.Shape35.Name = "Shape35";
			this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape35.Top = 3.9375F;
			this.Shape35.Width = 1.4375F;
			// 
			// txtLine34l2
			// 
			this.txtLine34l2.Height = 0.1875F;
			this.txtLine34l2.Left = 6F;
			this.txtLine34l2.Name = "txtLine34l2";
			this.txtLine34l2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34l2.Text = " ";
			this.txtLine34l2.Top = 3.9375F;
			this.txtLine34l2.Width = 1.3125F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 0.5F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label89.Text = "2) Total exempt value of those parsonages";
			this.Label89.Top = 3.9375F;
			this.Label89.Width = 4.75F;
			// 
			// Shape36
			// 
			this.Shape36.Height = 0.1875F;
			this.Shape36.Left = 5.9375F;
			this.Shape36.Name = "Shape36";
			this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape36.Top = 4.25F;
			this.Shape36.Width = 1.4375F;
			// 
			// txtLine34l3
			// 
			this.txtLine34l3.Height = 0.1875F;
			this.txtLine34l3.Left = 6F;
			this.txtLine34l3.Name = "txtLine34l3";
			this.txtLine34l3.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34l3.Text = " ";
			this.txtLine34l3.Top = 4.25F;
			this.txtLine34l3.Width = 1.3125F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1875F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 0.5F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label90.Text = "3) Total taxable value of those parsonages";
			this.Label90.Top = 4.25F;
			this.Label90.Width = 4.75F;
			// 
			// Shape37
			// 
			this.Shape37.Height = 0.1875F;
			this.Shape37.Left = 5.9375F;
			this.Shape37.Name = "Shape37";
			this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape37.Top = 4.5625F;
			this.Shape37.Width = 1.4375F;
			// 
			// txtLine34l4
			// 
			this.txtLine34l4.Height = 0.1875F;
			this.txtLine34l4.Left = 6F;
			this.txtLine34l4.Name = "txtLine34l4";
			this.txtLine34l4.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34l4.Text = " ";
			this.txtLine34l4.Top = 4.5625F;
			this.txtLine34l4.Width = 1.3125F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 0.5F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label91.Text = "4) Total exempt value of all houses of religious worship";
			this.Label91.Top = 4.5625F;
			this.Label91.Width = 4.75F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1875F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 5.6875F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "font-size: 9pt";
			this.Label92.Text = "40 l";
			this.Label92.Top = 5.125F;
			this.Label92.Width = 0.25F;
			// 
			// Shape18
			// 
			this.Shape18.Height = 0.1875F;
			this.Shape18.Left = 5.9375F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 5.125F;
			this.Shape18.Width = 1.4375F;
			// 
			// txtLine34l
			// 
			this.txtLine34l.Height = 0.1875F;
			this.txtLine34l.Left = 6F;
			this.txtLine34l.Name = "txtLine34l";
			this.txtLine34l.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34l.Text = " ";
			this.txtLine34l.Top = 5.125F;
			this.txtLine34l.Width = 1.3125F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1875F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 0.375F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label93.Text = "TOTAL EXEMPT VALUE OF ALL HOUSES OF RELIGIOUS WORSHIP";
			this.Label93.Top = 4.9375F;
			this.Label93.Width = 5.125F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1875F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 5.6875F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-size: 9pt";
			this.Label95.Text = "40m";
			this.Label95.Top = 5.75F;
			this.Label95.Width = 0.25F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.1875F;
			this.Shape19.Left = 5.9375F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 5.75F;
			this.Shape19.Width = 1.4375F;
			// 
			// txtLine34m
			// 
			this.txtLine34m.Height = 0.1875F;
			this.txtLine34m.Left = 6F;
			this.txtLine34m.Name = "txtLine34m";
			this.txtLine34m.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34m.Text = " ";
			this.txtLine34m.Top = 5.75F;
			this.txtLine34m.Width = 1.3125F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1875F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 0.375F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label96.Text = "m. Property owned or held in trust for fraternal organizations operating under th" +
    "e";
			this.Label96.Top = 5.5625F;
			this.Label96.Width = 4.75F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1875F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 0.5F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label97.Text = "lodge system (do not include college fraternities)  (§ 652(1)(H))";
			this.Label97.Top = 5.75F;
			this.Label97.Width = 4.75F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1875F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 5.6875F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-size: 9pt";
			this.Label98.Text = "40n";
			this.Label98.Top = 6.75F;
			this.Label98.Width = 0.25F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.1875F;
			this.Shape20.Left = 5.9375F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 6.75F;
			this.Shape20.Width = 1.4375F;
			// 
			// txtLine34n
			// 
			this.txtLine34n.Height = 0.1875F;
			this.txtLine34n.Left = 6F;
			this.txtLine34n.Name = "txtLine34n";
			this.txtLine34n.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34n.Text = " ";
			this.txtLine34n.Top = 6.75F;
			this.txtLine34n.Width = 1.3125F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1875F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.375F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label99.Text = "n. Personal property leased by a benevolent  and charitable organization exempt f" +
    "rom";
			this.Label99.Top = 6F;
			this.Label99.Width = 4.875F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1875F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 0.5F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label100.Text = "Services, health maintenance organization or blood bank  (§ 652(1)(K))";
			this.Label100.Top = 6.5625F;
			this.Label100.Width = 4.75F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1875F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 0.5F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label101.Text = "taxation under § 501 of the Internal Revenue Code of 1954 and the primary";
			this.Label101.Top = 6.1875F;
			this.Label101.Width = 4.875F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1875F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 0.5F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label102.Text = "purpose is the operation of a hospital licensed by the Dept of Health and Human";
			this.Label102.Top = 6.375F;
			this.Label102.Width = 4.875F;
			// 
			// Label152
			// 
			this.Label152.Height = 0.1875F;
			this.Label152.HyperLink = null;
			this.Label152.Left = 1F;
			this.Label152.Name = "Label152";
			this.Label152.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label152.Text = "EXEMPT PROPERTY CONTINUED";
			this.Label152.Top = 0.625F;
			this.Label152.Width = 5.5F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 0.625F;
			this.Line4.Y2 = 0.625F;
			// 
			// Label181
			// 
			this.Label181.Height = 0.1875F;
			this.Label181.HyperLink = null;
			this.Label181.Left = 0.5F;
			this.Label181.Name = "Label181";
			this.Label181.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label181.Text = " Navy Clubs of the USA.  (§ 652(1)(E))";
			this.Label181.Top = 1.875F;
			this.Label181.Width = 4.75F;
			// 
			// Label182
			// 
			this.Label182.Height = 0.1875F;
			this.Label182.HyperLink = null;
			this.Label182.Left = 0.375F;
			this.Label182.Name = "Label182";
			this.Label182.Style = "font-family: \'Times New Roman\'; font-size: 9pt";
			this.Label182.Text = "AND PARSONAGES (Sum of 40 l(2) and 40 l(4))";
			this.Label182.Top = 5.125F;
			this.Label182.Width = 3.375F;
			// 
			// Label184
			// 
			this.Label184.Height = 0.1875F;
			this.Label184.HyperLink = null;
			this.Label184.Left = 0.9375F;
			this.Label184.Name = "Label184";
			this.Label184.Style = "font-family: \'Times New Roman\'; font-size: 7pt";
			this.Label184.Text = "(Value of property owned by a hospital should be reported on line 40h)";
			this.Label184.Top = 6.75F;
			this.Label184.Width = 3.6875F;
			// 
			// Label185
			// 
			this.Label185.Height = 0.1875F;
			this.Label185.HyperLink = null;
			this.Label185.Left = 0.5F;
			this.Label185.Name = "Label185";
			this.Label185.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label185.Text = "(§ 656(1)(J)) (reimbursable exemption)";
			this.Label185.Top = 8.313F;
			this.Label185.Width = 4.75F;
			// 
			// Label206
			// 
			this.Label206.Height = 0.1875F;
			this.Label206.HyperLink = null;
			this.Label206.Left = 0.375F;
			this.Label206.Name = "Label206";
			this.Label206.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label206.Text = "i. Property of literary and scientific institutions  (§ 652(1)(B))";
			this.Label206.Top = 1.25F;
			this.Label206.Width = 4.75F;
			// 
			// Label207
			// 
			this.Label207.Height = 0.1875F;
			this.Label207.HyperLink = null;
			this.Label207.Left = 5.6875F;
			this.Label207.Name = "Label207";
			this.Label207.Style = "font-size: 9pt";
			this.Label207.Text = "40i";
			this.Label207.Top = 1.25F;
			this.Label207.Width = 0.25F;
			// 
			// Shape39
			// 
			this.Shape39.Height = 0.1875F;
			this.Shape39.Left = 5.9375F;
			this.Shape39.Name = "Shape39";
			this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape39.Top = 1.25F;
			this.Shape39.Width = 1.4375F;
			// 
			// txtLine34i
			// 
			this.txtLine34i.Height = 0.1875F;
			this.txtLine34i.Left = 6F;
			this.txtLine34i.Name = "txtLine34i";
			this.txtLine34i.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34i.Text = " ";
			this.txtLine34i.Top = 1.25F;
			this.txtLine34i.Width = 1.3125F;
			// 
			// Label208
			// 
			this.Label208.Height = 0.1875F;
			this.Label208.HyperLink = null;
			this.Label208.Left = 5.5625F;
			this.Label208.Name = "Label208";
			this.Label208.Style = "font-size: 9pt";
			this.Label208.Text = "40 j(2)";
			this.Label208.Top = 2.5625F;
			this.Label208.Width = 0.375F;
			// 
			// Shape40
			// 
			this.Shape40.Height = 0.1875F;
			this.Shape40.Left = 5.9375F;
			this.Shape40.Name = "Shape40";
			this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape40.Top = 2.5625F;
			this.Shape40.Width = 1.4375F;
			// 
			// txtLine38j2
			// 
			this.txtLine38j2.Height = 0.1875F;
			this.txtLine38j2.Left = 6F;
			this.txtLine38j2.Name = "txtLine38j2";
			this.txtLine38j2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine38j2.Text = " ";
			this.txtLine38j2.Top = 2.5625F;
			this.txtLine38j2.Width = 1.3125F;
			// 
			// Label209
			// 
			this.Label209.Height = 0.1875F;
			this.Label209.HyperLink = null;
			this.Label209.Left = 0.5F;
			this.Label209.Name = "Label209";
			this.Label209.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label209.Text = "2) Exempt value attributable to purposes other than meetings,";
			this.Label209.Top = 2.375F;
			this.Label209.Width = 4.75F;
			// 
			// Label210
			// 
			this.Label210.Height = 0.1875F;
			this.Label210.HyperLink = null;
			this.Label210.Left = 5.5625F;
			this.Label210.Name = "Label210";
			this.Label210.Style = "font-size: 9pt";
			this.Label210.Text = "40 l(1)";
			this.Label210.Top = 3.625F;
			this.Label210.Width = 0.375F;
			// 
			// Label211
			// 
			this.Label211.Height = 0.1875F;
			this.Label211.HyperLink = null;
			this.Label211.Left = 5.5625F;
			this.Label211.Name = "Label211";
			this.Label211.Style = "font-size: 9pt";
			this.Label211.Text = "40 l(2)";
			this.Label211.Top = 3.9375F;
			this.Label211.Width = 0.375F;
			// 
			// Label212
			// 
			this.Label212.Height = 0.1875F;
			this.Label212.HyperLink = null;
			this.Label212.Left = 5.5625F;
			this.Label212.Name = "Label212";
			this.Label212.Style = "font-size: 9pt";
			this.Label212.Text = "40 l(3)";
			this.Label212.Top = 4.25F;
			this.Label212.Width = 0.375F;
			// 
			// Label213
			// 
			this.Label213.Height = 0.1875F;
			this.Label213.HyperLink = null;
			this.Label213.Left = 5.5625F;
			this.Label213.Name = "Label213";
			this.Label213.Style = "font-size: 9pt";
			this.Label213.Text = "40 l(4)";
			this.Label213.Top = 4.5625F;
			this.Label213.Width = 0.375F;
			// 
			// Label214
			// 
			this.Label214.Height = 0.1875F;
			this.Label214.HyperLink = null;
			this.Label214.Left = 0.5F;
			this.Label214.Name = "Label214";
			this.Label214.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label214.Text = "1) Total exempt value of veterans organizations";
			this.Label214.Top = 2.125F;
			this.Label214.Width = 4.75F;
			// 
			// Label215
			// 
			this.Label215.Height = 0.1875F;
			this.Label215.HyperLink = null;
			this.Label215.Left = 0.375F;
			this.Label215.Name = "Label215";
			this.Label215.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label215.Text = "h. Property of benevolent and charitable institutions  (§ 652(1)(A))";
			this.Label215.Top = 0.9375F;
			this.Label215.Width = 4.75F;
			// 
			// Label217
			// 
			this.Label217.Height = 0.1875F;
			this.Label217.HyperLink = null;
			this.Label217.Left = 5.6875F;
			this.Label217.Name = "Label217";
			this.Label217.Style = "font-size: 9pt";
			this.Label217.Text = "40h";
			this.Label217.Top = 0.9375F;
			this.Label217.Width = 0.25F;
			// 
			// Shape41
			// 
			this.Shape41.Height = 0.1875F;
			this.Shape41.Left = 5.9375F;
			this.Shape41.Name = "Shape41";
			this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape41.Top = 0.9375F;
			this.Shape41.Width = 1.4375F;
			// 
			// txtLine34h
			// 
			this.txtLine34h.Height = 0.1875F;
			this.txtLine34h.Left = 6F;
			this.txtLine34h.Name = "txtLine34h";
			this.txtLine34h.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34h.Text = " ";
			this.txtLine34h.Top = 0.9375F;
			this.txtLine34h.Width = 1.3125F;
			// 
			// Label218
			// 
			this.Label218.Height = 0.1875F;
			this.Label218.HyperLink = null;
			this.Label218.Left = 0.648F;
			this.Label218.Name = "Label218";
			this.Label218.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label218.Text = "ceremonials, or instruction facilities (reimbursable exemption)";
			this.Label218.Top = 2.563F;
			this.Label218.Width = 4.5625F;
			// 
			// Label203
			// 
			this.Label203.Height = 0.1875F;
			this.Label203.HyperLink = null;
			this.Label203.Left = 0.375F;
			this.Label203.Name = "Label203";
			this.Label203.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label203.Text = "s.Snowmobile trail grooming equipment registerd under";
			this.Label203.Top = 9.125F;
			this.Label203.Width = 5.3125F;
			// 
			// Label204
			// 
			this.Label204.Height = 0.1875F;
			this.Label204.HyperLink = null;
			this.Label204.Left = 0.375F;
			this.Label204.Name = "Label204";
			this.Label204.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label204.Text = "12 M.R.S. § 13113  (§ 655(1)(T)) (reimbursable exemption)";
			this.Label204.Top = 9.3125F;
			this.Label204.Width = 4.75F;
			// 
			// Label205
			// 
			this.Label205.Height = 0.1875F;
			this.Label205.HyperLink = null;
			this.Label205.Left = 5.6875F;
			this.Label205.Name = "Label205";
			this.Label205.Style = "font-size: 9pt";
			this.Label205.Text = "40s";
			this.Label205.Top = 9.125F;
			this.Label205.Width = 0.25F;
			// 
			// Shape47
			// 
			this.Shape47.Height = 0.1875F;
			this.Shape47.Left = 5.9375F;
			this.Shape47.Name = "Shape47";
			this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape47.Top = 9.125F;
			this.Shape47.Width = 1.4375F;
			// 
			// txtLine34t
			// 
			this.txtLine34t.Height = 0.1875F;
			this.txtLine34t.Left = 6F;
			this.txtLine34t.Name = "txtLine34t";
			this.txtLine34t.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine34t.Text = " ";
			this.txtLine34t.Top = 9.125F;
			this.txtLine34t.Width = 1.3125F;
			// 
			// Label219
			// 
			this.Label219.Height = 0.1875F;
			this.Label219.HyperLink = null;
			this.Label219.Left = 0F;
			this.Label219.Name = "Label219";
			this.Label219.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: left";
			this.Label219.Text = "40.";
			this.Label219.Top = 0.938F;
			this.Label219.Width = 0.3125F;
			// 
			// srptMVRPage5
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34o)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34p)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34q)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34r)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34k)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34j)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34l)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34m)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34n)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34i)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine38j2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label214)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label215)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34h)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label218)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label203)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label204)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label205)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine34t)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label219)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34o;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34p;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34q;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34r;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34k;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34j;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34l1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34l2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34l3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34l4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34l;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34m;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34n;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label181;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label184;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label185;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label207;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34i;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label208;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine38j2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label210;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label211;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label213;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label214;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label215;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34h;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label218;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label203;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label204;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label205;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine34t;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label219;
	}
}
