﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage7.
	/// </summary>
	partial class srptMVRPage7
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage7));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtRevaluationCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRevaluationContractor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRevaluationEffectiveDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLineRevaluationPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLineRevaluationBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLineRevaluationLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRevaluationCompleted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxableAcreage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumberParcels = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxMapType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxMapPreparer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxMapDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHasTaxMaps = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptValue1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label212 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label216 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSolarAndWindExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtSolarAndWindApproved = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtSolarAndWindProcessed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalValueAllExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherExemptValue2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptValue3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptProvision1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptProvision2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptProvision3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherExemptName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label188 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label192 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label193 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label194 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label195 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label196 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label197 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label198 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label199 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label200 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label201 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label202 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape46 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label207 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label208 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label213 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label217 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationContractor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLineRevaluationPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLineRevaluationBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLineRevaluationLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationCompleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxableAcreage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberParcels)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxMapType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxMapPreparer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxMapDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHasTaxMaps)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptValue1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSolarAndWindExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSolarAndWindApproved)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSolarAndWindProcessed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptTotalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValueAllExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptValue2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptValue3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptProvision1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptProvision2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptProvision3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label187)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label188)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label192)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label193)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label194)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label195)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label196)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label197)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label198)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label199)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label200)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label201)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label202)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtRevaluationCost,
            this.txtRevaluationContractor,
            this.txtRevaluationEffectiveDate,
            this.txtLineRevaluationPP,
            this.txtLineRevaluationBuilding,
            this.txtLineRevaluationLand,
            this.txtRevaluationCompleted,
            this.txtTaxableAcreage,
            this.txtNumberParcels,
            this.txtTaxMapType,
            this.txtTaxMapPreparer,
            this.txtTaxMapDate,
            this.txtHasTaxMaps,
            this.txtOtherExemptValue1,
            this.Shape62,
            this.Label122,
            this.Label111,
            this.Label120,
            this.label1,
            this.Label212,
            this.Label216,
            this.txtSolarAndWindExempt,
            this.shape1,
            this.txtSolarAndWindApproved,
            this.Shape37,
            this.txtSolarAndWindProcessed,
            this.txtOtherExemptTotalValue,
            this.Shape38,
            this.Shape36,
            this.label3,
            this.txtTotalValueAllExempt,
            this.Shape53,
            this.Shape52,
            this.Shape51,
            this.Shape50,
            this.Shape49,
            this.Shape43,
            this.Shape42,
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label175,
            this.Label176,
            this.Line3,
            this.Label112,
            this.Label113,
            this.Label114,
            this.Label87,
            this.Label115,
            this.Label117,
            this.Label118,
            this.Shape15,
            this.Label121,
            this.Line5,
            this.Label123,
            this.Label126,
            this.Label127,
            this.Label128,
            this.Label129,
            this.Label130,
            this.Label131,
            this.Label132,
            this.Label135,
            this.Shape40,
            this.Label136,
            this.Label137,
            this.Label177,
            this.Label178,
            this.Label179,
            this.Label180,
            this.txtOtherExemptValue2,
            this.txtOtherExemptValue3,
            this.txtOtherExemptProvision1,
            this.txtOtherExemptProvision2,
            this.txtOtherExemptProvision3,
            this.txtOtherExemptName1,
            this.txtOtherExemptName2,
            this.txtOtherExemptName3,
            this.Label185,
            this.Label186,
            this.Shape41,
            this.Label187,
            this.Label188,
            this.Label192,
            this.Label193,
            this.Label194,
            this.Label195,
            this.Label196,
            this.Label197,
            this.Label198,
            this.Label199,
            this.Label200,
            this.Label201,
            this.Label202,
            this.Shape44,
            this.Shape45,
            this.Shape46,
            this.Label206,
            this.Label207,
            this.Label208,
            this.Shape48,
            this.Label209,
            this.Label210,
            this.Shape54,
            this.Shape55,
            this.Shape56,
            this.Shape58,
            this.Shape59,
            this.Shape60,
            this.Shape63,
            this.Shape64,
            this.label2,
            this.Label213,
            this.Label217,
            this.label4,
            this.label5,
            this.Label124,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.label11});
			this.Detail.Height = 9.695001F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// txtRevaluationCost
			// 
			this.txtRevaluationCost.Height = 0.1875F;
			this.txtRevaluationCost.Left = 3.75F;
			this.txtRevaluationCost.Name = "txtRevaluationCost";
			this.txtRevaluationCost.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtRevaluationCost.Text = " ";
			this.txtRevaluationCost.Top = 9.195001F;
			this.txtRevaluationCost.Width = 1.25F;
			// 
			// txtRevaluationContractor
			// 
			this.txtRevaluationContractor.Height = 0.1875F;
			this.txtRevaluationContractor.Left = 3.75F;
			this.txtRevaluationContractor.Name = "txtRevaluationContractor";
			this.txtRevaluationContractor.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtRevaluationContractor.Text = " ";
			this.txtRevaluationContractor.Top = 8.945001F;
			this.txtRevaluationContractor.Width = 1.9375F;
			// 
			// txtRevaluationEffectiveDate
			// 
			this.txtRevaluationEffectiveDate.Height = 0.1875F;
			this.txtRevaluationEffectiveDate.Left = 3.75F;
			this.txtRevaluationEffectiveDate.Name = "txtRevaluationEffectiveDate";
			this.txtRevaluationEffectiveDate.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtRevaluationEffectiveDate.Text = " ";
			this.txtRevaluationEffectiveDate.Top = 8.695001F;
			this.txtRevaluationEffectiveDate.Width = 1.25F;
			// 
			// txtLineRevaluationPP
			// 
			this.txtLineRevaluationPP.Height = 0.1875F;
			this.txtLineRevaluationPP.Left = 3.75F;
			this.txtLineRevaluationPP.Name = "txtLineRevaluationPP";
			this.txtLineRevaluationPP.Style = "text-align: right";
			this.txtLineRevaluationPP.Text = " ";
			this.txtLineRevaluationPP.Top = 8.445001F;
			this.txtLineRevaluationPP.Width = 0.75F;
			// 
			// txtLineRevaluationBuilding
			// 
			this.txtLineRevaluationBuilding.Height = 0.1875F;
			this.txtLineRevaluationBuilding.Left = 3.75F;
			this.txtLineRevaluationBuilding.Name = "txtLineRevaluationBuilding";
			this.txtLineRevaluationBuilding.Style = "text-align: right";
			this.txtLineRevaluationBuilding.Text = " ";
			this.txtLineRevaluationBuilding.Top = 8.257501F;
			this.txtLineRevaluationBuilding.Width = 0.75F;
			// 
			// txtLineRevaluationLand
			// 
			this.txtLineRevaluationLand.Height = 0.1875F;
			this.txtLineRevaluationLand.Left = 3.75F;
			this.txtLineRevaluationLand.Name = "txtLineRevaluationLand";
			this.txtLineRevaluationLand.Style = "text-align: right";
			this.txtLineRevaluationLand.Text = " ";
			this.txtLineRevaluationLand.Top = 8.070001F;
			this.txtLineRevaluationLand.Width = 0.75F;
			// 
			// txtRevaluationCompleted
			// 
			this.txtRevaluationCompleted.Height = 0.1875F;
			this.txtRevaluationCompleted.Left = 3.75F;
			this.txtRevaluationCompleted.Name = "txtRevaluationCompleted";
			this.txtRevaluationCompleted.Style = "text-align: right";
			this.txtRevaluationCompleted.Text = " ";
			this.txtRevaluationCompleted.Top = 7.440001F;
			this.txtRevaluationCompleted.Width = 0.75F;
			// 
			// txtTaxableAcreage
			// 
			this.txtTaxableAcreage.Height = 0.1875F;
			this.txtTaxableAcreage.Left = 6.062F;
			this.txtTaxableAcreage.Name = "txtTaxableAcreage";
			this.txtTaxableAcreage.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTaxableAcreage.Text = " ";
			this.txtTaxableAcreage.Top = 6.940001F;
			this.txtTaxableAcreage.Width = 1.25F;
			// 
			// txtNumberParcels
			// 
			this.txtNumberParcels.Height = 0.1875F;
			this.txtNumberParcels.Left = 6.062F;
			this.txtNumberParcels.Name = "txtNumberParcels";
			this.txtNumberParcels.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtNumberParcels.Text = " ";
			this.txtNumberParcels.Top = 6.690001F;
			this.txtNumberParcels.Width = 1.25F;
			// 
			// txtTaxMapType
			// 
			this.txtTaxMapType.Height = 0.1875F;
			this.txtTaxMapType.Left = 3.9995F;
			this.txtTaxMapType.Name = "txtTaxMapType";
			this.txtTaxMapType.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTaxMapType.Text = " ";
			this.txtTaxMapType.Top = 6.252501F;
			this.txtTaxMapType.Width = 1.25F;
			// 
			// txtTaxMapPreparer
			// 
			this.txtTaxMapPreparer.Height = 0.1875F;
			this.txtTaxMapPreparer.Left = 3.9995F;
			this.txtTaxMapPreparer.Name = "txtTaxMapPreparer";
			this.txtTaxMapPreparer.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtTaxMapPreparer.Text = " ";
			this.txtTaxMapPreparer.Top = 6.002501F;
			this.txtTaxMapPreparer.Width = 3.3125F;
			// 
			// txtTaxMapDate
			// 
			this.txtTaxMapDate.Height = 0.1875F;
			this.txtTaxMapDate.Left = 3.9995F;
			this.txtTaxMapDate.Name = "txtTaxMapDate";
			this.txtTaxMapDate.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTaxMapDate.Text = " ";
			this.txtTaxMapDate.Top = 5.752501F;
			this.txtTaxMapDate.Width = 1.25F;
			// 
			// txtHasTaxMaps
			// 
			this.txtHasTaxMaps.Height = 0.1875F;
			this.txtHasTaxMaps.Left = 4.4995F;
			this.txtHasTaxMaps.Name = "txtHasTaxMaps";
			this.txtHasTaxMaps.Style = "text-align: right";
			this.txtHasTaxMaps.Text = " ";
			this.txtHasTaxMaps.Top = 5.070501F;
			this.txtHasTaxMaps.Width = 0.75F;
			// 
			// txtOtherExemptValue1
			// 
			this.txtOtherExemptValue1.Height = 0.188F;
			this.txtOtherExemptValue1.Left = 6.063F;
			this.txtOtherExemptValue1.Name = "txtOtherExemptValue1";
			this.txtOtherExemptValue1.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOtherExemptValue1.Text = " ";
			this.txtOtherExemptValue1.Top = 3.461F;
			this.txtOtherExemptValue1.Width = 1.25F;
			// 
			// Shape62
			// 
			this.Shape62.Height = 0.1875F;
			this.Shape62.Left = 5.992F;
			this.Shape62.Name = "Shape62";
			this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape62.Top = 3.461F;
			this.Shape62.Width = 1.383F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1875F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 5.9375F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-family: \'Times New Roman\'; font-size: 7pt; font-weight: bold";
			this.Label122.Text = "(sum of all exempt value)";
			this.Label122.Top = 4.6705F;
			this.Label122.Width = 1.4375F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 5.7505F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-size: 9pt";
			this.Label111.Text = "40v";
			this.Label111.Top = 4.233F;
			this.Label111.Width = 0.2495003F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1875F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 5.8125F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-size: 9pt";
			this.Label120.Text = "40";
			this.Label120.Top = 4.483F;
			this.Label120.Width = 0.1875F;
			// 
			// label1
			// 
			this.label1.Height = 0.1875F;
			this.label1.HyperLink = null;
			this.label1.Left = 5.582F;
			this.label1.Name = "label1";
			this.label1.Style = "font-size: 9pt";
			this.label1.Text = "40 u(1)";
			this.label1.Top = 0.97F;
			this.label1.Width = 0.4000001F;
			// 
			// Label212
			// 
			this.Label212.Height = 0.1875F;
			this.Label212.HyperLink = null;
			this.Label212.Left = 5.582F;
			this.Label212.Name = "Label212";
			this.Label212.Style = "font-size: 9pt";
			this.Label212.Text = "40 u(2)";
			this.Label212.Top = 1.219995F;
			this.Label212.Width = 0.4000001F;
			// 
			// Label216
			// 
			this.Label216.Height = 0.1875F;
			this.Label216.HyperLink = null;
			this.Label216.Left = 5.582F;
			this.Label216.Name = "Label216";
			this.Label216.Style = "font-size: 9pt";
			this.Label216.Text = "40 u(3)";
			this.Label216.Top = 1.46999F;
			this.Label216.Width = 0.4000001F;
			// 
			// txtSolarAndWindExempt
			// 
			this.txtSolarAndWindExempt.Height = 0.1875F;
			this.txtSolarAndWindExempt.Left = 6.0625F;
			this.txtSolarAndWindExempt.Name = "txtSolarAndWindExempt";
			this.txtSolarAndWindExempt.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtSolarAndWindExempt.Text = " ";
			this.txtSolarAndWindExempt.Top = 1.46999F;
			this.txtSolarAndWindExempt.Width = 1.25F;
			// 
			// shape1
			// 
			this.shape1.Height = 0.1875F;
			this.shape1.Left = 6F;
			this.shape1.Name = "shape1";
			this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shape1.Top = 1.46999F;
			this.shape1.Width = 1.375F;
			// 
			// txtSolarAndWindApproved
			// 
			this.txtSolarAndWindApproved.Height = 0.1875F;
			this.txtSolarAndWindApproved.Left = 6.0625F;
			this.txtSolarAndWindApproved.Name = "txtSolarAndWindApproved";
			this.txtSolarAndWindApproved.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtSolarAndWindApproved.Text = " ";
			this.txtSolarAndWindApproved.Top = 1.219995F;
			this.txtSolarAndWindApproved.Width = 1.25F;
			// 
			// Shape37
			// 
			this.Shape37.Height = 0.1875F;
			this.Shape37.Left = 6F;
			this.Shape37.Name = "Shape37";
			this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape37.Top = 1.219995F;
			this.Shape37.Width = 1.375F;
			// 
			// txtSolarAndWindProcessed
			// 
			this.txtSolarAndWindProcessed.Height = 0.1875F;
			this.txtSolarAndWindProcessed.Left = 6.0625F;
			this.txtSolarAndWindProcessed.Name = "txtSolarAndWindProcessed";
			this.txtSolarAndWindProcessed.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtSolarAndWindProcessed.Text = " ";
			this.txtSolarAndWindProcessed.Top = 0.97F;
			this.txtSolarAndWindProcessed.Width = 1.25F;
			// 
			// txtOtherExemptTotalValue
			// 
			this.txtOtherExemptTotalValue.Height = 0.1875F;
			this.txtOtherExemptTotalValue.Left = 6.0625F;
			this.txtOtherExemptTotalValue.Name = "txtOtherExemptTotalValue";
			this.txtOtherExemptTotalValue.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOtherExemptTotalValue.Text = " ";
			this.txtOtherExemptTotalValue.Top = 4.233F;
			this.txtOtherExemptTotalValue.Width = 1.25F;
			// 
			// Shape38
			// 
			this.Shape38.Height = 0.1875F;
			this.Shape38.Left = 6F;
			this.Shape38.Name = "Shape38";
			this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape38.Top = 4.233F;
			this.Shape38.Width = 1.375F;
			// 
			// Shape36
			// 
			this.Shape36.Height = 0.1875F;
			this.Shape36.Left = 6F;
			this.Shape36.Name = "Shape36";
			this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape36.Top = 0.97F;
			this.Shape36.Width = 1.375F;
			// 
			// label3
			// 
			this.label3.Height = 0.1875F;
			this.label3.HyperLink = null;
			this.label3.Left = 0.375F;
			this.label3.Name = "label3";
			this.label3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.label3.Text = "u. Solar and wind energy equipment.  § 655(1)(U) & 656(1)(k) (reimbursable exempt" +
    "ion)";
			this.label3.Top = 0.7820001F;
			this.label3.Width = 5.24F;
			// 
			// txtTotalValueAllExempt
			// 
			this.txtTotalValueAllExempt.Height = 0.1875F;
			this.txtTotalValueAllExempt.Left = 6.0625F;
			this.txtTotalValueAllExempt.Name = "txtTotalValueAllExempt";
			this.txtTotalValueAllExempt.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTotalValueAllExempt.Text = " ";
			this.txtTotalValueAllExempt.Top = 4.483F;
			this.txtTotalValueAllExempt.Width = 1.25F;
			// 
			// Shape53
			// 
			this.Shape53.Height = 0.1875F;
			this.Shape53.Left = 3.6875F;
			this.Shape53.Name = "Shape53";
			this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape53.Top = 8.445001F;
			this.Shape53.Width = 0.875F;
			// 
			// Shape52
			// 
			this.Shape52.Height = 0.1875F;
			this.Shape52.Left = 3.6875F;
			this.Shape52.Name = "Shape52";
			this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape52.Top = 8.257501F;
			this.Shape52.Width = 0.875F;
			// 
			// Shape51
			// 
			this.Shape51.Height = 0.1875F;
			this.Shape51.Left = 3.6875F;
			this.Shape51.Name = "Shape51";
			this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape51.Top = 8.070001F;
			this.Shape51.Width = 0.875F;
			// 
			// Shape50
			// 
			this.Shape50.Height = 0.1875F;
			this.Shape50.Left = 3.6875F;
			this.Shape50.Name = "Shape50";
			this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape50.Top = 7.440001F;
			this.Shape50.Width = 0.875F;
			// 
			// Shape49
			// 
			this.Shape49.Height = 0.1875F;
			this.Shape49.Left = 4.437F;
			this.Shape49.Name = "Shape49";
			this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape49.Top = 5.070501F;
			this.Shape49.Width = 0.875F;
			// 
			// Shape43
			// 
			this.Shape43.Height = 0.1875F;
			this.Shape43.Left = 3.937F;
			this.Shape43.Name = "Shape43";
			this.Shape43.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape43.Top = 6.002501F;
			this.Shape43.Width = 3.4375F;
			// 
			// Shape42
			// 
			this.Shape42.Height = 0.1875F;
			this.Shape42.Left = 3.937F;
			this.Shape42.Name = "Shape42";
			this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape42.Top = 5.752501F;
			this.Shape42.Width = 1.375F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.9375F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.3125F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 1.75F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.3125F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.0625F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.125F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label175.Text = "-7-";
			this.Label175.Top = 9.507501F;
			this.Label175.Width = 1F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 1F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label176.Text = "EXEMPT PROPERTY CONTINUED";
			this.Label176.Top = 0.5625F;
			this.Label176.Width = 5.5F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.5625F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 0.5625F;
			this.Line3.Y2 = 0.5625F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0.375F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label112.Text = "v. Other. The Laws of the State of Maine provide for exemption of quasi-municipal" +
    " organizations such as authorities";
			this.Label112.Top = 1.7105F;
			this.Label112.Width = 6.0625F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1875F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 0.5F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label113.Text = "districts, and trust commisions. These exemptions will not be found in Title 36.";
			this.Label113.Top = 1.898F;
			this.Label113.Width = 4.75F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1875F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 0.5F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label114.Text = "Examples: Section 5114 of Title 30-A provides for exemption of real and personal " +
    "property of an Urban Renewal";
			this.Label114.Top = 2.148F;
			this.Label114.Width = 6.0625F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1875F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 0.5F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label87.Text = "Authority or Chapter 164, P. & S.L. of 1971 provides for exemption of real estate" +
    " owned by the Cobbossee-";
			this.Label87.Top = 2.3355F;
			this.Label87.Width = 5.875F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 0.5F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label115.Text = "Annabessacook Authority. (See also 30-A M.R.S. § 5413, Revenue Producing Municipa" +
    "l Facilities Act.)";
			this.Label115.Top = 2.523F;
			this.Label115.Width = 5.6875F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 0.5F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label117.Text = "Enter the full name of the organization in your municipality that has been grante" +
    "d exempt status through such";
			this.Label117.Top = 2.795501F;
			this.Label117.Width = 6.8125F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 0.5F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label118.Text = "a law, the provision of the law granting the exemption and the estimated full val" +
    "ue of real property";
			this.Label118.Top = 2.983001F;
			this.Label118.Width = 5.5F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1875F;
			this.Shape15.Left = 6F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 4.483F;
			this.Shape15.Width = 1.375F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 2.1875F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label121.Text = "MUNICIPAL RECORDS";
			this.Label121.Top = 4.8805F;
			this.Label121.Width = 2.9375F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 4.8805F;
			this.Line5.Width = 7.25F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.25F;
			this.Line5.Y1 = 4.8805F;
			this.Line5.Y2 = 4.8805F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 0.3745003F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label123.Text = "a. Does your municipality have tax maps?";
			this.Label123.Top = 5.070501F;
			this.Label123.Width = 2.3125F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 4.187F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-size: 9pt";
			this.Label126.Text = "41a";
			this.Label126.Top = 5.070501F;
			this.Label126.Width = 0.25F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 0.3745003F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label127.Text = "If yes, proceed to b, c and d. If no, move to line 42.  Give date when tax maps w" +
    "ere originally obtained and name of contractor.";
			this.Label127.Top = 5.315001F;
			this.Label127.Width = 6.25F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 0.3745003F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label128.Text = "(This does not refer to the annual updating of tax maps.)";
			this.Label128.Top = 5.502501F;
			this.Label128.Width = 4.75F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1875F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 3.687F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-size: 9pt";
			this.Label129.Text = "41b";
			this.Label129.Top = 5.752501F;
			this.Label129.Width = 0.25F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1875F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 0.3745003F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label130.Text = "b. Date";
			this.Label130.Top = 5.752501F;
			this.Label130.Width = 1.375F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1875F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 3.687F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-size: 9pt";
			this.Label131.Text = "41c";
			this.Label131.Top = 6.002501F;
			this.Label131.Width = 0.25F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1875F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0.3745003F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label132.Text = "c. Name of Contractor";
			this.Label132.Top = 6.002501F;
			this.Label132.Width = 1.375F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1875F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 5.7495F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-size: 9pt";
			this.Label135.Text = "42";
			this.Label135.Top = 6.690001F;
			this.Label135.Width = 0.25F;
			// 
			// Shape40
			// 
			this.Shape40.Height = 0.1875F;
			this.Shape40.Left = 5.9995F;
			this.Shape40.Name = "Shape40";
			this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape40.Top = 6.690001F;
			this.Shape40.Width = 1.375F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1875F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 0.3745003F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label136.Text = "Enter the number of land parcels within your municipality";
			this.Label136.Top = 6.502501F;
			this.Label136.Width = 4.0625F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1875F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 0.3745003F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label137.Text = "(Not the number of tax bills)";
			this.Label137.Top = 6.690001F;
			this.Label137.Width = 3.9375F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1875F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 0.375F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold";
			this.Label177.Text = "TOTAL VALUE OF ALL PROPERTY EXEMPTED BY LAW";
			this.Label177.Top = 4.483F;
			this.Label177.Width = 3.812F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.1875F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 0.5F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic; text-align:" +
    " left";
			this.Label178.Text = "NAME OF ORGANIZATION";
			this.Label178.Top = 3.2105F;
			this.Label178.Width = 1.8125F;
			// 
			// Label179
			// 
			this.Label179.Height = 0.1875F;
			this.Label179.HyperLink = null;
			this.Label179.Left = 3.3125F;
			this.Label179.Name = "Label179";
			this.Label179.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic; text-align:" +
    " left";
			this.Label179.Text = "PROVISION OF LAW";
			this.Label179.Top = 3.2105F;
			this.Label179.Width = 1.875F;
			// 
			// Label180
			// 
			this.Label180.Height = 0.1875F;
			this.Label180.HyperLink = null;
			this.Label180.Left = 6F;
			this.Label180.Name = "Label180";
			this.Label180.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic; text-align:" +
    " center";
			this.Label180.Text = "EXEMPT VALUE";
			this.Label180.Top = 3.2105F;
			this.Label180.Width = 1.375F;
			// 
			// txtOtherExemptValue2
			// 
			this.txtOtherExemptValue2.Height = 0.1875F;
			this.txtOtherExemptValue2.Left = 6.0625F;
			this.txtOtherExemptValue2.Name = "txtOtherExemptValue2";
			this.txtOtherExemptValue2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOtherExemptValue2.Text = " ";
			this.txtOtherExemptValue2.Top = 3.7105F;
			this.txtOtherExemptValue2.Width = 1.25F;
			// 
			// txtOtherExemptValue3
			// 
			this.txtOtherExemptValue3.Height = 0.1875F;
			this.txtOtherExemptValue3.Left = 6.0625F;
			this.txtOtherExemptValue3.Name = "txtOtherExemptValue3";
			this.txtOtherExemptValue3.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtOtherExemptValue3.Text = " ";
			this.txtOtherExemptValue3.Top = 3.9605F;
			this.txtOtherExemptValue3.Width = 1.25F;
			// 
			// txtOtherExemptProvision1
			// 
			this.txtOtherExemptProvision1.Height = 0.1875F;
			this.txtOtherExemptProvision1.Left = 3.3125F;
			this.txtOtherExemptProvision1.Name = "txtOtherExemptProvision1";
			this.txtOtherExemptProvision1.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtOtherExemptProvision1.Text = " ";
			this.txtOtherExemptProvision1.Top = 3.4605F;
			this.txtOtherExemptProvision1.Width = 2.375F;
			// 
			// txtOtherExemptProvision2
			// 
			this.txtOtherExemptProvision2.Height = 0.1875F;
			this.txtOtherExemptProvision2.Left = 3.3125F;
			this.txtOtherExemptProvision2.Name = "txtOtherExemptProvision2";
			this.txtOtherExemptProvision2.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtOtherExemptProvision2.Text = " ";
			this.txtOtherExemptProvision2.Top = 3.7105F;
			this.txtOtherExemptProvision2.Width = 2.375F;
			// 
			// txtOtherExemptProvision3
			// 
			this.txtOtherExemptProvision3.Height = 0.1875F;
			this.txtOtherExemptProvision3.Left = 3.3125F;
			this.txtOtherExemptProvision3.Name = "txtOtherExemptProvision3";
			this.txtOtherExemptProvision3.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtOtherExemptProvision3.Text = " ";
			this.txtOtherExemptProvision3.Top = 3.9605F;
			this.txtOtherExemptProvision3.Width = 2.375F;
			// 
			// txtOtherExemptName1
			// 
			this.txtOtherExemptName1.Height = 0.1875F;
			this.txtOtherExemptName1.Left = 0.5F;
			this.txtOtherExemptName1.Name = "txtOtherExemptName1";
			this.txtOtherExemptName1.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtOtherExemptName1.Text = " ";
			this.txtOtherExemptName1.Top = 3.4605F;
			this.txtOtherExemptName1.Width = 2.625F;
			// 
			// txtOtherExemptName2
			// 
			this.txtOtherExemptName2.Height = 0.1875F;
			this.txtOtherExemptName2.Left = 0.5F;
			this.txtOtherExemptName2.Name = "txtOtherExemptName2";
			this.txtOtherExemptName2.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtOtherExemptName2.Text = " ";
			this.txtOtherExemptName2.Top = 3.7105F;
			this.txtOtherExemptName2.Width = 2.625F;
			// 
			// txtOtherExemptName3
			// 
			this.txtOtherExemptName3.Height = 0.1875F;
			this.txtOtherExemptName3.Left = 0.5F;
			this.txtOtherExemptName3.Name = "txtOtherExemptName3";
			this.txtOtherExemptName3.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtOtherExemptName3.Text = " ";
			this.txtOtherExemptName3.Top = 3.9605F;
			this.txtOtherExemptName3.Width = 2.625F;
			// 
			// Label185
			// 
			this.Label185.Height = 0.1875F;
			this.Label185.HyperLink = null;
			this.Label185.Left = 0.3745003F;
			this.Label185.Name = "Label185";
			this.Label185.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label185.Text = "Total taxable land acreage in your municipality";
			this.Label185.Top = 6.940001F;
			this.Label185.Width = 3.1875F;
			// 
			// Label186
			// 
			this.Label186.Height = 0.1875F;
			this.Label186.HyperLink = null;
			this.Label186.Left = 5.7495F;
			this.Label186.Name = "Label186";
			this.Label186.Style = "font-size: 9pt";
			this.Label186.Text = "43";
			this.Label186.Top = 6.940001F;
			this.Label186.Width = 0.25F;
			// 
			// Shape41
			// 
			this.Shape41.Height = 0.1875F;
			this.Shape41.Left = 5.9995F;
			this.Shape41.Name = "Shape41";
			this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape41.Top = 6.940001F;
			this.Shape41.Width = 1.375F;
			// 
			// Label187
			// 
			this.Label187.Height = 0.1875F;
			this.Label187.HyperLink = null;
			this.Label187.Left = 0.3745003F;
			this.Label187.Name = "Label187";
			this.Label187.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label187.Text = "a. Has a professional town-wide revaluation been completed in your municipality?";
			this.Label187.Top = 7.190001F;
			this.Label187.Width = 4.375F;
			// 
			// Label188
			// 
			this.Label188.Height = 0.1875F;
			this.Label188.HyperLink = null;
			this.Label188.Left = 3.375F;
			this.Label188.Name = "Label188";
			this.Label188.Style = "font-size: 9pt";
			this.Label188.Text = "44a";
			this.Label188.Top = 7.440001F;
			this.Label188.Width = 0.25F;
			// 
			// Label192
			// 
			this.Label192.Height = 0.1875F;
			this.Label192.HyperLink = null;
			this.Label192.Left = 0.3745003F;
			this.Label192.Name = "Label192";
			this.Label192.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label192.Text = "b. Did the revaluation include any of the following? Please enter each category w" +
    "ith YES or NO.";
			this.Label192.Top = 7.82F;
			this.Label192.Width = 4.875F;
			// 
			// Label193
			// 
			this.Label193.Height = 0.1875F;
			this.Label193.HyperLink = null;
			this.Label193.Left = 0.615F;
			this.Label193.Name = "Label193";
			this.Label193.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label193.Text = "If yes, please answer the questions below.";
			this.Label193.Top = 7.437F;
			this.Label193.Width = 2.25F;
			// 
			// Label194
			// 
			this.Label194.Height = 0.1875F;
			this.Label194.HyperLink = null;
			this.Label194.Left = 3.25F;
			this.Label194.Name = "Label194";
			this.Label194.Style = "font-size: 9pt; text-align: right";
			this.Label194.Text = "44b(1)";
			this.Label194.Top = 8.070001F;
			this.Label194.Width = 0.375F;
			// 
			// Label195
			// 
			this.Label195.Height = 0.1875F;
			this.Label195.HyperLink = null;
			this.Label195.Left = 3.25F;
			this.Label195.Name = "Label195";
			this.Label195.Style = "font-size: 9pt; text-align: right";
			this.Label195.Text = "44b(2)";
			this.Label195.Top = 8.257501F;
			this.Label195.Width = 0.375F;
			// 
			// Label196
			// 
			this.Label196.Height = 0.1875F;
			this.Label196.HyperLink = null;
			this.Label196.Left = 4.68F;
			this.Label196.Name = "Label196";
			this.Label196.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label196.Text = "LAND";
			this.Label196.Top = 8.070001F;
			this.Label196.Width = 0.875F;
			// 
			// Label197
			// 
			this.Label197.Height = 0.1875F;
			this.Label197.HyperLink = null;
			this.Label197.Left = 4.68F;
			this.Label197.Name = "Label197";
			this.Label197.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label197.Text = "BUILDINGS";
			this.Label197.Top = 8.257501F;
			this.Label197.Width = 0.875F;
			// 
			// Label198
			// 
			this.Label198.Height = 0.1875F;
			this.Label198.HyperLink = null;
			this.Label198.Left = 3.25F;
			this.Label198.Name = "Label198";
			this.Label198.Style = "font-size: 9pt; text-align: right";
			this.Label198.Text = "44b(3)";
			this.Label198.Top = 8.445001F;
			this.Label198.Width = 0.375F;
			// 
			// Label199
			// 
			this.Label199.Height = 0.1875F;
			this.Label199.HyperLink = null;
			this.Label199.Left = 4.680001F;
			this.Label199.Name = "Label199";
			this.Label199.Style = "font-family: \'Times New Roman\'; font-size: 10pt";
			this.Label199.Text = "PERSONAL PROPERTY";
			this.Label199.Top = 8.445001F;
			this.Label199.Width = 1.6875F;
			// 
			// Label200
			// 
			this.Label200.Height = 0.1875F;
			this.Label200.HyperLink = null;
			this.Label200.Left = 0.3745003F;
			this.Label200.Name = "Label200";
			this.Label200.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label200.Text = "c. Effective Date";
			this.Label200.Top = 8.695001F;
			this.Label200.Width = 1.375F;
			// 
			// Label201
			// 
			this.Label201.Height = 0.1875F;
			this.Label201.HyperLink = null;
			this.Label201.Left = 0.3745003F;
			this.Label201.Name = "Label201";
			this.Label201.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label201.Text = "d. Contractor Name";
			this.Label201.Top = 8.945001F;
			this.Label201.Width = 1.375F;
			// 
			// Label202
			// 
			this.Label202.Height = 0.1875F;
			this.Label202.HyperLink = null;
			this.Label202.Left = 0.3745003F;
			this.Label202.Name = "Label202";
			this.Label202.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label202.Text = "e. Cost";
			this.Label202.Top = 9.195001F;
			this.Label202.Width = 1.375F;
			// 
			// Shape44
			// 
			this.Shape44.Height = 0.1875F;
			this.Shape44.Left = 3.6875F;
			this.Shape44.Name = "Shape44";
			this.Shape44.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape44.Top = 8.945001F;
			this.Shape44.Width = 2.0625F;
			// 
			// Shape45
			// 
			this.Shape45.Height = 0.1875F;
			this.Shape45.Left = 3.6875F;
			this.Shape45.Name = "Shape45";
			this.Shape45.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape45.Top = 9.195001F;
			this.Shape45.Width = 1.375F;
			// 
			// Shape46
			// 
			this.Shape46.Height = 0.1875F;
			this.Shape46.Left = 3.6875F;
			this.Shape46.Name = "Shape46";
			this.Shape46.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape46.Top = 8.695001F;
			this.Shape46.Width = 1.375F;
			// 
			// Label206
			// 
			this.Label206.Height = 0.1875F;
			this.Label206.HyperLink = null;
			this.Label206.Left = 3.25F;
			this.Label206.Name = "Label206";
			this.Label206.Style = "font-size: 9pt; text-align: right";
			this.Label206.Text = "44c";
			this.Label206.Top = 8.695001F;
			this.Label206.Width = 0.375F;
			// 
			// Label207
			// 
			this.Label207.Height = 0.1875F;
			this.Label207.HyperLink = null;
			this.Label207.Left = 3.25F;
			this.Label207.Name = "Label207";
			this.Label207.Style = "font-size: 9pt; text-align: right";
			this.Label207.Text = "44d";
			this.Label207.Top = 8.945001F;
			this.Label207.Width = 0.375F;
			// 
			// Label208
			// 
			this.Label208.Height = 0.1875F;
			this.Label208.HyperLink = null;
			this.Label208.Left = 3.25F;
			this.Label208.Name = "Label208";
			this.Label208.Style = "font-size: 9pt; text-align: right";
			this.Label208.Text = "44e";
			this.Label208.Top = 9.195001F;
			this.Label208.Width = 0.375F;
			// 
			// Shape48
			// 
			this.Shape48.Height = 0.1875F;
			this.Shape48.Left = 3.937F;
			this.Shape48.Name = "Shape48";
			this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape48.Top = 6.252501F;
			this.Shape48.Width = 1.375F;
			// 
			// Label209
			// 
			this.Label209.Height = 0.1875F;
			this.Label209.HyperLink = null;
			this.Label209.Left = 3.687F;
			this.Label209.Name = "Label209";
			this.Label209.Style = "font-size: 9pt";
			this.Label209.Text = "41d";
			this.Label209.Top = 6.252501F;
			this.Label209.Width = 0.25F;
			// 
			// Label210
			// 
			this.Label210.Height = 0.1875F;
			this.Label210.HyperLink = null;
			this.Label210.Left = 0.3745003F;
			this.Label210.Name = "Label210";
			this.Label210.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label210.Text = "d. Are your tax maps PAPER, GIS or CAD?";
			this.Label210.Top = 6.252501F;
			this.Label210.Width = 2.3125F;
			// 
			// Shape54
			// 
			this.Shape54.Height = 0.1875F;
			this.Shape54.Left = 0.5F;
			this.Shape54.Name = "Shape54";
			this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape54.Top = 3.4605F;
			this.Shape54.Width = 2.625F;
			// 
			// Shape55
			// 
			this.Shape55.Height = 0.1875F;
			this.Shape55.Left = 0.5F;
			this.Shape55.Name = "Shape55";
			this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape55.Top = 3.7105F;
			this.Shape55.Width = 2.625F;
			// 
			// Shape56
			// 
			this.Shape56.Height = 0.1875F;
			this.Shape56.Left = 0.5F;
			this.Shape56.Name = "Shape56";
			this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape56.Top = 3.9605F;
			this.Shape56.Width = 2.625F;
			// 
			// Shape58
			// 
			this.Shape58.Height = 0.1875F;
			this.Shape58.Left = 3.3125F;
			this.Shape58.Name = "Shape58";
			this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape58.Top = 3.4605F;
			this.Shape58.Width = 2.375F;
			// 
			// Shape59
			// 
			this.Shape59.Height = 0.1875F;
			this.Shape59.Left = 3.3125F;
			this.Shape59.Name = "Shape59";
			this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape59.Top = 3.7105F;
			this.Shape59.Width = 2.375F;
			// 
			// Shape60
			// 
			this.Shape60.Height = 0.1875F;
			this.Shape60.Left = 3.3125F;
			this.Shape60.Name = "Shape60";
			this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape60.Top = 3.9605F;
			this.Shape60.Width = 2.375F;
			// 
			// Shape63
			// 
			this.Shape63.Height = 0.1875F;
			this.Shape63.Left = 5.992F;
			this.Shape63.Name = "Shape63";
			this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape63.Top = 3.711F;
			this.Shape63.Width = 1.383F;
			// 
			// Shape64
			// 
			this.Shape64.Height = 0.1875F;
			this.Shape64.Left = 5.992F;
			this.Shape64.Name = "Shape64";
			this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape64.Top = 3.961F;
			this.Shape64.Width = 1.383F;
			// 
			// label2
			// 
			this.label2.Height = 0.1875F;
			this.label2.HyperLink = null;
			this.label2.Left = 0.5F;
			this.label2.Name = "label2";
			this.label2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.label2.Text = "1) Total number of solar and wind energy equipment applications processed.";
			this.label2.Top = 0.97F;
			this.label2.Width = 4.323F;
			// 
			// Label213
			// 
			this.Label213.Height = 0.1875F;
			this.Label213.HyperLink = null;
			this.Label213.Left = 0.5F;
			this.Label213.Name = "Label213";
			this.Label213.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label213.Text = "2) Total number of solar and wind energy equipment applications approved.";
			this.Label213.Top = 1.219995F;
			this.Label213.Width = 4.135F;
			// 
			// Label217
			// 
			this.Label217.Height = 0.1875F;
			this.Label217.HyperLink = null;
			this.Label217.Left = 0.5F;
			this.Label217.Name = "Label217";
			this.Label217.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label217.Text = "3) Total exempt value of solar and wind energy equipment.";
			this.Label217.Top = 1.46999F;
			this.Label217.Width = 3.4375F;
			// 
			// label4
			// 
			this.label4.Height = 0.1875F;
			this.label4.HyperLink = null;
			this.label4.Left = 0.615F;
			this.label4.Name = "label4";
			this.label4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.label4.Text = "If no, please proceed to line 45.";
			this.label4.Top = 7.624501F;
			this.label4.Width = 2.25F;
			// 
			// label5
			// 
			this.label5.Height = 0.1875F;
			this.label5.HyperLink = null;
			this.label5.Left = 4.141F;
			this.label5.Name = "label5";
			this.label5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic; text-align:" +
    " center";
			this.label5.Text = "TOTAL";
			this.label5.Top = 4.233F;
			this.label5.Width = 1.375F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.188F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-size: 9pt; text-align: left";
			this.Label124.Text = "40.";
			this.Label124.Top = 0.782F;
			this.Label124.Width = 0.313F;
			// 
			// label6
			// 
			this.label6.Height = 0.188F;
			this.label6.HyperLink = null;
			this.label6.Left = 0F;
			this.label6.Name = "label6";
			this.label6.Style = "font-size: 9pt; font-weight: bold; text-align: left";
			this.label6.Text = "40.";
			this.label6.Top = 4.483F;
			this.label6.Width = 0.313F;
			// 
			// label7
			// 
			this.label7.Height = 0.188F;
			this.label7.HyperLink = null;
			this.label7.Left = 0F;
			this.label7.Name = "label7";
			this.label7.Style = "font-size: 9pt; text-align: left";
			this.label7.Text = "41.";
			this.label7.Top = 5.070501F;
			this.label7.Width = 0.313F;
			// 
			// label8
			// 
			this.label8.Height = 0.188F;
			this.label8.HyperLink = null;
			this.label8.Left = 0F;
			this.label8.Name = "label8";
			this.label8.Style = "font-size: 9pt; text-align: left";
			this.label8.Text = "42.";
			this.label8.Top = 6.502501F;
			this.label8.Width = 0.313F;
			// 
			// label9
			// 
			this.label9.Height = 0.188F;
			this.label9.HyperLink = null;
			this.label9.Left = 0F;
			this.label9.Name = "label9";
			this.label9.Style = "font-size: 9pt; text-align: left";
			this.label9.Text = "43.";
			this.label9.Top = 6.94F;
			this.label9.Width = 0.313F;
			// 
			// label10
			// 
			this.label10.Height = 0.188F;
			this.label10.HyperLink = null;
			this.label10.Left = 0F;
			this.label10.Name = "label10";
			this.label10.Style = "font-size: 9pt; text-align: left";
			this.label10.Text = "44.";
			this.label10.Top = 7.19F;
			this.label10.Width = 0.313F;
			// 
			// label11
			// 
			this.label11.Height = 0.188F;
			this.label11.HyperLink = null;
			this.label11.Left = 0F;
			this.label11.Name = "label11";
			this.label11.Style = "font-size: 9pt; text-align: left";
			this.label11.Text = "40.";
			this.label11.Top = 1.7105F;
			this.label11.Width = 0.313F;
			// 
			// srptMVRPage7
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationContractor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLineRevaluationPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLineRevaluationBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLineRevaluationLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevaluationCompleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxableAcreage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberParcels)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxMapType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxMapPreparer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxMapDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHasTaxMaps)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptValue1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSolarAndWindExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSolarAndWindApproved)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSolarAndWindProcessed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptTotalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValueAllExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptValue2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptValue3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptProvision1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptProvision2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptProvision3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherExemptName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label187)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label188)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label192)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label193)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label194)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label195)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label196)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label197)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label198)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label199)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label200)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label201)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label202)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label207)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label208)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label210)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape43;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptTotalValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValueAllExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHasTaxMaps;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxMapDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxMapPreparer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumberParcels;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptValue1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptValue2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptValue3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptProvision1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptProvision2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptProvision3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherExemptName3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label185;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label186;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxableAcreage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label187;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRevaluationCompleted;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label188;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label192;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label193;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLineRevaluationLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label194;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLineRevaluationBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label195;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label196;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label197;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLineRevaluationPP;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label198;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label199;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label200;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label201;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label202;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape44;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRevaluationContractor;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRevaluationCost;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRevaluationEffectiveDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label207;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label208;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxMapType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label210;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label212;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label216;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSolarAndWindExempt;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSolarAndWindApproved;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSolarAndWindProcessed;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label213;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label217;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label label11;
	}
}
