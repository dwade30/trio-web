﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmFactorBillingValues.
	/// </summary>
	partial class frmFactorBillingValues : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbOverride;
		public fecherFoundation.FCLabel lblOverride;
		public fecherFoundation.FCComboBox cmbFactor;
		public fecherFoundation.FCCheckBox chkOnlyOverrides;
		public fecherFoundation.FCFrame framRange;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtFactor;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFactorBillingValues));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmbOverride = new fecherFoundation.FCComboBox();
            this.lblOverride = new fecherFoundation.FCLabel();
            this.cmbFactor = new fecherFoundation.FCComboBox();
            this.chkOnlyOverrides = new fecherFoundation.FCCheckBox();
            this.framRange = new fecherFoundation.FCFrame();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtFactor = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSaveContinue = new Wisej.Web.Button();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyOverrides)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
            this.framRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(565, 24);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSaveContinue);
            this.ClientArea.Controls.Add(this.chkOnlyOverrides);
            this.ClientArea.Controls.Add(this.framRange);
            this.ClientArea.Controls.Add(this.cmbOverride);
            this.ClientArea.Controls.Add(this.lblOverride);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(565, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(565, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(164, 30);
            this.HeaderText.Text = "Factor Values";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "Account",
            "Map/Lot",
            "Location",
            "Tran Code",
            "Land Code",
            "Building Code",
            "Neighborhood",
            "Zone"});
            this.cmbRange.Location = new System.Drawing.Point(107, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(170, 40);
            this.cmbRange.TabIndex = 19;
            this.cmbRange.Text = "Account";
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(51, 15);
            this.lblRange.TabIndex = 20;
            this.lblRange.Text = "RANGE";
            // 
            // cmbOverride
            // 
            this.cmbOverride.Items.AddRange(new object[] {
            "All",
            "Range"});
            this.cmbOverride.Location = new System.Drawing.Point(214, 140);
            this.cmbOverride.Name = "cmbOverride";
            this.cmbOverride.TabIndex = 22;
            this.cmbOverride.Text = "All";
            this.cmbOverride.SelectedIndexChanged += new System.EventHandler(this.optOverride_CheckedChanged);
            // 
            // lblOverride
            // 
            this.lblOverride.AutoSize = true;
            this.lblOverride.Location = new System.Drawing.Point(30, 154);
            this.lblOverride.Name = "lblOverride";
            this.lblOverride.Size = new System.Drawing.Size(158, 15);
            this.lblOverride.TabIndex = 23;
            this.lblOverride.Text = "RECORDS TO OVERRIDE";
            // 
            // cmbFactor
            // 
            this.cmbFactor.Items.AddRange(new object[] {
            "Land Values",
            "Building Values",
            "Outbuilding Sound Values"});
            this.cmbFactor.Location = new System.Drawing.Point(20, 30);
            this.cmbFactor.Name = "cmbFactor";
            this.cmbFactor.Size = new System.Drawing.Size(234, 40);
            this.cmbFactor.TabIndex = 7;
            this.cmbFactor.Text = "Land Values";
            this.cmbFactor.SelectedIndexChanged += new System.EventHandler(this.optFactor_CheckedChanged);
            // 
            // chkOnlyOverrides
            // 
            this.chkOnlyOverrides.Location = new System.Drawing.Point(30, 200);
            this.chkOnlyOverrides.Name = "chkOnlyOverrides";
            this.chkOnlyOverrides.Size = new System.Drawing.Size(317, 27);
            this.chkOnlyOverrides.TabIndex = 21;
            this.chkOnlyOverrides.Text = "Affect only previously overridden values";
            // 
            // framRange
            // 
            this.framRange.Controls.Add(this.txtEnd);
            this.framRange.Controls.Add(this.cmbRange);
            this.framRange.Controls.Add(this.lblRange);
            this.framRange.Controls.Add(this.txtStart);
            this.framRange.Controls.Add(this.Label2);
            this.framRange.Location = new System.Drawing.Point(30, 258);
            this.framRange.Name = "framRange";
            this.framRange.Size = new System.Drawing.Size(360, 152);
            this.framRange.TabIndex = 8;
            this.framRange.Text = "Range";
            this.framRange.Visible = false;
            // 
            // txtEnd
            // 
            this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnd.Location = new System.Drawing.Point(208, 90);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(132, 40);
            this.txtEnd.TabIndex = 18;
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(20, 90);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(132, 40);
            this.txtStart.TabIndex = 17;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(170, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(25, 17);
            this.Label2.TabIndex = 19;
            this.Label2.Text = "TO";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtFactor);
            this.Frame1.Controls.Add(this.cmbFactor);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(397, 90);
            this.Frame1.TabIndex = 24;
            this.Frame1.Text = "Factor";
            // 
            // txtFactor
            // 
            this.txtFactor.BackColor = System.Drawing.SystemColors.Window;
            this.txtFactor.Location = new System.Drawing.Point(321, 30);
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.Size = new System.Drawing.Size(54, 40);
            this.txtFactor.TabIndex = 6;
            this.txtFactor.Text = "1.0";
            this.txtFactor.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(274, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(25, 15);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "BY";
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(30, 447);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(152, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmFactorBillingValues
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(565, 604);
            this.FillColor = 0;
            this.Name = "frmFactorBillingValues";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Factor Values";
            this.Load += new System.EventHandler(this.frmFactorBillingValues_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFactorBillingValues_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyOverrides)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
            this.framRange.ResumeLayout(false);
            this.framRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSaveContinue;
	}
}
