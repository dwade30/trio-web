﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExemptionsByAccount.
	/// </summary>
	public partial class rptExemptionsByAccount : BaseSectionReport
	{
		public static rptExemptionsByAccount InstancePtr
		{
			get
			{
				return (rptExemptionsByAccount)Sys.GetInstance(typeof(rptExemptionsByAccount));
			}
		}

		protected rptExemptionsByAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                theReport = null;
            }
			base.Dispose(disposing);
		}

		public rptExemptionsByAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Exemptions By Account";
		}
		// nObj = 1
		//   0	rptExemptionsByAccount	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cExemptByAccountReport theReport;

		public void Init(ref cExemptByAccountReport eReport)
		{
			theReport = eReport;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !theReport.ExemptionRecords.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			theReport.ExemptionRecords.MoveFirst();
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtpage.Text = "Page 1";
			if (theReport.TownNumber > 0)
			{
				modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = theReport.TownNumber;
				txtmuni.Text = modGlobalVariables.Statics.CustomizedInfo.CurrentTownName;
			}
			else
			{
				txtmuni.Text = modGlobalConstants.Statics.MuniName;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(theReport == null))
			{
				if (theReport.ExemptionRecords.IsCurrent())
				{
					cAccountInfo REacct;
					cParty tParty = new cParty();
					string strSep = "";
					string strTemp = "";
					REacct = (cAccountInfo)theReport.ExemptionRecords.GetCurrentItem();
					txtTotal.Text = Strings.Format(REacct.ExemptTotal, "#,###,###,##0");
					txtAccount.Text = REacct.Account.ToString();
					strSep = "";
					strTemp = "";

					foreach (var owner in REacct.Owners)
					{
						strTemp += strSep + owner;
						strSep = "\r\n";
					}

					txtName.Text = strTemp;
					strSep = "";
					strTemp = "";
					string strAmount = "";
					strAmount = "";
					cExemptAssessInfo tEx;
					REacct.Exemptions.MoveFirst();
					while (REacct.Exemptions.IsCurrent())
					{
						tEx = (cExemptAssessInfo)REacct.Exemptions.GetCurrentItem();
						strAmount += strSep + Strings.Format(tEx.Amount, "#,###,###,##0");
						strTemp += strSep + tEx.Description;
						strSep = "\r\n";
						REacct.Exemptions.MoveNext();
					}
					txtAmount.Text = strAmount;
					txtExemptName.Text = strTemp;
					theReport.ExemptionRecords.MoveNext();
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtpage.Text = "Page " + this.PageNumber;
		}

		
	}
}
