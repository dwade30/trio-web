﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLoadReport.
	/// </summary>
	partial class frmLoadReport : BaseForm
	{
		public FCGrid Grid;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLoadReport));
            this.Grid = new fecherFoundation.FCGrid();
            this.cmdContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 369);
            this.BottomPanel.Size = new System.Drawing.Size(1033, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Size = new System.Drawing.Size(1033, 309);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1033, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(147, 30);
            this.HeaderText.Text = "Load Report";
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 10;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 30);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(983, 259);
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Location = new System.Drawing.Point(466, 30);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(100, 48);
            this.cmdContinue.Text = "Continue";
            this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // frmLoadReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1033, 477);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLoadReport";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Load Report";
            this.Load += new System.EventHandler(this.frmLoadReport_Load);
            this.Resize += new System.EventHandler(this.frmLoadReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLoadReport_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
