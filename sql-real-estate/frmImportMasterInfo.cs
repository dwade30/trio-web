﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Xml;
using System.IO;
using GrapeCity.ActiveReports.Rendering.Components.Map.Data.WellKnown;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportMasterInfo.
	/// </summary>
	public partial class frmImportMasterInfo : BaseForm
	{
		public frmImportMasterInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportMasterInfo InstancePtr
		{
			get
			{
				return (frmImportMasterInfo)Sys.GetInstance(typeof(frmImportMasterInfo));
			}
		}

		protected frmImportMasterInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolValidFile;
		StreamWriter ts;
		string strCYA = "";
		string strFileToLoad = "";
		int intICode;
		XmlDocument docObj;
		// vbPorter upgrade warning: curAcct As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		XmlElement curAcct;
		int lngAcct;
		int intCard;

		private void frmImportMasterInfo_Activated(object sender, System.EventArgs e)
		{
			if (!boolValidFile)
			{
				this.Unload();
				return;
			}
		}

		private void frmImportMasterInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}
		
		private void frmImportMasterInfo_Load(object sender, System.EventArgs e)
		{
			string strFileName = "";
			bool boolLoaded = false;
			//FileSystemObject fso = new FileSystemObject();
			string strTemp = "";
			string[] strLine = null;
			string strMuni = "";
			string strDate = "";
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				boolValidFile = false;
				while (!boolLoaded)
				{
					// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNPathMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
					//FC:FINAL:MSH - i.issue #1213: uncomment setting CancelError to true to prevent continue code execution if the 'Cancel' button clicked
					MDIParent.InstancePtr.CommonDialog1.CancelError = true;
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Choose File to Import";
					MDIParent.InstancePtr.CommonDialog1.FileName = "REExpFil.xml";
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
					if (File.Exists(strFileName))
					{
						if (GetInitInfoXML(ref strFileName))
						{
							boolLoaded = true;
							boolValidFile = true;
						}
					}
					else
					{
						MessageBox.Show("File does not exist", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 32755)
					return;
				// cancel button
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In form_load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool GetInitInfoXML(ref string strInfo)
		{
			bool GetInitInfoXML = false;
			try
			{
				// On Error GoTo ErrorHandler
				GetInitInfoXML = false;
				docObj = new XmlDocument();
				XmlNode tmpElement;
				string vbPorterVar;
				//docObj.async = false;
				docObj.Load(strInfo);
				if (docObj.DocumentElement.Name == "REExport")
				{
					tmpElement = docObj.SelectSingleNode("//REExport");
					if (MessageBox.Show("This file was created by " + tmpElement.Attributes[0].InnerText + " on " + tmpElement.Attributes[1].InnerText + "\r\n" + "Do you want to import this file?", "Import File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						//FC:FINAL:MSH - i.issue #1557: missing properties (do not need to use in VB6)
						//strCYA = "Imported " + tmpElement.Attributes["User"] + " from " + tmpElement.Attributes["Created"];
						strCYA = "Imported " + tmpElement.Attributes["User"].InnerText + " from " + tmpElement.Attributes["Created"].InnerText;
						tmpElement = tmpElement.SelectSingleNode("//ExportType");
						foreach (XmlNode curNode in tmpElement.ChildNodes)
						{
							if (curNode.NodeType == XmlNodeType.Element)
							{
								if (curNode.Name == "Include")
								{
									vbPorterVar = curNode.InnerText;
									if (vbPorterVar == "Owner")
									{
										ChkNameAddress.Enabled = true;
										ChkNameAddress.CheckState = Wisej.Web.CheckState.Checked;
									}
                                    else if (vbPorterVar == "DeedNames")
                                    {
                                        chkDeedNames.Enabled = true;
                                        chkDeedNames.CheckState = Wisej.Web.CheckState.Checked;
                                    }
									else if (vbPorterVar == "MapLot")
									{
										chkMapLot.Enabled = true;
										chkMapLot.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Location")
									{
										chkLocation.Enabled = true;
										chkLocation.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "BookPage")
									{
										chkBookPage.Enabled = true;
										chkBookPage.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "TaxAcquired")
									{
										chkTaxAcquired.Enabled = true;
										chkTaxAcquired.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Bankruptcy")
									{
										chkBankruptcy.Enabled = true;
										chkBankruptcy.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Exempt")
									{
										chkExemptInfo.Enabled = true;
										chkExemptInfo.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Sale")
									{
										chkSaleData.Enabled = true;
										chkSaleData.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "TranLandBldgProp")
									{
										ChkTranLandBldg.Enabled = true;
										ChkTranLandBldg.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Reference1")
									{
										ChkRef1.Enabled = true;
										ChkRef1.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Reference2")
									{
										chkRef2.Enabled = true;
										chkRef2.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Land")
									{
										chkLandInformation.Enabled = true;
										chkLandInformation.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Property")
									{
										chkPropertyInfo.Enabled = true;
										chkPropertyInfo.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Inspection")
									{
										chkInspection.Enabled = true;
										chkInspection.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Dwelling")
									{
										chkDwelling.Enabled = true;
										chkDwelling.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Commercial")
									{
										chkCommercial.Enabled = true;
										chkCommercial.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Outbuilding")
									{
										chkOutbuilding.Enabled = true;
										chkOutbuilding.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "InterestedParties")
									{
										chkInterestedParties.Enabled = true;
										chkInterestedParties.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "PreviousOwners")
									{
										chkPreviousOwners.Enabled = true;
										chkPreviousOwners.CheckState = Wisej.Web.CheckState.Checked;
									}
									else if (vbPorterVar == "Pictures")
									{
										chkPictures.Enabled = true;
										chkPictures.CheckState = Wisej.Web.CheckState.Checked;
									}
								}
							}
						}
						// curNode
						GetInitInfoXML = true;
					}
				}
				else
				{
					MessageBox.Show("This file does not appear to be in the correct format" + "\r\n" + "Cannot load this format", "Bad File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return GetInitInfoXML;
				}
				return GetInitInfoXML;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetInitInfoXML", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetInitInfoXML;
		}
		
		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (ImportFile())
			{
				//FC:FINAL:MSH - i.issue #1557: data will be missed and report won't be initialized after closing form
				//this.Unload();
				this.Hide();
			}
		}

		private bool ImportFile()
		{
			bool ImportFile = false;
			int lngIndex;
			string strTemp = "";
			bool boolUseNameAddress = false;
            bool boolUseDeedName = false;
			bool boolUseMaplot = false;
			bool boolUseBookPage = false;
			bool boolUseTaxAcquired = false;
			bool boolUseBankruptcy = false;
			bool boolUseExemptInformation = false;
			bool boolUseSaleData = false;
			bool boolUseTranLandBldgProp = false;
			bool boolUseRef1 = false;
			bool boolUseRef2 = false;
			bool boolUseLandInformation = false;
			bool boolUsePropertyInfo = false;
			bool boolUseLocation = false;
			bool boolUseDwelling = false;
			bool boolUseCommercial = false;
			bool boolUseOutbuilding = false;
			bool boolUseInterestedParties = false;
			bool boolUsePreviousOwners = false;
			bool boolUsePictures = false;
			bool boolUseInspection = false;
			int intCommercialIndex = 0;
			int intOutbuildingIndex = 0;
			int intPictureIndex = 0;
			// vbPorter upgrade warning: curElement As MSXML2.IXMLDOMNode	OnRead(XmlElement)
			XmlElement curElement;
			XmlNodeList AccountList;
			XmlNodeList PartyList;
			XmlElement subNode;
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				modGlobalFunctions.AddCYAEntry_8("RE", "Import Name Etc.", strCYA);
				ImportFile = false;
				if (ChkNameAddress.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseNameAddress = true;
				}

                if (chkDeedNames.CheckState == CheckState.Checked)
                {
                    boolUseDeedName = true;
                }
				if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseMaplot = true;
				}
				if (chkLocation.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseLocation = true;
				}
				if (chkBookPage.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseBookPage = true;
				}
				if (chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseTaxAcquired = true;
				}
				if (chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseBankruptcy = true;
				}
				if (chkExemptInfo.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseExemptInformation = true;
				}
				if (chkSaleData.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseSaleData = true;
				}
				if (ChkTranLandBldg.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseTranLandBldgProp = true;
				}
				if (ChkRef1.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseRef1 = true;
				}
				if (chkRef2.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseRef2 = true;
				}
				if (chkLandInformation.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseLandInformation = true;
				}
				if (chkPropertyInfo.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUsePropertyInfo = true;
				}
				if (chkDwelling.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseDwelling = true;
				}
				if (chkCommercial.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseCommercial = true;
				}
				if (chkOutbuilding.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseOutbuilding = true;
				}
				if (chkInterestedParties.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseInterestedParties = true;
				}
				if (chkPreviousOwners.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUsePreviousOwners = true;
				}
				if (chkPictures.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUsePictures = true;
				}
				if (chkInspection.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseInspection = true;
				}
				Grid.Rows = 0;
				lblUpdating.Visible = true;
				if (boolUseNameAddress)
				{
					//Application.DoEvents();
					PartyList = docObj.GetElementsByTagName("Party");
					cParty tParty = new cParty();
					// vbPorter upgrade warning: tempParty As cParty	OnWrite(cParty)
					cParty tempParty = new cParty();
					int lngTempID = 0;
					cPartyController tPC = new cPartyController();
					// Dim tPCol As New Collection
					foreach (XmlElement curElement_foreach in PartyList)
					{
						curElement = curElement_foreach;
						// check if that party exists or not
						//Application.DoEvents();
						tParty = PartyFromXML(ref curElement);
						if (!(tParty == null))
						{
							// lblUpdating.Caption = "Checking Party " & tParty.ID
							// lblUpdating.Refresh
							// DoEvents
							tempParty = tPC.GetPartyByGUID(tParty.PartyGUID, true);
							if (tempParty == null)
							{
								tPC.SaveParty(ref tParty, false, true);
								lngTempID = tParty.ID;
							}
							else
							{
								if (tempParty.ID < 1)
								{
									tPC.SaveParty(ref tParty, false, true);
									lngTempID = tParty.ID;
								}
								else
								{
									lngTempID = tempParty.ID;
								}
							}
							lblUpdating.Text = "Checking Party " + FCConvert.ToString(lngTempID);
							lblUpdating.Refresh();
							//FC:FINAL:MSH - i.issue #1557: update label
							FCUtils.ApplicationUpdate(lblUpdating);
							//Application.DoEvents();
						}
						curElement = null;
					}
					// curElement
				}
				AccountList = docObj.GetElementsByTagName("Account");
				// For lngIndex = 0 To AccountList.Length - 1
				//FC:FINAL:MSH - i.issue #1557: replace foreach cycle with checking type of the current XmlNode to prevent InvalidCastException 
				//foreach (XmlElement curElement_foreach in AccountList)
				//{
				//} // curElement
				for (int i = 0; i < AccountList.Count; i++)
				{
					var tempCurNode = AccountList[i];
					// Set curAcct = AccountList(lngIndex)
					//Application.DoEvents();
					if (tempCurNode.NodeType == XmlNodeType.Element)
					{
						curElement = (XmlElement)tempCurNode;
						if (curElement.Name == "Account")
						{
							intCommercialIndex = 0;
							intOutbuildingIndex = 0;
							intPictureIndex = 0;
							curAcct = curElement;
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(curAcct.GetAttribute("Number"))));
							intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(curAcct.GetAttribute("Card"))));
							lblUpdating.Text = "Updating Account " + FCConvert.ToString(lngAcct);
							//FC:FINAL:MSH - i.issue #1557: update label
							lblUpdating.Refresh();
							FCUtils.ApplicationUpdate(lblUpdating);
							//Application.DoEvents();
							if (intCard == 1)
							{
								Grid.AddItem(FCConvert.ToString(lngAcct) + "\t" + FCConvert.ToString(intCard));
								if (boolUseBookPage)
								{
									rsSave.Execute("delete from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
								}
							}
							if (boolUseDwelling)
							{
								rsSave.Execute("delete from dwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								if (boolUseCommercial)
								{
									rsSave.Execute("update master set rsdwellingcode = 'N' where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsSave.Execute("update master set rsdwellingcode = 'N' where not rsdwellingcode = 'C' and rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								}
							}
							if (boolUseCommercial)
							{
								rsSave.Execute("delete from commercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								if (boolUseDwelling)
								{
									rsSave.Execute("update master set rsdwellingcode = 'N' where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								}
								else
								{
									rsSave.Execute("update master set rsdwellingcode = 'N' where not rsdwellingcode = 'Y' and rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								}
							}
							if (boolUseOutbuilding)
							{
								rsSave.Execute("delete from outbuilding where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
								rsSave.Execute("update master set rsoutbuildingcode = 'N' where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
							}
							if (boolUsePreviousOwners && intCard == 1)
							{
								rsSave.Execute("delete from previousowner where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
								rsSave.Execute("delete from owners where associd > 0 and account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							}
							if (boolUseInterestedParties && intCard == 1)
							{
								rsSave.Execute("delete from owners where account = " + FCConvert.ToString(lngAcct) + " and associd = 0", modGlobalVariables.strREDatabase);
							}
							if (boolUsePictures)
							{
								rsSave.Execute("delete from picturerecord where mraccountnumber = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
							}

							for (int j = 0; j < curAcct.ChildNodes.Count; j++)
							{
								var tempNode = curAcct.ChildNodes[j];
								//Application.DoEvents();
								if (tempNode.NodeType == XmlNodeType.Element)
								{
									subNode = (XmlElement)tempNode;
									string vbPorterVar = subNode.Name;
									if (vbPorterVar == "Owner")
									{
										if (boolUseNameAddress)
										{
											if (!ImportNameAddress(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
                                    else if (vbPorterVar == "DeedNames")
                                    {
                                        if (boolUseDeedName)
                                        {
                                            if (!ImportDeedNames(ref subNode))
                                            {
                                                MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Hand);
                                            }
                                        }
                                    }
									else if (vbPorterVar == "MapLot")
									{
										if (boolUseMaplot)
										{
											if (!ImportMapLot(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Location")
									{
										if (boolUseLocation)
										{
											if (!ImportLocation(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "BookPage")
									{
										if (boolUseBookPage)
										{
											if (!ImportBookPage(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Reference1")
									{
										if (boolUseRef1)
										{
											if (!ImportRef1(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Reference2")
									{
										if (boolUseRef2)
										{
											if (!ImportRef2(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "TaxAcquired")
									{
										if (boolUseTaxAcquired)
										{
											if (!ImportTaxAcquired(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Bankruptcy")
									{
										if (boolUseBankruptcy)
										{
											if (!ImportBankruptcy(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "TranCode")
									{
										if (boolUseTranLandBldgProp)
										{
											if (!ImportTranCode(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "BuildingCode")
									{
										if (boolUseTranLandBldgProp)
										{
											if (!ImportbldgCode(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "PropertyCode")
									{
										if (boolUseTranLandBldgProp)
										{
											if (!ImportPropertyCode(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "LandCode")
									{
										if (boolUseTranLandBldgProp)
										{
											if (!ImportLandCode(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Property")
									{
										if (boolUsePropertyInfo)
										{
											if (!ImportProperty(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Inspection")
									{
										if (boolUseInspection)
										{
											if (!ImportInspection(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Sale")
									{
										if (boolUseSaleData)
										{
											if (!ImportSale(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Exemptions")
									{
										if (boolUseExemptInformation)
										{
											if (!ImportExempts(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "LandData")
									{
										if (boolUseLandInformation)
										{
											if (!ImportLand(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "InterestedParty")
									{
										if (boolUseInterestedParties)
										{
											if (!ImportInterestedParties(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "PreviousOwner")
									{
										if (boolUsePreviousOwners)
										{
											if (!ImportPreviousOwners(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Dwelling")
									{
										if (boolUseDwelling)
										{
											if (!ImportDwelling(ref subNode))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
										}
									}
									else if (vbPorterVar == "Commercial")
									{
										if (boolUseCommercial)
										{
											if (!ImportCommercial(ref subNode, ref intCommercialIndex))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
											intCommercialIndex += 1;
										}
									}
									else if (vbPorterVar == "Outbuilding")
									{
										if (boolUseOutbuilding)
										{
											if (!ImportOutbuilding(ref subNode, ref intOutbuildingIndex))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
											intOutbuildingIndex += 1;
										}
									}
									else if (vbPorterVar == "Picture")
									{
										if (boolUsePictures)
										{
											if (!ImportPictures(ref subNode, ref intPictureIndex))
											{
												MessageBox.Show("Error processing file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
												return ImportFile;
											}
											intPictureIndex += 1;
										}
									}
								}
							}
						}
					}
					curElement = null;
				}
				lblUpdating.Visible = false;
				ImportFile = true;
				MessageBox.Show("Data Imported", "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
				rptImportNameEtc.InstancePtr.Init(this.Modal);
				return ImportFile;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportFile;
		}
		// vbPorter upgrade warning: curEl As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportTranCode(ref XmlElement curEl)
		{
			bool ImportTranCode = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngCode;
				ImportTranCode = false;
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curEl.InnerText)));
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,ritrancode from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("ritrancode", lngCode);
				rsSave.Update();
				ImportTranCode = true;
				return ImportTranCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportTranCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportTranCode;
		}
		// vbPorter upgrade warning: curEl As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportLandCode(ref XmlElement curEl)
		{
			bool ImportLandCode = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngCode;
				ImportLandCode = false;
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curEl.InnerText)));
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,rilandcode from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("rilandcode", lngCode);
				rsSave.Update();
				ImportLandCode = true;
				return ImportLandCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportLandCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportLandCode;
		}
		// vbPorter upgrade warning: curEl As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportbldgCode(ref XmlElement curEl)
		{
			bool ImportbldgCode = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngCode;
				ImportbldgCode = false;
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curEl.InnerText)));
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,ribldgcode from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("ribldgcode", lngCode);
				rsSave.Update();
				ImportbldgCode = true;
				return ImportbldgCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportBldgCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportbldgCode;
		}
		// vbPorter upgrade warning: curEl As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportPropertyCode(ref XmlElement curEl)
		{
			bool ImportPropertyCode = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngCode;
				ImportPropertyCode = false;
				lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curEl.InnerText)));
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,propertycode from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("propertycode", lngCode);
				rsSave.Update();
				ImportPropertyCode = true;
				return ImportPropertyCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportPropertyCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportPropertyCode;
		}
		// vbPorter upgrade warning: curBank As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportBankruptcy(ref XmlElement curBank)
		{
			bool ImportBankruptcy = false;
			try
			{
				// On Error GoTo ErrorHandler
				bool boolBank;
				ImportBankruptcy = false;
				boolBank = FCConvert.CBool(curBank.InnerText);
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,inbankruptcy from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("inbankruptcy", boolBank);
				rsSave.Update();
				ImportBankruptcy = true;
				return ImportBankruptcy;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportBankruptcy", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportBankruptcy;
		}
		// vbPorter upgrade warning: curTax As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportTaxAcquired(ref XmlElement curTax)
		{
			bool ImportTaxAcquired = false;
			bool boolTax;
			try
			{
				ImportTaxAcquired = false;
				boolTax = FCConvert.CBool(curTax.InnerText);
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,TaxAcquired from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("taxacquired", boolTax);
				rsSave.Update();
				ImportTaxAcquired = true;
				return ImportTaxAcquired;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportTaxAcquired", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportTaxAcquired;
		}
		// vbPorter upgrade warning: curRef As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportRef1(ref XmlElement curRef)
		{
			bool ImportRef1 = false;
			string strRef;
			try
			{
				ImportRef1 = false;
				strRef = curRef.InnerText;
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,rsref1 from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("rsref1", strRef);
				rsSave.Update();
				ImportRef1 = true;
				return ImportRef1;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportRef1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportRef1;
		}
		// vbPorter upgrade warning: curRef As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportRef2(ref XmlElement curRef)
		{
			bool ImportRef2 = false;
			string strRef;
			try
			{
				ImportRef2 = false;
				strRef = curRef.InnerText;
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,rsref2 from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("rsref2", strRef);
				rsSave.Update();
				ImportRef2 = true;
				return ImportRef2;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Importref2", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportRef2;
		}
		// vbPorter upgrade warning: curProp As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportInspection(ref XmlElement curProp)
		{
			bool ImportInspection = false;
			string strDate;
			int intEntranceCode;
			int intInformationCode;
			string vbPorterVar;
			intEntranceCode = 0;
			intInformationCode = 0;
			strDate = "";
			try
			{
				// On Error GoTo ErrorHandler
				ImportInspection = false;
				foreach (XmlNode curNode in curProp.ChildNodes)
				{
					if (curNode.NodeType == XmlNodeType.Element)
					{
						vbPorterVar = curNode.Name;
						if (vbPorterVar == "InspectionDate")
						{
							if (Information.IsDate(curNode.InnerText))
							{
								strDate = curNode.InnerText;
							}
						}
						else if (vbPorterVar == "EntranceCode")
						{
							intEntranceCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "InformationCode")
						{
							intInformationCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("EntranceCode", intEntranceCode);
				rsSave.Set_Fields("InformationCode", intInformationCode);
				if (strDate != "")
				{
					rsSave.Set_Fields("DateInspected", strDate);
				}
				rsSave.Update();
				ImportInspection = true;
				return ImportInspection;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportInspection;
		}
		// vbPorter upgrade warning: curProp As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportProperty(ref XmlElement curProp)
		{
			bool ImportProperty = false;
			int intNeighborhood;
			int lngTreeGrowth;
			string strXCoord;
			string strYCoord;
			int intZone;
			int intSecZone;
			bool boolOverrideZone;
			int intStreet;
			int lngOpen1;
			int lngOpen2;
			int[] intTopography = new int[2 + 1];
			int[] intUtilities = new int[2 + 1];
			int intUIndex;
			int intTIndex;
			string strDate;
			int intEntranceCode;
			int intInformationCode;
			try
			{
				// On Error GoTo ErrorHandler
				ImportProperty = false;
				intNeighborhood = 0;
				lngTreeGrowth = 0;
				strXCoord = "";
				strYCoord = "";
				intZone = 0;
				intSecZone = 0;
				boolOverrideZone = false;
				intStreet = 0;
				lngOpen1 = 0;
				lngOpen2 = 0;
				intTopography[0] = 0;
				intTopography[1] = 0;
				intUtilities[0] = 0;
				intUtilities[1] = 0;
				intUIndex = 0;
				intTIndex = 0;
				intEntranceCode = 0;
				intInformationCode = 0;
				strDate = "";
				string vbPorterVar;
				foreach (XmlNode curNode in curProp.ChildNodes)
				{
					//Application.DoEvents();
					if (curNode.NodeType == XmlNodeType.Element)
					{
						vbPorterVar = curNode.Name;
						if (vbPorterVar == "Neighborhood")
						{
							intNeighborhood = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "TreeGrowthYear")
						{
							lngTreeGrowth = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "XCoord")
						{
							strXCoord = curNode.InnerText;
						}
						else if (vbPorterVar == "YCoord")
						{
							strYCoord = curNode.InnerText;
						}
						else if (vbPorterVar == "Utilities")
						{
							intUtilities[intUIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							intUIndex += 1;
						}
						else if (vbPorterVar == "Topography")
						{
							intTopography[intTIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							intTIndex += 1;
						}
						else if (vbPorterVar == "Zone")
						{
							intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "SecondaryZone")
						{
							intSecZone = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "OverrideZone")
						{
							boolOverrideZone = FCConvert.CBool(curNode.InnerText);
						}
						else if (vbPorterVar == "Street")
						{
							intStreet = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Open1")
						{
							lngOpen1 = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Open2")
						{
							lngOpen2 = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "InspectionDate")
						{
							if (Information.IsDate(curNode.InnerText))
							{
								strDate = curNode.InnerText;
							}
						}
						else if (vbPorterVar == "EntranceCode")
						{
							intEntranceCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "InformationCode")
						{
							intInformationCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("pineighborhood", intNeighborhood);
				rsSave.Set_Fields("pistreetcode", lngTreeGrowth);
				rsSave.Set_Fields("pixcoord", strXCoord);
				rsSave.Set_Fields("piycoord", strYCoord);
				rsSave.Set_Fields("piutilities1", intUtilities[0]);
				rsSave.Set_Fields("piutilities2", intUtilities[1]);
				rsSave.Set_Fields("pitopography1", intTopography[0]);
				rsSave.Set_Fields("pitopography2", intTopography[1]);
				rsSave.Set_Fields("pizone", intZone);
				rsSave.Set_Fields("piseczone", intSecZone);
				rsSave.Set_Fields("ZoneOverride", boolOverrideZone);
				rsSave.Set_Fields("pistreet", intStreet);
				rsSave.Set_Fields("piopen1", lngOpen1);
				rsSave.Set_Fields("piopen2", lngOpen2);
				rsSave.Set_Fields("EntranceCode", intEntranceCode);
				rsSave.Set_Fields("InformationCode", intInformationCode);
				if (strDate != "")
				{
					rsSave.Set_Fields("DateInspected", strDate);
				}
				rsSave.Update();
				ImportProperty = true;
				return ImportProperty;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportProperty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportProperty;
		}
		// vbPorter upgrade warning: curSale As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportSale(ref XmlElement curSale)
		{
			bool ImportSale = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngSalePrice;
				// vbPorter upgrade warning: dtSaleDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtSaleDate;
				int intValidity;
				int intVerified;
				int intFinanced;
				int intType;
				string vbPorterVar = "";
				ImportSale = false;
				lngSalePrice = 0;
				dtSaleDate = DateTime.FromOADate(0);
				intValidity = 0;
				intVerified = 0;
				intFinanced = 0;
				intType = 0;
				if (curSale.HasChildNodes)
				{
					foreach (XmlNode curNode in curSale.ChildNodes)
					{
						//Application.DoEvents();
						vbPorterVar = curNode.Name;
						if (vbPorterVar == "SaleDate")
						{
							if (Information.IsDate(curNode.InnerText))
							{
								dtSaleDate = Convert.ToDateTime(curNode.InnerText);
							}
						}
						else if (vbPorterVar == "Price")
						{
							lngSalePrice = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Validity")
						{
							intValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Verified")
						{
							intVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Type")
						{
							intType = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Financing")
						{
							intFinanced = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
					}
					// curNode
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("pisaleprice", lngSalePrice);
				rsSave.Set_Fields("pisalevalidity", intValidity);
				rsSave.Set_Fields("pisalefinancing", intFinanced);
				rsSave.Set_Fields("pisaleverified", intVerified);
				rsSave.Set_Fields("saledate", dtSaleDate);
				rsSave.Set_Fields("pisaletype", intType);
				rsSave.Update();
				ImportSale = true;
				return ImportSale;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportSale", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportSale;
		}
		// vbPorter upgrade warning: curExempt As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportExempts(ref XmlElement curExempt)
		{
			bool ImportExempts = false;
			int[] intCode = new int[3 + 1];
			double[] dblPerc = new double[3 + 1];
			int intIndex;
			try
			{
				// On Error GoTo ErrorHandler
				ImportExempts = false;
				for (intIndex = 0; intIndex <= 2; intIndex++)
				{
					intCode[intIndex] = 0;
					dblPerc[intIndex] = 100;
				}
				// intIndex
				intIndex = 0;
				if (curExempt.HasChildNodes)
				{
					foreach (XmlNode curNode in curExempt.ChildNodes)
					{
						if (curNode.Name == "Exemption")
						{
							intCode[intIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							dblPerc[intIndex] = Conversion.Val(curNode.Attributes[0].InnerText);
							intIndex += 1;
						}
					}
					// curNode
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				for (intIndex = 0; intIndex <= 2; intIndex++)
				{
					rsSave.Set_Fields("riexemptcd" + intIndex + 1, intCode[intIndex]);
					rsSave.Set_Fields("exemptpct" + intIndex + 1, dblPerc[intIndex]);
				}
				// intIndex
				rsSave.Update();
				ImportExempts = true;
				return ImportExempts;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportExempts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportExempts;
		}
		// vbPorter upgrade warning: curLand As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportLand(ref XmlElement curLand)
		{
			bool ImportLand = false;
			try
			{
				// On Error GoTo ErrorHandler
				int[] lngLandCode = new int[7 + 1];
				string[] strUnitsA = new string[7 + 1];
				string[] strUnitsB = new string[7 + 1];
				double[] dblInfluence = new double[7 + 1];
				int[] intInfCode = new int[7 + 1];
				int intIndex;
				double dblAcres;
				double dblSoftA;
				double dblMixedA;
				double dblHardA;
				double dblOtherA;
				int lngSoft;
				int lngMixed;
				int lngHard;
				int lngOther;
				ImportLand = false;
				for (intIndex = 0; intIndex <= 6; intIndex++)
				{
					//Application.DoEvents();
					lngLandCode[intIndex] = 0;
					strUnitsA[intIndex] = "000";
					strUnitsB[intIndex] = "000";
					dblInfluence[intIndex] = 100;
					intInfCode[intIndex] = 0;
				}
				// intIndex
				dblAcres = 0;
				dblSoftA = 0;
				dblMixedA = 0;
				dblHardA = 0;
				dblOtherA = 0;
				lngSoft = 0;
				lngMixed = 0;
				lngHard = 0;
				lngOther = 0;
				string vbPorterVar1 = "";
				string vbPorterVar = "";
				intIndex = 0;
				if (curLand.HasChildNodes)
				{
					foreach (XmlNode curEntry in curLand.ChildNodes)
					{
						vbPorterVar1 = curEntry.Name;
						if (vbPorterVar1 == "Land")
						{
							foreach (XmlNode curNode in curEntry.ChildNodes)
							{
								vbPorterVar = curNode.Name;
								if (vbPorterVar == "LandType")
								{
									lngLandCode[intIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
								}
								else if (vbPorterVar == "UnitsA")
								{
									strUnitsA[intIndex] = curNode.InnerText;
								}
								else if (vbPorterVar == "UnitsB")
								{
									strUnitsB[intIndex] = curNode.InnerText;
								}
								else if (vbPorterVar == "Influence")
								{
									dblInfluence[intIndex] = Conversion.Val(curNode.InnerText);
								}
								else if (vbPorterVar == "InfluenceCode")
								{
									intInfCode[intIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
								}
							}
							// curNode
							intIndex += 1;
						}
						else if (vbPorterVar1 == "Acres")
						{
							dblAcres = Conversion.Val(curEntry.InnerText);
						}
						else if (vbPorterVar1 == "SoftAcres")
						{
							dblSoftA = Conversion.Val(curEntry.InnerText);
						}
						else if (vbPorterVar1 == "MixedAcres")
						{
							dblMixedA = Conversion.Val(curEntry.InnerText);
						}
						else if (vbPorterVar1 == "HardAcres")
						{
							dblHardA = Conversion.Val(curEntry.InnerText);
						}
						else if (vbPorterVar1 == "OtherAcres")
						{
							dblOtherA = Conversion.Val(curEntry.InnerText);
						}
						else if (vbPorterVar1 == "SoftValue")
						{
							lngSoft = FCConvert.ToInt32(Math.Round(Conversion.Val(curEntry.InnerText)));
						}
						else if (vbPorterVar1 == "MixedValue")
						{
							lngMixed = FCConvert.ToInt32(Math.Round(Conversion.Val(curEntry.InnerText)));
						}
						else if (vbPorterVar1 == "HardValue")
						{
							lngHard = FCConvert.ToInt32(Math.Round(Conversion.Val(curEntry.InnerText)));
						}
						else if (vbPorterVar1 == "OtherValue")
						{
							lngOther = FCConvert.ToInt32(Math.Round(Conversion.Val(curEntry.InnerText)));
						}
					}
					// curEntry
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				int intField;
				for (intIndex = 0; intIndex <= 6; intIndex++)
				{
					intField = intIndex + 1;
					rsSave.Set_Fields("piland" + intField + "type", lngLandCode[intIndex]);
					rsSave.Set_Fields("piland" + intField + "unitsa", strUnitsA[intIndex]);
					rsSave.Set_Fields("piland" + intField + "unitsb", strUnitsB[intIndex]);
					rsSave.Set_Fields("piland" + intField + "inf", dblInfluence[intIndex]);
					rsSave.Set_Fields("piland" + intField + "infcode", intInfCode[intIndex]);
				}
				// intIndex
				rsSave.Set_Fields("piacres", dblAcres);
				rsSave.Set_Fields("rssoft", dblSoftA);
				rsSave.Set_Fields("rsmixed", dblMixedA);
				rsSave.Set_Fields("rshard", dblHardA);
				rsSave.Set_Fields("rsother", dblOtherA);
				rsSave.Set_Fields("rssoftvalue", lngSoft);
				rsSave.Set_Fields("rsmixedvalue", lngMixed);
				rsSave.Set_Fields("rshardvalue", lngHard);
				rsSave.Set_Fields("rsothervalue", lngOther);
				rsSave.Update();
				ImportLand = true;
				return ImportLand;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportLand", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportLand;
		}
		// vbPorter upgrade warning: curParty As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportInterestedParties(ref XmlElement curParty)
		{
			bool ImportInterestedParties = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strName;
				string[] straddr = new string[2 + 1];
				string strCity;
				string strState;
				string strZip;
				string strZip4;
				int lngID;
				int intIndex;
				ImportInterestedParties = false;
				strName = "";
				straddr[0] = "";
				straddr[1] = "";
				strCity = "";
				strState = "";
				strZip = "";
				strZip4 = "";
				lngID = 0;
				string vbPorterVar1 = "";
				string vbPorterVar = "";
				intIndex = 0;
				foreach (XmlNode curNode in curParty.ChildNodes)
				{
					//Application.DoEvents();
					if (curNode.NodeType == XmlNodeType.Element)
					{
						vbPorterVar1 = curNode.Name;
						if (vbPorterVar1 == "Name")
						{
							strName = curNode.InnerText;
						}
						else if (vbPorterVar1 == "AssociateID")
						{
							lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar1 == "Address")
						{
							foreach (XmlNode curEl in curNode.ChildNodes)
							{
								vbPorterVar = curEl.Name;
								if (vbPorterVar == "Addr")
								{
									straddr[intIndex] = curEl.InnerText;
									intIndex += 1;
								}
								else if (vbPorterVar == "City")
								{
									strCity = curEl.InnerText;
								}
								else if (vbPorterVar == "State")
								{
									strState = curEl.InnerText;
								}
								else if (vbPorterVar == "Zip")
								{
									strZip = curEl.InnerText;
								}
								else if (vbPorterVar == "Zip4")
								{
									strZip4 = curEl.InnerText;
								}
							}
							// curEl
						}
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from owners where id = -1", modGlobalVariables.strREDatabase);
				rsSave.AddNew();
				rsSave.Set_Fields("Name", strName);
				rsSave.Set_Fields("address1", straddr[0]);
				rsSave.Set_Fields("address2", straddr[1]);
				rsSave.Set_Fields("city", strCity);
				rsSave.Set_Fields("state", strState);
				rsSave.Set_Fields("zip", strZip);
				rsSave.Set_Fields("zip4", strZip4);
				rsSave.Set_Fields("AssocID", lngID);
				rsSave.Update();
				ImportInterestedParties = true;
				return ImportInterestedParties;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportInterestedParties", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportInterestedParties;
		}
		// vbPorter upgrade warning: curPic As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private bool ImportPictures(ref XmlElement curPic, ref int intIndex)
		{
			bool ImportPictures = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strPath;
				int intPNum;
				clsDRWrapper rsSave = new clsDRWrapper();
				string strDescription;
				string vbPorterVar = "";
				intPNum = intIndex + 1;
				ImportPictures = false;
				strPath = "";
				strDescription = "";
				foreach (XmlNode curNode in curPic.ChildNodes)
				{
					//Application.DoEvents();
					if (curNode.NodeType == XmlNodeType.Element)
					{
						vbPorterVar = curNode.Name;
						if (vbPorterVar == "Path")
						{
							strPath = Strings.Trim(curNode.InnerText);
						}
						else if (vbPorterVar == "Description")
						{
							strDescription = Strings.Trim(curNode.InnerText);
						}
					}
				}
				// curNode
				if (strPath != string.Empty)
				{
					rsSave.OpenRecordset("select * from picturerecord where mraccountnumber = -5", modGlobalVariables.strREDatabase);
					rsSave.AddNew();
					rsSave.Set_Fields("mraccountnumber", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
					rsSave.Set_Fields("picturelocation", strPath);
					rsSave.Set_Fields("description", strDescription);
					rsSave.Set_Fields("picnum", intPNum);
					rsSave.Update();
				}
				ImportPictures = true;
				return ImportPictures;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportPictures", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportPictures;
		}
		// vbPorter upgrade warning: curOwner As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportPreviousOwners(ref XmlElement curOwner)
		{
			bool ImportPreviousOwners = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strName;
				string strSecOwner;
				// vbPorter upgrade warning: dtSaleDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtSaleDate;
				// vbPorter upgrade warning: dtDateCreated As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtDateCreated;
				string[] straddr = new string[2 + 1];
				string strCity;
				string strState = "";
				string strZip;
				string strZip4;
				string vbPorterVar1 = "";
				string vbPorterVar = "";
				int intIndex;
				ImportPreviousOwners = false;
				strName = "";
				strSecOwner = "";
				dtSaleDate = DateTime.FromOADate(0);
				dtDateCreated = DateTime.FromOADate(0);
				straddr[0] = "";
				straddr[1] = "";
				strCity = "";
				strZip = "";
				strZip4 = "";
				intIndex = 0;
				foreach (XmlNode curNode in curOwner.ChildNodes)
				{
					//Application.DoEvents();
					if (curNode.NodeType == XmlNodeType.Element)
					{
						vbPorterVar1 = curNode.Name;
						if (vbPorterVar1 == "Name")
						{
							strName = Strings.Trim(curNode.InnerText);
						}
						else if (vbPorterVar1 == "SecondOwner")
						{
							strSecOwner = Strings.Trim(curNode.InnerText);
						}
						else if (vbPorterVar1 == "SaleDate")
						{
							if (Information.IsDate(curNode.InnerText))
							{
								dtSaleDate = Convert.ToDateTime(curNode.InnerText);
							}
						}
						else if (vbPorterVar1 == "DateCreated")
						{
							if (Information.IsDate(curNode.InnerText))
							{
								dtDateCreated = Convert.ToDateTime(curNode.InnerText);
							}
						}
						else if (vbPorterVar1 == "Address")
						{
							foreach (XmlNode curEl in curNode.ChildNodes)
							{
								vbPorterVar = curEl.Name;
								if (vbPorterVar == "Addr")
								{
									straddr[intIndex] = curEl.InnerText;
									intIndex += 1;
								}
								else if (vbPorterVar == "City")
								{
									strCity = curEl.InnerText;
								}
								else if (vbPorterVar == "State")
								{
									strState = curEl.InnerText;
								}
								else if (vbPorterVar == "Zip")
								{
									strZip = curEl.InnerText;
								}
								else if (vbPorterVar == "Zip4")
								{
									strZip4 = curEl.InnerText;
								}
							}
							// curEl
						}
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from previousowner where id = -5", modGlobalVariables.strREDatabase);
				rsSave.AddNew();
				rsSave.Set_Fields("account", lngAcct);
				rsSave.Set_Fields("name", strName);
				rsSave.Set_Fields("cardid", 0);
				rsSave.Set_Fields("secowner", strSecOwner);
				rsSave.Set_Fields("address1", straddr[0]);
				rsSave.Set_Fields("address2", straddr[1]);
				rsSave.Set_Fields("city", strCity);
				rsSave.Set_Fields("state", strState);
				rsSave.Set_Fields("zip", strZip);
				rsSave.Set_Fields("zip4", strZip4);
				rsSave.Set_Fields("DateCreated", dtDateCreated);
				rsSave.Set_Fields("saledate", dtSaleDate);
				rsSave.Update();
				ImportPreviousOwners = true;
				return ImportPreviousOwners;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportPreviousOwners", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportPreviousOwners;
		}
		// vbPorter upgrade warning: curDwelling As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportDwelling(ref XmlElement curDwelling)
		{
			bool ImportDwelling = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngStyle = 0;
				int intDwellUnits = 0;
				int intOtherUnits = 0;
				int intStories = 0;
				int intExterior = 0;
				int intRoof = 0;
				int intMasonry = 0;
				int intOpen3 = 0;
				int intOpen4 = 0;
				int intYearBuilt = 0;
				int intYearRemodeled = 0;
				int intFoundation = 0;
				int intBasement = 0;
				int intBasementGarage = 0;
				int lngSQFTBasementLiving = 0;
				int intBasementFinishedGrade = 0;
				int intBasementFinishedFactor = 0;
				int intOpen5 = 0;
				int intHeat = 0;
				int intPercentHeat = 0;
				int intCool = 0;
				int intPercentCooled = 0;
				int intKitchenStyle = 0;
				int intBathStyle = 0;
				int intRooms = 0;
				int intBedrooms = 0;
				int intFullBaths = 0;
				int intHalfBaths = 0;
				int intAdditionalFixtures = 0;
				int intWetBasement = 0;
				int intFireplaces = 0;
				int intLayout = 0;
				int intAttic = 0;
				int intInsulation = 0;
				int intPercUnfinished = 0;
				int intGrade = 0;
				int intFactor = 0;
				int lngSQFT = 0;
				int intCondition = 0;
				int intPhysicalPerc = 0;
				int intFuncPerc = 0;
				int intFuncCode = 0;
				int intEconPerc = 0;
				int intEconCode = 0;
				int lngSFLA = 0;
				string vbPorterVar = "";
				ImportDwelling = false;
				foreach (XmlNode curNode in curDwelling.ChildNodes)
				{
					//Application.DoEvents();
					vbPorterVar = curNode.Name;
					if (vbPorterVar == "Style")
					{
						lngStyle = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "DwellingUnits")
					{
						intDwellUnits = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "OtherUnits")
					{
						intOtherUnits = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Stories")
					{
						intStories = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Exterior")
					{
						intExterior = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Roof")
					{
						intRoof = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Masonry")
					{
						intMasonry = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Open3")
					{
						intOpen3 = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Open4")
					{
						intOpen4 = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "YearBuilt")
					{
						intYearBuilt = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "YearRemodeled")
					{
						intYearRemodeled = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Foundation")
					{
						intFoundation = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Basement")
					{
						intBasement = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "BasementGarage")
					{
						intBasementGarage = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "WetBasement")
					{
						intWetBasement = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "SQFTBasementLiving")
					{
						lngSQFTBasementLiving = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "BasementFinishedGrade")
					{
						intBasementFinishedGrade = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "BasementFinishedFactor")
					{
						intBasementFinishedFactor = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Open5")
					{
						intOpen5 = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Heat")
					{
						intHeat = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "PercentHeated")
					{
						intPercentHeat = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Cool")
					{
						intCool = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "PercentCooled")
					{
						intPercentCooled = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "KitchenStyle")
					{
						intKitchenStyle = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "BathStyle")
					{
						intBathStyle = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Rooms")
					{
						intRooms = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Bedrooms")
					{
						intBedrooms = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "FullBaths")
					{
						intFullBaths = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "HalfBaths")
					{
						intHalfBaths = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "AdditionalFixtures")
					{
						intAdditionalFixtures = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Fireplaces")
					{
						intFireplaces = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Layout")
					{
						intLayout = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Attic")
					{
						intAttic = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Insulation")
					{
						intInsulation = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "PercentUnfinished")
					{
						intPercUnfinished = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Grade")
					{
						intGrade = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Factor")
					{
						intFactor = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "SQFTFootprint")
					{
						lngSQFT = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Condition")
					{
						intCondition = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "PhysicalPercent")
					{
						intPhysicalPerc = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "FunctionalPercent")
					{
						intFuncPerc = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "FunctionalCode")
					{
						intFuncCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "EconomicPercent")
					{
						intEconPerc = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "EconomicCode")
					{
						intEconCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "SQFTLivingArea")
					{
						lngSFLA = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from dwelling where rsaccount = -1", modGlobalVariables.strREDatabase);
				rsSave.AddNew();
				rsSave.Set_Fields("rsaccount", lngAcct);
				rsSave.Set_Fields("rscard", intCard);
				rsSave.Set_Fields("distyle", lngStyle);
				rsSave.Set_Fields("diunitsdwelling", intDwellUnits);
				rsSave.Set_Fields("diunitsother", intOtherUnits);
				rsSave.Set_Fields("distories", intStories);
				rsSave.Set_Fields("diextwalls", intExterior);
				rsSave.Set_Fields("diroof", intRoof);
				rsSave.Set_Fields("disfmasonry", intMasonry);
				rsSave.Set_Fields("diopen3", intOpen3);
				rsSave.Set_Fields("diopen4", intOpen4);
				rsSave.Set_Fields("diyearbuilt", intYearBuilt);
				rsSave.Set_Fields("diyearremodel", intYearRemodeled);
				rsSave.Set_Fields("difoundation", intFoundation);
				rsSave.Set_Fields("dibsmt", intBasement);
				rsSave.Set_Fields("dibsmtgar", intBasementGarage);
				rsSave.Set_Fields("diwetbsmt", intWetBasement);
				rsSave.Set_Fields("disfbsmtliving", lngSQFTBasementLiving);
				rsSave.Set_Fields("dibsmtfingrade1", intBasementFinishedGrade);
				rsSave.Set_Fields("dibsmtfingrade2", intBasementFinishedFactor);
				rsSave.Set_Fields("diopen5", intOpen5);
				rsSave.Set_Fields("diheat", intHeat);
				rsSave.Set_Fields("dipctheat", intPercentHeat);
				rsSave.Set_Fields("dicool", intCool);
				rsSave.Set_Fields("dipctcool", intPercentCooled);
				rsSave.Set_Fields("dikitchens", intKitchenStyle);
				rsSave.Set_Fields("dibaths", intBathStyle);
				rsSave.Set_Fields("dirooms", intRooms);
				rsSave.Set_Fields("dibedrooms", intBedrooms);
				rsSave.Set_Fields("difullbaths", intFullBaths);
				rsSave.Set_Fields("dihalfbaths", intHalfBaths);
				rsSave.Set_Fields("diaddnfixtures", intAdditionalFixtures);
				rsSave.Set_Fields("difireplaces", intFireplaces);
				rsSave.Set_Fields("dilayout", intLayout);
				rsSave.Set_Fields("diattic", intAttic);
				rsSave.Set_Fields("diinsulation", intInsulation);
				rsSave.Set_Fields("dipctunfinished", intPercUnfinished);
				rsSave.Set_Fields("digrade1", intGrade);
				rsSave.Set_Fields("digrade2", intFactor);
				rsSave.Set_Fields("disqft", lngSQFT);
				rsSave.Set_Fields("dicondition", intCondition);
				rsSave.Set_Fields("dipctphys", intPhysicalPerc);
				rsSave.Set_Fields("dipctfunct", intFuncPerc);
				rsSave.Set_Fields("difunctcode", intFuncCode);
				rsSave.Set_Fields("dipctecon", intEconPerc);
				rsSave.Set_Fields("dieconcode", intEconCode);
				rsSave.Set_Fields("disfla", lngSFLA);
				rsSave.Update();
				rsSave.Execute("update master set rsdwellingcode = 'Y' where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				ImportDwelling = true;
				return ImportDwelling;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportDwelling", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportDwelling;
		}
		// vbPorter upgrade warning: curComm As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private bool ImportCommercial(ref XmlElement curComm, ref int intIndex)
		{
			bool ImportCommercial = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intBNum;
				int intOCCCode = 0;
				int intDwellingUnits = 0;
				int intClass = 0;
				int intQuality = 0;
				double dblGradeFactor = 0;
				int intExterior = 0;
				int intStories = 0;
				int intHeight = 0;
				int lngBaseFloorArea = 0;
				int lngPerimeter = 0;
				int intHeat = 0;
				int intYear = 0;
				int intYearRemodeled = 0;
				int intCondition = 0;
				int intPhysicalPercent = 0;
				int intEconomicPercent = 0;
				int intFunctionalPercent = 0;
				string vbPorterVar = "";
				ImportCommercial = false;
				intBNum = intIndex + 1;
				foreach (XmlNode curNode in curComm.ChildNodes)
				{
					//Application.DoEvents();
					if (curNode.NodeType == XmlNodeType.Element)
					{
						vbPorterVar = curNode.Name;
						if (vbPorterVar == "OccupancyCode")
						{
							intOCCCode = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "DwellingUnits")
						{
							intDwellingUnits = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Class")
						{
							intClass = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Quality")
						{
							intQuality = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "GradeFactor")
						{
							dblGradeFactor = Conversion.Val(curNode.InnerText);
						}
						else if (vbPorterVar == "Exterior")
						{
							intExterior = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Stories")
						{
							intStories = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Height")
						{
							intHeight = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "BaseFloorArea")
						{
							lngBaseFloorArea = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Perimeter")
						{
							lngPerimeter = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Heat")
						{
							intHeat = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "YearBuilt")
						{
							intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "YearRemodeled")
						{
							intYearRemodeled = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "Condition")
						{
							intCondition = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "PhysicalPercent")
						{
							intPhysicalPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "FunctionalPercent")
						{
							intFunctionalPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
						else if (vbPorterVar == "EconomicPercent")
						{
							intEconomicPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
						}
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				if (intBNum == 1)
				{
					rsSave.Set_Fields("cmecon", intEconomicPercent);
				}
				rsSave.Set_Fields("occ" + intBNum, intOCCCode);
				rsSave.Set_Fields("dwel" + intBNum, intDwellingUnits);
				rsSave.Set_Fields("c" + intBNum + "class", intClass);
				rsSave.Set_Fields("c" + intBNum + "quality", intQuality);
				rsSave.Set_Fields("c" + intBNum + "grade", dblGradeFactor);
				rsSave.Set_Fields("c" + intBNum + "extwalls", intExterior);
				rsSave.Set_Fields("c" + intBNum + "stories", intStories);
				rsSave.Set_Fields("c" + intBNum + "height", intHeight);
				rsSave.Set_Fields("c" + intBNum + "floor", lngBaseFloorArea);
				rsSave.Set_Fields("c" + intBNum + "perimeter", lngPerimeter);
				rsSave.Set_Fields("c" + intBNum + "heat", intHeat);
				rsSave.Set_Fields("c" + intBNum + "built", intYear);
				rsSave.Set_Fields("c" + intBNum + "remodel", intYearRemodeled);
				rsSave.Set_Fields("c" + intBNum + "condition", intCondition);
				rsSave.Set_Fields("c" + intBNum + "phys", intPhysicalPercent);
				rsSave.Set_Fields("c" + intBNum + "funct", intFunctionalPercent);
				rsSave.Update();
				rsSave.Execute("update master set rsdwellingcode = 'C' where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				ImportCommercial = true;
				return ImportCommercial;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportCommercial", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportCommercial;
		}
		// vbPorter upgrade warning: curOut As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		// vbPorter upgrade warning: intOutIndex As short	OnWriteFCConvert.ToInt32(
		private bool ImportOutbuilding(ref XmlElement curOut, ref int intOutIndex)
		{
			bool ImportOutbuilding = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCurO;
				int intType = 0;
				int intYear = 0;
				int lngUnits = 0;
				int intGrade = 0;
				int intFactor = 0;
				int intCondition = 0;
				int intPhysicalPercent = 0;
				int intFunctionalPercent = 0;
				bool boolUseSound = false;
				int lngSound = 0;
				string vbPorterVar = "";
				ImportOutbuilding = false;
				intCurO = intOutIndex + 1;
				foreach (XmlNode curNode in curOut.ChildNodes)
				{
					//Application.DoEvents();
					vbPorterVar = curNode.Name;
					if (vbPorterVar == "Type")
					{
						intType = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "YearBuilt")
					{
						intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Units")
					{
						lngUnits = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Grade")
					{
						intGrade = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Factor")
					{
						intFactor = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "Condition")
					{
						intCondition = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "PhysicalPercent")
					{
						intPhysicalPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "FunctionalPercent")
					{
						intFunctionalPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
					else if (vbPorterVar == "UseSoundValue")
					{
						boolUseSound = FCConvert.CBool(curNode.InnerText);
					}
					else if (vbPorterVar == "SoundValue")
					{
						lngSound = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
					}
				}
				// curNode
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("oitype" + intCurO, intType);
				rsSave.Set_Fields("oiyear" + intCurO, intYear);
				rsSave.Set_Fields("oiunits" + intCurO, lngUnits);
				rsSave.Set_Fields("oigradecd" + intCurO, intGrade);
				rsSave.Set_Fields("oigradepct" + intCurO, intFactor);
				rsSave.Set_Fields("oicond" + intCurO, intCondition);
				rsSave.Set_Fields("oipctphys" + intCurO, intPhysicalPercent);
				rsSave.Set_Fields("oipctfunct" + intCurO, intFunctionalPercent);
				rsSave.Set_Fields("oiusesound" + intCurO, boolUseSound);
				rsSave.Set_Fields("oisoundvalue" + intCurO, lngSound);
				rsSave.Update();
				rsSave.Execute("update master set rsoutbuildingcode = 'Y' where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				ImportOutbuilding = true;
				return ImportOutbuilding;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportOutbuilding", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportOutbuilding;
		}
		// vbPorter upgrade warning: curBookPage As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportBookPage(ref XmlElement curBookPage)
		{
			bool ImportBookPage = false;
			try
			{
				// On Error GoTo ErrorHandler
				int strBook = 0;
				int strPage = 0;
				string strBPDate = "";
				string strSDate = "";
				bool boolCurrent = false;
				string vbPorterVar = "";
				ImportBookPage = false;
				if (intCard == 1)
				{
					boolCurrent = FCConvert.ToBoolean(curBookPage.GetAttribute("Current"));
					foreach (XmlNode curNode in curBookPage.ChildNodes)
					{
						//Application.DoEvents();
						if (curNode.NodeType == XmlNodeType.Element)
						{
							vbPorterVar = curNode.Name;
							if (vbPorterVar == "Book")
							{
								strBook = FCConvert.ToInt32(curNode.InnerText);
							}
							else if (vbPorterVar == "Page")
							{
								strPage = FCConvert.ToInt32(curNode.InnerText);
							}
							else if (vbPorterVar == "BPDate")
							{
								strBPDate = curNode.InnerText;
							}
							else if (vbPorterVar == "SaleDate")
							{
								strSDate = curNode.InnerText;
							}
						}
					}
					// curNode
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
					rsSave.AddNew();
					rsSave.Set_Fields("account", lngAcct);
					rsSave.Set_Fields("card", 1);
					rsSave.Set_Fields("current", boolCurrent);
					rsSave.Set_Fields("BPDate", strBPDate);
					rsSave.Set_Fields("book", strBook);
					rsSave.Set_Fields("page", strPage);
					if (Information.IsDate(strSDate))
					{
						//if (strSDate != "12:00:00 AM")
						if (strSDate != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
						{
							rsSave.Set_Fields("saledate", strSDate);
						}
						rsSave.Set_Fields("saledate", 0);
					}
					else
					{
						rsSave.Set_Fields("saledate", 0);
					}
					rsSave.Update();
				}
				ImportBookPage = true;
				return ImportBookPage;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportBookPage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportBookPage;
		}
		// vbPorter upgrade warning: curMaplot As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportMapLot(ref XmlElement curMaplot)
		{
			bool ImportMapLot = false;
			string strMapLot = "";
			ImportMapLot = false;
			try
			{
				if (intCard == 1)
				{
					strMapLot = "";
					strMapLot = curMaplot.InnerText;
					clsDRWrapper rsSave = new clsDRWrapper();
					// Call rsSave.OpenRecordset("select rsaccount,rscard,rsmaplot from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
					rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("rsaccount", lngAcct);
						rsSave.Set_Fields("rscard", intCard);
					}
					rsSave.Set_Fields("rsmaplot", strMapLot);
					rsSave.Update();
				}
				ImportMapLot = true;
				return ImportMapLot;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportMapLot", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportMapLot;
		}
		// vbPorter upgrade warning: curLocation As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private bool ImportLocation(ref XmlElement curLocation)
		{
			bool ImportLocation = false;
			string strStreetNum;
			string strApt;
			string strStreetName;
			string vbPorterVar = "";
			try
			{
				ImportLocation = false;
				strStreetNum = "";
				strApt = "";
				strStreetName = "";
				foreach (XmlNode subElem in curLocation.ChildNodes)
				{
					vbPorterVar = subElem.Name;
					if (vbPorterVar == "StreetNumber")
					{
						strStreetNum = subElem.InnerText;
					}
					else if (vbPorterVar == "Apartment")
					{
						strApt = subElem.InnerText;
					}
					else if (vbPorterVar == "Street")
					{
						strStreetName = subElem.InnerText;
					}
				}
				// subElem
				clsDRWrapper rsSave = new clsDRWrapper();
				// Call rsSave.OpenRecordset("select rsaccount,rscard,rslocnumalph,rslocapt,rslocstreet from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
				rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("rsaccount", lngAcct);
					rsSave.Set_Fields("rscard", intCard);
				}
				rsSave.Set_Fields("rslocnumalph", Strings.Trim(strStreetNum));
				rsSave.Set_Fields("rslocapt", Strings.Trim(strApt));
				rsSave.Set_Fields("rslocstreet", Strings.Trim(strStreetName));
				rsSave.Update();
				ImportLocation = true;
				return ImportLocation;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportLocation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportLocation;
		}
		// vbPorter upgrade warning: curOwner As XmlElement	OnWrite(MSXML2.IXMLDOMNode)

        private bool ImportDeedNames(ref XmlElement curDeedNames)
        {
            try
            {
                if (intCard != 1)
                {
                    return true;
                }

                var deedName1 = "";
                var deedName2 = "";
                foreach (XmlNode subElem in curDeedNames.ChildNodes)
                {
                    switch (subElem.Name.ToLower())
                    {
                        case "deedname1":
                            deedName1 = subElem.Value;
                            break;
                        case "deedname2":
                            deedName2 = subElem.Value;
                            break;
                    }
                }

                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("select * from master where rsaccount = " + lngAcct + " and rscard = 1",
                    "RealEstate");
                if (!rsSave.EndOfFile())
                {
                    rsSave.Edit();
                }
                else
                {
                    rsSave.AddNew();
                    rsSave.Set_Fields("rsaccount",lngAcct);
                    rsSave.Set_Fields("rscard", 1);
                }

                rsSave.Set_Fields("DeedName1", deedName1);
                rsSave.Set_Fields("DeedName2",deedName2);
                rsSave.Update();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportLocation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            return false;
        }
		private bool ImportNameAddress(ref XmlElement curOwner)
		{
			bool ImportNameAddress = false;
			try
			{
				XmlNode curElement;
				// Dim curOwner As XmlElement
				XmlElement curSubElement;
				XmlNodeList lstSubElements;
				XmlNodeList lstAddressElements;

				int x;
				int intNode;
				string strNumber = "";
				string strDesc = "";
				string vbPorterVar = "";
				string strOwnerID = "";
				string strSecOwnerID = "";
				// Dim tPCol As New Collection
				// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				ImportNameAddress = false;
				if (intCard == 1)
				{
					strOwnerID = "";
					strSecOwnerID = "";
					clsDRWrapper rsSave = new clsDRWrapper();
					// Call rsSave.Execute("delete from phonenumbers where phonecode = 0 and parentid = " & lngAcct, strREDatabase)
					lstSubElements = curAcct.GetElementsByTagName("Owner");
					for (intNode = 0; intNode <= lstSubElements[0].ChildNodes.Count - 1; intNode++)
					{
						//Application.DoEvents();
						if (lstSubElements[0].ChildNodes[intNode].NodeType == XmlNodeType.Element)
						{
							curElement = lstSubElements[0].ChildNodes[intNode];
							vbPorterVar = curElement.Name;
							if (vbPorterVar == "OwnerPartyGUID")
							{
								strOwnerID = Strings.Trim(curElement.InnerText);
							}
							else if (vbPorterVar == "SecOwnerPartyGUID")
							{
								strSecOwnerID = Strings.Trim(curElement.InnerText);
								// Case "Party"
								// Set tParty = PartyFromXML(curElement)
								// If Not tParty Is Nothing Then
								// Call tPCol.Add(tParty)
								// End If
							}
						}
					}
					// intNode
					clsDRWrapper rsTemp = new clsDRWrapper();
					cPartyController tPC = new cPartyController();
					cParty tempParty = new cParty();

					rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("rsaccount", lngAcct);
						rsSave.Set_Fields("rscard", intCard);
					}
					if (strOwnerID != "")
					{
						tParty = tPC.GetPartyByGUID(strOwnerID);
						if (!(tParty == null))
						{
							if (tParty.ID > 0)
							{
								rsSave.Set_Fields("OwnerPartyID", tParty.ID);
							}
						}
						else
						{
						}
					}
					else
					{
						rsSave.Set_Fields("OwnerPartyID", 0);
					}
					if (strSecOwnerID != "")
					{
						tParty = tPC.GetPartyByGUID(strSecOwnerID);
						if (!(tParty == null))
						{
							if (tParty.ID > 0)
							{
								rsSave.Set_Fields("SecOwnerPartyID", tParty.ID);
							}
						}
					}
					else
					{
						rsSave.Set_Fields("SecOwnerPartyID", 0);
					}

					rsSave.Update();
				}
				ImportNameAddress = true;
				return ImportNameAddress;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportNameAddress", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return ImportNameAddress;
			}
		}
		// vbPorter upgrade warning: curParty As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private cParty PartyFromXML(ref XmlElement curParty)
		{
			cParty PartyFromXML = null;
			try
			{
				// On Error GoTo ErrorHandler
				if (curParty.HasChildNodes)
				{
					//FC:FINAL:MSH - i.issue #1557: initializing moved here, because in cycle each time creates new object and data won't be missed
					PartyFromXML = new cParty();
					foreach (XmlNode curNode in curParty.ChildNodes)
					{
						cPartyAddress tAddress;
						cPartyPhoneNumber tPhone;
						cContact tContact;
						cPartyComment tComment;
						//Application.DoEvents();
						if (Strings.LCase(curNode.Name) == "partyguid")
						{
							PartyFromXML.PartyGUID = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "partytype")
						{
							PartyFromXML.PartyType = FCConvert.ToInt32(curNode.InnerText);
						}
						else if (Strings.LCase(curNode.Name) == "firstname")
						{
							PartyFromXML.FirstName = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "middlename")
						{
							PartyFromXML.MiddleName = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "lastname")
						{
							PartyFromXML.LastName = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "designation")
						{
							PartyFromXML.Designation = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "email")
						{
							PartyFromXML.Email = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "webaddress")
						{
							PartyFromXML.WebAddress = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "address")
						{
							tAddress = AddressFromXML(curNode);
							if (!(tAddress == null))
							{
								PartyFromXML.Addresses.Add(tAddress);
							}
						}
						else if (Strings.LCase(curNode.Name) == "centralphonenumber")
						{
							tPhone = PhoneNumberFromXML(curNode);
							if (!(tPhone == null))
							{
								PartyFromXML.PhoneNumbers.Add(tPhone);
							}
						}
						else if (Strings.LCase(curNode.Name) == "centralcomment")
						{
							tComment = CommentFromXML(curNode);
							if (!(tComment == null))
							{
								PartyFromXML.Comments.Add(tComment);
							}
						}
						else if (Strings.LCase(curNode.Name) == "contact")
						{
							tContact = ContactFromXML(curNode);
							if (!(tContact == null))
							{
								PartyFromXML.Contacts.Add(tContact);
							}
						}
					}
					// curNode
				}
			}
			catch
			{
				// ErrorHandler:
			}
			return PartyFromXML;
		}
		// vbPorter upgrade warning: curAddr As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private cPartyAddress AddressFromXML(XmlNode curAddr)
		{
			cPartyAddress AddressFromXML = null;
			try
			{
				// On Error GoTo ErrorHandler
				AddressFromXML = null;
				if (curAddr.HasChildNodes)
				{
					cPartyAddress tAddress = new cPartyAddress();
					foreach (XmlNode curNode in curAddr.ChildNodes)
					{
						if (Strings.LCase(curNode.Name) == "address1")
						{
							tAddress.Address1 = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "address2")
						{
							tAddress.Address2 = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "address3")
						{
							tAddress.Address3 = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "city")
						{
							tAddress.City = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "state")
						{
							tAddress.State = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "zip")
						{
							tAddress.Zip = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "country")
						{
							tAddress.Country = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "overridename")
						{
							tAddress.OverrideName = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "addresstype")
						{
							tAddress.AddressType = FCConvert.ToString(Conversion.Val(curNode.InnerText));
						}
						else if (Strings.LCase(curNode.Name) == "seasonal")
						{
							if (!(curNode.InnerText == ""))
							{
								if (FCConvert.CBool(curNode.InnerText))
								{
									tAddress.Seasonal = true;
								}
								else
								{
									tAddress.Seasonal = false;
								}
							}
						}
						else if (Strings.LCase(curNode.Name) == "progmodule")
						{
							tAddress.ProgModule = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "comment")
						{
							tAddress.Comment = curNode.InnerText;
						}
						else if (Strings.LCase(curNode.Name) == "startmonth")
						{
							if (Conversion.Val(curNode.InnerText) > 0)
							{
								tAddress.StartMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							}
							else
							{
								tAddress.StartMonth = 0;
							}
						}
						else if (Strings.LCase(curNode.Name) == "startday")
						{
							if (Conversion.Val(curNode.InnerText) > 0)
							{
								tAddress.StartDay = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							}
							else
							{
								tAddress.StartDay = 0;
							}
						}
						else if (Strings.LCase(curNode.Name) == "endmonth")
						{
							if (Conversion.Val(curNode.InnerText) > 0)
							{
								tAddress.EndMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							}
							else
							{
								tAddress.EndMonth = 0;
							}
						}
						else if (Strings.LCase(curNode.Name) == "endday")
						{
							if (Conversion.Val(curNode.InnerText) > 0)
							{
								tAddress.EndDay = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							}
							else
							{
								tAddress.EndDay = 0;
							}
						}
						else if (Strings.LCase(curNode.Name) == "modaccountid")
						{
							if (Conversion.Val(curNode.InnerText) > 0)
							{
								tAddress.ModAccountID = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							}
							else
							{
								tAddress.ModAccountID = 0;
							}
						}
					}
					// curNode
					AddressFromXML = tAddress;
				}
				return AddressFromXML;
			}
			catch
			{
				// ErrorHandler:
			}
			return AddressFromXML;
		}
		// vbPorter upgrade warning: curPhone As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private cPartyPhoneNumber PhoneNumberFromXML(XmlNode curPhone)
		{
			cPartyPhoneNumber PhoneNumberFromXML = null;
			try
			{
				// On Error GoTo ErrorHandler
				cPartyPhoneNumber tPhone = new cPartyPhoneNumber();
				PhoneNumberFromXML = null;
				if (!(curPhone == null))
				{
					if (curPhone.HasChildNodes)
					{
						foreach (XmlNode curNode in curPhone.ChildNodes)
						{
							if (Strings.LCase(curNode.Name) == "phonenumber")
							{
								tPhone.PhoneNumber = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "description")
							{
								tPhone.Description = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "phoneorder")
							{
								tPhone.PhoneOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(curNode.InnerText)));
							}
							else if (Strings.LCase(curNode.Name) == "extension")
							{
								tPhone.Extension = curNode.InnerText;
							}
						}
						// curNode
						PhoneNumberFromXML = tPhone;
					}
				}
				return PhoneNumberFromXML;
			}
			catch
			{
				// ErrorHandler:
			}
			return PhoneNumberFromXML;
		}
		// vbPorter upgrade warning: curComment As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private cPartyComment CommentFromXML(XmlNode curComment)
		{
			cPartyComment CommentFromXML = null;
			try
			{
				// On Error GoTo ErrorHandler
				CommentFromXML = null;
				if (!(curComment == null))
				{
					if (curComment.HasChildNodes)
					{
						cPartyComment tComment = new cPartyComment();
						tComment.LastModified = DateTime.Now;
						foreach (XmlNode curNode in curComment.ChildNodes)
						{
							if (Strings.LCase(curNode.Name) == "comment")
							{
								tComment.Comment = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "progmodule")
							{
								tComment.ProgModule = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "enteredby")
							{
								tComment.EnteredBy = curNode.InnerText;
							}
						}
						// curNode
						CommentFromXML = tComment;
					}
				}
				return CommentFromXML;
			}
			catch
			{
				// ErrorHandler:
			}
			return CommentFromXML;
		}
		// vbPorter upgrade warning: curContact As XmlElement	OnWrite(MSXML2.IXMLDOMNode)
		private cContact ContactFromXML(XmlNode curContact)
		{
			cContact ContactFromXML = null;
			try
			{
				// On Error GoTo ErrorHandler
				ContactFromXML = null;
				if (!(curContact == null))
				{
					if (curContact.HasChildNodes)
					{
						cContact tContact = new cContact();
						cPartyPhoneNumber tPhone;
						foreach (XmlNode curNode in curContact.ChildNodes)
						{
							if (Strings.LCase(curNode.Name) == "description")
							{
								tContact.Description = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "name")
							{
								tContact.Name = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "email")
							{
								tContact.Email = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "comment")
							{
								tContact.Comment = curNode.InnerText;
							}
							else if (Strings.LCase(curNode.Name) == "contactphonenumber")
							{
								tPhone = PhoneNumberFromXML(curNode);
								if (!(tPhone == null))
								{
									tContact.PhoneNumbers.Add(tPhone);
								}
							}
						}
						// curNode
						ContactFromXML = tContact;
					}
				}
				return ContactFromXML;
			}
			catch
			{
				// ErrorHandler:
			}
			return ContactFromXML;
		}
	}
}
