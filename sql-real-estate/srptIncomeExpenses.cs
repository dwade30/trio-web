﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for sprtIncomeExpenses.
	/// </summary>
	public partial class srptIncomeExpenses : FCSectionReport
	{
		public static srptIncomeExpenses InstancePtr
		{
			get
			{
				return (srptIncomeExpenses)Sys.GetInstance(typeof(srptIncomeExpenses));
			}
		}

		protected srptIncomeExpenses _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public srptIncomeExpenses()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptIncomeExpenses	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		// vbPorter upgrade warning: curTotAmount As Decimal	OnWrite(short, Decimal)
		Decimal curTotAmount;
		bool boolLoadFromScreen;
		int lngRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolLoadFromScreen)
			{
				eArgs.EOF = clsLoad.EndOfFile();
			}
			else
			{
				if (lngRow >= frmREProperty.InstancePtr.GridExpenses.Rows)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: strAry As string()	OnReadFCConvert.ToInt32(
			string[] strAry = null;
			// vbPorter upgrade warning: lngAccount As int	OnWrite(string)
			int lngAccount;
			// vbPorter upgrade warning: intCard As short --> As int	OnWrite(string)
			int intCard;
			strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
			boolLoadFromScreen = FCConvert.CBool(strAry[2]);
			lngAccount = FCConvert.ToInt32(strAry[0]);
			intCard = FCConvert.ToInt32(strAry[1]);
			curTotAmount = 0;
			lngRow = 2;
			if (!boolLoadFromScreen)
			{
				clsLoad.OpenRecordset("SELECT * from incomeexpense where account = " + FCConvert.ToString(lngAccount) + " and card = " + FCConvert.ToString(intCard) + " order by code", modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					this.Close();
					return;
				}
			}
			else
			{
				txtTotal.Text = frmNewREProperty.InstancePtr.txtTotalExpense.Text;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblAmount = 0;
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                if (!boolLoadFromScreen)
                {
                    if (!clsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        txtCode.Text = clsLoad.Get_Fields("code");
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        clsTemp.OpenRecordset(
                            "select * from expensetable where code = " +
                            FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))),
                            modGlobalVariables.strREDatabase);
                        if (!clsTemp.EndOfFile())
                        {
                            txtDescription.Text = clsTemp.Get_Fields_String("description");
                        }
                        else
                        {
                            txtDescription.Text = "";
                        }

                        txtPctGross.Text =
                            Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("percgross")), "0.0");
                        txtPerSF.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("$SF")), "0.00");
                        // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                        dblAmount = Conversion.Val(clsLoad.Get_Fields("amount"));
                        txtAmount.Text = Strings.Format(dblAmount, "#,###,##0.00");
                        curTotAmount += FCConvert.ToDecimal(dblAmount);
                        clsLoad.MoveNext();
                    }
                }
                else
                {
                    if (lngRow < frmNewREProperty.InstancePtr.GridExpenses.Rows)
                    {
                        txtCode.Text = frmNewREProperty.InstancePtr.GridExpenses.TextMatrix(lngRow, 0);
                        clsTemp.OpenRecordset(
                            "select * from expensetable where code = " + FCConvert.ToString(
                                Conversion.Val(frmNewREProperty.InstancePtr.GridExpenses.TextMatrix(lngRow, 0))),
                            modGlobalVariables.strREDatabase);
                        if (!clsTemp.EndOfFile())
                        {
                            txtDescription.Text = clsTemp.Get_Fields_String("description");
                        }
                        else
                        {
                            txtDescription.Text = "";
                        }

                        txtPctGross.Text = frmNewREProperty.InstancePtr.GridExpenses.TextMatrix(lngRow, 1);
                        txtPerSF.Text = frmNewREProperty.InstancePtr.GridExpenses.TextMatrix(lngRow, 2);
                        txtAmount.Text = frmNewREProperty.InstancePtr.GridExpenses.TextMatrix(lngRow, 3);
                        lngRow += 1;
                    }
                }
            }
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (!boolLoadFromScreen)
			{
				txtTotal.Text = Strings.Format(curTotAmount, "#,###,##0.00");
			}
		}

		
	}
}
