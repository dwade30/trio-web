﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for Frmassessmentprogress.
	/// </summary>
	public partial class Frmassessmentprogress : BaseForm
	{
		public Frmassessmentprogress()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            //FC:FINAL:DSE:#1620 Window should be visible during computations
            this.TopMost = true;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static Frmassessmentprogress InstancePtr
		{
			get
			{
				return (Frmassessmentprogress)Sys.GetInstance(typeof(Frmassessmentprogress));
			}
		}

		protected Frmassessmentprogress _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool stillcalc;
		// the program checks this periodically to see if it should abort
		string strM;
		// vbPorter upgrade warning: stMessage As Variant --> As string
		public void Initialize(string stMessage = "")
		{
			// DoEvents
			// Me.ZOrder
			strM = "";
			if (!Information.IsNothing(stMessage))
				strM = stMessage;
			// lblMuniname.Caption = MuniName
			// lblDate.Caption = Date$
			// lblTime.Caption = Time
			ProgressBar1.Value = 0;
			lblpercentdone.Text = "0%";
			stillcalc = true;
			// Frmcalcexemptions.Show
		}

		private void cmdcancexempt_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: response As Variant --> As DialogResult
			DialogResult response;
			if (!(strM == string.Empty))
			{
				response = MessageBox.Show(strM, null, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
			}
			else
			{
				response = MessageBox.Show("This will stop calculating Assessments." + "\r\n" + "Some files will be left calculated and others won't", "Cancel Transfers", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
			}
			if (response == DialogResult.OK)
			{
				stillcalc = false;
			}
		}

		private void Frmassessmentprogress_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//Frmassessmentprogress properties;
			//Frmassessmentprogress.ScaleWidth	= 4680;
			//Frmassessmentprogress.ScaleHeight	= 2550;
			//Frmassessmentprogress.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//Application.DoEvents();
		}
	}
}
