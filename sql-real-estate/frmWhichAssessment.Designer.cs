﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation.Extensions;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmWhichAssessment.
	/// </summary>
	partial class frmWhichAssessment : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWhich;
		public fecherFoundation.FCLabel lblWhich;
		public fecherFoundation.FCButton cmdOkay;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbWhich = new fecherFoundation.FCComboBox();
			this.lblWhich = new fecherFoundation.FCLabel();
			this.cmdOkay = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOkay)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 256);
			this.BottomPanel.Size = new System.Drawing.Size(570, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbWhich);
			this.ClientArea.Controls.Add(this.lblWhich);
			this.ClientArea.Controls.Add(this.cmdOkay);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(570, 196);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(570, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// cmbWhich
			// 
			this.cmbWhich.AutoSize = false;
			this.cmbWhich.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWhich.FormattingEnabled = true;
			this.cmbWhich.Items.AddRange(new object[] {
				"Billing",
				"Current"
			});
			this.cmbWhich.Location = new System.Drawing.Point(143, 80);
			this.cmbWhich.Name = "cmbWhich";
			this.cmbWhich.Size = new System.Drawing.Size(229, 40);
			this.cmbWhich.TabIndex = 2;
			this.cmbWhich.Text = "Billing";
			// 
			// lblWhich
			// 
			this.lblWhich.AutoSize = true;
			this.lblWhich.Location = new System.Drawing.Point(30, 94);
			this.lblWhich.Name = "lblWhich";
			this.lblWhich.Size = new System.Drawing.Size(49, 15);
			this.lblWhich.TabIndex = 1;
			this.lblWhich.Text = "WHICH";
			// 
			// cmdOkay
			// 
			this.cmdOkay.AppearanceKey = "actionButton";
			this.cmdOkay.ForeColor = System.Drawing.Color.White;
			this.cmdOkay.Location = new System.Drawing.Point(30, 140);
			this.cmdOkay.Name = "cmdOkay";
			this.cmdOkay.Size = new System.Drawing.Size(86, 40);
			this.cmdOkay.TabIndex = 3;
			this.cmdOkay.Text = "OK";
			this.cmdOkay.Click += new System.EventHandler(this.cmdOkay_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(510, 25);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "DO YOU WANT THE ASSESSMENT FIGURES TO USE BILLING OR CURRENT VALUES?";
			// 
			// frmWhichAssessment
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(570, 364);
			this.ControlBox = false;
			this.Name = "frmWhichAssessment";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = " ";
			this.Load += new System.EventHandler(this.frmWhichAssessment_Load);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOkay)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
