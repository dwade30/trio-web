﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace TWRE0000
{
	public class cHeatingCoolingCollection
	{
		//=========================================================
		private cGenericCollection genericCollectionBase;

		private FCCollection cGenericCollection_Items
		{
			set
			{
				//Implements cGenericCollection.Items
				genericCollectionBase.Items = value;
			}
			get
			{
				FCCollection cGenericCollection_Items = null;
				// Implements cGenericCollection.Items
				cGenericCollection_Items = genericCollectionBase.Items;
				return cGenericCollection_Items;
			}
		}

		public cHeatingCoolingCollection() : base()
		{
			genericCollectionBase = new cGenericCollection();
		}

		~cHeatingCoolingCollection()
		{
			genericCollectionBase = null;
		}

		private void cGenericCollection_ClearList()
		{
			//Implements cGenericCollection.ClearList
			genericCollectionBase.ClearList();
		}

		private void cGenericCollection_AddItem(object tItem)
		{
			//Implements cGenericCollection.AddItem
			genericCollectionBase.AddItem(tItem);
		}

		private void cGenericCollection_InsertItemBefore(ref object tItem)
		{
			//Implements cGenericCollection.InsertItemBefore
			genericCollectionBase.InsertItemBefore(tItem);
		}

		private void cGenericCollection_InsertItemAfter(ref object tItem)
		{
			//Implements cGenericCollection.InsertItemAfter
			genericCollectionBase.InsertItemAfter(ref tItem);
		}

		private object cGenericCollection_GetCurrentItem()
		{
			object cGenericCollection_GetCurrentItem = null;
			//Implements cGenericCollection.GetCurrentItem
			cGenericCollection_GetCurrentItem = genericCollectionBase.GetCurrentItem();
			return cGenericCollection_GetCurrentItem;
		}

		private object cGenericCollection_GetItemByIndex(ref short intIndex)
		{
			object cGenericCollection_GetItemByIndex = null;
			//Implements cGenericCollection.GetItemByIndex
			cGenericCollection_GetItemByIndex = genericCollectionBase.GetItemByIndex(intIndex);
			return cGenericCollection_GetItemByIndex;
		}

		private int cGenericCollection_GetCurrentIndex()
		{
			int cGenericCollection_GetCurrentIndex = 0;
			//Implements cGenericCollection.GetCurrentIndex
			cGenericCollection_GetCurrentIndex = genericCollectionBase.GetCurrentIndex();
			return cGenericCollection_GetCurrentIndex;
		}

		private bool cGenericCollection_IsCurrent()
		{
			bool cGenericCollection_IsCurrent = false;
			//Implements cGenericCollection.IsCurrent
			cGenericCollection_IsCurrent = genericCollectionBase.IsCurrent();
			return cGenericCollection_IsCurrent;
		}

		private short cGenericCollection_ItemCount()
		{
			short cGenericCollection_ItemCount = 0;
			//Implements cGenericCollection.ItemCount
			cGenericCollection_ItemCount = genericCollectionBase.ItemCount();
			return cGenericCollection_ItemCount;
		}

		private void cGenericCollection_MoveFirst()
		{
			//Implements cGenericCollection.MoveFirst
			genericCollectionBase.MoveFirst();
		}

		private short cGenericCollection_MoveNext()
		{
			short cGenericCollection_MoveNext = 0;
			//Implements cGenericCollection.MoveNext
			cGenericCollection_MoveNext = genericCollectionBase.MoveNext();
			return cGenericCollection_MoveNext;
		}

		private short cGenericCollection_MovePrevious()
		{
			short cGenericCollection_MovePrevious = 0;
			//Implements cGenericCollection.MovePrevious
			cGenericCollection_MovePrevious = genericCollectionBase.MovePrevious();
			return cGenericCollection_MovePrevious;
		}

		private short cGenericCollection_MoveLast()
		{
			short cGenericCollection_MoveLast = 0;
			//Implements cGenericCollection.MoveLast
			cGenericCollection_MoveLast = genericCollectionBase.MoveLast();
			return cGenericCollection_MoveLast;
		}

		private void cGenericCollection_RemoveCurrent()
		{
			//Implements cGenericCollection.RemoveCurrent
			genericCollectionBase.RemoveCurrent();
		}

		public void ClearList()
		{
			cGenericCollection_ClearList();
		}
		// vbPorter upgrade warning: tItem As object	OnWrite(cCommercialHeatingCooling)
		public void AddItem(object tItem)
		{
			cGenericCollection_AddItem(tItem);
		}

		public void InsertItemBefore(ref object tItem)
		{
			cGenericCollection_InsertItemBefore(ref tItem);
		}

		public void InsertItemAfter(ref object tItem)
		{
			cGenericCollection_InsertItemAfter(ref tItem);
		}

		public object GetCurrentItem()
		{
			object GetCurrentItem = null;
			GetCurrentItem = cGenericCollection_GetCurrentItem();
			return GetCurrentItem;
		}

		public object GetItemByIndex(ref short intIndex)
		{
			object GetItemByIndex = null;
			GetItemByIndex = cGenericCollection_GetItemByIndex(ref intIndex);
			return GetItemByIndex;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = cGenericCollection_GetCurrentIndex();
			return GetCurrentIndex;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			IsCurrent = cGenericCollection_IsCurrent();
			return IsCurrent;
		}

		public short ItemCount()
		{
			short ItemCount = 0;
			ItemCount = cGenericCollection_ItemCount();
			return ItemCount;
		}

		public void MoveFirst()
		{
			cGenericCollection_MoveFirst();
		}

		public short MoveNext()
		{
			short MoveNext = 0;
			MoveNext = cGenericCollection_MoveNext();
			return MoveNext;
		}

		public short MovePrevious()
		{
			short MovePrevious = 0;
			MovePrevious = cGenericCollection_MovePrevious();
			return MovePrevious;
		}

		public short MoveLast()
		{
			short MoveLast = 0;
			MoveLast = cGenericCollection_MoveLast();
			return MoveLast;
		}

		public void RemoveCurrent()
		{
			cGenericCollection_RemoveCurrent();
		}

		public cCommercialHeatingCooling FindFirstCode(int lngCode)
		{
			cCommercialHeatingCooling FindFirstCode = null;
			cCommercialHeatingCooling tReturn;
			MoveFirst();
			while (IsCurrent())
			{
				tReturn = (cCommercialHeatingCooling)GetCurrentItem();
				if (tReturn.CodeNumber == lngCode)
				{
					FindFirstCode = tReturn;
					return FindFirstCode;
				}
				MoveNext();
			}
			FindFirstCode = null;
			return FindFirstCode;
		}
	}
}
