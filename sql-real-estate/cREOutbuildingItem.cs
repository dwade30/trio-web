﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cREOutbuildingItem
	{
		//=========================================================
		private int intBuildingNumber;
		private int lngBuildingType;
		private int intYearBuilt;
		private int lngUnits;
		private int intGradeCode;
		private int intGradePercent;
		private int intCondition;
		private int intPercentPhysical;
		private int intPercentFunctional;
		private double dblSoundValue;
		private bool boolUpdated;
		private bool boolDeleted;

		public short BuildingNumber
		{
			set
			{
				intBuildingNumber = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short BuildingNumber = 0;
				BuildingNumber = FCConvert.ToInt16(intBuildingNumber);
				return BuildingNumber;
			}
		}

		public int BuildingType
		{
			set
			{
				lngBuildingType = value;
				IsUpdated = true;
			}
			get
			{
				int BuildingType = 0;
				BuildingType = lngBuildingType;
				return BuildingType;
			}
		}

		public short YearBuilt
		{
			set
			{
				intYearBuilt = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short YearBuilt = 0;
				YearBuilt = FCConvert.ToInt16(intYearBuilt);
				return YearBuilt;
			}
		}

		public int Units
		{
			set
			{
				lngUnits = value;
				IsUpdated = true;
			}
			get
			{
				int Units = 0;
				Units = lngUnits;
				return Units;
			}
		}

		public short GradeCode
		{
			set
			{
				intGradeCode = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short GradeCode = 0;
				GradeCode = FCConvert.ToInt16(intGradeCode);
				return GradeCode;
			}
		}

		public short GradePercent
		{
			set
			{
				intGradePercent = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short GradePercent = 0;
				GradePercent = FCConvert.ToInt16(intGradePercent);
				return GradePercent;
			}
		}

		public short Condition
		{
			set
			{
				intCondition = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Condition = 0;
				Condition = FCConvert.ToInt16(intCondition);
				return Condition;
			}
		}

		public short PercentPhysical
		{
			set
			{
				intPercentPhysical = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short PercentPhysical = 0;
				PercentPhysical = FCConvert.ToInt16(intPercentPhysical);
				return PercentPhysical;
			}
		}

		public short PercentFunctional
		{
			set
			{
				intPercentFunctional = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short PercentFunctional = 0;
				PercentFunctional = FCConvert.ToInt16(intPercentFunctional);
				return PercentFunctional;
			}
		}

		public double SoundValue
		{
			set
			{
				dblSoundValue = value;
				IsUpdated = true;
			}
			get
			{
				double SoundValue = 0;
				SoundValue = dblSoundValue;
				return SoundValue;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
