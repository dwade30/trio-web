﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptComment.
	/// </summary>
	public partial class rptComment : BaseSectionReport
	{
		public static rptComment InstancePtr
		{
			get
			{
				return (rptComment)Sys.GetInstance(typeof(rptComment));
			}
		}

		protected rptComment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Comments";
		}
		// nObj = 1
		//   0	rptComment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!clsLoad.EndOfFile())
			{
				txtAcct.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
				txtCard.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rscard"));
				txtMapLot.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot"));
				txtName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rsname"));
				strTemp = "";
				if (Conversion.Val(clsLoad.Get_Fields_String("rslocnumalph")) > 0)
				{
					strTemp = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rslocnumalph"))) + " " + Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("rslocapt"))) + " ";
				}
				strTemp += FCConvert.ToString(clsLoad.Get_Fields_String("rslocstreet"));
				strTemp = Strings.Trim(strTemp);
				txtLocation.Text = strTemp;
				txtComment.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("CCOMMENT")));
				clsLoad.MoveNext();
			}
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref bool boolRange, ref short intType, string strmin = "", string strmax = "", bool boolFromExtract = false)
		{
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			string strREFullDBName;
			string strMasterJoin;
			strREFullDBName = clsLoad.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoinForJoin();
			switch (intType)
			{
				case 1:
					{
						// account
						strOrderBy = " order by rsaccount,mparty.rscard ";
						strWhere = " and rsaccount between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
						break;
					}
				case 2:
					{
						// name
						strOrderBy = " order by rsname,rsaccount,mparty.rscard ";
						strWhere = " and rsname between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 3:
					{
						// map lot
						strOrderBy = " order by rsmaplot,rsaccount,mparty.rscard ";
						strWhere = " and rsmaplot between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 4:
					{
						// location
						strOrderBy = " order by rslocstreet,CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float),rsaccount,mparty.rscard ";
						strWhere = " and rslocstreet between '" + strmin + "' and '" + strmax + "'";
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolFromExtract)
			{
				// strSQL = "select master.*,commrec.ccomment from (master inner join (commrec inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (commrec.crecordnumber = extracttable.accountnumber) and (commrec.rscard = extracttable.cardnumber)) on (commrec.crecordnumber = master.rsaccount) and (commrec.rscard = master.rscard)) where extracttable.userid = " & lngUID & " and not master.rsdeleted " & strOrderBy
				strSQL = "select mparty.*,commrec.ccomment from ( " + strMasterJoin + " inner join (commrec inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (commrec.crecordnumber = extracttable.accountnumber) and (commrec.rscard = extracttable.cardnumber)) on (commrec.crecordnumber = mj.rsaccount) and (commrec.rscard = mj.rscard)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and not mj.rsdeleted = 1" + strOrderBy;
				clsLoad.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (FCConvert.ToString(clsLoad.Get_Fields_String("title")) != string.Empty)
					{
						lblTitle2.Text = clsLoad.Get_Fields_String("title");
					}
				}
			}
			else
			{
				if (!boolRange)
				{
					strWhere = "";
				}
				strSQL = "select *,master.rscard as rscard from master inner join commrec on (commrec.crecordnumber = master.rsaccount) and (commrec.rscard = master.rscard) where not rsdeleted = 1 " + strWhere + strOrderBy;
				strSQL = "select *,mparty.rscard as rscard from " + strMasterJoin + " inner join " + strREFullDBName + ".dbo.commrec on (commrec.crecordnumber = mparty.rsaccount) and (commrec.rscard = mparty.rscard) where not rsdeleted = 1 " + strWhere + strOrderBy;
			}
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Comments");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = FCConvert.ToString(this.PageNumber);
		}

		
	}
}
