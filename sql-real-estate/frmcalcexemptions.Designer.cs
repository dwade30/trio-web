﻿//Fecher vbPorter - Version 1.0.0.40
using Global;
using Wisej.Web;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for Frmcalcexemptions.
	/// </summary>
	partial class Frmcalcexemptions : BaseForm
	{
		public fecherFoundation.FCButton cmdcancexempt;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblpercentdone;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdcancexempt = new fecherFoundation.FCButton();
			this.ProgressBar1 = new fecherFoundation.FCProgressBar();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblpercentdone = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdcancexempt)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 248);
			this.BottomPanel.Size = new System.Drawing.Size(295, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdcancexempt);
			this.ClientArea.Controls.Add(this.ProgressBar1);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblpercentdone);
			this.ClientArea.Size = new System.Drawing.Size(295, 188);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(295, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(251, 30);
			this.HeaderText.Text = "Calculate Exemptions";
			// 
			// cmdcancexempt
			// 
			this.cmdcancexempt.AppearanceKey = "actionButton";
			this.cmdcancexempt.Location = new System.Drawing.Point(94, 120);
			this.cmdcancexempt.Name = "cmdcancexempt";
			this.cmdcancexempt.Size = new System.Drawing.Size(94, 40);
			this.cmdcancexempt.TabIndex = 2;
			this.cmdcancexempt.Text = "Cancel";
			this.cmdcancexempt.Click += new System.EventHandler(this.cmdcancexempt_Click);
			// 
			// ProgressBar1
			// 
			this.ProgressBar1.Location = new System.Drawing.Point(30, 85);
			this.ProgressBar1.Name = "ProgressBar1";
			this.ProgressBar1.Size = new System.Drawing.Size(251, 16);
			this.ProgressBar1.TabIndex = 0;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(106, 53);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(52, 17);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "DONE";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 22);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(255, 26);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "CALCULATING EXEMPTIONS, PLEASE WAIT...";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblpercentdone
			// 
			this.lblpercentdone.Location = new System.Drawing.Point(30, 53);
			this.lblpercentdone.Name = "lblpercentdone";
			this.lblpercentdone.Size = new System.Drawing.Size(65, 17);
			this.lblpercentdone.TabIndex = 1;
			this.lblpercentdone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Frmcalcexemptions
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(295, 248);
			this.ControlBox = false;
			this.Name = "Frmcalcexemptions";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Calculate Exemptions";
			this.Load += new System.EventHandler(this.Frmcalcexemptions_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdcancexempt)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
