﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmListing.
	/// </summary>
	partial class frmListing : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCComboBox cmbInfo;
		public fecherFoundation.FCLabel lblInfo;
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbPrevious;
		public fecherFoundation.FCLabel lblPrevious;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkInclusive;
		public fecherFoundation.FCTextBox txtTo;
		public fecherFoundation.FCTextBox txtFrom;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCLabel lblFrom;
		public Global.T2KDateBox t2kDateStart;
		public Global.T2KDateBox t2kDateEnd;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkBldgValue;
		public fecherFoundation.FCCheckBox chkArchiveEntries;
		public FCGrid Grid;
		public fecherFoundation.FCFrame Frame5;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblend;
		public fecherFoundation.FCLabel lblstart;
		public fecherFoundation.FCFrame framComparison;
		public Global.T2KDateBox t2kDeletedSince;
		public fecherFoundation.FCCheckBox chkShowDeleted;
		public fecherFoundation.FCCheckBox chkChanges;
		public fecherFoundation.FCLabel lblEndDate;
		public fecherFoundation.FCLabel lblStartDate;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmListing));
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.cmbInfo = new fecherFoundation.FCComboBox();
            this.lblInfo = new fecherFoundation.FCLabel();
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.cmbPrevious = new fecherFoundation.FCComboBox();
            this.lblPrevious = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkInclusive = new fecherFoundation.FCCheckBox();
            this.txtTo = new fecherFoundation.FCTextBox();
            this.txtFrom = new fecherFoundation.FCTextBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.lblFrom = new fecherFoundation.FCLabel();
            this.t2kDateStart = new Global.T2KDateBox();
            this.t2kDateEnd = new Global.T2KDateBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtDate = new Global.T2KDateBox();
            this.txtAccount = new fecherFoundation.FCTextBox();
            this.lblend = new fecherFoundation.FCLabel();
            this.lblstart = new fecherFoundation.FCLabel();
            this.framComparison = new fecherFoundation.FCFrame();
            this.t2kDeletedSince = new Global.T2KDateBox();
            this.chkShowDeleted = new fecherFoundation.FCCheckBox();
            this.chkChanges = new fecherFoundation.FCCheckBox();
            this.chkBldgValue = new fecherFoundation.FCCheckBox();
            this.chkArchiveEntries = new fecherFoundation.FCCheckBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.lblEndDate = new fecherFoundation.FCLabel();
            this.lblStartDate = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclusive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framComparison)).BeginInit();
            this.framComparison.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDeletedSince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBldgValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkArchiveEntries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(924, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblEndDate);
            this.ClientArea.Controls.Add(this.lblStartDate);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.t2kDateStart);
            this.ClientArea.Controls.Add(this.t2kDateEnd);
            this.ClientArea.Controls.Add(this.cmbInfo);
            this.ClientArea.Controls.Add(this.lblInfo);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Size = new System.Drawing.Size(924, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(924, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(98, 30);
            this.HeaderText.Text = "Reports";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "All Accounts",
            "Range",
            "From Extract",
            "Single"});
            this.cmbPrint.Location = new System.Drawing.Point(20, 30);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(214, 40);
            this.cmbPrint.TabIndex = 33;
            this.cmbPrint.Text = "All Accounts";
            this.ToolTip1.SetToolTip(this.cmbPrint, null);
            this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.optPrint_CheckedChanged);
            // 
            // cmbInfo
            // 
            this.cmbInfo.Items.AddRange(new object[] {
            "By Account",
            "By Card"});
            this.cmbInfo.Location = new System.Drawing.Point(480, 190);
            this.cmbInfo.Name = "cmbInfo";
            this.cmbInfo.Size = new System.Drawing.Size(163, 40);
            this.cmbInfo.TabIndex = 15;
            this.cmbInfo.Text = "By Account";
            this.ToolTip1.SetToolTip(this.cmbInfo, null);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(325, 204);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(150, 15);
            this.lblInfo.TabIndex = 29;
            this.lblInfo.Text = "DISPLAY INFORMATION";
            this.ToolTip1.SetToolTip(this.lblInfo, null);
            // 
            // cmbOrder
            // 
            this.cmbOrder.Items.AddRange(new object[] {
            "Account",
            "Name",
            "Map/Lot",
            "Location"});
            this.cmbOrder.Location = new System.Drawing.Point(112, 190);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(163, 40);
            this.cmbOrder.TabIndex = 14;
            this.cmbOrder.Text = "Account";
            this.ToolTip1.SetToolTip(this.cmbOrder, null);
            this.cmbOrder.SelectedIndexChanged += new System.EventHandler(this.optOrder_CheckedChanged);
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(30, 204);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(72, 15);
            this.lblOrder.TabIndex = 31;
            this.lblOrder.Text = "ORDER BY";
            this.ToolTip1.SetToolTip(this.lblOrder, null);
            // 
            // cmbPrevious
            // 
            this.cmbPrevious.Items.AddRange(new object[] {
            "Account",
            "Date Range"});
            this.cmbPrevious.Location = new System.Drawing.Point(92, 30);
            this.cmbPrevious.Name = "cmbPrevious";
            this.cmbPrevious.Size = new System.Drawing.Size(185, 40);
            this.cmbPrevious.TabIndex = 34;
            this.cmbPrevious.Text = "Account";
            this.ToolTip1.SetToolTip(this.cmbPrevious, null);
            this.cmbPrevious.SelectedIndexChanged += new System.EventHandler(this.optPrevious_CheckedChanged);
            // 
            // lblPrevious
            // 
            this.lblPrevious.AutoSize = true;
            this.lblPrevious.Location = new System.Drawing.Point(20, 44);
            this.lblPrevious.Name = "lblPrevious";
            this.lblPrevious.Size = new System.Drawing.Size(64, 15);
            this.lblPrevious.TabIndex = 35;
            this.lblPrevious.Text = "PRINT BY";
            this.ToolTip1.SetToolTip(this.lblPrevious, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkInclusive);
            this.Frame1.Controls.Add(this.cmbPrint);
            this.Frame1.Controls.Add(this.txtTo);
            this.Frame1.Controls.Add(this.txtFrom);
            this.Frame1.Controls.Add(this.lblTo);
            this.Frame1.Controls.Add(this.lblFrom);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(847, 140);
            this.Frame1.TabIndex = 13;
            this.Frame1.Text = "Print";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // chkInclusive
            // 
            this.chkInclusive.Location = new System.Drawing.Point(30, 90);
            this.chkInclusive.Name = "chkInclusive";
            this.chkInclusive.Size = new System.Drawing.Size(259, 27);
            this.chkInclusive.TabIndex = 32;
            this.chkInclusive.Text = "Records start with range criteria";
            this.ToolTip1.SetToolTip(this.chkInclusive, "If option is checked a range of 001 to 002 would also include maps starting with " +
        "002. If unchecked the range is strictly maps between 001 and 002.");
            this.chkInclusive.Visible = false;
            // 
            // txtTo
            // 
            this.txtTo.BackColor = System.Drawing.SystemColors.Window;
            this.txtTo.Location = new System.Drawing.Point(589, 30);
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(130, 40);
            this.txtTo.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtTo, null);
            this.txtTo.Visible = false;
            // 
            // txtFrom
            // 
            this.txtFrom.BackColor = System.Drawing.SystemColors.Window;
            this.txtFrom.Location = new System.Drawing.Point(369, 30);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(130, 40);
            this.txtFrom.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtFrom, null);
            this.txtFrom.Visible = false;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(536, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 18);
            this.lblTo.TabIndex = 31;
            this.lblTo.Text = "TO";
            this.ToolTip1.SetToolTip(this.lblTo, null);
            this.lblTo.Visible = false;
            // 
            // lblFrom
            // 
            this.lblFrom.Location = new System.Drawing.Point(288, 44);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(107, 18);
            this.lblFrom.TabIndex = 30;
            this.lblFrom.Text = "FROM";
            this.ToolTip1.SetToolTip(this.lblFrom, null);
            this.lblFrom.Visible = false;
            // 
            // t2kDateStart
            // 
            this.t2kDateStart.Location = new System.Drawing.Point(405, 191);
            this.t2kDateStart.Mask = "##/##/####";
            this.t2kDateStart.MaxLength = 10;
            this.t2kDateStart.Name = "t2kDateStart";
            this.t2kDateStart.Size = new System.Drawing.Size(113, 40);
            this.t2kDateStart.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.t2kDateStart, null);
            this.t2kDateStart.Visible = false;
            // 
            // t2kDateEnd
            // 
            this.t2kDateEnd.Location = new System.Drawing.Point(650, 191);
            this.t2kDateEnd.Mask = "##/##/####";
            this.t2kDateEnd.MaxLength = 10;
            this.t2kDateEnd.Name = "t2kDateEnd";
            this.t2kDateEnd.Size = new System.Drawing.Size(113, 40);
            this.t2kDateEnd.TabIndex = 27;
            this.ToolTip1.SetToolTip(this.t2kDateEnd, null);
            this.t2kDateEnd.Visible = false;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.Frame5);
            this.Frame2.Controls.Add(this.framComparison);
            this.Frame2.Controls.Add(this.chkBldgValue);
            this.Frame2.Controls.Add(this.chkArchiveEntries);
            this.Frame2.Controls.Add(this.Grid);
            this.Frame2.Location = new System.Drawing.Point(30, 250);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(847, 360);
            this.Frame2.TabIndex = 16;
            this.Frame2.Text = "Format";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtDate);
            this.Frame5.Controls.Add(this.cmbPrevious);
            this.Frame5.Controls.Add(this.lblPrevious);
            this.Frame5.Controls.Add(this.txtAccount);
            this.Frame5.Controls.Add(this.lblend);
            this.Frame5.Controls.Add(this.lblstart);
            this.Frame5.Location = new System.Drawing.Point(20, 252);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(802, 80);
            this.Frame5.TabIndex = 17;
            this.Frame5.Text = "Print";
            this.ToolTip1.SetToolTip(this.Frame5, null);
            this.Frame5.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(600, 30);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(185, 40);
            this.txtDate.TabIndex = 33;
            this.ToolTip1.SetToolTip(this.txtDate, null);
            this.txtDate.Visible = false;
            // 
            // txtAccount
            // 
            this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccount.Location = new System.Drawing.Point(357, 30);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(185, 40);
            this.txtAccount.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.txtAccount, null);
            this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
            this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
            this.txtAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAccount_KeyPress);
            this.txtAccount.KeyUp += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyUp);
            // 
            // lblend
            // 
            this.lblend.Location = new System.Drawing.Point(562, 44);
            this.lblend.Name = "lblend";
            this.lblend.Size = new System.Drawing.Size(30, 16);
            this.lblend.TabIndex = 22;
            this.lblend.Text = "END";
            this.ToolTip1.SetToolTip(this.lblend, null);
            this.lblend.Visible = false;
            // 
            // lblstart
            // 
            this.lblstart.Location = new System.Drawing.Point(305, 44);
            this.lblstart.Name = "lblstart";
            this.lblstart.Size = new System.Drawing.Size(38, 16);
            this.lblstart.TabIndex = 21;
            this.lblstart.Text = "START";
            this.ToolTip1.SetToolTip(this.lblstart, null);
            this.lblstart.Visible = false;
            // 
            // framComparison
            // 
            this.framComparison.Controls.Add(this.t2kDeletedSince);
            this.framComparison.Controls.Add(this.chkShowDeleted);
            this.framComparison.Controls.Add(this.chkChanges);
            this.framComparison.Location = new System.Drawing.Point(20, 252);
            this.framComparison.Name = "framComparison";
            this.framComparison.Size = new System.Drawing.Size(802, 80);
            this.framComparison.TabIndex = 23;
            this.framComparison.Text = "Show";
            this.ToolTip1.SetToolTip(this.framComparison, null);
            this.framComparison.Visible = false;
            // 
            // t2kDeletedSince
            // 
            this.t2kDeletedSince.Location = new System.Drawing.Point(430, 25);
            this.t2kDeletedSince.Mask = "##/##/####";
            this.t2kDeletedSince.Name = "t2kDeletedSince";
            this.t2kDeletedSince.TabIndex = 34;
            this.ToolTip1.SetToolTip(this.t2kDeletedSince, null);
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.Location = new System.Drawing.Point(193, 30);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(202, 27);
            this.chkShowDeleted.TabIndex = 25;
            this.chkShowDeleted.Text = "Accounts Deleted Since";
            this.ToolTip1.SetToolTip(this.chkShowDeleted, null);
            this.chkShowDeleted.Visible = false;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // chkChanges
            // 
            this.chkChanges.Location = new System.Drawing.Point(20, 30);
            this.chkChanges.Name = "chkChanges";
            this.chkChanges.Size = new System.Drawing.Size(131, 27);
            this.chkChanges.TabIndex = 24;
            this.chkChanges.Text = "Changes Only";
            this.ToolTip1.SetToolTip(this.chkChanges, null);
            // 
            // chkBldgValue
            // 
            this.chkBldgValue.Location = new System.Drawing.Point(20, 252);
            this.chkBldgValue.Name = "chkBldgValue";
            this.chkBldgValue.Size = new System.Drawing.Size(334, 27);
            this.chkBldgValue.TabIndex = 37;
            this.chkBldgValue.Text = "Only include properties with building value";
            this.ToolTip1.SetToolTip(this.chkBldgValue, "If this option is not checked the current entries will appear on the next report");
            this.chkBldgValue.Visible = false;
            // 
            // chkArchiveEntries
            // 
            this.chkArchiveEntries.Checked = true;
            this.chkArchiveEntries.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkArchiveEntries.Location = new System.Drawing.Point(20, 252);
            this.chkArchiveEntries.Name = "chkArchiveEntries";
            this.chkArchiveEntries.Size = new System.Drawing.Size(314, 27);
            this.chkArchiveEntries.TabIndex = 36;
            this.chkArchiveEntries.Text = "Move all current entries to audit archive";
            this.ToolTip1.SetToolTip(this.chkArchiveEntries, "If this option is not checked the current entries will appear on the next report");
            this.chkArchiveEntries.Visible = false;
            // 
            // Grid
            // 
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(20, 30);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 0;
            this.Grid.Size = new System.Drawing.Size(802, 202);
            this.Grid.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.Grid, null);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            // 
            // lblEndDate
            // 
            this.lblEndDate.Location = new System.Drawing.Point(543, 204);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(90, 18);
            this.lblEndDate.TabIndex = 29;
            this.lblEndDate.Text = "END DATE";
            this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblEndDate, null);
            this.lblEndDate.Visible = false;
            // 
            // lblStartDate
            // 
            this.lblStartDate.Location = new System.Drawing.Point(302, 204);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(82, 18);
            this.lblStartDate.TabIndex = 28;
            this.lblStartDate.Text = "START DATE";
            this.lblStartDate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblStartDate, null);
            this.lblStartDate.Visible = false;
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(338, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(134, 48);
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // frmListing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(924, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmListing";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Reports";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmListing_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmListing_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclusive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framComparison)).EndInit();
            this.framComparison.ResumeLayout(false);
            this.framComparison.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDeletedSince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBldgValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkArchiveEntries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}

       
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdPrintPreview;
	}
}
