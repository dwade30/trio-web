﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewLandBreakdown.
	/// </summary>
	partial class rptNewLandBreakdown
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewLandBreakdown));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAcreage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAcreage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCode,
				this.txtCount,
				this.txtCategory,
				this.txtAssess,
				this.txtAcreage,
				this.Field6,
				this.txtTotCount,
				this.txtTotAssess,
				this.txtTotAcreage
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5
			});
			this.ReportHeader.Height = 0.21875F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'";
			this.Field1.Text = "LAND CODE";
			this.Field1.Top = 0F;
			this.Field1.Width = 0.9375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 1.9375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'";
			this.Field2.Text = "CATEGORY";
			this.Field2.Top = 0F;
			this.Field2.Width = 0.9375F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 3.625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field3.Text = "COUNT";
			this.Field3.Top = 0F;
			this.Field3.Width = 0.9375F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 5.3125F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field4.Text = "ASSESSMENT";
			this.Field4.Top = 0F;
			this.Field4.Width = 0.9375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 6.4375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field5.Text = "ACREAGE";
			this.Field5.Top = 0F;
			this.Field5.Width = 0.9375F;
			// 
			// txtCode
			// 
			this.txtCode.CanGrow = false;
			this.txtCode.Height = 0.19F;
			this.txtCode.Left = 0F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtCode.Text = "Field1";
			this.txtCode.Top = 0.03125F;
			this.txtCode.Width = 1.875F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 3.875F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.03125F;
			this.txtCount.Width = 0.6875F;
			// 
			// txtCategory
			// 
			this.txtCategory.CanGrow = false;
			this.txtCategory.Height = 0.19F;
			this.txtCategory.Left = 1.9375F;
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtCategory.Text = null;
			this.txtCategory.Top = 0.03125F;
			this.txtCategory.Width = 1.875F;
			// 
			// txtAssess
			// 
			this.txtAssess.CanGrow = false;
			this.txtAssess.Height = 0.19F;
			this.txtAssess.Left = 4.6875F;
			this.txtAssess.MultiLine = false;
			this.txtAssess.Name = "txtAssess";
			this.txtAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssess.Text = "Field1";
			this.txtAssess.Top = 0.03125F;
			this.txtAssess.Width = 1.5625F;
			// 
			// txtAcreage
			// 
			this.txtAcreage.CanGrow = false;
			this.txtAcreage.Height = 0.19F;
			this.txtAcreage.Left = 6.3125F;
			this.txtAcreage.MultiLine = false;
			this.txtAcreage.Name = "txtAcreage";
			this.txtAcreage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAcreage.Text = "Field1";
			this.txtAcreage.Top = 0.03125F;
			this.txtAcreage.Width = 1.0625F;
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.19F;
			this.Field6.Left = 1.6875F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'";
			this.Field6.Text = "LAND CODE TOTAL";
			this.Field6.Top = 0.25F;
			this.Field6.Width = 1.875F;
			// 
			// txtTotCount
			// 
			this.txtTotCount.Height = 0.19F;
			this.txtTotCount.Left = 3.875F;
			this.txtTotCount.Name = "txtTotCount";
			this.txtTotCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotCount.Text = null;
			this.txtTotCount.Top = 0.25F;
			this.txtTotCount.Width = 0.6875F;
			// 
			// txtTotAssess
			// 
			this.txtTotAssess.CanGrow = false;
			this.txtTotAssess.Height = 0.19F;
			this.txtTotAssess.Left = 4.6875F;
			this.txtTotAssess.MultiLine = false;
			this.txtTotAssess.Name = "txtTotAssess";
			this.txtTotAssess.Style = "font-family: \'0\'; text-align: right";
			this.txtTotAssess.Text = "Field1";
			this.txtTotAssess.Top = 0.25F;
			this.txtTotAssess.Width = 1.5625F;
			// 
			// txtTotAcreage
			// 
			this.txtTotAcreage.CanGrow = false;
			this.txtTotAcreage.Height = 0.19F;
			this.txtTotAcreage.Left = 6.3125F;
			this.txtTotAcreage.MultiLine = false;
			this.txtTotAcreage.Name = "txtTotAcreage";
			this.txtTotAcreage.Style = "font-family: \'0\'; text-align: right";
			this.txtTotAcreage.Text = "Field1";
			this.txtTotAcreage.Top = 0.25F;
			this.txtTotAcreage.Width = 1.0625F;
			// 
			// rptNewLandBreakdown
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAcreage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAcreage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
