﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandExtract.
	/// </summary>
	public partial class frmLandExtract : BaseForm
	{
		public frmLandExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLandExtract InstancePtr
		{
			get
			{
				return (frmLandExtract)Sys.GetInstance(typeof(frmLandExtract));
			}
		}

		protected frmLandExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private struct AcctInfo
		{
			// vbPorter upgrade warning: Account As int	OnRead(string)
			public int Account;
			public short CARD;
			public string Name;
			public string MapLot;
			public int Neighborhood;
			public string NeighborhoodDesc;
			public int Zone;
			public string ZoneDesc;
			public int SecondaryZone;
			public string SecondaryZoneDesc;
			public bool boolPricedAsSecondary;
			// vbPorter upgrade warning: Schedule As int	OnWriteFCConvert.ToDouble(
			public int Schedule;
			public string ScheduleDesc;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public AcctInfo(int unusedParam)
			{
				this.Account = 0;
				this.CARD = 0;
				this.Name = string.Empty;
				this.MapLot = string.Empty;
				this.Neighborhood = 0;
				this.NeighborhoodDesc = string.Empty;
				this.Zone = 0;
				this.ZoneDesc = string.Empty;
				this.SecondaryZone = 0;
				this.SecondaryZoneDesc = string.Empty;
				this.boolPricedAsSecondary = false;
				this.Schedule = 0;
				this.ScheduleDesc = string.Empty;
			}
		};

		private clsDRWrapper rsInfluence = new clsDRWrapper();
		private clsLandTableRecord tScheds = new clsLandTableRecord();

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//FileSystemObject fso = new FileSystemObject();
				string strPath;
				string strFile = "";
				strPath = Directory.GetParent(txtFilename.Text).FullName;
				if (Strings.Trim(strPath) == "")
					strPath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				if (Directory.Exists(txtFilename.Text))
				{
					strPath = txtFilename.Text;
				}
				else
				{
					strFile = Strings.Trim(new FileInfo(txtFilename.Text).Name);
				}
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.Filter = "csv files|*.csv";
				MDIParent.InstancePtr.CommonDialog1.DefaultExt = "csv";
				MDIParent.InstancePtr.CommonDialog1.InitDir = strPath;
				if (strFile != "")
				{
					MDIParent.InstancePtr.CommonDialog1.FileName = strFile;
				}
				else
				{
					MDIParent.InstancePtr.CommonDialog1.FileName = "";
				}
				MDIParent.InstancePtr.CommonDialog1.ShowSave();
				txtFilename.Text = MDIParent.InstancePtr.CommonDialog1.FileName;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 32755)
					return;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Browse", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmLandExtract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLandExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLandExtract properties;
			//frmLandExtract.FillStyle	= 0;
			//frmLandExtract.ScaleWidth	= 5880;
			//frmLandExtract.ScaleHeight	= 4170;
			//frmLandExtract.LinkTopic	= "Form2";
			//frmLandExtract.LockControls	= true;
			//frmLandExtract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			string strFile;
			strFile = modRegistry.GetRegistryKey("LandExtractFile", "RE", "LandExtract.csv");
			txtFilename.Text = strFile;
		}

		private void mnuCreateFile_Click(object sender, System.EventArgs e)
		{
			CreateFile();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void CreateFile()
		{
			bool boolFileOpen = false;
			StreamWriter ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				string strFile;
				string strSQL = "";
				//FileSystemObject fso = new FileSystemObject();
				string strPath;
				int lngUID = 0;
				string strOut = "";
				if (Strings.Trim(txtFilename.Text) == "")
				{
					MessageBox.Show("You must provide a filename to save as", "No Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				strPath = Directory.GetParent(txtFilename.Text).FullName;
				if (strPath == "")
					strPath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strPath, 1) != "\\")
				{
					strPath += "\\";
				}
				if (Directory.Exists(txtFilename.Text))
				{
					MessageBox.Show("This is a valid path but you did not provide a valid filename", "Invalid Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Strings.Trim(new FileInfo(txtFilename.Text).Extension) == "")
				{
					txtFilename.Text = txtFilename.Text + ".csv";
				}
				if (File.Exists(txtFilename.Text))
				{
					if (MessageBox.Show("File " + txtFilename.Text + " already exists." + "\r\n" + "Do you wish to overwrite it?", "Overwrite?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
					{
						return;
					}
				}
				if (!Directory.Exists(strPath))
				{
					if (MessageBox.Show("Path " + strPath + " does not exist." + "\r\n" + "Do you wish to create it?", "Create Folder?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
					{
						return;
					}
				}
				boolFileOpen = false;
				strFile = txtFilename.Text;
				modRegistry.SaveRegistryKey("LandExtractFile", strFile, "RE");
				clsDRWrapper RSMast = new clsDRWrapper();
				if (cmbAll.Text == "Account")
				{
					if (Conversion.Val(txtStart.Text) > 0)
					{
						if (Conversion.Val(txtEnd.Text) > 0)
						{
							if (Conversion.Val(txtEnd.Text) < Conversion.Val(txtStart.Text))
							{
								MessageBox.Show("The end range is less than the start range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							strSQL = "Select * from master where not rsdeleted = 1 and rsaccount >= " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and rsaccount <= " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " order by rsaccount,rscard";
						}
						else
						{
							strSQL = "Select * from master where not rsdeleted = 1 and rsaccount >= " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " order by rsaccount,rscard";
						}
					}
					else if (Conversion.Val(txtEnd.Text) > 0)
					{
						strSQL = "Select * from master where not rsdeleted = 1 and rsaccount <= " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " order by rsaccount,rscard";
					}
					else
					{
						MessageBox.Show("You chose to use a range of account but have not supplied a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAll.Text == "Map Lot")
				{
					if (Strings.Trim(txtStart.Text) != "")
					{
						if (Strings.Trim(txtEnd.Text) != "")
						{
							if (Strings.CompareString(Strings.Trim(txtEnd.Text), "<", Strings.Trim(txtStart.Text)))
							{
								MessageBox.Show("The end range is less than the start range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							strSQL = "select * from master where not rsdeleted = 1 and rsmaplot >= '" + txtStart.Text + "' and rsmaplot <= '" + txtEnd.Text + "zzz' order by rsmaplot,rsaccount,rscard";
						}
						else
						{
							strSQL = "Select * from master where not rsdeleted = 1 and rsmaplot >= '" + txtStart.Text + "' order by rsmaplot,rsaccount,rscard";
						}
					}
					else if (Strings.Trim(txtEnd.Text) != "")
					{
						strSQL = "Select * from master where not rsdeleted = 1 and rsmaplot <= '" + txtEnd.Text + "zzz' order by rsmaplot,rsaccount,rscard";
					}
					else
					{
						MessageBox.Show("You chose to use a range of Map Lot but have not supplied a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAll.Text == "Name")
				{
					if (Strings.Trim(txtStart.Text) != "")
					{
						if (Strings.Trim(txtEnd.Text) != "")
						{
							if (Strings.CompareString(Strings.Trim(txtEnd.Text), "<", Strings.Trim(txtStart.Text)))
							{
								MessageBox.Show("The end range is less that the start range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							strSQL = "Select * from master where not rsdeleted = 1 and rsname >= '" + txtStart.Text + "' and <= '" + txtEnd.Text + "zzz' order by rsname,rsaccount,rscard";
						}
						else
						{
							strSQL = "Select * from master where not rsdeleted = 1 and rsname >= '" + txtStart.Text + "' order by rsname,rsaccount,rscard";
						}
					}
					else if (Strings.Trim(txtEnd.Text) != "")
					{
						strSQL = "Select * from master where not rsdeleted = 1 and rsname <= '" + txtEnd.Text + "zzz' order by rsname,rsaccount,rscard";
					}
					else
					{
						MessageBox.Show("You chose to use a range of name but have not supplied a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAll.Text == "From Extract")
				{
					lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
					strSQL = "select * from master where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + ") AND master.rsdeleted = 0 order by rsaccount,rscard";
				}
				else
				{
					// all
					strSQL = "Select * from master where not rsdeleted = 1 order by rsaccount,rscard";
				}
				RSMast.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				int lngAccount;
				string strName = "";
				string strMapLot = "";
				bool boolDynamic = false;
				bool boolCard = false;
				bool boolName = false;
				bool boolAcres = false;
				if (cmbLandUnitOneColumn.Text == "Let land type determine format")
				{
					boolDynamic = true;
				}
				else
				{
					boolDynamic = false;
				}
				if (chkCard.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolCard = true;
				}
				else
				{
					boolCard = false;
				}
				if (chkOwner.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolName = true;
				}
				else
				{
					boolName = false;
				}
				if (chkAcres.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolAcres = true;
				}
				else
				{
					boolAcres = false;
				}
				if (!RSMast.EndOfFile())
				{
					modGlobalVariables.Statics.LandTypes.Init();
					clsDRWrapper rsScheds = new clsDRWrapper();
					rsScheds.OpenRecordset("select * from landschedule where code = -3", modGlobalVariables.strREDatabase);
					rsInfluence.OpenRecordset("select * from costrecord where crecordnumber between 1441 and 1449 order by crecordnumber", modGlobalVariables.strREDatabase);
					//FC:FINAL:RPU - Use custom constructor to initialize fields
					AcctInfo tInfo = new AcctInfo(0);
					//FC:FINAL:RPU:#1355 - Check for file existance in UserData folder
					//if (File.Exists(txtFilename.Text))
					//{
					//    File.Delete(strFile);
					//}
					//ts = File.CreateText(strFile);
					if (FCFileSystem.FileExists(txtFilename.Text))
					{
						FCFileSystem.DeleteFile(strFile);
					}
					ts = File.CreateText(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFile));
					boolFileOpen = true;
					if (chkColumnHeaders.CheckState == Wisej.Web.CheckState.Checked)
					{
						strOut = FCConvert.ToString(Convert.ToChar(34)) + "Account" + FCConvert.ToString(Convert.ToChar(34));
						if (boolCard)
						{
							strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Card" + FCConvert.ToString(Convert.ToChar(34));
						}
						if (boolName)
						{
							strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Name" + FCConvert.ToString(Convert.ToChar(34));
						}
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Map Lot" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Neighborhood" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Neighborhood Description" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Zone" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Zone Description" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "2nd Zone" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "2nd Zone Description" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Priced On 2nd" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Schedule" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Schedule Description" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Land Type" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Land Type Description" + FCConvert.ToString(Convert.ToChar(34));
						if (boolDynamic)
						{
							strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Units 1" + FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)) + "Units 2" + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Units" + FCConvert.ToString(Convert.ToChar(34));
						}
						if (boolAcres)
						{
							strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Acres" + FCConvert.ToString(Convert.ToChar(34));
						}
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Influence" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Influence Code" + FCConvert.ToString(Convert.ToChar(34));
						strOut += "," + FCConvert.ToString(Convert.ToChar(34)) + "Influence Description" + FCConvert.ToString(Convert.ToChar(34));
						ts.WriteLine(strOut);
					}
					int intNeighborhood;
					string strNeighborhood = "";
					int intZone;
					string strZone = "";
					int intSecondaryZone;
					string strSecondaryZone = "";
					int intSchedule;
					string strSchedule = "";
					short x;
					clsDRWrapper rsZones = new clsDRWrapper();
					clsDRWrapper rsNeigh = new clsDRWrapper();
					rsZones.OpenRecordset("select * from zones order by code", modGlobalVariables.strREDatabase);
					rsNeigh.OpenRecordset("select * from neighborhood order by code", modGlobalVariables.strREDatabase);
					RSMast.MoveLast();
					RSMast.MoveFirst();
					int lngMax = 0;
					double dblincrement = 0;
					lngMax = RSMast.RecordCount();
					double dblProgress = 0;
					double dblCount = 0;
					dblCount = 0;
					dblProgress = 0;
					if (lngMax > 0)
					{
						dblincrement = 1 / (lngMax / 100.0);
						frmWait.InstancePtr.Init("Creating File" + "\r\n" + "\r\n" + "Please Wait", true, 100);
						// frmWait.Image1.Visible = False
					}
					while (!RSMast.EndOfFile())
					{
						//Application.DoEvents();
						if (FCConvert.ToInt32(RSMast.Get_Fields_Int32("rscard")) == 1)
						{
							tInfo.Name = FCConvert.ToString(RSMast.Get_Fields_String("rsname")).Replace(FCConvert.ToString(Convert.ToChar(34)), "");
							tInfo.MapLot = FCConvert.ToString(RSMast.Get_Fields_String("rsmaplot"));
							if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
							{
								modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("ritrancode"))));
							}
							else
							{
								modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown = FCConvert.ToBoolean(0);
							}
						}
						tInfo.Account = FCConvert.ToInt32(RSMast.Get_Fields_Int32("rsaccount"));
						tInfo.CARD = FCConvert.ToInt16(RSMast.Get_Fields_Int32("rscard"));
						tInfo.Neighborhood = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("pineighborhood"))));
						tInfo.Zone = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("pizone"))));
						tInfo.SecondaryZone = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("piseczone"))));
						tInfo.boolPricedAsSecondary = FCConvert.ToBoolean(RSMast.Get_Fields_Boolean("zoneoverride"));
						if (!tInfo.boolPricedAsSecondary)
						{
							tInfo.Schedule = FCConvert.ToInt32(tScheds.Schedule(ref tInfo.Neighborhood, ref tInfo.Zone));
						}
						else
						{
							tInfo.Schedule = FCConvert.ToInt32(tScheds.Schedule(ref tInfo.Neighborhood, ref tInfo.SecondaryZone));
						}
						if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
						{
							// If rsScheds.FindFirstRecord(, , "Schedule = " & tInfo.Schedule & " and townnumber = " & CustomizedInfo.CurrentTownNumber) Then
							if (rsScheds.FindFirstRecord2("Schedule,townnumber", FCConvert.ToString(tInfo.Schedule) + "," + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), ","))
							{
								tInfo.ScheduleDesc = FCConvert.ToString(rsScheds.Get_Fields_String("Description"));
							}
							else
							{
								tInfo.ScheduleDesc = "";
							}
							// If rsZones.FindFirstRecord(, , "Code = " & tInfo.Zone & " and townnumber = " & CustomizedInfo.CurrentTownNumber) Then
							if (rsZones.FindFirstRecord2("Code,townnumber", FCConvert.ToString(tInfo.Zone) + "," + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), ","))
							{
								tInfo.ZoneDesc = FCConvert.ToString(rsZones.Get_Fields_String("description"));
							}
							else
							{
								tInfo.ZoneDesc = "";
							}
							// If rsZones.FindFirstRecord(, , "Code = " & tInfo.SecondaryZone & " and townnumber = " & CustomizedInfo.CurrentTownNumber) Then
							if (rsZones.FindFirstRecord2("Code,Townnumber", FCConvert.ToString(tInfo.SecondaryZone) + "," + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), ","))
							{
								tInfo.SecondaryZoneDesc = FCConvert.ToString(rsZones.Get_Fields_String("SecondaryDescription"));
							}
							else
							{
								tInfo.SecondaryZoneDesc = "";
							}
							// If rsNeigh.FindFirstRecord(, , "Code = " & tInfo.Neighborhood & " and townnumber = " & CustomizedInfo.CurrentTownNumber) Then
							if (rsNeigh.FindFirstRecord2("code,townnumber", FCConvert.ToString(tInfo.Neighborhood) + "," + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), ","))
							{
								tInfo.NeighborhoodDesc = FCConvert.ToString(rsNeigh.Get_Fields_String("Description"));
							}
							else
							{
								tInfo.NeighborhoodDesc = "";
							}
						}
						else
						{
							if (rsScheds.FindFirstRecord("Schedule", tInfo.Schedule))
							{
								tInfo.ScheduleDesc = FCConvert.ToString(rsScheds.Get_Fields_String("Description"));
							}
							else
							{
								tInfo.ScheduleDesc = "";
							}
							if (rsZones.FindFirstRecord("Code", tInfo.Zone))
							{
								tInfo.ZoneDesc = FCConvert.ToString(rsZones.Get_Fields_String("description"));
							}
							else
							{
								tInfo.ZoneDesc = "";
							}
							if (rsZones.FindFirstRecord("Code", tInfo.SecondaryZone))
							{
								tInfo.SecondaryZoneDesc = FCConvert.ToString(rsZones.Get_Fields_String("SecondaryDescription"));
							}
							else
							{
								tInfo.SecondaryZoneDesc = "";
							}
							if (rsNeigh.FindFirstRecord("Code", tInfo.Neighborhood))
							{
								tInfo.NeighborhoodDesc = FCConvert.ToString(rsNeigh.Get_Fields_String("Description"));
							}
							else
							{
								tInfo.NeighborhoodDesc = "";
							}
						}
						for (x = 1; x <= 7; x++)
						{
							//Application.DoEvents();
							// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
							if (Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "type")) > 0)
							{
								strOut = MakeRecord(ref boolDynamic, ref tInfo, ref boolCard, ref boolName, ref boolAcres, ref x, ref RSMast);
								if (strOut != "")
								{
									ts.WriteLine(strOut);
								}
							}
						}
						// x
						RSMast.MoveNext();
						dblProgress += dblincrement;
						dblCount += dblincrement;
						if (dblProgress > 100)
							dblProgress = 100;
						frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32(dblProgress);
						if (dblCount >= 1)
						{
							dblCount = 0;
							frmWait.InstancePtr.prgProgress.Refresh();
							//Application.DoEvents();
						}
					}
					ts.Close();
					boolFileOpen = false;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("File Created", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
					//FC:FINAL:RPU:#i1355 - Use Application.Download to download the file to clent
					FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFile), strFile);
				}
				else
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				if (boolFileOpen)
					ts.Close();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CreateFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intLandLine As short	OnWriteFCConvert.ToInt32(
		private string MakeRecord(ref bool boolDynamic, ref AcctInfo tAcct, ref bool boolCard, ref bool boolOwner, ref bool boolAcres, ref short intLandLine, ref clsDRWrapper RSMast)
		{
			string MakeRecord = "";
			// vbPorter upgrade warning: strReturn As string	OnWrite(string, int)
			string strReturn;
			strReturn = "";
			if (!RSMast.EndOfFile())
			{
				strReturn = FCConvert.ToString(tAcct.Account);
				if (boolCard)
				{
					strReturn += "," + FCConvert.ToString(tAcct.CARD);
				}
				if (boolOwner)
				{
					strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + tAcct.Name + FCConvert.ToString(Convert.ToChar(34));
				}
				strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + tAcct.MapLot + FCConvert.ToString(Convert.ToChar(34));
				strReturn += "," + FCConvert.ToString(tAcct.Neighborhood);
				strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + tAcct.NeighborhoodDesc + FCConvert.ToString(Convert.ToChar(34));
				strReturn += "," + FCConvert.ToString(tAcct.Zone);
				strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + tAcct.ZoneDesc + FCConvert.ToString(Convert.ToChar(34));
				strReturn += "," + FCConvert.ToString(tAcct.SecondaryZone);
				strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + tAcct.SecondaryZoneDesc + FCConvert.ToString(Convert.ToChar(34));
				if (tAcct.boolPricedAsSecondary)
				{
					strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + "Y" + FCConvert.ToString(Convert.ToChar(34));
				}
				else
				{
					strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + "N" + FCConvert.ToString(Convert.ToChar(34));
				}
				strReturn += "," + FCConvert.ToString(tAcct.Schedule);
				strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + tAcct.ScheduleDesc + FCConvert.ToString(Convert.ToChar(34));
				int intLandType = 0;
				// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
				intLandType = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "type"))));
				strReturn += "," + FCConvert.ToString(intLandType);
				if (modGlobalVariables.Statics.LandTypes.FindCode(intLandType))
				{
					strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + modGlobalVariables.Statics.LandTypes.Description + FCConvert.ToString(Convert.ToChar(34));
					switch (modGlobalVariables.Statics.LandTypes.UnitsType)
					{
						case modREConstants.CNSTLANDTYPEEXTRAINFLUENCE:
							{
								if (boolDynamic)
								{
									strReturn += ",0,0";
								}
								else
								{
									strReturn += ",0";
								}
								if (boolAcres)
								{
									strReturn += ",0";
								}
								break;
							}
						case modREConstants.CNSTLANDTYPEFRONTFOOT:
						case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
							{
								if (boolDynamic)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strReturn += "," + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA"))) + "," + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB")));
								}
								else
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA"))) + " X " + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB"))) + FCConvert.ToString(Convert.ToChar(34));
								}
								if (boolAcres)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strReturn += "," + Strings.Format((Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA")) * Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB"))) / 43560 * modGlobalVariables.Statics.LandTypes.Factor, "0.00");
								}
								break;
							}
						case modREConstants.CNSTLANDTYPESQUAREFEET:
							{
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								strReturn += "," + Strings.Format(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA") + RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB")), "0.00");
								if (boolDynamic)
								{
									strReturn += ",0";
								}
								if (boolAcres)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strReturn += "," + Strings.Format((Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA") + RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB"))) / 43560 * modGlobalVariables.Statics.LandTypes.Factor, "0.00");
								}
								break;
							}
						default:
							{
								// acres
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								strReturn += "," + Strings.Format(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA") + RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB")) / 100, "0.00");
								if (boolDynamic)
								{
									strReturn += ",0";
								}
								if (boolAcres)
								{
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strReturn += "," + Strings.Format((Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA") + RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB")) / 100) * modGlobalVariables.Statics.LandTypes.Factor, "0.00");
								}
								break;
							}
					}
					//end switch
				}
				else
				{
					strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + "Unknown" + FCConvert.ToString(Convert.ToChar(34));
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					strReturn += "," + Strings.Format(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsA") + RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "UnitsB")) / 100, "0.00");
					if (boolDynamic)
					{
						strReturn += ",0";
					}
					if (boolAcres)
					{
						strReturn += ",0";
					}
				}
				// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
				strReturn += "," + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "inf")));
				// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
				strReturn += "," + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "Infcode")));
				// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
				if (Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "Infcode")) > 0)
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						if (rsInfluence.FindFirstRecord("crecordnumber", 1440 + Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "Infcode"))))
						{
							strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(FCConvert.ToString(rsInfluence.Get_Fields_String("cldesc"))) + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
						}
					}
					else
					{
						// If rsInfluence.FindFirstRecord(, , "crecordnumber = " & 1440 + Val(RSMast.Fields("piland" & intLandLine & "Infcode")) & " and townnumber = " & Val(RSMast.Fields("ritrancode"))) Then
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						if (rsInfluence.FindFirstRecord2("crecordnumber,townnumber", FCConvert.ToString(1440 + Conversion.Val(RSMast.Get_Fields("piland" + FCConvert.ToString(intLandLine) + "Infcode"))) + "," + FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("ritrancode"))), ","))
						{
							strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(FCConvert.ToString(rsInfluence.Get_Fields_String("cldesc"))) + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
						}
					}
				}
				else
				{
					strReturn += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
				}
			}
			MakeRecord = strReturn;
			return MakeRecord;
		}

		private void optAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			txtStart.Visible = true;
			txtEnd.Visible = true;
			lblTo.Visible = true;
			txtStart.Text = "";
			txtEnd.Text = "";
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			txtStart.Visible = false;
			txtEnd.Visible = false;
			lblTo.Visible = false;
			txtStart.Text = "";
			txtEnd.Text = "";
		}

		private void optExtract_CheckedChanged(object sender, System.EventArgs e)
		{
			txtStart.Visible = false;
			txtEnd.Visible = false;
			lblTo.Visible = false;
			txtStart.Text = "";
			txtEnd.Text = "";
		}

		private void optMapLot_CheckedChanged(object sender, System.EventArgs e)
		{
			txtStart.Visible = true;
			txtEnd.Visible = true;
			lblTo.Visible = true;
			txtStart.Text = "";
			txtEnd.Text = "";
		}

		private void optName_CheckedChanged(object sender, System.EventArgs e)
		{
			txtStart.Visible = true;
			txtEnd.Visible = true;
			lblTo.Visible = true;
			txtStart.Text = "";
			txtEnd.Text = "";
		}

		private void cmbAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.SelectedItem.ToString() == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.SelectedItem.ToString() == "Account")
			{
				optAccount_CheckedChanged(sender, e);
			}
			else if (cmbAll.SelectedItem.ToString() == "Name")
			{
				optName_CheckedChanged(sender, e);
			}
			else if (cmbAll.SelectedItem.ToString() == "Map Lot")
			{
				optMapLot_CheckedChanged(sender, e);
			}
			else if (cmbAll.SelectedItem.ToString() == "From Extract")
			{
				optExtract_CheckedChanged(sender, e);
			}
		}
	}
}
