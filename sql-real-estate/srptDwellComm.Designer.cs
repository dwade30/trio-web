﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptComm.
	/// </summary>
	partial class srptComm
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptComm));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOcc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCQ1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDwell1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStories1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeating1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuilt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemodeled1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBase1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCool1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSize1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAdjusted1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotSqft1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReplacement1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSub1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEcon = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOcc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCQ2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDwell2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStories2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeating2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuilt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemodeled2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBase2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCool2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSize2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAdjusted2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalSqft2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReplacement2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSub2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotTotSqft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotSQFT = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOcc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCQ1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwell1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeating1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSize1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSqft1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSub1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOcc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCQ2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwell2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeating2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSize2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSqft2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSub2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotSqft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotSQFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.txtOcc1,
				this.txtCQ1,
				this.txtDwell1,
				this.txtExt1,
				this.txtStories1,
				this.txtHeating1,
				this.txtBuilt1,
				this.txtRemodeled1,
				this.txtBase1,
				this.txtHeatCool1,
				this.txtTotal1,
				this.txtSize1,
				this.txtAdjusted1,
				this.txtTotSqft1,
				this.txtReplacement1,
				this.txtCondition1,
				this.txtPhys1,
				this.txtFunctional1,
				this.txtSub1,
				this.txtEcon,
				this.txtTotVal,
				this.txtOcc2,
				this.txtCQ2,
				this.txtDwell2,
				this.txtExt2,
				this.txtStories2,
				this.txtHeating2,
				this.txtBuilt2,
				this.txtRemodeled2,
				this.txtBase2,
				this.txtHeatCool2,
				this.txtTotal2,
				this.txtSize2,
				this.txtAdjusted2,
				this.txtTotalSqft2,
				this.txtReplacement2,
				this.txtCondition2,
				this.txtPhys2,
				this.txtFunctional2,
				this.txtSub2,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.txtTotTotSqft,
				this.lblTotSQFT
			});
			this.Detail.Height = 3.125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.19625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'";
			this.Label1.Tag = "text";
			this.Label1.Text = "Occupancy Type";
			this.Label1.Top = 0F;
			this.Label1.Width = 1.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'";
			this.Label2.Tag = "text";
			this.Label2.Text = "Class & Quality";
			this.Label2.Top = 0.15625F;
			this.Label2.Width = 1.5625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'";
			this.Label3.Tag = "text";
			this.Label3.Text = "# Dwelling Units";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 1.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'";
			this.Label4.Tag = "text";
			this.Label4.Text = "Exterior";
			this.Label4.Top = 0.46875F;
			this.Label4.Width = 1.5625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'";
			this.Label5.Tag = "text";
			this.Label5.Text = "Stories & Height";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 1.5625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Tag = "text";
			this.Label6.Text = "Heating/Cooling";
			this.Label6.Top = 0.78125F;
			this.Label6.Width = 1.5625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'";
			this.Label7.Tag = "text";
			this.Label7.Text = "Built";
			this.Label7.Top = 0.9375F;
			this.Label7.Width = 1.5625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19625F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'";
			this.Label8.Tag = "text";
			this.Label8.Text = "Remodeled";
			this.Label8.Top = 1.09375F;
			this.Label8.Width = 1.5625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'";
			this.Label9.Tag = "text";
			this.Label9.Text = "Base Cost/Sqft";
			this.Label9.Top = 1.25F;
			this.Label9.Width = 1.5625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19625F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'";
			this.Label10.Tag = "text";
			this.Label10.Text = "Heat-Cool/Sqft";
			this.Label10.Top = 1.40625F;
			this.Label10.Width = 1.5625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19625F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.4375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'";
			this.Label11.Tag = "text";
			this.Label11.Text = "Total";
			this.Label11.Top = 1.5625F;
			this.Label11.Width = 1.1875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19625F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'";
			this.Label12.Tag = "text";
			this.Label12.Text = "Size Factor";
			this.Label12.Top = 1.71875F;
			this.Label12.Width = 1.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19625F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'";
			this.Label13.Tag = "text";
			this.Label13.Text = "Adjusted Cost/Sqft";
			this.Label13.Top = 1.875F;
			this.Label13.Width = 1.5625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19625F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'";
			this.Label14.Tag = "text";
			this.Label14.Text = "Total Square Feet";
			this.Label14.Top = 2.03125F;
			this.Label14.Width = 1.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19625F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'";
			this.Label15.Tag = "text";
			this.Label15.Text = "Replacement Cost";
			this.Label15.Top = 2.1875F;
			this.Label15.Width = 1.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19625F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.0625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'";
			this.Label16.Tag = "text";
			this.Label16.Text = "Condition";
			this.Label16.Top = 2.34375F;
			this.Label16.Width = 1.5625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19625F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.0625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'";
			this.Label17.Tag = "text";
			this.Label17.Text = "% Good Physical";
			this.Label17.Top = 2.5F;
			this.Label17.Width = 1.5625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19625F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.0625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'";
			this.Label18.Tag = "text";
			this.Label18.Text = "       Functional";
			this.Label18.Top = 2.65625F;
			this.Label18.Width = 1.5625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19625F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.0625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'";
			this.Label19.Tag = "text";
			this.Label19.Text = "Subtotal";
			this.Label19.Top = 2.8125F;
			this.Label19.Width = 1.5625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19625F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.0625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'";
			this.Label20.Tag = "text";
			this.Label20.Text = "Economic Factor";
			this.Label20.Top = 2.96875F;
			this.Label20.Width = 1.5625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19625F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'";
			this.Label21.Tag = "text";
			this.Label21.Text = "Total Value";
			this.Label21.Top = 2.96875F;
			this.Label21.Width = 1.125F;
			// 
			// txtOcc1
			// 
			this.txtOcc1.Height = 0.19625F;
			this.txtOcc1.Left = 1.8125F;
			this.txtOcc1.MultiLine = false;
			this.txtOcc1.Name = "txtOcc1";
			this.txtOcc1.Style = "font-family: \'Tahoma\'";
			this.txtOcc1.Tag = "text";
			this.txtOcc1.Text = "Field2";
			this.txtOcc1.Top = 0F;
			this.txtOcc1.Width = 2F;
			// 
			// txtCQ1
			// 
			this.txtCQ1.Height = 0.19625F;
			this.txtCQ1.Left = 1.8125F;
			this.txtCQ1.MultiLine = false;
			this.txtCQ1.Name = "txtCQ1";
			this.txtCQ1.Style = "font-family: \'Tahoma\'";
			this.txtCQ1.Tag = "text";
			this.txtCQ1.Text = "Field3";
			this.txtCQ1.Top = 0.15625F;
			this.txtCQ1.Width = 2F;
			// 
			// txtDwell1
			// 
			this.txtDwell1.Height = 0.19625F;
			this.txtDwell1.Left = 1.8125F;
			this.txtDwell1.MultiLine = false;
			this.txtDwell1.Name = "txtDwell1";
			this.txtDwell1.Style = "font-family: \'Tahoma\'";
			this.txtDwell1.Tag = "text";
			this.txtDwell1.Text = "Field4";
			this.txtDwell1.Top = 0.3125F;
			this.txtDwell1.Width = 2F;
			// 
			// txtExt1
			// 
			this.txtExt1.Height = 0.19625F;
			this.txtExt1.Left = 1.8125F;
			this.txtExt1.MultiLine = false;
			this.txtExt1.Name = "txtExt1";
			this.txtExt1.Style = "font-family: \'Tahoma\'";
			this.txtExt1.Tag = "text";
			this.txtExt1.Text = "Field5";
			this.txtExt1.Top = 0.46875F;
			this.txtExt1.Width = 2F;
			// 
			// txtStories1
			// 
			this.txtStories1.Height = 0.19625F;
			this.txtStories1.Left = 1.8125F;
			this.txtStories1.MultiLine = false;
			this.txtStories1.Name = "txtStories1";
			this.txtStories1.Style = "font-family: \'Tahoma\'";
			this.txtStories1.Tag = "text";
			this.txtStories1.Text = "Field6";
			this.txtStories1.Top = 0.625F;
			this.txtStories1.Width = 2F;
			// 
			// txtHeating1
			// 
			this.txtHeating1.Height = 0.19625F;
			this.txtHeating1.Left = 1.8125F;
			this.txtHeating1.MultiLine = false;
			this.txtHeating1.Name = "txtHeating1";
			this.txtHeating1.Style = "font-family: \'Tahoma\'";
			this.txtHeating1.Tag = "text";
			this.txtHeating1.Text = "Field7";
			this.txtHeating1.Top = 0.78125F;
			this.txtHeating1.Width = 2F;
			// 
			// txtBuilt1
			// 
			this.txtBuilt1.Height = 0.19625F;
			this.txtBuilt1.Left = 1.8125F;
			this.txtBuilt1.MultiLine = false;
			this.txtBuilt1.Name = "txtBuilt1";
			this.txtBuilt1.Style = "font-family: \'Tahoma\'";
			this.txtBuilt1.Tag = "text";
			this.txtBuilt1.Text = "Field8";
			this.txtBuilt1.Top = 0.9375F;
			this.txtBuilt1.Width = 2F;
			// 
			// txtRemodeled1
			// 
			this.txtRemodeled1.Height = 0.19625F;
			this.txtRemodeled1.Left = 1.8125F;
			this.txtRemodeled1.MultiLine = false;
			this.txtRemodeled1.Name = "txtRemodeled1";
			this.txtRemodeled1.Style = "font-family: \'Tahoma\'";
			this.txtRemodeled1.Tag = "text";
			this.txtRemodeled1.Text = "Field9";
			this.txtRemodeled1.Top = 1.09375F;
			this.txtRemodeled1.Width = 2F;
			// 
			// txtBase1
			// 
			this.txtBase1.Height = 0.19625F;
			this.txtBase1.Left = 1.8125F;
			this.txtBase1.MultiLine = false;
			this.txtBase1.Name = "txtBase1";
			this.txtBase1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBase1.Tag = "text";
			this.txtBase1.Text = "Field10";
			this.txtBase1.Top = 1.25F;
			this.txtBase1.Width = 2F;
			// 
			// txtHeatCool1
			// 
			this.txtHeatCool1.Height = 0.19625F;
			this.txtHeatCool1.Left = 1.8125F;
			this.txtHeatCool1.MultiLine = false;
			this.txtHeatCool1.Name = "txtHeatCool1";
			this.txtHeatCool1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHeatCool1.Tag = "text";
			this.txtHeatCool1.Text = "Field11";
			this.txtHeatCool1.Top = 1.40625F;
			this.txtHeatCool1.Width = 2F;
			// 
			// txtTotal1
			// 
			this.txtTotal1.Height = 0.19625F;
			this.txtTotal1.Left = 1.8125F;
			this.txtTotal1.MultiLine = false;
			this.txtTotal1.Name = "txtTotal1";
			this.txtTotal1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal1.Tag = "text";
			this.txtTotal1.Text = "Field12";
			this.txtTotal1.Top = 1.5625F;
			this.txtTotal1.Width = 2F;
			// 
			// txtSize1
			// 
			this.txtSize1.Height = 0.19625F;
			this.txtSize1.Left = 1.8125F;
			this.txtSize1.MultiLine = false;
			this.txtSize1.Name = "txtSize1";
			this.txtSize1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSize1.Tag = "text";
			this.txtSize1.Text = "Field13";
			this.txtSize1.Top = 1.71875F;
			this.txtSize1.Width = 2F;
			// 
			// txtAdjusted1
			// 
			this.txtAdjusted1.Height = 0.19625F;
			this.txtAdjusted1.Left = 1.8125F;
			this.txtAdjusted1.MultiLine = false;
			this.txtAdjusted1.Name = "txtAdjusted1";
			this.txtAdjusted1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAdjusted1.Tag = "text";
			this.txtAdjusted1.Text = "Field14";
			this.txtAdjusted1.Top = 1.875F;
			this.txtAdjusted1.Width = 2F;
			// 
			// txtTotSqft1
			// 
			this.txtTotSqft1.Height = 0.19625F;
			this.txtTotSqft1.Left = 1.8125F;
			this.txtTotSqft1.MultiLine = false;
			this.txtTotSqft1.Name = "txtTotSqft1";
			this.txtTotSqft1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotSqft1.Tag = "text";
			this.txtTotSqft1.Text = "Field15";
			this.txtTotSqft1.Top = 2.03125F;
			this.txtTotSqft1.Width = 2F;
			// 
			// txtReplacement1
			// 
			this.txtReplacement1.Height = 0.19625F;
			this.txtReplacement1.Left = 1.8125F;
			this.txtReplacement1.MultiLine = false;
			this.txtReplacement1.Name = "txtReplacement1";
			this.txtReplacement1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtReplacement1.Tag = "text";
			this.txtReplacement1.Text = "Field16";
			this.txtReplacement1.Top = 2.1875F;
			this.txtReplacement1.Width = 2F;
			// 
			// txtCondition1
			// 
			this.txtCondition1.Height = 0.19625F;
			this.txtCondition1.Left = 1.8125F;
			this.txtCondition1.MultiLine = false;
			this.txtCondition1.Name = "txtCondition1";
			this.txtCondition1.Style = "font-family: \'Tahoma\'";
			this.txtCondition1.Tag = "text";
			this.txtCondition1.Text = "Field17";
			this.txtCondition1.Top = 2.34375F;
			this.txtCondition1.Width = 2F;
			// 
			// txtPhys1
			// 
			this.txtPhys1.Height = 0.19625F;
			this.txtPhys1.Left = 1.8125F;
			this.txtPhys1.MultiLine = false;
			this.txtPhys1.Name = "txtPhys1";
			this.txtPhys1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPhys1.Tag = "text";
			this.txtPhys1.Text = "Field18";
			this.txtPhys1.Top = 2.5F;
			this.txtPhys1.Width = 2F;
			// 
			// txtFunctional1
			// 
			this.txtFunctional1.Height = 0.19625F;
			this.txtFunctional1.Left = 1.8125F;
			this.txtFunctional1.MultiLine = false;
			this.txtFunctional1.Name = "txtFunctional1";
			this.txtFunctional1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFunctional1.Tag = "text";
			this.txtFunctional1.Text = "Field19";
			this.txtFunctional1.Top = 2.65625F;
			this.txtFunctional1.Width = 2F;
			// 
			// txtSub1
			// 
			this.txtSub1.Height = 0.19625F;
			this.txtSub1.Left = 1.8125F;
			this.txtSub1.MultiLine = false;
			this.txtSub1.Name = "txtSub1";
			this.txtSub1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSub1.Tag = "text";
			this.txtSub1.Text = "Field20";
			this.txtSub1.Top = 2.8125F;
			this.txtSub1.Width = 2F;
			// 
			// txtEcon
			// 
			this.txtEcon.Height = 0.19625F;
			this.txtEcon.Left = 2F;
			this.txtEcon.MultiLine = false;
			this.txtEcon.Name = "txtEcon";
			this.txtEcon.Style = "font-family: \'Tahoma\'";
			this.txtEcon.Tag = "text";
			this.txtEcon.Text = "Field2";
			this.txtEcon.Top = 2.96875F;
			this.txtEcon.Width = 1.5625F;
			// 
			// txtTotVal
			// 
			this.txtTotVal.Height = 0.19625F;
			this.txtTotVal.Left = 5.25F;
			this.txtTotVal.Name = "txtTotVal";
			this.txtTotVal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotVal.Tag = "text";
			this.txtTotVal.Text = "Field2";
			this.txtTotVal.Top = 2.96875F;
			this.txtTotVal.Width = 1.4375F;
			// 
			// txtOcc2
			// 
			this.txtOcc2.Height = 0.19625F;
			this.txtOcc2.Left = 4.0625F;
			this.txtOcc2.MultiLine = false;
			this.txtOcc2.Name = "txtOcc2";
			this.txtOcc2.Style = "font-family: \'Tahoma\'";
			this.txtOcc2.Tag = "text";
			this.txtOcc2.Text = null;
			this.txtOcc2.Top = 0F;
			this.txtOcc2.Width = 2F;
			// 
			// txtCQ2
			// 
			this.txtCQ2.Height = 0.19625F;
			this.txtCQ2.Left = 4.0625F;
			this.txtCQ2.MultiLine = false;
			this.txtCQ2.Name = "txtCQ2";
			this.txtCQ2.Style = "font-family: \'Tahoma\'";
			this.txtCQ2.Tag = "text";
			this.txtCQ2.Text = null;
			this.txtCQ2.Top = 0.15625F;
			this.txtCQ2.Width = 2F;
			// 
			// txtDwell2
			// 
			this.txtDwell2.Height = 0.19625F;
			this.txtDwell2.Left = 4.0625F;
			this.txtDwell2.MultiLine = false;
			this.txtDwell2.Name = "txtDwell2";
			this.txtDwell2.Style = "font-family: \'Tahoma\'";
			this.txtDwell2.Tag = "text";
			this.txtDwell2.Text = null;
			this.txtDwell2.Top = 0.3125F;
			this.txtDwell2.Width = 2F;
			// 
			// txtExt2
			// 
			this.txtExt2.Height = 0.19625F;
			this.txtExt2.Left = 4.0625F;
			this.txtExt2.MultiLine = false;
			this.txtExt2.Name = "txtExt2";
			this.txtExt2.Style = "font-family: \'Tahoma\'";
			this.txtExt2.Tag = "text";
			this.txtExt2.Text = null;
			this.txtExt2.Top = 0.46875F;
			this.txtExt2.Width = 2F;
			// 
			// txtStories2
			// 
			this.txtStories2.Height = 0.19625F;
			this.txtStories2.Left = 4.0625F;
			this.txtStories2.MultiLine = false;
			this.txtStories2.Name = "txtStories2";
			this.txtStories2.Style = "font-family: \'Tahoma\'";
			this.txtStories2.Tag = "text";
			this.txtStories2.Text = null;
			this.txtStories2.Top = 0.625F;
			this.txtStories2.Width = 2F;
			// 
			// txtHeating2
			// 
			this.txtHeating2.Height = 0.19625F;
			this.txtHeating2.Left = 4.0625F;
			this.txtHeating2.MultiLine = false;
			this.txtHeating2.Name = "txtHeating2";
			this.txtHeating2.Style = "font-family: \'Tahoma\'";
			this.txtHeating2.Tag = "text";
			this.txtHeating2.Text = null;
			this.txtHeating2.Top = 0.78125F;
			this.txtHeating2.Width = 2F;
			// 
			// txtBuilt2
			// 
			this.txtBuilt2.Height = 0.19625F;
			this.txtBuilt2.Left = 4.0625F;
			this.txtBuilt2.MultiLine = false;
			this.txtBuilt2.Name = "txtBuilt2";
			this.txtBuilt2.Style = "font-family: \'Tahoma\'";
			this.txtBuilt2.Tag = "text";
			this.txtBuilt2.Text = null;
			this.txtBuilt2.Top = 0.9375F;
			this.txtBuilt2.Width = 2F;
			// 
			// txtRemodeled2
			// 
			this.txtRemodeled2.Height = 0.19625F;
			this.txtRemodeled2.Left = 4.0625F;
			this.txtRemodeled2.MultiLine = false;
			this.txtRemodeled2.Name = "txtRemodeled2";
			this.txtRemodeled2.Style = "font-family: \'Tahoma\'";
			this.txtRemodeled2.Tag = "text";
			this.txtRemodeled2.Text = null;
			this.txtRemodeled2.Top = 1.09375F;
			this.txtRemodeled2.Width = 2F;
			// 
			// txtBase2
			// 
			this.txtBase2.Height = 0.19625F;
			this.txtBase2.Left = 4.0625F;
			this.txtBase2.MultiLine = false;
			this.txtBase2.Name = "txtBase2";
			this.txtBase2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBase2.Tag = "text";
			this.txtBase2.Text = null;
			this.txtBase2.Top = 1.25F;
			this.txtBase2.Width = 2F;
			// 
			// txtHeatCool2
			// 
			this.txtHeatCool2.Height = 0.19625F;
			this.txtHeatCool2.Left = 4.0625F;
			this.txtHeatCool2.MultiLine = false;
			this.txtHeatCool2.Name = "txtHeatCool2";
			this.txtHeatCool2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHeatCool2.Tag = "text";
			this.txtHeatCool2.Text = null;
			this.txtHeatCool2.Top = 1.40625F;
			this.txtHeatCool2.Width = 2F;
			// 
			// txtTotal2
			// 
			this.txtTotal2.Height = 0.19625F;
			this.txtTotal2.Left = 4.0625F;
			this.txtTotal2.MultiLine = false;
			this.txtTotal2.Name = "txtTotal2";
			this.txtTotal2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal2.Tag = "text";
			this.txtTotal2.Text = null;
			this.txtTotal2.Top = 1.5625F;
			this.txtTotal2.Width = 2F;
			// 
			// txtSize2
			// 
			this.txtSize2.Height = 0.19625F;
			this.txtSize2.Left = 4.0625F;
			this.txtSize2.MultiLine = false;
			this.txtSize2.Name = "txtSize2";
			this.txtSize2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSize2.Tag = "text";
			this.txtSize2.Text = null;
			this.txtSize2.Top = 1.71875F;
			this.txtSize2.Width = 2F;
			// 
			// txtAdjusted2
			// 
			this.txtAdjusted2.Height = 0.19625F;
			this.txtAdjusted2.Left = 4.0625F;
			this.txtAdjusted2.MultiLine = false;
			this.txtAdjusted2.Name = "txtAdjusted2";
			this.txtAdjusted2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAdjusted2.Tag = "text";
			this.txtAdjusted2.Text = null;
			this.txtAdjusted2.Top = 1.875F;
			this.txtAdjusted2.Width = 2F;
			// 
			// txtTotalSqft2
			// 
			this.txtTotalSqft2.Height = 0.19625F;
			this.txtTotalSqft2.Left = 4.0625F;
			this.txtTotalSqft2.MultiLine = false;
			this.txtTotalSqft2.Name = "txtTotalSqft2";
			this.txtTotalSqft2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalSqft2.Tag = "text";
			this.txtTotalSqft2.Text = null;
			this.txtTotalSqft2.Top = 2.03125F;
			this.txtTotalSqft2.Width = 2F;
			// 
			// txtReplacement2
			// 
			this.txtReplacement2.Height = 0.19625F;
			this.txtReplacement2.Left = 4.0625F;
			this.txtReplacement2.MultiLine = false;
			this.txtReplacement2.Name = "txtReplacement2";
			this.txtReplacement2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtReplacement2.Tag = "text";
			this.txtReplacement2.Text = null;
			this.txtReplacement2.Top = 2.1875F;
			this.txtReplacement2.Width = 2F;
			// 
			// txtCondition2
			// 
			this.txtCondition2.Height = 0.19625F;
			this.txtCondition2.Left = 4.0625F;
			this.txtCondition2.MultiLine = false;
			this.txtCondition2.Name = "txtCondition2";
			this.txtCondition2.Style = "font-family: \'Tahoma\'";
			this.txtCondition2.Tag = "text";
			this.txtCondition2.Text = null;
			this.txtCondition2.Top = 2.34375F;
			this.txtCondition2.Width = 2F;
			// 
			// txtPhys2
			// 
			this.txtPhys2.Height = 0.19625F;
			this.txtPhys2.Left = 4.0625F;
			this.txtPhys2.MultiLine = false;
			this.txtPhys2.Name = "txtPhys2";
			this.txtPhys2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPhys2.Tag = "text";
			this.txtPhys2.Text = null;
			this.txtPhys2.Top = 2.5F;
			this.txtPhys2.Width = 2F;
			// 
			// txtFunctional2
			// 
			this.txtFunctional2.Height = 0.19625F;
			this.txtFunctional2.Left = 4.0625F;
			this.txtFunctional2.MultiLine = false;
			this.txtFunctional2.Name = "txtFunctional2";
			this.txtFunctional2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFunctional2.Tag = "text";
			this.txtFunctional2.Text = null;
			this.txtFunctional2.Top = 2.65625F;
			this.txtFunctional2.Width = 2F;
			// 
			// txtSub2
			// 
			this.txtSub2.Height = 0.19625F;
			this.txtSub2.Left = 4.0625F;
			this.txtSub2.MultiLine = false;
			this.txtSub2.Name = "txtSub2";
			this.txtSub2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSub2.Tag = "text";
			this.txtSub2.Text = null;
			this.txtSub2.Top = 2.8125F;
			this.txtSub2.Width = 2F;
			// 
			// Label22
			// 
			this.Label22.Height = 1.75F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 6.125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'";
			this.Label22.Tag = "text";
			this.Label22.Text = "Data used for calculations supplied by Marshall & Swift which hereby reserves all" + " rights herein.  Copyright 2002, Marshall & Swift.";
			this.Label22.Top = 0F;
			this.Label22.Visible = false;
			this.Label22.Width = 1.3125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19625F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 1.625F;
			this.Label23.MultiLine = false;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label23.Tag = "text";
			this.Label23.Text = "+";
			this.Label23.Top = 1.40625F;
			this.Label23.Width = 0.1875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19625F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.625F;
			this.Label24.MultiLine = false;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label24.Tag = "text";
			this.Label24.Text = "X";
			this.Label24.Top = 1.71875F;
			this.Label24.Width = 0.1875F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19625F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 1.625F;
			this.Label25.MultiLine = false;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label25.Tag = "text";
			this.Label25.Text = "X";
			this.Label25.Top = 2.03125F;
			this.Label25.Width = 0.1875F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19625F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1.625F;
			this.Label26.MultiLine = false;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label26.Tag = "text";
			this.Label26.Text = "X";
			this.Label26.Top = 2.5F;
			this.Label26.Width = 0.1875F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19625F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 1.625F;
			this.Label27.MultiLine = false;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label27.Tag = "text";
			this.Label27.Text = "X";
			this.Label27.Top = 2.65625F;
			this.Label27.Width = 0.1875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19625F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 1.625F;
			this.Label28.MultiLine = false;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label28.Tag = "text";
			this.Label28.Text = "X";
			this.Label28.Top = 2.96875F;
			this.Label28.Width = 0.1875F;
			// 
			// txtTotTotSqft
			// 
			this.txtTotTotSqft.Height = 0.19625F;
			this.txtTotTotSqft.Left = 6.1875F;
			this.txtTotTotSqft.MultiLine = false;
			this.txtTotTotSqft.Name = "txtTotTotSqft";
			this.txtTotTotSqft.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotTotSqft.Tag = "text";
			this.txtTotTotSqft.Text = null;
			this.txtTotTotSqft.Top = 2.03125F;
			this.txtTotTotSqft.Visible = false;
			this.txtTotTotSqft.Width = 1.25F;
			// 
			// lblTotSQFT
			// 
			this.lblTotSQFT.Height = 0.19625F;
			this.lblTotSQFT.HyperLink = null;
			this.lblTotSQFT.Left = 6.1875F;
			this.lblTotSQFT.Name = "lblTotSQFT";
			this.lblTotSQFT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblTotSQFT.Tag = "text";
			this.lblTotSQFT.Text = "Total";
			this.lblTotSQFT.Top = 1.875F;
			this.lblTotSQFT.Visible = false;
			this.lblTotSQFT.Width = 1.25F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Line1,
				this.Line2
			});
			this.GroupHeader1.Height = 0.19625F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// Field1
			// 
			this.Field1.Height = 0.19625F;
			this.Field1.Left = 2.6875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field1.Tag = "bold";
			this.Field1.Text = "Commercial Description";
			this.Field1.Top = 0F;
			this.Field1.Width = 2.125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.09375F;
			this.Line1.Width = 2.59375F;
			this.Line1.X1 = 2.65625F;
			this.Line1.X2 = 0.0625F;
			this.Line1.Y1 = 0.09375F;
			this.Line1.Y2 = 0.09375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.84375F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 2.59375F;
			this.Line2.X1 = 4.84375F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// srptComm
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOcc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCQ1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwell1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeating1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSize1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSqft1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSub1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOcc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCQ2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwell2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeating2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemodeled2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSize2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSqft2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSub2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotSqft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotSQFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOcc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCQ1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDwell1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStories1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeating1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemodeled1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCool1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSize1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAdjusted1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSqft1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSub1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEcon;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOcc2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCQ2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDwell2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStories2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeating2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemodeled2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCool2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSize2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAdjusted2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSqft2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSub2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTotSqft;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotSQFT;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
