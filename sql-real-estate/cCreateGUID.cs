//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Text;
using System.Runtime.InteropServices;


namespace TWRE0000
{
	public class cCreateGUID
	{

		//=========================================================

		DefLng(modGlobalVariables.CommercialClassCategory.a-Z);



		[StructLayout(LayoutKind.Sequential)]
		private struct GUID
		{
			public int data1;
			public short Data2;
			public short Data3;
			public char []Data4 /*?  = new char;
		};


		[DllImport("ole32.dll")]
		private static extern int CoCreateGuid(ref GUID tGUIDStructure);

		[DllImport("ole32.dll")]
		private static extern int StringFromGUID2(ref Any rguid, int lpstrClsId, int cbMax);
		private int StringFromGUID2Wrp(ref Any rguid, void* lpstrClsId, int cbMax)
		{
			int ret = StringFromGUID2(ref rguid, lpstrClsId, cbMax);
			return ret;
		}


		private bool mbNoBraces;
		// 

		public bool bNoBraces
		{
			get
			{
					bool bNoBraces = false;
				bNoBraces = mbNoBraces;
				return bNoBraces;
			}

			set
			{
				mbNoBraces = value;
			}
		}





		public string CreateGUID(ref bool bNoBraces = false)
		{
			string CreateGUID = "";
			string sGUID = ""; // store result here
			GUID tGuid = new GUID(); // get into this structure
			// vbPorter upgrade warning: bGuid As byte()	OnWrite(string)
			byte[] bGuid = null; // get formatted string here
			int lRtn = 0;
			const int clLen = 50;

			Me.bNoBraces = bNoBraces;

			if (CoCreateGuid(ref tGuid)==0) { // use API to get the GUID
				bGuid = Encoding.Unicode.GetBytes(Strings.StrDup(clLen, Strings.Chr(0)));
				lRtn = StringFromGUID2Wrp(ref tGuid, ref bGuid[0], clLen); // use API to format it
				if (lRtn>0) { // truncate nulls
					sGUID = Strings.Mid(Encoding.UTF8.GetString(bGuid), 1, lRtn-1);
				}
				if (FCConvert.CBool(Me.bNoBraces)) {
					CreateGUID = Strings.Mid(sGUID, 2, sGUID.Length-2); // 2005/12/18 Remove braces {}
				} else {
					CreateGUID = sGUID;
				}
			}
			return CreateGUID;
		}

		public bool IsValidGUID(ref object GUID)
		{
			bool IsValidGUID = false;
			IsValidGUID = IsGUIDValid(ref GUID);
			return IsValidGUID;
		}

		public bool IsGUIDValid(ref object GUID)
		{
			bool IsGUIDValid = false;
			const string sSample = "{0547C3D5-FA24-11D0-B3F9-004445535400}";
			string[] ary = null;
			string sTemp;
			int iPos = 0;

			if (fecherFoundation.FCUtils.IsNull(GUID)) return IsGUIDValid; // 2004/12/31 Added
			if (fecherFoundation.FCUtils.IsEmpty(GUID)) return IsGUIDValid; // 2004/12/31 Added

			sTemp = GUID.ToString(); // convert to string
			sTemp = Strings.Trim(sTemp); // 2004/12/31 Make sure no extra spaces

			if (sTemp.Length<(sSample.Length-2)) return IsGUIDValid; // can't be less than min with out braces

			// 2003/03/21 Strip off prefix, if any
			if (sTemp.Length>0) {
				if (Strings.Right(sTemp, 1)=="}") { // 2004/12/31 Prefix in this form supported
					iPos = Strings.InStr(sTemp, "{", CompareConstants.vbBinaryCompare); // in first position?
					if (iPos>1) {
						sTemp = Strings.Mid(sTemp, iPos);
					}
				}
			}

			// 2003/03/21 Add braces if none
			if (sTemp.Length==sSample.Length-2) { // maybe no braces
				if (Strings.Left(sTemp, 1)!="{") {
					sTemp = "{"+sTemp;
				}
				if (Strings.Right(sTemp, 1)!="}") {
					sTemp += "}";
				}
			}

			if (sTemp.Length==sSample.Length) { // correct length
				if (Strings.Left(sTemp, 1)=="{" && Strings.Right(sTemp, 1)=="}") { // has braces
					if (Convert.ToBoolean(Strings.InStr(sTemp, "-", CompareConstants.vbBinaryCompare))) { // 2004/12/31 Must have at least one dash
						ary = Strings.Split(sTemp, "-", -1, CompareConstants.vbBinaryCompare); // right number of dashes
						if (Information.UBound(ary, 1)==4) { // must be this
							if (ary[0].Length=="{0547C3D5".Length) { // correct lengths
								if (ary[1].Length==4) {
									if (ary[2].Length==4) {
										if (ary[3].Length==4) {
											if (ary[4].Length=="004445535400}".Length) {
												IsGUIDValid = true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return IsGUIDValid;
		}

		public string CreateGUIDWithPrefix(ref string sPrefix)
		{
			string CreateGUIDWithPrefix = "";
			string GUID;

			GUID = CreateGUID();
			GUID = sPrefix+GUID;
			CreateGUIDWithPrefix = GUID;

			return CreateGUIDWithPrefix;
		}

		// vbPorter upgrade warning: 'Return' As short	OnWrite(int)
		public short LenGUID(ref object vntGUID = null)
		{
			short LenGUID = 0;
			if (Information.IsNothing(vntGUID)) {
				LenGUID = (short)CreateGUID().Length;
			} else {
				LenGUID = (short)Convert.ToString(vntGUID).Length; // 2005/12/18 Added
			}
			return LenGUID;
		}


	}
}