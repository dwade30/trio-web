﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmaudit.
	/// </summary>
	public partial class frmaudit : BaseForm
	{
		public frmaudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmaudit InstancePtr
		{
			get
			{
				return (frmaudit)Sys.GetInstance(typeof(frmaudit));
			}
		}

		protected frmaudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTRANGEALL = -1;
		const int CNSTRANGERANGE = -2;

		private void cmdcancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			frmaudit.InstancePtr.Unload();
		}

		public void cmdcancel_Click()
		{
			cmdcancel_Click(cmdcancel, new System.EventArgs());
		}

		private void cmdok_Click(object sender, System.EventArgs e)
		{
			string strResponse = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolByTown = false;
			int intTownCode;
			if (cmbtBilOrCor.Text == "Current")
			{
				clsTemp.OpenRecordset("select * from status", modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					if (Information.IsDate(clsTemp.Get_Fields("currentexemptions")))
					{
						if (clsTemp.Get_Fields_DateTime("currentexemptions") != DateTime.FromOADate(0))
						{
							bool boolAsk = false;
							boolAsk = false;
							if (Information.IsDate(clsTemp.Get_Fields("Batchcalc")))
							{
								if (clsTemp.Get_Fields_DateTime("batchcalc") != DateTime.FromOADate(0))
								{
									if (DateAndTime.DateDiff("n", (DateTime)clsTemp.Get_Fields_DateTime("batchcalc"), (DateTime)clsTemp.Get_Fields_DateTime("currentexemptions")) < 0)
									{
										boolAsk = true;
									}
								}
							}
							if (Information.IsDate(clsTemp.Get_Fields("calculated")))
							{
								if (clsTemp.Get_Fields_DateTime("calculated") != DateTime.FromOADate(0))
								{
									if (DateAndTime.DateDiff("n", (DateTime)clsTemp.Get_Fields_DateTime("calculated"), (DateTime)clsTemp.Get_Fields_DateTime("currentexemptions")) < 0)
									{
										boolAsk = true;
									}
								}
							}
							if (boolAsk)
							{
								if (MessageBox.Show("Current exemptions have not been recalculated since account(s) have been recalculated" + "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
								{
									return;
								}
							}
						}
					}
					else
					{
						if (MessageBox.Show("Current exemptions have not been recalculated since account(s) have been recalculated" + "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
						{
							return;
						}
					}
				}
			}
			clsTemp.Execute("delete from summrecord where isnull(cardnumber,0) < 1", modGlobalVariables.strREDatabase);
			if (cmbtinclude.Text == "Detail")
			{
				modGlobalVariables.Statics.whatinclude = 1;
			}
			else if (cmbtinclude.Text == "Subtotal")
			{
				modGlobalVariables.Statics.whatinclude = 2;
				modGlobalVariables.Statics.lengthoffield = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Combo1.Text))));
			}
			else if (cmbtinclude.Text == "Both")
			{
				modGlobalVariables.Statics.whatinclude = 3;
				modGlobalVariables.Statics.lengthoffield = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Combo1.Text))));
			}
			else if (cmbtinclude.Text == "Totals Only")
			{
				modGlobalVariables.Statics.whatinclude = 4;
			}
			// If optRange(0).Value = True Then
			// boolRange = False
			// Else
			// boolRange = True
			// End If
			intTownCode = 0;
			int vbPorterVar = cmbRange.ItemData(cmbRange.SelectedIndex);
			if (vbPorterVar == CNSTRANGEALL)
			{
				modGlobalVariables.Statics.boolRange = false;
				boolByTown = false;
			}
			else if (vbPorterVar == CNSTRANGERANGE)
			{
				modGlobalVariables.Statics.boolRange = true;
				boolByTown = false;
			}
			else
			{
				modGlobalVariables.Statics.boolRange = true;
				boolByTown = true;
				intTownCode = cmbRange.ItemData(cmbRange.SelectedIndex);
			}
			if (chkIncTot.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.boolinctot = true;
			}
			else
			{
				modGlobalVariables.Statics.boolinctot = false;
			}
			if (cmbtBilOrCor.Text == "Billing")
			{
				modGlobalVariables.Statics.boolBillingorCorrelated = true;
			}
			else
			{
				modGlobalVariables.Statics.boolBillingorCorrelated = false;
			}
			if (cmbtorder.Text == "Account")
			{
				modPrintRoutines.Statics.intwhichorder = 1;
			}
			else if (cmbtorder.Text == "Map/Lot")
			{
				modPrintRoutines.Statics.intwhichorder = 2;
			}
			else if (cmbtorder.Text == "Name")
			{
				modPrintRoutines.Statics.intwhichorder = 3;
			}
			else if (cmbtorder.Text == "Location")
			{
				modPrintRoutines.Statics.intwhichorder = 4;
			}
			modPrintRoutines.Statics.gstrMinAccountRange = "";
			modPrintRoutines.Statics.gstrMaxAccountRange = "ZZZZ";
			modGlobalVariables.Statics.gintMinAccountRange = 1;
			modGlobalVariables.Statics.gintMaxAccountRange = 2147483000;
			// almost a max long
			if (modGlobalVariables.Statics.boolRange && cmbtatype.Text != "Exempt" && !boolByTown)
			{
				if (modPrintRoutines.Statics.intwhichorder == 1)
				{
					// by account
					Getranges:
					;
					strResponse = Interaction.InputBox("Please enter the account to start from", "", null/*, -1, -1*/);
					if (strResponse == string.Empty)
						return;
					modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(strResponse)));
					strResponse = Interaction.InputBox("Please enter the account to end with", "", null/*, -1, -1*/);
					if (strResponse == string.Empty)
						return;
					modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(strResponse)));
					if ((modGlobalVariables.Statics.gintMaxAccountRange < modGlobalVariables.Statics.gintMinAccountRange) || (modGlobalVariables.Statics.gintMaxAccountRange == 0))
					{
						MessageBox.Show("This is an invalid range." + "\r\n" + "Please re-enter the range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto Getranges;
					}
					modPrintRoutines.Statics.gstrMinAccountRange = FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange);
					modPrintRoutines.Statics.gstrMaxAccountRange = FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
				}
				else
				{
					getranges2:
					;
					modPrintRoutines.Statics.gstrMinAccountRange = Interaction.InputBox("Please enter the value to start from", "", null/*, -1, -1*/);
					// If gstrMinAccountRange = "a" Or gstrMinAccountRange = "A" Then gstrMinAccountRange = ""
					if (modPrintRoutines.Statics.gstrMinAccountRange == string.Empty)
						return;
					modPrintRoutines.Statics.gstrMaxAccountRange = Interaction.InputBox("Please enter the value to end with", "", null/*, -1, -1*/);
					// If gstrMaxAccountRange = "z" Then gstrMaxAccountRange = "zzzz"
					if (modPrintRoutines.Statics.gstrMaxAccountRange == string.Empty)
						return;
					if ((Strings.CompareString(modPrintRoutines.Statics.gstrMaxAccountRange, "<", modPrintRoutines.Statics.gstrMinAccountRange)) || (modPrintRoutines.Statics.gstrMaxAccountRange == ""))
					{
						MessageBox.Show("This is an invalid range." + "\r\n" + "Please re-enter the range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto getranges2;
					}
					if (modPrintRoutines.Statics.intwhichorder != 2)
					{
						modPrintRoutines.Statics.gstrMaxAccountRange += "zzz";
					}
				}
			}
			frmaudit.InstancePtr.Unload();
			if (cmbtatype.Text == "Exempt")
			{
				frmexemptions.InstancePtr.Show();
				modPrintRoutines.Statics.allorexempt = "exempt";
			}
			else
			{
				modPrintRoutines.Statics.allorexempt = "all";
				// rptaudit.Show
				// Call frmReportViewer.Init(rptaudit)
				rptaudit.InstancePtr.Init(false, ref modGlobalVariables.Statics.whatinclude, ref modPrintRoutines.Statics.intwhichorder, ref modGlobalVariables.Statics.boolBillingorCorrelated, ref modGlobalVariables.Statics.boolRange, ref modPrintRoutines.Statics.gstrMinAccountRange, ref modPrintRoutines.Statics.gstrMaxAccountRange, false, FCConvert.ToInt16(intTownCode));
			}
		}

		public void cmdok_Click()
		{
			cmdok_Click(cmdContinue, new System.EventArgs());
		}

		private void frmaudit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmaudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmaudit properties;
			//frmaudit.ScaleWidth	= 5880;
			//frmaudit.ScaleHeight	= 4485;
			//frmaudit.LinkTopic	= "Form1";
			//frmaudit.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			if (modREMain.Statics.boolShortRealEstate)
			{
				cmbtBilOrCor.Visible = false;
				lbltBilOrCor.Visible = false;
			}
			Combo1.AddItem("1", 0);
			Combo1.AddItem("2", 1);
			Combo1.AddItem("3", 2);
			Combo1.AddItem("4", 3);
			Combo1.AddItem("5", 4);
			Combo1.AddItem("6", 5);
			Combo1.AddItem("7", 6);
			Combo1.AddItem("8", 7);
			Combo1.AddItem("9", 8);
			Combo1.AddItem("10", 9);
			Combo1.AddItem("11", 10);
			Combo1.AddItem("12", 11);
			Combo1.AddItem("13", 12);
			Combo1.AddItem("14", 13);
			Combo1.AddItem("15", 14);
			Combo1.SelectedIndex = 0;
			FillRangeCombo();
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdok_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdcancel_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void Optatype_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				cmbtinclude.Clear();
				cmbtinclude.Items.Add("Detail");
				cmbtinclude.Items.Add("Subtotal");
				cmbtinclude.Items.Add("Both");
				cmbtinclude.Items.Add("Totals Only");
				chkIncTot.Enabled = true;
				// optRange(0).Enabled = True
				// optRange(1).Enabled = True
				cmbRange.Enabled = true;
				cmbtorder.Enabled = true;
				Combo1.Enabled = true;
			}
			else
			{
				// optRange(0).Enabled = False
				// optRange(1).Enabled = False
				cmbRange.Enabled = false;
				cmbtorder.Enabled = false;
				cmbtinclude.Clear();
				cmbtinclude.Items.Add("Subtotal");
				cmbtinclude.Items.Add("Both");
				Combo1.Enabled = false;
				if (cmbtinclude.Text != "Both" && cmbtinclude.Text != "Subtotal")
				{
					cmbtinclude.Text = "Both";
				}
				chkIncTot.Enabled = false;
			}
		}

		private void Optatype_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtatype.SelectedIndex;
			Optatype_CheckedChanged(index, sender, e);
		}

		private void FillRangeCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbRange.Clear();
			cmbRange.AddItem("All");
			cmbRange.ItemData(cmbRange.NewIndex, CNSTRANGEALL);
			cmbRange.AddItem("Range");
			cmbRange.ItemData(cmbRange.NewIndex, CNSTRANGERANGE);
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblRegions where townnumber > 0 order by townnumber", modGlobalVariables.Statics.strGNDatabase);
				while (!clsLoad.EndOfFile())
				{
					cmbRange.AddItem(clsLoad.Get_Fields_String("townname"));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					cmbRange.ItemData(cmbRange.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("townnumber"))));
					clsLoad.MoveNext();
				}
			}
			cmbRange.SelectedIndex = 0;
		}

		private void cmbtatype_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbtatype.SelectedIndex == 0)
			{
				Optatype_CheckedChanged(sender, e);
			}
			else if (cmbtatype.SelectedIndex == 1)
			{
				Optatype_CheckedChanged(sender, e);
			}
		}
	}
}
