﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cHeatingCoolingController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cHeatingCoolingCollection GetCommercialHeatingCoolings()
		{
			cHeatingCoolingCollection GetCommercialHeatingCoolings = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cHeatingCoolingCollection gColl = new cHeatingCoolingCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialHeatingCooling heatCode;
				rs.OpenRecordset("select * from CommercialHeatingCooling order by ID", "RealEstate");
				while (!rs.EndOfFile())
				{
					heatCode = new cCommercialHeatingCooling();
					// TODO Get_Fields: Field [FullDescription] not found!! (maybe it is an alias?)
					heatCode.FullDescription = FCConvert.ToString(rs.Get_Fields("FullDescription"));
					heatCode.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					heatCode.ShortDescription = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					heatCode.CodeNumber = FCConvert.ToInt32(rs.Get_Fields("CodeNumber"));
					heatCode.IsUpdated = false;
					gColl.AddItem(heatCode);
					rs.MoveNext();
				}
				GetCommercialHeatingCoolings = gColl;
				return GetCommercialHeatingCoolings;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialHeatingCoolings;
		}

		public cHeatingCoolingCollection GetCommercialHeatingCoolingsDescriptionOrder()
		{
			cHeatingCoolingCollection GetCommercialHeatingCoolingsDescriptionOrder = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cHeatingCoolingCollection gColl = new cHeatingCoolingCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialHeatingCooling heatCode;
				rs.OpenRecordset("select * from CommercialHeatingCooling order by FullDescription", "RealEstate");
				while (!rs.EndOfFile())
				{
					heatCode = new cCommercialHeatingCooling();
					// TODO Get_Fields: Field [FullDescription] not found!! (maybe it is an alias?)
					heatCode.FullDescription = FCConvert.ToString(rs.Get_Fields("FullDescription"));
					heatCode.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					heatCode.CodeNumber = FCConvert.ToInt32(rs.Get_Fields("CodeNumber"));
					heatCode.ShortDescription = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
					heatCode.IsUpdated = false;
					gColl.AddItem(heatCode);
					rs.MoveNext();
				}
				GetCommercialHeatingCoolingsDescriptionOrder = gColl;
				return GetCommercialHeatingCoolingsDescriptionOrder;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialHeatingCoolingsDescriptionOrder;
		}

		public void DeleteHeatingCoolingCost(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialHeatingCoolingCosts where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteHeatingCoolingRecord(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialHeatingCooling where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteCommercialHeatingCoolings()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("Delete from CommercialHeatingCooling", "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialHeatingCooling(ref cCommercialHeatingCooling heatCool)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (heatCool.IsDeleted)
				{
					DeleteHeatingCoolingRecord(heatCool.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialheatingcooling where id = " + heatCool.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (heatCool.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Heating cooling record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("FullDescription", heatCool.FullDescription);
				rsSave.Set_Fields("ShortDescription", heatCool.ShortDescription);
				rsSave.Set_Fields("CodeNumber", heatCool.CodeNumber);
				rsSave.Update();
				heatCool.ID = rsSave.Get_Fields_Int32("ID");
				heatCool.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveHeatingCoolingCost(ref cHeatingCoolingCost heatCost)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (heatCost.IsDeleted)
				{
					DeleteHeatingCoolingCost(heatCost.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialheatingcoolingcosts where id = " + heatCost.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (heatCost.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Heating cooling cost record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("ExtremeClimateCost", heatCost.ExtremeClimateCost);
				rsSave.Set_Fields("CommercialHeatingCoolingID", heatCost.HeatingCoolingID);
				rsSave.Set_Fields("MildClimateCost", heatCost.MildClimateCost);
				rsSave.Set_Fields("ModerateClimateCost", heatCost.ModerateClimateCost);
				rsSave.Set_Fields("SectionID", heatCost.SectionID);
				rsSave.Update();
				heatCost.ID = rsSave.Get_Fields_Int32("ID");
				heatCost.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteCommercialHeatingCoolingCosts()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("Delete from CommercialHeatingCoolingCosts", "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
