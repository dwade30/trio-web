﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWRE0000
{
	public class modDwelling
	{
		private static void new_functional_percent()
		{
			// corey
			// Wet Basement
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1560 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diWETBSMT"))));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Wet Basement cost record " + FCConvert.ToString(1560 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diwetbsmt"))) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.dblFunctionalPercent = modGlobalVariables.Statics.CostRec.CUnit.ToDoubleValue() / 100;
			// Layout
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1630 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diLAYOUT"))));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Layout cost record " + FCConvert.ToString(1630 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dilayout"))) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.dblFunctionalPercent *= modGlobalVariables.Statics.CostRec.CUnit.ToDoubleValue() / 100;
			// Functional Code
			modProperty.Statics.WKey = 1680 + modDataTypes.Statics.DWL.Get_Fields_Int32("diFUNCTCODE");
			// If WKey = 1680 Then WKey = 1689
			// Call OpenCRTable(CR, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Functional Code cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.dblFunctionalPercent *= modGlobalVariables.Statics.CostRec.CUnit.ToDoubleValue() / 100;
			// Kitchens
			modProperty.Statics.WKey = 1610 + modDataTypes.Statics.DWL.Get_Fields_Int32("diKITCHENS");
			// If WKey = 1610 Then
			// boolCalcErrors = True
			// CalcLog = CalcLog & "Incorrect functional value for " & gintLastAccountNumber & " card " & gintCardNumber & " due to an invalid Kitchen Style" & vbNewLine
			// Else
			// Call OpenCRTable(CR, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Kitchen cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.dblFunctionalPercent *= modGlobalVariables.Statics.CostRec.CUnit.ToDoubleValue() / 100;
			// End If
			// Baths
			modProperty.Statics.WKey = 1620 + modDataTypes.Statics.DWL.Get_Fields_Int32("diBATHS");
			// If WKey = 1620 Then
			// boolCalcErrors = True
			// CalcLog = CalcLog & "Incorrect functional value for " & gintLastAccountNumber & " card " & gintCardNumber & " due to an incorrect Bath Style" & vbNewLine
			// Else
			// Call OpenCRTable(CR, WKey)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Bath cost record " + FCConvert.ToString(modProperty.Statics.WKey) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.dblFunctionalPercent *= modGlobalVariables.Statics.CostRec.CUnit.ToDoubleValue() / 100;
			// End If
			// 
			Statics.dblFunctionalPercent *= modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTFUNCT");
			// dblFunctionalPercent = Int(0.5 + dblFunctionalPercent)
			Statics.dblFunctionalPercent = modGlobalRoutines.Round(Statics.dblFunctionalPercent, 0) / 100;
		}

		public static bool CheckUnits(ref string strField)
		{
			bool CheckUnits = false;
			if (Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("cunit")) > 9999)
			{
				MessageBox.Show("There is an invalid code in the " + strField + " field.", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckUnits = true;
			}
			else
			{
				CheckUnits = false;
			}
			return CheckUnits;
		}

		public static void REAS25_SETUP_RSZ()
		{
			modDataTypes.Statics.RSZ.Set_Fields("RSZ", "BF");
			int x;
			for (x = 1; x <= 25; x++)
			{
				modDataTypes.Statics.RSZ.Set_Fields("rszfct" + Strings.Format(x, "00"), 0);
				modDataTypes.Statics.RSZ.Set_Fields("rszrge" + Strings.Format(x, "00"), "0000");
			}
			// x
		}

		public static void Speed_compute_dwelling()
		{
			// Dim ts As TextStream
			double dblFactorTrend;
			double dblFactor2 = 0;
			// 
			Statics.dblBase = 0;
			Statics.intFixtures = 0;
			Statics.intRoof = 0;
			Statics.intTrim = 0;
			Statics.intOpen3 = 0;
			Statics.intOpen4 = 0;
			Statics.intOpen5 = 0;
			Statics.intFinBsmt = 0;
			Statics.intCool = 0;
			Statics.intFirePlace = 0;
			Statics.intUnfinished = 0;
			Statics.intHeat = 0;
			Statics.intBasement = 0;
			Statics.intPlumbing = 0;
			Statics.intAttic = 0;
			Statics.intInsulation = 0;
			Statics.dblPhysicalPercent = 0;
			modREASValuations.Statics.SaveYear = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diYEARBUILT"));
			modREASValuations.Statics.HOLDYEAR = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diYEARBUILT"));
			// Grade
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1"))));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Grade cost record " + FCConvert.ToString(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1") + "")) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			dblFactorTrend = FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2"));
			// Grade Factor
			// Call GetCostRecordDwelling(1452)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1452);
			dblFactorTrend *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 1000000;
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Trend Factor"
			// End If
			// Dwelling Grade
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1") + "") == 9)
			{
				dblFactor2 = dblFactorTrend * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2")) / 100;
			}
			else
			{
				dblFactor2 = FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2"));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1"))));
				dblFactor2 *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 1000000;
			}
			// Basement Square Footage
			// Call GetCostRecordDwelling(1453)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1453);
			Statics.WBas = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase);
			// Exterior Walls
			if (!modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diextwalls") + "")), "DWELLING", "EXTERIOR"))
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Exterior walls code " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diextwalls"))) + " in account " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " card " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + " is not defined" + "\r\n";
			}
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Exterior walls code " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diextwalls"))) + " has all 9's for a value. Cannot continue calculating Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Exterior"
			// End If
			Statics.WBas *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit);
			// Stories
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1480 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSTORIES"))));
			Statics.WBas *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit);
			Statics.intSFLA = FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) / 100);
			// Style
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("Distyle") + "")), "DWELLING", "STYLE");
			Statics.WBas *= FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit);
			// Dwelling Units
			modProperty.Statics.WKey = FCConvert.ToInt32(1470 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsdwelling")) + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsother")));
			if (modProperty.Statics.WKey > 1479)
				modProperty.Statics.WKey = 1479;
			// Call GetCostRecordDwelling(CInt(WKey))
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				// WBas# = (WBas# * CostRec.CUnit / 100000000)
				Statics.WBas = (Statics.WBas * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 10000);
			}
			else
			{
				// WBas# = (WBas# * CostRec.CUnit / 100000000) + CostRec.CBase
				Statics.WBas = (Statics.WBas * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) / 10000) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase);
			}
			Statics.WBas *= dblFactorTrend;
			Statics.dblBase = (Statics.WBas / 100);
			// SF Masonry Trim
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFMASONRY") + "") != 0)
			{
				// Call GetCostRecordDwelling(1454)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1454);
				Statics.intTrim = ((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFMASONRY")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100;
			}
			// Roof
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1510 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diROOF"))));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Roof Surface cost record " + FCConvert.ToString(1510 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diroof") + "")) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.intRoof = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Roof"
			// End If
			// Open 3
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3")) == 0)
				goto REAS03_4003_TAG;
			// Get #2, 1520, CR
			// Call OpenCRTable(CR, 1520)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1520);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) < 99999999)
				goto REAS03_4002_TAG;
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3")) > 9)
			{
				goto REAS03_4003_TAG;
			}
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1520 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3"))));
			Statics.intOpen3 = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			goto REAS03_4003_TAG;
			REAS03_4002_TAG:
			;
			Statics.intOpen3 = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			REAS03_4003_TAG:
			;
			// Open 4
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4")) == 0)
				goto REAS03_4005_TAG;
			// Get #2, 1530, CR
			// Call OpenCRTable(CR, 1530)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1530);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) < 99999999)
				goto REAS03_4004_TAG;
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4")) > 9)
			{
				goto REAS03_4005_TAG;
			}
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1530 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4"))));
			Statics.intOpen4 = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			goto REAS03_4005_TAG;
			REAS03_4004_TAG:
			;
			Statics.intOpen4 = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			REAS03_4005_TAG:
			;
			// Foundation
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1540 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diFOUNDATION") + "")));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in foundation cost record " + FCConvert.ToString(1540 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("difoundation"))) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.intBasement = ((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT") + "") * (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) / 100) + (FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase) / 100)) * dblFactorTrend);
			// Basement
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1550 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMT") + "")));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Basement cost record " + 1550 + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmt") + "")) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.intBasement = (Statics.intBasement + ((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT") + "") * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Basement"
			// End If
			// SF Basement Living Area
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFBSMTLIVING") + "") != 0)
			{
				// Call GetCostRecordDwelling(1458)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1458);
				Statics.intFinBsmt = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFBSMTLIVING")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactor2) / 100);
			}
			// Basement Garage
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMTGAR") + "") != 0)
			{
				// Call GetCostRecordDwelling(1459)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1459);
				Statics.intFinBsmt = (Statics.intFinBsmt + (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMTGAR")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100));
			}
			// Heating
			if (!modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diheat") + "")), "DWELLING", "HEAT"))
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(0);
				modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(0);
				modSpeedCalc.Statics.CalcLog += "Error using heat code of " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diheat") + "")) + " for Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				// Exit Sub
			}
			Statics.intHeat = ((Statics.intSFLA * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit)) * dblFactorTrend);
			Statics.WORK1 = modGlobalVariables.Statics.CostRec.CUnit;
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diheat") + "") != 9)
			{
				// 9 is no heat
				// Call GetCostRecordDwelling(1599)
				// Call Get_Cost_Record(1599)
				if (!modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(9, "DWELLING", "HEAT"))
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(0);
					modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(0);
					modSpeedCalc.Statics.CalcLog += "Error calculating heat for Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + ". Could not load code 9 (no heat)" + "\r\n";
					// Exit Sub
				}
			}
			//FC:FINAL:MSH - i.issue #1192: invalid cast exception
			//Work3 = ((FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) - FCConvert.ToDouble(WORK1) * (intSFLA * (100 - Conversion.Val(modDataTypes.DWL.Get_Fields("diPCTHEAT") + ""))) * dblFactorTrend) / 100;
			Statics.Work3 = ((FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) - FCConvert.ToDouble(Statics.WORK1)) * (Statics.intSFLA * (100 - Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTHEAT") + ""))) * dblFactorTrend) / 100;
			Statics.intHeat = (Statics.intHeat + Statics.Work3);
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTHEAT") + "") == 0)
			{
				Statics.intHeat = (Statics.intHeat + ((FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) * dblFactorTrend)));
			}
			// Cooling
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTCOOL") + "") != 0)
			{
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1600 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diCOOL"))));
				Statics.intCool = ((Statics.intSFLA * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTCOOL")) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 10000;
				Statics.intHeat = (Statics.intHeat + Statics.intCool);
			}
			// Open 5
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN5") + "") == 0)
				goto REAS03_4008_TAG;
			// Get #2, 1570, CR
			// Call OpenCRTable(CR, 1570)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1570);
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) < 99999999)
				goto REAS03_4007_TAG;
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN5")) > 9)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "Open 5 is greater than 9 but there is no valid data in the Unit price for CostRecord 1570." + "\r\n";
				goto REAS03_4008_TAG;
			}
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1570 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen5"))));
			Statics.intOpen5 = (((Statics.intSFLA * FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			goto REAS03_4008_TAG;
			REAS03_4007_TAG:
			;
			Statics.intOpen5 = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen5")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			REAS03_4008_TAG:
			;
			// Call GetCostRecordDwelling(1457)
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1457);
			Statics.intFixtures = (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diFULLBATHS") + "") * 3) + (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diHALFBATHS") + "") * 2) + (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diADDNFIXTURES") + "") * 1) + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsdwelling") + "");
			Statics.intFixtures -= 4;
			Statics.intPlumbing = (Statics.intFixtures * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) * dblFactorTrend / 100);
			// Attic
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1640 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diATTIC") + "")));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				modSpeedCalc.Statics.boolCalcErrors = true;
				modSpeedCalc.Statics.CalcLog += "All 9's in Attic cost record " + FCConvert.ToString(1640 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diattic") + "")) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
				return;
			}
			Statics.intAttic = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disqft") + "") * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			// Fireplaces
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("difireplaces") + "") != 0)
			{
				// Call GetCostRecordDwelling(1455)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1455);
				if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "All 9's in Fireplace cost record 1455 Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
					return;
				}
				Statics.intFirePlace = (((Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("difireplaces")) * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			}
			// Insulation
			// Call GetCostRecordDwelling(CInt(1650 + Val(dwl.fields("diinsulation") & "")))
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1650 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diinsulation") + "")));
			if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
			{
				Statics.intInsulation = 0;
			}
			else
			{
				Statics.intInsulation = (((Statics.intSFLA * FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CUnit) + FCConvert.ToInt32(modGlobalVariables.Statics.CostRec.CBase)) * dblFactorTrend) / 100);
			}
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Insulation"
			// End If
			// Percent Unfinished
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished") + "") != 0)
			{
				// Call GetCostRecordDwelling(1456)
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1456);
				if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
				{
					Statics.intUnfinished = 0;
				}
				else
				{
					Statics.intUnfinished = ((Statics.intSFLA * FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished")) * dblFactorTrend) / 10000);
				}
			}
			// 
			// round things so the printout matches the actual
			Statics.dblBase = modGlobalRoutines.Round(Statics.dblBase, 0);
			Statics.intTrim = modGlobalRoutines.Round(Statics.intTrim, 0);
			Statics.intRoof = modGlobalRoutines.Round(Statics.intRoof, 0);
			Statics.intOpen3 = modGlobalRoutines.Round(Statics.intOpen3, 0);
			Statics.intOpen4 = modGlobalRoutines.Round(Statics.intOpen4, 0);
			Statics.intBasement = modGlobalRoutines.Round(Statics.intBasement, 0);
			Statics.intFinBsmt = modGlobalRoutines.Round(Statics.intFinBsmt, 0);
			Statics.intHeat = modGlobalRoutines.Round(Statics.intHeat, 0);
			Statics.intOpen5 = modGlobalRoutines.Round(Statics.intOpen5, 0);
			Statics.intPlumbing = modGlobalRoutines.Round(Statics.intPlumbing, 0);
			Statics.intAttic = modGlobalRoutines.Round(Statics.intAttic, 0);
			Statics.intFirePlace = modGlobalRoutines.Round(Statics.intFirePlace, 0);
			Statics.intInsulation = modGlobalRoutines.Round(Statics.intInsulation, 0);
			Statics.intUnfinished = modGlobalRoutines.Round(Statics.intUnfinished, 0);
			Statics.lngRepCostNew[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Statics.dblBase + Statics.intTrim + Statics.intRoof + Statics.intOpen3 + Statics.intOpen4 + Statics.intBasement + Statics.intFinBsmt + Statics.intHeat);
			Statics.lngRepCostNew[modGlobalVariables.Statics.intCurrentCard] += FCConvert.ToInt32(Statics.intOpen5 + Statics.intPlumbing + Statics.intAttic + Statics.intFirePlace + Statics.intInsulation + Statics.intUnfinished);
			Statics.intHoldYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diyearbuilt") + "")));
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Percent Unfinished"
			// End If
			// Physical Percent Good
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctphys") + "") == 0)
			{
				// Get #2, (1460 + Val(dwl.fields("dicondition"))), CR
				// Call OpenCRTable(CR, 1460 + Val(dwl.fields("dicondition") & ""))
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(1460 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dicondition") + "")));
				if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 || FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CBase) == 99999999)
				{
					modSpeedCalc.Statics.boolCalcErrors = true;
					modSpeedCalc.Statics.CalcLog += "All 9's in Condition Code cost record " + FCConvert.ToString(1460 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dicondition") + "")) + " Acct: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") + " Card: " + modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") + "\r\n";
					return;
				}
				Statics.strDwellingCode = modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") + " ";
				// Call PhysicalPercentRoutine
				modREASValuations.new_physicalpercentroutine();
			}
			else
			{
				Statics.dblPhysicalPercent = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctphys")) / 100;
			}
			Statics.dblHoldPhysicalPercent = Statics.dblPhysicalPercent;
			Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard] = Statics.lngRepCostNew[modGlobalVariables.Statics.intCurrentCard];
			Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Statics.lngRepCostNew[modGlobalVariables.Statics.intCurrentCard] * Statics.dblPhysicalPercent);
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Physical percent"
			// End If
			// Functional Percent Good
			// Call REAS03_4200_FUNCTIONAL_PERCENT
			new_functional_percent();
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Calculated Functional Percent"
			// End If
			// corey
			Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard] * Statics.dblFunctionalPercent);
			// Economic Percent Good
			Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard] * Statics.dblEconomicPercent);
			// Replacement Cost New Less Depreciation
			// lngRepCostNewLD(intCurrentCard) = Int(0.5 + lngRepCostNewLD(intCurrentCard))
			modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] = Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard];
			Statics.lngSFLATotal = Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard];
			Statics.lngDwellingWRCNLD = Statics.lngRepCostNewLD[modGlobalVariables.Statics.intCurrentCard];
			// lngDwellingWRCNLD = ((lngDwellingWRCNLD / RoundingVar) \ 1) * RoundingVar
			Statics.HoldCondition = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dicondition") + "")));
			Statics.HoldGrade1 = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1") + "")));
			Statics.HoldGrade2 = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2") + "")));
			if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished") + "") != 0)
			{
				Statics.intSFLA -= (fecherFoundation.FCUtils.iDiv(Statics.intSFLA * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished")), 100));
			}
			modGlobalVariables.Statics.dbldwellphys = Statics.dblPhysicalPercent;
			modGlobalVariables.Statics.dbldwelleco = Statics.dblEconomicPercent;
			modGlobalVariables.Statics.dbldwellfunctional = Statics.dblFunctionalPercent;
			// If UCase(MuniName) = "WINSLOW" Then
			// ts.WriteLine "Done Dwelling Specific"
			// ts.Close
			// End If
			// Call modDwelling.ResidentialSizeFactor
		}

		public static void SpeedResidentialSizeFactor()
		{
			// vbPorter upgrade warning: intUnits As short --> As int	OnWriteFCConvert.ToDouble(
			int intUnits;
			int DIFF;
			// Dim RSZFactor As Double
			double RSZFCTR = 0;
			double R1;
			double R2;
			double R3;
			double R4;
			double R5;
			double F1;
			double F2;
			double F3;
			double F4;
			double F5;
			double F6 = 0;
			double V1;
			double V2;
			double V3;
			double V4;
			double V5;
			double V6 = 0;
			intUnits = FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsdwelling")) + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsother")));
			R1 = Conversion.Val(modGlobalVariables.Statics.RSZRange[1] + "");
			F1 = Conversion.Val(modGlobalVariables.Statics.RSZFact[1] + "");
			R2 = Conversion.Val(modGlobalVariables.Statics.RSZRange[2] + "");
			F2 = Conversion.Val(modGlobalVariables.Statics.RSZFact[2] + "");
			R3 = Conversion.Val(modGlobalVariables.Statics.RSZRange[3] + "");
			F3 = Conversion.Val(modGlobalVariables.Statics.RSZFact[3] + "");
			R4 = Conversion.Val(modGlobalVariables.Statics.RSZRange[4] + "");
			F4 = Conversion.Val(modGlobalVariables.Statics.RSZFact[4] + "");
			R5 = Conversion.Val(modGlobalVariables.Statics.RSZRange[5] + "");
			F5 = Conversion.Val(modGlobalVariables.Statics.RSZFact[5] + "");
			if (intUnits == 2)
			{
				R1 = Conversion.Val(modGlobalVariables.Statics.RSZRange[6] + "");
				F1 = Conversion.Val(modGlobalVariables.Statics.RSZFact[6] + "");
				R2 = Conversion.Val(modGlobalVariables.Statics.RSZRange[7] + "");
				F2 = Conversion.Val(modGlobalVariables.Statics.RSZFact[7] + "");
				R3 = Conversion.Val(modGlobalVariables.Statics.RSZRange[8] + "");
				F3 = Conversion.Val(modGlobalVariables.Statics.RSZFact[8] + "");
				R4 = Conversion.Val(modGlobalVariables.Statics.RSZRange[9] + "");
				F4 = Conversion.Val(modGlobalVariables.Statics.RSZFact[9] + "");
				R5 = Conversion.Val(modGlobalVariables.Statics.RSZRange[10] + "");
				F5 = Conversion.Val(modGlobalVariables.Statics.RSZFact[10] + "");
			}
			else if (intUnits == 3)
			{
				R1 = Conversion.Val(modGlobalVariables.Statics.RSZRange[11] + "");
				F1 = Conversion.Val(modGlobalVariables.Statics.RSZFact[11] + "");
				R2 = Conversion.Val(modGlobalVariables.Statics.RSZRange[12] + "");
				F2 = Conversion.Val(modGlobalVariables.Statics.RSZFact[12] + "");
				R3 = Conversion.Val(modGlobalVariables.Statics.RSZRange[13] + "");
				F3 = Conversion.Val(modGlobalVariables.Statics.RSZFact[13] + "");
				R4 = Conversion.Val(modGlobalVariables.Statics.RSZRange[14] + "");
				F4 = Conversion.Val(modGlobalVariables.Statics.RSZFact[14] + "");
				R5 = Conversion.Val(modGlobalVariables.Statics.RSZRange[15] + "");
				F5 = Conversion.Val(modGlobalVariables.Statics.RSZFact[15] + "");
			}
			else if (intUnits == 4)
			{
				R1 = Conversion.Val(modGlobalVariables.Statics.RSZRange[16] + "");
				F1 = Conversion.Val(modGlobalVariables.Statics.RSZFact[16] + "");
				R2 = Conversion.Val(modGlobalVariables.Statics.RSZRange[17] + "");
				F2 = Conversion.Val(modGlobalVariables.Statics.RSZFact[17] + "");
				R3 = Conversion.Val(modGlobalVariables.Statics.RSZRange[18] + "");
				F3 = Conversion.Val(modGlobalVariables.Statics.RSZFact[18] + "");
				R4 = Conversion.Val(modGlobalVariables.Statics.RSZRange[19] + "");
				F4 = Conversion.Val(modGlobalVariables.Statics.RSZFact[19] + "");
				R5 = Conversion.Val(modGlobalVariables.Statics.RSZRange[20] + "");
				F5 = Conversion.Val(modGlobalVariables.Statics.RSZFact[20] + "");
			}
			else if (intUnits > 4)
			{
				R1 = Conversion.Val(modGlobalVariables.Statics.RSZRange[21] + "");
				F1 = Conversion.Val(modGlobalVariables.Statics.RSZFact[21] + "");
				R2 = Conversion.Val(modGlobalVariables.Statics.RSZRange[22] + "");
				F2 = Conversion.Val(modGlobalVariables.Statics.RSZFact[22] + "");
				R3 = Conversion.Val(modGlobalVariables.Statics.RSZRange[23] + "");
				F3 = Conversion.Val(modGlobalVariables.Statics.RSZFact[23] + "");
				R4 = Conversion.Val(modGlobalVariables.Statics.RSZRange[24] + "");
				F4 = Conversion.Val(modGlobalVariables.Statics.RSZFact[24] + "");
				R5 = Conversion.Val(modGlobalVariables.Statics.RSZRange[25] + "");
				F5 = Conversion.Val(modGlobalVariables.Statics.RSZFact[25] + "");
			}
			V1 = 0;
			V2 = 0;
			V3 = 0;
			V4 = 0;
			V5 = 0;
			if (R1 == 0)
				goto REAS25_END_SFLA1;
			if (Statics.intSFLA > R1)
			{
				V1 = R1;
			}
			else
			{
				V1 = Statics.intSFLA;
			}
			if (R2 == 0)
				goto REAS25_END_SFLA1;
			if (Statics.intSFLA > R2)
			{
				V2 = R2 - R1;
			}
			else
			{
				V2 = Statics.intSFLA - R1;
			}
			if (R3 == 0)
				goto REAS25_END_SFLA1;
			if (Statics.intSFLA > R3)
			{
				V3 = R3 - R2;
			}
			else
			{
				V3 = Statics.intSFLA - R2;
			}
			if (R4 == 0)
				goto REAS25_END_SFLA1;
			if (Statics.intSFLA > R4)
			{
				V4 = R4 - R3;
			}
			else
			{
				V4 = Statics.intSFLA - R3;
			}
			if (R5 == 0)
				goto REAS25_END_SFLA1;
			if (Statics.intSFLA > R5)
			{
				V5 = R5 - R4;
			}
			else
			{
				V5 = Statics.intSFLA - R4;
			}
			REAS25_END_SFLA1:
			;
			if (R5 > 0)
			{
				V6 = Statics.intSFLA - R5;
				F6 = F5;
			}
			else if (R4 > 0)
			{
				V6 = Statics.intSFLA - R4;
				F6 = F4;
			}
			else if (R3 > 0)
			{
				V6 = Statics.intSFLA - R3;
				F6 = F3;
			}
			else if (R2 > 0)
			{
				V6 = Statics.intSFLA - R2;
				F6 = F2;
			}
			else if (R1 > 0)
			{
				V6 = Statics.intSFLA - R1;
				F6 = F1;
			}
			if (V1 < 0)
				V1 = 0;
			if (V2 < 0)
				V2 = 0;
			if (V3 < 0)
				V3 = 0;
			if (V4 < 0)
				V4 = 0;
			if (V5 < 0)
				V5 = 0;
			if (V6 < 0)
				V6 = 0;
			modREASValuations.Statics.RSZFactor = V1 * F1;
			modREASValuations.Statics.RSZFactor += (V2 * F2);
			modREASValuations.Statics.RSZFactor += (V3 * F3);
			modREASValuations.Statics.RSZFactor += (V4 * F4);
			modREASValuations.Statics.RSZFactor += (V5 * F5);
			modREASValuations.Statics.RSZFactor += (V6 * F6);
			if (Statics.intSFLA != 0)
			{
				RSZFCTR = modREASValuations.Statics.RSZFactor / Statics.intSFLA;
			}
			RSZFCTR = fecherFoundation.FCUtils.iDiv((RSZFCTR * 100), 1);
			RSZFCTR /= 100;
			// RSZFactor = Int(0.5 + RSZFCTR#) / 100
			modREASValuations.Statics.RSZFactor = RSZFCTR / 100;
			if (modREASValuations.Statics.RSZFactor != 0)
			{
				Statics.dblFunctionalPercent *= modREASValuations.Statics.RSZFactor;
				// dblFunctionalPercent = Int(0.5 + dblFunctionalPercent)
				// dblFunctionalPercent = dblFunctionalPercent / 100
				Statics.dblFunctionalPercent = FCConvert.ToDouble(Strings.Format(Statics.dblFunctionalPercent, "0.00"));
			}
			Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard] * FCConvert.ToDouble(Statics.dblEconomicPercent * Statics.dblFunctionalPercent * Statics.dblPhysicalPercent));
			// lngdwellcostnew(intCurrentCard) = ((lngdwellcostnew(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
			// lngdwellcostnew(intCurrentCard) = lngdwellcostnew(intCurrentCard) * RSZFactor '* dblFunctionalPercent
			// lngRepCostNewLD(intCurrentCard) = lngdwellcostnew(intCurrentCard)
			// DIFF = lngDwellingWRCNLD - lngdwellcostnew(intCurrentCard)
			// lngSFLATotal = lngSFLATotal - DIFF
			// BuildingTotal(intCurrentCard) = BuildingTotal(intCurrentCard) - DIFF
			modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] = Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard];
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				if (modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") == modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") && modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") == modDataTypes.Statics.MR.Get_Fields_Int32("rscard"))
				{
					modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].sstyle = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("distyle") + ""));
				}
				else
				{
					modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].sstyle = FCConvert.ToString(0);
				}
				modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].sdwellrcnld = FCConvert.ToString(modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard]);
				modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].dwellcode = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ribldgcode") + "")));
				// clsTemp.Fields("sdwellrcnld") = BuildingTotal(intCurrentCard)
				// clsTemp.Fields("sstyle") = Val(DWL.Fields("distyle") & "")
				// clsTemp.Fields("dwellcode") = Val(MR.Fields("ribldgcode") & "")
				// clsTemp.Update
			}
			Statics.lngSFLATotal = Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard];
			if (modREASValuations.Statics.RSZFactor == 0)
				modREASValuations.Statics.RSZFactor = 1;
			if (modREASValuations.Statics.RSZFactor != 0)
			{
				int x;
				double RCNLDOB;
				modOutbuilding.Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard] = 0;
				for (x = 1; x <= 10; x++)
				{
					if (modOutbuilding.Statics.contributestosfla[x] && modOutbuilding.Statics.dblFunctional[x] != 0)
					{
						// RCNLDOB# = lngRepCostNewLD(X) / (dblFunctional(X) / 100)
						modOutbuilding.Statics.dblFunctional[x] *= modREASValuations.Statics.RSZFactor;
						// dblFunctional(X) = Int(0.5 + dblFunctional(X))
						modOutbuilding.Statics.dblFunctional[x] = FCConvert.ToDouble(Strings.Format(modOutbuilding.Statics.dblFunctional[x], "0"));
						modOutbuilding.Statics.lngRepCostNewLD[x] = FCConvert.ToInt32(modGlobalRoutines.Round(FCConvert.ToDouble(modOutbuilding.Statics.lngRepCostNewLD[x]) * modOutbuilding.Statics.dblFunctional[x] / 100, 0));
						// modOutbuilding.lngRepCostNewLD(x) = Round(modOutbuilding.lngRepCostNewLD(x) * (dblFunctional(x) / 100), 0)
						Statics.lngSFLATotal += modOutbuilding.Statics.lngRepCostNewLD[x];
						// BuildingTotal(intCurrentCard) = BuildingTotal(intCurrentCard) - DIFF
					}
					if (modOutbuilding.Statics.contributestosfla[x] && modOutbuilding.Statics.dblFunctional[x] == 0)
					{
						modOutbuilding.Statics.lngRepCostNewLD[x] = 0;
					}
					modOutbuilding.Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard] += modOutbuilding.Statics.lngRepCostNewLD[x];
					// lngSFLATotal = lngSFLATotal + modOutbuilding.lngRepCostNewLD(x)
					modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] += modOutbuilding.Statics.lngRepCostNewLD[x];
				}
				// x
			}
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				if (modDataTypes.Statics.DWL.Get_Fields_Int32("rsaccount") == modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") && modDataTypes.Statics.DWL.Get_Fields_Int32("rscard") == modDataTypes.Statics.MR.Get_Fields_Int32("rscard"))
				{
					modSpeedCalc.Statics.SummaryList[modSpeedCalc.Statics.SummaryListIndex].ssfla = FCConvert.ToString(Statics.lngSFLATotal);
				}
			}
			// BuildingTotal(intCurrentCard) = ((BuildingTotal(intCurrentCard) / RoundingVar) \ 1) * RoundingVar
		}

		public static void Speed_DWELLING_DATA()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// If UCase(MuniName) = "WINSLOW" Then
				// Dim fso As New FCFileSystemObject
				// Dim ts As TextStream
				// If File.Exists("RECalcTest.txt") Then
				// Call fso.DeleteFile("RECalcTest.txt")
				// End If
				// Set ts = fso.CreateTextFile("RECalcTest.txt")
				// ts.WriteLine ("Starting Dwelling Display for Account " & DWL.Fields("rsaccount"))
				// End If
				// With frmNewValuationReport.DwellingGrid(intCurrentCard - 1)
				// .TextMatrix(0, 1) = Format(dblBase, "#,###")
				// .TextMatrix(1, 1) = Format(intTrim, "#,##0")
				// .TextMatrix(2, 1) = Format(intRoof, "#,##0")
				// .TextMatrix(3, 1) = Format(intOpen3, "#,##0")
				// .TextMatrix(4, 1) = Format(intOpen4, "#,##0")
				// .TextMatrix(5, 1) = Format(intBasement, "#,##0")
				// .TextMatrix(6, 1) = Format(intFinBsmt, "#,##0")
				// .TextMatrix(7, 1) = Format(intHeat, "#,##0")
				// .TextMatrix(10, 1) = Format(intPlumbing, "#,##0")
				// .TextMatrix(11, 1) = Format(intAttic, "#,##0")
				// .TextMatrix(12, 1) = Format(intFirePlace, "#,##0")
				// .TextMatrix(13, 1) = Format(intInsulation, "#,##0")
				// .TextMatrix(14, 1) = Format(intUnfinished, "#,##0")
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Base = Strings.Format(Statics.dblBase, "#,###");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Trim = Strings.Format(Statics.intTrim, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].RoofVal = Strings.Format(Statics.intRoof, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open3Val = Strings.Format(Statics.intOpen3, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open4Val = Strings.Format(Statics.intOpen4, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].BasementVal = Strings.Format(Statics.intBasement, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].FinBasementVal = Strings.Format(Statics.intFinBsmt, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].HeatVal = Strings.Format(Statics.intHeat, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].PlumbingVal = Strings.Format(Statics.intPlumbing, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].AtticVal = Strings.Format(Statics.intAttic, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].FireplaceVal = Strings.Format(Statics.intFirePlace, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].InsulationVal = Strings.Format(Statics.intInsulation, "#,##0");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].UnfinishedVal = Strings.Format(Statics.intUnfinished, "#,##0");
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("distyle") + "")));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "STYLE");
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].BuildingType = modGlobalVariables.Statics.CostRec.ClDesc;
				Statics.WORK1 = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2") + "") / 100;
				modProperty.Statics.WKey = FCConvert.ToInt32(1480 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSTORIES") + ""));
				if (modProperty.Statics.WKey != 1480)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Stories = modGlobalVariables.Statics.CostRec.ClDesc;
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].StoriesShort = modGlobalVariables.Statics.CostRec.CSDesc;
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diextwalls") + "")));
				if (modProperty.Statics.WKey != 0)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "EXTERIOR");
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Exterior = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].DwellUnits = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsdwelling") + "")) + "  " + "OTHER Units-" + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsother") + ""));
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3") + "") > 0)
				{
					modProperty.Statics.WKey = 1520;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WORKA3 = modGlobalVariables.Statics.CostRec.CSDesc;
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 && Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen3")) < 10)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1520 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen3")));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
					}
					else
					{
						modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen3"));
					}
					// .TextMatrix(3, 1) = Trim(WORKA2) & " " & Mid(WORKA3, 1, 6)
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open3Title = Strings.Trim(modProperty.Statics.WORKA1);
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open3 = Strings.Trim(modREASValuations.Statics.WORKA2) + " " + Strings.Mid(FCConvert.ToString(modProperty.Statics.WORKA3), 1, 6);
				}
				else
				{
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open3Title = "";
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open3 = "";
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Open 3")
				// End If
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen4") + "") > 0)
				{
					modProperty.Statics.WKey = 1530;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WORKA3 = modGlobalVariables.Statics.CostRec.CSDesc;
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1530 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen4")));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
					}
					else
					{
						modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen4"));
					}
					// .TextMatrix(4, 1) = Trim(WORKA2) & " " & Mid(WORKA3, 1, 6)
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open4Title = Strings.Trim(modProperty.Statics.WORKA1);
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open4 = Strings.Trim(modREASValuations.Statics.WORKA2) + " " + Strings.Mid(FCConvert.ToString(modProperty.Statics.WORKA3), 1, 6);
				}
				else
				{
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open4Title = "";
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open4 = "";
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Open 4")
				// End If
				modProperty.Statics.WKey = FCConvert.ToInt32(1540 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("difoundation") + ""));
				if (modProperty.Statics.WKey != 1540)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(5, 1) = Trim(CostRec.ClDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Foundation = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disfbsmtliving") + "") == 0)
				{
					// .TextMatrix(6, 1) = "None"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].FinBasementArea = "None";
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1")) == 9)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1")));
						Statics.WORK1 = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2")) * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2")) / 10000;
					}
					else
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1")));
						Statics.WORK1 = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2")) / 100;
					}
					if (modProperty.Statics.WKey != 1570)
					{
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						// .TextMatrix(6, 1) = Val(dwl.fields("disfbsmtliving")) & " Sqft, Grade " & Mid(CostRec.CSDesc, 1, 2) & Format(WORK1, "0.00")
						modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].FinBasementArea = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disfbsmtliving"))) + " Sqft, Grade " + Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + Strings.Format(Statics.WORK1, "0.00");
					}
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diheat") + "")));
				// If WKey <> 1590 Then
				if (modProperty.Statics.WKey != 0)
				{
					// Call Get_Cost_Record(WKey)
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "HEAT");
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				// .TextMatrix(7, 1) = Trim(Val(dwl.fields("dipctheat") & "")) & "% "
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Heating = Strings.Trim(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctheat") + ""))) + "% ";
				// If WKey <> 1590 Then .TextMatrix(7, 1) = .TextMatrix(7, 1) & " " & WORKA1
				// If WKey <> 1590 Then CalcDwellList(CalcDwellIndex).Heating = CalcDwellList(CalcDwellIndex).Heating & " " & WORKA1
				if (modProperty.Statics.WKey != 0)
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Heating += " " + modProperty.Statics.WORKA1;
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Heat")
				// End If
				// .TextMatrix(8, 1) = Val(Trim(dwl.fields("dirooms") & ""))
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Rooms = FCConvert.ToString(Conversion.Val(Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("dirooms") + "")));
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Bedrooms = FCConvert.ToString(Conversion.Val(Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diBEDROOMS") + "")));
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Bath = FCConvert.ToString(Conversion.Val(Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diFULLBATHS") + "")));
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Rooms")
				// End If
				modProperty.Statics.WKey = FCConvert.ToInt32(1640 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diattic") + ""));
				if (modProperty.Statics.WKey != 1640)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(11, 1) = CostRec.ClDesc
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Attic = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				// .TextMatrix(12, 1) = dwl.fields("difireplaces") & ""
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Fireplaces = modDataTypes.Statics.DWL.Get_Fields_Int32("difireplaces") + "";
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Attic and Fireplaces")
				// End If
				modProperty.Statics.WKey = FCConvert.ToInt32(1650 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diinsulation") + ""));
				if (modProperty.Statics.WKey != 1650)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(13, 1) = CostRec.ClDesc
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Insulation = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished") + "") == 0)
				{
					// .TextMatrix(14, 1) = "NONE"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].UnfinLivingArea = "NONE";
				}
				else
				{
					// .TextMatrix(14, 1) = dwl.fields("dipctunfinished") & "%"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].UnfinLivingArea = modDataTypes.Statics.DWL.Get_Fields_Int32("dipctunfinished") + "%";
				}
				// If RUNTYPE = "D" Then
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed insulation and unfinished")
				// End If
				// 
				// CalcDwellList(CalcDwellIndex).Open3Title = .TextMatrix(3, 0)
				// CalcDwellList(CalcDwellIndex).Open4Title = .TextMatrix(4, 0)
				// 
				// End If
				// End With
				// With frmNewValuationReport.DwellMiscGrid(intCurrentCard - 1)
				// .TextMatrix(0, 0) = Format(Val(dwl.fields("disqft") & ""), "#,##0") & " Sqft"
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Sqft = Strings.Format(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disqft") + ""), "#,##0") + " Sqft";
				modProperty.Statics.WKey = FCConvert.ToInt32(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1") + ""));
				if (modProperty.Statics.WKey != 1670)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(0, 1) = "Grade " & Mid(CostRec.CSDesc, 1, 2) & (Val(DWL.Fields("digrade2") & ""))
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Grade = "Grade " + Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + (FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2") + "")));
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disfmasonry") + "") == 0)
				{
					// .TextMatrix(1, 1) = "None"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].MasonryTrim = "None";
				}
				else
				{
					// .TextMatrix(1, 1) = Format(Val(dwl.fields("disfmasonry")), "#,###") & "Sqft"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].MasonryTrim = Strings.Format(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disfmasonry")), "#,###") + "Sqft";
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed SQFT and SFMasonry")
				// End If
				modProperty.Statics.WKey = FCConvert.ToInt32(1510 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diroof") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				// If WKey <> 1510 Then .TextMatrix(2, 1) = Trim(CostRec.ClDesc & "")
				if (modProperty.Statics.WKey != 1510)
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Roof = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				modREASValuations.Statics.WORKA2 = "";
				modProperty.Statics.WKey = FCConvert.ToInt32(1560 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diWETBSMT") + ""));
				if (modProperty.Statics.WKey != 1560)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modREASValuations.Statics.WORKA2 = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc);
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Roof and Wet basement")
				// End If
				modProperty.Statics.WKey = FCConvert.ToInt32(1550 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMT") + ""));
				if (modProperty.Statics.WKey != 1550)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(5, 1) = Trim(WORKA2 & " " & CostRec.CSDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Basement = Strings.Trim(modREASValuations.Statics.WORKA2 + " " + modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMTGAR") + "") == 0)
				{
					// .TextMatrix(6, 1) = "None"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].BsmtGarage = "None";
				}
				else
				{
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].BsmtGarage = modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMTGAR") + " CAR";
				}
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Cooling = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTCOOL") + "")) + "% ";
				modProperty.Statics.WKey = FCConvert.ToInt32(1600 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dicool") + ""));
				if (modProperty.Statics.WKey != 1600)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(7, 1) = .TextMatrix(7, 1) & CostRec.ClDesc
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Cooling += modGlobalVariables.Statics.CostRec.ClDesc;
				}
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].HalfBaths = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diHALFBATHS") + ""));
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].AddnFixtures = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diADDNFIXTURES") + ""));
				// 
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen5") + "") > 0)
				{
					modProperty.Statics.WKey = 1570;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
					modProperty.Statics.WORKA3 = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1570 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen5") + ""));
						if (modProperty.Statics.WKey != 1570)
						{
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
						}
					}
					else
					{
						modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diopen5"));
					}
					// .TextMatrix(8, 0) = Trim(WORKA1)
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open5Title = Strings.Trim(modProperty.Statics.WORKA1);
					// If WKey <> 1570 Then .TextMatrix(8, 1) = Trim(WORKA2)
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open5 = Strings.Trim(modREASValuations.Statics.WORKA2);
					// frmNewValuationReport.DwellingGrid(intCurrentCard - 1).TextMatrix(8, 0) = Trim(WORKA3)
					// CalcDwellList(CalcDwellIndex).Open5Title = Trim(WORKA3)
					// frmNewValuationReport.DwellingGrid(intCurrentCard - 1).TextMatrix(8, 1) = Format(intOpen5, "#,##0")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open5Val = Strings.Format(Statics.intOpen5, "#,##0");
				}
				else
				{
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open5Title = "";
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open5 = "";
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Open5Val = "";
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Open 5")
				// End If
				// End With
				// With frmNewValuationReport.DwellCondGrid(intCurrentCard - 1)
				modProperty.Statics.WKey = FCConvert.ToInt32(1610 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diKITCHENS") + ""));
				if (modProperty.Statics.WKey != 1610)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(1, 2) = Trim(CostRec.CSDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Kitchens = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diyearbuilt") + "") == 1)
				{
					// .TextMatrix(1, 0) = "Old"
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].YearBuilt = "Old";
				}
				else if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diyearbuilt") + "") == 2)
				{
					// .TextMatrix(1, 0) = "Unk."
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].YearBuilt = "Unk.";
				}
				else
				{
					// .TextMatrix(1, 0) = dwl.fields("diyearbuilt") & ""
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].YearBuilt = modDataTypes.Statics.DWL.Get_Fields_Int32("diyearbuilt") + "";
				}
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Renovated = modDataTypes.Statics.DWL.Get_Fields_Int32("diYEARREMODEL") + "";
				modProperty.Statics.WKey = FCConvert.ToInt32(1620 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBATHS") + ""));
				if (modProperty.Statics.WKey != 1620)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(1, 3) = Trim(CostRec.CSDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Baths = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed Kitchens,year built and remodeled")
				// End If
				// .TextMatrix(1, 6) = Format(lngRepCostNew(intCurrentCard), "#,##0")
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Total = Strings.Format(Statics.lngRepCostNew[modGlobalVariables.Statics.intCurrentCard], "#,##0");
				modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dicondition") + ""));
				if (modProperty.Statics.WKey != 1460)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(1, 4) = Trim(CostRec.ClDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Condition = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(1630 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diLAYOUT") + ""));
				if (modProperty.Statics.WKey != 1630)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(1, 5) = Trim(CostRec.CSDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].Layout = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Displayed condition and layout")
				// End If
				// End With
				// With frmNewValuationReport.DwellPercGrid(intCurrentCard - 1)
				modProperty.Statics.WKey = FCConvert.ToInt32(1680 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diFUNCTCODE") + ""));
				if (modProperty.Statics.WKey != 1680)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(1, 0) = Trim(CostRec.ClDesc & "")
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].FuncObs = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dieconcode") + "")));
				if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "ECONOMIC"))
				{
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].EconObs = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				else
				{
					modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].EconObs = "";
				}
				// .TextMatrix(1, 2) = Format(dblPhysicalPercent * 100, "###") & "%"
				// .TextMatrix(1, 3) = Format(dblFunctionalPercent * 100, "###") & "%"
				// .TextMatrix(1, 4) = Format(dblEconomicPercent * 100, "###") & "%"
				// .TextMatrix(1, 5) = Format(lngdwellcostnew(intCurrentCard), "#,##0")
				// If RUNTYPE = "D" Then
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].PhysPerc = Strings.Format(Statics.dblPhysicalPercent * 100, "###") + "%";
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].FuncPerc = Strings.Format(Statics.dblFunctionalPercent * 100, "###") + "%";
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].EconPerc = Strings.Format(Statics.dblEconomicPercent * 100, "###") + "%";
				modSpeedCalc.Statics.CalcDwellList[modSpeedCalc.Statics.CalcDwellIndex].DwellValue = Strings.Format(Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard], "#,##0");
				// End If
				// End With
				// 
				// If UCase(MuniName) = "WINSLOW" Then
				// ts.WriteLine ("Done")
				// ts.Close
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Dwelling Data", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				NoDwelling:
				;
			}
		}

		public class StaticVariables
		{
			// vbPorter upgrade warning: WHigh As double	OnWrite(string, int)
			//=========================================================
			public double WHigh;
			// vbPorter upgrade warning: WLow As double	OnWrite(string, double)	OnReadFCConvert.ToSingle(
			public double WLow;
			public double dblLife;
			public double dblExponent;
			public double dblPhysicalPercent;
			public double dblHoldPhysicalPercent;
			// vbPorter upgrade warning: dblFunctionalPercent As double	OnWrite(double, string)
			public double dblFunctionalPercent;
			public double dblEconomicPercent;
			// vbPorter upgrade warning: intHoldYear As short --> As int	OnRead(int, string)
			public int intHoldYear;
			// vbPorter upgrade warning: intSFLA As int	OnWrite(double, int)
			// corey
			public int intSFLA;
			public int HoldCondition;
			// vbPorter upgrade warning: HoldGrade1 As short --> As int	OnWrite(int, double)
			public int HoldGrade1;
			// vbPorter upgrade warning: HoldGrade2 As short --> As int	OnWrite(int, double)
			public int HoldGrade2;
			public string strDwellingCode = "";
			// vbPorter upgrade warning: lngSFLATotal As int	OnRead(string, int)
			public int lngSFLATotal;
			public int LINECOUNT;
			// vbPorter upgrade warning: WORK1 As object	OnWrite(string, double)
			public object WORK1;
			public object Work2;
			// vbPorter upgrade warning: Work3 As Variant --> As double
			public double Work3;
			public double WBas;
			// vbPorter upgrade warning: lngRepCostNew As int	OnWriteFCConvert.ToDouble(
			// Double
			public int[] lngRepCostNew = new int[999 + 1];
			// vbPorter upgrade warning: lngRepCostNewLD As int	OnWriteFCConvert.ToDouble(
			public int[] lngRepCostNewLD = new int[999 + 1];
			// vbPorter upgrade warning: lngdwellcostnew As int	OnWrite(int, double)
			public int[] lngdwellcostnew = new int[999 + 1];
			public int lngDwellingWRCNLD;
			// Dwelling Data Keyed in from Screen
			public double dblBase;
			public double intFixtures;
			public double intRoof;
			public double intTrim;
			public double intOpen3;
			public double intOpen4;
			public double intOpen5;
			public double intFinBsmt;
			public double intCool;
			public double intFirePlace;
			public double intUnfinished;
			public double intHeat;
			public double intBasement;
			public double intPlumbing;
			public double intAttic;
			public double intInsulation;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
