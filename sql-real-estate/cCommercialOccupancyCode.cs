﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialOccupancyCode
	{
		//=========================================================
		private int lngRecordID;
		private string strName = string.Empty;
		// Private strShortDescription As String
		private int lngSectionID;
		private string strTypeCode = string.Empty;
		// Private intLife As Integer
		private int lngOccupancyNumber;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int OccupancyCodeNumber
		{
			set
			{
				lngOccupancyNumber = value;
			}
			get
			{
				int OccupancyCodeNumber = 0;
				OccupancyCodeNumber = lngOccupancyNumber;
				return OccupancyCodeNumber;
			}
		}

		public string Code
		{
			set
			{
				strTypeCode = value;
				IsUpdated = true;
			}
			get
			{
				string Code = "";
				Code = strTypeCode;
				return Code;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
				IsUpdated = true;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}
		//
		// Public Property Let ShortDescription(ByVal strDescription As String)
		// strShortDescription = strDescription
		// End Property
		//
		// Public Property Get ShortDescription() As String
		// strShortDescription = strShortDescription
		// End Property
		public int SectionID
		{
			set
			{
				lngSectionID = value;
				IsUpdated = true;
			}
			get
			{
				int SectionID = 0;
				SectionID = lngSectionID;
				return SectionID;
			}
		}
	}
}
