﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptUpdatedAccounts.
	/// </summary>
	public partial class rptUpdatedAccounts : BaseSectionReport
	{
		public rptUpdatedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Accounts Updated";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptUpdatedAccounts InstancePtr
		{
			get
			{
				return (rptUpdatedAccounts)Sys.GetInstance(typeof(rptUpdatedAccounts));
			}
		}

		protected rptUpdatedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUpdatedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTCMBUPDATEAMOUNTS = 0;
		const int CNSTCMBUPDATEINFOONLY = 1;
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;
		double lngTotLand;
		double lngTotBldg;
		double lngTotExempt;
		double lngTotAssess;
		double lngTotOldLand;
		double lngTotOldBldg;
		double lngTotOldExempt;
		double lngTotOldAssess;
		bool boolUseExceeds;
		bool boolChangesOnly;
		int lngTotalAccounts;
		bool boolInfoOnly;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool boolHidden = false;
			if (boolChangesOnly)
			{
				if (lngCRow < frmImportCompetitorXF.InstancePtr.GridGood.Rows)
				{
					if (frmImportCompetitorXF.InstancePtr.GridGood.RowHidden(lngCRow))
					{
						boolHidden = true;
					}
					else
					{
						boolHidden = false;
					}
					while (boolHidden && lngCRow < frmImportCompetitorXF.InstancePtr.GridGood.Rows)
					{
						if (frmImportCompetitorXF.InstancePtr.GridGood.RowHidden(lngCRow))
						{
							boolHidden = true;
						}
						else
						{
							boolHidden = false;
						}
						if (boolHidden)
						{
							lngCRow += 1;
						}
					}
				}
			}
			eArgs.EOF = lngCRow >= frmImportCompetitorXF.InstancePtr.GridGood.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblChange = 0;
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			if (frmImportCompetitorXF.InstancePtr.chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblExceeds.Visible = true;
				txtBldgExceed.Visible = true;
				txtLandExceed.Visible = true;
				boolUseExceeds = true;
				dblChange = Conversion.Val(frmImportCompetitorXF.InstancePtr.txtPercentChg.Text);
				lblExceeds.Text = "Exceeds " + FCConvert.ToString(dblChange) + "%";
			}
			else
			{
				lblExceeds.Visible = false;
				txtBldgExceed.Visible = false;
				txtLandExceed.Visible = false;
				boolUseExceeds = false;
			}
			if (frmImportCompetitorXF.InstancePtr.cmbUpdateType.ItemData(frmImportCompetitorXF.InstancePtr.cmbUpdateType.SelectedIndex) == CNSTCMBUPDATEINFOONLY)
			{
				boolInfoOnly = true;
				Label12.Visible = false;
				Label13.Visible = false;
				lblExceeds.Visible = false;
				Label6.Visible = false;
				Label8.Visible = false;
				Label9.Visible = false;
				Label10.Visible = false;
				Label11.Visible = false;
				Label14.Visible = false;
				Label15.Visible = false;
				Label16.Visible = false;
				Label17.Visible = false;
				txtOldLand.Visible = false;
				txtLand.Visible = false;
				txtOldBuilding.Visible = false;
				txtBuilding.Visible = false;
				txtOldExempt.Visible = false;
				txtExempt.Visible = false;
				txtOldAssessment.Visible = false;
				txtAssessment.Visible = false;
				txtTotalOldLand.Visible = false;
				txtTotalLand.Visible = false;
				txtTotalOldBldg.Visible = false;
				txtTotalBuilding.Visible = false;
				txtTotalOldExempt.Visible = false;
				txtTotalExempt.Visible = false;
				txtTotalOldAssessment.Visible = false;
				txtTotalAssessment.Visible = false;
			}
			else
			{
				boolInfoOnly = false;
			}
			lngTotalAccounts = 0;
			if (frmImportCompetitorXF.InstancePtr.chkShowChangesOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolChangesOnly = true;
			}
			else
			{
				boolChangesOnly = false;
			}
			lngTotLand = 0;
			lngTotBldg = 0;
			lngTotExempt = 0;
			lngTotAssess = 0;
			lngTotOldLand = 0;
			lngTotOldBldg = 0;
			lngTotOldExempt = 0;
			lngTotOldAssess = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngLand = 0;
			int lngBldg = 0;
			// vbPorter upgrade warning: lngExempt As int	OnWriteFCConvert.ToDouble(
			int lngExempt = 0;
			int lngAssess = 0;
			int lngOldLand = 0;
			int lngOldBldg = 0;
			int lngOldExempt = 0;
			int lngOldAssess = 0;
			if (lngCRow <= frmImportCompetitorXF.InstancePtr.GridGood.Rows - 1)
			{
				lngTotalAccounts += 1;
				txtMapLot.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTMAPLOT));
				txtName.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTOWNER));
				lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDVALUE))));
				lngBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTBLDGVALUE))));
				lngOldLand = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALLAND))));
				lngOldBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALBLDG))));
				if (boolUseExceeds)
				{
					if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDEXCEED)) != 0)
					{
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDEXCEED)) > 0)
						{
							txtLandExceed.Text = "UP";
						}
						else
						{
							txtLandExceed.Text = "DOWN";
						}
					}
					else
					{
						txtLandExceed.Text = "";
					}
					if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTBLDGEXCEED)) != 0)
					{
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTBLDGEXCEED)) > 0)
						{
							txtBldgExceed.Text = "UP";
						}
						else
						{
							txtBldgExceed.Text = "DOWN";
						}
					}
					else
					{
						txtBldgExceed.Text = "";
					}
				}
				lngExempt = FCConvert.ToInt32(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3)));
				lngOldExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTORIGINALEXEMPTION))));
				// If UCase(MuniName) = "ARUNDEL" Then
				// lngExempt = lngOldExempt
				// End If
				lngAssess = lngLand + lngBldg - lngExempt;
				lngOldAssess = lngOldLand + lngOldBldg - lngOldExempt;
				lngTotLand += lngLand;
				lngTotOldLand += lngOldLand;
				lngTotBldg += lngBldg;
				lngTotOldBldg += lngOldBldg;
				lngTotExempt += lngExempt;
				lngTotOldExempt += lngOldExempt;
				lngTotAssess += lngAssess;
				lngTotOldAssess += lngOldAssess;
				if (!boolInfoOnly)
				{
					txtExempt.Text = Strings.Format(lngExempt, "#,###,###,##0");
					txtLand.Text = Strings.Format(lngLand, "#,###,###,##0");
					txtBuilding.Text = Strings.Format(lngBldg, "#,###,###,##0");
					txtAssessment.Text = Strings.Format(lngAssess, "#,###,###,##0");
					txtOldLand.Text = Strings.Format(lngOldLand, "#,###,###,##0");
					txtOldBuilding.Text = Strings.Format(lngOldBldg, "#,###,###,##0");
					txtOldExempt.Text = Strings.Format(lngOldExempt, "#,###,###,##0");
					txtOldAssessment.Text = Strings.Format(lngOldAssess, "#,###,###,##0");
				}
			}
			else
			{
				txtLandExceed.Text = "";
				txtBldgExceed.Text = "";
				txtMapLot.Text = "";
				txtName.Text = "";
				txtExempt.Text = "";
				txtLand.Text = "";
				txtBuilding.Text = "";
				txtAssessment.Text = "";
				txtOldLand.Text = "";
				txtOldBuilding.Text = "";
				txtOldExempt.Text = "";
				txtOldAssessment.Text = "";
			}
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (!boolInfoOnly)
			{
				txtTotalAssessment.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
				txtTotalBuilding.Text = Strings.Format(lngTotBldg, "#,###,###,##0");
				txtTotalExempt.Text = Strings.Format(lngTotExempt, "#,###,###,##0");
				txtTotalLand.Text = Strings.Format(lngTotLand, "#,###,###,##0");
				txtTotalOldAssessment.Text = Strings.Format(lngTotOldAssess, "#,###,###,##0");
				txtTotalOldExempt.Text = Strings.Format(lngTotOldExempt, "#,###,###,##0");
				txtTotalOldLand.Text = Strings.Format(lngTotOldLand, "#,###,###,##0");
				txtTotalOldBldg.Text = Strings.Format(lngTotOldBldg, "#,###,###,##0");
			}
			txtTotal.Text = Strings.Format(lngTotalAccounts, "#,###,###,##0");
		}

	
	}
}
