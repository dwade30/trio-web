﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmUpdateGroup.
	/// </summary>
	partial class frmUpdateGroup : BaseForm
	{
		public FCGrid GridDest;
		public FCGrid gridSource;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSave;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateGroup));
            this.GridDest = new fecherFoundation.FCGrid();
            this.gridSource = new fecherFoundation.FCGrid();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 563);
            this.BottomPanel.Size = new System.Drawing.Size(681, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridDest);
            this.ClientArea.Controls.Add(this.gridSource);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(681, 503);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(681, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(166, 30);
            this.HeaderText.Text = "Update Group";
            // 
            // GridDest
            // 
            this.GridDest.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridDest.Cols = 6;
            this.GridDest.FixedCols = 0;
            this.GridDest.Location = new System.Drawing.Point(30, 308);
            this.GridDest.Name = "GridDest";
            this.GridDest.RowHeadersVisible = false;
            this.GridDest.Rows = 1;
            this.GridDest.Size = new System.Drawing.Size(638, 192);
            this.GridDest.TabIndex = 4;
            this.GridDest.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDest_ValidateEdit);
            this.GridDest.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridDest_MouseDownEvent);
            this.GridDest.DragDrop += new Wisej.Web.DragEventHandler(this.GridDest_OLEDragDrop);
            // 
            // gridSource
            // 
            this.gridSource.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridSource.Cols = 6;
            this.gridSource.FixedCols = 0;
            this.gridSource.Location = new System.Drawing.Point(30, 96);
            this.gridSource.Name = "gridSource";
            this.gridSource.RowHeadersVisible = false;
            this.gridSource.Rows = 1;
            this.gridSource.Size = new System.Drawing.Size(638, 165);
            this.gridSource.TabIndex = 2;
            this.gridSource.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridSource_ValidateEdit);
            this.gridSource.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.gridSource_MouseDownEvent);
            this.gridSource.DragDrop += new Wisej.Web.DragEventHandler(this.gridSource_OLEDragDrop);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(603, 33);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "DRAG ENTRIES TO PLACE IN THE CORRECT GROUP.  YOU MAY HAVE ALL ENTRIES IN THE ORIG" +
    "INAL GROUP, ALL ENTRIES IN THE NEW GROUP, OR A COMBINATION OF THE TWO";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 70);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(221, 18);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "ORIGINAL GROUP";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 276);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(221, 18);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "NEW GROUP";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(196, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(187, 48);
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmUpdateGroup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(681, 671);
            this.FillColor = 0;
            this.Name = "frmUpdateGroup";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Update Group";
            this.Load += new System.EventHandler(this.frmUpdateGroup_Load);
            this.Resize += new System.EventHandler(this.frmUpdateGroup_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUpdateGroup_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
