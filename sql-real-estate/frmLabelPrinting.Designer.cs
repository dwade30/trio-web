//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLabelPrinting.
	/// </summary>
	partial class frmLabelPrinting : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbtPrint;
		public fecherFoundation.FCLabel lbltPrint;
		public fecherFoundation.FCComboBox cmbtLabelType;
		public fecherFoundation.FCLabel lbltLabelType;
		public fecherFoundation.FCComboBox cmbCharWidth;
		public fecherFoundation.FCLabel lblCharWidth;
		public fecherFoundation.FCTextBox txtNumDuplicates;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtIMPB;
		public fecherFoundation.FCCheckBox chkIMPB;
		public fecherFoundation.FCTextBox txtCMFNumber;
		public fecherFoundation.FCCheckBox chkSeparateMapLot;
		public fecherFoundation.FCCheckBox chkIgnoreNumLines;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblIMPB;
		public fecherFoundation.FCLabel lblCMFNumber;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCCheckBox chkEliminate;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkOwner;
		public fecherFoundation.FCCheckBox chkAcres;
		public fecherFoundation.FCCheckBox chkSecOwner;
		public fecherFoundation.FCCheckBox ChkLocation;
		public fecherFoundation.FCCheckBox ChkRef2;
		public fecherFoundation.FCCheckBox chkETax;
		public fecherFoundation.FCCheckBox chkAccount;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCCheckBox chkRef1;
		public fecherFoundation.FCCheckBox chkMapLot;
		public fecherFoundation.FCFrame framLaser;
		public fecherFoundation.FCCheckBox chkEightPoint;
		public fecherFoundation.FCCheckBox chkChooseLabelStart;
		public fecherFoundation.FCCheckBox chkAdjust;
		public fecherFoundation.FCTextBox txtAlignment;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdPrintPreview;
		public fecherFoundation.FCButton cmdReprintCertifiedMailList;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLabelPrinting));
			this.cmbOrder = new fecherFoundation.FCComboBox();
			this.lblOrder = new fecherFoundation.FCLabel();
			this.cmbtPrint = new fecherFoundation.FCComboBox();
			this.lbltPrint = new fecherFoundation.FCLabel();
			this.cmbtLabelType = new fecherFoundation.FCComboBox();
			this.lbltLabelType = new fecherFoundation.FCLabel();
			this.cmbCharWidth = new fecherFoundation.FCComboBox();
			this.lblCharWidth = new fecherFoundation.FCLabel();
			this.txtNumDuplicates = new fecherFoundation.FCTextBox();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.txtIMPB = new fecherFoundation.FCTextBox();
			this.chkIMPB = new fecherFoundation.FCCheckBox();
			this.txtCMFNumber = new fecherFoundation.FCTextBox();
			this.chkSeparateMapLot = new fecherFoundation.FCCheckBox();
			this.chkIgnoreNumLines = new fecherFoundation.FCCheckBox();
			this.cmbLabelType = new fecherFoundation.FCComboBox();
			this.lblIMPB = new fecherFoundation.FCLabel();
			this.lblCMFNumber = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.chkEliminate = new fecherFoundation.FCCheckBox();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.chkOwner = new fecherFoundation.FCCheckBox();
			this.chkAcres = new fecherFoundation.FCCheckBox();
			this.chkSecOwner = new fecherFoundation.FCCheckBox();
			this.ChkLocation = new fecherFoundation.FCCheckBox();
			this.ChkRef2 = new fecherFoundation.FCCheckBox();
			this.chkETax = new fecherFoundation.FCCheckBox();
			this.chkAccount = new fecherFoundation.FCCheckBox();
			this.chkAddress = new fecherFoundation.FCCheckBox();
			this.chkRef1 = new fecherFoundation.FCCheckBox();
			this.chkMapLot = new fecherFoundation.FCCheckBox();
			this.framLaser = new fecherFoundation.FCFrame();
			this.chkEightPoint = new fecherFoundation.FCCheckBox();
			this.chkChooseLabelStart = new fecherFoundation.FCCheckBox();
			this.chkAdjust = new fecherFoundation.FCCheckBox();
			this.txtAlignment = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.cmdReprintCertifiedMailList = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkIMPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSeparateMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIgnoreNumLines)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEliminate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSecOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ChkLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ChkRef2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkETax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRef1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framLaser)).BeginInit();
			this.framLaser.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEightPoint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReprintCertifiedMailList)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrintPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtNumDuplicates);
			this.ClientArea.Controls.Add(this.Frame5);
			this.ClientArea.Controls.Add(this.chkEliminate);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.cmbOrder);
			this.ClientArea.Controls.Add(this.lblCharWidth);
			this.ClientArea.Controls.Add(this.lblOrder);
			this.ClientArea.Controls.Add(this.cmbtPrint);
			this.ClientArea.Controls.Add(this.lbltPrint);
			this.ClientArea.Controls.Add(this.cmbtLabelType);
			this.ClientArea.Controls.Add(this.lbltLabelType);
			this.ClientArea.Controls.Add(this.cmbCharWidth);
			this.ClientArea.Controls.Add(this.framLaser);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdReprintCertifiedMailList);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.cmdReprintCertifiedMailList, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(84, 30);
			this.HeaderText.Text = "Labels";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbOrder
			// 
			this.cmbOrder.AutoSize = false;
			this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrder.FormattingEnabled = true;
			this.cmbOrder.Items.AddRange(new object[] {
				"Account Number",
				"Map/Lot",
				"Zip Code",
				"Name",
				"Location"
			});
			this.cmbOrder.Location = new System.Drawing.Point(691, 90);
			this.cmbOrder.Name = "cmbOrder";
			this.cmbOrder.Size = new System.Drawing.Size(260, 40);
			this.cmbOrder.TabIndex = 37;
			this.cmbOrder.Text = "Account Number";
			this.ToolTip1.SetToolTip(this.cmbOrder, null);
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Location = new System.Drawing.Point(519, 104);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(72, 15);
			this.lblOrder.TabIndex = 38;
			this.lblOrder.Text = "ORDER BY";
			this.lblOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblOrder, null);
			// 
			// cmbtPrint
			// 
			this.cmbtPrint.AutoSize = false;
			this.cmbtPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbtPrint.FormattingEnabled = true;
			this.cmbtPrint.Items.AddRange(new object[] {
				"All Accounts",
				"Range",
				"Individual",
				"From Extract"
			});
			this.cmbtPrint.Location = new System.Drawing.Point(691, 30);
			this.cmbtPrint.Name = "cmbtPrint";
			this.cmbtPrint.Size = new System.Drawing.Size(260, 40);
			this.cmbtPrint.TabIndex = 39;
			this.cmbtPrint.Text = "All Accounts";
			this.ToolTip1.SetToolTip(this.cmbtPrint, null);
			// 
			// lbltPrint
			// 
			this.lbltPrint.AutoSize = true;
			this.lbltPrint.Location = new System.Drawing.Point(519, 44);
			this.lbltPrint.Name = "lbltPrint";
			this.lbltPrint.Size = new System.Drawing.Size(45, 15);
			this.lbltPrint.TabIndex = 40;
			this.lbltPrint.Text = "PRINT";
			this.lbltPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lbltPrint, null);
			// 
			// cmbtLabelType
			// 
			this.cmbtLabelType.AutoSize = false;
			this.cmbtLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbtLabelType.FormattingEnabled = true;
			this.cmbtLabelType.Items.AddRange(new object[] {
				"Mailing Labels",
				"Address Labels - Card",
				"Change of Assessment",
				"Assessment Labels - Card",
				"Card Label",
				"Certified Mail Forms",
				"Selected Items",
			});
			this.cmbtLabelType.Location = new System.Drawing.Point(239, 30);
			this.cmbtLabelType.Name = "cmbtLabelType";
			this.cmbtLabelType.Size = new System.Drawing.Size(260, 40);
			this.cmbtLabelType.TabIndex = 41;
			this.cmbtLabelType.Text = "Mailing Labels";
			this.ToolTip1.SetToolTip(this.cmbtLabelType, null);
			this.cmbtLabelType.SelectedIndexChanged += new System.EventHandler(this.OptLabelType_CheckedChanged);
			// 
			// lbltLabelType
			// 
			this.lbltLabelType.AutoSize = true;
			this.lbltLabelType.Location = new System.Drawing.Point(30, 44);
			this.lbltLabelType.Name = "lbltLabelType";
			this.lbltLabelType.Size = new System.Drawing.Size(163, 15);
			this.lbltLabelType.TabIndex = 42;
			this.lbltLabelType.Text = "TYPE OF LABEL / REPORT";
			this.lbltLabelType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lbltLabelType, null);
			// 
			// cmbCharWidth
			// 
			this.cmbCharWidth.AutoSize = false;
			this.cmbCharWidth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCharWidth.FormattingEnabled = true;
			this.cmbCharWidth.Items.AddRange(new object[] {
				"10 Characters per inch",
				"12 Characters per inch",
				"17 Characters per inch"
			});
			this.cmbCharWidth.Location = new System.Drawing.Point(691, 242);
			this.cmbCharWidth.Name = "cmbCharWidth";
			this.cmbCharWidth.Size = new System.Drawing.Size(260, 40);
			this.cmbCharWidth.TabIndex = 43;
			this.cmbCharWidth.Text = "10 Characters per inch";
			this.ToolTip1.SetToolTip(this.cmbCharWidth, null);
			// 
			// lblCharWidth
			// 
			this.lblCharWidth.AutoSize = true;
			this.lblCharWidth.Location = new System.Drawing.Point(519, 256);
			this.lblCharWidth.Name = "lblCharWidth";
			this.lblCharWidth.Size = new System.Drawing.Size(124, 15);
			this.lblCharWidth.TabIndex = 44;
			this.lblCharWidth.Text = "PRINTER IS SET TO";
			this.lblCharWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblCharWidth, null);
			// 
			// txtNumDuplicates
			// 
			this.txtNumDuplicates.AutoSize = false;
			this.txtNumDuplicates.BackColor = System.Drawing.SystemColors.Window;
			this.txtNumDuplicates.LinkItem = null;
			this.txtNumDuplicates.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNumDuplicates.LinkTopic = null;
			this.txtNumDuplicates.Location = new System.Drawing.Point(691, 182);
			this.txtNumDuplicates.Name = "txtNumDuplicates";
			this.txtNumDuplicates.Size = new System.Drawing.Size(80, 40);
			this.txtNumDuplicates.TabIndex = 35;
			this.txtNumDuplicates.Text = "0";
			this.txtNumDuplicates.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtNumDuplicates, null);
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.txtIMPB);
			this.Frame5.Controls.Add(this.chkIMPB);
			this.Frame5.Controls.Add(this.txtCMFNumber);
			this.Frame5.Controls.Add(this.chkSeparateMapLot);
			this.Frame5.Controls.Add(this.chkIgnoreNumLines);
			this.Frame5.Controls.Add(this.cmbLabelType);
			this.Frame5.Controls.Add(this.lblIMPB);
			this.Frame5.Controls.Add(this.lblCMFNumber);
			this.Frame5.Controls.Add(this.lblDescription);
			this.Frame5.Location = new System.Drawing.Point(30, 310);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(468, 290);
			this.Frame5.TabIndex = 36;
			this.Frame5.Text = "Label Type";
			this.ToolTip1.SetToolTip(this.Frame5, null);
			// 
			// txtIMPB
			// 
			this.txtIMPB.AutoSize = false;
			this.txtIMPB.BackColor = System.Drawing.SystemColors.Window;
			this.txtIMPB.LinkItem = null;
			this.txtIMPB.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtIMPB.LinkTopic = null;
			this.txtIMPB.Location = new System.Drawing.Point(233, 230);
			this.txtIMPB.Name = "txtIMPB";
			this.txtIMPB.Size = new System.Drawing.Size(215, 40);
			this.txtIMPB.TabIndex = 52;
			this.ToolTip1.SetToolTip(this.txtIMPB, null);
			this.txtIMPB.Visible = false;
			// 
			// chkIMPB
			// 
			this.chkIMPB.Location = new System.Drawing.Point(20, 190);
			this.chkIMPB.Name = "chkIMPB";
			this.chkIMPB.Size = new System.Drawing.Size(281, 27);
			this.chkIMPB.TabIndex = 51;
			this.chkIMPB.Text = "Do not use IMPB Tracking Number";
			this.ToolTip1.SetToolTip(this.chkIMPB, null);
			this.chkIMPB.Visible = false;
			// 
			// txtCMFNumber
			// 
			this.txtCMFNumber.AutoSize = false;
			this.txtCMFNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtCMFNumber.LinkItem = null;
			this.txtCMFNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCMFNumber.LinkTopic = null;
			this.txtCMFNumber.Location = new System.Drawing.Point(233, 137);
			this.txtCMFNumber.Name = "txtCMFNumber";
			this.txtCMFNumber.Size = new System.Drawing.Size(215, 40);
			this.txtCMFNumber.TabIndex = 21;
			this.ToolTip1.SetToolTip(this.txtCMFNumber, null);
			this.txtCMFNumber.Visible = false;
			// 
			// chkSeparateMapLot
			// 
			this.chkSeparateMapLot.Location = new System.Drawing.Point(20, 224);
			this.chkSeparateMapLot.Name = "chkSeparateMapLot";
			this.chkSeparateMapLot.Size = new System.Drawing.Size(362, 27);
			this.chkSeparateMapLot.TabIndex = 20;
			this.chkSeparateMapLot.Text = "Show Map, Lot and Sub lots on separate lines";
			this.ToolTip1.SetToolTip(this.chkSeparateMapLot, "Map and Lot information will not be printed on its own line.  Instead Map,Lot and" + " Sub Lots will be separated and shown on the right side of other lines.");
			this.chkSeparateMapLot.Visible = false;
			// 
			// chkIgnoreNumLines
			// 
			this.chkIgnoreNumLines.Location = new System.Drawing.Point(20, 190);
			this.chkIgnoreNumLines.Name = "chkIgnoreNumLines";
			this.chkIgnoreNumLines.Size = new System.Drawing.Size(381, 27);
			this.chkIgnoreNumLines.TabIndex = 19;
			this.chkIgnoreNumLines.Text = "Add any # lines. Check each label when printing.";
			this.ToolTip1.SetToolTip(this.chkIgnoreNumLines, null);
			this.chkIgnoreNumLines.Visible = false;
			this.chkIgnoreNumLines.CheckedChanged += new System.EventHandler(this.chkIgnoreNumLines_CheckedChanged);
			// 
			// cmbLabelType
			// 
			this.cmbLabelType.AutoSize = false;
			this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLabelType.FormattingEnabled = true;
			this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
			this.cmbLabelType.Name = "cmbLabelType";
			this.cmbLabelType.Size = new System.Drawing.Size(428, 40);
			this.cmbLabelType.TabIndex = 18;
			this.cmbLabelType.Text = "Combo1";
			this.ToolTip1.SetToolTip(this.cmbLabelType, null);
			this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
			// 
			// lblIMPB
			// 
			this.lblIMPB.Location = new System.Drawing.Point(20, 244);
			this.lblIMPB.Name = "lblIMPB";
			this.lblIMPB.Size = new System.Drawing.Size(156, 16);
			this.lblIMPB.TabIndex = 53;
			this.lblIMPB.Text = "IMPB NUMBER";
			this.lblIMPB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblIMPB, null);
			this.lblIMPB.Visible = false;
			// 
			// lblCMFNumber
			// 
			this.lblCMFNumber.Location = new System.Drawing.Point(20, 151);
			this.lblCMFNumber.Name = "lblCMFNumber";
			this.lblCMFNumber.Size = new System.Drawing.Size(156, 16);
			this.lblCMFNumber.TabIndex = 50;
			this.lblCMFNumber.Text = "FIRST BARCODE NUMBER";
			this.lblCMFNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblCMFNumber, null);
			this.lblCMFNumber.Visible = false;
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(20, 91);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(428, 51);
			this.lblDescription.TabIndex = 38;
			this.lblDescription.Text = "LABEL1";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// chkEliminate
			// 
			this.chkEliminate.Location = new System.Drawing.Point(519, 142);
			this.chkEliminate.Name = "chkEliminate";
			this.chkEliminate.Size = new System.Drawing.Size(222, 27);
			this.chkEliminate.TabIndex = 34;
			this.chkEliminate.Text = "Eliminate Duplicate names";
			this.ToolTip1.SetToolTip(this.chkEliminate, null);
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.chkOwner);
			this.Frame4.Controls.Add(this.chkAcres);
			this.Frame4.Controls.Add(this.chkSecOwner);
			this.Frame4.Controls.Add(this.ChkLocation);
			this.Frame4.Controls.Add(this.ChkRef2);
			this.Frame4.Controls.Add(this.chkETax);
			this.Frame4.Controls.Add(this.chkAccount);
			this.Frame4.Controls.Add(this.chkAddress);
			this.Frame4.Controls.Add(this.chkRef1);
			this.Frame4.Controls.Add(this.chkMapLot);
			this.Frame4.Location = new System.Drawing.Point(30, 90);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(469, 210);
			this.Frame4.TabIndex = 33;
			this.Frame4.Text = "Include";
			this.ToolTip1.SetToolTip(this.Frame4, null);
			// 
			// chkOwner
			// 
			this.chkOwner.Checked = true;
			this.chkOwner.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkOwner.Location = new System.Drawing.Point(20, 133);
			this.chkOwner.Name = "chkOwner";
			this.chkOwner.Size = new System.Drawing.Size(75, 27);
			this.chkOwner.TabIndex = 11;
			this.chkOwner.Text = "Owner";
			this.ToolTip1.SetToolTip(this.chkOwner, null);
			this.chkOwner.CheckedChanged += new System.EventHandler(this.chkOwner_CheckedChanged);
			// 
			// chkAcres
			// 
			this.chkAcres.Location = new System.Drawing.Point(224, 170);
			this.chkAcres.Name = "chkAcres";
			this.chkAcres.Size = new System.Drawing.Size(139, 27);
			this.chkAcres.TabIndex = 17;
			this.chkAcres.Text = "Acreage (Card)";
			this.ToolTip1.SetToolTip(this.chkAcres, null);
			this.chkAcres.CheckedChanged += new System.EventHandler(this.chkAcres_CheckedChanged);
			// 
			// chkSecOwner
			// 
			this.chkSecOwner.Location = new System.Drawing.Point(20, 170);
			this.chkSecOwner.Name = "chkSecOwner";
			this.chkSecOwner.Size = new System.Drawing.Size(108, 27);
			this.chkSecOwner.TabIndex = 12;
			this.chkSecOwner.Text = "Sec Owner";
			this.ToolTip1.SetToolTip(this.chkSecOwner, null);
			this.chkSecOwner.CheckedChanged += new System.EventHandler(this.chkSecOwner_CheckedChanged);
			// 
			// ChkLocation
			// 
			this.ChkLocation.Location = new System.Drawing.Point(224, 64);
			this.ChkLocation.Name = "ChkLocation";
			this.ChkLocation.Size = new System.Drawing.Size(89, 27);
			this.ChkLocation.TabIndex = 14;
			this.ChkLocation.Text = "Location";
			this.ToolTip1.SetToolTip(this.ChkLocation, null);
			this.ChkLocation.CheckedChanged += new System.EventHandler(this.ChkLocation_CheckedChanged);
			// 
			// ChkRef2
			// 
			this.ChkRef2.Location = new System.Drawing.Point(224, 98);
			this.ChkRef2.Name = "ChkRef2";
			this.ChkRef2.Size = new System.Drawing.Size(116, 27);
			this.ChkRef2.TabIndex = 15;
			this.ChkRef2.Text = "Reference 2";
			this.ToolTip1.SetToolTip(this.ChkRef2, null);
			this.ChkRef2.CheckedChanged += new System.EventHandler(this.ChkRef2_CheckedChanged);
			// 
			// chkETax
			// 
			this.chkETax.Location = new System.Drawing.Point(224, 133);
			this.chkETax.Name = "chkETax";
			this.chkETax.Size = new System.Drawing.Size(132, 27);
			this.chkETax.TabIndex = 16;
			this.chkETax.Text = "Estimated Tax";
			this.ToolTip1.SetToolTip(this.chkETax, null);
			// 
			// chkAccount
			// 
			this.chkAccount.Location = new System.Drawing.Point(20, 30);
			this.chkAccount.Name = "chkAccount";
			this.chkAccount.Size = new System.Drawing.Size(86, 27);
			this.chkAccount.TabIndex = 8;
			this.chkAccount.Text = "Account";
			this.ToolTip1.SetToolTip(this.chkAccount, null);
			// 
			// chkAddress
			// 
			this.chkAddress.Location = new System.Drawing.Point(224, 30);
			this.chkAddress.Name = "chkAddress";
			this.chkAddress.Size = new System.Drawing.Size(87, 27);
			this.chkAddress.TabIndex = 13;
			this.chkAddress.Text = "Address";
			this.ToolTip1.SetToolTip(this.chkAddress, null);
			this.chkAddress.CheckedChanged += new System.EventHandler(this.chkAddress_CheckedChanged);
			// 
			// chkRef1
			// 
			this.chkRef1.Location = new System.Drawing.Point(20, 98);
			this.chkRef1.Name = "chkRef1";
			this.chkRef1.Size = new System.Drawing.Size(201, 27);
			this.chkRef1.TabIndex = 10;
			this.chkRef1.Text = "Reference 1/Book Page";
			this.ToolTip1.SetToolTip(this.chkRef1, "To change whether book & page or Reference 1 is printed go to File Maintenance ->" + " Customize");
			this.chkRef1.CheckedChanged += new System.EventHandler(this.chkRef1_CheckedChanged);
			// 
			// chkMapLot
			// 
			this.chkMapLot.Location = new System.Drawing.Point(20, 64);
			this.chkMapLot.Name = "chkMapLot";
			this.chkMapLot.Size = new System.Drawing.Size(86, 27);
			this.chkMapLot.TabIndex = 9;
			this.chkMapLot.Text = "Map/Lot";
			this.ToolTip1.SetToolTip(this.chkMapLot, null);
			this.chkMapLot.CheckedChanged += new System.EventHandler(this.chkMapLot_CheckedChanged);
			// 
			// framLaser
			// 
			this.framLaser.Controls.Add(this.chkEightPoint);
			this.framLaser.Controls.Add(this.chkChooseLabelStart);
			this.framLaser.Controls.Add(this.chkAdjust);
			this.framLaser.Controls.Add(this.txtAlignment);
			this.framLaser.Controls.Add(this.Label2);
			this.framLaser.Location = new System.Drawing.Point(519, 242);
			this.framLaser.Name = "framLaser";
			this.framLaser.Size = new System.Drawing.Size(432, 243);
			this.framLaser.TabIndex = 37;
			this.framLaser.Text = "Laser Printer Options";
			this.ToolTip1.SetToolTip(this.framLaser, null);
			// 
			// chkEightPoint
			// 
			this.chkEightPoint.Location = new System.Drawing.Point(20, 185);
			this.chkEightPoint.Name = "chkEightPoint";
			this.chkEightPoint.Size = new System.Drawing.Size(142, 27);
			this.chkEightPoint.TabIndex = 40;
			this.chkEightPoint.Text = "Use 8 point font";
			this.ToolTip1.SetToolTip(this.chkEightPoint, null);
			// 
			// chkChooseLabelStart
			// 
			this.chkChooseLabelStart.Location = new System.Drawing.Point(20, 55);
			this.chkChooseLabelStart.Name = "chkChooseLabelStart";
			this.chkChooseLabelStart.Size = new System.Drawing.Size(229, 27);
			this.chkChooseLabelStart.TabIndex = 45;
			this.chkChooseLabelStart.Text = "Choose Label to Start From";
			this.ToolTip1.SetToolTip(this.chkChooseLabelStart, "Choose this option if this sheet is already partially printed on");
			// 
			// chkAdjust
			// 
			this.chkAdjust.Location = new System.Drawing.Point(20, 92);
			this.chkAdjust.Name = "chkAdjust";
			this.chkAdjust.Size = new System.Drawing.Size(189, 27);
			this.chkAdjust.TabIndex = 42;
			this.chkAdjust.Text = "Use Laser Adjustment";
			this.ToolTip1.SetToolTip(this.chkAdjust, null);
			// 
			// txtAlignment
			// 
			this.txtAlignment.AutoSize = false;
			this.txtAlignment.BackColor = System.Drawing.SystemColors.Window;
			this.txtAlignment.LinkItem = null;
			this.txtAlignment.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAlignment.LinkTopic = null;
			this.txtAlignment.Location = new System.Drawing.Point(20, 132);
			this.txtAlignment.Name = "txtAlignment";
			this.txtAlignment.Size = new System.Drawing.Size(65, 40);
			this.txtAlignment.TabIndex = 39;
			this.txtAlignment.Text = "0";
			this.txtAlignment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAlignment, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(154, 146);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(53, 17);
			this.Label2.TabIndex = 41;
			this.Label2.Text = "INCHES";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(791, 196);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(124, 16);
			this.Label3.TabIndex = 44;
			this.Label3.Text = "DUPLICATE LABELS";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(519, 196);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(39, 16);
			this.Label1.TabIndex = 43;
			this.Label1.Text = "PRINT";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(350, 30);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.Size = new System.Drawing.Size(148, 48);
			this.cmdPrintPreview.TabIndex = 0;
			this.cmdPrintPreview.Text = "Print Preview";
			this.ToolTip1.SetToolTip(this.cmdPrintPreview, null);
			this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// cmdReprintCertifiedMailList
			// 
			this.cmdReprintCertifiedMailList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdReprintCertifiedMailList.AppearanceKey = "toolbarButton";
			this.cmdReprintCertifiedMailList.Location = new System.Drawing.Point(879, 29);
			this.cmdReprintCertifiedMailList.Name = "cmdReprintCertifiedMailList";
			this.cmdReprintCertifiedMailList.Size = new System.Drawing.Size(170, 24);
			this.cmdReprintCertifiedMailList.TabIndex = 1;
			this.cmdReprintCertifiedMailList.Text = "Reprint Certified Mail List";
			this.ToolTip1.SetToolTip(this.cmdReprintCertifiedMailList, null);
			this.cmdReprintCertifiedMailList.Click += new System.EventHandler(this.mnuReprintCertifiedMailList_Click);
			// 
			// frmLabelPrinting
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmLabelPrinting";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Labels";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmLabelPrinting_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLabelPrinting_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkIMPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSeparateMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIgnoreNumLines)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEliminate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSecOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ChkLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ChkRef2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkETax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRef1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framLaser)).EndInit();
			this.framLaser.ResumeLayout(false);
			this.framLaser.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEightPoint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReprintCertifiedMailList)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}