﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmDeleteDuplicatePictures.
	/// </summary>
	partial class frmDeleteDuplicatePictures : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDuplicate;
		public fecherFoundation.FCLabel lblDuplicate;
		public fecherFoundation.FCCheckBox chkMissing;
		public fecherFoundation.FCLabel lblProgress;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeleteDuplicatePictures));
            this.cmbDuplicate = new fecherFoundation.FCComboBox();
            this.lblDuplicate = new fecherFoundation.FCLabel();
            this.chkMissing = new fecherFoundation.FCCheckBox();
            this.lblProgress = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcess = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMissing)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 98);
            this.BottomPanel.Size = new System.Drawing.Size(625, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkMissing);
            this.ClientArea.Controls.Add(this.cmbDuplicate);
            this.ClientArea.Controls.Add(this.lblDuplicate);
            this.ClientArea.Controls.Add(this.lblProgress);
            this.ClientArea.Size = new System.Drawing.Size(645, 399);
            this.ClientArea.Controls.SetChildIndex(this.lblProgress, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDuplicate, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbDuplicate, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkMissing, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(645, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(277, 30);
            this.HeaderText.Text = "Delete Duplicate Entries";
            // 
            // cmbDuplicate
            // 
            this.cmbDuplicate.Items.AddRange(new object[] {
            "Path and filename are the same",
            "Filename is the same, path may be different"});
            this.cmbDuplicate.Location = new System.Drawing.Point(246, -1);
            this.cmbDuplicate.Name = "cmbDuplicate";
            this.cmbDuplicate.Size = new System.Drawing.Size(368, 40);
            this.cmbDuplicate.TabIndex = 6;
            this.cmbDuplicate.Text = "Path and filename are the same";
            this.cmbDuplicate.Visible = false;
            // 
            // lblDuplicate
            // 
            this.lblDuplicate.AutoSize = true;
            this.lblDuplicate.Location = new System.Drawing.Point(30, 13);
            this.lblDuplicate.Name = "lblDuplicate";
            this.lblDuplicate.Size = new System.Drawing.Size(196, 17);
            this.lblDuplicate.TabIndex = 7;
            this.lblDuplicate.Text = "DUPLICATE REQUIREMENTS";
            this.lblDuplicate.Visible = false;
            // 
            // chkMissing
            // 
            this.chkMissing.Location = new System.Drawing.Point(30, 29);
            this.chkMissing.Name = "chkMissing";
            this.chkMissing.Size = new System.Drawing.Size(176, 24);
            this.chkMissing.TabIndex = 3;
            this.chkMissing.Text = "Delete missing pictures";
            this.ToolTip1.SetToolTip(this.chkMissing, "Deletes links to pictures with broken links ");
            this.chkMissing.Visible = false;
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = Wisej.Web.AnchorStyles.Left;
            this.lblProgress.Location = new System.Drawing.Point(30, 59);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(558, 39);
            this.lblProgress.TabIndex = 4;
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(232, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(154, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Delete Duplicates";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmDeleteDuplicatePictures
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(645, 459);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmDeleteDuplicatePictures";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Delete Duplicate Entries";
            this.Load += new System.EventHandler(this.frmDeleteDuplicatePictures_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeleteDuplicatePictures_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMissing)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdProcess;
	}
}
