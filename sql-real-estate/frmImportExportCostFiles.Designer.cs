﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportExportCostFiles.
	/// </summary>
	partial class frmImportExportCostFiles : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkCostFiles;
		public fecherFoundation.FCCheckBox chkAll;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkCostFiles_11;
		public fecherFoundation.FCCheckBox chkCostFiles_9;
		public fecherFoundation.FCCheckBox chkCostFiles_3;
		public fecherFoundation.FCCheckBox chkCostFiles_7;
		public fecherFoundation.FCCheckBox chkCostFiles_6;
		public fecherFoundation.FCCheckBox chkCostFiles_10;
		public fecherFoundation.FCCheckBox chkCostFiles_2;
		public fecherFoundation.FCCheckBox chkCostFiles_1;
		public fecherFoundation.FCCheckBox chkCostFiles_0;
		public fecherFoundation.FCCheckBox chkCostFiles_4;
		public fecherFoundation.FCCheckBox chkCostFiles_5;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportExportCostFiles));
			this.chkAll = new fecherFoundation.FCCheckBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkCostFiles_11 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_9 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_3 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_7 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_6 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_10 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_2 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_1 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_0 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_4 = new fecherFoundation.FCCheckBox();
			this.chkCostFiles_5 = new fecherFoundation.FCCheckBox();
			this.cmdSave = new Wisej.Web.Button();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_5)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 381);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkAll);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(610, 321);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(199, 30);
			this.HeaderText.Text = "Export Cost Files";
			// 
			// chkAll
			// 
			this.chkAll.Location = new System.Drawing.Point(30, 30);
			this.chkAll.Name = "chkAll";
			this.chkAll.Size = new System.Drawing.Size(97, 27);
			this.chkAll.TabIndex = 0;
			this.chkAll.Text = "Export All";
			this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkCostFiles_11);
			this.Frame1.Controls.Add(this.chkCostFiles_9);
			this.Frame1.Controls.Add(this.chkCostFiles_3);
			this.Frame1.Controls.Add(this.chkCostFiles_7);
			this.Frame1.Controls.Add(this.chkCostFiles_6);
			this.Frame1.Controls.Add(this.chkCostFiles_10);
			this.Frame1.Controls.Add(this.chkCostFiles_2);
			this.Frame1.Controls.Add(this.chkCostFiles_1);
			this.Frame1.Controls.Add(this.chkCostFiles_0);
			this.Frame1.Controls.Add(this.chkCostFiles_4);
			this.Frame1.Controls.Add(this.chkCostFiles_5);
			this.Frame1.Location = new System.Drawing.Point(30, 77);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(310, 447);
			this.Frame1.TabIndex = 12;
			this.Frame1.Text = "Export";
			// 
			// chkCostFiles_11
			// 
			this.chkCostFiles_11.Location = new System.Drawing.Point(20, 326);
			this.chkCostFiles_11.Name = "chkCostFiles_11";
			this.chkCostFiles_11.Size = new System.Drawing.Size(270, 27);
			this.chkCostFiles_11.TabIndex = 9;
			this.chkCostFiles_11.Text = "Land Types and Acreage Factors";
			// 
			// chkCostFiles_9
			// 
			this.chkCostFiles_9.Location = new System.Drawing.Point(20, 363);
			this.chkCostFiles_9.Name = "chkCostFiles_9";
			this.chkCostFiles_9.Size = new System.Drawing.Size(196, 27);
			this.chkCostFiles_9.TabIndex = 10;
			this.chkCostFiles_9.Text = "Residential Size Factor";
			// 
			// chkCostFiles_3
			// 
			this.chkCostFiles_3.Location = new System.Drawing.Point(20, 141);
			this.chkCostFiles_3.Name = "chkCostFiles_3";
			this.chkCostFiles_3.Size = new System.Drawing.Size(210, 27);
			this.chkCostFiles_3.TabIndex = 4;
			this.chkCostFiles_3.Text = "Outbuilding SQFT Factor";
			// 
			// chkCostFiles_7
			// 
			this.chkCostFiles_7.Location = new System.Drawing.Point(20, 289);
			this.chkCostFiles_7.Name = "chkCostFiles_7";
			this.chkCostFiles_7.Size = new System.Drawing.Size(173, 27);
			this.chkCostFiles_7.TabIndex = 8;
			this.chkCostFiles_7.Text = "Zone/Neighborhood";
			// 
			// chkCostFiles_6
			// 
			this.chkCostFiles_6.Location = new System.Drawing.Point(20, 252);
			this.chkCostFiles_6.Name = "chkCostFiles_6";
			this.chkCostFiles_6.Size = new System.Drawing.Size(137, 27);
			this.chkCostFiles_6.TabIndex = 7;
			this.chkCostFiles_6.Text = "Land Schedule";
			// 
			// chkCostFiles_10
			// 
			this.chkCostFiles_10.Location = new System.Drawing.Point(20, 400);
			this.chkCostFiles_10.Name = "chkCostFiles_10";
			this.chkCostFiles_10.Size = new System.Drawing.Size(209, 27);
			this.chkCostFiles_10.TabIndex = 11;
			this.chkCostFiles_10.Text = "Tran / Land / Bldg Codes";
			// 
			// chkCostFiles_2
			// 
			this.chkCostFiles_2.Location = new System.Drawing.Point(20, 104);
			this.chkCostFiles_2.Name = "chkCostFiles_2";
			this.chkCostFiles_2.Size = new System.Drawing.Size(110, 27);
			this.chkCostFiles_2.TabIndex = 3;
			this.chkCostFiles_2.Text = "Outbuilding";
			// 
			// chkCostFiles_1
			// 
			this.chkCostFiles_1.Location = new System.Drawing.Point(20, 67);
			this.chkCostFiles_1.Name = "chkCostFiles_1";
			this.chkCostFiles_1.Size = new System.Drawing.Size(89, 27);
			this.chkCostFiles_1.TabIndex = 2;
			this.chkCostFiles_1.Text = "Dwelling";
			// 
			// chkCostFiles_0
			// 
			this.chkCostFiles_0.Location = new System.Drawing.Point(20, 30);
			this.chkCostFiles_0.Name = "chkCostFiles_0";
			this.chkCostFiles_0.Size = new System.Drawing.Size(115, 27);
			this.chkCostFiles_0.TabIndex = 1;
			this.chkCostFiles_0.Text = "Commercial";
			// 
			// chkCostFiles_4
			// 
			this.chkCostFiles_4.Location = new System.Drawing.Point(20, 178);
			this.chkCostFiles_4.Name = "chkCostFiles_4";
			this.chkCostFiles_4.Size = new System.Drawing.Size(105, 27);
			this.chkCostFiles_4.TabIndex = 5;
			this.chkCostFiles_4.Text = "Exemption";
			// 
			// chkCostFiles_5
			// 
			this.chkCostFiles_5.Location = new System.Drawing.Point(20, 215);
			this.chkCostFiles_5.Name = "chkCostFiles_5";
			this.chkCostFiles_5.Size = new System.Drawing.Size(89, 27);
			this.chkCostFiles_5.TabIndex = 6;
			this.chkCostFiles_5.Text = "Property";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(256, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			//FC:FINAL:MSH - i.issue #1140: restored missing handling of click on btn
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Click += new EventHandler(this.mnuSaveExit_Click);
			// 
			// frmImportExportCostFiles
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 489);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmImportExportCostFiles";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Export Cost Files";
			this.Load += new System.EventHandler(this.frmImportExportCostFiles_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportExportCostFiles_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCostFiles_5)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSave;
	}
}
