﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetGroupsToPrint.
	/// </summary>
	partial class frmGetGroupsToPrint : BaseForm
	{
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCListBox List1;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetGroupsToPrint));
            this.Command1 = new fecherFoundation.FCButton();
            this.List1 = new fecherFoundation.FCListBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdContinue = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 376);
            this.BottomPanel.Size = new System.Drawing.Size(550, 83);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.List1);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(550, 316);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.Command1);
            this.TopPanel.Size = new System.Drawing.Size(550, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.Command1, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(206, 30);
            this.HeaderText.Text = "Groups to Include";
            // 
            // Command1
            // 
            this.Command1.Location = new System.Drawing.Point(518, 31);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(84, 25);
            this.Command1.TabIndex = 2;
            this.Command1.Text = "Done";
            this.Command1.Visible = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // List1
            // 
            this.List1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.List1.BackColor = System.Drawing.SystemColors.Window;
            this.List1.Location = new System.Drawing.Point(30, 68);
            this.List1.Name = "List1";
            this.List1.Size = new System.Drawing.Size(483, 225);
            this.List1.SelectedIndexChanged += new System.EventHandler(this.List1_SelectedIndexChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(300, 18);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "CLICK ON GROUPS THAT YOU WOULD LIKE TO PRINT";
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Location = new System.Drawing.Point(225, 17);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(100, 48);
            this.cmdContinue.TabIndex = 0;
            this.cmdContinue.Text = "Continue";
            this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // frmGetGroupsToPrint
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(550, 459);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmGetGroupsToPrint";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Groups to Include";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmGetGroupsToPrint_Load);
            this.Activated += new System.EventHandler(this.frmGetGroupsToPrint_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetGroupsToPrint_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdContinue;
	}
}
