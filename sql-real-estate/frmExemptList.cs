﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExemptList.
	/// </summary>
	public partial class frmExemptList : BaseForm
	{
		public frmExemptList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExemptList InstancePtr
		{
			get
			{
				return (frmExemptList)Sys.GetInstance(typeof(frmExemptList));
			}
		}

		protected frmExemptList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolCancel;

		private void frmExemptList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExemptList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExemptList properties;
			//frmExemptList.ScaleWidth	= 9225;
			//frmExemptList.ScaleHeight	= 7785;
			//frmExemptList.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalVariables.Statics.CancelledIt = true;
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			// Label1.ForeColor = TRIOCOLORBLUE
			SetupGrid();
		}

		private void frmExemptList_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		public bool Init()
		{
			bool Init = false;
			Init = true;
			boolCancel = true;
			//FC:FINAL:MSH - i.issue #1070: show form as tab
			//this.Show(FCForm.FormShowEnum.Modal);
			this.Show(App.MainForm);
			Init = !boolCancel;
			return Init;
		}

		private void FrmExemptList_FormClosed(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1070: call calculation of exemptions if this need
			if (!boolCancel)
			{
                FCUtils.StartTask(this, () =>
                {
                    modcalcexemptions.NewCalcExemptions();
                });
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = false;
			boolCancel = false;
			this.Unload();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			Grid.Cols = 4;
			// Grid.TextMatrix(0, 0) = "Key"
			Grid.TextMatrix(0, 0, "Code");
			Grid.TextMatrix(0, 1, "Assessment Amount");
			Grid.TextMatrix(0, 2, "Description");
			Grid.TextMatrix(0, 3, "Short Description");
            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// Call clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1901 and 1999 And val(cunit & '') <> 99999999 order by crecordnumber", strREDatabase)
			clsLoad.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				Grid.AddItem(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("amount"))) + "\t" + Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) + "\t" + Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("shortdescription"))));
				clsLoad.MoveNext();
			}
		}

		private void ResizeGrid()
		{
			// vbPorter upgrade warning: GridWidth As Variant --> As int
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(2, FCConvert.ToInt32(0.4 * GridWidth));
			// .ColWidth(3) = 0.15 * GridWidth
		}
	}
}
