﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptValueComparison.
	/// </summary>
	public partial class rptValueComparison : BaseSectionReport
	{
		public static rptValueComparison InstancePtr
		{
			get
			{
				return (rptValueComparison)Sys.GetInstance(typeof(rptValueComparison));
			}
		}

		protected rptValueComparison _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptValueComparison()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Assessment Comparison Report";
		}
		// nObj = 1
		//   0	rptValueComparison	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolOnlyChanges;
		bool boolDeletedAsChanges;
		clsDRWrapper clsLoad = new clsDRWrapper();
		// vbPorter upgrade warning: lngTotalCurrLand As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalCurrLand;
		// vbPorter upgrade warning: lngTotalCurrBldg As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalCurrBldg;
		// vbPorter upgrade warning: lngTotalCurrAssess As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalCurrAssess;
		// vbPorter upgrade warning: lngTotalBillLand As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillLand;
		// vbPorter upgrade warning: lngTotalBillBldg As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillBldg;
		// vbPorter upgrade warning: lngTotalBillAssess As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillAssess;
		bool boolUseMaplot;
		string strMinRange = "";
		string strMaxRange = "";
		bool boolUseRange;
		bool boolFromExtract;
		// vbPorter upgrade warning: dtDeletedByDate As DateTime	OnWrite(string, short)
		DateTime dtDeletedByDate;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strWhere = "";
			string strSelect = "";
			string strField = "";
			string strGroupBy = "";
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngTotalCurrLand = 0;
			lngTotalCurrBldg = 0;
			lngTotalCurrAssess = 0;
			lngTotalBillLand = 0;
			lngTotalBillBldg = 0;
			lngTotalBillAssess = 0;
			if (boolDeletedAsChanges)
			{
				if (!boolUseMaplot)
				{
					strWhere = " where (not rsdeleted = 1 or cdate(HLUPDATE & '') BETWEEN #" + FCConvert.ToString(dtDeletedByDate) + "# and #" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "# ) ";
				}
				else
				{
					strWhere = " where trim(rsmaplot & '') <> '' ";
				}
				strSelect = ",sum(rsdeleted) as Deleted ";
			}
			else
			{
				if (!boolUseMaplot)
				{
					strWhere = " where not rsdeleted = 1 ";
				}
				else
				{
					strWhere = " where not rsdeleted = 1 and trim(rsmaplot & '') <> '' ";
				}
				strSelect = "";
			}
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolUseRange)
			{
				if (strWhere == "")
				{
					strWhere = " where ";
				}
				else
				{
					strWhere += " and ";
				}
				if (boolUseMaplot)
				{
					strWhere += " rsmaplot between '" + strMinRange + "' and '" + strMaxRange + "' ";
				}
				else
				{
					strWhere += " rsaccount between " + FCConvert.ToString(Conversion.Val(strMinRange)) + " and " + FCConvert.ToString(Conversion.Val(strMaxRange)) + " ";
				}
			}
			else if (boolFromExtract)
			{
				if (strWhere == "")
				{
					strWhere = " where ";
				}
				else
				{
					strWhere += " and ";
				}
				strWhere += " rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + ")";
			}
			if (boolUseMaplot)
			{
				strField = "RSMapLot";
				lblAccount.Text = "Map / Lot";
				lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				strGroupBy = " rsmaplot,rsaccount";
			}
			else
			{
				strField = "RSAccount";
				lblAccount.Text = "Account";
				lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strGroupBy = " rsaccount";
			}
			// If Not boolUseMaplot Then
			// If boolOnlyChanges Then
			// If Not boolDeletedAsChanges Then
			// Call clsLoad.OpenRecordset("select rsaccount" & strSelect & ",sum(lastlandval) as BillLand,sum(lastbldgval) as BillBldg,sum(rllandval) as currland,sum(rlbldgval) as CurrBldg from master " & strWhere & " group by rsaccount having (sum(rllandval & '') + sum(rlbldgval & '')) <> (sum(lastlandval & '') + sum(lastbldgval & '')) order by rsaccount", strREDatabase)
			// Else
			// Call clsLoad.OpenRecordset("select rsaccount" & strSelect & ",sum(lastlandval) as BillLand,sum(lastbldgval) as BillBldg,sum(rllandval) as currland,sum(rlbldgval) as CurrBldg from master " & strWhere & " group by rsaccount having ((sum(rllandval & '') + sum(rlbldgval & '')) <> (sum(lastlandval & '') + sum(lastbldgval & ''))) or sum(rsdeleted) < 0 order by rsaccount", strREDatabase)
			// End If
			// Else
			// Call clsLoad.OpenRecordset("select rsaccount" & strSelect & ",sum(lastlandval) as BillLand,sum(lastbldgval) as billBldg,sum(rllandval) as currland,sum(rlbldgval) as currbldg from master " & strWhere & " group by rsaccount order by rsaccount", strREDatabase)
			// End If
			// Else
			if (boolOnlyChanges)
			{
				if (!boolDeletedAsChanges)
				{
					clsLoad.OpenRecordset("select " + strField + " " + strSelect + ",sum(lastlandval) as BillLand,sum(lastbldgval) as BillBldg,sum(rllandval) as currland,sum(rlbldgval) as CurrBldg from master " + strWhere + " group by " + strGroupBy + " having (sum(rllandval & '') + sum(rlbldgval & '')) <> (sum(lastlandval & '') + sum(lastbldgval & '')) order by " + strField, modGlobalVariables.strREDatabase);
				}
				else
				{
					clsLoad.OpenRecordset("select " + strField + " " + strSelect + ",sum(lastlandval) as BillLand,sum(lastbldgval) as BillBldg,sum(rllandval) as currland,sum(rlbldgval) as CurrBldg from master " + strWhere + " group by " + strGroupBy + " having ((sum(rllandval & '') + sum(rlbldgval & '')) <> (sum(lastlandval & '') + sum(lastbldgval & ''))) or sum(rsdeleted) < 0 order by " + strField, modGlobalVariables.strREDatabase);
				}
			}
			else
			{
				clsLoad.OpenRecordset("select " + strField + " " + strSelect + ",sum(lastlandval) as BillLand,sum(lastbldgval) as billBldg,sum(rllandval) as currland,sum(rlbldgval) as currbldg from master " + strWhere + " group by " + strGroupBy + " order by " + strField, modGlobalVariables.strREDatabase);
			}
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngCurrLand = 0;
			int lngCurrBldg = 0;
			int lngCurrAssess = 0;
			int lngBillLand = 0;
			int lngBillBldg = 0;
			int lngBillAssess = 0;
			double dblIncDec = 0;
			bool boolDeleted;
			boolDeleted = false;
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [billland] not found!! (maybe it is an alias?)
				lngBillLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("billland"))));
				// TODO Get_Fields: Field [billbldg] not found!! (maybe it is an alias?)
				lngBillBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("billbldg"))));
				lngBillAssess = lngBillLand + lngBillBldg;
				lngTotalBillLand += lngBillLand;
				lngTotalBillBldg += lngBillBldg;
				lngTotalBillAssess += lngBillAssess;
				if (boolDeletedAsChanges)
				{
					if (clsLoad.Get_Fields_Boolean("deleted"))
					{
						boolDeleted = true;
					}
				}
				if (!boolDeleted)
				{
					// TODO Get_Fields: Field [currland] not found!! (maybe it is an alias?)
					lngCurrLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("currland"))));
					// TODO Get_Fields: Field [currbldg] not found!! (maybe it is an alias?)
					lngCurrBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("currbldg"))));
					lngCurrAssess = lngCurrLand + lngCurrBldg;
					lngTotalCurrLand += lngCurrLand;
					lngTotalCurrBldg += lngCurrBldg;
					lngTotalCurrAssess += lngCurrAssess;
				}
				else
				{
					lngCurrLand = 0;
					lngCurrBldg = 0;
					lngCurrAssess = 0;
				}
				if (!boolUseMaplot)
				{
					txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
				}
				else
				{
					txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot"));
				}
				txtPrevLand.Text = Strings.Format(lngBillLand, "#,###,###,##0");
				txtPrevBldg.Text = Strings.Format(lngBillBldg, "#,###,###,##0");
				if (!boolDeleted)
				{
					txtCurrLand.Text = Strings.Format(lngCurrLand, "#,###,###,##0");
					txtCurrBldg.Text = Strings.Format(lngCurrBldg, "#,###,###,##0");
					txtCurrTotal.Text = Strings.Format(lngCurrAssess, "#,###,###,##0");
				}
				else
				{
					txtCurrLand.Text = "Deleted";
					txtCurrBldg.Text = "Account";
					txtCurrTotal.Text = "";
				}
				if (lngBillAssess == lngCurrAssess)
				{
					txtIncDec.Text = "0% ";
				}
				else if (lngBillAssess > lngCurrAssess)
				{
					if (lngCurrAssess == 0)
					{
						txtIncDec.Text = "100% -";
					}
					else
					{
						dblIncDec = FCConvert.ToDouble(lngCurrAssess) / lngBillAssess;
						dblIncDec *= 100;
						dblIncDec = 100 - dblIncDec;
						txtIncDec.Text = Strings.Format(dblIncDec, "##0.0") + "% -";
					}
				}
				else
				{
					if (lngBillAssess == 0)
					{
						txtIncDec.Text = "100% +";
					}
					else
					{
						dblIncDec = FCConvert.ToDouble(lngCurrAssess) / lngBillAssess;
						dblIncDec *= 100;
						dblIncDec -= 100;
						txtIncDec.Text = Strings.Format(dblIncDec, "##0.0") + "% +";
					}
				}
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		public void Init(bool boolChangesOnly, bool boolShowDeleted = false, bool boolByMapLot = false, bool boolByRange = false, bool boolByExtract = false, string strmin = "", string strmax = "", string strDeletedDate = "")
		{
			boolOnlyChanges = boolChangesOnly;
			boolDeletedAsChanges = boolShowDeleted;
			boolUseMaplot = boolByMapLot;
			boolFromExtract = boolByExtract;
			boolUseRange = boolByRange;
			if (Information.IsDate(strDeletedDate))
			{
				dtDeletedByDate = FCConvert.ToDateTime(Strings.Format(strDeletedDate, "MM/dd/yyyy"));
			}
			else
			{
				dtDeletedByDate = DateTime.FromOADate(0);
			}
			strMinRange = strmin;
			strMaxRange = strmax;
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "ValueComparison");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblIncDec As double	OnWrite(Decimal, double)
			double dblIncDec = 0;
			txtTotalPrevLand.Text = Strings.Format(lngTotalBillLand, "#,###,###,##0");
			txtTotalPrevBldg.Text = Strings.Format(lngTotalBillBldg, "#,###,###,##0");
			txtTotalCurrLand.Text = Strings.Format(lngTotalCurrLand, "#,###,###,##0");
			txtTotalCurrBldg.Text = Strings.Format(lngTotalCurrBldg, "#,###,###,##0");
			txtTotalTotal.Text = Strings.Format(lngTotalCurrAssess, "#,###,###,##0");
			if (lngTotalCurrAssess == lngTotalBillAssess)
			{
				txtTotalIncDec.Text = "0% ";
			}
			else if (lngTotalBillAssess > lngTotalCurrAssess)
			{
				if (lngTotalCurrAssess == 0)
				{
					txtTotalIncDec.Text = "100% -";
				}
				else
				{
					dblIncDec = FCConvert.ToDouble(lngTotalBillAssess / lngTotalCurrAssess);
					dblIncDec *= 100;
					txtTotalIncDec.Text = Strings.Format(dblIncDec, "##0") + "% -";
				}
			}
			else
			{
				if (lngTotalBillAssess == 0)
				{
					txtTotalIncDec.Text = "100% +";
				}
				else
				{
					dblIncDec = FCConvert.ToDouble(lngTotalCurrAssess / lngTotalBillAssess);
					dblIncDec *= 100;
					txtTotalIncDec.Text = Strings.Format(dblIncDec, "##0") + "% +";
				}
			}
		}

		
	}
}
