﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptSales.
	/// </summary>
	partial class srptSales
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptSales));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRatio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMean = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDev = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtMapLot,
            this.txtDate,
            this.txtPrice,
            this.txtValuation,
            this.txtRatio,
            this.txtMean,
            this.txtDev});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.19F;
            this.txtAccount.Left = 0.0625F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtAccount.Text = "Field1";
            this.txtAccount.Top = 0.03125F;
            this.txtAccount.Width = 0.75F;
            // 
            // txtMapLot
            // 
            this.txtMapLot.Height = 0.19F;
            this.txtMapLot.Left = 0.875F;
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Style = "font-family: \'Tahoma\'";
            this.txtMapLot.Text = "Field1";
            this.txtMapLot.Top = 0.03125F;
            this.txtMapLot.Width = 1.25F;
            // 
            // txtDate
            // 
            this.txtDate.CanGrow = false;
            this.txtDate.Height = 0.19F;
            this.txtDate.Left = 2.15F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDate.Text = "Field1";
            this.txtDate.Top = 0.03125F;
            this.txtDate.Width = 0.8125F;
            // 
            // txtPrice
            // 
            this.txtPrice.CanGrow = false;
            this.txtPrice.Height = 0.19F;
            this.txtPrice.Left = 3F;
            this.txtPrice.MultiLine = false;
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPrice.Text = "Field1";
            this.txtPrice.Top = 0.03125F;
            this.txtPrice.Width = 1F;
            // 
            // txtValuation
            // 
            this.txtValuation.CanGrow = false;
            this.txtValuation.Height = 0.19F;
            this.txtValuation.Left = 4.02F;
            this.txtValuation.MultiLine = false;
            this.txtValuation.Name = "txtValuation";
            this.txtValuation.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtValuation.Text = "Field1";
            this.txtValuation.Top = 0.031F;
            this.txtValuation.Width = 1F;
            // 
            // txtRatio
            // 
            this.txtRatio.CanGrow = false;
            this.txtRatio.Height = 0.19F;
            this.txtRatio.Left = 5.03F;
            this.txtRatio.MultiLine = false;
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtRatio.Text = "Field2";
            this.txtRatio.Top = 0.031F;
            this.txtRatio.Width = 0.8F;
            // 
            // txtMean
            // 
            this.txtMean.CanGrow = false;
            this.txtMean.Height = 0.19F;
            this.txtMean.Left = 5.85F;
            this.txtMean.MultiLine = false;
            this.txtMean.Name = "txtMean";
            this.txtMean.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtMean.Text = "Field3";
            this.txtMean.Top = 0.031F;
            this.txtMean.Width = 0.8F;
            // 
            // txtDev
            // 
            this.txtDev.CanGrow = false;
            this.txtDev.Height = 0.19F;
            this.txtDev.Left = 6.7F;
            this.txtDev.MultiLine = false;
            this.txtDev.Name = "txtDev";
            this.txtDev.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDev.Text = "Field4";
            this.txtDev.Top = 0.031F;
            this.txtDev.Width = 0.8F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // srptSales
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRatio;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMean;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDev;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
