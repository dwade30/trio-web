﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTreeGrowthCalc.
	/// </summary>
	partial class rptTreeGrowthCalc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTreeGrowthCalc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotPSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotPMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotPHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtName,
            this.txtPSoft,
            this.txtPMixed,
            this.txtPHard,
            this.txtSoft,
            this.txtMixed,
            this.txtHard});
			this.Detail.Height = 0.2F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.15625F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.15625F;
			this.txtName.Left = 0.6875F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtName.Text = null;
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 2.3125F;
			// 
			// txtPSoft
			// 
			this.txtPSoft.Height = 0.15625F;
			this.txtPSoft.Left = 3.1875F;
			this.txtPSoft.Name = "txtPSoft";
			this.txtPSoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPSoft.Text = null;
			this.txtPSoft.Top = 0.03125F;
			this.txtPSoft.Width = 0.6875F;
			// 
			// txtPMixed
			// 
			this.txtPMixed.Height = 0.15625F;
			this.txtPMixed.Left = 3.875F;
			this.txtPMixed.Name = "txtPMixed";
			this.txtPMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPMixed.Text = null;
			this.txtPMixed.Top = 0.03125F;
			this.txtPMixed.Width = 0.6875F;
			// 
			// txtPHard
			// 
			this.txtPHard.Height = 0.15625F;
			this.txtPHard.Left = 4.5625F;
			this.txtPHard.Name = "txtPHard";
			this.txtPHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPHard.Text = null;
			this.txtPHard.Top = 0.03125F;
			this.txtPHard.Width = 0.6875F;
			// 
			// txtSoft
			// 
			this.txtSoft.Height = 0.15625F;
			this.txtSoft.Left = 5.3125F;
			this.txtSoft.Name = "txtSoft";
			this.txtSoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSoft.Text = null;
			this.txtSoft.Top = 0.03125F;
			this.txtSoft.Width = 0.6875F;
			// 
			// txtMixed
			// 
			this.txtMixed.Height = 0.15625F;
			this.txtMixed.Left = 6.0625F;
			this.txtMixed.Name = "txtMixed";
			this.txtMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMixed.Text = null;
			this.txtMixed.Top = 0.03125F;
			this.txtMixed.Width = 0.6875F;
			// 
			// txtHard
			// 
			this.txtHard.Height = 0.15625F;
			this.txtHard.Left = 6.8125F;
			this.txtHard.Name = "txtHard";
			this.txtHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtHard.Text = null;
			this.txtHard.Top = 0.03125F;
			this.txtHard.Width = 0.6875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field11,
            this.txtTotPSoft,
            this.txtTotPMixed,
            this.txtTotPHard,
            this.txtTotSoft,
            this.txtTotMixed,
            this.txtTotHard});
			this.ReportFooter.Height = 0.3229167F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// Field11
			// 
			this.Field11.Height = 0.15625F;
			this.Field11.Left = 0F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Field11.Text = "Total";
			this.Field11.Top = 0.15625F;
			this.Field11.Width = 0.9375F;
			// 
			// txtTotPSoft
			// 
			this.txtTotPSoft.Height = 0.15625F;
			this.txtTotPSoft.Left = 3.0625F;
			this.txtTotPSoft.Name = "txtTotPSoft";
			this.txtTotPSoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotPSoft.Text = null;
			this.txtTotPSoft.Top = 0.15625F;
			this.txtTotPSoft.Width = 0.6875F;
			// 
			// txtTotPMixed
			// 
			this.txtTotPMixed.Height = 0.15625F;
			this.txtTotPMixed.Left = 3.8125F;
			this.txtTotPMixed.Name = "txtTotPMixed";
			this.txtTotPMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotPMixed.Text = "Field13";
			this.txtTotPMixed.Top = 0.15625F;
			this.txtTotPMixed.Width = 0.6875F;
			// 
			// txtTotPHard
			// 
			this.txtTotPHard.Height = 0.15625F;
			this.txtTotPHard.Left = 4.5625F;
			this.txtTotPHard.Name = "txtTotPHard";
			this.txtTotPHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotPHard.Text = "Field14";
			this.txtTotPHard.Top = 0.15625F;
			this.txtTotPHard.Width = 0.6875F;
			// 
			// txtTotSoft
			// 
			this.txtTotSoft.Height = 0.15625F;
			this.txtTotSoft.Left = 5.3125F;
			this.txtTotSoft.Name = "txtTotSoft";
			this.txtTotSoft.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotSoft.Text = "Field15";
			this.txtTotSoft.Top = 0.15625F;
			this.txtTotSoft.Width = 0.6875F;
			// 
			// txtTotMixed
			// 
			this.txtTotMixed.Height = 0.15625F;
			this.txtTotMixed.Left = 6.0625F;
			this.txtTotMixed.Name = "txtTotMixed";
			this.txtTotMixed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotMixed.Text = "Field16";
			this.txtTotMixed.Top = 0.15625F;
			this.txtTotMixed.Width = 0.6875F;
			// 
			// txtTotHard
			// 
			this.txtTotHard.Height = 0.15625F;
			this.txtTotHard.Left = 6.8125F;
			this.txtTotHard.Name = "txtTotHard";
			this.txtTotHard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtTotHard.Text = "Field17";
			this.txtTotHard.Top = 0.15625F;
			this.txtTotHard.Width = 0.6875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage,
            this.Field2,
            this.Field18,
            this.Field19,
            this.Field20,
            this.Field21,
            this.Field22,
            this.Field23,
            this.Field24,
            this.Field25,
            this.Field26,
            this.Field27,
            this.Line1,
            this.Line2,
            this.Line3,
            this.Line4});
			this.PageHeader.Height = 0.7916667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Tree Growth Summary Report";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.813F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.17F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 7F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 0.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 6.375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field2.Text = "Page";
			this.Field2.Top = 0.25F;
			this.Field2.Width = 0.625F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.17F;
			this.Field18.Left = 0F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field18.Text = "Account";
			this.Field18.Top = 0.6070001F;
			this.Field18.Width = 0.625F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.17F;
			this.Field19.Left = 3.0625F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field19.Text = "Soft";
			this.Field19.Top = 0.6070001F;
			this.Field19.Width = 0.688F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.17F;
			this.Field20.Left = 3.8125F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field20.Text = "Mixed";
			this.Field20.Top = 0.6070001F;
			this.Field20.Width = 0.688F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.17F;
			this.Field21.Left = 4.5625F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field21.Text = "Hard";
			this.Field21.Top = 0.6070001F;
			this.Field21.Width = 0.688F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.17F;
			this.Field22.Left = 5.3125F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field22.Text = "Soft";
			this.Field22.Top = 0.6070001F;
			this.Field22.Width = 0.688F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.17F;
			this.Field23.Left = 6.0625F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field23.Text = "Mixed";
			this.Field23.Top = 0.6070001F;
			this.Field23.Width = 0.688F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.17F;
			this.Field24.Left = 6.8125F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field24.Text = "Hard";
			this.Field24.Top = 0.6070001F;
			this.Field24.Width = 0.688F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.17F;
			this.Field25.Left = 0.6875F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field25.Text = "Name";
			this.Field25.Top = 0.6070001F;
			this.Field25.Width = 1.063F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.17F;
			this.Field26.Left = 3.687F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field26.Text = "Previous";
			this.Field26.Top = 0.437F;
			this.Field26.Width = 0.875F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.17F;
			this.Field27.Left = 6.1245F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field27.Text = "New";
			this.Field27.Top = 0.437F;
			this.Field27.Width = 0.563F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.0625F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.53125F;
			this.Line1.Width = 0.5625F;
			this.Line1.X1 = 3.625F;
			this.Line1.X2 = 3.0625F;
			this.Line1.Y1 = 0.53125F;
			this.Line1.Y2 = 0.53125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.625F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.53125F;
			this.Line2.Width = 0.5625F;
			this.Line2.X1 = 5.1875F;
			this.Line2.X2 = 4.625F;
			this.Line2.Y1 = 0.53125F;
			this.Line2.Y2 = 0.53125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.375F;
			this.Line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.53125F;
			this.Line3.Width = 0.6875F;
			this.Line3.X1 = 6.0625F;
			this.Line3.X2 = 5.375F;
			this.Line3.Y1 = 0.53125F;
			this.Line3.Y2 = 0.53125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 6.75F;
			this.Line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.53125F;
			this.Line4.Width = 0.6875F;
			this.Line4.X1 = 7.4375F;
			this.Line4.X2 = 6.75F;
			this.Line4.Y1 = 0.53125F;
			this.Line4.Y2 = 0.53125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptTreeGrowthCalc
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotPHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHard;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotPSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotPMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotPHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotHard;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
