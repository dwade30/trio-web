using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
    /// <summary>
    /// Summary description for rptDeedNames.
    /// </summary>
    public partial class rptDeedNames : BaseSectionReport
    {
        private string subTitle = "";
        private int pageNumber = 0;
        private IEnumerable<REAccountPropertyBrief> properties;
        private IEnumerator<REAccountPropertyBrief> propertyEnumerator;
        
        public rptDeedNames()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
            AddEventHandlers();
        }

        private void InitializeComponentEx()
        {
            this.Name = "Deed Names";
        }
        public void Init(string subTitleCaption, IEnumerable<REAccountPropertyBrief> propertyBriefs)
        {
            subTitle = subTitleCaption;
            properties = propertyBriefs;
            frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Deed Names");
        }
        private void AddEventHandlers()
        { 
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);            
            this.pageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.detail.Format += DetailOnFormat;
        }

        private void DetailOnFormat(object sender, EventArgs e)
        {
            var currentProperty = propertyEnumerator.Current;
            if (currentProperty != null)
            {
                txtAccount.Text = currentProperty.Account.ToString();
                txtDeedName1.Text = currentProperty.DeedName1;
                txtDeedName2.Text = currentProperty.DeedName2;
                txtMap.Text = currentProperty.MapLot;
                propertyEnumerator.MoveNext();
            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            pageNumber += 1;
            txtPage.Text = "Page " + pageNumber.ToString();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = propertyEnumerator.Current == null;            
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            txtMuni.Text = modGlobalConstants.Statics.MuniName;
            txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
            txtCaption2.Text = subTitle;
            propertyEnumerator = properties.GetEnumerator();
            propertyEnumerator.Reset();
            propertyEnumerator.MoveNext();
        }
    }
}
