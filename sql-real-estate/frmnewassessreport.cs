﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewAssessReports.
	/// </summary>
	public partial class frmNewAssessReports : BaseForm
	{
		public frmNewAssessReports()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNewAssessReports InstancePtr
		{
			get
			{
				return (frmNewAssessReports)Sys.GetInstance(typeof(frmNewAssessReports));
			}
		}

		protected frmNewAssessReports _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intCurrentReportNumber;
		bool boolReportSaved;
		// true if the report was saved
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLCODE = 2;
		const int CNSTGRIDCOLIE = 3;
		const int CNSTGRIDCOLMIN = 4;
		const int CNSTGRIDCOLMAX = 5;
		const int CNSTGRIDCOLTP = 6;
		const int CNSTGRIDCOLCAT = 1;
		const int CNSTIEINCLUDE = 0;
		const int CNSTIEEXCLUDE = 1;
		const int CNSTIENEITHER = 2;
		const int CNSTTPTOTAL = 2;
		const int CNSTTPPRINT = 1;
		const int CNSTTPNEITHER = 0;
		// Private Const CNSTREPCATOWNER = 0
		// Private Const CNSTREPCATPROPERTY = 1
		// Private Const CNSTREPCATLAND = 2
		// Private Const CNSTREPCATDWELLING = 3
		// Private Const CNSTREPCATCOMMERCIAL = 4
		// Private Const CNSTREPCATOUTBUILDING = 5
		private clsReportCodes clCodeList = new clsReportCodes();
		private string[] aryCodesByCat = new string[7 + 1];
		private string[] aryCategories = new string[7 + 1];
		private int[] aryCatsFirstCode = new int[7 + 1];
		private string[] aryCatsFirstDesc = new string[7 + 1];

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.TextMatrix(0, CNSTGRIDCOLCAT, "Category");
			Grid.TextMatrix(0, CNSTGRIDCOLCODE, "Code");
			Grid.TextMatrix(0, CNSTGRIDCOLIE, "I/E");
			Grid.TextMatrix(0, CNSTGRIDCOLMIN, "Min");
			Grid.TextMatrix(0, CNSTGRIDCOLMAX, "Max");
			Grid.TextMatrix(0, CNSTGRIDCOLTP, "T/P");
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			string strCategories;
			strCategories = "";
			clCodeList.MoveFirst();
			clCodeList.MovePrevious();
			clsReportParameter tCode;
			int x;
			for (x = 0; x <= 7; x++)
			{
				aryCodesByCat[x] = "";
				aryCatsFirstDesc[x] = "";
			}
			// x
			modGlobalVariables.Statics.LandTypes.Init();
			// If boolShortRealEstate Then
			// Set rsTemp = dbTemp.OpenRecordset("select * from reportcodes where (CODE = 33) or  (code = 34) or (code = 40) or ((NOT code = 400) and (code not between 32 and 43) and (code not between 1 and 17) and (code <> 61) and (code not between 22 and 27)  and (code not between 98 and 146) and (code < 200) and ( not code = 44) and (not code = 46) and (not code between 70 and 71)) order by code",strredatabase)
			// Else
			// Set rsTemp = dbTemp.OpenRecordset("Select * from reportcodes where ((code = 33) or (code = 34) or (code = 40) or (code not between 32 and 39) and (not code = 43) and (code <> 61) and (not code between 70 and 71) and (not code = 400)) order by code",strredatabase)
			// End If
			bool boolSkip;
			while (clCodeList.MoveNext() > 0)
			{
				tCode = clCodeList.GetCurrentCode();
				if (!(tCode == null))
				{
					int vbPorterVar = tCode.Code;
					if ((vbPorterVar == 32) || (vbPorterVar >= 35 && vbPorterVar <= 39))
					{
					}
					else if ((vbPorterVar == 43) || (vbPorterVar == 61) || (vbPorterVar == 70) || (vbPorterVar == 71))
					{
					}
					else
					{
						boolSkip = false;
						if (tCode.Code != 500)
						{
							aryCodesByCat[tCode.CategoryNo] += "#" + FCConvert.ToString(tCode.Code) + ";" + tCode.Description + "|";
						}
						else
						{
							if (modGlobalVariables.Statics.LandTypes.FindCode(tCode.ID))
							{
								aryCodesByCat[tCode.CategoryNo] += "#" + FCConvert.ToString(tCode.ID) + ";" + tCode.Description + "\t" + modGlobalVariables.Statics.LandTypes.TypeDescription() + "|";
							}
						}
						aryCategories[tCode.CategoryNo] = tCode.CategoryName;
						if (aryCatsFirstDesc[tCode.CategoryNo] == "")
						{
							aryCatsFirstDesc[tCode.CategoryNo] = tCode.Description;
							if (tCode.Code != 500)
							{
								aryCatsFirstCode[tCode.CategoryNo] = tCode.Code;
							}
							else
							{
								aryCatsFirstCode[tCode.CategoryNo] = tCode.ID;
							}
							strCategories += "#" + FCConvert.ToString(tCode.CategoryNo) + ";" + tCode.CategoryName + "|";
						}
					}
				}
			}
			for (x = 1; x <= 7; x++)
			{
				if (aryCodesByCat[x] != "")
				{
					aryCodesByCat[x] = Strings.Mid(aryCodesByCat[x], 1, aryCodesByCat[x].Length - 1);
				}
				// If aryCodesByCat(x) <> "" Then
				// strCategories = strCategories & "#" & x & ";" & aryCategories(x) & "|"
				// End If
			}
			// x
			if (strCategories != "")
			{
				strCategories = Strings.Mid(strCategories, 1, strCategories.Length - 1);
			}
			Grid.ColComboList(CNSTGRIDCOLCAT, strCategories);
			string strTemp;
			strTemp = "";
			strTemp = "#" + FCConvert.ToString(CNSTTPNEITHER) + ";Neither|#" + FCConvert.ToString(CNSTTPPRINT) + ";Print|#" + FCConvert.ToString(CNSTTPTOTAL) + ";Total & Print";
			Grid.ColComboList(CNSTGRIDCOLTP, strTemp);
			strTemp = "";
			strTemp = "#" + FCConvert.ToString(CNSTIEINCLUDE) + ";Include|#" + FCConvert.ToString(CNSTIEEXCLUDE) + ";Exclude|#" + FCConvert.ToString(CNSTIENEITHER) + ";Neither";
			Grid.ColComboList(CNSTGRIDCOLIE, strTemp);

            Grid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		private void ResizeGrid()
		{
			int GridWidth = 0;
			//FC:FINAL:MSH - i.issue #1161: Width replaced by WidthOriginal for correct resizing of columns
			//GridWidth = Grid.Width;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLCAT, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCODE, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMIN, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMAX, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLIE, FCConvert.ToInt32(0.08 * GridWidth));
			// .ColWidth(CNSTGRIDCOLTP) = 0.1 * GridWidth
		}

		private void cmdQuit_Click()
		{
			this.Unload();
		}
		// Private Sub cmdReplace_Click()
		// Dim strItem As String
		// Dim strTemp As String
		// Dim intIndex As Integer
		// Dim intCode As Integer
		// Dim strTP As String
		// Dim strie As String
		// Dim strAry() As String
		// Dim strmin As String
		// Dim strmax As String
		// Dim strDesc As String
		//
		// txtMin.Text = Replace(txtMin.Text, ",", "", , , vbTextCompare)
		// txtMax.Text = Replace(txtMax.Text, ",", "", , , vbTextCompare)
		// strAry = Split(txtCategory.Text, " ", , vbTextCompare)
		// intCode = Val(txtCategory.Text & "")
		// intCode = Val(strAry(0))
		// If intCode <> 15 And intCode <> 50 And intCode <> 69 And intCode <> 74 Then
		// If intCode = 18 Then
		// If Val(txtMin.Text) > 32000 Then
		// MsgBox "The min for exemption codes is too large", vbExclamation, "Invalid Range"
		// Exit Sub
		// End If
		// If Val(txtMax.Text) > 32000 Then
		// MsgBox "The max for exemption codes is too large", vbExclamation, "Invalid Range"
		// Exit Sub
		// End If
		// End If
		// If Val(txtMax.Text) < Val(txtMin.Text) Then
		// MsgBox "The Max value must be greater than or equal to the Min value.", vbExclamation
		// Exit Sub
		// End If
		// End If
		//
		// If Not optIE(2).Value And intCode = 69 Then
		// txtMax.Text = txtMin.Text
		// If UCase(txtMin.Text) <> "FALSE" And UCase(txtMin.Text) <> "TRUE" Then
		// MsgBox "Only the Min need be specified for Tax Acquired but the value must be True or False", vbExclamation, "Invalid Value"
		// Exit Sub
		// End If
		// End If
		//
		//
		//
		//
		// strmin = Trim(txtMin.Text & "")
		// strmax = Trim(txtMax.Text & "")
		// strDesc = Trim(Mid(txtCategory.Text, 4))
		//
		// If intCode = 15 And Not optIE(2).Value Then
		// If InStr(1, strmin, "/", vbTextCompare) <= 0 Or InStr(1, strmax, "/", vbTextCompare) <= 0 Then
		// MsgBox "The min and max must be in the format month/year." & vbNewLine & "The year can be in two or four digit format and the month must be numeric.", vbExclamation, "Invalid Range"
		// Exit Sub
		// End If
		// End If
		// If intCode = 74 And Not optIE(2).Value Then
		// If Not IsDate(strmin) Then
		// MsgBox "Ranges for Date Created must be in the format month/day/year" & vbNewLine & "The year may be in two or four digit format", vbExclamation, "Invalid Range"
		// Exit Sub
		// Else
		// strmin = Format(strmin, "MM/dd/yyyy")
		// If Not IsDate(strmax) Then
		// strmax = strmin
		// End If
		// End If
		// End If
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		// strItem = Format(Val(txtCategory.Text), "@@@@")
		// strItem = Format(intCode, "@@@@")
		// Grid1.TextMatrix(Grid1.Row, 0) = intCode
		//
		// If optTP(0) Then
		//
		// strTP = "T"
		// ElseIf optTP(1) Then
		//
		// strTP = "P"
		// ElseIf optTP(2) Then
		//
		// strTP = "N"
		// End If
		//
		// Grid1.TextMatrix(Grid1.Row, 1) = strTP
		//
		// If optIE(0) Then
		//
		// strie = "I"
		// ElseIf optIE(1) Then
		//
		// strie = "E"
		// ElseIf optIE(2) Then
		// strie = "N"
		// End If
		//
		// Grid1.TextMatrix(Grid1.Row, 2) = strie
		//
		//
		// strTemp = Trim(txtMin.Text & "") & String(8 - Len(Mid(txtMin.Text, 1)), " ")
		// strTemp = Trim(txtMin.Text & "")
		// Grid1.TextMatrix(Grid1.Row, 3) = strmin
		// strTemp = Trim(txtMax.Text & "") & String(8 - Len(Mid(txtMax.Text, 1)), " ")
		// strTemp = Trim(txtMax.Text & "")
		// Grid1.TextMatrix(Grid1.Row, 4) = strmax
		//
		// strTemp = Mid(txtCategory, InStr(1, txtCategory, " ") + 2)
		// Grid1.TextMatrix(Grid1.Row, 5) = strDesc
		//
		//
		//
		//
		//
		// txtMin.Text = ""
		// txtMax.Text = ""
		// End Sub
		//
		private void FillcmbUsers()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' order by userid", "ClientSettings");
			cmbUsers.Clear();
			cmbUsers.AddItem("");
			cmbUsers.ItemData(cmbUsers.NewIndex, 0);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
				cmbUsers.AddItem(FCConvert.ToString(clsLoad.Get_Fields("userid")));
				cmbUsers.ItemData(cmbUsers.NewIndex, clsLoad.Get_Fields_Int32("id"));
				clsLoad.MoveNext();
			}
		}

		private bool SaveReport()
		{
			bool SaveReport = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveReport = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				int x;
				string strTitle;
				bool boolHasAnIncludeOrExclude;
				bool boolHasNoMinMax;
				string strIncEx = "";
				int lngID = 0;
				int intMaxReport = 0;
				Grid.Row = -1;
				Grid.Refresh();
				//Application.DoEvents();
				rsSave.Execute("delete from reporttable where reportnumber = " + FCConvert.ToString(intCurrentReportNumber), modGlobalVariables.strREDatabase);
				if (intCurrentReportNumber == 0)
				{
					rsSave.OpenRecordset("select max(reportnumber) as maxreport from reporttitles", modGlobalVariables.strREDatabase);
					if (!rsSave.EndOfFile())
					{
						// TODO Get_Fields: Field [maxreport] not found!! (maybe it is an alias?)
						intMaxReport = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSave.Get_Fields("maxreport"))));
					}
					else
					{
						intMaxReport = 0;
					}
					intCurrentReportNumber = intMaxReport + 1;
				}
				boolHasAnIncludeOrExclude = false;
				for (x = 1; x <= Grid.Rows - 1; x++)
				{
					boolHasNoMinMax = false;
					strSQL = "Insert into reporttable (reportnumber,code,codeid,torp,incorexc,min,max,linenumber) values (";
					strSQL += FCConvert.ToString(intCurrentReportNumber) + ",";
					if (FCConvert.ToInt32(Grid.TextMatrix(x, CNSTGRIDCOLCAT)) != FCConvert.ToInt32(clsReportCodes.REReportCodeCategory.LandType))
					{
						strSQL += FCConvert.ToString(Conversion.Val(Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE))) + ",0";
					}
					else
					{
						strSQL += "500," + FCConvert.ToString(Conversion.Val(Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE)));
					}
					switch (FCConvert.ToInt32(Grid.TextMatrix(x, CNSTGRIDCOLTP)))
					{
						case CNSTTPTOTAL:
							{
								strSQL += ",'T'";
								break;
							}
						case CNSTTPPRINT:
							{
								strSQL += ",'P'";
								break;
							}
						default:
							{
								strSQL += ",'N'";
								break;
							}
					}
					//end switch
					switch (FCConvert.ToInt32(Grid.TextMatrix(x, CNSTGRIDCOLIE)))
					{
						case CNSTIEEXCLUDE:
							{
								strSQL += ",'E'";
								break;
							}
						case CNSTIEINCLUDE:
							{
								strSQL += ",'I'";
								break;
							}
						default:
							{
								strSQL += ",'N'";
								break;
							}
					}
					//end switch
					if (FCConvert.ToInt32(Grid.TextMatrix(x, CNSTGRIDCOLCAT)) != FCConvert.ToInt32(clsReportCodes.REReportCodeCategory.LandType))
					{
						//FC:FINAL:MSH - i.issue #1218: '==' can't be applied to operands of type 'string' and 'int'
						//if (Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE) == 15
						//    || Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE) == 50
						//    || Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE) == 74)
						int code = FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE));
						if (code == 15 || code == 50 || code == 74)
						{
							if (Strings.Trim(Grid.TextMatrix(x, CNSTGRIDCOLMIN).Replace("/", "").Replace("_", "")) == "")
							{
								strSQL += ",''";
							}
							else
							{
								strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMIN) + "'";
							}
							if (Strings.Trim(Grid.TextMatrix(x, CNSTGRIDCOLMAX).Replace("/", "").Replace("_", "")) == "")
							{
								strSQL += ",''";
							}
							else
							{
								strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMAX) + "'";
							}
						}
						//FC:FINAL:MSH - i.issue #1218: '==' can't be applied to operands of type 'string' and 'int'.
						// Restore missing 'else' (in the original app swicth..case is used here)
						//if (Grid.Cell(FCGrid.CellPropertySettings.flexcpData, x, CNSTGRIDCOLCODE) == 69)
						else if (code == 69)
						{
							// can't have a range. just use min and min
							strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMIN) + "'";
							strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMIN) + "'";
						}
						else
						{
							strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMIN) + "'";
							strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMAX) + "'";
						}
					}
					else
					{
						strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMIN) + "'";
						strSQL += ",'" + Grid.TextMatrix(x, CNSTGRIDCOLMAX) + "'";
					}
					strSQL += "," + FCConvert.ToString(x);
					strSQL += ")";
					rsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
					if (FCConvert.ToDouble(Grid.TextMatrix(x, CNSTGRIDCOLIE)) != CNSTIENEITHER)
					{
						boolHasAnIncludeOrExclude = true;
						if (Strings.Trim(Grid.TextMatrix(x, CNSTGRIDCOLMIN)) == "" && Strings.Trim(Grid.TextMatrix(x, CNSTGRIDCOLMAX)) == "")
						{
							if (FCConvert.ToDouble(Grid.TextMatrix(x, CNSTGRIDCOLIE)) == CNSTIEINCLUDE)
							{
								strIncEx = "Include";
							}
							else
							{
								strIncEx = "Exclude";
							}
							MessageBox.Show("Warning: Line " + FCConvert.ToString(x) + " is set to " + strIncEx + " and it has no min or max values" + "\r\n" + "This will cause an error if you run the extract", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				// x
				rsSave.Execute("delete from reporttitles where reportnumber = " + FCConvert.ToString(intCurrentReportNumber), modGlobalVariables.strREDatabase);
				strTitle = Strings.Trim(txtTitle.Text);
				if (strTitle != string.Empty)
				{
					modGlobalRoutines.escapequote(ref strTitle);
				}
				if (cmbUsers.SelectedIndex >= 0)
				{
					lngID = cmbUsers.ItemData(cmbUsers.SelectedIndex);
				}
				else
				{
					lngID = 0;
				}
				rsSave.Execute("insert into reporttitles (reportnumber,title,description,userid) values (" + FCConvert.ToString(intCurrentReportNumber) + ",'" + strTitle + "','" + txtDescription.Text + "'," + FCConvert.ToString(lngID) + ")", modGlobalVariables.strREDatabase);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save completed", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				if (!boolHasAnIncludeOrExclude)
				{
					MessageBox.Show("Warning:  The extract will not work if you do not include or exclude by at least one category", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				SaveReport = true;
				return SaveReport;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveReport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveReport;
		}
		// Private Sub cmdSave_Click()
		// Dim clsSave As New clsDRWrapper
		// Dim strSQL As String
		// Dim x As Integer
		// Dim strTitle As String
		// Dim boolHasAnIncludeOrExclude As Boolean
		// Dim boolHasNoMinMax As Boolean
		// Dim strIncEx As String
		// Dim lngID As Long
		// Dim intMaxReport As Long
		//
		// On Error GoTo ErrorHandler
		// Screen.MousePointer = vbHourglass
		// Call clsSave.Execute("delete from reporttable where reportnumber = " & intCurrentReportNumber, strREDatabase)
		//
		// If intCurrentReportNumber = 0 Then
		// Call clsSave.OpenRecordset("SELECT MAX(ReportNumber) as MaxReport FROM ReportTITLES", strREDatabase)
		// If Not clsSave.EndOfFile Then
		// intMaxReport = Val(clsSave.Fields("maxreport"))
		// Else
		// intMaxReport = 0
		// End If
		// intCurrentReportNumber = intMaxReport + 1
		// End If
		//
		// boolHasAnIncludeOrExclude = False
		// For x = 1 To (Grid1.Rows - 1)
		// boolHasNoMinMax = False
		// strSQL = "Insert into reporttable (reportnumber,code,torp,incorexc,min,max,linenumber) values ("
		//
		// strSQL = strSQL & intCurrentReportNumber & ","
		// strSQL = strSQL & Val(Grid1.TextMatrix(x, 0) & "")
		// strSQL = strSQL & ",'" & Grid1.TextMatrix(x, 1) & "'"
		// strSQL = strSQL & ",'" & Grid1.TextMatrix(x, 2) & "'"
		// strSQL = strSQL & ",'" & Grid1.TextMatrix(x, 3) & "'"
		// strSQL = strSQL & ",'" & Grid1.TextMatrix(x, 4) & "'"
		// strSQL = strSQL & "," & x
		//
		// strSQL = strSQL & ")"
		// Call clsSave.Execute(strSQL, strREDatabase)
		// If Trim(UCase(Grid1.TextMatrix(x, 2))) = "I" Or Trim(UCase(Grid1.TextMatrix(x, 2))) = "E" Then
		// If Trim(UCase(Grid1.TextMatrix(x, 2))) = "I" Then
		// strIncEx = "INCLUDE"
		// Else
		// strIncEx = "EXCLUDE"
		// End If
		// boolHasAnIncludeOrExclude = True
		// If Trim(Grid1.TextMatrix(x, 3)) = vbNullString Or Trim(Grid1.TextMatrix(x, 4)) = vbNullString Then
		// MsgBox "Warning: Line " & x & " is set to " & strIncEx & " and it has no min or max values" & vbNewLine & "This will cause an error if you run the extract", vbExclamation, "Warning"
		// End If
		// End If
		// Next x
		//
		// Call clsSave.Execute("delete from reporttitles where reportnumber = " & intCurrentReportNumber, strREDatabase)
		// strTitle = Trim(txtTitle.Text)
		// If strTitle <> vbNullString Then
		// Call escapequote(strTitle)
		// End If
		//
		// If cmbUsers.ListIndex >= 0 Then
		// lngID = cmbUsers.ItemData(cmbUsers.ListIndex)
		// Else
		// lngID = 0
		// End If
		// Call clsSave.Execute("insert into reporttitles (reportnumber,title,description,userid) values (" & intCurrentReportNumber & ",'" & strTitle & "','" & txtDescription.Text & "'," & lngID & ")", strREDatabase)
		//
		// Screen.MousePointer = vbDefault
		//
		// MsgBox "Save completed", vbInformation
		// If Not boolHasAnIncludeOrExclude Then
		// MsgBox "Warning:  The extract will not work if you do not include or exclude by at least one category", vbExclamation, "Warning"
		// End If
		// Exit Sub
		// ErrorHandler:
		// Screen.MousePointer = vbDefault
		// MsgBox "Error Number " & Err.Number & "  " & Err.Description & vbNewLine & "In cmdSave_Click", vbCritical
		// End Sub
		private void frmNewAssessReports_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
				modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				cmdQuit_Click();
			}
		}

		private void frmNewAssessReports_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.F9)
			{
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmNewAssessReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNewAssessReports properties;
			//frmNewAssessReports.ScaleWidth	= 9045;
			//frmNewAssessReports.ScaleHeight	= 7170;
			//frmNewAssessReports.LinkTopic	= "Form1";
			//frmNewAssessReports.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			//clCodeList.LoadCodes();
			//SetupGrid();
			//FC:FINAL:DDU:#1850 - check if are items in combobox
			if (cmbUsers.Items.Count <= 0)
			{
				FillcmbUsers();
			}
		}

		private void frmNewAssessReports_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}
		// Private Sub Grid_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// Dim lngRow As Long
		// Dim lngCol As Long
		// lngRow = Grid.Row
		// lngCol = Grid.Col
		// If lngRow > 0 Then
		// Select Case lngCol
		// Case CNSTGRIDCOLMIN, CNSTGRIDCOLMAX
		// Grid.ComboList = ""
		// If Grid.TextMatrix(lngRow, CNSTGRIDCOLCAT) <> REReportCodeCategory.LandType Then
		// Select Case Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE)
		// Case 15, 74, 50
		// date
		// Grid.EditMask = "99/99/9999"
		// Case 69
		// tax acquired
		// Grid.ComboList = "True|False"
		// Grid.EditMask = ""
		// Case Else
		// Grid.EditMask = ""
		// End Select
		// Else
		// Grid.EditMask = ""
		// End If
		// Case Else
		// Grid.EditMask = ""
		// End Select
		// End If
		// End Sub
		private void Grid_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1180: save and use correct indexes of the cell
			int row = Grid.GetFlexRowIndex(e.RowIndex);
			int col = Grid.GetFlexColIndex(e.ColumnIndex);
			if (row > 0)
			{
				// vbPorter upgrade warning: intCat As short --> As int	OnWrite(string)
				int intCat = 0;
				switch (col)
				{
					case CNSTGRIDCOLCAT:
						{
							Grid.TextMatrix(row, CNSTGRIDCOLMIN, "");
							Grid.TextMatrix(row, CNSTGRIDCOLMAX, "");
							intCat = FCConvert.ToInt32(Grid.TextMatrix(row, CNSTGRIDCOLCAT));
							Grid.TextMatrix(row, CNSTGRIDCOLCODE, aryCatsFirstDesc[intCat]);
							Grid.Cell(FCGrid.CellPropertySettings.flexcpData, row, CNSTGRIDCOLCODE, aryCatsFirstCode[intCat]);
							break;
						}
					case CNSTGRIDCOLCODE:
						{
							if (FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpData, row, CNSTGRIDCOLCODE)) != Grid.ComboData())
							{
								if (Conversion.Val(Grid.ComboData()) > 0)
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpData, row, CNSTGRIDCOLCODE, Grid.ComboData());
								}
							}
							Grid.TextMatrix(row, CNSTGRIDCOLMIN, "");
							Grid.TextMatrix(row, CNSTGRIDCOLMAX, "");
							break;
						}
				}
				//end switch
			}
		}

		private void Grid_ComboCloseUp(object sender, EventArgs e)
		{
			int lngRow;
			int lngCol;
			lngRow = Grid.Row;
			lngCol = Grid.Col;
			if (lngRow > 0)
			{
				switch (lngCol)
				{
					case CNSTGRIDCOLCODE:
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol, Grid.ComboData());
							break;
						}
				}
				//end switch
			}
			Grid.Col = CNSTGRIDCOLAUTOID;
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						// add row
						KeyCode = 0;
						AddParameter();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteParameter();
						break;
					}
			}
			//end switch
		}

		private void DeleteParameter()
		{
			if (Grid.Row > 0)
			{
				Grid.RemoveItem(Grid.Row);
			}
		}

		private void AddParameter()
		{
			int lngRow;
			Grid.Rows += 1;
			lngRow = Grid.Rows - 1;
			Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(0));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLIE, FCConvert.ToString(CNSTIEINCLUDE));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLTP, FCConvert.ToString(CNSTTPNEITHER));
			//FC:FINAL:MSH - i.issue #1180: setting incorrect property
			//Grid.TextMatrix(lngRow, CNSTGRIDCOLCAT, clsReportCodes.REReportCodeCategory.PropertyOwner);
			Grid.TextMatrix(lngRow, CNSTGRIDCOLCAT, (int)clsReportCodes.REReportCodeCategory.Property);
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngCol;
			//FC:FINAL:MSH - i.issue #1180: MouseRow and MouseCol have incorrect indexes
			//lngRow = Grid.MouseRow;
			//lngCol = Grid.MouseCol;
			lngRow = Grid.Row;
			lngCol = Grid.Col;
			if (lngRow > 0)
			{
				switch (lngCol)
				{
					case CNSTGRIDCOLCODE:
						{
							// vbPorter upgrade warning: intCat As short --> As int	OnWrite(string)
							int intCat = 0;
							intCat = FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLCAT));
							Grid.ComboList = aryCodesByCat[intCat];
							Grid.EditMask = "";
							break;
						}
					case CNSTGRIDCOLMIN:
					case CNSTGRIDCOLMAX:
						{
							Grid.ComboList = "";
							if (FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLCAT)) != FCConvert.ToInt32(clsReportCodes.REReportCodeCategory.LandType))
							{
								//FC:FINAL:MSH - i.issue #1218: '==' can't be applied to operands of type 'string' and 'int'
								//if (Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDCOLCODE) == 15
								//    || Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDCOLCODE) == 74
								//    || Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDCOLCODE) == 50)
								int code = FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDCOLCODE));
								if (code == 15 || code == 74 || code == 50)
								{
									// date
									Grid.EditMask = "99/99/9999";
								}
								else if (code == 69)
								{
									// tax acquired
									Grid.ComboList = "True|False";
								}
								else
								{
									Grid.EditMask = "";
								}
							}
							else
							{
								Grid.EditMask = "";
							}
							break;
						}
					case CNSTGRIDCOLAUTOID:
						{
							Grid.ComboList = "";
							Grid.EditMask = "";
							break;
						}
					default:
						{
							//FC:FINAL:MSH - i.issue #1180: clear combolist for correct work of selecting on another columns
							Grid.ComboList = "";
							Grid.EditMask = "";
							break;
						}
				}
				//end switch
			}
		}

		private void mnuAddParameter_Click(object sender, System.EventArgs e)
		{
			AddParameter();
		}
		// Private Sub lstReport_dblClick()
		// Dim dbTemp As DAO.Database
		// Dim rsTemp As clsDRWrapper
		// Dim strGroup As String
		// Dim strItem As String
		// Dim x As Integer
		// Dim compstr As String
		//
		//
		//
		//
		//
		// Select Case RSReport.fields("torp")
		// Case "T"
		// optTP(0).Value = True
		// Case "P"
		// optTP(1).Value = True
		// Case "N"
		// optTP(2).Value = True
		// End Select
		//
		// Select Case RSReport.fields("incorexc")
		// Case "I"
		// optIE(0).Value = True
		// Case "E"
		// optIE(1).Value = True
		// Case "N"
		// optIE(2).Value = True
		// End Select
		//
		// End Sub
		private void mnuLoadReport_Click(object sender, System.EventArgs e)
		{
			int lngRepNum;
			lngRepNum = frmLoadReport.InstancePtr.Init();
			if (lngRepNum > 0)
			{
				NewLoadReport(FCConvert.ToInt16(lngRepNum));
			}
		}

		private void mnuNewReport_Click(object sender, System.EventArgs e)
		{
			int lngID;
			int x;
			Grid.Rows = 1;
			intCurrentReportNumber = 0;
			txtDescription.Text = "";
			txtTitle.Text = "";
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			//FC:FINAL:DDU:#1850 - check if are items in combobox
			if (cmbUsers.Items.Count <= 0)
			{
				FillcmbUsers();
			}
			for (x = 0; x <= cmbUsers.Items.Count - 1; x++)
			{
				if (cmbUsers.ItemData(x) == lngID)
				{
					cmbUsers.SelectedIndex = x;
					break;
				}
			}
			// x
		}

		public void mnuNewReport_Click()
		{
			mnuNewReport_Click(cmdNewReport, new System.EventArgs());
		}

		private void mnuPrintCodes_Click(object sender, System.EventArgs e)
		{
			// Call rptReportCodes.Show
			rptReportCodes.InstancePtr.Init(this.Modal);
		}

		private void mnuPrintReport_Click(object sender, System.EventArgs e)
		{
			rptExtractParameters.InstancePtr.Init(ref intCurrentReportNumber, this.Modal);
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		public void mnuQuit_Click()
		{
			//mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuSaveandExit_Click(object sender, System.EventArgs e)
		{
            if (SaveReport())
			{
                //FC:FINAL:BSE #3364 after save form should close
                mnuQuit_Click(sender, e);
			}
		}

		private void mnuSaveAs_Click(object sender, System.EventArgs e)
		{
			intCurrentReportNumber = 0;
			SaveReport();
		}

		private void mnuSaveReport_Click(object sender, System.EventArgs e)
		{
			SaveReport();
        }

		public void Init(int lngReportToLoad)
		{
            //FC:FINAL:AM:#3365 - moved code from load
            clCodeList.LoadCodes();
            SetupGrid();
            if (lngReportToLoad <= 0)
			{
				// new
				intCurrentReportNumber = 0;
				mnuNewReport_Click();
			}
			else
			{
				// load a specific one
				NewLoadReport(FCConvert.ToInt16(lngReportToLoad));
			}
			this.Show(FCForm.FormShowEnum.Modal);
		}
		// vbPorter upgrade warning: intReportNum As short	OnWriteFCConvert.ToInt32(
		private void NewLoadReport(short intReportNum)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngID = 0;
				int lngRow;
				int x;
				intCurrentReportNumber = intReportNum;
				rsLoad.OpenRecordset("select * from reporttitles where reportnumber = " + FCConvert.ToString(intCurrentReportNumber), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					txtTitle.Text = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("TITLE")));
					txtDescription.Text = FCConvert.ToString(rsLoad.Get_Fields_String("description"));
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("userid"))));
					//FC:FINAL:DDU:#1850 - check if are items in combobox
					if (cmbUsers.Items.Count <= 0)
					{
						FillcmbUsers();
					}
					for (x = 0; x <= cmbUsers.Items.Count - 1; x++)
					{
						if (cmbUsers.ItemData(x) == lngID)
						{
							cmbUsers.SelectedIndex = x;
							break;
						}
					}
					// x
				}
				rsLoad.OpenRecordset("select * from reporttable where reportnumber = " + FCConvert.ToString(intCurrentReportNumber) + " order by linenumber", modGlobalVariables.strREDatabase);
				SetupGrid();
				Grid.Rows = 1;
				int intTemp1 = 0;
				int intTemp2 = 0;
				clsReportParameter tCode;
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					tCode = clCodeList.GetCodeByCode(rsLoad.Get_Fields("code"), rsLoad.Get_Fields_Int32("codeid"));
					if (!(tCode == null))
					{
						AddParameter();
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCAT, FCConvert.ToString(tCode.CategoryNo));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, tCode.Description);
						if (tCode.ID == 0)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDCOLCODE, tCode.Code);
						}
						else
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDCOLCODE, tCode.ID);
						}
						switch (tCode.Code)
						{
							case 15:
							case 74:
							case 50:
								{
									if (Information.IsDate(rsLoad.Get_Fields("min")))
									{
										intTemp1 = Strings.InStr(1, FCConvert.ToString(rsLoad.Get_Fields_String("min")), "/", CompareConstants.vbBinaryCompare);
										intTemp2 = Strings.InStrRev(FCConvert.ToString(rsLoad.Get_Fields_String("min")), "/", -1, CompareConstants.vbTextCompare/*?*/);
										if (intTemp1 != intTemp2)
										{
											Grid.TextMatrix(lngRow, CNSTGRIDCOLMIN, Strings.Format(rsLoad.Get_Fields_String("min"), "MM/dd/yyyy"));
										}
										else
										{
											Grid.TextMatrix(lngRow, CNSTGRIDCOLMIN, Strings.Format(Strings.Mid(FCConvert.ToString(rsLoad.Get_Fields_String("min")), 1, intTemp1 - 1) + "/1/" + Strings.Mid(FCConvert.ToString(rsLoad.Get_Fields_String("min")), intTemp1 + 1), "MM/dd/yyyy"));
										}
									}
									else
									{
										Grid.TextMatrix(lngRow, CNSTGRIDCOLMIN, "");
									}
									if (Information.IsDate(rsLoad.Get_Fields("max")))
									{
										intTemp1 = Strings.InStr(1, FCConvert.ToString(rsLoad.Get_Fields_String("max")), "/", CompareConstants.vbBinaryCompare);
										intTemp2 = Strings.InStrRev(FCConvert.ToString(rsLoad.Get_Fields_String("max")), "/", -1, CompareConstants.vbTextCompare/*?*/);
										if (intTemp1 != intTemp2)
										{
											Grid.TextMatrix(lngRow, CNSTGRIDCOLMAX, Strings.Format(rsLoad.Get_Fields_String("max"), "MM/dd/yyyy"));
										}
										else
										{
											Grid.TextMatrix(lngRow, CNSTGRIDCOLMAX, Strings.Format(Strings.Mid(FCConvert.ToString(rsLoad.Get_Fields_String("max")), 1, intTemp1 - 1) + "/1/" + Strings.Mid(FCConvert.ToString(rsLoad.Get_Fields_String("max")), intTemp1 + 1), "MM/dd/yyyy"));
										}
									}
									else
									{
										Grid.TextMatrix(lngRow, CNSTGRIDCOLMAX, "");
									}
									break;
								}
							default:
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLMIN, FCConvert.ToString(rsLoad.Get_Fields_String("min")));
									Grid.TextMatrix(lngRow, CNSTGRIDCOLMAX, FCConvert.ToString(rsLoad.Get_Fields_String("max")));
									break;
								}
						}
						//end switch
						if (Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("torp"))) == "T")
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTP, FCConvert.ToString(CNSTTPTOTAL));
						}
						else if (Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("torp"))) == "P")
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTP, FCConvert.ToString(CNSTTPPRINT));
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTP, FCConvert.ToString(CNSTTPNEITHER));
						}
						if (Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("incorexc"))) == "I")
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLIE, FCConvert.ToString(CNSTIEINCLUDE));
						}
						else if (Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("incorexc"))) == "E")
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLIE, FCConvert.ToString(CNSTIEEXCLUDE));
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLIE, FCConvert.ToString(CNSTIENEITHER));
						}
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Load Report", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
