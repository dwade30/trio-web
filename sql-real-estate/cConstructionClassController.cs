﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cConstructionClassController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCommercialConstructionClass GetCommercialConstructionClass(int lngId)
		{
			cCommercialConstructionClass GetCommercialConstructionClass = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialConstructionClass conClass = null;
				rsLoad.OpenRecordset("select * from CommercialConstructionClasses where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					conClass = new cCommercialConstructionClass();
					// TODO Get_Fields: Field [ClassCategory] not found!! (maybe it is an alias?)
					conClass.ClassCategory = FCConvert.ToInt32(rsLoad.Get_Fields("ClassCategory"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					conClass.CodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields("CodeNumber"));
					conClass.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					conClass.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					conClass.IsUpdated = false;
				}
				GetCommercialConstructionClass = conClass;
				return GetCommercialConstructionClass;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialConstructionClass;
		}

		public cConstructionClassCollection GetCommercialConstructionClasses()
		{
			cConstructionClassCollection GetCommercialConstructionClasses = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cConstructionClassCollection gColl = new cConstructionClassCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialConstructionClass conClass;
				rs.OpenRecordset("select * from CommercialConstructionClasses order by Name", "RealEstate");
				while (!rs.EndOfFile())
				{
					conClass = new cCommercialConstructionClass();
					// TODO Get_Fields: Field [ClassCategory] not found!! (maybe it is an alias?)
					conClass.ClassCategory = FCConvert.ToInt32(rs.Get_Fields("ClassCategory"));
					// TODO Get_Fields: Field [CodeNumber] not found!! (maybe it is an alias?)
					conClass.CodeNumber = FCConvert.ToInt32(rs.Get_Fields("CodeNumber"));
					conClass.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					conClass.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					conClass.IsUpdated = false;
					gColl.AddItem(conClass);
					rs.MoveNext();
				}
				GetCommercialConstructionClasses = gColl;
				return GetCommercialConstructionClasses;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialConstructionClasses;
		}

		public void DeleteCommercialConstructionClass(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialConstructionClasses where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialConstructionClass(ref cCommercialConstructionClass conClass)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (conClass.IsDeleted)
				{
					DeleteCommercialConstructionClass(conClass.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialConstructionClasses where id = " + conClass.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (conClass.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Commercial construction class record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("ClassCategory", conClass.ClassCategory);
				rsSave.Set_Fields("CodeNumber", conClass.CodeNumber);
				rsSave.Set_Fields("Name", conClass.Name);
				rsSave.Update();
				conClass.ID = rsSave.Get_Fields_Int32("ID");
				conClass.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
