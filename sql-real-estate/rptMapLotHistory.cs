﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptMapLotHistory.
	/// </summary>
	public partial class rptMapLotHistory : BaseSectionReport
	{
		public static rptMapLotHistory InstancePtr
		{
			get
			{
				return (rptMapLotHistory)Sys.GetInstance(typeof(rptMapLotHistory));
			}
		}

		protected rptMapLotHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptMapLotHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Map/Lot History";
		}
		// nObj = 1
		//   0	rptMapLotHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		int lngLastAccount;
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref bool boolRange, ref short intType, string strmin = "", string strmax = "")
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				string strOrderBy = "";
				string strWhere;
				string strWhere1;
				string strPreWhere = "";
				string strREFullDBName;
				string strMasterJoin;
				string strMasterJoinJoin = "";
				strREFullDBName = rsReport.Get_GetFullDBName("RealEstate");
				strMasterJoin = modREMain.GetMasterJoin();
				string strMasterJoinQuery;
				strMasterJoinQuery = "(" + strMasterJoin + ") mj";
				lngLastAccount = 0;
				strWhere = "";
				strWhere1 = "";
				switch (intType)
				{
					case 1:
						{
							// account
							strOrderBy = " order by account ";
							if (boolRange)
							{
								strPreWhere = " where account between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
								strWhere1 = " and account between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
							}
							break;
						}
					case 2:
						{
							// name
							strOrderBy = " order by name,account ";
							if (boolRange)
							{
								strWhere1 = " and name between '" + strmin + "' and '" + strmax + "'";
							}
							break;
						}
				}
				//end switch
				// strSQL = "select rsaccount as account,rsname as name,rsmaplot as maplot,0 as line,'' AS mpDATE from master where not rsdeleted = 1 and rscard = 1 " & strWhere1 & " union all (select account,rsname as name,maplot,line,mpdate from maplot inner join master on (master.rsaccount = maplot.account) where rscard = 1 and not rsdeleted = 1) " & strWhere & "  " & strOrderBy & ",line"
				// strSQL = "select mj.rsaccount as account,mj.rsname as name,mj.rsmaplot as maplot,0 as line, '' as mpDate from " & strMasterJoinQuery & " where not rsdeleted = 1 and rscard = 1 " & strWhere1 & " union all (select account,rsname as name,maplot,line,mpdate from maplot inner join master on (master.rsaccount = maplot.account) where rscard = 1 and not rsdeleted = 1) " & strWhere & "  " & strOrderBy & ",line"
				strSQL = "select account,maplot,line,mpdate,rsname as name from (select rsaccount as account,rsmaplot as maplot,0 as line,'' AS mpDATE from master where not rsdeleted = 1 and rscard = 1 " + strWhere1 + " union all (select account,maplot,line,mpdate from maplot  " + strWhere + " )) tbl1 ";
				strSQL += " inner join " + strMasterJoinQuery + " on (mj.rsaccount = tbl1.account) where not rsdeleted = 1 and rscard = 1 " + strWhere1;
				rsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (rsReport.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "MapLotHistory");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsReport.Get_Fields("line")) == 0)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					txtAccount.Text = FCConvert.ToString(rsReport.Get_Fields("account"));
					txtName.Text = FCConvert.ToString(rsReport.Get_Fields_String("name"));
				}
				else
				{
					txtAccount.Text = "";
					txtName.Text = "";
				}
				txtMaplot.Text = FCConvert.ToString(rsReport.Get_Fields_String("maplot"));
				if (Information.IsDate(rsReport.Get_Fields("mpdate")))
				{
					if (rsReport.Get_Fields_DateTime("mpdate") != DateTime.FromOADate(0))
					{
						txtSaleDate.Text = Strings.Format(rsReport.Get_Fields_DateTime("mpdate"), "MM/dd/yyyy");
					}
					else
					{
						txtSaleDate.Text = "";
					}
				}
				else
				{
					txtSaleDate.Text = "";
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
