﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptZoneNeighborhoodDifference.
	/// </summary>
	public partial class rptZoneNeighborhoodDifference : BaseSectionReport
	{
		public static rptZoneNeighborhoodDifference InstancePtr
		{
			get
			{
				return (rptZoneNeighborhoodDifference)Sys.GetInstance(typeof(rptZoneNeighborhoodDifference));
			}
		}

		protected rptZoneNeighborhoodDifference _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				dcZNDiff?.Dispose();
				dcTemp?.Dispose();
                dcTemp = null;
                dcZNDiff = null;
            }
			base.Dispose(disposing);
		}

		public rptZoneNeighborhoodDifference()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptZoneNeighborhoodDifference	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper dcZNDiff = new clsDRWrapper();
		clsDRWrapper dcTemp = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (dcZNDiff.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			eArgs.EOF = false;
		}

		public void Init()
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = dcTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			dcZNDiff.OpenRecordset(strMasterJoin + " where ((pizone <> 0) or (pineighborhood <> 0)) and (not rsdeleted = 1) and rsaccount in (select rsaccount from master group by rsaccount having (avg(pizone) <> convert(int,avg(pizone))) or ( avg(pineighborhood) <> convert(int,avg(pineighborhood)))) order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			if (dcZNDiff.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "ZoneNeighborhood");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			dcTemp.OpenRecordset("Select count(*) as thecount from master where ((pizone <> 0) or (pineighborhood <> 0)) and (not rsdeleted = 1) and rsaccount = " + dcZNDiff.GetData("rsaccount"), modGlobalVariables.strREDatabase);
			while (FCConvert.ToInt32(dcTemp.GetData("thecount")) < 2)
			{
				dcZNDiff.MoveNext();
				if (dcZNDiff.EndOfFile())
				{
					txtaccount.Visible = false;
					txtName.Visible = false;
					txtCard.Visible = false;
					txtZone.Visible = false;
					txtNeighborhood.Visible = false;
					return;
				}
				else
				{
					dcTemp.OpenRecordset("Select count(*) as thecount from master where ((pizone <> 0) or (pineighborhood <> 0)) and rsaccount = " + dcZNDiff.GetData("rsaccount"), modGlobalVariables.strREDatabase);
				}
			}
			if (FCConvert.ToInt32(dcZNDiff.GetData("rscard")) > 1)
			{
				txtaccount.Visible = false;
				txtName.Visible = false;
			}
			else
			{
				txtaccount.Visible = true;
				txtName.Visible = true;
			}
			txtaccount.Text = FCConvert.ToString(dcZNDiff.GetData("rsaccount"));
			txtName.Text = FCConvert.ToString(dcZNDiff.GetData("rsname"));
			txtCard.Text = FCConvert.ToString(dcZNDiff.GetData("rscard"));
			txtZone.Text = FCConvert.ToString(dcZNDiff.GetData("pizone"));
			txtNeighborhood.Text = FCConvert.ToString(dcZNDiff.GetData("pineighborhood"));
			dcZNDiff.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
