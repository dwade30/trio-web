﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialTrend
	{
		//=========================================================
		private int lngSectionID;
		private int lngConstructionClassID;
		private double dblMultiplier;
		private int lngRecordID;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public double Multiplier
		{
			set
			{
				dblMultiplier = value;
				IsUpdated = true;
			}
			get
			{
				double Multiplier = 0;
				Multiplier = dblMultiplier;
				return Multiplier;
			}
		}

		public int SectionID
		{
			set
			{
				lngSectionID = value;
				IsUpdated = true;
			}
			get
			{
				int SectionID = 0;
				SectionID = lngSectionID;
				return SectionID;
			}
		}

		public int ConstructionClassID
		{
			set
			{
				lngConstructionClassID = value;
				IsUpdated = true;
			}
			get
			{
				int ConstructionClassID = 0;
				ConstructionClassID = lngConstructionClassID;
				return ConstructionClassID;
			}
		}
	}
}
