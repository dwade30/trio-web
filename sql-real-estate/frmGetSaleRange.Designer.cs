﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetSaleRange.
	/// </summary>
	partial class frmGetSaleRange : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtBegin;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetSaleRange));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.txtBegin = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSearch);
            this.BottomPanel.Location = new System.Drawing.Point(0, 260);
            this.BottomPanel.Size = new System.Drawing.Size(388, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtEnd);
            this.ClientArea.Controls.Add(this.txtBegin);
            this.ClientArea.Controls.Add(this.cmbRange);
            this.ClientArea.Controls.Add(this.lblRange);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(388, 200);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(388, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(276, 30);
            this.HeaderText.Text = "Range of Sale Accounts";
            // 
            // cmbRange
            // 
            this.cmbRange.AutoSize = false;
            this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRange.FormattingEnabled = true;
            this.cmbRange.Items.AddRange(new object[] {
            "Month and Year",
            "Year",
            "Multi-Year"});
            this.cmbRange.Location = new System.Drawing.Point(198, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(162, 40);
            this.cmbRange.TabIndex = 1;
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(30, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(66, 15);
            this.lblRange.TabIndex = 10;
            this.lblRange.Text = "RANGE IS";
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "acceptButton";
            this.cmdSearch.Location = new System.Drawing.Point(148, 30);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSearch.Size = new System.Drawing.Size(94, 48);
            this.cmdSearch.TabIndex = 8;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtEnd
            // 
            this.txtEnd.AutoSize = false;
            this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnd.LinkItem = null;
            this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEnd.LinkTopic = null;
            this.txtEnd.Location = new System.Drawing.Point(198, 150);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(162, 40);
            this.txtEnd.TabIndex = 3;
            this.txtEnd.Enter += new System.EventHandler(this.txtEnd_Enter);
            // 
            // txtBegin
            // 
            this.txtBegin.AutoSize = false;
            this.txtBegin.BackColor = System.Drawing.SystemColors.Window;
            this.txtBegin.LinkItem = null;
            this.txtBegin.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBegin.LinkTopic = null;
            this.txtBegin.Location = new System.Drawing.Point(198, 90);
            this.txtBegin.Name = "txtBegin";
            this.txtBegin.Size = new System.Drawing.Size(162, 40);
            this.txtBegin.TabIndex = 2;
            this.txtBegin.Enter += new System.EventHandler(this.txtBegin_Enter);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 164);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(116, 17);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "RANGE ENDS IN";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 104);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(112, 17);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "RANGE BEGINS IN";
            // 
            // frmGetSaleRange
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(388, 368);
            this.FillColor = 0;
            this.Name = "frmGetSaleRange";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Range of Sale Accounts";
            this.Load += new System.EventHandler(this.frmGetSaleRange_Load);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
