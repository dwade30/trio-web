﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptMVR.
	/// </summary>
	public partial class rptMVR : BaseSectionReport
	{
		public static rptMVR InstancePtr
		{
			get
			{
				return (rptMVR)Sys.GetInstance(typeof(rptMVR));
			}
		}

		protected rptMVR _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptMVR()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptMVR	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SubReport1.Report = new srptMVRPage1();
			SubReport2.Report = new srptMVRPage2();
			SubReport3.Report = new srptMVRPage3();
			SubReport4.Report = new srptMVRPage4();
			SubReport5.Report = new srptMVRPage5();
			SubReport6.Report = new srptVetExemptWorksheet();
			SubReport7.Report = new srptMVRPage7();
			SubReport8.Report = new srptMVRPage8();
			SubReport9.Report = new srptMVRPage9();
			SubReport10.Report = new rptTaxRateForm();
			SubReport10.Report.UserData = "MVR";
		}

		
	}
}
