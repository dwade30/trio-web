﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage4.
	/// </summary>
	public partial class srptMVRPage4 : FCSectionReport
	{
		public srptMVRPage4()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage4(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }

		private void InitializeComponentEx()
		{		
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp = "";
			// lblYear.Text = .TextMatrix(CNSTMVRROWYEAR, 0)
            strTemp = valuationReturn.ReportYear.ToString();
			lblDateRange1.Text = "4/2/" + Strings.Right(FCConvert.ToString(Conversion.Val(strTemp) - 1), 2) + " through 4/1/" + Strings.Right(strTemp, 2);
			lblYear1.Text = strTemp;
			lblYear2.Text = strTemp;
			lblDateRange2.Text = lblDateRange1.Text;
			lblDateRange3.Text = lblDateRange1.Text;
			lblDateRange4.Text = lblDateRange1.Text;
			lblDateRange5.Text = lblDateRange1.Text;
			lblDateRange6.Text = lblDateRange1.Text;
            txtOpenSpaceA.Text =
                valuationReturn.FarmAndOpenSpace.TotalNumberOfOpenSpaceParcelsWithdrawn.FormatAsNumber();
            txtOpenSpaceB.Text = valuationReturn.FarmAndOpenSpace.TotalNumberOfOpenSpaceAcresWithdrawn
                .FormatAsCurrencyNoSymbol();
            txtOpenSpaceC.Text = valuationReturn.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnOpenSpace
                .FormatAsCurrencyNoSymbol();
            txtNewLine35.Text = valuationReturn.Waterfront.NumberOfParcelsFirstClassified.FormatAsNumber();
            txtLine36.Text = valuationReturn.Waterfront.NumberOfAcresFirstClassified.FormatAsCurrencyNoSymbol();
            txtLine37.Text = valuationReturn.Waterfront.TotalAcresOfWaterfront.FormatAsCurrencyNoSymbol();
            txtLine38.Text = valuationReturn.Waterfront.TotalValuationOfWaterfront.ToInteger().FormatAsNumber();
            txt39a.Text = valuationReturn.Waterfront.NumberOfWaterfrontParcelsWithdrawn.FormatAsNumber();
            txt39b.Text = valuationReturn.Waterfront.NumberOfWaterfrontAcresWithdrawn.FormatAsCurrencyNoSymbol();
            txt39c.Text = valuationReturn.Waterfront.TotalAmountOfPenalties.FormatAsCurrencyNoSymbol();

            txtLine34a1.Text = valuationReturn.ExemptProperty.UnitedStates.ToInteger().FormatAsNumber();
            txtLine34a2.Text = valuationReturn.ExemptProperty.StateOfMaine.ToInteger().FormatAsNumber();
            txtLine34a.Text =
                (valuationReturn.ExemptProperty.UnitedStates + valuationReturn.ExemptProperty.StateOfMaine)
                .ToInteger().FormatAsNumber();
            txtLine34b.Text = valuationReturn.ExemptProperty.NewhampshireWater.ToInteger().FormatAsNumber();
            txtLine34c.Text = valuationReturn.ExemptProperty.Municipal.ToInteger().FormatAsNumber();
            txtLine34d.Text = valuationReturn.ExemptProperty.MunicipalUtilities.ToInteger().FormatAsNumber();
            txtLine34e.Text = valuationReturn.ExemptProperty.MunicipalAirport.ToInteger().FormatAsNumber();

            txtMunicipality.Text = valuationReturn.Municipality;
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
            txtLine34f.Text = valuationReturn.ExemptProperty.PrivateAirport.ToInteger().FormatAsNumber();
			txtLine34g.Text = valuationReturn.ExemptProperty.MunicipalSewage.ToInteger().FormatAsNumber();
		}

		
	}
}
