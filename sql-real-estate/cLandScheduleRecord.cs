﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cLandScheduleRecord
	{
		//=========================================================
		private int lngRecordID;
		private int lngLandCode;
		private double dblCostAmount;
		private double dblExponent1;
		private double dblExponent2;
		private int lngTownNumber;
		private int lngScheduleNumber;
		private bool boolUpdated;
		private bool boolDeleted;
		private double dblWidthExponent1;
		private double dblWidthExponent2;

		public int ScheduleNumber
		{
			set
			{
				lngScheduleNumber = value;
				IsUpdated = true;
			}
			get
			{
				int ScheduleNumber = 0;
				ScheduleNumber = lngScheduleNumber;
				return ScheduleNumber;
			}
		}

		public int LandTypeCode
		{
			set
			{
				lngLandCode = value;
				IsUpdated = true;
			}
			get
			{
				int LandTypeCode = 0;
				LandTypeCode = lngLandCode;
				return LandTypeCode;
			}
		}

		public double Amount
		{
			set
			{
				dblCostAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblCostAmount;
				return Amount;
			}
		}

		public double Exponent2
		{
			set
			{
				dblExponent2 = value;
				IsUpdated = true;
			}
			get
			{
				double Exponent2 = 0;
				Exponent2 = dblExponent2;
				return Exponent2;
			}
		}

		public double Exponent1
		{
			set
			{
				dblExponent1 = value;
				IsUpdated = true;
			}
			get
			{
				double Exponent1 = 0;
				Exponent1 = dblExponent1;
				return Exponent1;
			}
		}

		public int TownNumber
		{
			set
			{
				lngTownNumber = value;
				IsUpdated = true;
			}
			get
			{
				int TownNumber = 0;
				TownNumber = lngTownNumber;
				return TownNumber;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public double WidthExponent2
		{
			set
			{
				dblWidthExponent2 = value;
				IsUpdated = true;
			}
			get
			{
				double WidthExponent2 = 0;
				WidthExponent2 = dblWidthExponent2;
				return WidthExponent2;
			}
		}

		public double WidthExponent1
		{
			set
			{
				dblWidthExponent1 = value;
				IsUpdated = true;
			}
			get
			{
				double WidthExponent1 = 0;
				WidthExponent1 = dblWidthExponent1;
				return WidthExponent1;
			}
		}
	}
}
