﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using Wisej.Web;
using System.IO;
using System.Threading;
using fecherFoundation.VisualBasicLayer;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.PageReportModel;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptpdfPropertyCard.
	/// </summary>
	public partial class rptpdfPropertyCard : BaseSectionReport
	{
		public static rptpdfPropertyCard InstancePtr
		{
			get
			{
				return (rptpdfPropertyCard)Sys.GetInstance(typeof(rptpdfPropertyCard));
			}
		}

		protected rptpdfPropertyCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCost_AutoInitialized?.Dispose();
				clsProp_AutoInitialized?.Dispose();
                clsCost_AutoInitialized = null;
                clsProp_AutoInitialized = null;
            }
			base.Dispose(disposing);
		}

		public rptpdfPropertyCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Property Card";
		}
		// nObj = 1
		//   0	rptpdfPropertyCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper clsCost = new clsDRWrapper();
			public clsDRWrapper clsCost_AutoInitialized = null;
			public clsDRWrapper clsCost
			{
				get
				{
					if ( clsCost_AutoInitialized == null)
					{
						 clsCost_AutoInitialized = new clsDRWrapper();
					}
					return clsCost_AutoInitialized;
				}
				set
				{
					 clsCost_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper clsProp = new clsDRWrapper();
			public clsDRWrapper clsProp_AutoInitialized = null;
			public clsDRWrapper clsProp
			{
				get
				{
					if ( clsProp_AutoInitialized == null)
					{
						 clsProp_AutoInitialized = new clsDRWrapper();
					}
					return clsProp_AutoInitialized;
				}
				set
				{
					 clsProp_AutoInitialized = value;
				}
			}
		private bool boolHidePics;
		string strPropSQL = "";
		bool boolFirstTime;
		
		string strCurrentAccount;
		FCCollection collPageInfo = new FCCollection();
		bool boolShowPreview;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("CurrentAccount");
			this.Fields["CurrentAccount"].Value = 0;
			strCurrentAccount = "";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// If Not clsProp.EndOfFile Then
			// collPageInfo.Add (clsProp.Fields("rsaccount"))
			// End If
			if (!boolFirstTime)
			{
				clsProp.MoveNext();
			}
			else
			{
				boolFirstTime = false;
			}
			eArgs.EOF = clsProp.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{            
            SavePDFFiles();
			frmWait.InstancePtr.Unload();
		}

		public bool ShowPictures
		{
			get
			{
				bool ShowPictures = false;
				ShowPictures = !boolHidePics;
				return ShowPictures;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int const_printtoolid;
			int cnt;
			boolFirstTime = true;
			clsProp.OpenRecordset(strPropSQL, modGlobalVariables.strREDatabase);
			if (clsProp.EndOfFile())
			{
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				//FC:FINAL:MSH - i.issue #1208: change displaying mode to not modal for displaying message box
				//MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning, modal:false);
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			modGlobalVariables.Statics.LandTypes.Init();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so we can control it
			const_printtoolid = 9950;

			clsCost.OpenRecordset("Select * from costrecord order by crecordnumber", modGlobalVariables.strREDatabase);
			SubReport1.Report = new srptPropertyCard1();
			SubReport2.Report = new srptPropertyCard2();
			(SubReport1.Report as srptPropertyCard1).FromPDF = true;
			(SubReport2.Report as srptPropertyCard2).ShowPics = !boolHidePics;
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolRange, short intType, string strStart = "", string strEnd = "", bool boolFromExtract = false, bool boolSingleCard = false, bool boolPreview = false)
		{
			// intType tells what to order by and what the range is of
			string strSaveDir;
			boolShowPreview = boolPreview;
			strSaveDir = FCFileSystem.Statics.UserDataFolder + "\\RealEstate\\PropertyCards";
			if (!Directory.Exists(FCFileSystem.Statics.UserDataFolder + "\\RealEstate"))
			{
				Directory.CreateDirectory(FCFileSystem.Statics.UserDataFolder + "\\RealEstate");
			}
			if (!Directory.Exists(strSaveDir))
			{
				Directory.CreateDirectory(strSaveDir);
			}
			string strOrderBy = "";
			string strWhere;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsProp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			boolHidePics = false;
			strWhere = "where not rsdeleted = 1 ";
			if (boolFromExtract)
			{
				if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
				{
					boolHidePics = true;
				}
			}
			switch (intType)
			{
				case 1:
					{
						// account
						strOrderBy = " Order by rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rsaccount between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd));
							if (Conversion.Val(strStart) != Conversion.Val(strEnd))
							{
								if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
								{
									boolHidePics = true;
								}
							}
						}
						else if (boolSingleCard)
						{
							strWhere += " and rsaccount = " + FCConvert.ToString(Conversion.Val(strStart)) + " and rscard = " + FCConvert.ToString(Conversion.Val(strEnd));
						}
						else if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
				case 2:
					{
						// name
						strOrderBy = " order by rsname,rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rsname between '" + strStart + "' and '" + strEnd + "'";
						}
						if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
				case 3:
					{
						// maplot
						strOrderBy = " order by rsmaplot,rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rsmaplot between '" + strStart + "' and '" + strEnd + "'";
						}
						if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
				case 4:
					{
						// location
						strOrderBy = " order by rslocstreet,rslocnumalph ,rsaccount,rscard ";
						if (boolRange)
						{
							strWhere += " and rslocstreet between '" + strStart + "' and '" + strEnd + "'";
						}
						if (modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges)
						{
							boolHidePics = true;
						}
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolFromExtract)
			{
				strPropSQL = "select master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and master.rsdeleted = 0 " + strOrderBy;
			}
			else
			{
				// strPropSQL = "Select * from master " & strWhere & strOrderBy
				strPropSQL = strMasterJoin + strWhere + strOrderBy;
			}
			if (boolShowPreview)
			{
				frmReportViewer.InstancePtr.Init(this, "", 0, true, true, "Cards", false, "", "TRIO Software", false, true, "PropertyCard");
			}
			else
			{
				frmWait.InstancePtr.Init("Creating PDF files");
				this.Run(true);
			}
			// Me.Show , MDIParent
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			collPageInfo.Add(clsProp.Get_Fields_Int32("rsaccount"));
			SubReport1.Report.UserData = FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rsaccount"))) + "," + FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rscard")));
			SubReport2.Report.UserData = FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rsaccount"))) + "," + FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_Int32("rscard")));
		}

		private void SavePDFFiles()
		{
			int x;
			int lngStart = 0;
			int lngEnd = 0;
			int lngLastAcct;
			int lngCurrAcct = 0;
			GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
			string strFilename = "";
			string strSaveDir;
			bool boolErrors;
			try
			{
				// On Error GoTo ErrorHandler
				strSaveDir = FCFileSystem.Statics.UserDataFolder + "\\RealEstate\\PropertyCards\\";
                //FC:FINAL:AM:#3308 - download the files as zip
                if(File.Exists(strSaveDir + "PDF.zip"))
                {
                    File.Delete(strSaveDir + "PDF.zip");
                }
                var zip = fecherFoundation.ZipFile.Open(strSaveDir + "PDF.zip", System.IO.Compression.ZipArchiveMode.Create);
                boolErrors = false;
				lngLastAcct = 0;
				for (x = 1; x <= collPageInfo.Count; x++)
				{
					lngCurrAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(collPageInfo[x])));
					if (lngCurrAcct != lngLastAcct)
					{
						if (lngLastAcct > 0)
						{
							if (!SaveRange(strSaveDir + FCConvert.ToString(lngLastAcct) + ".pdf", lngStart, lngEnd))
							{
								boolErrors = true;
							}
                            else
                            {
                                zip.CreateEntryFromFile(strSaveDir + FCConvert.ToString(lngLastAcct) + ".pdf", FCConvert.ToString(lngLastAcct) + ".pdf", System.IO.Compression.CompressionLevel.Optimal);
                            }
						}
						lngStart = (2 * x) - 1;
					}
					lngLastAcct = lngCurrAcct;
					lngEnd = 2 * x;
					if (x == collPageInfo.Count)
					{
						if (!SaveRange(strSaveDir + FCConvert.ToString(lngLastAcct) + ".pdf", lngStart, lngEnd))
						{
							boolErrors = true;
						}
                        else
                        {
                            zip.CreateEntryFromFile(strSaveDir + FCConvert.ToString(lngLastAcct) + ".pdf", FCConvert.ToString(lngLastAcct) + ".pdf", System.IO.Compression.CompressionLevel.Optimal);
                        }
                    }
				}

				frmWait.InstancePtr.Unload();
                zip.Dispose();
				if (this.Document.Pages.Count > 0)
				{
					if (!boolErrors)
					{
                        //FC:FINAL:AM:#3308 - download the zip
                        //MessageBox.Show("Pdf file(s) saved in " + strSaveDir);
                        FCUtils.Download(strSaveDir + "PDF.zip");
					}
					else
					{
                        //MessageBox.Show("Not all pdf files were saved successfully in " + strSaveDir);
                        MessageBox.Show("Not all pdf files were saved successfully");
                    }
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
                FCMessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In SavePDFFiles",
                    MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Error");
            }
		}

		private bool SaveRange(string strFilename, int lngStart, int lngEnd)
		{
			bool SaveRange = false;
            try
            {
                // On Error GoTo ErrorHandler
                if (strFilename != "")
                {
                    if (lngEnd >= lngStart)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pe =
                            new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        pe.Export(this.Document, strFilename, FCConvert.ToString(lngStart) + "-" + FCConvert.ToString(lngEnd));
                    }
                }

                SaveRange = true;
                return SaveRange;
            }
            catch (ThreadAbortException exc)
            {
	            FCMessageBox.Show("Thread Exception", MsgBoxStyle.Exclamation, "Save Range");
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("General Exception", MsgBoxStyle.Exclamation, "Save Range");
			}
			return SaveRange;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			//this.Canvas.Tag = strCurrentAccount;
		}

		
	}
}
