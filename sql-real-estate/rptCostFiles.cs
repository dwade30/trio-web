﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptCostFiles.
	/// </summary>
	public partial class rptCostFiles : BaseSectionReport
	{
		public static rptCostFiles InstancePtr
		{
			get
			{
				return (rptCostFiles)Sys.GetInstance(typeof(rptCostFiles));
			}
		}

		protected rptCostFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptCostFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:MSH - i.issue #1111: restore missing report title
			this.Name = "Cost Files";
		}
		// nObj = 1
		//   0	rptCostFiles	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrRow;
		int intPage;
		int lngMaxRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCurrRow > lngMaxRow;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngtempwidth;
			int lngtempheight;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtUnit.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(0, 1);
			txtBase.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(0, 2);
			lblTitle.Text = frmCostFiles.InstancePtr.Text + " " + "Cost Files";
			lngCurrRow = 1;
			intPage = 1;
			lngMaxRow = frmCostFiles.InstancePtr.Grid1.Rows - 1;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// lngtempwidth = MDIParent.Width - MDIParent.GRID.Width - 200
			// If lngtempwidth < 1 Then Exit Sub
			// lngtempheight = MDIParent.GRID.Height ' - 500
			// If lngtempheight < 1 Then Exit Sub
			// 
			// Me.Left = MDIParent.GRID.Left + MDIParent.GRID.Width + 50 ' from 100
			// Me.Top = MDIParent.Top + 600
			// 
			// Me.Width = MDIParent.Width - MDIParent.GRID.Width - 200  ' from 500
			// Me.Height = MDIParent.GRID.Height ' - 500
			// Me.Zoom = -1
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtKey.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(lngCurrRow, 0);
			txtUnitData.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(lngCurrRow, 1);
			txtBaseData.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(lngCurrRow, 2);
			txtLDesc.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(lngCurrRow, 3);
			txtSDesc.Text = frmCostFiles.InstancePtr.Grid1.TextMatrix(lngCurrRow, 4);
			if (frmCostFiles.InstancePtr.Grid1.RowHidden(lngCurrRow))
			{
				Detail.Visible = false;
			}
			else
			{
				Detail.Visible = true;
			}
			lngCurrRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		
	}
}
