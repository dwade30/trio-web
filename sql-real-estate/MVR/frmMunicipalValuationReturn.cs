﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

//using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.IO;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using TWRE0000.MVR;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMVRViewer.
	/// </summary>
	public partial class frmMunicipalValuationReturn : BaseForm, IView<IMunicipalValuationReturnViewModel>
	{
		public frmMunicipalValuationReturn()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmMunicipalValuationReturn(IMunicipalValuationReturnViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            //if (_InstancePtr == null)
            //	_InstancePtr = this;
            this.Shown += FrmMunicipalValuationReturn_Shown;
            this.cmdSave.Click += CmdSave_Click;
            this.cmdLoadLast.Click += CmdLoadLast_Click;
            this.GridResidentialBuildings.CellValidated += GridResidentialBuildings_CellValidated;
            this.txtTaxableBuilding.Validating += TxtTaxableBuilding_Validating;
            this.txtTaxableLand.Validating += TxtTaxableLand_Validating;
            this.txtOtherPersonalProperty.Validating += TxtOtherPersonalProperty_Validating;
            this.txtBusinessEquipment.Validating += TxtBusinessEquipment_Validating;
            this.txtMachineryAndEquipment.Validating += TxtMachineryAndEquipment_Validating;
            this.cmdPrint.Click += CmdPrint_Click;
            SetupControls();
        }

        private void CmdPrint_Click(object sender, EventArgs e)
        {
            SaveMVR();
            ViewModel.PrintMVR();
        }

        private void CmdLoadLast_Click(object sender, EventArgs e)
        {
            ViewModel.LoadMVR();
            ShowData();
        }

        private void TxtMachineryAndEquipment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalculatePropertyTaxTotals();
        }

        private void TxtBusinessEquipment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalculatePropertyTaxTotals();
        }

        private void TxtOtherPersonalProperty_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalculatePropertyTaxTotals();
        }

        private void TxtTaxableLand_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
           RecalculatePropertyTaxTotals();
        }

        private void TxtTaxableBuilding_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalculatePropertyTaxTotals();
        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            SaveMVR();
        }

        private void GridResidentialBuildings_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            RecalculateGridValues();
        }        

        private void FrmMunicipalValuationReturn_Shown(object sender, EventArgs e)
        {
            ShowData();
        }

        private void SetupControls()
        {
            txtCertifiedRatio.AllowOnlyNumericInput(true);
            txtTaxableBuilding.AllowOnlyNumericInput(false);
            txtTaxableLand.AllowOnlyNumericInput(false);
            txtBusinessEquipment.AllowOnlyNumericInput(false);
            txtMachineryAndEquipment.AllowOnlyNumericInput(false);
            txtOtherPersonalProperty.AllowOnlyNumericInput(false);
            txtPropertyTaxRate.AllowOnlyNumericInput(true);
            txtTaxLevy.AllowOnlyNumericInput(false);
            txtTotalFullHomesteadsGranted.AllowOnlyNumericInput(false);
            txtTotalFullHomesteadExemptions.AllowOnlyNumericInput(false);
            txtTotalFullHomesteadsGranted.AllowOnlyNumericInput(false);
            txtHomesteadFullyExemptValue.AllowOnlyNumericInput(false);
            txtHomesteadTotalExemptions.AllowOnlyNumericInput(false);
            txtNumberHomesteadFullyExempt.AllowOnlyNumericInput(false);
            txtTotalHomesteadAssessments.AllowOnlyNumericInput(false);
            txtTotalNumberHomesteads.AllowOnlyNumericInput(false);

            txtNumberBeteApplicationsProcessed.AllowOnlyNumericInput(false);
            txtNumberBeteApproved.AllowOnlyNumericInput(false);
            txtExemptValueBeteQualified.AllowOnlyNumericInput(false);
            txtBeteInTIF.AllowOnlyNumericInput(false);
            txtIncreasedTif.AllowOnlyNumericInput(false);
            txtTifDeposited.AllowOnlyNumericInput(false);
            txtTifBeteDeposited.AllowOnlyNumericInput(false);
            txtMVExcise.AllowOnlyNumericInput(false);
            txtWatercraftExcise.AllowOnlyNumericInput(false);
            txtTransmissionLines.AllowOnlyNumericInput(false);
            txtElectricalGenerationFacilities.AllowOnlyNumericInput(false);
            txtForestAveragePerAcre.AllowOnlyNumericInput(true);
            txtForestNumberOfParcels.AllowOnlyNumericInput(false);
            txtForestSoftwoodAcreage.AllowOnlyNumericInput(true);
            txtForestMixedWoodAcreage.AllowOnlyNumericInput(true);
            txtForestHardwoodAcreage.AllowOnlyNumericInput(true);
            txtForestNumberOfAcres.AllowOnlyNumericInput(true);
            txtTifCaptured.AllowOnlyNumericInput(false);
            txtForestSoftwoodRate.AllowOnlyNumericInput(true);
            txtForestMixedwoodRate.AllowOnlyNumericInput(true);
            txtForestHardwoodRate.AllowOnlyNumericInput(true);
            txtForestTotalAssessed.AllowOnlyNumericInput(false);

            txtForestAcresFirstClassifiedThisYear.AllowOnlyNumericInput(true);
            txtForestNumberOfNonCompliancePenalties.AllowOnlyNumericInput(false);
            txtForestAcresWithdrawn.AllowOnlyNumericInput(true);
            txtForestAmountOfPenalties.AllowOnlyNumericInput(true);
            
            txtFarmNumberOfParcelsFirstClassified.AllowOnlyNumericInput(false);
            txtFarmNumberOfAcresFirstClassified.AllowOnlyNumericInput(true);
            txtFarmTotalAcresTillable.AllowOnlyNumericInput(true);
            txtFarmTotalValuationTillable.AllowOnlyNumericInput(false);
            txtFarmSoftwoodRate.AllowOnlyNumericInput(true);
            txtFarmSoftwoodAcres.AllowOnlyNumericInput(true);
            txtFarmMixedWoodRate.AllowOnlyNumericInput(true);
            txtFarmMixedWoodAcres.AllowOnlyNumericInput(true);
            txtFarmHardwoodAcres.AllowOnlyNumericInput(true);
            txtFarmHardwoodRate.AllowOnlyNumericInput(true);
            txtFarmTotalAcresWoodland.AllowOnlyNumericInput(true);
            txtFarmTotalValuationWoodland.AllowOnlyNumericInput(false);
            txtOpenParcelsFirstClassified.AllowOnlyNumericInput(false);
            txtOpenAcresFirstClassified.AllowOnlyNumericInput(true);
            txtOpenTotalAcres.AllowOnlyNumericInput(true);
            txtOpenTotalValuation.AllowOnlyNumericInput(false);
            txtOpenAcresWithdrawn.AllowOnlyNumericInput(true);
            txtOpenParcelsWithdrawn.AllowOnlyNumericInput(false);
            txtOpenTotalAmountOfPenalties.AllowOnlyNumericInput(true);

            txtWaterfrontTotalValuation.AllowOnlyNumericInput(false);
            txtWaterfrontAcresFirstClassified.AllowOnlyNumericInput(true);
            txtWaterfrontAcresWithdrawn.AllowOnlyNumericInput(true);
            txtWaterfrontParcelsFirstClassified.AllowOnlyNumericInput(false);
            txtWaterfrontParcelsWithdrawn.AllowOnlyNumericInput(false);
            txtWaterfrontPenalties.AllowOnlyNumericInput(true);
            txtWaterfrontTotalAcreage.AllowOnlyNumericInput(true);

            txtExemptFederal.AllowOnlyNumericInput(false);
            txtExemptMaine.AllowOnlyNumericInput(false);
            txtExemptNHWater.AllowOnlyNumericInput(false);
            txtExemptMunicipal.AllowOnlyNumericInput(false);
            txtExemptMunicipalUtility.AllowOnlyNumericInput(false);
            txtExemptMunicipalAirport.AllowOnlyNumericInput(false);
            txtExemptPrivateAirport.AllowOnlyNumericInput(false);
            txtExemptWaterSupplyTransport.AllowOnlyNumericInput(false);

            txtExemptBenevolent.AllowOnlyNumericInput(false);
            txtExemptVeteransOrganizations.AllowOnlyNumericInput(false);
            txtExemptVetOrgReimbursable.AllowOnlyNumericInput(false);
            txtExemptChamberOfCommerce.AllowOnlyNumericInput(false);
            txtExemptParsonages.AllowOnlyNumericInput(false);
            txtExemptValueOfParsonages.AllowOnlyNumericInput(false);
            txtExemptTaxableValueOfParsonages.AllowOnlyNumericInput(false);
            txtExemptReligiousWorship.AllowOnlyNumericInput(false);
            txtExemptFraternal.AllowOnlyNumericInput(false);
            txtExemptHospitalPersonalProperty.AllowOnlyNumericInput(false);
            txtExemptLegallyBlind.AllowOnlyNumericInput(false);
            txtExemptSewage.AllowOnlyNumericInput(false);
            txtExemptAnimalWasteStorage.AllowOnlyNumericInput(false);
            txtExemptPollutionControl.AllowOnlyNumericInput(false);
            txtExemptSnowmobileGroomingEquipment.AllowOnlyNumericInput(false);
            txtExemptSolarAndWindApproved.AllowOnlyNumericInput(false);
            txtExemptSolarAndWindProcessed.AllowOnlyNumericInput(false);
            txtExemptSolarAndWindValue.AllowOnlyNumericInput( false);

            txtExemptVetSurvivingMaleCount.AllowOnlyNumericInput(false);
            txtExemptVetSurvivingMaleValue.AllowOnlyNumericInput(false);
            txtExemptVetParaplegicLivingTrustValue.AllowOnlyNumericInput(false);
            txtExemptVetParaplegicLivingTrustCount.AllowOnlyNumericInput(false);
            txtExemptVetLivingTrustValue.AllowOnlyNumericInput(false);
            txtExemptVetLivingTrustCount.AllowOnlyNumericInput(false);
            txtExemptVetWWIResidentCount.AllowOnlyNumericInput(false);
            txtExemptVetWWIResidentValue.AllowOnlyNumericInput(false);
            txtExemptVetWWINonResidentCount.AllowOnlyNumericInput(false);
            txtExemptVetWWINonResidentValue.AllowOnlyNumericInput(false);
            txtExemptVetParaplegicCount.AllowOnlyNumericInput(false);
            txtExemptVetParaplegicValue.AllowOnlyNumericInput(false);
            txtExemptVetCoopCount.AllowOnlyNumericInput(false);
            txtExemptVetCoopValue.AllowOnlyNumericInput(false);
            txtExemptVetResidentCount.AllowOnlyNumericInput(false);
            txtExemptVetResidentValue.AllowOnlyNumericInput(false);
            txtExemptVetNonResidentCount.AllowOnlyNumericInput(false);
            txtExemptVetNonResidentValue.AllowOnlyNumericInput(false);
            txtExemptVetDisabledCount.AllowOnlyNumericInput(false);
            txtExemptVetDisabledValue.AllowOnlyNumericInput(false);
            txtExemptVetLebanonPanamaCount.AllowOnlyNumericInput(false);
            txtExemptVetLebanonPanamaValue.AllowOnlyNumericInput(false);
            txtExemptVetVietnamCount.AllowOnlyNumericInput(false);
            txtExemptVetVietnamValue.AllowOnlyNumericInput(false);
            
            txtMunicipalRecordsTotalTaxableAcreage.AllowOnlyNumericInput(true);
            txtMunicipalRecordsTotalNumberOfParcels.AllowOnlyNumericInput(false);
            txtRevaluationCost.AllowOnlyNumericInput(true);
            txtInterestRate.AllowOnlyNumericInput(true);

            txtAmountOfTaxRelief.AllowOnlyNumericInput(true);
            txtNumberQualifiedForTaxRelief.AllowOnlyNumericInput(false);
            txtAmountOfElderlyTaxCredit.AllowOnlyNumericInput(true);
            txtNumberQualifiedForElderlyTaxCredit.AllowOnlyNumericInput(false);
            txtAmountOfSeniorTaxDeferral.AllowOnlyNumericInput(true);
            txtNumberQualifiedForSeniorDeferral.AllowOnlyNumericInput(false);
        }


        private void frmMVRViewer_Load(object sender, System.EventArgs e)
        {
            SetupGridResidentialBuildings();
            SetupExciseCombo();
            SetupAssessmentCombo();
            SetupTaxMapCombo();
        }

		private void frmMVRViewer_Resize(object sender, System.EventArgs e)
		{
			ResizeGridResidentialBuildings();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

        private void SetupGridResidentialBuildings()
        {
            GridResidentialBuildings.Rows = 7;
            GridResidentialBuildings.Cols = 7;
            GridResidentialBuildings.TextMatrix(0, (int) ValuationGridColumn.OneFamily, "One Family");
            GridResidentialBuildings.TextMatrix(0, (int) ValuationGridColumn.TwoFamily, "Two Family");
            GridResidentialBuildings.TextMatrix(0, (int) ValuationGridColumn.ThreeToFourFamily, "3-4 Family");
            GridResidentialBuildings.TextMatrix(0, (int) ValuationGridColumn.FiveFamily, "5 Family Plus");
            GridResidentialBuildings.TextMatrix(0, (int) ValuationGridColumn.MobileHomes, "Mobile Homes");
            GridResidentialBuildings.TextMatrix(0, (int) ValuationGridColumn.SeasonalHomes, "Seasonal Homes");
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.New, 0, "New");
            GridResidentialBuildings.TextMatrix((int) ValuationGridRow.Demolished, 0, "Demolished");
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.Converted, 0, "Converted");
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, 0, "Valuation Increase +");
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, 0, "Valuation Loss -");
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.NetChange, 0, "Net increase/loss");
            GridResidentialBuildings.SetColReadOnly(0, true);
            GridResidentialBuildings.SetRowReadOnly((int)ValuationGridRow.NetChange, true);
        }

        private void SetupExciseCombo()
        {
            cmbExcisePeriodType.Items.Clear();
            cmbExcisePeriodType.AddItem("Calendar");
            cmbExcisePeriodType.AddItem("Fiscal");
        }

        private void SetupAssessmentCombo()
        {
            cmbAssessorFunctionality.Items.Clear();
            cmbAssessorFunctionality.Items.Add(new GenericDescriptionPair<MVRMunicipalityAssessmentFunction>()
                {Description = "", ID = MVRMunicipalityAssessmentFunction.None});
            cmbAssessorFunctionality.Items.Add(new GenericDescriptionPair<MVRMunicipalityAssessmentFunction>()
                {Description = "SINGLE ASSESSOR", ID = MVRMunicipalityAssessmentFunction.SingleAssessor});
            cmbAssessorFunctionality.Items.Add(new GenericDescriptionPair<MVRMunicipalityAssessmentFunction>()
                {Description = "ASSESSORS AGENT", ID =  MVRMunicipalityAssessmentFunction.AssessorsAgent});
            cmbAssessorFunctionality.Items.Add(new GenericDescriptionPair<MVRMunicipalityAssessmentFunction>()
                {Description = "BOARD OF ASSESSORS", ID =  MVRMunicipalityAssessmentFunction.BoardOfAssessors});
            cmbAssessorFunctionality.DisplayMember = "Description";
            cmbAssessorFunctionality.ValueMember = "ID";
        }

        private void SetupTaxMapCombo()
        {
            cmbTaxMapType.Items.Clear();
            cmbTaxMapType.Items.Add(new GenericDescriptionPair<MVRTaxMapType>()
            {
                Description = "",
                ID = MVRTaxMapType.None
            });
            cmbTaxMapType.Items.Add(new GenericDescriptionPair<MVRTaxMapType>()
            {
                Description =  MVRTaxMapType.Paper.ToString(), ID = MVRTaxMapType.Paper
            });
            cmbTaxMapType.Items.Add(new GenericDescriptionPair<MVRTaxMapType>()
            {
                Description = MVRTaxMapType.GIS.ToString(), ID = MVRTaxMapType.GIS
            });
            cmbTaxMapType.Items.Add(new GenericDescriptionPair<MVRTaxMapType>()
            {
                Description = MVRTaxMapType.CAD.ToString(),
                ID = MVRTaxMapType.CAD
            });
            cmbTaxMapType.DisplayMember = "Description";
            cmbTaxMapType.ValueMember = "ID";
        }
        private void ResizeGridResidentialBuildings()
        {
            var originalWidth = GridResidentialBuildings.WidthOriginal;
            var colWidth = (Convert.ToDouble(originalWidth) / 8).ToInteger();
            GridResidentialBuildings.ColWidth(0, colWidth * 2);
            GridResidentialBuildings.ColWidth(1, colWidth);
            GridResidentialBuildings.ColWidth(2, colWidth);
            GridResidentialBuildings.ColWidth(3, colWidth);
            GridResidentialBuildings.ColWidth(4, colWidth);
            GridResidentialBuildings.ColWidth(5, colWidth);
            GridResidentialBuildings.ColWidth(6, colWidth);
        }
		
		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

        public IMunicipalValuationReturnViewModel ViewModel { get; set; }

        private void ShowData()
        {
            var mvr = ViewModel.ValuationReturn;            
            txtCounty.Text = mvr.County;
            txtMunicipality.Text = mvr.Municipality;
            txtCertifiedRatio.Text = mvr.CertifiedRatio.ToString();
            if (mvr.CommitmentDate != DateTime.MinValue)
            {
                dtCommitmentDate.Value = mvr.CommitmentDate;
            }
            else
            {
                dtCommitmentDate.NullableValue = null;

            }
            ShowRealEstateData(mvr);
            ShowPersonalPropertyData(mvr);
            ShowHomesteadData(mvr);
            ShowOtherTax(mvr);
            ShowBete(mvr);
            ShowTif(mvr);
            ShowExcise(mvr);
            ShowElectricalGeneration(mvr);
            ShowTreeGrowth(mvr);
            ShowFarmAndOpenSpace(mvr);
            ShowWaterfront(mvr);
            ShowExemptProperty(mvr);
            ShowVeteranExemptions(mvr);
            ShowMunicipalRecords(mvr);
            ShowValuationInformation(mvr);
        }

        private void RecalculatePropertyTaxTotals()
        {
            var totalRE =
                txtTaxableBuilding.Text.ToIntegerValue() + txtTaxableLand.Text.ToIntegerValue();
            var totalPP = txtOtherPersonalProperty.Text.ToIntegerValue() + txtBusinessEquipment.Text.ToIntegerValue() +
                          txtMachineryAndEquipment.Text.ToIntegerValue();
            var totalProperty = totalRE + totalPP;
            txtTotalTaxableValuation.Text = totalProperty.FormatAsNumber();
            txtTotalTaxableRealEstate.Text = totalRE.FormatAsNumber();
            txtTotalTaxablePersonalProperty.Text = totalPP.FormatAsNumber();
        }
        private void ShowRealEstateData(MunicipalValuationReturn mvr)
        {
            txtTaxableLand.Text = mvr.RealEstate.TaxableLand.FormatAsNumber();
            txtTaxableBuilding.Text = mvr.RealEstate.TaxableBuilding.FormatAsNumber();
            txtTotalTaxableRealEstate.Text = mvr.RealEstate.TaxableRealEstate.FormatAsNumber();
        }

        private void ShowPersonalPropertyData(MunicipalValuationReturn mvr)
        {
            txtOtherPersonalProperty.Text = mvr.PersonalProperty.OtherPersonalProperty.FormatAsNumber();
            txtBusinessEquipment.Text = mvr.PersonalProperty.BusinessEquipment.FormatAsNumber();
            txtMachineryAndEquipment.Text = mvr.PersonalProperty.ProductionMachineryAndEquipment.FormatAsNumber();
            txtTotalTaxablePersonalProperty.Text = mvr.PersonalProperty.TaxablePersonalProperty.FormatAsNumber();
        }

        private void ShowOtherTax(MunicipalValuationReturn mvr)
        {
            txtTaxLevy.Text = mvr.OtherTaxInformation.TaxLevy.FormatAsCurrencyNoSymbol();
            txtPropertyTaxRate.Text = mvr.OtherTaxInformation.TaxRate.ToString();
            txtTotalTaxableValuation.Text = (mvr.PersonalProperty.TaxablePersonalProperty + mvr.RealEstate.TaxableRealEstate)
                .FormatAsNumber();
        }

        private void ShowHomesteadData(MunicipalValuationReturn mvr)
        {
            txtTotalFullHomesteadsGranted.Text = mvr.Homestead.NumberOfFullHomesteadsGranted.FormatAsNumber();
            txtTotalFullHomesteadExemptions.Text = (mvr.Homestead.TotalValueOfFullHomesteadExemptions).ToInteger().FormatAsNumber();
            txtNumberHomesteadFullyExempt.Text = mvr.Homestead.NumberOfFullyExemptedHomesteads.FormatAsNumber();
            txtHomesteadFullyExemptValue.Text = mvr.Homestead.TotalValueOfFullyExemptedHomesteads.ToInteger().FormatAsNumber();
            txtTotalNumberHomesteads.Text = mvr.Homestead.TotalNumberOfHomesteadExemptions.FormatAsNumber();
            txtTotalHomesteadAssessments.Text = mvr.Homestead.TotalAssessedValueOfAllHomesteadProperty.FormatAsNumber();
            txtHomesteadTotalExemptions.Text = mvr.Homestead.TotalExemptValueOfAllHomesteads.ToInteger().FormatAsNumber();
        }

        private void ShowBete(MunicipalValuationReturn mvr)
        {
            txtBeteInTIF.Text = mvr.Bete.TotalBeteInTIF.FormatAsNumber();
            txtExemptValueBeteQualified.Text = mvr.Bete.TotalBeteQualifiedExempt.FormatAsNumber();
            txtNumberBeteApplicationsProcessed.Text = mvr.Bete.ApplicationsProcessed.FormatAsNumber();
            txtNumberBeteApproved.Text = mvr.Bete.ApplicationsApproved.FormatAsNumber();
        }

        private void ShowTif(MunicipalValuationReturn mvr)
        {
            txtTifBeteDeposited.Text = mvr.TIF.BeteRevenueDeposited.FormatAsCurrencyNoSymbol();
            txtTifCaptured.Text = mvr.TIF.CapturedAssessedValue.FormatAsNumber();
            txtTifDeposited.Text = mvr.TIF.RevenueDeposited.FormatAsCurrencyNoSymbol();
            txtIncreasedTif.Text = mvr.TIF.TifIncrease.FormatAsNumber();
        }

        private void ShowExcise(MunicipalValuationReturn mvr)
        {
            if (mvr.Excise.Fiscal)
            {
                cmbExcisePeriodType.Text = "Fiscal";
            }
            else
            {
                cmbExcisePeriodType.Text = "Calendar";
            }

            txtMVExcise.Text = mvr.Excise.MVExcise.FormatAsCurrencyNoSymbol();
            txtWatercraftExcise.Text = mvr.Excise.WatercraftExcise.FormatAsCurrencyNoSymbol();
        }

        private void ShowElectricalGeneration(MunicipalValuationReturn mvr)
        {
            txtTransmissionLines.Text = mvr.ElectricalGeneration.DistributionAndTransmissionLines.FormatAsNumber();
            txtElectricalGenerationFacilities.Text = mvr.ElectricalGeneration.GenerationFacilities.FormatAsNumber();
        }

        private void ShowTreeGrowth(MunicipalValuationReturn mvr)
        {
            txtForestAveragePerAcre.Text = mvr.ForestTreeGrowth.AveragePerAcreUnitValue.ToString();
            txtForestHardwoodAcreage.Text = mvr.ForestTreeGrowth.HardwoodAcreage.FormatAsNumber(2);
            txtForestHardwoodRate.Text = mvr.ForestTreeGrowth.HardwoodRate.ToString();
            txtForestMixedWoodAcreage.Text = mvr.ForestTreeGrowth.MixedwoodAcreage.FormatAsNumber(2);
            txtForestMixedwoodRate.Text = mvr.ForestTreeGrowth.MixedWoodRate.ToString();
            txtForestNumberOfAcres.Text = mvr.ForestTreeGrowth.TotalAcres.FormatAsNumber(2);
            txtForestNumberOfParcels.Text = mvr.ForestTreeGrowth.NumberOfParcelsClassified.FormatAsNumber();
            txtForestSoftwoodAcreage.Text = mvr.ForestTreeGrowth.SoftwoodAcreage.FormatAsNumber(2);
            txtForestSoftwoodRate.Text = mvr.ForestTreeGrowth.SoftwoodRate.ToString();
            txtForestTotalAssessed.Text = mvr.ForestTreeGrowth.TotalAssessedValuation.ToInteger().FormatAsNumber();

            txtForestAcresFirstClassifiedThisYear.Text =
                mvr.ForestTreeGrowth.NumberOfAcresFirstClassifiedForTaxYear.ToInteger().FormatAsNumber();
            txtForestParcelsWithdrawn.Text = mvr.ForestTreeGrowth.NumberOfParcelsWithdrawn.FormatAsNumber();
            txtForestAcresWithdrawn.Text = mvr.ForestTreeGrowth.NumberOfAcresWithdrawn.FormatAsCurrencyNoSymbol();
            txtForestAmountOfPenalties.Text = mvr.ForestTreeGrowth.TotalAmountOfPenaltiesAssessed.FormatAsCurrencyNoSymbol();
            txtForestNumberOfNonCompliancePenalties.Text =
                mvr.ForestTreeGrowth.TotalNumberOfNonCompliancePenalties.FormatAsNumber();
            chkForestTransferredToFarmland.Checked =
                mvr.ForestTreeGrowth.TreeGrowthHasBeenTransferredToFarmlandThisYear;
        }

        private void ShowFarmAndOpenSpace(MunicipalValuationReturn mvr)
        {
            txtFarmNumberOfParcelsFirstClassified.Text =
                mvr.FarmAndOpenSpace.NumberOfParcelsClassifiedAsFarmland.FormatAsNumber();
            txtFarmNumberOfAcresFirstClassified.Text =
                mvr.FarmAndOpenSpace.NumberOfAcresClassifiedAsFarmlandForTaxYear.FormatAsNumber(2);
            txtFarmTotalAcresTillable.Text = mvr.FarmAndOpenSpace.TotalNumberOfAcresOfAllFarmland.FormatAsNumber(2);
            txtFarmSoftwoodAcres.Text = mvr.FarmAndOpenSpace.NumberOfFarmSoftwoodAcres.FormatAsNumber(2);
            txtFarmMixedWoodAcres.Text = mvr.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres.FormatAsNumber(2);
            txtFarmHardwoodAcres.Text = mvr.FarmAndOpenSpace.NumberOfFarmHardwoodAcres.FormatAsNumber(2);
            txtFarmTotalAcresWoodland.Text = mvr.FarmAndOpenSpace.TotalNumberOfFarmWoodlandAcres.FormatAsNumber(2);
            txtFarmTotalValuationWoodland.Text = mvr.FarmAndOpenSpace.TotalValuationOfFarmWoodland.ToInteger().FormatAsNumber();
            txtFarmTotalValuationTillable.Text = mvr.FarmAndOpenSpace.TotalValuationOfAllFarmland.ToInteger().FormatAsNumber();
            txtFarmSoftwoodRate.Text = mvr.FarmAndOpenSpace.FarmSoftwoodRate.ToString();
            txtFarmMixedWoodRate.Text = mvr.FarmAndOpenSpace.FarmMixedWoodRate.ToString();
            txtFarmHardwoodRate.Text = mvr.FarmAndOpenSpace.FarmHardwoodRate.ToString();
            txtFarmParcelsWithdrawn.Text = mvr.FarmAndOpenSpace.TotalNumberOfFarmlandParcelsWithdrawn.FormatAsNumber();
            txtFarmAcresWithdrawn.Text = mvr.FarmAndOpenSpace.TotalNumberOfFarmlandAcresWithdrawn.FormatAsNumber(2);
            txtFarmTotalPenalties.Text = mvr.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnFarmland.FormatAsCurrencyNoSymbol();
            txtOpenParcelsFirstClassified.Text =
                mvr.FarmAndOpenSpace.NumberOfOpenSpaceParcelsFirstClassified.FormatAsNumber();
            txtOpenAcresFirstClassified.Text =
                mvr.FarmAndOpenSpace.NumberOfOpenSpaceAcresFirstClassified.FormatAsNumber(2);
            txtOpenTotalAcres.Text = mvr.FarmAndOpenSpace.TotalAcresOfOpenSpaceFirstClassified.FormatAsNumber(2);
            txtOpenTotalValuation.Text = mvr.FarmAndOpenSpace.TotalValuationOfAllOpenSpace.ToInteger().FormatAsNumber();
            txtOpenParcelsWithdrawn.Text = mvr.FarmAndOpenSpace.TotalNumberOfOpenSpaceParcelsWithdrawn.FormatAsNumber();
            txtOpenAcresWithdrawn.Text = mvr.FarmAndOpenSpace.TotalNumberOfOpenSpaceAcresWithdrawn.FormatAsNumber(2);
            txtOpenTotalAmountOfPenalties.Text =
                mvr.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnOpenSpace.FormatAsCurrencyNoSymbol();
        }

        private void ShowWaterfront(MunicipalValuationReturn mvr)

        {
            txtWaterfrontParcelsFirstClassified.Text = mvr.Waterfront.NumberOfParcelsFirstClassified.FormatAsNumber();
            txtWaterfrontAcresFirstClassified.Text = mvr.Waterfront.NumberOfAcresFirstClassified.FormatAsNumber(2);
            txtWaterfrontTotalAcreage.Text = mvr.Waterfront.TotalAcresOfWaterfront.FormatAsNumber(2);
            txtWaterfrontTotalValuation.Text = mvr.Waterfront.TotalValuationOfWaterfront.ToInteger().FormatAsNumber();
            txtWaterfrontParcelsWithdrawn.Text = mvr.Waterfront.NumberOfWaterfrontParcelsWithdrawn.FormatAsNumber();
            txtWaterfrontAcresWithdrawn.Text = mvr.Waterfront.NumberOfWaterfrontAcresWithdrawn.FormatAsNumber(2);
            txtWaterfrontPenalties.Text = mvr.Waterfront.TotalAmountOfPenalties.FormatAsCurrencyNoSymbol();
        }

        private void ShowExemptProperty(MunicipalValuationReturn mvr)
        {
            txtExemptFederal.Text = mvr.ExemptProperty.UnitedStates.FormatAsNumber(0);
            txtExemptMaine.Text = mvr.ExemptProperty.StateOfMaine.FormatAsNumber(0);
            txtExemptNHWater.Text = mvr.ExemptProperty.NewhampshireWater.FormatAsNumber(0);
            txtExemptMunicipal.Text = mvr.ExemptProperty.Municipal.FormatAsNumber(0);
            txtExemptMunicipalUtility.Text = mvr.ExemptProperty.MunicipalUtilities.FormatAsNumber(0);
            txtExemptMunicipalAirport.Text = mvr.ExemptProperty.MunicipalAirport.FormatAsNumber(0);
            txtExemptPrivateAirport.Text = mvr.ExemptProperty.PrivateAirport.FormatAsNumber(0);
            txtExemptSewage.Text = mvr.ExemptProperty.MunicipalSewage.FormatAsNumber(0);
            txtExemptBenevolent.Text = mvr.ExemptProperty.Benevolent.FormatAsNumber(0);
            txtExemptLiteraryAndScientific.Text = mvr.ExemptProperty.LiteraryAndScientific.FormatAsNumber(0);
            txtExemptVeteransOrganizations.Text = mvr.ExemptProperty.VeteranOrganizations.FormatAsNumber(0);
            txtExemptVetOrgReimbursable.Text = mvr.ExemptProperty.VeteranOrgReimbursableExemption.FormatAsNumber(0);
            txtExemptChamberOfCommerce.Text = mvr.ExemptProperty.ChamberOfCommerce.FormatAsNumber(0);
            txtExemptParsonages.Text = mvr.ExemptProperty.NumberOfParsonages.FormatAsNumber();
            txtExemptValueOfParsonages.Text = mvr.ExemptProperty.ExemptValueOfParsonages.FormatAsNumber(0);
            txtExemptTaxableValueOfParsonages.Text = mvr.ExemptProperty.TaxableValueOfParsonages.FormatAsNumber(0);
            txtExemptReligiousWorship.Text = mvr.ExemptProperty.ReligiousWorship.FormatAsNumber(0);
            txtExemptFraternal.Text = mvr.ExemptProperty.FraternalOrganizations.FormatAsNumber(0);
            txtExemptHospitalPersonalProperty.Text = mvr.ExemptProperty.HospitalPersonalProperty.FormatAsNumber(0);
            txtExemptLegallyBlind.Text = mvr.ExemptProperty.LegallyBlind.FormatAsNumber(0);
            txtExemptWaterSupplyTransport.Text = mvr.ExemptProperty.WaterSupplyTransport.FormatAsNumber(0);
            txtExemptAnimalWasteStorage.Text = mvr.ExemptProperty.CertifiedAnimalWasteStorage.FormatAsNumber(0);
            txtExemptPollutionControl.Text = mvr.ExemptProperty.PollutionControlFacilities.FormatAsNumber(0);
            txtExemptSnowmobileGroomingEquipment.Text = mvr.ExemptProperty.SnowmobileGroomingEquipment.FormatAsNumber(0);
            txtExemptSolarAndWindProcessed.Text = mvr.ExemptProperty.SolarAndWindApplicationsProcessed.FormatAsNumber();
            txtExemptSolarAndWindApproved.Text = mvr.ExemptProperty.SolarAndWindApplicationsApproved.FormatAsNumber();
            txtExemptSolarAndWindValue.Text = mvr.ExemptProperty.SolarAndWindEquipment.FormatAsNumber(0);
            if (!string.IsNullOrWhiteSpace(mvr.ExemptProperty.ExemptOrganizations[0].Name))
            {
                txtExemptOrganization1Name.Text = mvr.ExemptProperty.ExemptOrganizations[0].Name;
                txtExemptProvisionOfLaw1.Text = mvr.ExemptProperty.ExemptOrganizations[0].ProvisionOfLaw;
                txtExemptOrganization1ExemptValue.Text =
                    mvr.ExemptProperty.ExemptOrganizations[0].ExemptValue.FormatAsNumber(0);
            }

            if (!string.IsNullOrWhiteSpace(mvr.ExemptProperty.ExemptOrganizations[1].Name))
            {
                txtExemptOrganization2Name.Text = mvr.ExemptProperty.ExemptOrganizations[1].Name;
                txtExemptProvisionOfLaw2.Text = mvr.ExemptProperty.ExemptOrganizations[1].ProvisionOfLaw;
                txtExemptOrganization2ExemptValue.Text =
                    mvr.ExemptProperty.ExemptOrganizations[1].ExemptValue.FormatAsNumber(0);
            }

            if (!string.IsNullOrWhiteSpace(mvr.ExemptProperty.ExemptOrganizations[2].Name))
            {
                txtExemptOrganization3Name.Text = mvr.ExemptProperty.ExemptOrganizations[2].Name;
                txtExemptProvisionOfLaw3.Text = mvr.ExemptProperty.ExemptOrganizations[2].ProvisionOfLaw;
                txtExemptOrganization3ExemptValue.Text =
                    mvr.ExemptProperty.ExemptOrganizations[2].ExemptValue.FormatAsNumber(0);
            }

            //if (!string.IsNullOrWhiteSpace(mvr.ExemptProperty.ExemptOrganizations[3].Name))
            //{
            //    txtExemptOrganization4Name.Text = mvr.ExemptProperty.ExemptOrganizations[3].Name;
            //    txtExemptProvisionOfLaw4.Text = mvr.ExemptProperty.ExemptOrganizations[3].ProvisionOfLaw;
            //    txtExemptOrganization4ExemptValue.Text =
            //        mvr.ExemptProperty.ExemptOrganizations[3].ExemptValue.ToString();
            //}
        }

        private void ShowVeteranExemptions(MunicipalValuationReturn mvr)
        {
            txtExemptVetSurvivingMaleCount.Text = mvr.VeteranExemptions.SurvivingMale.Count.FormatAsNumber();
            txtExemptVetSurvivingMaleValue.Text = mvr.VeteranExemptions.SurvivingMale.ExemptValue.FormatAsNumber(0);
            txtExemptVetParaplegicLivingTrustCount.Text =
                mvr.VeteranExemptions.ParaplegicRevocableTrusts.Count.FormatAsNumber();
            txtExemptVetParaplegicLivingTrustValue.Text =
                mvr.VeteranExemptions.ParaplegicRevocableTrusts.ExemptValue.FormatAsNumber(0);
            txtExemptVetLivingTrustCount.Text = mvr.VeteranExemptions.MiscRevocableTrust.Count.FormatAsNumber();
            txtExemptVetLivingTrustValue.Text = mvr.VeteranExemptions.MiscRevocableTrust.ExemptValue.FormatAsNumber(0);
            txtExemptVetWWIResidentCount.Text = mvr.VeteranExemptions.MaineEnlistedWWIVeteran.Count.FormatAsNumber();
            txtExemptVetWWIResidentValue.Text = mvr.VeteranExemptions.MaineEnlistedWWIVeteran.ExemptValue.FormatAsNumber(0);
            txtExemptVetWWINonResidentCount.Text = mvr.VeteranExemptions.NonResidentWWIVeteran.Count.FormatAsNumber();
            txtExemptVetWWINonResidentValue.Text = mvr.VeteranExemptions.NonResidentWWIVeteran.ExemptValue.FormatAsNumber(0);
            txtExemptVetParaplegicCount.Text = mvr.VeteranExemptions.ParaplegicVeteran.Count.FormatAsNumber();
            txtExemptVetParaplegicValue.Text = mvr.VeteranExemptions.ParaplegicVeteran.ExemptValue.FormatAsNumber(0);
            txtExemptVetCoopCount.Text = mvr.VeteranExemptions.COOPHousing.Count.FormatAsNumber();
            txtExemptVetCoopValue.Text = mvr.VeteranExemptions.COOPHousing.ExemptValue.FormatAsNumber(0);
            txtExemptVetResidentCount.Text = mvr.VeteranExemptions.MiscMaineEnlisted.Count.FormatAsNumber();
            txtExemptVetResidentValue.Text = mvr.VeteranExemptions.MiscMaineEnlisted.ExemptValue.FormatAsNumber(0);
            txtExemptVetNonResidentCount.Text = mvr.VeteranExemptions.MiscNonResident.Count.FormatAsNumber();
            txtExemptVetNonResidentValue.Text = mvr.VeteranExemptions.MiscNonResident.ExemptValue.FormatAsNumber(0);
            txtExemptVetDisabledCount.Text = mvr.VeteranExemptions.DisabledInLineOfDuty.Count.FormatAsNumber();
            txtExemptVetDisabledValue.Text = mvr.VeteranExemptions.DisabledInLineOfDuty.ExemptValue.FormatAsNumber(0);
            txtExemptVetLebanonPanamaCount.Text = mvr.VeteranExemptions.LebanonPanama.Count.FormatAsNumber();
            txtExemptVetLebanonPanamaValue.Text = mvr.VeteranExemptions.LebanonPanama.ExemptValue.FormatAsNumber(0);
            txtExemptVetVietnamCount.Text = mvr.VeteranExemptions.EarlyVietnamConflict.Count.FormatAsNumber();
            txtExemptVetVietnamValue.Text = mvr.VeteranExemptions.EarlyVietnamConflict.ExemptValue.FormatAsNumber(0);            
        }

        private void ShowMunicipalRecords(MunicipalValuationReturn mvr)
        {
            chkHasTaxMaps.Checked = mvr.MunicipalRecords.HasTaxMaps;
            dtMapsOriginallyObtained.NullableValue = mvr.MunicipalRecords.DateMapsOriginallyObtained;
            txtNameOfOriginalContractor.Text = mvr.MunicipalRecords.NameOfOriginalContractor;
            txtMunicipalRecordsTotalNumberOfParcels.Text =
                mvr.MunicipalRecords.NumberOfParcelsInMunicipality.FormatAsNumber();
            txtMunicipalRecordsTotalTaxableAcreage.Text = mvr.MunicipalRecords.TotalTaxableAcreage.FormatAsNumber(2);
            chkRevaluationCompleted.Checked = mvr.MunicipalRecords.ProfessionalRevaluationWasCompleted;
            chkRevaluationIncludedLand.Checked = mvr.MunicipalRecords.RevaluationIncludedLand;
            chkRevaluationIncludedBuildings.Checked = mvr.MunicipalRecords.RevaluationIncludedBuildings;
            chkRevaluationIncludedPersonalProperty.Checked = mvr.MunicipalRecords.RevaluationIncludedPersonalProperty;
            dtRevaluationEffectiveDate.NullableValue = mvr.MunicipalRecords.RevaluationEffectiveDate;
            txtRevaluationContractorName.Text = mvr.MunicipalRecords.RevaluationContractorName;
            txtRevaluationCost.Text = mvr.MunicipalRecords.RevaluationCost.FormatAsMoney();
            dtpSignatureDate.NullableValue = mvr.MunicipalRecords.SignatureDate;
            SetAssessmentCombo(mvr.MunicipalRecords.AssessmentFunction);
            txtAssessorName.Text = mvr.MunicipalRecords.AssessorOrAgentName;
            txtAssessorEmail.Text = mvr.MunicipalRecords.AssessorOrAgentEmail;
            dtpFiscalStart.NullableValue = mvr.MunicipalRecords.FiscalYearStart;
            dtpFiscalEnd.NullableValue = mvr.MunicipalRecords.FiscalYearEnd;
            txtInterestRate.Text = mvr.MunicipalRecords.OverDueInterestRate.ToString();
            dtpPeriod1Due.NullableValue = mvr.MunicipalRecords.Period1DueDate;
            dtpPeriod2Due.NullableValue = mvr.MunicipalRecords.Period2DueDate;
            dtpPeriod3Due.NullableValue = mvr.MunicipalRecords.Period3DueDate;
            dtpPeriod4Due.NullableValue = mvr.MunicipalRecords.Period4DueDate;
            chkAssessmentRecordsAreComputerized.Checked = mvr.MunicipalRecords.AssessmentRecordsAreComputerized;
            txtAssessmentSoftware.Text = mvr.MunicipalRecords.AssessmentSoftware;
            SetTaxMapCombo(mvr.MunicipalRecords.TaxMapType);
            chkTaxReliefProgram.Checked = mvr.MunicipalRecords.ImplementedATaxReliefProgram;
            txtNumberQualifiedForTaxRelief.Text = mvr.MunicipalRecords.NumberOfPeopleQualifiedForTaxProgram.FormatAsNumber();
            txtAmountOfTaxRelief.Text = mvr.MunicipalRecords.AmountOfReliefGrantedForTaxProgram.FormatAsNumber(0);
            chkElderlyTaxCredit.Checked = mvr.MunicipalRecords.ImplementedElderlyTaxCredit;
            txtNumberQualifiedForElderlyTaxCredit.Text =
                mvr.MunicipalRecords.NumberOfPeopleQualifiedForElderlyCredit.FormatAsNumber();
            txtAmountOfElderlyTaxCredit.Text = mvr.MunicipalRecords.AmountOfReliefGrantedForElderlyCredit.FormatAsCurrencyNoSymbol();
            chkSeniorTaxDeferral.Checked = mvr.MunicipalRecords.ImplementedSeniorTaxDeferral;
            txtNumberQualifiedForSeniorDeferral.Text =
                mvr.MunicipalRecords.NumberOfPeopleQualifiedForSeniorTaxDeferral.FormatAsNumber();
            txtAmountOfSeniorTaxDeferral.Text =
                mvr.MunicipalRecords.AmountOfReliefGrantedForSeniorTaxDeferral.FormatAsCurrencyNoSymbol();
        }

        private void SetAssessmentCombo(MVRMunicipalityAssessmentFunction assessmentFunction)
        {
	        foreach (GenericDescriptionPair<MVRMunicipalityAssessmentFunction> idPair in cmbAssessorFunctionality.Items)
            {
                if (idPair.ID ==  assessmentFunction)
                {
                    cmbAssessorFunctionality.SelectedItem = idPair;
                    return;
                }
            }
        }

        private void SetTaxMapCombo(MVRTaxMapType taxMapType)
        {
            foreach (GenericDescriptionPair<MVRTaxMapType> idPair in cmbTaxMapType.Items)
            {
                if (idPair.ID == taxMapType)
                {
                    cmbTaxMapType.SelectedItem = idPair;
                    return;
                }
            }
        }

        private void ShowValuationInformation(MunicipalValuationReturn mvr)
        {
            ShowGridStat((int)ValuationGridRow.New,mvr.ValuationInformation.New);
            ShowGridStat((int)ValuationGridRow.Converted,mvr.ValuationInformation.Converted);
            ShowGridStat((int)ValuationGridRow.Demolished,mvr.ValuationInformation.Demolished);
            ShowGridValues((int)ValuationGridRow.ValuationIncrease,mvr.ValuationInformation.ValuationIncrease);
            ShowGridValues((int)ValuationGridRow.ValuationLoss,mvr.ValuationInformation.ValuationDecrease);
            ShowIndustrialGrowth(mvr.ValuationInformation.IndustrialMercantileGrowth);
            ShowExtremeLosses(mvr.ValuationInformation.ExtremeLosses);
            ShowGeneralValuationChange(mvr.ValuationInformation.GeneralValuationChange);
        }


        private void ShowGridStat(int row, ValuationInformationStats stats)
        {
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.OneFamily, stats.OneFamily.FormatAsNumber());
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.TwoFamily, stats.TwoFamily.FormatAsNumber());
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.ThreeToFourFamily,
                stats.ThreeToFourFamily.FormatAsNumber());
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.FiveFamily,
                stats.FiveFamilyPlus.FormatAsNumber());
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.MobileHomes,
                stats.MobileHomes.FormatAsNumber());
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.SeasonalHomes,
                stats.SeasonalHomes.FormatAsNumber());
        }

        private ValuationInformationStats GetGridStat(ValuationGridRow valuationGridRow)
        {
            int row = (int) valuationGridRow;
            return new ValuationInformationStats()
            {
                OneFamily = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.OneFamily).ToIntegerValue(),
                TwoFamily = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.TwoFamily).ToIntegerValue(),
                ThreeToFourFamily =  GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.ThreeToFourFamily).ToIntegerValue(),
                FiveFamilyPlus = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.FiveFamily).ToIntegerValue(),
                MobileHomes = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.MobileHomes).ToIntegerValue(),
                SeasonalHomes = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.SeasonalHomes).ToIntegerValue()
            };
        }

        private void ShowGridValues(int row, ValuationInformationValues values)
        {
            GridResidentialBuildings.TextMatrix(row, (int) ValuationGridColumn.OneFamily, values.OneFamily.FormatAsNumber(0));
            GridResidentialBuildings.TextMatrix(row, (int)ValuationGridColumn.TwoFamily, values.TwoFamily.FormatAsNumber(0));
            GridResidentialBuildings.TextMatrix(row, (int)ValuationGridColumn.ThreeToFourFamily,
                values.ThreeToFourFamily.FormatAsNumber(0));
            GridResidentialBuildings.TextMatrix(row, (int)ValuationGridColumn.FiveFamily,
                values.FiveFamilyPlus.FormatAsNumber(0));
            GridResidentialBuildings.TextMatrix(row, (int)ValuationGridColumn.MobileHomes,
                values.MobileHomes.FormatAsNumber(0));
            GridResidentialBuildings.TextMatrix(row, (int)ValuationGridColumn.SeasonalHomes,
                values.SeasonalHomes.FormatAsNumber(0));
            RecalculateGridValues();
        }

        private ValuationInformationValues GetGridValues(ValuationGridRow valuationGridRow)
        {
            var row = (int) valuationGridRow;            
            return new ValuationInformationValues()
            {
                OneFamily = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.OneFamily).ToDecimalValue(),
                TwoFamily = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.TwoFamily).ToDecimalValue(),
                ThreeToFourFamily = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.ThreeToFourFamily).ToDecimalValue(),
                FiveFamilyPlus = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.FiveFamily).ToDecimalValue(),
                MobileHomes = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.MobileHomes).ToDecimalValue(),
                SeasonalHomes = GridResidentialBuildings.TextMatrix(row,(int)ValuationGridColumn.SeasonalHomes).ToDecimalValue()
            };
        }

        private void RecalculateGridValues()
        {
            var net = GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, (int)ValuationGridColumn.OneFamily).ToString().ToDecimalValue()
                - GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, (int)ValuationGridColumn.OneFamily).ToString()
                .ToDecimalValue();
            GridResidentialBuildings.TextMatrix((int) ValuationGridRow.NetChange, (int) ValuationGridColumn.OneFamily,
                net.FormatAsNumber(0));
            net = GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, (int)ValuationGridColumn.TwoFamily).ToString().ToDecimalValue()
                      - GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, (int)ValuationGridColumn.TwoFamily).ToString()
                          .ToDecimalValue();
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.NetChange, (int)ValuationGridColumn.TwoFamily,
                net.FormatAsNumber(0));

            net = GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, (int)ValuationGridColumn.ThreeToFourFamily).ToString().ToDecimalValue()
                  - GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, (int)ValuationGridColumn.ThreeToFourFamily).ToString()
                      .ToDecimalValue();
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.NetChange, (int)ValuationGridColumn.ThreeToFourFamily,
                net.FormatAsNumber(0));

            net = GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, (int)ValuationGridColumn.FiveFamily).ToString().ToDecimalValue()
                  - GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, (int)ValuationGridColumn.FiveFamily).ToString()
                      .ToDecimalValue();
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.NetChange, (int)ValuationGridColumn.FiveFamily,
                net.FormatAsNumber(0));

            net = GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, (int)ValuationGridColumn.MobileHomes).ToString().ToDecimalValue()
                  - GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, (int)ValuationGridColumn.MobileHomes).ToString()
                      .ToDecimalValue();
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.NetChange, (int)ValuationGridColumn.MobileHomes,
                net.FormatAsNumber(0));

            net = GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationIncrease, (int)ValuationGridColumn.SeasonalHomes).ToString().ToDecimalValue()
                  - GridResidentialBuildings.TextMatrix((int)ValuationGridRow.ValuationLoss, (int)ValuationGridColumn.SeasonalHomes).ToString()
                      .ToDecimalValue();
            GridResidentialBuildings.TextMatrix((int)ValuationGridRow.NetChange, (int)ValuationGridColumn.SeasonalHomes,
                net.FormatAsNumber(0));
        }


        private void ShowIndustrialGrowth(List<string> lines)
        {
            txtIndustrialGrowthLine1.Text = lines[0];
            txtIndustrialGrowthLine2.Text = lines[1];
            txtIndustrialGrowthLine3.Text = lines[2];
            txtIndustrialGrowthLine4.Text = lines[3];
            txtIndustrialGrowthLine5.Text = lines[4];
            txtIndustrialGrowthLine6.Text = lines[5];
            txtIndustrialGrowthLine7.Text = lines[6];
            txtIndustrialGrowthLine8.Text = lines[7];
        }

        private List<string> GetIndustrialGrowth()
        {
            return new List<string>()
            {
                txtIndustrialGrowthLine1.Text,
                txtIndustrialGrowthLine2.Text,
                txtIndustrialGrowthLine3.Text,
                txtIndustrialGrowthLine4.Text,
                txtIndustrialGrowthLine5.Text,
                txtIndustrialGrowthLine6.Text,
                txtIndustrialGrowthLine7.Text,
                txtIndustrialGrowthLine8.Text
            };
        }

        private void ShowExtremeLosses(List<string> lines)
        {
            txtExtremeLossesLine1.Text = lines[0];
            txtExtremeLossesLine2.Text = lines[1];
            txtExtremeLossesLine3.Text = lines[2];
            txtExtremeLossesLine4.Text = lines[3];
            txtExtremeLossesLine5.Text = lines[4];
            txtExtremeLossesLine6.Text = lines[5];
            txtExtremeLossesLine7.Text = lines[6];
            txtExtremeLossesLine8.Text = lines[7];
        }

        private List<string> GetExtremeLosses()
        {
            return new List<string>()
            {
                txtExtremeLossesLine1.Text,
                txtExtremeLossesLine2.Text,
                txtExtremeLossesLine3.Text,
                txtExtremeLossesLine4.Text,
                txtExtremeLossesLine5.Text,
                txtExtremeLossesLine6.Text,
                txtExtremeLossesLine7.Text,
                txtExtremeLossesLine8.Text
            };
        }
        private void ShowGeneralValuationChange(List<string> lines)
        {
            txtGeneralChangeLine1.Text = lines[0];
            txtGeneralChangeLine2.Text = lines[1];
            txtGeneralChangeLine3.Text = lines[2];
            txtGeneralChangeLine4.Text = lines[3];
            txtGeneralChangeLine5.Text = lines[4];
            txtGeneralChangeLine6.Text = lines[5];
            txtGeneralChangeLine7.Text = lines[6];
            txtGeneralChangeLine8.Text = lines[7];
            txtGeneralChangeLine9.Text = lines[8];
            txtGeneralChangeLine10.Text = lines[9];
        }

        private List<string> GetGeneralValuationChange()
        {
            return new List<string>()
            {
                txtGeneralChangeLine1.Text,
                txtGeneralChangeLine2.Text,
                txtGeneralChangeLine3.Text,
                txtGeneralChangeLine4.Text,
                txtGeneralChangeLine5.Text,
                txtGeneralChangeLine6.Text,
                txtGeneralChangeLine7.Text,
                txtGeneralChangeLine8.Text,
                txtGeneralChangeLine9.Text,
                txtGeneralChangeLine10.Text
            };
        }

        private void SaveMVR()
        {
            var mvr = ViewModel.ValuationReturn;
            mvr.County = txtCounty.Text;
            mvr.Municipality = txtMunicipality.Text;
            mvr.CertifiedRatio =  txtCertifiedRatio.Text.ToDecimalValue();
            mvr.CommitmentDate = dtCommitmentDate.NullableValue ?? DateTime.MinValue;
            UpdateRealEstateData(mvr);
            UpdatePersonalPropertyData(mvr);
            UpdateHomesteadData(mvr);
            UpdateOtherTax(mvr);
            UpdateBete(mvr);
            UpdateTif(mvr);
            UpdateExcise(mvr);
            UpdateElectricalGeneration(mvr);
            UpdateTreeGrowth(mvr);
            UpdateFarmAndOpenSpace(mvr);
            UpdateWaterfront(mvr);
            UpdateExemptProperty(mvr);
            UpdateVeteranExemptions(mvr);
            UpdateMunicipalRecords(mvr);
            UpdateValuationInformation(mvr);
            ViewModel.SaveMVR();
            MessageBox.Show("MVR Values Saved." + "\r\n" + "");
		}

        private void UpdateValuationInformation(MunicipalValuationReturn mvr)
        {
            mvr.ValuationInformation.New = GetGridStat(ValuationGridRow.New);
            mvr.ValuationInformation.Converted = GetGridStat(ValuationGridRow.Converted);
            mvr.ValuationInformation.Demolished = GetGridStat(ValuationGridRow.Demolished);
            mvr.ValuationInformation.ValuationIncrease = GetGridValues(ValuationGridRow.ValuationIncrease);
            mvr.ValuationInformation.ValuationDecrease = GetGridValues(ValuationGridRow.ValuationLoss);
            mvr.ValuationInformation.IndustrialMercantileGrowth = GetIndustrialGrowth();
            mvr.ValuationInformation.ExtremeLosses = GetExtremeLosses();
            mvr.ValuationInformation.GeneralValuationChange = GetGeneralValuationChange();            
        }

        private void UpdateMunicipalRecords(MunicipalValuationReturn mvr)
        {
            mvr.MunicipalRecords.HasTaxMaps = chkHasTaxMaps.Checked;
            mvr.MunicipalRecords.DateMapsOriginallyObtained = dtMapsOriginallyObtained.NullableValue;
            mvr.MunicipalRecords.NameOfOriginalContractor = txtNameOfOriginalContractor.Text;
            mvr.MunicipalRecords.NumberOfParcelsInMunicipality = txtMunicipalRecordsTotalNumberOfParcels.Text.ToIntegerValue();
            mvr.MunicipalRecords.TotalTaxableAcreage = txtMunicipalRecordsTotalTaxableAcreage.Text.ToDecimalValue();
            mvr.MunicipalRecords.ProfessionalRevaluationWasCompleted = chkRevaluationCompleted.Checked;
            mvr.MunicipalRecords.RevaluationIncludedLand = chkRevaluationIncludedLand.Checked;
            mvr.MunicipalRecords.RevaluationIncludedBuildings = chkRevaluationIncludedBuildings.Checked;
            mvr.MunicipalRecords.RevaluationIncludedPersonalProperty = chkRevaluationIncludedPersonalProperty.Checked;
            mvr.MunicipalRecords.RevaluationEffectiveDate = dtRevaluationEffectiveDate.NullableValue;
            mvr.MunicipalRecords.RevaluationContractorName = txtRevaluationContractorName.Text;
            mvr.MunicipalRecords.RevaluationCost = txtRevaluationCost.Text.ToDecimalValue();
            var assessmentFunction = (GenericDescriptionPair<MVRMunicipalityAssessmentFunction>)cmbAssessorFunctionality.SelectedItem;
            if (assessmentFunction != null)
            {
                mvr.MunicipalRecords.AssessmentFunction =
                    assessmentFunction.ID;
            }

            var taxMapType = (GenericDescriptionPair<MVRTaxMapType>) cmbTaxMapType.SelectedItem;
            if (taxMapType != null)
            {
                mvr.MunicipalRecords.TaxMapType = taxMapType.ID;
            }
            mvr.MunicipalRecords.AssessorOrAgentName = txtAssessorName.Text;
            mvr.MunicipalRecords.AssessorOrAgentEmail = txtAssessorEmail.Text;
            mvr.MunicipalRecords.FiscalYearStart = dtpFiscalStart.NullableValue;
            mvr.MunicipalRecords.FiscalYearEnd = dtpFiscalEnd.NullableValue;
            mvr.MunicipalRecords.OverDueInterestRate = txtInterestRate.Text.ToDecimalValue();
            mvr.MunicipalRecords.Period1DueDate = dtpPeriod1Due.NullableValue;
            mvr.MunicipalRecords.Period2DueDate = dtpPeriod2Due.NullableValue;
            mvr.MunicipalRecords.Period3DueDate = dtpPeriod3Due.NullableValue;
            mvr.MunicipalRecords.Period4DueDate = dtpPeriod4Due.NullableValue;
            mvr.MunicipalRecords.AssessmentRecordsAreComputerized = chkAssessmentRecordsAreComputerized.Checked;
            mvr.MunicipalRecords.AssessmentSoftware = txtAssessmentSoftware.Text;
            mvr.MunicipalRecords.ImplementedATaxReliefProgram = chkTaxReliefProgram.Checked;
            mvr.MunicipalRecords.NumberOfPeopleQualifiedForTaxProgram = txtNumberQualifiedForTaxRelief.Text.ToIntegerValue();
            mvr.MunicipalRecords.AmountOfReliefGrantedForTaxProgram = txtAmountOfTaxRelief.Text.ToDecimalValue();
            mvr.MunicipalRecords.ImplementedElderlyTaxCredit = chkElderlyTaxCredit.Checked;
            mvr.MunicipalRecords.NumberOfPeopleQualifiedForElderlyCredit = txtNumberQualifiedForElderlyTaxCredit.Text.ToIntegerValue();
            mvr.MunicipalRecords.AmountOfReliefGrantedForElderlyCredit = txtAmountOfElderlyTaxCredit.Text.ToDecimalValue();
            mvr.MunicipalRecords.ImplementedSeniorTaxDeferral = chkSeniorTaxDeferral.Checked;
            mvr.MunicipalRecords.NumberOfPeopleQualifiedForSeniorTaxDeferral =
                txtNumberQualifiedForSeniorDeferral.Text.ToIntegerValue();
            mvr.MunicipalRecords.AmountOfReliefGrantedForSeniorTaxDeferral =
                txtAmountOfSeniorTaxDeferral.Text.ToDecimalValue();
        }

        private void UpdateVeteranExemptions(MunicipalValuationReturn mvr)
        {
            mvr.VeteranExemptions.SurvivingMale.Count = txtExemptVetSurvivingMaleCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.SurvivingMale.ExemptValue = txtExemptVetSurvivingMaleValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.ParaplegicRevocableTrusts.Count = txtExemptVetParaplegicLivingTrustCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.ParaplegicRevocableTrusts.ExemptValue = txtExemptVetParaplegicLivingTrustValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.MiscRevocableTrust.Count = txtExemptVetLivingTrustCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.MiscRevocableTrust.ExemptValue = txtExemptVetLivingTrustValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.MaineEnlistedWWIVeteran.Count = txtExemptVetWWIResidentCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.MaineEnlistedWWIVeteran.ExemptValue = txtExemptVetWWIResidentValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.NonResidentWWIVeteran.Count = txtExemptVetWWINonResidentCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.NonResidentWWIVeteran.ExemptValue = txtExemptVetWWINonResidentValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.ParaplegicVeteran.Count = txtExemptVetParaplegicCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.ParaplegicVeteran.ExemptValue = txtExemptVetParaplegicValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.COOPHousing.Count = txtExemptVetCoopCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.COOPHousing.ExemptValue = txtExemptVetCoopValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.MiscMaineEnlisted.Count = txtExemptVetResidentCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.MiscMaineEnlisted.ExemptValue = txtExemptVetResidentValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.MiscNonResident.Count = txtExemptVetNonResidentCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.MiscNonResident.ExemptValue = txtExemptVetNonResidentValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.DisabledInLineOfDuty.Count = txtExemptVetDisabledCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.DisabledInLineOfDuty.ExemptValue = txtExemptVetDisabledValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.LebanonPanama.Count = txtExemptVetLebanonPanamaCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.LebanonPanama.ExemptValue = txtExemptVetLebanonPanamaValue.Text.ToDecimalValue();
            mvr.VeteranExemptions.EarlyVietnamConflict.Count = txtExemptVetVietnamCount.Text.ToIntegerValue();
            mvr.VeteranExemptions.EarlyVietnamConflict.ExemptValue = txtExemptVetVietnamValue.Text.ToDecimalValue();
        }

        private void UpdateExemptProperty(MunicipalValuationReturn mvr)
        {
            mvr.ExemptProperty.UnitedStates = txtExemptFederal.Text.ToDecimalValue();
            mvr.ExemptProperty.StateOfMaine = txtExemptMaine.Text.ToDecimalValue();
            mvr.ExemptProperty.NewhampshireWater = txtExemptNHWater.Text.ToDecimalValue();
            mvr.ExemptProperty.Municipal = txtExemptMunicipal.Text.ToDecimalValue();
            mvr.ExemptProperty.MunicipalUtilities = txtExemptMunicipalUtility.Text.ToDecimalValue();
            mvr.ExemptProperty.MunicipalAirport = txtExemptMunicipalAirport.Text.ToDecimalValue();
            mvr.ExemptProperty.PrivateAirport = txtExemptPrivateAirport.Text.ToDecimalValue();
            mvr.ExemptProperty.MunicipalSewage = txtExemptSewage.Text.ToDecimalValue();
            mvr.ExemptProperty.Benevolent = txtExemptBenevolent.Text.ToDecimalValue();
            mvr.ExemptProperty.LiteraryAndScientific = txtExemptLiteraryAndScientific.Text.ToDecimalValue();
            mvr.ExemptProperty.VeteranOrganizations = txtExemptVeteransOrganizations.Text.ToDecimalValue();
            mvr.ExemptProperty.VeteranOrgReimbursableExemption = txtExemptVetOrgReimbursable.Text.ToDecimalValue();
            mvr.ExemptProperty.ChamberOfCommerce = txtExemptChamberOfCommerce.Text.ToDecimalValue();
            mvr.ExemptProperty.NumberOfParsonages = txtExemptParsonages.Text.ToIntegerValue();
            mvr.ExemptProperty.ExemptValueOfParsonages = txtExemptValueOfParsonages.Text.ToDecimalValue();
            mvr.ExemptProperty.TaxableValueOfParsonages  = txtExemptTaxableValueOfParsonages.Text.ToDecimalValue();
            mvr.ExemptProperty.ReligiousWorship = txtExemptReligiousWorship.Text.ToDecimalValue();
            mvr.ExemptProperty.FraternalOrganizations = txtExemptFraternal.Text.ToDecimalValue();
            mvr.ExemptProperty.HospitalPersonalProperty = txtExemptHospitalPersonalProperty.Text.ToDecimalValue();
            mvr.ExemptProperty.LegallyBlind = txtExemptLegallyBlind.Text.ToDecimalValue();
            mvr.ExemptProperty.WaterSupplyTransport = txtExemptWaterSupplyTransport.Text.ToDecimalValue();
            mvr.ExemptProperty.CertifiedAnimalWasteStorage = txtExemptAnimalWasteStorage.Text.ToDecimalValue();
            mvr.ExemptProperty.PollutionControlFacilities = txtExemptPollutionControl.Text.ToDecimalValue();
            mvr.ExemptProperty.SnowmobileGroomingEquipment = txtExemptSnowmobileGroomingEquipment.Text.ToDecimalValue();
            mvr.ExemptProperty.SolarAndWindApplicationsProcessed = txtExemptSolarAndWindProcessed.Text.ToIntegerValue();
            mvr.ExemptProperty.SolarAndWindApplicationsApproved = txtExemptSolarAndWindApproved.Text.ToIntegerValue();
            mvr.ExemptProperty.SolarAndWindEquipment = txtExemptSolarAndWindValue.Text.ToDecimalValue();
            mvr.ExemptProperty.ExemptOrganizations[0].Name = txtExemptOrganization1Name.Text;
            mvr.ExemptProperty.ExemptOrganizations[0].ProvisionOfLaw = txtExemptProvisionOfLaw1.Text;
            mvr.ExemptProperty.ExemptOrganizations[0].ExemptValue = txtExemptOrganization1ExemptValue.Text.ToDecimalValue();

            mvr.ExemptProperty.ExemptOrganizations[1].Name = txtExemptOrganization2Name.Text;
            mvr.ExemptProperty.ExemptOrganizations[1].ProvisionOfLaw = txtExemptProvisionOfLaw2.Text;
            mvr.ExemptProperty.ExemptOrganizations[1].ExemptValue = txtExemptOrganization2ExemptValue.Text.ToDecimalValue();

            mvr.ExemptProperty.ExemptOrganizations[2].Name = txtExemptOrganization3Name.Text;
            mvr.ExemptProperty.ExemptOrganizations[2].ProvisionOfLaw = txtExemptProvisionOfLaw3.Text;
            mvr.ExemptProperty.ExemptOrganizations[2].ExemptValue = txtExemptOrganization3ExemptValue.Text.ToDecimalValue();

            //mvr.ExemptProperty.ExemptOrganizations[3].Name = txtExemptOrganization4Name.Text;
            //mvr.ExemptProperty.ExemptOrganizations[3].ProvisionOfLaw = txtExemptProvisionOfLaw4.Text;
            //mvr.ExemptProperty.ExemptOrganizations[3].ExemptValue = txtExemptOrganization4ExemptValue.Text.ToDecimalValue();
        }

        private void UpdateWaterfront(MunicipalValuationReturn mvr)
        {
            mvr.Waterfront.NumberOfParcelsFirstClassified = txtWaterfrontParcelsFirstClassified.Text.ToIntegerValue();
            mvr.Waterfront.NumberOfAcresFirstClassified = txtWaterfrontAcresFirstClassified.Text.ToDecimalValue();
            mvr.Waterfront.TotalAcresOfWaterfront = txtWaterfrontTotalAcreage.Text.ToDecimalValue();
            mvr.Waterfront.TotalValuationOfWaterfront = txtWaterfrontTotalValuation.Text.ToDecimalValue();
            mvr.Waterfront.NumberOfWaterfrontParcelsWithdrawn = txtWaterfrontParcelsWithdrawn.Text.ToIntegerValue();
            mvr.Waterfront.NumberOfWaterfrontAcresWithdrawn = txtWaterfrontAcresWithdrawn.Text.ToDecimalValue();
            mvr.Waterfront.TotalAmountOfPenalties = txtWaterfrontPenalties.Text.ToDecimalValue();
        }

        private void UpdateFarmAndOpenSpace(MunicipalValuationReturn mvr)
        {
            mvr.FarmAndOpenSpace.NumberOfParcelsClassifiedAsFarmland = txtFarmNumberOfParcelsFirstClassified.Text.ToIntegerValue();
            mvr.FarmAndOpenSpace.NumberOfAcresClassifiedAsFarmlandForTaxYear = txtFarmNumberOfAcresFirstClassified.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalNumberOfAcresOfAllFarmland = txtFarmTotalAcresTillable.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.NumberOfFarmSoftwoodAcres = txtFarmSoftwoodAcres.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres = txtFarmMixedWoodAcres.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.NumberOfFarmHardwoodAcres = txtFarmHardwoodAcres.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalNumberOfFarmWoodlandAcres = txtFarmTotalAcresWoodland.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalValuationOfFarmWoodland = txtFarmTotalValuationWoodland.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.FarmSoftwoodRate = txtFarmSoftwoodRate.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.FarmMixedWoodRate = txtFarmMixedWoodRate.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.FarmHardwoodRate = txtFarmHardwoodRate.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalNumberOfFarmlandParcelsWithdrawn = txtFarmParcelsWithdrawn.Text.ToIntegerValue();
            mvr.FarmAndOpenSpace.TotalNumberOfFarmlandAcresWithdrawn = txtFarmAcresWithdrawn.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnFarmland = txtFarmTotalPenalties.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.NumberOfOpenSpaceParcelsFirstClassified = txtOpenParcelsFirstClassified.Text.ToIntegerValue();
            mvr.FarmAndOpenSpace.NumberOfOpenSpaceAcresFirstClassified = txtOpenAcresFirstClassified.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalAcresOfOpenSpaceFirstClassified = txtOpenTotalAcres.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalValuationOfAllOpenSpace = txtOpenTotalValuation.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalNumberOfOpenSpaceParcelsWithdrawn = txtOpenParcelsWithdrawn.Text.ToIntegerValue();
            mvr.FarmAndOpenSpace.TotalNumberOfOpenSpaceAcresWithdrawn = txtOpenAcresWithdrawn.Text.ToDecimalValue();
            mvr.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnOpenSpace = txtOpenTotalAmountOfPenalties.Text.ToDecimalValue();
        }

        private void UpdateTreeGrowth(MunicipalValuationReturn mvr)
        {
            mvr.ForestTreeGrowth.AveragePerAcreUnitValue = txtForestAveragePerAcre.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.HardwoodAcreage = txtForestHardwoodAcreage.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.HardwoodRate = txtForestHardwoodRate.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.MixedwoodAcreage = txtForestMixedWoodAcreage.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.MixedWoodRate = txtForestMixedwoodRate.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.NumberOfParcelsClassified = txtForestNumberOfParcels.Text.ToIntegerValue();
            mvr.ForestTreeGrowth.SoftwoodAcreage = txtForestSoftwoodAcreage.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.SoftwoodRate = txtForestSoftwoodRate.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.TotalAssessedValuation = txtForestTotalAssessed.Text.ToIntegerValue();

            mvr.ForestTreeGrowth.NumberOfAcresFirstClassifiedForTaxYear = txtForestAcresFirstClassifiedThisYear.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.NumberOfParcelsWithdrawn = txtForestParcelsWithdrawn.Text.ToIntegerValue();
            mvr.ForestTreeGrowth.NumberOfAcresWithdrawn = txtForestAcresWithdrawn.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.TotalAmountOfPenaltiesAssessed = txtForestAmountOfPenalties.Text.ToDecimalValue();
            mvr.ForestTreeGrowth.TotalNumberOfNonCompliancePenalties = txtForestNumberOfNonCompliancePenalties.Text.ToIntegerValue();
            mvr.ForestTreeGrowth.TreeGrowthHasBeenTransferredToFarmlandThisYear = chkForestTransferredToFarmland.Checked;
        }

        private void UpdateElectricalGeneration(MunicipalValuationReturn mvr)
        {
            mvr.ElectricalGeneration.DistributionAndTransmissionLines = txtTransmissionLines.Text.ToIntegerValue();
            mvr.ElectricalGeneration.GenerationFacilities = txtElectricalGenerationFacilities.Text.ToIntegerValue();
        }

        private void UpdateExcise(MunicipalValuationReturn mvr)
        {
            mvr.Excise.Fiscal = cmbExcisePeriodType.Text == "Fiscal";            
            mvr.Excise.MVExcise = txtMVExcise.Text.ToDecimalValue();
            mvr.Excise.WatercraftExcise = txtWatercraftExcise.Text.ToDecimalValue();
        }

        private void UpdateTif(MunicipalValuationReturn mvr)
        {
            mvr.TIF.BeteRevenueDeposited = txtTifBeteDeposited.Text.ToDecimalValue();
            mvr.TIF.CapturedAssessedValue = txtTifCaptured.Text.ToIntegerValue();
            mvr.TIF.RevenueDeposited = txtTifDeposited.Text.ToDecimalValue();
            mvr.TIF.TifIncrease = txtIncreasedTif.Text.ToIntegerValue();
        }

        private void UpdateBete(MunicipalValuationReturn mvr)
        {
            mvr.Bete.TotalBeteInTIF = txtBeteInTIF.Text.ToIntegerValue();
            mvr.Bete.TotalBeteQualifiedExempt = txtExemptValueBeteQualified.Text.ToIntegerValue();
            mvr.Bete.ApplicationsProcessed = txtNumberBeteApplicationsProcessed.Text.ToIntegerValue();
            mvr.Bete.ApplicationsApproved = txtNumberBeteApproved.Text.ToIntegerValue();

        }

        private void UpdateOtherTax(MunicipalValuationReturn mvr)
        {
            mvr.OtherTaxInformation.TaxLevy = txtTaxLevy.Text.ToDecimalValue();
            mvr.OtherTaxInformation.TaxRate = txtPropertyTaxRate.Text.ToDecimalValue();
        }

        private void UpdateHomesteadData(MunicipalValuationReturn mvr)
        {
            mvr.Homestead.NumberOfFullHomesteadsGranted = txtTotalFullHomesteadsGranted.Text.ToIntegerValue();            
            mvr.Homestead.NumberOfFullyExemptedHomesteads = txtNumberHomesteadFullyExempt.Text.ToIntegerValue();
            mvr.Homestead.TotalValueOfFullyExemptedHomesteads = txtHomesteadFullyExemptValue.Text.ToDecimalValue();            
            mvr.Homestead.TotalAssessedValueOfAllHomesteadProperty = txtTotalHomesteadAssessments.Text.ToIntegerValue();            
        }

        private void UpdatePersonalPropertyData(MunicipalValuationReturn mvr)
        {
            mvr.PersonalProperty.OtherPersonalProperty = txtOtherPersonalProperty.Text.ToIntegerValue();
            mvr.PersonalProperty.BusinessEquipment = txtBusinessEquipment.Text.ToIntegerValue();
            mvr.PersonalProperty.ProductionMachineryAndEquipment = txtMachineryAndEquipment.Text.ToIntegerValue();            
        }

        private void UpdateRealEstateData(MunicipalValuationReturn mvr)
        {
            mvr.RealEstate.TaxableLand = txtTaxableLand.Text.ToIntegerValue();
            mvr.RealEstate.TaxableBuilding = txtTaxableBuilding.Text.ToIntegerValue();            
        }

        private enum ValuationGridRow
        {
            New = 1,
            Demolished = 2,
            Converted = 3,
            ValuationIncrease = 4,
            ValuationLoss = 5,
            NetChange = 6
        }

        private enum ValuationGridColumn
        {
            OneFamily = 1,
            TwoFamily = 2,
            ThreeToFourFamily = 3,
            FiveFamily = 4,
            MobileHomes = 5,
            SeasonalHomes = 6
        }
	}
}
