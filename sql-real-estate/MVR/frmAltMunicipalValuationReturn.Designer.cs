﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

//using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMVRViewer.
	/// </summary>
	partial class frmAltMunicipalValuationReturn : BaseForm
	{
		public fecherFoundation.FCLine LineRatio;
		public fecherFoundation.FCButton cmdLoadLast;
		public fecherFoundation.FCButton cmdExcelDoc;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAltMunicipalValuationReturn));
            this.LineRatio = new fecherFoundation.FCLine();
            this.cmdLoadLast = new fecherFoundation.FCButton();
            this.cmdExcelDoc = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.fcLabel7 = new fecherFoundation.FCLabel();
            this.fcLabel9 = new fecherFoundation.FCLabel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.fcLabel16 = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel23 = new fecherFoundation.FCLabel();
            this.fcLabel20 = new fecherFoundation.FCLabel();
            this.fcLabel22 = new fecherFoundation.FCLabel();
            this.fcLabel17 = new fecherFoundation.FCLabel();
            this.fcLabel19 = new fecherFoundation.FCLabel();
            this.betePanel = new fecherFoundation.FCPanel();
            this.fcTextBox3 = new fecherFoundation.FCTextBox();
            this.fcTextBox5 = new fecherFoundation.FCTextBox();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.fcTextBox7 = new fecherFoundation.FCTextBox();
            this.fcTextBox8 = new fecherFoundation.FCTextBox();
            this.fcLabel8 = new fecherFoundation.FCLabel();
            this.flowLayoutPanel1 = new Wisej.Web.FlowLayoutPanel();
            this.setupPanel = new fecherFoundation.FCPanel();
            this.fcLabel12 = new fecherFoundation.FCLabel();
            this.dtCommitmentDate = new fecherFoundation.FCDateTimePicker();
            this.fcTextBox11 = new fecherFoundation.FCTextBox();
            this.fcLabel11 = new fecherFoundation.FCLabel();
            this.txtLDNBottomAdjust = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.txtLDNTopAdjust = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.realEstatePanel = new fecherFoundation.FCPanel();
            this.fcTextBox12 = new fecherFoundation.FCTextBox();
            this.fcLabel13 = new fecherFoundation.FCLabel();
            this.fcTextBox6 = new fecherFoundation.FCTextBox();
            this.fcLabel6 = new fecherFoundation.FCLabel();
            this.fcTextBox9 = new fecherFoundation.FCTextBox();
            this.personalPropertyPanel = new fecherFoundation.FCPanel();
            this.fcTextBox13 = new fecherFoundation.FCTextBox();
            this.fcLabel14 = new fecherFoundation.FCLabel();
            this.fcTextBox2 = new fecherFoundation.FCTextBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcTextBox4 = new fecherFoundation.FCTextBox();
            this.fcTextBox10 = new fecherFoundation.FCTextBox();
            this.fcLabel10 = new fecherFoundation.FCLabel();
            this.otherTaxPanel = new fecherFoundation.FCPanel();
            this.fcTextBox15 = new fecherFoundation.FCTextBox();
            this.fcTextBox14 = new fecherFoundation.FCTextBox();
            this.fcLabel15 = new fecherFoundation.FCLabel();
            this.fcTextBox1 = new fecherFoundation.FCTextBox();
            this.homesteadPanel = new fecherFoundation.FCPanel();
            this.fcTextBox16 = new fecherFoundation.FCTextBox();
            this.fcTextBox26 = new fecherFoundation.FCTextBox();
            this.fcTextBox19 = new fecherFoundation.FCTextBox();
            this.fcTextBox25 = new fecherFoundation.FCTextBox();
            this.fcTextBox20 = new fecherFoundation.FCTextBox();
            this.fcLabel21 = new fecherFoundation.FCLabel();
            this.fcTextBox23 = new fecherFoundation.FCTextBox();
            this.fcTextBox17 = new fecherFoundation.FCTextBox();
            this.fcLabel18 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExcelDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.betePanel)).BeginInit();
            this.betePanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setupPanel)).BeginInit();
            this.setupPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.realEstatePanel)).BeginInit();
            this.realEstatePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personalPropertyPanel)).BeginInit();
            this.personalPropertyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.otherTaxPanel)).BeginInit();
            this.otherTaxPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.homesteadPanel)).BeginInit();
            this.homesteadPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 593);
            this.BottomPanel.Size = new System.Drawing.Size(978, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.flowLayoutPanel1);
            this.ClientArea.Controls.Add(this.LineRatio);
            this.ClientArea.Size = new System.Drawing.Size(998, 469);
            this.ClientArea.Controls.SetChildIndex(this.LineRatio, 0);
            this.ClientArea.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdExcelDoc);
            this.TopPanel.Controls.Add(this.cmdLoadLast);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLoadLast, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdExcelDoc, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // LineRatio
            // 
            this.LineRatio.Name = "LineRatio";
            this.LineRatio.Size = new System.Drawing.Size(1440, 1);
            this.LineRatio.Visible = false;
            this.LineRatio.X2 = 1440F;
            // 
            // cmdLoadLast
            // 
            this.cmdLoadLast.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLoadLast.Location = new System.Drawing.Point(583, 29);
            this.cmdLoadLast.Name = "cmdLoadLast";
            this.cmdLoadLast.Size = new System.Drawing.Size(117, 24);
            this.cmdLoadLast.TabIndex = 1;
            this.cmdLoadLast.Text = "Load Last Saved";
            // 
            // cmdExcelDoc
            // 
            this.cmdExcelDoc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdExcelDoc.Location = new System.Drawing.Point(704, 29);
            this.cmdExcelDoc.Name = "cmdExcelDoc";
            this.cmdExcelDoc.Size = new System.Drawing.Size(218, 24);
            this.cmdExcelDoc.TabIndex = 2;
            this.cmdExcelDoc.Text = "Create Excel Document for State";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(925, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(46, 24);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(421, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(18, 169);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(429, 17);
            this.fcLabel3.TabIndex = 36;
            this.fcLabel3.Text = "TOTAL EXEMPT VALUE OF BETE PROPERTY IN A TIF DISCTRICT";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel3, "furniture, furnishings and equipment");
            // 
            // fcLabel7
            // 
            this.fcLabel7.AutoSize = true;
            this.fcLabel7.Location = new System.Drawing.Point(18, 68);
            this.fcLabel7.Name = "fcLabel7";
            this.fcLabel7.Size = new System.Drawing.Size(311, 17);
            this.fcLabel7.TabIndex = 32;
            this.fcLabel7.Text = "NUMBER OF BETE APPLICATIONS APPROVED";
            this.fcLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel7, "furniture, furnishings and equipment");
            // 
            // fcLabel9
            // 
            this.fcLabel9.AutoSize = true;
            this.fcLabel9.Location = new System.Drawing.Point(5, 20);
            this.fcLabel9.Name = "fcLabel9";
            this.fcLabel9.Size = new System.Drawing.Size(45, 17);
            this.fcLabel9.TabIndex = 30;
            this.fcLabel9.Text = "LAND";
            this.fcLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel9, "Include value of transmission, distribution lines and substations, dams and power" +
        " houses");
            // 
            // fcLabel4
            // 
            this.fcLabel4.AutoSize = true;
            this.fcLabel4.Location = new System.Drawing.Point(5, 76);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(160, 17);
            this.fcLabel4.TabIndex = 54;
            this.fcLabel4.Text = "BUSINESS EQUIPMENT";
            this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel4, "furniture, furnishings and equipment");
            // 
            // fcLabel16
            // 
            this.fcLabel16.AutoSize = true;
            this.fcLabel16.Location = new System.Drawing.Point(5, 126);
            this.fcLabel16.Name = "fcLabel16";
            this.fcLabel16.Size = new System.Drawing.Size(151, 17);
            this.fcLabel16.TabIndex = 60;
            this.fcLabel16.Text = "PROPERTY TAX LEVY";
            this.fcLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel16, "furniture, furnishings and equipment");
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(5, 74);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(152, 17);
            this.fcLabel1.TabIndex = 56;
            this.fcLabel1.Text = "PROPERTY TAX RATE";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel1, "furniture, furnishings and equipment");
            // 
            // fcLabel23
            // 
            this.fcLabel23.AutoSize = true;
            this.fcLabel23.Location = new System.Drawing.Point(7, 334);
            this.fcLabel23.Name = "fcLabel23";
            this.fcLabel23.Size = new System.Drawing.Size(473, 17);
            this.fcLabel23.TabIndex = 82;
            this.fcLabel23.Text = "TOTAL ASSESSED VALUE OF ALL HOMESTEAD QUALIFIED PROPERTY";
            this.fcLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel23, "furniture, furnishings and equipment");
            // 
            // fcLabel20
            // 
            this.fcLabel20.AutoSize = true;
            this.fcLabel20.Location = new System.Drawing.Point(7, 282);
            this.fcLabel20.Name = "fcLabel20";
            this.fcLabel20.Size = new System.Drawing.Size(473, 17);
            this.fcLabel20.TabIndex = 79;
            this.fcLabel20.Text = "TOTAL EXEMPT VALUE FOR ALL HOMESTEAD EXEMPTIONS GRANTED";
            this.fcLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel20, "furniture, furnishings and equipment");
            // 
            // fcLabel22
            // 
            this.fcLabel22.AutoSize = true;
            this.fcLabel22.Location = new System.Drawing.Point(7, 230);
            this.fcLabel22.Name = "fcLabel22";
            this.fcLabel22.Size = new System.Drawing.Size(388, 17);
            this.fcLabel22.TabIndex = 75;
            this.fcLabel22.Text = "TOTAL NUMBER OF HOMESTEAD EXEMPTIONS GRANTED";
            this.fcLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel22, "furniture, furnishings and equipment");
            // 
            // fcLabel17
            // 
            this.fcLabel17.AutoSize = true;
            this.fcLabel17.Location = new System.Drawing.Point(7, 126);
            this.fcLabel17.Name = "fcLabel17";
            this.fcLabel17.Size = new System.Drawing.Size(542, 17);
            this.fcLabel17.TabIndex = 73;
            this.fcLabel17.Text = "TOTAL NUMBER OF PROPERTY FULLY EXEMPTED BY HOMESTEAD EXEMPTIONS";
            this.fcLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel17, "furniture, furnishings and equipment");
            // 
            // fcLabel19
            // 
            this.fcLabel19.AutoSize = true;
            this.fcLabel19.Location = new System.Drawing.Point(7, 74);
            this.fcLabel19.Name = "fcLabel19";
            this.fcLabel19.Size = new System.Drawing.Size(525, 17);
            this.fcLabel19.TabIndex = 70;
            this.fcLabel19.Text = "TOTAL EXEMPT VALUE FOR ALL $20,000 HOMESTEAD EXEMPTIONS GRANTED";
            this.fcLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel19, "furniture, furnishings and equipment");
            // 
            // betePanel
            // 
            this.betePanel.Collapsed = true;
            this.betePanel.Controls.Add(this.fcTextBox3);
            this.betePanel.Controls.Add(this.fcLabel3);
            this.betePanel.Controls.Add(this.fcTextBox5);
            this.betePanel.Controls.Add(this.fcLabel5);
            this.betePanel.Controls.Add(this.fcTextBox7);
            this.betePanel.Controls.Add(this.fcLabel7);
            this.betePanel.Controls.Add(this.fcTextBox8);
            this.betePanel.Controls.Add(this.fcLabel8);
            this.betePanel.Location = new System.Drawing.Point(3, 425);
            this.betePanel.Name = "betePanel";
            this.betePanel.RestoreBounds = new System.Drawing.Rectangle(3, 425, 968, 270);
            this.betePanel.ShowHeader = true;
            this.betePanel.Size = new System.Drawing.Size(968, 28);
            this.betePanel.TabIndex = 1004;
            this.betePanel.Text = "BETE REIMBURSEMENT CLAIM";
            // 
            // fcTextBox3
            // 
            this.fcTextBox3.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox3.Location = new System.Drawing.Point(580, 155);
            this.fcTextBox3.MaxLength = 5;
            this.fcTextBox3.Name = "fcTextBox3";
            this.fcTextBox3.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox3.TabIndex = 37;
            this.fcTextBox3.Text = "0";
            this.fcTextBox3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox5
            // 
            this.fcTextBox5.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox5.Location = new System.Drawing.Point(580, 105);
            this.fcTextBox5.MaxLength = 5;
            this.fcTextBox5.Name = "fcTextBox5";
            this.fcTextBox5.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox5.TabIndex = 35;
            this.fcTextBox5.Text = "0";
            this.fcTextBox5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel5
            // 
            this.fcLabel5.AutoSize = true;
            this.fcLabel5.Location = new System.Drawing.Point(18, 119);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(405, 17);
            this.fcLabel5.TabIndex = 34;
            this.fcLabel5.Text = "TOTAL EXEMPT VALUE OF ALL BETE QUALIFIED PROPERTY";
            this.fcLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox7
            // 
            this.fcTextBox7.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox7.Location = new System.Drawing.Point(580, 54);
            this.fcTextBox7.MaxLength = 5;
            this.fcTextBox7.Name = "fcTextBox7";
            this.fcTextBox7.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox7.TabIndex = 33;
            this.fcTextBox7.Text = "0";
            this.fcTextBox7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox8
            // 
            this.fcTextBox8.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox8.Location = new System.Drawing.Point(580, 4);
            this.fcTextBox8.MaxLength = 5;
            this.fcTextBox8.Name = "fcTextBox8";
            this.fcTextBox8.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox8.TabIndex = 31;
            this.fcTextBox8.Text = "0";
            this.fcTextBox8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel8
            // 
            this.fcLabel8.AutoSize = true;
            this.fcLabel8.Location = new System.Drawing.Point(18, 18);
            this.fcLabel8.Name = "fcLabel8";
            this.fcLabel8.Size = new System.Drawing.Size(453, 17);
            this.fcLabel8.TabIndex = 30;
            this.fcLabel8.Text = "NUMBER OF BETE APPLICATIONS PROCESSED FOR THE TAX YEAR";
            this.fcLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.setupPanel);
            this.flowLayoutPanel1.Controls.Add(this.realEstatePanel);
            this.flowLayoutPanel1.Controls.Add(this.personalPropertyPanel);
            this.flowLayoutPanel1.Controls.Add(this.otherTaxPanel);
            this.flowLayoutPanel1.Controls.Add(this.homesteadPanel);
            this.flowLayoutPanel1.Controls.Add(this.betePanel);
            this.flowLayoutPanel1.FlowDirection = Wisej.Web.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new Wisej.Web.Padding(0, 3, 0, 3);
            this.flowLayoutPanel1.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(978, 590);
            this.flowLayoutPanel1.TabIndex = 1005;
            this.flowLayoutPanel1.TabStop = true;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // setupPanel
            // 
            this.setupPanel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
            this.setupPanel.Controls.Add(this.fcLabel12);
            this.setupPanel.Controls.Add(this.dtCommitmentDate);
            this.setupPanel.Controls.Add(this.fcTextBox11);
            this.setupPanel.Controls.Add(this.fcLabel11);
            this.setupPanel.Controls.Add(this.txtLDNBottomAdjust);
            this.setupPanel.Controls.Add(this.Label7);
            this.setupPanel.Controls.Add(this.txtLDNTopAdjust);
            this.setupPanel.Controls.Add(this.Label2);
            this.setupPanel.Location = new System.Drawing.Point(3, 6);
            this.setupPanel.Name = "setupPanel";
            this.setupPanel.ShowHeader = true;
            this.setupPanel.Size = new System.Drawing.Size(968, 277);
            this.setupPanel.TabIndex = 1007;
            this.setupPanel.Text = "Municipal Valuation Setup";
            // 
            // fcLabel12
            // 
            this.fcLabel12.AutoSize = true;
            this.fcLabel12.Location = new System.Drawing.Point(18, 22);
            this.fcLabel12.Name = "fcLabel12";
            this.fcLabel12.Size = new System.Drawing.Size(140, 17);
            this.fcLabel12.TabIndex = 57;
            this.fcLabel12.Text = "COMMITMENT DATE";
            this.fcLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtCommitmentDate
            // 
            this.dtCommitmentDate.AutoSize = false;
            this.dtCommitmentDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtCommitmentDate.Location = new System.Drawing.Point(328, 7);
            this.dtCommitmentDate.Mask = "00/00/0000";
            this.dtCommitmentDate.Name = "dtCommitmentDate";
            this.dtCommitmentDate.Size = new System.Drawing.Size(170, 40);
            this.dtCommitmentDate.TabIndex = 56;
            // 
            // fcTextBox11
            // 
            this.fcTextBox11.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox11.Location = new System.Drawing.Point(328, 166);
            this.fcTextBox11.MaxLength = 5;
            this.fcTextBox11.Name = "fcTextBox11";
            this.fcTextBox11.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox11.TabIndex = 55;
            this.fcTextBox11.Text = "0";
            this.fcTextBox11.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel11
            // 
            this.fcLabel11.AutoSize = true;
            this.fcLabel11.Location = new System.Drawing.Point(18, 180);
            this.fcLabel11.Name = "fcLabel11";
            this.fcLabel11.Size = new System.Drawing.Size(124, 17);
            this.fcLabel11.TabIndex = 54;
            this.fcLabel11.Text = "CERTIFIED RATIO";
            this.fcLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLDNBottomAdjust
            // 
            this.txtLDNBottomAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNBottomAdjust.Location = new System.Drawing.Point(328, 111);
            this.txtLDNBottomAdjust.MaxLength = 5;
            this.txtLDNBottomAdjust.Name = "txtLDNBottomAdjust";
            this.txtLDNBottomAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNBottomAdjust.TabIndex = 53;
            this.txtLDNBottomAdjust.Text = "0";
            this.txtLDNBottomAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(18, 125);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(103, 17);
            this.Label7.TabIndex = 52;
            this.Label7.Text = "MUNICIPALITY";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLDNTopAdjust
            // 
            this.txtLDNTopAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNTopAdjust.Location = new System.Drawing.Point(328, 61);
            this.txtLDNTopAdjust.MaxLength = 5;
            this.txtLDNTopAdjust.Name = "txtLDNTopAdjust";
            this.txtLDNTopAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNTopAdjust.TabIndex = 51;
            this.txtLDNTopAdjust.Text = "0";
            this.txtLDNTopAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(18, 75);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(64, 17);
            this.Label2.TabIndex = 50;
            this.Label2.Text = "COUNTY";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // realEstatePanel
            // 
            this.realEstatePanel.Collapsed = true;
            this.realEstatePanel.Controls.Add(this.fcTextBox12);
            this.realEstatePanel.Controls.Add(this.fcLabel13);
            this.realEstatePanel.Controls.Add(this.fcTextBox6);
            this.realEstatePanel.Controls.Add(this.fcLabel6);
            this.realEstatePanel.Controls.Add(this.fcTextBox9);
            this.realEstatePanel.Controls.Add(this.fcLabel9);
            this.realEstatePanel.Location = new System.Drawing.Point(3, 289);
            this.realEstatePanel.Name = "realEstatePanel";
            this.realEstatePanel.RestoreBounds = new System.Drawing.Rectangle(3, 289, 968, 216);
            this.realEstatePanel.ShowHeader = true;
            this.realEstatePanel.Size = new System.Drawing.Size(968, 28);
            this.realEstatePanel.TabIndex = 1006;
            this.realEstatePanel.Text = "Taxable Valuation of Real Estate";
            // 
            // fcTextBox12
            // 
            this.fcTextBox12.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox12.Focusable = false;
            this.fcTextBox12.Location = new System.Drawing.Point(328, 108);
            this.fcTextBox12.LockedOriginal = true;
            this.fcTextBox12.MaxLength = 5;
            this.fcTextBox12.Name = "fcTextBox12";
            this.fcTextBox12.ReadOnly = true;
            this.fcTextBox12.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox12.TabIndex = 35;
            this.fcTextBox12.Text = "0";
            this.fcTextBox12.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel13
            // 
            this.fcLabel13.AutoSize = true;
            this.fcLabel13.Location = new System.Drawing.Point(5, 120);
            this.fcLabel13.Name = "fcLabel13";
            this.fcLabel13.Size = new System.Drawing.Size(201, 17);
            this.fcLabel13.TabIndex = 34;
            this.fcLabel13.Text = "TOTAL TAXABLE VALUATION";
            this.fcLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox6
            // 
            this.fcTextBox6.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox6.Location = new System.Drawing.Point(328, 58);
            this.fcTextBox6.MaxLength = 5;
            this.fcTextBox6.Name = "fcTextBox6";
            this.fcTextBox6.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox6.TabIndex = 33;
            this.fcTextBox6.Text = "0";
            this.fcTextBox6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel6
            // 
            this.fcLabel6.AutoSize = true;
            this.fcLabel6.Location = new System.Drawing.Point(5, 70);
            this.fcLabel6.Name = "fcLabel6";
            this.fcLabel6.Size = new System.Drawing.Size(73, 17);
            this.fcLabel6.TabIndex = 32;
            this.fcLabel6.Text = "BUILDING";
            this.fcLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox9
            // 
            this.fcTextBox9.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox9.Location = new System.Drawing.Point(328, 8);
            this.fcTextBox9.MaxLength = 5;
            this.fcTextBox9.Name = "fcTextBox9";
            this.fcTextBox9.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox9.TabIndex = 31;
            this.fcTextBox9.Text = "0";
            this.fcTextBox9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // personalPropertyPanel
            // 
            this.personalPropertyPanel.Anchor = Wisej.Web.AnchorStyles.Left;
            this.personalPropertyPanel.Collapsed = true;
            this.personalPropertyPanel.Controls.Add(this.fcTextBox13);
            this.personalPropertyPanel.Controls.Add(this.fcLabel14);
            this.personalPropertyPanel.Controls.Add(this.fcTextBox2);
            this.personalPropertyPanel.Controls.Add(this.fcLabel2);
            this.personalPropertyPanel.Controls.Add(this.fcTextBox4);
            this.personalPropertyPanel.Controls.Add(this.fcLabel4);
            this.personalPropertyPanel.Controls.Add(this.fcTextBox10);
            this.personalPropertyPanel.Controls.Add(this.fcLabel10);
            this.personalPropertyPanel.Location = new System.Drawing.Point(3, 323);
            this.personalPropertyPanel.Name = "personalPropertyPanel";
            this.personalPropertyPanel.RestoreBounds = new System.Drawing.Rectangle(3, 323, 968, 279);
            this.personalPropertyPanel.ShowHeader = true;
            this.personalPropertyPanel.Size = new System.Drawing.Size(968, 28);
            this.personalPropertyPanel.TabIndex = 1008;
            this.personalPropertyPanel.Text = "Taxable Valuation of Personal Property";
            // 
            // fcTextBox13
            // 
            this.fcTextBox13.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox13.Focusable = false;
            this.fcTextBox13.Location = new System.Drawing.Point(328, 163);
            this.fcTextBox13.LockedOriginal = true;
            this.fcTextBox13.MaxLength = 5;
            this.fcTextBox13.Name = "fcTextBox13";
            this.fcTextBox13.ReadOnly = true;
            this.fcTextBox13.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox13.TabIndex = 59;
            this.fcTextBox13.Text = "0";
            this.fcTextBox13.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel14
            // 
            this.fcLabel14.AutoSize = true;
            this.fcLabel14.Location = new System.Drawing.Point(5, 177);
            this.fcLabel14.Name = "fcLabel14";
            this.fcLabel14.Size = new System.Drawing.Size(201, 17);
            this.fcLabel14.TabIndex = 58;
            this.fcLabel14.Text = "TOTAL TAXABLE VALUATION";
            this.fcLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox2
            // 
            this.fcTextBox2.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox2.Location = new System.Drawing.Point(328, 113);
            this.fcTextBox2.MaxLength = 5;
            this.fcTextBox2.Name = "fcTextBox2";
            this.fcTextBox2.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox2.TabIndex = 57;
            this.fcTextBox2.Text = "0";
            this.fcTextBox2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(5, 127);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(240, 17);
            this.fcLabel2.TabIndex = 56;
            this.fcLabel2.Text = "ALL OTHER PERSONAL PROPERTY";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox4
            // 
            this.fcTextBox4.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox4.Location = new System.Drawing.Point(328, 62);
            this.fcTextBox4.MaxLength = 5;
            this.fcTextBox4.Name = "fcTextBox4";
            this.fcTextBox4.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox4.TabIndex = 55;
            this.fcTextBox4.Text = "0";
            this.fcTextBox4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox10
            // 
            this.fcTextBox10.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox10.Location = new System.Drawing.Point(328, 12);
            this.fcTextBox10.MaxLength = 5;
            this.fcTextBox10.Name = "fcTextBox10";
            this.fcTextBox10.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox10.TabIndex = 53;
            this.fcTextBox10.Text = "0";
            this.fcTextBox10.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel10
            // 
            this.fcLabel10.AutoSize = true;
            this.fcLabel10.Location = new System.Drawing.Point(5, 26);
            this.fcLabel10.Name = "fcLabel10";
            this.fcLabel10.Size = new System.Drawing.Size(299, 17);
            this.fcLabel10.TabIndex = 52;
            this.fcLabel10.Text = "PRODUCTION MACHINERY AND EQUIPMENT";
            this.fcLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // otherTaxPanel
            // 
            this.otherTaxPanel.Collapsed = true;
            this.otherTaxPanel.Controls.Add(this.fcTextBox15);
            this.otherTaxPanel.Controls.Add(this.fcLabel16);
            this.otherTaxPanel.Controls.Add(this.fcTextBox14);
            this.otherTaxPanel.Controls.Add(this.fcLabel15);
            this.otherTaxPanel.Controls.Add(this.fcTextBox1);
            this.otherTaxPanel.Controls.Add(this.fcLabel1);
            this.otherTaxPanel.Location = new System.Drawing.Point(3, 357);
            this.otherTaxPanel.Name = "otherTaxPanel";
            this.otherTaxPanel.RestoreBounds = new System.Drawing.Rectangle(3, 357, 968, 231);
            this.otherTaxPanel.ShowHeader = true;
            this.otherTaxPanel.Size = new System.Drawing.Size(968, 28);
            this.otherTaxPanel.TabIndex = 1009;
            this.otherTaxPanel.Text = "Other Tax Information";
            // 
            // fcTextBox15
            // 
            this.fcTextBox15.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox15.Location = new System.Drawing.Point(328, 112);
            this.fcTextBox15.MaxLength = 5;
            this.fcTextBox15.Name = "fcTextBox15";
            this.fcTextBox15.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox15.TabIndex = 61;
            this.fcTextBox15.Text = "0";
            this.fcTextBox15.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox14
            // 
            this.fcTextBox14.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox14.Focusable = false;
            this.fcTextBox14.Location = new System.Drawing.Point(328, 9);
            this.fcTextBox14.LockedOriginal = true;
            this.fcTextBox14.MaxLength = 5;
            this.fcTextBox14.Name = "fcTextBox14";
            this.fcTextBox14.ReadOnly = true;
            this.fcTextBox14.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox14.TabIndex = 59;
            this.fcTextBox14.Text = "0";
            this.fcTextBox14.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel15
            // 
            this.fcLabel15.AutoSize = true;
            this.fcLabel15.Location = new System.Drawing.Point(5, 23);
            this.fcLabel15.Name = "fcLabel15";
            this.fcLabel15.Size = new System.Drawing.Size(201, 17);
            this.fcLabel15.TabIndex = 58;
            this.fcLabel15.Text = "TOTAL TAXABLE VALUATION";
            this.fcLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox1
            // 
            this.fcTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox1.Location = new System.Drawing.Point(328, 60);
            this.fcTextBox1.MaxLength = 5;
            this.fcTextBox1.Name = "fcTextBox1";
            this.fcTextBox1.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox1.TabIndex = 57;
            this.fcTextBox1.Text = "0";
            this.fcTextBox1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // homesteadPanel
            // 
            this.homesteadPanel.Collapsed = true;
            this.homesteadPanel.Controls.Add(this.fcTextBox16);
            this.homesteadPanel.Controls.Add(this.fcLabel23);
            this.homesteadPanel.Controls.Add(this.fcTextBox26);
            this.homesteadPanel.Controls.Add(this.fcTextBox19);
            this.homesteadPanel.Controls.Add(this.fcTextBox25);
            this.homesteadPanel.Controls.Add(this.fcLabel20);
            this.homesteadPanel.Controls.Add(this.fcTextBox20);
            this.homesteadPanel.Controls.Add(this.fcLabel21);
            this.homesteadPanel.Controls.Add(this.fcTextBox23);
            this.homesteadPanel.Controls.Add(this.fcLabel22);
            this.homesteadPanel.Controls.Add(this.fcLabel17);
            this.homesteadPanel.Controls.Add(this.fcTextBox17);
            this.homesteadPanel.Controls.Add(this.fcLabel18);
            this.homesteadPanel.Controls.Add(this.fcLabel19);
            this.homesteadPanel.Location = new System.Drawing.Point(3, 391);
            this.homesteadPanel.Name = "homesteadPanel";
            this.homesteadPanel.RestoreBounds = new System.Drawing.Rectangle(3, 391, 968, 434);
            this.homesteadPanel.ShowHeader = true;
            this.homesteadPanel.Size = new System.Drawing.Size(968, 28);
            this.homesteadPanel.TabIndex = 1010;
            this.homesteadPanel.Text = "Homestead Exemption Reimbursement Claim";
            // 
            // fcTextBox16
            // 
            this.fcTextBox16.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox16.Location = new System.Drawing.Point(578, 320);
            this.fcTextBox16.MaxLength = 5;
            this.fcTextBox16.Name = "fcTextBox16";
            this.fcTextBox16.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox16.TabIndex = 83;
            this.fcTextBox16.Text = "0";
            this.fcTextBox16.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox26
            // 
            this.fcTextBox26.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox26.Location = new System.Drawing.Point(578, 216);
            this.fcTextBox26.MaxLength = 5;
            this.fcTextBox26.Name = "fcTextBox26";
            this.fcTextBox26.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox26.TabIndex = 80;
            this.fcTextBox26.Text = "0";
            this.fcTextBox26.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox19
            // 
            this.fcTextBox19.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox19.Location = new System.Drawing.Point(578, 268);
            this.fcTextBox19.MaxLength = 5;
            this.fcTextBox19.Name = "fcTextBox19";
            this.fcTextBox19.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox19.TabIndex = 81;
            this.fcTextBox19.Text = "0";
            this.fcTextBox19.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox25
            // 
            this.fcTextBox25.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox25.Focusable = false;
            this.fcTextBox25.Location = new System.Drawing.Point(578, 113);
            this.fcTextBox25.LockedOriginal = true;
            this.fcTextBox25.MaxLength = 5;
            this.fcTextBox25.Name = "fcTextBox25";
            this.fcTextBox25.ReadOnly = true;
            this.fcTextBox25.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox25.TabIndex = 77;
            this.fcTextBox25.Text = "0";
            this.fcTextBox25.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox20
            // 
            this.fcTextBox20.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox20.Focusable = false;
            this.fcTextBox20.Location = new System.Drawing.Point(578, 165);
            this.fcTextBox20.LockedOriginal = true;
            this.fcTextBox20.MaxLength = 5;
            this.fcTextBox20.Name = "fcTextBox20";
            this.fcTextBox20.ReadOnly = true;
            this.fcTextBox20.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox20.TabIndex = 78;
            this.fcTextBox20.Text = "0";
            this.fcTextBox20.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel21
            // 
            this.fcLabel21.AutoSize = true;
            this.fcLabel21.Location = new System.Drawing.Point(7, 179);
            this.fcLabel21.Name = "fcLabel21";
            this.fcLabel21.Size = new System.Drawing.Size(436, 17);
            this.fcLabel21.TabIndex = 76;
            this.fcLabel21.Text = "TOTAL EXEMPT VALUE FOR ALL PROPERTIES FULLY EXEMPTED";
            this.fcLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox23
            // 
            this.fcTextBox23.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox23.Location = new System.Drawing.Point(578, 60);
            this.fcTextBox23.MaxLength = 5;
            this.fcTextBox23.Name = "fcTextBox23";
            this.fcTextBox23.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox23.TabIndex = 74;
            this.fcTextBox23.Text = "0";
            this.fcTextBox23.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcTextBox17
            // 
            this.fcTextBox17.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox17.Focusable = false;
            this.fcTextBox17.Location = new System.Drawing.Point(578, 9);
            this.fcTextBox17.LockedOriginal = true;
            this.fcTextBox17.MaxLength = 5;
            this.fcTextBox17.Name = "fcTextBox17";
            this.fcTextBox17.ReadOnly = true;
            this.fcTextBox17.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox17.TabIndex = 72;
            this.fcTextBox17.Text = "0";
            this.fcTextBox17.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel18
            // 
            this.fcLabel18.AutoSize = true;
            this.fcLabel18.Location = new System.Drawing.Point(7, 23);
            this.fcLabel18.Name = "fcLabel18";
            this.fcLabel18.Size = new System.Drawing.Size(440, 17);
            this.fcLabel18.TabIndex = 71;
            this.fcLabel18.Text = "TOTAL NUMBER OF $20,000 HOMESTEAD EXEMPTIONS GRANTED";
            this.fcLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmAltMunicipalValuationReturn
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(998, 529);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmAltMunicipalValuationReturn";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Municipal Valuation Return";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmMVRViewer_Load);
            this.Resize += new System.EventHandler(this.frmMVRViewer_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExcelDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.betePanel)).EndInit();
            this.betePanel.ResumeLayout(false);
            this.betePanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.setupPanel)).EndInit();
            this.setupPanel.ResumeLayout(false);
            this.setupPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.realEstatePanel)).EndInit();
            this.realEstatePanel.ResumeLayout(false);
            this.realEstatePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personalPropertyPanel)).EndInit();
            this.personalPropertyPanel.ResumeLayout(false);
            this.personalPropertyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.otherTaxPanel)).EndInit();
            this.otherTaxPanel.ResumeLayout(false);
            this.otherTaxPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.homesteadPanel)).EndInit();
            this.homesteadPanel.ResumeLayout(false);
            this.homesteadPanel.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
        private FCPanel betePanel;
        private FlowLayoutPanel flowLayoutPanel1;
        private FCPanel realEstatePanel;
        private FCPanel setupPanel;
        public FCTextBox fcTextBox3;
        public FCLabel fcLabel3;
        public FCTextBox fcTextBox5;
        public FCLabel fcLabel5;
        public FCTextBox fcTextBox7;
        public FCLabel fcLabel7;
        public FCTextBox fcTextBox8;
        public FCLabel fcLabel8;
        public FCTextBox fcTextBox12;
        public FCLabel fcLabel13;
        public FCTextBox fcTextBox6;
        public FCLabel fcLabel6;
        public FCTextBox fcTextBox9;
        public FCLabel fcLabel9;
        public FCLabel fcLabel12;
        private FCDateTimePicker dtCommitmentDate;
        public FCTextBox fcTextBox11;
        public FCLabel fcLabel11;
        public FCTextBox txtLDNBottomAdjust;
        public FCLabel Label7;
        public FCTextBox txtLDNTopAdjust;
        public FCLabel Label2;
        private FCPanel personalPropertyPanel;
        public FCTextBox fcTextBox13;
        public FCLabel fcLabel14;
        public FCTextBox fcTextBox2;
        public FCLabel fcLabel2;
        public FCTextBox fcTextBox4;
        public FCLabel fcLabel4;
        public FCTextBox fcTextBox10;
        public FCLabel fcLabel10;
        private FCPanel otherTaxPanel;
        public FCTextBox fcTextBox15;
        public FCLabel fcLabel16;
        public FCTextBox fcTextBox14;
        public FCLabel fcLabel15;
        public FCTextBox fcTextBox1;
        public FCLabel fcLabel1;
        private FCPanel homesteadPanel;
        public FCTextBox fcTextBox16;
        public FCLabel fcLabel23;
        public FCTextBox fcTextBox26;
        public FCTextBox fcTextBox19;
        public FCTextBox fcTextBox25;
        public FCLabel fcLabel20;
        public FCTextBox fcTextBox20;
        public FCLabel fcLabel21;
        public FCTextBox fcTextBox23;
        public FCLabel fcLabel22;
        public FCLabel fcLabel17;
        public FCTextBox fcTextBox17;
        public FCLabel fcLabel18;
        public FCLabel fcLabel19;
    }
}
