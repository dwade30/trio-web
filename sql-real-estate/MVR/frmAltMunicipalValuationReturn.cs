﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

//using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.IO;
using TWRE0000.MVR;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMVRViewer.
	/// </summary>
	public partial class frmAltMunicipalValuationReturn : BaseForm
	{
		public frmAltMunicipalValuationReturn()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            flowLayoutPanel1.Anchor =
                ((Wisej.Web.AnchorStyles) ((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right |
                                            Wisej.Web.AnchorStyles.Top)));
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAltMunicipalValuationReturn InstancePtr
		{
			get
			{
				return (frmAltMunicipalValuationReturn)Sys.GetInstance(typeof(frmAltMunicipalValuationReturn));
			}
		}

		protected frmAltMunicipalValuationReturn _InstancePtr = null;






		private void frmMVRViewer_Load(object sender, System.EventArgs e)
		{
            //fcPanel1.Controls.Add(new MVRPage1View());
            //flowLayoutPanel1.Controls.Add(new FCPanel()
            //{
            //    Text = "Tree Growth",
            //    Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Top))),
            //    AutoSize = false,
            //    ShowHeader = true,
            //    Size = new Size(968,50),
            //    Margin = new Padding(3)
            //});
            //flowLayoutPanel1.Controls.Add(new FCPanel()
            //{
            //    Text = "Exemptions",
            //    Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Top))),
            //    AutoSize = false,
            //    ShowHeader = true,
            //    Size = new Size(968, 50),
            //    Margin = new Padding(3)
            //});
            //flowLayoutPanel1.Controls.Add(new FCTextBox()
            //{
            //    Visible = true,
            //    Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Top))),
            //    AutoSize = false,
            //    Size = new Size(10,30),
            //    Enabled = false,
            //    BorderStyle = BorderStyle.None
            //});
        }

		private void frmMVRViewer_Resize(object sender, System.EventArgs e)
		{
			
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		

		private void GridValuation_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == Convert.ToByte(","[0]))
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		
		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

	


		

		



	}
}
