﻿namespace TWRE0000.MVR
{
    partial class MVRPage1View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtLDNBottomAdjust = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.txtLDNTopAdjust = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.fcTextBox2 = new fecherFoundation.FCTextBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.toolTip1 = new Wisej.Web.ToolTip(this.components);
            this.fcTextBox7 = new fecherFoundation.FCTextBox();
            this.fcLabel7 = new fecherFoundation.FCLabel();
            this.fcTextBox8 = new fecherFoundation.FCTextBox();
            this.fcLabel8 = new fecherFoundation.FCLabel();
            this.fcTextBox3 = new fecherFoundation.FCTextBox();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.fcTextBox5 = new fecherFoundation.FCTextBox();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.fcPanel1 = new fecherFoundation.FCPanel();
            this.fcTextBox6 = new fecherFoundation.FCTextBox();
            this.fcLabel6 = new fecherFoundation.FCLabel();
            this.fcTextBox9 = new fecherFoundation.FCTextBox();
            this.fcLabel9 = new fecherFoundation.FCLabel();
            this.fcFrame1 = new fecherFoundation.FCFrame();
            ((System.ComponentModel.ISupportInitialize)(this.fcPanel1)).BeginInit();
            this.fcPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLDNBottomAdjust
            // 
            this.txtLDNBottomAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNBottomAdjust.Location = new System.Drawing.Point(339, 63);
            this.txtLDNBottomAdjust.MaxLength = 5;
            this.txtLDNBottomAdjust.Name = "txtLDNBottomAdjust";
            this.txtLDNBottomAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNBottomAdjust.TabIndex = 13;
            this.txtLDNBottomAdjust.Text = "0";
            this.txtLDNBottomAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(16, 77);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(103, 17);
            this.Label7.TabIndex = 12;
            this.Label7.Text = "MUNICIPALITY";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLDNTopAdjust
            // 
            this.txtLDNTopAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNTopAdjust.Location = new System.Drawing.Point(339, 13);
            this.txtLDNTopAdjust.MaxLength = 5;
            this.txtLDNTopAdjust.Name = "txtLDNTopAdjust";
            this.txtLDNTopAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNTopAdjust.TabIndex = 11;
            this.txtLDNTopAdjust.Text = "0";
            this.txtLDNTopAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(16, 27);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(64, 17);
            this.Label2.TabIndex = 10;
            this.Label2.Text = "COUNTY";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox2
            // 
            this.fcTextBox2.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox2.Location = new System.Drawing.Point(339, 118);
            this.fcTextBox2.MaxLength = 5;
            this.fcTextBox2.Name = "fcTextBox2";
            this.fcTextBox2.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox2.TabIndex = 15;
            this.fcTextBox2.Text = "0";
            this.fcTextBox2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(16, 132);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(124, 17);
            this.fcLabel2.TabIndex = 14;
            this.fcLabel2.Text = "CERTIFIED RATIO";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox7
            // 
            this.fcTextBox7.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox7.Location = new System.Drawing.Point(339, 375);
            this.fcTextBox7.MaxLength = 5;
            this.fcTextBox7.Name = "fcTextBox7";
            this.fcTextBox7.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox7.TabIndex = 25;
            this.fcTextBox7.Text = "0";
            this.fcTextBox7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel7
            // 
            this.fcLabel7.AutoSize = true;
            this.fcLabel7.Location = new System.Drawing.Point(16, 389);
            this.fcLabel7.Name = "fcLabel7";
            this.fcLabel7.Size = new System.Drawing.Size(160, 17);
            this.fcLabel7.TabIndex = 24;
            this.fcLabel7.Text = "BUSINESS EQUIPMENT";
            this.fcLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.fcLabel7, "furniture, furnishings and equipment");
            // 
            // fcTextBox8
            // 
            this.fcTextBox8.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox8.Location = new System.Drawing.Point(339, 325);
            this.fcTextBox8.MaxLength = 5;
            this.fcTextBox8.Name = "fcTextBox8";
            this.fcTextBox8.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox8.TabIndex = 23;
            this.fcTextBox8.Text = "0";
            this.fcTextBox8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel8
            // 
            this.fcLabel8.AutoSize = true;
            this.fcLabel8.Location = new System.Drawing.Point(16, 339);
            this.fcLabel8.Name = "fcLabel8";
            this.fcLabel8.Size = new System.Drawing.Size(299, 17);
            this.fcLabel8.TabIndex = 22;
            this.fcLabel8.Text = "PRODUCTION MACHINERY AND EQUIPMENT";
            this.fcLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox3
            // 
            this.fcTextBox3.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox3.Location = new System.Drawing.Point(339, 476);
            this.fcTextBox3.MaxLength = 5;
            this.fcTextBox3.Name = "fcTextBox3";
            this.fcTextBox3.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox3.TabIndex = 29;
            this.fcTextBox3.Text = "0";
            this.fcTextBox3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(16, 490);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(152, 17);
            this.fcLabel3.TabIndex = 28;
            this.fcLabel3.Text = "PROPERTY TAX RATE";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.fcLabel3, "furniture, furnishings and equipment");
            // 
            // fcTextBox5
            // 
            this.fcTextBox5.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox5.Location = new System.Drawing.Point(339, 426);
            this.fcTextBox5.MaxLength = 5;
            this.fcTextBox5.Name = "fcTextBox5";
            this.fcTextBox5.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox5.TabIndex = 27;
            this.fcTextBox5.Text = "0";
            this.fcTextBox5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel5
            // 
            this.fcLabel5.AutoSize = true;
            this.fcLabel5.Location = new System.Drawing.Point(16, 440);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(240, 17);
            this.fcLabel5.TabIndex = 26;
            this.fcLabel5.Text = "ALL OTHER PERSONAL PROPERTY";
            this.fcLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcPanel1
            // 
            this.fcPanel1.Controls.Add(this.fcTextBox6);
            this.fcPanel1.Controls.Add(this.fcLabel6);
            this.fcPanel1.Controls.Add(this.fcTextBox9);
            this.fcPanel1.Controls.Add(this.fcLabel9);
            this.fcPanel1.Location = new System.Drawing.Point(16, 173);
            this.fcPanel1.Name = "fcPanel1";
            this.fcPanel1.ShowHeader = true;
            this.fcPanel1.Size = new System.Drawing.Size(408, 146);
            this.fcPanel1.TabIndex = 30;
            this.fcPanel1.Text = "Taxable Valuation of Real Estate";
            // 
            // fcTextBox6
            // 
            this.fcTextBox6.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox6.Location = new System.Drawing.Point(269, 57);
            this.fcTextBox6.MaxLength = 5;
            this.fcTextBox6.Name = "fcTextBox6";
            this.fcTextBox6.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox6.TabIndex = 23;
            this.fcTextBox6.Text = "0";
            this.fcTextBox6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel6
            // 
            this.fcLabel6.AutoSize = true;
            this.fcLabel6.Location = new System.Drawing.Point(15, 69);
            this.fcLabel6.Name = "fcLabel6";
            this.fcLabel6.Size = new System.Drawing.Size(73, 17);
            this.fcLabel6.TabIndex = 22;
            this.fcLabel6.Text = "BUILDING";
            this.fcLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcTextBox9
            // 
            this.fcTextBox9.BackColor = System.Drawing.SystemColors.Window;
            this.fcTextBox9.Location = new System.Drawing.Point(269, 2);
            this.fcTextBox9.MaxLength = 5;
            this.fcTextBox9.Name = "fcTextBox9";
            this.fcTextBox9.Size = new System.Drawing.Size(70, 40);
            this.fcTextBox9.TabIndex = 21;
            this.fcTextBox9.Text = "0";
            this.fcTextBox9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel9
            // 
            this.fcLabel9.AutoSize = true;
            this.fcLabel9.Location = new System.Drawing.Point(15, 15);
            this.fcLabel9.Name = "fcLabel9";
            this.fcLabel9.Size = new System.Drawing.Size(45, 17);
            this.fcLabel9.TabIndex = 20;
            this.fcLabel9.Text = "LAND";
            this.fcLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.fcLabel9, "Include value of transmission, distribution lines and substations, dams and power" +
        " houses");
            // 
            // fcFrame1
            // 
            this.fcFrame1.Location = new System.Drawing.Point(481, 173);
            this.fcFrame1.Name = "fcFrame1";
            this.fcFrame1.Size = new System.Drawing.Size(250, 162);
            this.fcFrame1.TabIndex = 31;
            this.fcFrame1.Text = "Taxable Valuation Of Real Estate";
            // 
            // MVRPage1View
            // 
            this.Controls.Add(this.fcFrame1);
            this.Controls.Add(this.fcPanel1);
            this.Controls.Add(this.fcTextBox3);
            this.Controls.Add(this.fcLabel3);
            this.Controls.Add(this.fcTextBox5);
            this.Controls.Add(this.fcLabel5);
            this.Controls.Add(this.fcTextBox7);
            this.Controls.Add(this.fcLabel7);
            this.Controls.Add(this.fcTextBox8);
            this.Controls.Add(this.fcLabel8);
            this.Controls.Add(this.fcTextBox2);
            this.Controls.Add(this.fcLabel2);
            this.Controls.Add(this.txtLDNBottomAdjust);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.txtLDNTopAdjust);
            this.Controls.Add(this.Label2);
            this.Name = "MVRPage1View";
            this.Size = new System.Drawing.Size(802, 578);
            ((System.ComponentModel.ISupportInitialize)(this.fcPanel1)).EndInit();
            this.fcPanel1.ResumeLayout(false);
            this.fcPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public fecherFoundation.FCTextBox txtLDNBottomAdjust;
        public fecherFoundation.FCLabel Label7;
        public fecherFoundation.FCTextBox txtLDNTopAdjust;
        public fecherFoundation.FCLabel Label2;
        public fecherFoundation.FCTextBox fcTextBox2;
        public fecherFoundation.FCLabel fcLabel2;
        private Wisej.Web.ToolTip toolTip1;
        public fecherFoundation.FCTextBox fcTextBox7;
        public fecherFoundation.FCLabel fcLabel7;
        public fecherFoundation.FCTextBox fcTextBox8;
        public fecherFoundation.FCLabel fcLabel8;
        public fecherFoundation.FCTextBox fcTextBox3;
        public fecherFoundation.FCLabel fcLabel3;
        public fecherFoundation.FCTextBox fcTextBox5;
        public fecherFoundation.FCLabel fcLabel5;
        private fecherFoundation.FCPanel fcPanel1;
        public fecherFoundation.FCTextBox fcTextBox6;
        public fecherFoundation.FCLabel fcLabel6;
        public fecherFoundation.FCTextBox fcTextBox9;
        public fecherFoundation.FCLabel fcLabel9;
        private fecherFoundation.FCFrame fcFrame1;
    }
}
