﻿//Fecher vbPorter - Version 1.0.0.40
using System;//Fecher vbPorter - Version 1.0.0.4frmNewMunicipalValuationReturn0
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

//using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMVRViewer.
	/// </summary>
	partial class frmMunicipalValuationReturn : BaseForm
	{
		public fecherFoundation.FCLine LineRatio;
		public fecherFoundation.FCButton cmdLoadLast;
		public fecherFoundation.FCButton cmdExcelDoc;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{

			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMunicipalValuationReturn));
			this.LineRatio = new fecherFoundation.FCLine();
			this.cmdLoadLast = new fecherFoundation.FCButton();
			this.cmdExcelDoc = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.fcLabel23 = new fecherFoundation.FCLabel();
			this.fcLabel20 = new fecherFoundation.FCLabel();
			this.fcLabel22 = new fecherFoundation.FCLabel();
			this.fcLabel17 = new fecherFoundation.FCLabel();
			this.fcLabel19 = new fecherFoundation.FCLabel();
			this.fcLabel16 = new fecherFoundation.FCLabel();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.fcLabel4 = new fecherFoundation.FCLabel();
			this.fcLabel9 = new fecherFoundation.FCLabel();
			this.fcLabel3 = new fecherFoundation.FCLabel();
			this.fcLabel7 = new fecherFoundation.FCLabel();
			this.fcLabel24 = new fecherFoundation.FCLabel();
			this.fcLabel26 = new fecherFoundation.FCLabel();
			this.fcLabel29 = new fecherFoundation.FCLabel();
			this.fcLabel31 = new fecherFoundation.FCLabel();
			this.fcLabel33 = new fecherFoundation.FCLabel();
			this.lblForestParcelsClassified = new fecherFoundation.FCLabel();
			this.fcLabel35 = new fecherFoundation.FCLabel();
			this.fcLabel38 = new fecherFoundation.FCLabel();
			this.fcLabel41 = new fecherFoundation.FCLabel();
			this.fcLabel42 = new fecherFoundation.FCLabel();
			this.fcLabel43 = new fecherFoundation.FCLabel();
			this.lblTotalForestParcelsWithdrawn = new fecherFoundation.FCLabel();
			this.fcLabel45 = new fecherFoundation.FCLabel();
			this.fcLabel46 = new fecherFoundation.FCLabel();
			this.fcLabel48 = new fecherFoundation.FCLabel();
			this.fcLabel51 = new fecherFoundation.FCLabel();
			this.fcLabel52 = new fecherFoundation.FCLabel();
			this.fcLabel54 = new fecherFoundation.FCLabel();
			this.fcLabel57 = new fecherFoundation.FCLabel();
			this.fcLabel58 = new fecherFoundation.FCLabel();
			this.fcLabel60 = new fecherFoundation.FCLabel();
			this.fcLabel64 = new fecherFoundation.FCLabel();
			this.fcLabel68 = new fecherFoundation.FCLabel();
			this.fcLabel69 = new fecherFoundation.FCLabel();
			this.fcLabel71 = new fecherFoundation.FCLabel();
			this.fcLabel74 = new fecherFoundation.FCLabel();
			this.fcLabel77 = new fecherFoundation.FCLabel();
			this.fcLabel78 = new fecherFoundation.FCLabel();
			this.fcLabel80 = new fecherFoundation.FCLabel();
			this.fcLabel139 = new fecherFoundation.FCLabel();
			this.fcLabel144 = new fecherFoundation.FCLabel();
			this.fcLabel153 = new fecherFoundation.FCLabel();
			this.tabControl1 = new fecherFoundation.FCTabControl();
			this.tabPage1 = new fecherFoundation.FCTabPage();
			this.fcFrame4 = new fecherFoundation.FCFrame();
			this.txtTotalHomesteadAssessments = new fecherFoundation.FCTextBox();
			this.txtTotalNumberHomesteads = new fecherFoundation.FCTextBox();
			this.txtHomesteadTotalExemptions = new fecherFoundation.FCTextBox();
			this.txtNumberHomesteadFullyExempt = new fecherFoundation.FCTextBox();
			this.txtHomesteadFullyExemptValue = new fecherFoundation.FCTextBox();
			this.fcLabel21 = new fecherFoundation.FCLabel();
			this.txtTotalFullHomesteadExemptions = new fecherFoundation.FCTextBox();
			this.txtTotalFullHomesteadsGranted = new fecherFoundation.FCTextBox();
			this.fcLabel18 = new fecherFoundation.FCLabel();
			this.fcFrame3 = new fecherFoundation.FCFrame();
			this.txtTaxLevy = new fecherFoundation.FCTextBox();
			this.txtTotalTaxableValuation = new fecherFoundation.FCTextBox();
			this.fcLabel15 = new fecherFoundation.FCLabel();
			this.txtPropertyTaxRate = new fecherFoundation.FCTextBox();
			this.fcFrame2 = new fecherFoundation.FCFrame();
			this.txtTotalTaxablePersonalProperty = new fecherFoundation.FCTextBox();
			this.fcLabel14 = new fecherFoundation.FCLabel();
			this.txtOtherPersonalProperty = new fecherFoundation.FCTextBox();
			this.fcLabel2 = new fecherFoundation.FCLabel();
			this.txtBusinessEquipment = new fecherFoundation.FCTextBox();
			this.txtMachineryAndEquipment = new fecherFoundation.FCTextBox();
			this.fcLabel10 = new fecherFoundation.FCLabel();
			this.fcFrame1 = new fecherFoundation.FCFrame();
			this.txtTotalTaxableRealEstate = new fecherFoundation.FCTextBox();
			this.fcLabel13 = new fecherFoundation.FCLabel();
			this.txtTaxableBuilding = new fecherFoundation.FCTextBox();
			this.fcLabel6 = new fecherFoundation.FCLabel();
			this.txtTaxableLand = new fecherFoundation.FCTextBox();
			this.fcLabel12 = new fecherFoundation.FCLabel();
			this.dtCommitmentDate = new fecherFoundation.FCDateTimePicker();
			this.txtCertifiedRatio = new fecherFoundation.FCTextBox();
			this.fcLabel11 = new fecherFoundation.FCLabel();
			this.txtMunicipality = new fecherFoundation.FCTextBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.txtCounty = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.tabPage2 = new fecherFoundation.FCTabPage();
			this.fcFrame9 = new fecherFoundation.FCFrame();
			this.txtForestHardwoodRate = new fecherFoundation.FCTextBox();
			this.fcLabel40 = new fecherFoundation.FCLabel();
			this.txtForestMixedwoodRate = new fecherFoundation.FCTextBox();
			this.txtForestSoftwoodRate = new fecherFoundation.FCTextBox();
			this.txtForestTotalAssessed = new fecherFoundation.FCTextBox();
			this.lblTotalValuationForestLand = new fecherFoundation.FCLabel();
			this.txtForestNumberOfAcres = new fecherFoundation.FCTextBox();
			this.txtForestHardwoodAcreage = new fecherFoundation.FCTextBox();
			this.fcLabel39 = new fecherFoundation.FCLabel();
			this.txtForestMixedWoodAcreage = new fecherFoundation.FCTextBox();
			this.txtForestSoftwoodAcreage = new fecherFoundation.FCTextBox();
			this.fcLabel34 = new fecherFoundation.FCLabel();
			this.txtForestNumberOfParcels = new fecherFoundation.FCTextBox();
			this.txtForestAveragePerAcre = new fecherFoundation.FCTextBox();
			this.fcLabel36 = new fecherFoundation.FCLabel();
			this.fcFrame8 = new fecherFoundation.FCFrame();
			this.txtElectricalGenerationFacilities = new fecherFoundation.FCTextBox();
			this.txtTransmissionLines = new fecherFoundation.FCTextBox();
			this.fcLabel32 = new fecherFoundation.FCLabel();
			this.fcFrame7 = new fecherFoundation.FCFrame();
			this.cmbExcisePeriodType = new fecherFoundation.FCComboBox();
			this.txtWatercraftExcise = new fecherFoundation.FCTextBox();
			this.fcLabel28 = new fecherFoundation.FCLabel();
			this.txtMVExcise = new fecherFoundation.FCTextBox();
			this.fcLabel30 = new fecherFoundation.FCLabel();
			this.fcFrame6 = new fecherFoundation.FCFrame();
			this.txtTifBeteDeposited = new fecherFoundation.FCTextBox();
			this.txtTifDeposited = new fecherFoundation.FCTextBox();
			this.fcLabel25 = new fecherFoundation.FCLabel();
			this.txtTifCaptured = new fecherFoundation.FCTextBox();
			this.txtIncreasedTif = new fecherFoundation.FCTextBox();
			this.fcLabel27 = new fecherFoundation.FCLabel();
			this.fcFrame5 = new fecherFoundation.FCFrame();
			this.txtBeteInTIF = new fecherFoundation.FCTextBox();
			this.txtExemptValueBeteQualified = new fecherFoundation.FCTextBox();
			this.fcLabel5 = new fecherFoundation.FCLabel();
			this.txtNumberBeteApproved = new fecherFoundation.FCTextBox();
			this.txtNumberBeteApplicationsProcessed = new fecherFoundation.FCTextBox();
			this.fcLabel8 = new fecherFoundation.FCLabel();
			this.fcTabPage1 = new fecherFoundation.FCTabPage();
			this.fcFrame11 = new fecherFoundation.FCFrame();
			this.txtOpenTotalValuation = new fecherFoundation.FCTextBox();
			this.fcLabel56 = new fecherFoundation.FCLabel();
			this.txtOpenTotalAcres = new fecherFoundation.FCTextBox();
			this.txtOpenAcresFirstClassified = new fecherFoundation.FCTextBox();
			this.txtOpenParcelsFirstClassified = new fecherFoundation.FCTextBox();
			this.fcLabel59 = new fecherFoundation.FCLabel();
			this.txtFarmTotalPenalties = new fecherFoundation.FCTextBox();
			this.txtFarmAcresWithdrawn = new fecherFoundation.FCTextBox();
			this.fcLabel61 = new fecherFoundation.FCLabel();
			this.txtFarmParcelsWithdrawn = new fecherFoundation.FCTextBox();
			this.fcLabel62 = new fecherFoundation.FCLabel();
			this.txtFarmHardwoodRate = new fecherFoundation.FCTextBox();
			this.fcLabel50 = new fecherFoundation.FCLabel();
			this.txtFarmMixedWoodRate = new fecherFoundation.FCTextBox();
			this.txtFarmSoftwoodRate = new fecherFoundation.FCTextBox();
			this.txtFarmTotalValuationWoodland = new fecherFoundation.FCTextBox();
			this.fcLabel53 = new fecherFoundation.FCLabel();
			this.txtFarmTotalAcresWoodland = new fecherFoundation.FCTextBox();
			this.txtFarmHardwoodAcres = new fecherFoundation.FCTextBox();
			this.fcLabel55 = new fecherFoundation.FCLabel();
			this.txtFarmMixedWoodAcres = new fecherFoundation.FCTextBox();
			this.fcLabel44 = new fecherFoundation.FCLabel();
			this.txtFarmSoftwoodAcres = new fecherFoundation.FCTextBox();
			this.txtFarmTotalValuationTillable = new fecherFoundation.FCTextBox();
			this.txtFarmTotalAcresTillable = new fecherFoundation.FCTextBox();
			this.fcLabel47 = new fecherFoundation.FCLabel();
			this.txtFarmNumberOfAcresFirstClassified = new fecherFoundation.FCTextBox();
			this.txtFarmNumberOfParcelsFirstClassified = new fecherFoundation.FCTextBox();
			this.fcLabel49 = new fecherFoundation.FCLabel();
			this.fcFrame10 = new fecherFoundation.FCFrame();
			this.chkForestTransferredToFarmland = new fecherFoundation.FCCheckBox();
			this.fcLabel37 = new fecherFoundation.FCLabel();
			this.txtForestNumberOfNonCompliancePenalties = new fecherFoundation.FCTextBox();
			this.txtForestAmountOfPenalties = new fecherFoundation.FCTextBox();
			this.txtForestAcresWithdrawn = new fecherFoundation.FCTextBox();
			this.lblForestAcresWithdrawn = new fecherFoundation.FCLabel();
			this.txtForestParcelsWithdrawn = new fecherFoundation.FCTextBox();
			this.txtForestAcresFirstClassifiedThisYear = new fecherFoundation.FCTextBox();
			this.lblNumberForestAcres = new fecherFoundation.FCLabel();
			this.fcTabPage2 = new fecherFoundation.FCTabPage();
			this.fcFrame14 = new fecherFoundation.FCFrame();
			this.txtExemptSewage = new fecherFoundation.FCTextBox();
			this.txtExemptPrivateAirport = new fecherFoundation.FCTextBox();
			this.fcLabel75 = new fecherFoundation.FCLabel();
			this.txtExemptMunicipalAirport = new fecherFoundation.FCTextBox();
			this.fcLabel76 = new fecherFoundation.FCLabel();
			this.txtExemptMunicipalUtility = new fecherFoundation.FCTextBox();
			this.txtExemptMunicipal = new fecherFoundation.FCTextBox();
			this.txtExemptNHWater = new fecherFoundation.FCTextBox();
			this.fcLabel79 = new fecherFoundation.FCLabel();
			this.txtExemptMaine = new fecherFoundation.FCTextBox();
			this.txtExemptFederal = new fecherFoundation.FCTextBox();
			this.fcLabel81 = new fecherFoundation.FCLabel();
			this.fcFrame13 = new fecherFoundation.FCFrame();
			this.txtWaterfrontPenalties = new fecherFoundation.FCTextBox();
			this.fcLabel66 = new fecherFoundation.FCLabel();
			this.txtWaterfrontAcresWithdrawn = new fecherFoundation.FCTextBox();
			this.fcLabel67 = new fecherFoundation.FCLabel();
			this.txtWaterfrontParcelsWithdrawn = new fecherFoundation.FCTextBox();
			this.txtWaterfrontTotalValuation = new fecherFoundation.FCTextBox();
			this.txtWaterfrontTotalAcreage = new fecherFoundation.FCTextBox();
			this.fcLabel70 = new fecherFoundation.FCLabel();
			this.txtWaterfrontAcresFirstClassified = new fecherFoundation.FCTextBox();
			this.txtWaterfrontParcelsFirstClassified = new fecherFoundation.FCTextBox();
			this.fcLabel72 = new fecherFoundation.FCLabel();
			this.fcFrame12 = new fecherFoundation.FCFrame();
			this.txtOpenTotalAmountOfPenalties = new fecherFoundation.FCTextBox();
			this.fcLabel63 = new fecherFoundation.FCLabel();
			this.txtOpenAcresWithdrawn = new fecherFoundation.FCTextBox();
			this.txtOpenParcelsWithdrawn = new fecherFoundation.FCTextBox();
			this.fcLabel65 = new fecherFoundation.FCLabel();
			this.fcTabPage3 = new fecherFoundation.FCTabPage();
			this.fcFrame15 = new fecherFoundation.FCFrame();
			this.txtExemptSnowmobileGroomingEquipment = new fecherFoundation.FCTextBox();
			this.fcLabel91 = new fecherFoundation.FCLabel();
			this.txtExemptPollutionControl = new fecherFoundation.FCTextBox();
			this.fcLabel92 = new fecherFoundation.FCLabel();
			this.txtExemptAnimalWasteStorage = new fecherFoundation.FCTextBox();
			this.fcLabel93 = new fecherFoundation.FCLabel();
			this.txtExemptWaterSupplyTransport = new fecherFoundation.FCTextBox();
			this.fcLabel94 = new fecherFoundation.FCLabel();
			this.txtExemptLegallyBlind = new fecherFoundation.FCTextBox();
			this.fcLabel95 = new fecherFoundation.FCLabel();
			this.txtExemptHospitalPersonalProperty = new fecherFoundation.FCTextBox();
			this.fcLabel96 = new fecherFoundation.FCLabel();
			this.txtExemptFraternal = new fecherFoundation.FCTextBox();
			this.fcLabel87 = new fecherFoundation.FCLabel();
			this.txtExemptReligiousWorship = new fecherFoundation.FCTextBox();
			this.fcLabel88 = new fecherFoundation.FCLabel();
			this.txtExemptTaxableValueOfParsonages = new fecherFoundation.FCTextBox();
			this.fcLabel89 = new fecherFoundation.FCLabel();
			this.txtExemptValueOfParsonages = new fecherFoundation.FCTextBox();
			this.fcLabel90 = new fecherFoundation.FCLabel();
			this.txtExemptParsonages = new fecherFoundation.FCTextBox();
			this.fcLabel73 = new fecherFoundation.FCLabel();
			this.txtExemptChamberOfCommerce = new fecherFoundation.FCTextBox();
			this.fcLabel82 = new fecherFoundation.FCLabel();
			this.txtExemptVetOrgReimbursable = new fecherFoundation.FCTextBox();
			this.fcLabel83 = new fecherFoundation.FCLabel();
			this.txtExemptVeteransOrganizations = new fecherFoundation.FCTextBox();
			this.fcLabel84 = new fecherFoundation.FCLabel();
			this.txtExemptLiteraryAndScientific = new fecherFoundation.FCTextBox();
			this.fcLabel85 = new fecherFoundation.FCLabel();
			this.txtExemptBenevolent = new fecherFoundation.FCTextBox();
			this.fcLabel86 = new fecherFoundation.FCLabel();
			this.fcTabPage4 = new fecherFoundation.FCTabPage();
			this.fcFrame18 = new fecherFoundation.FCFrame();
			this.txtExemptVetVietnamValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetLebanonPanamaValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetVietnamCount = new fecherFoundation.FCTextBox();
			this.fcLabel109 = new fecherFoundation.FCLabel();
			this.txtExemptVetLebanonPanamaCount = new fecherFoundation.FCTextBox();
			this.fcLabel110 = new fecherFoundation.FCLabel();
			this.txtExemptVetDisabledValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetDisabledCount = new fecherFoundation.FCTextBox();
			this.fcLabel111 = new fecherFoundation.FCLabel();
			this.fcFrame17 = new fecherFoundation.FCFrame();
			this.txtExemptVetNonResidentValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetResidentValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetNonResidentCount = new fecherFoundation.FCTextBox();
			this.fcLabel107 = new fecherFoundation.FCLabel();
			this.txtExemptVetResidentCount = new fecherFoundation.FCTextBox();
			this.fcLabel108 = new fecherFoundation.FCLabel();
			this.txtExemptVetCoopValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetParaplegicValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetWWINonResidentValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetWWIResidentValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetLivingTrustValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetParaplegicLivingTrustValue = new fecherFoundation.FCTextBox();
			this.txtExemptVetSurvivingMaleValue = new fecherFoundation.FCTextBox();
			this.fcLabel106 = new fecherFoundation.FCLabel();
			this.fcLabel105 = new fecherFoundation.FCLabel();
			this.txtExemptVetCoopCount = new fecherFoundation.FCTextBox();
			this.fcLabel98 = new fecherFoundation.FCLabel();
			this.txtExemptVetParaplegicCount = new fecherFoundation.FCTextBox();
			this.fcLabel99 = new fecherFoundation.FCLabel();
			this.txtExemptVetWWINonResidentCount = new fecherFoundation.FCTextBox();
			this.fcLabel100 = new fecherFoundation.FCLabel();
			this.txtExemptVetWWIResidentCount = new fecherFoundation.FCTextBox();
			this.fcLabel101 = new fecherFoundation.FCLabel();
			this.txtExemptVetLivingTrustCount = new fecherFoundation.FCTextBox();
			this.fcLabel102 = new fecherFoundation.FCLabel();
			this.txtExemptVetParaplegicLivingTrustCount = new fecherFoundation.FCTextBox();
			this.fcLabel103 = new fecherFoundation.FCLabel();
			this.txtExemptVetSurvivingMaleCount = new fecherFoundation.FCTextBox();
			this.fcLabel104 = new fecherFoundation.FCLabel();
			this.fcTabPage5 = new fecherFoundation.FCTabPage();
			this.fcFrame19 = new fecherFoundation.FCFrame();
			this.chkHasTaxMaps = new fecherFoundation.FCCheckBox();
			this.txtRevaluationCost = new fecherFoundation.FCTextBox();
			this.fcLabel126 = new fecherFoundation.FCLabel();
			this.txtRevaluationContractorName = new fecherFoundation.FCTextBox();
			this.fcLabel124 = new fecherFoundation.FCLabel();
			this.dtRevaluationEffectiveDate = new fecherFoundation.FCDateTimePicker();
			this.fcLabel125 = new fecherFoundation.FCLabel();
			this.chkRevaluationIncludedPersonalProperty = new fecherFoundation.FCCheckBox();
			this.fcLabel122 = new fecherFoundation.FCLabel();
			this.chkRevaluationIncludedBuildings = new fecherFoundation.FCCheckBox();
			this.fcLabel123 = new fecherFoundation.FCLabel();
			this.chkRevaluationIncludedLand = new fecherFoundation.FCCheckBox();
			this.fcLabel121 = new fecherFoundation.FCLabel();
			this.chkRevaluationCompleted = new fecherFoundation.FCCheckBox();
			this.fcLabel120 = new fecherFoundation.FCLabel();
			this.txtMunicipalRecordsTotalTaxableAcreage = new fecherFoundation.FCTextBox();
			this.fcLabel119 = new fecherFoundation.FCLabel();
			this.txtMunicipalRecordsTotalNumberOfParcels = new fecherFoundation.FCTextBox();
			this.fcLabel118 = new fecherFoundation.FCLabel();
			this.cmbTaxMapType = new fecherFoundation.FCComboBox();
			this.fcLabel117 = new fecherFoundation.FCLabel();
			this.txtNameOfOriginalContractor = new fecherFoundation.FCTextBox();
			this.fcLabel116 = new fecherFoundation.FCLabel();
			this.dtMapsOriginallyObtained = new fecherFoundation.FCDateTimePicker();
			this.fcLabel115 = new fecherFoundation.FCLabel();
			this.fcLabel114 = new fecherFoundation.FCLabel();
			this.fcFrame16 = new fecherFoundation.FCFrame();
			this.fcLabel154 = new fecherFoundation.FCLabel();
			this.fcLabel148 = new fecherFoundation.FCLabel();
			this.txtExemptSolarAndWindValue = new fecherFoundation.FCTextBox();
			this.txtExemptOrganization3ExemptValue = new fecherFoundation.FCTextBox();
			this.txtExemptProvisionOfLaw3 = new fecherFoundation.FCTextBox();
			this.txtExemptOrganization3Name = new fecherFoundation.FCTextBox();
			this.txtExemptOrganization2ExemptValue = new fecherFoundation.FCTextBox();
			this.txtExemptSolarAndWindApproved = new fecherFoundation.FCTextBox();
			this.txtExemptProvisionOfLaw2 = new fecherFoundation.FCTextBox();
			this.txtExemptOrganization2Name = new fecherFoundation.FCTextBox();
			this.fcLabel149 = new fecherFoundation.FCLabel();
			this.fcLabel113 = new fecherFoundation.FCLabel();
			this.fcLabel112 = new fecherFoundation.FCLabel();
			this.txtExemptSolarAndWindProcessed = new fecherFoundation.FCTextBox();
			this.fcLabel97 = new fecherFoundation.FCLabel();
			this.txtExemptOrganization1ExemptValue = new fecherFoundation.FCTextBox();
			this.fcLabel150 = new fecherFoundation.FCLabel();
			this.txtExemptProvisionOfLaw1 = new fecherFoundation.FCTextBox();
			this.txtExemptOrganization1Name = new fecherFoundation.FCTextBox();
			this.fcTabPage6 = new fecherFoundation.FCTabPage();
			this.fcFrame20 = new fecherFoundation.FCFrame();
			this.dtpSignatureDate = new fecherFoundation.FCDateTimePicker();
			this.fcLabel155 = new fecherFoundation.FCLabel();
			this.txtAmountOfSeniorTaxDeferral = new fecherFoundation.FCTextBox();
			this.fcLabel151 = new fecherFoundation.FCLabel();
			this.txtNumberQualifiedForSeniorDeferral = new fecherFoundation.FCTextBox();
			this.fcLabel152 = new fecherFoundation.FCLabel();
			this.chkSeniorTaxDeferral = new fecherFoundation.FCCheckBox();
			this.txtAmountOfElderlyTaxCredit = new fecherFoundation.FCTextBox();
			this.fcLabel142 = new fecherFoundation.FCLabel();
			this.txtNumberQualifiedForElderlyTaxCredit = new fecherFoundation.FCTextBox();
			this.fcLabel143 = new fecherFoundation.FCLabel();
			this.chkElderlyTaxCredit = new fecherFoundation.FCCheckBox();
			this.txtAmountOfTaxRelief = new fecherFoundation.FCTextBox();
			this.fcLabel140 = new fecherFoundation.FCLabel();
			this.txtNumberQualifiedForTaxRelief = new fecherFoundation.FCTextBox();
			this.fcLabel141 = new fecherFoundation.FCLabel();
			this.chkTaxReliefProgram = new fecherFoundation.FCCheckBox();
			this.txtAssessmentSoftware = new fecherFoundation.FCTextBox();
			this.fcLabel138 = new fecherFoundation.FCLabel();
			this.chkAssessmentRecordsAreComputerized = new fecherFoundation.FCCheckBox();
			this.fcLabel137 = new fecherFoundation.FCLabel();
			this.dtpPeriod4Due = new fecherFoundation.FCDateTimePicker();
			this.fcLabel135 = new fecherFoundation.FCLabel();
			this.dtpPeriod3Due = new fecherFoundation.FCDateTimePicker();
			this.fcLabel136 = new fecherFoundation.FCLabel();
			this.dtpPeriod2Due = new fecherFoundation.FCDateTimePicker();
			this.fcLabel133 = new fecherFoundation.FCLabel();
			this.dtpPeriod1Due = new fecherFoundation.FCDateTimePicker();
			this.fcLabel134 = new fecherFoundation.FCLabel();
			this.txtInterestRate = new fecherFoundation.FCTextBox();
			this.fcLabel132 = new fecherFoundation.FCLabel();
			this.dtpFiscalEnd = new fecherFoundation.FCDateTimePicker();
			this.fcLabel131 = new fecherFoundation.FCLabel();
			this.dtpFiscalStart = new fecherFoundation.FCDateTimePicker();
			this.fcLabel130 = new fecherFoundation.FCLabel();
			this.txtAssessorEmail = new fecherFoundation.FCTextBox();
			this.fcLabel129 = new fecherFoundation.FCLabel();
			this.txtAssessorName = new fecherFoundation.FCTextBox();
			this.fcLabel128 = new fecherFoundation.FCLabel();
			this.cmbAssessorFunctionality = new fecherFoundation.FCComboBox();
			this.fcLabel127 = new fecherFoundation.FCLabel();
			this.fcTabPage7 = new fecherFoundation.FCTabPage();
			this.txtGeneralChangeLine10 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine9 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine8 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine7 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine6 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine5 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine4 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine3 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine2 = new fecherFoundation.FCTextBox();
			this.txtGeneralChangeLine1 = new fecherFoundation.FCTextBox();
			this.fcLabel147 = new fecherFoundation.FCLabel();
			this.txtExtremeLossesLine8 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine7 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine6 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine5 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine4 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine3 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine2 = new fecherFoundation.FCTextBox();
			this.txtExtremeLossesLine1 = new fecherFoundation.FCTextBox();
			this.fcLabel146 = new fecherFoundation.FCLabel();
			this.txtIndustrialGrowthLine8 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine7 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine6 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine5 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine4 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine3 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine2 = new fecherFoundation.FCTextBox();
			this.txtIndustrialGrowthLine1 = new fecherFoundation.FCTextBox();
			this.fcLabel145 = new fecherFoundation.FCLabel();
			this.GridResidentialBuildings = new fecherFoundation.FCGrid();
			this.javaScript1 = new Wisej.Web.JavaScript(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExcelDoc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame4)).BeginInit();
			this.fcFrame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame3)).BeginInit();
			this.fcFrame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame2)).BeginInit();
			this.fcFrame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).BeginInit();
			this.fcFrame1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame9)).BeginInit();
			this.fcFrame9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame8)).BeginInit();
			this.fcFrame8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame7)).BeginInit();
			this.fcFrame7.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame6)).BeginInit();
			this.fcFrame6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame5)).BeginInit();
			this.fcFrame5.SuspendLayout();
			this.fcTabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame11)).BeginInit();
			this.fcFrame11.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame10)).BeginInit();
			this.fcFrame10.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkForestTransferredToFarmland)).BeginInit();
			this.fcTabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame14)).BeginInit();
			this.fcFrame14.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame13)).BeginInit();
			this.fcFrame13.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame12)).BeginInit();
			this.fcFrame12.SuspendLayout();
			this.fcTabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame15)).BeginInit();
			this.fcFrame15.SuspendLayout();
			this.fcTabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame18)).BeginInit();
			this.fcFrame18.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame17)).BeginInit();
			this.fcFrame17.SuspendLayout();
			this.fcTabPage5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame19)).BeginInit();
			this.fcFrame19.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHasTaxMaps)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationIncludedPersonalProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationIncludedBuildings)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationIncludedLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationCompleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame16)).BeginInit();
			this.fcFrame16.SuspendLayout();
			this.fcTabPage6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame20)).BeginInit();
			this.fcFrame20.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSeniorTaxDeferral)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkElderlyTaxCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxReliefProgram)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAssessmentRecordsAreComputerized)).BeginInit();
			this.fcTabPage7.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridResidentialBuildings)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 601);
			this.BottomPanel.Size = new System.Drawing.Size(978, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.AutoSize = true;
			this.ClientArea.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
			this.ClientArea.Controls.Add(this.tabControl1);
			this.ClientArea.Controls.Add(this.LineRatio);
			this.ClientArea.Size = new System.Drawing.Size(998, 469);
			this.ClientArea.Controls.SetChildIndex(this.LineRatio, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.tabControl1, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdExcelDoc);
			this.TopPanel.Controls.Add(this.cmdLoadLast);
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(998, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoadLast, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdExcelDoc, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// LineRatio
			// 
			this.LineRatio.Name = "LineRatio";
			this.LineRatio.Size = new System.Drawing.Size(1440, 1);
			this.LineRatio.Visible = false;
			this.LineRatio.X2 = 1440F;
			// 
			// cmdLoadLast
			// 
			this.cmdLoadLast.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoadLast.Cursor = Wisej.Web.Cursors.Default;
			this.cmdLoadLast.Location = new System.Drawing.Point(583, 29);
			this.cmdLoadLast.Name = "cmdLoadLast";
			this.cmdLoadLast.Size = new System.Drawing.Size(117, 24);
			this.cmdLoadLast.TabIndex = 1;
			this.cmdLoadLast.Text = "Load Last Saved";
			// 
			// cmdExcelDoc
			// 
			this.cmdExcelDoc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdExcelDoc.Cursor = Wisej.Web.Cursors.Default;
			this.cmdExcelDoc.Enabled = false;
			this.cmdExcelDoc.Location = new System.Drawing.Point(704, 29);
			this.cmdExcelDoc.Name = "cmdExcelDoc";
			this.cmdExcelDoc.Size = new System.Drawing.Size(218, 24);
			this.cmdExcelDoc.TabIndex = 2;
			this.cmdExcelDoc.Text = "Create Excel Document for State";
			this.cmdExcelDoc.Visible = false;
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPrint.Location = new System.Drawing.Point(925, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(46, 24);
			this.cmdPrint.TabIndex = 5;
			this.cmdPrint.Text = "Print";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSave.Location = new System.Drawing.Point(421, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			// 
			// fcLabel23
			// 
			this.fcLabel23.AutoSize = true;
			this.fcLabel23.Location = new System.Drawing.Point(6, 346);
			this.fcLabel23.Name = "fcLabel23";
			this.fcLabel23.Size = new System.Drawing.Size(483, 15);
			this.fcLabel23.TabIndex = 68;
			this.fcLabel23.Text = "14G - TOTAL ASSESSED VALUE OF ALL HOMESTEAD QUALIFIED PROPERTY";
			this.fcLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel23, "furniture, furnishings and equipment");
			// 
			// fcLabel20
			// 
			this.fcLabel20.AutoSize = true;
			this.fcLabel20.Location = new System.Drawing.Point(6, 294);
			this.fcLabel20.Name = "fcLabel20";
			this.fcLabel20.Size = new System.Drawing.Size(482, 15);
			this.fcLabel20.TabIndex = 66;
			this.fcLabel20.Text = "14F - TOTAL EXEMPT VALUE FOR ALL HOMESTEAD EXEMPTIONS GRANTED";
			this.fcLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel20, "furniture, furnishings and equipment");
			// 
			// fcLabel22
			// 
			this.fcLabel22.AutoSize = true;
			this.fcLabel22.Location = new System.Drawing.Point(6, 242);
			this.fcLabel22.Name = "fcLabel22";
			this.fcLabel22.Size = new System.Drawing.Size(404, 15);
			this.fcLabel22.TabIndex = 62;
			this.fcLabel22.Text = "14E - TOTAL NUMBER OF HOMESTEAD EXEMPTIONS GRANTED";
			this.fcLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel22, "furniture, furnishings and equipment");
			// 
			// fcLabel17
			// 
			this.fcLabel17.AutoSize = true;
			this.fcLabel17.Location = new System.Drawing.Point(6, 138);
			this.fcLabel17.Name = "fcLabel17";
			this.fcLabel17.Size = new System.Drawing.Size(565, 15);
			this.fcLabel17.TabIndex = 60;
			this.fcLabel17.Text = "14C - TOTAL NUMBER OF PROPERTIES FULLY EXEMPTED BY HOMESTEAD EXEMPTIONS";
			this.fcLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel17, "furniture, furnishings and equipment");
			// 
			// fcLabel19
			// 
			this.fcLabel19.AutoSize = true;
			this.fcLabel19.Location = new System.Drawing.Point(6, 86);
			this.fcLabel19.Name = "fcLabel19";
			this.fcLabel19.Size = new System.Drawing.Size(534, 15);
			this.fcLabel19.TabIndex = 56;
			this.fcLabel19.Text = "14B - TOTAL EXEMPT VALUE FOR ALL $25,000 HOMESTEAD EXEMPTIONS GRANTED";
			this.fcLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel19, "furniture, furnishings and equipment");
			// 
			// fcLabel16
			// 
			this.fcLabel16.AutoSize = true;
			this.fcLabel16.Location = new System.Drawing.Point(6, 132);
			this.fcLabel16.Name = "fcLabel16";
			this.fcLabel16.Size = new System.Drawing.Size(167, 15);
			this.fcLabel16.TabIndex = 54;
			this.fcLabel16.Text = "13 - PROPERTY TAX LEVY";
			this.fcLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel16, "furniture, furnishings and equipment");
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(6, 80);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(168, 15);
			this.fcLabel1.TabIndex = 46;
			this.fcLabel1.Text = "12 - PROPERTY TAX RATE";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel1, "furniture, furnishings and equipment");
			// 
			// fcLabel4
			// 
			this.fcLabel4.AutoSize = true;
			this.fcLabel4.Location = new System.Drawing.Point(6, 78);
			this.fcLabel4.Name = "fcLabel4";
			this.fcLabel4.Size = new System.Drawing.Size(169, 15);
			this.fcLabel4.TabIndex = 46;
			this.fcLabel4.Text = "8 - BUSINESS EQUIPMENT";
			this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel4, "furniture, furnishings and equipment");
			// 
			// fcLabel9
			// 
			this.fcLabel9.AutoSize = true;
			this.fcLabel9.Location = new System.Drawing.Point(9, 39);
			this.fcLabel9.Name = "fcLabel9";
			this.fcLabel9.Size = new System.Drawing.Size(57, 15);
			this.fcLabel9.TabIndex = 24;
			this.fcLabel9.Text = "4 - LAND";
			this.fcLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel9, "Include value of transmission, distribution lines and substations, dams and power" +
        " houses");
			// 
			// fcLabel3
			// 
			this.fcLabel3.AutoSize = true;
			this.fcLabel3.Location = new System.Drawing.Point(9, 191);
			this.fcLabel3.Name = "fcLabel3";
			this.fcLabel3.Size = new System.Drawing.Size(443, 15);
			this.fcLabel3.TabIndex = 44;
			this.fcLabel3.Text = "15D - TOTAL EXEMPT VALUE OF BETE PROPERTY IN A TIF DISCTRICT";
			this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel3, "furniture, furnishings and equipment");
			// 
			// fcLabel7
			// 
			this.fcLabel7.AutoSize = true;
			this.fcLabel7.Location = new System.Drawing.Point(9, 90);
			this.fcLabel7.Name = "fcLabel7";
			this.fcLabel7.Size = new System.Drawing.Size(328, 15);
			this.fcLabel7.TabIndex = 40;
			this.fcLabel7.Text = "15B - NUMBER OF BETE APPLICATIONS APPROVED";
			this.fcLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel7, "furniture, furnishings and equipment");
			// 
			// fcLabel24
			// 
			this.fcLabel24.Location = new System.Drawing.Point(9, 180);
			this.fcLabel24.Name = "fcLabel24";
			this.fcLabel24.Size = new System.Drawing.Size(556, 40);
			this.fcLabel24.TabIndex = 52;
			this.fcLabel24.Text = "16D - BETE REIMBURSEMENT REVENUE THAT IS APPROPRIATED AND DEPOSITED INTO EITHER A" +
    " PROJECT COST ACCOUNT OR A SINKING FUND ACCOUNT";
			this.fcLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel24, "furniture, furnishings and equipment");
			// 
			// fcLabel26
			// 
			this.fcLabel26.AutoSize = true;
			this.fcLabel26.Location = new System.Drawing.Point(9, 93);
			this.fcLabel26.Name = "fcLabel26";
			this.fcLabel26.Size = new System.Drawing.Size(456, 15);
			this.fcLabel26.TabIndex = 48;
			this.fcLabel26.Text = "16B - AMOUNT OF CAPTURED ASSESSED VALUE WITHIN TIF DISTRICTS";
			this.fcLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel26, "furniture, furnishings and equipment");
			// 
			// fcLabel29
			// 
			this.fcLabel29.AutoSize = true;
			this.fcLabel29.Location = new System.Drawing.Point(9, 88);
			this.fcLabel29.Name = "fcLabel29";
			this.fcLabel29.Size = new System.Drawing.Size(308, 15);
			this.fcLabel29.TabIndex = 46;
			this.fcLabel29.Text = "17B - MOTOR VEHICLE EXCISE TAX COLLECTED";
			this.fcLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel29, "furniture, furnishings and equipment");
			// 
			// fcLabel31
			// 
			this.fcLabel31.AutoSize = true;
			this.fcLabel31.Location = new System.Drawing.Point(9, 92);
			this.fcLabel31.Name = "fcLabel31";
			this.fcLabel31.Size = new System.Drawing.Size(443, 15);
			this.fcLabel31.TabIndex = 44;
			this.fcLabel31.Text = "19 - TOTAL VALUATION OF ALL ELECTRICAL GENERATION FACILITIES";
			this.fcLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel31, "furniture, furnishings and equipment");
			// 
			// fcLabel33
			// 
			this.fcLabel33.AutoSize = true;
			this.fcLabel33.Location = new System.Drawing.Point(9, 194);
			this.fcLabel33.Name = "fcLabel33";
			this.fcLabel33.Size = new System.Drawing.Size(193, 15);
			this.fcLabel33.TabIndex = 52;
			this.fcLabel33.Text = "21C - MIXED WOOD ACREAGE";
			this.fcLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel33, "furniture, furnishings and equipment");
			// 
			// lblForestParcelsClassified
			// 
			this.lblForestParcelsClassified.AutoSize = true;
			this.lblForestParcelsClassified.Location = new System.Drawing.Point(9, 93);
			this.lblForestParcelsClassified.Name = "lblForestParcelsClassified";
			this.lblForestParcelsClassified.Size = new System.Drawing.Size(355, 15);
			this.lblForestParcelsClassified.TabIndex = 48;
			this.lblForestParcelsClassified.Text = "21A - NUMBER OF PARCELS CLASSIFIED AS OF APRIL 1";
			this.lblForestParcelsClassified.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblForestParcelsClassified, "furniture, furnishings and equipment");
			// 
			// fcLabel35
			// 
			this.fcLabel35.AutoSize = true;
			this.fcLabel35.Location = new System.Drawing.Point(9, 395);
			this.fcLabel35.Name = "fcLabel35";
			this.fcLabel35.Size = new System.Drawing.Size(209, 15);
			this.fcLabel35.TabIndex = 60;
			this.fcLabel35.Text = "22A(1) - SOFTWOOD (PER ACRE)";
			this.fcLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel35, "furniture, furnishings and equipment");
			// 
			// fcLabel38
			// 
			this.fcLabel38.AutoSize = true;
			this.fcLabel38.Location = new System.Drawing.Point(9, 294);
			this.fcLabel38.Name = "fcLabel38";
			this.fcLabel38.Size = new System.Drawing.Size(365, 15);
			this.fcLabel38.TabIndex = 56;
			this.fcLabel38.Text = "21E - TOTAL NUMBER OF ACRES OF FOREST LAND ONLY";
			this.fcLabel38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel38, "furniture, furnishings and equipment");
			// 
			// fcLabel41
			// 
			this.fcLabel41.AutoSize = true;
			this.fcLabel41.Location = new System.Drawing.Point(9, 447);
			this.fcLabel41.Name = "fcLabel41";
			this.fcLabel41.Size = new System.Drawing.Size(219, 15);
			this.fcLabel41.TabIndex = 62;
			this.fcLabel41.Text = "22A(2) - MIXED WOOD (PER ACRE)";
			this.fcLabel41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel41, "furniture, furnishings and equipment");
			// 
			// fcLabel42
			// 
			this.fcLabel42.AutoSize = true;
			this.fcLabel42.Location = new System.Drawing.Point(9, 246);
			this.fcLabel42.Name = "fcLabel42";
			this.fcLabel42.Size = new System.Drawing.Size(498, 15);
			this.fcLabel42.TabIndex = 74;
			this.fcLabel42.Text = "24D - TOTAL NUMBER OF $500 PENALTIES ASSESSED FOR NON-COMPLIANCE";
			this.fcLabel42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel42, "furniture, furnishings and equipment");
			// 
			// fcLabel43
			// 
			this.fcLabel43.AutoSize = true;
			this.fcLabel43.Location = new System.Drawing.Point(9, 194);
			this.fcLabel43.Name = "fcLabel43";
			this.fcLabel43.Size = new System.Drawing.Size(427, 15);
			this.fcLabel43.TabIndex = 72;
			this.fcLabel43.Text = "24C - TOTAL AMOUNT OF PENALTIES ASSESSED BY MUNICIPALITY";
			this.fcLabel43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel43, "furniture, furnishings and equipment");
			// 
			// lblTotalForestParcelsWithdrawn
			// 
			this.lblTotalForestParcelsWithdrawn.AutoSize = true;
			this.lblTotalForestParcelsWithdrawn.Location = new System.Drawing.Point(9, 93);
			this.lblTotalForestParcelsWithdrawn.Name = "lblTotalForestParcelsWithdrawn";
			this.lblTotalForestParcelsWithdrawn.Size = new System.Drawing.Size(354, 15);
			this.lblTotalForestParcelsWithdrawn.TabIndex = 68;
			this.lblTotalForestParcelsWithdrawn.Text = "24A - TOTAL NUMBER OF PARCELS WITHDRAWN FROM";
			this.lblTotalForestParcelsWithdrawn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblTotalForestParcelsWithdrawn, "furniture, furnishings and equipment");
			// 
			// fcLabel45
			// 
			this.fcLabel45.AutoSize = true;
			this.fcLabel45.Location = new System.Drawing.Point(8, 248);
			this.fcLabel45.Name = "fcLabel45";
			this.fcLabel45.Size = new System.Drawing.Size(301, 15);
			this.fcLabel45.TabIndex = 86;
			this.fcLabel45.Text = "28A(1) - NUMBER OF FARM SOFTWOOD ACRES";
			this.fcLabel45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel45, "furniture, furnishings and equipment");
			// 
			// fcLabel46
			// 
			this.fcLabel46.Location = new System.Drawing.Point(8, 182);
			this.fcLabel46.Name = "fcLabel46";
			this.fcLabel46.Size = new System.Drawing.Size(543, 40);
			this.fcLabel46.TabIndex = 84;
			this.fcLabel46.Text = "27B - TOTAL VALUATION OF ALL LAND NOW CLASSIFIED AS FARMLAND (NOT INCLUDING FARM " +
    "WOODLAND)";
			this.fcLabel46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel46, "furniture, furnishings and equipment");
			// 
			// fcLabel48
			// 
			this.fcLabel48.AutoSize = true;
			this.fcLabel48.Location = new System.Drawing.Point(8, 95);
			this.fcLabel48.Name = "fcLabel48";
			this.fcLabel48.Size = new System.Drawing.Size(472, 15);
			this.fcLabel48.TabIndex = 80;
			this.fcLabel48.Text = "26 - NUMBER OF ACRES FIRST CLASSIFIED AS FARMLAND FOR TAX YEAR";
			this.fcLabel48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel48, "furniture, furnishings and equipment");
			// 
			// fcLabel51
			// 
			this.fcLabel51.AutoSize = true;
			this.fcLabel51.Location = new System.Drawing.Point(8, 554);
			this.fcLabel51.Name = "fcLabel51";
			this.fcLabel51.Size = new System.Drawing.Size(290, 15);
			this.fcLabel51.TabIndex = 98;
			this.fcLabel51.Text = "28D(2) - FARM MIXED WOOD PER ACRE RATE";
			this.fcLabel51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel51, "furniture, furnishings and equipment");
			// 
			// fcLabel52
			// 
			this.fcLabel52.Location = new System.Drawing.Point(8, 488);
			this.fcLabel52.Name = "fcLabel52";
			this.fcLabel52.Size = new System.Drawing.Size(543, 40);
			this.fcLabel52.TabIndex = 96;
			this.fcLabel52.Text = "28D(1) - FARM SOFTWOOD PER ACRE RATE";
			this.fcLabel52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel52, "furniture, furnishings and equipment");
			// 
			// fcLabel54
			// 
			this.fcLabel54.AutoSize = true;
			this.fcLabel54.Location = new System.Drawing.Point(8, 401);
			this.fcLabel54.Name = "fcLabel54";
			this.fcLabel54.Size = new System.Drawing.Size(553, 15);
			this.fcLabel54.TabIndex = 92;
			this.fcLabel54.Text = "28B - TOTAL NUMBER OF ACRES OF ALL LAND NOW CLASSIFIED AS FARM WOODLAND";
			this.fcLabel54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel54, "furniture, furnishings and equipment");
			// 
			// fcLabel57
			// 
			this.fcLabel57.AutoSize = true;
			this.fcLabel57.Location = new System.Drawing.Point(8, 912);
			this.fcLabel57.Name = "fcLabel57";
			this.fcLabel57.Size = new System.Drawing.Size(485, 15);
			this.fcLabel57.TabIndex = 112;
			this.fcLabel57.Text = "32 - TOTAL NUMBER OF ACRES OF LAND NOW CLASSIFIED AS OPEN SPACE";
			this.fcLabel57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel57, "furniture, furnishings and equipment");
			// 
			// fcLabel58
			// 
			this.fcLabel58.Location = new System.Drawing.Point(8, 846);
			this.fcLabel58.Name = "fcLabel58";
			this.fcLabel58.Size = new System.Drawing.Size(543, 40);
			this.fcLabel58.TabIndex = 110;
			this.fcLabel58.Text = "31 - NUMBER OF ACRES FIRST CLASSIFIED AS OPEN SPACE FOR TAX YEAR";
			this.fcLabel58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel58, "furniture, furnishings and equipment");
			// 
			// fcLabel60
			// 
			this.fcLabel60.Location = new System.Drawing.Point(8, 745);
			this.fcLabel60.Name = "fcLabel60";
			this.fcLabel60.Size = new System.Drawing.Size(549, 40);
			this.fcLabel60.TabIndex = 106;
			this.fcLabel60.Text = "29C - TOTAL AMOUNT OF PENALTIES ASSESSED BY MUNICIPALITY DUE TO THE WITHDRAWAL OF" +
    " CLASSIFIED FARMLAND";
			this.fcLabel60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel60, "furniture, furnishings and equipment");
			// 
			// fcLabel64
			// 
			this.fcLabel64.AutoSize = true;
			this.fcLabel64.Location = new System.Drawing.Point(9, 97);
			this.fcLabel64.Name = "fcLabel64";
			this.fcLabel64.Size = new System.Drawing.Size(384, 15);
			this.fcLabel64.TabIndex = 74;
			this.fcLabel64.Text = "34B - TOTAL NUMBER OF OPEN SPACE ACRES WITHDRAWN";
			this.fcLabel64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel64, "furniture, furnishings and equipment");
			// 
			// fcLabel68
			// 
			this.fcLabel68.AutoSize = true;
			this.fcLabel68.Location = new System.Drawing.Point(9, 249);
			this.fcLabel68.Name = "fcLabel68";
			this.fcLabel68.Size = new System.Drawing.Size(474, 15);
			this.fcLabel68.TabIndex = 100;
			this.fcLabel68.Text = "39A - TOTAL NUMBER OF WORKING WATERFRONT PARCELS WITHDRAWN";
			this.fcLabel68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel68, "furniture, furnishings and equipment");
			// 
			// fcLabel69
			// 
			this.fcLabel69.Location = new System.Drawing.Point(9, 183);
			this.fcLabel69.Name = "fcLabel69";
			this.fcLabel69.Size = new System.Drawing.Size(543, 40);
			this.fcLabel69.TabIndex = 98;
			this.fcLabel69.Text = "38 - TOTAL VALUATION OF ALL LAND NOW CLASSIFIED AS WORKING WATERFRONT";
			this.fcLabel69.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel69, "furniture, furnishings and equipment");
			// 
			// fcLabel71
			// 
			this.fcLabel71.AutoSize = true;
			this.fcLabel71.Location = new System.Drawing.Point(9, 96);
			this.fcLabel71.Name = "fcLabel71";
			this.fcLabel71.Size = new System.Drawing.Size(559, 15);
			this.fcLabel71.TabIndex = 94;
			this.fcLabel71.Text = "36 - NUMBER OF ACRES FIRST CLASSIFIED AS WORKING WATERFRONT FOR TAX YEAR";
			this.fcLabel71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel71, "furniture, furnishings and equipment");
			// 
			// fcLabel74
			// 
			this.fcLabel74.Location = new System.Drawing.Point(9, 409);
			this.fcLabel74.Name = "fcLabel74";
			this.fcLabel74.Size = new System.Drawing.Size(594, 53);
			this.fcLabel74.TabIndex = 110;
			this.fcLabel74.Text = "40G - PIPES, FIXTURES, CONDUITS, BUILDINGS, PUMPING STATIONS, AND OTHER FACILITIE" +
    "S OF A PUBLIC MUNICIPAL CORPORATION USED FOR SEWERAGE DISPOSAL IF LOCATED OUTSID" +
    "E THE LIMITS OF THE MUNICIPALITY";
			this.fcLabel74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel74, "furniture, furnishings and equipment");
			// 
			// fcLabel77
			// 
			this.fcLabel77.Location = new System.Drawing.Point(9, 235);
			this.fcLabel77.Name = "fcLabel77";
			this.fcLabel77.Size = new System.Drawing.Size(560, 56);
			this.fcLabel77.TabIndex = 104;
			this.fcLabel77.Text = resources.GetString("fcLabel77.Text");
			this.fcLabel77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel77, "furniture, furnishings and equipment");
			// 
			// fcLabel78
			// 
			this.fcLabel78.Location = new System.Drawing.Point(9, 180);
			this.fcLabel78.Name = "fcLabel78";
			this.fcLabel78.Size = new System.Drawing.Size(543, 40);
			this.fcLabel78.TabIndex = 102;
			this.fcLabel78.Text = "40C - PROPERTY OF ANY PUBLIC MUNICIPAL CORPORATION OF THIS STATE (INCLUDING COUNT" +
    "Y PROPERTY) APPROPRIATED TO PUBLIC USES";
			this.fcLabel78.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel78, "furniture, furnishings and equipment");
			// 
			// fcLabel80
			// 
			this.fcLabel80.AutoSize = true;
			this.fcLabel80.Location = new System.Drawing.Point(9, 93);
			this.fcLabel80.Name = "fcLabel80";
			this.fcLabel80.Size = new System.Drawing.Size(300, 15);
			this.fcLabel80.TabIndex = 98;
			this.fcLabel80.Text = "40A(2) - STATE OF MAINE (EXCLUDING ROADS)";
			this.fcLabel80.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel80, "furniture, furnishings and equipment");
			// 
			// fcLabel139
			// 
			this.fcLabel139.Location = new System.Drawing.Point(6, 625);
			this.fcLabel139.Name = "fcLabel139";
			this.fcLabel139.Size = new System.Drawing.Size(373, 40);
			this.fcLabel139.TabIndex = 106;
			this.fcLabel139.Text = "50A - MUNICIPALITY HAS IMPLEMENTED A LOCAL PROPERTY TAX RELIEF PROGRAM";
			this.fcLabel139.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel139, "A tax relief program similar to the state\'s circuitbreaker porgram");
			// 
			// fcLabel144
			// 
			this.fcLabel144.Location = new System.Drawing.Point(6, 775);
			this.fcLabel144.Name = "fcLabel144";
			this.fcLabel144.Size = new System.Drawing.Size(373, 40);
			this.fcLabel144.TabIndex = 112;
			this.fcLabel144.Text = "51A - MUNICIPALITY HAS IMPLEMENTED A LOCAL SENIOR VOLUNTEER TAX CREDIT PROGRAM";
			this.fcLabel144.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel144, "A tax relief program similar to the state\'s circuitbreaker porgram");
			// 
			// fcLabel153
			// 
			this.fcLabel153.Location = new System.Drawing.Point(6, 925);
			this.fcLabel153.Name = "fcLabel153";
			this.fcLabel153.Size = new System.Drawing.Size(373, 40);
			this.fcLabel153.TabIndex = 118;
			this.fcLabel153.Text = "52A - MUNICIPALITY HAS IMPLEMENTED A LOCAL TAX DEFERRAL FOR SENIOR CITIZENS";
			this.fcLabel153.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.fcLabel153, "A tax relief program similar to the state\'s circuitbreaker porgram");
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.fcTabPage1);
			this.tabControl1.Controls.Add(this.fcTabPage2);
			this.tabControl1.Controls.Add(this.fcTabPage3);
			this.tabControl1.Controls.Add(this.fcTabPage4);
			this.tabControl1.Controls.Add(this.fcTabPage5);
			this.tabControl1.Controls.Add(this.fcTabPage6);
			this.tabControl1.Controls.Add(this.fcTabPage7);
			this.tabControl1.Location = new System.Drawing.Point(6, 1);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
			this.tabControl1.Size = new System.Drawing.Size(968, 600);
			this.tabControl1.TabIndex = 1;
			this.tabControl1.Text = "Page 1";
			// 
			// tabPage1
			// 
			this.tabPage1.AutoScroll = true;
			this.tabPage1.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.tabPage1.Controls.Add(this.fcFrame4);
			this.tabPage1.Controls.Add(this.fcFrame3);
			this.tabPage1.Controls.Add(this.fcFrame2);
			this.tabPage1.Controls.Add(this.fcFrame1);
			this.tabPage1.Controls.Add(this.fcLabel12);
			this.tabPage1.Controls.Add(this.dtCommitmentDate);
			this.tabPage1.Controls.Add(this.txtCertifiedRatio);
			this.tabPage1.Controls.Add(this.fcLabel11);
			this.tabPage1.Controls.Add(this.txtMunicipality);
			this.tabPage1.Controls.Add(this.Label7);
			this.tabPage1.Controls.Add(this.txtCounty);
			this.tabPage1.Controls.Add(this.Label2);
			this.tabPage1.Location = new System.Drawing.Point(1, 35);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(966, 564);
			this.tabPage1.Text = "Page 1";
			// 
			// fcFrame4
			// 
			this.fcFrame4.Controls.Add(this.txtTotalHomesteadAssessments);
			this.fcFrame4.Controls.Add(this.fcLabel23);
			this.fcFrame4.Controls.Add(this.txtTotalNumberHomesteads);
			this.fcFrame4.Controls.Add(this.txtHomesteadTotalExemptions);
			this.fcFrame4.Controls.Add(this.txtNumberHomesteadFullyExempt);
			this.fcFrame4.Controls.Add(this.fcLabel20);
			this.fcFrame4.Controls.Add(this.txtHomesteadFullyExemptValue);
			this.fcFrame4.Controls.Add(this.fcLabel21);
			this.fcFrame4.Controls.Add(this.txtTotalFullHomesteadExemptions);
			this.fcFrame4.Controls.Add(this.fcLabel22);
			this.fcFrame4.Controls.Add(this.fcLabel17);
			this.fcFrame4.Controls.Add(this.txtTotalFullHomesteadsGranted);
			this.fcFrame4.Controls.Add(this.fcLabel18);
			this.fcFrame4.Controls.Add(this.fcLabel19);
			this.fcFrame4.Location = new System.Drawing.Point(8, 828);
			this.fcFrame4.Name = "fcFrame4";
			this.fcFrame4.Size = new System.Drawing.Size(734, 383);
			this.fcFrame4.TabIndex = 8;
			this.fcFrame4.Text = "Homestead Exemption Reimbursement Claim";
			// 
			// txtTotalHomesteadAssessments
			// 
			this.txtTotalHomesteadAssessments.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalHomesteadAssessments.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalHomesteadAssessments.Focusable = false;
			this.txtTotalHomesteadAssessments.Location = new System.Drawing.Point(577, 332);
			this.txtTotalHomesteadAssessments.LockedOriginal = true;
			this.txtTotalHomesteadAssessments.MaxLength = 0;
			this.txtTotalHomesteadAssessments.Name = "txtTotalHomesteadAssessments";
			this.txtTotalHomesteadAssessments.ReadOnly = true;
			this.txtTotalHomesteadAssessments.Size = new System.Drawing.Size(130, 40);
			this.txtTotalHomesteadAssessments.TabIndex = 7;
			this.txtTotalHomesteadAssessments.Text = "0";
			this.txtTotalHomesteadAssessments.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTotalNumberHomesteads
			// 
			this.txtTotalNumberHomesteads.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalNumberHomesteads.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalNumberHomesteads.Focusable = false;
			this.txtTotalNumberHomesteads.Location = new System.Drawing.Point(577, 228);
			this.txtTotalNumberHomesteads.LockedOriginal = true;
			this.txtTotalNumberHomesteads.MaxLength = 0;
			this.txtTotalNumberHomesteads.Name = "txtTotalNumberHomesteads";
			this.txtTotalNumberHomesteads.ReadOnly = true;
			this.txtTotalNumberHomesteads.Size = new System.Drawing.Size(130, 40);
			this.txtTotalNumberHomesteads.TabIndex = 5;
			this.txtTotalNumberHomesteads.Text = "0";
			this.txtTotalNumberHomesteads.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtHomesteadTotalExemptions
			// 
			this.txtHomesteadTotalExemptions.BackColor = System.Drawing.Color.FromName("@control");
			this.txtHomesteadTotalExemptions.Cursor = Wisej.Web.Cursors.Default;
			this.txtHomesteadTotalExemptions.Focusable = false;
			this.txtHomesteadTotalExemptions.Location = new System.Drawing.Point(577, 280);
			this.txtHomesteadTotalExemptions.LockedOriginal = true;
			this.txtHomesteadTotalExemptions.MaxLength = 0;
			this.txtHomesteadTotalExemptions.Name = "txtHomesteadTotalExemptions";
			this.txtHomesteadTotalExemptions.ReadOnly = true;
			this.txtHomesteadTotalExemptions.Size = new System.Drawing.Size(130, 40);
			this.txtHomesteadTotalExemptions.TabIndex = 6;
			this.txtHomesteadTotalExemptions.Text = "0";
			this.txtHomesteadTotalExemptions.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtNumberHomesteadFullyExempt
			// 
			this.txtNumberHomesteadFullyExempt.BackColor = System.Drawing.Color.FromName("@control");
			this.txtNumberHomesteadFullyExempt.Cursor = Wisej.Web.Cursors.Default;
			this.txtNumberHomesteadFullyExempt.Focusable = false;
			this.txtNumberHomesteadFullyExempt.Location = new System.Drawing.Point(577, 125);
			this.txtNumberHomesteadFullyExempt.LockedOriginal = true;
			this.txtNumberHomesteadFullyExempt.MaxLength = 0;
			this.txtNumberHomesteadFullyExempt.Name = "txtNumberHomesteadFullyExempt";
			this.txtNumberHomesteadFullyExempt.ReadOnly = true;
			this.txtNumberHomesteadFullyExempt.Size = new System.Drawing.Size(130, 40);
			this.txtNumberHomesteadFullyExempt.TabIndex = 3;
			this.txtNumberHomesteadFullyExempt.Text = "0";
			this.txtNumberHomesteadFullyExempt.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtHomesteadFullyExemptValue
			// 
			this.txtHomesteadFullyExemptValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtHomesteadFullyExemptValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtHomesteadFullyExemptValue.Focusable = false;
			this.txtHomesteadFullyExemptValue.Location = new System.Drawing.Point(577, 177);
			this.txtHomesteadFullyExemptValue.LockedOriginal = true;
			this.txtHomesteadFullyExemptValue.MaxLength = 0;
			this.txtHomesteadFullyExemptValue.Name = "txtHomesteadFullyExemptValue";
			this.txtHomesteadFullyExemptValue.ReadOnly = true;
			this.txtHomesteadFullyExemptValue.Size = new System.Drawing.Size(130, 40);
			this.txtHomesteadFullyExemptValue.TabIndex = 4;
			this.txtHomesteadFullyExemptValue.Text = "0";
			this.txtHomesteadFullyExemptValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel21
			// 
			this.fcLabel21.AutoSize = true;
			this.fcLabel21.Location = new System.Drawing.Point(6, 191);
			this.fcLabel21.Name = "fcLabel21";
			this.fcLabel21.Size = new System.Drawing.Size(449, 15);
			this.fcLabel21.TabIndex = 64;
			this.fcLabel21.Text = "14D - TOTAL EXEMPT VALUE FOR ALL PROPERTIES FULLY EXEMPTED";
			this.fcLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTotalFullHomesteadExemptions
			// 
			this.txtTotalFullHomesteadExemptions.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalFullHomesteadExemptions.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalFullHomesteadExemptions.Focusable = false;
			this.txtTotalFullHomesteadExemptions.Location = new System.Drawing.Point(577, 72);
			this.txtTotalFullHomesteadExemptions.LockedOriginal = true;
			this.txtTotalFullHomesteadExemptions.MaxLength = 0;
			this.txtTotalFullHomesteadExemptions.Name = "txtTotalFullHomesteadExemptions";
			this.txtTotalFullHomesteadExemptions.ReadOnly = true;
			this.txtTotalFullHomesteadExemptions.Size = new System.Drawing.Size(130, 40);
			this.txtTotalFullHomesteadExemptions.TabIndex = 2;
			this.txtTotalFullHomesteadExemptions.Text = "0";
			this.txtTotalFullHomesteadExemptions.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTotalFullHomesteadsGranted
			// 
			this.txtTotalFullHomesteadsGranted.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalFullHomesteadsGranted.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalFullHomesteadsGranted.Focusable = false;
			this.txtTotalFullHomesteadsGranted.Location = new System.Drawing.Point(577, 21);
			this.txtTotalFullHomesteadsGranted.LockedOriginal = true;
			this.txtTotalFullHomesteadsGranted.MaxLength = 0;
			this.txtTotalFullHomesteadsGranted.Name = "txtTotalFullHomesteadsGranted";
			this.txtTotalFullHomesteadsGranted.ReadOnly = true;
			this.txtTotalFullHomesteadsGranted.Size = new System.Drawing.Size(130, 40);
			this.txtTotalFullHomesteadsGranted.TabIndex = 1;
			this.txtTotalFullHomesteadsGranted.Text = "0";
			this.txtTotalFullHomesteadsGranted.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel18
			// 
			this.fcLabel18.AutoSize = true;
			this.fcLabel18.Location = new System.Drawing.Point(6, 35);
			this.fcLabel18.Name = "fcLabel18";
			this.fcLabel18.Size = new System.Drawing.Size(454, 15);
			this.fcLabel18.TabIndex = 58;
			this.fcLabel18.Text = "14A - TOTAL NUMBER OF $25,000 HOMESTEAD EXEMPTIONS GRANTED";
			this.fcLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame3
			// 
			this.fcFrame3.Controls.Add(this.txtTaxLevy);
			this.fcFrame3.Controls.Add(this.fcLabel16);
			this.fcFrame3.Controls.Add(this.txtTotalTaxableValuation);
			this.fcFrame3.Controls.Add(this.fcLabel15);
			this.fcFrame3.Controls.Add(this.txtPropertyTaxRate);
			this.fcFrame3.Controls.Add(this.fcLabel1);
			this.fcFrame3.Location = new System.Drawing.Point(8, 635);
			this.fcFrame3.Name = "fcFrame3";
			this.fcFrame3.Size = new System.Drawing.Size(734, 173);
			this.fcFrame3.TabIndex = 7;
			this.fcFrame3.Text = "Other Tax Information";
			// 
			// txtTaxLevy
			// 
			this.txtTaxLevy.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTaxLevy.Cursor = Wisej.Web.Cursors.Default;
			this.txtTaxLevy.Focusable = false;
			this.txtTaxLevy.Location = new System.Drawing.Point(577, 118);
			this.txtTaxLevy.LockedOriginal = true;
			this.txtTaxLevy.MaxLength = 0;
			this.txtTaxLevy.Name = "txtTaxLevy";
			this.txtTaxLevy.ReadOnly = true;
			this.txtTaxLevy.Size = new System.Drawing.Size(133, 40);
			this.txtTaxLevy.TabIndex = 3;
			this.txtTaxLevy.Text = "0";
			this.txtTaxLevy.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTotalTaxableValuation
			// 
			this.txtTotalTaxableValuation.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalTaxableValuation.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalTaxableValuation.Focusable = false;
			this.txtTotalTaxableValuation.Location = new System.Drawing.Point(577, 15);
			this.txtTotalTaxableValuation.LockedOriginal = true;
			this.txtTotalTaxableValuation.MaxLength = 0;
			this.txtTotalTaxableValuation.Name = "txtTotalTaxableValuation";
			this.txtTotalTaxableValuation.ReadOnly = true;
			this.txtTotalTaxableValuation.Size = new System.Drawing.Size(133, 40);
			this.txtTotalTaxableValuation.TabIndex = 1;
			this.txtTotalTaxableValuation.Text = "0";
			this.txtTotalTaxableValuation.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel15
			// 
			this.fcLabel15.AutoSize = true;
			this.fcLabel15.Location = new System.Drawing.Point(6, 29);
			this.fcLabel15.Name = "fcLabel15";
			this.fcLabel15.Size = new System.Drawing.Size(211, 15);
			this.fcLabel15.TabIndex = 52;
			this.fcLabel15.Text = "11 - TOTAL TAXABLE VALUATION";
			this.fcLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtPropertyTaxRate
			// 
			this.txtPropertyTaxRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtPropertyTaxRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtPropertyTaxRate.Focusable = false;
			this.txtPropertyTaxRate.Location = new System.Drawing.Point(577, 66);
			this.txtPropertyTaxRate.LockedOriginal = true;
			this.txtPropertyTaxRate.MaxLength = 0;
			this.txtPropertyTaxRate.Name = "txtPropertyTaxRate";
			this.txtPropertyTaxRate.ReadOnly = true;
			this.txtPropertyTaxRate.Size = new System.Drawing.Size(133, 40);
			this.txtPropertyTaxRate.TabIndex = 2;
			this.txtPropertyTaxRate.Text = "0";
			this.txtPropertyTaxRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcFrame2
			// 
			this.fcFrame2.Controls.Add(this.txtTotalTaxablePersonalProperty);
			this.fcFrame2.Controls.Add(this.fcLabel14);
			this.fcFrame2.Controls.Add(this.txtOtherPersonalProperty);
			this.fcFrame2.Controls.Add(this.fcLabel2);
			this.fcFrame2.Controls.Add(this.txtBusinessEquipment);
			this.fcFrame2.Controls.Add(this.fcLabel4);
			this.fcFrame2.Controls.Add(this.txtMachineryAndEquipment);
			this.fcFrame2.Controls.Add(this.fcLabel10);
			this.fcFrame2.Location = new System.Drawing.Point(8, 402);
			this.fcFrame2.Name = "fcFrame2";
			this.fcFrame2.Size = new System.Drawing.Size(734, 220);
			this.fcFrame2.TabIndex = 6;
			this.fcFrame2.Text = "Taxable Valuation Of Personal Property";
			// 
			// txtTotalTaxablePersonalProperty
			// 
			this.txtTotalTaxablePersonalProperty.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalTaxablePersonalProperty.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalTaxablePersonalProperty.Focusable = false;
			this.txtTotalTaxablePersonalProperty.Location = new System.Drawing.Point(577, 165);
			this.txtTotalTaxablePersonalProperty.LockedOriginal = true;
			this.txtTotalTaxablePersonalProperty.MaxLength = 0;
			this.txtTotalTaxablePersonalProperty.Name = "txtTotalTaxablePersonalProperty";
			this.txtTotalTaxablePersonalProperty.ReadOnly = true;
			this.txtTotalTaxablePersonalProperty.Size = new System.Drawing.Size(133, 40);
			this.txtTotalTaxablePersonalProperty.TabIndex = 4;
			this.txtTotalTaxablePersonalProperty.Text = "0";
			this.txtTotalTaxablePersonalProperty.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel14
			// 
			this.fcLabel14.AutoSize = true;
			this.fcLabel14.Location = new System.Drawing.Point(6, 179);
			this.fcLabel14.Name = "fcLabel14";
			this.fcLabel14.Size = new System.Drawing.Size(211, 15);
			this.fcLabel14.TabIndex = 50;
			this.fcLabel14.Text = "10 - TOTAL TAXABLE VALUATION";
			this.fcLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtOtherPersonalProperty
			// 
			this.txtOtherPersonalProperty.BackColor = System.Drawing.SystemColors.Window;
			this.txtOtherPersonalProperty.Cursor = Wisej.Web.Cursors.Default;
			this.txtOtherPersonalProperty.Location = new System.Drawing.Point(577, 115);
			this.txtOtherPersonalProperty.MaxLength = 0;
			this.txtOtherPersonalProperty.Name = "txtOtherPersonalProperty";
			this.txtOtherPersonalProperty.Size = new System.Drawing.Size(133, 40);
			this.txtOtherPersonalProperty.TabIndex = 3;
			this.txtOtherPersonalProperty.Text = "0";
			this.txtOtherPersonalProperty.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel2
			// 
			this.fcLabel2.AutoSize = true;
			this.fcLabel2.Location = new System.Drawing.Point(6, 129);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(244, 15);
			this.fcLabel2.TabIndex = 48;
			this.fcLabel2.Text = "9 - ALL OTHER PERSONAL PROPERTY";
			this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBusinessEquipment
			// 
			this.txtBusinessEquipment.BackColor = System.Drawing.SystemColors.Window;
			this.txtBusinessEquipment.Cursor = Wisej.Web.Cursors.Default;
			this.txtBusinessEquipment.Location = new System.Drawing.Point(577, 64);
			this.txtBusinessEquipment.MaxLength = 0;
			this.txtBusinessEquipment.Name = "txtBusinessEquipment";
			this.txtBusinessEquipment.Size = new System.Drawing.Size(133, 40);
			this.txtBusinessEquipment.TabIndex = 2;
			this.txtBusinessEquipment.Text = "0";
			this.txtBusinessEquipment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtMachineryAndEquipment
			// 
			this.txtMachineryAndEquipment.BackColor = System.Drawing.SystemColors.Window;
			this.txtMachineryAndEquipment.Cursor = Wisej.Web.Cursors.Default;
			this.txtMachineryAndEquipment.Location = new System.Drawing.Point(577, 14);
			this.txtMachineryAndEquipment.MaxLength = 0;
			this.txtMachineryAndEquipment.Name = "txtMachineryAndEquipment";
			this.txtMachineryAndEquipment.Size = new System.Drawing.Size(133, 40);
			this.txtMachineryAndEquipment.TabIndex = 1;
			this.txtMachineryAndEquipment.Text = "0";
			this.txtMachineryAndEquipment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel10
			// 
			this.fcLabel10.AutoSize = true;
			this.fcLabel10.Location = new System.Drawing.Point(6, 28);
			this.fcLabel10.Name = "fcLabel10";
			this.fcLabel10.Size = new System.Drawing.Size(303, 15);
			this.fcLabel10.TabIndex = 44;
			this.fcLabel10.Text = "7 - PRODUCTION MACHINERY AND EQUIPMENT";
			this.fcLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame1
			// 
			this.fcFrame1.Controls.Add(this.txtTotalTaxableRealEstate);
			this.fcFrame1.Controls.Add(this.fcLabel13);
			this.fcFrame1.Controls.Add(this.txtTaxableBuilding);
			this.fcFrame1.Controls.Add(this.fcLabel6);
			this.fcFrame1.Controls.Add(this.txtTaxableLand);
			this.fcFrame1.Controls.Add(this.fcLabel9);
			this.fcFrame1.Location = new System.Drawing.Point(8, 216);
			this.fcFrame1.Name = "fcFrame1";
			this.fcFrame1.Size = new System.Drawing.Size(734, 178);
			this.fcFrame1.TabIndex = 5;
			this.fcFrame1.Text = "Taxable Valuation Of Real Estate";
			// 
			// txtTotalTaxableRealEstate
			// 
			this.txtTotalTaxableRealEstate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTotalTaxableRealEstate.Cursor = Wisej.Web.Cursors.Default;
			this.txtTotalTaxableRealEstate.Focusable = false;
			this.txtTotalTaxableRealEstate.Location = new System.Drawing.Point(577, 127);
			this.txtTotalTaxableRealEstate.LockedOriginal = true;
			this.txtTotalTaxableRealEstate.MaxLength = 0;
			this.txtTotalTaxableRealEstate.Name = "txtTotalTaxableRealEstate";
			this.txtTotalTaxableRealEstate.ReadOnly = true;
			this.txtTotalTaxableRealEstate.Size = new System.Drawing.Size(130, 40);
			this.txtTotalTaxableRealEstate.TabIndex = 3;
			this.txtTotalTaxableRealEstate.Text = "0";
			this.txtTotalTaxableRealEstate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel13
			// 
			this.fcLabel13.AutoSize = true;
			this.fcLabel13.Location = new System.Drawing.Point(9, 139);
			this.fcLabel13.Name = "fcLabel13";
			this.fcLabel13.Size = new System.Drawing.Size(204, 15);
			this.fcLabel13.TabIndex = 28;
			this.fcLabel13.Text = "6 - TOTAL TAXABLE VALUATION";
			this.fcLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTaxableBuilding
			// 
			this.txtTaxableBuilding.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTaxableBuilding.Cursor = Wisej.Web.Cursors.Default;
			this.txtTaxableBuilding.Focusable = false;
			this.txtTaxableBuilding.Location = new System.Drawing.Point(577, 77);
			this.txtTaxableBuilding.LockedOriginal = true;
			this.txtTaxableBuilding.MaxLength = 0;
			this.txtTaxableBuilding.Name = "txtTaxableBuilding";
			this.txtTaxableBuilding.ReadOnly = true;
			this.txtTaxableBuilding.Size = new System.Drawing.Size(130, 40);
			this.txtTaxableBuilding.TabIndex = 2;
			this.txtTaxableBuilding.Text = "0";
			this.txtTaxableBuilding.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel6
			// 
			this.fcLabel6.AutoSize = true;
			this.fcLabel6.Location = new System.Drawing.Point(9, 89);
			this.fcLabel6.Name = "fcLabel6";
			this.fcLabel6.Size = new System.Drawing.Size(84, 15);
			this.fcLabel6.TabIndex = 26;
			this.fcLabel6.Text = "5 - BUILDING";
			this.fcLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTaxableLand
			// 
			this.txtTaxableLand.BackColor = System.Drawing.Color.FromName("@control");
			this.txtTaxableLand.Cursor = Wisej.Web.Cursors.Default;
			this.txtTaxableLand.Focusable = false;
			this.txtTaxableLand.Location = new System.Drawing.Point(577, 27);
			this.txtTaxableLand.LockedOriginal = true;
			this.txtTaxableLand.MaxLength = 0;
			this.txtTaxableLand.Name = "txtTaxableLand";
			this.txtTaxableLand.ReadOnly = true;
			this.txtTaxableLand.Size = new System.Drawing.Size(130, 40);
			this.txtTaxableLand.TabIndex = 1;
			this.txtTaxableLand.Text = "0";
			this.txtTaxableLand.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel12
			// 
			this.fcLabel12.AutoSize = true;
			this.fcLabel12.Location = new System.Drawing.Point(17, 20);
			this.fcLabel12.Name = "fcLabel12";
			this.fcLabel12.Size = new System.Drawing.Size(131, 15);
			this.fcLabel12.TabIndex = 49;
			this.fcLabel12.Text = "COMMITMENT DATE";
			this.fcLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtCommitmentDate
			// 
			this.dtCommitmentDate.AutoSize = false;
			this.dtCommitmentDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtCommitmentDate.Location = new System.Drawing.Point(585, 10);
			this.dtCommitmentDate.Mask = "00/00/0000";
			this.dtCommitmentDate.Name = "dtCommitmentDate";
			this.dtCommitmentDate.Size = new System.Drawing.Size(170, 40);
			this.dtCommitmentDate.TabIndex = 1;
			// 
			// txtCertifiedRatio
			// 
			this.txtCertifiedRatio.BackColor = System.Drawing.Color.FromName("@control");
			this.txtCertifiedRatio.Cursor = Wisej.Web.Cursors.Default;
			this.txtCertifiedRatio.Focusable = false;
			this.javaScript1.SetJavaScript(this.txtCertifiedRatio, resources.GetString("txtCertifiedRatio.JavaScript"));
			this.txtCertifiedRatio.Location = new System.Drawing.Point(585, 160);
			this.txtCertifiedRatio.LockedOriginal = true;
			this.txtCertifiedRatio.MaxLength = 0;
			this.txtCertifiedRatio.Name = "txtCertifiedRatio";
			this.txtCertifiedRatio.ReadOnly = true;
			this.txtCertifiedRatio.Size = new System.Drawing.Size(130, 40);
			this.txtCertifiedRatio.TabIndex = 4;
			this.txtCertifiedRatio.Text = "0";
			this.txtCertifiedRatio.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel11
			// 
			this.fcLabel11.AutoSize = true;
			this.fcLabel11.Location = new System.Drawing.Point(17, 170);
			this.fcLabel11.Name = "fcLabel11";
			this.fcLabel11.Size = new System.Drawing.Size(134, 15);
			this.fcLabel11.TabIndex = 36;
			this.fcLabel11.Text = "3 - CERTIFIED RATIO";
			this.fcLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.BackColor = System.Drawing.SystemColors.Window;
			this.txtMunicipality.Cursor = Wisej.Web.Cursors.Default;
			this.txtMunicipality.Location = new System.Drawing.Point(585, 110);
			this.txtMunicipality.MaxLength = 75;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Size = new System.Drawing.Size(130, 40);
			this.txtMunicipality.TabIndex = 3;
			// 
			// Label7
			// 
			this.Label7.AutoSize = true;
			this.Label7.Location = new System.Drawing.Point(17, 120);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(113, 15);
			this.Label7.TabIndex = 34;
			this.Label7.Text = "2 - MUNICIPALITY";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCounty
			// 
			this.txtCounty.BackColor = System.Drawing.SystemColors.Window;
			this.txtCounty.Cursor = Wisej.Web.Cursors.Default;
			this.txtCounty.Location = new System.Drawing.Point(585, 60);
			this.txtCounty.MaxLength = 30;
			this.txtCounty.Name = "txtCounty";
			this.txtCounty.Size = new System.Drawing.Size(130, 40);
			this.txtCounty.TabIndex = 2;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(17, 70);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(77, 15);
			this.Label2.TabIndex = 32;
			this.Label2.Text = "1 - COUNTY";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tabPage2
			// 
			this.tabPage2.AutoScroll = true;
			this.tabPage2.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.tabPage2.Controls.Add(this.fcFrame9);
			this.tabPage2.Controls.Add(this.fcFrame8);
			this.tabPage2.Controls.Add(this.fcFrame7);
			this.tabPage2.Controls.Add(this.fcFrame6);
			this.tabPage2.Controls.Add(this.fcFrame5);
			this.tabPage2.Location = new System.Drawing.Point(1, 35);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(966, 564);
			this.tabPage2.Text = "Page 2";
			// 
			// fcFrame9
			// 
			this.fcFrame9.Controls.Add(this.txtForestHardwoodRate);
			this.fcFrame9.Controls.Add(this.fcLabel40);
			this.fcFrame9.Controls.Add(this.txtForestMixedwoodRate);
			this.fcFrame9.Controls.Add(this.fcLabel41);
			this.fcFrame9.Controls.Add(this.txtForestSoftwoodRate);
			this.fcFrame9.Controls.Add(this.fcLabel35);
			this.fcFrame9.Controls.Add(this.txtForestTotalAssessed);
			this.fcFrame9.Controls.Add(this.lblTotalValuationForestLand);
			this.fcFrame9.Controls.Add(this.txtForestNumberOfAcres);
			this.fcFrame9.Controls.Add(this.fcLabel38);
			this.fcFrame9.Controls.Add(this.txtForestHardwoodAcreage);
			this.fcFrame9.Controls.Add(this.fcLabel39);
			this.fcFrame9.Controls.Add(this.txtForestMixedWoodAcreage);
			this.fcFrame9.Controls.Add(this.fcLabel33);
			this.fcFrame9.Controls.Add(this.txtForestSoftwoodAcreage);
			this.fcFrame9.Controls.Add(this.fcLabel34);
			this.fcFrame9.Controls.Add(this.txtForestNumberOfParcels);
			this.fcFrame9.Controls.Add(this.lblForestParcelsClassified);
			this.fcFrame9.Controls.Add(this.txtForestAveragePerAcre);
			this.fcFrame9.Controls.Add(this.fcLabel36);
			this.fcFrame9.Location = new System.Drawing.Point(14, 914);
			this.fcFrame9.Name = "fcFrame9";
			this.fcFrame9.Size = new System.Drawing.Size(728, 541);
			this.fcFrame9.TabIndex = 5;
			this.fcFrame9.Text = "Forest Land Classified Under The Tree Growth Tax Law";
			// 
			// txtForestHardwoodRate
			// 
			this.txtForestHardwoodRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestHardwoodRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestHardwoodRate.Focusable = false;
			this.txtForestHardwoodRate.Location = new System.Drawing.Point(571, 484);
			this.txtForestHardwoodRate.LockedOriginal = true;
			this.txtForestHardwoodRate.MaxLength = 0;
			this.txtForestHardwoodRate.Name = "txtForestHardwoodRate";
			this.txtForestHardwoodRate.ReadOnly = true;
			this.txtForestHardwoodRate.Size = new System.Drawing.Size(130, 40);
			this.txtForestHardwoodRate.TabIndex = 10;
			this.txtForestHardwoodRate.Text = "0";
			this.txtForestHardwoodRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel40
			// 
			this.fcLabel40.AutoSize = true;
			this.fcLabel40.Location = new System.Drawing.Point(9, 498);
			this.fcLabel40.Name = "fcLabel40";
			this.fcLabel40.Size = new System.Drawing.Size(212, 15);
			this.fcLabel40.TabIndex = 64;
			this.fcLabel40.Text = "22A(3) - HARDWOOD (PER ACRE)";
			this.fcLabel40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtForestMixedwoodRate
			// 
			this.txtForestMixedwoodRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestMixedwoodRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestMixedwoodRate.Focusable = false;
			this.txtForestMixedwoodRate.Location = new System.Drawing.Point(571, 433);
			this.txtForestMixedwoodRate.LockedOriginal = true;
			this.txtForestMixedwoodRate.MaxLength = 0;
			this.txtForestMixedwoodRate.Name = "txtForestMixedwoodRate";
			this.txtForestMixedwoodRate.ReadOnly = true;
			this.txtForestMixedwoodRate.Size = new System.Drawing.Size(130, 40);
			this.txtForestMixedwoodRate.TabIndex = 9;
			this.txtForestMixedwoodRate.Text = "0";
			this.txtForestMixedwoodRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestSoftwoodRate
			// 
			this.txtForestSoftwoodRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestSoftwoodRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestSoftwoodRate.Focusable = false;
			this.txtForestSoftwoodRate.Location = new System.Drawing.Point(571, 381);
			this.txtForestSoftwoodRate.LockedOriginal = true;
			this.txtForestSoftwoodRate.MaxLength = 0;
			this.txtForestSoftwoodRate.Name = "txtForestSoftwoodRate";
			this.txtForestSoftwoodRate.ReadOnly = true;
			this.txtForestSoftwoodRate.Size = new System.Drawing.Size(130, 40);
			this.txtForestSoftwoodRate.TabIndex = 8;
			this.txtForestSoftwoodRate.Text = "0";
			this.txtForestSoftwoodRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestTotalAssessed
			// 
			this.txtForestTotalAssessed.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestTotalAssessed.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestTotalAssessed.Focusable = false;
			this.txtForestTotalAssessed.Location = new System.Drawing.Point(571, 331);
			this.txtForestTotalAssessed.LockedOriginal = true;
			this.txtForestTotalAssessed.MaxLength = 0;
			this.txtForestTotalAssessed.Name = "txtForestTotalAssessed";
			this.txtForestTotalAssessed.ReadOnly = true;
			this.txtForestTotalAssessed.Size = new System.Drawing.Size(130, 40);
			this.txtForestTotalAssessed.TabIndex = 7;
			this.txtForestTotalAssessed.Text = "0";
			this.txtForestTotalAssessed.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblTotalValuationForestLand
			// 
			this.lblTotalValuationForestLand.AutoSize = true;
			this.lblTotalValuationForestLand.Location = new System.Drawing.Point(9, 345);
			this.lblTotalValuationForestLand.Name = "lblTotalValuationForestLand";
			this.lblTotalValuationForestLand.Size = new System.Drawing.Size(544, 15);
			this.lblTotalValuationForestLand.TabIndex = 58;
			this.lblTotalValuationForestLand.Text = "22 - TOTAL ASSESSED VALUATION OF ALL CLASSIFIED FOREST LAND FOR TAX YEAR";
			this.lblTotalValuationForestLand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtForestNumberOfAcres
			// 
			this.txtForestNumberOfAcres.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestNumberOfAcres.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestNumberOfAcres.Focusable = false;
			this.txtForestNumberOfAcres.Location = new System.Drawing.Point(571, 280);
			this.txtForestNumberOfAcres.LockedOriginal = true;
			this.txtForestNumberOfAcres.MaxLength = 0;
			this.txtForestNumberOfAcres.Name = "txtForestNumberOfAcres";
			this.txtForestNumberOfAcres.ReadOnly = true;
			this.txtForestNumberOfAcres.Size = new System.Drawing.Size(130, 40);
			this.txtForestNumberOfAcres.TabIndex = 6;
			this.txtForestNumberOfAcres.Text = "0";
			this.txtForestNumberOfAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestHardwoodAcreage
			// 
			this.txtForestHardwoodAcreage.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestHardwoodAcreage.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestHardwoodAcreage.Focusable = false;
			this.txtForestHardwoodAcreage.Location = new System.Drawing.Point(571, 230);
			this.txtForestHardwoodAcreage.LockedOriginal = true;
			this.txtForestHardwoodAcreage.MaxLength = 0;
			this.txtForestHardwoodAcreage.Name = "txtForestHardwoodAcreage";
			this.txtForestHardwoodAcreage.ReadOnly = true;
			this.txtForestHardwoodAcreage.Size = new System.Drawing.Size(130, 40);
			this.txtForestHardwoodAcreage.TabIndex = 5;
			this.txtForestHardwoodAcreage.Text = "0";
			this.txtForestHardwoodAcreage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel39
			// 
			this.fcLabel39.Location = new System.Drawing.Point(9, 230);
			this.fcLabel39.Name = "fcLabel39";
			this.fcLabel39.Size = new System.Drawing.Size(505, 40);
			this.fcLabel39.TabIndex = 54;
			this.fcLabel39.Text = "21D - HARDWOOD ACREAGE";
			this.fcLabel39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtForestMixedWoodAcreage
			// 
			this.txtForestMixedWoodAcreage.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestMixedWoodAcreage.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestMixedWoodAcreage.Focusable = false;
			this.txtForestMixedWoodAcreage.Location = new System.Drawing.Point(571, 180);
			this.txtForestMixedWoodAcreage.LockedOriginal = true;
			this.txtForestMixedWoodAcreage.MaxLength = 0;
			this.txtForestMixedWoodAcreage.Name = "txtForestMixedWoodAcreage";
			this.txtForestMixedWoodAcreage.ReadOnly = true;
			this.txtForestMixedWoodAcreage.Size = new System.Drawing.Size(130, 40);
			this.txtForestMixedWoodAcreage.TabIndex = 4;
			this.txtForestMixedWoodAcreage.Text = "0";
			this.txtForestMixedWoodAcreage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestSoftwoodAcreage
			// 
			this.txtForestSoftwoodAcreage.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestSoftwoodAcreage.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestSoftwoodAcreage.Focusable = false;
			this.txtForestSoftwoodAcreage.Location = new System.Drawing.Point(571, 130);
			this.txtForestSoftwoodAcreage.LockedOriginal = true;
			this.txtForestSoftwoodAcreage.MaxLength = 0;
			this.txtForestSoftwoodAcreage.Name = "txtForestSoftwoodAcreage";
			this.txtForestSoftwoodAcreage.ReadOnly = true;
			this.txtForestSoftwoodAcreage.Size = new System.Drawing.Size(130, 40);
			this.txtForestSoftwoodAcreage.TabIndex = 3;
			this.txtForestSoftwoodAcreage.Text = "0";
			this.txtForestSoftwoodAcreage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel34
			// 
			this.fcLabel34.AutoSize = true;
			this.fcLabel34.Location = new System.Drawing.Point(9, 144);
			this.fcLabel34.Name = "fcLabel34";
			this.fcLabel34.Size = new System.Drawing.Size(182, 15);
			this.fcLabel34.TabIndex = 50;
			this.fcLabel34.Text = "21B - SOFTWOOD ACREAGE";
			this.fcLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtForestNumberOfParcels
			// 
			this.txtForestNumberOfParcels.BackColor = System.Drawing.Color.FromName("@control");
			this.txtForestNumberOfParcels.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestNumberOfParcels.Focusable = false;
			this.txtForestNumberOfParcels.Location = new System.Drawing.Point(571, 79);
			this.txtForestNumberOfParcels.LockedOriginal = true;
			this.txtForestNumberOfParcels.MaxLength = 0;
			this.txtForestNumberOfParcels.Name = "txtForestNumberOfParcels";
			this.txtForestNumberOfParcels.ReadOnly = true;
			this.txtForestNumberOfParcels.Size = new System.Drawing.Size(130, 40);
			this.txtForestNumberOfParcels.TabIndex = 2;
			this.txtForestNumberOfParcels.Text = "0";
			this.txtForestNumberOfParcels.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestAveragePerAcre
			// 
			this.txtForestAveragePerAcre.BackColor = System.Drawing.SystemColors.Window;
			this.txtForestAveragePerAcre.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestAveragePerAcre.Location = new System.Drawing.Point(571, 29);
			this.txtForestAveragePerAcre.MaxLength = 0;
			this.txtForestAveragePerAcre.Name = "txtForestAveragePerAcre";
			this.txtForestAveragePerAcre.Size = new System.Drawing.Size(130, 40);
			this.txtForestAveragePerAcre.TabIndex = 1;
			this.txtForestAveragePerAcre.Text = "0";
			this.txtForestAveragePerAcre.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel36
			// 
			this.fcLabel36.Location = new System.Drawing.Point(9, 29);
			this.fcLabel36.Name = "fcLabel36";
			this.fcLabel36.Size = new System.Drawing.Size(505, 40);
			this.fcLabel36.TabIndex = 46;
			this.fcLabel36.Text = "20 - AVERAGE PER ACRE UNIT VALUE UTILIZED FOR UNDEVELOPED ACREAGE (LAND NOT CLASS" +
    "IFIED)";
			this.fcLabel36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame8
			// 
			this.fcFrame8.Controls.Add(this.txtElectricalGenerationFacilities);
			this.fcFrame8.Controls.Add(this.fcLabel31);
			this.fcFrame8.Controls.Add(this.txtTransmissionLines);
			this.fcFrame8.Controls.Add(this.fcLabel32);
			this.fcFrame8.Location = new System.Drawing.Point(14, 763);
			this.fcFrame8.Name = "fcFrame8";
			this.fcFrame8.Size = new System.Drawing.Size(728, 135);
			this.fcFrame8.TabIndex = 4;
			this.fcFrame8.Text = "Electrical Generation And Distribution Property";
			// 
			// txtElectricalGenerationFacilities
			// 
			this.txtElectricalGenerationFacilities.BackColor = System.Drawing.SystemColors.Window;
			this.txtElectricalGenerationFacilities.Cursor = Wisej.Web.Cursors.Default;
			this.txtElectricalGenerationFacilities.Location = new System.Drawing.Point(571, 78);
			this.txtElectricalGenerationFacilities.MaxLength = 0;
			this.txtElectricalGenerationFacilities.Name = "txtElectricalGenerationFacilities";
			this.txtElectricalGenerationFacilities.Size = new System.Drawing.Size(130, 40);
			this.txtElectricalGenerationFacilities.TabIndex = 2;
			this.txtElectricalGenerationFacilities.Text = "0";
			this.txtElectricalGenerationFacilities.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTransmissionLines
			// 
			this.txtTransmissionLines.BackColor = System.Drawing.SystemColors.Window;
			this.txtTransmissionLines.Cursor = Wisej.Web.Cursors.Default;
			this.txtTransmissionLines.Location = new System.Drawing.Point(571, 28);
			this.txtTransmissionLines.MaxLength = 0;
			this.txtTransmissionLines.Name = "txtTransmissionLines";
			this.txtTransmissionLines.Size = new System.Drawing.Size(130, 40);
			this.txtTransmissionLines.TabIndex = 1;
			this.txtTransmissionLines.Text = "0";
			this.txtTransmissionLines.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel32
			// 
			this.fcLabel32.Location = new System.Drawing.Point(9, 28);
			this.fcLabel32.Name = "fcLabel32";
			this.fcLabel32.Size = new System.Drawing.Size(530, 40);
			this.fcLabel32.TabIndex = 42;
			this.fcLabel32.Text = "18 - TOTAL VALUATION OF DISTRIBUTION AND TRANSMISSION LINES OWNED BY ELECTRIC UTI" +
    "LITY COMPANIES";
			this.fcLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame7
			// 
			this.fcFrame7.Controls.Add(this.cmbExcisePeriodType);
			this.fcFrame7.Controls.Add(this.txtWatercraftExcise);
			this.fcFrame7.Controls.Add(this.fcLabel28);
			this.fcFrame7.Controls.Add(this.txtMVExcise);
			this.fcFrame7.Controls.Add(this.fcLabel29);
			this.fcFrame7.Controls.Add(this.fcLabel30);
			this.fcFrame7.Location = new System.Drawing.Point(14, 567);
			this.fcFrame7.Name = "fcFrame7";
			this.fcFrame7.Size = new System.Drawing.Size(728, 179);
			this.fcFrame7.TabIndex = 3;
			this.fcFrame7.Text = "Excise Tax";
			// 
			// cmbExcisePeriodType
			// 
			this.cmbExcisePeriodType.Location = new System.Drawing.Point(571, 25);
			this.cmbExcisePeriodType.Name = "cmbExcisePeriodType";
			this.cmbExcisePeriodType.Size = new System.Drawing.Size(129, 40);
			this.cmbExcisePeriodType.TabIndex = 1;
			// 
			// txtWatercraftExcise
			// 
			this.txtWatercraftExcise.BackColor = System.Drawing.SystemColors.Window;
			this.txtWatercraftExcise.Cursor = Wisej.Web.Cursors.Default;
			this.txtWatercraftExcise.Location = new System.Drawing.Point(571, 125);
			this.txtWatercraftExcise.MaxLength = 0;
			this.txtWatercraftExcise.Name = "txtWatercraftExcise";
			this.txtWatercraftExcise.Size = new System.Drawing.Size(130, 40);
			this.txtWatercraftExcise.TabIndex = 3;
			this.txtWatercraftExcise.Text = "49";
			this.txtWatercraftExcise.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel28
			// 
			this.fcLabel28.AutoSize = true;
			this.fcLabel28.Location = new System.Drawing.Point(9, 139);
			this.fcLabel28.Name = "fcLabel28";
			this.fcLabel28.Size = new System.Drawing.Size(291, 15);
			this.fcLabel28.TabIndex = 48;
			this.fcLabel28.Text = "17C - WATERCRAFT EXCISE TAX COLLECTED";
			this.fcLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMVExcise
			// 
			this.txtMVExcise.BackColor = System.Drawing.SystemColors.Window;
			this.txtMVExcise.Cursor = Wisej.Web.Cursors.Default;
			this.txtMVExcise.Location = new System.Drawing.Point(571, 74);
			this.txtMVExcise.MaxLength = 0;
			this.txtMVExcise.Name = "txtMVExcise";
			this.txtMVExcise.Size = new System.Drawing.Size(130, 40);
			this.txtMVExcise.TabIndex = 2;
			this.txtMVExcise.Text = "0";
			this.txtMVExcise.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel30
			// 
			this.fcLabel30.Location = new System.Drawing.Point(9, 24);
			this.fcLabel30.Name = "fcLabel30";
			this.fcLabel30.Size = new System.Drawing.Size(556, 40);
			this.fcLabel30.TabIndex = 44;
			this.fcLabel30.Text = "17A - EXCISE TAXES COLLECTED DURING A RECENTLY COMPLETED TWELVE MONTH PERIOD. ENT" +
    "ER EITHER CALENDAR OR FISCAL";
			this.fcLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame6
			// 
			this.fcFrame6.Controls.Add(this.txtTifBeteDeposited);
			this.fcFrame6.Controls.Add(this.fcLabel24);
			this.fcFrame6.Controls.Add(this.txtTifDeposited);
			this.fcFrame6.Controls.Add(this.fcLabel25);
			this.fcFrame6.Controls.Add(this.txtTifCaptured);
			this.fcFrame6.Controls.Add(this.fcLabel26);
			this.fcFrame6.Controls.Add(this.txtIncreasedTif);
			this.fcFrame6.Controls.Add(this.fcLabel27);
			this.fcFrame6.Location = new System.Drawing.Point(14, 275);
			this.fcFrame6.Name = "fcFrame6";
			this.fcFrame6.Size = new System.Drawing.Size(728, 274);
			this.fcFrame6.TabIndex = 2;
			this.fcFrame6.Text = "Tax Increment Financing";
			// 
			// txtTifBeteDeposited
			// 
			this.txtTifBeteDeposited.BackColor = System.Drawing.SystemColors.Window;
			this.txtTifBeteDeposited.Cursor = Wisej.Web.Cursors.Default;
			this.txtTifBeteDeposited.Location = new System.Drawing.Point(571, 180);
			this.txtTifBeteDeposited.MaxLength = 0;
			this.txtTifBeteDeposited.Name = "txtTifBeteDeposited";
			this.txtTifBeteDeposited.Size = new System.Drawing.Size(130, 40);
			this.txtTifBeteDeposited.TabIndex = 4;
			this.txtTifBeteDeposited.Text = "0";
			this.txtTifBeteDeposited.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTifDeposited
			// 
			this.txtTifDeposited.BackColor = System.Drawing.SystemColors.Window;
			this.txtTifDeposited.Cursor = Wisej.Web.Cursors.Default;
			this.txtTifDeposited.Location = new System.Drawing.Point(571, 130);
			this.txtTifDeposited.MaxLength = 0;
			this.txtTifDeposited.Name = "txtTifDeposited";
			this.txtTifDeposited.Size = new System.Drawing.Size(130, 40);
			this.txtTifDeposited.TabIndex = 3;
			this.txtTifDeposited.Text = "0";
			this.txtTifDeposited.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel25
			// 
			this.fcLabel25.Location = new System.Drawing.Point(9, 130);
			this.fcLabel25.Name = "fcLabel25";
			this.fcLabel25.Size = new System.Drawing.Size(544, 40);
			this.fcLabel25.TabIndex = 50;
			this.fcLabel25.Text = "16C - PROPERTY TAX REVENUE THAT IS APPROPRIATED AND DEPOSITED INTO EITHER A PROJE" +
    "CT COST ACCOUNT OR A SINKING FUND ACCOUNT";
			this.fcLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTifCaptured
			// 
			this.txtTifCaptured.BackColor = System.Drawing.SystemColors.Window;
			this.txtTifCaptured.Cursor = Wisej.Web.Cursors.Default;
			this.txtTifCaptured.Location = new System.Drawing.Point(571, 79);
			this.txtTifCaptured.MaxLength = 0;
			this.txtTifCaptured.Name = "txtTifCaptured";
			this.txtTifCaptured.Size = new System.Drawing.Size(130, 40);
			this.txtTifCaptured.TabIndex = 2;
			this.txtTifCaptured.Text = "0";
			this.txtTifCaptured.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtIncreasedTif
			// 
			this.txtIncreasedTif.BackColor = System.Drawing.SystemColors.Window;
			this.txtIncreasedTif.Cursor = Wisej.Web.Cursors.Default;
			this.txtIncreasedTif.Location = new System.Drawing.Point(571, 29);
			this.txtIncreasedTif.MaxLength = 0;
			this.txtIncreasedTif.Name = "txtIncreasedTif";
			this.txtIncreasedTif.Size = new System.Drawing.Size(130, 40);
			this.txtIncreasedTif.TabIndex = 1;
			this.txtIncreasedTif.Text = "0";
			this.txtIncreasedTif.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel27
			// 
			this.fcLabel27.Location = new System.Drawing.Point(9, 29);
			this.fcLabel27.Name = "fcLabel27";
			this.fcLabel27.Size = new System.Drawing.Size(556, 40);
			this.fcLabel27.TabIndex = 46;
			this.fcLabel27.Text = "16A - TOTAL AMOUNT OF INCREASED TAXABLE VALUATION ABOVE ORIGINAL ASSESSED VALUE W" +
    "ITHIN TIF DISTRICTS";
			this.fcLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame5
			// 
			this.fcFrame5.Controls.Add(this.txtBeteInTIF);
			this.fcFrame5.Controls.Add(this.fcLabel3);
			this.fcFrame5.Controls.Add(this.txtExemptValueBeteQualified);
			this.fcFrame5.Controls.Add(this.fcLabel5);
			this.fcFrame5.Controls.Add(this.txtNumberBeteApproved);
			this.fcFrame5.Controls.Add(this.fcLabel7);
			this.fcFrame5.Controls.Add(this.txtNumberBeteApplicationsProcessed);
			this.fcFrame5.Controls.Add(this.fcLabel8);
			this.fcFrame5.Location = new System.Drawing.Point(14, 18);
			this.fcFrame5.Name = "fcFrame5";
			this.fcFrame5.Size = new System.Drawing.Size(728, 240);
			this.fcFrame5.TabIndex = 1;
			this.fcFrame5.Text = "Business Equipment Tax Exemption Reimbursement Claim";
			// 
			// txtBeteInTIF
			// 
			this.txtBeteInTIF.BackColor = System.Drawing.SystemColors.Window;
			this.txtBeteInTIF.Cursor = Wisej.Web.Cursors.Default;
			this.txtBeteInTIF.Location = new System.Drawing.Point(571, 177);
			this.txtBeteInTIF.MaxLength = 0;
			this.txtBeteInTIF.Name = "txtBeteInTIF";
			this.txtBeteInTIF.Size = new System.Drawing.Size(130, 40);
			this.txtBeteInTIF.TabIndex = 4;
			this.txtBeteInTIF.Text = "0";
			this.txtBeteInTIF.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptValueBeteQualified
			// 
			this.txtExemptValueBeteQualified.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptValueBeteQualified.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptValueBeteQualified.Location = new System.Drawing.Point(571, 127);
			this.txtExemptValueBeteQualified.MaxLength = 0;
			this.txtExemptValueBeteQualified.Name = "txtExemptValueBeteQualified";
			this.txtExemptValueBeteQualified.Size = new System.Drawing.Size(130, 40);
			this.txtExemptValueBeteQualified.TabIndex = 3;
			this.txtExemptValueBeteQualified.Text = "0";
			this.txtExemptValueBeteQualified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel5
			// 
			this.fcLabel5.AutoSize = true;
			this.fcLabel5.Location = new System.Drawing.Point(9, 141);
			this.fcLabel5.Name = "fcLabel5";
			this.fcLabel5.Size = new System.Drawing.Size(417, 15);
			this.fcLabel5.TabIndex = 42;
			this.fcLabel5.Text = "15C - TOTAL EXEMPT VALUE OF ALL BETE QUALIFIED PROPERTY";
			this.fcLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNumberBeteApproved
			// 
			this.txtNumberBeteApproved.BackColor = System.Drawing.SystemColors.Window;
			this.txtNumberBeteApproved.Cursor = Wisej.Web.Cursors.Default;
			this.txtNumberBeteApproved.Location = new System.Drawing.Point(571, 76);
			this.txtNumberBeteApproved.MaxLength = 0;
			this.txtNumberBeteApproved.Name = "txtNumberBeteApproved";
			this.txtNumberBeteApproved.Size = new System.Drawing.Size(130, 40);
			this.txtNumberBeteApproved.TabIndex = 2;
			this.txtNumberBeteApproved.Text = "0";
			this.txtNumberBeteApproved.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtNumberBeteApplicationsProcessed
			// 
			this.txtNumberBeteApplicationsProcessed.BackColor = System.Drawing.SystemColors.Window;
			this.txtNumberBeteApplicationsProcessed.Cursor = Wisej.Web.Cursors.Default;
			this.txtNumberBeteApplicationsProcessed.Location = new System.Drawing.Point(571, 26);
			this.txtNumberBeteApplicationsProcessed.MaxLength = 0;
			this.txtNumberBeteApplicationsProcessed.Name = "txtNumberBeteApplicationsProcessed";
			this.txtNumberBeteApplicationsProcessed.Size = new System.Drawing.Size(130, 40);
			this.txtNumberBeteApplicationsProcessed.TabIndex = 1;
			this.txtNumberBeteApplicationsProcessed.Text = "0";
			this.txtNumberBeteApplicationsProcessed.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel8
			// 
			this.fcLabel8.AutoSize = true;
			this.fcLabel8.Location = new System.Drawing.Point(9, 40);
			this.fcLabel8.Name = "fcLabel8";
			this.fcLabel8.Size = new System.Drawing.Size(466, 15);
			this.fcLabel8.TabIndex = 38;
			this.fcLabel8.Text = "15A - NUMBER OF BETE APPLICATIONS PROCESSED FOR THE TAX YEAR";
			this.fcLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcTabPage1
			// 
			this.fcTabPage1.AutoScroll = true;
			this.fcTabPage1.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage1.Controls.Add(this.fcFrame11);
			this.fcTabPage1.Controls.Add(this.fcFrame10);
			this.fcTabPage1.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage1.Name = "fcTabPage1";
			this.fcTabPage1.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage1.Text = "Page 3";
			// 
			// fcFrame11
			// 
			this.fcFrame11.Controls.Add(this.txtOpenTotalValuation);
			this.fcFrame11.Controls.Add(this.fcLabel56);
			this.fcFrame11.Controls.Add(this.txtOpenTotalAcres);
			this.fcFrame11.Controls.Add(this.fcLabel57);
			this.fcFrame11.Controls.Add(this.txtOpenAcresFirstClassified);
			this.fcFrame11.Controls.Add(this.fcLabel58);
			this.fcFrame11.Controls.Add(this.txtOpenParcelsFirstClassified);
			this.fcFrame11.Controls.Add(this.fcLabel59);
			this.fcFrame11.Controls.Add(this.txtFarmTotalPenalties);
			this.fcFrame11.Controls.Add(this.fcLabel60);
			this.fcFrame11.Controls.Add(this.txtFarmAcresWithdrawn);
			this.fcFrame11.Controls.Add(this.fcLabel61);
			this.fcFrame11.Controls.Add(this.txtFarmParcelsWithdrawn);
			this.fcFrame11.Controls.Add(this.fcLabel62);
			this.fcFrame11.Controls.Add(this.txtFarmHardwoodRate);
			this.fcFrame11.Controls.Add(this.fcLabel50);
			this.fcFrame11.Controls.Add(this.txtFarmMixedWoodRate);
			this.fcFrame11.Controls.Add(this.fcLabel51);
			this.fcFrame11.Controls.Add(this.txtFarmSoftwoodRate);
			this.fcFrame11.Controls.Add(this.fcLabel52);
			this.fcFrame11.Controls.Add(this.txtFarmTotalValuationWoodland);
			this.fcFrame11.Controls.Add(this.fcLabel53);
			this.fcFrame11.Controls.Add(this.txtFarmTotalAcresWoodland);
			this.fcFrame11.Controls.Add(this.fcLabel54);
			this.fcFrame11.Controls.Add(this.txtFarmHardwoodAcres);
			this.fcFrame11.Controls.Add(this.fcLabel55);
			this.fcFrame11.Controls.Add(this.txtFarmMixedWoodAcres);
			this.fcFrame11.Controls.Add(this.fcLabel44);
			this.fcFrame11.Controls.Add(this.txtFarmSoftwoodAcres);
			this.fcFrame11.Controls.Add(this.fcLabel45);
			this.fcFrame11.Controls.Add(this.txtFarmTotalValuationTillable);
			this.fcFrame11.Controls.Add(this.fcLabel46);
			this.fcFrame11.Controls.Add(this.txtFarmTotalAcresTillable);
			this.fcFrame11.Controls.Add(this.fcLabel47);
			this.fcFrame11.Controls.Add(this.txtFarmNumberOfAcresFirstClassified);
			this.fcFrame11.Controls.Add(this.fcLabel48);
			this.fcFrame11.Controls.Add(this.txtFarmNumberOfParcelsFirstClassified);
			this.fcFrame11.Controls.Add(this.fcLabel49);
			this.fcFrame11.Location = new System.Drawing.Point(15, 377);
			this.fcFrame11.Name = "fcFrame11";
			this.fcFrame11.Size = new System.Drawing.Size(722, 1009);
			this.fcFrame11.TabIndex = 2;
			this.fcFrame11.Text = "Land Classified Under The Farm And Open Space Tax Law Program";
			// 
			// txtOpenTotalValuation
			// 
			this.txtOpenTotalValuation.BackColor = System.Drawing.Color.FromName("@control");
			this.txtOpenTotalValuation.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenTotalValuation.Focusable = false;
			this.txtOpenTotalValuation.Location = new System.Drawing.Point(570, 949);
			this.txtOpenTotalValuation.LockedOriginal = true;
			this.txtOpenTotalValuation.MaxLength = 0;
			this.txtOpenTotalValuation.Name = "txtOpenTotalValuation";
			this.txtOpenTotalValuation.ReadOnly = true;
			this.txtOpenTotalValuation.Size = new System.Drawing.Size(130, 40);
			this.txtOpenTotalValuation.TabIndex = 19;
			this.txtOpenTotalValuation.Text = "0";
			this.txtOpenTotalValuation.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel56
			// 
			this.fcLabel56.Location = new System.Drawing.Point(8, 949);
			this.fcLabel56.Name = "fcLabel56";
			this.fcLabel56.Size = new System.Drawing.Size(526, 40);
			this.fcLabel56.TabIndex = 114;
			this.fcLabel56.Text = "33 - TOTAL VALUATION OF ALL LAND NOW CLASSIFIED AS OPEN SPACE";
			this.fcLabel56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtOpenTotalAcres
			// 
			this.txtOpenTotalAcres.BackColor = System.Drawing.Color.FromName("@control");
			this.txtOpenTotalAcres.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenTotalAcres.Focusable = false;
			this.txtOpenTotalAcres.Location = new System.Drawing.Point(570, 898);
			this.txtOpenTotalAcres.LockedOriginal = true;
			this.txtOpenTotalAcres.MaxLength = 0;
			this.txtOpenTotalAcres.Name = "txtOpenTotalAcres";
			this.txtOpenTotalAcres.ReadOnly = true;
			this.txtOpenTotalAcres.Size = new System.Drawing.Size(130, 40);
			this.txtOpenTotalAcres.TabIndex = 18;
			this.txtOpenTotalAcres.Text = "0";
			this.txtOpenTotalAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtOpenAcresFirstClassified
			// 
			this.txtOpenAcresFirstClassified.BackColor = System.Drawing.SystemColors.Window;
			this.txtOpenAcresFirstClassified.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenAcresFirstClassified.Location = new System.Drawing.Point(570, 846);
			this.txtOpenAcresFirstClassified.MaxLength = 0;
			this.txtOpenAcresFirstClassified.Name = "txtOpenAcresFirstClassified";
			this.txtOpenAcresFirstClassified.Size = new System.Drawing.Size(130, 40);
			this.txtOpenAcresFirstClassified.TabIndex = 17;
			this.txtOpenAcresFirstClassified.Text = "0";
			this.txtOpenAcresFirstClassified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtOpenParcelsFirstClassified
			// 
			this.txtOpenParcelsFirstClassified.BackColor = System.Drawing.Color.FromName("@control");
			this.txtOpenParcelsFirstClassified.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenParcelsFirstClassified.Focusable = false;
			this.txtOpenParcelsFirstClassified.Location = new System.Drawing.Point(570, 796);
			this.txtOpenParcelsFirstClassified.LockedOriginal = true;
			this.txtOpenParcelsFirstClassified.MaxLength = 0;
			this.txtOpenParcelsFirstClassified.Name = "txtOpenParcelsFirstClassified";
			this.txtOpenParcelsFirstClassified.ReadOnly = true;
			this.txtOpenParcelsFirstClassified.Size = new System.Drawing.Size(130, 40);
			this.txtOpenParcelsFirstClassified.TabIndex = 16;
			this.txtOpenParcelsFirstClassified.Text = "0";
			this.txtOpenParcelsFirstClassified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel59
			// 
			this.fcLabel59.Location = new System.Drawing.Point(8, 796);
			this.fcLabel59.Name = "fcLabel59";
			this.fcLabel59.Size = new System.Drawing.Size(543, 40);
			this.fcLabel59.TabIndex = 108;
			this.fcLabel59.Text = "30 - NUMBER OF PARCELS CLASSIFIED AS OPEN SPACE AS OF APRIL 1";
			this.fcLabel59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmTotalPenalties
			// 
			this.txtFarmTotalPenalties.BackColor = System.Drawing.SystemColors.Window;
			this.txtFarmTotalPenalties.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmTotalPenalties.Location = new System.Drawing.Point(570, 745);
			this.txtFarmTotalPenalties.MaxLength = 0;
			this.txtFarmTotalPenalties.Name = "txtFarmTotalPenalties";
			this.txtFarmTotalPenalties.Size = new System.Drawing.Size(130, 40);
			this.txtFarmTotalPenalties.TabIndex = 15;
			this.txtFarmTotalPenalties.Text = "0";
			this.txtFarmTotalPenalties.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmAcresWithdrawn
			// 
			this.txtFarmAcresWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtFarmAcresWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmAcresWithdrawn.Location = new System.Drawing.Point(570, 695);
			this.txtFarmAcresWithdrawn.MaxLength = 0;
			this.txtFarmAcresWithdrawn.Name = "txtFarmAcresWithdrawn";
			this.txtFarmAcresWithdrawn.Size = new System.Drawing.Size(130, 40);
			this.txtFarmAcresWithdrawn.TabIndex = 14;
			this.txtFarmAcresWithdrawn.Text = "0";
			this.txtFarmAcresWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel61
			// 
			this.fcLabel61.Location = new System.Drawing.Point(8, 695);
			this.fcLabel61.Name = "fcLabel61";
			this.fcLabel61.Size = new System.Drawing.Size(505, 40);
			this.fcLabel61.TabIndex = 104;
			this.fcLabel61.Text = "29B - TOTAL NUMBER OF FARMLAND ACRES WITHDRAWN FROM";
			this.fcLabel61.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmParcelsWithdrawn
			// 
			this.txtFarmParcelsWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtFarmParcelsWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmParcelsWithdrawn.Location = new System.Drawing.Point(570, 643);
			this.txtFarmParcelsWithdrawn.MaxLength = 0;
			this.txtFarmParcelsWithdrawn.Name = "txtFarmParcelsWithdrawn";
			this.txtFarmParcelsWithdrawn.Size = new System.Drawing.Size(130, 40);
			this.txtFarmParcelsWithdrawn.TabIndex = 13;
			this.txtFarmParcelsWithdrawn.Text = "0";
			this.txtFarmParcelsWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel62
			// 
			this.fcLabel62.Location = new System.Drawing.Point(8, 643);
			this.fcLabel62.Name = "fcLabel62";
			this.fcLabel62.Size = new System.Drawing.Size(526, 40);
			this.fcLabel62.TabIndex = 102;
			this.fcLabel62.Text = "29A - TOTAL NUMBER OF FARMLAND PARCELS WITHDRAWN FROM";
			this.fcLabel62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmHardwoodRate
			// 
			this.txtFarmHardwoodRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmHardwoodRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmHardwoodRate.Focusable = false;
			this.txtFarmHardwoodRate.Location = new System.Drawing.Point(570, 591);
			this.txtFarmHardwoodRate.LockedOriginal = true;
			this.txtFarmHardwoodRate.MaxLength = 0;
			this.txtFarmHardwoodRate.Name = "txtFarmHardwoodRate";
			this.txtFarmHardwoodRate.ReadOnly = true;
			this.txtFarmHardwoodRate.Size = new System.Drawing.Size(130, 40);
			this.txtFarmHardwoodRate.TabIndex = 12;
			this.txtFarmHardwoodRate.Text = "0";
			this.txtFarmHardwoodRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel50
			// 
			this.fcLabel50.Location = new System.Drawing.Point(8, 591);
			this.fcLabel50.Name = "fcLabel50";
			this.fcLabel50.Size = new System.Drawing.Size(526, 40);
			this.fcLabel50.TabIndex = 100;
			this.fcLabel50.Text = "28D(3) - FARM HARDWOOD PER ACRE RATE";
			this.fcLabel50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmMixedWoodRate
			// 
			this.txtFarmMixedWoodRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmMixedWoodRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmMixedWoodRate.Focusable = false;
			this.txtFarmMixedWoodRate.Location = new System.Drawing.Point(570, 540);
			this.txtFarmMixedWoodRate.LockedOriginal = true;
			this.txtFarmMixedWoodRate.MaxLength = 0;
			this.txtFarmMixedWoodRate.Name = "txtFarmMixedWoodRate";
			this.txtFarmMixedWoodRate.ReadOnly = true;
			this.txtFarmMixedWoodRate.Size = new System.Drawing.Size(130, 40);
			this.txtFarmMixedWoodRate.TabIndex = 11;
			this.txtFarmMixedWoodRate.Text = "0";
			this.txtFarmMixedWoodRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmSoftwoodRate
			// 
			this.txtFarmSoftwoodRate.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmSoftwoodRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmSoftwoodRate.Focusable = false;
			this.txtFarmSoftwoodRate.Location = new System.Drawing.Point(570, 488);
			this.txtFarmSoftwoodRate.LockedOriginal = true;
			this.txtFarmSoftwoodRate.MaxLength = 0;
			this.txtFarmSoftwoodRate.Name = "txtFarmSoftwoodRate";
			this.txtFarmSoftwoodRate.ReadOnly = true;
			this.txtFarmSoftwoodRate.Size = new System.Drawing.Size(130, 40);
			this.txtFarmSoftwoodRate.TabIndex = 10;
			this.txtFarmSoftwoodRate.Text = "0";
			this.txtFarmSoftwoodRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmTotalValuationWoodland
			// 
			this.txtFarmTotalValuationWoodland.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmTotalValuationWoodland.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmTotalValuationWoodland.Focusable = false;
			this.txtFarmTotalValuationWoodland.Location = new System.Drawing.Point(570, 438);
			this.txtFarmTotalValuationWoodland.LockedOriginal = true;
			this.txtFarmTotalValuationWoodland.MaxLength = 0;
			this.txtFarmTotalValuationWoodland.Name = "txtFarmTotalValuationWoodland";
			this.txtFarmTotalValuationWoodland.ReadOnly = true;
			this.txtFarmTotalValuationWoodland.Size = new System.Drawing.Size(130, 40);
			this.txtFarmTotalValuationWoodland.TabIndex = 9;
			this.txtFarmTotalValuationWoodland.Text = "0";
			this.txtFarmTotalValuationWoodland.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel53
			// 
			this.fcLabel53.Location = new System.Drawing.Point(8, 438);
			this.fcLabel53.Name = "fcLabel53";
			this.fcLabel53.Size = new System.Drawing.Size(543, 40);
			this.fcLabel53.TabIndex = 94;
			this.fcLabel53.Text = "28C - TOTAL VALUATION OF ALL LAND NOW CLASSIFIED AS FARM WOODLAND";
			this.fcLabel53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmTotalAcresWoodland
			// 
			this.txtFarmTotalAcresWoodland.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmTotalAcresWoodland.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmTotalAcresWoodland.Focusable = false;
			this.txtFarmTotalAcresWoodland.Location = new System.Drawing.Point(570, 387);
			this.txtFarmTotalAcresWoodland.LockedOriginal = true;
			this.txtFarmTotalAcresWoodland.MaxLength = 0;
			this.txtFarmTotalAcresWoodland.Name = "txtFarmTotalAcresWoodland";
			this.txtFarmTotalAcresWoodland.ReadOnly = true;
			this.txtFarmTotalAcresWoodland.Size = new System.Drawing.Size(130, 40);
			this.txtFarmTotalAcresWoodland.TabIndex = 8;
			this.txtFarmTotalAcresWoodland.Text = "0";
			this.txtFarmTotalAcresWoodland.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmHardwoodAcres
			// 
			this.txtFarmHardwoodAcres.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmHardwoodAcres.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmHardwoodAcres.Focusable = false;
			this.txtFarmHardwoodAcres.Location = new System.Drawing.Point(570, 337);
			this.txtFarmHardwoodAcres.LockedOriginal = true;
			this.txtFarmHardwoodAcres.MaxLength = 0;
			this.txtFarmHardwoodAcres.Name = "txtFarmHardwoodAcres";
			this.txtFarmHardwoodAcres.ReadOnly = true;
			this.txtFarmHardwoodAcres.Size = new System.Drawing.Size(130, 40);
			this.txtFarmHardwoodAcres.TabIndex = 7;
			this.txtFarmHardwoodAcres.Text = "0";
			this.txtFarmHardwoodAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel55
			// 
			this.fcLabel55.Location = new System.Drawing.Point(8, 337);
			this.fcLabel55.Name = "fcLabel55";
			this.fcLabel55.Size = new System.Drawing.Size(505, 40);
			this.fcLabel55.TabIndex = 90;
			this.fcLabel55.Text = "28A(3) - NUMBER OF FARM HARDWOOD ACRES";
			this.fcLabel55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmMixedWoodAcres
			// 
			this.txtFarmMixedWoodAcres.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmMixedWoodAcres.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmMixedWoodAcres.Focusable = false;
			this.txtFarmMixedWoodAcres.Location = new System.Drawing.Point(570, 285);
			this.txtFarmMixedWoodAcres.LockedOriginal = true;
			this.txtFarmMixedWoodAcres.MaxLength = 0;
			this.txtFarmMixedWoodAcres.Name = "txtFarmMixedWoodAcres";
			this.txtFarmMixedWoodAcres.ReadOnly = true;
			this.txtFarmMixedWoodAcres.Size = new System.Drawing.Size(130, 40);
			this.txtFarmMixedWoodAcres.TabIndex = 6;
			this.txtFarmMixedWoodAcres.Text = "0";
			this.txtFarmMixedWoodAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel44
			// 
			this.fcLabel44.Location = new System.Drawing.Point(8, 285);
			this.fcLabel44.Name = "fcLabel44";
			this.fcLabel44.Size = new System.Drawing.Size(526, 40);
			this.fcLabel44.TabIndex = 88;
			this.fcLabel44.Text = "28A(2) - NUMBER OF FARM MIXED WOOD ACRES";
			this.fcLabel44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmSoftwoodAcres
			// 
			this.txtFarmSoftwoodAcres.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmSoftwoodAcres.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmSoftwoodAcres.Focusable = false;
			this.txtFarmSoftwoodAcres.Location = new System.Drawing.Point(570, 234);
			this.txtFarmSoftwoodAcres.LockedOriginal = true;
			this.txtFarmSoftwoodAcres.MaxLength = 0;
			this.txtFarmSoftwoodAcres.Name = "txtFarmSoftwoodAcres";
			this.txtFarmSoftwoodAcres.ReadOnly = true;
			this.txtFarmSoftwoodAcres.Size = new System.Drawing.Size(130, 40);
			this.txtFarmSoftwoodAcres.TabIndex = 5;
			this.txtFarmSoftwoodAcres.Text = "0";
			this.txtFarmSoftwoodAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmTotalValuationTillable
			// 
			this.txtFarmTotalValuationTillable.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmTotalValuationTillable.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmTotalValuationTillable.Focusable = false;
			this.txtFarmTotalValuationTillable.Location = new System.Drawing.Point(570, 182);
			this.txtFarmTotalValuationTillable.LockedOriginal = true;
			this.txtFarmTotalValuationTillable.MaxLength = 0;
			this.txtFarmTotalValuationTillable.Name = "txtFarmTotalValuationTillable";
			this.txtFarmTotalValuationTillable.ReadOnly = true;
			this.txtFarmTotalValuationTillable.Size = new System.Drawing.Size(130, 40);
			this.txtFarmTotalValuationTillable.TabIndex = 4;
			this.txtFarmTotalValuationTillable.Text = "0";
			this.txtFarmTotalValuationTillable.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmTotalAcresTillable
			// 
			this.txtFarmTotalAcresTillable.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmTotalAcresTillable.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmTotalAcresTillable.Focusable = false;
			this.txtFarmTotalAcresTillable.Location = new System.Drawing.Point(570, 132);
			this.txtFarmTotalAcresTillable.LockedOriginal = true;
			this.txtFarmTotalAcresTillable.MaxLength = 0;
			this.txtFarmTotalAcresTillable.Name = "txtFarmTotalAcresTillable";
			this.txtFarmTotalAcresTillable.ReadOnly = true;
			this.txtFarmTotalAcresTillable.Size = new System.Drawing.Size(130, 40);
			this.txtFarmTotalAcresTillable.TabIndex = 3;
			this.txtFarmTotalAcresTillable.Text = "0";
			this.txtFarmTotalAcresTillable.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel47
			// 
			this.fcLabel47.Location = new System.Drawing.Point(8, 132);
			this.fcLabel47.Name = "fcLabel47";
			this.fcLabel47.Size = new System.Drawing.Size(543, 40);
			this.fcLabel47.TabIndex = 82;
			this.fcLabel47.Text = "27A - TOTAL NUMBER OF ACRES OF ALL LAND NOW CLASSIFIED AS FARMLAND (NOT INCLUDING" +
    " FARM WOODLAND)";
			this.fcLabel47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFarmNumberOfAcresFirstClassified
			// 
			this.txtFarmNumberOfAcresFirstClassified.BackColor = System.Drawing.SystemColors.Window;
			this.txtFarmNumberOfAcresFirstClassified.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmNumberOfAcresFirstClassified.Location = new System.Drawing.Point(570, 81);
			this.txtFarmNumberOfAcresFirstClassified.MaxLength = 0;
			this.txtFarmNumberOfAcresFirstClassified.Name = "txtFarmNumberOfAcresFirstClassified";
			this.txtFarmNumberOfAcresFirstClassified.Size = new System.Drawing.Size(130, 40);
			this.txtFarmNumberOfAcresFirstClassified.TabIndex = 2;
			this.txtFarmNumberOfAcresFirstClassified.Text = "0";
			this.txtFarmNumberOfAcresFirstClassified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFarmNumberOfParcelsFirstClassified
			// 
			this.txtFarmNumberOfParcelsFirstClassified.BackColor = System.Drawing.Color.FromName("@control");
			this.txtFarmNumberOfParcelsFirstClassified.Cursor = Wisej.Web.Cursors.Default;
			this.txtFarmNumberOfParcelsFirstClassified.Focusable = false;
			this.txtFarmNumberOfParcelsFirstClassified.Location = new System.Drawing.Point(570, 31);
			this.txtFarmNumberOfParcelsFirstClassified.LockedOriginal = true;
			this.txtFarmNumberOfParcelsFirstClassified.MaxLength = 0;
			this.txtFarmNumberOfParcelsFirstClassified.Name = "txtFarmNumberOfParcelsFirstClassified";
			this.txtFarmNumberOfParcelsFirstClassified.ReadOnly = true;
			this.txtFarmNumberOfParcelsFirstClassified.Size = new System.Drawing.Size(130, 40);
			this.txtFarmNumberOfParcelsFirstClassified.TabIndex = 1;
			this.txtFarmNumberOfParcelsFirstClassified.Text = "0";
			this.txtFarmNumberOfParcelsFirstClassified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel49
			// 
			this.fcLabel49.Location = new System.Drawing.Point(8, 31);
			this.fcLabel49.Name = "fcLabel49";
			this.fcLabel49.Size = new System.Drawing.Size(505, 40);
			this.fcLabel49.TabIndex = 78;
			this.fcLabel49.Text = "25 - NUMBER OF PARCELS CLASSIFIED AS FARMLAND AS OF APRIL 1";
			this.fcLabel49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame10
			// 
			this.fcFrame10.Controls.Add(this.chkForestTransferredToFarmland);
			this.fcFrame10.Controls.Add(this.fcLabel37);
			this.fcFrame10.Controls.Add(this.txtForestNumberOfNonCompliancePenalties);
			this.fcFrame10.Controls.Add(this.fcLabel42);
			this.fcFrame10.Controls.Add(this.txtForestAmountOfPenalties);
			this.fcFrame10.Controls.Add(this.fcLabel43);
			this.fcFrame10.Controls.Add(this.txtForestAcresWithdrawn);
			this.fcFrame10.Controls.Add(this.lblForestAcresWithdrawn);
			this.fcFrame10.Controls.Add(this.txtForestParcelsWithdrawn);
			this.fcFrame10.Controls.Add(this.lblTotalForestParcelsWithdrawn);
			this.fcFrame10.Controls.Add(this.txtForestAcresFirstClassifiedThisYear);
			this.fcFrame10.Controls.Add(this.lblNumberForestAcres);
			this.fcFrame10.Location = new System.Drawing.Point(14, 18);
			this.fcFrame10.Name = "fcFrame10";
			this.fcFrame10.Size = new System.Drawing.Size(723, 341);
			this.fcFrame10.TabIndex = 1;
			this.fcFrame10.Text = "Tree Growth Tax Law";
			// 
			// chkForestTransferredToFarmland
			// 
			this.chkForestTransferredToFarmland.Location = new System.Drawing.Point(571, 294);
			this.chkForestTransferredToFarmland.Name = "chkForestTransferredToFarmland";
			this.chkForestTransferredToFarmland.Size = new System.Drawing.Size(32, 22);
			this.chkForestTransferredToFarmland.TabIndex = 6;
			// 
			// fcLabel37
			// 
			this.fcLabel37.Location = new System.Drawing.Point(9, 283);
			this.fcLabel37.Name = "fcLabel37";
			this.fcLabel37.Size = new System.Drawing.Size(526, 40);
			this.fcLabel37.TabIndex = 76;
			this.fcLabel37.Text = "24-1 - SINCE APRIL 1 HAVE ANY TREE GROWTH ACRES BEEN TRANSFERRED TO FARMLAND?";
			this.fcLabel37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtForestNumberOfNonCompliancePenalties
			// 
			this.txtForestNumberOfNonCompliancePenalties.BackColor = System.Drawing.SystemColors.Window;
			this.txtForestNumberOfNonCompliancePenalties.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestNumberOfNonCompliancePenalties.Location = new System.Drawing.Point(571, 232);
			this.txtForestNumberOfNonCompliancePenalties.MaxLength = 0;
			this.txtForestNumberOfNonCompliancePenalties.Name = "txtForestNumberOfNonCompliancePenalties";
			this.txtForestNumberOfNonCompliancePenalties.Size = new System.Drawing.Size(130, 40);
			this.txtForestNumberOfNonCompliancePenalties.TabIndex = 5;
			this.txtForestNumberOfNonCompliancePenalties.Text = "0";
			this.txtForestNumberOfNonCompliancePenalties.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestAmountOfPenalties
			// 
			this.txtForestAmountOfPenalties.BackColor = System.Drawing.SystemColors.Window;
			this.txtForestAmountOfPenalties.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestAmountOfPenalties.Location = new System.Drawing.Point(571, 180);
			this.txtForestAmountOfPenalties.MaxLength = 0;
			this.txtForestAmountOfPenalties.Name = "txtForestAmountOfPenalties";
			this.txtForestAmountOfPenalties.Size = new System.Drawing.Size(130, 40);
			this.txtForestAmountOfPenalties.TabIndex = 4;
			this.txtForestAmountOfPenalties.Text = "0";
			this.txtForestAmountOfPenalties.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestAcresWithdrawn
			// 
			this.txtForestAcresWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtForestAcresWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestAcresWithdrawn.Location = new System.Drawing.Point(571, 130);
			this.txtForestAcresWithdrawn.MaxLength = 0;
			this.txtForestAcresWithdrawn.Name = "txtForestAcresWithdrawn";
			this.txtForestAcresWithdrawn.Size = new System.Drawing.Size(130, 40);
			this.txtForestAcresWithdrawn.TabIndex = 3;
			this.txtForestAcresWithdrawn.Text = "0";
			this.txtForestAcresWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblForestAcresWithdrawn
			// 
			this.lblForestAcresWithdrawn.AutoSize = true;
			this.lblForestAcresWithdrawn.Location = new System.Drawing.Point(9, 144);
			this.lblForestAcresWithdrawn.Name = "lblForestAcresWithdrawn";
			this.lblForestAcresWithdrawn.Size = new System.Drawing.Size(338, 15);
			this.lblForestAcresWithdrawn.TabIndex = 70;
			this.lblForestAcresWithdrawn.Text = "24B - TOTAL NUMBER OF ACRES WITHDRAWN FROM";
			this.lblForestAcresWithdrawn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtForestParcelsWithdrawn
			// 
			this.txtForestParcelsWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtForestParcelsWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestParcelsWithdrawn.Location = new System.Drawing.Point(571, 79);
			this.txtForestParcelsWithdrawn.MaxLength = 0;
			this.txtForestParcelsWithdrawn.Name = "txtForestParcelsWithdrawn";
			this.txtForestParcelsWithdrawn.Size = new System.Drawing.Size(130, 40);
			this.txtForestParcelsWithdrawn.TabIndex = 2;
			this.txtForestParcelsWithdrawn.Text = "0";
			this.txtForestParcelsWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtForestAcresFirstClassifiedThisYear
			// 
			this.txtForestAcresFirstClassifiedThisYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtForestAcresFirstClassifiedThisYear.Cursor = Wisej.Web.Cursors.Default;
			this.txtForestAcresFirstClassifiedThisYear.Location = new System.Drawing.Point(571, 29);
			this.txtForestAcresFirstClassifiedThisYear.MaxLength = 0;
			this.txtForestAcresFirstClassifiedThisYear.Name = "txtForestAcresFirstClassifiedThisYear";
			this.txtForestAcresFirstClassifiedThisYear.Size = new System.Drawing.Size(130, 40);
			this.txtForestAcresFirstClassifiedThisYear.TabIndex = 1;
			this.txtForestAcresFirstClassifiedThisYear.Text = "0";
			this.txtForestAcresFirstClassifiedThisYear.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblNumberForestAcres
			// 
			this.lblNumberForestAcres.Location = new System.Drawing.Point(9, 29);
			this.lblNumberForestAcres.Name = "lblNumberForestAcres";
			this.lblNumberForestAcres.Size = new System.Drawing.Size(505, 40);
			this.lblNumberForestAcres.TabIndex = 66;
			this.lblNumberForestAcres.Text = "23 - NUMBER OF FORESTLAND ACRES FIRST CLASSIFIED FOR TAX YEAR";
			this.lblNumberForestAcres.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcTabPage2
			// 
			this.fcTabPage2.AutoScroll = true;
			this.fcTabPage2.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage2.Controls.Add(this.fcFrame14);
			this.fcTabPage2.Controls.Add(this.fcFrame13);
			this.fcTabPage2.Controls.Add(this.fcFrame12);
			this.fcTabPage2.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage2.Name = "fcTabPage2";
			this.fcTabPage2.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage2.Text = "Page 4";
			// 
			// fcFrame14
			// 
			this.fcFrame14.Controls.Add(this.txtExemptSewage);
			this.fcFrame14.Controls.Add(this.fcLabel74);
			this.fcFrame14.Controls.Add(this.txtExemptPrivateAirport);
			this.fcFrame14.Controls.Add(this.fcLabel75);
			this.fcFrame14.Controls.Add(this.txtExemptMunicipalAirport);
			this.fcFrame14.Controls.Add(this.fcLabel76);
			this.fcFrame14.Controls.Add(this.txtExemptMunicipalUtility);
			this.fcFrame14.Controls.Add(this.fcLabel77);
			this.fcFrame14.Controls.Add(this.txtExemptMunicipal);
			this.fcFrame14.Controls.Add(this.fcLabel78);
			this.fcFrame14.Controls.Add(this.txtExemptNHWater);
			this.fcFrame14.Controls.Add(this.fcLabel79);
			this.fcFrame14.Controls.Add(this.txtExemptMaine);
			this.fcFrame14.Controls.Add(this.fcLabel80);
			this.fcFrame14.Controls.Add(this.txtExemptFederal);
			this.fcFrame14.Controls.Add(this.fcLabel81);
			this.fcFrame14.Location = new System.Drawing.Point(14, 660);
			this.fcFrame14.Name = "fcFrame14";
			this.fcFrame14.Size = new System.Drawing.Size(724, 482);
			this.fcFrame14.TabIndex = 3;
			this.fcFrame14.Text = "Exempt Property";
			// 
			// txtExemptSewage
			// 
			this.txtExemptSewage.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptSewage.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptSewage.Focusable = false;
			this.txtExemptSewage.Location = new System.Drawing.Point(609, 409);
			this.txtExemptSewage.LockedOriginal = true;
			this.txtExemptSewage.MaxLength = 0;
			this.txtExemptSewage.Name = "txtExemptSewage";
			this.txtExemptSewage.ReadOnly = true;
			this.txtExemptSewage.Size = new System.Drawing.Size(100, 40);
			this.txtExemptSewage.TabIndex = 15;
			this.txtExemptSewage.Text = "0";
			this.txtExemptSewage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptPrivateAirport
			// 
			this.txtExemptPrivateAirport.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptPrivateAirport.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptPrivateAirport.Focusable = false;
			this.txtExemptPrivateAirport.Location = new System.Drawing.Point(609, 359);
			this.txtExemptPrivateAirport.LockedOriginal = true;
			this.txtExemptPrivateAirport.MaxLength = 0;
			this.txtExemptPrivateAirport.Name = "txtExemptPrivateAirport";
			this.txtExemptPrivateAirport.ReadOnly = true;
			this.txtExemptPrivateAirport.Size = new System.Drawing.Size(100, 40);
			this.txtExemptPrivateAirport.TabIndex = 14;
			this.txtExemptPrivateAirport.Text = "0";
			this.txtExemptPrivateAirport.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel75
			// 
			this.fcLabel75.Location = new System.Drawing.Point(9, 359);
			this.fcLabel75.Name = "fcLabel75";
			this.fcLabel75.Size = new System.Drawing.Size(505, 40);
			this.fcLabel75.TabIndex = 108;
			this.fcLabel75.Text = "40F - LANDING AREA OF A PRIVATELY OWNED AIRPORT WHEN OWNER GRANTS FREE USE OF THA" +
    "T LANDING AREA TO THE PUBLIC";
			this.fcLabel75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptMunicipalAirport
			// 
			this.txtExemptMunicipalAirport.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptMunicipalAirport.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptMunicipalAirport.Focusable = false;
			this.txtExemptMunicipalAirport.Location = new System.Drawing.Point(609, 307);
			this.txtExemptMunicipalAirport.LockedOriginal = true;
			this.txtExemptMunicipalAirport.MaxLength = 0;
			this.txtExemptMunicipalAirport.Name = "txtExemptMunicipalAirport";
			this.txtExemptMunicipalAirport.ReadOnly = true;
			this.txtExemptMunicipalAirport.Size = new System.Drawing.Size(100, 40);
			this.txtExemptMunicipalAirport.TabIndex = 13;
			this.txtExemptMunicipalAirport.Text = "0";
			this.txtExemptMunicipalAirport.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel76
			// 
			this.fcLabel76.Location = new System.Drawing.Point(9, 307);
			this.fcLabel76.Name = "fcLabel76";
			this.fcLabel76.Size = new System.Drawing.Size(526, 40);
			this.fcLabel76.TabIndex = 106;
			this.fcLabel76.Text = "40E - AIRPORT OR LANDING FIELD OF A PUBLIC MUNICIPAL CORPORATION USED FOR AIRPORT" +
    " OR AERONAUTICAL PURPOSES";
			this.fcLabel76.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptMunicipalUtility
			// 
			this.txtExemptMunicipalUtility.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptMunicipalUtility.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptMunicipalUtility.Focusable = false;
			this.txtExemptMunicipalUtility.Location = new System.Drawing.Point(609, 232);
			this.txtExemptMunicipalUtility.LockedOriginal = true;
			this.txtExemptMunicipalUtility.MaxLength = 0;
			this.txtExemptMunicipalUtility.Name = "txtExemptMunicipalUtility";
			this.txtExemptMunicipalUtility.ReadOnly = true;
			this.txtExemptMunicipalUtility.Size = new System.Drawing.Size(100, 40);
			this.txtExemptMunicipalUtility.TabIndex = 12;
			this.txtExemptMunicipalUtility.Text = "0";
			this.txtExemptMunicipalUtility.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptMunicipal
			// 
			this.txtExemptMunicipal.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptMunicipal.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptMunicipal.Focusable = false;
			this.txtExemptMunicipal.Location = new System.Drawing.Point(609, 180);
			this.txtExemptMunicipal.LockedOriginal = true;
			this.txtExemptMunicipal.MaxLength = 0;
			this.txtExemptMunicipal.Name = "txtExemptMunicipal";
			this.txtExemptMunicipal.ReadOnly = true;
			this.txtExemptMunicipal.Size = new System.Drawing.Size(100, 40);
			this.txtExemptMunicipal.TabIndex = 11;
			this.txtExemptMunicipal.Text = "0";
			this.txtExemptMunicipal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptNHWater
			// 
			this.txtExemptNHWater.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptNHWater.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptNHWater.Focusable = false;
			this.txtExemptNHWater.Location = new System.Drawing.Point(609, 130);
			this.txtExemptNHWater.LockedOriginal = true;
			this.txtExemptNHWater.MaxLength = 0;
			this.txtExemptNHWater.Name = "txtExemptNHWater";
			this.txtExemptNHWater.ReadOnly = true;
			this.txtExemptNHWater.Size = new System.Drawing.Size(100, 40);
			this.txtExemptNHWater.TabIndex = 10;
			this.txtExemptNHWater.Text = "0";
			this.txtExemptNHWater.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel79
			// 
			this.fcLabel79.Location = new System.Drawing.Point(9, 130);
			this.fcLabel79.Name = "fcLabel79";
			this.fcLabel79.Size = new System.Drawing.Size(543, 40);
			this.fcLabel79.TabIndex = 100;
			this.fcLabel79.Text = "40B - REAL ESTATE OWNED BY THE WATER RESOURCES BOARD OF THE STATE OF NEW HAMPSHIR" +
    "E LOCATED WITHIN THIS STATE";
			this.fcLabel79.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptMaine
			// 
			this.txtExemptMaine.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptMaine.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptMaine.Focusable = false;
			this.txtExemptMaine.Location = new System.Drawing.Point(609, 79);
			this.txtExemptMaine.LockedOriginal = true;
			this.txtExemptMaine.MaxLength = 0;
			this.txtExemptMaine.Name = "txtExemptMaine";
			this.txtExemptMaine.ReadOnly = true;
			this.txtExemptMaine.Size = new System.Drawing.Size(100, 40);
			this.txtExemptMaine.TabIndex = 9;
			this.txtExemptMaine.Text = "0";
			this.txtExemptMaine.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptFederal
			// 
			this.txtExemptFederal.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptFederal.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptFederal.Focusable = false;
			this.txtExemptFederal.Location = new System.Drawing.Point(609, 29);
			this.txtExemptFederal.LockedOriginal = true;
			this.txtExemptFederal.MaxLength = 0;
			this.txtExemptFederal.Name = "txtExemptFederal";
			this.txtExemptFederal.ReadOnly = true;
			this.txtExemptFederal.Size = new System.Drawing.Size(100, 40);
			this.txtExemptFederal.TabIndex = 8;
			this.txtExemptFederal.Text = "0";
			this.txtExemptFederal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel81
			// 
			this.fcLabel81.Location = new System.Drawing.Point(9, 29);
			this.fcLabel81.Name = "fcLabel81";
			this.fcLabel81.Size = new System.Drawing.Size(505, 40);
			this.fcLabel81.TabIndex = 96;
			this.fcLabel81.Text = "40A(1) - UNITED STATES";
			this.fcLabel81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame13
			// 
			this.fcFrame13.Controls.Add(this.txtWaterfrontPenalties);
			this.fcFrame13.Controls.Add(this.fcLabel66);
			this.fcFrame13.Controls.Add(this.txtWaterfrontAcresWithdrawn);
			this.fcFrame13.Controls.Add(this.fcLabel67);
			this.fcFrame13.Controls.Add(this.txtWaterfrontParcelsWithdrawn);
			this.fcFrame13.Controls.Add(this.fcLabel68);
			this.fcFrame13.Controls.Add(this.txtWaterfrontTotalValuation);
			this.fcFrame13.Controls.Add(this.fcLabel69);
			this.fcFrame13.Controls.Add(this.txtWaterfrontTotalAcreage);
			this.fcFrame13.Controls.Add(this.fcLabel70);
			this.fcFrame13.Controls.Add(this.txtWaterfrontAcresFirstClassified);
			this.fcFrame13.Controls.Add(this.fcLabel71);
			this.fcFrame13.Controls.Add(this.txtWaterfrontParcelsFirstClassified);
			this.fcFrame13.Controls.Add(this.fcLabel72);
			this.fcFrame13.Location = new System.Drawing.Point(14, 238);
			this.fcFrame13.Name = "fcFrame13";
			this.fcFrame13.Size = new System.Drawing.Size(724, 404);
			this.fcFrame13.TabIndex = 2;
			this.fcFrame13.Text = "Land Classified Under The Working Waterfront Tax Law";
			// 
			// txtWaterfrontPenalties
			// 
			this.txtWaterfrontPenalties.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontPenalties.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontPenalties.Location = new System.Drawing.Point(609, 338);
			this.txtWaterfrontPenalties.MaxLength = 0;
			this.txtWaterfrontPenalties.Name = "txtWaterfrontPenalties";
			this.txtWaterfrontPenalties.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontPenalties.TabIndex = 7;
			this.txtWaterfrontPenalties.Text = "0";
			this.txtWaterfrontPenalties.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel66
			// 
			this.fcLabel66.Location = new System.Drawing.Point(9, 338);
			this.fcLabel66.Name = "fcLabel66";
			this.fcLabel66.Size = new System.Drawing.Size(505, 40);
			this.fcLabel66.TabIndex = 104;
			this.fcLabel66.Text = "39C - TOTAL AMOUNT OF PENALTIES ASSESSED BY THE MUNICIPALITY DUE TO THE WITHDRAWA" +
    "L OF CLASSIFIED WORKING WATERFRONT LAND";
			this.fcLabel66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtWaterfrontAcresWithdrawn
			// 
			this.txtWaterfrontAcresWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontAcresWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontAcresWithdrawn.Location = new System.Drawing.Point(609, 286);
			this.txtWaterfrontAcresWithdrawn.MaxLength = 0;
			this.txtWaterfrontAcresWithdrawn.Name = "txtWaterfrontAcresWithdrawn";
			this.txtWaterfrontAcresWithdrawn.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontAcresWithdrawn.TabIndex = 6;
			this.txtWaterfrontAcresWithdrawn.Text = "0";
			this.txtWaterfrontAcresWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel67
			// 
			this.fcLabel67.Location = new System.Drawing.Point(9, 286);
			this.fcLabel67.Name = "fcLabel67";
			this.fcLabel67.Size = new System.Drawing.Size(526, 40);
			this.fcLabel67.TabIndex = 102;
			this.fcLabel67.Text = "39B - TOTAL NUMBER OF WORKING WATERFRONT ACRES WITHDRAWN";
			this.fcLabel67.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtWaterfrontParcelsWithdrawn
			// 
			this.txtWaterfrontParcelsWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontParcelsWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontParcelsWithdrawn.Location = new System.Drawing.Point(609, 235);
			this.txtWaterfrontParcelsWithdrawn.MaxLength = 0;
			this.txtWaterfrontParcelsWithdrawn.Name = "txtWaterfrontParcelsWithdrawn";
			this.txtWaterfrontParcelsWithdrawn.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontParcelsWithdrawn.TabIndex = 5;
			this.txtWaterfrontParcelsWithdrawn.Text = "0";
			this.txtWaterfrontParcelsWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtWaterfrontTotalValuation
			// 
			this.txtWaterfrontTotalValuation.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontTotalValuation.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontTotalValuation.Location = new System.Drawing.Point(609, 183);
			this.txtWaterfrontTotalValuation.MaxLength = 0;
			this.txtWaterfrontTotalValuation.Name = "txtWaterfrontTotalValuation";
			this.txtWaterfrontTotalValuation.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontTotalValuation.TabIndex = 4;
			this.txtWaterfrontTotalValuation.Text = "0";
			this.txtWaterfrontTotalValuation.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtWaterfrontTotalAcreage
			// 
			this.txtWaterfrontTotalAcreage.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontTotalAcreage.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontTotalAcreage.Location = new System.Drawing.Point(609, 133);
			this.txtWaterfrontTotalAcreage.MaxLength = 0;
			this.txtWaterfrontTotalAcreage.Name = "txtWaterfrontTotalAcreage";
			this.txtWaterfrontTotalAcreage.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontTotalAcreage.TabIndex = 3;
			this.txtWaterfrontTotalAcreage.Text = "0";
			this.txtWaterfrontTotalAcreage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel70
			// 
			this.fcLabel70.Location = new System.Drawing.Point(9, 133);
			this.fcLabel70.Name = "fcLabel70";
			this.fcLabel70.Size = new System.Drawing.Size(543, 40);
			this.fcLabel70.TabIndex = 96;
			this.fcLabel70.Text = "37 - TOTAL ACREAGE OF ALL LAND NOW CLASSIFIED AS WORKING WATERFRONT";
			this.fcLabel70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtWaterfrontAcresFirstClassified
			// 
			this.txtWaterfrontAcresFirstClassified.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontAcresFirstClassified.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontAcresFirstClassified.Location = new System.Drawing.Point(609, 82);
			this.txtWaterfrontAcresFirstClassified.MaxLength = 0;
			this.txtWaterfrontAcresFirstClassified.Name = "txtWaterfrontAcresFirstClassified";
			this.txtWaterfrontAcresFirstClassified.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontAcresFirstClassified.TabIndex = 2;
			this.txtWaterfrontAcresFirstClassified.Text = "0";
			this.txtWaterfrontAcresFirstClassified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtWaterfrontParcelsFirstClassified
			// 
			this.txtWaterfrontParcelsFirstClassified.BackColor = System.Drawing.SystemColors.Window;
			this.txtWaterfrontParcelsFirstClassified.Cursor = Wisej.Web.Cursors.Default;
			this.txtWaterfrontParcelsFirstClassified.Location = new System.Drawing.Point(609, 32);
			this.txtWaterfrontParcelsFirstClassified.MaxLength = 0;
			this.txtWaterfrontParcelsFirstClassified.Name = "txtWaterfrontParcelsFirstClassified";
			this.txtWaterfrontParcelsFirstClassified.Size = new System.Drawing.Size(100, 40);
			this.txtWaterfrontParcelsFirstClassified.TabIndex = 1;
			this.txtWaterfrontParcelsFirstClassified.Text = "0";
			this.txtWaterfrontParcelsFirstClassified.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel72
			// 
			this.fcLabel72.Location = new System.Drawing.Point(9, 32);
			this.fcLabel72.Name = "fcLabel72";
			this.fcLabel72.Size = new System.Drawing.Size(526, 40);
			this.fcLabel72.TabIndex = 92;
			this.fcLabel72.Text = "35 - NUMBER OF PARCELS CLASSIFIED AS WORKING WATERFRONT AS OF APRIL 1";
			this.fcLabel72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame12
			// 
			this.fcFrame12.Controls.Add(this.txtOpenTotalAmountOfPenalties);
			this.fcFrame12.Controls.Add(this.fcLabel63);
			this.fcFrame12.Controls.Add(this.txtOpenAcresWithdrawn);
			this.fcFrame12.Controls.Add(this.fcLabel64);
			this.fcFrame12.Controls.Add(this.txtOpenParcelsWithdrawn);
			this.fcFrame12.Controls.Add(this.fcLabel65);
			this.fcFrame12.Location = new System.Drawing.Point(14, 18);
			this.fcFrame12.Name = "fcFrame12";
			this.fcFrame12.Size = new System.Drawing.Size(724, 200);
			this.fcFrame12.TabIndex = 1;
			this.fcFrame12.Text = "Land Classified Under The Open Space Tax Law Continued";
			// 
			// txtOpenTotalAmountOfPenalties
			// 
			this.txtOpenTotalAmountOfPenalties.BackColor = System.Drawing.SystemColors.Window;
			this.txtOpenTotalAmountOfPenalties.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenTotalAmountOfPenalties.Location = new System.Drawing.Point(609, 134);
			this.txtOpenTotalAmountOfPenalties.MaxLength = 0;
			this.txtOpenTotalAmountOfPenalties.Name = "txtOpenTotalAmountOfPenalties";
			this.txtOpenTotalAmountOfPenalties.Size = new System.Drawing.Size(100, 40);
			this.txtOpenTotalAmountOfPenalties.TabIndex = 3;
			this.txtOpenTotalAmountOfPenalties.Text = "0";
			this.txtOpenTotalAmountOfPenalties.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel63
			// 
			this.fcLabel63.Location = new System.Drawing.Point(9, 134);
			this.fcLabel63.Name = "fcLabel63";
			this.fcLabel63.Size = new System.Drawing.Size(540, 40);
			this.fcLabel63.TabIndex = 76;
			this.fcLabel63.Text = "34C - TOTAL AMOUNT OF PENALTIES ASSESSED BY MUNICIPALITY DUE TO THE WITHDRAWAL OF" +
    " CLASSIFIED OPEN SPACE LAND";
			this.fcLabel63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtOpenAcresWithdrawn
			// 
			this.txtOpenAcresWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtOpenAcresWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenAcresWithdrawn.Location = new System.Drawing.Point(609, 83);
			this.txtOpenAcresWithdrawn.MaxLength = 0;
			this.txtOpenAcresWithdrawn.Name = "txtOpenAcresWithdrawn";
			this.txtOpenAcresWithdrawn.Size = new System.Drawing.Size(100, 40);
			this.txtOpenAcresWithdrawn.TabIndex = 2;
			this.txtOpenAcresWithdrawn.Text = "0";
			this.txtOpenAcresWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtOpenParcelsWithdrawn
			// 
			this.txtOpenParcelsWithdrawn.BackColor = System.Drawing.SystemColors.Window;
			this.txtOpenParcelsWithdrawn.Cursor = Wisej.Web.Cursors.Default;
			this.txtOpenParcelsWithdrawn.Location = new System.Drawing.Point(609, 33);
			this.txtOpenParcelsWithdrawn.MaxLength = 0;
			this.txtOpenParcelsWithdrawn.Name = "txtOpenParcelsWithdrawn";
			this.txtOpenParcelsWithdrawn.Size = new System.Drawing.Size(100, 40);
			this.txtOpenParcelsWithdrawn.TabIndex = 1;
			this.txtOpenParcelsWithdrawn.Text = "0";
			this.txtOpenParcelsWithdrawn.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel65
			// 
			this.fcLabel65.Location = new System.Drawing.Point(9, 33);
			this.fcLabel65.Name = "fcLabel65";
			this.fcLabel65.Size = new System.Drawing.Size(505, 40);
			this.fcLabel65.TabIndex = 72;
			this.fcLabel65.Text = "34A - TOTAL NUMBER OF OPEN SPACE PARCELS WITHDRAWN";
			this.fcLabel65.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcTabPage3
			// 
			this.fcTabPage3.AutoScroll = true;
			this.fcTabPage3.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage3.Controls.Add(this.fcFrame15);
			this.fcTabPage3.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage3.Name = "fcTabPage3";
			this.fcTabPage3.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage3.Text = "Page 5";
			// 
			// fcFrame15
			// 
			this.fcFrame15.Controls.Add(this.txtExemptSnowmobileGroomingEquipment);
			this.fcFrame15.Controls.Add(this.fcLabel91);
			this.fcFrame15.Controls.Add(this.txtExemptPollutionControl);
			this.fcFrame15.Controls.Add(this.fcLabel92);
			this.fcFrame15.Controls.Add(this.txtExemptAnimalWasteStorage);
			this.fcFrame15.Controls.Add(this.fcLabel93);
			this.fcFrame15.Controls.Add(this.txtExemptWaterSupplyTransport);
			this.fcFrame15.Controls.Add(this.fcLabel94);
			this.fcFrame15.Controls.Add(this.txtExemptLegallyBlind);
			this.fcFrame15.Controls.Add(this.fcLabel95);
			this.fcFrame15.Controls.Add(this.txtExemptHospitalPersonalProperty);
			this.fcFrame15.Controls.Add(this.fcLabel96);
			this.fcFrame15.Controls.Add(this.txtExemptFraternal);
			this.fcFrame15.Controls.Add(this.fcLabel87);
			this.fcFrame15.Controls.Add(this.txtExemptReligiousWorship);
			this.fcFrame15.Controls.Add(this.fcLabel88);
			this.fcFrame15.Controls.Add(this.txtExemptTaxableValueOfParsonages);
			this.fcFrame15.Controls.Add(this.fcLabel89);
			this.fcFrame15.Controls.Add(this.txtExemptValueOfParsonages);
			this.fcFrame15.Controls.Add(this.fcLabel90);
			this.fcFrame15.Controls.Add(this.txtExemptParsonages);
			this.fcFrame15.Controls.Add(this.fcLabel73);
			this.fcFrame15.Controls.Add(this.txtExemptChamberOfCommerce);
			this.fcFrame15.Controls.Add(this.fcLabel82);
			this.fcFrame15.Controls.Add(this.txtExemptVetOrgReimbursable);
			this.fcFrame15.Controls.Add(this.fcLabel83);
			this.fcFrame15.Controls.Add(this.txtExemptVeteransOrganizations);
			this.fcFrame15.Controls.Add(this.fcLabel84);
			this.fcFrame15.Controls.Add(this.txtExemptLiteraryAndScientific);
			this.fcFrame15.Controls.Add(this.fcLabel85);
			this.fcFrame15.Controls.Add(this.txtExemptBenevolent);
			this.fcFrame15.Controls.Add(this.fcLabel86);
			this.fcFrame15.Location = new System.Drawing.Point(14, 18);
			this.fcFrame15.Name = "fcFrame15";
			this.fcFrame15.Size = new System.Drawing.Size(730, 967);
			this.fcFrame15.TabIndex = 0;
			this.fcFrame15.Text = "Exempt Property Continued";
			// 
			// txtExemptSnowmobileGroomingEquipment
			// 
			this.txtExemptSnowmobileGroomingEquipment.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptSnowmobileGroomingEquipment.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptSnowmobileGroomingEquipment.Location = new System.Drawing.Point(571, 821);
			this.txtExemptSnowmobileGroomingEquipment.MaxLength = 0;
			this.txtExemptSnowmobileGroomingEquipment.Name = "txtExemptSnowmobileGroomingEquipment";
			this.txtExemptSnowmobileGroomingEquipment.Size = new System.Drawing.Size(100, 40);
			this.txtExemptSnowmobileGroomingEquipment.TabIndex = 109;
			this.txtExemptSnowmobileGroomingEquipment.Text = "0";
			this.txtExemptSnowmobileGroomingEquipment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel91
			// 
			this.fcLabel91.AutoSize = true;
			this.fcLabel91.Location = new System.Drawing.Point(9, 835);
			this.fcLabel91.Name = "fcLabel91";
			this.fcLabel91.Size = new System.Drawing.Size(326, 15);
			this.fcLabel91.TabIndex = 108;
			this.fcLabel91.Text = "40S - SNOWMOBILE TRAIL GROOMING EQUIPMENT";
			this.fcLabel91.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptPollutionControl
			// 
			this.txtExemptPollutionControl.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptPollutionControl.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptPollutionControl.Focusable = false;
			this.txtExemptPollutionControl.Location = new System.Drawing.Point(571, 769);
			this.txtExemptPollutionControl.LockedOriginal = true;
			this.txtExemptPollutionControl.MaxLength = 0;
			this.txtExemptPollutionControl.Name = "txtExemptPollutionControl";
			this.txtExemptPollutionControl.ReadOnly = true;
			this.txtExemptPollutionControl.Size = new System.Drawing.Size(100, 40);
			this.txtExemptPollutionControl.TabIndex = 107;
			this.txtExemptPollutionControl.Text = "0";
			this.txtExemptPollutionControl.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel92
			// 
			this.fcLabel92.Location = new System.Drawing.Point(9, 769);
			this.fcLabel92.Name = "fcLabel92";
			this.fcLabel92.Size = new System.Drawing.Size(546, 40);
			this.fcLabel92.TabIndex = 106;
			this.fcLabel92.Text = "40R - POLLUTION CONTROL FACILITIES THAT ARE CERTIFIED BY THE COMMISSIONER OF ENVI" +
    "RONMENTAL PROTECTION";
			this.fcLabel92.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptAnimalWasteStorage
			// 
			this.txtExemptAnimalWasteStorage.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptAnimalWasteStorage.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptAnimalWasteStorage.Focusable = false;
			this.txtExemptAnimalWasteStorage.Location = new System.Drawing.Point(571, 717);
			this.txtExemptAnimalWasteStorage.LockedOriginal = true;
			this.txtExemptAnimalWasteStorage.MaxLength = 0;
			this.txtExemptAnimalWasteStorage.Name = "txtExemptAnimalWasteStorage";
			this.txtExemptAnimalWasteStorage.ReadOnly = true;
			this.txtExemptAnimalWasteStorage.Size = new System.Drawing.Size(100, 40);
			this.txtExemptAnimalWasteStorage.TabIndex = 105;
			this.txtExemptAnimalWasteStorage.Text = "0";
			this.txtExemptAnimalWasteStorage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel93
			// 
			this.fcLabel93.Location = new System.Drawing.Point(9, 717);
			this.fcLabel93.Name = "fcLabel93";
			this.fcLabel93.Size = new System.Drawing.Size(556, 40);
			this.fcLabel93.TabIndex = 104;
			this.fcLabel93.Text = "40Q - ANIMAL WASTE STORAGE FACILITIES CONSTRUCTED SINCE APRIL 1, 1999 AND CERTIFI" +
    "ED AS EXEMPT";
			this.fcLabel93.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptWaterSupplyTransport
			// 
			this.txtExemptWaterSupplyTransport.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptWaterSupplyTransport.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptWaterSupplyTransport.Focusable = false;
			this.txtExemptWaterSupplyTransport.Location = new System.Drawing.Point(571, 664);
			this.txtExemptWaterSupplyTransport.LockedOriginal = true;
			this.txtExemptWaterSupplyTransport.MaxLength = 0;
			this.txtExemptWaterSupplyTransport.Name = "txtExemptWaterSupplyTransport";
			this.txtExemptWaterSupplyTransport.ReadOnly = true;
			this.txtExemptWaterSupplyTransport.Size = new System.Drawing.Size(100, 40);
			this.txtExemptWaterSupplyTransport.TabIndex = 103;
			this.txtExemptWaterSupplyTransport.Text = "0";
			this.txtExemptWaterSupplyTransport.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel94
			// 
			this.fcLabel94.Location = new System.Drawing.Point(9, 664);
			this.fcLabel94.Name = "fcLabel94";
			this.fcLabel94.Size = new System.Drawing.Size(526, 40);
			this.fcLabel94.TabIndex = 102;
			this.fcLabel94.Text = "40P - AQUEDUCTS, PIPES AND CONDUITS OF ANY CORPORATION SUPPLYING A MUNICIPALITY W" +
    "ITH WATER";
			this.fcLabel94.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptLegallyBlind
			// 
			this.txtExemptLegallyBlind.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptLegallyBlind.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptLegallyBlind.Focusable = false;
			this.txtExemptLegallyBlind.Location = new System.Drawing.Point(571, 613);
			this.txtExemptLegallyBlind.LockedOriginal = true;
			this.txtExemptLegallyBlind.MaxLength = 0;
			this.txtExemptLegallyBlind.Name = "txtExemptLegallyBlind";
			this.txtExemptLegallyBlind.ReadOnly = true;
			this.txtExemptLegallyBlind.Size = new System.Drawing.Size(100, 40);
			this.txtExemptLegallyBlind.TabIndex = 101;
			this.txtExemptLegallyBlind.Text = "0";
			this.txtExemptLegallyBlind.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel95
			// 
			this.fcLabel95.Anchor = Wisej.Web.AnchorStyles.Top;
			this.fcLabel95.Location = new System.Drawing.Point(9, 613);
			this.fcLabel95.Name = "fcLabel95";
			this.fcLabel95.Size = new System.Drawing.Size(546, 40);
			this.fcLabel95.TabIndex = 100;
			this.fcLabel95.Text = "40O - EXEMPT VALUE OF REAL PROPERTY OF ALL PERSONS DETERMINED TO BE LEGALLY BLIND" +
    "";
			this.fcLabel95.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptHospitalPersonalProperty
			// 
			this.txtExemptHospitalPersonalProperty.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptHospitalPersonalProperty.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptHospitalPersonalProperty.Focusable = false;
			this.txtExemptHospitalPersonalProperty.Location = new System.Drawing.Point(571, 561);
			this.txtExemptHospitalPersonalProperty.LockedOriginal = true;
			this.txtExemptHospitalPersonalProperty.MaxLength = 0;
			this.txtExemptHospitalPersonalProperty.Name = "txtExemptHospitalPersonalProperty";
			this.txtExemptHospitalPersonalProperty.ReadOnly = true;
			this.txtExemptHospitalPersonalProperty.Size = new System.Drawing.Size(100, 40);
			this.txtExemptHospitalPersonalProperty.TabIndex = 99;
			this.txtExemptHospitalPersonalProperty.Text = "0";
			this.txtExemptHospitalPersonalProperty.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel96
			// 
			this.fcLabel96.Location = new System.Drawing.Point(9, 561);
			this.fcLabel96.Name = "fcLabel96";
			this.fcLabel96.Size = new System.Drawing.Size(546, 40);
			this.fcLabel96.TabIndex = 98;
			this.fcLabel96.Text = "40N - PERSONAL PROPERTY LEASED BY A BENEVOLENT AND CHARITABLE ORGANIZATION AND TH" +
    "E PRIMARY PURPOSE IS THE OPERATION OF A HOSPITAL";
			this.fcLabel96.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptFraternal
			// 
			this.txtExemptFraternal.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptFraternal.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptFraternal.Focusable = false;
			this.txtExemptFraternal.Location = new System.Drawing.Point(571, 496);
			this.txtExemptFraternal.LockedOriginal = true;
			this.txtExemptFraternal.MaxLength = 0;
			this.txtExemptFraternal.Name = "txtExemptFraternal";
			this.txtExemptFraternal.ReadOnly = true;
			this.txtExemptFraternal.Size = new System.Drawing.Size(100, 40);
			this.txtExemptFraternal.TabIndex = 97;
			this.txtExemptFraternal.Text = "0";
			this.txtExemptFraternal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel87
			// 
			this.fcLabel87.Location = new System.Drawing.Point(9, 496);
			this.fcLabel87.Name = "fcLabel87";
			this.fcLabel87.Size = new System.Drawing.Size(526, 52);
			this.fcLabel87.TabIndex = 96;
			this.fcLabel87.Text = "40M - PROPERTY OWNED OR HELD IN TRUST FOR FRATERNAL ORGANIZATIONS OPERATING UNDER" +
    " THE LODGE SYSTEM (DOES NOT INCLUDE COLLEGE FRATERNITIES)";
			this.fcLabel87.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptReligiousWorship
			// 
			this.txtExemptReligiousWorship.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptReligiousWorship.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptReligiousWorship.Focusable = false;
			this.txtExemptReligiousWorship.Location = new System.Drawing.Point(571, 445);
			this.txtExemptReligiousWorship.LockedOriginal = true;
			this.txtExemptReligiousWorship.MaxLength = 0;
			this.txtExemptReligiousWorship.Name = "txtExemptReligiousWorship";
			this.txtExemptReligiousWorship.ReadOnly = true;
			this.txtExemptReligiousWorship.Size = new System.Drawing.Size(100, 40);
			this.txtExemptReligiousWorship.TabIndex = 95;
			this.txtExemptReligiousWorship.Text = "0";
			this.txtExemptReligiousWorship.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel88
			// 
			this.fcLabel88.AutoSize = true;
			this.fcLabel88.Location = new System.Drawing.Point(9, 459);
			this.fcLabel88.Name = "fcLabel88";
			this.fcLabel88.Size = new System.Drawing.Size(552, 15);
			this.fcLabel88.TabIndex = 94;
			this.fcLabel88.Text = "40L(4) - INDICATE THE TOTAL EXEMPT VALUE OF ALL HOUSES OF RELIGIOUS WORHIP";
			this.fcLabel88.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptTaxableValueOfParsonages
			// 
			this.txtExemptTaxableValueOfParsonages.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptTaxableValueOfParsonages.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptTaxableValueOfParsonages.Focusable = false;
			this.txtExemptTaxableValueOfParsonages.Location = new System.Drawing.Point(571, 393);
			this.txtExemptTaxableValueOfParsonages.LockedOriginal = true;
			this.txtExemptTaxableValueOfParsonages.MaxLength = 0;
			this.txtExemptTaxableValueOfParsonages.Name = "txtExemptTaxableValueOfParsonages";
			this.txtExemptTaxableValueOfParsonages.ReadOnly = true;
			this.txtExemptTaxableValueOfParsonages.Size = new System.Drawing.Size(100, 40);
			this.txtExemptTaxableValueOfParsonages.TabIndex = 93;
			this.txtExemptTaxableValueOfParsonages.Text = "0";
			this.txtExemptTaxableValueOfParsonages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel89
			// 
			this.fcLabel89.Location = new System.Drawing.Point(9, 393);
			this.fcLabel89.Name = "fcLabel89";
			this.fcLabel89.Size = new System.Drawing.Size(546, 40);
			this.fcLabel89.TabIndex = 92;
			this.fcLabel89.Text = "40L(3) - INDICATE THE TOTAL TAXABLE VALUE OF THOSE PARSONAGES";
			this.fcLabel89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptValueOfParsonages
			// 
			this.txtExemptValueOfParsonages.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptValueOfParsonages.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptValueOfParsonages.Focusable = false;
			this.txtExemptValueOfParsonages.Location = new System.Drawing.Point(571, 341);
			this.txtExemptValueOfParsonages.LockedOriginal = true;
			this.txtExemptValueOfParsonages.MaxLength = 0;
			this.txtExemptValueOfParsonages.Name = "txtExemptValueOfParsonages";
			this.txtExemptValueOfParsonages.ReadOnly = true;
			this.txtExemptValueOfParsonages.Size = new System.Drawing.Size(100, 40);
			this.txtExemptValueOfParsonages.TabIndex = 91;
			this.txtExemptValueOfParsonages.Text = "0";
			this.txtExemptValueOfParsonages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel90
			// 
			this.fcLabel90.Location = new System.Drawing.Point(9, 341);
			this.fcLabel90.Name = "fcLabel90";
			this.fcLabel90.Size = new System.Drawing.Size(556, 40);
			this.fcLabel90.TabIndex = 90;
			this.fcLabel90.Text = "40L(2) - INDICATE THE TOTAL EXEMPT VALUE OF THOSE PARSONAGES";
			this.fcLabel90.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptParsonages
			// 
			this.txtExemptParsonages.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptParsonages.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptParsonages.Focusable = false;
			this.txtExemptParsonages.Location = new System.Drawing.Point(571, 288);
			this.txtExemptParsonages.LockedOriginal = true;
			this.txtExemptParsonages.MaxLength = 0;
			this.txtExemptParsonages.Name = "txtExemptParsonages";
			this.txtExemptParsonages.ReadOnly = true;
			this.txtExemptParsonages.Size = new System.Drawing.Size(100, 40);
			this.txtExemptParsonages.TabIndex = 89;
			this.txtExemptParsonages.Text = "0";
			this.txtExemptParsonages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel73
			// 
			this.fcLabel73.Location = new System.Drawing.Point(9, 288);
			this.fcLabel73.Name = "fcLabel73";
			this.fcLabel73.Size = new System.Drawing.Size(526, 40);
			this.fcLabel73.TabIndex = 88;
			this.fcLabel73.Text = "40L(1) - NUMBER OF PARSONAGES WITHIN THIS MUNICIPALITY";
			this.fcLabel73.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptChamberOfCommerce
			// 
			this.txtExemptChamberOfCommerce.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptChamberOfCommerce.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptChamberOfCommerce.Focusable = false;
			this.txtExemptChamberOfCommerce.Location = new System.Drawing.Point(571, 237);
			this.txtExemptChamberOfCommerce.LockedOriginal = true;
			this.txtExemptChamberOfCommerce.MaxLength = 0;
			this.txtExemptChamberOfCommerce.Name = "txtExemptChamberOfCommerce";
			this.txtExemptChamberOfCommerce.ReadOnly = true;
			this.txtExemptChamberOfCommerce.Size = new System.Drawing.Size(100, 40);
			this.txtExemptChamberOfCommerce.TabIndex = 87;
			this.txtExemptChamberOfCommerce.Text = "0";
			this.txtExemptChamberOfCommerce.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel82
			// 
			this.fcLabel82.AutoSize = true;
			this.fcLabel82.Location = new System.Drawing.Point(9, 251);
			this.fcLabel82.Name = "fcLabel82";
			this.fcLabel82.Size = new System.Drawing.Size(462, 15);
			this.fcLabel82.TabIndex = 86;
			this.fcLabel82.Text = "40K - PROPERTY OF CHAMBERS OF COMMERCE OR BOARDS OF TRADE";
			this.fcLabel82.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetOrgReimbursable
			// 
			this.txtExemptVetOrgReimbursable.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptVetOrgReimbursable.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetOrgReimbursable.Location = new System.Drawing.Point(571, 185);
			this.txtExemptVetOrgReimbursable.MaxLength = 0;
			this.txtExemptVetOrgReimbursable.Name = "txtExemptVetOrgReimbursable";
			this.txtExemptVetOrgReimbursable.Size = new System.Drawing.Size(100, 40);
			this.txtExemptVetOrgReimbursable.TabIndex = 85;
			this.txtExemptVetOrgReimbursable.Text = "0";
			this.txtExemptVetOrgReimbursable.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel83
			// 
			this.fcLabel83.Location = new System.Drawing.Point(9, 185);
			this.fcLabel83.Name = "fcLabel83";
			this.fcLabel83.Size = new System.Drawing.Size(546, 40);
			this.fcLabel83.TabIndex = 84;
			this.fcLabel83.Text = "40J(2) - EXEMPT VALUE ATTRIBUTABLE TO PURPOSES OTHER THAN MEETINGS, CEREMONIALS, " +
    "OR INSTRUCTION FACILITIES (REIMBURSABLE EXEMPTION)";
			this.fcLabel83.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVeteransOrganizations
			// 
			this.txtExemptVeteransOrganizations.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVeteransOrganizations.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVeteransOrganizations.Focusable = false;
			this.txtExemptVeteransOrganizations.Location = new System.Drawing.Point(571, 133);
			this.txtExemptVeteransOrganizations.LockedOriginal = true;
			this.txtExemptVeteransOrganizations.MaxLength = 0;
			this.txtExemptVeteransOrganizations.Name = "txtExemptVeteransOrganizations";
			this.txtExemptVeteransOrganizations.ReadOnly = true;
			this.txtExemptVeteransOrganizations.Size = new System.Drawing.Size(100, 40);
			this.txtExemptVeteransOrganizations.TabIndex = 83;
			this.txtExemptVeteransOrganizations.Text = "0";
			this.txtExemptVeteransOrganizations.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel84
			// 
			this.fcLabel84.Location = new System.Drawing.Point(9, 133);
			this.fcLabel84.Name = "fcLabel84";
			this.fcLabel84.Size = new System.Drawing.Size(556, 58);
			this.fcLabel84.TabIndex = 82;
			this.fcLabel84.Text = "40J(1) - TOTAL EXEMPT VALUE OF VETERANS ORGANIZATIONS";
			this.fcLabel84.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptLiteraryAndScientific
			// 
			this.txtExemptLiteraryAndScientific.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptLiteraryAndScientific.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptLiteraryAndScientific.Focusable = false;
			this.txtExemptLiteraryAndScientific.Location = new System.Drawing.Point(571, 82);
			this.txtExemptLiteraryAndScientific.LockedOriginal = true;
			this.txtExemptLiteraryAndScientific.MaxLength = 0;
			this.txtExemptLiteraryAndScientific.Name = "txtExemptLiteraryAndScientific";
			this.txtExemptLiteraryAndScientific.ReadOnly = true;
			this.txtExemptLiteraryAndScientific.Size = new System.Drawing.Size(100, 40);
			this.txtExemptLiteraryAndScientific.TabIndex = 81;
			this.txtExemptLiteraryAndScientific.Text = "0";
			this.txtExemptLiteraryAndScientific.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel85
			// 
			this.fcLabel85.AutoSize = true;
			this.fcLabel85.Location = new System.Drawing.Point(9, 96);
			this.fcLabel85.Name = "fcLabel85";
			this.fcLabel85.Size = new System.Drawing.Size(394, 15);
			this.fcLabel85.TabIndex = 80;
			this.fcLabel85.Text = "40I - PROPERTY OF LITERARY AND SCIENTIFIC INSTITUTIONS";
			this.fcLabel85.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptBenevolent
			// 
			this.txtExemptBenevolent.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptBenevolent.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptBenevolent.Focusable = false;
			this.txtExemptBenevolent.Location = new System.Drawing.Point(571, 32);
			this.txtExemptBenevolent.LockedOriginal = true;
			this.txtExemptBenevolent.MaxLength = 0;
			this.txtExemptBenevolent.Name = "txtExemptBenevolent";
			this.txtExemptBenevolent.ReadOnly = true;
			this.txtExemptBenevolent.Size = new System.Drawing.Size(100, 40);
			this.txtExemptBenevolent.TabIndex = 79;
			this.txtExemptBenevolent.Text = "0";
			this.txtExemptBenevolent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel86
			// 
			this.fcLabel86.Location = new System.Drawing.Point(9, 32);
			this.fcLabel86.Name = "fcLabel86";
			this.fcLabel86.Size = new System.Drawing.Size(505, 40);
			this.fcLabel86.TabIndex = 78;
			this.fcLabel86.Text = "40H - PROPERTY OF BENEVOLENT AND CHARITABLE INSTITUTIONS";
			this.fcLabel86.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcTabPage4
			// 
			this.fcTabPage4.AutoScroll = true;
			this.fcTabPage4.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage4.Controls.Add(this.fcFrame18);
			this.fcTabPage4.Controls.Add(this.fcFrame17);
			this.fcTabPage4.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage4.Name = "fcTabPage4";
			this.fcTabPage4.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage4.Text = "Page 6";
			// 
			// fcFrame18
			// 
			this.fcFrame18.Controls.Add(this.txtExemptVetVietnamValue);
			this.fcFrame18.Controls.Add(this.txtExemptVetLebanonPanamaValue);
			this.fcFrame18.Controls.Add(this.txtExemptVetVietnamCount);
			this.fcFrame18.Controls.Add(this.fcLabel109);
			this.fcFrame18.Controls.Add(this.txtExemptVetLebanonPanamaCount);
			this.fcFrame18.Controls.Add(this.fcLabel110);
			this.fcFrame18.Controls.Add(this.txtExemptVetDisabledValue);
			this.fcFrame18.Controls.Add(this.txtExemptVetDisabledCount);
			this.fcFrame18.Controls.Add(this.fcLabel111);
			this.fcFrame18.Location = new System.Drawing.Point(14, 584);
			this.fcFrame18.Name = "fcFrame18";
			this.fcFrame18.Size = new System.Drawing.Size(854, 201);
			this.fcFrame18.TabIndex = 1;
			this.fcFrame18.Text = "Veterans That Did Not Server During A Federally Recognized War Period";
			// 
			// txtExemptVetVietnamValue
			// 
			this.txtExemptVetVietnamValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetVietnamValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetVietnamValue.Focusable = false;
			this.txtExemptVetVietnamValue.Location = new System.Drawing.Point(674, 142);
			this.txtExemptVetVietnamValue.LockedOriginal = true;
			this.txtExemptVetVietnamValue.MaxLength = 0;
			this.txtExemptVetVietnamValue.Name = "txtExemptVetVietnamValue";
			this.txtExemptVetVietnamValue.ReadOnly = true;
			this.txtExemptVetVietnamValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetVietnamValue.TabIndex = 129;
			this.txtExemptVetVietnamValue.Text = "0";
			this.txtExemptVetVietnamValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetLebanonPanamaValue
			// 
			this.txtExemptVetLebanonPanamaValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetLebanonPanamaValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetLebanonPanamaValue.Focusable = false;
			this.txtExemptVetLebanonPanamaValue.Location = new System.Drawing.Point(674, 89);
			this.txtExemptVetLebanonPanamaValue.LockedOriginal = true;
			this.txtExemptVetLebanonPanamaValue.MaxLength = 0;
			this.txtExemptVetLebanonPanamaValue.Name = "txtExemptVetLebanonPanamaValue";
			this.txtExemptVetLebanonPanamaValue.ReadOnly = true;
			this.txtExemptVetLebanonPanamaValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetLebanonPanamaValue.TabIndex = 128;
			this.txtExemptVetLebanonPanamaValue.Text = "0";
			this.txtExemptVetLebanonPanamaValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetVietnamCount
			// 
			this.txtExemptVetVietnamCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetVietnamCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetVietnamCount.Focusable = false;
			this.txtExemptVetVietnamCount.Location = new System.Drawing.Point(571, 142);
			this.txtExemptVetVietnamCount.LockedOriginal = true;
			this.txtExemptVetVietnamCount.MaxLength = 0;
			this.txtExemptVetVietnamCount.Name = "txtExemptVetVietnamCount";
			this.txtExemptVetVietnamCount.ReadOnly = true;
			this.txtExemptVetVietnamCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetVietnamCount.TabIndex = 127;
			this.txtExemptVetVietnamCount.Text = "0";
			this.txtExemptVetVietnamCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel109
			// 
			this.fcLabel109.Location = new System.Drawing.Point(9, 142);
			this.fcLabel109.Name = "fcLabel109";
			this.fcLabel109.Size = new System.Drawing.Size(556, 40);
			this.fcLabel109.TabIndex = 126;
			this.fcLabel109.Text = "40T(12) - VETERANS (OR THEIR WIDOW) WHO SERVED DURING THE PERIOD FROM 2/27/1961 A" +
    "ND 8/5/1964, BUT DID NOT SERVE PRIOR TO 2/1/1955 OR AFTER 8/4/1964";
			this.fcLabel109.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetLebanonPanamaCount
			// 
			this.txtExemptVetLebanonPanamaCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetLebanonPanamaCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetLebanonPanamaCount.Focusable = false;
			this.txtExemptVetLebanonPanamaCount.Location = new System.Drawing.Point(571, 89);
			this.txtExemptVetLebanonPanamaCount.LockedOriginal = true;
			this.txtExemptVetLebanonPanamaCount.MaxLength = 0;
			this.txtExemptVetLebanonPanamaCount.Name = "txtExemptVetLebanonPanamaCount";
			this.txtExemptVetLebanonPanamaCount.ReadOnly = true;
			this.txtExemptVetLebanonPanamaCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetLebanonPanamaCount.TabIndex = 125;
			this.txtExemptVetLebanonPanamaCount.Text = "0";
			this.txtExemptVetLebanonPanamaCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel110
			// 
			this.fcLabel110.Location = new System.Drawing.Point(9, 89);
			this.fcLabel110.Name = "fcLabel110";
			this.fcLabel110.Size = new System.Drawing.Size(445, 40);
			this.fcLabel110.TabIndex = 124;
			this.fcLabel110.Text = "40T(11) - VETERANS (OR THEIR WIDOW) WHO SERVED DURING THE PERIODS FROM 8/24/1982 " +
    "TO 7/31/1984 AND 12/20/1989 TO 1/31/1990";
			this.fcLabel110.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetDisabledValue
			// 
			this.txtExemptVetDisabledValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetDisabledValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetDisabledValue.Focusable = false;
			this.txtExemptVetDisabledValue.Location = new System.Drawing.Point(674, 36);
			this.txtExemptVetDisabledValue.LockedOriginal = true;
			this.txtExemptVetDisabledValue.MaxLength = 0;
			this.txtExemptVetDisabledValue.Name = "txtExemptVetDisabledValue";
			this.txtExemptVetDisabledValue.ReadOnly = true;
			this.txtExemptVetDisabledValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetDisabledValue.TabIndex = 123;
			this.txtExemptVetDisabledValue.Text = "0";
			this.txtExemptVetDisabledValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetDisabledCount
			// 
			this.txtExemptVetDisabledCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetDisabledCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetDisabledCount.Focusable = false;
			this.txtExemptVetDisabledCount.Location = new System.Drawing.Point(571, 36);
			this.txtExemptVetDisabledCount.LockedOriginal = true;
			this.txtExemptVetDisabledCount.MaxLength = 0;
			this.txtExemptVetDisabledCount.Name = "txtExemptVetDisabledCount";
			this.txtExemptVetDisabledCount.ReadOnly = true;
			this.txtExemptVetDisabledCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetDisabledCount.TabIndex = 122;
			this.txtExemptVetDisabledCount.Text = "0";
			this.txtExemptVetDisabledCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel111
			// 
			this.fcLabel111.Location = new System.Drawing.Point(9, 36);
			this.fcLabel111.Name = "fcLabel111";
			this.fcLabel111.Size = new System.Drawing.Size(556, 40);
			this.fcLabel111.TabIndex = 121;
			this.fcLabel111.Text = "40T(10) - VETERANS (OR THEIR WIDOW) DISABLED IN THE LINE OF DUTY";
			this.fcLabel111.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame17
			// 
			this.fcFrame17.Controls.Add(this.txtExemptVetNonResidentValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetResidentValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetNonResidentCount);
			this.fcFrame17.Controls.Add(this.fcLabel107);
			this.fcFrame17.Controls.Add(this.txtExemptVetResidentCount);
			this.fcFrame17.Controls.Add(this.fcLabel108);
			this.fcFrame17.Controls.Add(this.txtExemptVetCoopValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetParaplegicValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetWWINonResidentValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetWWIResidentValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetLivingTrustValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetParaplegicLivingTrustValue);
			this.fcFrame17.Controls.Add(this.txtExemptVetSurvivingMaleValue);
			this.fcFrame17.Controls.Add(this.fcLabel106);
			this.fcFrame17.Controls.Add(this.fcLabel105);
			this.fcFrame17.Controls.Add(this.txtExemptVetCoopCount);
			this.fcFrame17.Controls.Add(this.fcLabel98);
			this.fcFrame17.Controls.Add(this.txtExemptVetParaplegicCount);
			this.fcFrame17.Controls.Add(this.fcLabel99);
			this.fcFrame17.Controls.Add(this.txtExemptVetWWINonResidentCount);
			this.fcFrame17.Controls.Add(this.fcLabel100);
			this.fcFrame17.Controls.Add(this.txtExemptVetWWIResidentCount);
			this.fcFrame17.Controls.Add(this.fcLabel101);
			this.fcFrame17.Controls.Add(this.txtExemptVetLivingTrustCount);
			this.fcFrame17.Controls.Add(this.fcLabel102);
			this.fcFrame17.Controls.Add(this.txtExemptVetParaplegicLivingTrustCount);
			this.fcFrame17.Controls.Add(this.fcLabel103);
			this.fcFrame17.Controls.Add(this.txtExemptVetSurvivingMaleCount);
			this.fcFrame17.Controls.Add(this.fcLabel104);
			this.fcFrame17.Location = new System.Drawing.Point(14, 18);
			this.fcFrame17.Name = "fcFrame17";
			this.fcFrame17.Size = new System.Drawing.Size(855, 550);
			this.fcFrame17.TabIndex = 2;
			this.fcFrame17.Text = "Veterans That Served During A Federally Recognized War Period";
			// 
			// txtExemptVetNonResidentValue
			// 
			this.txtExemptVetNonResidentValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetNonResidentValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetNonResidentValue.Focusable = false;
			this.txtExemptVetNonResidentValue.Location = new System.Drawing.Point(674, 487);
			this.txtExemptVetNonResidentValue.LockedOriginal = true;
			this.txtExemptVetNonResidentValue.MaxLength = 0;
			this.txtExemptVetNonResidentValue.Name = "txtExemptVetNonResidentValue";
			this.txtExemptVetNonResidentValue.ReadOnly = true;
			this.txtExemptVetNonResidentValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetNonResidentValue.TabIndex = 120;
			this.txtExemptVetNonResidentValue.Text = "0";
			this.txtExemptVetNonResidentValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetResidentValue
			// 
			this.txtExemptVetResidentValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetResidentValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetResidentValue.Focusable = false;
			this.txtExemptVetResidentValue.Location = new System.Drawing.Point(674, 434);
			this.txtExemptVetResidentValue.LockedOriginal = true;
			this.txtExemptVetResidentValue.MaxLength = 0;
			this.txtExemptVetResidentValue.Name = "txtExemptVetResidentValue";
			this.txtExemptVetResidentValue.ReadOnly = true;
			this.txtExemptVetResidentValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetResidentValue.TabIndex = 119;
			this.txtExemptVetResidentValue.Text = "0";
			this.txtExemptVetResidentValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetNonResidentCount
			// 
			this.txtExemptVetNonResidentCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetNonResidentCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetNonResidentCount.Focusable = false;
			this.txtExemptVetNonResidentCount.Location = new System.Drawing.Point(571, 487);
			this.txtExemptVetNonResidentCount.LockedOriginal = true;
			this.txtExemptVetNonResidentCount.MaxLength = 0;
			this.txtExemptVetNonResidentCount.Name = "txtExemptVetNonResidentCount";
			this.txtExemptVetNonResidentCount.ReadOnly = true;
			this.txtExemptVetNonResidentCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetNonResidentCount.TabIndex = 118;
			this.txtExemptVetNonResidentCount.Text = "0";
			this.txtExemptVetNonResidentCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel107
			// 
			this.fcLabel107.Location = new System.Drawing.Point(9, 487);
			this.fcLabel107.Name = "fcLabel107";
			this.fcLabel107.Size = new System.Drawing.Size(556, 40);
			this.fcLabel107.TabIndex = 117;
			this.fcLabel107.Text = "40T(9) - ALL OTHER VETERANS (OR THEIR WIDOWS) ENLISTED AS NON-MAINE RESIDENTS";
			this.fcLabel107.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetResidentCount
			// 
			this.txtExemptVetResidentCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetResidentCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetResidentCount.Focusable = false;
			this.txtExemptVetResidentCount.Location = new System.Drawing.Point(571, 434);
			this.txtExemptVetResidentCount.LockedOriginal = true;
			this.txtExemptVetResidentCount.MaxLength = 0;
			this.txtExemptVetResidentCount.Name = "txtExemptVetResidentCount";
			this.txtExemptVetResidentCount.ReadOnly = true;
			this.txtExemptVetResidentCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetResidentCount.TabIndex = 116;
			this.txtExemptVetResidentCount.Text = "0";
			this.txtExemptVetResidentCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel108
			// 
			this.fcLabel108.Location = new System.Drawing.Point(9, 434);
			this.fcLabel108.Name = "fcLabel108";
			this.fcLabel108.Size = new System.Drawing.Size(445, 40);
			this.fcLabel108.TabIndex = 115;
			this.fcLabel108.Text = "40T(8) - ALL OTHER VETERANS (OR THEIR WIDOWS) ENLISTED AS MAINE RESIDENTS";
			this.fcLabel108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetCoopValue
			// 
			this.txtExemptVetCoopValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetCoopValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetCoopValue.Focusable = false;
			this.txtExemptVetCoopValue.Location = new System.Drawing.Point(674, 381);
			this.txtExemptVetCoopValue.LockedOriginal = true;
			this.txtExemptVetCoopValue.MaxLength = 0;
			this.txtExemptVetCoopValue.Name = "txtExemptVetCoopValue";
			this.txtExemptVetCoopValue.ReadOnly = true;
			this.txtExemptVetCoopValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetCoopValue.TabIndex = 114;
			this.txtExemptVetCoopValue.Text = "0";
			this.txtExemptVetCoopValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetParaplegicValue
			// 
			this.txtExemptVetParaplegicValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetParaplegicValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetParaplegicValue.Focusable = false;
			this.txtExemptVetParaplegicValue.Location = new System.Drawing.Point(674, 328);
			this.txtExemptVetParaplegicValue.LockedOriginal = true;
			this.txtExemptVetParaplegicValue.MaxLength = 0;
			this.txtExemptVetParaplegicValue.Name = "txtExemptVetParaplegicValue";
			this.txtExemptVetParaplegicValue.ReadOnly = true;
			this.txtExemptVetParaplegicValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetParaplegicValue.TabIndex = 113;
			this.txtExemptVetParaplegicValue.Text = "0";
			this.txtExemptVetParaplegicValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetWWINonResidentValue
			// 
			this.txtExemptVetWWINonResidentValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetWWINonResidentValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetWWINonResidentValue.Focusable = false;
			this.txtExemptVetWWINonResidentValue.Location = new System.Drawing.Point(674, 277);
			this.txtExemptVetWWINonResidentValue.LockedOriginal = true;
			this.txtExemptVetWWINonResidentValue.MaxLength = 0;
			this.txtExemptVetWWINonResidentValue.Name = "txtExemptVetWWINonResidentValue";
			this.txtExemptVetWWINonResidentValue.ReadOnly = true;
			this.txtExemptVetWWINonResidentValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetWWINonResidentValue.TabIndex = 112;
			this.txtExemptVetWWINonResidentValue.Text = "0";
			this.txtExemptVetWWINonResidentValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetWWIResidentValue
			// 
			this.txtExemptVetWWIResidentValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetWWIResidentValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetWWIResidentValue.Focusable = false;
			this.txtExemptVetWWIResidentValue.Location = new System.Drawing.Point(674, 225);
			this.txtExemptVetWWIResidentValue.LockedOriginal = true;
			this.txtExemptVetWWIResidentValue.MaxLength = 0;
			this.txtExemptVetWWIResidentValue.Name = "txtExemptVetWWIResidentValue";
			this.txtExemptVetWWIResidentValue.ReadOnly = true;
			this.txtExemptVetWWIResidentValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetWWIResidentValue.TabIndex = 111;
			this.txtExemptVetWWIResidentValue.Text = "0";
			this.txtExemptVetWWIResidentValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetLivingTrustValue
			// 
			this.txtExemptVetLivingTrustValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetLivingTrustValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetLivingTrustValue.Focusable = false;
			this.txtExemptVetLivingTrustValue.Location = new System.Drawing.Point(674, 173);
			this.txtExemptVetLivingTrustValue.LockedOriginal = true;
			this.txtExemptVetLivingTrustValue.MaxLength = 0;
			this.txtExemptVetLivingTrustValue.Name = "txtExemptVetLivingTrustValue";
			this.txtExemptVetLivingTrustValue.ReadOnly = true;
			this.txtExemptVetLivingTrustValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetLivingTrustValue.TabIndex = 110;
			this.txtExemptVetLivingTrustValue.Text = "0";
			this.txtExemptVetLivingTrustValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetParaplegicLivingTrustValue
			// 
			this.txtExemptVetParaplegicLivingTrustValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetParaplegicLivingTrustValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetParaplegicLivingTrustValue.Focusable = false;
			this.txtExemptVetParaplegicLivingTrustValue.Location = new System.Drawing.Point(674, 122);
			this.txtExemptVetParaplegicLivingTrustValue.LockedOriginal = true;
			this.txtExemptVetParaplegicLivingTrustValue.MaxLength = 0;
			this.txtExemptVetParaplegicLivingTrustValue.Name = "txtExemptVetParaplegicLivingTrustValue";
			this.txtExemptVetParaplegicLivingTrustValue.ReadOnly = true;
			this.txtExemptVetParaplegicLivingTrustValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetParaplegicLivingTrustValue.TabIndex = 109;
			this.txtExemptVetParaplegicLivingTrustValue.Text = "0";
			this.txtExemptVetParaplegicLivingTrustValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptVetSurvivingMaleValue
			// 
			this.txtExemptVetSurvivingMaleValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetSurvivingMaleValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetSurvivingMaleValue.Focusable = false;
			this.txtExemptVetSurvivingMaleValue.Location = new System.Drawing.Point(674, 72);
			this.txtExemptVetSurvivingMaleValue.LockedOriginal = true;
			this.txtExemptVetSurvivingMaleValue.MaxLength = 0;
			this.txtExemptVetSurvivingMaleValue.Name = "txtExemptVetSurvivingMaleValue";
			this.txtExemptVetSurvivingMaleValue.ReadOnly = true;
			this.txtExemptVetSurvivingMaleValue.Size = new System.Drawing.Size(95, 40);
			this.txtExemptVetSurvivingMaleValue.TabIndex = 108;
			this.txtExemptVetSurvivingMaleValue.Text = "0";
			this.txtExemptVetSurvivingMaleValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel106
			// 
			this.fcLabel106.Location = new System.Drawing.Point(699, 11);
			this.fcLabel106.Name = "fcLabel106";
			this.fcLabel106.Size = new System.Drawing.Size(70, 57);
			this.fcLabel106.TabIndex = 107;
			this.fcLabel106.Text = "EXEMPT VALUE";
			this.fcLabel106.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// fcLabel105
			// 
			this.fcLabel105.Location = new System.Drawing.Point(542, 11);
			this.fcLabel105.Name = "fcLabel105";
			this.fcLabel105.Size = new System.Drawing.Size(99, 57);
			this.fcLabel105.TabIndex = 106;
			this.fcLabel105.Text = "NUMBER OF EXEMPTIONS";
			this.fcLabel105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtExemptVetCoopCount
			// 
			this.txtExemptVetCoopCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetCoopCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetCoopCount.Focusable = false;
			this.txtExemptVetCoopCount.Location = new System.Drawing.Point(571, 381);
			this.txtExemptVetCoopCount.LockedOriginal = true;
			this.txtExemptVetCoopCount.MaxLength = 0;
			this.txtExemptVetCoopCount.Name = "txtExemptVetCoopCount";
			this.txtExemptVetCoopCount.ReadOnly = true;
			this.txtExemptVetCoopCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetCoopCount.TabIndex = 105;
			this.txtExemptVetCoopCount.Text = "0";
			this.txtExemptVetCoopCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel98
			// 
			this.fcLabel98.Location = new System.Drawing.Point(9, 381);
			this.fcLabel98.Name = "fcLabel98";
			this.fcLabel98.Size = new System.Drawing.Size(556, 40);
			this.fcLabel98.TabIndex = 104;
			this.fcLabel98.Text = "40T(7) - QUALIFYING SHAREHOLDERS OF COOPERATIVE HOUSING CORPORATIONS";
			this.fcLabel98.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetParaplegicCount
			// 
			this.txtExemptVetParaplegicCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetParaplegicCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetParaplegicCount.Focusable = false;
			this.txtExemptVetParaplegicCount.Location = new System.Drawing.Point(571, 328);
			this.txtExemptVetParaplegicCount.LockedOriginal = true;
			this.txtExemptVetParaplegicCount.MaxLength = 0;
			this.txtExemptVetParaplegicCount.Name = "txtExemptVetParaplegicCount";
			this.txtExemptVetParaplegicCount.ReadOnly = true;
			this.txtExemptVetParaplegicCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetParaplegicCount.TabIndex = 103;
			this.txtExemptVetParaplegicCount.Text = "0";
			this.txtExemptVetParaplegicCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel99
			// 
			this.fcLabel99.Location = new System.Drawing.Point(9, 328);
			this.fcLabel99.Name = "fcLabel99";
			this.fcLabel99.Size = new System.Drawing.Size(526, 40);
			this.fcLabel99.TabIndex = 102;
			this.fcLabel99.Text = "40T(6) - PARAPLEGIC STATUS VETERAN OR THEIR UNREMARRIED WIDOW";
			this.fcLabel99.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetWWINonResidentCount
			// 
			this.txtExemptVetWWINonResidentCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetWWINonResidentCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetWWINonResidentCount.Focusable = false;
			this.txtExemptVetWWINonResidentCount.Location = new System.Drawing.Point(571, 277);
			this.txtExemptVetWWINonResidentCount.LockedOriginal = true;
			this.txtExemptVetWWINonResidentCount.MaxLength = 0;
			this.txtExemptVetWWINonResidentCount.Name = "txtExemptVetWWINonResidentCount";
			this.txtExemptVetWWINonResidentCount.ReadOnly = true;
			this.txtExemptVetWWINonResidentCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetWWINonResidentCount.TabIndex = 101;
			this.txtExemptVetWWINonResidentCount.Text = "0";
			this.txtExemptVetWWINonResidentCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel100
			// 
			this.fcLabel100.AutoSize = true;
			this.fcLabel100.Location = new System.Drawing.Point(9, 291);
			this.fcLabel100.Name = "fcLabel100";
			this.fcLabel100.Size = new System.Drawing.Size(529, 15);
			this.fcLabel100.TabIndex = 100;
			this.fcLabel100.Text = "40T(5) - WWI VETERAN (OR THEIR WIDOWS) ENLISTED AS A NON-MAINE RESIDENT";
			this.fcLabel100.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetWWIResidentCount
			// 
			this.txtExemptVetWWIResidentCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetWWIResidentCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetWWIResidentCount.Focusable = false;
			this.txtExemptVetWWIResidentCount.Location = new System.Drawing.Point(571, 225);
			this.txtExemptVetWWIResidentCount.LockedOriginal = true;
			this.txtExemptVetWWIResidentCount.MaxLength = 0;
			this.txtExemptVetWWIResidentCount.Name = "txtExemptVetWWIResidentCount";
			this.txtExemptVetWWIResidentCount.ReadOnly = true;
			this.txtExemptVetWWIResidentCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetWWIResidentCount.TabIndex = 99;
			this.txtExemptVetWWIResidentCount.Text = "0";
			this.txtExemptVetWWIResidentCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel101
			// 
			this.fcLabel101.Location = new System.Drawing.Point(9, 225);
			this.fcLabel101.Name = "fcLabel101";
			this.fcLabel101.Size = new System.Drawing.Size(546, 40);
			this.fcLabel101.TabIndex = 98;
			this.fcLabel101.Text = "40T(4) - WWI VETERAN (OR THEIR WIDOWS) ENLISTED AS A MAINE RESIDENT";
			this.fcLabel101.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetLivingTrustCount
			// 
			this.txtExemptVetLivingTrustCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetLivingTrustCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetLivingTrustCount.Focusable = false;
			this.txtExemptVetLivingTrustCount.Location = new System.Drawing.Point(571, 173);
			this.txtExemptVetLivingTrustCount.LockedOriginal = true;
			this.txtExemptVetLivingTrustCount.MaxLength = 0;
			this.txtExemptVetLivingTrustCount.Name = "txtExemptVetLivingTrustCount";
			this.txtExemptVetLivingTrustCount.ReadOnly = true;
			this.txtExemptVetLivingTrustCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetLivingTrustCount.TabIndex = 97;
			this.txtExemptVetLivingTrustCount.Text = "0";
			this.txtExemptVetLivingTrustCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel102
			// 
			this.fcLabel102.Location = new System.Drawing.Point(9, 173);
			this.fcLabel102.Name = "fcLabel102";
			this.fcLabel102.Size = new System.Drawing.Size(556, 40);
			this.fcLabel102.TabIndex = 96;
			this.fcLabel102.Text = "40T(3) - ALL OTHER VETERANS (OR THEIR WIDOWS) WHO ARE THE BENEFICIARY OF A REVOCA" +
    "BLE TRUST";
			this.fcLabel102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetParaplegicLivingTrustCount
			// 
			this.txtExemptVetParaplegicLivingTrustCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetParaplegicLivingTrustCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetParaplegicLivingTrustCount.Focusable = false;
			this.txtExemptVetParaplegicLivingTrustCount.Location = new System.Drawing.Point(571, 122);
			this.txtExemptVetParaplegicLivingTrustCount.LockedOriginal = true;
			this.txtExemptVetParaplegicLivingTrustCount.MaxLength = 0;
			this.txtExemptVetParaplegicLivingTrustCount.Name = "txtExemptVetParaplegicLivingTrustCount";
			this.txtExemptVetParaplegicLivingTrustCount.ReadOnly = true;
			this.txtExemptVetParaplegicLivingTrustCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetParaplegicLivingTrustCount.TabIndex = 95;
			this.txtExemptVetParaplegicLivingTrustCount.Text = "0";
			this.txtExemptVetParaplegicLivingTrustCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel103
			// 
			this.fcLabel103.Location = new System.Drawing.Point(9, 122);
			this.fcLabel103.Name = "fcLabel103";
			this.fcLabel103.Size = new System.Drawing.Size(535, 40);
			this.fcLabel103.TabIndex = 94;
			this.fcLabel103.Text = "40T(2) - PARAPLEGIC VETERANS (OR THEIR WIDOW) WHO ARE THE BENEFICIARY OF A REVOCA" +
    "BLE LIVING TRUST";
			this.fcLabel103.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptVetSurvivingMaleCount
			// 
			this.txtExemptVetSurvivingMaleCount.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptVetSurvivingMaleCount.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptVetSurvivingMaleCount.Focusable = false;
			this.txtExemptVetSurvivingMaleCount.Location = new System.Drawing.Point(571, 72);
			this.txtExemptVetSurvivingMaleCount.LockedOriginal = true;
			this.txtExemptVetSurvivingMaleCount.MaxLength = 0;
			this.txtExemptVetSurvivingMaleCount.Name = "txtExemptVetSurvivingMaleCount";
			this.txtExemptVetSurvivingMaleCount.ReadOnly = true;
			this.txtExemptVetSurvivingMaleCount.Size = new System.Drawing.Size(70, 40);
			this.txtExemptVetSurvivingMaleCount.TabIndex = 93;
			this.txtExemptVetSurvivingMaleCount.Text = "0";
			this.txtExemptVetSurvivingMaleCount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel104
			// 
			this.fcLabel104.Location = new System.Drawing.Point(9, 72);
			this.fcLabel104.Name = "fcLabel104";
			this.fcLabel104.Size = new System.Drawing.Size(505, 40);
			this.fcLabel104.TabIndex = 92;
			this.fcLabel104.Text = "40T(1) - LIVING MALE SPOUSE OR MALE PARENT OF A DECEASED VETERAN";
			this.fcLabel104.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcTabPage5
			// 
			this.fcTabPage5.AutoScroll = true;
			this.fcTabPage5.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage5.Controls.Add(this.fcFrame19);
			this.fcTabPage5.Controls.Add(this.fcFrame16);
			this.fcTabPage5.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage5.Name = "fcTabPage5";
			this.fcTabPage5.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage5.Text = "Page 7";
			// 
			// fcFrame19
			// 
			this.fcFrame19.Controls.Add(this.chkHasTaxMaps);
			this.fcFrame19.Controls.Add(this.txtRevaluationCost);
			this.fcFrame19.Controls.Add(this.fcLabel126);
			this.fcFrame19.Controls.Add(this.txtRevaluationContractorName);
			this.fcFrame19.Controls.Add(this.fcLabel124);
			this.fcFrame19.Controls.Add(this.dtRevaluationEffectiveDate);
			this.fcFrame19.Controls.Add(this.fcLabel125);
			this.fcFrame19.Controls.Add(this.chkRevaluationIncludedPersonalProperty);
			this.fcFrame19.Controls.Add(this.fcLabel122);
			this.fcFrame19.Controls.Add(this.chkRevaluationIncludedBuildings);
			this.fcFrame19.Controls.Add(this.fcLabel123);
			this.fcFrame19.Controls.Add(this.chkRevaluationIncludedLand);
			this.fcFrame19.Controls.Add(this.fcLabel121);
			this.fcFrame19.Controls.Add(this.chkRevaluationCompleted);
			this.fcFrame19.Controls.Add(this.fcLabel120);
			this.fcFrame19.Controls.Add(this.txtMunicipalRecordsTotalTaxableAcreage);
			this.fcFrame19.Controls.Add(this.fcLabel119);
			this.fcFrame19.Controls.Add(this.txtMunicipalRecordsTotalNumberOfParcels);
			this.fcFrame19.Controls.Add(this.fcLabel118);
			this.fcFrame19.Controls.Add(this.cmbTaxMapType);
			this.fcFrame19.Controls.Add(this.fcLabel117);
			this.fcFrame19.Controls.Add(this.txtNameOfOriginalContractor);
			this.fcFrame19.Controls.Add(this.fcLabel116);
			this.fcFrame19.Controls.Add(this.dtMapsOriginallyObtained);
			this.fcFrame19.Controls.Add(this.fcLabel115);
			this.fcFrame19.Controls.Add(this.fcLabel114);
			this.fcFrame19.Location = new System.Drawing.Point(23, 428);
			this.fcFrame19.Name = "fcFrame19";
			this.fcFrame19.Size = new System.Drawing.Size(782, 691);
			this.fcFrame19.TabIndex = 2;
			this.fcFrame19.Text = "Municipal Records";
			// 
			// chkHasTaxMaps
			// 
			this.chkHasTaxMaps.AutoSize = false;
			this.chkHasTaxMaps.Location = new System.Drawing.Point(334, 23);
			this.chkHasTaxMaps.Name = "chkHasTaxMaps";
			this.chkHasTaxMaps.Size = new System.Drawing.Size(50, 39);
			this.chkHasTaxMaps.TabIndex = 1;
			// 
			// txtRevaluationCost
			// 
			this.txtRevaluationCost.BackColor = System.Drawing.SystemColors.Window;
			this.txtRevaluationCost.Cursor = Wisej.Web.Cursors.Default;
			this.txtRevaluationCost.Location = new System.Drawing.Point(334, 625);
			this.txtRevaluationCost.MaxLength = 0;
			this.txtRevaluationCost.Name = "txtRevaluationCost";
			this.txtRevaluationCost.Size = new System.Drawing.Size(117, 40);
			this.txtRevaluationCost.TabIndex = 13;
			this.txtRevaluationCost.Text = "0";
			this.txtRevaluationCost.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel126
			// 
			this.fcLabel126.Location = new System.Drawing.Point(6, 625);
			this.fcLabel126.Name = "fcLabel126";
			this.fcLabel126.Size = new System.Drawing.Size(373, 40);
			this.fcLabel126.TabIndex = 99;
			this.fcLabel126.Text = "44E - COST";
			this.fcLabel126.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtRevaluationContractorName
			// 
			this.txtRevaluationContractorName.Cursor = Wisej.Web.Cursors.Default;
			this.txtRevaluationContractorName.Location = new System.Drawing.Point(334, 575);
			this.txtRevaluationContractorName.MaxLength = 75;
			this.txtRevaluationContractorName.Name = "txtRevaluationContractorName";
			this.txtRevaluationContractorName.Size = new System.Drawing.Size(368, 40);
			this.txtRevaluationContractorName.TabIndex = 12;
			// 
			// fcLabel124
			// 
			this.fcLabel124.Location = new System.Drawing.Point(6, 575);
			this.fcLabel124.Name = "fcLabel124";
			this.fcLabel124.Size = new System.Drawing.Size(373, 40);
			this.fcLabel124.TabIndex = 97;
			this.fcLabel124.Text = "44D - CONTRACTOR NAME";
			this.fcLabel124.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtRevaluationEffectiveDate
			// 
			this.dtRevaluationEffectiveDate.AutoSize = false;
			this.dtRevaluationEffectiveDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtRevaluationEffectiveDate.Location = new System.Drawing.Point(334, 525);
			this.dtRevaluationEffectiveDate.Mask = "00/00/0000";
			this.dtRevaluationEffectiveDate.Name = "dtRevaluationEffectiveDate";
			this.dtRevaluationEffectiveDate.Size = new System.Drawing.Size(166, 40);
			this.dtRevaluationEffectiveDate.TabIndex = 11;
			// 
			// fcLabel125
			// 
			this.fcLabel125.Location = new System.Drawing.Point(6, 525);
			this.fcLabel125.Name = "fcLabel125";
			this.fcLabel125.Size = new System.Drawing.Size(505, 40);
			this.fcLabel125.TabIndex = 95;
			this.fcLabel125.Text = "44C - EFFECTIVE DATE";
			this.fcLabel125.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkRevaluationIncludedPersonalProperty
			// 
			this.chkRevaluationIncludedPersonalProperty.AutoSize = false;
			this.chkRevaluationIncludedPersonalProperty.Location = new System.Drawing.Point(425, 475);
			this.chkRevaluationIncludedPersonalProperty.Name = "chkRevaluationIncludedPersonalProperty";
			this.chkRevaluationIncludedPersonalProperty.Size = new System.Drawing.Size(50, 39);
			this.chkRevaluationIncludedPersonalProperty.TabIndex = 10;
			// 
			// fcLabel122
			// 
			this.fcLabel122.Location = new System.Drawing.Point(6, 475);
			this.fcLabel122.Name = "fcLabel122";
			this.fcLabel122.Size = new System.Drawing.Size(373, 40);
			this.fcLabel122.TabIndex = 93;
			this.fcLabel122.Text = "44B(3) - REVALUATION INCLUDED PERSONAL PROPERTY";
			this.fcLabel122.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkRevaluationIncludedBuildings
			// 
			this.chkRevaluationIncludedBuildings.Anchor = Wisej.Web.AnchorStyles.Top;
			this.chkRevaluationIncludedBuildings.AutoSize = false;
			this.chkRevaluationIncludedBuildings.Location = new System.Drawing.Point(425, 425);
			this.chkRevaluationIncludedBuildings.Name = "chkRevaluationIncludedBuildings";
			this.chkRevaluationIncludedBuildings.Size = new System.Drawing.Size(50, 39);
			this.chkRevaluationIncludedBuildings.TabIndex = 9;
			// 
			// fcLabel123
			// 
			this.fcLabel123.Location = new System.Drawing.Point(6, 425);
			this.fcLabel123.Name = "fcLabel123";
			this.fcLabel123.Size = new System.Drawing.Size(373, 40);
			this.fcLabel123.TabIndex = 91;
			this.fcLabel123.Text = "44B(2) - REVALUATION INCLUDED BUILDINGS";
			this.fcLabel123.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkRevaluationIncludedLand
			// 
			this.chkRevaluationIncludedLand.AutoSize = false;
			this.chkRevaluationIncludedLand.Location = new System.Drawing.Point(425, 375);
			this.chkRevaluationIncludedLand.Name = "chkRevaluationIncludedLand";
			this.chkRevaluationIncludedLand.Size = new System.Drawing.Size(50, 39);
			this.chkRevaluationIncludedLand.TabIndex = 8;
			// 
			// fcLabel121
			// 
			this.fcLabel121.Location = new System.Drawing.Point(6, 375);
			this.fcLabel121.Name = "fcLabel121";
			this.fcLabel121.Size = new System.Drawing.Size(373, 40);
			this.fcLabel121.TabIndex = 89;
			this.fcLabel121.Text = "44B(1) - REVALUATION INCLUDED LAND";
			this.fcLabel121.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkRevaluationCompleted
			// 
			this.chkRevaluationCompleted.AutoSize = false;
			this.chkRevaluationCompleted.Location = new System.Drawing.Point(425, 325);
			this.chkRevaluationCompleted.Name = "chkRevaluationCompleted";
			this.chkRevaluationCompleted.Size = new System.Drawing.Size(50, 39);
			this.chkRevaluationCompleted.TabIndex = 7;
			// 
			// fcLabel120
			// 
			this.fcLabel120.Location = new System.Drawing.Point(6, 325);
			this.fcLabel120.Name = "fcLabel120";
			this.fcLabel120.Size = new System.Drawing.Size(373, 40);
			this.fcLabel120.TabIndex = 86;
			this.fcLabel120.Text = "44A - A PROFESSIONAL TOWN-WIDE REVALUATION HAS BEEN COMPLETED IN THIS MUNICIPALIT" +
    "Y";
			this.fcLabel120.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMunicipalRecordsTotalTaxableAcreage
			// 
			this.txtMunicipalRecordsTotalTaxableAcreage.BackColor = System.Drawing.Color.FromName("@control");
			this.txtMunicipalRecordsTotalTaxableAcreage.Cursor = Wisej.Web.Cursors.Default;
			this.txtMunicipalRecordsTotalTaxableAcreage.Focusable = false;
			this.txtMunicipalRecordsTotalTaxableAcreage.Location = new System.Drawing.Point(425, 275);
			this.txtMunicipalRecordsTotalTaxableAcreage.LockedOriginal = true;
			this.txtMunicipalRecordsTotalTaxableAcreage.MaxLength = 0;
			this.txtMunicipalRecordsTotalTaxableAcreage.Name = "txtMunicipalRecordsTotalTaxableAcreage";
			this.txtMunicipalRecordsTotalTaxableAcreage.ReadOnly = true;
			this.txtMunicipalRecordsTotalTaxableAcreage.Size = new System.Drawing.Size(117, 40);
			this.txtMunicipalRecordsTotalTaxableAcreage.TabIndex = 6;
			this.txtMunicipalRecordsTotalTaxableAcreage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel119
			// 
			this.fcLabel119.Location = new System.Drawing.Point(6, 275);
			this.fcLabel119.Name = "fcLabel119";
			this.fcLabel119.Size = new System.Drawing.Size(396, 40);
			this.fcLabel119.TabIndex = 84;
			this.fcLabel119.Text = "43 - TOTAL TAXABLE LAND ACREAGE IN YOUR MUNICIPALITY";
			this.fcLabel119.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMunicipalRecordsTotalNumberOfParcels
			// 
			this.txtMunicipalRecordsTotalNumberOfParcels.BackColor = System.Drawing.Color.FromName("@control");
			this.txtMunicipalRecordsTotalNumberOfParcels.Cursor = Wisej.Web.Cursors.Default;
			this.txtMunicipalRecordsTotalNumberOfParcels.Focusable = false;
			this.txtMunicipalRecordsTotalNumberOfParcels.Location = new System.Drawing.Point(425, 225);
			this.txtMunicipalRecordsTotalNumberOfParcels.LockedOriginal = true;
			this.txtMunicipalRecordsTotalNumberOfParcels.MaxLength = 0;
			this.txtMunicipalRecordsTotalNumberOfParcels.Name = "txtMunicipalRecordsTotalNumberOfParcels";
			this.txtMunicipalRecordsTotalNumberOfParcels.ReadOnly = true;
			this.txtMunicipalRecordsTotalNumberOfParcels.Size = new System.Drawing.Size(117, 40);
			this.txtMunicipalRecordsTotalNumberOfParcels.TabIndex = 5;
			this.txtMunicipalRecordsTotalNumberOfParcels.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel118
			// 
			this.fcLabel118.Location = new System.Drawing.Point(6, 225);
			this.fcLabel118.Name = "fcLabel118";
			this.fcLabel118.Size = new System.Drawing.Size(373, 40);
			this.fcLabel118.TabIndex = 82;
			this.fcLabel118.Text = "42 - NUMBER OF PARCELS WITHIN YOUR MUNICIPAL ASSESSING JURISDICTION";
			this.fcLabel118.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbTaxMapType
			// 
			this.cmbTaxMapType.Location = new System.Drawing.Point(334, 175);
			this.cmbTaxMapType.Name = "cmbTaxMapType";
			this.cmbTaxMapType.Size = new System.Drawing.Size(166, 40);
			this.cmbTaxMapType.TabIndex = 4;
			// 
			// fcLabel117
			// 
			this.fcLabel117.Location = new System.Drawing.Point(6, 175);
			this.fcLabel117.Name = "fcLabel117";
			this.fcLabel117.Size = new System.Drawing.Size(373, 40);
			this.fcLabel117.TabIndex = 80;
			this.fcLabel117.Text = "41D - ARE YOUR TAX MAPS PAPER, GIS OR CAD?";
			this.fcLabel117.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNameOfOriginalContractor
			// 
			this.txtNameOfOriginalContractor.Cursor = Wisej.Web.Cursors.Default;
			this.txtNameOfOriginalContractor.Location = new System.Drawing.Point(334, 125);
			this.txtNameOfOriginalContractor.MaxLength = 75;
			this.txtNameOfOriginalContractor.Name = "txtNameOfOriginalContractor";
			this.txtNameOfOriginalContractor.Size = new System.Drawing.Size(428, 40);
			this.txtNameOfOriginalContractor.TabIndex = 3;
			// 
			// fcLabel116
			// 
			this.fcLabel116.Location = new System.Drawing.Point(6, 125);
			this.fcLabel116.Name = "fcLabel116";
			this.fcLabel116.Size = new System.Drawing.Size(373, 40);
			this.fcLabel116.TabIndex = 78;
			this.fcLabel116.Text = "41C - NAME OF ORIGINAL CONTRACTOR";
			this.fcLabel116.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtMapsOriginallyObtained
			// 
			this.dtMapsOriginallyObtained.AutoSize = false;
			this.dtMapsOriginallyObtained.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtMapsOriginallyObtained.Location = new System.Drawing.Point(334, 75);
			this.dtMapsOriginallyObtained.Mask = "00/00/0000";
			this.dtMapsOriginallyObtained.Name = "dtMapsOriginallyObtained";
			this.dtMapsOriginallyObtained.Size = new System.Drawing.Size(166, 40);
			this.dtMapsOriginallyObtained.TabIndex = 2;
			// 
			// fcLabel115
			// 
			this.fcLabel115.Location = new System.Drawing.Point(6, 75);
			this.fcLabel115.Name = "fcLabel115";
			this.fcLabel115.Size = new System.Drawing.Size(373, 40);
			this.fcLabel115.TabIndex = 76;
			this.fcLabel115.Text = "41B - DATE MAPS ORIGINALLY OBTAINED";
			this.fcLabel115.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcLabel114
			// 
			this.fcLabel114.Location = new System.Drawing.Point(6, 25);
			this.fcLabel114.Name = "fcLabel114";
			this.fcLabel114.Size = new System.Drawing.Size(373, 40);
			this.fcLabel114.TabIndex = 74;
			this.fcLabel114.Text = "41A - MUNICIPALITY HAS TAX MAPS";
			this.fcLabel114.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcFrame16
			// 
			this.fcFrame16.Controls.Add(this.fcLabel154);
			this.fcFrame16.Controls.Add(this.fcLabel148);
			this.fcFrame16.Controls.Add(this.txtExemptSolarAndWindValue);
			this.fcFrame16.Controls.Add(this.txtExemptOrganization3ExemptValue);
			this.fcFrame16.Controls.Add(this.txtExemptProvisionOfLaw3);
			this.fcFrame16.Controls.Add(this.txtExemptOrganization3Name);
			this.fcFrame16.Controls.Add(this.txtExemptOrganization2ExemptValue);
			this.fcFrame16.Controls.Add(this.txtExemptSolarAndWindApproved);
			this.fcFrame16.Controls.Add(this.txtExemptProvisionOfLaw2);
			this.fcFrame16.Controls.Add(this.txtExemptOrganization2Name);
			this.fcFrame16.Controls.Add(this.fcLabel149);
			this.fcFrame16.Controls.Add(this.fcLabel113);
			this.fcFrame16.Controls.Add(this.fcLabel112);
			this.fcFrame16.Controls.Add(this.txtExemptSolarAndWindProcessed);
			this.fcFrame16.Controls.Add(this.fcLabel97);
			this.fcFrame16.Controls.Add(this.txtExemptOrganization1ExemptValue);
			this.fcFrame16.Controls.Add(this.fcLabel150);
			this.fcFrame16.Controls.Add(this.txtExemptProvisionOfLaw1);
			this.fcFrame16.Controls.Add(this.txtExemptOrganization1Name);
			this.fcFrame16.Location = new System.Drawing.Point(23, 29);
			this.fcFrame16.Name = "fcFrame16";
			this.fcFrame16.Size = new System.Drawing.Size(782, 381);
			this.fcFrame16.TabIndex = 1;
			this.fcFrame16.Text = "Exempt Property Continued";
			// 
			// fcLabel154
			// 
			this.fcLabel154.Location = new System.Drawing.Point(6, 170);
			this.fcLabel154.Name = "fcLabel154";
			this.fcLabel154.Size = new System.Drawing.Size(556, 40);
			this.fcLabel154.TabIndex = 89;
			this.fcLabel154.Text = "40V - OTHER";
			this.fcLabel154.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcLabel148
			// 
			this.fcLabel148.Location = new System.Drawing.Point(6, 119);
			this.fcLabel148.Name = "fcLabel148";
			this.fcLabel148.Size = new System.Drawing.Size(556, 40);
			this.fcLabel148.TabIndex = 88;
			this.fcLabel148.Text = "40U(3) - TOTAL EXEMPT VALUE OF SOLAR AND WIND ENERGY EQUIPMENT";
			this.fcLabel148.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptSolarAndWindValue
			// 
			this.txtExemptSolarAndWindValue.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptSolarAndWindValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptSolarAndWindValue.Focusable = false;
			this.txtExemptSolarAndWindValue.Location = new System.Drawing.Point(588, 121);
			this.txtExemptSolarAndWindValue.LockedOriginal = true;
			this.txtExemptSolarAndWindValue.MaxLength = 0;
			this.txtExemptSolarAndWindValue.Name = "txtExemptSolarAndWindValue";
			this.txtExemptSolarAndWindValue.ReadOnly = true;
			this.txtExemptSolarAndWindValue.Size = new System.Drawing.Size(100, 40);
			this.txtExemptSolarAndWindValue.TabIndex = 77;
			this.txtExemptSolarAndWindValue.Text = "0";
			this.txtExemptSolarAndWindValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptOrganization3ExemptValue
			// 
			this.txtExemptOrganization3ExemptValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptOrganization3ExemptValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptOrganization3ExemptValue.Location = new System.Drawing.Point(588, 324);
			this.txtExemptOrganization3ExemptValue.MaxLength = 0;
			this.txtExemptOrganization3ExemptValue.Name = "txtExemptOrganization3ExemptValue";
			this.txtExemptOrganization3ExemptValue.Size = new System.Drawing.Size(100, 40);
			this.txtExemptOrganization3ExemptValue.TabIndex = 86;
			this.txtExemptOrganization3ExemptValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptProvisionOfLaw3
			// 
			this.txtExemptProvisionOfLaw3.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptProvisionOfLaw3.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptProvisionOfLaw3.Location = new System.Drawing.Point(391, 324);
			this.txtExemptProvisionOfLaw3.MaxLength = 40;
			this.txtExemptProvisionOfLaw3.Name = "txtExemptProvisionOfLaw3";
			this.txtExemptProvisionOfLaw3.Size = new System.Drawing.Size(182, 40);
			this.txtExemptProvisionOfLaw3.TabIndex = 85;
			// 
			// txtExemptOrganization3Name
			// 
			this.txtExemptOrganization3Name.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptOrganization3Name.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptOrganization3Name.Location = new System.Drawing.Point(3, 324);
			this.txtExemptOrganization3Name.MaxLength = 75;
			this.txtExemptOrganization3Name.Name = "txtExemptOrganization3Name";
			this.txtExemptOrganization3Name.Size = new System.Drawing.Size(373, 40);
			this.txtExemptOrganization3Name.TabIndex = 84;
			// 
			// txtExemptOrganization2ExemptValue
			// 
			this.txtExemptOrganization2ExemptValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptOrganization2ExemptValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptOrganization2ExemptValue.Location = new System.Drawing.Point(588, 274);
			this.txtExemptOrganization2ExemptValue.MaxLength = 0;
			this.txtExemptOrganization2ExemptValue.Name = "txtExemptOrganization2ExemptValue";
			this.txtExemptOrganization2ExemptValue.Size = new System.Drawing.Size(100, 40);
			this.txtExemptOrganization2ExemptValue.TabIndex = 83;
			this.txtExemptOrganization2ExemptValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptSolarAndWindApproved
			// 
			this.txtExemptSolarAndWindApproved.BackColor = System.Drawing.Color.FromName("@control");
			this.txtExemptSolarAndWindApproved.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptSolarAndWindApproved.Focusable = false;
			this.txtExemptSolarAndWindApproved.Location = new System.Drawing.Point(588, 70);
			this.txtExemptSolarAndWindApproved.LockedOriginal = true;
			this.txtExemptSolarAndWindApproved.MaxLength = 0;
			this.txtExemptSolarAndWindApproved.Name = "txtExemptSolarAndWindApproved";
			this.txtExemptSolarAndWindApproved.ReadOnly = true;
			this.txtExemptSolarAndWindApproved.Size = new System.Drawing.Size(100, 40);
			this.txtExemptSolarAndWindApproved.TabIndex = 76;
			this.txtExemptSolarAndWindApproved.Text = "0";
			this.txtExemptSolarAndWindApproved.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtExemptProvisionOfLaw2
			// 
			this.txtExemptProvisionOfLaw2.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptProvisionOfLaw2.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptProvisionOfLaw2.Location = new System.Drawing.Point(391, 274);
			this.txtExemptProvisionOfLaw2.MaxLength = 40;
			this.txtExemptProvisionOfLaw2.Name = "txtExemptProvisionOfLaw2";
			this.txtExemptProvisionOfLaw2.Size = new System.Drawing.Size(182, 40);
			this.txtExemptProvisionOfLaw2.TabIndex = 82;
			// 
			// txtExemptOrganization2Name
			// 
			this.txtExemptOrganization2Name.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptOrganization2Name.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptOrganization2Name.Location = new System.Drawing.Point(3, 274);
			this.txtExemptOrganization2Name.MaxLength = 75;
			this.txtExemptOrganization2Name.Name = "txtExemptOrganization2Name";
			this.txtExemptOrganization2Name.Size = new System.Drawing.Size(373, 40);
			this.txtExemptOrganization2Name.TabIndex = 81;
			// 
			// fcLabel149
			// 
			this.fcLabel149.AutoSize = true;
			this.fcLabel149.Location = new System.Drawing.Point(6, 82);
			this.fcLabel149.Name = "fcLabel149";
			this.fcLabel149.Size = new System.Drawing.Size(482, 15);
			this.fcLabel149.TabIndex = 86;
			this.fcLabel149.Text = "40U(2) - SOLAR AND WIND ENERGY EQUIPMENT APPLICATIONS APPROVED";
			this.fcLabel149.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcLabel113
			// 
			this.fcLabel113.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fcLabel113.AutoSize = true;
			this.fcLabel113.Location = new System.Drawing.Point(588, 192);
			this.fcLabel113.Name = "fcLabel113";
			this.fcLabel113.Size = new System.Drawing.Size(57, 29);
			this.fcLabel113.TabIndex = 81;
			this.fcLabel113.Text = "EXEMPT VALUE";
			this.fcLabel113.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcLabel112
			// 
			this.fcLabel112.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fcLabel112.AutoSize = true;
			this.fcLabel112.Location = new System.Drawing.Point(414, 207);
			this.fcLabel112.Name = "fcLabel112";
			this.fcLabel112.Size = new System.Drawing.Size(129, 15);
			this.fcLabel112.TabIndex = 80;
			this.fcLabel112.Text = "PROVISION OF LAW";
			this.fcLabel112.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptSolarAndWindProcessed
			// 
			this.txtExemptSolarAndWindProcessed.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptSolarAndWindProcessed.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptSolarAndWindProcessed.Location = new System.Drawing.Point(588, 20);
			this.txtExemptSolarAndWindProcessed.MaxLength = 0;
			this.txtExemptSolarAndWindProcessed.Name = "txtExemptSolarAndWindProcessed";
			this.txtExemptSolarAndWindProcessed.Size = new System.Drawing.Size(100, 40);
			this.txtExemptSolarAndWindProcessed.TabIndex = 75;
			this.txtExemptSolarAndWindProcessed.Text = "0";
			this.txtExemptSolarAndWindProcessed.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel97
			// 
			this.fcLabel97.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fcLabel97.AutoSize = true;
			this.fcLabel97.Location = new System.Drawing.Point(102, 207);
			this.fcLabel97.Name = "fcLabel97";
			this.fcLabel97.Size = new System.Drawing.Size(165, 15);
			this.fcLabel97.TabIndex = 79;
			this.fcLabel97.Text = "NAME OF ORGANIZATION";
			this.fcLabel97.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptOrganization1ExemptValue
			// 
			this.txtExemptOrganization1ExemptValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptOrganization1ExemptValue.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptOrganization1ExemptValue.Location = new System.Drawing.Point(588, 224);
			this.txtExemptOrganization1ExemptValue.MaxLength = 0;
			this.txtExemptOrganization1ExemptValue.Name = "txtExemptOrganization1ExemptValue";
			this.txtExemptOrganization1ExemptValue.Size = new System.Drawing.Size(100, 40);
			this.txtExemptOrganization1ExemptValue.TabIndex = 80;
			this.txtExemptOrganization1ExemptValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel150
			// 
			this.fcLabel150.Location = new System.Drawing.Point(6, 18);
			this.fcLabel150.Name = "fcLabel150";
			this.fcLabel150.Size = new System.Drawing.Size(505, 40);
			this.fcLabel150.TabIndex = 84;
			this.fcLabel150.Text = "40U(1) - SOLAR AND WIND ENERGY EQUIPMENT APPLICATIONS PROCESSED";
			this.fcLabel150.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptProvisionOfLaw1
			// 
			this.txtExemptProvisionOfLaw1.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptProvisionOfLaw1.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptProvisionOfLaw1.Location = new System.Drawing.Point(391, 224);
			this.txtExemptProvisionOfLaw1.MaxLength = 40;
			this.txtExemptProvisionOfLaw1.Name = "txtExemptProvisionOfLaw1";
			this.txtExemptProvisionOfLaw1.Size = new System.Drawing.Size(182, 40);
			this.txtExemptProvisionOfLaw1.TabIndex = 79;
			// 
			// txtExemptOrganization1Name
			// 
			this.txtExemptOrganization1Name.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtExemptOrganization1Name.BackColor = System.Drawing.SystemColors.Window;
			this.txtExemptOrganization1Name.Cursor = Wisej.Web.Cursors.Default;
			this.txtExemptOrganization1Name.Location = new System.Drawing.Point(3, 224);
			this.txtExemptOrganization1Name.MaxLength = 75;
			this.txtExemptOrganization1Name.Name = "txtExemptOrganization1Name";
			this.txtExemptOrganization1Name.Size = new System.Drawing.Size(373, 40);
			this.txtExemptOrganization1Name.TabIndex = 78;
			// 
			// fcTabPage6
			// 
			this.fcTabPage6.AutoScroll = true;
			this.fcTabPage6.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage6.Controls.Add(this.fcFrame20);
			this.fcTabPage6.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage6.Name = "fcTabPage6";
			this.fcTabPage6.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage6.Text = "Page 8";
			// 
			// fcFrame20
			// 
			this.fcFrame20.Controls.Add(this.dtpSignatureDate);
			this.fcFrame20.Controls.Add(this.fcLabel155);
			this.fcFrame20.Controls.Add(this.txtAmountOfSeniorTaxDeferral);
			this.fcFrame20.Controls.Add(this.fcLabel151);
			this.fcFrame20.Controls.Add(this.txtNumberQualifiedForSeniorDeferral);
			this.fcFrame20.Controls.Add(this.fcLabel152);
			this.fcFrame20.Controls.Add(this.chkSeniorTaxDeferral);
			this.fcFrame20.Controls.Add(this.fcLabel153);
			this.fcFrame20.Controls.Add(this.txtAmountOfElderlyTaxCredit);
			this.fcFrame20.Controls.Add(this.fcLabel142);
			this.fcFrame20.Controls.Add(this.txtNumberQualifiedForElderlyTaxCredit);
			this.fcFrame20.Controls.Add(this.fcLabel143);
			this.fcFrame20.Controls.Add(this.chkElderlyTaxCredit);
			this.fcFrame20.Controls.Add(this.fcLabel144);
			this.fcFrame20.Controls.Add(this.txtAmountOfTaxRelief);
			this.fcFrame20.Controls.Add(this.fcLabel140);
			this.fcFrame20.Controls.Add(this.txtNumberQualifiedForTaxRelief);
			this.fcFrame20.Controls.Add(this.fcLabel141);
			this.fcFrame20.Controls.Add(this.chkTaxReliefProgram);
			this.fcFrame20.Controls.Add(this.fcLabel139);
			this.fcFrame20.Controls.Add(this.txtAssessmentSoftware);
			this.fcFrame20.Controls.Add(this.fcLabel138);
			this.fcFrame20.Controls.Add(this.chkAssessmentRecordsAreComputerized);
			this.fcFrame20.Controls.Add(this.fcLabel137);
			this.fcFrame20.Controls.Add(this.dtpPeriod4Due);
			this.fcFrame20.Controls.Add(this.fcLabel135);
			this.fcFrame20.Controls.Add(this.dtpPeriod3Due);
			this.fcFrame20.Controls.Add(this.fcLabel136);
			this.fcFrame20.Controls.Add(this.dtpPeriod2Due);
			this.fcFrame20.Controls.Add(this.fcLabel133);
			this.fcFrame20.Controls.Add(this.dtpPeriod1Due);
			this.fcFrame20.Controls.Add(this.fcLabel134);
			this.fcFrame20.Controls.Add(this.txtInterestRate);
			this.fcFrame20.Controls.Add(this.fcLabel132);
			this.fcFrame20.Controls.Add(this.dtpFiscalEnd);
			this.fcFrame20.Controls.Add(this.fcLabel131);
			this.fcFrame20.Controls.Add(this.dtpFiscalStart);
			this.fcFrame20.Controls.Add(this.fcLabel130);
			this.fcFrame20.Controls.Add(this.txtAssessorEmail);
			this.fcFrame20.Controls.Add(this.fcLabel129);
			this.fcFrame20.Controls.Add(this.txtAssessorName);
			this.fcFrame20.Controls.Add(this.fcLabel128);
			this.fcFrame20.Controls.Add(this.cmbAssessorFunctionality);
			this.fcFrame20.Controls.Add(this.fcLabel127);
			this.fcFrame20.Location = new System.Drawing.Point(14, 18);
			this.fcFrame20.Name = "fcFrame20";
			this.fcFrame20.Size = new System.Drawing.Size(696, 1125);
			this.fcFrame20.TabIndex = 0;
			this.fcFrame20.Text = "Municipal Records Continued";
			// 
			// dtpSignatureDate
			// 
			this.dtpSignatureDate.AutoSize = false;
			this.dtpSignatureDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpSignatureDate.Location = new System.Drawing.Point(394, 1075);
			this.dtpSignatureDate.Mask = "00/00/0000";
			this.dtpSignatureDate.Name = "dtpSignatureDate";
			this.dtpSignatureDate.Size = new System.Drawing.Size(166, 40);
			this.dtpSignatureDate.TabIndex = 125;
			// 
			// fcLabel155
			// 
			this.fcLabel155.Location = new System.Drawing.Point(6, 1075);
			this.fcLabel155.Name = "fcLabel155";
			this.fcLabel155.Size = new System.Drawing.Size(373, 40);
			this.fcLabel155.TabIndex = 124;
			this.fcLabel155.Text = "SIGNATURE DATE";
			this.fcLabel155.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAmountOfSeniorTaxDeferral
			// 
			this.txtAmountOfSeniorTaxDeferral.BackColor = System.Drawing.SystemColors.Window;
			this.txtAmountOfSeniorTaxDeferral.Cursor = Wisej.Web.Cursors.Default;
			this.txtAmountOfSeniorTaxDeferral.Location = new System.Drawing.Point(394, 1025);
			this.txtAmountOfSeniorTaxDeferral.MaxLength = 0;
			this.txtAmountOfSeniorTaxDeferral.Name = "txtAmountOfSeniorTaxDeferral";
			this.txtAmountOfSeniorTaxDeferral.Size = new System.Drawing.Size(103, 40);
			this.txtAmountOfSeniorTaxDeferral.TabIndex = 123;
			this.txtAmountOfSeniorTaxDeferral.Text = "0";
			this.txtAmountOfSeniorTaxDeferral.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel151
			// 
			this.fcLabel151.Location = new System.Drawing.Point(6, 1025);
			this.fcLabel151.Name = "fcLabel151";
			this.fcLabel151.Size = new System.Drawing.Size(382, 40);
			this.fcLabel151.TabIndex = 122;
			this.fcLabel151.Text = "52C - AMOUNT OF RELIEF GRANTED";
			this.fcLabel151.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNumberQualifiedForSeniorDeferral
			// 
			this.txtNumberQualifiedForSeniorDeferral.BackColor = System.Drawing.SystemColors.Window;
			this.txtNumberQualifiedForSeniorDeferral.Cursor = Wisej.Web.Cursors.Default;
			this.txtNumberQualifiedForSeniorDeferral.Location = new System.Drawing.Point(394, 975);
			this.txtNumberQualifiedForSeniorDeferral.MaxLength = 0;
			this.txtNumberQualifiedForSeniorDeferral.Name = "txtNumberQualifiedForSeniorDeferral";
			this.txtNumberQualifiedForSeniorDeferral.Size = new System.Drawing.Size(103, 40);
			this.txtNumberQualifiedForSeniorDeferral.TabIndex = 121;
			this.txtNumberQualifiedForSeniorDeferral.Text = "0";
			this.txtNumberQualifiedForSeniorDeferral.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel152
			// 
			this.fcLabel152.Location = new System.Drawing.Point(6, 975);
			this.fcLabel152.Name = "fcLabel152";
			this.fcLabel152.Size = new System.Drawing.Size(373, 40);
			this.fcLabel152.TabIndex = 120;
			this.fcLabel152.Text = "52B - NUMBER OF PEOPLE WHO QUALIFIED";
			this.fcLabel152.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkSeniorTaxDeferral
			// 
			this.chkSeniorTaxDeferral.AutoSize = false;
			this.chkSeniorTaxDeferral.Location = new System.Drawing.Point(394, 925);
			this.chkSeniorTaxDeferral.Name = "chkSeniorTaxDeferral";
			this.chkSeniorTaxDeferral.Size = new System.Drawing.Size(50, 39);
			this.chkSeniorTaxDeferral.TabIndex = 119;
			// 
			// txtAmountOfElderlyTaxCredit
			// 
			this.txtAmountOfElderlyTaxCredit.BackColor = System.Drawing.SystemColors.Window;
			this.txtAmountOfElderlyTaxCredit.Cursor = Wisej.Web.Cursors.Default;
			this.txtAmountOfElderlyTaxCredit.Location = new System.Drawing.Point(394, 875);
			this.txtAmountOfElderlyTaxCredit.MaxLength = 0;
			this.txtAmountOfElderlyTaxCredit.Name = "txtAmountOfElderlyTaxCredit";
			this.txtAmountOfElderlyTaxCredit.Size = new System.Drawing.Size(103, 40);
			this.txtAmountOfElderlyTaxCredit.TabIndex = 117;
			this.txtAmountOfElderlyTaxCredit.Text = "0";
			this.txtAmountOfElderlyTaxCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel142
			// 
			this.fcLabel142.Location = new System.Drawing.Point(6, 875);
			this.fcLabel142.Name = "fcLabel142";
			this.fcLabel142.Size = new System.Drawing.Size(382, 40);
			this.fcLabel142.TabIndex = 116;
			this.fcLabel142.Text = "51C - AMOUNT OF RELIEF GRANTED";
			this.fcLabel142.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNumberQualifiedForElderlyTaxCredit
			// 
			this.txtNumberQualifiedForElderlyTaxCredit.BackColor = System.Drawing.SystemColors.Window;
			this.txtNumberQualifiedForElderlyTaxCredit.Cursor = Wisej.Web.Cursors.Default;
			this.txtNumberQualifiedForElderlyTaxCredit.Location = new System.Drawing.Point(394, 825);
			this.txtNumberQualifiedForElderlyTaxCredit.MaxLength = 0;
			this.txtNumberQualifiedForElderlyTaxCredit.Name = "txtNumberQualifiedForElderlyTaxCredit";
			this.txtNumberQualifiedForElderlyTaxCredit.Size = new System.Drawing.Size(103, 40);
			this.txtNumberQualifiedForElderlyTaxCredit.TabIndex = 115;
			this.txtNumberQualifiedForElderlyTaxCredit.Text = "0";
			this.txtNumberQualifiedForElderlyTaxCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel143
			// 
			this.fcLabel143.Location = new System.Drawing.Point(6, 825);
			this.fcLabel143.Name = "fcLabel143";
			this.fcLabel143.Size = new System.Drawing.Size(373, 40);
			this.fcLabel143.TabIndex = 114;
			this.fcLabel143.Text = "51B - NUMBER OF PEOPLE WHO QUALIFIED";
			this.fcLabel143.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkElderlyTaxCredit
			// 
			this.chkElderlyTaxCredit.AutoSize = false;
			this.chkElderlyTaxCredit.Location = new System.Drawing.Point(394, 775);
			this.chkElderlyTaxCredit.Name = "chkElderlyTaxCredit";
			this.chkElderlyTaxCredit.Size = new System.Drawing.Size(50, 39);
			this.chkElderlyTaxCredit.TabIndex = 113;
			// 
			// txtAmountOfTaxRelief
			// 
			this.txtAmountOfTaxRelief.BackColor = System.Drawing.SystemColors.Window;
			this.txtAmountOfTaxRelief.Cursor = Wisej.Web.Cursors.Default;
			this.txtAmountOfTaxRelief.Location = new System.Drawing.Point(394, 725);
			this.txtAmountOfTaxRelief.MaxLength = 0;
			this.txtAmountOfTaxRelief.Name = "txtAmountOfTaxRelief";
			this.txtAmountOfTaxRelief.Size = new System.Drawing.Size(103, 40);
			this.txtAmountOfTaxRelief.TabIndex = 111;
			this.txtAmountOfTaxRelief.Text = "0";
			this.txtAmountOfTaxRelief.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel140
			// 
			this.fcLabel140.Location = new System.Drawing.Point(6, 725);
			this.fcLabel140.Name = "fcLabel140";
			this.fcLabel140.Size = new System.Drawing.Size(382, 40);
			this.fcLabel140.TabIndex = 110;
			this.fcLabel140.Text = "50C - AMOUNT OF RELIEF GRANTED";
			this.fcLabel140.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNumberQualifiedForTaxRelief
			// 
			this.txtNumberQualifiedForTaxRelief.BackColor = System.Drawing.SystemColors.Window;
			this.txtNumberQualifiedForTaxRelief.Cursor = Wisej.Web.Cursors.Default;
			this.txtNumberQualifiedForTaxRelief.Location = new System.Drawing.Point(394, 675);
			this.txtNumberQualifiedForTaxRelief.MaxLength = 0;
			this.txtNumberQualifiedForTaxRelief.Name = "txtNumberQualifiedForTaxRelief";
			this.txtNumberQualifiedForTaxRelief.Size = new System.Drawing.Size(103, 40);
			this.txtNumberQualifiedForTaxRelief.TabIndex = 109;
			this.txtNumberQualifiedForTaxRelief.Text = "0";
			this.txtNumberQualifiedForTaxRelief.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel141
			// 
			this.fcLabel141.Location = new System.Drawing.Point(6, 679);
			this.fcLabel141.Name = "fcLabel141";
			this.fcLabel141.Size = new System.Drawing.Size(373, 40);
			this.fcLabel141.TabIndex = 108;
			this.fcLabel141.Text = "50B - NUMBER OF PEOPLE WHO QUALIFIED";
			this.fcLabel141.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkTaxReliefProgram
			// 
			this.chkTaxReliefProgram.AutoSize = false;
			this.chkTaxReliefProgram.Location = new System.Drawing.Point(394, 625);
			this.chkTaxReliefProgram.Name = "chkTaxReliefProgram";
			this.chkTaxReliefProgram.Size = new System.Drawing.Size(50, 39);
			this.chkTaxReliefProgram.TabIndex = 107;
			// 
			// txtAssessmentSoftware
			// 
			this.txtAssessmentSoftware.BackColor = System.Drawing.SystemColors.Window;
			this.txtAssessmentSoftware.Cursor = Wisej.Web.Cursors.Default;
			this.txtAssessmentSoftware.Location = new System.Drawing.Point(394, 575);
			this.txtAssessmentSoftware.MaxLength = 75;
			this.txtAssessmentSoftware.Name = "txtAssessmentSoftware";
			this.txtAssessmentSoftware.Size = new System.Drawing.Size(197, 40);
			this.txtAssessmentSoftware.TabIndex = 105;
			// 
			// fcLabel138
			// 
			this.fcLabel138.Location = new System.Drawing.Point(6, 575);
			this.fcLabel138.Name = "fcLabel138";
			this.fcLabel138.Size = new System.Drawing.Size(373, 40);
			this.fcLabel138.TabIndex = 104;
			this.fcLabel138.Text = "49B - ASSESSMENT SOFTWARE USED";
			this.fcLabel138.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkAssessmentRecordsAreComputerized
			// 
			this.chkAssessmentRecordsAreComputerized.AutoSize = false;
			this.chkAssessmentRecordsAreComputerized.Location = new System.Drawing.Point(394, 525);
			this.chkAssessmentRecordsAreComputerized.Name = "chkAssessmentRecordsAreComputerized";
			this.chkAssessmentRecordsAreComputerized.Size = new System.Drawing.Size(50, 39);
			this.chkAssessmentRecordsAreComputerized.TabIndex = 103;
			// 
			// fcLabel137
			// 
			this.fcLabel137.Location = new System.Drawing.Point(6, 525);
			this.fcLabel137.Name = "fcLabel137";
			this.fcLabel137.Size = new System.Drawing.Size(373, 40);
			this.fcLabel137.TabIndex = 102;
			this.fcLabel137.Text = "49A - ASSESSMENT RECORDS ARE COMPUTERIZED";
			this.fcLabel137.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpPeriod4Due
			// 
			this.dtpPeriod4Due.AutoSize = false;
			this.dtpPeriod4Due.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpPeriod4Due.Location = new System.Drawing.Point(394, 475);
			this.dtpPeriod4Due.Mask = "00/00/0000";
			this.dtpPeriod4Due.Name = "dtpPeriod4Due";
			this.dtpPeriod4Due.Size = new System.Drawing.Size(166, 40);
			this.dtpPeriod4Due.TabIndex = 101;
			// 
			// fcLabel135
			// 
			this.fcLabel135.Location = new System.Drawing.Point(6, 475);
			this.fcLabel135.Name = "fcLabel135";
			this.fcLabel135.Size = new System.Drawing.Size(373, 40);
			this.fcLabel135.TabIndex = 100;
			this.fcLabel135.Text = "48D - PROPERTY TAX PERIOD 4 DUE";
			this.fcLabel135.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpPeriod3Due
			// 
			this.dtpPeriod3Due.AutoSize = false;
			this.dtpPeriod3Due.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpPeriod3Due.Location = new System.Drawing.Point(394, 425);
			this.dtpPeriod3Due.Mask = "00/00/0000";
			this.dtpPeriod3Due.Name = "dtpPeriod3Due";
			this.dtpPeriod3Due.Size = new System.Drawing.Size(166, 40);
			this.dtpPeriod3Due.TabIndex = 99;
			// 
			// fcLabel136
			// 
			this.fcLabel136.Location = new System.Drawing.Point(6, 425);
			this.fcLabel136.Name = "fcLabel136";
			this.fcLabel136.Size = new System.Drawing.Size(373, 40);
			this.fcLabel136.TabIndex = 98;
			this.fcLabel136.Text = "48C - PROPERTY TAX PERIOD 3 DUE";
			this.fcLabel136.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpPeriod2Due
			// 
			this.dtpPeriod2Due.AutoSize = false;
			this.dtpPeriod2Due.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpPeriod2Due.Location = new System.Drawing.Point(394, 375);
			this.dtpPeriod2Due.Mask = "00/00/0000";
			this.dtpPeriod2Due.Name = "dtpPeriod2Due";
			this.dtpPeriod2Due.Size = new System.Drawing.Size(166, 40);
			this.dtpPeriod2Due.TabIndex = 97;
			// 
			// fcLabel133
			// 
			this.fcLabel133.Location = new System.Drawing.Point(6, 375);
			this.fcLabel133.Name = "fcLabel133";
			this.fcLabel133.Size = new System.Drawing.Size(373, 40);
			this.fcLabel133.TabIndex = 96;
			this.fcLabel133.Text = "48B - PROPERTY TAX PERIOD 2 DUE";
			this.fcLabel133.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpPeriod1Due
			// 
			this.dtpPeriod1Due.AutoSize = false;
			this.dtpPeriod1Due.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpPeriod1Due.Location = new System.Drawing.Point(394, 325);
			this.dtpPeriod1Due.Mask = "00/00/0000";
			this.dtpPeriod1Due.Name = "dtpPeriod1Due";
			this.dtpPeriod1Due.Size = new System.Drawing.Size(166, 40);
			this.dtpPeriod1Due.TabIndex = 95;
			// 
			// fcLabel134
			// 
			this.fcLabel134.Location = new System.Drawing.Point(6, 325);
			this.fcLabel134.Name = "fcLabel134";
			this.fcLabel134.Size = new System.Drawing.Size(373, 40);
			this.fcLabel134.TabIndex = 94;
			this.fcLabel134.Text = "48A - PROPERTY TAX PERIOD 1 DUE";
			this.fcLabel134.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtInterestRate
			// 
			this.txtInterestRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtInterestRate.Cursor = Wisej.Web.Cursors.Default;
			this.txtInterestRate.Location = new System.Drawing.Point(394, 275);
			this.txtInterestRate.MaxLength = 0;
			this.txtInterestRate.Name = "txtInterestRate";
			this.txtInterestRate.Size = new System.Drawing.Size(86, 40);
			this.txtInterestRate.TabIndex = 93;
			this.txtInterestRate.Text = "0";
			this.txtInterestRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fcLabel132
			// 
			this.fcLabel132.Location = new System.Drawing.Point(6, 275);
			this.fcLabel132.Name = "fcLabel132";
			this.fcLabel132.Size = new System.Drawing.Size(373, 40);
			this.fcLabel132.TabIndex = 92;
			this.fcLabel132.Text = "47 - INTEREST RATE ON OVERDUE PROPERTY TAXES";
			this.fcLabel132.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpFiscalEnd
			// 
			this.dtpFiscalEnd.AutoSize = false;
			this.dtpFiscalEnd.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpFiscalEnd.Location = new System.Drawing.Point(394, 225);
			this.dtpFiscalEnd.Mask = "00/00/0000";
			this.dtpFiscalEnd.Name = "dtpFiscalEnd";
			this.dtpFiscalEnd.Size = new System.Drawing.Size(166, 40);
			this.dtpFiscalEnd.TabIndex = 91;
			// 
			// fcLabel131
			// 
			this.fcLabel131.Location = new System.Drawing.Point(6, 225);
			this.fcLabel131.Name = "fcLabel131";
			this.fcLabel131.Size = new System.Drawing.Size(373, 40);
			this.fcLabel131.TabIndex = 90;
			this.fcLabel131.Text = "46B - FISCAL YEAR END";
			this.fcLabel131.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpFiscalStart
			// 
			this.dtpFiscalStart.AutoSize = false;
			this.dtpFiscalStart.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpFiscalStart.Location = new System.Drawing.Point(394, 175);
			this.dtpFiscalStart.Mask = "00/00/0000";
			this.dtpFiscalStart.Name = "dtpFiscalStart";
			this.dtpFiscalStart.Size = new System.Drawing.Size(166, 40);
			this.dtpFiscalStart.TabIndex = 89;
			// 
			// fcLabel130
			// 
			this.fcLabel130.Location = new System.Drawing.Point(6, 175);
			this.fcLabel130.Name = "fcLabel130";
			this.fcLabel130.Size = new System.Drawing.Size(373, 40);
			this.fcLabel130.TabIndex = 88;
			this.fcLabel130.Text = "46A - FISCAL YEAR START";
			this.fcLabel130.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAssessorEmail
			// 
			this.txtAssessorEmail.BackColor = System.Drawing.SystemColors.Window;
			this.txtAssessorEmail.Cursor = Wisej.Web.Cursors.Default;
			this.txtAssessorEmail.Location = new System.Drawing.Point(394, 125);
			this.txtAssessorEmail.MaxLength = 100;
			this.txtAssessorEmail.Name = "txtAssessorEmail";
			this.txtAssessorEmail.Size = new System.Drawing.Size(197, 40);
			this.txtAssessorEmail.TabIndex = 87;
			// 
			// fcLabel129
			// 
			this.fcLabel129.Location = new System.Drawing.Point(6, 125);
			this.fcLabel129.Name = "fcLabel129";
			this.fcLabel129.Size = new System.Drawing.Size(373, 40);
			this.fcLabel129.TabIndex = 86;
			this.fcLabel129.Text = "45C - EMAIL ADDRESS";
			this.fcLabel129.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAssessorName
			// 
			this.txtAssessorName.BackColor = System.Drawing.SystemColors.Window;
			this.txtAssessorName.Cursor = Wisej.Web.Cursors.Default;
			this.txtAssessorName.Location = new System.Drawing.Point(394, 75);
			this.txtAssessorName.MaxLength = 75;
			this.txtAssessorName.Name = "txtAssessorName";
			this.txtAssessorName.Size = new System.Drawing.Size(197, 40);
			this.txtAssessorName.TabIndex = 85;
			// 
			// fcLabel128
			// 
			this.fcLabel128.Location = new System.Drawing.Point(6, 75);
			this.fcLabel128.Name = "fcLabel128";
			this.fcLabel128.Size = new System.Drawing.Size(373, 40);
			this.fcLabel128.TabIndex = 84;
			this.fcLabel128.Text = "45B - ASSESSOR OR AGENT NAME";
			this.fcLabel128.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbAssessorFunctionality
			// 
			this.cmbAssessorFunctionality.Location = new System.Drawing.Point(394, 25);
			this.cmbAssessorFunctionality.Name = "cmbAssessorFunctionality";
			this.cmbAssessorFunctionality.Size = new System.Drawing.Size(197, 40);
			this.cmbAssessorFunctionality.TabIndex = 83;
			// 
			// fcLabel127
			// 
			this.fcLabel127.Location = new System.Drawing.Point(6, 25);
			this.fcLabel127.Name = "fcLabel127";
			this.fcLabel127.Size = new System.Drawing.Size(373, 40);
			this.fcLabel127.TabIndex = 82;
			this.fcLabel127.Text = "45A - MUNICIPALITY ASSESSMENT FUNCTION";
			this.fcLabel127.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcTabPage7
			// 
			this.fcTabPage7.AutoScroll = true;
			this.fcTabPage7.AutoScrollMargin = new System.Drawing.Size(0, 6);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine10);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine9);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine8);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine7);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine6);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine5);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine4);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine3);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine2);
			this.fcTabPage7.Controls.Add(this.txtGeneralChangeLine1);
			this.fcTabPage7.Controls.Add(this.fcLabel147);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine8);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine7);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine6);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine5);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine4);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine3);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine2);
			this.fcTabPage7.Controls.Add(this.txtExtremeLossesLine1);
			this.fcTabPage7.Controls.Add(this.fcLabel146);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine8);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine7);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine6);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine5);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine4);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine3);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine2);
			this.fcTabPage7.Controls.Add(this.txtIndustrialGrowthLine1);
			this.fcTabPage7.Controls.Add(this.fcLabel145);
			this.fcTabPage7.Controls.Add(this.GridResidentialBuildings);
			this.fcTabPage7.Location = new System.Drawing.Point(1, 35);
			this.fcTabPage7.Name = "fcTabPage7";
			this.fcTabPage7.Size = new System.Drawing.Size(966, 564);
			this.fcTabPage7.Text = "Page 9";
			// 
			// txtGeneralChangeLine10
			// 
			this.txtGeneralChangeLine10.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine10.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine10.Location = new System.Drawing.Point(23, 1783);
			this.txtGeneralChangeLine10.MaxLength = 0;
			this.txtGeneralChangeLine10.Name = "txtGeneralChangeLine10";
			this.txtGeneralChangeLine10.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine10.TabIndex = 131;
			// 
			// txtGeneralChangeLine9
			// 
			this.txtGeneralChangeLine9.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine9.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine9.Location = new System.Drawing.Point(23, 1733);
			this.txtGeneralChangeLine9.MaxLength = 0;
			this.txtGeneralChangeLine9.Name = "txtGeneralChangeLine9";
			this.txtGeneralChangeLine9.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine9.TabIndex = 130;
			// 
			// txtGeneralChangeLine8
			// 
			this.txtGeneralChangeLine8.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine8.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine8.Location = new System.Drawing.Point(23, 1683);
			this.txtGeneralChangeLine8.MaxLength = 0;
			this.txtGeneralChangeLine8.Name = "txtGeneralChangeLine8";
			this.txtGeneralChangeLine8.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine8.TabIndex = 129;
			// 
			// txtGeneralChangeLine7
			// 
			this.txtGeneralChangeLine7.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine7.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine7.Location = new System.Drawing.Point(23, 1633);
			this.txtGeneralChangeLine7.MaxLength = 0;
			this.txtGeneralChangeLine7.Name = "txtGeneralChangeLine7";
			this.txtGeneralChangeLine7.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine7.TabIndex = 128;
			// 
			// txtGeneralChangeLine6
			// 
			this.txtGeneralChangeLine6.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine6.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine6.Location = new System.Drawing.Point(23, 1583);
			this.txtGeneralChangeLine6.MaxLength = 0;
			this.txtGeneralChangeLine6.Name = "txtGeneralChangeLine6";
			this.txtGeneralChangeLine6.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine6.TabIndex = 127;
			// 
			// txtGeneralChangeLine5
			// 
			this.txtGeneralChangeLine5.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine5.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine5.Location = new System.Drawing.Point(23, 1533);
			this.txtGeneralChangeLine5.MaxLength = 0;
			this.txtGeneralChangeLine5.Name = "txtGeneralChangeLine5";
			this.txtGeneralChangeLine5.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine5.TabIndex = 126;
			// 
			// txtGeneralChangeLine4
			// 
			this.txtGeneralChangeLine4.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine4.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine4.Location = new System.Drawing.Point(23, 1483);
			this.txtGeneralChangeLine4.MaxLength = 0;
			this.txtGeneralChangeLine4.Name = "txtGeneralChangeLine4";
			this.txtGeneralChangeLine4.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine4.TabIndex = 125;
			// 
			// txtGeneralChangeLine3
			// 
			this.txtGeneralChangeLine3.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine3.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine3.Location = new System.Drawing.Point(23, 1433);
			this.txtGeneralChangeLine3.MaxLength = 0;
			this.txtGeneralChangeLine3.Name = "txtGeneralChangeLine3";
			this.txtGeneralChangeLine3.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine3.TabIndex = 124;
			// 
			// txtGeneralChangeLine2
			// 
			this.txtGeneralChangeLine2.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine2.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine2.Location = new System.Drawing.Point(23, 1383);
			this.txtGeneralChangeLine2.MaxLength = 0;
			this.txtGeneralChangeLine2.Name = "txtGeneralChangeLine2";
			this.txtGeneralChangeLine2.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine2.TabIndex = 123;
			// 
			// txtGeneralChangeLine1
			// 
			this.txtGeneralChangeLine1.BackColor = System.Drawing.SystemColors.Window;
			this.txtGeneralChangeLine1.Cursor = Wisej.Web.Cursors.Default;
			this.txtGeneralChangeLine1.Location = new System.Drawing.Point(23, 1333);
			this.txtGeneralChangeLine1.MaxLength = 0;
			this.txtGeneralChangeLine1.Name = "txtGeneralChangeLine1";
			this.txtGeneralChangeLine1.Size = new System.Drawing.Size(658, 40);
			this.txtGeneralChangeLine1.TabIndex = 122;
			// 
			// fcLabel147
			// 
			this.fcLabel147.Location = new System.Drawing.Point(23, 1280);
			this.fcLabel147.Name = "fcLabel147";
			this.fcLabel147.Size = new System.Drawing.Size(669, 40);
			this.fcLabel147.TabIndex = 121;
			this.fcLabel147.Text = "EXPLAIN ANY GENERAL INCREASE OR DECREASE IN VALUATION SINCE APRIL 1 SUCH AS REVAL" +
    "UATIONS, CHANGE IN RATIO USED, ADJUSTMENTS ETC.";
			this.fcLabel147.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExtremeLossesLine8
			// 
			this.txtExtremeLossesLine8.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine8.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine8.Location = new System.Drawing.Point(23, 1198);
			this.txtExtremeLossesLine8.MaxLength = 0;
			this.txtExtremeLossesLine8.Name = "txtExtremeLossesLine8";
			this.txtExtremeLossesLine8.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine8.TabIndex = 120;
			// 
			// txtExtremeLossesLine7
			// 
			this.txtExtremeLossesLine7.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine7.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine7.Location = new System.Drawing.Point(23, 1148);
			this.txtExtremeLossesLine7.MaxLength = 0;
			this.txtExtremeLossesLine7.Name = "txtExtremeLossesLine7";
			this.txtExtremeLossesLine7.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine7.TabIndex = 119;
			// 
			// txtExtremeLossesLine6
			// 
			this.txtExtremeLossesLine6.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine6.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine6.Location = new System.Drawing.Point(23, 1098);
			this.txtExtremeLossesLine6.MaxLength = 0;
			this.txtExtremeLossesLine6.Name = "txtExtremeLossesLine6";
			this.txtExtremeLossesLine6.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine6.TabIndex = 118;
			// 
			// txtExtremeLossesLine5
			// 
			this.txtExtremeLossesLine5.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine5.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine5.Location = new System.Drawing.Point(23, 1048);
			this.txtExtremeLossesLine5.MaxLength = 0;
			this.txtExtremeLossesLine5.Name = "txtExtremeLossesLine5";
			this.txtExtremeLossesLine5.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine5.TabIndex = 117;
			// 
			// txtExtremeLossesLine4
			// 
			this.txtExtremeLossesLine4.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine4.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine4.Location = new System.Drawing.Point(23, 998);
			this.txtExtremeLossesLine4.MaxLength = 0;
			this.txtExtremeLossesLine4.Name = "txtExtremeLossesLine4";
			this.txtExtremeLossesLine4.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine4.TabIndex = 116;
			// 
			// txtExtremeLossesLine3
			// 
			this.txtExtremeLossesLine3.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine3.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine3.Location = new System.Drawing.Point(23, 948);
			this.txtExtremeLossesLine3.MaxLength = 0;
			this.txtExtremeLossesLine3.Name = "txtExtremeLossesLine3";
			this.txtExtremeLossesLine3.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine3.TabIndex = 115;
			// 
			// txtExtremeLossesLine2
			// 
			this.txtExtremeLossesLine2.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine2.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine2.Location = new System.Drawing.Point(23, 898);
			this.txtExtremeLossesLine2.MaxLength = 0;
			this.txtExtremeLossesLine2.Name = "txtExtremeLossesLine2";
			this.txtExtremeLossesLine2.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine2.TabIndex = 114;
			// 
			// txtExtremeLossesLine1
			// 
			this.txtExtremeLossesLine1.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtremeLossesLine1.Cursor = Wisej.Web.Cursors.Default;
			this.txtExtremeLossesLine1.Location = new System.Drawing.Point(23, 848);
			this.txtExtremeLossesLine1.MaxLength = 0;
			this.txtExtremeLossesLine1.Name = "txtExtremeLossesLine1";
			this.txtExtremeLossesLine1.Size = new System.Drawing.Size(658, 40);
			this.txtExtremeLossesLine1.TabIndex = 113;
			// 
			// fcLabel146
			// 
			this.fcLabel146.Location = new System.Drawing.Point(23, 795);
			this.fcLabel146.Name = "fcLabel146";
			this.fcLabel146.Size = new System.Drawing.Size(669, 40);
			this.fcLabel146.TabIndex = 112;
			this.fcLabel146.Text = "LIST ANY EXTREME LOSSES IN VALUATION SINCE APRIL 1 GIVING A BRIEF EXPLANATION SUC" +
    "H AS \"FIRE\" OR \"MILL CLOSING\", ETC. GIVING THE LOSS AT FULL MARKET VALUE";
			this.fcLabel146.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtIndustrialGrowthLine8
			// 
			this.txtIndustrialGrowthLine8.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine8.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine8.Location = new System.Drawing.Point(24, 726);
			this.txtIndustrialGrowthLine8.MaxLength = 0;
			this.txtIndustrialGrowthLine8.Name = "txtIndustrialGrowthLine8";
			this.txtIndustrialGrowthLine8.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine8.TabIndex = 111;
			// 
			// txtIndustrialGrowthLine7
			// 
			this.txtIndustrialGrowthLine7.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine7.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine7.Location = new System.Drawing.Point(24, 676);
			this.txtIndustrialGrowthLine7.MaxLength = 0;
			this.txtIndustrialGrowthLine7.Name = "txtIndustrialGrowthLine7";
			this.txtIndustrialGrowthLine7.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine7.TabIndex = 110;
			// 
			// txtIndustrialGrowthLine6
			// 
			this.txtIndustrialGrowthLine6.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine6.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine6.Location = new System.Drawing.Point(24, 626);
			this.txtIndustrialGrowthLine6.MaxLength = 0;
			this.txtIndustrialGrowthLine6.Name = "txtIndustrialGrowthLine6";
			this.txtIndustrialGrowthLine6.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine6.TabIndex = 109;
			// 
			// txtIndustrialGrowthLine5
			// 
			this.txtIndustrialGrowthLine5.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine5.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine5.Location = new System.Drawing.Point(24, 576);
			this.txtIndustrialGrowthLine5.MaxLength = 0;
			this.txtIndustrialGrowthLine5.Name = "txtIndustrialGrowthLine5";
			this.txtIndustrialGrowthLine5.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine5.TabIndex = 108;
			// 
			// txtIndustrialGrowthLine4
			// 
			this.txtIndustrialGrowthLine4.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine4.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine4.Location = new System.Drawing.Point(24, 526);
			this.txtIndustrialGrowthLine4.MaxLength = 0;
			this.txtIndustrialGrowthLine4.Name = "txtIndustrialGrowthLine4";
			this.txtIndustrialGrowthLine4.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine4.TabIndex = 107;
			// 
			// txtIndustrialGrowthLine3
			// 
			this.txtIndustrialGrowthLine3.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine3.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine3.Location = new System.Drawing.Point(24, 476);
			this.txtIndustrialGrowthLine3.MaxLength = 0;
			this.txtIndustrialGrowthLine3.Name = "txtIndustrialGrowthLine3";
			this.txtIndustrialGrowthLine3.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine3.TabIndex = 106;
			// 
			// txtIndustrialGrowthLine2
			// 
			this.txtIndustrialGrowthLine2.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine2.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine2.Location = new System.Drawing.Point(24, 426);
			this.txtIndustrialGrowthLine2.MaxLength = 0;
			this.txtIndustrialGrowthLine2.Name = "txtIndustrialGrowthLine2";
			this.txtIndustrialGrowthLine2.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine2.TabIndex = 105;
			// 
			// txtIndustrialGrowthLine1
			// 
			this.txtIndustrialGrowthLine1.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndustrialGrowthLine1.Cursor = Wisej.Web.Cursors.Default;
			this.txtIndustrialGrowthLine1.Location = new System.Drawing.Point(24, 376);
			this.txtIndustrialGrowthLine1.MaxLength = 0;
			this.txtIndustrialGrowthLine1.Name = "txtIndustrialGrowthLine1";
			this.txtIndustrialGrowthLine1.Size = new System.Drawing.Size(658, 40);
			this.txtIndustrialGrowthLine1.TabIndex = 104;
			// 
			// fcLabel145
			// 
			this.fcLabel145.Location = new System.Drawing.Point(24, 323);
			this.fcLabel145.Name = "fcLabel145";
			this.fcLabel145.Size = new System.Drawing.Size(669, 40);
			this.fcLabel145.TabIndex = 103;
			this.fcLabel145.Text = "LIST NEW INDUSTRIAL OR MERCANTILE GROWTH STARTED OR EXPANDED SINCE APRIL 1 GIVING" +
    " THE APPROXIMAGE FULL MARKET VALUE AND ADDITIONAL MACHINERY, EQUIPMENT ETC.";
			this.fcLabel145.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// GridResidentialBuildings
			// 
			this.GridResidentialBuildings.AllowBigSelection = false;
			this.GridResidentialBuildings.Cols = 7;
			this.GridResidentialBuildings.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridResidentialBuildings.Location = new System.Drawing.Point(23, 23);
			this.GridResidentialBuildings.MultiSelect = false;
			this.GridResidentialBuildings.Name = "GridResidentialBuildings";
			this.GridResidentialBuildings.ReadOnly = false;
			this.GridResidentialBuildings.Rows = 1;
			this.GridResidentialBuildings.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.GridResidentialBuildings.Size = new System.Drawing.Size(899, 283);
			this.GridResidentialBuildings.StandardTab = false;
			this.GridResidentialBuildings.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridResidentialBuildings.TabIndex = 102;
			// 
			// frmMunicipalValuationReturn
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(998, 529);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmMunicipalValuationReturn";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Municipal Valuation Return";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmMVRViewer_Load);
			this.Resize += new System.EventHandler(this.frmMVRViewer_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExcelDoc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame4)).EndInit();
			this.fcFrame4.ResumeLayout(false);
			this.fcFrame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame3)).EndInit();
			this.fcFrame3.ResumeLayout(false);
			this.fcFrame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame2)).EndInit();
			this.fcFrame2.ResumeLayout(false);
			this.fcFrame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).EndInit();
			this.fcFrame1.ResumeLayout(false);
			this.fcFrame1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame9)).EndInit();
			this.fcFrame9.ResumeLayout(false);
			this.fcFrame9.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame8)).EndInit();
			this.fcFrame8.ResumeLayout(false);
			this.fcFrame8.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame7)).EndInit();
			this.fcFrame7.ResumeLayout(false);
			this.fcFrame7.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame6)).EndInit();
			this.fcFrame6.ResumeLayout(false);
			this.fcFrame6.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame5)).EndInit();
			this.fcFrame5.ResumeLayout(false);
			this.fcFrame5.PerformLayout();
			this.fcTabPage1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame11)).EndInit();
			this.fcFrame11.ResumeLayout(false);
			this.fcFrame11.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame10)).EndInit();
			this.fcFrame10.ResumeLayout(false);
			this.fcFrame10.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkForestTransferredToFarmland)).EndInit();
			this.fcTabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame14)).EndInit();
			this.fcFrame14.ResumeLayout(false);
			this.fcFrame14.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame13)).EndInit();
			this.fcFrame13.ResumeLayout(false);
			this.fcFrame13.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame12)).EndInit();
			this.fcFrame12.ResumeLayout(false);
			this.fcFrame12.PerformLayout();
			this.fcTabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame15)).EndInit();
			this.fcFrame15.ResumeLayout(false);
			this.fcFrame15.PerformLayout();
			this.fcTabPage4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame18)).EndInit();
			this.fcFrame18.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame17)).EndInit();
			this.fcFrame17.ResumeLayout(false);
			this.fcFrame17.PerformLayout();
			this.fcTabPage5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame19)).EndInit();
			this.fcFrame19.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkHasTaxMaps)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationIncludedPersonalProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationIncludedBuildings)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationIncludedLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRevaluationCompleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame16)).EndInit();
			this.fcFrame16.ResumeLayout(false);
			this.fcFrame16.PerformLayout();
			this.fcTabPage6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcFrame20)).EndInit();
			this.fcFrame20.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkSeniorTaxDeferral)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkElderlyTaxCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxReliefProgram)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAssessmentRecordsAreComputerized)).EndInit();
			this.fcTabPage7.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridResidentialBuildings)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private System.ComponentModel.IContainer components;
        private FCTabControl tabControl1;
        private FCTabPage tabPage1;
        private FCFrame fcFrame4;
        public FCTextBox txtTotalHomesteadAssessments;
        public FCLabel fcLabel23;
        public FCTextBox txtTotalNumberHomesteads;
        public FCTextBox txtHomesteadTotalExemptions;
        public FCTextBox txtNumberHomesteadFullyExempt;
        public FCLabel fcLabel20;
        public FCTextBox txtHomesteadFullyExemptValue;
        public FCLabel fcLabel21;
        public FCTextBox txtTotalFullHomesteadExemptions;
        public FCLabel fcLabel22;
        public FCLabel fcLabel17;
        public FCTextBox txtTotalFullHomesteadsGranted;
        public FCLabel fcLabel18;
        public FCLabel fcLabel19;
        private FCFrame fcFrame3;
        public FCTextBox txtTaxLevy;
        public FCLabel fcLabel16;
        public FCTextBox txtTotalTaxableValuation;
        public FCLabel fcLabel15;
        public FCTextBox txtPropertyTaxRate;
        public FCLabel fcLabel1;
        private FCFrame fcFrame2;
        public FCTextBox txtTotalTaxablePersonalProperty;
        public FCLabel fcLabel14;
        public FCTextBox txtOtherPersonalProperty;
        public FCLabel fcLabel2;
        public FCTextBox txtBusinessEquipment;
        public FCLabel fcLabel4;
        public FCTextBox txtMachineryAndEquipment;
        public FCLabel fcLabel10;
        private FCFrame fcFrame1;
        public FCTextBox txtTotalTaxableRealEstate;
        public FCLabel fcLabel13;
        public FCTextBox txtTaxableBuilding;
        public FCLabel fcLabel6;
        public FCTextBox txtTaxableLand;
        public FCLabel fcLabel9;
        public FCLabel fcLabel12;
        private FCDateTimePicker dtCommitmentDate;
        public FCTextBox txtCertifiedRatio;
        public FCLabel fcLabel11;
        public FCTextBox txtMunicipality;
        public FCLabel Label7;
        public FCTextBox txtCounty;
        public FCLabel Label2;
        private FCTabPage tabPage2;
        private FCTabPage fcTabPage1;
        private FCTabPage fcTabPage2;
        private FCTabPage fcTabPage3;
        private FCTabPage fcTabPage4;
        private FCTabPage fcTabPage5;
        private FCTabPage fcTabPage6;
        private FCTabPage fcTabPage7;
        private FCFrame fcFrame5;
        private FCFrame fcFrame7;
        private FCFrame fcFrame6;
        public FCTextBox txtTifBeteDeposited;
        public FCLabel fcLabel24;
        public FCTextBox txtTifDeposited;
        public FCLabel fcLabel25;
        public FCTextBox txtTifCaptured;
        public FCLabel fcLabel26;
        public FCTextBox txtIncreasedTif;
        public FCLabel fcLabel27;
        public FCTextBox txtBeteInTIF;
        public FCLabel fcLabel3;
        public FCTextBox txtExemptValueBeteQualified;
        public FCLabel fcLabel5;
        public FCTextBox txtNumberBeteApproved;
        public FCLabel fcLabel7;
        public FCTextBox txtNumberBeteApplicationsProcessed;
        public FCLabel fcLabel8;
        public FCTextBox txtWatercraftExcise;
        public FCLabel fcLabel28;
        public FCTextBox txtMVExcise;
        public FCLabel fcLabel29;
        public FCLabel fcLabel30;
        private FCFrame fcFrame9;
        public FCTextBox txtForestHardwoodRate;
        public FCLabel fcLabel40;
        public FCTextBox txtForestMixedwoodRate;
        public FCLabel fcLabel41;
        public FCTextBox txtForestSoftwoodRate;
        public FCLabel fcLabel35;
        public FCTextBox txtForestTotalAssessed;
        public FCLabel lblTotalValuationForestLand;
        public FCTextBox txtForestNumberOfAcres;
        public FCLabel fcLabel38;
        public FCTextBox txtForestHardwoodAcreage;
        public FCLabel fcLabel39;
        public FCTextBox txtForestMixedWoodAcreage;
        public FCLabel fcLabel33;
        public FCTextBox txtForestSoftwoodAcreage;
        public FCLabel fcLabel34;
        public FCTextBox txtForestNumberOfParcels;
        public FCLabel lblForestParcelsClassified;
        public FCTextBox txtForestAveragePerAcre;
        public FCLabel fcLabel36;
        private FCFrame fcFrame8;
        public FCTextBox txtElectricalGenerationFacilities;
        public FCLabel fcLabel31;
        public FCTextBox txtTransmissionLines;
        public FCLabel fcLabel32;
        private FCFrame fcFrame10;
        public FCLabel fcLabel37;
        public FCTextBox txtForestNumberOfNonCompliancePenalties;
        public FCLabel fcLabel42;
        public FCTextBox txtForestAmountOfPenalties;
        public FCLabel fcLabel43;
        public FCTextBox txtForestAcresWithdrawn;
        public FCLabel lblForestAcresWithdrawn;
        public FCTextBox txtForestParcelsWithdrawn;
        public FCLabel lblTotalForestParcelsWithdrawn;
        public FCTextBox txtForestAcresFirstClassifiedThisYear;
        public FCLabel lblNumberForestAcres;
        private FCFrame fcFrame11;
        public FCTextBox txtOpenTotalValuation;
        public FCLabel fcLabel56;
        public FCTextBox txtOpenTotalAcres;
        public FCLabel fcLabel57;
        public FCTextBox txtOpenAcresFirstClassified;
        public FCLabel fcLabel58;
        public FCTextBox txtOpenParcelsFirstClassified;
        public FCLabel fcLabel59;
        public FCTextBox txtFarmTotalPenalties;
        public FCLabel fcLabel60;
        public FCTextBox txtFarmAcresWithdrawn;
        public FCLabel fcLabel61;
        public FCTextBox txtFarmParcelsWithdrawn;
        public FCLabel fcLabel62;
        public FCTextBox txtFarmHardwoodRate;
        public FCLabel fcLabel50;
        public FCTextBox txtFarmMixedWoodRate;
        public FCLabel fcLabel51;
        public FCTextBox txtFarmSoftwoodRate;
        public FCLabel fcLabel52;
        public FCTextBox txtFarmTotalValuationWoodland;
        public FCLabel fcLabel53;
        public FCTextBox txtFarmTotalAcresWoodland;
        public FCLabel fcLabel54;
        public FCTextBox txtFarmHardwoodAcres;
        public FCLabel fcLabel55;
        public FCTextBox txtFarmMixedWoodAcres;
        public FCLabel fcLabel44;
        public FCTextBox txtFarmSoftwoodAcres;
        public FCLabel fcLabel45;
        public FCTextBox txtFarmTotalValuationTillable;
        public FCLabel fcLabel46;
        public FCTextBox txtFarmTotalAcresTillable;
        public FCLabel fcLabel47;
        public FCTextBox txtFarmNumberOfAcresFirstClassified;
        public FCLabel fcLabel48;
        public FCTextBox txtFarmNumberOfParcelsFirstClassified;
        public FCLabel fcLabel49;
        private FCFrame fcFrame14;
        private FCFrame fcFrame13;
        public FCTextBox txtWaterfrontPenalties;
        public FCLabel fcLabel66;
        public FCTextBox txtWaterfrontAcresWithdrawn;
        public FCLabel fcLabel67;
        public FCTextBox txtWaterfrontParcelsWithdrawn;
        public FCLabel fcLabel68;
        public FCTextBox txtWaterfrontTotalValuation;
        public FCLabel fcLabel69;
        public FCTextBox txtWaterfrontTotalAcreage;
        public FCLabel fcLabel70;
        public FCTextBox txtWaterfrontAcresFirstClassified;
        public FCLabel fcLabel71;
        public FCTextBox txtWaterfrontParcelsFirstClassified;
        public FCLabel fcLabel72;
        private FCFrame fcFrame12;
        public FCTextBox txtOpenTotalAmountOfPenalties;
        public FCLabel fcLabel63;
        public FCTextBox txtOpenAcresWithdrawn;
        public FCLabel fcLabel64;
        public FCTextBox txtOpenParcelsWithdrawn;
        public FCLabel fcLabel65;
        public FCTextBox txtExemptSewage;
        public FCLabel fcLabel74;
        public FCTextBox txtExemptPrivateAirport;
        public FCLabel fcLabel75;
        public FCTextBox txtExemptMunicipalAirport;
        public FCLabel fcLabel76;
        public FCTextBox txtExemptMunicipalUtility;
        public FCLabel fcLabel77;
        public FCTextBox txtExemptMunicipal;
        public FCLabel fcLabel78;
        public FCTextBox txtExemptNHWater;
        public FCLabel fcLabel79;
        public FCTextBox txtExemptMaine;
        public FCLabel fcLabel80;
        public FCTextBox txtExemptFederal;
        public FCLabel fcLabel81;
        private FCFrame fcFrame15;
        public FCTextBox txtExemptFraternal;
        public FCLabel fcLabel87;
        public FCTextBox txtExemptReligiousWorship;
        public FCLabel fcLabel88;
        public FCTextBox txtExemptTaxableValueOfParsonages;
        public FCLabel fcLabel89;
        public FCTextBox txtExemptValueOfParsonages;
        public FCLabel fcLabel90;
        public FCTextBox txtExemptParsonages;
        public FCLabel fcLabel73;
        public FCTextBox txtExemptChamberOfCommerce;
        public FCLabel fcLabel82;
        public FCTextBox txtExemptVetOrgReimbursable;
        public FCLabel fcLabel83;
        public FCTextBox txtExemptVeteransOrganizations;
        public FCLabel fcLabel84;
        public FCTextBox txtExemptLiteraryAndScientific;
        public FCLabel fcLabel85;
        public FCTextBox txtExemptBenevolent;
        public FCLabel fcLabel86;
        public FCTextBox txtExemptSnowmobileGroomingEquipment;
        public FCLabel fcLabel91;
        public FCTextBox txtExemptPollutionControl;
        public FCLabel fcLabel92;
        public FCTextBox txtExemptAnimalWasteStorage;
        public FCLabel fcLabel93;
        public FCTextBox txtExemptWaterSupplyTransport;
        public FCLabel fcLabel94;
        public FCTextBox txtExemptLegallyBlind;
        public FCLabel fcLabel95;
        public FCTextBox txtExemptHospitalPersonalProperty;
        public FCLabel fcLabel96;
        private FCFrame fcFrame16;
        public FCLabel fcLabel97;
        public FCTextBox txtExemptOrganization1ExemptValue;
        public FCTextBox txtExemptProvisionOfLaw1;
        public FCTextBox txtExemptOrganization1Name;
        private FCFrame fcFrame17;
        public FCTextBox txtExemptVetCoopCount;
        public FCLabel fcLabel98;
        public FCTextBox txtExemptVetParaplegicCount;
        public FCLabel fcLabel99;
        public FCTextBox txtExemptVetWWINonResidentCount;
        public FCLabel fcLabel100;
        public FCTextBox txtExemptVetWWIResidentCount;
        public FCLabel fcLabel101;
        public FCTextBox txtExemptVetLivingTrustCount;
        public FCLabel fcLabel102;
        public FCTextBox txtExemptVetParaplegicLivingTrustCount;
        public FCLabel fcLabel103;
        public FCTextBox txtExemptVetSurvivingMaleCount;
        public FCLabel fcLabel104;
        private FCFrame fcFrame18;
        public FCTextBox txtExemptVetNonResidentValue;
        public FCTextBox txtExemptVetResidentValue;
        public FCTextBox txtExemptVetNonResidentCount;
        public FCLabel fcLabel107;
        public FCTextBox txtExemptVetResidentCount;
        public FCLabel fcLabel108;
        public FCTextBox txtExemptVetCoopValue;
        public FCTextBox txtExemptVetParaplegicValue;
        public FCTextBox txtExemptVetWWINonResidentValue;
        public FCTextBox txtExemptVetWWIResidentValue;
        public FCTextBox txtExemptVetLivingTrustValue;
        public FCTextBox txtExemptVetParaplegicLivingTrustValue;
        public FCTextBox txtExemptVetSurvivingMaleValue;
        public FCLabel fcLabel106;
        public FCLabel fcLabel105;
        public FCTextBox txtExemptVetVietnamValue;
        public FCTextBox txtExemptVetLebanonPanamaValue;
        public FCTextBox txtExemptVetVietnamCount;
        public FCLabel fcLabel109;
        public FCTextBox txtExemptVetLebanonPanamaCount;
        public FCLabel fcLabel110;
        public FCTextBox txtExemptVetDisabledValue;
        public FCTextBox txtExemptVetDisabledCount;
        public FCLabel fcLabel111;
        public FCLabel fcLabel113;
        public FCLabel fcLabel112;
        public FCTextBox txtExemptOrganization3ExemptValue;
        public FCTextBox txtExemptProvisionOfLaw3;
        public FCTextBox txtExemptOrganization3Name;
        public FCTextBox txtExemptOrganization2ExemptValue;
        public FCTextBox txtExemptProvisionOfLaw2;
        public FCTextBox txtExemptOrganization2Name;
        private FCFrame fcFrame19;
        public FCTextBox txtRevaluationCost;
        public FCLabel fcLabel126;
        private FCTextBox txtRevaluationContractorName;
        public FCLabel fcLabel124;
        private FCDateTimePicker dtRevaluationEffectiveDate;
        public FCLabel fcLabel125;
        private FCCheckBox chkRevaluationIncludedPersonalProperty;
        public FCLabel fcLabel122;
        private FCCheckBox chkRevaluationIncludedBuildings;
        public FCLabel fcLabel123;
        private FCCheckBox chkRevaluationIncludedLand;
        public FCLabel fcLabel121;
        private FCCheckBox chkRevaluationCompleted;
        public FCLabel fcLabel120;
        public FCTextBox txtMunicipalRecordsTotalTaxableAcreage;
        public FCLabel fcLabel119;
        public FCTextBox txtMunicipalRecordsTotalNumberOfParcels;
        public FCLabel fcLabel118;
        private FCComboBox cmbTaxMapType;
        public FCLabel fcLabel117;
        private FCTextBox txtNameOfOriginalContractor;
        public FCLabel fcLabel116;
        private FCDateTimePicker dtMapsOriginallyObtained;
        public FCLabel fcLabel115;
        public FCLabel fcLabel114;
        private FCFrame fcFrame20;
        private FCComboBox cmbAssessorFunctionality;
        public FCLabel fcLabel127;
        public FCTextBox txtAssessorEmail;
        public FCLabel fcLabel129;
        public FCTextBox txtAssessorName;
        public FCLabel fcLabel128;
        private FCDateTimePicker dtpFiscalEnd;
        public FCLabel fcLabel131;
        private FCDateTimePicker dtpFiscalStart;
        public FCLabel fcLabel130;
        private FCCheckBox chkHasTaxMaps;
        private FCDateTimePicker dtpPeriod4Due;
        public FCLabel fcLabel135;
        private FCDateTimePicker dtpPeriod3Due;
        public FCLabel fcLabel136;
        private FCDateTimePicker dtpPeriod2Due;
        public FCLabel fcLabel133;
        private FCDateTimePicker dtpPeriod1Due;
        public FCLabel fcLabel134;
        public FCTextBox txtInterestRate;
        public FCLabel fcLabel132;
        private FCCheckBox chkTaxReliefProgram;
        public FCLabel fcLabel139;
        public FCTextBox txtAssessmentSoftware;
        public FCLabel fcLabel138;
        private FCCheckBox chkAssessmentRecordsAreComputerized;
        public FCLabel fcLabel137;
        public FCTextBox txtAmountOfElderlyTaxCredit;
        public FCLabel fcLabel142;
        public FCTextBox txtNumberQualifiedForElderlyTaxCredit;
        public FCLabel fcLabel143;
        private FCCheckBox chkElderlyTaxCredit;
        public FCLabel fcLabel144;
        public FCTextBox txtAmountOfTaxRelief;
        public FCLabel fcLabel140;
        public FCTextBox txtNumberQualifiedForTaxRelief;
        public FCLabel fcLabel141;
        public FCTextBox txtIndustrialGrowthLine8;
        public FCTextBox txtIndustrialGrowthLine7;
        public FCTextBox txtIndustrialGrowthLine6;
        public FCTextBox txtIndustrialGrowthLine5;
        public FCTextBox txtIndustrialGrowthLine4;
        public FCTextBox txtIndustrialGrowthLine3;
        public FCTextBox txtIndustrialGrowthLine2;
        public FCTextBox txtIndustrialGrowthLine1;
        public FCLabel fcLabel145;
        private FCGrid GridResidentialBuildings;
        public FCTextBox txtExtremeLossesLine8;
        public FCTextBox txtExtremeLossesLine7;
        public FCTextBox txtExtremeLossesLine6;
        public FCTextBox txtExtremeLossesLine5;
        public FCTextBox txtExtremeLossesLine4;
        public FCTextBox txtExtremeLossesLine3;
        public FCTextBox txtExtremeLossesLine2;
        public FCTextBox txtExtremeLossesLine1;
        public FCLabel fcLabel146;
        public FCTextBox txtGeneralChangeLine8;
        public FCTextBox txtGeneralChangeLine7;
        public FCTextBox txtGeneralChangeLine6;
        public FCTextBox txtGeneralChangeLine5;
        public FCTextBox txtGeneralChangeLine4;
        public FCTextBox txtGeneralChangeLine3;
        public FCTextBox txtGeneralChangeLine2;
        public FCTextBox txtGeneralChangeLine1;
        public FCLabel fcLabel147;
        private JavaScript javaScript1;
        private FCComboBox cmbExcisePeriodType;
        private FCCheckBox chkForestTransferredToFarmland;
        public FCLabel fcLabel148;
        public FCTextBox txtExemptSolarAndWindValue;
        public FCTextBox txtExemptSolarAndWindApproved;
        public FCLabel fcLabel149;
        public FCTextBox txtExemptSolarAndWindProcessed;
        public FCLabel fcLabel150;
        public FCTextBox txtAmountOfSeniorTaxDeferral;
        public FCLabel fcLabel151;
        public FCTextBox txtNumberQualifiedForSeniorDeferral;
        public FCLabel fcLabel152;
        private FCCheckBox chkSeniorTaxDeferral;
        public FCLabel fcLabel153;
        public FCTextBox txtGeneralChangeLine10;
        public FCTextBox txtGeneralChangeLine9;
		public FCLabel fcLabel154;
		private FCDateTimePicker dtpSignatureDate;
		public FCLabel fcLabel155;
	}
}
