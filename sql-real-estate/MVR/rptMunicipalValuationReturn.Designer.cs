﻿namespace TWRE0000.MVR
{
	/// <summary>
	/// Summary description for rptMVR.
	/// </summary>
	partial class rptMunicipalValuationReturn
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMunicipalValuationReturn));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport4 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport5 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport6 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport7 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport8 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport9 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport10 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1,
				this.SubReport2,
				this.SubReport3,
				this.SubReport4,
				this.SubReport5,
				this.SubReport6,
				this.SubReport7,
				this.SubReport8,
				this.SubReport9,
				this.SubReport10
			});
			this.Detail.Height = 1.385417F;
			this.Detail.Name = "Detail";
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.08333334F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0F;
			this.SubReport1.Width = 7.416667F;
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.0625F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.125F;
			this.SubReport2.Width = 7.416667F;
			// 
			// SubReport3
			// 
			this.SubReport3.CloseBorder = false;
			this.SubReport3.Height = 0.0625F;
			this.SubReport3.Left = 0F;
			this.SubReport3.Name = "SubReport3";
			this.SubReport3.Report = null;
			this.SubReport3.Top = 0.25F;
			this.SubReport3.Width = 7.416667F;
			// 
			// SubReport4
			// 
			this.SubReport4.CloseBorder = false;
			this.SubReport4.Height = 0.0625F;
			this.SubReport4.Left = 0F;
			this.SubReport4.Name = "SubReport4";
			this.SubReport4.Report = null;
			this.SubReport4.Top = 0.375F;
			this.SubReport4.Width = 7.416667F;
			// 
			// SubReport5
			// 
			this.SubReport5.CloseBorder = false;
			this.SubReport5.Height = 0.0625F;
			this.SubReport5.Left = 0F;
			this.SubReport5.Name = "SubReport5";
			this.SubReport5.Report = null;
			this.SubReport5.Top = 0.5F;
			this.SubReport5.Width = 7.416667F;
			// 
			// SubReport6
			// 
			this.SubReport6.CloseBorder = false;
			this.SubReport6.Height = 0.0625F;
			this.SubReport6.Left = 0F;
			this.SubReport6.Name = "SubReport6";
			this.SubReport6.Report = null;
			this.SubReport6.Top = 0.625F;
			this.SubReport6.Width = 7.416667F;
			// 
			// SubReport7
			// 
			this.SubReport7.CloseBorder = false;
			this.SubReport7.Height = 0.0625F;
			this.SubReport7.Left = 0F;
			this.SubReport7.Name = "SubReport7";
			this.SubReport7.Report = null;
			this.SubReport7.Top = 0.75F;
			this.SubReport7.Width = 7.416667F;
			// 
			// SubReport8
			// 
			this.SubReport8.CloseBorder = false;
			this.SubReport8.Height = 0.0625F;
			this.SubReport8.Left = 0F;
			this.SubReport8.Name = "SubReport8";
			this.SubReport8.Report = null;
			this.SubReport8.Top = 0.875F;
			this.SubReport8.Width = 7.416667F;
			// 
			// SubReport9
			// 
			this.SubReport9.CloseBorder = false;
			this.SubReport9.Height = 0.0625F;
			this.SubReport9.Left = 0F;
			this.SubReport9.Name = "SubReport9";
			this.SubReport9.Report = null;
			this.SubReport9.Top = 1F;
			this.SubReport9.Width = 7.416667F;
			// 
			// SubReport10
			// 
			this.SubReport10.CloseBorder = false;
			this.SubReport10.Height = 0.0625F;
			this.SubReport10.Left = 0F;
			this.SubReport10.Name = "SubReport10";
			this.SubReport10.Report = null;
			this.SubReport10.Top = 1.166667F;
			this.SubReport10.Width = 7.416667F;
			// 
			// rptMVR
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport3;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport4;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport5;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport6;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport7;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport8;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport9;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport10;
	}
}
