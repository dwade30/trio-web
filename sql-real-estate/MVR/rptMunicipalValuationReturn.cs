﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.MVR;
using TWSharedLibrary;

namespace TWRE0000.MVR
{
	/// <summary>
	/// Summary description for rptMVR.
	/// </summary>
	public partial class rptMunicipalValuationReturn : BaseSectionReport, IView<IMunicipalValuationReportViewModel>
    {
       

        public rptMunicipalValuationReturn()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptMunicipalValuationReturn(IMunicipalValuationReportViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

       
		private void InitializeComponentEx()
        {
            this.Name = "Municipal Valuation Return";
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SubReport1.Report = new srptMVRPage1(ViewModel.ValuationReturn);
			SubReport2.Report = new srptMVRPage2(ViewModel.ValuationReturn);
			SubReport3.Report = new srptMVRPage3(ViewModel.ValuationReturn);
			SubReport4.Report = new srptMVRPage4(ViewModel.ValuationReturn);
			SubReport5.Report = new srptMVRPage5(ViewModel.ValuationReturn);
			SubReport6.Report = new srptVetExemptWorksheet(ViewModel.ValuationReturn);
			SubReport7.Report = new srptMVRPage7(ViewModel.ValuationReturn);
			SubReport8.Report = new srptMVRPage8(ViewModel.ValuationReturn);
			SubReport9.Report = new srptMVRPage9(ViewModel.ValuationReturn);
			SubReport10.Report = new rptTaxRateForm(ViewModel.ValuationReturn);
			SubReport10.Report.UserData = "MVR";
		}

		private void rptMVR_Load(object sender, System.EventArgs e)
		{

		}

        public void Show()
        {
            var reportViewer = new frmReportViewer();
            reportViewer.Init(this);
        }

        public IMunicipalValuationReportViewModel ViewModel { get; set; }
    }
}
