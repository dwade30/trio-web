﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmHelp.
	/// </summary>
	partial class frmHelp : BaseForm
	{
		public fecherFoundation.FCTextBox txtHelpComments;
		public fecherFoundation.FCListBox List1;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblHelpHeading;
		public fecherFoundation.FCButton cmdQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHelp));
			this.txtHelpComments = new fecherFoundation.FCTextBox();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.List1 = new fecherFoundation.FCListBox();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblHelpHeading = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 688);
			this.BottomPanel.Size = new System.Drawing.Size(652, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdQuit);
			this.ClientArea.Controls.Add(this.txtHelpComments);
			this.ClientArea.Controls.Add(this.List1);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblHelpHeading);
			//FC:FINAL:CHN - i.issue #1636: Increase size of lists. 
			// this.ClientArea.Size = new System.Drawing.Size(652, 628);
			this.ClientArea.Size = new System.Drawing.Size(652, 808);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(652, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(197, 30);
			this.HeaderText.Text = "Real Estate Help";
			// 
			// txtHelpComments
			// 
			this.txtHelpComments.AutoSize = false;
			this.txtHelpComments.BackColor = System.Drawing.SystemColors.Menu;
			this.txtHelpComments.LinkItem = null;
			this.txtHelpComments.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHelpComments.LinkTopic = null;
			//FC:FINAL:CHN - i.issue #1636: Increase size of lists. 
			// this.txtHelpComments.Location = new System.Drawing.Point(30, 295);
			this.txtHelpComments.Location = new System.Drawing.Point(30, 475);
			this.txtHelpComments.LockedOriginal = true;
			this.txtHelpComments.MaxLength = 25000;
			this.txtHelpComments.Multiline = true;
			this.txtHelpComments.Name = "txtHelpComments";
			this.txtHelpComments.ReadOnly = true;
			this.txtHelpComments.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.txtHelpComments.Size = new System.Drawing.Size(591, 190);
			this.txtHelpComments.TabIndex = 8;
			this.txtHelpComments.TabStop = false;
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.ForeColor = System.Drawing.Color.White;
			//FC:FINAL:CHN - i.issue #1636: Increase size of lists. 
			// this.cmdQuit.Location = new System.Drawing.Point(30, 505);
			this.cmdQuit.Location = new System.Drawing.Point(280, 685);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(92, 40);
			this.cmdQuit.TabIndex = 9;
			this.cmdQuit.Text = "Return";
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// List1
			// 
			this.List1.Appearance = 0;
			this.List1.BackColor = System.Drawing.SystemColors.Window;
			this.List1.Location = new System.Drawing.Point(30, 85);
			this.List1.MultiSelect = 0;
			this.List1.Name = "List1";
			//FC:FINAL:CHN - i.issue #1636: Increase size of lists. 
			// this.List1.Size = new System.Drawing.Size(591, 190);
			this.List1.Size = new System.Drawing.Size(591, 370);
			this.List1.Sorted = false;
			this.List1.TabIndex = 7;
			this.List1.DoubleClick += new System.EventHandler(this.List1_DoubleClick);
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.Location = new System.Drawing.Point(95, 60);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(43, 15);
			this.Label6.TabIndex = 2;
			this.Label6.Text = "CODE";
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(515, 60);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(88, 15);
			this.Label5.TabIndex = 6;
			this.Label5.Text = "SHORT DESC";
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.Location = new System.Drawing.Point(370, 60);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(92, 15);
			this.Label4.TabIndex = 5;
			this.Label4.Text = "DESCRIPTION";
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(272, 60);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(40, 15);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "BASE";
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(183, 60);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(36, 15);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "UNIT";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 60);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(32, 15);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "KEY";
			// 
			// lblHelpHeading
			// 
			this.lblHelpHeading.Location = new System.Drawing.Point(30, 30);
			this.lblHelpHeading.Name = "lblHelpHeading";
			this.lblHelpHeading.Size = new System.Drawing.Size(277, 13);
			this.lblHelpHeading.TabIndex = 0;
			// 
			// frmHelp
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			//FC:FINAL:CHN - i.issue #1636: Increase size of lists. 
			// this.ClientSize = new System.Drawing.Size(652, 630);
			this.ClientSize = new System.Drawing.Size(652, 810);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmHelp";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Real Estate Help";
			this.Load += new System.EventHandler(this.frmHelp_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmHelp_KeyUp);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
