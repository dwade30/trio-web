﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewValuationReport.
	/// </summary>
	partial class rptNewValuationReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewValuationReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtmuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtassessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaccount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmaplot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddedLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtcard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddedBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotaltotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtnbtotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotaltotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNLTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtmuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtassessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddedLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddedBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnbtotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNLTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtmaplot,
				this.txtname,
				this.txtaccount,
				this.txtAddedLand,
				this.txtcard,
				this.txtAddedBuilding,
				this.txtLocation,
				this.txtNewValuation
			});
			this.Detail.Height = 0.3333333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txttotaltotal1,
				this.txtnbtotal,
				this.txttotaltotal,
				this.txtNLTotal,
				this.Field15,
				this.txtCount
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtmuniname,
				this.txtassessment,
				this.txtdate,
				this.txtpage,
				this.txtaccount1,
				this.Field4,
				this.Field8,
				this.Field9,
				this.txtTime,
				this.Field10,
				this.Field12,
				this.Field13,
				this.Field14
			});
			this.PageHeader.Height = 0.9270833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtmuniname
			// 
			this.txtmuniname.Height = 0.19F;
			this.txtmuniname.Left = 0F;
			this.txtmuniname.Name = "txtmuniname";
			this.txtmuniname.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmuniname.Text = "Muniname";
			this.txtmuniname.Top = 0F;
			this.txtmuniname.Width = 2.416667F;
			// 
			// txtassessment
			// 
			this.txtassessment.Height = 0.25F;
			this.txtassessment.Left = 2.0625F;
			this.txtassessment.Name = "txtassessment";
			this.txtassessment.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtassessment.Text = "Valuation Change Estimate";
			this.txtassessment.Top = 0F;
			this.txtassessment.Width = 3.4375F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.19F;
			this.txtdate.Left = 6F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtdate.Text = "DATE";
			this.txtdate.Top = 0F;
			this.txtdate.Width = 1.5F;
			// 
			// txtpage
			// 
			this.txtpage.Height = 0.25F;
			this.txtpage.Left = 6.333333F;
			this.txtpage.Name = "txtpage";
			this.txtpage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtpage.Text = "page";
			this.txtpage.Top = 0.1666667F;
			this.txtpage.Width = 1.166667F;
			// 
			// txtaccount1
			// 
			this.txtaccount1.Height = 0.19F;
			this.txtaccount1.Left = 0F;
			this.txtaccount1.Name = "txtaccount1";
			this.txtaccount1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.txtaccount1.Text = "Account";
			this.txtaccount1.Top = 0.75F;
			this.txtaccount1.Width = 0.6666667F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 0F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field4.Text = "Map/Lot";
			this.Field4.Top = 0.5833333F;
			this.Field4.Width = 0.8333333F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 4.416667F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field8.Text = "New Land";
			this.Field8.Top = 0.75F;
			this.Field8.Width = 0.9166667F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.19F;
			this.Field9.Left = 0.6666667F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field9.Text = "Card";
			this.Field9.Top = 0.75F;
			this.Field9.Width = 0.4166667F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = "Muniname";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.6875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 5.416667F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field10.Text = "New Building";
			this.Field10.Top = 0.75F;
			this.Field10.Width = 1F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.19F;
			this.Field12.Left = 1.333333F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field12.Text = "Location";
			this.Field12.Top = 0.75F;
			this.Field12.Width = 1.333333F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.19F;
			this.Field13.Left = 1.333333F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field13.Text = "Name";
			this.Field13.Top = 0.5833333F;
			this.Field13.Width = 1.333333F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.19F;
			this.Field14.Left = 6.416667F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field14.Text = "New Valuation";
			this.Field14.Top = 0.75F;
			this.Field14.Width = 1.083333F;
			// 
			// txtmaplot
			// 
			this.txtmaplot.Height = 0.19F;
			this.txtmaplot.Left = 0F;
			this.txtmaplot.Name = "txtmaplot";
			this.txtmaplot.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmaplot.Text = null;
			this.txtmaplot.Top = 0F;
			this.txtmaplot.Width = 1.25F;
			// 
			// txtname
			// 
			this.txtname.Height = 0.19F;
			this.txtname.Left = 1.333333F;
			this.txtname.Name = "txtname";
			this.txtname.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtname.Text = null;
			this.txtname.Top = 0F;
			this.txtname.Width = 3F;
			// 
			// txtaccount
			// 
			this.txtaccount.CanGrow = false;
			this.txtaccount.Height = 0.19F;
			this.txtaccount.Left = 0F;
			this.txtaccount.MultiLine = false;
			this.txtaccount.Name = "txtaccount";
			this.txtaccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtaccount.Text = "00000000";
			this.txtaccount.Top = 0.1666667F;
			this.txtaccount.Width = 0.5833333F;
			// 
			// txtAddedLand
			// 
			this.txtAddedLand.CanGrow = false;
			this.txtAddedLand.Height = 0.19F;
			this.txtAddedLand.Left = 4.416667F;
			this.txtAddedLand.MultiLine = false;
			this.txtAddedLand.Name = "txtAddedLand";
			this.txtAddedLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAddedLand.Text = null;
			this.txtAddedLand.Top = 0F;
			this.txtAddedLand.Width = 0.9166667F;
			// 
			// txtcard
			// 
			this.txtcard.CanGrow = false;
			this.txtcard.Height = 0.19F;
			this.txtcard.Left = 0.6666667F;
			this.txtcard.MultiLine = false;
			this.txtcard.Name = "txtcard";
			this.txtcard.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtcard.Text = null;
			this.txtcard.Top = 0.1666667F;
			this.txtcard.Width = 0.25F;
			// 
			// txtAddedBuilding
			// 
			this.txtAddedBuilding.CanGrow = false;
			this.txtAddedBuilding.Height = 0.19F;
			this.txtAddedBuilding.Left = 5.416667F;
			this.txtAddedBuilding.MultiLine = false;
			this.txtAddedBuilding.Name = "txtAddedBuilding";
			this.txtAddedBuilding.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAddedBuilding.Text = "0000000000";
			this.txtAddedBuilding.Top = 0F;
			this.txtAddedBuilding.Width = 1F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 1.333333F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.1666667F;
			this.txtLocation.Width = 3F;
			// 
			// txtNewValuation
			// 
			this.txtNewValuation.CanGrow = false;
			this.txtNewValuation.Height = 0.19F;
			this.txtNewValuation.Left = 6.5F;
			this.txtNewValuation.MultiLine = false;
			this.txtNewValuation.Name = "txtNewValuation";
			this.txtNewValuation.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtNewValuation.Text = "0000000000";
			this.txtNewValuation.Top = 0F;
			this.txtNewValuation.Width = 1F;
			// 
			// txttotaltotal1
			// 
			this.txttotaltotal1.Height = 0.1875F;
			this.txttotaltotal1.Left = 0F;
			this.txttotaltotal1.Name = "txttotaltotal1";
			this.txttotaltotal1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txttotaltotal1.Text = "TOTAL";
			this.txttotaltotal1.Top = 0.0625F;
			this.txttotaltotal1.Width = 0.8125F;
			// 
			// txtnbtotal
			// 
			this.txtnbtotal.CanShrink = true;
			this.txtnbtotal.Height = 0.19F;
			this.txtnbtotal.Left = 5.416667F;
			this.txtnbtotal.Name = "txtnbtotal";
			this.txtnbtotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtnbtotal.Text = null;
			this.txtnbtotal.Top = 0.08333334F;
			this.txtnbtotal.Width = 1F;
			// 
			// txttotaltotal
			// 
			this.txttotaltotal.Height = 0.19F;
			this.txttotaltotal.Left = 6.5F;
			this.txttotaltotal.Name = "txttotaltotal";
			this.txttotaltotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txttotaltotal.Text = null;
			this.txttotaltotal.Top = 0.08333334F;
			this.txttotaltotal.Width = 1F;
			// 
			// txtNLTotal
			// 
			this.txtNLTotal.CanShrink = true;
			this.txtNLTotal.Height = 0.19F;
			this.txtNLTotal.Left = 4.416667F;
			this.txtNLTotal.Name = "txtNLTotal";
			this.txtNLTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtNLTotal.Text = null;
			this.txtNLTotal.Top = 0.08333334F;
			this.txtNLTotal.Width = 0.9166667F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.19F;
			this.Field15.Left = 0.9166667F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field15.Text = "Accounts:";
			this.Field15.Top = 0.08333334F;
			this.Field15.Width = 0.8333333F;
			// 
			// txtCount
			// 
			this.txtCount.CanShrink = true;
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 1.75F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.08333334F;
			this.txtCount.Width = 0.9166667F;
			// 
			// rptNewValuationReport
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.499306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtmuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtassessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddedLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddedBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnbtotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNLTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmaplot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddedLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtcard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddedBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewValuation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotaltotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtnbtotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotaltotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNLTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtassessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
