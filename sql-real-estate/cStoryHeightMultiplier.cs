﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cStoryHeightMultiplier
	{
		//=========================================================
		private int lngRecordID;
		private int lngSectionID;
		private double dblWallHeight;
		private double dblSqftMultiplier;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int SectionID
		{
			set
			{
				lngSectionID = value;
				IsUpdated = true;
			}
			get
			{
				int SectionID = 0;
				SectionID = lngSectionID;
				return SectionID;
			}
		}

		public double WallHeight
		{
			set
			{
				dblWallHeight = value;
				IsUpdated = true;
			}
			get
			{
				double WallHeight = 0;
				WallHeight = dblWallHeight;
				return WallHeight;
			}
		}

		public double SqftMultiplier
		{
			set
			{
				dblSqftMultiplier = value;
				IsUpdated = true;
			}
			get
			{
				double SqftMultiplier = 0;
				SqftMultiplier = dblSqftMultiplier;
				return SqftMultiplier;
			}
		}
	}
}
