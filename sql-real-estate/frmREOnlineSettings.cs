﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmREOnlineSettings.
	/// </summary>
	public partial class frmREOnlineSettings : BaseForm
	{
		public frmREOnlineSettings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		cSettingsController setCont = new cSettingsController();

		private void frmREOnlineSettings_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmREOnlineSettings_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmREOnlineSettings properties;
			//frmREOnlineSettings.FillStyle	= 0;
			//frmREOnlineSettings.ScaleWidth	= 5880;
			//frmREOnlineSettings.ScaleHeight	= 4005;
			//frmREOnlineSettings.LinkTopic	= "Form2";
			//frmREOnlineSettings.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			ShowSettings();
		}

		private void ShowSettings()
		{
			string strTemp;
			strTemp = setCont.GetSettingValue("REOnlineClientID", "REOnline", "", "", "");
			txtClientID.Text = FCConvert.ToString(Conversion.Val(strTemp));
			if (Conversion.Val(strTemp) > 0)
			{
				txtClientID.Enabled = false;
			}
			strTemp = setCont.GetSettingValue("REOnlineDataSource", "REOnline", "", "", "");
			if (strTemp == "")
			{
				strTemp = "209.166.175.114";
			}
			txtSQLServer.Text = strTemp;
			strTemp = setCont.GetSettingValue("REOnlineUserName", "REOnline", "", "", "");
			if (strTemp == "")
			{
				strTemp = "REWebAdmin";
			}
			txtUserName.Text = strTemp;
			strTemp = setCont.GetSettingValue("REOnlinePassword", "REOnline", "", "", "");
			if (strTemp == "")
			{
				strTemp = "adminpass1";
			}
			txtPassword.Text = strTemp;
		}

		private bool SaveSettings()
		{
			bool SaveSettings = false;
			try
			{
				// On Error GoTo ErrorHandler
				setCont.SaveSetting(FCConvert.ToString(Conversion.Val(txtClientID.Text)), "REOnlineClientID", "REOnline", "", "", "");
				setCont.SaveSetting(txtSQLServer.Text, "REOnlineDataSource", "REOnline", "", "", "");
				setCont.SaveSetting(txtUserName.Text, "REOnlineUserName", "REOnline", "", "", "");
				setCont.SaveSetting(txtPassword.Text, "REOnlinePassword", "REOnline", "", "", "");
				SaveSettings = true;
				MessageBox.Show("Settings Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveSettings;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveSettings;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveSettings())
			{
				this.Unload();
			}
		}
	}
}
