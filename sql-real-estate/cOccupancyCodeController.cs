﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cOccupancyCodeController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cGenericCollection GetOccupancyCodes()
		{
			cGenericCollection GetOccupancyCodes = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialOccupancyCode occCode;
				rs.OpenRecordset("select * from CommercialOccupancyCodes order by name", "RealEstate");
				while (!rs.EndOfFile())
				{
					occCode = new cCommercialOccupancyCode();
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					occCode.Code = FCConvert.ToString(rs.Get_Fields("Code"));
					occCode.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					occCode.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					occCode.SectionID = FCConvert.ToInt32(rs.Get_Fields("SectionID"));
					// TODO Get_Fields: Field [OccupancyCodeNumber] not found!! (maybe it is an alias?)
					occCode.OccupancyCodeNumber = FCConvert.ToInt32(rs.Get_Fields("OccupancyCodeNumber"));
					occCode.IsUpdated = false;
					gColl.AddItem(occCode);
					rs.MoveNext();
				}
				GetOccupancyCodes = gColl;
				return GetOccupancyCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetOccupancyCodes;
		}

		public cGenericCollection GetOccupancyCodesForSection(int lngSectionID)
		{
			cGenericCollection GetOccupancyCodesForSection = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialOccupancyCode occCode;
				rs.OpenRecordset("select * from CommercialOccupancyCodes where sectionid = " + FCConvert.ToString(lngSectionID) + " order by name", "RealEstate");
				while (!rs.EndOfFile())
				{
					occCode = new cCommercialOccupancyCode();
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					occCode.Code = FCConvert.ToString(rs.Get_Fields("Code"));
					occCode.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					occCode.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					// TODO Get_Fields: Field [OccupancyCodeNumber] not found!! (maybe it is an alias?)
					occCode.OccupancyCodeNumber = FCConvert.ToInt32(rs.Get_Fields("OccupancyCodeNumber"));
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					occCode.SectionID = FCConvert.ToInt32(rs.Get_Fields("SectionID"));
					occCode.IsUpdated = false;
					gColl.AddItem(occCode);
					rs.MoveNext();
				}
				GetOccupancyCodesForSection = gColl;
				return GetOccupancyCodesForSection;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetOccupancyCodesForSection;
		}

		public cCommercialOccupancyCode GetOccupancyCode(int lngID)
		{
			cCommercialOccupancyCode GetOccupancyCode = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialOccupancyCode occCode;
				rs.OpenRecordset("select * from CommercialOccupancyCodes where id = " + FCConvert.ToString(lngID), "RealEstate");
				if (!rs.EndOfFile())
				{
					occCode = new cCommercialOccupancyCode();
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					occCode.Code = FCConvert.ToString(rs.Get_Fields("Code"));
					occCode.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					occCode.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					// TODO Get_Fields: Field [OccupancyCodeNumber] not found!! (maybe it is an alias?)
					occCode.OccupancyCodeNumber = FCConvert.ToInt32(rs.Get_Fields("OccupancyCodeNumber"));
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					occCode.SectionID = FCConvert.ToInt32(rs.Get_Fields("SectionID"));
					occCode.IsUpdated = false;
					GetOccupancyCode = occCode;
				}
				return GetOccupancyCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetOccupancyCode;
		}

		public cCommercialOccupancyCode GetOccupancyCodeByCodeNumber(int lngCode)
		{
			cCommercialOccupancyCode GetOccupancyCodeByCodeNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialOccupancyCode occCode;
				rs.OpenRecordset("select * from CommercialOccupancyCodes where OccupancyCodeNumber = " + FCConvert.ToString(lngCode), "RealEstate");
				if (!rs.EndOfFile())
				{
					occCode = new cCommercialOccupancyCode();
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					occCode.Code = FCConvert.ToString(rs.Get_Fields("Code"));
					occCode.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					occCode.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					// TODO Get_Fields: Field [OccupancyCodeNumber] not found!! (maybe it is an alias?)
					occCode.OccupancyCodeNumber = FCConvert.ToInt32(rs.Get_Fields("OccupancyCodeNumber"));
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					occCode.SectionID = FCConvert.ToInt32(rs.Get_Fields("SectionID"));
					occCode.IsUpdated = false;
					GetOccupancyCodeByCodeNumber = occCode;
				}
				return GetOccupancyCodeByCodeNumber;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetOccupancyCodeByCodeNumber;
		}

		public void DeleteOccupancyCode(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialOccupancyCodes where id = " + FCConvert.ToString(lngID), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveOccupancyCode(ref cCommercialOccupancyCode occCode)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (occCode.IsDeleted)
				{
					DeleteOccupancyCode(occCode.ID);
					return;
				}
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("select * from commercialoccupancycodes where id = " + occCode.ID, "RealEstate");
				if (!rs.EndOfFile())
				{
					rs.Edit();
				}
				else
				{
					if (occCode.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Occupancy Code Record not found";
						return;
					}
					rs.AddNew();
				}
				rs.Set_Fields("Name", occCode.Name);
				rs.Set_Fields("SectionID", occCode.SectionID);
				rs.Set_Fields("Code", occCode.Code);
				rs.Set_Fields("OccupancyCodeNumber", occCode.OccupancyCodeNumber);
				rs.Update();
				occCode.ID = rs.Get_Fields_Int32("ID");
				occCode.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveOccupancyCodes(ref cGenericCollection occCodes)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(occCodes == null))
				{
					cCommercialOccupancyCode occCode = null;
					int lngIndex;
					occCodes.MoveFirst();
					while (occCodes.IsCurrent())
					{
						if (occCode.IsUpdated || occCode.IsDeleted)
						{
							SaveOccupancyCode(ref occCode);
						}
						if (HadError)
						{
							return;
						}
						if (occCode.IsDeleted)
						{
							occCodes.RemoveCurrent();
						}
						occCodes.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
