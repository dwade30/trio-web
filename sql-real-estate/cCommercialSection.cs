﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialSection
	{
		//=========================================================
		private int lngRecordID;
		private string strSectionName = string.Empty;
		private string strDescription = string.Empty;
		// Private lngCodeNumber As Long
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Name
		{
			set
			{
				strSectionName = value;
				IsUpdated = true;
			}
			get
			{
				string Name = "";
				Name = strSectionName;
				return Name;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
	}
}
