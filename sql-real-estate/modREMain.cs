﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using TWSharedLibrary;

namespace TWRE0000
{
	public class modREMain
	{
		public int success;

		public static void Main()
		{
			string strWhereFrom = "";
			clsDRWrapper clsTemp;
			DateTime dttemp;

			try
			{
				modGlobalVariables.Statics.boolInArchiveMode = false;
				modGlobalVariables.Statics.boolRE = true;
				modGlobalVariables.Statics.strRECostFileDatabase = "TWRE0000.vb1";
				modGlobalVariables.Statics.strCommitDB = "RECommit";
				modGlobalVariables.Statics.strGNDatabase = "SystemSettings";
				modGlobalVariables.Statics.strBLDatabase = "twbl0000.vb1";
				modGlobalVariables.Statics.strTranCodeTownCode = "Tran";
				modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown = false;
				string strDataPath;
				strDataPath = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strDataPath, 1) != "\\")
				{
					strDataPath += "\\";
				}
				
				if (!modGlobalFunctions.LoadSQLConfig("TWRE0000.VB1"))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				clsTemp = new clsDRWrapper();
				modGlobalVariables.Statics.searchRS = new clsDRWrapper();
				modGlobalVariables.Statics.gRstemp = new clsDRWrapper();
				Statics.strDbCP = clsTemp.Get_GetFullDBName("CentralParties") + ".dbo.";
				if (modRegionalTown.IsRegionalTown())
				{
					modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown = true;
				}
				modGlobalFunctions.GetGlobalRegistrySetting();
				modGlobalFunctions.UpdateUsedModules();
				if (modGlobalConstants.Statics.gboolBL)
				{
					modGlobalConstants.Statics.gboolPP = true;
				}
				if (modGlobalConstants.Statics.gboolCL)
				{
					if (clsTemp.OpenRecordset("select basis,OVERPAYINTERESTRATE from COLLECTIONS", modGlobalVariables.strCLDatabase))
					{
						if (!clsTemp.EndOfFile())
						{
							modGlobalVariables.Statics.gintBasis = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("Basis"))));
							modGlobalVariables.Statics.dblOverPayRate = Conversion.Val(clsTemp.Get_Fields_Double("overpayinterestrate"));
						}
					}
				}
				
				modReplaceWorkFiles.Statics.gstrNetworkFlag = (modRegistry.GetRegistryKey("NetworkFlag") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "Y");
				modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;
				modGlobalVariables.Statics.boolLandSChanged = true;
				modGlobalVariables.Statics.boolRSZChanged = true;
                modGlobalRoutines.CheckVersion();
				modReplaceWorkFiles.GetGlobalVariables();
				modReplaceWorkFiles.Statics.rsVariables = null;
				if (modGlobalConstants.Statics.gstrArchiveYear != string.Empty)
				{
					modGlobalVariables.Statics.boolInArchiveMode = true;
				}
				modGlobalVariables.Statics.boolRegRecords = true;
				
				modGlobalVariables.Statics.gintLastAccountNumber = (modRegistry.GetRegistryKey("RELastAccountNumber") != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn))) : 0);
				modREASValuations.Statics.Account = modGlobalVariables.Statics.gintLastAccountNumber;
				modGlobalVariables.Statics.boolFromBilling = false;
				modReplaceWorkFiles.EntryFlagFile(true);
				if (modGlobalConstants.Statics.gstrEntryFlag == "BL")
				{
					modGlobalVariables.Statics.boolFromBilling = true;
				}
				modReplaceWorkFiles.EntryFlagFile(true, "", true);
				modGNBas.Statics.gboolViewCommercial = false;
				clsTemp.OpenRecordset("select * from modules", modGlobalVariables.Statics.strGNDatabase);
				// If clsTemp.Fields("CM") Then
				if (Information.IsDate(clsTemp.Get_Fields("cmdate")))
				{
					if (Information.IsDate(clsTemp.Get_Fields("cmdate")) && clsTemp.Get_Fields_DateTime("cmdate").ToOADate() != 0)
					{
						dttemp = (DateTime)clsTemp.Get_Fields_DateTime("cmdate");
						if (dttemp.Year > DateTime.Now.Year)
						{
							modGNBas.Statics.gboolCommercial = true;
							/*? 49 */
						}
						else if (dttemp.Year == DateTime.Now.Year)
						{
							if (dttemp.Month >= DateTime.Now.Month)
							{
								modGNBas.Statics.gboolCommercial = true;
							}
						}
						else
						{
							modGNBas.Statics.gboolCommercial = false;
						}
					}
					else
					{
						modGNBas.Statics.gboolCommercial = false;
					}
				}
				else
				{
					modGNBas.Statics.gboolCommercial = false;
				}
				// Else
				// gboolCommercial = False
				// End If
				// 
				if (modGNBas.Statics.gboolCommercial)
				{
					modGNBas.Statics.gboolViewCommercial = true;
					clsTemp.OpenRecordset("select * from modules", modGlobalVariables.Statics.strGNDatabase);
					if (Conversion.Val(clsTemp.Get_Fields_Int16("cmlevel")) == 1)
					{
						modGNBas.Statics.gboolCommercial = false;
					}
				}
				if (modGlobalConstants.Statics.gboolRE)
				{
					Statics.boolShortRealEstate = false;
				}
				else
				{
					Statics.boolShortRealEstate = true;
					modGlobalVariables.Statics.boolBillingorCorrelated = true;
				}
				if (!FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("BL")) && Statics.boolShortRealEstate)
				{
					// not supposed to be here
					Application.Exit();
				}
				Statics.boolHandHeld = false;
				Statics.boolOldHH = true;
				if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("RH")))
				{
					dttemp = (DateTime)clsTemp.Get_Fields_DateTime("RHDate");
					if (dttemp.Year > DateTime.Now.Year)
					{
						Statics.boolHandHeld = true;
						/*? 64 */
					}
					else if (dttemp.Year == DateTime.Now.Year)
					{
						if (dttemp.Month >= DateTime.Now.Month)
						{
							Statics.boolHandHeld = true;
						}
					}
					if (Statics.boolHandHeld)
					{
						if (Conversion.Val(clsTemp.Get_Fields_Int16("RHLevel")) == 1)
						{
							Statics.boolOldHH = false;
						}
						else
						{
							Statics.boolOldHH = true;
						}
					}
				}
				else
				{
					Statics.boolHandHeld = false;
				}
				Statics.boolAllowXF = false;
				if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("XF")))
				{
					dttemp = (DateTime)clsTemp.Get_Fields_DateTime("xfdate");
					if (dttemp.Year > DateTime.Now.Year)
					{
						Statics.boolAllowXF = true;
						Statics.intXFLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("xflevel"))));
					}
					else if (dttemp.Year == DateTime.Now.Year)
					{
						if (dttemp.Month >= DateTime.Now.Month)
						{
							Statics.boolAllowXF = true;
							Statics.intXFLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("xflevel"))));
						}
					}
				}
				modGlobalRoutines.LoadCustomizedInfo();
                modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
				modGlobalConstants.Statics.clsSecurityClass.Init("RE", modGlobalVariables.Statics.strGNDatabase);
				MDIParent.InstancePtr.Init();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In main function. Line " + Information.Erl(), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				modReplaceWorkFiles.EntryFlagFile(true, "RE", true);
                App.MainForm.OpenModule("TWGNENTY");
                Application.Exit();
			}
		}
       
		public static void OpenSRMasterTable_39(ref clsDRWrapper rsRecordSet, object lngSaleID = null)
		{
			OpenSRMasterTable(ref rsRecordSet, null, null, "", lngSaleID);
		}

		public static void OpenSRMasterTable_54(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "", object lngSaleID = null)
		{
			OpenSRMasterTable(ref rsRecordSet, intAccount, intCard, stdate, lngSaleID);
		}

		public static void OpenSRMasterTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "", object lngSaleID = null)
		{
			string strDate = "";
			try
			{
				if (rsRecordSet == null)
				{
					rsRecordSet = new clsDRWrapper();
				}
				if (!Information.IsNothing(stdate))
				{
					if (Information.IsDate(stdate))
					{
						strDate = stdate;
					}
					else
					{
						strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
					}
				}
				else
				{
					strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
				}
				modGlobalVariables.Statics.gcurrsaledate = strDate;
				if (!Information.IsNothing(intAccount))
				{
					if (!Information.IsNothing(intCard))
					{
						if (!Information.IsNothing(lngSaleID))
						{
							rsRecordSet.OpenRecordset("select * from srmaster where rscard = " + intCard + " and saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("SELECT * FROM SRMaster where RSAccount = " + FCConvert.ToString(intAccount) + " and rscard = " + intCard + " and saledate = '" + FCConvert.ToString(DateAndTime.DateValue(strDate)) + "'", modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						if (!Information.IsNothing(lngSaleID))
						{
							rsRecordSet.OpenRecordset("select * from srmaster where saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("SELECT * FROM SRMaster where RSAccount = " + FCConvert.ToString(intAccount), modGlobalVariables.strREDatabase);
						}
					}
				}
				else
				{
					if (!Information.IsNothing(lngSaleID))
					{
						rsRecordSet.OpenRecordset("select * from srmaster where saleid = " + FCConvert.ToString(lngSaleID), modGlobalVariables.strREDatabase);
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM SRMaster", modGlobalVariables.strREDatabase);
					}
				}
				// End If
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenSRMasterTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void SaveSRMasterTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "", object lngSaleID = null)
		{
			try
			{
				rsRecordSet.Update();
				if (!Information.IsNothing(lngSaleID))
				{
					OpenSRMasterTable_54(ref rsRecordSet, intAccount, intCard, "", lngSaleID);
				}
				else
				{
					if (Information.IsDate(stdate + ""))
					{
						OpenSRMasterTable(ref rsRecordSet, intAccount, intCard, stdate);
					}
					else
					{
						OpenSRMasterTable(ref rsRecordSet, intAccount, intCard);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveSRMasterTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void OpenMasterTable_9(ref clsDRWrapper rsRecordSet, object intAccount = null, string strSale = "", object lngSaleID = null)
		{
			OpenMasterTable(ref rsRecordSet, intAccount, null, strSale, lngSaleID);
		}

		public static void OpenMasterTable_27(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, object lngSaleID = null)
		{
			OpenMasterTable(ref rsRecordSet, intAccount, intCard, "", lngSaleID);
		}

		public static void OpenMasterTable_36(ref clsDRWrapper rsRecordSet, object intAccount = null, object lngSaleID = null)
		{
			OpenMasterTable(ref rsRecordSet, intAccount, null, "", lngSaleID);
		}

		public static void OpenMasterTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string strSale = "", object lngSaleID = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (rsRecordSet == null)
				{
					rsRecordSet = new clsDRWrapper();
				}
				// Set MRDB = Nothing
				// If ff.FileExists(strREDatabase) = True Then
				// Set MRDB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (!Information.IsNothing(intAccount))
					{
						if (!Information.IsNothing(intCard))
						{
							rsRecordSet.OpenRecordset("SELECT * FROM master where RSAccount = " + intAccount + " and RSCard = " + intCard, modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("SELECT * FROM master where RSAccount = " + intAccount + " order by rscard", modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM master where not rsdeleted = 1", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					if (!Information.IsNothing(lngSaleID))
					{
						if (!Information.IsNothing(intCard))
						{
							rsRecordSet.OpenRecordset("select * from srmaster where saleid = " + lngSaleID + " and rscard = " + intCard, modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("select * from srmaster where saleid = " + lngSaleID + " order by rscard", modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						if (!Information.IsNothing(intCard))
						{
							rsRecordSet.OpenRecordset("select * from srmaster where rsaccount = " + intAccount + " and rscard = " + intCard + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("select * from srmaster where rsaccount = " + intAccount + " and saledate like '%" + Strings.Trim(modGlobalVariables.Statics.gcurrsaledate) + "%'", modGlobalVariables.strREDatabase);
						}
					}
				}

				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
					modGlobalVariables.Statics.HoldDwelling = rsRecordSet.Get_Fields_String("rsdwellingcode");
				}
				else
				{
					rsRecordSet.AddNew();
					modGlobalVariables.Statics.HoldDwelling = "N";
					if (modGlobalVariables.Statics.boolRegRecords && !Information.IsNothing(intCard))
					{
						cCreateGUID tGuid = new cCreateGUID();
						rsRecordSet.Set_Fields("CardID", tGuid.CreateGUID());
						// TODO Get_Fields: Check the table for the column [CardID] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(intCard) == 1)
							rsRecordSet.Set_Fields("AccountID", rsRecordSet.Get_Fields("CardID"));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " in OpenMasterTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As object	OnWrite(int, object)
		// vbPorter upgrade warning: intCard As object	OnWrite(int, object)
		public static void SaveMasterTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				OpenMasterTable(ref rsRecordSet, intAccount, intCard);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveMasterTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant --> As int
		// vbPorter upgrade warning: strSale As Variant --> As string
		public static void OpenOutBuildingTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string strSale = "")
		{
			// Dim ff As New FCFileSystemObject
			try
			{
				// On Error GoTo ErrorHandler
				// If ff.FileExists(strREDatabase) = True Then
				if (rsRecordSet == null)
				{
					rsRecordSet = new clsDRWrapper();
				}
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (!Information.IsNothing(intAccount))
					{
						if (!Information.IsNothing(intCard))
						{
							rsRecordSet.OpenRecordset("SELECT * FROM outbuilding where RSAccount = " + FCConvert.ToString(intAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("SELECT * FROM OUTBUILDING where RSAccount = " + FCConvert.ToString(intAccount), modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM OutBuilding", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsRecordSet.OpenRecordset("select * from sroutbuilding where rsaccount = " + FCConvert.ToString(intAccount) + " and rscard = " + FCConvert.ToString(intCard) + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
				}
				// End If
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenOutbuildingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		public static void SaveOutBuildingTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				OpenOutBuildingTable(ref rsRecordSet, intAccount, intCard);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveOutbuildingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Sub OpenIncomeTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount, Optional ByVal intCard)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set INCVBDB = Nothing
		//
		// If ff.FileExists(strredatabase) = True Then
		// Set INCVBDB = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		// If Not IsMissing(intAccount) Then
		// If Not IsMissing(intCard) Then
		// Set rsRecordSet = INCVBDB.OpenRecordset("SELECT * FROM Income where RSAccount = " & intAccount & " and RSCard = " & intCard,strredatabase)
		// Else
		// Set rsRecordSet = INCVBDB.OpenRecordset("SELECT * FROM Income where RSAccount = " & intAccount,strredatabase)
		// End If
		// Else
		// Set rsRecordSet = INCVBDB.OpenRecordset("SELECT * FROM Income",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		//
		// End Sub
		//
		// Public Sub SaveIncomeTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount, Optional ByVal intCard)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenIncomeTable(rsRecordSet, intAccount, intCard)
		// End Sub
		// vbPorter upgrade warning: intAccount As object	OnWrite(int, object)
		// vbPorter upgrade warning: intCard As object	OnWrite(int, object)
		// vbPorter upgrade warning: intsale As Variant --> As string
		public static void OpenCOMMERCIALTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string intsale = "")
		{
			// Dim ff As New FCFileSystemObject
			try
			{
				// On Error GoTo ErrorHandler
				// Set rsRecordSet = Nothing
				if (rsRecordSet == null)
				{
					rsRecordSet = new clsDRWrapper();
				}
				// Set CMRDB = Nothing
				// If ff.FileExists(strREDatabase) = True Then
				// Set CMRDB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (!Information.IsNothing(intAccount))
					{
						if (!Information.IsNothing(intCard))
						{
							rsRecordSet.OpenRecordset("SELECT * FROM Commercial where RSAccount = " + intAccount + " and RSCard = " + intCard, modGlobalVariables.strREDatabase);
						}
						else
						{
							// COREY
							rsRecordSet.OpenRecordset("SELECT * FROM COMMERCIAL where RSAccount = " + intAccount, modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM Commercial", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsRecordSet.OpenRecordset("select * from srcommercial where rsaccount = " + intAccount + " and rscard = " + intCard + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
				}
				// End If
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenCommercialTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		public static void SaveCommercialTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				OpenCOMMERCIALTable(ref rsRecordSet, intAccount, intCard);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveCommercialTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Sub OpenDRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set DRDB = Nothing
		//
		// If ff.FileExists(strredatabase) = True Then
		// Set DRDB = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = DRDB.OpenRecordset("SELECT * FROM DR where DRKey = '" & Format(intAccount, "000") & "'",strredatabase)
		// Else
		// Set rsRecordSet = DRDB.OpenRecordset("SELECT * FROM DR",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		//
		// End Sub
		//
		// Public Sub SaveDRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenDRTable(rsRecordSet, intAccount)
		// End Sub
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As object	OnWrite(int, object)
		// vbPorter upgrade warning: intsale As Variant --> As string
		public static void OpenDwellingTable_27(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string strGuid = "")
		{
			OpenDwellingTable(ref rsRecordSet, intAccount, intCard, "", strGuid);
		}

		public static void OpenDwellingTable_36(ref clsDRWrapper rsRecordSet, object intAccount = null, string strGuid = "")
		{
			OpenDwellingTable(ref rsRecordSet, intAccount, null, "", strGuid);
		}

		public static void OpenDwellingTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string intsale = "", string strGuid = "")
		{
			// Dim ff As New FCFileSystemObject
			try
			{
				// On Error GoTo ErrorHandler
				if (rsRecordSet == null)
				{
					rsRecordSet = new clsDRWrapper();
				}
				// If ff.FileExists(strREDatabase) = True Then
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (!Information.IsNothing(intAccount))
					{
						if (!Information.IsNothing(intCard))
						{
							rsRecordSet.OpenRecordset("SELECT * FROM dwelling where RSAccount = " + FCConvert.ToString(intAccount) + " and RSCard = " + intCard, modGlobalVariables.strREDatabase);
						}
						else
						{
							rsRecordSet.OpenRecordset("SELECT * FROM Dwelling where RSAccount = " + FCConvert.ToString(intAccount), modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM Dwelling", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsRecordSet.OpenRecordset("select * from srdwelling where rsaccount = " + FCConvert.ToString(intAccount) + " and rscard = " + intCard + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
				}
				// End If
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
					// now put in some defaults
					rsRecordSet.Set_Fields("diUNITSOTHER", 0);
					rsRecordSet.Set_Fields("disfmasonry", 0);
					rsRecordSet.Set_Fields("diopen3", 0);
					rsRecordSet.Set_Fields("diopen4", 0);
					rsRecordSet.Set_Fields("dibsmtgar", 0);
					rsRecordSet.Set_Fields("disfbsmtliving", 0);
					rsRecordSet.Set_Fields("dibsmtfingrade1", 0);
					rsRecordSet.Set_Fields("dibsmtfingrade2", 0);
					rsRecordSet.Set_Fields("diopen5", 0);
					rsRecordSet.Set_Fields("dipctheat", 100);
					rsRecordSet.Set_Fields("dicool", 9);
					rsRecordSet.Set_Fields("dipctcool", 0);
					rsRecordSet.Set_Fields("dihalfbaths", 0);
					rsRecordSet.Set_Fields("diaddnfixtures", 0);
					rsRecordSet.Set_Fields("difireplaces", 0);
					rsRecordSet.Set_Fields("dipctunfinished", 0);
					rsRecordSet.Set_Fields("dipctphys", 0);
					rsRecordSet.Set_Fields("dipctfunct", 100);
					rsRecordSet.Set_Fields("difunctcode", 9);
					rsRecordSet.Set_Fields("dipctecon", 100);
					rsRecordSet.Set_Fields("dieconcode", 9);
					if (strGuid != "" && modGlobalVariables.Statics.boolRegRecords)
					{
						rsRecordSet.Set_Fields("AccountID", strGuid);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenDwellingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		public static void SaveDwellingTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				OpenDwellingTable(ref rsRecordSet, intAccount, intCard);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveDwellingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant --> As int
		// vbPorter upgrade warning: intpic As Variant, short --> As int
		//public static void OpenPICTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, object intpic = null)
		//{
		//	try
		//	{
		//		// On Error GoTo ErrorHandler
		//		/*? 2 */// Set PICDB = Nothing
		//		if (rsRecordSet == null)
		//		{
		//			rsRecordSet = new clsDRWrapper();
		//		}
		//		/*? 3 */// If ff.FileExists(strREDatabase) = True Then
		//		/*? 4 */// Set PICDB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//		if (!Information.IsNothing(intAccount))
		//		{
		//			if (!Information.IsNothing(intCard))
		//			{
		//				if (!Information.IsNothing(intpic))
		//				{
		//					rsRecordSet.OpenRecordset("SELECT * FROM PictureRecord where MRACCOUNTNUMBER = " + FCConvert.ToString(intAccount) + " and RSCard = " + FCConvert.ToString(intCard) + " and PicNum = " + FCConvert.ToString(intpic), modGlobalVariables.strREDatabase);
		//				}
		//				else
		//				{
		//					rsRecordSet.OpenRecordset("SELECT * FROM PictureRecord where MRACCOUNTNUMBER = " + FCConvert.ToString(intAccount) + " and RSCard = " + FCConvert.ToString(intCard) + " order by picnum", modGlobalVariables.strREDatabase);
		//				}
		//			}
		//			else
		//			{
		//				rsRecordSet.OpenRecordset("SELECT * FROM PictureRecord where MRACCOUNTNUMBER = " + FCConvert.ToString(intAccount) + " order by picnum", modGlobalVariables.strREDatabase);
		//			}
		//		}
		//		else
		//		{
		//			rsRecordSet.OpenRecordset("SELECT * FROM PictureRecord order my mraccountnumber,picnum", modGlobalVariables.strREDatabase);
		//		}
		//		// End If
		//		if (!rsRecordSet.EndOfFile())
		//		{
		//			rsRecordSet.Edit();
		//		}
		//		else
		//		{
		//			// 13        rsRecordSet.AddNew
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ErrorHandler:
		//		MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in OpenPicTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		// vbPorter upgrade warning: intpic As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		//public static void SavePICTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, object intpic = null)
		//{
		//	try
		//	{
		//		// On Error GoTo ErrorHandler
		//		rsRecordSet.Update();
		//		OpenPICTable(ref rsRecordSet, intAccount, intCard, intpic);
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ErrorHandler:
		//		MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " In line " + Information.Erl() + " in SavePICTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}

		public static int GetCardsUsed()
		{
			int GetCardsUsed = 0;
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngCards = 0;
			GetCardsUsed = 0;
			clsTemp.OpenRecordset("select count(rsaccount) as thecount from master", modGlobalVariables.strREDatabase);
			if (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				lngCards = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
			}
			else
			{
				lngCards = 0;
			}
			GetCardsUsed = lngCards;
			return GetCardsUsed;
		}
		// Public Sub OpenWRETable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set wreDB = Nothing
		// If ff.FileExists(strgndatabase) = True Then
		// Set wreDB = OpenDatabase(strgndatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = wreDB.OpenRecordset("SELECT * FROM WRE where WRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = wreDB.OpenRecordset("SELECT * FROM WRE",strredatabase)
		// End If
		//
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error number " & Err.Number & " " & Err.Description & " occured in OpenWRETable.", vbCritical
		//
		// End Sub
		// Public Sub SaveWRETable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenWRETable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenWWKTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Dim dbTemp As DAO.Database
		// Dim tbl As DAO.TableDef
		// Dim fld As DAO.Field
		//
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set wwkDB = Nothing
		// If ff.FileExists(strgndatabase) = True Then
		// Set wwkDB = OpenDatabase(strgndatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		// Set dbTemp = OpenDatabase(strgndatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		// For Each tbl In dbTemp.TableDefs
		// If UCase(tbl.Name) = "REAL ESTATE WWK" Then
		//
		// Exit For
		// End If
		// Next
		//
		// If Not FieldExists(tbl, "PicDirectory") Then
		// Set fld = tbl.CreateField("PicDirectory", dbText)
		// fld.AllowZeroLength = True
		// Call tbl.Fields.Append(fld)
		// End If
		//
		//
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordset = wwkDB.OpenRecordset("SELECT * FROM [Real Estate WWK] where RSAccount = " & intAccount)
		// Else
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM [Real Estate WWK]",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "error number " & Err.Number & " " & Err.Description & " in OpenWWKTable.", vbCritical
		// End Sub
		//
		// Public Sub SaveWWKTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenWWKTable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenWW2Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set wwkDB = Nothing
		// If ff.FileExists(strgndatabase) = True Then
		// Set wwkDB = OpenDatabase(strgndatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM WWORKREC2 where WRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM WWORKREC2",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error number " & Err.Number & " " & Err.Description & " occured in OpenWW2Table.", vbCritical
		//
		// End Sub
		// Public Sub SaveWW2Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenWWKTable(rsRecordSet, intAccount)
		// End Sub
		//
		// Public Sub OpenWW3Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set wwkDB = Nothing
		// If ff.FileExists(strgndatabase) = True Then
		// Set wwkDB = OpenDatabase(strgndatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM WWORKREC3 where WRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM WWORKREC3",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error number " & Err.Number & " " & Err.Description & " occured in OpenWW3Table.", vbCritical
		//
		// End Sub
		// Public Sub SaveWW3Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenWWKTable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenWW4Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set wwkDB = Nothing
		// If ff.FileExists(strgndatabase) = True Then
		// Set wwkDB = OpenDatabase(strgndatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM WWORKREC4 where WRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = wwkDB.OpenRecordset("SELECT * FROM WWORKREC4",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error number " & Err.Number & " " & Err.Description & " occured in OpenWW4Table."
		//
		// End Sub
		// Public Sub SaveWW4Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenWWKTable(rsRecordSet, intAccount)
		// End Sub
		public static void OpenLSTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			if (System.IO.File.Exists(modGlobalVariables.strREDatabase) == true)
			{
				rsRecordSet = new clsDRWrapper();
				if (!Information.IsNothing(intAccount))
				{
					rsRecordSet.OpenRecordset("SELECT * FROM LISTRECORD where LRecordNumber = " + intAccount, modGlobalVariables.strREDatabase);
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM LISTRECORD", modGlobalVariables.strREDatabase);
				}
			}
			if (!rsRecordSet.EndOfFile())
			{
				rsRecordSet.Edit();
			}
			else
			{
				rsRecordSet.AddNew();
			}
		}

		public static void SaveLSTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			rsRecordSet.Update();
			OpenLSTable(ref rsRecordSet, intAccount);
		}
		// Public Sub OpenMHTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set MHDB = Nothing
		// If ff.FileExists(strredatabase) = True Then
		// Set MHDB = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = MHDB.OpenRecordset("SELECT * FROM MASTRECORD where MRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = MHDB.OpenRecordset("SELECT * FROM MASTRECORD",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// End Sub
		//
		// Public Sub SaveMHTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenMHTable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenCMRArrayTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set CMRArrayDB = Nothing
		// If ff.FileExists(strredatabase) = True Then
		// Set CMRArrayDB = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = CMRArrayDB.OpenRecordset("SELECT * FROM RECOMMRECORD where RecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = CMRArrayDB.OpenRecordset("SELECT * FROM RECOMMRECORD",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// End Sub
		//
		// Public Sub SaveCMRArrayTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenCMRArrayTable(rsRecordSet, intAccount)
		// End Sub
		// vbPorter upgrade warning: intAccount As object	OnWrite(int, object)
		// vbPorter upgrade warning: intCard As object	OnWrite(int, object)
		public static void OpenCCTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			// Dim ff As New FCFileSystemObject
			// Set rsRecordSet = Nothing
			if (rsRecordSet == null)
			{
				rsRecordSet = new clsDRWrapper();
			}
			if (!Information.IsNothing(intAccount))
			{
				if (!Information.IsNothing(intCard))
				{
					rsRecordSet.OpenRecordset("SELECT * FROM COMMREC where CRecordNumber = " + intAccount + " and RSCard = " + intCard + " order by RSCard", modGlobalVariables.strREDatabase);
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM COMMREC where CRecordNumber = " + intAccount, modGlobalVariables.strREDatabase);
				}
			}
			else
			{
				rsRecordSet.OpenRecordset("SELECT * FROM COMMREC", modGlobalVariables.strREDatabase);
			}
			if (!rsRecordSet.EndOfFile())
			{
				rsRecordSet.Edit();
			}
			else
			{
				rsRecordSet.AddNew();
			}
		}

		public static void SaveCCTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			rsRecordSet.Update();
			OpenCCTable(ref rsRecordSet, intAccount, intCard);
		}
		// vbPorter upgrade warning: intAccount As object	OnWrite(int, object, double)
		public static void OpenCRTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet = null;
				rsRecordSet = new clsDRWrapper();
				if (!Information.IsNothing(intAccount))
				{
					// If Not IsMissing(Rectype) Then
					// Set rsRecordset = CRDB.OpenRecordset("select * from costrecord where crecordnumber = " & intAccount, Rectype, 0, dbOptimistic)
					// Else
					rsRecordSet.OpenRecordset("SELECT * FROM COSTRECORD where CRecordNumber = " + intAccount, modGlobalVariables.strREDatabase);
					// End If
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM COSTRECORD order by crecordnumber", modGlobalVariables.strREDatabase);
				}
				// If IsMissing(Rectype) Then
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				// End If
				// If Not IsMissing(Rectype) Then
				// If ((Rectype <> dbOpenForwardOnly) And (Rectype <> dbOpenSnapshot)) Then
				// If Not rsRecordset.eof Then
				// rsRecordset.Edit
				// Else
				// rsRecordset.AddNew
				// End If
				// End If
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenCRTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void SaveCRTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			// Set rsRecordset = CR
			rsRecordSet.Update();
			OpenCRTable(ref rsRecordSet, intAccount);
		}
		// Public Sub OpenCOMMERCIALCOSTTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set CRDB = Nothing
		// If ff.FileExists(strREDatabase) = True Then
		// Set CRDB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = CRDB.OpenRecordset("SELECT * FROM COMMERCIALCOST where CRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = CRDB.OpenRecordset("SELECT * FROM COMMERCIALCOST",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// End Sub
		//
		// Public Sub SaveCOMMERCIALCOSTTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// Set rsRecordset = CR
		// rsRecordSet.Update
		// Call OpenCOMMERCIALCOSTTable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenLS2Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set LS2DB = Nothing
		// If ff.FileExists(strredatabase) = True Then
		// Set LS2DB = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = LS2DB.OpenRecordset("SELECT * FROM LANDSRECORD where LRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = LS2DB.OpenRecordset("SELECT * FROM LANDSRECORD",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// End Sub
		//
		// Public Sub SaveLS2Table(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenLS2Table(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenRSZTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// Set rsRecordSet = Nothing
		// Set RSZDB = Nothing
		// If ff.FileExists(strredatabase) = True Then
		// Set RSZDB = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = RSZDB.OpenRecordset("SELECT * FROM RSZRECORD where RRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = RSZDB.OpenRecordset("SELECT * FROM RSZRECORD",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// End Sub
		//
		// Public Sub SaveRSZTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenRSZTable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenSRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount, Optional ByVal intCard)
		// Dim ff As New FCFileSystemObject
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set SRDB = Nothing
		// If ff.FileExists(strREDatabase) = True Then
		// Set SRDB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		// If Not IsMissing(intAccount) Then
		// If Not IsMissing(intCard) Then
		// Set rsRecordSet = SRDB.OpenRecordset("SELECT * FROM SUMMRECORD where SRecordNumber = " & intAccount & " and cardnumber = " & intCard,strredatabase)
		// Else
		// Set rsRecordSet = SRDB.OpenRecordset("select * from summrecord where srecordnumber = " & intAccount & " order by cardnumber",strredatabase)
		// End If
		// Else
		// Set rsRecordSet = SRDB.OpenRecordset("SELECT * FROM SUMMRECORD order by srecordnumber,cardnumber",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error number " & Err.Number & " " & Err.Description & " occured in OpenSRTable.", vbCritical
		//
		// End Sub
		//
		// Public Sub SaveSRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount, Optional ByVal intCard)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// If IsMissing(intAccount) Then
		// Call OpenSRTable(rsRecordSet)
		// Else
		// Call OpenSRTable(rsRecordSet, intAccount, intCard)
		// End If
		// End Sub
		// vbPorter upgrade warning: intAccount As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		public static void OpenHRTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			// Set rsRecordSet = Nothing
			if (rsRecordSet == null)
			{
				rsRecordSet = new clsDRWrapper();
			}
			if (!Information.IsNothing(intAccount))
			{
				rsRecordSet.OpenRecordset("SELECT * FROM HelpRecords where HRecordNumber = " + FCConvert.ToString(intAccount), modGlobalVariables.strREDatabase);
			}
			else
			{
				rsRecordSet.OpenRecordset("SELECT * FROM HelpRecords", modGlobalVariables.strREDatabase);
			}
			// If Not rsRecordSet.EndOfFile Then
			// rsRecordSet.Edit
			// Else
			// rsRecordSet.AddNew
			// End If
		}
		// Public Sub SaveHRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		// Call OpenHRTable(rsRecordSet, intAccount)
		// End Sub
		// Public Sub OpenLSRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount)
		// Dim ff As New FCFileSystemObject
		// On Error GoTo ErrorHandler
		// Set rsRecordSet = Nothing
		// Set LSRDB = Nothing
		// If ff.FileExists(strREDatabase) = True Then
		// Set LSRDB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not IsMissing(intAccount) Then
		// Set rsRecordSet = LSRDB.OpenRecordset("SELECT * FROM LANDSRECORD where LRecordNumber = " & intAccount,strredatabase)
		// Else
		// Set rsRecordSet = LSRDB.OpenRecordset("SELECT * FROM LANDSRECORD",strredatabase)
		// End If
		// End If
		//
		// If Not rsRecordSet.EOF Then
		// rsRecordSet.Edit
		// Else
		// rsRecordSet.AddNew
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error number " & Err.Number & " " & Err.Description & " occured in OpenLSRTable.", vbCritical
		//
		// End Sub
		//
		// Public Sub SaveLSRTable(rsRecordSet As clsDRWrapper, Optional ByVal intAccount, Optional boolSkip As Boolean = False)
		// Dim ff As New FCFileSystemObject
		//
		// rsRecordSet.Update
		//
		// If Not boolSkip Then Call OpenLSRTable(rsRecordSet, intAccount)
		// End Sub
		public static void OpenLTRTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet = null;
				rsRecordSet = new clsDRWrapper();
				if (!Information.IsNothing(intAccount))
				{
					rsRecordSet.OpenRecordset("SELECT * FROM LANDTRECORD where LRecordNumber = " + intAccount, modGlobalVariables.strREDatabase);
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM LANDTRECORD", modGlobalVariables.strREDatabase);
				}
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenLTRTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void SaveLTRTable(ref clsDRWrapper rsRecordSet, object intAccount = null)
		{
			rsRecordSet.Update();
			OpenLTRTable(ref rsRecordSet, intAccount);
		}
		
		public static double GetRecordCount(string strTableName, string strAccount, int lngSID = 0)
		{
			double GetRecordCount = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			// If ff.FileExists(strREDatabase) = True Then
			// Set dbTemp = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				rsTemp.OpenRecordset("SELECT * FROM " + strTableName + " where " + strAccount + " = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
			}
			else
			{
				if (Strings.Left(Strings.UCase(strTableName), 2) == "SR")
				{
					// Set rsTemp = dbTemp.OpenRecordset("select * from " & strTableName & " where " & strAccount & " = " & gintLastAccountNumber & " and saledate = #" & gcurrsaledate & "#",strredatabase)
					if (lngSID > 0)
					{
						rsTemp.OpenRecordset("select max(rscard) as mcard from " + strTableName + " where " + strAccount + " = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saleid = " + FCConvert.ToString(lngSID), modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset("select max(rscard) as mcard from " + strTableName + " where " + strAccount + " = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM " + strTableName + " where " + strAccount + " = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
				}
			}
			if (!rsTemp.EndOfFile())
			{
				rsTemp.MoveLast();
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					GetRecordCount = rsTemp.RecordCount();
				}
				else
				{
					if (Strings.Left(Strings.UCase(strTableName), 2) == "SR")
					{
						// TODO Get_Fields: Field [mcard] not found!! (maybe it is an alias?)
						GetRecordCount = Conversion.Val(rsTemp.Get_Fields("mcard"));
					}
					else
					{
						GetRecordCount = rsTemp.RecordCount();
					}
					// If GetRecordCount > 1 Then GetRecordCount = 1
				}
			}
			else
			{
				GetRecordCount = 0;
			}
			// Else
			// GetRecordCount = 0
			// End If
			rsTemp = null;
			return GetRecordCount;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As int
		public static int GetMaxCards(string strTableName, string strAccount, int intAccountNumber)
		{
			int GetMaxCards = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM " + strTableName + " where " + strAccount + " = " + FCConvert.ToString(intAccountNumber), modGlobalVariables.strREDatabase);
			if (!rsTemp.EndOfFile())
			{
				rsTemp.MoveLast();
				GetMaxCards = rsTemp.RecordCount();
			}
			else
			{
				GetMaxCards = 0;
			}
			return GetMaxCards;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As int
		public static int GetTotalRecords(string strTableName)
		{
			int GetTotalRecords = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM " + strTableName, modGlobalVariables.strREDatabase);
			if (!rsTemp.EndOfFile())
			{
				rsTemp.MoveLast();
				GetTotalRecords = rsTemp.RecordCount();
			}
			else
			{
				GetTotalRecords = 0;
			}
			return GetTotalRecords;
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant --> As int
		public static void OpenSRDwellingTable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "")
		{
			string strDate = "";
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet = new clsDRWrapper();
				if (!Information.IsNothing(stdate))
				{
					if (Information.IsDate(stdate))
					{
						strDate = stdate;
					}
					else
					{
						strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
					}
				}
				else
				{
					strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
				}
				modGlobalVariables.Statics.gcurrsaledate = strDate;
				if (!Information.IsNothing(intAccount))
				{
					if (!Information.IsNothing(intCard))
					{
						// Set rsRecordSet = dbTemp.OpenRecordset("SELECT * FROM SRdwelling where RSAccount = " & intAccount & " and RSCard = " & intCard & " and saledate like '*" & strDate & "*'",strredatabase)
						rsRecordSet.OpenRecordset("SELECT * FROM SRdwelling where RSAccount = " + FCConvert.ToString(intAccount) + " and RSCard = " + FCConvert.ToString(intCard) + " and saledate = '" + strDate + "'", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM SRdwelling where RSAccount = " + FCConvert.ToString(intAccount), modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM SRdwelling", modGlobalVariables.strREDatabase);
				}
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenSRDwellingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant --> As int
		public static void opensroutbuildingtable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "")
		{
			string strDate = "";
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet = new clsDRWrapper();
				if (Information.IsNothing(stdate))
				{
					strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
				}
				else
				{
					if (Information.IsDate(stdate))
					{
						strDate = stdate;
					}
					else
					{
						strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
					}
				}
				modGlobalVariables.Statics.gcurrsaledate = strDate;
				if (!Information.IsNothing(intAccount))
				{
					if (!Information.IsNothing(intCard))
					{
						rsRecordSet.OpenRecordset("SELECT * FROM SRoutbuilding where RSAccount = " + FCConvert.ToString(intAccount) + " and RSCard = " + FCConvert.ToString(intCard) + " and saledate = '" + strDate + "'", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM SRoutbuilding where RSAccount = " + FCConvert.ToString(intAccount), modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM SRoutbuilding", modGlobalVariables.strREDatabase);
				}
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenSROutbuildingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As object	OnWrite(int, object)
		// vbPorter upgrade warning: intCard As object	OnWrite(int, object)
		public static void opensrcommercialtable_54(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "")
		{
			opensrcommercialtable(ref rsRecordSet, intAccount, intCard, stdate);
		}

		public static void opensrcommercialtable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "")
		{
			// Dim dbTemp As DAO.Database
			string strDate = "";
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet = null;
				// Set dbTemp = Nothing
				rsRecordSet = new clsDRWrapper();
				if (Information.IsNothing(stdate))
				{
					strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
				}
				else
				{
					if (Information.IsDate(stdate))
					{
						strDate = stdate;
					}
					else
					{
						strDate = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_DateTime("SaleDate"));
					}
				}
				modGlobalVariables.Statics.gcurrsaledate = strDate;
				// If ff.FileExists(strREDatabase) = True Then
				// Set dbTemp = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
				if (!Information.IsNothing(intAccount))
				{
					if (!Information.IsNothing(intCard))
					{
						rsRecordSet.OpenRecordset("SELECT * FROM SRcommercial where RSAccount = " + intAccount + " and RSCard = " + intCard + " and saledate = '" + strDate + "'", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsRecordSet.OpenRecordset("SELECT * FROM SRcommercial where RSAccount = " + intAccount, modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					rsRecordSet.OpenRecordset("SELECT * FROM SRcommercial", modGlobalVariables.strREDatabase);
				}
				// End If
				if (!rsRecordSet.EndOfFile())
				{
					rsRecordSet.Edit();
				}
				else
				{
					rsRecordSet.AddNew();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in OpenSRCommercialTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		public static void savesrdwellingtable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "")
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				if (Information.IsNothing(stdate))
				{
					OpenSRDwellingTable(ref rsRecordSet, intAccount, intCard);
				}
				else
				{
					if (Information.IsDate(stdate))
					{
						OpenSRDwellingTable(ref rsRecordSet, intAccount, intCard, stdate);
					}
					else
					{
						OpenSRDwellingTable(ref rsRecordSet, intAccount, intCard);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveSRDwellingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		public static void savesrcommercialtable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null, string stdate = "")
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				if (Information.IsNothing(stdate))
				{
					opensrcommercialtable(ref rsRecordSet, intAccount, intCard);
				}
				else
				{
					if (Information.IsDate(stdate))
					{
						opensrcommercialtable(ref rsRecordSet, intAccount, intCard, stdate);
					}
					else
					{
						opensrcommercialtable(ref rsRecordSet, intAccount, intCard);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveSRCommercialTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intAccount As Variant --> As int
		// vbPorter upgrade warning: intCard As Variant, short --> As int
		public static void savesroutbuildingtable(ref clsDRWrapper rsRecordSet, object intAccount = null, object intCard = null)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsRecordSet.Update();
				opensroutbuildingtable(ref rsRecordSet, intAccount, intCard);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured in SaveSROutbuildingTable.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static string GetMasterJoin()
		{
			string GetMasterJoin = "";
			string strReturn;
			strReturn = "select master.*, master.deedname1 as RSName,master.deedname2 as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.country as country, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.ID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.ID)";
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetMasterJoin = strReturn;
			return GetMasterJoin;
		}

		public static string GetMasterJoinForJoin()
		{
			string GetMasterJoinForJoin = "";
			string strReturn;
			strReturn = "select master.*, master.deedname1 as RSName,master.deedname2 as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4,cpo.country as country, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.PartyID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.PartyID)";
			strReturn = " (" + strReturn + ") mparty ";
			GetMasterJoinForJoin = strReturn;
			return GetMasterJoinForJoin;
		}

		public static bool ChangePasswordNow(ref cSecurityUser tUser)
		{
			bool ChangePasswordNow = false;
			ChangePasswordNow = false;
			if (tUser == null)
				return ChangePasswordNow;
			string strPass;
			string strCalcHash;
			strPass = frmUpdatePassword.InstancePtr.Init();
			if (strPass == "")
			{
				return ChangePasswordNow;
			}
			// If frmSecurity.GoodPassword Then
			//TODO
			if (tUser.PasswordHash == string.Empty)
			{
				// goto EditPassword;
			}
			return ChangePasswordNow;
		}

		public class StaticVariables
		{
            //=========================================================
            public bool moduleInitialized = false;
            public bool boolShortRealEstate;
			public bool boolHandHeld;
			public bool boolOldHH;
			public bool boolAllowXF;
			public int intXFLevel;
			public string strDbCP = string.Empty;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
