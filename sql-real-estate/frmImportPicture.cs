﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportPicture.
	/// </summary>
	public partial class frmImportPicture : BaseForm
	{
		public frmImportPicture()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportPicture InstancePtr
		{
			get
			{
				return (frmImportPicture)Sys.GetInstance(typeof(frmImportPicture));
			}
		}

		protected frmImportPicture _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// vbPorter upgrade warning: strReturn As string	OnWrite(string, short)
		string strReturn;

		private void frmImportPicture_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportPicture_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportPicture properties;
			//frmImportPicture.ScaleWidth	= 3885;
			//frmImportPicture.ScaleHeight	= 2250;
			//frmImportPicture.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public string Init()
		{
			string Init = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			strReturn = "";
			clsLoad.OpenRecordset("select * from defaultpicturelocations", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				cmbDirectory.AddItem(clsLoad.Get_Fields_String("location"));
				clsLoad.MoveNext();
			}
			cmbDirectory.SelectedIndex = 0;
			//Application.DoEvents();
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
			DialogResult intReturn;
			if (chkCreate.CheckState == Wisej.Web.CheckState.Checked)
			{
				strReturn = FCConvert.ToString(1);
			}
			else
			{
				strReturn = FCConvert.ToString(0);
			}
			strReturn += "," + cmbDirectory.Text;
			if (chkDelete.CheckState == Wisej.Web.CheckState.Checked)
			{
				//Application.DoEvents();
				intReturn = MessageBox.Show("The options you have chosen will delete the original pictures." + "\r\n" + "Are you sure you want to delete the original pictures?", "Delete Pictures?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
				if (intReturn == DialogResult.Yes)
				{
					strReturn += ",1";
				}
				else
				{
					strReturn += ",0";
				}
			}
			else
			{
				strReturn += ",0";
			}
			this.Unload();
		}
	}
}
