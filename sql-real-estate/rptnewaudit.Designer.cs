﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptaudit.
	/// </summary>
	partial class rptaudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptaudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmaplot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtexemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtfinaltotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotbuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotexemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotassessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotalaccts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalnumaccts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotaltotallyexempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotnumtotexempts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtmuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtorderby = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.grpbinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtgroupname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtsubland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtsubbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtsubexempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtsubtotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumAccts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtnumtotexempts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfinaltotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotbuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotexemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotassessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotalaccts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalnumaccts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotallyexempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotnumtotexempts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtorderby)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.grpbinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtgroupname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubexempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubtotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumAccts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnumtotexempts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtname,
				this.txtAccount,
				this.txtmaplot,
				this.txtland,
				this.txtbldg,
				this.txtexemption,
				this.txttotal
			});
			this.Detail.Height = 0.4166667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
			this.Detail.AfterPrint += new System.EventHandler(this.Detail_AfterPrint);
			// 
			// txtname
			// 
			this.txtname.CanGrow = false;
			this.txtname.Height = 0.1875F;
			this.txtname.Left = 1.25F;
			this.txtname.MultiLine = false;
			this.txtname.Name = "txtname";
			this.txtname.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtname.Text = null;
			this.txtname.Top = 0F;
			this.txtname.Width = 2.6875F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.125F;
			// 
			// txtmaplot
			// 
			this.txtmaplot.CanGrow = false;
			this.txtmaplot.Height = 0.1875F;
			this.txtmaplot.Left = 1.25F;
			this.txtmaplot.MultiLine = false;
			this.txtmaplot.Name = "txtmaplot";
			this.txtmaplot.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmaplot.Text = null;
			this.txtmaplot.Top = 0.1875F;
			this.txtmaplot.Width = 1.5F;
			// 
			// txtland
			// 
			this.txtland.CanGrow = false;
			this.txtland.Height = 0.1875F;
			this.txtland.Left = 2.875F;
			this.txtland.MultiLine = false;
			this.txtland.Name = "txtland";
			this.txtland.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtland.Text = null;
			this.txtland.Top = 0.1875F;
			this.txtland.Width = 1.0625F;
			// 
			// txtbldg
			// 
			this.txtbldg.CanGrow = false;
			this.txtbldg.Height = 0.1875F;
			this.txtbldg.Left = 4F;
			this.txtbldg.MultiLine = false;
			this.txtbldg.Name = "txtbldg";
			this.txtbldg.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtbldg.Text = null;
			this.txtbldg.Top = 0.1875F;
			this.txtbldg.Width = 1.25F;
			// 
			// txtexemption
			// 
			this.txtexemption.CanGrow = false;
			this.txtexemption.Height = 0.1875F;
			this.txtexemption.Left = 5.3125F;
			this.txtexemption.MultiLine = false;
			this.txtexemption.Name = "txtexemption";
			this.txtexemption.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtexemption.Text = null;
			this.txtexemption.Top = 0.1875F;
			this.txtexemption.Width = 1F;
			// 
			// txttotal
			// 
			this.txttotal.CanGrow = false;
			this.txttotal.Height = 0.1875F;
			this.txttotal.Left = 6.375F;
			this.txttotal.MultiLine = false;
			this.txttotal.Name = "txttotal";
			this.txttotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txttotal.Text = null;
			this.txttotal.Top = 0.1875F;
			this.txttotal.Width = 1.0625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtfinaltotal,
				this.txttotland,
				this.txttotbuilding,
				this.txttotexemption,
				this.txttotassessment,
				this.txttotalaccts,
				this.txtTotalnumaccts,
				this.txttotaltotallyexempt,
				this.txttotnumtotexempts,
				this.SubReport1
			});
			this.ReportFooter.Height = 0.8125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtfinaltotal
			// 
			this.txtfinaltotal.Height = 0.1875F;
			this.txtfinaltotal.Left = 0.0625F;
			this.txtfinaltotal.Name = "txtfinaltotal";
			this.txtfinaltotal.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtfinaltotal.Text = "Final Total";
			this.txtfinaltotal.Top = 0.3125F;
			this.txtfinaltotal.Visible = false;
			this.txtfinaltotal.Width = 1F;
			// 
			// txttotland
			// 
			this.txttotland.Height = 0.1875F;
			this.txttotland.Left = 2.6875F;
			this.txttotland.Name = "txttotland";
			this.txttotland.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txttotland.Text = null;
			this.txttotland.Top = 0.3125F;
			this.txttotland.Visible = false;
			this.txttotland.Width = 1.25F;
			// 
			// txttotbuilding
			// 
			this.txttotbuilding.Height = 0.1875F;
			this.txttotbuilding.Left = 4F;
			this.txttotbuilding.Name = "txttotbuilding";
			this.txttotbuilding.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txttotbuilding.Text = null;
			this.txttotbuilding.Top = 0.3125F;
			this.txttotbuilding.Visible = false;
			this.txttotbuilding.Width = 1.1875F;
			// 
			// txttotexemption
			// 
			this.txttotexemption.Height = 0.1875F;
			this.txttotexemption.Left = 5.1875F;
			this.txttotexemption.Name = "txttotexemption";
			this.txttotexemption.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txttotexemption.Text = null;
			this.txttotexemption.Top = 0.3125F;
			this.txttotexemption.Visible = false;
			this.txttotexemption.Width = 1.125F;
			// 
			// txttotassessment
			// 
			this.txttotassessment.Height = 0.1875F;
			this.txttotassessment.Left = 6.3125F;
			this.txttotassessment.Name = "txttotassessment";
			this.txttotassessment.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txttotassessment.Text = null;
			this.txttotassessment.Top = 0.3125F;
			this.txttotassessment.Visible = false;
			this.txttotassessment.Width = 1.125F;
			// 
			// txttotalaccts
			// 
			this.txttotalaccts.Height = 0.1875F;
			this.txttotalaccts.Left = 0.0625F;
			this.txttotalaccts.Name = "txttotalaccts";
			this.txttotalaccts.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txttotalaccts.Text = "Total Accts.";
			this.txttotalaccts.Top = 0.5625F;
			this.txttotalaccts.Visible = false;
			this.txttotalaccts.Width = 0.75F;
			// 
			// txtTotalnumaccts
			// 
			this.txtTotalnumaccts.Height = 0.1875F;
			this.txtTotalnumaccts.Left = 0.9375F;
			this.txtTotalnumaccts.Name = "txtTotalnumaccts";
			this.txtTotalnumaccts.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtTotalnumaccts.Text = null;
			this.txtTotalnumaccts.Top = 0.5625F;
			this.txtTotalnumaccts.Visible = false;
			this.txtTotalnumaccts.Width = 0.6875F;
			// 
			// txttotaltotallyexempt
			// 
			this.txttotaltotallyexempt.Height = 0.1875F;
			this.txttotaltotallyexempt.Left = 1.6875F;
			this.txttotaltotallyexempt.Name = "txttotaltotallyexempt";
			this.txttotaltotallyexempt.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txttotaltotallyexempt.Text = " Totally Exempt Accts";
			this.txttotaltotallyexempt.Top = 0.5625F;
			this.txttotaltotallyexempt.Visible = false;
			this.txttotaltotallyexempt.Width = 1.4375F;
			// 
			// txttotnumtotexempts
			// 
			this.txttotnumtotexempts.Height = 0.1875F;
			this.txttotnumtotexempts.Left = 3.1875F;
			this.txttotnumtotexempts.Name = "txttotnumtotexempts";
			this.txttotnumtotexempts.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txttotnumtotexempts.Text = null;
			this.txttotnumtotexempts.Top = 0.5625F;
			this.txttotnumtotexempts.Visible = false;
			this.txttotnumtotexempts.Width = 0.75F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.125F;
			this.SubReport1.Width = 7.5F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.txtmuni,
				this.Field1,
				this.Field2,
				this.txtdate,
				this.txtpage,
				this.txtorderby,
				this.txtTime
			});
			this.PageHeader.Height = 0.6666667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label1.Text = "-LAND-";
			this.Label1.Top = 0.4375F;
			this.Label1.Width = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label2.Text = "-BUILDINGS-";
			this.Label2.Top = 0.4375F;
			this.Label2.Width = 1.1875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.3125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label3.Text = "-EXEMPTION-";
			this.Label3.Top = 0.4375F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label4.Text = "ASSESSMENT";
			this.Label4.Top = 0.4375F;
			this.Label4.Width = 1F;
			// 
			// txtmuni
			// 
			this.txtmuni.Height = 0.1875F;
			this.txtmuni.Left = 0.0625F;
			this.txtmuni.Name = "txtmuni";
			this.txtmuni.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmuni.Text = null;
			this.txtmuni.Top = 0F;
			this.txtmuni.Width = 1.4375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0F;
			this.Field1.Left = 1.8125F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 8pt; ddo-char-set: 1";
			this.Field1.Text = "Field1";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 0F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 2F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field2.Text = "AUDIT OF BILLING AMOUNTS";
			this.Field2.Top = 0F;
			this.Field2.Width = 3.125F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.1875F;
			this.txtdate.Left = 6.1875F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtdate.Text = null;
			this.txtdate.Top = 0F;
			this.txtdate.Width = 1.1875F;
			// 
			// txtpage
			// 
			this.txtpage.Height = 0.1875F;
			this.txtpage.Left = 6.4375F;
			this.txtpage.Name = "txtpage";
			this.txtpage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtpage.Text = null;
			this.txtpage.Top = 0.1875F;
			this.txtpage.Width = 0.9375F;
			// 
			// txtorderby
			// 
			this.txtorderby.Height = 0.1875F;
			this.txtorderby.Left = 2.1875F;
			this.txtorderby.Name = "txtorderby";
			this.txtorderby.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.txtorderby.Text = null;
			this.txtorderby.Top = 0.1875F;
			this.txtorderby.Width = 2.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.grpbinder,
				this.txtgroupname
			});
			this.GroupHeader1.DataField = "thebinder";
			this.GroupHeader1.Height = 0.1875F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// grpbinder
			// 
			this.grpbinder.DataField = "thebinder";
			this.grpbinder.Height = 0.1875F;
			this.grpbinder.Left = 0F;
			this.grpbinder.Name = "grpbinder";
			this.grpbinder.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.grpbinder.Text = null;
			this.grpbinder.Top = 0F;
			this.grpbinder.Visible = false;
			this.grpbinder.Width = 1.3125F;
			// 
			// txtgroupname
			// 
			this.txtgroupname.Height = 0.1875F;
			this.txtgroupname.Left = 1.9375F;
			this.txtgroupname.Name = "txtgroupname";
			this.txtgroupname.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtgroupname.Text = null;
			this.txtgroupname.Top = 0F;
			this.txtgroupname.Width = 3.5F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field3,
				this.txtsubland,
				this.txtsubbldg,
				this.txtsubexempt,
				this.txtsubtotal,
				this.Field6,
				this.txtNumAccts,
				this.Field10,
				this.txtnumtotexempts,
				this.Line1
			});
			this.GroupFooter1.Height = 0.7604167F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.BeforePrint += new System.EventHandler(this.GroupFooter1_BeforePrint);
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.0625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.Field3.Text = "Subtotal";
			this.Field3.Top = 0.125F;
			this.Field3.Width = 0.9375F;
			// 
			// txtsubland
			// 
			this.txtsubland.Height = 0.1875F;
			this.txtsubland.Left = 2.6875F;
			this.txtsubland.Name = "txtsubland";
			this.txtsubland.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtsubland.Text = null;
			this.txtsubland.Top = 0.125F;
			this.txtsubland.Width = 1.25F;
			// 
			// txtsubbldg
			// 
			this.txtsubbldg.Height = 0.1875F;
			this.txtsubbldg.Left = 4F;
			this.txtsubbldg.Name = "txtsubbldg";
			this.txtsubbldg.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtsubbldg.Text = null;
			this.txtsubbldg.Top = 0.125F;
			this.txtsubbldg.Width = 1.25F;
			// 
			// txtsubexempt
			// 
			this.txtsubexempt.Height = 0.1875F;
			this.txtsubexempt.Left = 5.25F;
			this.txtsubexempt.Name = "txtsubexempt";
			this.txtsubexempt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtsubexempt.Text = null;
			this.txtsubexempt.Top = 0.125F;
			this.txtsubexempt.Width = 1.125F;
			// 
			// txtsubtotal
			// 
			this.txtsubtotal.Height = 0.1875F;
			this.txtsubtotal.Left = 6.375F;
			this.txtsubtotal.Name = "txtsubtotal";
			this.txtsubtotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtsubtotal.Text = null;
			this.txtsubtotal.Top = 0.125F;
			this.txtsubtotal.Width = 1.0625F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 0.0625F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.Field6.Text = "No. of Accts.";
			this.Field6.Top = 0.375F;
			this.Field6.Width = 0.8125F;
			// 
			// txtNumAccts
			// 
			this.txtNumAccts.Height = 0.1875F;
			this.txtNumAccts.Left = 0.9375F;
			this.txtNumAccts.Name = "txtNumAccts";
			this.txtNumAccts.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtNumAccts.Text = null;
			this.txtNumAccts.Top = 0.375F;
			this.txtNumAccts.Width = 0.6875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 1.875F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.Field10.Text = "Totally Exempt Accts";
			this.Field10.Top = 0.375F;
			this.Field10.Width = 1.25F;
			// 
			// txtnumtotexempts
			// 
			this.txtnumtotexempts.Height = 0.1875F;
			this.txtnumtotexempts.Left = 3.25F;
			this.txtnumtotexempts.Name = "txtnumtotexempts";
			this.txtnumtotexempts.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtnumtotexempts.Text = null;
			this.txtnumtotexempts.Top = 0.375F;
			this.txtnumtotexempts.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 7.25F;
			this.Line1.X1 = 0.125F;
			this.Line1.X2 = 7.375F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// rptaudit
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.4861111F;
			this.PageSettings.Margins.Right = 0.4861111F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfinaltotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotbuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotexemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotassessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotalaccts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalnumaccts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotallyexempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotnumtotexempts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtorderby)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.grpbinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtgroupname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubexempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsubtotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumAccts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnumtotexempts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmaplot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtexemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtfinaltotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotbuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotexemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotassessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotalaccts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalnumaccts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotaltotallyexempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotnumtotexempts;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtorderby;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox grpbinder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgroupname;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsubland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsubbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsubexempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsubtotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumAccts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtnumtotexempts;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
