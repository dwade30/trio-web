﻿namespace TWXF0000
{
	public class modGlobalVariables
	{
		//=========================================================
		public const uint HKEY_LOCAL_MACHINE = 0x80000001;
		public const uint HKEY_MACHINE = 0x80000002;
		public const int REG_SZ = 1;
		// Unicode nul terminated string
		public const int REG_DWORD = 4;
		public const int CNSTUPDATEAMOUNT = 0;
		public const int CNSTUPDATEINFOONLY = 1;

		public class StaticVariables
		{
			//Fecher vbPorter - Version 1.0.0.59
			public string strREDatabase = string.Empty;
			public cREXferSetting CustomizedInfo = new cREXferSetting();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
