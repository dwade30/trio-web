﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for Frmcalcshowassessment.
	/// </summary>
	public partial class Frmcalcshowassessment : BaseForm
	{
		public Frmcalcshowassessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static Frmcalcshowassessment InstancePtr
		{
			get
			{
				return (Frmcalcshowassessment)Sys.GetInstance(typeof(Frmcalcshowassessment));
			}
		}

		protected Frmcalcshowassessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTRANGEALL = -1;
		const int CNSTRANGERANGE = -2;
		const int CNSTRANGESPECIFIC = -3;
		const int CNSTRANGEEXTRACT = -4;

		private void FillRangeCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbRange.Clear();
			cmbRange.AddItem("All");
			cmbRange.ItemData(cmbRange.NewIndex, CNSTRANGEALL);
			cmbRange.AddItem("Range");
			cmbRange.ItemData(cmbRange.NewIndex, CNSTRANGERANGE);
			cmbRange.AddItem("Specific");
			cmbRange.ItemData(cmbRange.NewIndex, CNSTRANGESPECIFIC);
			cmbRange.AddItem("From Extract List");
			cmbRange.ItemData(cmbRange.NewIndex, CNSTRANGEEXTRACT);
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblRegions where townnumber > 0 order by townnumber", modGlobalVariables.Statics.strGNDatabase);
				while (!clsLoad.EndOfFile())
				{
					cmbRange.AddItem(clsLoad.Get_Fields_String("townname"));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					cmbRange.ItemData(cmbRange.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("townnumber"))));
					clsLoad.MoveNext();
				}
			}
			cmbRange.SelectedIndex = 0;
		}

		private void cmdCancel_Click()
		{
			modGlobalVariables.Statics.CancelledIt = true;
			modGlobalVariables.Statics.boolUseArrays = false;
			Frmcalcshowassessment.InstancePtr.Unload();
		}

		private void cmdOkay_Click()
		{
			string tstring = "";
			string strTemp;
			clsDRWrapper clsSave = new clsDRWrapper();
			int intTownCode;
			int intOrder = 0;
			int intRType = 0;
			modGlobalVariables.Statics.CancelledIt = false;

			strTemp = "";
			intTownCode = 0;
			int vbPorterVar = cmbRange.ItemData(cmbRange.SelectedIndex);
			if (vbPorterVar == CNSTRANGEALL)
			{
				modGlobalVariables.Statics.boolByRange = false;
				// boolByTown = False
				intRType = 0;
			}
			else if (vbPorterVar == CNSTRANGERANGE)
			{
				modGlobalVariables.Statics.boolByRange = true;
				// boolByTown = False
				intRType = 1;
			}
			else if (vbPorterVar == CNSTRANGESPECIFIC)
			{
				modGlobalVariables.Statics.boolByRange = false;
				intRType = 2;
			}
			else if (vbPorterVar == CNSTRANGEEXTRACT)
			{
				modGlobalVariables.Statics.boolByRange = false;
				intRType = 3;
			}
			else
			{
				intRType = 4;
				modGlobalVariables.Statics.boolByRange = false;
				// boolByTown = True
				intTownCode = cmbRange.ItemData(cmbRange.SelectedIndex);
			}
			if (cmbpresentation.Text == "Account Number")
			{
				modPrintRoutines.Statics.gstrFieldName = "RSAccount";
				modPrintRoutines.Statics.intwhichorder = 1;
				intOrder = 1;
			}
			else if (cmbpresentation.Text == "Name")
			{
				modPrintRoutines.Statics.gstrFieldName = "DeedName1";
				modPrintRoutines.Statics.intwhichorder = 2;
				intOrder = 2;
			}
			else if (cmbpresentation.Text == "Map/Lot Number")
			{
				modPrintRoutines.Statics.gstrFieldName = "RSMapLot";
				modPrintRoutines.Statics.intwhichorder = 3;
				intOrder = 3;
			}
			else if (cmbpresentation.Text == "Location")
			{
				modPrintRoutines.Statics.gstrFieldName = "RSLocStreet";
				modPrintRoutines.Statics.intwhichorder = 4;
				intOrder = 4;
			}
			modPrintRoutines.Statics.gstrMinAccountRange = "";
			modPrintRoutines.Statics.gstrMaxAccountRange = "ZZZZ";
			modGlobalVariables.Statics.gintMinAccountRange = 1;
			modGlobalVariables.Statics.gintMaxAccountRange = 2147483000;
			// almost a max long
			modSpeedCalc.Statics.boolCalcErrors = false;
			modSpeedCalc.Statics.CalcLog = "";
			if (cmbtupdate.Text == "Update")
			{
				modGlobalVariables.Statics.boolUpdateWhenCalculate = true;
				strTemp = "updated";
			}
			else
			{
				modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
				strTemp = "not updated";
			}
			switch (intRType)
			{
				case 0:
					{
						// all
						tstring = " by all";
						break;
					}
				case 1:
					{
						// range
						tstring = " by range";
						if (intOrder == 1)
						{
							modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStart.Text)));
							modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(txtEnd.Text)));
							if (modGlobalVariables.Statics.gintMaxAccountRange < modGlobalVariables.Statics.gintMinAccountRange || modGlobalVariables.Statics.gintMinAccountRange <= 0)
							{
								MessageBox.Show("This is an invalid range" + "\r\n" + "Please try another range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						else
						{
							modPrintRoutines.Statics.gstrMinAccountRange = Strings.Trim(txtStart.Text);
							modPrintRoutines.Statics.gstrMaxAccountRange = Strings.Trim(txtEnd.Text);
							if (modPrintRoutines.Statics.gstrMaxAccountRange == string.Empty)
								modPrintRoutines.Statics.gstrMaxAccountRange = modPrintRoutines.Statics.gstrMinAccountRange;
							if (modPrintRoutines.Statics.gstrMinAccountRange == string.Empty || Strings.CompareString(modPrintRoutines.Statics.gstrMaxAccountRange, "<", modPrintRoutines.Statics.gstrMinAccountRange))
							{
								MessageBox.Show("This is an invalid range" + "\r\n" + "Please try another range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							if (cmbpresentation.Text != "Map/Lot Number")
							{
								modPrintRoutines.Statics.gstrMaxAccountRange += "zzz";
							}
						}
						break;
					}
				case 2:
					{
						tstring = " by specific";
						modGlobalVariables.Statics.CancelledIt = false;
						frmGetAccountsToPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
						if (modGlobalVariables.Statics.CancelledIt)
							return;
						break;
					}
				case 3:
					{
						// extract
						tstring = " by extract";
						break;
					}
				case 4:
					{
						// town
						tstring = " for " + modRegionalTown.GetTownKeyName_2(intTownCode);
						break;
					}
			}
			
			if (!modGlobalVariables.Statics.CancelledIt)
			{
				// Call clsSave.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_OpID & "',#" & Now & "#,'Batch Calculation','Batch Calculation " & tstring & ", files were " & strTemp & ".')", strredatabase)
				modGlobalFunctions.AddCYAEntry_26("RE", "Batch Calculation", "Batch Calculation " + tstring + ", files were " + strTemp);
				if (modGlobalVariables.Statics.boolUpdateWhenCalculate)
				{
					clsSave.Execute("update status set batchcalc = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
				}
				modProperty.Statics.RUNTYPE = "P";
				if (cmbReport.Text == "Valuation")
				{
                    if (!chkExportPDFs.Checked)
                    {
                        rptRangeValuation.InstancePtr.Init(intRType, intOrder, this.Modal, intTownCode, false,
                            cmbshow.Text == "Changes Only");
                    }
                    else
                    {
                        int lngUID;
                        lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                        var rsList = new clsDRWrapper();
                        int lngRecCount;
                        int lngCurrent;
                        switch (intRType)
                        {
                            case 0:
                                rsList.OpenRecordset(
                                    "select rsaccount from master where isnull(rsdeleted,0) = 0 and rscard = 1",
                                    "RealEstate");
                                break;
                            case 1:
                                if (modPrintRoutines.Statics.gstrFieldName.ToLower() == "rsaccount")
                                {
                                    rsList.OpenRecordset("select rsaccount from master where isnull(rsdeleted,0) = 0 and rscard = 1 and rsaccount between " +
                                        modGlobalVariables.Statics.gintMinAccountRange + " AND " + modGlobalVariables.Statics.gintMaxAccountRange,
                                        "RealEstate");
                                }
                                else
                                {
                                    rsList.OpenRecordset(
                                        "select rsaccount from master where isnull(rsdeleted,0) = 0 and rscard = 1 and " +
                                        modPrintRoutines.Statics.gstrFieldName + " >= '" +
                                        modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                                        modPrintRoutines.Statics.gstrFieldName + " <= '" +
                                        modPrintRoutines.Statics.gstrMaxAccountRange + "'", "RealEstate");
                                }
                                break;
                            case 2:
                                rsList.OpenRecordset(
                                    "select accountnumber as rsaccount from labelaccounts where userid = " + lngUID,
                                    "RealEstate");
                                break;
                            case 3:
                                rsList.OpenRecordset(
                                    "select ACCOUNTNUMBER as rsaccount from extracttable where userid = " + lngUID,
                                    "RealEstate");
                                break;
                            case 4:
                                rsList.OpenRecordset(
                                    "select rsaccount from master where isnull(rsdeleted,0) = 0 and rscard = 1 and ritrancode = " +
                                    intTownCode.ToString() + " order by " + modPrintRoutines.Statics.gstrFieldName,
                                    "RealEstate");
                                break;
                        }

                        if (!rsList.EndOfFile())
                        {
                            lngCurrent = 0;
                            lngRecCount = rsList.RecordCount();
                            rsList.MoveFirst();
                            var pdfFolder = GetTempFolderName();
                            while (!rsList.EndOfFile())
                            {
                                lngCurrent = lngCurrent + 1;
                                // bWait.Message = "Exporting " & lngCurrent & " of " & lngRecCount

                                modGlobalVariables.Statics.gintMinAccountRange = rsList.Get_Fields_Int32("rsaccount");
                                modGlobalVariables.Statics.gintMaxAccountRange =
                                    modGlobalVariables.Statics.gintMinAccountRange;
                                var rrv = new rptRangeValuation();
                                if (lngCurrent < lngRecCount)
                                {
                                    rrv.Init(1, 1, false, intTownCode, false, false, null, true, true, pdfFolder);
                                }
                                else
                                {
                                    rrv.Init(1, 1, false, intTownCode, false, false, () => ZipPdfs(pdfFolder), true, true, pdfFolder);
                                }
                                rsList.MoveNext();
                            }
                            
                        }
                        else
                        {
                            MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK);
                        }
                        return;
                    }
                }
				else if (cmbReport.Text == "One Page Card")
				{
					rptRange1PageProperty.InstancePtr.Init(ref intRType, ref intOrder, this.Modal, intTownCode, true, false);
				}
				else
				{
					rptNewAssessment.InstancePtr.Init(ref intRType, ref intOrder, intTownCode);
				}
				Frmcalcshowassessment.InstancePtr.Unload();
			}
			else
			{
				modGlobalVariables.Statics.boolUseArrays = false;
			}
		}

        private void ZipPdfs(string pdfFolder)
        {
            var zipFile = GetTempZipName();
            ZipFile.CreateFromDirectory(pdfFolder, zipFile);
            FCUtils.Download( zipFile);
            //FCUtils.DownloadAndOpen("_blank",zipFile);
            
        }

        private void Frmcalcshowassessment_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void Frmcalcshowassessment_Load(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			modPrintRoutines.Statics.gstrFieldName = "RSAccount";
			modPrintRoutines.Statics.intwhichorder = 1;
			modPrintRoutines.Statics.regorchanged = "reg";
			FillRangeCombo();
			if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTACCEPTCORRELATION, false))
			{
				cmbtupdate.Clear();
				cmbtupdate.Items.Add("Don't Update");
				cmbtupdate.Text = "Don't Update";
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdOkay_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optpresentation_CheckedChanged(int Index, object sender, System.EventArgs e)
		{

		}

		private void optpresentation_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbpresentation.SelectedIndex;
			optpresentation_CheckedChanged(index, sender, e);
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{

		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void optshow_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				modPrintRoutines.Statics.regorchanged = "reg";
			}
			else
			{
				modPrintRoutines.Statics.regorchanged = "chg";
			}
		}

		private void optshow_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbshow.SelectedIndex;
			optshow_CheckedChanged(index, sender, e);
		}

		private void Optupdate_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// boolupdatewhencalculated = Optupdate(0).Value
		}

		private void Optupdate_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtupdate.SelectedIndex;
			Optupdate_CheckedChanged(index, sender, e);
		}

		private void cmbpresentation_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbpresentation.SelectedIndex == 0)
			{
				optpresentation_CheckedChanged(sender, e);
			}
			else if (cmbpresentation.SelectedIndex == 1)
			{
				optpresentation_CheckedChanged(sender, e);
			}
		}

		private void cmbshow_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbshow.SelectedIndex == 0)
			{
				optshow_CheckedChanged(sender, e);
			}
			else if (cmbshow.SelectedIndex == 1)
			{
				optshow_CheckedChanged(sender, e);
			}
		}

		private void cmbtupdate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbtupdate.SelectedIndex == 0)
			{
				Optupdate_CheckedChanged(sender, e);
			}
			else if (cmbtupdate.SelectedIndex == 1)
			{
				Optupdate_CheckedChanged(sender, e);
			}
		}

		private void cmbReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            if (cmbReport.SelectedIndex == 1)
            {
                chkExportPDFs.Visible = true;
            }
            else
            {
                chkExportPDFs.Visible = false;
            }
			if (cmbReport.SelectedIndex == 0)
			{
				optReport_CheckedChanged(sender, e);
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				optReport_CheckedChanged(sender, e);
			}
			else if (cmbReport.SelectedIndex == 2)
			{
				optReport_CheckedChanged(sender, e);
			}
		}

        private string GetTempFolderName()
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            string tempFolder = GetBaseTempFolder();
            Guid guid = Guid.NewGuid();
            string tempFolderName = guid.ToString();
            string tempFolderPath = Path.Combine(tempFolder, tempFolderName);
            if (!Directory.Exists(tempFolderPath))
            {
                Directory.CreateDirectory(tempFolderPath);
            }
            return tempFolderPath;
        }

        private string GetBaseTempFolder()
        {
            string tempFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }

            return tempFolder;
        }

        private string GetTempZipName()
        {
            string tempFolder = GetBaseTempFolder();
            Guid guid = Guid.NewGuid();
            string fileName = "Valuations" + guid.ToString() + ".zip";
            return Path.Combine(tempFolder, fileName);
        }
    }
}
