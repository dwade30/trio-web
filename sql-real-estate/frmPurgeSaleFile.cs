﻿//Fecher vbPorter - Version 1.0.0.40
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using Global;
using System;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPurgeSaleFile.
	/// </summary>
	public partial class frmPurgeSaleFile : BaseForm
	{
		public frmPurgeSaleFile()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgeSaleFile InstancePtr
		{
			get
			{
				return (frmPurgeSaleFile)Sys.GetInstance(typeof(frmPurgeSaleFile));
			}
		}

		protected frmPurgeSaleFile _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rsDelete;

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			string strDate;
			object ans;
			clsDRWrapper clsSave = new clsDRWrapper();
			// strDate = mebDate.Text
			strDate = t2kDate.Text;
			if (strDate.Length == 10)
			{
				// strDate = Mid(strDate, 1, 2) & "/" & Mid(strDate, 3, 6)
				// strDate = Mid(strDate, 1, 5) & "/" & Mid(strDate, 6, 4)
				if (Information.IsDate(strDate) == true)
				{
					// Call clsSave.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_OpID & "',#" & Now & "#,'Purge Sale File','Purged from " & strDate & "')", strredatabase)
					modGlobalFunctions.AddCYAEntry_26("RE", "Purge Sale File", "Purged from " + strDate);
					clsSave.Execute("Delete  from srmaster where saledate <= '" + Strings.Format(strDate, "MM/dd/yyyy") + "'", modGlobalVariables.strREDatabase);
					clsSave.Execute("Delete  from srDwelling where saledate <= '" + Strings.Format(strDate, "MM/dd/yyyy") + "'", modGlobalVariables.strREDatabase);
					clsSave.Execute("Delete  from srCommercial where saledate <= '" + Strings.Format(strDate, "MM/dd/yyyy") + "'", modGlobalVariables.strREDatabase);
					clsSave.Execute("Delete  from sroutbuilding where saledate <= '" + Strings.Format(strDate, "MM/dd/yyyy") + "'", modGlobalVariables.strREDatabase);
					MessageBox.Show("Purge of Sale file completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //FC:FINAL:AM:#3374 - don't close the form
                    //frmPurgeSaleFile.InstancePtr.Unload();
                    t2kDate.Text = "";
				}
				else
				{
					MessageBox.Show(strDate + " is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// mebDate.Text = ""
					// mebDate.SetFocus
				}
			}
			else
			{
				MessageBox.Show("Incorrect Date.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			frmPurgeSaleFile.InstancePtr.Unload();
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void frmPurgeSaleFile_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPurgeSaleFile.InstancePtr.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPurgeSaleFile_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeSaleFile properties;
			//frmPurgeSaleFile.ScaleWidth	= 3885;
			//frmPurgeSaleFile.ScaleHeight	= 2325;
			//frmPurgeSaleFile.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			//Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// menuSRMain.Show
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuPurge_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}
	}
}
