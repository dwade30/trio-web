﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class Modexemptionrpt
	{
		//=========================================================
		public struct ExemptionAudit
		{
			public int AccountNumber;
			public string Name;
			public short[] Code/*?  = new short[3 + 1] */;
			public int Land;
			// vbPorter upgrade warning: Building As int	OnWrite(short, double)
			public int Building;
			public int assessment;
			public int[] Exemption/*?  = new int[3 + 1] */;
			// vbPorter upgrade warning: totalexemption As int	OnWrite(short, double)
			public int totalexemption;
			public string MapLot;
			public string Location;
			public short beenprinted;
			public bool booltotallyexempt;
			public int next;
			public int previous;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public ExemptionAudit(int unusedParam)
			{
				this.AccountNumber = 0;
				this.Name = string.Empty;
				this.Code = new short[4];
				this.Land = 0;
				this.Building = 0;
				this.assessment = 0;
				this.Exemption = new int[4];
				this.totalexemption = 0;
				this.MapLot = string.Empty;
				this.Location = string.Empty;
				this.beenprinted = 0;
				this.booltotallyexempt = false;
				this.next = 0;
				this.previous = 0;
			}
		};

		public static void Example()
		{
			clsDRWrapper rs = new clsDRWrapper();
			ExemptionAudit[] AuditReport = null;
			int intCounter;
			rs.OpenRecordset("Select * from Master where RLExemption > 0", modGlobalVariables.strREDatabase);
			rs.MoveLast();
			AuditReport = new ExemptionAudit[rs.RecordCount() + 1];
			rs.MoveFirst();
			intCounter = 0;
			while (!rs.EndOfFile())
			{
				AuditReport[intCounter].AccountNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("RSAccount"));
				rs.MoveNext();
				intCounter += 1;
			}
		}

		//public class StaticVariables
		//{
		//	public int exemptcodetouse;
		//}

		//public static StaticVariables Statics
		//{
		//	get
		//	{
		//		return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
		//	}
		//}
	}
}
